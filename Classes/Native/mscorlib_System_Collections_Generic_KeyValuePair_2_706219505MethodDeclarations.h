﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21341048298MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3107995795(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t706219505 *, int32_t, OnMoveCallBackFun_t3535987578 *, const MethodInfo*))KeyValuePair_2__ctor_m3766851247_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::get_Key()
#define KeyValuePair_2_get_Key_m2992075125(__this, method) ((  int32_t (*) (KeyValuePair_2_t706219505 *, const MethodInfo*))KeyValuePair_2_get_Key_m2147945433_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3994536374(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t706219505 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m1306792474_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::get_Value()
#define KeyValuePair_2_get_Value_m2302850905(__this, method) ((  OnMoveCallBackFun_t3535987578 * (*) (KeyValuePair_2_t706219505 *, const MethodInfo*))KeyValuePair_2_get_Value_m3787238589_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m426463926(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t706219505 *, OnMoveCallBackFun_t3535987578 *, const MethodInfo*))KeyValuePair_2_set_Value_m2941569306_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::ToString()
#define KeyValuePair_2_ToString_m1147355346(__this, method) ((  String_t* (*) (KeyValuePair_2_t706219505 *, const MethodInfo*))KeyValuePair_2_ToString_m3413285102_gshared)(__this, method)
