﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._5a831d133e768c3c948101db67b391b5
struct _5a831d133e768c3c948101db67b391b5_t3525352721;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__5a831d133e768c3c948101db3525352721.h"

// System.Void Little._5a831d133e768c3c948101db67b391b5::.ctor()
extern "C"  void _5a831d133e768c3c948101db67b391b5__ctor_m2430839868 (_5a831d133e768c3c948101db67b391b5_t3525352721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._5a831d133e768c3c948101db67b391b5::_5a831d133e768c3c948101db67b391b5m2(System.Int32)
extern "C"  int32_t _5a831d133e768c3c948101db67b391b5__5a831d133e768c3c948101db67b391b5m2_m3741052921 (_5a831d133e768c3c948101db67b391b5_t3525352721 * __this, int32_t ____5a831d133e768c3c948101db67b391b5a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._5a831d133e768c3c948101db67b391b5::_5a831d133e768c3c948101db67b391b5m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _5a831d133e768c3c948101db67b391b5__5a831d133e768c3c948101db67b391b5m_m1448977117 (_5a831d133e768c3c948101db67b391b5_t3525352721 * __this, int32_t ____5a831d133e768c3c948101db67b391b5a0, int32_t ____5a831d133e768c3c948101db67b391b5171, int32_t ____5a831d133e768c3c948101db67b391b5c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._5a831d133e768c3c948101db67b391b5::ilo__5a831d133e768c3c948101db67b391b5m21(Little._5a831d133e768c3c948101db67b391b5,System.Int32)
extern "C"  int32_t _5a831d133e768c3c948101db67b391b5_ilo__5a831d133e768c3c948101db67b391b5m21_m1723635704 (Il2CppObject * __this /* static, unused */, _5a831d133e768c3c948101db67b391b5_t3525352721 * ____this0, int32_t ____5a831d133e768c3c948101db67b391b5a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
