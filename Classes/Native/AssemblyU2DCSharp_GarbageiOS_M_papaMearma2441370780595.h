﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_papaMearma244
struct  M_papaMearma244_t1370780595  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_papaMearma244::_raltesaiBemtordra
	float ____raltesaiBemtordra_0;
	// System.String GarbageiOS.M_papaMearma244::_jagairyouHagow
	String_t* ____jagairyouHagow_1;
	// System.UInt32 GarbageiOS.M_papaMearma244::_xolereCheazur
	uint32_t ____xolereCheazur_2;
	// System.Int32 GarbageiOS.M_papaMearma244::_tawacouJowsufay
	int32_t ____tawacouJowsufay_3;
	// System.String GarbageiOS.M_papaMearma244::_qisasnear
	String_t* ____qisasnear_4;
	// System.String GarbageiOS.M_papaMearma244::_xaimouPaicone
	String_t* ____xaimouPaicone_5;
	// System.Int32 GarbageiOS.M_papaMearma244::_fabeariNairparne
	int32_t ____fabeariNairparne_6;
	// System.Int32 GarbageiOS.M_papaMearma244::_gairwhawnereMirce
	int32_t ____gairwhawnereMirce_7;
	// System.String GarbageiOS.M_papaMearma244::_sairlasKerai
	String_t* ____sairlasKerai_8;
	// System.UInt32 GarbageiOS.M_papaMearma244::_bowtujall
	uint32_t ____bowtujall_9;
	// System.Boolean GarbageiOS.M_papaMearma244::_kerur
	bool ____kerur_10;
	// System.Boolean GarbageiOS.M_papaMearma244::_xostouLashofer
	bool ____xostouLashofer_11;
	// System.Single GarbageiOS.M_papaMearma244::_tallloupaw
	float ____tallloupaw_12;
	// System.Single GarbageiOS.M_papaMearma244::_yudomear
	float ____yudomear_13;
	// System.UInt32 GarbageiOS.M_papaMearma244::_jounawcaw
	uint32_t ____jounawcaw_14;
	// System.Single GarbageiOS.M_papaMearma244::_lernearjeGelpo
	float ____lernearjeGelpo_15;
	// System.Single GarbageiOS.M_papaMearma244::_lawri
	float ____lawri_16;
	// System.UInt32 GarbageiOS.M_papaMearma244::_cahogor
	uint32_t ____cahogor_17;
	// System.Single GarbageiOS.M_papaMearma244::_trarsu
	float ____trarsu_18;
	// System.Single GarbageiOS.M_papaMearma244::_rerre
	float ____rerre_19;
	// System.Single GarbageiOS.M_papaMearma244::_hakadas
	float ____hakadas_20;
	// System.Single GarbageiOS.M_papaMearma244::_moupumorDrayler
	float ____moupumorDrayler_21;
	// System.UInt32 GarbageiOS.M_papaMearma244::_bujarmar
	uint32_t ____bujarmar_22;
	// System.Boolean GarbageiOS.M_papaMearma244::_sairta
	bool ____sairta_23;
	// System.Int32 GarbageiOS.M_papaMearma244::_saterdreBouhape
	int32_t ____saterdreBouhape_24;
	// System.UInt32 GarbageiOS.M_papaMearma244::_yoojaslarBairkair
	uint32_t ____yoojaslarBairkair_25;
	// System.UInt32 GarbageiOS.M_papaMearma244::_raistou
	uint32_t ____raistou_26;
	// System.String GarbageiOS.M_papaMearma244::_trirwener
	String_t* ____trirwener_27;
	// System.UInt32 GarbageiOS.M_papaMearma244::_setaMircea
	uint32_t ____setaMircea_28;
	// System.Boolean GarbageiOS.M_papaMearma244::_biszeedrou
	bool ____biszeedrou_29;
	// System.Boolean GarbageiOS.M_papaMearma244::_paimaykiFerrepor
	bool ____paimaykiFerrepor_30;
	// System.Int32 GarbageiOS.M_papaMearma244::_silalXerzer
	int32_t ____silalXerzer_31;
	// System.String GarbageiOS.M_papaMearma244::_rutiMirvem
	String_t* ____rutiMirvem_32;
	// System.Single GarbageiOS.M_papaMearma244::_gichiva
	float ____gichiva_33;
	// System.String GarbageiOS.M_papaMearma244::_kegajirVallartou
	String_t* ____kegajirVallartou_34;
	// System.Boolean GarbageiOS.M_papaMearma244::_setou
	bool ____setou_35;
	// System.Boolean GarbageiOS.M_papaMearma244::_nayhallstuJestel
	bool ____nayhallstuJestel_36;
	// System.Single GarbageiOS.M_papaMearma244::_sadouDrorde
	float ____sadouDrorde_37;
	// System.Boolean GarbageiOS.M_papaMearma244::_drearmeJefear
	bool ____drearmeJefear_38;
	// System.UInt32 GarbageiOS.M_papaMearma244::_katrounemForkibor
	uint32_t ____katrounemForkibor_39;
	// System.UInt32 GarbageiOS.M_papaMearma244::_poujer
	uint32_t ____poujer_40;
	// System.Single GarbageiOS.M_papaMearma244::_mawiSouzace
	float ____mawiSouzace_41;
	// System.String GarbageiOS.M_papaMearma244::_dacha
	String_t* ____dacha_42;
	// System.Boolean GarbageiOS.M_papaMearma244::_meartreWemem
	bool ____meartreWemem_43;
	// System.Single GarbageiOS.M_papaMearma244::_nelwoo
	float ____nelwoo_44;
	// System.Boolean GarbageiOS.M_papaMearma244::_whainislai
	bool ____whainislai_45;

public:
	inline static int32_t get_offset_of__raltesaiBemtordra_0() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____raltesaiBemtordra_0)); }
	inline float get__raltesaiBemtordra_0() const { return ____raltesaiBemtordra_0; }
	inline float* get_address_of__raltesaiBemtordra_0() { return &____raltesaiBemtordra_0; }
	inline void set__raltesaiBemtordra_0(float value)
	{
		____raltesaiBemtordra_0 = value;
	}

	inline static int32_t get_offset_of__jagairyouHagow_1() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____jagairyouHagow_1)); }
	inline String_t* get__jagairyouHagow_1() const { return ____jagairyouHagow_1; }
	inline String_t** get_address_of__jagairyouHagow_1() { return &____jagairyouHagow_1; }
	inline void set__jagairyouHagow_1(String_t* value)
	{
		____jagairyouHagow_1 = value;
		Il2CppCodeGenWriteBarrier(&____jagairyouHagow_1, value);
	}

	inline static int32_t get_offset_of__xolereCheazur_2() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____xolereCheazur_2)); }
	inline uint32_t get__xolereCheazur_2() const { return ____xolereCheazur_2; }
	inline uint32_t* get_address_of__xolereCheazur_2() { return &____xolereCheazur_2; }
	inline void set__xolereCheazur_2(uint32_t value)
	{
		____xolereCheazur_2 = value;
	}

	inline static int32_t get_offset_of__tawacouJowsufay_3() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____tawacouJowsufay_3)); }
	inline int32_t get__tawacouJowsufay_3() const { return ____tawacouJowsufay_3; }
	inline int32_t* get_address_of__tawacouJowsufay_3() { return &____tawacouJowsufay_3; }
	inline void set__tawacouJowsufay_3(int32_t value)
	{
		____tawacouJowsufay_3 = value;
	}

	inline static int32_t get_offset_of__qisasnear_4() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____qisasnear_4)); }
	inline String_t* get__qisasnear_4() const { return ____qisasnear_4; }
	inline String_t** get_address_of__qisasnear_4() { return &____qisasnear_4; }
	inline void set__qisasnear_4(String_t* value)
	{
		____qisasnear_4 = value;
		Il2CppCodeGenWriteBarrier(&____qisasnear_4, value);
	}

	inline static int32_t get_offset_of__xaimouPaicone_5() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____xaimouPaicone_5)); }
	inline String_t* get__xaimouPaicone_5() const { return ____xaimouPaicone_5; }
	inline String_t** get_address_of__xaimouPaicone_5() { return &____xaimouPaicone_5; }
	inline void set__xaimouPaicone_5(String_t* value)
	{
		____xaimouPaicone_5 = value;
		Il2CppCodeGenWriteBarrier(&____xaimouPaicone_5, value);
	}

	inline static int32_t get_offset_of__fabeariNairparne_6() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____fabeariNairparne_6)); }
	inline int32_t get__fabeariNairparne_6() const { return ____fabeariNairparne_6; }
	inline int32_t* get_address_of__fabeariNairparne_6() { return &____fabeariNairparne_6; }
	inline void set__fabeariNairparne_6(int32_t value)
	{
		____fabeariNairparne_6 = value;
	}

	inline static int32_t get_offset_of__gairwhawnereMirce_7() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____gairwhawnereMirce_7)); }
	inline int32_t get__gairwhawnereMirce_7() const { return ____gairwhawnereMirce_7; }
	inline int32_t* get_address_of__gairwhawnereMirce_7() { return &____gairwhawnereMirce_7; }
	inline void set__gairwhawnereMirce_7(int32_t value)
	{
		____gairwhawnereMirce_7 = value;
	}

	inline static int32_t get_offset_of__sairlasKerai_8() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____sairlasKerai_8)); }
	inline String_t* get__sairlasKerai_8() const { return ____sairlasKerai_8; }
	inline String_t** get_address_of__sairlasKerai_8() { return &____sairlasKerai_8; }
	inline void set__sairlasKerai_8(String_t* value)
	{
		____sairlasKerai_8 = value;
		Il2CppCodeGenWriteBarrier(&____sairlasKerai_8, value);
	}

	inline static int32_t get_offset_of__bowtujall_9() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____bowtujall_9)); }
	inline uint32_t get__bowtujall_9() const { return ____bowtujall_9; }
	inline uint32_t* get_address_of__bowtujall_9() { return &____bowtujall_9; }
	inline void set__bowtujall_9(uint32_t value)
	{
		____bowtujall_9 = value;
	}

	inline static int32_t get_offset_of__kerur_10() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____kerur_10)); }
	inline bool get__kerur_10() const { return ____kerur_10; }
	inline bool* get_address_of__kerur_10() { return &____kerur_10; }
	inline void set__kerur_10(bool value)
	{
		____kerur_10 = value;
	}

	inline static int32_t get_offset_of__xostouLashofer_11() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____xostouLashofer_11)); }
	inline bool get__xostouLashofer_11() const { return ____xostouLashofer_11; }
	inline bool* get_address_of__xostouLashofer_11() { return &____xostouLashofer_11; }
	inline void set__xostouLashofer_11(bool value)
	{
		____xostouLashofer_11 = value;
	}

	inline static int32_t get_offset_of__tallloupaw_12() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____tallloupaw_12)); }
	inline float get__tallloupaw_12() const { return ____tallloupaw_12; }
	inline float* get_address_of__tallloupaw_12() { return &____tallloupaw_12; }
	inline void set__tallloupaw_12(float value)
	{
		____tallloupaw_12 = value;
	}

	inline static int32_t get_offset_of__yudomear_13() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____yudomear_13)); }
	inline float get__yudomear_13() const { return ____yudomear_13; }
	inline float* get_address_of__yudomear_13() { return &____yudomear_13; }
	inline void set__yudomear_13(float value)
	{
		____yudomear_13 = value;
	}

	inline static int32_t get_offset_of__jounawcaw_14() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____jounawcaw_14)); }
	inline uint32_t get__jounawcaw_14() const { return ____jounawcaw_14; }
	inline uint32_t* get_address_of__jounawcaw_14() { return &____jounawcaw_14; }
	inline void set__jounawcaw_14(uint32_t value)
	{
		____jounawcaw_14 = value;
	}

	inline static int32_t get_offset_of__lernearjeGelpo_15() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____lernearjeGelpo_15)); }
	inline float get__lernearjeGelpo_15() const { return ____lernearjeGelpo_15; }
	inline float* get_address_of__lernearjeGelpo_15() { return &____lernearjeGelpo_15; }
	inline void set__lernearjeGelpo_15(float value)
	{
		____lernearjeGelpo_15 = value;
	}

	inline static int32_t get_offset_of__lawri_16() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____lawri_16)); }
	inline float get__lawri_16() const { return ____lawri_16; }
	inline float* get_address_of__lawri_16() { return &____lawri_16; }
	inline void set__lawri_16(float value)
	{
		____lawri_16 = value;
	}

	inline static int32_t get_offset_of__cahogor_17() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____cahogor_17)); }
	inline uint32_t get__cahogor_17() const { return ____cahogor_17; }
	inline uint32_t* get_address_of__cahogor_17() { return &____cahogor_17; }
	inline void set__cahogor_17(uint32_t value)
	{
		____cahogor_17 = value;
	}

	inline static int32_t get_offset_of__trarsu_18() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____trarsu_18)); }
	inline float get__trarsu_18() const { return ____trarsu_18; }
	inline float* get_address_of__trarsu_18() { return &____trarsu_18; }
	inline void set__trarsu_18(float value)
	{
		____trarsu_18 = value;
	}

	inline static int32_t get_offset_of__rerre_19() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____rerre_19)); }
	inline float get__rerre_19() const { return ____rerre_19; }
	inline float* get_address_of__rerre_19() { return &____rerre_19; }
	inline void set__rerre_19(float value)
	{
		____rerre_19 = value;
	}

	inline static int32_t get_offset_of__hakadas_20() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____hakadas_20)); }
	inline float get__hakadas_20() const { return ____hakadas_20; }
	inline float* get_address_of__hakadas_20() { return &____hakadas_20; }
	inline void set__hakadas_20(float value)
	{
		____hakadas_20 = value;
	}

	inline static int32_t get_offset_of__moupumorDrayler_21() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____moupumorDrayler_21)); }
	inline float get__moupumorDrayler_21() const { return ____moupumorDrayler_21; }
	inline float* get_address_of__moupumorDrayler_21() { return &____moupumorDrayler_21; }
	inline void set__moupumorDrayler_21(float value)
	{
		____moupumorDrayler_21 = value;
	}

	inline static int32_t get_offset_of__bujarmar_22() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____bujarmar_22)); }
	inline uint32_t get__bujarmar_22() const { return ____bujarmar_22; }
	inline uint32_t* get_address_of__bujarmar_22() { return &____bujarmar_22; }
	inline void set__bujarmar_22(uint32_t value)
	{
		____bujarmar_22 = value;
	}

	inline static int32_t get_offset_of__sairta_23() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____sairta_23)); }
	inline bool get__sairta_23() const { return ____sairta_23; }
	inline bool* get_address_of__sairta_23() { return &____sairta_23; }
	inline void set__sairta_23(bool value)
	{
		____sairta_23 = value;
	}

	inline static int32_t get_offset_of__saterdreBouhape_24() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____saterdreBouhape_24)); }
	inline int32_t get__saterdreBouhape_24() const { return ____saterdreBouhape_24; }
	inline int32_t* get_address_of__saterdreBouhape_24() { return &____saterdreBouhape_24; }
	inline void set__saterdreBouhape_24(int32_t value)
	{
		____saterdreBouhape_24 = value;
	}

	inline static int32_t get_offset_of__yoojaslarBairkair_25() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____yoojaslarBairkair_25)); }
	inline uint32_t get__yoojaslarBairkair_25() const { return ____yoojaslarBairkair_25; }
	inline uint32_t* get_address_of__yoojaslarBairkair_25() { return &____yoojaslarBairkair_25; }
	inline void set__yoojaslarBairkair_25(uint32_t value)
	{
		____yoojaslarBairkair_25 = value;
	}

	inline static int32_t get_offset_of__raistou_26() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____raistou_26)); }
	inline uint32_t get__raistou_26() const { return ____raistou_26; }
	inline uint32_t* get_address_of__raistou_26() { return &____raistou_26; }
	inline void set__raistou_26(uint32_t value)
	{
		____raistou_26 = value;
	}

	inline static int32_t get_offset_of__trirwener_27() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____trirwener_27)); }
	inline String_t* get__trirwener_27() const { return ____trirwener_27; }
	inline String_t** get_address_of__trirwener_27() { return &____trirwener_27; }
	inline void set__trirwener_27(String_t* value)
	{
		____trirwener_27 = value;
		Il2CppCodeGenWriteBarrier(&____trirwener_27, value);
	}

	inline static int32_t get_offset_of__setaMircea_28() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____setaMircea_28)); }
	inline uint32_t get__setaMircea_28() const { return ____setaMircea_28; }
	inline uint32_t* get_address_of__setaMircea_28() { return &____setaMircea_28; }
	inline void set__setaMircea_28(uint32_t value)
	{
		____setaMircea_28 = value;
	}

	inline static int32_t get_offset_of__biszeedrou_29() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____biszeedrou_29)); }
	inline bool get__biszeedrou_29() const { return ____biszeedrou_29; }
	inline bool* get_address_of__biszeedrou_29() { return &____biszeedrou_29; }
	inline void set__biszeedrou_29(bool value)
	{
		____biszeedrou_29 = value;
	}

	inline static int32_t get_offset_of__paimaykiFerrepor_30() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____paimaykiFerrepor_30)); }
	inline bool get__paimaykiFerrepor_30() const { return ____paimaykiFerrepor_30; }
	inline bool* get_address_of__paimaykiFerrepor_30() { return &____paimaykiFerrepor_30; }
	inline void set__paimaykiFerrepor_30(bool value)
	{
		____paimaykiFerrepor_30 = value;
	}

	inline static int32_t get_offset_of__silalXerzer_31() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____silalXerzer_31)); }
	inline int32_t get__silalXerzer_31() const { return ____silalXerzer_31; }
	inline int32_t* get_address_of__silalXerzer_31() { return &____silalXerzer_31; }
	inline void set__silalXerzer_31(int32_t value)
	{
		____silalXerzer_31 = value;
	}

	inline static int32_t get_offset_of__rutiMirvem_32() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____rutiMirvem_32)); }
	inline String_t* get__rutiMirvem_32() const { return ____rutiMirvem_32; }
	inline String_t** get_address_of__rutiMirvem_32() { return &____rutiMirvem_32; }
	inline void set__rutiMirvem_32(String_t* value)
	{
		____rutiMirvem_32 = value;
		Il2CppCodeGenWriteBarrier(&____rutiMirvem_32, value);
	}

	inline static int32_t get_offset_of__gichiva_33() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____gichiva_33)); }
	inline float get__gichiva_33() const { return ____gichiva_33; }
	inline float* get_address_of__gichiva_33() { return &____gichiva_33; }
	inline void set__gichiva_33(float value)
	{
		____gichiva_33 = value;
	}

	inline static int32_t get_offset_of__kegajirVallartou_34() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____kegajirVallartou_34)); }
	inline String_t* get__kegajirVallartou_34() const { return ____kegajirVallartou_34; }
	inline String_t** get_address_of__kegajirVallartou_34() { return &____kegajirVallartou_34; }
	inline void set__kegajirVallartou_34(String_t* value)
	{
		____kegajirVallartou_34 = value;
		Il2CppCodeGenWriteBarrier(&____kegajirVallartou_34, value);
	}

	inline static int32_t get_offset_of__setou_35() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____setou_35)); }
	inline bool get__setou_35() const { return ____setou_35; }
	inline bool* get_address_of__setou_35() { return &____setou_35; }
	inline void set__setou_35(bool value)
	{
		____setou_35 = value;
	}

	inline static int32_t get_offset_of__nayhallstuJestel_36() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____nayhallstuJestel_36)); }
	inline bool get__nayhallstuJestel_36() const { return ____nayhallstuJestel_36; }
	inline bool* get_address_of__nayhallstuJestel_36() { return &____nayhallstuJestel_36; }
	inline void set__nayhallstuJestel_36(bool value)
	{
		____nayhallstuJestel_36 = value;
	}

	inline static int32_t get_offset_of__sadouDrorde_37() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____sadouDrorde_37)); }
	inline float get__sadouDrorde_37() const { return ____sadouDrorde_37; }
	inline float* get_address_of__sadouDrorde_37() { return &____sadouDrorde_37; }
	inline void set__sadouDrorde_37(float value)
	{
		____sadouDrorde_37 = value;
	}

	inline static int32_t get_offset_of__drearmeJefear_38() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____drearmeJefear_38)); }
	inline bool get__drearmeJefear_38() const { return ____drearmeJefear_38; }
	inline bool* get_address_of__drearmeJefear_38() { return &____drearmeJefear_38; }
	inline void set__drearmeJefear_38(bool value)
	{
		____drearmeJefear_38 = value;
	}

	inline static int32_t get_offset_of__katrounemForkibor_39() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____katrounemForkibor_39)); }
	inline uint32_t get__katrounemForkibor_39() const { return ____katrounemForkibor_39; }
	inline uint32_t* get_address_of__katrounemForkibor_39() { return &____katrounemForkibor_39; }
	inline void set__katrounemForkibor_39(uint32_t value)
	{
		____katrounemForkibor_39 = value;
	}

	inline static int32_t get_offset_of__poujer_40() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____poujer_40)); }
	inline uint32_t get__poujer_40() const { return ____poujer_40; }
	inline uint32_t* get_address_of__poujer_40() { return &____poujer_40; }
	inline void set__poujer_40(uint32_t value)
	{
		____poujer_40 = value;
	}

	inline static int32_t get_offset_of__mawiSouzace_41() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____mawiSouzace_41)); }
	inline float get__mawiSouzace_41() const { return ____mawiSouzace_41; }
	inline float* get_address_of__mawiSouzace_41() { return &____mawiSouzace_41; }
	inline void set__mawiSouzace_41(float value)
	{
		____mawiSouzace_41 = value;
	}

	inline static int32_t get_offset_of__dacha_42() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____dacha_42)); }
	inline String_t* get__dacha_42() const { return ____dacha_42; }
	inline String_t** get_address_of__dacha_42() { return &____dacha_42; }
	inline void set__dacha_42(String_t* value)
	{
		____dacha_42 = value;
		Il2CppCodeGenWriteBarrier(&____dacha_42, value);
	}

	inline static int32_t get_offset_of__meartreWemem_43() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____meartreWemem_43)); }
	inline bool get__meartreWemem_43() const { return ____meartreWemem_43; }
	inline bool* get_address_of__meartreWemem_43() { return &____meartreWemem_43; }
	inline void set__meartreWemem_43(bool value)
	{
		____meartreWemem_43 = value;
	}

	inline static int32_t get_offset_of__nelwoo_44() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____nelwoo_44)); }
	inline float get__nelwoo_44() const { return ____nelwoo_44; }
	inline float* get_address_of__nelwoo_44() { return &____nelwoo_44; }
	inline void set__nelwoo_44(float value)
	{
		____nelwoo_44 = value;
	}

	inline static int32_t get_offset_of__whainislai_45() { return static_cast<int32_t>(offsetof(M_papaMearma244_t1370780595, ____whainislai_45)); }
	inline bool get__whainislai_45() const { return ____whainislai_45; }
	inline bool* get_address_of__whainislai_45() { return &____whainislai_45; }
	inline void set__whainislai_45(bool value)
	{
		____whainislai_45 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
