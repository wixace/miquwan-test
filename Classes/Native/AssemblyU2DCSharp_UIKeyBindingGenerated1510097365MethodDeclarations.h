﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIKeyBindingGenerated
struct UIKeyBindingGenerated_t1510097365;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UIKeyBindingGenerated::.ctor()
extern "C"  void UIKeyBindingGenerated__ctor_m4068013878 (UIKeyBindingGenerated_t1510097365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIKeyBindingGenerated::UIKeyBinding_UIKeyBinding1(JSVCall,System.Int32)
extern "C"  bool UIKeyBindingGenerated_UIKeyBinding_UIKeyBinding1_m889822268 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBindingGenerated::UIKeyBinding_keyCode(JSVCall)
extern "C"  void UIKeyBindingGenerated_UIKeyBinding_keyCode_m4189551122 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBindingGenerated::UIKeyBinding_modifier(JSVCall)
extern "C"  void UIKeyBindingGenerated_UIKeyBinding_modifier_m1598975895 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBindingGenerated::UIKeyBinding_action(JSVCall)
extern "C"  void UIKeyBindingGenerated_UIKeyBinding_action_m2619306264 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBindingGenerated::__Register()
extern "C"  void UIKeyBindingGenerated___Register_m391157585 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBindingGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UIKeyBindingGenerated_ilo_addJSCSRel1_m1988078322 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBindingGenerated::ilo_setEnum2(System.Int32,System.Int32)
extern "C"  void UIKeyBindingGenerated_ilo_setEnum2_m2140517430 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
