﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AIPath
struct AIPath_t1930792045;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// Pathfinding.Path
struct Path_t1974241691;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_AIPath1930792045.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void AIPath::.ctor()
extern "C"  void AIPath__ctor_m2918482638 (AIPath_t1930792045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AIPath::get_canMove()
extern "C"  bool AIPath_get_canMove_m1269061488 (AIPath_t1930792045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPath::set_canMove(System.Boolean)
extern "C"  void AIPath_set_canMove_m2657557799 (AIPath_t1930792045 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AIPath::get_TargetReached()
extern "C"  bool AIPath_get_TargetReached_m3909350288 (AIPath_t1930792045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPath::Awake()
extern "C"  void AIPath_Awake_m3156087857 (AIPath_t1930792045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPath::Start()
extern "C"  void AIPath_Start_m1865620430 (AIPath_t1930792045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPath::OnEnable()
extern "C"  void AIPath_OnEnable_m3564874232 (AIPath_t1930792045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPath::OnDisable()
extern "C"  void AIPath_OnDisable_m3577856053 (AIPath_t1930792045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AIPath::RepeatTrySearchPath()
extern "C"  Il2CppObject * AIPath_RepeatTrySearchPath_m1472163921 (AIPath_t1930792045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AIPath::TrySearchPath()
extern "C"  float AIPath_TrySearchPath_m1563083432 (AIPath_t1930792045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Path AIPath::SearchPath()
extern "C"  Path_t1974241691 * AIPath_SearchPath_m828109297 (AIPath_t1930792045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPath::OnTargetReached()
extern "C"  void AIPath_OnTargetReached_m4040030350 (AIPath_t1930792045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPath::OnPathComplete(Pathfinding.Path)
extern "C"  void AIPath_OnPathComplete_m3993508036 (AIPath_t1930792045 * __this, Path_t1974241691 * ____p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 AIPath::GetFeetPosition()
extern "C"  Vector3_t4282066566  AIPath_GetFeetPosition_m2826995231 (AIPath_t1930792045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPath::Update()
extern "C"  void AIPath_Update_m2005510655 (AIPath_t1930792045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AIPath::XZSqrMagnitude(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float AIPath_XZSqrMagnitude_m1852054668 (AIPath_t1930792045 * __this, Vector3_t4282066566  ___a0, Vector3_t4282066566  ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 AIPath::CalculateVelocity(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  AIPath_CalculateVelocity_m3606437476 (AIPath_t1930792045 * __this, Vector3_t4282066566  ___currentPosition0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPath::RotateTowards(UnityEngine.Vector3)
extern "C"  void AIPath_RotateTowards_m513600524 (AIPath_t1930792045 * __this, Vector3_t4282066566  ___dir0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 AIPath::CalculateTargetPoint(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  AIPath_CalculateTargetPoint_m2364903734 (AIPath_t1930792045 * __this, Vector3_t4282066566  ___p0, Vector3_t4282066566  ___a1, Vector3_t4282066566  ___b2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 AIPath::ilo_GetFeetPosition1(AIPath)
extern "C"  Vector3_t4282066566  AIPath_ilo_GetFeetPosition1_m1024137786 (Il2CppObject * __this /* static, unused */, AIPath_t1930792045 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPath::ilo_set_id2(Pathfinding.Path,System.UInt32)
extern "C"  void AIPath_ilo_set_id2_m527527512 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AIPath::ilo_Release3(Pathfinding.Path,System.Object)
extern "C"  void AIPath_ilo_Release3_m2301309460 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, Il2CppObject * ___o1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String AIPath::ilo_get_errorLog4(Pathfinding.Path)
extern "C"  String_t* AIPath_ilo_get_errorLog4_m690702518 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AIPath::ilo_NearestPointFactor5(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float AIPath_ilo_NearestPointFactor5_m4239182526 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___lineStart0, Vector3_t4282066566  ___lineEnd1, Vector3_t4282066566  ___point2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AIPath::ilo_Clamp016(System.Single)
extern "C"  float AIPath_ilo_Clamp016_m1182301916 (Il2CppObject * __this /* static, unused */, float ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
