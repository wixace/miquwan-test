//
//  MQWRole.h
//  MiquwanSDK
//
//  Created by MiQuWang on 2019/11/6.
//  Copyright © 2019 LQX. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MQWRole : NSObject

/// 服务器ID
@property (nonatomic, strong)NSString *serverID;

/// 服务器名称
@property (nonatomic, strong)NSString *serverName;

/// 游戏角色名
@property (nonatomic, strong)NSString *gameRoleName;
/// 游戏角色ID
@property (nonatomic, strong)NSString *gameRoleID;
/// 角色等级
@property (nonatomic, strong)NSNumber *gameRoleLevel;

/// vip等级
@property (nonatomic, strong)NSNumber *vipLevel;

/// 请求来源
@property (nonatomic, strong)NSString *source;


/// 创建角色模型
/// @param serverId      所在服务器ID
/// @param serverName    服务器名称
/// @param gameRoleName  角色名
/// @param gameRoleID    角色id
/// @param gameRoleLevel 角色等级
/// @param vipLevel      vip等级
/// @param source        来源
+ (MQWRole *)createRoleWithServerId:(NSString *)serverId
                         serverName:(NSString *)serverName
                       gameRoleName:(NSString *)gameRoleName
                         gameRoleID:(NSString *)gameRoleID
                      gameRoleLevel:(NSNumber *)gameRoleLevel
                           vipLevel:(NSNumber *)vipLevel
                             source:(NSString *)source;
/// 角色信息
- (NSDictionary *)roleInfo;
@end

NS_ASSUME_NONNULL_END
