﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.GraphNode
struct GraphNode_t23612370;
// Pathfinding.PathNode
struct PathNode_t417131581;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.ABPath
struct  ABPath_t1187561148  : public Path_t1974241691
{
public:
	// System.Boolean Pathfinding.ABPath::recalcStartEndCosts
	bool ___recalcStartEndCosts_38;
	// Pathfinding.GraphNode Pathfinding.ABPath::startNode
	GraphNode_t23612370 * ___startNode_39;
	// Pathfinding.GraphNode Pathfinding.ABPath::endNode
	GraphNode_t23612370 * ___endNode_40;
	// Pathfinding.GraphNode Pathfinding.ABPath::startHint
	GraphNode_t23612370 * ___startHint_41;
	// Pathfinding.GraphNode Pathfinding.ABPath::endHint
	GraphNode_t23612370 * ___endHint_42;
	// UnityEngine.Vector3 Pathfinding.ABPath::originalStartPoint
	Vector3_t4282066566  ___originalStartPoint_43;
	// UnityEngine.Vector3 Pathfinding.ABPath::originalEndPoint
	Vector3_t4282066566  ___originalEndPoint_44;
	// UnityEngine.Vector3 Pathfinding.ABPath::startPoint
	Vector3_t4282066566  ___startPoint_45;
	// UnityEngine.Vector3 Pathfinding.ABPath::endPoint
	Vector3_t4282066566  ___endPoint_46;
	// System.Boolean Pathfinding.ABPath::hasEndPoint
	bool ___hasEndPoint_47;
	// Pathfinding.Int3 Pathfinding.ABPath::startIntPoint
	Int3_t1974045594  ___startIntPoint_48;
	// System.Boolean Pathfinding.ABPath::calculatePartial
	bool ___calculatePartial_49;
	// Pathfinding.PathNode Pathfinding.ABPath::partialBestTarget
	PathNode_t417131581 * ___partialBestTarget_50;
	// System.Int32[] Pathfinding.ABPath::endNodeCosts
	Int32U5BU5D_t3230847821* ___endNodeCosts_51;

public:
	inline static int32_t get_offset_of_recalcStartEndCosts_38() { return static_cast<int32_t>(offsetof(ABPath_t1187561148, ___recalcStartEndCosts_38)); }
	inline bool get_recalcStartEndCosts_38() const { return ___recalcStartEndCosts_38; }
	inline bool* get_address_of_recalcStartEndCosts_38() { return &___recalcStartEndCosts_38; }
	inline void set_recalcStartEndCosts_38(bool value)
	{
		___recalcStartEndCosts_38 = value;
	}

	inline static int32_t get_offset_of_startNode_39() { return static_cast<int32_t>(offsetof(ABPath_t1187561148, ___startNode_39)); }
	inline GraphNode_t23612370 * get_startNode_39() const { return ___startNode_39; }
	inline GraphNode_t23612370 ** get_address_of_startNode_39() { return &___startNode_39; }
	inline void set_startNode_39(GraphNode_t23612370 * value)
	{
		___startNode_39 = value;
		Il2CppCodeGenWriteBarrier(&___startNode_39, value);
	}

	inline static int32_t get_offset_of_endNode_40() { return static_cast<int32_t>(offsetof(ABPath_t1187561148, ___endNode_40)); }
	inline GraphNode_t23612370 * get_endNode_40() const { return ___endNode_40; }
	inline GraphNode_t23612370 ** get_address_of_endNode_40() { return &___endNode_40; }
	inline void set_endNode_40(GraphNode_t23612370 * value)
	{
		___endNode_40 = value;
		Il2CppCodeGenWriteBarrier(&___endNode_40, value);
	}

	inline static int32_t get_offset_of_startHint_41() { return static_cast<int32_t>(offsetof(ABPath_t1187561148, ___startHint_41)); }
	inline GraphNode_t23612370 * get_startHint_41() const { return ___startHint_41; }
	inline GraphNode_t23612370 ** get_address_of_startHint_41() { return &___startHint_41; }
	inline void set_startHint_41(GraphNode_t23612370 * value)
	{
		___startHint_41 = value;
		Il2CppCodeGenWriteBarrier(&___startHint_41, value);
	}

	inline static int32_t get_offset_of_endHint_42() { return static_cast<int32_t>(offsetof(ABPath_t1187561148, ___endHint_42)); }
	inline GraphNode_t23612370 * get_endHint_42() const { return ___endHint_42; }
	inline GraphNode_t23612370 ** get_address_of_endHint_42() { return &___endHint_42; }
	inline void set_endHint_42(GraphNode_t23612370 * value)
	{
		___endHint_42 = value;
		Il2CppCodeGenWriteBarrier(&___endHint_42, value);
	}

	inline static int32_t get_offset_of_originalStartPoint_43() { return static_cast<int32_t>(offsetof(ABPath_t1187561148, ___originalStartPoint_43)); }
	inline Vector3_t4282066566  get_originalStartPoint_43() const { return ___originalStartPoint_43; }
	inline Vector3_t4282066566 * get_address_of_originalStartPoint_43() { return &___originalStartPoint_43; }
	inline void set_originalStartPoint_43(Vector3_t4282066566  value)
	{
		___originalStartPoint_43 = value;
	}

	inline static int32_t get_offset_of_originalEndPoint_44() { return static_cast<int32_t>(offsetof(ABPath_t1187561148, ___originalEndPoint_44)); }
	inline Vector3_t4282066566  get_originalEndPoint_44() const { return ___originalEndPoint_44; }
	inline Vector3_t4282066566 * get_address_of_originalEndPoint_44() { return &___originalEndPoint_44; }
	inline void set_originalEndPoint_44(Vector3_t4282066566  value)
	{
		___originalEndPoint_44 = value;
	}

	inline static int32_t get_offset_of_startPoint_45() { return static_cast<int32_t>(offsetof(ABPath_t1187561148, ___startPoint_45)); }
	inline Vector3_t4282066566  get_startPoint_45() const { return ___startPoint_45; }
	inline Vector3_t4282066566 * get_address_of_startPoint_45() { return &___startPoint_45; }
	inline void set_startPoint_45(Vector3_t4282066566  value)
	{
		___startPoint_45 = value;
	}

	inline static int32_t get_offset_of_endPoint_46() { return static_cast<int32_t>(offsetof(ABPath_t1187561148, ___endPoint_46)); }
	inline Vector3_t4282066566  get_endPoint_46() const { return ___endPoint_46; }
	inline Vector3_t4282066566 * get_address_of_endPoint_46() { return &___endPoint_46; }
	inline void set_endPoint_46(Vector3_t4282066566  value)
	{
		___endPoint_46 = value;
	}

	inline static int32_t get_offset_of_hasEndPoint_47() { return static_cast<int32_t>(offsetof(ABPath_t1187561148, ___hasEndPoint_47)); }
	inline bool get_hasEndPoint_47() const { return ___hasEndPoint_47; }
	inline bool* get_address_of_hasEndPoint_47() { return &___hasEndPoint_47; }
	inline void set_hasEndPoint_47(bool value)
	{
		___hasEndPoint_47 = value;
	}

	inline static int32_t get_offset_of_startIntPoint_48() { return static_cast<int32_t>(offsetof(ABPath_t1187561148, ___startIntPoint_48)); }
	inline Int3_t1974045594  get_startIntPoint_48() const { return ___startIntPoint_48; }
	inline Int3_t1974045594 * get_address_of_startIntPoint_48() { return &___startIntPoint_48; }
	inline void set_startIntPoint_48(Int3_t1974045594  value)
	{
		___startIntPoint_48 = value;
	}

	inline static int32_t get_offset_of_calculatePartial_49() { return static_cast<int32_t>(offsetof(ABPath_t1187561148, ___calculatePartial_49)); }
	inline bool get_calculatePartial_49() const { return ___calculatePartial_49; }
	inline bool* get_address_of_calculatePartial_49() { return &___calculatePartial_49; }
	inline void set_calculatePartial_49(bool value)
	{
		___calculatePartial_49 = value;
	}

	inline static int32_t get_offset_of_partialBestTarget_50() { return static_cast<int32_t>(offsetof(ABPath_t1187561148, ___partialBestTarget_50)); }
	inline PathNode_t417131581 * get_partialBestTarget_50() const { return ___partialBestTarget_50; }
	inline PathNode_t417131581 ** get_address_of_partialBestTarget_50() { return &___partialBestTarget_50; }
	inline void set_partialBestTarget_50(PathNode_t417131581 * value)
	{
		___partialBestTarget_50 = value;
		Il2CppCodeGenWriteBarrier(&___partialBestTarget_50, value);
	}

	inline static int32_t get_offset_of_endNodeCosts_51() { return static_cast<int32_t>(offsetof(ABPath_t1187561148, ___endNodeCosts_51)); }
	inline Int32U5BU5D_t3230847821* get_endNodeCosts_51() const { return ___endNodeCosts_51; }
	inline Int32U5BU5D_t3230847821** get_address_of_endNodeCosts_51() { return &___endNodeCosts_51; }
	inline void set_endNodeCosts_51(Int32U5BU5D_t3230847821* value)
	{
		___endNodeCosts_51 = value;
		Il2CppCodeGenWriteBarrier(&___endNodeCosts_51, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
