﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Path
struct Path_t1974241691;
// System.Object
struct Il2CppObject;
// Seeker
struct Seeker_t2472610117;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Seeker/<DelayPathStart>c__Iterator7
struct  U3CDelayPathStartU3Ec__Iterator7_t4147751032  : public Il2CppObject
{
public:
	// Pathfinding.Path Seeker/<DelayPathStart>c__Iterator7::p
	Path_t1974241691 * ___p_0;
	// System.Int32 Seeker/<DelayPathStart>c__Iterator7::$PC
	int32_t ___U24PC_1;
	// System.Object Seeker/<DelayPathStart>c__Iterator7::$current
	Il2CppObject * ___U24current_2;
	// Pathfinding.Path Seeker/<DelayPathStart>c__Iterator7::<$>p
	Path_t1974241691 * ___U3CU24U3Ep_3;
	// Seeker Seeker/<DelayPathStart>c__Iterator7::<>f__this
	Seeker_t2472610117 * ___U3CU3Ef__this_4;

public:
	inline static int32_t get_offset_of_p_0() { return static_cast<int32_t>(offsetof(U3CDelayPathStartU3Ec__Iterator7_t4147751032, ___p_0)); }
	inline Path_t1974241691 * get_p_0() const { return ___p_0; }
	inline Path_t1974241691 ** get_address_of_p_0() { return &___p_0; }
	inline void set_p_0(Path_t1974241691 * value)
	{
		___p_0 = value;
		Il2CppCodeGenWriteBarrier(&___p_0, value);
	}

	inline static int32_t get_offset_of_U24PC_1() { return static_cast<int32_t>(offsetof(U3CDelayPathStartU3Ec__Iterator7_t4147751032, ___U24PC_1)); }
	inline int32_t get_U24PC_1() const { return ___U24PC_1; }
	inline int32_t* get_address_of_U24PC_1() { return &___U24PC_1; }
	inline void set_U24PC_1(int32_t value)
	{
		___U24PC_1 = value;
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CDelayPathStartU3Ec__Iterator7_t4147751032, ___U24current_2)); }
	inline Il2CppObject * get_U24current_2() const { return ___U24current_2; }
	inline Il2CppObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(Il2CppObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_2, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ep_3() { return static_cast<int32_t>(offsetof(U3CDelayPathStartU3Ec__Iterator7_t4147751032, ___U3CU24U3Ep_3)); }
	inline Path_t1974241691 * get_U3CU24U3Ep_3() const { return ___U3CU24U3Ep_3; }
	inline Path_t1974241691 ** get_address_of_U3CU24U3Ep_3() { return &___U3CU24U3Ep_3; }
	inline void set_U3CU24U3Ep_3(Path_t1974241691 * value)
	{
		___U3CU24U3Ep_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ep_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_4() { return static_cast<int32_t>(offsetof(U3CDelayPathStartU3Ec__Iterator7_t4147751032, ___U3CU3Ef__this_4)); }
	inline Seeker_t2472610117 * get_U3CU3Ef__this_4() const { return ___U3CU3Ef__this_4; }
	inline Seeker_t2472610117 ** get_address_of_U3CU3Ef__this_4() { return &___U3CU3Ef__this_4; }
	inline void set_U3CU3Ef__this_4(Seeker_t2472610117 * value)
	{
		___U3CU3Ef__this_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
