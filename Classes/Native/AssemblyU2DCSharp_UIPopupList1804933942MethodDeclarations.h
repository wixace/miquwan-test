﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIPopupList
struct UIPopupList_t1804933942;
// UnityEngine.Object
struct Object_t3071478659;
// UIPopupList/LegacyEvent
struct LegacyEvent_t524649176;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UILabel
struct UILabel_t291504320;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UIWidget
struct UIWidget_t769069560;
// UIFont
struct UIFont_t2503090435;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t1077642479;
// UnityEngine.Font
struct Font_t4241557075;
// EventDelegate
struct EventDelegate_t4004424223;
// EventDelegate/Callback
struct Callback_t1094463061;
// UnityEngine.Transform
struct Transform_t1659122786;
// UIRect
struct UIRect_t2503437976;
// TweenPosition
struct TweenPosition_t3684358292;
// UISpriteData
struct UISpriteData_t3578345923;
// UISprite
struct UISprite_t661437049;
// TweenColor
struct TweenColor_t2922258392;
// UnityEngine.Camera
struct Camera_t2727095145;
// UIAtlas
struct UIAtlas_t281921111;
// UIEventListener
struct UIEventListener_t1278105402;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "AssemblyU2DCSharp_UIPopupList_LegacyEvent524649176.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UIFont2503090435.h"
#include "UnityEngine_UnityEngine_FontStyle3350479768.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_UIPopupList1804933942.h"
#include "AssemblyU2DCSharp_UIRect2503437976.h"
#include "AssemblyU2DCSharp_UISprite661437049.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "AssemblyU2DCSharp_UIAtlas281921111.h"
#include "AssemblyU2DCSharp_UIWidget_Pivot240933195.h"
#include "AssemblyU2DCSharp_UILabel_Overflow229724305.h"

// System.Void UIPopupList::.ctor()
extern "C"  void UIPopupList__ctor_m1705155829 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UIPopupList::get_ambigiousFont()
extern "C"  Object_t3071478659 * UIPopupList_get_ambigiousFont_m3581015704 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::set_ambigiousFont(UnityEngine.Object)
extern "C"  void UIPopupList_set_ambigiousFont_m1416996903 (UIPopupList_t1804933942 * __this, Object_t3071478659 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIPopupList/LegacyEvent UIPopupList::get_onSelectionChange()
extern "C"  LegacyEvent_t524649176 * UIPopupList_get_onSelectionChange_m1623620290 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::set_onSelectionChange(UIPopupList/LegacyEvent)
extern "C"  void UIPopupList_set_onSelectionChange_m2086357287 (UIPopupList_t1804933942 * __this, LegacyEvent_t524649176 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPopupList::get_isOpen()
extern "C"  bool UIPopupList_get_isOpen_m117539640 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIPopupList::get_value()
extern "C"  String_t* UIPopupList_get_value_m2684566430 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::set_value(System.String)
extern "C"  void UIPopupList_set_value_m3561584347 (UIPopupList_t1804933942 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIPopupList::get_data()
extern "C"  Il2CppObject * UIPopupList_get_data_m15282285 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIPopupList::get_selection()
extern "C"  String_t* UIPopupList_get_selection_m3735218937 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::set_selection(System.String)
extern "C"  void UIPopupList_set_selection_m1452711904 (UIPopupList_t1804933942 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPopupList::get_handleEvents()
extern "C"  bool UIPopupList_get_handleEvents_m2309011589 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::set_handleEvents(System.Boolean)
extern "C"  void UIPopupList_set_handleEvents_m738081252 (UIPopupList_t1804933942 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPopupList::get_isValid()
extern "C"  bool UIPopupList_get_isValid_m843256112 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIPopupList::get_activeFontSize()
extern "C"  int32_t UIPopupList_get_activeFontSize_m2328924960 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIPopupList::get_activeFontScale()
extern "C"  float UIPopupList_get_activeFontScale_m232542315 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::Clear()
extern "C"  void UIPopupList_Clear_m3406256416 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::AddItem(System.String)
extern "C"  void UIPopupList_AddItem_m3868540539 (UIPopupList_t1804933942 * __this, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::AddItem(System.String,System.Object)
extern "C"  void UIPopupList_AddItem_m2880640521 (UIPopupList_t1804933942 * __this, String_t* ___text0, Il2CppObject * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::TriggerCallbacks()
extern "C"  void UIPopupList_TriggerCallbacks_m1268155685 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::OnEnable()
extern "C"  void UIPopupList_OnEnable_m3790670449 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::OnValidate()
extern "C"  void UIPopupList_OnValidate_m3061575172 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::Start()
extern "C"  void UIPopupList_Start_m652293621 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::OnLocalize()
extern "C"  void UIPopupList_OnLocalize_m123298359 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::Highlight(UILabel,System.Boolean)
extern "C"  void UIPopupList_Highlight_m4128262930 (UIPopupList_t1804933942 * __this, UILabel_t291504320 * ___lbl0, bool ___instant1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIPopupList::GetHighlightPosition()
extern "C"  Vector3_t4282066566  UIPopupList_GetHighlightPosition_m2028450172 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UIPopupList::UpdateTweenPosition()
extern "C"  Il2CppObject * UIPopupList_UpdateTweenPosition_m1834919558 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::OnItemHover(UnityEngine.GameObject,System.Boolean)
extern "C"  void UIPopupList_OnItemHover_m1133824584 (UIPopupList_t1804933942 * __this, GameObject_t3674682005 * ___go0, bool ___isOver1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::Select(UILabel,System.Boolean)
extern "C"  void UIPopupList_Select_m1334452086 (UIPopupList_t1804933942 * __this, UILabel_t291504320 * ___lbl0, bool ___instant1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::OnItemPress(UnityEngine.GameObject,System.Boolean)
extern "C"  void UIPopupList_OnItemPress_m2497979745 (UIPopupList_t1804933942 * __this, GameObject_t3674682005 * ___go0, bool ___isPressed1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::OnItemClick(UnityEngine.GameObject)
extern "C"  void UIPopupList_OnItemClick_m1779369185 (UIPopupList_t1804933942 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::OnKey(UnityEngine.KeyCode)
extern "C"  void UIPopupList_OnKey_m1135837386 (UIPopupList_t1804933942 * __this, int32_t ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::OnDisable()
extern "C"  void UIPopupList_OnDisable_m1987604188 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::OnSelect(System.Boolean)
extern "C"  void UIPopupList_OnSelect_m2119050145 (UIPopupList_t1804933942 * __this, bool ___isSelected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::Close()
extern "C"  void UIPopupList_Close_m3416015371 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::AnimateColor(UIWidget)
extern "C"  void UIPopupList_AnimateColor_m1330687289 (UIPopupList_t1804933942 * __this, UIWidget_t769069560 * ___widget0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::AnimatePosition(UIWidget,System.Boolean,System.Single)
extern "C"  void UIPopupList_AnimatePosition_m555456637 (UIPopupList_t1804933942 * __this, UIWidget_t769069560 * ___widget0, bool ___placeAbove1, float ___bottom2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::AnimateScale(UIWidget,System.Boolean,System.Single)
extern "C"  void UIPopupList_AnimateScale_m4049832258 (UIPopupList_t1804933942 * __this, UIWidget_t769069560 * ___widget0, bool ___placeAbove1, float ___bottom2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::Animate(UIWidget,System.Boolean,System.Single)
extern "C"  void UIPopupList_Animate_m604406502 (UIPopupList_t1804933942 * __this, UIWidget_t769069560 * ___widget0, bool ___placeAbove1, float ___bottom2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::OnClick()
extern "C"  void UIPopupList_OnClick_m4116417084 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::OnDoubleClick()
extern "C"  void UIPopupList_OnDoubleClick_m1167628011 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UIPopupList::CloseIfUnselected()
extern "C"  Il2CppObject * UIPopupList_CloseIfUnselected_m311420516 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::Show()
extern "C"  void UIPopupList_Show_m3889704300 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::OnDestory()
extern "C"  void UIPopupList_OnDestory_m4162455048 (UIPopupList_t1804933942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIPopupList::ilo_get_defaultSize1(UIFont)
extern "C"  int32_t UIPopupList_ilo_get_defaultSize1_m2836112425 (Il2CppObject * __this /* static, unused */, UIFont_t2503090435 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::ilo_Execute2(System.Collections.Generic.List`1<EventDelegate>)
extern "C"  void UIPopupList_ilo_Execute2_m921469474 (Il2CppObject * __this /* static, unused */, List_1_t1077642479 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Font UIPopupList::ilo_get_dynamicFont3(UIFont)
extern "C"  Font_t4241557075 * UIPopupList_ilo_get_dynamicFont3_m3760186902 (Il2CppObject * __this /* static, unused */, UIFont_t2503090435 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.FontStyle UIPopupList::ilo_get_dynamicFontStyle4(UIFont)
extern "C"  int32_t UIPopupList_ilo_get_dynamicFontStyle4_m2890257517 (Il2CppObject * __this /* static, unused */, UIFont_t2503090435 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EventDelegate UIPopupList::ilo_Add5(System.Collections.Generic.List`1<EventDelegate>,EventDelegate/Callback)
extern "C"  EventDelegate_t4004424223 * UIPopupList_ilo_Add5_m9489740 (Il2CppObject * __this /* static, unused */, List_1_t1077642479 * ___list0, Callback_t1094463061 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::ilo_TriggerCallbacks6(UIPopupList)
extern "C"  void UIPopupList_ilo_TriggerCallbacks6_m846990716 (Il2CppObject * __this /* static, unused */, UIPopupList_t1804933942 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UIPopupList::ilo_get_cachedTransform7(UIRect)
extern "C"  Transform_t1659122786 * UIPopupList_ilo_get_cachedTransform7_m950573922 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TweenPosition UIPopupList::ilo_Begin8(UnityEngine.GameObject,System.Single,UnityEngine.Vector3)
extern "C"  TweenPosition_t3684358292 * UIPopupList_ilo_Begin8_m2245489890 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, float ___duration1, Vector3_t4282066566  ___pos2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UISpriteData UIPopupList::ilo_GetAtlasSprite9(UISprite)
extern "C"  UISpriteData_t3578345923 * UIPopupList_ilo_GetAtlasSprite9_m2238668160 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::ilo_Close10(UIPopupList)
extern "C"  void UIPopupList_ilo_Close10_m2338047749 (Il2CppObject * __this /* static, unused */, UIPopupList_t1804933942 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPopupList::ilo_get_handleEvents11(UIPopupList)
extern "C"  bool UIPopupList_ilo_get_handleEvents11_m3983644964 (Il2CppObject * __this /* static, unused */, UIPopupList_t1804933942 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::ilo_OnSelect12(UIPopupList,System.Boolean)
extern "C"  void UIPopupList_ilo_OnSelect12_m707561727 (Il2CppObject * __this /* static, unused */, UIPopupList_t1804933942 * ____this0, bool ___isSelected1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::ilo_set_handleEvents13(UIPopupList,System.Boolean)
extern "C"  void UIPopupList_ilo_set_handleEvents13_m2282237827 (Il2CppObject * __this /* static, unused */, UIPopupList_t1804933942 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TweenColor UIPopupList::ilo_Begin14(UnityEngine.GameObject,System.Single,UnityEngine.Color)
extern "C"  TweenColor_t2922258392 * UIPopupList_ilo_Begin14_m798160494 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, float ___duration1, Color_t4194546905  ___color2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UIPopupList::ilo_get_color15(UIWidget)
extern "C"  Color_t4194546905  UIPopupList_ilo_get_color15_m3984584909 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::ilo_set_color16(UIWidget,UnityEngine.Color)
extern "C"  void UIPopupList_ilo_set_color16_m1299762739 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, Color_t4194546905  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIPopupList::ilo_get_activeFontSize17(UIPopupList)
extern "C"  int32_t UIPopupList_ilo_get_activeFontSize17_m4062767075 (Il2CppObject * __this /* static, unused */, UIPopupList_t1804933942 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIPopupList::ilo_get_height18(UIWidget)
extern "C"  int32_t UIPopupList_ilo_get_height18_m2118684499 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::ilo_AnimateColor19(UIPopupList,UIWidget)
extern "C"  void UIPopupList_ilo_AnimateColor19_m3909457950 (Il2CppObject * __this /* static, unused */, UIPopupList_t1804933942 * ____this0, UIWidget_t769069560 * ___widget1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::ilo_AnimatePosition20(UIPopupList,UIWidget,System.Boolean,System.Single)
extern "C"  void UIPopupList_ilo_AnimatePosition20_m720175976 (Il2CppObject * __this /* static, unused */, UIPopupList_t1804933942 * ____this0, UIWidget_t769069560 * ___widget1, bool ___placeAbove2, float ___bottom3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::ilo_Show21(UIPopupList)
extern "C"  void UIPopupList_ilo_Show21_m3966825310 (Il2CppObject * __this /* static, unused */, UIPopupList_t1804933942 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPopupList::ilo_GetActive22(UnityEngine.GameObject)
extern "C"  bool UIPopupList_ilo_GetActive22_m3323818408 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPopupList::ilo_get_isValid23(UIPopupList)
extern "C"  bool UIPopupList_ilo_get_isValid23_m1917332190 (Il2CppObject * __this /* static, unused */, UIPopupList_t1804933942 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UIPopupList::ilo_get_anchorCamera24(UIRect)
extern "C"  Camera_t2727095145 * UIPopupList_ilo_get_anchorCamera24_m3731512814 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIPopupList::ilo_CalculateNextDepth25(UnityEngine.GameObject)
extern "C"  int32_t UIPopupList_ilo_CalculateNextDepth25_m55621337 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UIPopupList::ilo_get_border26(UISprite)
extern "C"  Vector4_t4282066567  UIPopupList_ilo_get_border26_m1308512361 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UISprite UIPopupList::ilo_AddSprite27(UnityEngine.GameObject,UIAtlas,System.String)
extern "C"  UISprite_t661437049 * UIPopupList_ilo_AddSprite27_m4067270552 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, UIAtlas_t281921111 * ___atlas1, String_t* ___spriteName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::ilo_set_pivot28(UIWidget,UIWidget/Pivot)
extern "C"  void UIPopupList_ilo_set_pivot28_m376951208 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::ilo_set_bitmapFont29(UILabel,UIFont)
extern "C"  void UIPopupList_ilo_set_bitmapFont29_m3158099285 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, UIFont_t2503090435 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIPopupList::ilo_Get30(System.String)
extern "C"  String_t* UIPopupList_ilo_Get30_m3420352172 (Il2CppObject * __this /* static, unused */, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener UIPopupList::ilo_Get31(UnityEngine.GameObject)
extern "C"  UIEventListener_t1278105402 * UIPopupList_ilo_Get31_m3716651397 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::ilo_AddWidgetCollider32(UnityEngine.GameObject)
extern "C"  void UIPopupList_ilo_AddWidgetCollider32_m3290838576 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::ilo_set_overflowMethod33(UILabel,UILabel/Overflow)
extern "C"  void UIPopupList_ilo_set_overflowMethod33_m1874186373 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList::ilo_Animate34(UIPopupList,UIWidget,System.Boolean,System.Single)
extern "C"  void UIPopupList_ilo_Animate34_m3268968462 (Il2CppObject * __this /* static, unused */, UIPopupList_t1804933942 * ____this0, UIWidget_t769069560 * ___widget1, bool ___placeAbove2, float ___bottom3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIPopupList::ilo_get_width35(UIWidget)
extern "C"  int32_t UIPopupList_ilo_get_width35_m3469542901 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
