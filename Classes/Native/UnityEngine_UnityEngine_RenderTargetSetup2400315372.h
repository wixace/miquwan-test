﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RenderBuffer[]
struct RenderBufferU5BU5D_t1970987103;
// UnityEngine.Rendering.RenderBufferLoadAction[]
struct RenderBufferLoadActionU5BU5D_t3840699671;
// UnityEngine.Rendering.RenderBufferStoreAction[]
struct RenderBufferStoreActionU5BU5D_t2354850566;
struct RenderBuffer_t3529837690_marshaled_pinvoke;
struct RenderBuffer_t3529837690_marshaled_com;

#include "mscorlib_System_ValueType1744280289.h"
#include "UnityEngine_UnityEngine_RenderBuffer3529837690.h"
#include "UnityEngine_UnityEngine_CubemapFace2005084858.h"
#include "UnityEngine_UnityEngine_Rendering_RenderBufferLoad4243858466.h"
#include "UnityEngine_UnityEngine_Rendering_RenderBufferStor2198970271.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RenderTargetSetup
struct  RenderTargetSetup_t2400315372 
{
public:
	// UnityEngine.RenderBuffer[] UnityEngine.RenderTargetSetup::color
	RenderBufferU5BU5D_t1970987103* ___color_0;
	// UnityEngine.RenderBuffer UnityEngine.RenderTargetSetup::depth
	RenderBuffer_t3529837690  ___depth_1;
	// System.Int32 UnityEngine.RenderTargetSetup::mipLevel
	int32_t ___mipLevel_2;
	// UnityEngine.CubemapFace UnityEngine.RenderTargetSetup::cubemapFace
	int32_t ___cubemapFace_3;
	// UnityEngine.Rendering.RenderBufferLoadAction[] UnityEngine.RenderTargetSetup::colorLoad
	RenderBufferLoadActionU5BU5D_t3840699671* ___colorLoad_4;
	// UnityEngine.Rendering.RenderBufferStoreAction[] UnityEngine.RenderTargetSetup::colorStore
	RenderBufferStoreActionU5BU5D_t2354850566* ___colorStore_5;
	// UnityEngine.Rendering.RenderBufferLoadAction UnityEngine.RenderTargetSetup::depthLoad
	int32_t ___depthLoad_6;
	// UnityEngine.Rendering.RenderBufferStoreAction UnityEngine.RenderTargetSetup::depthStore
	int32_t ___depthStore_7;

public:
	inline static int32_t get_offset_of_color_0() { return static_cast<int32_t>(offsetof(RenderTargetSetup_t2400315372, ___color_0)); }
	inline RenderBufferU5BU5D_t1970987103* get_color_0() const { return ___color_0; }
	inline RenderBufferU5BU5D_t1970987103** get_address_of_color_0() { return &___color_0; }
	inline void set_color_0(RenderBufferU5BU5D_t1970987103* value)
	{
		___color_0 = value;
		Il2CppCodeGenWriteBarrier(&___color_0, value);
	}

	inline static int32_t get_offset_of_depth_1() { return static_cast<int32_t>(offsetof(RenderTargetSetup_t2400315372, ___depth_1)); }
	inline RenderBuffer_t3529837690  get_depth_1() const { return ___depth_1; }
	inline RenderBuffer_t3529837690 * get_address_of_depth_1() { return &___depth_1; }
	inline void set_depth_1(RenderBuffer_t3529837690  value)
	{
		___depth_1 = value;
	}

	inline static int32_t get_offset_of_mipLevel_2() { return static_cast<int32_t>(offsetof(RenderTargetSetup_t2400315372, ___mipLevel_2)); }
	inline int32_t get_mipLevel_2() const { return ___mipLevel_2; }
	inline int32_t* get_address_of_mipLevel_2() { return &___mipLevel_2; }
	inline void set_mipLevel_2(int32_t value)
	{
		___mipLevel_2 = value;
	}

	inline static int32_t get_offset_of_cubemapFace_3() { return static_cast<int32_t>(offsetof(RenderTargetSetup_t2400315372, ___cubemapFace_3)); }
	inline int32_t get_cubemapFace_3() const { return ___cubemapFace_3; }
	inline int32_t* get_address_of_cubemapFace_3() { return &___cubemapFace_3; }
	inline void set_cubemapFace_3(int32_t value)
	{
		___cubemapFace_3 = value;
	}

	inline static int32_t get_offset_of_colorLoad_4() { return static_cast<int32_t>(offsetof(RenderTargetSetup_t2400315372, ___colorLoad_4)); }
	inline RenderBufferLoadActionU5BU5D_t3840699671* get_colorLoad_4() const { return ___colorLoad_4; }
	inline RenderBufferLoadActionU5BU5D_t3840699671** get_address_of_colorLoad_4() { return &___colorLoad_4; }
	inline void set_colorLoad_4(RenderBufferLoadActionU5BU5D_t3840699671* value)
	{
		___colorLoad_4 = value;
		Il2CppCodeGenWriteBarrier(&___colorLoad_4, value);
	}

	inline static int32_t get_offset_of_colorStore_5() { return static_cast<int32_t>(offsetof(RenderTargetSetup_t2400315372, ___colorStore_5)); }
	inline RenderBufferStoreActionU5BU5D_t2354850566* get_colorStore_5() const { return ___colorStore_5; }
	inline RenderBufferStoreActionU5BU5D_t2354850566** get_address_of_colorStore_5() { return &___colorStore_5; }
	inline void set_colorStore_5(RenderBufferStoreActionU5BU5D_t2354850566* value)
	{
		___colorStore_5 = value;
		Il2CppCodeGenWriteBarrier(&___colorStore_5, value);
	}

	inline static int32_t get_offset_of_depthLoad_6() { return static_cast<int32_t>(offsetof(RenderTargetSetup_t2400315372, ___depthLoad_6)); }
	inline int32_t get_depthLoad_6() const { return ___depthLoad_6; }
	inline int32_t* get_address_of_depthLoad_6() { return &___depthLoad_6; }
	inline void set_depthLoad_6(int32_t value)
	{
		___depthLoad_6 = value;
	}

	inline static int32_t get_offset_of_depthStore_7() { return static_cast<int32_t>(offsetof(RenderTargetSetup_t2400315372, ___depthStore_7)); }
	inline int32_t get_depthStore_7() const { return ___depthStore_7; }
	inline int32_t* get_address_of_depthStore_7() { return &___depthStore_7; }
	inline void set_depthStore_7(int32_t value)
	{
		___depthStore_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: UnityEngine.RenderTargetSetup
struct RenderTargetSetup_t2400315372_marshaled_pinvoke
{
	RenderBuffer_t3529837690_marshaled_pinvoke* ___color_0;
	RenderBuffer_t3529837690_marshaled_pinvoke ___depth_1;
	int32_t ___mipLevel_2;
	int32_t ___cubemapFace_3;
	int32_t* ___colorLoad_4;
	int32_t* ___colorStore_5;
	int32_t ___depthLoad_6;
	int32_t ___depthStore_7;
};
// Native definition for marshalling of: UnityEngine.RenderTargetSetup
struct RenderTargetSetup_t2400315372_marshaled_com
{
	RenderBuffer_t3529837690_marshaled_com* ___color_0;
	RenderBuffer_t3529837690_marshaled_com ___depth_1;
	int32_t ___mipLevel_2;
	int32_t ___cubemapFace_3;
	int32_t* ___colorLoad_4;
	int32_t* ___colorStore_5;
	int32_t ___depthLoad_6;
	int32_t ___depthStore_7;
};
