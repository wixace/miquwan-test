﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pomelo.Protobuf.MsgDecoder
struct MsgDecoder_t242483159;
// Pomelo.Protobuf.MsgEncoder
struct MsgEncoder_t1387649199;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pomelo.Protobuf.Protobuf
struct  Protobuf_t1750100383  : public Il2CppObject
{
public:
	// Pomelo.Protobuf.MsgDecoder Pomelo.Protobuf.Protobuf::decoder
	MsgDecoder_t242483159 * ___decoder_0;
	// Pomelo.Protobuf.MsgEncoder Pomelo.Protobuf.Protobuf::encoder
	MsgEncoder_t1387649199 * ___encoder_1;

public:
	inline static int32_t get_offset_of_decoder_0() { return static_cast<int32_t>(offsetof(Protobuf_t1750100383, ___decoder_0)); }
	inline MsgDecoder_t242483159 * get_decoder_0() const { return ___decoder_0; }
	inline MsgDecoder_t242483159 ** get_address_of_decoder_0() { return &___decoder_0; }
	inline void set_decoder_0(MsgDecoder_t242483159 * value)
	{
		___decoder_0 = value;
		Il2CppCodeGenWriteBarrier(&___decoder_0, value);
	}

	inline static int32_t get_offset_of_encoder_1() { return static_cast<int32_t>(offsetof(Protobuf_t1750100383, ___encoder_1)); }
	inline MsgEncoder_t1387649199 * get_encoder_1() const { return ___encoder_1; }
	inline MsgEncoder_t1387649199 ** get_address_of_encoder_1() { return &___encoder_1; }
	inline void set_encoder_1(MsgEncoder_t1387649199 * value)
	{
		___encoder_1 = value;
		Il2CppCodeGenWriteBarrier(&___encoder_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
