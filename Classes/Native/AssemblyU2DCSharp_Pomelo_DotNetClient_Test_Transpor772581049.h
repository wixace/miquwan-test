﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t1333978725;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pomelo.DotNetClient.Test.TransportTest
struct  TransportTest_t772581049  : public Il2CppObject
{
public:

public:
};

struct TransportTest_t772581049_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Byte[]> Pomelo.DotNetClient.Test.TransportTest::result
	List_1_t1333978725 * ___result_0;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(TransportTest_t772581049_StaticFields, ___result_0)); }
	inline List_1_t1333978725 * get_result_0() const { return ___result_0; }
	inline List_1_t1333978725 ** get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(List_1_t1333978725 * value)
	{
		___result_0 = value;
		Il2CppCodeGenWriteBarrier(&___result_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
