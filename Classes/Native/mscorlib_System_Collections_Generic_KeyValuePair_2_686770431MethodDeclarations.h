﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,skill_upgradeCfg>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m4091628636(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t686770431 *, int32_t, skill_upgradeCfg_t790726486 *, const MethodInfo*))KeyValuePair_2__ctor_m11197230_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,skill_upgradeCfg>::get_Key()
#define KeyValuePair_2_get_Key_m983216012(__this, method) ((  int32_t (*) (KeyValuePair_2_t686770431 *, const MethodInfo*))KeyValuePair_2_get_Key_m494458106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,skill_upgradeCfg>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m376357965(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t686770431 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m4229413435_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,skill_upgradeCfg>::get_Value()
#define KeyValuePair_2_get_Value_m4267150106(__this, method) ((  skill_upgradeCfg_t790726486 * (*) (KeyValuePair_2_t686770431 *, const MethodInfo*))KeyValuePair_2_get_Value_m1563175098_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,skill_upgradeCfg>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m4060424653(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t686770431 *, skill_upgradeCfg_t790726486 *, const MethodInfo*))KeyValuePair_2_set_Value_m1296398523_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,skill_upgradeCfg>::ToString()
#define KeyValuePair_2_ToString_m2908239131(__this, method) ((  String_t* (*) (KeyValuePair_2_t686770431 *, const MethodInfo*))KeyValuePair_2_ToString_m491888647_gshared)(__this, method)
