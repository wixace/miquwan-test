﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Attribute2523058482.h"
#include "mscorlib_System_Nullable_1_gen2838778904.h"
#include "mscorlib_System_Nullable_1_gen1653574568.h"
#include "mscorlib_System_Nullable_1_gen2845787645.h"
#include "mscorlib_System_Nullable_1_gen140208118.h"
#include "mscorlib_System_Nullable_1_gen2443451997.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"
#include "mscorlib_System_Nullable_1_gen1237965023.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Required3921306327.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonPropertyAttribute
struct  JsonPropertyAttribute_t3435603053  : public Attribute_t2523058482
{
public:
	// System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.JsonPropertyAttribute::_nullValueHandling
	Nullable_1_t2838778904  ____nullValueHandling_0;
	// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling> Newtonsoft.Json.JsonPropertyAttribute::_defaultValueHandling
	Nullable_1_t1653574568  ____defaultValueHandling_1;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.JsonPropertyAttribute::_referenceLoopHandling
	Nullable_1_t2845787645  ____referenceLoopHandling_2;
	// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling> Newtonsoft.Json.JsonPropertyAttribute::_objectCreationHandling
	Nullable_1_t140208118  ____objectCreationHandling_3;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.JsonPropertyAttribute::_typeNameHandling
	Nullable_1_t2443451997  ____typeNameHandling_4;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonPropertyAttribute::_isReference
	Nullable_1_t560925241  ____isReference_5;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonPropertyAttribute::_order
	Nullable_1_t1237965023  ____order_6;
	// System.String Newtonsoft.Json.JsonPropertyAttribute::<PropertyName>k__BackingField
	String_t* ___U3CPropertyNameU3Ek__BackingField_7;
	// Newtonsoft.Json.Required Newtonsoft.Json.JsonPropertyAttribute::<Required>k__BackingField
	int32_t ___U3CRequiredU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of__nullValueHandling_0() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t3435603053, ____nullValueHandling_0)); }
	inline Nullable_1_t2838778904  get__nullValueHandling_0() const { return ____nullValueHandling_0; }
	inline Nullable_1_t2838778904 * get_address_of__nullValueHandling_0() { return &____nullValueHandling_0; }
	inline void set__nullValueHandling_0(Nullable_1_t2838778904  value)
	{
		____nullValueHandling_0 = value;
	}

	inline static int32_t get_offset_of__defaultValueHandling_1() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t3435603053, ____defaultValueHandling_1)); }
	inline Nullable_1_t1653574568  get__defaultValueHandling_1() const { return ____defaultValueHandling_1; }
	inline Nullable_1_t1653574568 * get_address_of__defaultValueHandling_1() { return &____defaultValueHandling_1; }
	inline void set__defaultValueHandling_1(Nullable_1_t1653574568  value)
	{
		____defaultValueHandling_1 = value;
	}

	inline static int32_t get_offset_of__referenceLoopHandling_2() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t3435603053, ____referenceLoopHandling_2)); }
	inline Nullable_1_t2845787645  get__referenceLoopHandling_2() const { return ____referenceLoopHandling_2; }
	inline Nullable_1_t2845787645 * get_address_of__referenceLoopHandling_2() { return &____referenceLoopHandling_2; }
	inline void set__referenceLoopHandling_2(Nullable_1_t2845787645  value)
	{
		____referenceLoopHandling_2 = value;
	}

	inline static int32_t get_offset_of__objectCreationHandling_3() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t3435603053, ____objectCreationHandling_3)); }
	inline Nullable_1_t140208118  get__objectCreationHandling_3() const { return ____objectCreationHandling_3; }
	inline Nullable_1_t140208118 * get_address_of__objectCreationHandling_3() { return &____objectCreationHandling_3; }
	inline void set__objectCreationHandling_3(Nullable_1_t140208118  value)
	{
		____objectCreationHandling_3 = value;
	}

	inline static int32_t get_offset_of__typeNameHandling_4() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t3435603053, ____typeNameHandling_4)); }
	inline Nullable_1_t2443451997  get__typeNameHandling_4() const { return ____typeNameHandling_4; }
	inline Nullable_1_t2443451997 * get_address_of__typeNameHandling_4() { return &____typeNameHandling_4; }
	inline void set__typeNameHandling_4(Nullable_1_t2443451997  value)
	{
		____typeNameHandling_4 = value;
	}

	inline static int32_t get_offset_of__isReference_5() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t3435603053, ____isReference_5)); }
	inline Nullable_1_t560925241  get__isReference_5() const { return ____isReference_5; }
	inline Nullable_1_t560925241 * get_address_of__isReference_5() { return &____isReference_5; }
	inline void set__isReference_5(Nullable_1_t560925241  value)
	{
		____isReference_5 = value;
	}

	inline static int32_t get_offset_of__order_6() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t3435603053, ____order_6)); }
	inline Nullable_1_t1237965023  get__order_6() const { return ____order_6; }
	inline Nullable_1_t1237965023 * get_address_of__order_6() { return &____order_6; }
	inline void set__order_6(Nullable_1_t1237965023  value)
	{
		____order_6 = value;
	}

	inline static int32_t get_offset_of_U3CPropertyNameU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t3435603053, ___U3CPropertyNameU3Ek__BackingField_7)); }
	inline String_t* get_U3CPropertyNameU3Ek__BackingField_7() const { return ___U3CPropertyNameU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CPropertyNameU3Ek__BackingField_7() { return &___U3CPropertyNameU3Ek__BackingField_7; }
	inline void set_U3CPropertyNameU3Ek__BackingField_7(String_t* value)
	{
		___U3CPropertyNameU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPropertyNameU3Ek__BackingField_7, value);
	}

	inline static int32_t get_offset_of_U3CRequiredU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(JsonPropertyAttribute_t3435603053, ___U3CRequiredU3Ek__BackingField_8)); }
	inline int32_t get_U3CRequiredU3Ek__BackingField_8() const { return ___U3CRequiredU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CRequiredU3Ek__BackingField_8() { return &___U3CRequiredU3Ek__BackingField_8; }
	inline void set_U3CRequiredU3Ek__BackingField_8(int32_t value)
	{
		___U3CRequiredU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
