﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICameraGenerated/<UICamera_onDragEnd_GetDelegate_member52_arg0>c__AnonStoreyAD
struct U3CUICamera_onDragEnd_GetDelegate_member52_arg0U3Ec__AnonStoreyAD_t2424362384;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UICameraGenerated/<UICamera_onDragEnd_GetDelegate_member52_arg0>c__AnonStoreyAD::.ctor()
extern "C"  void U3CUICamera_onDragEnd_GetDelegate_member52_arg0U3Ec__AnonStoreyAD__ctor_m3265983451 (U3CUICamera_onDragEnd_GetDelegate_member52_arg0U3Ec__AnonStoreyAD_t2424362384 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated/<UICamera_onDragEnd_GetDelegate_member52_arg0>c__AnonStoreyAD::<>m__11E(UnityEngine.GameObject)
extern "C"  void U3CUICamera_onDragEnd_GetDelegate_member52_arg0U3Ec__AnonStoreyAD_U3CU3Em__11E_m520736411 (U3CUICamera_onDragEnd_GetDelegate_member52_arg0U3Ec__AnonStoreyAD_t2424362384 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
