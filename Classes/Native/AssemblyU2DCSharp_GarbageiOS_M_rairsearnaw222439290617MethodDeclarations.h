﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_rairsearnaw222
struct M_rairsearnaw222_t439290617;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_rairsearnaw222::.ctor()
extern "C"  void M_rairsearnaw222__ctor_m2064438618 (M_rairsearnaw222_t439290617 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rairsearnaw222::M_tapelKelkairnay0(System.String[],System.Int32)
extern "C"  void M_rairsearnaw222_M_tapelKelkairnay0_m2230228772 (M_rairsearnaw222_t439290617 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rairsearnaw222::M_qoujule1(System.String[],System.Int32)
extern "C"  void M_rairsearnaw222_M_qoujule1_m2116770985 (M_rairsearnaw222_t439290617 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rairsearnaw222::M_cijaka2(System.String[],System.Int32)
extern "C"  void M_rairsearnaw222_M_cijaka2_m3091234812 (M_rairsearnaw222_t439290617 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rairsearnaw222::M_paremo3(System.String[],System.Int32)
extern "C"  void M_rairsearnaw222_M_paremo3_m3293398922 (M_rairsearnaw222_t439290617 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rairsearnaw222::M_trasu4(System.String[],System.Int32)
extern "C"  void M_rairsearnaw222_M_trasu4_m3024131650 (M_rairsearnaw222_t439290617 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
