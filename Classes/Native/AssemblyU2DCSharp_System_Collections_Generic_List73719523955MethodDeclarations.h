﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_FindIndex_GetDelegate_member15_arg2>c__AnonStorey83`1<System.Object>
struct U3CListA1_FindIndex_GetDelegate_member15_arg2U3Ec__AnonStorey83_1_t3719523955;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_FindIndex_GetDelegate_member15_arg2>c__AnonStorey83`1<System.Object>::.ctor()
extern "C"  void U3CListA1_FindIndex_GetDelegate_member15_arg2U3Ec__AnonStorey83_1__ctor_m2194871426_gshared (U3CListA1_FindIndex_GetDelegate_member15_arg2U3Ec__AnonStorey83_1_t3719523955 * __this, const MethodInfo* method);
#define U3CListA1_FindIndex_GetDelegate_member15_arg2U3Ec__AnonStorey83_1__ctor_m2194871426(__this, method) ((  void (*) (U3CListA1_FindIndex_GetDelegate_member15_arg2U3Ec__AnonStorey83_1_t3719523955 *, const MethodInfo*))U3CListA1_FindIndex_GetDelegate_member15_arg2U3Ec__AnonStorey83_1__ctor_m2194871426_gshared)(__this, method)
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_FindIndex_GetDelegate_member15_arg2>c__AnonStorey83`1<System.Object>::<>m__A8(T)
extern "C"  bool U3CListA1_FindIndex_GetDelegate_member15_arg2U3Ec__AnonStorey83_1_U3CU3Em__A8_m2554841416_gshared (U3CListA1_FindIndex_GetDelegate_member15_arg2U3Ec__AnonStorey83_1_t3719523955 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CListA1_FindIndex_GetDelegate_member15_arg2U3Ec__AnonStorey83_1_U3CU3Em__A8_m2554841416(__this, ___obj0, method) ((  bool (*) (U3CListA1_FindIndex_GetDelegate_member15_arg2U3Ec__AnonStorey83_1_t3719523955 *, Il2CppObject *, const MethodInfo*))U3CListA1_FindIndex_GetDelegate_member15_arg2U3Ec__AnonStorey83_1_U3CU3Em__A8_m2554841416_gshared)(__this, ___obj0, method)
