﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.RaycastHit>
struct DefaultComparer_t2450952393;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.RaycastHit>::.ctor()
extern "C"  void DefaultComparer__ctor_m1714860621_gshared (DefaultComparer_t2450952393 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1714860621(__this, method) ((  void (*) (DefaultComparer_t2450952393 *, const MethodInfo*))DefaultComparer__ctor_m1714860621_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.RaycastHit>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4011927814_gshared (DefaultComparer_t2450952393 * __this, RaycastHit_t4003175726  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m4011927814(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2450952393 *, RaycastHit_t4003175726 , const MethodInfo*))DefaultComparer_GetHashCode_m4011927814_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.RaycastHit>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m977725026_gshared (DefaultComparer_t2450952393 * __this, RaycastHit_t4003175726  ___x0, RaycastHit_t4003175726  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m977725026(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2450952393 *, RaycastHit_t4003175726 , RaycastHit_t4003175726 , const MethodInfo*))DefaultComparer_Equals_m977725026_gshared)(__this, ___x0, ___y1, method)
