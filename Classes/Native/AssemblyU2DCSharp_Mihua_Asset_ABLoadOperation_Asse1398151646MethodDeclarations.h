﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated
struct Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated_t1398151646;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// Mihua.Asset.ABLoadOperation.AssetOperationLoaded
struct AssetOperationLoaded_t667103362;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset667103362.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated::.ctor()
extern "C"  void Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated__ctor_m1127007565 (Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated_t1398151646 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated::AssetOperationLoaded_AssetOperationLoaded1(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated_AssetOperationLoaded_AssetOperationLoaded1_m1834541509 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated::AssetOperationLoaded_progress(JSVCall)
extern "C"  void Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated_AssetOperationLoaded_progress_m841082124 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated::AssetOperationLoaded_Clear(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated_AssetOperationLoaded_Clear_m909423517 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated::AssetOperationLoaded_GetAsset(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated_AssetOperationLoaded_GetAsset_m943678060 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated::AssetOperationLoaded_IsDone(JSVCall,System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated_AssetOperationLoaded_IsDone_m3925289918 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated::__Register()
extern "C"  void Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated___Register_m3960274842 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated_ilo_attachFinalizerObject1_m2074754890 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated::ilo_getStringS2(System.Int32)
extern "C"  String_t* Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated_ilo_getStringS2_m1333168690 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated::ilo_Clear3(Mihua.Asset.ABLoadOperation.AssetOperationLoaded)
extern "C"  void Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated_ilo_Clear3_m3445153394 (Il2CppObject * __this /* static, unused */, AssetOperationLoaded_t667103362 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t Mihua_Asset_ABLoadOperation_AssetOperationLoadedGenerated_ilo_setObject4_m766012100 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
