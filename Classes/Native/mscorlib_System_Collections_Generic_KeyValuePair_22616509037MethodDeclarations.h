﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_866109363MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3888845807(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2616509037 *, Int2_t1974045593 , TriangleMeshNode_t1626248749 *, const MethodInfo*))KeyValuePair_2__ctor_m3790321200_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::get_Key()
#define KeyValuePair_2_get_Key_m1395000985(__this, method) ((  Int2_t1974045593  (*) (KeyValuePair_2_t2616509037 *, const MethodInfo*))KeyValuePair_2_get_Key_m4013756344_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1777683674(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2616509037 *, Int2_t1974045593 , const MethodInfo*))KeyValuePair_2_set_Key_m93750777_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::get_Value()
#define KeyValuePair_2_get_Value_m2506337405(__this, method) ((  TriangleMeshNode_t1626248749 * (*) (KeyValuePair_2_t2616509037 *, const MethodInfo*))KeyValuePair_2_get_Value_m167978232_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2529862618(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2616509037 *, TriangleMeshNode_t1626248749 *, const MethodInfo*))KeyValuePair_2_set_Value_m2665231737_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,Pathfinding.TriangleMeshNode>::ToString()
#define KeyValuePair_2_ToString_m866720174(__this, method) ((  String_t* (*) (KeyValuePair_2_t2616509037 *, const MethodInfo*))KeyValuePair_2_ToString_m3662222985_gshared)(__this, method)
