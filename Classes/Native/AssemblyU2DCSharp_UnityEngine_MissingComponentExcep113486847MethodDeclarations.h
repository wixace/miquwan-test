﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_MissingComponentExceptionGenerated
struct UnityEngine_MissingComponentExceptionGenerated_t113486847;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_MissingComponentExceptionGenerated::.ctor()
extern "C"  void UnityEngine_MissingComponentExceptionGenerated__ctor_m1553190780 (UnityEngine_MissingComponentExceptionGenerated_t113486847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MissingComponentExceptionGenerated::MissingComponentException_MissingComponentException1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MissingComponentExceptionGenerated_MissingComponentException_MissingComponentException1_m1055947980 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MissingComponentExceptionGenerated::MissingComponentException_MissingComponentException2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MissingComponentExceptionGenerated_MissingComponentException_MissingComponentException2_m2300712461 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MissingComponentExceptionGenerated::MissingComponentException_MissingComponentException3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MissingComponentExceptionGenerated_MissingComponentException_MissingComponentException3_m3545476942 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MissingComponentExceptionGenerated::__Register()
extern "C"  void UnityEngine_MissingComponentExceptionGenerated___Register_m4222614219 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MissingComponentExceptionGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_MissingComponentExceptionGenerated_ilo_getObject1_m3665979542 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MissingComponentExceptionGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_MissingComponentExceptionGenerated_ilo_addJSCSRel2_m2135047353 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MissingComponentExceptionGenerated::ilo_attachFinalizerObject3(System.Int32)
extern "C"  bool UnityEngine_MissingComponentExceptionGenerated_ilo_attachFinalizerObject3_m875047269 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_MissingComponentExceptionGenerated::ilo_getStringS4(System.Int32)
extern "C"  String_t* UnityEngine_MissingComponentExceptionGenerated_ilo_getStringS4_m2972389693 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
