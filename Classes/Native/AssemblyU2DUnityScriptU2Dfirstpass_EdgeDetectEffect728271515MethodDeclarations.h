﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EdgeDetectEffectNormals
struct EdgeDetectEffectNormals_t728271515;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void EdgeDetectEffectNormals::.ctor()
extern "C"  void EdgeDetectEffectNormals__ctor_m2019444391 (EdgeDetectEffectNormals_t728271515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EdgeDetectEffectNormals::CheckResources()
extern "C"  bool EdgeDetectEffectNormals_CheckResources_m2979558356 (EdgeDetectEffectNormals_t728271515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EdgeDetectEffectNormals::Start()
extern "C"  void EdgeDetectEffectNormals_Start_m966582183 (EdgeDetectEffectNormals_t728271515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EdgeDetectEffectNormals::SetCameraFlag()
extern "C"  void EdgeDetectEffectNormals_SetCameraFlag_m2533427512 (EdgeDetectEffectNormals_t728271515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EdgeDetectEffectNormals::OnEnable()
extern "C"  void EdgeDetectEffectNormals_OnEnable_m3732515711 (EdgeDetectEffectNormals_t728271515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EdgeDetectEffectNormals::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void EdgeDetectEffectNormals_OnRenderImage_m2515312983 (EdgeDetectEffectNormals_t728271515 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EdgeDetectEffectNormals::Main()
extern "C"  void EdgeDetectEffectNormals_Main_m811921718 (EdgeDetectEffectNormals_t728271515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
