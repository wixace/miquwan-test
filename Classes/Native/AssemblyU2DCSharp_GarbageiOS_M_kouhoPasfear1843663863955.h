﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_kouhoPasfear184
struct  M_kouhoPasfear184_t3663863955  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_kouhoPasfear184::_wurnewhu
	String_t* ____wurnewhu_0;
	// System.Boolean GarbageiOS.M_kouhoPasfear184::_qirherefisXoochel
	bool ____qirherefisXoochel_1;
	// System.UInt32 GarbageiOS.M_kouhoPasfear184::_trersirlu
	uint32_t ____trersirlu_2;
	// System.Single GarbageiOS.M_kouhoPasfear184::_lehas
	float ____lehas_3;
	// System.Boolean GarbageiOS.M_kouhoPasfear184::_rodehalWootrouho
	bool ____rodehalWootrouho_4;
	// System.Int32 GarbageiOS.M_kouhoPasfear184::_traitislall
	int32_t ____traitislall_5;
	// System.Single GarbageiOS.M_kouhoPasfear184::_rertawTeraqu
	float ____rertawTeraqu_6;
	// System.Int32 GarbageiOS.M_kouhoPasfear184::_lirloserBalve
	int32_t ____lirloserBalve_7;
	// System.Boolean GarbageiOS.M_kouhoPasfear184::_counou
	bool ____counou_8;
	// System.Int32 GarbageiOS.M_kouhoPasfear184::_nispear
	int32_t ____nispear_9;
	// System.Int32 GarbageiOS.M_kouhoPasfear184::_sarse
	int32_t ____sarse_10;
	// System.UInt32 GarbageiOS.M_kouhoPasfear184::_yuni
	uint32_t ____yuni_11;
	// System.Int32 GarbageiOS.M_kouhoPasfear184::_peanezeaWhalljilea
	int32_t ____peanezeaWhalljilea_12;
	// System.Single GarbageiOS.M_kouhoPasfear184::_sitis
	float ____sitis_13;
	// System.UInt32 GarbageiOS.M_kouhoPasfear184::_gucaineeLesepe
	uint32_t ____gucaineeLesepe_14;
	// System.Boolean GarbageiOS.M_kouhoPasfear184::_darlirjeSerewheyair
	bool ____darlirjeSerewheyair_15;
	// System.UInt32 GarbageiOS.M_kouhoPasfear184::_girhouchooWaixearjas
	uint32_t ____girhouchooWaixearjas_16;

public:
	inline static int32_t get_offset_of__wurnewhu_0() { return static_cast<int32_t>(offsetof(M_kouhoPasfear184_t3663863955, ____wurnewhu_0)); }
	inline String_t* get__wurnewhu_0() const { return ____wurnewhu_0; }
	inline String_t** get_address_of__wurnewhu_0() { return &____wurnewhu_0; }
	inline void set__wurnewhu_0(String_t* value)
	{
		____wurnewhu_0 = value;
		Il2CppCodeGenWriteBarrier(&____wurnewhu_0, value);
	}

	inline static int32_t get_offset_of__qirherefisXoochel_1() { return static_cast<int32_t>(offsetof(M_kouhoPasfear184_t3663863955, ____qirherefisXoochel_1)); }
	inline bool get__qirherefisXoochel_1() const { return ____qirherefisXoochel_1; }
	inline bool* get_address_of__qirherefisXoochel_1() { return &____qirherefisXoochel_1; }
	inline void set__qirherefisXoochel_1(bool value)
	{
		____qirherefisXoochel_1 = value;
	}

	inline static int32_t get_offset_of__trersirlu_2() { return static_cast<int32_t>(offsetof(M_kouhoPasfear184_t3663863955, ____trersirlu_2)); }
	inline uint32_t get__trersirlu_2() const { return ____trersirlu_2; }
	inline uint32_t* get_address_of__trersirlu_2() { return &____trersirlu_2; }
	inline void set__trersirlu_2(uint32_t value)
	{
		____trersirlu_2 = value;
	}

	inline static int32_t get_offset_of__lehas_3() { return static_cast<int32_t>(offsetof(M_kouhoPasfear184_t3663863955, ____lehas_3)); }
	inline float get__lehas_3() const { return ____lehas_3; }
	inline float* get_address_of__lehas_3() { return &____lehas_3; }
	inline void set__lehas_3(float value)
	{
		____lehas_3 = value;
	}

	inline static int32_t get_offset_of__rodehalWootrouho_4() { return static_cast<int32_t>(offsetof(M_kouhoPasfear184_t3663863955, ____rodehalWootrouho_4)); }
	inline bool get__rodehalWootrouho_4() const { return ____rodehalWootrouho_4; }
	inline bool* get_address_of__rodehalWootrouho_4() { return &____rodehalWootrouho_4; }
	inline void set__rodehalWootrouho_4(bool value)
	{
		____rodehalWootrouho_4 = value;
	}

	inline static int32_t get_offset_of__traitislall_5() { return static_cast<int32_t>(offsetof(M_kouhoPasfear184_t3663863955, ____traitislall_5)); }
	inline int32_t get__traitislall_5() const { return ____traitislall_5; }
	inline int32_t* get_address_of__traitislall_5() { return &____traitislall_5; }
	inline void set__traitislall_5(int32_t value)
	{
		____traitislall_5 = value;
	}

	inline static int32_t get_offset_of__rertawTeraqu_6() { return static_cast<int32_t>(offsetof(M_kouhoPasfear184_t3663863955, ____rertawTeraqu_6)); }
	inline float get__rertawTeraqu_6() const { return ____rertawTeraqu_6; }
	inline float* get_address_of__rertawTeraqu_6() { return &____rertawTeraqu_6; }
	inline void set__rertawTeraqu_6(float value)
	{
		____rertawTeraqu_6 = value;
	}

	inline static int32_t get_offset_of__lirloserBalve_7() { return static_cast<int32_t>(offsetof(M_kouhoPasfear184_t3663863955, ____lirloserBalve_7)); }
	inline int32_t get__lirloserBalve_7() const { return ____lirloserBalve_7; }
	inline int32_t* get_address_of__lirloserBalve_7() { return &____lirloserBalve_7; }
	inline void set__lirloserBalve_7(int32_t value)
	{
		____lirloserBalve_7 = value;
	}

	inline static int32_t get_offset_of__counou_8() { return static_cast<int32_t>(offsetof(M_kouhoPasfear184_t3663863955, ____counou_8)); }
	inline bool get__counou_8() const { return ____counou_8; }
	inline bool* get_address_of__counou_8() { return &____counou_8; }
	inline void set__counou_8(bool value)
	{
		____counou_8 = value;
	}

	inline static int32_t get_offset_of__nispear_9() { return static_cast<int32_t>(offsetof(M_kouhoPasfear184_t3663863955, ____nispear_9)); }
	inline int32_t get__nispear_9() const { return ____nispear_9; }
	inline int32_t* get_address_of__nispear_9() { return &____nispear_9; }
	inline void set__nispear_9(int32_t value)
	{
		____nispear_9 = value;
	}

	inline static int32_t get_offset_of__sarse_10() { return static_cast<int32_t>(offsetof(M_kouhoPasfear184_t3663863955, ____sarse_10)); }
	inline int32_t get__sarse_10() const { return ____sarse_10; }
	inline int32_t* get_address_of__sarse_10() { return &____sarse_10; }
	inline void set__sarse_10(int32_t value)
	{
		____sarse_10 = value;
	}

	inline static int32_t get_offset_of__yuni_11() { return static_cast<int32_t>(offsetof(M_kouhoPasfear184_t3663863955, ____yuni_11)); }
	inline uint32_t get__yuni_11() const { return ____yuni_11; }
	inline uint32_t* get_address_of__yuni_11() { return &____yuni_11; }
	inline void set__yuni_11(uint32_t value)
	{
		____yuni_11 = value;
	}

	inline static int32_t get_offset_of__peanezeaWhalljilea_12() { return static_cast<int32_t>(offsetof(M_kouhoPasfear184_t3663863955, ____peanezeaWhalljilea_12)); }
	inline int32_t get__peanezeaWhalljilea_12() const { return ____peanezeaWhalljilea_12; }
	inline int32_t* get_address_of__peanezeaWhalljilea_12() { return &____peanezeaWhalljilea_12; }
	inline void set__peanezeaWhalljilea_12(int32_t value)
	{
		____peanezeaWhalljilea_12 = value;
	}

	inline static int32_t get_offset_of__sitis_13() { return static_cast<int32_t>(offsetof(M_kouhoPasfear184_t3663863955, ____sitis_13)); }
	inline float get__sitis_13() const { return ____sitis_13; }
	inline float* get_address_of__sitis_13() { return &____sitis_13; }
	inline void set__sitis_13(float value)
	{
		____sitis_13 = value;
	}

	inline static int32_t get_offset_of__gucaineeLesepe_14() { return static_cast<int32_t>(offsetof(M_kouhoPasfear184_t3663863955, ____gucaineeLesepe_14)); }
	inline uint32_t get__gucaineeLesepe_14() const { return ____gucaineeLesepe_14; }
	inline uint32_t* get_address_of__gucaineeLesepe_14() { return &____gucaineeLesepe_14; }
	inline void set__gucaineeLesepe_14(uint32_t value)
	{
		____gucaineeLesepe_14 = value;
	}

	inline static int32_t get_offset_of__darlirjeSerewheyair_15() { return static_cast<int32_t>(offsetof(M_kouhoPasfear184_t3663863955, ____darlirjeSerewheyair_15)); }
	inline bool get__darlirjeSerewheyair_15() const { return ____darlirjeSerewheyair_15; }
	inline bool* get_address_of__darlirjeSerewheyair_15() { return &____darlirjeSerewheyair_15; }
	inline void set__darlirjeSerewheyair_15(bool value)
	{
		____darlirjeSerewheyair_15 = value;
	}

	inline static int32_t get_offset_of__girhouchooWaixearjas_16() { return static_cast<int32_t>(offsetof(M_kouhoPasfear184_t3663863955, ____girhouchooWaixearjas_16)); }
	inline uint32_t get__girhouchooWaixearjas_16() const { return ____girhouchooWaixearjas_16; }
	inline uint32_t* get_address_of__girhouchooWaixearjas_16() { return &____girhouchooWaixearjas_16; }
	inline void set__girhouchooWaixearjas_16(uint32_t value)
	{
		____girhouchooWaixearjas_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
