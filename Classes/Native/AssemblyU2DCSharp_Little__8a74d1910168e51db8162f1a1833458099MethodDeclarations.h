﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._8a74d1910168e51db8162f1ac073e2c7
struct _8a74d1910168e51db8162f1ac073e2c7_t1833458099;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._8a74d1910168e51db8162f1ac073e2c7::.ctor()
extern "C"  void _8a74d1910168e51db8162f1ac073e2c7__ctor_m388564442 (_8a74d1910168e51db8162f1ac073e2c7_t1833458099 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8a74d1910168e51db8162f1ac073e2c7::_8a74d1910168e51db8162f1ac073e2c7m2(System.Int32)
extern "C"  int32_t _8a74d1910168e51db8162f1ac073e2c7__8a74d1910168e51db8162f1ac073e2c7m2_m882023609 (_8a74d1910168e51db8162f1ac073e2c7_t1833458099 * __this, int32_t ____8a74d1910168e51db8162f1ac073e2c7a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8a74d1910168e51db8162f1ac073e2c7::_8a74d1910168e51db8162f1ac073e2c7m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _8a74d1910168e51db8162f1ac073e2c7__8a74d1910168e51db8162f1ac073e2c7m_m3160329757 (_8a74d1910168e51db8162f1ac073e2c7_t1833458099 * __this, int32_t ____8a74d1910168e51db8162f1ac073e2c7a0, int32_t ____8a74d1910168e51db8162f1ac073e2c7201, int32_t ____8a74d1910168e51db8162f1ac073e2c7c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
