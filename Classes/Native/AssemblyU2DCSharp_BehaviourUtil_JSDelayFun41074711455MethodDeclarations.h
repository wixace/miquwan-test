﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/JSDelayFun4
struct JSDelayFun4_t1074711455;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void BehaviourUtil/JSDelayFun4::.ctor(System.Object,System.IntPtr)
extern "C"  void JSDelayFun4__ctor_m1590260726 (JSDelayFun4_t1074711455 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/JSDelayFun4::Invoke(System.Object,System.Object,System.Object,System.Object)
extern "C"  void JSDelayFun4_Invoke_m3573323182 (JSDelayFun4_t1074711455 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult BehaviourUtil/JSDelayFun4::BeginInvoke(System.Object,System.Object,System.Object,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * JSDelayFun4_BeginInvoke_m4110008963 (JSDelayFun4_t1074711455 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, Il2CppObject * ___arg43, AsyncCallback_t1369114871 * ___callback4, Il2CppObject * ___object5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/JSDelayFun4::EndInvoke(System.IAsyncResult)
extern "C"  void JSDelayFun4_EndInvoke_m4155592966 (JSDelayFun4_t1074711455 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
