﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TeamSkill
struct TeamSkill_t1816898004;
// superskillCfg
struct superskillCfg_t3024131278;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_superskillCfg3024131278.h"

// System.Void TeamSkill::.ctor(superskillCfg)
extern "C"  void TeamSkill__ctor_m2828590573 (TeamSkill_t1816898004 * __this, superskillCfg_t3024131278 * ___skillCfg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TeamSkill::get_isSelect()
extern "C"  bool TeamSkill_get_isSelect_m3904848328 (TeamSkill_t1816898004 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
