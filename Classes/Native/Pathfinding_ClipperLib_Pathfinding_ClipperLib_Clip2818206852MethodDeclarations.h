﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.ClipperLib.ClipperException
struct ClipperException_t2818206852;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void Pathfinding.ClipperLib.ClipperException::.ctor(System.String)
extern "C"  void ClipperException__ctor_m106504313 (ClipperException_t2818206852 * __this, String_t* ___description0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
