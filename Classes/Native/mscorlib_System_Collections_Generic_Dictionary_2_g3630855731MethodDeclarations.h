﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>
struct Dictionary_2_t3630855731;
// System.Collections.Generic.IEqualityComparer`1<AIEnum.EAIEventtype>
struct IEqualityComparer_1_t392356419;
// System.Collections.Generic.IDictionary`2<AIEnum.EAIEventtype,System.Object>
struct IDictionary_2_t3208729076;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<AIEnum.EAIEventtype>
struct ICollection_1_t495912002;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<AIEnum.EAIEventtype,System.Object>[]
struct KeyValuePair_2U5BU5D_t3174887608;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<AIEnum.EAIEventtype,System.Object>>
struct IEnumerator_1_t1146534190;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Object>
struct KeyCollection_t962647886;
// System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Object>
struct ValueCollection_t2331461444;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23529636437.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_AIEnum_EAIEventtype3896289311.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En653211827.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m3464281485_gshared (Dictionary_2_t3630855731 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3464281485(__this, method) ((  void (*) (Dictionary_2_t3630855731 *, const MethodInfo*))Dictionary_2__ctor_m3464281485_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3155105668_gshared (Dictionary_2_t3630855731 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3155105668(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3630855731 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3155105668_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m3334721067_gshared (Dictionary_2_t3630855731 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m3334721067(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t3630855731 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3334721067_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m620857182_gshared (Dictionary_2_t3630855731 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m620857182(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3630855731 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m620857182_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2978690738_gshared (Dictionary_2_t3630855731 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2978690738(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t3630855731 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2978690738_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2234577230_gshared (Dictionary_2_t3630855731 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2234577230(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3630855731 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m2234577230_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1640362721_gshared (Dictionary_2_t3630855731 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1640362721(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3630855731 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1640362721_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3372006497_gshared (Dictionary_2_t3630855731 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3372006497(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3630855731 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3372006497_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m1880917779_gshared (Dictionary_2_t3630855731 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1880917779(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3630855731 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m1880917779_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m1748308801_gshared (Dictionary_2_t3630855731 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m1748308801(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3630855731 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m1748308801_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2715983860_gshared (Dictionary_2_t3630855731 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2715983860(__this, method) ((  bool (*) (Dictionary_2_t3630855731 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2715983860_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1788547205_gshared (Dictionary_2_t3630855731 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1788547205(__this, method) ((  bool (*) (Dictionary_2_t3630855731 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1788547205_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m2965909915_gshared (Dictionary_2_t3630855731 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2965909915(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3630855731 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2965909915_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m4070553280_gshared (Dictionary_2_t3630855731 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m4070553280(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3630855731 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m4070553280_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m3797897105_gshared (Dictionary_2_t3630855731 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m3797897105(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3630855731 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m3797897105_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m2877536453_gshared (Dictionary_2_t3630855731 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m2877536453(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3630855731 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2877536453_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2232922366_gshared (Dictionary_2_t3630855731 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m2232922366(__this, ___key0, method) ((  void (*) (Dictionary_2_t3630855731 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2232922366_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2793223343_gshared (Dictionary_2_t3630855731 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2793223343(__this, method) ((  bool (*) (Dictionary_2_t3630855731 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2793223343_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2402814875_gshared (Dictionary_2_t3630855731 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2402814875(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3630855731 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2402814875_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1090462771_gshared (Dictionary_2_t3630855731 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1090462771(__this, method) ((  bool (*) (Dictionary_2_t3630855731 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1090462771_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m515917460_gshared (Dictionary_2_t3630855731 * __this, KeyValuePair_2_t3529636437  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m515917460(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3630855731 *, KeyValuePair_2_t3529636437 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m515917460_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2896941742_gshared (Dictionary_2_t3630855731 * __this, KeyValuePair_2_t3529636437  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2896941742(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3630855731 *, KeyValuePair_2_t3529636437 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2896941742_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m957888888_gshared (Dictionary_2_t3630855731 * __this, KeyValuePair_2U5BU5D_t3174887608* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m957888888(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3630855731 *, KeyValuePair_2U5BU5D_t3174887608*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m957888888_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m4241149267_gshared (Dictionary_2_t3630855731 * __this, KeyValuePair_2_t3529636437  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m4241149267(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3630855731 *, KeyValuePair_2_t3529636437 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m4241149267_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m2566437975_gshared (Dictionary_2_t3630855731 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2566437975(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3630855731 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m2566437975_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1146165650_gshared (Dictionary_2_t3630855731 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1146165650(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3630855731 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1146165650_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2747728079_gshared (Dictionary_2_t3630855731 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2747728079(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3630855731 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2747728079_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m862633386_gshared (Dictionary_2_t3630855731 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m862633386(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3630855731 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m862633386_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3371522933_gshared (Dictionary_2_t3630855731 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m3371522933(__this, method) ((  int32_t (*) (Dictionary_2_t3630855731 *, const MethodInfo*))Dictionary_2_get_Count_m3371522933_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m2388779862_gshared (Dictionary_2_t3630855731 * __this, uint8_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m2388779862(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3630855731 *, uint8_t, const MethodInfo*))Dictionary_2_get_Item_m2388779862_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m549173389_gshared (Dictionary_2_t3630855731 * __this, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m549173389(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3630855731 *, uint8_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m549173389_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m2159979781_gshared (Dictionary_2_t3630855731 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m2159979781(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3630855731 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m2159979781_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m4157114706_gshared (Dictionary_2_t3630855731 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m4157114706(__this, ___size0, method) ((  void (*) (Dictionary_2_t3630855731 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m4157114706_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3111103950_gshared (Dictionary_2_t3630855731 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3111103950(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3630855731 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3111103950_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t3529636437  Dictionary_2_make_pair_m1548760090_gshared (Il2CppObject * __this /* static, unused */, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m1548760090(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3529636437  (*) (Il2CppObject * /* static, unused */, uint8_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m1548760090_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::pick_key(TKey,TValue)
extern "C"  uint8_t Dictionary_2_pick_key_m2457108380_gshared (Il2CppObject * __this /* static, unused */, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m2457108380(__this /* static, unused */, ___key0, ___value1, method) ((  uint8_t (*) (Il2CppObject * /* static, unused */, uint8_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m2457108380_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m1452238300_gshared (Il2CppObject * __this /* static, unused */, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m1452238300(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, uint8_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m1452238300_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m3882441281_gshared (Dictionary_2_t3630855731 * __this, KeyValuePair_2U5BU5D_t3174887608* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m3882441281(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3630855731 *, KeyValuePair_2U5BU5D_t3174887608*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m3882441281_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m76340299_gshared (Dictionary_2_t3630855731 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m76340299(__this, method) ((  void (*) (Dictionary_2_t3630855731 *, const MethodInfo*))Dictionary_2_Resize_m76340299_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m3993134536_gshared (Dictionary_2_t3630855731 * __this, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m3993134536(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3630855731 *, uint8_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m3993134536_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m870414776_gshared (Dictionary_2_t3630855731 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m870414776(__this, method) ((  void (*) (Dictionary_2_t3630855731 *, const MethodInfo*))Dictionary_2_Clear_m870414776_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m3911803422_gshared (Dictionary_2_t3630855731 * __this, uint8_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m3911803422(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3630855731 *, uint8_t, const MethodInfo*))Dictionary_2_ContainsKey_m3911803422_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2276095774_gshared (Dictionary_2_t3630855731 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m2276095774(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3630855731 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m2276095774_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m1858800299_gshared (Dictionary_2_t3630855731 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1858800299(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3630855731 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m1858800299_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m1209568409_gshared (Dictionary_2_t3630855731 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1209568409(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3630855731 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1209568409_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m67992594_gshared (Dictionary_2_t3630855731 * __this, uint8_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m67992594(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3630855731 *, uint8_t, const MethodInfo*))Dictionary_2_Remove_m67992594_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m1379449975_gshared (Dictionary_2_t3630855731 * __this, uint8_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1379449975(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3630855731 *, uint8_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m1379449975_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::get_Keys()
extern "C"  KeyCollection_t962647886 * Dictionary_2_get_Keys_m972455272_gshared (Dictionary_2_t3630855731 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m972455272(__this, method) ((  KeyCollection_t962647886 * (*) (Dictionary_2_t3630855731 *, const MethodInfo*))Dictionary_2_get_Keys_m972455272_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::get_Values()
extern "C"  ValueCollection_t2331461444 * Dictionary_2_get_Values_m2033489512_gshared (Dictionary_2_t3630855731 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m2033489512(__this, method) ((  ValueCollection_t2331461444 * (*) (Dictionary_2_t3630855731 *, const MethodInfo*))Dictionary_2_get_Values_m2033489512_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::ToTKey(System.Object)
extern "C"  uint8_t Dictionary_2_ToTKey_m1906967287_gshared (Dictionary_2_t3630855731 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1906967287(__this, ___key0, method) ((  uint8_t (*) (Dictionary_2_t3630855731 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1906967287_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m4059519735_gshared (Dictionary_2_t3630855731 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m4059519735(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t3630855731 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m4059519735_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m2479167989_gshared (Dictionary_2_t3630855731 * __this, KeyValuePair_2_t3529636437  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m2479167989(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3630855731 *, KeyValuePair_2_t3529636437 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m2479167989_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::GetEnumerator()
extern "C"  Enumerator_t653211827  Dictionary_2_GetEnumerator_m1282515666_gshared (Dictionary_2_t3630855731 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1282515666(__this, method) ((  Enumerator_t653211827  (*) (Dictionary_2_t3630855731 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1282515666_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m228584353_gshared (Il2CppObject * __this /* static, unused */, uint8_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m228584353(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, uint8_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m228584353_gshared)(__this /* static, unused */, ___key0, ___value1, method)
