﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Artngame.SKYMASTER.WaterBaseSM
struct WaterBaseSM_t3016193228;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"

// System.Void Artngame.SKYMASTER.WaterBaseSM::.ctor()
extern "C"  void WaterBaseSM__ctor_m2283267989 (WaterBaseSM_t3016193228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Artngame.SKYMASTER.WaterBaseSM::UpdateShader()
extern "C"  void WaterBaseSM_UpdateShader_m2080106397 (WaterBaseSM_t3016193228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Artngame.SKYMASTER.WaterBaseSM::WaterTileBeingRendered(UnityEngine.Transform,UnityEngine.Camera)
extern "C"  void WaterBaseSM_WaterTileBeingRendered_m4117315915 (WaterBaseSM_t3016193228 * __this, Transform_t1659122786 * ___tr0, Camera_t2727095145 * ___currentCam1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Artngame.SKYMASTER.WaterBaseSM::Update()
extern "C"  void WaterBaseSM_Update_m3788693016 (WaterBaseSM_t3016193228 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
