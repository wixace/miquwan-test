﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_FontGenerated
struct UnityEngine_FontGenerated_t1906546072;
// JSVCall
struct JSVCall_t3708497963;
// System.String[]
struct StringU5BU5D_t4054002952;
// UnityEngine.CharacterInfo[]
struct CharacterInfoU5BU5D_t4214273472;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_FontGenerated::.ctor()
extern "C"  void UnityEngine_FontGenerated__ctor_m2627959507 (UnityEngine_FontGenerated_t1906546072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_FontGenerated::Font_Font1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_FontGenerated_Font_Font1_m4199895327 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_FontGenerated::Font_Font2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_FontGenerated_Font_Font2_m1149692512 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_FontGenerated::Font_material(JSVCall)
extern "C"  void UnityEngine_FontGenerated_Font_material_m2078321503 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_FontGenerated::Font_fontNames(JSVCall)
extern "C"  void UnityEngine_FontGenerated_Font_fontNames_m3494676045 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_FontGenerated::Font_characterInfo(JSVCall)
extern "C"  void UnityEngine_FontGenerated_Font_characterInfo_m3673700559 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_FontGenerated::Font_dynamic(JSVCall)
extern "C"  void UnityEngine_FontGenerated_Font_dynamic_m362952967 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_FontGenerated::Font_ascent(JSVCall)
extern "C"  void UnityEngine_FontGenerated_Font_ascent_m3786155276 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_FontGenerated::Font_lineHeight(JSVCall)
extern "C"  void UnityEngine_FontGenerated_Font_lineHeight_m1106991307 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_FontGenerated::Font_fontSize(JSVCall)
extern "C"  void UnityEngine_FontGenerated_Font_fontSize_m3095765430 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_FontGenerated::Font_GetCharacterInfo__Char__CharacterInfo__Int32__FontStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_FontGenerated_Font_GetCharacterInfo__Char__CharacterInfo__Int32__FontStyle_m636981873 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_FontGenerated::Font_GetCharacterInfo__Char__CharacterInfo__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_FontGenerated_Font_GetCharacterInfo__Char__CharacterInfo__Int32_m1987133331 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_FontGenerated::Font_GetCharacterInfo__Char__CharacterInfo(JSVCall,System.Int32)
extern "C"  bool UnityEngine_FontGenerated_Font_GetCharacterInfo__Char__CharacterInfo_m4072725789 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_FontGenerated::Font_HasCharacter__Char(JSVCall,System.Int32)
extern "C"  bool UnityEngine_FontGenerated_Font_HasCharacter__Char_m1845916874 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_FontGenerated::Font_RequestCharactersInTexture__String__Int32__FontStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_FontGenerated_Font_RequestCharactersInTexture__String__Int32__FontStyle_m3354909447 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_FontGenerated::Font_RequestCharactersInTexture__String__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_FontGenerated_Font_RequestCharactersInTexture__String__Int32_m4043864253 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_FontGenerated::Font_RequestCharactersInTexture__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_FontGenerated_Font_RequestCharactersInTexture__String_m3400613683 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_FontGenerated::Font_CreateDynamicFontFromOSFont__String__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_FontGenerated_Font_CreateDynamicFontFromOSFont__String__Int32_m3234584339 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_FontGenerated::Font_CreateDynamicFontFromOSFont__String_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_FontGenerated_Font_CreateDynamicFontFromOSFont__String_Array__Int32_m1484107033 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_FontGenerated::Font_GetMaxVertsForString__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_FontGenerated_Font_GetMaxVertsForString__String_m496428892 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_FontGenerated::Font_GetOSInstalledFontNames(JSVCall,System.Int32)
extern "C"  bool UnityEngine_FontGenerated_Font_GetOSInstalledFontNames_m3794499670 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_FontGenerated::__Register()
extern "C"  void UnityEngine_FontGenerated___Register_m3458780628 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UnityEngine_FontGenerated::<Font_fontNames>m__1B5()
extern "C"  StringU5BU5D_t4054002952* UnityEngine_FontGenerated_U3CFont_fontNamesU3Em__1B5_m572768384 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CharacterInfo[] UnityEngine_FontGenerated::<Font_characterInfo>m__1B6()
extern "C"  CharacterInfoU5BU5D_t4214273472* UnityEngine_FontGenerated_U3CFont_characterInfoU3Em__1B6_m1504584883 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UnityEngine_FontGenerated::<Font_CreateDynamicFontFromOSFont__String_Array__Int32>m__1B7()
extern "C"  StringU5BU5D_t4054002952* UnityEngine_FontGenerated_U3CFont_CreateDynamicFontFromOSFont__String_Array__Int32U3Em__1B7_m4157364353 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_FontGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_FontGenerated_ilo_attachFinalizerObject1_m962921092 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_FontGenerated::ilo_getStringS2(System.Int32)
extern "C"  String_t* UnityEngine_FontGenerated_ilo_getStringS2_m944964408 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_FontGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_FontGenerated_ilo_setObject3_m2453159613 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_FontGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_FontGenerated_ilo_getObject4_m2623945205 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_FontGenerated::ilo_moveSaveID2Arr5(System.Int32)
extern "C"  void UnityEngine_FontGenerated_ilo_moveSaveID2Arr5_m3478183980 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_FontGenerated::ilo_setArrayS6(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_FontGenerated_ilo_setArrayS6_m669035605 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_FontGenerated::ilo_setBooleanS7(System.Int32,System.Boolean)
extern "C"  void UnityEngine_FontGenerated_ilo_setBooleanS7_m2153721022 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_FontGenerated::ilo_setInt328(System.Int32,System.Int32)
extern "C"  void UnityEngine_FontGenerated_ilo_setInt328_m432616188 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 UnityEngine_FontGenerated::ilo_getChar9(System.Int32)
extern "C"  int16_t UnityEngine_FontGenerated_ilo_getChar9_m555466458 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_FontGenerated::ilo_getInt3210(System.Int32)
extern "C"  int32_t UnityEngine_FontGenerated_ilo_getInt3210_m2189958110 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_FontGenerated::ilo_getEnum11(System.Int32)
extern "C"  int32_t UnityEngine_FontGenerated_ilo_getEnum11_m2058091892 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_FontGenerated::ilo_incArgIndex12()
extern "C"  int32_t UnityEngine_FontGenerated_ilo_incArgIndex12_m1494443527 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_FontGenerated::ilo_getElement13(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_FontGenerated_ilo_getElement13_m3752747496 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_FontGenerated::ilo_getObject14(System.Int32)
extern "C"  int32_t UnityEngine_FontGenerated_ilo_getObject14_m4263916085 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_FontGenerated::ilo_getArrayLength15(System.Int32)
extern "C"  int32_t UnityEngine_FontGenerated_ilo_getArrayLength15_m458886644 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
