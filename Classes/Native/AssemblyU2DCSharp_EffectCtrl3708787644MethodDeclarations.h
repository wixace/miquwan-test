﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EffectCtrl
struct EffectCtrl_t3708787644;
// ActorLine
struct ActorLine_t2375805801;
// ActorBullet
struct ActorBullet_t2246481719;
// LoopEffect
struct LoopEffect_t2053225845;
// TimeEffect
struct TimeEffect_t2372650718;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Transform
struct Transform_t1659122786;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// IEffComponent
struct IEffComponent_t3802264961;
// SoundMgr
struct SoundMgr_t1807284905;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// IZUpdate
struct IZUpdate_t3482043738;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ActorLine2375805801.h"
#include "AssemblyU2DCSharp_ActorBullet2246481719.h"
#include "AssemblyU2DCSharp_LoopEffect2053225845.h"
#include "AssemblyU2DCSharp_TimeEffect2372650718.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_EffectCtrl3708787644.h"
#include "AssemblyU2DCSharp_IEffComponent3802264961.h"
#include "AssemblyU2DCSharp_SoundMgr1807284905.h"
#include "mscorlib_System_String7231557.h"

// System.Void EffectCtrl::.ctor()
extern "C"  void EffectCtrl__ctor_m3708383327 (EffectCtrl_t3708787644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::set_actorLine(ActorLine)
extern "C"  void EffectCtrl_set_actorLine_m2053187456 (EffectCtrl_t3708787644 * __this, ActorLine_t2375805801 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ActorLine EffectCtrl::get_actorLine()
extern "C"  ActorLine_t2375805801 * EffectCtrl_get_actorLine_m3041492275 (EffectCtrl_t3708787644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::set_actorBullet(ActorBullet)
extern "C"  void EffectCtrl_set_actorBullet_m267973668 (EffectCtrl_t3708787644 * __this, ActorBullet_t2246481719 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ActorBullet EffectCtrl::get_actorBullet()
extern "C"  ActorBullet_t2246481719 * EffectCtrl_get_actorBullet_m2060075343 (EffectCtrl_t3708787644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::set_loopEffect(LoopEffect)
extern "C"  void EffectCtrl_set_loopEffect_m3230273634 (EffectCtrl_t3708787644 * __this, LoopEffect_t2053225845 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LoopEffect EffectCtrl::get_loopEffect()
extern "C"  LoopEffect_t2053225845 * EffectCtrl_get_loopEffect_m3619313387 (EffectCtrl_t3708787644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::set_timeEffect(TimeEffect)
extern "C"  void EffectCtrl_set_timeEffect_m1371760642 (EffectCtrl_t3708787644 * __this, TimeEffect_t2372650718 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TimeEffect EffectCtrl::get_timeEffect()
extern "C"  TimeEffect_t2372650718 * EffectCtrl_get_timeEffect_m712549707 (EffectCtrl_t3708787644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::set_effGO(UnityEngine.GameObject)
extern "C"  void EffectCtrl_set_effGO_m4180960453 (EffectCtrl_t3708787644 * __this, GameObject_t3674682005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject EffectCtrl::get_effGO()
extern "C"  GameObject_t3674682005 * EffectCtrl_get_effGO_m3582965164 (EffectCtrl_t3708787644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::set_effTr(UnityEngine.Transform)
extern "C"  void EffectCtrl_set_effTr_m12742106 (EffectCtrl_t3708787644 * __this, Transform_t1659122786 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform EffectCtrl::get_effTr()
extern "C"  Transform_t1659122786 * EffectCtrl_get_effTr_m1841837593 (EffectCtrl_t3708787644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::Init(UnityEngine.GameObject)
extern "C"  void EffectCtrl_Init_m693704653 (EffectCtrl_t3708787644 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::StartPlay()
extern "C"  void EffectCtrl_StartPlay_m108405939 (EffectCtrl_t3708787644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::SetPlaySpeed()
extern "C"  void EffectCtrl_SetPlaySpeed_m1288667126 (EffectCtrl_t3708787644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::SetPause()
extern "C"  void EffectCtrl_SetPause_m1141808313 (EffectCtrl_t3708787644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::SetCancelPause()
extern "C"  void EffectCtrl_SetCancelPause_m3885674335 (EffectCtrl_t3708787644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::Stop()
extern "C"  void EffectCtrl_Stop_m4103947399 (EffectCtrl_t3708787644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::ZUpdate()
extern "C"  void EffectCtrl_ZUpdate_m2797018016 (EffectCtrl_t3708787644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::ScrDeadReset(CEvent.ZEvent)
extern "C"  void EffectCtrl_ScrDeadReset_m3878035245 (EffectCtrl_t3708787644 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::SoundClear()
extern "C"  void EffectCtrl_SoundClear_m1995229443 (EffectCtrl_t3708787644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::Reset()
extern "C"  void EffectCtrl_Reset_m1354816268 (EffectCtrl_t3708787644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::Clear()
extern "C"  void EffectCtrl_Clear_m1114516618 (EffectCtrl_t3708787644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::ilo_set_actorBullet1(EffectCtrl,ActorBullet)
extern "C"  void EffectCtrl_ilo_set_actorBullet1_m588341800 (Il2CppObject * __this /* static, unused */, EffectCtrl_t3708787644 * ____this0, ActorBullet_t2246481719 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform EffectCtrl::ilo_get_effTr2(EffectCtrl)
extern "C"  Transform_t1659122786 * EffectCtrl_ilo_get_effTr2_m945544402 (Il2CppObject * __this /* static, unused */, EffectCtrl_t3708787644 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::ilo_Play3(IEffComponent)
extern "C"  void EffectCtrl_ilo_Play3_m721500488 (Il2CppObject * __this /* static, unused */, IEffComponent_t3802264961 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SoundMgr EffectCtrl::ilo_get_SoundMgr4()
extern "C"  SoundMgr_t1807284905 * EffectCtrl_ilo_get_SoundMgr4_m172053464 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::ilo_PlaySound5(SoundMgr,System.Int32)
extern "C"  void EffectCtrl_ilo_PlaySound5_m1452191086 (Il2CppObject * __this /* static, unused */, SoundMgr_t1807284905 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::ilo_AddEventListener6(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void EffectCtrl_ilo_AddEventListener6_m1898786640 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::ilo_SetPlaySpeed7(EffectCtrl)
extern "C"  void EffectCtrl_ilo_SetPlaySpeed7_m2159633812 (Il2CppObject * __this /* static, unused */, EffectCtrl_t3708787644 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::ilo_SoundClear8(EffectCtrl)
extern "C"  void EffectCtrl_ilo_SoundClear8_m3039678504 (Il2CppObject * __this /* static, unused */, EffectCtrl_t3708787644 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::ilo_RemoveUpdate9(IZUpdate)
extern "C"  void EffectCtrl_ilo_RemoveUpdate9_m692707900 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject EffectCtrl::ilo_get_effGO10(EffectCtrl)
extern "C"  GameObject_t3674682005 * EffectCtrl_ilo_get_effGO10_m989308796 (Il2CppObject * __this /* static, unused */, EffectCtrl_t3708787644 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::ilo_Clear11(IEffComponent)
extern "C"  void EffectCtrl_ilo_Clear11_m1387451418 (Il2CppObject * __this /* static, unused */, IEffComponent_t3802264961 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::ilo_set_effCtrl12(IEffComponent,EffectCtrl)
extern "C"  void EffectCtrl_ilo_set_effCtrl12_m1874609047 (Il2CppObject * __this /* static, unused */, IEffComponent_t3802264961 * ____this0, EffectCtrl_t3708787644 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EffectCtrl::ilo_set_timeEffect13(EffectCtrl,TimeEffect)
extern "C"  void EffectCtrl_ilo_set_timeEffect13_m1874208655 (Il2CppObject * __this /* static, unused */, EffectCtrl_t3708787644 * ____this0, TimeEffect_t2372650718 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
