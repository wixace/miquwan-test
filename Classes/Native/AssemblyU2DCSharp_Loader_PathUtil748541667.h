﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t243639308;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Loader.PathUtil
struct  PathUtil_t748541667  : public Il2CppObject
{
public:

public:
};

struct PathUtil_t748541667_StaticFields
{
public:
	// System.Text.StringBuilder Loader.PathUtil::sb
	StringBuilder_t243639308 * ___sb_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Loader.PathUtil::streamDic
	Dictionary_2_t827649927 * ___streamDic_13;
	// System.String Loader.PathUtil::PATH_BULLETIN
	String_t* ___PATH_BULLETIN_14;
	// System.String Loader.PathUtil::persistentDataPath
	String_t* ___persistentDataPath_15;
	// System.String Loader.PathUtil::SaySoundDataPath
	String_t* ___SaySoundDataPath_16;
	// System.String Loader.PathUtil::streamingDataPath
	String_t* ___streamingDataPath_17;
	// System.String Loader.PathUtil::main_bgPath
	String_t* ___main_bgPath_18;
	// System.String Loader.PathUtil::update_bgPath
	String_t* ___update_bgPath_19;
	// System.String Loader.PathUtil::splashScreenPath
	String_t* ___splashScreenPath_20;
	// System.String Loader.PathUtil::LOGIN_URL
	String_t* ___LOGIN_URL_21;
	// System.String Loader.PathUtil::HELP_URL
	String_t* ___HELP_URL_22;
	// System.String Loader.PathUtil::ALLVS_URL
	String_t* ___ALLVS_URL_23;
	// System.String Loader.PathUtil::SERVER_URL
	String_t* ___SERVER_URL_24;
	// System.String Loader.PathUtil::REVIEW_URL
	String_t* ___REVIEW_URL_25;

public:
	inline static int32_t get_offset_of_sb_12() { return static_cast<int32_t>(offsetof(PathUtil_t748541667_StaticFields, ___sb_12)); }
	inline StringBuilder_t243639308 * get_sb_12() const { return ___sb_12; }
	inline StringBuilder_t243639308 ** get_address_of_sb_12() { return &___sb_12; }
	inline void set_sb_12(StringBuilder_t243639308 * value)
	{
		___sb_12 = value;
		Il2CppCodeGenWriteBarrier(&___sb_12, value);
	}

	inline static int32_t get_offset_of_streamDic_13() { return static_cast<int32_t>(offsetof(PathUtil_t748541667_StaticFields, ___streamDic_13)); }
	inline Dictionary_2_t827649927 * get_streamDic_13() const { return ___streamDic_13; }
	inline Dictionary_2_t827649927 ** get_address_of_streamDic_13() { return &___streamDic_13; }
	inline void set_streamDic_13(Dictionary_2_t827649927 * value)
	{
		___streamDic_13 = value;
		Il2CppCodeGenWriteBarrier(&___streamDic_13, value);
	}

	inline static int32_t get_offset_of_PATH_BULLETIN_14() { return static_cast<int32_t>(offsetof(PathUtil_t748541667_StaticFields, ___PATH_BULLETIN_14)); }
	inline String_t* get_PATH_BULLETIN_14() const { return ___PATH_BULLETIN_14; }
	inline String_t** get_address_of_PATH_BULLETIN_14() { return &___PATH_BULLETIN_14; }
	inline void set_PATH_BULLETIN_14(String_t* value)
	{
		___PATH_BULLETIN_14 = value;
		Il2CppCodeGenWriteBarrier(&___PATH_BULLETIN_14, value);
	}

	inline static int32_t get_offset_of_persistentDataPath_15() { return static_cast<int32_t>(offsetof(PathUtil_t748541667_StaticFields, ___persistentDataPath_15)); }
	inline String_t* get_persistentDataPath_15() const { return ___persistentDataPath_15; }
	inline String_t** get_address_of_persistentDataPath_15() { return &___persistentDataPath_15; }
	inline void set_persistentDataPath_15(String_t* value)
	{
		___persistentDataPath_15 = value;
		Il2CppCodeGenWriteBarrier(&___persistentDataPath_15, value);
	}

	inline static int32_t get_offset_of_SaySoundDataPath_16() { return static_cast<int32_t>(offsetof(PathUtil_t748541667_StaticFields, ___SaySoundDataPath_16)); }
	inline String_t* get_SaySoundDataPath_16() const { return ___SaySoundDataPath_16; }
	inline String_t** get_address_of_SaySoundDataPath_16() { return &___SaySoundDataPath_16; }
	inline void set_SaySoundDataPath_16(String_t* value)
	{
		___SaySoundDataPath_16 = value;
		Il2CppCodeGenWriteBarrier(&___SaySoundDataPath_16, value);
	}

	inline static int32_t get_offset_of_streamingDataPath_17() { return static_cast<int32_t>(offsetof(PathUtil_t748541667_StaticFields, ___streamingDataPath_17)); }
	inline String_t* get_streamingDataPath_17() const { return ___streamingDataPath_17; }
	inline String_t** get_address_of_streamingDataPath_17() { return &___streamingDataPath_17; }
	inline void set_streamingDataPath_17(String_t* value)
	{
		___streamingDataPath_17 = value;
		Il2CppCodeGenWriteBarrier(&___streamingDataPath_17, value);
	}

	inline static int32_t get_offset_of_main_bgPath_18() { return static_cast<int32_t>(offsetof(PathUtil_t748541667_StaticFields, ___main_bgPath_18)); }
	inline String_t* get_main_bgPath_18() const { return ___main_bgPath_18; }
	inline String_t** get_address_of_main_bgPath_18() { return &___main_bgPath_18; }
	inline void set_main_bgPath_18(String_t* value)
	{
		___main_bgPath_18 = value;
		Il2CppCodeGenWriteBarrier(&___main_bgPath_18, value);
	}

	inline static int32_t get_offset_of_update_bgPath_19() { return static_cast<int32_t>(offsetof(PathUtil_t748541667_StaticFields, ___update_bgPath_19)); }
	inline String_t* get_update_bgPath_19() const { return ___update_bgPath_19; }
	inline String_t** get_address_of_update_bgPath_19() { return &___update_bgPath_19; }
	inline void set_update_bgPath_19(String_t* value)
	{
		___update_bgPath_19 = value;
		Il2CppCodeGenWriteBarrier(&___update_bgPath_19, value);
	}

	inline static int32_t get_offset_of_splashScreenPath_20() { return static_cast<int32_t>(offsetof(PathUtil_t748541667_StaticFields, ___splashScreenPath_20)); }
	inline String_t* get_splashScreenPath_20() const { return ___splashScreenPath_20; }
	inline String_t** get_address_of_splashScreenPath_20() { return &___splashScreenPath_20; }
	inline void set_splashScreenPath_20(String_t* value)
	{
		___splashScreenPath_20 = value;
		Il2CppCodeGenWriteBarrier(&___splashScreenPath_20, value);
	}

	inline static int32_t get_offset_of_LOGIN_URL_21() { return static_cast<int32_t>(offsetof(PathUtil_t748541667_StaticFields, ___LOGIN_URL_21)); }
	inline String_t* get_LOGIN_URL_21() const { return ___LOGIN_URL_21; }
	inline String_t** get_address_of_LOGIN_URL_21() { return &___LOGIN_URL_21; }
	inline void set_LOGIN_URL_21(String_t* value)
	{
		___LOGIN_URL_21 = value;
		Il2CppCodeGenWriteBarrier(&___LOGIN_URL_21, value);
	}

	inline static int32_t get_offset_of_HELP_URL_22() { return static_cast<int32_t>(offsetof(PathUtil_t748541667_StaticFields, ___HELP_URL_22)); }
	inline String_t* get_HELP_URL_22() const { return ___HELP_URL_22; }
	inline String_t** get_address_of_HELP_URL_22() { return &___HELP_URL_22; }
	inline void set_HELP_URL_22(String_t* value)
	{
		___HELP_URL_22 = value;
		Il2CppCodeGenWriteBarrier(&___HELP_URL_22, value);
	}

	inline static int32_t get_offset_of_ALLVS_URL_23() { return static_cast<int32_t>(offsetof(PathUtil_t748541667_StaticFields, ___ALLVS_URL_23)); }
	inline String_t* get_ALLVS_URL_23() const { return ___ALLVS_URL_23; }
	inline String_t** get_address_of_ALLVS_URL_23() { return &___ALLVS_URL_23; }
	inline void set_ALLVS_URL_23(String_t* value)
	{
		___ALLVS_URL_23 = value;
		Il2CppCodeGenWriteBarrier(&___ALLVS_URL_23, value);
	}

	inline static int32_t get_offset_of_SERVER_URL_24() { return static_cast<int32_t>(offsetof(PathUtil_t748541667_StaticFields, ___SERVER_URL_24)); }
	inline String_t* get_SERVER_URL_24() const { return ___SERVER_URL_24; }
	inline String_t** get_address_of_SERVER_URL_24() { return &___SERVER_URL_24; }
	inline void set_SERVER_URL_24(String_t* value)
	{
		___SERVER_URL_24 = value;
		Il2CppCodeGenWriteBarrier(&___SERVER_URL_24, value);
	}

	inline static int32_t get_offset_of_REVIEW_URL_25() { return static_cast<int32_t>(offsetof(PathUtil_t748541667_StaticFields, ___REVIEW_URL_25)); }
	inline String_t* get_REVIEW_URL_25() const { return ___REVIEW_URL_25; }
	inline String_t** get_address_of_REVIEW_URL_25() { return &___REVIEW_URL_25; }
	inline void set_REVIEW_URL_25(String_t* value)
	{
		___REVIEW_URL_25 = value;
		Il2CppCodeGenWriteBarrier(&___REVIEW_URL_25, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
