﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BiaocheGenerated
struct BiaocheGenerated_t1653926948;
// JSVCall
struct JSVCall_t3708497963;
// Biaoche
struct Biaoche_t1544748459;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// CEvent.ZEvent
struct ZEvent_t3638018500;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_Biaoche1544748459.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"

// System.Void BiaocheGenerated::.ctor()
extern "C"  void BiaocheGenerated__ctor_m987603703 (BiaocheGenerated_t1653926948 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BiaocheGenerated::Biaoche_Biaoche1(JSVCall,System.Int32)
extern "C"  bool BiaocheGenerated_Biaoche_Biaoche1_m3030421131 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BiaocheGenerated::Biaoche_MOVE_TO_END(JSVCall)
extern "C"  void BiaocheGenerated_Biaoche_MOVE_TO_END_m574737761 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BiaocheGenerated::Biaoche_MoveJindu(JSVCall)
extern "C"  void BiaocheGenerated_Biaoche_MoveJindu_m2547435351 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BiaocheGenerated::Biaoche_BattleStart__ZEvent(JSVCall,System.Int32)
extern "C"  bool BiaocheGenerated_Biaoche_BattleStart__ZEvent_m363617703 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BiaocheGenerated::Biaoche_BattleWin__ZEvent(JSVCall,System.Int32)
extern "C"  bool BiaocheGenerated_Biaoche_BattleWin__ZEvent_m4168376481 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BiaocheGenerated::Biaoche_Clear(JSVCall,System.Int32)
extern "C"  bool BiaocheGenerated_Biaoche_Clear_m1380375018 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BiaocheGenerated::Biaoche_Create__monstersCfg__JSCLevelMonsterConfig(JSVCall,System.Int32)
extern "C"  bool BiaocheGenerated_Biaoche_Create__monstersCfg__JSCLevelMonsterConfig_m981521192 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BiaocheGenerated::Biaoche_OnTriggerFun__Int32(JSVCall,System.Int32)
extern "C"  bool BiaocheGenerated_Biaoche_OnTriggerFun__Int32_m192304197 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BiaocheGenerated::Biaoche_Update(JSVCall,System.Int32)
extern "C"  bool BiaocheGenerated_Biaoche_Update_m3599924302 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BiaocheGenerated::__Register()
extern "C"  void BiaocheGenerated___Register_m2205555760 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BiaocheGenerated::ilo_get_MoveJindu1(Biaoche)
extern "C"  float BiaocheGenerated_ilo_get_MoveJindu1_m2189229200 (Il2CppObject * __this /* static, unused */, Biaoche_t1544748459 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BiaocheGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * BiaocheGenerated_ilo_getObject2_m142829337 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BiaocheGenerated::ilo_BattleStart3(Biaoche,CEvent.ZEvent)
extern "C"  void BiaocheGenerated_ilo_BattleStart3_m3904608819 (Il2CppObject * __this /* static, unused */, Biaoche_t1544748459 * ____this0, ZEvent_t3638018500 * ___ev1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BiaocheGenerated::ilo_Clear4(Biaoche)
extern "C"  void BiaocheGenerated_ilo_Clear4_m323161824 (Il2CppObject * __this /* static, unused */, Biaoche_t1544748459 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BiaocheGenerated::ilo_OnTriggerFun5(Biaoche,System.Int32)
extern "C"  void BiaocheGenerated_ilo_OnTriggerFun5_m1668520545 (Il2CppObject * __this /* static, unused */, Biaoche_t1544748459 * ____this0, int32_t ___effectId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
