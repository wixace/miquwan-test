﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.FleePath
struct FleePath_t909342113;
// OnPathDelegate
struct OnPathDelegate_t598607977;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_OnPathDelegate598607977.h"
#include "AssemblyU2DCSharp_Pathfinding_FleePath909342113.h"

// System.Void Pathfinding.FleePath::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32,OnPathDelegate)
extern "C"  void FleePath__ctor_m4244646412 (FleePath_t909342113 * __this, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___avoid1, int32_t ___length2, OnPathDelegate_t598607977 * ___callbackDelegate3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.FleePath::.ctor()
extern "C"  void FleePath__ctor_m580461030 (FleePath_t909342113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.FleePath Pathfinding.FleePath::Construct(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32,OnPathDelegate)
extern "C"  FleePath_t909342113 * FleePath_Construct_m2478141375 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___avoid1, int32_t ___searchLength2, OnPathDelegate_t598607977 * ___callback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.FleePath::Setup(UnityEngine.Vector3,UnityEngine.Vector3,System.Int32,OnPathDelegate)
extern "C"  void FleePath_Setup_m1051814865 (FleePath_t909342113 * __this, Vector3_t4282066566  ___start0, Vector3_t4282066566  ___avoid1, int32_t ___searchLength2, OnPathDelegate_t598607977 * ___callback3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.FleePath::Recycle()
extern "C"  void FleePath_Recycle_m390933527 (FleePath_t909342113 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.FleePath::ilo_Setup1(Pathfinding.FleePath,UnityEngine.Vector3,UnityEngine.Vector3,System.Int32,OnPathDelegate)
extern "C"  void FleePath_ilo_Setup1_m3664880668 (Il2CppObject * __this /* static, unused */, FleePath_t909342113 * ____this0, Vector3_t4282066566  ___start1, Vector3_t4282066566  ___avoid2, int32_t ___searchLength3, OnPathDelegate_t598607977 * ___callback4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
