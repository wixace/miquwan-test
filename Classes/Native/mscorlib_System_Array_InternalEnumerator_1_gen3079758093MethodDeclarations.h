﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2953159047MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<T4MBillBObjSC>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m2903186878(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3079758093 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2616641763_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<T4MBillBObjSC>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3569947426(__this, method) ((  void (*) (InternalEnumerator_1_t3079758093 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224260061_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<T4MBillBObjSC>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2296831320(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3079758093 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m390763987_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<T4MBillBObjSC>::Dispose()
#define InternalEnumerator_1_Dispose_m77667605(__this, method) ((  void (*) (InternalEnumerator_1_t3079758093 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2760671866_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<T4MBillBObjSC>::MoveNext()
#define InternalEnumerator_1_MoveNext_m2147794770(__this, method) ((  bool (*) (InternalEnumerator_1_t3079758093 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3716548237_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<T4MBillBObjSC>::get_Current()
#define InternalEnumerator_1_get_Current_m983464743(__this, method) ((  T4MBillBObjSC_t2448121 * (*) (InternalEnumerator_1_t3079758093 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2178852364_gshared)(__this, method)
