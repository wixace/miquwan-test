﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TBPan/PanEventHandler
struct PanEventHandler_t1012212141;
// System.Object
struct Il2CppObject;
// TBPan
struct TBPan_t79621967;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_TBPan79621967.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void TBPan/PanEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void PanEventHandler__ctor_m2940675844 (PanEventHandler_t1012212141 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPan/PanEventHandler::Invoke(TBPan,UnityEngine.Vector3)
extern "C"  void PanEventHandler_Invoke_m1855808696 (PanEventHandler_t1012212141 * __this, TBPan_t79621967 * ___source0, Vector3_t4282066566  ___move1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult TBPan/PanEventHandler::BeginInvoke(TBPan,UnityEngine.Vector3,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * PanEventHandler_BeginInvoke_m3879500573 (PanEventHandler_t1012212141 * __this, TBPan_t79621967 * ___source0, Vector3_t4282066566  ___move1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBPan/PanEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void PanEventHandler_EndInvoke_m4163812116 (PanEventHandler_t1012212141 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
