﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t3870600107;
// System.Collections.Generic.List`1<FS_ShadowSimple>
struct List_1_t1281967124;
// UnityEngine.Mesh
struct Mesh_t4241756145;
// UnityEngine.MeshFilter
struct MeshFilter_t3839065225;
// UnityEngine.Renderer
struct Renderer_t3076687687;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FS_ShadowManagerMesh
struct  FS_ShadowManagerMesh_t1809690344  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Material FS_ShadowManagerMesh::shadowMaterial
	Material_t3870600107 * ___shadowMaterial_2;
	// System.Boolean FS_ShadowManagerMesh::isStatic
	bool ___isStatic_3;
	// System.Int32 FS_ShadowManagerMesh::numShadows
	int32_t ___numShadows_4;
	// System.Collections.Generic.List`1<FS_ShadowSimple> FS_ShadowManagerMesh::shadows
	List_1_t1281967124 * ___shadows_5;
	// UnityEngine.Mesh FS_ShadowManagerMesh::_mesh
	Mesh_t4241756145 * ____mesh_6;
	// UnityEngine.Mesh FS_ShadowManagerMesh::_mesh1
	Mesh_t4241756145 * ____mesh1_7;
	// UnityEngine.Mesh FS_ShadowManagerMesh::_mesh2
	Mesh_t4241756145 * ____mesh2_8;
	// System.Boolean FS_ShadowManagerMesh::pingPong
	bool ___pingPong_9;
	// UnityEngine.MeshFilter FS_ShadowManagerMesh::_filter
	MeshFilter_t3839065225 * ____filter_10;
	// UnityEngine.Renderer FS_ShadowManagerMesh::_ren
	Renderer_t3076687687 * ____ren_11;
	// UnityEngine.Vector3[] FS_ShadowManagerMesh::_verts
	Vector3U5BU5D_t215400611* ____verts_12;
	// UnityEngine.Vector2[] FS_ShadowManagerMesh::_uvs
	Vector2U5BU5D_t4024180168* ____uvs_13;
	// UnityEngine.Vector3[] FS_ShadowManagerMesh::_norms
	Vector3U5BU5D_t215400611* ____norms_14;
	// UnityEngine.Color[] FS_ShadowManagerMesh::_colors
	ColorU5BU5D_t2441545636* ____colors_15;
	// System.Int32[] FS_ShadowManagerMesh::_indices
	Int32U5BU5D_t3230847821* ____indices_16;
	// System.Int32 FS_ShadowManagerMesh::blockGrowSize
	int32_t ___blockGrowSize_17;

public:
	inline static int32_t get_offset_of_shadowMaterial_2() { return static_cast<int32_t>(offsetof(FS_ShadowManagerMesh_t1809690344, ___shadowMaterial_2)); }
	inline Material_t3870600107 * get_shadowMaterial_2() const { return ___shadowMaterial_2; }
	inline Material_t3870600107 ** get_address_of_shadowMaterial_2() { return &___shadowMaterial_2; }
	inline void set_shadowMaterial_2(Material_t3870600107 * value)
	{
		___shadowMaterial_2 = value;
		Il2CppCodeGenWriteBarrier(&___shadowMaterial_2, value);
	}

	inline static int32_t get_offset_of_isStatic_3() { return static_cast<int32_t>(offsetof(FS_ShadowManagerMesh_t1809690344, ___isStatic_3)); }
	inline bool get_isStatic_3() const { return ___isStatic_3; }
	inline bool* get_address_of_isStatic_3() { return &___isStatic_3; }
	inline void set_isStatic_3(bool value)
	{
		___isStatic_3 = value;
	}

	inline static int32_t get_offset_of_numShadows_4() { return static_cast<int32_t>(offsetof(FS_ShadowManagerMesh_t1809690344, ___numShadows_4)); }
	inline int32_t get_numShadows_4() const { return ___numShadows_4; }
	inline int32_t* get_address_of_numShadows_4() { return &___numShadows_4; }
	inline void set_numShadows_4(int32_t value)
	{
		___numShadows_4 = value;
	}

	inline static int32_t get_offset_of_shadows_5() { return static_cast<int32_t>(offsetof(FS_ShadowManagerMesh_t1809690344, ___shadows_5)); }
	inline List_1_t1281967124 * get_shadows_5() const { return ___shadows_5; }
	inline List_1_t1281967124 ** get_address_of_shadows_5() { return &___shadows_5; }
	inline void set_shadows_5(List_1_t1281967124 * value)
	{
		___shadows_5 = value;
		Il2CppCodeGenWriteBarrier(&___shadows_5, value);
	}

	inline static int32_t get_offset_of__mesh_6() { return static_cast<int32_t>(offsetof(FS_ShadowManagerMesh_t1809690344, ____mesh_6)); }
	inline Mesh_t4241756145 * get__mesh_6() const { return ____mesh_6; }
	inline Mesh_t4241756145 ** get_address_of__mesh_6() { return &____mesh_6; }
	inline void set__mesh_6(Mesh_t4241756145 * value)
	{
		____mesh_6 = value;
		Il2CppCodeGenWriteBarrier(&____mesh_6, value);
	}

	inline static int32_t get_offset_of__mesh1_7() { return static_cast<int32_t>(offsetof(FS_ShadowManagerMesh_t1809690344, ____mesh1_7)); }
	inline Mesh_t4241756145 * get__mesh1_7() const { return ____mesh1_7; }
	inline Mesh_t4241756145 ** get_address_of__mesh1_7() { return &____mesh1_7; }
	inline void set__mesh1_7(Mesh_t4241756145 * value)
	{
		____mesh1_7 = value;
		Il2CppCodeGenWriteBarrier(&____mesh1_7, value);
	}

	inline static int32_t get_offset_of__mesh2_8() { return static_cast<int32_t>(offsetof(FS_ShadowManagerMesh_t1809690344, ____mesh2_8)); }
	inline Mesh_t4241756145 * get__mesh2_8() const { return ____mesh2_8; }
	inline Mesh_t4241756145 ** get_address_of__mesh2_8() { return &____mesh2_8; }
	inline void set__mesh2_8(Mesh_t4241756145 * value)
	{
		____mesh2_8 = value;
		Il2CppCodeGenWriteBarrier(&____mesh2_8, value);
	}

	inline static int32_t get_offset_of_pingPong_9() { return static_cast<int32_t>(offsetof(FS_ShadowManagerMesh_t1809690344, ___pingPong_9)); }
	inline bool get_pingPong_9() const { return ___pingPong_9; }
	inline bool* get_address_of_pingPong_9() { return &___pingPong_9; }
	inline void set_pingPong_9(bool value)
	{
		___pingPong_9 = value;
	}

	inline static int32_t get_offset_of__filter_10() { return static_cast<int32_t>(offsetof(FS_ShadowManagerMesh_t1809690344, ____filter_10)); }
	inline MeshFilter_t3839065225 * get__filter_10() const { return ____filter_10; }
	inline MeshFilter_t3839065225 ** get_address_of__filter_10() { return &____filter_10; }
	inline void set__filter_10(MeshFilter_t3839065225 * value)
	{
		____filter_10 = value;
		Il2CppCodeGenWriteBarrier(&____filter_10, value);
	}

	inline static int32_t get_offset_of__ren_11() { return static_cast<int32_t>(offsetof(FS_ShadowManagerMesh_t1809690344, ____ren_11)); }
	inline Renderer_t3076687687 * get__ren_11() const { return ____ren_11; }
	inline Renderer_t3076687687 ** get_address_of__ren_11() { return &____ren_11; }
	inline void set__ren_11(Renderer_t3076687687 * value)
	{
		____ren_11 = value;
		Il2CppCodeGenWriteBarrier(&____ren_11, value);
	}

	inline static int32_t get_offset_of__verts_12() { return static_cast<int32_t>(offsetof(FS_ShadowManagerMesh_t1809690344, ____verts_12)); }
	inline Vector3U5BU5D_t215400611* get__verts_12() const { return ____verts_12; }
	inline Vector3U5BU5D_t215400611** get_address_of__verts_12() { return &____verts_12; }
	inline void set__verts_12(Vector3U5BU5D_t215400611* value)
	{
		____verts_12 = value;
		Il2CppCodeGenWriteBarrier(&____verts_12, value);
	}

	inline static int32_t get_offset_of__uvs_13() { return static_cast<int32_t>(offsetof(FS_ShadowManagerMesh_t1809690344, ____uvs_13)); }
	inline Vector2U5BU5D_t4024180168* get__uvs_13() const { return ____uvs_13; }
	inline Vector2U5BU5D_t4024180168** get_address_of__uvs_13() { return &____uvs_13; }
	inline void set__uvs_13(Vector2U5BU5D_t4024180168* value)
	{
		____uvs_13 = value;
		Il2CppCodeGenWriteBarrier(&____uvs_13, value);
	}

	inline static int32_t get_offset_of__norms_14() { return static_cast<int32_t>(offsetof(FS_ShadowManagerMesh_t1809690344, ____norms_14)); }
	inline Vector3U5BU5D_t215400611* get__norms_14() const { return ____norms_14; }
	inline Vector3U5BU5D_t215400611** get_address_of__norms_14() { return &____norms_14; }
	inline void set__norms_14(Vector3U5BU5D_t215400611* value)
	{
		____norms_14 = value;
		Il2CppCodeGenWriteBarrier(&____norms_14, value);
	}

	inline static int32_t get_offset_of__colors_15() { return static_cast<int32_t>(offsetof(FS_ShadowManagerMesh_t1809690344, ____colors_15)); }
	inline ColorU5BU5D_t2441545636* get__colors_15() const { return ____colors_15; }
	inline ColorU5BU5D_t2441545636** get_address_of__colors_15() { return &____colors_15; }
	inline void set__colors_15(ColorU5BU5D_t2441545636* value)
	{
		____colors_15 = value;
		Il2CppCodeGenWriteBarrier(&____colors_15, value);
	}

	inline static int32_t get_offset_of__indices_16() { return static_cast<int32_t>(offsetof(FS_ShadowManagerMesh_t1809690344, ____indices_16)); }
	inline Int32U5BU5D_t3230847821* get__indices_16() const { return ____indices_16; }
	inline Int32U5BU5D_t3230847821** get_address_of__indices_16() { return &____indices_16; }
	inline void set__indices_16(Int32U5BU5D_t3230847821* value)
	{
		____indices_16 = value;
		Il2CppCodeGenWriteBarrier(&____indices_16, value);
	}

	inline static int32_t get_offset_of_blockGrowSize_17() { return static_cast<int32_t>(offsetof(FS_ShadowManagerMesh_t1809690344, ___blockGrowSize_17)); }
	inline int32_t get_blockGrowSize_17() const { return ___blockGrowSize_17; }
	inline int32_t* get_address_of_blockGrowSize_17() { return &___blockGrowSize_17; }
	inline void set_blockGrowSize_17(int32_t value)
	{
		___blockGrowSize_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
