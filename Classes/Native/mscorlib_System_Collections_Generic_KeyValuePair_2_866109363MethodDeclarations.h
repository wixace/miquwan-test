﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_866109363.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3790321200_gshared (KeyValuePair_2_t866109363 * __this, Int2_t1974045593  ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3790321200(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t866109363 *, Int2_t1974045593 , Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m3790321200_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>::get_Key()
extern "C"  Int2_t1974045593  KeyValuePair_2_get_Key_m4013756344_gshared (KeyValuePair_2_t866109363 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m4013756344(__this, method) ((  Int2_t1974045593  (*) (KeyValuePair_2_t866109363 *, const MethodInfo*))KeyValuePair_2_get_Key_m4013756344_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m93750777_gshared (KeyValuePair_2_t866109363 * __this, Int2_t1974045593  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m93750777(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t866109363 *, Int2_t1974045593 , const MethodInfo*))KeyValuePair_2_set_Key_m93750777_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m167978232_gshared (KeyValuePair_2_t866109363 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m167978232(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t866109363 *, const MethodInfo*))KeyValuePair_2_get_Value_m167978232_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2665231737_gshared (KeyValuePair_2_t866109363 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m2665231737(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t866109363 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m2665231737_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3662222985_gshared (KeyValuePair_2_t866109363 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m3662222985(__this, method) ((  String_t* (*) (KeyValuePair_2_t866109363 *, const MethodInfo*))KeyValuePair_2_ToString_m3662222985_gshared)(__this, method)
