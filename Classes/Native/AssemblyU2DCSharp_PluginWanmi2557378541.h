﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginWanmi
struct  PluginWanmi_t2557378541  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginWanmi::userid
	String_t* ___userid_2;
	// System.String PluginWanmi::nickname
	String_t* ___nickname_3;
	// System.String PluginWanmi::usertoken
	String_t* ___usertoken_4;

public:
	inline static int32_t get_offset_of_userid_2() { return static_cast<int32_t>(offsetof(PluginWanmi_t2557378541, ___userid_2)); }
	inline String_t* get_userid_2() const { return ___userid_2; }
	inline String_t** get_address_of_userid_2() { return &___userid_2; }
	inline void set_userid_2(String_t* value)
	{
		___userid_2 = value;
		Il2CppCodeGenWriteBarrier(&___userid_2, value);
	}

	inline static int32_t get_offset_of_nickname_3() { return static_cast<int32_t>(offsetof(PluginWanmi_t2557378541, ___nickname_3)); }
	inline String_t* get_nickname_3() const { return ___nickname_3; }
	inline String_t** get_address_of_nickname_3() { return &___nickname_3; }
	inline void set_nickname_3(String_t* value)
	{
		___nickname_3 = value;
		Il2CppCodeGenWriteBarrier(&___nickname_3, value);
	}

	inline static int32_t get_offset_of_usertoken_4() { return static_cast<int32_t>(offsetof(PluginWanmi_t2557378541, ___usertoken_4)); }
	inline String_t* get_usertoken_4() const { return ___usertoken_4; }
	inline String_t** get_address_of_usertoken_4() { return &___usertoken_4; }
	inline void set_usertoken_4(String_t* value)
	{
		___usertoken_4 = value;
		Il2CppCodeGenWriteBarrier(&___usertoken_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
