﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Bson.BsonProperty
struct BsonProperty_t4293821333;
// Newtonsoft.Json.Bson.BsonString
struct BsonString_t2875117585;
// Newtonsoft.Json.Bson.BsonToken
struct BsonToken_t455725415;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonString2875117585.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Bson_BsonToken455725415.h"

// System.Void Newtonsoft.Json.Bson.BsonProperty::.ctor()
extern "C"  void BsonProperty__ctor_m1282405085 (BsonProperty_t4293821333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonString Newtonsoft.Json.Bson.BsonProperty::get_Name()
extern "C"  BsonString_t2875117585 * BsonProperty_get_Name_m3716459698 (BsonProperty_t4293821333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonProperty::set_Name(Newtonsoft.Json.Bson.BsonString)
extern "C"  void BsonProperty_set_Name_m667096057 (BsonProperty_t4293821333 * __this, BsonString_t2875117585 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Bson.BsonToken Newtonsoft.Json.Bson.BsonProperty::get_Value()
extern "C"  BsonToken_t455725415 * BsonProperty_get_Value_m4153432160 (BsonProperty_t4293821333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Bson.BsonProperty::set_Value(Newtonsoft.Json.Bson.BsonToken)
extern "C"  void BsonProperty_set_Value_m4255916719 (BsonProperty_t4293821333 * __this, BsonToken_t455725415 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
