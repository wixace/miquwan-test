﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen2974409999MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<Pathfinding.FloodPathTracer>::.ctor()
#define Stack_1__ctor_m240652544(__this, method) ((  void (*) (Stack_1_t472535918 *, const MethodInfo*))Stack_1__ctor_m2725689112_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Pathfinding.FloodPathTracer>::.ctor(System.Int32)
#define Stack_1__ctor_m341067345(__this, ___count0, method) ((  void (*) (Stack_1_t472535918 *, int32_t, const MethodInfo*))Stack_1__ctor_m3186788457_gshared)(__this, ___count0, method)
// System.Boolean System.Collections.Generic.Stack`1<Pathfinding.FloodPathTracer>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m55691386(__this, method) ((  bool (*) (Stack_1_t472535918 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m1582336274_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<Pathfinding.FloodPathTracer>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m2635590104(__this, method) ((  Il2CppObject * (*) (Stack_1_t472535918 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m2938343088_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Pathfinding.FloodPathTracer>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m780020804(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t472535918 *, Il2CppArray *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m3277353260_gshared)(__this, ___dest0, ___idx1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<Pathfinding.FloodPathTracer>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2411425546(__this, method) ((  Il2CppObject* (*) (Stack_1_t472535918 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m625377314_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<Pathfinding.FloodPathTracer>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m3746511423(__this, method) ((  Il2CppObject * (*) (Stack_1_t472535918 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m4095051687_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Pathfinding.FloodPathTracer>::Clear()
#define Stack_1_Clear_m1941753131(__this, method) ((  void (*) (Stack_1_t472535918 *, const MethodInfo*))Stack_1_Clear_m131822403_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<Pathfinding.FloodPathTracer>::Contains(T)
#define Stack_1_Contains_m1075717169(__this, ___t0, method) ((  bool (*) (Stack_1_t472535918 *, FloodPathTracer_t1668942290 *, const MethodInfo*))Stack_1_Contains_m328948937_gshared)(__this, ___t0, method)
// T System.Collections.Generic.Stack`1<Pathfinding.FloodPathTracer>::Peek()
#define Stack_1_Peek_m692215744(__this, method) ((  FloodPathTracer_t1668942290 * (*) (Stack_1_t472535918 *, const MethodInfo*))Stack_1_Peek_m3418768488_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<Pathfinding.FloodPathTracer>::Pop()
#define Stack_1_Pop_m2377940590(__this, method) ((  FloodPathTracer_t1668942290 * (*) (Stack_1_t472535918 *, const MethodInfo*))Stack_1_Pop_m4267009222_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Pathfinding.FloodPathTracer>::Push(T)
#define Stack_1_Push_m865129536(__this, ___t0, method) ((  void (*) (Stack_1_t472535918 *, FloodPathTracer_t1668942290 *, const MethodInfo*))Stack_1_Push_m3350166104_gshared)(__this, ___t0, method)
// System.Int32 System.Collections.Generic.Stack`1<Pathfinding.FloodPathTracer>::get_Count()
#define Stack_1_get_Count_m1763666228(__this, method) ((  int32_t (*) (Stack_1_t472535918 *, const MethodInfo*))Stack_1_get_Count_m3631765324_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<Pathfinding.FloodPathTracer>::GetEnumerator()
#define Stack_1_GetEnumerator_m3334386170(__this, method) ((  Enumerator_t30321944  (*) (Stack_1_t472535918 *, const MethodInfo*))Stack_1_GetEnumerator_m202302354_gshared)(__this, method)
