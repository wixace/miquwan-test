﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AnimationCurveGenerated
struct UnityEngine_AnimationCurveGenerated_t1891870620;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t3589549831;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_AnimationCurveGenerated::.ctor()
extern "C"  void UnityEngine_AnimationCurveGenerated__ctor_m132410191 (UnityEngine_AnimationCurveGenerated_t1891870620 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationCurveGenerated::AnimationCurve_AnimationCurve1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationCurveGenerated_AnimationCurve_AnimationCurve1_m3354169507 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationCurveGenerated::AnimationCurve_AnimationCurve2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationCurveGenerated_AnimationCurve_AnimationCurve2_m303966692 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationCurveGenerated::AnimationCurve_keys(JSVCall)
extern "C"  void UnityEngine_AnimationCurveGenerated_AnimationCurve_keys_m3021184178 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationCurveGenerated::AnimationCurve_Item_Int32(JSVCall)
extern "C"  void UnityEngine_AnimationCurveGenerated_AnimationCurve_Item_Int32_m883924996 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationCurveGenerated::AnimationCurve_length(JSVCall)
extern "C"  void UnityEngine_AnimationCurveGenerated_AnimationCurve_length_m55695360 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationCurveGenerated::AnimationCurve_preWrapMode(JSVCall)
extern "C"  void UnityEngine_AnimationCurveGenerated_AnimationCurve_preWrapMode_m4177734678 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationCurveGenerated::AnimationCurve_postWrapMode(JSVCall)
extern "C"  void UnityEngine_AnimationCurveGenerated_AnimationCurve_postWrapMode_m2054441209 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationCurveGenerated::AnimationCurve_AddKey__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationCurveGenerated_AnimationCurve_AddKey__Single__Single_m2696507731 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationCurveGenerated::AnimationCurve_AddKey__Keyframe(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationCurveGenerated_AnimationCurve_AddKey__Keyframe_m3538231857 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationCurveGenerated::AnimationCurve_Evaluate__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationCurveGenerated_AnimationCurve_Evaluate__Single_m1721838950 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationCurveGenerated::AnimationCurve_MoveKey__Int32__Keyframe(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationCurveGenerated_AnimationCurve_MoveKey__Int32__Keyframe_m2031920691 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationCurveGenerated::AnimationCurve_RemoveKey__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationCurveGenerated_AnimationCurve_RemoveKey__Int32_m425767672 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationCurveGenerated::AnimationCurve_SmoothTangents__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationCurveGenerated_AnimationCurve_SmoothTangents__Int32__Single_m1926066583 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationCurveGenerated::AnimationCurve_EaseInOut__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationCurveGenerated_AnimationCurve_EaseInOut__Single__Single__Single__Single_m839402456 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationCurveGenerated::AnimationCurve_Linear__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationCurveGenerated_AnimationCurve_Linear__Single__Single__Single__Single_m3446878154 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationCurveGenerated::__Register()
extern "C"  void UnityEngine_AnimationCurveGenerated___Register_m3382102744 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Keyframe[] UnityEngine_AnimationCurveGenerated::<AnimationCurve_AnimationCurve1>m__17A()
extern "C"  KeyframeU5BU5D_t3589549831* UnityEngine_AnimationCurveGenerated_U3CAnimationCurve_AnimationCurve1U3Em__17A_m1871661373 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Keyframe[] UnityEngine_AnimationCurveGenerated::<AnimationCurve_keys>m__17B()
extern "C"  KeyframeU5BU5D_t3589549831* UnityEngine_AnimationCurveGenerated_U3CAnimationCurve_keysU3Em__17B_m348844740 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimationCurveGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_AnimationCurveGenerated_ilo_getObject1_m1020854599 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationCurveGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_AnimationCurveGenerated_ilo_attachFinalizerObject2_m895085769 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimationCurveGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_AnimationCurveGenerated_ilo_setObject3_m1835797825 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationCurveGenerated::ilo_setEnum4(System.Int32,System.Int32)
extern "C"  void UnityEngine_AnimationCurveGenerated_ilo_setEnum4_m367301389 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationCurveGenerated::ilo_setInt325(System.Int32,System.Int32)
extern "C"  void UnityEngine_AnimationCurveGenerated_ilo_setInt325_m2807442403 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_AnimationCurveGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_AnimationCurveGenerated_ilo_getObject6_m3159884859 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationCurveGenerated::ilo_setSingle7(System.Int32,System.Single)
extern "C"  void UnityEngine_AnimationCurveGenerated_ilo_setSingle7_m1379175883 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_AnimationCurveGenerated::ilo_getSingle8(System.Int32)
extern "C"  float UnityEngine_AnimationCurveGenerated_ilo_getSingle8_m94765543 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimationCurveGenerated::ilo_getArrayLength9(System.Int32)
extern "C"  int32_t UnityEngine_AnimationCurveGenerated_ilo_getArrayLength9_m3538251657 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimationCurveGenerated::ilo_getElement10(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_AnimationCurveGenerated_ilo_getElement10_m3324712135 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
