﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.Serialization.JsonPropertyCollection
struct JsonPropertyCollection_t717767559;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t4136801618;

#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js1328848902.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_MemberSerializat1550301796.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JObjectContract
struct  JObjectContract_t2867848225  : public JsonContract_t1328848902
{
public:
	// Newtonsoft.Json.MemberSerialization Newtonsoft.Json.Serialization.JObjectContract::<MemberSerialization>k__BackingField
	int32_t ___U3CMemberSerializationU3Ek__BackingField_12;
	// Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JObjectContract::<Properties>k__BackingField
	JsonPropertyCollection_t717767559 * ___U3CPropertiesU3Ek__BackingField_13;
	// Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JObjectContract::<ConstructorParameters>k__BackingField
	JsonPropertyCollection_t717767559 * ___U3CConstructorParametersU3Ek__BackingField_14;
	// System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.JObjectContract::<OverrideConstructor>k__BackingField
	ConstructorInfo_t4136801618 * ___U3COverrideConstructorU3Ek__BackingField_15;
	// System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.JObjectContract::<ParametrizedConstructor>k__BackingField
	ConstructorInfo_t4136801618 * ___U3CParametrizedConstructorU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_U3CMemberSerializationU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(JObjectContract_t2867848225, ___U3CMemberSerializationU3Ek__BackingField_12)); }
	inline int32_t get_U3CMemberSerializationU3Ek__BackingField_12() const { return ___U3CMemberSerializationU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CMemberSerializationU3Ek__BackingField_12() { return &___U3CMemberSerializationU3Ek__BackingField_12; }
	inline void set_U3CMemberSerializationU3Ek__BackingField_12(int32_t value)
	{
		___U3CMemberSerializationU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CPropertiesU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JObjectContract_t2867848225, ___U3CPropertiesU3Ek__BackingField_13)); }
	inline JsonPropertyCollection_t717767559 * get_U3CPropertiesU3Ek__BackingField_13() const { return ___U3CPropertiesU3Ek__BackingField_13; }
	inline JsonPropertyCollection_t717767559 ** get_address_of_U3CPropertiesU3Ek__BackingField_13() { return &___U3CPropertiesU3Ek__BackingField_13; }
	inline void set_U3CPropertiesU3Ek__BackingField_13(JsonPropertyCollection_t717767559 * value)
	{
		___U3CPropertiesU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPropertiesU3Ek__BackingField_13, value);
	}

	inline static int32_t get_offset_of_U3CConstructorParametersU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JObjectContract_t2867848225, ___U3CConstructorParametersU3Ek__BackingField_14)); }
	inline JsonPropertyCollection_t717767559 * get_U3CConstructorParametersU3Ek__BackingField_14() const { return ___U3CConstructorParametersU3Ek__BackingField_14; }
	inline JsonPropertyCollection_t717767559 ** get_address_of_U3CConstructorParametersU3Ek__BackingField_14() { return &___U3CConstructorParametersU3Ek__BackingField_14; }
	inline void set_U3CConstructorParametersU3Ek__BackingField_14(JsonPropertyCollection_t717767559 * value)
	{
		___U3CConstructorParametersU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier(&___U3CConstructorParametersU3Ek__BackingField_14, value);
	}

	inline static int32_t get_offset_of_U3COverrideConstructorU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(JObjectContract_t2867848225, ___U3COverrideConstructorU3Ek__BackingField_15)); }
	inline ConstructorInfo_t4136801618 * get_U3COverrideConstructorU3Ek__BackingField_15() const { return ___U3COverrideConstructorU3Ek__BackingField_15; }
	inline ConstructorInfo_t4136801618 ** get_address_of_U3COverrideConstructorU3Ek__BackingField_15() { return &___U3COverrideConstructorU3Ek__BackingField_15; }
	inline void set_U3COverrideConstructorU3Ek__BackingField_15(ConstructorInfo_t4136801618 * value)
	{
		___U3COverrideConstructorU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier(&___U3COverrideConstructorU3Ek__BackingField_15, value);
	}

	inline static int32_t get_offset_of_U3CParametrizedConstructorU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(JObjectContract_t2867848225, ___U3CParametrizedConstructorU3Ek__BackingField_16)); }
	inline ConstructorInfo_t4136801618 * get_U3CParametrizedConstructorU3Ek__BackingField_16() const { return ___U3CParametrizedConstructorU3Ek__BackingField_16; }
	inline ConstructorInfo_t4136801618 ** get_address_of_U3CParametrizedConstructorU3Ek__BackingField_16() { return &___U3CParametrizedConstructorU3Ek__BackingField_16; }
	inline void set_U3CParametrizedConstructorU3Ek__BackingField_16(ConstructorInfo_t4136801618 * value)
	{
		___U3CParametrizedConstructorU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier(&___U3CParametrizedConstructorU3Ek__BackingField_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
