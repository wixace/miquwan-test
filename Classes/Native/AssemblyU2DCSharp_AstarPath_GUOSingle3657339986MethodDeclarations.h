﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AstarPath/GUOSingle
struct GUOSingle_t3657339986;
struct GUOSingle_t3657339986_marshaled_pinvoke;
struct GUOSingle_t3657339986_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct GUOSingle_t3657339986;
struct GUOSingle_t3657339986_marshaled_pinvoke;

extern "C" void GUOSingle_t3657339986_marshal_pinvoke(const GUOSingle_t3657339986& unmarshaled, GUOSingle_t3657339986_marshaled_pinvoke& marshaled);
extern "C" void GUOSingle_t3657339986_marshal_pinvoke_back(const GUOSingle_t3657339986_marshaled_pinvoke& marshaled, GUOSingle_t3657339986& unmarshaled);
extern "C" void GUOSingle_t3657339986_marshal_pinvoke_cleanup(GUOSingle_t3657339986_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct GUOSingle_t3657339986;
struct GUOSingle_t3657339986_marshaled_com;

extern "C" void GUOSingle_t3657339986_marshal_com(const GUOSingle_t3657339986& unmarshaled, GUOSingle_t3657339986_marshaled_com& marshaled);
extern "C" void GUOSingle_t3657339986_marshal_com_back(const GUOSingle_t3657339986_marshaled_com& marshaled, GUOSingle_t3657339986& unmarshaled);
extern "C" void GUOSingle_t3657339986_marshal_com_cleanup(GUOSingle_t3657339986_marshaled_com& marshaled);
