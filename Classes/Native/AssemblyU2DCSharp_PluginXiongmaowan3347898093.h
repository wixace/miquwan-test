﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginXiongmaowan
struct  PluginXiongmaowan_t3347898093  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginXiongmaowan::userName
	String_t* ___userName_2;
	// System.String PluginXiongmaowan::sessionId
	String_t* ___sessionId_3;
	// System.String[] PluginXiongmaowan::sdkInfos
	StringU5BU5D_t4054002952* ___sdkInfos_4;
	// System.String PluginXiongmaowan::configId
	String_t* ___configId_5;
	// System.Boolean PluginXiongmaowan::isOut
	bool ___isOut_6;

public:
	inline static int32_t get_offset_of_userName_2() { return static_cast<int32_t>(offsetof(PluginXiongmaowan_t3347898093, ___userName_2)); }
	inline String_t* get_userName_2() const { return ___userName_2; }
	inline String_t** get_address_of_userName_2() { return &___userName_2; }
	inline void set_userName_2(String_t* value)
	{
		___userName_2 = value;
		Il2CppCodeGenWriteBarrier(&___userName_2, value);
	}

	inline static int32_t get_offset_of_sessionId_3() { return static_cast<int32_t>(offsetof(PluginXiongmaowan_t3347898093, ___sessionId_3)); }
	inline String_t* get_sessionId_3() const { return ___sessionId_3; }
	inline String_t** get_address_of_sessionId_3() { return &___sessionId_3; }
	inline void set_sessionId_3(String_t* value)
	{
		___sessionId_3 = value;
		Il2CppCodeGenWriteBarrier(&___sessionId_3, value);
	}

	inline static int32_t get_offset_of_sdkInfos_4() { return static_cast<int32_t>(offsetof(PluginXiongmaowan_t3347898093, ___sdkInfos_4)); }
	inline StringU5BU5D_t4054002952* get_sdkInfos_4() const { return ___sdkInfos_4; }
	inline StringU5BU5D_t4054002952** get_address_of_sdkInfos_4() { return &___sdkInfos_4; }
	inline void set_sdkInfos_4(StringU5BU5D_t4054002952* value)
	{
		___sdkInfos_4 = value;
		Il2CppCodeGenWriteBarrier(&___sdkInfos_4, value);
	}

	inline static int32_t get_offset_of_configId_5() { return static_cast<int32_t>(offsetof(PluginXiongmaowan_t3347898093, ___configId_5)); }
	inline String_t* get_configId_5() const { return ___configId_5; }
	inline String_t** get_address_of_configId_5() { return &___configId_5; }
	inline void set_configId_5(String_t* value)
	{
		___configId_5 = value;
		Il2CppCodeGenWriteBarrier(&___configId_5, value);
	}

	inline static int32_t get_offset_of_isOut_6() { return static_cast<int32_t>(offsetof(PluginXiongmaowan_t3347898093, ___isOut_6)); }
	inline bool get_isOut_6() const { return ___isOut_6; }
	inline bool* get_address_of_isOut_6() { return &___isOut_6; }
	inline void set_isOut_6(bool value)
	{
		___isOut_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
