﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSharpGenerated
struct CSharpGenerated_t1164266472;

#include "codegen/il2cpp-codegen.h"

// System.Void CSharpGenerated::.ctor()
extern "C"  void CSharpGenerated__ctor_m3038077059 (CSharpGenerated_t1164266472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSharpGenerated::Register()
extern "C"  bool CSharpGenerated_Register_m1026921232 (CSharpGenerated_t1164266472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSharpGenerated::RegisterReal()
extern "C"  void CSharpGenerated_RegisterReal_m582946562 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSharpGenerated::RegisterAll()
extern "C"  bool CSharpGenerated_RegisterAll_m4278529491 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSharpGenerated::ilo_RegisterReal1()
extern "C"  void CSharpGenerated_ilo_RegisterReal1_m183937150 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
