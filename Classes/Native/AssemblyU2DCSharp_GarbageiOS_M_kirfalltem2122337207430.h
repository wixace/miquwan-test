﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_kirfalltem212
struct  M_kirfalltem212_t2337207430  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_kirfalltem212::_detrallnur
	bool ____detrallnur_0;
	// System.String GarbageiOS.M_kirfalltem212::_hobis
	String_t* ____hobis_1;
	// System.String GarbageiOS.M_kirfalltem212::_sootokowHuhairwe
	String_t* ____sootokowHuhairwe_2;
	// System.Boolean GarbageiOS.M_kirfalltem212::_nerviDime
	bool ____nerviDime_3;
	// System.String GarbageiOS.M_kirfalltem212::_hube
	String_t* ____hube_4;
	// System.UInt32 GarbageiOS.M_kirfalltem212::_tairdourayNeretas
	uint32_t ____tairdourayNeretas_5;
	// System.Single GarbageiOS.M_kirfalltem212::_bewur
	float ____bewur_6;
	// System.UInt32 GarbageiOS.M_kirfalltem212::_beetirRerna
	uint32_t ____beetirRerna_7;
	// System.UInt32 GarbageiOS.M_kirfalltem212::_wayzerBemta
	uint32_t ____wayzerBemta_8;
	// System.Single GarbageiOS.M_kirfalltem212::_waydee
	float ____waydee_9;
	// System.Single GarbageiOS.M_kirfalltem212::_pispeeqaw
	float ____pispeeqaw_10;
	// System.String GarbageiOS.M_kirfalltem212::_lallsalnoo
	String_t* ____lallsalnoo_11;
	// System.Boolean GarbageiOS.M_kirfalltem212::_joraigay
	bool ____joraigay_12;
	// System.Single GarbageiOS.M_kirfalltem212::_jasbiseHejoni
	float ____jasbiseHejoni_13;
	// System.UInt32 GarbageiOS.M_kirfalltem212::_jeregenu
	uint32_t ____jeregenu_14;
	// System.Boolean GarbageiOS.M_kirfalltem212::_teavur
	bool ____teavur_15;
	// System.String GarbageiOS.M_kirfalltem212::_pakisJaxemcai
	String_t* ____pakisJaxemcai_16;

public:
	inline static int32_t get_offset_of__detrallnur_0() { return static_cast<int32_t>(offsetof(M_kirfalltem212_t2337207430, ____detrallnur_0)); }
	inline bool get__detrallnur_0() const { return ____detrallnur_0; }
	inline bool* get_address_of__detrallnur_0() { return &____detrallnur_0; }
	inline void set__detrallnur_0(bool value)
	{
		____detrallnur_0 = value;
	}

	inline static int32_t get_offset_of__hobis_1() { return static_cast<int32_t>(offsetof(M_kirfalltem212_t2337207430, ____hobis_1)); }
	inline String_t* get__hobis_1() const { return ____hobis_1; }
	inline String_t** get_address_of__hobis_1() { return &____hobis_1; }
	inline void set__hobis_1(String_t* value)
	{
		____hobis_1 = value;
		Il2CppCodeGenWriteBarrier(&____hobis_1, value);
	}

	inline static int32_t get_offset_of__sootokowHuhairwe_2() { return static_cast<int32_t>(offsetof(M_kirfalltem212_t2337207430, ____sootokowHuhairwe_2)); }
	inline String_t* get__sootokowHuhairwe_2() const { return ____sootokowHuhairwe_2; }
	inline String_t** get_address_of__sootokowHuhairwe_2() { return &____sootokowHuhairwe_2; }
	inline void set__sootokowHuhairwe_2(String_t* value)
	{
		____sootokowHuhairwe_2 = value;
		Il2CppCodeGenWriteBarrier(&____sootokowHuhairwe_2, value);
	}

	inline static int32_t get_offset_of__nerviDime_3() { return static_cast<int32_t>(offsetof(M_kirfalltem212_t2337207430, ____nerviDime_3)); }
	inline bool get__nerviDime_3() const { return ____nerviDime_3; }
	inline bool* get_address_of__nerviDime_3() { return &____nerviDime_3; }
	inline void set__nerviDime_3(bool value)
	{
		____nerviDime_3 = value;
	}

	inline static int32_t get_offset_of__hube_4() { return static_cast<int32_t>(offsetof(M_kirfalltem212_t2337207430, ____hube_4)); }
	inline String_t* get__hube_4() const { return ____hube_4; }
	inline String_t** get_address_of__hube_4() { return &____hube_4; }
	inline void set__hube_4(String_t* value)
	{
		____hube_4 = value;
		Il2CppCodeGenWriteBarrier(&____hube_4, value);
	}

	inline static int32_t get_offset_of__tairdourayNeretas_5() { return static_cast<int32_t>(offsetof(M_kirfalltem212_t2337207430, ____tairdourayNeretas_5)); }
	inline uint32_t get__tairdourayNeretas_5() const { return ____tairdourayNeretas_5; }
	inline uint32_t* get_address_of__tairdourayNeretas_5() { return &____tairdourayNeretas_5; }
	inline void set__tairdourayNeretas_5(uint32_t value)
	{
		____tairdourayNeretas_5 = value;
	}

	inline static int32_t get_offset_of__bewur_6() { return static_cast<int32_t>(offsetof(M_kirfalltem212_t2337207430, ____bewur_6)); }
	inline float get__bewur_6() const { return ____bewur_6; }
	inline float* get_address_of__bewur_6() { return &____bewur_6; }
	inline void set__bewur_6(float value)
	{
		____bewur_6 = value;
	}

	inline static int32_t get_offset_of__beetirRerna_7() { return static_cast<int32_t>(offsetof(M_kirfalltem212_t2337207430, ____beetirRerna_7)); }
	inline uint32_t get__beetirRerna_7() const { return ____beetirRerna_7; }
	inline uint32_t* get_address_of__beetirRerna_7() { return &____beetirRerna_7; }
	inline void set__beetirRerna_7(uint32_t value)
	{
		____beetirRerna_7 = value;
	}

	inline static int32_t get_offset_of__wayzerBemta_8() { return static_cast<int32_t>(offsetof(M_kirfalltem212_t2337207430, ____wayzerBemta_8)); }
	inline uint32_t get__wayzerBemta_8() const { return ____wayzerBemta_8; }
	inline uint32_t* get_address_of__wayzerBemta_8() { return &____wayzerBemta_8; }
	inline void set__wayzerBemta_8(uint32_t value)
	{
		____wayzerBemta_8 = value;
	}

	inline static int32_t get_offset_of__waydee_9() { return static_cast<int32_t>(offsetof(M_kirfalltem212_t2337207430, ____waydee_9)); }
	inline float get__waydee_9() const { return ____waydee_9; }
	inline float* get_address_of__waydee_9() { return &____waydee_9; }
	inline void set__waydee_9(float value)
	{
		____waydee_9 = value;
	}

	inline static int32_t get_offset_of__pispeeqaw_10() { return static_cast<int32_t>(offsetof(M_kirfalltem212_t2337207430, ____pispeeqaw_10)); }
	inline float get__pispeeqaw_10() const { return ____pispeeqaw_10; }
	inline float* get_address_of__pispeeqaw_10() { return &____pispeeqaw_10; }
	inline void set__pispeeqaw_10(float value)
	{
		____pispeeqaw_10 = value;
	}

	inline static int32_t get_offset_of__lallsalnoo_11() { return static_cast<int32_t>(offsetof(M_kirfalltem212_t2337207430, ____lallsalnoo_11)); }
	inline String_t* get__lallsalnoo_11() const { return ____lallsalnoo_11; }
	inline String_t** get_address_of__lallsalnoo_11() { return &____lallsalnoo_11; }
	inline void set__lallsalnoo_11(String_t* value)
	{
		____lallsalnoo_11 = value;
		Il2CppCodeGenWriteBarrier(&____lallsalnoo_11, value);
	}

	inline static int32_t get_offset_of__joraigay_12() { return static_cast<int32_t>(offsetof(M_kirfalltem212_t2337207430, ____joraigay_12)); }
	inline bool get__joraigay_12() const { return ____joraigay_12; }
	inline bool* get_address_of__joraigay_12() { return &____joraigay_12; }
	inline void set__joraigay_12(bool value)
	{
		____joraigay_12 = value;
	}

	inline static int32_t get_offset_of__jasbiseHejoni_13() { return static_cast<int32_t>(offsetof(M_kirfalltem212_t2337207430, ____jasbiseHejoni_13)); }
	inline float get__jasbiseHejoni_13() const { return ____jasbiseHejoni_13; }
	inline float* get_address_of__jasbiseHejoni_13() { return &____jasbiseHejoni_13; }
	inline void set__jasbiseHejoni_13(float value)
	{
		____jasbiseHejoni_13 = value;
	}

	inline static int32_t get_offset_of__jeregenu_14() { return static_cast<int32_t>(offsetof(M_kirfalltem212_t2337207430, ____jeregenu_14)); }
	inline uint32_t get__jeregenu_14() const { return ____jeregenu_14; }
	inline uint32_t* get_address_of__jeregenu_14() { return &____jeregenu_14; }
	inline void set__jeregenu_14(uint32_t value)
	{
		____jeregenu_14 = value;
	}

	inline static int32_t get_offset_of__teavur_15() { return static_cast<int32_t>(offsetof(M_kirfalltem212_t2337207430, ____teavur_15)); }
	inline bool get__teavur_15() const { return ____teavur_15; }
	inline bool* get_address_of__teavur_15() { return &____teavur_15; }
	inline void set__teavur_15(bool value)
	{
		____teavur_15 = value;
	}

	inline static int32_t get_offset_of__pakisJaxemcai_16() { return static_cast<int32_t>(offsetof(M_kirfalltem212_t2337207430, ____pakisJaxemcai_16)); }
	inline String_t* get__pakisJaxemcai_16() const { return ____pakisJaxemcai_16; }
	inline String_t** get_address_of__pakisJaxemcai_16() { return &____pakisJaxemcai_16; }
	inline void set__pakisJaxemcai_16(String_t* value)
	{
		____pakisJaxemcai_16 = value;
		Il2CppCodeGenWriteBarrier(&____pakisJaxemcai_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
