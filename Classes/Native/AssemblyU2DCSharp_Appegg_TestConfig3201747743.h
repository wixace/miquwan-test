﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Appegg/ServerConfig
struct ServerConfig_t2996416080;
// Appegg/HotfixConfig
struct HotfixConfig_t2954663925;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Appegg/TestConfig
struct  TestConfig_t3201747743  : public Il2CppObject
{
public:
	// System.Boolean Appegg/TestConfig::EnableServerConfigTest
	bool ___EnableServerConfigTest_0;
	// System.Boolean Appegg/TestConfig::EnableHotfixConfigTest
	bool ___EnableHotfixConfigTest_1;
	// Appegg/ServerConfig Appegg/TestConfig::ServerConfig
	ServerConfig_t2996416080 * ___ServerConfig_2;
	// Appegg/HotfixConfig Appegg/TestConfig::HotfixConfig
	HotfixConfig_t2954663925 * ___HotfixConfig_3;

public:
	inline static int32_t get_offset_of_EnableServerConfigTest_0() { return static_cast<int32_t>(offsetof(TestConfig_t3201747743, ___EnableServerConfigTest_0)); }
	inline bool get_EnableServerConfigTest_0() const { return ___EnableServerConfigTest_0; }
	inline bool* get_address_of_EnableServerConfigTest_0() { return &___EnableServerConfigTest_0; }
	inline void set_EnableServerConfigTest_0(bool value)
	{
		___EnableServerConfigTest_0 = value;
	}

	inline static int32_t get_offset_of_EnableHotfixConfigTest_1() { return static_cast<int32_t>(offsetof(TestConfig_t3201747743, ___EnableHotfixConfigTest_1)); }
	inline bool get_EnableHotfixConfigTest_1() const { return ___EnableHotfixConfigTest_1; }
	inline bool* get_address_of_EnableHotfixConfigTest_1() { return &___EnableHotfixConfigTest_1; }
	inline void set_EnableHotfixConfigTest_1(bool value)
	{
		___EnableHotfixConfigTest_1 = value;
	}

	inline static int32_t get_offset_of_ServerConfig_2() { return static_cast<int32_t>(offsetof(TestConfig_t3201747743, ___ServerConfig_2)); }
	inline ServerConfig_t2996416080 * get_ServerConfig_2() const { return ___ServerConfig_2; }
	inline ServerConfig_t2996416080 ** get_address_of_ServerConfig_2() { return &___ServerConfig_2; }
	inline void set_ServerConfig_2(ServerConfig_t2996416080 * value)
	{
		___ServerConfig_2 = value;
		Il2CppCodeGenWriteBarrier(&___ServerConfig_2, value);
	}

	inline static int32_t get_offset_of_HotfixConfig_3() { return static_cast<int32_t>(offsetof(TestConfig_t3201747743, ___HotfixConfig_3)); }
	inline HotfixConfig_t2954663925 * get_HotfixConfig_3() const { return ___HotfixConfig_3; }
	inline HotfixConfig_t2954663925 ** get_address_of_HotfixConfig_3() { return &___HotfixConfig_3; }
	inline void set_HotfixConfig_3(HotfixConfig_t2954663925 * value)
	{
		___HotfixConfig_3 = value;
		Il2CppCodeGenWriteBarrier(&___HotfixConfig_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
