﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEventListenerGenerated/<UIEventListener_onTooltip_GetDelegate_member15_arg0>c__AnonStoreyC3
struct U3CUIEventListener_onTooltip_GetDelegate_member15_arg0U3Ec__AnonStoreyC3_t844418158;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UIEventListenerGenerated/<UIEventListener_onTooltip_GetDelegate_member15_arg0>c__AnonStoreyC3::.ctor()
extern "C"  void U3CUIEventListener_onTooltip_GetDelegate_member15_arg0U3Ec__AnonStoreyC3__ctor_m3911164093 (U3CUIEventListener_onTooltip_GetDelegate_member15_arg0U3Ec__AnonStoreyC3_t844418158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated/<UIEventListener_onTooltip_GetDelegate_member15_arg0>c__AnonStoreyC3::<>m__14A(UnityEngine.GameObject,System.Boolean)
extern "C"  void U3CUIEventListener_onTooltip_GetDelegate_member15_arg0U3Ec__AnonStoreyC3_U3CU3Em__14A_m2065267691 (U3CUIEventListener_onTooltip_GetDelegate_member15_arg0U3Ec__AnonStoreyC3_t844418158 * __this, GameObject_t3674682005 * ___go0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
