﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._f76c0b20e0470771be5082ab4f845192
struct _f76c0b20e0470771be5082ab4f845192_t2821628786;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._f76c0b20e0470771be5082ab4f845192::.ctor()
extern "C"  void _f76c0b20e0470771be5082ab4f845192__ctor_m4077929211 (_f76c0b20e0470771be5082ab4f845192_t2821628786 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f76c0b20e0470771be5082ab4f845192::_f76c0b20e0470771be5082ab4f845192m2(System.Int32)
extern "C"  int32_t _f76c0b20e0470771be5082ab4f845192__f76c0b20e0470771be5082ab4f845192m2_m2994588761 (_f76c0b20e0470771be5082ab4f845192_t2821628786 * __this, int32_t ____f76c0b20e0470771be5082ab4f845192a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f76c0b20e0470771be5082ab4f845192::_f76c0b20e0470771be5082ab4f845192m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _f76c0b20e0470771be5082ab4f845192__f76c0b20e0470771be5082ab4f845192m_m988443261 (_f76c0b20e0470771be5082ab4f845192_t2821628786 * __this, int32_t ____f76c0b20e0470771be5082ab4f845192a0, int32_t ____f76c0b20e0470771be5082ab4f845192341, int32_t ____f76c0b20e0470771be5082ab4f845192c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
