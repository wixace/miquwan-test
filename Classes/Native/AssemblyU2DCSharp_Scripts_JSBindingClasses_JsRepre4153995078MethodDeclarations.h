﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Scripts.JSBindingClasses.JsRepresentClassManager
struct JsRepresentClassManager_t4153995078;
// Scripts.JSBindingClasses.JsRepresentClass
struct JsRepresentClass_t2913492551;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Scripts_JSBindingClasses_JsRepre2913492551.h"

// System.Void Scripts.JSBindingClasses.JsRepresentClassManager::.ctor()
extern "C"  void JsRepresentClassManager__ctor_m1060911877 (JsRepresentClassManager_t4153995078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Scripts.JSBindingClasses.JsRepresentClassManager::.cctor()
extern "C"  void JsRepresentClassManager__cctor_m2341400904 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Scripts.JSBindingClasses.JsRepresentClass Scripts.JSBindingClasses.JsRepresentClassManager::AllocJsRepresentClass(System.String,System.Boolean)
extern "C"  JsRepresentClass_t2913492551 * JsRepresentClassManager_AllocJsRepresentClass_m3890609968 (JsRepresentClassManager_t4153995078 * __this, String_t* ___JSClassName0, bool ___isStatic1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Scripts.JSBindingClasses.JsRepresentClassManager Scripts.JSBindingClasses.JsRepresentClassManager::get_Instance()
extern "C"  JsRepresentClassManager_t4153995078 * JsRepresentClassManager_get_Instance_m1292800420 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Scripts.JSBindingClasses.JsRepresentClassManager::Awake()
extern "C"  void JsRepresentClassManager_Awake_m1298517096 (JsRepresentClassManager_t4153995078 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Scripts.JSBindingClasses.JsRepresentClassManager::ilo_initJS1(Scripts.JSBindingClasses.JsRepresentClass,System.String)
extern "C"  void JsRepresentClassManager_ilo_initJS1_m927502543 (Il2CppObject * __this /* static, unused */, JsRepresentClass_t2913492551 * ____this0, String_t* ___jsClassName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
