﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraShotSkillCfg
struct CameraShotSkillCfg_t260684114;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraShotSkillCfg::.ctor()
extern "C"  void CameraShotSkillCfg__ctor_m2231542665 (CameraShotSkillCfg_t260684114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension CameraShotSkillCfg::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * CameraShotSkillCfg_ProtoBuf_IExtensible_GetExtensionObject_m2269463001 (CameraShotSkillCfg_t260684114 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotSkillCfg::get_id()
extern "C"  int32_t CameraShotSkillCfg_get_id_m2442605741 (CameraShotSkillCfg_t260684114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotSkillCfg::set_id(System.Int32)
extern "C"  void CameraShotSkillCfg_set_id_m3449609956 (CameraShotSkillCfg_t260684114 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotSkillCfg::get_heroOrNpcId()
extern "C"  int32_t CameraShotSkillCfg_get_heroOrNpcId_m511887727 (CameraShotSkillCfg_t260684114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotSkillCfg::set_heroOrNpcId(System.Int32)
extern "C"  void CameraShotSkillCfg_set_heroOrNpcId_m1795510938 (CameraShotSkillCfg_t260684114 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotSkillCfg::get_isHero()
extern "C"  int32_t CameraShotSkillCfg_get_isHero_m670997782 (CameraShotSkillCfg_t260684114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotSkillCfg::set_isHero(System.Int32)
extern "C"  void CameraShotSkillCfg_set_isHero_m1706774221 (CameraShotSkillCfg_t260684114 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotSkillCfg::get_skillId()
extern "C"  int32_t CameraShotSkillCfg_get_skillId_m1085444764 (CameraShotSkillCfg_t260684114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotSkillCfg::set_skillId(System.Int32)
extern "C"  void CameraShotSkillCfg_set_skillId_m3520789831 (CameraShotSkillCfg_t260684114 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
