﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.BasicList/Group
struct Group_t3447155990;

#include "codegen/il2cpp-codegen.h"

// System.Void ProtoBuf.Meta.BasicList/Group::.ctor(System.Int32)
extern "C"  void Group__ctor_m1180187366 (Group_t3447155990 * __this, int32_t ___first0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
