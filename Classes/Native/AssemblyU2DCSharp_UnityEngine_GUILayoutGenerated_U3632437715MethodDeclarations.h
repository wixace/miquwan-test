﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUILayoutGenerated/<GUILayout_Window_GetDelegate_member106_arg2>c__AnonStoreyF8
struct U3CGUILayout_Window_GetDelegate_member106_arg2U3Ec__AnonStoreyF8_t632437715;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_GUILayoutGenerated/<GUILayout_Window_GetDelegate_member106_arg2>c__AnonStoreyF8::.ctor()
extern "C"  void U3CGUILayout_Window_GetDelegate_member106_arg2U3Ec__AnonStoreyF8__ctor_m1553321208 (U3CGUILayout_Window_GetDelegate_member106_arg2U3Ec__AnonStoreyF8_t632437715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUILayoutGenerated/<GUILayout_Window_GetDelegate_member106_arg2>c__AnonStoreyF8::<>m__245(System.Int32)
extern "C"  void U3CGUILayout_Window_GetDelegate_member106_arg2U3Ec__AnonStoreyF8_U3CU3Em__245_m3518697733 (U3CGUILayout_Window_GetDelegate_member106_arg2U3Ec__AnonStoreyF8_t632437715 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
