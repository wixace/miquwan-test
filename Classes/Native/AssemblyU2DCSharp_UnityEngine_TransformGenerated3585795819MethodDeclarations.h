﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_TransformGenerated
struct UnityEngine_TransformGenerated_t3585795819;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_TransformGenerated::.ctor()
extern "C"  void UnityEngine_TransformGenerated__ctor_m2509065744 (UnityEngine_TransformGenerated_t3585795819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TransformGenerated::Transform_position(JSVCall)
extern "C"  void UnityEngine_TransformGenerated_Transform_position_m1222081047 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TransformGenerated::Transform_localPosition(JSVCall)
extern "C"  void UnityEngine_TransformGenerated_Transform_localPosition_m3295059704 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TransformGenerated::Transform_eulerAngles(JSVCall)
extern "C"  void UnityEngine_TransformGenerated_Transform_eulerAngles_m551520003 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TransformGenerated::Transform_localEulerAngles(JSVCall)
extern "C"  void UnityEngine_TransformGenerated_Transform_localEulerAngles_m3618908802 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TransformGenerated::Transform_right(JSVCall)
extern "C"  void UnityEngine_TransformGenerated_Transform_right_m1860245904 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TransformGenerated::Transform_up(JSVCall)
extern "C"  void UnityEngine_TransformGenerated_Transform_up_m2567323749 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TransformGenerated::Transform_forward(JSVCall)
extern "C"  void UnityEngine_TransformGenerated_Transform_forward_m741467527 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TransformGenerated::Transform_rotation(JSVCall)
extern "C"  void UnityEngine_TransformGenerated_Transform_rotation_m890786658 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TransformGenerated::Transform_localRotation(JSVCall)
extern "C"  void UnityEngine_TransformGenerated_Transform_localRotation_m2963765315 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TransformGenerated::Transform_localScale(JSVCall)
extern "C"  void UnityEngine_TransformGenerated_Transform_localScale_m482407649 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TransformGenerated::Transform_parent(JSVCall)
extern "C"  void UnityEngine_TransformGenerated_Transform_parent_m61278774 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TransformGenerated::Transform_worldToLocalMatrix(JSVCall)
extern "C"  void UnityEngine_TransformGenerated_Transform_worldToLocalMatrix_m790366305 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TransformGenerated::Transform_localToWorldMatrix(JSVCall)
extern "C"  void UnityEngine_TransformGenerated_Transform_localToWorldMatrix_m1518699635 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TransformGenerated::Transform_root(JSVCall)
extern "C"  void UnityEngine_TransformGenerated_Transform_root_m1879248766 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TransformGenerated::Transform_childCount(JSVCall)
extern "C"  void UnityEngine_TransformGenerated_Transform_childCount_m1818912429 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TransformGenerated::Transform_lossyScale(JSVCall)
extern "C"  void UnityEngine_TransformGenerated_Transform_lossyScale_m2479150060 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TransformGenerated::Transform_hasChanged(JSVCall)
extern "C"  void UnityEngine_TransformGenerated_Transform_hasChanged_m462889254 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_DetachChildren(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_DetachChildren_m57779261 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_Find__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_Find__String_m1235893973 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_FindChild__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_FindChild__String_m47227339 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_GetChild__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_GetChild__Int32_m3720603839 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_GetEnumerator(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_GetEnumerator_m4132189201 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_GetSiblingIndex(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_GetSiblingIndex_m2830442365 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_InverseTransformDirection__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_InverseTransformDirection__Single__Single__Single_m3083171634 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_InverseTransformDirection__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_InverseTransformDirection__Vector3_m3270459448 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_InverseTransformPoint__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_InverseTransformPoint__Single__Single__Single_m3146933379 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_InverseTransformPoint__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_InverseTransformPoint__Vector3_m387545863 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_InverseTransformVector__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_InverseTransformVector__Single__Single__Single_m2363642402 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_InverseTransformVector__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_InverseTransformVector__Vector3_m712368968 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_IsChildOf__Transform(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_IsChildOf__Transform_m945800078 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_LookAt__Transform__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_LookAt__Transform__Vector3_m3280891681 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_LookAt__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_LookAt__Vector3__Vector3_m3376484797 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_LookAt__Transform(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_LookAt__Transform_m1792280145 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_LookAt__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_LookAt__Vector3_m3092169781 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_Rotate__Single__Single__Single__Space(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_Rotate__Single__Single__Single__Space_m3382706698 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_Rotate__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_Rotate__Single__Single__Single_m2071049214 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_Rotate__Vector3__Single__Space(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_Rotate__Vector3__Single__Space_m3063612692 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_Rotate__Vector3__Space(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_Rotate__Vector3__Space_m4202490204 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_Rotate__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_Rotate__Vector3__Single_m3868960820 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_Rotate__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_Rotate__Vector3_m3831661804 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_RotateAround__Vector3__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_RotateAround__Vector3__Vector3__Single_m2362165339 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_SetAsFirstSibling(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_SetAsFirstSibling_m2527395997 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_SetAsLastSibling(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_SetAsLastSibling_m1915257091 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_SetParent__Transform__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_SetParent__Transform__Boolean_m3373780415 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_SetParent__Transform(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_SetParent__Transform_m1372234699 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_SetSiblingIndex__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_SetSiblingIndex__Int32_m185893287 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_TransformDirection__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_TransformDirection__Single__Single__Single_m3391298326 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_TransformDirection__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_TransformDirection__Vector3_m2380452052 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_TransformPoint__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_TransformPoint__Single__Single__Single_m1208326759 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_TransformPoint__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_TransformPoint__Vector3_m2781198755 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_TransformVector__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_TransformVector__Single__Single__Single_m2396379326 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_TransformVector__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_TransformVector__Vector3_m1901164588 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_Translate__Single__Single__Single__Space(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_Translate__Single__Single__Single__Space_m2731016363 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_Translate__Single__Single__Single__Transform(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_Translate__Single__Single__Single__Transform_m834363537 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_Translate__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_Translate__Single__Single__Single_m1012562045 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_Translate__Vector3__Space(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_Translate__Vector3__Space_m735419611 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_Translate__Vector3__Transform(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_Translate__Vector3__Transform_m88040641 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TransformGenerated::Transform_Translate__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TransformGenerated_Transform_Translate__Vector3_m4247728717 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TransformGenerated::__Register()
extern "C"  void UnityEngine_TransformGenerated___Register_m1971484855 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TransformGenerated::ilo_setVector3S1(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_TransformGenerated_ilo_setVector3S1_m892894775 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_TransformGenerated::ilo_getVector3S2(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_TransformGenerated_ilo_getVector3S2_m884118379 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_TransformGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_TransformGenerated_ilo_setObject3_m593676924 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_TransformGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_TransformGenerated_ilo_getObject4_m2076997922 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TransformGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void UnityEngine_TransformGenerated_ilo_setBooleanS5_m842289917 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_TransformGenerated::ilo_getStringS6(System.Int32)
extern "C"  String_t* UnityEngine_TransformGenerated_ilo_getStringS6_m844928851 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_TransformGenerated::ilo_getSingle7(System.Int32)
extern "C"  float UnityEngine_TransformGenerated_ilo_getSingle7_m3381185949 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_TransformGenerated::ilo_getEnum8(System.Int32)
extern "C"  int32_t UnityEngine_TransformGenerated_ilo_getEnum8_m2675303367 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
