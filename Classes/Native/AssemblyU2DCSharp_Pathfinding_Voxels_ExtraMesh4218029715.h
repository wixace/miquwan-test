﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.MeshFilter
struct MeshFilter_t3839065225;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
struct Vector3_t4282066566_marshaled_pinvoke;
struct Vector3_t4282066566_marshaled_com;

#include "mscorlib_System_ValueType1744280289.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Voxels.ExtraMesh
struct  ExtraMesh_t4218029715 
{
public:
	// UnityEngine.MeshFilter Pathfinding.Voxels.ExtraMesh::original
	MeshFilter_t3839065225 * ___original_0;
	// System.Int32 Pathfinding.Voxels.ExtraMesh::area
	int32_t ___area_1;
	// UnityEngine.Vector3[] Pathfinding.Voxels.ExtraMesh::vertices
	Vector3U5BU5D_t215400611* ___vertices_2;
	// System.Int32[] Pathfinding.Voxels.ExtraMesh::triangles
	Int32U5BU5D_t3230847821* ___triangles_3;
	// UnityEngine.Bounds Pathfinding.Voxels.ExtraMesh::bounds
	Bounds_t2711641849  ___bounds_4;
	// UnityEngine.Matrix4x4 Pathfinding.Voxels.ExtraMesh::matrix
	Matrix4x4_t1651859333  ___matrix_5;

public:
	inline static int32_t get_offset_of_original_0() { return static_cast<int32_t>(offsetof(ExtraMesh_t4218029715, ___original_0)); }
	inline MeshFilter_t3839065225 * get_original_0() const { return ___original_0; }
	inline MeshFilter_t3839065225 ** get_address_of_original_0() { return &___original_0; }
	inline void set_original_0(MeshFilter_t3839065225 * value)
	{
		___original_0 = value;
		Il2CppCodeGenWriteBarrier(&___original_0, value);
	}

	inline static int32_t get_offset_of_area_1() { return static_cast<int32_t>(offsetof(ExtraMesh_t4218029715, ___area_1)); }
	inline int32_t get_area_1() const { return ___area_1; }
	inline int32_t* get_address_of_area_1() { return &___area_1; }
	inline void set_area_1(int32_t value)
	{
		___area_1 = value;
	}

	inline static int32_t get_offset_of_vertices_2() { return static_cast<int32_t>(offsetof(ExtraMesh_t4218029715, ___vertices_2)); }
	inline Vector3U5BU5D_t215400611* get_vertices_2() const { return ___vertices_2; }
	inline Vector3U5BU5D_t215400611** get_address_of_vertices_2() { return &___vertices_2; }
	inline void set_vertices_2(Vector3U5BU5D_t215400611* value)
	{
		___vertices_2 = value;
		Il2CppCodeGenWriteBarrier(&___vertices_2, value);
	}

	inline static int32_t get_offset_of_triangles_3() { return static_cast<int32_t>(offsetof(ExtraMesh_t4218029715, ___triangles_3)); }
	inline Int32U5BU5D_t3230847821* get_triangles_3() const { return ___triangles_3; }
	inline Int32U5BU5D_t3230847821** get_address_of_triangles_3() { return &___triangles_3; }
	inline void set_triangles_3(Int32U5BU5D_t3230847821* value)
	{
		___triangles_3 = value;
		Il2CppCodeGenWriteBarrier(&___triangles_3, value);
	}

	inline static int32_t get_offset_of_bounds_4() { return static_cast<int32_t>(offsetof(ExtraMesh_t4218029715, ___bounds_4)); }
	inline Bounds_t2711641849  get_bounds_4() const { return ___bounds_4; }
	inline Bounds_t2711641849 * get_address_of_bounds_4() { return &___bounds_4; }
	inline void set_bounds_4(Bounds_t2711641849  value)
	{
		___bounds_4 = value;
	}

	inline static int32_t get_offset_of_matrix_5() { return static_cast<int32_t>(offsetof(ExtraMesh_t4218029715, ___matrix_5)); }
	inline Matrix4x4_t1651859333  get_matrix_5() const { return ___matrix_5; }
	inline Matrix4x4_t1651859333 * get_address_of_matrix_5() { return &___matrix_5; }
	inline void set_matrix_5(Matrix4x4_t1651859333  value)
	{
		___matrix_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Pathfinding.Voxels.ExtraMesh
struct ExtraMesh_t4218029715_marshaled_pinvoke
{
	MeshFilter_t3839065225 * ___original_0;
	int32_t ___area_1;
	Vector3_t4282066566_marshaled_pinvoke* ___vertices_2;
	int32_t* ___triangles_3;
	Bounds_t2711641849_marshaled_pinvoke ___bounds_4;
	Matrix4x4_t1651859333_marshaled_pinvoke ___matrix_5;
};
// Native definition for marshalling of: Pathfinding.Voxels.ExtraMesh
struct ExtraMesh_t4218029715_marshaled_com
{
	MeshFilter_t3839065225 * ___original_0;
	int32_t ___area_1;
	Vector3_t4282066566_marshaled_com* ___vertices_2;
	int32_t* ___triangles_3;
	Bounds_t2711641849_marshaled_com ___bounds_4;
	Matrix4x4_t1651859333_marshaled_com ___matrix_5;
};
