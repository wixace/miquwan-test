﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,System.Object>
struct ValueCollection_t3962901666;
// System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>
struct Dictionary_2_t967328657;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3194129361.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1322936629_gshared (ValueCollection_t3962901666 * __this, Dictionary_2_t967328657 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1322936629(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3962901666 *, Dictionary_2_t967328657 *, const MethodInfo*))ValueCollection__ctor_m1322936629_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,System.Object>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m189225053_gshared (ValueCollection_t3962901666 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m189225053(__this, ___item0, method) ((  void (*) (ValueCollection_t3962901666 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m189225053_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,System.Object>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3762944934_gshared (ValueCollection_t3962901666 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3762944934(__this, method) ((  void (*) (ValueCollection_t3962901666 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3762944934_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,System.Object>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3159435437_gshared (ValueCollection_t3962901666 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3159435437(__this, ___item0, method) ((  bool (*) (ValueCollection_t3962901666 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3159435437_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,System.Object>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m214337682_gshared (ValueCollection_t3962901666 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m214337682(__this, ___item0, method) ((  bool (*) (ValueCollection_t3962901666 *, Il2CppObject *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m214337682_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2368026598_gshared (ValueCollection_t3962901666 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2368026598(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3962901666 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2368026598_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m3972973866_gshared (ValueCollection_t3962901666 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m3972973866(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3962901666 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3972973866_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3407700665_gshared (ValueCollection_t3962901666 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3407700665(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3962901666 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3407700665_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,System.Object>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1434611296_gshared (ValueCollection_t3962901666 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1434611296(__this, method) ((  bool (*) (ValueCollection_t3962901666 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1434611296_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4000531520_gshared (ValueCollection_t3962901666 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4000531520(__this, method) ((  bool (*) (ValueCollection_t3962901666 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4000531520_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m1893444914_gshared (ValueCollection_t3962901666 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m1893444914(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3962901666 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1893444914_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,System.Object>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m637061180_gshared (ValueCollection_t3962901666 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m637061180(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3962901666 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m637061180_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3194129361  ValueCollection_GetEnumerator_m2862149669_gshared (ValueCollection_t3962901666 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m2862149669(__this, method) ((  Enumerator_t3194129361  (*) (ValueCollection_t3962901666 *, const MethodInfo*))ValueCollection_GetEnumerator_m2862149669_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,System.Object>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m3583443578_gshared (ValueCollection_t3962901666 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m3583443578(__this, method) ((  int32_t (*) (ValueCollection_t3962901666 *, const MethodInfo*))ValueCollection_get_Count_m3583443578_gshared)(__this, method)
