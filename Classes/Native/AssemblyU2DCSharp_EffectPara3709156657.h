﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CombatEntity
struct CombatEntity_t684137495;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectPara
struct  EffectPara_t3709156657  : public Il2CppObject
{
public:
	// System.Int32 EffectPara::id
	int32_t ___id_0;
	// CombatEntity EffectPara::srcTf
	CombatEntity_t684137495 * ___srcTf_1;
	// CombatEntity EffectPara::host
	CombatEntity_t684137495 * ___host_2;
	// UnityEngine.Vector3 EffectPara::pos
	Vector3_t4282066566  ___pos_3;
	// System.Boolean EffectPara::isBlackInShow
	bool ___isBlackInShow_4;
	// System.Single EffectPara::palySpeed
	float ___palySpeed_5;
	// System.Int32 EffectPara::p1
	int32_t ___p1_6;
	// CombatEntity EffectPara::entity
	CombatEntity_t684137495 * ___entity_7;
	// CombatEntity EffectPara::combatEntity
	CombatEntity_t684137495 * ___combatEntity_8;
	// UnityEngine.Vector3 EffectPara::vector3
	Vector3_t4282066566  ___vector3_9;
	// System.Boolean EffectPara::p2
	bool ___p2_10;
	// System.Single EffectPara::p3
	float ___p3_11;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(EffectPara_t3709156657, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_srcTf_1() { return static_cast<int32_t>(offsetof(EffectPara_t3709156657, ___srcTf_1)); }
	inline CombatEntity_t684137495 * get_srcTf_1() const { return ___srcTf_1; }
	inline CombatEntity_t684137495 ** get_address_of_srcTf_1() { return &___srcTf_1; }
	inline void set_srcTf_1(CombatEntity_t684137495 * value)
	{
		___srcTf_1 = value;
		Il2CppCodeGenWriteBarrier(&___srcTf_1, value);
	}

	inline static int32_t get_offset_of_host_2() { return static_cast<int32_t>(offsetof(EffectPara_t3709156657, ___host_2)); }
	inline CombatEntity_t684137495 * get_host_2() const { return ___host_2; }
	inline CombatEntity_t684137495 ** get_address_of_host_2() { return &___host_2; }
	inline void set_host_2(CombatEntity_t684137495 * value)
	{
		___host_2 = value;
		Il2CppCodeGenWriteBarrier(&___host_2, value);
	}

	inline static int32_t get_offset_of_pos_3() { return static_cast<int32_t>(offsetof(EffectPara_t3709156657, ___pos_3)); }
	inline Vector3_t4282066566  get_pos_3() const { return ___pos_3; }
	inline Vector3_t4282066566 * get_address_of_pos_3() { return &___pos_3; }
	inline void set_pos_3(Vector3_t4282066566  value)
	{
		___pos_3 = value;
	}

	inline static int32_t get_offset_of_isBlackInShow_4() { return static_cast<int32_t>(offsetof(EffectPara_t3709156657, ___isBlackInShow_4)); }
	inline bool get_isBlackInShow_4() const { return ___isBlackInShow_4; }
	inline bool* get_address_of_isBlackInShow_4() { return &___isBlackInShow_4; }
	inline void set_isBlackInShow_4(bool value)
	{
		___isBlackInShow_4 = value;
	}

	inline static int32_t get_offset_of_palySpeed_5() { return static_cast<int32_t>(offsetof(EffectPara_t3709156657, ___palySpeed_5)); }
	inline float get_palySpeed_5() const { return ___palySpeed_5; }
	inline float* get_address_of_palySpeed_5() { return &___palySpeed_5; }
	inline void set_palySpeed_5(float value)
	{
		___palySpeed_5 = value;
	}

	inline static int32_t get_offset_of_p1_6() { return static_cast<int32_t>(offsetof(EffectPara_t3709156657, ___p1_6)); }
	inline int32_t get_p1_6() const { return ___p1_6; }
	inline int32_t* get_address_of_p1_6() { return &___p1_6; }
	inline void set_p1_6(int32_t value)
	{
		___p1_6 = value;
	}

	inline static int32_t get_offset_of_entity_7() { return static_cast<int32_t>(offsetof(EffectPara_t3709156657, ___entity_7)); }
	inline CombatEntity_t684137495 * get_entity_7() const { return ___entity_7; }
	inline CombatEntity_t684137495 ** get_address_of_entity_7() { return &___entity_7; }
	inline void set_entity_7(CombatEntity_t684137495 * value)
	{
		___entity_7 = value;
		Il2CppCodeGenWriteBarrier(&___entity_7, value);
	}

	inline static int32_t get_offset_of_combatEntity_8() { return static_cast<int32_t>(offsetof(EffectPara_t3709156657, ___combatEntity_8)); }
	inline CombatEntity_t684137495 * get_combatEntity_8() const { return ___combatEntity_8; }
	inline CombatEntity_t684137495 ** get_address_of_combatEntity_8() { return &___combatEntity_8; }
	inline void set_combatEntity_8(CombatEntity_t684137495 * value)
	{
		___combatEntity_8 = value;
		Il2CppCodeGenWriteBarrier(&___combatEntity_8, value);
	}

	inline static int32_t get_offset_of_vector3_9() { return static_cast<int32_t>(offsetof(EffectPara_t3709156657, ___vector3_9)); }
	inline Vector3_t4282066566  get_vector3_9() const { return ___vector3_9; }
	inline Vector3_t4282066566 * get_address_of_vector3_9() { return &___vector3_9; }
	inline void set_vector3_9(Vector3_t4282066566  value)
	{
		___vector3_9 = value;
	}

	inline static int32_t get_offset_of_p2_10() { return static_cast<int32_t>(offsetof(EffectPara_t3709156657, ___p2_10)); }
	inline bool get_p2_10() const { return ___p2_10; }
	inline bool* get_address_of_p2_10() { return &___p2_10; }
	inline void set_p2_10(bool value)
	{
		___p2_10 = value;
	}

	inline static int32_t get_offset_of_p3_11() { return static_cast<int32_t>(offsetof(EffectPara_t3709156657, ___p3_11)); }
	inline float get_p3_11() const { return ___p3_11; }
	inline float* get_address_of_p3_11() { return &___p3_11; }
	inline void set_p3_11(float value)
	{
		___p3_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
