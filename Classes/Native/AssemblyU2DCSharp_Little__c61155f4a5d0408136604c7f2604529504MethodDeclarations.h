﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._c61155f4a5d0408136604c7f8af73de0
struct _c61155f4a5d0408136604c7f8af73de0_t2604529504;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._c61155f4a5d0408136604c7f8af73de0::.ctor()
extern "C"  void _c61155f4a5d0408136604c7f8af73de0__ctor_m3973413581 (_c61155f4a5d0408136604c7f8af73de0_t2604529504 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c61155f4a5d0408136604c7f8af73de0::_c61155f4a5d0408136604c7f8af73de0m2(System.Int32)
extern "C"  int32_t _c61155f4a5d0408136604c7f8af73de0__c61155f4a5d0408136604c7f8af73de0m2_m3396392345 (_c61155f4a5d0408136604c7f8af73de0_t2604529504 * __this, int32_t ____c61155f4a5d0408136604c7f8af73de0a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c61155f4a5d0408136604c7f8af73de0::_c61155f4a5d0408136604c7f8af73de0m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _c61155f4a5d0408136604c7f8af73de0__c61155f4a5d0408136604c7f8af73de0m_m77091133 (_c61155f4a5d0408136604c7f8af73de0_t2604529504 * __this, int32_t ____c61155f4a5d0408136604c7f8af73de0a0, int32_t ____c61155f4a5d0408136604c7f8af73de0731, int32_t ____c61155f4a5d0408136604c7f8af73de0c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
