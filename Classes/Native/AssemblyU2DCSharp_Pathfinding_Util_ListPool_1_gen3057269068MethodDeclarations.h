﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Boolean>
struct List_1_t1844984270;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Util.ListPool`1<System.Boolean>::.cctor()
extern "C"  void ListPool_1__cctor_m1350248616_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1__cctor_m1350248616(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m1350248616_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<System.Boolean>::Claim()
extern "C"  List_1_t1844984270 * ListPool_1_Claim_m3398115476_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_Claim_m3398115476(__this /* static, unused */, method) ((  List_1_t1844984270 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_Claim_m3398115476_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<System.Boolean>::Claim(System.Int32)
extern "C"  List_1_t1844984270 * ListPool_1_Claim_m2905118693_gshared (Il2CppObject * __this /* static, unused */, int32_t ___capacity0, const MethodInfo* method);
#define ListPool_1_Claim_m2905118693(__this /* static, unused */, ___capacity0, method) ((  List_1_t1844984270 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))ListPool_1_Claim_m2905118693_gshared)(__this /* static, unused */, ___capacity0, method)
// System.Void Pathfinding.Util.ListPool`1<System.Boolean>::Warmup(System.Int32,System.Int32)
extern "C"  void ListPool_1_Warmup_m440773287_gshared (Il2CppObject * __this /* static, unused */, int32_t ___count0, int32_t ___size1, const MethodInfo* method);
#define ListPool_1_Warmup_m440773287(__this /* static, unused */, ___count0, ___size1, method) ((  void (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))ListPool_1_Warmup_m440773287_gshared)(__this /* static, unused */, ___count0, ___size1, method)
// System.Void Pathfinding.Util.ListPool`1<System.Boolean>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m4050264546_gshared (Il2CppObject * __this /* static, unused */, List_1_t1844984270 * ___list0, const MethodInfo* method);
#define ListPool_1_Release_m4050264546(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, List_1_t1844984270 *, const MethodInfo*))ListPool_1_Release_m4050264546_gshared)(__this /* static, unused */, ___list0, method)
// System.Void Pathfinding.Util.ListPool`1<System.Boolean>::Clear()
extern "C"  void ListPool_1_Clear_m1067471824_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_Clear_m1067471824(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_Clear_m1067471824_gshared)(__this /* static, unused */, method)
// System.Int32 Pathfinding.Util.ListPool`1<System.Boolean>::GetSize()
extern "C"  int32_t ListPool_1_GetSize_m699885192_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_GetSize_m699885192(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_GetSize_m699885192_gshared)(__this /* static, unused */, method)
