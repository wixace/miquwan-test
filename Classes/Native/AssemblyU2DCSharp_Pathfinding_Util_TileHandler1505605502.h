﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.RecastGraph
struct RecastGraph_t2197443166;
// System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType>
struct List_1_t3732775565;
// Pathfinding.ClipperLib.Clipper
struct Clipper_t636949239;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>
struct Dictionary_2_t2731505821;
// System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>
struct Dictionary_2_t2245318082;
// Pathfinding.Util.TileHandler/TileType[]
struct TileTypeU5BU5D_t2868017328;
// System.Boolean[]
struct BooleanU5BU5D_t3456302923;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Util.TileHandler
struct  TileHandler_t1505605502  : public Il2CppObject
{
public:
	// Pathfinding.RecastGraph Pathfinding.Util.TileHandler::_graph
	RecastGraph_t2197443166 * ____graph_3;
	// System.Collections.Generic.List`1<Pathfinding.Util.TileHandler/TileType> Pathfinding.Util.TileHandler::tileTypes
	List_1_t3732775565 * ___tileTypes_4;
	// Pathfinding.ClipperLib.Clipper Pathfinding.Util.TileHandler::clipper
	Clipper_t636949239 * ___clipper_5;
	// System.Int32[] Pathfinding.Util.TileHandler::cached_int_array
	Int32U5BU5D_t3230847821* ___cached_int_array_6;
	// System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32> Pathfinding.Util.TileHandler::cached_Int3_int_dict
	Dictionary_2_t2731505821 * ___cached_Int3_int_dict_7;
	// System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32> Pathfinding.Util.TileHandler::cached_Int2_int_dict
	Dictionary_2_t2245318082 * ___cached_Int2_int_dict_8;
	// Pathfinding.Util.TileHandler/TileType[] Pathfinding.Util.TileHandler::activeTileTypes
	TileTypeU5BU5D_t2868017328* ___activeTileTypes_9;
	// System.Int32[] Pathfinding.Util.TileHandler::activeTileRotations
	Int32U5BU5D_t3230847821* ___activeTileRotations_10;
	// System.Int32[] Pathfinding.Util.TileHandler::activeTileOffsets
	Int32U5BU5D_t3230847821* ___activeTileOffsets_11;
	// System.Boolean[] Pathfinding.Util.TileHandler::reloadedInBatch
	BooleanU5BU5D_t3456302923* ___reloadedInBatch_12;
	// System.Boolean Pathfinding.Util.TileHandler::isBatching
	bool ___isBatching_13;

public:
	inline static int32_t get_offset_of__graph_3() { return static_cast<int32_t>(offsetof(TileHandler_t1505605502, ____graph_3)); }
	inline RecastGraph_t2197443166 * get__graph_3() const { return ____graph_3; }
	inline RecastGraph_t2197443166 ** get_address_of__graph_3() { return &____graph_3; }
	inline void set__graph_3(RecastGraph_t2197443166 * value)
	{
		____graph_3 = value;
		Il2CppCodeGenWriteBarrier(&____graph_3, value);
	}

	inline static int32_t get_offset_of_tileTypes_4() { return static_cast<int32_t>(offsetof(TileHandler_t1505605502, ___tileTypes_4)); }
	inline List_1_t3732775565 * get_tileTypes_4() const { return ___tileTypes_4; }
	inline List_1_t3732775565 ** get_address_of_tileTypes_4() { return &___tileTypes_4; }
	inline void set_tileTypes_4(List_1_t3732775565 * value)
	{
		___tileTypes_4 = value;
		Il2CppCodeGenWriteBarrier(&___tileTypes_4, value);
	}

	inline static int32_t get_offset_of_clipper_5() { return static_cast<int32_t>(offsetof(TileHandler_t1505605502, ___clipper_5)); }
	inline Clipper_t636949239 * get_clipper_5() const { return ___clipper_5; }
	inline Clipper_t636949239 ** get_address_of_clipper_5() { return &___clipper_5; }
	inline void set_clipper_5(Clipper_t636949239 * value)
	{
		___clipper_5 = value;
		Il2CppCodeGenWriteBarrier(&___clipper_5, value);
	}

	inline static int32_t get_offset_of_cached_int_array_6() { return static_cast<int32_t>(offsetof(TileHandler_t1505605502, ___cached_int_array_6)); }
	inline Int32U5BU5D_t3230847821* get_cached_int_array_6() const { return ___cached_int_array_6; }
	inline Int32U5BU5D_t3230847821** get_address_of_cached_int_array_6() { return &___cached_int_array_6; }
	inline void set_cached_int_array_6(Int32U5BU5D_t3230847821* value)
	{
		___cached_int_array_6 = value;
		Il2CppCodeGenWriteBarrier(&___cached_int_array_6, value);
	}

	inline static int32_t get_offset_of_cached_Int3_int_dict_7() { return static_cast<int32_t>(offsetof(TileHandler_t1505605502, ___cached_Int3_int_dict_7)); }
	inline Dictionary_2_t2731505821 * get_cached_Int3_int_dict_7() const { return ___cached_Int3_int_dict_7; }
	inline Dictionary_2_t2731505821 ** get_address_of_cached_Int3_int_dict_7() { return &___cached_Int3_int_dict_7; }
	inline void set_cached_Int3_int_dict_7(Dictionary_2_t2731505821 * value)
	{
		___cached_Int3_int_dict_7 = value;
		Il2CppCodeGenWriteBarrier(&___cached_Int3_int_dict_7, value);
	}

	inline static int32_t get_offset_of_cached_Int2_int_dict_8() { return static_cast<int32_t>(offsetof(TileHandler_t1505605502, ___cached_Int2_int_dict_8)); }
	inline Dictionary_2_t2245318082 * get_cached_Int2_int_dict_8() const { return ___cached_Int2_int_dict_8; }
	inline Dictionary_2_t2245318082 ** get_address_of_cached_Int2_int_dict_8() { return &___cached_Int2_int_dict_8; }
	inline void set_cached_Int2_int_dict_8(Dictionary_2_t2245318082 * value)
	{
		___cached_Int2_int_dict_8 = value;
		Il2CppCodeGenWriteBarrier(&___cached_Int2_int_dict_8, value);
	}

	inline static int32_t get_offset_of_activeTileTypes_9() { return static_cast<int32_t>(offsetof(TileHandler_t1505605502, ___activeTileTypes_9)); }
	inline TileTypeU5BU5D_t2868017328* get_activeTileTypes_9() const { return ___activeTileTypes_9; }
	inline TileTypeU5BU5D_t2868017328** get_address_of_activeTileTypes_9() { return &___activeTileTypes_9; }
	inline void set_activeTileTypes_9(TileTypeU5BU5D_t2868017328* value)
	{
		___activeTileTypes_9 = value;
		Il2CppCodeGenWriteBarrier(&___activeTileTypes_9, value);
	}

	inline static int32_t get_offset_of_activeTileRotations_10() { return static_cast<int32_t>(offsetof(TileHandler_t1505605502, ___activeTileRotations_10)); }
	inline Int32U5BU5D_t3230847821* get_activeTileRotations_10() const { return ___activeTileRotations_10; }
	inline Int32U5BU5D_t3230847821** get_address_of_activeTileRotations_10() { return &___activeTileRotations_10; }
	inline void set_activeTileRotations_10(Int32U5BU5D_t3230847821* value)
	{
		___activeTileRotations_10 = value;
		Il2CppCodeGenWriteBarrier(&___activeTileRotations_10, value);
	}

	inline static int32_t get_offset_of_activeTileOffsets_11() { return static_cast<int32_t>(offsetof(TileHandler_t1505605502, ___activeTileOffsets_11)); }
	inline Int32U5BU5D_t3230847821* get_activeTileOffsets_11() const { return ___activeTileOffsets_11; }
	inline Int32U5BU5D_t3230847821** get_address_of_activeTileOffsets_11() { return &___activeTileOffsets_11; }
	inline void set_activeTileOffsets_11(Int32U5BU5D_t3230847821* value)
	{
		___activeTileOffsets_11 = value;
		Il2CppCodeGenWriteBarrier(&___activeTileOffsets_11, value);
	}

	inline static int32_t get_offset_of_reloadedInBatch_12() { return static_cast<int32_t>(offsetof(TileHandler_t1505605502, ___reloadedInBatch_12)); }
	inline BooleanU5BU5D_t3456302923* get_reloadedInBatch_12() const { return ___reloadedInBatch_12; }
	inline BooleanU5BU5D_t3456302923** get_address_of_reloadedInBatch_12() { return &___reloadedInBatch_12; }
	inline void set_reloadedInBatch_12(BooleanU5BU5D_t3456302923* value)
	{
		___reloadedInBatch_12 = value;
		Il2CppCodeGenWriteBarrier(&___reloadedInBatch_12, value);
	}

	inline static int32_t get_offset_of_isBatching_13() { return static_cast<int32_t>(offsetof(TileHandler_t1505605502, ___isBatching_13)); }
	inline bool get_isBatching_13() const { return ___isBatching_13; }
	inline bool* get_address_of_isBatching_13() { return &___isBatching_13; }
	inline void set_isBatching_13(bool value)
	{
		___isBatching_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
