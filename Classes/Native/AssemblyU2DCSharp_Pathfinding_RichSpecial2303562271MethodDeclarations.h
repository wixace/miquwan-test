﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RichSpecial
struct RichSpecial_t2303562271;
// Pathfinding.NodeLink2
struct NodeLink2_t1645404664;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_NodeLink21645404664.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.RichSpecial::.ctor()
extern "C"  void RichSpecial__ctor_m2418685944 (RichSpecial_t2303562271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichSpecial::OnEnterPool()
extern "C"  void RichSpecial_OnEnterPool_m1431925771 (RichSpecial_t2303562271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.RichSpecial Pathfinding.RichSpecial::Initialize(Pathfinding.NodeLink2,Pathfinding.GraphNode)
extern "C"  RichSpecial_t2303562271 * RichSpecial_Initialize_m615051440 (RichSpecial_t2303562271 * __this, NodeLink2_t1645404664 * ___nodeLink0, GraphNode_t23612370 * ___first1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Pathfinding.RichSpecial::ilo_get_EndTransform1(Pathfinding.NodeLink2)
extern "C"  Transform_t1659122786 * RichSpecial_ilo_get_EndTransform1_m4088254350 (Il2CppObject * __this /* static, unused */, NodeLink2_t1645404664 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
