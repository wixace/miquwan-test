﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.GraphNodeDelegate
struct GraphNodeDelegate_t1466738551;
// System.Object
struct Il2CppObject;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void Pathfinding.GraphNodeDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void GraphNodeDelegate__ctor_m2065203330 (GraphNodeDelegate_t1466738551 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNodeDelegate::Invoke(Pathfinding.GraphNode)
extern "C"  void GraphNodeDelegate_Invoke_m2762306832 (GraphNodeDelegate_t1466738551 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Pathfinding.GraphNodeDelegate::BeginInvoke(Pathfinding.GraphNode,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * GraphNodeDelegate_BeginInvoke_m2644323849 (GraphNodeDelegate_t1466738551 * __this, GraphNode_t23612370 * ___node0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNodeDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void GraphNodeDelegate_EndInvoke_m3308553106 (GraphNodeDelegate_t1466738551 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
