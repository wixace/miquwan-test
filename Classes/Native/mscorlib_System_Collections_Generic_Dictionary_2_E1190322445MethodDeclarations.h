﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.UInt32,System.Object>
struct Dictionary_2_t4167966349;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1190322445.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066747055.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m330901371_gshared (Enumerator_t1190322445 * __this, Dictionary_2_t4167966349 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m330901371(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1190322445 *, Dictionary_2_t4167966349 *, const MethodInfo*))Enumerator__ctor_m330901371_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1174381702_gshared (Enumerator_t1190322445 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1174381702(__this, method) ((  Il2CppObject * (*) (Enumerator_t1190322445 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1174381702_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2713473626_gshared (Enumerator_t1190322445 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2713473626(__this, method) ((  void (*) (Enumerator_t1190322445 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2713473626_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2775655267_gshared (Enumerator_t1190322445 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2775655267(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1190322445 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2775655267_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2261181666_gshared (Enumerator_t1190322445 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2261181666(__this, method) ((  Il2CppObject * (*) (Enumerator_t1190322445 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2261181666_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m790565620_gshared (Enumerator_t1190322445 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m790565620(__this, method) ((  Il2CppObject * (*) (Enumerator_t1190322445 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m790565620_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1009697350_gshared (Enumerator_t1190322445 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1009697350(__this, method) ((  bool (*) (Enumerator_t1190322445 *, const MethodInfo*))Enumerator_MoveNext_m1009697350_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t4066747055  Enumerator_get_Current_m303915562_gshared (Enumerator_t1190322445 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m303915562(__this, method) ((  KeyValuePair_2_t4066747055  (*) (Enumerator_t1190322445 *, const MethodInfo*))Enumerator_get_Current_m303915562_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,System.Object>::get_CurrentKey()
extern "C"  uint32_t Enumerator_get_CurrentKey_m4277694867_gshared (Enumerator_t1190322445 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m4277694867(__this, method) ((  uint32_t (*) (Enumerator_t1190322445 *, const MethodInfo*))Enumerator_get_CurrentKey_m4277694867_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m490765687_gshared (Enumerator_t1190322445 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m490765687(__this, method) ((  Il2CppObject * (*) (Enumerator_t1190322445 *, const MethodInfo*))Enumerator_get_CurrentValue_m490765687_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m1609873741_gshared (Enumerator_t1190322445 * __this, const MethodInfo* method);
#define Enumerator_Reset_m1609873741(__this, method) ((  void (*) (Enumerator_t1190322445 *, const MethodInfo*))Enumerator_Reset_m1609873741_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m353966998_gshared (Enumerator_t1190322445 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m353966998(__this, method) ((  void (*) (Enumerator_t1190322445 *, const MethodInfo*))Enumerator_VerifyState_m353966998_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m3604740478_gshared (Enumerator_t1190322445 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m3604740478(__this, method) ((  void (*) (Enumerator_t1190322445 *, const MethodInfo*))Enumerator_VerifyCurrent_m3604740478_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.UInt32,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3423867613_gshared (Enumerator_t1190322445 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3423867613(__this, method) ((  void (*) (Enumerator_t1190322445 *, const MethodInfo*))Enumerator_Dispose_m3423867613_gshared)(__this, method)
