﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pomelo.Protobuf.Protobuf
struct Protobuf_t1750100383;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "mscorlib_System_String7231557.h"

// System.Void Pomelo.Protobuf.Protobuf::.ctor(Newtonsoft.Json.Linq.JObject,Newtonsoft.Json.Linq.JObject)
extern "C"  void Protobuf__ctor_m214687269 (Protobuf_t1750100383 * __this, JObject_t1798544199 * ___encodeProtos0, JObject_t1798544199 * ___decodeProtos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.Protobuf.Protobuf::encode(System.String,Newtonsoft.Json.Linq.JObject)
extern "C"  ByteU5BU5D_t4260760469* Protobuf_encode_m477559351 (Protobuf_t1750100383 * __this, String_t* ___route0, JObject_t1798544199 * ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject Pomelo.Protobuf.Protobuf::decode(System.String,System.Byte[])
extern "C"  JObject_t1798544199 * Protobuf_decode_m1117375655 (Protobuf_t1750100383 * __this, String_t* ___route0, ByteU5BU5D_t4260760469* ___buffer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
