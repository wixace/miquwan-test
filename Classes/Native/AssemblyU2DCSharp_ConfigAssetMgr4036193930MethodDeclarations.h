﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConfigAssetMgr
struct ConfigAssetMgr_t4036193930;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.String
struct String_t;
// UnityEngine.Coroutine
struct Coroutine_t3621161934;
// ResToABName
struct ResToABName_t627124519;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ResToABName627124519.h"

// System.Void ConfigAssetMgr::.ctor()
extern "C"  void ConfigAssetMgr__ctor_m2836732241 (ConfigAssetMgr_t4036193930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConfigAssetMgr::get_isLoaded()
extern "C"  bool ConfigAssetMgr_get_isLoaded_m538837503 (ConfigAssetMgr_t4036193930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConfigAssetMgr::set_isLoaded(System.Boolean)
extern "C"  void ConfigAssetMgr_set_isLoaded_m2294052086 (ConfigAssetMgr_t4036193930 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConfigAssetMgr::get_isLoading()
extern "C"  bool ConfigAssetMgr_get_isLoading_m3823113252 (ConfigAssetMgr_t4036193930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConfigAssetMgr::set_isLoading(System.Boolean)
extern "C"  void ConfigAssetMgr_set_isLoading_m2273643227 (ConfigAssetMgr_t4036193930 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ConfigAssetMgr::get_loadProgress()
extern "C"  float ConfigAssetMgr_get_loadProgress_m1465701627 (ConfigAssetMgr_t4036193930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ConfigAssetMgr::LoadConfig()
extern "C"  void ConfigAssetMgr_LoadConfig_m1705041435 (ConfigAssetMgr_t4036193930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ConfigAssetMgr::RunLoadConfig()
extern "C"  Il2CppObject * ConfigAssetMgr_RunLoadConfig_m3973586202 (ConfigAssetMgr_t4036193930 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConfigAssetMgr::LoadPathToABPath(System.String)
extern "C"  String_t* ConfigAssetMgr_LoadPathToABPath_m2422014880 (ConfigAssetMgr_t4036193930 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConfigAssetMgr::LoadPathToABFullPath(System.String)
extern "C"  String_t* ConfigAssetMgr_LoadPathToABFullPath_m76621457 (ConfigAssetMgr_t4036193930 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConfigAssetMgr::LoadPathToAssetName(System.String)
extern "C"  String_t* ConfigAssetMgr_LoadPathToAssetName_m303369729 (ConfigAssetMgr_t4036193930 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConfigAssetMgr::AssetNameToABPath(System.String)
extern "C"  String_t* ConfigAssetMgr_AssetNameToABPath_m913305018 (ConfigAssetMgr_t4036193930 * __this, String_t* ___assetName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConfigAssetMgr::GetIsCompressByABName(System.String)
extern "C"  bool ConfigAssetMgr_GetIsCompressByABName_m456842466 (ConfigAssetMgr_t4036193930 * __this, String_t* ___abName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ConfigAssetMgr::IsNullAsset(System.String)
extern "C"  bool ConfigAssetMgr_IsNullAsset_m1145204648 (ConfigAssetMgr_t4036193930 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Coroutine ConfigAssetMgr::ilo_StartCoroutine1(System.Collections.IEnumerator)
extern "C"  Coroutine_t3621161934 * ConfigAssetMgr_ilo_StartCoroutine1_m2924239568 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___routine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ConfigAssetMgr::ilo_GetABNameByAssetName2(ResToABName,System.String)
extern "C"  String_t* ConfigAssetMgr_ilo_GetABNameByAssetName2_m1774481150 (Il2CppObject * __this /* static, unused */, ResToABName_t627124519 * ____this0, String_t* ___assetName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
