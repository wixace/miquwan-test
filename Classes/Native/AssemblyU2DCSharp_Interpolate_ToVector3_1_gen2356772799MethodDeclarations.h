﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Interpolate_ToVector3_1_gen573499088MethodDeclarations.h"

// System.Void Interpolate/ToVector3`1<UnityEngine.Transform>::.ctor(System.Object,System.IntPtr)
#define ToVector3_1__ctor_m55492756(__this, ___object0, ___method1, method) ((  void (*) (ToVector3_1_t2356772799 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ToVector3_1__ctor_m4161488491_gshared)(__this, ___object0, ___method1, method)
// UnityEngine.Vector3 Interpolate/ToVector3`1<UnityEngine.Transform>::Invoke(T)
#define ToVector3_1_Invoke_m1977316598(__this, ___v0, method) ((  Vector3_t4282066566  (*) (ToVector3_1_t2356772799 *, Transform_t1659122786 *, const MethodInfo*))ToVector3_1_Invoke_m1249282367_gshared)(__this, ___v0, method)
// System.IAsyncResult Interpolate/ToVector3`1<UnityEngine.Transform>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define ToVector3_1_BeginInvoke_m3559691517(__this, ___v0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (ToVector3_1_t2356772799 *, Transform_t1659122786 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))ToVector3_1_BeginInvoke_m774210118_gshared)(__this, ___v0, ___callback1, ___object2, method)
// UnityEngine.Vector3 Interpolate/ToVector3`1<UnityEngine.Transform>::EndInvoke(System.IAsyncResult)
#define ToVector3_1_EndInvoke_m1827316894(__this, ___result0, method) ((  Vector3_t4282066566  (*) (ToVector3_1_t2356772799 *, Il2CppObject *, const MethodInfo*))ToVector3_1_EndInvoke_m3857074165_gshared)(__this, ___result0, method)
