﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSFileLoader/OnLoadJS
struct OnLoadJS_t2928531685;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void JSFileLoader/OnLoadJS::.ctor(System.Object,System.IntPtr)
extern "C"  void OnLoadJS__ctor_m2858852924 (OnLoadJS_t2928531685 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSFileLoader/OnLoadJS::Invoke(System.String,System.Byte[],System.String)
extern "C"  void OnLoadJS_Invoke_m3468998443 (OnLoadJS_t2928531685 * __this, String_t* ___shortName0, ByteU5BU5D_t4260760469* ___bytes1, String_t* ___fullName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult JSFileLoader/OnLoadJS::BeginInvoke(System.String,System.Byte[],System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnLoadJS_BeginInvoke_m3961429594 (OnLoadJS_t2928531685 * __this, String_t* ___shortName0, ByteU5BU5D_t4260760469* ___bytes1, String_t* ___fullName2, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSFileLoader/OnLoadJS::EndInvoke(System.IAsyncResult)
extern "C"  void OnLoadJS_EndInvoke_m1224512076 (OnLoadJS_t2928531685 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
