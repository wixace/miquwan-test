﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_SplatPrototypeGenerated
struct UnityEngine_SplatPrototypeGenerated_t735735271;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine_SplatPrototypeGenerated::.ctor()
extern "C"  void UnityEngine_SplatPrototypeGenerated__ctor_m3297806692 (UnityEngine_SplatPrototypeGenerated_t735735271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SplatPrototypeGenerated::SplatPrototype_SplatPrototype1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SplatPrototypeGenerated_SplatPrototype_SplatPrototype1_m875657550 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SplatPrototypeGenerated::SplatPrototype_texture(JSVCall)
extern "C"  void UnityEngine_SplatPrototypeGenerated_SplatPrototype_texture_m899412619 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SplatPrototypeGenerated::SplatPrototype_normalMap(JSVCall)
extern "C"  void UnityEngine_SplatPrototypeGenerated_SplatPrototype_normalMap_m4081506801 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SplatPrototypeGenerated::SplatPrototype_tileSize(JSVCall)
extern "C"  void UnityEngine_SplatPrototypeGenerated_SplatPrototype_tileSize_m864768727 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SplatPrototypeGenerated::SplatPrototype_tileOffset(JSVCall)
extern "C"  void UnityEngine_SplatPrototypeGenerated_SplatPrototype_tileOffset_m1011650053 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SplatPrototypeGenerated::SplatPrototype_specular(JSVCall)
extern "C"  void UnityEngine_SplatPrototypeGenerated_SplatPrototype_specular_m1996759523 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SplatPrototypeGenerated::SplatPrototype_metallic(JSVCall)
extern "C"  void UnityEngine_SplatPrototypeGenerated_SplatPrototype_metallic_m1444988551 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SplatPrototypeGenerated::SplatPrototype_smoothness(JSVCall)
extern "C"  void UnityEngine_SplatPrototypeGenerated_SplatPrototype_smoothness_m1000695393 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SplatPrototypeGenerated::__Register()
extern "C"  void UnityEngine_SplatPrototypeGenerated___Register_m1839411683 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SplatPrototypeGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_SplatPrototypeGenerated_ilo_getObject1_m411300434 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SplatPrototypeGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_SplatPrototypeGenerated_ilo_setObject2_m735964171 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_SplatPrototypeGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_SplatPrototypeGenerated_ilo_getObject3_m2220078211 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SplatPrototypeGenerated::ilo_setVector2S4(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_SplatPrototypeGenerated_ilo_setVector2S4_m1964204550 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
