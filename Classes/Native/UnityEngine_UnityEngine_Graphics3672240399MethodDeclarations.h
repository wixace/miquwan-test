﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Graphics
struct Graphics_t3672240399;
// UnityEngine.Mesh
struct Mesh_t4241756145;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t1322387783;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.ComputeBuffer
struct ComputeBuffer_t37359565;
// UnityEngine.Texture
struct Texture_t2526458961;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t1654378665;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;
// UnityEngine.RenderBuffer[]
struct RenderBufferU5BU5D_t1970987103;
// UnityEngine.Rendering.RenderBufferLoadAction[]
struct RenderBufferLoadActionU5BU5D_t3840699671;
// UnityEngine.Rendering.RenderBufferStoreAction[]
struct RenderBufferStoreActionU5BU5D_t2354850566;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_MaterialPropertyBlock1322387783.h"
#include "UnityEngine_UnityEngine_Rendering_ShadowCastingMod2598729988.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_Internal_DrawMeshTRArgumen1240439529.h"
#include "UnityEngine_UnityEngine_Internal_DrawMeshMatrixArg1378504422.h"
#include "UnityEngine_UnityEngine_MeshTopology2348044928.h"
#include "UnityEngine_UnityEngine_ComputeBuffer37359565.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_InternalDrawTextureArgumen4047764288.h"
#include "UnityEngine_UnityEngine_Rendering_CommandBuffer1654378665.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"
#include "UnityEngine_UnityEngine_RenderBuffer3529837690.h"
#include "UnityEngine_UnityEngine_CubemapFace2005084858.h"
#include "UnityEngine_UnityEngine_Rendering_RenderBufferLoad4243858466.h"
#include "UnityEngine_UnityEngine_Rendering_RenderBufferStor2198970271.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_RenderTargetSetup2400315372.h"

// System.Void UnityEngine.Graphics::.ctor()
extern "C"  void Graphics__ctor_m594635586 (Graphics_t3672240399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMesh(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Material,System.Int32,UnityEngine.Camera,System.Int32,UnityEngine.MaterialPropertyBlock,System.Boolean)
extern "C"  void Graphics_DrawMesh_m83847552 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, Material_t3870600107 * ___material3, int32_t ___layer4, Camera_t2727095145 * ___camera5, int32_t ___submeshIndex6, MaterialPropertyBlock_t1322387783 * ___properties7, bool ___castShadows8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMesh(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Material,System.Int32,UnityEngine.Camera,System.Int32,UnityEngine.MaterialPropertyBlock)
extern "C"  void Graphics_DrawMesh_m4109763677 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, Material_t3870600107 * ___material3, int32_t ___layer4, Camera_t2727095145 * ___camera5, int32_t ___submeshIndex6, MaterialPropertyBlock_t1322387783 * ___properties7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMesh(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Material,System.Int32,UnityEngine.Camera,System.Int32)
extern "C"  void Graphics_DrawMesh_m1136977259 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, Material_t3870600107 * ___material3, int32_t ___layer4, Camera_t2727095145 * ___camera5, int32_t ___submeshIndex6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMesh(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Material,System.Int32,UnityEngine.Camera)
extern "C"  void Graphics_DrawMesh_m3099580140 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, Material_t3870600107 * ___material3, int32_t ___layer4, Camera_t2727095145 * ___camera5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMesh(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Material,System.Int32)
extern "C"  void Graphics_DrawMesh_m1926667262 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, Material_t3870600107 * ___material3, int32_t ___layer4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMesh(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Material,System.Int32,UnityEngine.Camera,System.Int32,UnityEngine.MaterialPropertyBlock,System.Boolean,System.Boolean)
extern "C"  void Graphics_DrawMesh_m626047485 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, Material_t3870600107 * ___material3, int32_t ___layer4, Camera_t2727095145 * ___camera5, int32_t ___submeshIndex6, MaterialPropertyBlock_t1322387783 * ___properties7, bool ___castShadows8, bool ___receiveShadows9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMesh(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Material,System.Int32,UnityEngine.Camera,System.Int32,UnityEngine.MaterialPropertyBlock,UnityEngine.Rendering.ShadowCastingMode,System.Boolean)
extern "C"  void Graphics_DrawMesh_m1450904193 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, Material_t3870600107 * ___material3, int32_t ___layer4, Camera_t2727095145 * ___camera5, int32_t ___submeshIndex6, MaterialPropertyBlock_t1322387783 * ___properties7, int32_t ___castShadows8, bool ___receiveShadows9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMesh(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Material,System.Int32,UnityEngine.Camera,System.Int32,UnityEngine.MaterialPropertyBlock,UnityEngine.Rendering.ShadowCastingMode)
extern "C"  void Graphics_DrawMesh_m1630970236 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, Material_t3870600107 * ___material3, int32_t ___layer4, Camera_t2727095145 * ___camera5, int32_t ___submeshIndex6, MaterialPropertyBlock_t1322387783 * ___properties7, int32_t ___castShadows8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMesh(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Material,System.Int32,UnityEngine.Camera,System.Int32,UnityEngine.MaterialPropertyBlock,UnityEngine.Rendering.ShadowCastingMode,System.Boolean,UnityEngine.Transform)
extern "C"  void Graphics_DrawMesh_m2777006648 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, Material_t3870600107 * ___material3, int32_t ___layer4, Camera_t2727095145 * ___camera5, int32_t ___submeshIndex6, MaterialPropertyBlock_t1322387783 * ___properties7, int32_t ___castShadows8, bool ___receiveShadows9, Transform_t1659122786 * ___probeAnchor10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMesh(UnityEngine.Mesh,UnityEngine.Matrix4x4,UnityEngine.Material,System.Int32,UnityEngine.Camera,System.Int32,UnityEngine.MaterialPropertyBlock,System.Boolean)
extern "C"  void Graphics_DrawMesh_m39997868 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Matrix4x4_t1651859333  ___matrix1, Material_t3870600107 * ___material2, int32_t ___layer3, Camera_t2727095145 * ___camera4, int32_t ___submeshIndex5, MaterialPropertyBlock_t1322387783 * ___properties6, bool ___castShadows7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMesh(UnityEngine.Mesh,UnityEngine.Matrix4x4,UnityEngine.Material,System.Int32,UnityEngine.Camera,System.Int32,UnityEngine.MaterialPropertyBlock)
extern "C"  void Graphics_DrawMesh_m291326385 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Matrix4x4_t1651859333  ___matrix1, Material_t3870600107 * ___material2, int32_t ___layer3, Camera_t2727095145 * ___camera4, int32_t ___submeshIndex5, MaterialPropertyBlock_t1322387783 * ___properties6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMesh(UnityEngine.Mesh,UnityEngine.Matrix4x4,UnityEngine.Material,System.Int32,UnityEngine.Camera,System.Int32)
extern "C"  void Graphics_DrawMesh_m3980640191 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Matrix4x4_t1651859333  ___matrix1, Material_t3870600107 * ___material2, int32_t ___layer3, Camera_t2727095145 * ___camera4, int32_t ___submeshIndex5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMesh(UnityEngine.Mesh,UnityEngine.Matrix4x4,UnityEngine.Material,System.Int32,UnityEngine.Camera)
extern "C"  void Graphics_DrawMesh_m2315830296 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Matrix4x4_t1651859333  ___matrix1, Material_t3870600107 * ___material2, int32_t ___layer3, Camera_t2727095145 * ___camera4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMesh(UnityEngine.Mesh,UnityEngine.Matrix4x4,UnityEngine.Material,System.Int32)
extern "C"  void Graphics_DrawMesh_m548652626 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Matrix4x4_t1651859333  ___matrix1, Material_t3870600107 * ___material2, int32_t ___layer3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMesh(UnityEngine.Mesh,UnityEngine.Matrix4x4,UnityEngine.Material,System.Int32,UnityEngine.Camera,System.Int32,UnityEngine.MaterialPropertyBlock,System.Boolean,System.Boolean)
extern "C"  void Graphics_DrawMesh_m1842541649 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Matrix4x4_t1651859333  ___matrix1, Material_t3870600107 * ___material2, int32_t ___layer3, Camera_t2727095145 * ___camera4, int32_t ___submeshIndex5, MaterialPropertyBlock_t1322387783 * ___properties6, bool ___castShadows7, bool ___receiveShadows8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMesh(UnityEngine.Mesh,UnityEngine.Matrix4x4,UnityEngine.Material,System.Int32,UnityEngine.Camera,System.Int32,UnityEngine.MaterialPropertyBlock,UnityEngine.Rendering.ShadowCastingMode,System.Boolean)
extern "C"  void Graphics_DrawMesh_m475879085 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Matrix4x4_t1651859333  ___matrix1, Material_t3870600107 * ___material2, int32_t ___layer3, Camera_t2727095145 * ___camera4, int32_t ___submeshIndex5, MaterialPropertyBlock_t1322387783 * ___properties6, int32_t ___castShadows7, bool ___receiveShadows8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMesh(UnityEngine.Mesh,UnityEngine.Matrix4x4,UnityEngine.Material,System.Int32,UnityEngine.Camera,System.Int32,UnityEngine.MaterialPropertyBlock,UnityEngine.Rendering.ShadowCastingMode)
extern "C"  void Graphics_DrawMesh_m448987856 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Matrix4x4_t1651859333  ___matrix1, Material_t3870600107 * ___material2, int32_t ___layer3, Camera_t2727095145 * ___camera4, int32_t ___submeshIndex5, MaterialPropertyBlock_t1322387783 * ___properties6, int32_t ___castShadows7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMesh(UnityEngine.Mesh,UnityEngine.Matrix4x4,UnityEngine.Material,System.Int32,UnityEngine.Camera,System.Int32,UnityEngine.MaterialPropertyBlock,UnityEngine.Rendering.ShadowCastingMode,System.Boolean,UnityEngine.Transform)
extern "C"  void Graphics_DrawMesh_m1877828964 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Matrix4x4_t1651859333  ___matrix1, Material_t3870600107 * ___material2, int32_t ___layer3, Camera_t2727095145 * ___camera4, int32_t ___submeshIndex5, MaterialPropertyBlock_t1322387783 * ___properties6, int32_t ___castShadows7, bool ___receiveShadows8, Transform_t1659122786 * ___probeAnchor9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Internal_DrawMeshTR(UnityEngine.Internal_DrawMeshTRArguments&,UnityEngine.MaterialPropertyBlock,UnityEngine.Material,UnityEngine.Mesh,UnityEngine.Camera)
extern "C"  void Graphics_Internal_DrawMeshTR_m2432487527 (Il2CppObject * __this /* static, unused */, Internal_DrawMeshTRArguments_t1240439529 * ___arguments0, MaterialPropertyBlock_t1322387783 * ___properties1, Material_t3870600107 * ___material2, Mesh_t4241756145 * ___mesh3, Camera_t2727095145 * ___camera4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Internal_DrawMeshMatrix(UnityEngine.Internal_DrawMeshMatrixArguments&,UnityEngine.MaterialPropertyBlock,UnityEngine.Material,UnityEngine.Mesh,UnityEngine.Camera)
extern "C"  void Graphics_Internal_DrawMeshMatrix_m3555155501 (Il2CppObject * __this /* static, unused */, Internal_DrawMeshMatrixArguments_t1378504422 * ___arguments0, MaterialPropertyBlock_t1322387783 * ___properties1, Material_t3870600107 * ___material2, Mesh_t4241756145 * ___mesh3, Camera_t2727095145 * ___camera4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMeshNow(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  void Graphics_DrawMeshNow_m872452541 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMeshNow(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32)
extern "C"  void Graphics_DrawMeshNow_m2601780794 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, int32_t ___materialIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMeshNow(UnityEngine.Mesh,UnityEngine.Matrix4x4)
extern "C"  void Graphics_DrawMeshNow_m2524242293 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Matrix4x4_t1651859333  ___matrix1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawMeshNow(UnityEngine.Mesh,UnityEngine.Matrix4x4,System.Int32)
extern "C"  void Graphics_DrawMeshNow_m2646671746 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Matrix4x4_t1651859333  ___matrix1, int32_t ___materialIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Internal_DrawMeshNow1(UnityEngine.Mesh,UnityEngine.Vector3,UnityEngine.Quaternion,System.Int32)
extern "C"  void Graphics_Internal_DrawMeshNow1_m44339171 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Vector3_t4282066566  ___position1, Quaternion_t1553702882  ___rotation2, int32_t ___materialIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::INTERNAL_CALL_Internal_DrawMeshNow1(UnityEngine.Mesh,UnityEngine.Vector3&,UnityEngine.Quaternion&,System.Int32)
extern "C"  void Graphics_INTERNAL_CALL_Internal_DrawMeshNow1_m2485926446 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Vector3_t4282066566 * ___position1, Quaternion_t1553702882 * ___rotation2, int32_t ___materialIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Internal_DrawMeshNow2(UnityEngine.Mesh,UnityEngine.Matrix4x4,System.Int32)
extern "C"  void Graphics_Internal_DrawMeshNow2_m1490231000 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Matrix4x4_t1651859333  ___matrix1, int32_t ___materialIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::INTERNAL_CALL_Internal_DrawMeshNow2(UnityEngine.Mesh,UnityEngine.Matrix4x4&,System.Int32)
extern "C"  void Graphics_INTERNAL_CALL_Internal_DrawMeshNow2_m3606840651 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mesh0, Matrix4x4_t1651859333 * ___matrix1, int32_t ___materialIndex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawProcedural(UnityEngine.MeshTopology,System.Int32,System.Int32)
extern "C"  void Graphics_DrawProcedural_m3792820144 (Il2CppObject * __this /* static, unused */, int32_t ___topology0, int32_t ___vertexCount1, int32_t ___instanceCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawProcedural(UnityEngine.MeshTopology,System.Int32)
extern "C"  void Graphics_DrawProcedural_m1205704839 (Il2CppObject * __this /* static, unused */, int32_t ___topology0, int32_t ___vertexCount1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawProceduralIndirect(UnityEngine.MeshTopology,UnityEngine.ComputeBuffer,System.Int32)
extern "C"  void Graphics_DrawProceduralIndirect_m2816657261 (Il2CppObject * __this /* static, unused */, int32_t ___topology0, ComputeBuffer_t37359565 * ___bufferWithArgs1, int32_t ___argsOffset2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawProceduralIndirect(UnityEngine.MeshTopology,UnityEngine.ComputeBuffer)
extern "C"  void Graphics_DrawProceduralIndirect_m3127957930 (Il2CppObject * __this /* static, unused */, int32_t ___topology0, ComputeBuffer_t37359565 * ___bufferWithArgs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.Rect,UnityEngine.Texture)
extern "C"  void Graphics_DrawTexture_m584866180 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___screenRect0, Texture_t2526458961 * ___texture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.Material)
extern "C"  void Graphics_DrawTexture_m3741603172 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___screenRect0, Texture_t2526458961 * ___texture1, Material_t3870600107 * ___mat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void Graphics_DrawTexture_m3578729028 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___screenRect0, Texture_t2526458961 * ___texture1, int32_t ___leftBorder2, int32_t ___rightBorder3, int32_t ___topBorder4, int32_t ___bottomBorder5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Material)
extern "C"  void Graphics_DrawTexture_m2319488164 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___screenRect0, Texture_t2526458961 * ___texture1, int32_t ___leftBorder2, int32_t ___rightBorder3, int32_t ___topBorder4, int32_t ___bottomBorder5, Material_t3870600107 * ___mat6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.Rect,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void Graphics_DrawTexture_m3040507527 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___screenRect0, Texture_t2526458961 * ___texture1, Rect_t4241904616  ___sourceRect2, int32_t ___leftBorder3, int32_t ___rightBorder4, int32_t ___topBorder5, int32_t ___bottomBorder6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.Rect,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Material)
extern "C"  void Graphics_DrawTexture_m3441133633 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___screenRect0, Texture_t2526458961 * ___texture1, Rect_t4241904616  ___sourceRect2, int32_t ___leftBorder3, int32_t ___rightBorder4, int32_t ___topBorder5, int32_t ___bottomBorder6, Material_t3870600107 * ___mat7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.Rect,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color)
extern "C"  void Graphics_DrawTexture_m1244490023 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___screenRect0, Texture_t2526458961 * ___texture1, Rect_t4241904616  ___sourceRect2, int32_t ___leftBorder3, int32_t ___rightBorder4, int32_t ___topBorder5, int32_t ___bottomBorder6, Color_t4194546905  ___color7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.Rect,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color,UnityEngine.Material)
extern "C"  void Graphics_DrawTexture_m2707978657 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___screenRect0, Texture_t2526458961 * ___texture1, Rect_t4241904616  ___sourceRect2, int32_t ___leftBorder3, int32_t ___rightBorder4, int32_t ___topBorder5, int32_t ___bottomBorder6, Color_t4194546905  ___color7, Material_t3870600107 * ___mat8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.InternalDrawTextureArguments&)
extern "C"  void Graphics_DrawTexture_m2938050632 (Il2CppObject * __this /* static, unused */, InternalDrawTextureArguments_t4047764288 * ___arguments0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::ExecuteCommandBuffer(UnityEngine.Rendering.CommandBuffer)
extern "C"  void Graphics_ExecuteCommandBuffer_m2257396648 (Il2CppObject * __this /* static, unused */, CommandBuffer_t1654378665 * ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture)
extern "C"  void Graphics_Blit_m3408836917 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___source0, RenderTexture_t1963041563 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material)
extern "C"  void Graphics_Blit_m2695454291 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___source0, RenderTexture_t1963041563 * ___dest1, Material_t3870600107 * ___mat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32)
extern "C"  void Graphics_Blit_m336256356 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___source0, RenderTexture_t1963041563 * ___dest1, Material_t3870600107 * ___mat2, int32_t ___pass3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.Material)
extern "C"  void Graphics_Blit_m2458820145 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___source0, Material_t3870600107 * ___mat1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.Material,System.Int32)
extern "C"  void Graphics_Blit_m2436421190 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___source0, Material_t3870600107 * ___mat1, int32_t ___pass2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Internal_BlitMaterial(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32,System.Boolean)
extern "C"  void Graphics_Internal_BlitMaterial_m2813555808 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___source0, RenderTexture_t1963041563 * ___dest1, Material_t3870600107 * ___mat2, int32_t ___pass3, bool ___setRT4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::BlitMultiTap(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Vector2[])
extern "C"  void Graphics_BlitMultiTap_m4211810031 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___source0, RenderTexture_t1963041563 * ___dest1, Material_t3870600107 * ___mat2, Vector2U5BU5D_t4024180168* ___offsets3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Internal_BlitMultiTap(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,UnityEngine.Vector2[])
extern "C"  void Graphics_Internal_BlitMultiTap_m1682166383 (Il2CppObject * __this /* static, unused */, Texture_t2526458961 * ___source0, RenderTexture_t1963041563 * ___dest1, Material_t3870600107 * ___mat2, Vector2U5BU5D_t4024180168* ___offsets3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Internal_SetNullRT()
extern "C"  void Graphics_Internal_SetNullRT_m2775863211 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Internal_SetRTSimple(UnityEngine.RenderBuffer&,UnityEngine.RenderBuffer&,System.Int32,UnityEngine.CubemapFace)
extern "C"  void Graphics_Internal_SetRTSimple_m3984063584 (Il2CppObject * __this /* static, unused */, RenderBuffer_t3529837690 * ___color0, RenderBuffer_t3529837690 * ___depth1, int32_t ___mip2, int32_t ___face3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Internal_SetMRTFullSetup(UnityEngine.RenderBuffer[],UnityEngine.RenderBuffer&,System.Int32,UnityEngine.CubemapFace,UnityEngine.Rendering.RenderBufferLoadAction[],UnityEngine.Rendering.RenderBufferStoreAction[],UnityEngine.Rendering.RenderBufferLoadAction,UnityEngine.Rendering.RenderBufferStoreAction)
extern "C"  void Graphics_Internal_SetMRTFullSetup_m4051125909 (Il2CppObject * __this /* static, unused */, RenderBufferU5BU5D_t1970987103* ___colorSA0, RenderBuffer_t3529837690 * ___depth1, int32_t ___mip2, int32_t ___face3, RenderBufferLoadActionU5BU5D_t3840699671* ___colorLoadSA4, RenderBufferStoreActionU5BU5D_t2354850566* ___colorStoreSA5, int32_t ___depthLoad6, int32_t ___depthStore7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Internal_SetMRTSimple(UnityEngine.RenderBuffer[],UnityEngine.RenderBuffer&,System.Int32,UnityEngine.CubemapFace)
extern "C"  void Graphics_Internal_SetMRTSimple_m3011847185 (Il2CppObject * __this /* static, unused */, RenderBufferU5BU5D_t1970987103* ___colorSA0, RenderBuffer_t3529837690 * ___depth1, int32_t ___mip2, int32_t ___face3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderBuffer UnityEngine.Graphics::get_activeColorBuffer()
extern "C"  RenderBuffer_t3529837690  Graphics_get_activeColorBuffer_m413627794 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderBuffer UnityEngine.Graphics::get_activeDepthBuffer()
extern "C"  RenderBuffer_t3529837690  Graphics_get_activeDepthBuffer_m143634674 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::GetActiveColorBuffer(UnityEngine.RenderBuffer&)
extern "C"  void Graphics_GetActiveColorBuffer_m1183461872 (Il2CppObject * __this /* static, unused */, RenderBuffer_t3529837690 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::GetActiveDepthBuffer(UnityEngine.RenderBuffer&)
extern "C"  void Graphics_GetActiveDepthBuffer_m1397326480 (Il2CppObject * __this /* static, unused */, RenderBuffer_t3529837690 * ___res0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::SetRandomWriteTarget(System.Int32,UnityEngine.RenderTexture)
extern "C"  void Graphics_SetRandomWriteTarget_m2353092604 (Il2CppObject * __this /* static, unused */, int32_t ___index0, RenderTexture_t1963041563 * ___uav1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::SetRandomWriteTarget(System.Int32,UnityEngine.ComputeBuffer)
extern "C"  void Graphics_SetRandomWriteTarget_m2786492810 (Il2CppObject * __this /* static, unused */, int32_t ___index0, ComputeBuffer_t37359565 * ___uav1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::ClearRandomWriteTargets()
extern "C"  void Graphics_ClearRandomWriteTargets_m223572883 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Internal_SetRandomWriteTargetRT(System.Int32,UnityEngine.RenderTexture)
extern "C"  void Graphics_Internal_SetRandomWriteTargetRT_m3885680830 (Il2CppObject * __this /* static, unused */, int32_t ___index0, RenderTexture_t1963041563 * ___uav1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::Internal_SetRandomWriteTargetBuffer(System.Int32,UnityEngine.ComputeBuffer)
extern "C"  void Graphics_Internal_SetRandomWriteTargetBuffer_m3862178922 (Il2CppObject * __this /* static, unused */, int32_t ___index0, ComputeBuffer_t37359565 * ___uav1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::CheckLoadActionValid(UnityEngine.Rendering.RenderBufferLoadAction,System.String)
extern "C"  void Graphics_CheckLoadActionValid_m3502649419 (Il2CppObject * __this /* static, unused */, int32_t ___load0, String_t* ___bufferType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::CheckStoreActionValid(UnityEngine.Rendering.RenderBufferStoreAction,System.String)
extern "C"  void Graphics_CheckStoreActionValid_m3656144377 (Il2CppObject * __this /* static, unused */, int32_t ___store0, String_t* ___bufferType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::SetRenderTargetImpl(UnityEngine.RenderTargetSetup)
extern "C"  void Graphics_SetRenderTargetImpl_m1813215914 (Il2CppObject * __this /* static, unused */, RenderTargetSetup_t2400315372  ___setup0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::SetRenderTargetImpl(UnityEngine.RenderBuffer,UnityEngine.RenderBuffer,System.Int32,UnityEngine.CubemapFace)
extern "C"  void Graphics_SetRenderTargetImpl_m1430595897 (Il2CppObject * __this /* static, unused */, RenderBuffer_t3529837690  ___colorBuffer0, RenderBuffer_t3529837690  ___depthBuffer1, int32_t ___mipLevel2, int32_t ___face3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::SetRenderTargetImpl(UnityEngine.RenderTexture,System.Int32,UnityEngine.CubemapFace)
extern "C"  void Graphics_SetRenderTargetImpl_m2407939515 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___rt0, int32_t ___mipLevel1, int32_t ___face2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::SetRenderTargetImpl(UnityEngine.RenderBuffer[],UnityEngine.RenderBuffer,System.Int32,UnityEngine.CubemapFace)
extern "C"  void Graphics_SetRenderTargetImpl_m2181171415 (Il2CppObject * __this /* static, unused */, RenderBufferU5BU5D_t1970987103* ___colorBuffers0, RenderBuffer_t3529837690  ___depthBuffer1, int32_t ___mipLevel2, int32_t ___face3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::SetRenderTarget(UnityEngine.RenderTexture)
extern "C"  void Graphics_SetRenderTarget_m3051614107 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___rt0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::SetRenderTarget(UnityEngine.RenderTexture,System.Int32)
extern "C"  void Graphics_SetRenderTarget_m4070875420 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___rt0, int32_t ___mipLevel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::SetRenderTarget(UnityEngine.RenderTexture,System.Int32,UnityEngine.CubemapFace)
extern "C"  void Graphics_SetRenderTarget_m602274683 (Il2CppObject * __this /* static, unused */, RenderTexture_t1963041563 * ___rt0, int32_t ___mipLevel1, int32_t ___face2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::SetRenderTarget(UnityEngine.RenderBuffer,UnityEngine.RenderBuffer)
extern "C"  void Graphics_SetRenderTarget_m1691358557 (Il2CppObject * __this /* static, unused */, RenderBuffer_t3529837690  ___colorBuffer0, RenderBuffer_t3529837690  ___depthBuffer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::SetRenderTarget(UnityEngine.RenderBuffer,UnityEngine.RenderBuffer,System.Int32)
extern "C"  void Graphics_SetRenderTarget_m460285594 (Il2CppObject * __this /* static, unused */, RenderBuffer_t3529837690  ___colorBuffer0, RenderBuffer_t3529837690  ___depthBuffer1, int32_t ___mipLevel2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::SetRenderTarget(UnityEngine.RenderBuffer,UnityEngine.RenderBuffer,System.Int32,UnityEngine.CubemapFace)
extern "C"  void Graphics_SetRenderTarget_m1236772601 (Il2CppObject * __this /* static, unused */, RenderBuffer_t3529837690  ___colorBuffer0, RenderBuffer_t3529837690  ___depthBuffer1, int32_t ___mipLevel2, int32_t ___face3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::SetRenderTarget(UnityEngine.RenderBuffer[],UnityEngine.RenderBuffer)
extern "C"  void Graphics_SetRenderTarget_m2335105791 (Il2CppObject * __this /* static, unused */, RenderBufferU5BU5D_t1970987103* ___colorBuffers0, RenderBuffer_t3529837690  ___depthBuffer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Graphics::SetRenderTarget(UnityEngine.RenderTargetSetup)
extern "C"  void Graphics_SetRenderTarget_m2023621866 (Il2CppObject * __this /* static, unused */, RenderTargetSetup_t2400315372  ___setup0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
