﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Collision2DGenerated
struct UnityEngine_Collision2DGenerated_t4071397971;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine_Collision2DGenerated::.ctor()
extern "C"  void UnityEngine_Collision2DGenerated__ctor_m3958786472 (UnityEngine_Collision2DGenerated_t4071397971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Collision2DGenerated::Collision2D_Collision2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Collision2DGenerated_Collision2D_Collision2D1_m3731314568 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Collision2DGenerated::Collision2D_enabled(JSVCall)
extern "C"  void UnityEngine_Collision2DGenerated_Collision2D_enabled_m824119451 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Collision2DGenerated::Collision2D_rigidbody(JSVCall)
extern "C"  void UnityEngine_Collision2DGenerated_Collision2D_rigidbody_m950789519 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Collision2DGenerated::Collision2D_collider(JSVCall)
extern "C"  void UnityEngine_Collision2DGenerated_Collision2D_collider_m1762236828 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Collision2DGenerated::Collision2D_transform(JSVCall)
extern "C"  void UnityEngine_Collision2DGenerated_Collision2D_transform_m3066149712 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Collision2DGenerated::Collision2D_gameObject(JSVCall)
extern "C"  void UnityEngine_Collision2DGenerated_Collision2D_gameObject_m569942495 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Collision2DGenerated::Collision2D_contacts(JSVCall)
extern "C"  void UnityEngine_Collision2DGenerated_Collision2D_contacts_m1570032669 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Collision2DGenerated::Collision2D_relativeVelocity(JSVCall)
extern "C"  void UnityEngine_Collision2DGenerated_Collision2D_relativeVelocity_m3617650535 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Collision2DGenerated::__Register()
extern "C"  void UnityEngine_Collision2DGenerated___Register_m870251551 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Collision2DGenerated::ilo_setBooleanS1(System.Int32,System.Boolean)
extern "C"  void UnityEngine_Collision2DGenerated_ilo_setBooleanS1_m3270061593 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Collision2DGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_Collision2DGenerated_ilo_setObject2_m3757157411 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Collision2DGenerated::ilo_setVector2S3(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_Collision2DGenerated_ilo_setVector2S3_m797499265 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
