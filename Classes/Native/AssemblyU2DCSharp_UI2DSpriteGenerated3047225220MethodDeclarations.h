﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI2DSpriteGenerated
struct UI2DSpriteGenerated_t3047225220;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// UI2DSprite
struct UI2DSprite_t1326097995;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.Shader
struct Shader_t3191267369;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_UI2DSprite1326097995.h"
#include "UnityEngine_UnityEngine_Sprite3199167241.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Shader3191267369.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"

// System.Void UI2DSpriteGenerated::.ctor()
extern "C"  void UI2DSpriteGenerated__ctor_m310464615 (UI2DSpriteGenerated_t3047225220 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UI2DSpriteGenerated::UI2DSprite_UI2DSprite1(JSVCall,System.Int32)
extern "C"  bool UI2DSpriteGenerated_UI2DSprite_UI2DSprite1_m4152312971 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteGenerated::UI2DSprite_nextSprite(JSVCall)
extern "C"  void UI2DSpriteGenerated_UI2DSprite_nextSprite_m3957821398 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteGenerated::UI2DSprite_sprite2D(JSVCall)
extern "C"  void UI2DSpriteGenerated_UI2DSprite_sprite2D_m3582217335 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteGenerated::UI2DSprite_material(JSVCall)
extern "C"  void UI2DSpriteGenerated_UI2DSprite_material_m2842938663 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteGenerated::UI2DSprite_shader(JSVCall)
extern "C"  void UI2DSpriteGenerated_UI2DSprite_shader_m524159529 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteGenerated::UI2DSprite_mainTexture(JSVCall)
extern "C"  void UI2DSpriteGenerated_UI2DSprite_mainTexture_m2399857852 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteGenerated::UI2DSprite_premultipliedAlpha(JSVCall)
extern "C"  void UI2DSpriteGenerated_UI2DSprite_premultipliedAlpha_m3553745670 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteGenerated::UI2DSprite_pixelSize(JSVCall)
extern "C"  void UI2DSpriteGenerated_UI2DSprite_pixelSize_m3547847127 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteGenerated::UI2DSprite_drawingDimensions(JSVCall)
extern "C"  void UI2DSpriteGenerated_UI2DSprite_drawingDimensions_m738390899 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteGenerated::UI2DSprite_border(JSVCall)
extern "C"  void UI2DSpriteGenerated_UI2DSprite_border_m728755650 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UI2DSpriteGenerated::UI2DSprite_MakePixelPerfect(JSVCall,System.Int32)
extern "C"  bool UI2DSpriteGenerated_UI2DSprite_MakePixelPerfect_m3787142898 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UI2DSpriteGenerated::UI2DSprite_OnFill__BetterListT1_Vector3__BetterListT1_Vector2__BetterListT1_Color32(JSVCall,System.Int32)
extern "C"  bool UI2DSpriteGenerated_UI2DSprite_OnFill__BetterListT1_Vector3__BetterListT1_Vector2__BetterListT1_Color32_m519909412 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteGenerated::__Register()
extern "C"  void UI2DSpriteGenerated___Register_m4013249728 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UI2DSpriteGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UI2DSpriteGenerated_ilo_setObject1_m927825575 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteGenerated::ilo_set_sprite2D2(UI2DSprite,UnityEngine.Sprite)
extern "C"  void UI2DSpriteGenerated_ilo_set_sprite2D2_m2555472997 (Il2CppObject * __this /* static, unused */, UI2DSprite_t1326097995 * ____this0, Sprite_t3199167241 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UI2DSpriteGenerated::ilo_get_material3(UI2DSprite)
extern "C"  Material_t3870600107 * UI2DSpriteGenerated_ilo_get_material3_m388523657 (Il2CppObject * __this /* static, unused */, UI2DSprite_t1326097995 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteGenerated::ilo_set_material4(UI2DSprite,UnityEngine.Material)
extern "C"  void UI2DSpriteGenerated_ilo_set_material4_m753168273 (Il2CppObject * __this /* static, unused */, UI2DSprite_t1326097995 * ____this0, Material_t3870600107 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Shader UI2DSpriteGenerated::ilo_get_shader5(UI2DSprite)
extern "C"  Shader_t3191267369 * UI2DSpriteGenerated_ilo_get_shader5_m1434322895 (Il2CppObject * __this /* static, unused */, UI2DSprite_t1326097995 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteGenerated::ilo_set_shader6(UI2DSprite,UnityEngine.Shader)
extern "C"  void UI2DSpriteGenerated_ilo_set_shader6_m1552658895 (Il2CppObject * __this /* static, unused */, UI2DSprite_t1326097995 * ____this0, Shader_t3191267369 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteGenerated::ilo_setBooleanS7(System.Int32,System.Boolean)
extern "C"  void UI2DSpriteGenerated_ilo_setBooleanS7_m2864984914 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UI2DSpriteGenerated::ilo_get_pixelSize8(UI2DSprite)
extern "C"  float UI2DSpriteGenerated_ilo_get_pixelSize8_m3790918675 (Il2CppObject * __this /* static, unused */, UI2DSprite_t1326097995 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UI2DSpriteGenerated::ilo_get_border9(UI2DSprite)
extern "C"  Vector4_t4282066567  UI2DSpriteGenerated_ilo_get_border9_m1014164580 (Il2CppObject * __this /* static, unused */, UI2DSprite_t1326097995 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI2DSpriteGenerated::ilo_MakePixelPerfect10(UI2DSprite)
extern "C"  void UI2DSpriteGenerated_ilo_MakePixelPerfect10_m2691055769 (Il2CppObject * __this /* static, unused */, UI2DSprite_t1326097995 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UI2DSpriteGenerated::ilo_getObject11(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UI2DSpriteGenerated_ilo_getObject11_m3984567011 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
