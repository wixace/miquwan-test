﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginXiaomi
struct PluginXiaomi_t2004955630;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// System.Object
struct Il2CppObject;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_FloatTextMgr630384591.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"

// System.Void PluginXiaomi::.ctor()
extern "C"  void PluginXiaomi__ctor_m475335789 (PluginXiaomi_t2004955630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaomi::Init()
extern "C"  void PluginXiaomi_Init_m1629438023 (PluginXiaomi_t2004955630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginXiaomi::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginXiaomi_ReqSDKHttpLogin_m666296450 (PluginXiaomi_t2004955630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXiaomi::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginXiaomi_IsLoginSuccess_m2779629638 (PluginXiaomi_t2004955630 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaomi::OpenUserLogin()
extern "C"  void PluginXiaomi_OpenUserLogin_m2271572415 (PluginXiaomi_t2004955630 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaomi::UserPay(CEvent.ZEvent)
extern "C"  void PluginXiaomi_UserPay_m137198803 (PluginXiaomi_t2004955630 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaomi::OnInitSuccess(System.String)
extern "C"  void PluginXiaomi_OnInitSuccess_m3670569315 (PluginXiaomi_t2004955630 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaomi::OnInitFail(System.String)
extern "C"  void PluginXiaomi_OnInitFail_m1732915902 (PluginXiaomi_t2004955630 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaomi::OnLoginSuccess(System.String)
extern "C"  void PluginXiaomi_OnLoginSuccess_m2508211122 (PluginXiaomi_t2004955630 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaomi::OnLoginFail(System.String)
extern "C"  void PluginXiaomi_OnLoginFail_m810621647 (PluginXiaomi_t2004955630 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaomi::OnPaySuccess(System.String)
extern "C"  void PluginXiaomi_OnPaySuccess_m3869764209 (PluginXiaomi_t2004955630 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaomi::OnPayFail(System.String)
extern "C"  void PluginXiaomi_OnPayFail_m3453879408 (PluginXiaomi_t2004955630 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaomi::OnPayCancel(System.String)
extern "C"  void PluginXiaomi_OnPayCancel_m1312248404 (PluginXiaomi_t2004955630 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaomi::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginXiaomi_ilo_AddEventListener1_m3285977831 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaomi::ilo_Log2(System.Object,System.Boolean)
extern "C"  void PluginXiaomi_ilo_Log2_m4215526785 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginXiaomi::ilo_get_isSdkLogin3(VersionMgr)
extern "C"  bool PluginXiaomi_ilo_get_isSdkLogin3_m3808573173 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginXiaomi::ilo_get_PluginsSdkMgr4()
extern "C"  PluginsSdkMgr_t3884624670 * PluginXiaomi_ilo_get_PluginsSdkMgr4_m3826741022 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginXiaomi::ilo_get_DeviceID5(PluginsSdkMgr)
extern "C"  String_t* PluginXiaomi_ilo_get_DeviceID5_m3595785764 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginXiaomi::ilo_get_Instance6()
extern "C"  VersionMgr_t1322950208 * PluginXiaomi_ilo_get_Instance6_m3349465331 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaomi::ilo_ReqSDKHttpLogin7(PluginsSdkMgr)
extern "C"  void PluginXiaomi_ilo_ReqSDKHttpLogin7_m3414744376 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr PluginXiaomi::ilo_get_FloatTextMgr8()
extern "C"  FloatTextMgr_t630384591 * PluginXiaomi_ilo_get_FloatTextMgr8_m1723711018 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaomi::ilo_DispatchEvent9(CEvent.ZEvent)
extern "C"  void PluginXiaomi_ilo_DispatchEvent9_m461983928 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginXiaomi::ilo_FloatText10(FloatTextMgr,FLOAT_TEXT_ID,System.Int32,System.Object[])
extern "C"  void PluginXiaomi_ilo_FloatText10_m1250258256 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, int32_t ___textId1, int32_t ___offsetY2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
