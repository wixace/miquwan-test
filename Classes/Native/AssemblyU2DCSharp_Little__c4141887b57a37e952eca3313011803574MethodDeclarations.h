﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._c4141887b57a37e952eca331d746dfd0
struct _c4141887b57a37e952eca331d746dfd0_t3011803574;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__c4141887b57a37e952eca3313011803574.h"

// System.Void Little._c4141887b57a37e952eca331d746dfd0::.ctor()
extern "C"  void _c4141887b57a37e952eca331d746dfd0__ctor_m527251767 (_c4141887b57a37e952eca331d746dfd0_t3011803574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c4141887b57a37e952eca331d746dfd0::_c4141887b57a37e952eca331d746dfd0m2(System.Int32)
extern "C"  int32_t _c4141887b57a37e952eca331d746dfd0__c4141887b57a37e952eca331d746dfd0m2_m1973814745 (_c4141887b57a37e952eca331d746dfd0_t3011803574 * __this, int32_t ____c4141887b57a37e952eca331d746dfd0a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c4141887b57a37e952eca331d746dfd0::_c4141887b57a37e952eca331d746dfd0m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _c4141887b57a37e952eca331d746dfd0__c4141887b57a37e952eca331d746dfd0m_m619571453 (_c4141887b57a37e952eca331d746dfd0_t3011803574 * __this, int32_t ____c4141887b57a37e952eca331d746dfd0a0, int32_t ____c4141887b57a37e952eca331d746dfd081, int32_t ____c4141887b57a37e952eca331d746dfd0c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._c4141887b57a37e952eca331d746dfd0::ilo__c4141887b57a37e952eca331d746dfd0m21(Little._c4141887b57a37e952eca331d746dfd0,System.Int32)
extern "C"  int32_t _c4141887b57a37e952eca331d746dfd0_ilo__c4141887b57a37e952eca331d746dfd0m21_m2672064573 (Il2CppObject * __this /* static, unused */, _c4141887b57a37e952eca331d746dfd0_t3011803574 * ____this0, int32_t ____c4141887b57a37e952eca331d746dfd0a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
