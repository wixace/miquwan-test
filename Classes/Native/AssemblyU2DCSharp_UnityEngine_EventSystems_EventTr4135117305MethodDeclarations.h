﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_EventTrigger_TriggerEventGenerated
struct UnityEngine_EventSystems_EventTrigger_TriggerEventGenerated_t4135117305;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_EventSystems_EventTrigger_TriggerEventGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_EventTrigger_TriggerEventGenerated__ctor_m1943425426 (UnityEngine_EventSystems_EventTrigger_TriggerEventGenerated_t4135117305 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventTrigger_TriggerEventGenerated::TriggerEvent_TriggerEvent1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventTrigger_TriggerEventGenerated_TriggerEvent_TriggerEvent1_m2596983648 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_EventTrigger_TriggerEventGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_EventTrigger_TriggerEventGenerated___Register_m423188085 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_EventTrigger_TriggerEventGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_EventSystems_EventTrigger_TriggerEventGenerated_ilo_attachFinalizerObject1_m2006977701 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_EventTrigger_TriggerEventGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_EventSystems_EventTrigger_TriggerEventGenerated_ilo_addJSCSRel2_m2270686159 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
