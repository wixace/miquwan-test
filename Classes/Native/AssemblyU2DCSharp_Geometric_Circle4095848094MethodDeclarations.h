﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Geometric/Circle
struct Circle_t4095848094;
struct Circle_t4095848094_marshaled_pinvoke;
struct Circle_t4095848094_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Geometric_Circle4095848094.h"

// System.Void Geometric/Circle::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Circle__ctor_m1385304856 (Circle_t4095848094 * __this, float ____x0, float ____y1, float ____r2, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Circle_t4095848094;
struct Circle_t4095848094_marshaled_pinvoke;

extern "C" void Circle_t4095848094_marshal_pinvoke(const Circle_t4095848094& unmarshaled, Circle_t4095848094_marshaled_pinvoke& marshaled);
extern "C" void Circle_t4095848094_marshal_pinvoke_back(const Circle_t4095848094_marshaled_pinvoke& marshaled, Circle_t4095848094& unmarshaled);
extern "C" void Circle_t4095848094_marshal_pinvoke_cleanup(Circle_t4095848094_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Circle_t4095848094;
struct Circle_t4095848094_marshaled_com;

extern "C" void Circle_t4095848094_marshal_com(const Circle_t4095848094& unmarshaled, Circle_t4095848094_marshaled_com& marshaled);
extern "C" void Circle_t4095848094_marshal_com_back(const Circle_t4095848094_marshaled_com& marshaled, Circle_t4095848094& unmarshaled);
extern "C" void Circle_t4095848094_marshal_com_cleanup(Circle_t4095848094_marshaled_com& marshaled);
