﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_equip_streng_master
struct Float_equip_streng_master_t3807538562;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String
struct String_t;
// FloatTextUnit
struct FloatTextUnit_t2362298029;
// UIEventListener
struct UIEventListener_t1278105402;
// UITweener
struct UITweener_t105489188;
// UILabel
struct UILabel_t291504320;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "AssemblyU2DCSharp_UITweener105489188.h"
#include "AssemblyU2DCSharp_Float_equip_streng_master3807538562.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"

// System.Void Float_equip_streng_master::.ctor()
extern "C"  void Float_equip_streng_master__ctor_m3157781033 (Float_equip_streng_master_t3807538562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng_master::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_equip_streng_master_OnAwake_m795353829 (Float_equip_streng_master_t3807538562 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng_master::OnDestroy()
extern "C"  void Float_equip_streng_master_OnDestroy_m1011253922 (Float_equip_streng_master_t3807538562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_equip_streng_master::FloatID()
extern "C"  int32_t Float_equip_streng_master_FloatID_m548109223 (Float_equip_streng_master_t3807538562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng_master::Init()
extern "C"  void Float_equip_streng_master_Init_m1993063179 (Float_equip_streng_master_t3807538562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng_master::PlayEffect()
extern "C"  void Float_equip_streng_master_PlayEffect_m3355396896 (Float_equip_streng_master_t3807538562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng_master::OnClickClose(UnityEngine.GameObject)
extern "C"  void Float_equip_streng_master_OnClickClose_m690352322 (Float_equip_streng_master_t3807538562 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng_master::PlayMasterTween()
extern "C"  void Float_equip_streng_master_PlayMasterTween_m3123633116 (Float_equip_streng_master_t3807538562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng_master::SetFile(System.Object[])
extern "C"  void Float_equip_streng_master_SetFile_m3916639693 (Float_equip_streng_master_t3807538562 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng_master::SetLeftValue(System.String)
extern "C"  void Float_equip_streng_master_SetLeftValue_m1029116543 (Float_equip_streng_master_t3807538562 * __this, String_t* ___attStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng_master::SetRightValue(System.String)
extern "C"  void Float_equip_streng_master_SetRightValue_m3065939332 (Float_equip_streng_master_t3807538562 * __this, String_t* ___attStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng_master::SetAddValue(System.String)
extern "C"  void Float_equip_streng_master_SetAddValue_m3852219785 (Float_equip_streng_master_t3807538562 * __this, String_t* ___attStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng_master::<PlayMasterTween>m__3E0()
extern "C"  void Float_equip_streng_master_U3CPlayMasterTweenU3Em__3E0_m349928029 (Float_equip_streng_master_t3807538562 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng_master::ilo_OnAwake1(FloatTextUnit,UnityEngine.GameObject)
extern "C"  void Float_equip_streng_master_ilo_OnAwake1_m2465684082 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, GameObject_t3674682005 * ___viewGO1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener Float_equip_streng_master::ilo_Get2(UnityEngine.GameObject)
extern "C"  UIEventListener_t1278105402 * Float_equip_streng_master_ilo_Get2_m4144394185 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng_master::ilo_ResetToBeginning3(UITweener)
extern "C"  void Float_equip_streng_master_ilo_ResetToBeginning3_m952980762 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng_master::ilo_PlayForward4(UITweener)
extern "C"  void Float_equip_streng_master_ilo_PlayForward4_m4293293597 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Float_equip_streng_master::ilo_FormatUIString5(System.Int32,System.Object[])
extern "C"  String_t* Float_equip_streng_master_ilo_FormatUIString5_m3417819533 (Il2CppObject * __this /* static, unused */, int32_t ___key0, ObjectU5BU5D_t1108656482* ___datas1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng_master::ilo_SetLeftValue6(Float_equip_streng_master,System.String)
extern "C"  void Float_equip_streng_master_ilo_SetLeftValue6_m2282728170 (Il2CppObject * __this /* static, unused */, Float_equip_streng_master_t3807538562 * ____this0, String_t* ___attStr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng_master::ilo_set_text7(UILabel,System.String)
extern "C"  void Float_equip_streng_master_ilo_set_text7_m3621259885 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
