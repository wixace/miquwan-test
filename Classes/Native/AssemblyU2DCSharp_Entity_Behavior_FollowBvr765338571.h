﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.FollowBvr
struct  FollowBvr_t765338571  : public IBehavior_t770859129
{
public:
	// System.Single Entity.Behavior.FollowBvr::distance
	float ___distance_2;
	// UnityEngine.Vector3 Entity.Behavior.FollowBvr::targetpos
	Vector3_t4282066566  ___targetpos_3;
	// UnityEngine.Vector3 Entity.Behavior.FollowBvr::lastpos
	Vector3_t4282066566  ___lastpos_4;
	// System.Single Entity.Behavior.FollowBvr::curTime
	float ___curTime_5;
	// System.Single Entity.Behavior.FollowBvr::intervalTime
	float ___intervalTime_6;
	// System.Boolean Entity.Behavior.FollowBvr::isBox
	bool ___isBox_7;
	// System.Boolean Entity.Behavior.FollowBvr::isLockTarget
	bool ___isLockTarget_8;
	// System.Boolean Entity.Behavior.FollowBvr::newFrame
	bool ___newFrame_9;

public:
	inline static int32_t get_offset_of_distance_2() { return static_cast<int32_t>(offsetof(FollowBvr_t765338571, ___distance_2)); }
	inline float get_distance_2() const { return ___distance_2; }
	inline float* get_address_of_distance_2() { return &___distance_2; }
	inline void set_distance_2(float value)
	{
		___distance_2 = value;
	}

	inline static int32_t get_offset_of_targetpos_3() { return static_cast<int32_t>(offsetof(FollowBvr_t765338571, ___targetpos_3)); }
	inline Vector3_t4282066566  get_targetpos_3() const { return ___targetpos_3; }
	inline Vector3_t4282066566 * get_address_of_targetpos_3() { return &___targetpos_3; }
	inline void set_targetpos_3(Vector3_t4282066566  value)
	{
		___targetpos_3 = value;
	}

	inline static int32_t get_offset_of_lastpos_4() { return static_cast<int32_t>(offsetof(FollowBvr_t765338571, ___lastpos_4)); }
	inline Vector3_t4282066566  get_lastpos_4() const { return ___lastpos_4; }
	inline Vector3_t4282066566 * get_address_of_lastpos_4() { return &___lastpos_4; }
	inline void set_lastpos_4(Vector3_t4282066566  value)
	{
		___lastpos_4 = value;
	}

	inline static int32_t get_offset_of_curTime_5() { return static_cast<int32_t>(offsetof(FollowBvr_t765338571, ___curTime_5)); }
	inline float get_curTime_5() const { return ___curTime_5; }
	inline float* get_address_of_curTime_5() { return &___curTime_5; }
	inline void set_curTime_5(float value)
	{
		___curTime_5 = value;
	}

	inline static int32_t get_offset_of_intervalTime_6() { return static_cast<int32_t>(offsetof(FollowBvr_t765338571, ___intervalTime_6)); }
	inline float get_intervalTime_6() const { return ___intervalTime_6; }
	inline float* get_address_of_intervalTime_6() { return &___intervalTime_6; }
	inline void set_intervalTime_6(float value)
	{
		___intervalTime_6 = value;
	}

	inline static int32_t get_offset_of_isBox_7() { return static_cast<int32_t>(offsetof(FollowBvr_t765338571, ___isBox_7)); }
	inline bool get_isBox_7() const { return ___isBox_7; }
	inline bool* get_address_of_isBox_7() { return &___isBox_7; }
	inline void set_isBox_7(bool value)
	{
		___isBox_7 = value;
	}

	inline static int32_t get_offset_of_isLockTarget_8() { return static_cast<int32_t>(offsetof(FollowBvr_t765338571, ___isLockTarget_8)); }
	inline bool get_isLockTarget_8() const { return ___isLockTarget_8; }
	inline bool* get_address_of_isLockTarget_8() { return &___isLockTarget_8; }
	inline void set_isLockTarget_8(bool value)
	{
		___isLockTarget_8 = value;
	}

	inline static int32_t get_offset_of_newFrame_9() { return static_cast<int32_t>(offsetof(FollowBvr_t765338571, ___newFrame_9)); }
	inline bool get_newFrame_9() const { return ___newFrame_9; }
	inline bool* get_address_of_newFrame_9() { return &___newFrame_9; }
	inline void set_newFrame_9(bool value)
	{
		___newFrame_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
