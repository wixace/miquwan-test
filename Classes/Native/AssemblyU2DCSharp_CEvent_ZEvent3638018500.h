﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;

#include "AssemblyU2DCSharp_CEvent_EventBase210756865.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CEvent.ZEvent
struct  ZEvent_t3638018500  : public EventBase_t210756865
{
public:
	// System.Object[] CEvent.ZEvent::args
	ObjectU5BU5D_t1108656482* ___args_2;
	// System.Object CEvent.ZEvent::arg
	Il2CppObject * ___arg_3;
	// System.Object CEvent.ZEvent::arg1
	Il2CppObject * ___arg1_4;
	// System.Object CEvent.ZEvent::arg2
	Il2CppObject * ___arg2_5;

public:
	inline static int32_t get_offset_of_args_2() { return static_cast<int32_t>(offsetof(ZEvent_t3638018500, ___args_2)); }
	inline ObjectU5BU5D_t1108656482* get_args_2() const { return ___args_2; }
	inline ObjectU5BU5D_t1108656482** get_address_of_args_2() { return &___args_2; }
	inline void set_args_2(ObjectU5BU5D_t1108656482* value)
	{
		___args_2 = value;
		Il2CppCodeGenWriteBarrier(&___args_2, value);
	}

	inline static int32_t get_offset_of_arg_3() { return static_cast<int32_t>(offsetof(ZEvent_t3638018500, ___arg_3)); }
	inline Il2CppObject * get_arg_3() const { return ___arg_3; }
	inline Il2CppObject ** get_address_of_arg_3() { return &___arg_3; }
	inline void set_arg_3(Il2CppObject * value)
	{
		___arg_3 = value;
		Il2CppCodeGenWriteBarrier(&___arg_3, value);
	}

	inline static int32_t get_offset_of_arg1_4() { return static_cast<int32_t>(offsetof(ZEvent_t3638018500, ___arg1_4)); }
	inline Il2CppObject * get_arg1_4() const { return ___arg1_4; }
	inline Il2CppObject ** get_address_of_arg1_4() { return &___arg1_4; }
	inline void set_arg1_4(Il2CppObject * value)
	{
		___arg1_4 = value;
		Il2CppCodeGenWriteBarrier(&___arg1_4, value);
	}

	inline static int32_t get_offset_of_arg2_5() { return static_cast<int32_t>(offsetof(ZEvent_t3638018500, ___arg2_5)); }
	inline Il2CppObject * get_arg2_5() const { return ___arg2_5; }
	inline Il2CppObject ** get_address_of_arg2_5() { return &___arg2_5; }
	inline void set_arg2_5(Il2CppObject * value)
	{
		___arg2_5 = value;
		Il2CppCodeGenWriteBarrier(&___arg2_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
