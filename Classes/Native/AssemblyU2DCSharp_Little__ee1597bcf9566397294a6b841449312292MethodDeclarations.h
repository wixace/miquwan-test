﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._ee1597bcf9566397294a6b84e91413bf
struct _ee1597bcf9566397294a6b84e91413bf_t1449312292;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__ee1597bcf9566397294a6b841449312292.h"

// System.Void Little._ee1597bcf9566397294a6b84e91413bf::.ctor()
extern "C"  void _ee1597bcf9566397294a6b84e91413bf__ctor_m3805040265 (_ee1597bcf9566397294a6b84e91413bf_t1449312292 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._ee1597bcf9566397294a6b84e91413bf::_ee1597bcf9566397294a6b84e91413bfm2(System.Int32)
extern "C"  int32_t _ee1597bcf9566397294a6b84e91413bf__ee1597bcf9566397294a6b84e91413bfm2_m2715811609 (_ee1597bcf9566397294a6b84e91413bf_t1449312292 * __this, int32_t ____ee1597bcf9566397294a6b84e91413bfa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._ee1597bcf9566397294a6b84e91413bf::_ee1597bcf9566397294a6b84e91413bfm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _ee1597bcf9566397294a6b84e91413bf__ee1597bcf9566397294a6b84e91413bfm_m3540178877 (_ee1597bcf9566397294a6b84e91413bf_t1449312292 * __this, int32_t ____ee1597bcf9566397294a6b84e91413bfa0, int32_t ____ee1597bcf9566397294a6b84e91413bf471, int32_t ____ee1597bcf9566397294a6b84e91413bfc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._ee1597bcf9566397294a6b84e91413bf::ilo__ee1597bcf9566397294a6b84e91413bfm21(Little._ee1597bcf9566397294a6b84e91413bf,System.Int32)
extern "C"  int32_t _ee1597bcf9566397294a6b84e91413bf_ilo__ee1597bcf9566397294a6b84e91413bfm21_m1014355563 (Il2CppObject * __this /* static, unused */, _ee1597bcf9566397294a6b84e91413bf_t1449312292 * ____this0, int32_t ____ee1597bcf9566397294a6b84e91413bfa1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
