﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSHeroDataGenerated
struct CSHeroDataGenerated_t87397531;
// JSVCall
struct JSVCall_t3708497963;
// System.Collections.Generic.List`1<CSHeroUnit>
struct List_1_t837576702;
// CSHeroData
struct CSHeroData_t3763839828;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSHeroData3763839828.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void CSHeroDataGenerated::.ctor()
extern "C"  void CSHeroDataGenerated__ctor_m107380016 (CSHeroDataGenerated_t87397531 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSHeroDataGenerated::CSHeroData_CSHeroData1(JSVCall,System.Int32)
extern "C"  bool CSHeroDataGenerated_CSHeroData_CSHeroData1_m3429070082 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSHeroDataGenerated::CSHeroData_GetFightingHeroUntis__Int32(JSVCall,System.Int32)
extern "C"  bool CSHeroDataGenerated_CSHeroData_GetFightingHeroUntis__Int32_m3673863952 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSHeroDataGenerated::CSHeroData_GetFightingHeroUntis2(JSVCall,System.Int32)
extern "C"  bool CSHeroDataGenerated_CSHeroData_GetFightingHeroUntis2_m1010490996 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSHeroDataGenerated::CSHeroData_GetHeroInfos(JSVCall,System.Int32)
extern "C"  bool CSHeroDataGenerated_CSHeroData_GetHeroInfos_m3575476370 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSHeroDataGenerated::CSHeroData_GetHeroUnit__UInt32(JSVCall,System.Int32)
extern "C"  bool CSHeroDataGenerated_CSHeroData_GetHeroUnit__UInt32_m2754934450 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSHeroDataGenerated::CSHeroData_GetLeaderPlusAtt(JSVCall,System.Int32)
extern "C"  bool CSHeroDataGenerated_CSHeroData_GetLeaderPlusAtt_m3841760517 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSHeroDataGenerated::CSHeroData_GetLearerAttUnit__String(JSVCall,System.Int32)
extern "C"  bool CSHeroDataGenerated_CSHeroData_GetLearerAttUnit__String_m2550375206 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSHeroDataGenerated::CSHeroData_GetReinAttUnit__String(JSVCall,System.Int32)
extern "C"  bool CSHeroDataGenerated_CSHeroData_GetReinAttUnit__String_m1467862085 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSHeroDataGenerated::CSHeroData_GetReinPlusAtts(JSVCall,System.Int32)
extern "C"  bool CSHeroDataGenerated_CSHeroData_GetReinPlusAtts_m2862972831 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSHeroDataGenerated::CSHeroData_LoadData__String(JSVCall,System.Int32)
extern "C"  bool CSHeroDataGenerated_CSHeroData_LoadData__String_m2086191198 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSHeroDataGenerated::CSHeroData_UnLoadData(JSVCall,System.Int32)
extern "C"  bool CSHeroDataGenerated_CSHeroData_UnLoadData_m1421165830 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSHeroDataGenerated::__Register()
extern "C"  void CSHeroDataGenerated___Register_m246005143 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSHeroUnit> CSHeroDataGenerated::ilo_GetFightingHeroUntis1(CSHeroData,System.Int32)
extern "C"  List_1_t837576702 * CSHeroDataGenerated_ilo_GetFightingHeroUntis1_m584595265 (Il2CppObject * __this /* static, unused */, CSHeroData_t3763839828 * ____this0, int32_t ___fuctionType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSHeroUnit> CSHeroDataGenerated::ilo_GetHeroInfos2(CSHeroData)
extern "C"  List_1_t837576702 * CSHeroDataGenerated_ilo_GetHeroInfos2_m1517711941 (Il2CppObject * __this /* static, unused */, CSHeroData_t3763839828 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSHeroDataGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t CSHeroDataGenerated_ilo_setObject3_m2362870272 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSHeroDataGenerated::ilo_getStringS4(System.Int32)
extern "C"  String_t* CSHeroDataGenerated_ilo_getStringS4_m1247763607 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
