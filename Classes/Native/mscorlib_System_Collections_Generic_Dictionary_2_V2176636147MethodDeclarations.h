﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ProductsCfgMgr/ProductInfo>
struct ValueCollection_t2176636147;
// System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>
struct Dictionary_2_t3476030434;
// System.Collections.Generic.IEnumerator`1<ProductsCfgMgr/ProductInfo>
struct IEnumerator_1_t3217856287;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// ProductsCfgMgr/ProductInfo[]
struct ProductInfoU5BU5D_t182341603;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr_ProductInfo1305991238.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1407863842.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ProductsCfgMgr/ProductInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ValueCollection__ctor_m1696084746_gshared (ValueCollection_t2176636147 * __this, Dictionary_2_t3476030434 * ___dictionary0, const MethodInfo* method);
#define ValueCollection__ctor_m1696084746(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2176636147 *, Dictionary_2_t3476030434 *, const MethodInfo*))ValueCollection__ctor_m1696084746_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3387850728_gshared (ValueCollection_t2176636147 * __this, ProductInfo_t1305991238  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3387850728(__this, ___item0, method) ((  void (*) (ValueCollection_t2176636147 *, ProductInfo_t1305991238 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3387850728_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<TValue>.Clear()
extern "C"  void ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3962609841_gshared (ValueCollection_t2176636147 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3962609841(__this, method) ((  void (*) (ValueCollection_t2176636147 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3962609841_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2336892290_gshared (ValueCollection_t2176636147 * __this, ProductInfo_t1305991238  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2336892290(__this, ___item0, method) ((  bool (*) (ValueCollection_t2176636147 *, ProductInfo_t1305991238 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2336892290_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4262639271_gshared (ValueCollection_t2176636147 * __this, ProductInfo_t1305991238  ___item0, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4262639271(__this, ___item0, method) ((  bool (*) (ValueCollection_t2176636147 *, ProductInfo_t1305991238 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4262639271_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3988116145_gshared (ValueCollection_t2176636147 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3988116145(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2176636147 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3988116145_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ValueCollection_System_Collections_ICollection_CopyTo_m2654918773_gshared (ValueCollection_t2176636147 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_CopyTo_m2654918773(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2176636147 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2654918773_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4013782596_gshared (ValueCollection_t2176636147 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4013782596(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2176636147 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4013782596_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
extern "C"  bool ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m612068149_gshared (ValueCollection_t2176636147 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m612068149(__this, method) ((  bool (*) (ValueCollection_t2176636147 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m612068149_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3255376021_gshared (ValueCollection_t2176636147 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3255376021(__this, method) ((  bool (*) (ValueCollection_t2176636147 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3255376021_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ValueCollection_System_Collections_ICollection_get_SyncRoot_m3392405895_gshared (ValueCollection_t2176636147 * __this, const MethodInfo* method);
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3392405895(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2176636147 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m3392405895_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ProductsCfgMgr/ProductInfo>::CopyTo(TValue[],System.Int32)
extern "C"  void ValueCollection_CopyTo_m2638939345_gshared (ValueCollection_t2176636147 * __this, ProductInfoU5BU5D_t182341603* ___array0, int32_t ___index1, const MethodInfo* method);
#define ValueCollection_CopyTo_m2638939345(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2176636147 *, ProductInfoU5BU5D_t182341603*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2638939345_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ProductsCfgMgr/ProductInfo>::GetEnumerator()
extern "C"  Enumerator_t1407863842  ValueCollection_GetEnumerator_m4114035130_gshared (ValueCollection_t2176636147 * __this, const MethodInfo* method);
#define ValueCollection_GetEnumerator_m4114035130(__this, method) ((  Enumerator_t1407863842  (*) (ValueCollection_t2176636147 *, const MethodInfo*))ValueCollection_GetEnumerator_m4114035130_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,ProductsCfgMgr/ProductInfo>::get_Count()
extern "C"  int32_t ValueCollection_get_Count_m845657935_gshared (ValueCollection_t2176636147 * __this, const MethodInfo* method);
#define ValueCollection_get_Count_m845657935(__this, method) ((  int32_t (*) (ValueCollection_t2176636147 *, const MethodInfo*))ValueCollection_get_Count_m845657935_gshared)(__this, method)
