﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._1519195eb7ca7dc7ac84baf0853b209f
struct _1519195eb7ca7dc7ac84baf0853b209f_t1437772111;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._1519195eb7ca7dc7ac84baf0853b209f::.ctor()
extern "C"  void _1519195eb7ca7dc7ac84baf0853b209f__ctor_m2359854526 (_1519195eb7ca7dc7ac84baf0853b209f_t1437772111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1519195eb7ca7dc7ac84baf0853b209f::_1519195eb7ca7dc7ac84baf0853b209fm2(System.Int32)
extern "C"  int32_t _1519195eb7ca7dc7ac84baf0853b209f__1519195eb7ca7dc7ac84baf0853b209fm2_m1727633209 (_1519195eb7ca7dc7ac84baf0853b209f_t1437772111 * __this, int32_t ____1519195eb7ca7dc7ac84baf0853b209fa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1519195eb7ca7dc7ac84baf0853b209f::_1519195eb7ca7dc7ac84baf0853b209fm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _1519195eb7ca7dc7ac84baf0853b209f__1519195eb7ca7dc7ac84baf0853b209fm_m277783453 (_1519195eb7ca7dc7ac84baf0853b209f_t1437772111 * __this, int32_t ____1519195eb7ca7dc7ac84baf0853b209fa0, int32_t ____1519195eb7ca7dc7ac84baf0853b209f171, int32_t ____1519195eb7ca7dc7ac84baf0853b209fc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
