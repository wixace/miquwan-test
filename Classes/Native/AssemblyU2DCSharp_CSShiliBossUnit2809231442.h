﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSShiliBossUnit
struct  CSShiliBossUnit_t2809231442  : public Il2CppObject
{
public:
	// System.String CSShiliBossUnit::bossId
	String_t* ___bossId_0;
	// System.UInt32 CSShiliBossUnit::progress
	uint32_t ___progress_1;
	// System.UInt32 CSShiliBossUnit::hp
	uint32_t ___hp_2;
	// System.UInt32 CSShiliBossUnit::monsterID
	uint32_t ___monsterID_3;
	// System.UInt32 CSShiliBossUnit::maxHp
	uint32_t ___maxHp_4;
	// System.UInt32 CSShiliBossUnit::buffID
	uint32_t ___buffID_5;

public:
	inline static int32_t get_offset_of_bossId_0() { return static_cast<int32_t>(offsetof(CSShiliBossUnit_t2809231442, ___bossId_0)); }
	inline String_t* get_bossId_0() const { return ___bossId_0; }
	inline String_t** get_address_of_bossId_0() { return &___bossId_0; }
	inline void set_bossId_0(String_t* value)
	{
		___bossId_0 = value;
		Il2CppCodeGenWriteBarrier(&___bossId_0, value);
	}

	inline static int32_t get_offset_of_progress_1() { return static_cast<int32_t>(offsetof(CSShiliBossUnit_t2809231442, ___progress_1)); }
	inline uint32_t get_progress_1() const { return ___progress_1; }
	inline uint32_t* get_address_of_progress_1() { return &___progress_1; }
	inline void set_progress_1(uint32_t value)
	{
		___progress_1 = value;
	}

	inline static int32_t get_offset_of_hp_2() { return static_cast<int32_t>(offsetof(CSShiliBossUnit_t2809231442, ___hp_2)); }
	inline uint32_t get_hp_2() const { return ___hp_2; }
	inline uint32_t* get_address_of_hp_2() { return &___hp_2; }
	inline void set_hp_2(uint32_t value)
	{
		___hp_2 = value;
	}

	inline static int32_t get_offset_of_monsterID_3() { return static_cast<int32_t>(offsetof(CSShiliBossUnit_t2809231442, ___monsterID_3)); }
	inline uint32_t get_monsterID_3() const { return ___monsterID_3; }
	inline uint32_t* get_address_of_monsterID_3() { return &___monsterID_3; }
	inline void set_monsterID_3(uint32_t value)
	{
		___monsterID_3 = value;
	}

	inline static int32_t get_offset_of_maxHp_4() { return static_cast<int32_t>(offsetof(CSShiliBossUnit_t2809231442, ___maxHp_4)); }
	inline uint32_t get_maxHp_4() const { return ___maxHp_4; }
	inline uint32_t* get_address_of_maxHp_4() { return &___maxHp_4; }
	inline void set_maxHp_4(uint32_t value)
	{
		___maxHp_4 = value;
	}

	inline static int32_t get_offset_of_buffID_5() { return static_cast<int32_t>(offsetof(CSShiliBossUnit_t2809231442, ___buffID_5)); }
	inline uint32_t get_buffID_5() const { return ___buffID_5; }
	inline uint32_t* get_address_of_buffID_5() { return &___buffID_5; }
	inline void set_buffID_5(uint32_t value)
	{
		___buffID_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
