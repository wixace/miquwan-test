﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FingerEventDetector
struct FingerEventDetector_t1271053175;
// FingerEvent
struct FingerEvent_t252338321;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// FingerGestures
struct FingerGestures_t2907604723;
// FingerGestures/Finger
struct Finger_t182428197;
// ScreenRaycaster
struct ScreenRaycaster_t4188861866;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FingerEvent252338321.h"
#include "AssemblyU2DCSharp_ScreenRaycastData1110901127.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_FingerEventDetector1271053175.h"
#include "AssemblyU2DCSharp_FingerGestures_Finger182428197.h"
#include "AssemblyU2DCSharp_FingerGestures2907604723.h"
#include "AssemblyU2DCSharp_ScreenRaycaster4188861866.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void FingerEventDetector::.ctor()
extern "C"  void FingerEventDetector__ctor_m2757840340 (FingerEventDetector_t1271053175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerEventDetector::Awake()
extern "C"  void FingerEventDetector_Awake_m2995445559 (FingerEventDetector_t1271053175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerEventDetector::Start()
extern "C"  void FingerEventDetector_Start_m1704978132 (FingerEventDetector_t1271053175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerEventDetector::Update()
extern "C"  void FingerEventDetector_Update_m1320566713 (FingerEventDetector_t1271053175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerEventDetector::ProcessFingers()
extern "C"  void FingerEventDetector_ProcessFingers_m756370475 (FingerEventDetector_t1271053175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerEventDetector::TrySendMessage(FingerEvent)
extern "C"  void FingerEventDetector_TrySendMessage_m3569391917 (FingerEventDetector_t1271053175 * __this, FingerEvent_t252338321 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ScreenRaycastData FingerEventDetector::get_Raycast()
extern "C"  ScreenRaycastData_t1110901127  FingerEventDetector_get_Raycast_m2008066142 (FingerEventDetector_t1271053175 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject FingerEventDetector::PickObject(UnityEngine.Vector2)
extern "C"  GameObject_t3674682005 * FingerEventDetector_PickObject_m211463039 (FingerEventDetector_t1271053175 * __this, Vector2_t4282066565  ___screenPos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerEventDetector::UpdateSelection(FingerEvent)
extern "C"  void FingerEventDetector_UpdateSelection_m802849452 (FingerEventDetector_t1271053175 * __this, FingerEvent_t252338321 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerEventDetector::ilo_ProcessFingers1(FingerEventDetector)
extern "C"  void FingerEventDetector_ilo_ProcessFingers1_m2103506822 (Il2CppObject * __this /* static, unused */, FingerEventDetector_t1271053175 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures FingerEventDetector::ilo_get_Instance2()
extern "C"  FingerGestures_t2907604723 * FingerEventDetector_ilo_get_Instance2_m2561056989 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/Finger FingerEventDetector::ilo_GetFinger3(System.Int32)
extern "C"  Finger_t182428197 * FingerEventDetector_ilo_GetFinger3_m3195353306 (Il2CppObject * __this /* static, unused */, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerEventDetector::ilo_ProcessFinger4(FingerEventDetector,FingerGestures/Finger)
extern "C"  void FingerEventDetector_ilo_ProcessFinger4_m1650828547 (Il2CppObject * __this /* static, unused */, FingerEventDetector_t1271053175 * ____this0, Finger_t182428197 * ___finger1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FingerEventDetector::ilo_get_MaxFingers5(FingerGestures)
extern "C"  int32_t FingerEventDetector_ilo_get_MaxFingers5_m2548447680 (Il2CppObject * __this /* static, unused */, FingerGestures_t2907604723 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject FingerEventDetector::ilo_get_Selection6(FingerEvent)
extern "C"  GameObject_t3674682005 * FingerEventDetector_ilo_get_Selection6_m2303392544 (Il2CppObject * __this /* static, unused */, FingerEvent_t252338321 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FingerEventDetector::ilo_Raycast7(ScreenRaycaster,UnityEngine.Vector2,ScreenRaycastData&)
extern "C"  bool FingerEventDetector_ilo_Raycast7_m1292487304 (Il2CppObject * __this /* static, unused */, ScreenRaycaster_t4188861866 * ____this0, Vector2_t4282066565  ___screenPos1, ScreenRaycastData_t1110901127 * ___hitData2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 FingerEventDetector::ilo_get_Position8(FingerEvent)
extern "C"  Vector2_t4282066565  FingerEventDetector_ilo_get_Position8_m3943984961 (Il2CppObject * __this /* static, unused */, FingerEvent_t252338321 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerEventDetector::ilo_set_Selection9(FingerEvent,UnityEngine.GameObject)
extern "C"  void FingerEventDetector_ilo_set_Selection9_m3288543786 (Il2CppObject * __this /* static, unused */, FingerEvent_t252338321 * ____this0, GameObject_t3674682005 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerEventDetector::ilo_set_Raycast10(FingerEvent,ScreenRaycastData)
extern "C"  void FingerEventDetector_ilo_set_Raycast10_m650882428 (Il2CppObject * __this /* static, unused */, FingerEvent_t252338321 * ____this0, ScreenRaycastData_t1110901127  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
