﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.GraphModifier
struct GraphModifier_t2555428519;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphModifier_EventT2343033120.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphModifier2555428519.h"

// System.Void Pathfinding.GraphModifier::.ctor()
extern "C"  void GraphModifier__ctor_m2170718832 (GraphModifier_t2555428519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphModifier::FindAllModifiers()
extern "C"  void GraphModifier_FindAllModifiers_m2756277128 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphModifier::TriggerEvent(Pathfinding.GraphModifier/EventType)
extern "C"  void GraphModifier_TriggerEvent_m2179781372 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphModifier::OnEnable()
extern "C"  void GraphModifier_OnEnable_m633726742 (GraphModifier_t2555428519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphModifier::OnDisable()
extern "C"  void GraphModifier_OnDisable_m2906597079 (GraphModifier_t2555428519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphModifier::OnPostScan()
extern "C"  void GraphModifier_OnPostScan_m3132076496 (GraphModifier_t2555428519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphModifier::OnPreScan()
extern "C"  void GraphModifier_OnPreScan_m1257050543 (GraphModifier_t2555428519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphModifier::OnLatePostScan()
extern "C"  void GraphModifier_OnLatePostScan_m1054789654 (GraphModifier_t2555428519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphModifier::OnPostCacheLoad()
extern "C"  void GraphModifier_OnPostCacheLoad_m1722430519 (GraphModifier_t2555428519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphModifier::OnGraphsPreUpdate()
extern "C"  void GraphModifier_OnGraphsPreUpdate_m3170987030 (GraphModifier_t2555428519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphModifier::OnGraphsPostUpdate()
extern "C"  void GraphModifier_OnGraphsPostUpdate_m4277218465 (GraphModifier_t2555428519 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphModifier::ilo_OnPostScan1(Pathfinding.GraphModifier)
extern "C"  void GraphModifier_ilo_OnPostScan1_m3932121095 (Il2CppObject * __this /* static, unused */, GraphModifier_t2555428519 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphModifier::ilo_OnDisable2(Pathfinding.GraphModifier)
extern "C"  void GraphModifier_ilo_OnDisable2_m2928927335 (Il2CppObject * __this /* static, unused */, GraphModifier_t2555428519 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
