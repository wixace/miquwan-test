﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_ConvertAllT1__ConverterT2_T_TOutput>c__AnonStorey7C
struct U3CListA1_ConvertAllT1__ConverterT2_T_TOutputU3Ec__AnonStorey7C_t3936113534;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_ConvertAllT1__ConverterT2_T_TOutput>c__AnonStorey7C::.ctor()
extern "C"  void U3CListA1_ConvertAllT1__ConverterT2_T_TOutputU3Ec__AnonStorey7C__ctor_m920321757 (U3CListA1_ConvertAllT1__ConverterT2_T_TOutputU3Ec__AnonStorey7C_t3936113534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Collections_Generic_List77Generated/<ListA1_ConvertAllT1__ConverterT2_T_TOutput>c__AnonStorey7C::<>m__9E()
extern "C"  Il2CppObject * U3CListA1_ConvertAllT1__ConverterT2_T_TOutputU3Ec__AnonStorey7C_U3CU3Em__9E_m1729811069 (U3CListA1_ConvertAllT1__ConverterT2_T_TOutputU3Ec__AnonStorey7C_t3936113534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
