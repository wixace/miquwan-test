﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Runtime_InteropServices__ExceptionGenerated
struct System_Runtime_InteropServices__ExceptionGenerated_t1177216676;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void System_Runtime_InteropServices__ExceptionGenerated::.ctor()
extern "C"  void System_Runtime_InteropServices__ExceptionGenerated__ctor_m2621698679 (System_Runtime_InteropServices__ExceptionGenerated_t1177216676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Runtime_InteropServices__ExceptionGenerated::_Exception_HelpLink(JSVCall)
extern "C"  void System_Runtime_InteropServices__ExceptionGenerated__Exception_HelpLink_m2899660520 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Runtime_InteropServices__ExceptionGenerated::_Exception_InnerException(JSVCall)
extern "C"  void System_Runtime_InteropServices__ExceptionGenerated__Exception_InnerException_m2372244266 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Runtime_InteropServices__ExceptionGenerated::_Exception_Message(JSVCall)
extern "C"  void System_Runtime_InteropServices__ExceptionGenerated__Exception_Message_m2983530466 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Runtime_InteropServices__ExceptionGenerated::_Exception_Source(JSVCall)
extern "C"  void System_Runtime_InteropServices__ExceptionGenerated__Exception_Source_m3859352808 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Runtime_InteropServices__ExceptionGenerated::_Exception_StackTrace(JSVCall)
extern "C"  void System_Runtime_InteropServices__ExceptionGenerated__Exception_StackTrace_m2334317862 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Runtime_InteropServices__ExceptionGenerated::_Exception_TargetSite(JSVCall)
extern "C"  void System_Runtime_InteropServices__ExceptionGenerated__Exception_TargetSite_m50111147 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Runtime_InteropServices__ExceptionGenerated::_Exception_Equals__Object(JSVCall,System.Int32)
extern "C"  bool System_Runtime_InteropServices__ExceptionGenerated__Exception_Equals__Object_m2476468830 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Runtime_InteropServices__ExceptionGenerated::_Exception_GetBaseException(JSVCall,System.Int32)
extern "C"  bool System_Runtime_InteropServices__ExceptionGenerated__Exception_GetBaseException_m602627048 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Runtime_InteropServices__ExceptionGenerated::_Exception_GetHashCode(JSVCall,System.Int32)
extern "C"  bool System_Runtime_InteropServices__ExceptionGenerated__Exception_GetHashCode_m2060925555 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Runtime_InteropServices__ExceptionGenerated::_Exception_GetObjectData__SerializationInfo__StreamingContext(JSVCall,System.Int32)
extern "C"  bool System_Runtime_InteropServices__ExceptionGenerated__Exception_GetObjectData__SerializationInfo__StreamingContext_m2737968236 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Runtime_InteropServices__ExceptionGenerated::_Exception_GetType(JSVCall,System.Int32)
extern "C"  bool System_Runtime_InteropServices__ExceptionGenerated__Exception_GetType_m426657746 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Runtime_InteropServices__ExceptionGenerated::_Exception_ToString(JSVCall,System.Int32)
extern "C"  bool System_Runtime_InteropServices__ExceptionGenerated__Exception_ToString_m3044661644 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Runtime_InteropServices__ExceptionGenerated::__Register()
extern "C"  void System_Runtime_InteropServices__ExceptionGenerated___Register_m3138747568 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Runtime_InteropServices__ExceptionGenerated::ilo_setStringS1(System.Int32,System.String)
extern "C"  void System_Runtime_InteropServices__ExceptionGenerated_ilo_setStringS1_m497980864 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Runtime_InteropServices__ExceptionGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_Runtime_InteropServices__ExceptionGenerated_ilo_getObject2_m1178417753 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
