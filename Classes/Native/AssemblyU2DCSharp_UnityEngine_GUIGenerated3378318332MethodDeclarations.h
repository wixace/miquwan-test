﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUIGenerated
struct UnityEngine_GUIGenerated_t3378318332;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.GUI/WindowFunction
struct WindowFunction_t2749288659;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// UnityEngine.GUIContent[]
struct GUIContentU5BU5D_t3588725815;
// System.String[]
struct StringU5BU5D_t4054002952;
// UnityEngine.Texture[]
struct TextureU5BU5D_t606176076;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Delegate
struct Delegate_t3310234105;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "mscorlib_System_Delegate3310234105.h"

// System.Void UnityEngine_GUIGenerated::.ctor()
extern "C"  void UnityEngine_GUIGenerated__ctor_m2427207199 (UnityEngine_GUIGenerated_t3378318332 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_GUI1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_GUI1_m2497833347 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated::GUI_skin(JSVCall)
extern "C"  void UnityEngine_GUIGenerated_GUI_skin_m2069251041 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated::GUI_matrix(JSVCall)
extern "C"  void UnityEngine_GUIGenerated_GUI_matrix_m2516447261 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated::GUI_tooltip(JSVCall)
extern "C"  void UnityEngine_GUIGenerated_GUI_tooltip_m3058017963 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated::GUI_color(JSVCall)
extern "C"  void UnityEngine_GUIGenerated_GUI_color_m4242695499 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated::GUI_backgroundColor(JSVCall)
extern "C"  void UnityEngine_GUIGenerated_GUI_backgroundColor_m3352609817 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated::GUI_contentColor(JSVCall)
extern "C"  void UnityEngine_GUIGenerated_GUI_contentColor_m833895828 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated::GUI_changed(JSVCall)
extern "C"  void UnityEngine_GUIGenerated_GUI_changed_m2658241306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated::GUI_enabled(JSVCall)
extern "C"  void UnityEngine_GUIGenerated_GUI_enabled_m1963831597 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated::GUI_depth(JSVCall)
extern "C"  void UnityEngine_GUIGenerated_GUI_depth_m167876075 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_BeginClip__Rect__Vector2__Vector2__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_BeginClip__Rect__Vector2__Vector2__Boolean_m4289406680 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_BeginClip__Rect(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_BeginClip__Rect_m681619442 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_BeginGroup__Rect__String__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_BeginGroup__Rect__String__GUIStyle_m1359148590 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_BeginGroup__Rect__GUIContent__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_BeginGroup__Rect__GUIContent__GUIStyle_m1013874139 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_BeginGroup__Rect__Texture__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_BeginGroup__Rect__Texture__GUIStyle_m1948609868 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_BeginGroup__Rect__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_BeginGroup__Rect__String_m3238865464 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_BeginGroup__Rect__Texture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_BeginGroup__Rect__Texture_m1629900502 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_BeginGroup__Rect__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_BeginGroup__Rect__GUIStyle_m560873501 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_BeginGroup__Rect__GUIContent(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_BeginGroup__Rect__GUIContent_m3200316965 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_BeginGroup__Rect(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_BeginGroup__Rect_m3558884071 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_BeginScrollView__Rect__Vector2__Rect__Boolean__Boolean__GUIStyle__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_BeginScrollView__Rect__Vector2__Rect__Boolean__Boolean__GUIStyle__GUIStyle_m567318893 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_BeginScrollView__Rect__Vector2__Rect__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_BeginScrollView__Rect__Vector2__Rect__Boolean__Boolean_m3461839745 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_BeginScrollView__Rect__Vector2__Rect__GUIStyle__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_BeginScrollView__Rect__Vector2__Rect__GUIStyle__GUIStyle_m4244711533 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_BeginScrollView__Rect__Vector2__Rect(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_BeginScrollView__Rect__Vector2__Rect_m2787478145 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Box__Rect__Texture__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Box__Rect__Texture__GUIStyle_m415723247 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Box__Rect__GUIContent__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Box__Rect__GUIContent__GUIStyle_m3175806296 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Box__Rect__String__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Box__Rect__String__GUIStyle_m1863889963 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Box__Rect__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Box__Rect__String_m976998517 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Box__Rect__Texture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Box__Rect__Texture_m231501881 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Box__Rect__GUIContent(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Box__Rect__GUIContent_m394802658 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_BringWindowToBack__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_BringWindowToBack__Int32_m1956137847 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_BringWindowToFront__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_BringWindowToFront__Int32_m1439922583 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Button__Rect__GUIContent__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Button__Rect__GUIContent__GUIStyle_m2493846711 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Button__Rect__String__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Button__Rect__String__GUIStyle_m1911511818 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Button__Rect__Texture__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Button__Rect__Texture__GUIStyle_m1892000752 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Button__Rect__Texture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Button__Rect__Texture_m540525178 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Button__Rect__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Button__Rect__String_m2649534996 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Button__Rect__GUIContent(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Button__Rect__GUIContent_m2392928257 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_DragWindow__Rect(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_DragWindow__Rect_m1582063829 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_DragWindow(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_DragWindow_m2458444273 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_DrawTexture__Rect__Texture__ScaleMode__Boolean__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_DrawTexture__Rect__Texture__ScaleMode__Boolean__Single_m353117712 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_DrawTexture__Rect__Texture__ScaleMode__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_DrawTexture__Rect__Texture__ScaleMode__Boolean_m1663868616 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_DrawTexture__Rect__Texture__ScaleMode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_DrawTexture__Rect__Texture__ScaleMode_m460969122 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_DrawTexture__Rect__Texture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_DrawTexture__Rect__Texture_m1829517901 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_DrawTextureWithTexCoords__Rect__Texture__Rect__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_DrawTextureWithTexCoords__Rect__Texture__Rect__Boolean_m1207590434 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_DrawTextureWithTexCoords__Rect__Texture__Rect(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_DrawTextureWithTexCoords__Rect__Texture__Rect_m2647164936 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_EndClip(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_EndClip_m2315569536 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_EndGroup(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_EndGroup_m4061745425 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_EndScrollView__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_EndScrollView__Boolean_m3631641320 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_EndScrollView(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_EndScrollView_m2410339458 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_FocusControl__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_FocusControl__String_m799965091 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_FocusWindow__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_FocusWindow__Int32_m2930577619 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_GetNameOfFocusedControl(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_GetNameOfFocusedControl_m3651716915 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_HorizontalScrollbar__Rect__Single__Single__Single__Single__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_HorizontalScrollbar__Rect__Single__Single__Single__Single__GUIStyle_m3561248657 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_HorizontalScrollbar__Rect__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_HorizontalScrollbar__Rect__Single__Single__Single__Single_m2186500955 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_HorizontalSlider__Rect__Single__Single__Single__GUIStyle__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_HorizontalSlider__Rect__Single__Single__Single__GUIStyle__GUIStyle_m193298810 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_HorizontalSlider__Rect__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_HorizontalSlider__Rect__Single__Single__Single_m4152699918 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Label__Rect__GUIContent__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Label__Rect__GUIContent__GUIStyle_m4284289345 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Label__Rect__String__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Label__Rect__String__GUIStyle_m2740923540 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Label__Rect__Texture__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Label__Rect__Texture__GUIStyle_m1833960358 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Label__Rect__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Label__Rect__String_m1801521182 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Label__Rect__GUIContent(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Label__Rect__GUIContent_m1049063691 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Label__Rect__Texture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Label__Rect__Texture_m21900720 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::GUI_ModalWindow_GetDelegate_member53_arg2(CSRepresentedObject)
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_GUI_ModalWindow_GetDelegate_member53_arg2_m3282715056 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_ModalWindow__Int32__Rect__WindowFunction__String__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_ModalWindow__Int32__Rect__WindowFunction__String__GUIStyle_m3131250801 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::GUI_ModalWindow_GetDelegate_member54_arg2(CSRepresentedObject)
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_GUI_ModalWindow_GetDelegate_member54_arg2_m4244329073 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_ModalWindow__Int32__Rect__WindowFunction__Texture__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_ModalWindow__Int32__Rect__WindowFunction__Texture__GUIStyle_m1049203561 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::GUI_ModalWindow_GetDelegate_member55_arg2(CSRepresentedObject)
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_GUI_ModalWindow_GetDelegate_member55_arg2_m910975794 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_ModalWindow__Int32__Rect__WindowFunction__GUIContent__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_ModalWindow__Int32__Rect__WindowFunction__GUIContent__GUIStyle_m3101542046 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::GUI_ModalWindow_GetDelegate_member56_arg2(CSRepresentedObject)
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_GUI_ModalWindow_GetDelegate_member56_arg2_m1872589811 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_ModalWindow__Int32__Rect__WindowFunction__GUIContent(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_ModalWindow__Int32__Rect__WindowFunction__GUIContent_m3802973864 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::GUI_ModalWindow_GetDelegate_member57_arg2(CSRepresentedObject)
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_GUI_ModalWindow_GetDelegate_member57_arg2_m2834203828 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_ModalWindow__Int32__Rect__WindowFunction__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_ModalWindow__Int32__Rect__WindowFunction__String_m1193522235 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::GUI_ModalWindow_GetDelegate_member58_arg2(CSRepresentedObject)
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_GUI_ModalWindow_GetDelegate_member58_arg2_m3795817845 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_ModalWindow__Int32__Rect__WindowFunction__Texture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_ModalWindow__Int32__Rect__WindowFunction__Texture_m2648769843 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_PasswordField__Rect__String__Char__Int32__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_PasswordField__Rect__String__Char__Int32__GUIStyle_m4149397383 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_PasswordField__Rect__String__Char__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_PasswordField__Rect__String__Char__GUIStyle_m1837823701 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_PasswordField__Rect__String__Char__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_PasswordField__Rect__String__Char__Int32_m1417225937 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_PasswordField__Rect__String__Char(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_PasswordField__Rect__String__Char_m2461499295 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_RepeatButton__Rect__GUIContent__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_RepeatButton__Rect__GUIContent__GUIStyle_m190100594 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_RepeatButton__Rect__Texture__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_RepeatButton__Rect__Texture__GUIStyle_m1967756821 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_RepeatButton__Rect__String__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_RepeatButton__Rect__String__GUIStyle_m667029573 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_RepeatButton__Rect__GUIContent(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_RepeatButton__Rect__GUIContent_m1896295804 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_RepeatButton__Rect__Texture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_RepeatButton__Rect__Texture_m51916511 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_RepeatButton__Rect__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_RepeatButton__Rect__String_m3603604751 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_ScrollTo__Rect(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_ScrollTo__Rect_m2222853561 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_ScrollTowards__Rect__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_ScrollTowards__Rect__Single_m594118896 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_SelectionGrid__Rect__Int32__GUIContent_Array__Int32__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_SelectionGrid__Rect__Int32__GUIContent_Array__Int32__GUIStyle_m3633007945 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_SelectionGrid__Rect__Int32__String_Array__Int32__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_SelectionGrid__Rect__Int32__String_Array__Int32__GUIStyle_m3113747670 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_SelectionGrid__Rect__Int32__Texture_Array__Int32__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_SelectionGrid__Rect__Int32__Texture_Array__Int32__GUIStyle_m119817908 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_SelectionGrid__Rect__Int32__GUIContent_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_SelectionGrid__Rect__Int32__GUIContent_Array__Int32_m3725039379 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_SelectionGrid__Rect__Int32__Texture_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_SelectionGrid__Rect__Int32__Texture_Array__Int32_m398084670 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_SelectionGrid__Rect__Int32__String_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_SelectionGrid__Rect__Int32__String_Array__Int32_m3694979808 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_SetNextControlName__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_SetNextControlName__String_m573325457 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Slider__Rect__Single__Single__Single__Single__GUIStyle__GUIStyle__Boolean__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Slider__Rect__Single__Single__Single__Single__GUIStyle__GUIStyle__Boolean__Int32_m739354468 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_TextArea__Rect__String__Int32__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_TextArea__Rect__String__Int32__GUIStyle_m1861798282 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_TextArea__Rect__String__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_TextArea__Rect__String__GUIStyle_m333701682 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_TextArea__Rect__String__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_TextArea__Rect__String__Int32_m1958101140 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_TextArea__Rect__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_TextArea__Rect__String_m1415613244 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_TextField__Rect__String__Int32__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_TextField__Rect__String__Int32__GUIStyle_m2764698543 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_TextField__Rect__String__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_TextField__Rect__String__GUIStyle_m2834016173 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_TextField__Rect__String__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_TextField__Rect__String__Int32_m361358585 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_TextField__Rect__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_TextField__Rect__String_m809426551 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Toggle__Rect__Int32__Boolean__GUIContent__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Toggle__Rect__Int32__Boolean__GUIContent__GUIStyle_m3470910131 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Toggle__Rect__Boolean__GUIContent__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Toggle__Rect__Boolean__GUIContent__GUIStyle_m1723900761 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Toggle__Rect__Boolean__Texture__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Toggle__Rect__Boolean__Texture__GUIStyle_m703293582 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Toggle__Rect__Boolean__String__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Toggle__Rect__Boolean__String__GUIStyle_m349145772 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Toggle__Rect__Boolean__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Toggle__Rect__Boolean__String_m3639845942 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Toggle__Rect__Boolean__Texture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Toggle__Rect__Boolean__Texture_m1175393432 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Toggle__Rect__Boolean__GUIContent(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Toggle__Rect__Boolean__GUIContent_m717111587 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Toolbar__Rect__Int32__GUIContent_Array__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Toolbar__Rect__Int32__GUIContent_Array__GUIStyle_m838778538 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Toolbar__Rect__Int32__Texture_Array__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Toolbar__Rect__Int32__Texture_Array__GUIStyle_m1640128593 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Toolbar__Rect__Int32__String_Array__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Toolbar__Rect__Int32__String_Array__GUIStyle_m4013489981 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Toolbar__Rect__Int32__GUIContent_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Toolbar__Rect__Int32__GUIContent_Array_m4153810356 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Toolbar__Rect__Int32__Texture_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Toolbar__Rect__Int32__Texture_Array_m56710171 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Toolbar__Rect__Int32__String_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Toolbar__Rect__Int32__String_Array_m2706991623 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_UnfocusWindow(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_UnfocusWindow_m2279076708 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_VerticalScrollbar__Rect__Single__Single__Single__Single__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_VerticalScrollbar__Rect__Single__Single__Single__Single__GUIStyle_m4159825663 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_VerticalScrollbar__Rect__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_VerticalScrollbar__Rect__Single__Single__Single__Single_m2199567433 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_VerticalSlider__Rect__Single__Single__Single__GUIStyle__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_VerticalSlider__Rect__Single__Single__Single__GUIStyle__GUIStyle_m2706459724 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_VerticalSlider__Rect__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_VerticalSlider__Rect__Single__Single__Single_m1171330016 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::GUI_Window_GetDelegate_member105_arg2(CSRepresentedObject)
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_GUI_Window_GetDelegate_member105_arg2_m3067945723 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Window__Int32__Rect__WindowFunction__Texture__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Window__Int32__Rect__WindowFunction__Texture__GUIStyle_m603561428 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::GUI_Window_GetDelegate_member106_arg2(CSRepresentedObject)
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_GUI_Window_GetDelegate_member106_arg2_m4029559740 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Window__Int32__Rect__WindowFunction__String__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Window__Int32__Rect__WindowFunction__String__GUIStyle_m623023270 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::GUI_Window_GetDelegate_member107_arg2(CSRepresentedObject)
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_GUI_Window_GetDelegate_member107_arg2_m696206461 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Window__Int32__Rect__WindowFunction__GUIContent__GUIStyle(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Window__Int32__Rect__WindowFunction__GUIContent__GUIStyle_m2720669779 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::GUI_Window_GetDelegate_member108_arg2(CSRepresentedObject)
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_GUI_Window_GetDelegate_member108_arg2_m1657820478 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Window__Int32__Rect__WindowFunction__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Window__Int32__Rect__WindowFunction__String_m1928462000 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::GUI_Window_GetDelegate_member109_arg2(CSRepresentedObject)
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_GUI_Window_GetDelegate_member109_arg2_m2619434495 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Window__Int32__Rect__WindowFunction__Texture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Window__Int32__Rect__WindowFunction__Texture_m3957066078 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::GUI_Window_GetDelegate_member110_arg2(CSRepresentedObject)
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_GUI_Window_GetDelegate_member110_arg2_m2300106389 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::GUI_Window__Int32__Rect__WindowFunction__GUIContent(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_GUI_Window__Int32__Rect__WindowFunction__GUIContent_m2427899549 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated::__Register()
extern "C"  void UnityEngine_GUIGenerated___Register_m1641167368 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::<GUI_ModalWindow__Int32__Rect__WindowFunction__String__GUIStyle>m__1B9()
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_U3CGUI_ModalWindow__Int32__Rect__WindowFunction__String__GUIStyleU3Em__1B9_m3901270628 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::<GUI_ModalWindow__Int32__Rect__WindowFunction__Texture__GUIStyle>m__1BB()
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_U3CGUI_ModalWindow__Int32__Rect__WindowFunction__Texture__GUIStyleU3Em__1BB_m3909987553 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::<GUI_ModalWindow__Int32__Rect__WindowFunction__GUIContent__GUIStyle>m__1BD()
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_U3CGUI_ModalWindow__Int32__Rect__WindowFunction__GUIContent__GUIStyleU3Em__1BD_m3496078178 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::<GUI_ModalWindow__Int32__Rect__WindowFunction__GUIContent>m__1BF()
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_U3CGUI_ModalWindow__Int32__Rect__WindowFunction__GUIContentU3Em__1BF_m1960025882 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::<GUI_ModalWindow__Int32__Rect__WindowFunction__String>m__1C1()
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_U3CGUI_ModalWindow__Int32__Rect__WindowFunction__StringU3Em__1C1_m2202351921 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::<GUI_ModalWindow__Int32__Rect__WindowFunction__Texture>m__1C3()
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_U3CGUI_ModalWindow__Int32__Rect__WindowFunction__TextureU3Em__1C3_m2752885351 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIContent[] UnityEngine_GUIGenerated::<GUI_SelectionGrid__Rect__Int32__GUIContent_Array__Int32__GUIStyle>m__1C4()
extern "C"  GUIContentU5BU5D_t3588725815* UnityEngine_GUIGenerated_U3CGUI_SelectionGrid__Rect__Int32__GUIContent_Array__Int32__GUIStyleU3Em__1C4_m1285760790 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UnityEngine_GUIGenerated::<GUI_SelectionGrid__Rect__Int32__String_Array__Int32__GUIStyle>m__1C5()
extern "C"  StringU5BU5D_t4054002952* UnityEngine_GUIGenerated_U3CGUI_SelectionGrid__Rect__Int32__String_Array__Int32__GUIStyleU3Em__1C5_m2938981007 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture[] UnityEngine_GUIGenerated::<GUI_SelectionGrid__Rect__Int32__Texture_Array__Int32__GUIStyle>m__1C6()
extern "C"  TextureU5BU5D_t606176076* UnityEngine_GUIGenerated_U3CGUI_SelectionGrid__Rect__Int32__Texture_Array__Int32__GUIStyleU3Em__1C6_m798115648 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIContent[] UnityEngine_GUIGenerated::<GUI_SelectionGrid__Rect__Int32__GUIContent_Array__Int32>m__1C7()
extern "C"  GUIContentU5BU5D_t3588725815* UnityEngine_GUIGenerated_U3CGUI_SelectionGrid__Rect__Int32__GUIContent_Array__Int32U3Em__1C7_m1008330959 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture[] UnityEngine_GUIGenerated::<GUI_SelectionGrid__Rect__Int32__Texture_Array__Int32>m__1C8()
extern "C"  TextureU5BU5D_t606176076* UnityEngine_GUIGenerated_U3CGUI_SelectionGrid__Rect__Int32__Texture_Array__Int32U3Em__1C8_m1321744184 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UnityEngine_GUIGenerated::<GUI_SelectionGrid__Rect__Int32__String_Array__Int32>m__1C9()
extern "C"  StringU5BU5D_t4054002952* UnityEngine_GUIGenerated_U3CGUI_SelectionGrid__Rect__Int32__String_Array__Int32U3Em__1C9_m1847449097 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIContent[] UnityEngine_GUIGenerated::<GUI_Toolbar__Rect__Int32__GUIContent_Array__GUIStyle>m__1CA()
extern "C"  GUIContentU5BU5D_t3588725815* UnityEngine_GUIGenerated_U3CGUI_Toolbar__Rect__Int32__GUIContent_Array__GUIStyleU3Em__1CA_m2542956078 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture[] UnityEngine_GUIGenerated::<GUI_Toolbar__Rect__Int32__Texture_Array__GUIStyle>m__1CB()
extern "C"  TextureU5BU5D_t606176076* UnityEngine_GUIGenerated_U3CGUI_Toolbar__Rect__Int32__Texture_Array__GUIStyleU3Em__1CB_m4109875733 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UnityEngine_GUIGenerated::<GUI_Toolbar__Rect__Int32__String_Array__GUIStyle>m__1CC()
extern "C"  StringU5BU5D_t4054002952* UnityEngine_GUIGenerated_U3CGUI_Toolbar__Rect__Int32__String_Array__GUIStyleU3Em__1CC_m1257761208 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIContent[] UnityEngine_GUIGenerated::<GUI_Toolbar__Rect__Int32__GUIContent_Array>m__1CD()
extern "C"  GUIContentU5BU5D_t3588725815* UnityEngine_GUIGenerated_U3CGUI_Toolbar__Rect__Int32__GUIContent_ArrayU3Em__1CD_m1087865255 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture[] UnityEngine_GUIGenerated::<GUI_Toolbar__Rect__Int32__Texture_Array>m__1CE()
extern "C"  TextureU5BU5D_t606176076* UnityEngine_GUIGenerated_U3CGUI_Toolbar__Rect__Int32__Texture_ArrayU3Em__1CE_m1632347406 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UnityEngine_GUIGenerated::<GUI_Toolbar__Rect__Int32__String_Array>m__1CF()
extern "C"  StringU5BU5D_t4054002952* UnityEngine_GUIGenerated_U3CGUI_Toolbar__Rect__Int32__String_ArrayU3Em__1CF_m2209670961 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::<GUI_Window__Int32__Rect__WindowFunction__Texture__GUIStyle>m__1D1()
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_U3CGUI_Window__Int32__Rect__WindowFunction__Texture__GUIStyleU3Em__1D1_m897916887 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::<GUI_Window__Int32__Rect__WindowFunction__String__GUIStyle>m__1D3()
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_U3CGUI_Window__Int32__Rect__WindowFunction__String__GUIStyleU3Em__1D3_m3665612147 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::<GUI_Window__Int32__Rect__WindowFunction__GUIContent__GUIStyle>m__1D5()
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_U3CGUI_Window__Int32__Rect__WindowFunction__GUIContent__GUIStyleU3Em__1D5_m67348776 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::<GUI_Window__Int32__Rect__WindowFunction__String>m__1D7()
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_U3CGUI_Window__Int32__Rect__WindowFunction__StringU3Em__1D7_m3205031341 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::<GUI_Window__Int32__Rect__WindowFunction__Texture>m__1D9()
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_U3CGUI_Window__Int32__Rect__WindowFunction__TextureU3Em__1D9_m3770109589 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::<GUI_Window__Int32__Rect__WindowFunction__GUIContent>m__1DB()
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_U3CGUI_Window__Int32__Rect__WindowFunction__GUIContentU3Em__1DB_m4033804011 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_ilo_attachFinalizerObject1_m2920514976 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUIGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_GUIGenerated_ilo_setObject2_m2149679628 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated::ilo_setStringS3(System.Int32,System.String)
extern "C"  void UnityEngine_GUIGenerated_ilo_setStringS3_m1641018986 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_GUIGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_GUIGenerated_ilo_getObject4_m3768058355 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_GUIGenerated::ilo_getVector2S5(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_GUIGenerated_ilo_getVector2S5_m2194362271 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::ilo_getBooleanS6(System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_ilo_getBooleanS6_m1283008274 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_GUIGenerated::ilo_getStringS7(System.Int32)
extern "C"  String_t* UnityEngine_GUIGenerated_ilo_getStringS7_m1789623843 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated::ilo_setVector2S8(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_GUIGenerated_ilo_setVector2S8_m2118329711 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated::ilo_setBooleanS9(System.Int32,System.Boolean)
extern "C"  void UnityEngine_GUIGenerated_ilo_setBooleanS9_m2556456520 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_GUIGenerated::ilo_getSingle10(System.Int32)
extern "C"  float UnityEngine_GUIGenerated_ilo_getSingle10_m1876625452 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated::ilo_setSingle11(System.Int32,System.Single)
extern "C"  void UnityEngine_GUIGenerated_ilo_setSingle11_m1334963946 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated::ilo_addJSFunCSDelegateRel12(System.Int32,System.Delegate)
extern "C"  void UnityEngine_GUIGenerated_ilo_addJSFunCSDelegateRel12_m56609651 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUIGenerated::ilo_getInt3213(System.Int32)
extern "C"  int32_t UnityEngine_GUIGenerated_ilo_getInt3213_m3003647025 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 UnityEngine_GUIGenerated::ilo_getChar14(System.Int32)
extern "C"  int16_t UnityEngine_GUIGenerated_ilo_getChar14_m4240147522 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated::ilo_setInt3215(System.Int32,System.Int32)
extern "C"  void UnityEngine_GUIGenerated_ilo_setInt3215_m714063326 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject UnityEngine_GUIGenerated::ilo_getFunctionS16(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * UnityEngine_GUIGenerated_ilo_getFunctionS16_m837689744 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::ilo_GUI_ModalWindow_GetDelegate_member53_arg217(CSRepresentedObject)
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_ilo_GUI_ModalWindow_GetDelegate_member53_arg217_m1793099069 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::ilo_GUI_ModalWindow_GetDelegate_member54_arg218(CSRepresentedObject)
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_ilo_GUI_ModalWindow_GetDelegate_member54_arg218_m448144477 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIGenerated::ilo_isFunctionS19(System.Int32)
extern "C"  bool UnityEngine_GUIGenerated_ilo_isFunctionS19_m691521664 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::ilo_GUI_ModalWindow_GetDelegate_member55_arg220(CSRepresentedObject)
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_ilo_GUI_ModalWindow_GetDelegate_member55_arg220_m1510591783 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::ilo_GUI_ModalWindow_GetDelegate_member57_arg221(CSRepresentedObject)
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_ilo_GUI_ModalWindow_GetDelegate_member57_arg221_m858738888 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::ilo_GUI_ModalWindow_GetDelegate_member58_arg222(CSRepresentedObject)
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_ilo_GUI_ModalWindow_GetDelegate_member58_arg222_m3808751592 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUIGenerated::ilo_getObject23(System.Int32)
extern "C"  int32_t UnityEngine_GUIGenerated_ilo_getObject23_m3231985219 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUIGenerated::ilo_getElement24(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_GUIGenerated_ilo_getElement24_m751260216 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUIGenerated::ilo_getArrayLength25(System.Int32)
extern "C"  int32_t UnityEngine_GUIGenerated_ilo_getArrayLength25_m951593315 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::ilo_GUI_Window_GetDelegate_member105_arg226(CSRepresentedObject)
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_ilo_GUI_Window_GetDelegate_member105_arg226_m3053039594 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::ilo_GUI_Window_GetDelegate_member107_arg227(CSRepresentedObject)
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_ilo_GUI_Window_GetDelegate_member107_arg227_m2401186699 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUI/WindowFunction UnityEngine_GUIGenerated::ilo_GUI_Window_GetDelegate_member108_arg228(CSRepresentedObject)
extern "C"  WindowFunction_t2749288659 * UnityEngine_GUIGenerated_ilo_GUI_Window_GetDelegate_member108_arg228_m1056232107 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
