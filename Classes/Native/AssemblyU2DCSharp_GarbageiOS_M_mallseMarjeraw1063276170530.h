﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_mallseMarjeraw106
struct  M_mallseMarjeraw106_t3276170530  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_mallseMarjeraw106::_korturborCuzall
	String_t* ____korturborCuzall_0;
	// System.UInt32 GarbageiOS.M_mallseMarjeraw106::_fearfelnalXemxair
	uint32_t ____fearfelnalXemxair_1;
	// System.Boolean GarbageiOS.M_mallseMarjeraw106::_lorfere
	bool ____lorfere_2;
	// System.Boolean GarbageiOS.M_mallseMarjeraw106::_penapeDrupou
	bool ____penapeDrupou_3;
	// System.Boolean GarbageiOS.M_mallseMarjeraw106::_lisbiJemperegea
	bool ____lisbiJemperegea_4;
	// System.String GarbageiOS.M_mallseMarjeraw106::_sibasfeeCherwea
	String_t* ____sibasfeeCherwea_5;
	// System.Boolean GarbageiOS.M_mallseMarjeraw106::_jelrearXounisye
	bool ____jelrearXounisye_6;
	// System.Boolean GarbageiOS.M_mallseMarjeraw106::_nanearnem
	bool ____nanearnem_7;
	// System.UInt32 GarbageiOS.M_mallseMarjeraw106::_joosteebuLallheadra
	uint32_t ____joosteebuLallheadra_8;
	// System.Single GarbageiOS.M_mallseMarjeraw106::_jirwhorso
	float ____jirwhorso_9;
	// System.Single GarbageiOS.M_mallseMarjeraw106::_wellape
	float ____wellape_10;
	// System.Single GarbageiOS.M_mallseMarjeraw106::_gelbe
	float ____gelbe_11;
	// System.Single GarbageiOS.M_mallseMarjeraw106::_zubairWhebea
	float ____zubairWhebea_12;

public:
	inline static int32_t get_offset_of__korturborCuzall_0() { return static_cast<int32_t>(offsetof(M_mallseMarjeraw106_t3276170530, ____korturborCuzall_0)); }
	inline String_t* get__korturborCuzall_0() const { return ____korturborCuzall_0; }
	inline String_t** get_address_of__korturborCuzall_0() { return &____korturborCuzall_0; }
	inline void set__korturborCuzall_0(String_t* value)
	{
		____korturborCuzall_0 = value;
		Il2CppCodeGenWriteBarrier(&____korturborCuzall_0, value);
	}

	inline static int32_t get_offset_of__fearfelnalXemxair_1() { return static_cast<int32_t>(offsetof(M_mallseMarjeraw106_t3276170530, ____fearfelnalXemxair_1)); }
	inline uint32_t get__fearfelnalXemxair_1() const { return ____fearfelnalXemxair_1; }
	inline uint32_t* get_address_of__fearfelnalXemxair_1() { return &____fearfelnalXemxair_1; }
	inline void set__fearfelnalXemxair_1(uint32_t value)
	{
		____fearfelnalXemxair_1 = value;
	}

	inline static int32_t get_offset_of__lorfere_2() { return static_cast<int32_t>(offsetof(M_mallseMarjeraw106_t3276170530, ____lorfere_2)); }
	inline bool get__lorfere_2() const { return ____lorfere_2; }
	inline bool* get_address_of__lorfere_2() { return &____lorfere_2; }
	inline void set__lorfere_2(bool value)
	{
		____lorfere_2 = value;
	}

	inline static int32_t get_offset_of__penapeDrupou_3() { return static_cast<int32_t>(offsetof(M_mallseMarjeraw106_t3276170530, ____penapeDrupou_3)); }
	inline bool get__penapeDrupou_3() const { return ____penapeDrupou_3; }
	inline bool* get_address_of__penapeDrupou_3() { return &____penapeDrupou_3; }
	inline void set__penapeDrupou_3(bool value)
	{
		____penapeDrupou_3 = value;
	}

	inline static int32_t get_offset_of__lisbiJemperegea_4() { return static_cast<int32_t>(offsetof(M_mallseMarjeraw106_t3276170530, ____lisbiJemperegea_4)); }
	inline bool get__lisbiJemperegea_4() const { return ____lisbiJemperegea_4; }
	inline bool* get_address_of__lisbiJemperegea_4() { return &____lisbiJemperegea_4; }
	inline void set__lisbiJemperegea_4(bool value)
	{
		____lisbiJemperegea_4 = value;
	}

	inline static int32_t get_offset_of__sibasfeeCherwea_5() { return static_cast<int32_t>(offsetof(M_mallseMarjeraw106_t3276170530, ____sibasfeeCherwea_5)); }
	inline String_t* get__sibasfeeCherwea_5() const { return ____sibasfeeCherwea_5; }
	inline String_t** get_address_of__sibasfeeCherwea_5() { return &____sibasfeeCherwea_5; }
	inline void set__sibasfeeCherwea_5(String_t* value)
	{
		____sibasfeeCherwea_5 = value;
		Il2CppCodeGenWriteBarrier(&____sibasfeeCherwea_5, value);
	}

	inline static int32_t get_offset_of__jelrearXounisye_6() { return static_cast<int32_t>(offsetof(M_mallseMarjeraw106_t3276170530, ____jelrearXounisye_6)); }
	inline bool get__jelrearXounisye_6() const { return ____jelrearXounisye_6; }
	inline bool* get_address_of__jelrearXounisye_6() { return &____jelrearXounisye_6; }
	inline void set__jelrearXounisye_6(bool value)
	{
		____jelrearXounisye_6 = value;
	}

	inline static int32_t get_offset_of__nanearnem_7() { return static_cast<int32_t>(offsetof(M_mallseMarjeraw106_t3276170530, ____nanearnem_7)); }
	inline bool get__nanearnem_7() const { return ____nanearnem_7; }
	inline bool* get_address_of__nanearnem_7() { return &____nanearnem_7; }
	inline void set__nanearnem_7(bool value)
	{
		____nanearnem_7 = value;
	}

	inline static int32_t get_offset_of__joosteebuLallheadra_8() { return static_cast<int32_t>(offsetof(M_mallseMarjeraw106_t3276170530, ____joosteebuLallheadra_8)); }
	inline uint32_t get__joosteebuLallheadra_8() const { return ____joosteebuLallheadra_8; }
	inline uint32_t* get_address_of__joosteebuLallheadra_8() { return &____joosteebuLallheadra_8; }
	inline void set__joosteebuLallheadra_8(uint32_t value)
	{
		____joosteebuLallheadra_8 = value;
	}

	inline static int32_t get_offset_of__jirwhorso_9() { return static_cast<int32_t>(offsetof(M_mallseMarjeraw106_t3276170530, ____jirwhorso_9)); }
	inline float get__jirwhorso_9() const { return ____jirwhorso_9; }
	inline float* get_address_of__jirwhorso_9() { return &____jirwhorso_9; }
	inline void set__jirwhorso_9(float value)
	{
		____jirwhorso_9 = value;
	}

	inline static int32_t get_offset_of__wellape_10() { return static_cast<int32_t>(offsetof(M_mallseMarjeraw106_t3276170530, ____wellape_10)); }
	inline float get__wellape_10() const { return ____wellape_10; }
	inline float* get_address_of__wellape_10() { return &____wellape_10; }
	inline void set__wellape_10(float value)
	{
		____wellape_10 = value;
	}

	inline static int32_t get_offset_of__gelbe_11() { return static_cast<int32_t>(offsetof(M_mallseMarjeraw106_t3276170530, ____gelbe_11)); }
	inline float get__gelbe_11() const { return ____gelbe_11; }
	inline float* get_address_of__gelbe_11() { return &____gelbe_11; }
	inline void set__gelbe_11(float value)
	{
		____gelbe_11 = value;
	}

	inline static int32_t get_offset_of__zubairWhebea_12() { return static_cast<int32_t>(offsetof(M_mallseMarjeraw106_t3276170530, ____zubairWhebea_12)); }
	inline float get__zubairWhebea_12() const { return ____zubairWhebea_12; }
	inline float* get_address_of__zubairWhebea_12() { return &____zubairWhebea_12; }
	inline void set__zubairWhebea_12(float value)
	{
		____zubairWhebea_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
