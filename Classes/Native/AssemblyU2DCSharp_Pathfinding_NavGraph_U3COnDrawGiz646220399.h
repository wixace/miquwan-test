﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.GraphNode
struct GraphNode_t23612370;
// Pathfinding.PathHandler
struct PathHandler_t918952263;
// Pathfinding.GraphNodeDelegate
struct GraphNodeDelegate_t1466738551;
// Pathfinding.NavGraph
struct NavGraph_t1254319713;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.NavGraph/<OnDrawGizmos>c__AnonStorey116
struct  U3COnDrawGizmosU3Ec__AnonStorey116_t646220399  : public Il2CppObject
{
public:
	// Pathfinding.GraphNode Pathfinding.NavGraph/<OnDrawGizmos>c__AnonStorey116::node
	GraphNode_t23612370 * ___node_0;
	// Pathfinding.PathHandler Pathfinding.NavGraph/<OnDrawGizmos>c__AnonStorey116::data
	PathHandler_t918952263 * ___data_1;
	// Pathfinding.GraphNodeDelegate Pathfinding.NavGraph/<OnDrawGizmos>c__AnonStorey116::del
	GraphNodeDelegate_t1466738551 * ___del_2;
	// Pathfinding.NavGraph Pathfinding.NavGraph/<OnDrawGizmos>c__AnonStorey116::<>f__this
	NavGraph_t1254319713 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_node_0() { return static_cast<int32_t>(offsetof(U3COnDrawGizmosU3Ec__AnonStorey116_t646220399, ___node_0)); }
	inline GraphNode_t23612370 * get_node_0() const { return ___node_0; }
	inline GraphNode_t23612370 ** get_address_of_node_0() { return &___node_0; }
	inline void set_node_0(GraphNode_t23612370 * value)
	{
		___node_0 = value;
		Il2CppCodeGenWriteBarrier(&___node_0, value);
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(U3COnDrawGizmosU3Ec__AnonStorey116_t646220399, ___data_1)); }
	inline PathHandler_t918952263 * get_data_1() const { return ___data_1; }
	inline PathHandler_t918952263 ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(PathHandler_t918952263 * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier(&___data_1, value);
	}

	inline static int32_t get_offset_of_del_2() { return static_cast<int32_t>(offsetof(U3COnDrawGizmosU3Ec__AnonStorey116_t646220399, ___del_2)); }
	inline GraphNodeDelegate_t1466738551 * get_del_2() const { return ___del_2; }
	inline GraphNodeDelegate_t1466738551 ** get_address_of_del_2() { return &___del_2; }
	inline void set_del_2(GraphNodeDelegate_t1466738551 * value)
	{
		___del_2 = value;
		Il2CppCodeGenWriteBarrier(&___del_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3COnDrawGizmosU3Ec__AnonStorey116_t646220399, ___U3CU3Ef__this_3)); }
	inline NavGraph_t1254319713 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline NavGraph_t1254319713 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(NavGraph_t1254319713 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
