﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_keralay37
struct M_keralay37_t2282020575;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_keralay37::.ctor()
extern "C"  void M_keralay37__ctor_m3977481572 (M_keralay37_t2282020575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_keralay37::M_nepo0(System.String[],System.Int32)
extern "C"  void M_keralay37_M_nepo0_m3929165601 (M_keralay37_t2282020575 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
