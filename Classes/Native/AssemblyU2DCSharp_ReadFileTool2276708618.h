﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.Byte[]>
struct Dictionary_2_t786211543;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReadFileTool
struct  ReadFileTool_t2276708618  : public MonoBehaviour_t667441552
{
public:

public:
};

struct ReadFileTool_t2276708618_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Byte[]> ReadFileTool::dataDic
	Dictionary_2_t786211543 * ___dataDic_2;

public:
	inline static int32_t get_offset_of_dataDic_2() { return static_cast<int32_t>(offsetof(ReadFileTool_t2276708618_StaticFields, ___dataDic_2)); }
	inline Dictionary_2_t786211543 * get_dataDic_2() const { return ___dataDic_2; }
	inline Dictionary_2_t786211543 ** get_address_of_dataDic_2() { return &___dataDic_2; }
	inline void set_dataDic_2(Dictionary_2_t786211543 * value)
	{
		___dataDic_2 = value;
		Il2CppCodeGenWriteBarrier(&___dataDic_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
