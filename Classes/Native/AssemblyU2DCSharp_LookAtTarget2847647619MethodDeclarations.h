﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LookAtTarget
struct LookAtTarget_t2847647619;

#include "codegen/il2cpp-codegen.h"

// System.Void LookAtTarget::.ctor()
extern "C"  void LookAtTarget__ctor_m2328694392 (LookAtTarget_t2847647619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LookAtTarget::Update()
extern "C"  void LookAtTarget_Update_m901944213 (LookAtTarget_t2847647619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
