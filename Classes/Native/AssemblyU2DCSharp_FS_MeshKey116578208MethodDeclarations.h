﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FS_MeshKey
struct FS_MeshKey_t116578208;
// UnityEngine.Material
struct Material_t3870600107;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void FS_MeshKey::.ctor(UnityEngine.Material,System.Boolean)
extern "C"  void FS_MeshKey__ctor_m3325563264 (FS_MeshKey_t116578208 * __this, Material_t3870600107 * ___m0, bool ___s1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FS_MeshKey::Equals(System.Object)
extern "C"  bool FS_MeshKey_Equals_m397046648 (FS_MeshKey_t116578208 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FS_MeshKey::GetHashCode()
extern "C"  int32_t FS_MeshKey_GetHashCode_m1282145052 (FS_MeshKey_t116578208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
