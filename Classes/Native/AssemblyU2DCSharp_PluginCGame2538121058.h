﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginCGame
struct  PluginCGame_t2538121058  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginCGame::userId
	String_t* ___userId_2;
	// System.String PluginCGame::token
	String_t* ___token_3;
	// System.String PluginCGame::configId
	String_t* ___configId_4;
	// System.String PluginCGame::appId
	String_t* ___appId_5;
	// System.String PluginCGame::appkey
	String_t* ___appkey_6;
	// Mihua.SDK.PayInfo PluginCGame::iapInfo
	PayInfo_t1775308120 * ___iapInfo_7;

public:
	inline static int32_t get_offset_of_userId_2() { return static_cast<int32_t>(offsetof(PluginCGame_t2538121058, ___userId_2)); }
	inline String_t* get_userId_2() const { return ___userId_2; }
	inline String_t** get_address_of_userId_2() { return &___userId_2; }
	inline void set_userId_2(String_t* value)
	{
		___userId_2 = value;
		Il2CppCodeGenWriteBarrier(&___userId_2, value);
	}

	inline static int32_t get_offset_of_token_3() { return static_cast<int32_t>(offsetof(PluginCGame_t2538121058, ___token_3)); }
	inline String_t* get_token_3() const { return ___token_3; }
	inline String_t** get_address_of_token_3() { return &___token_3; }
	inline void set_token_3(String_t* value)
	{
		___token_3 = value;
		Il2CppCodeGenWriteBarrier(&___token_3, value);
	}

	inline static int32_t get_offset_of_configId_4() { return static_cast<int32_t>(offsetof(PluginCGame_t2538121058, ___configId_4)); }
	inline String_t* get_configId_4() const { return ___configId_4; }
	inline String_t** get_address_of_configId_4() { return &___configId_4; }
	inline void set_configId_4(String_t* value)
	{
		___configId_4 = value;
		Il2CppCodeGenWriteBarrier(&___configId_4, value);
	}

	inline static int32_t get_offset_of_appId_5() { return static_cast<int32_t>(offsetof(PluginCGame_t2538121058, ___appId_5)); }
	inline String_t* get_appId_5() const { return ___appId_5; }
	inline String_t** get_address_of_appId_5() { return &___appId_5; }
	inline void set_appId_5(String_t* value)
	{
		___appId_5 = value;
		Il2CppCodeGenWriteBarrier(&___appId_5, value);
	}

	inline static int32_t get_offset_of_appkey_6() { return static_cast<int32_t>(offsetof(PluginCGame_t2538121058, ___appkey_6)); }
	inline String_t* get_appkey_6() const { return ___appkey_6; }
	inline String_t** get_address_of_appkey_6() { return &___appkey_6; }
	inline void set_appkey_6(String_t* value)
	{
		___appkey_6 = value;
		Il2CppCodeGenWriteBarrier(&___appkey_6, value);
	}

	inline static int32_t get_offset_of_iapInfo_7() { return static_cast<int32_t>(offsetof(PluginCGame_t2538121058, ___iapInfo_7)); }
	inline PayInfo_t1775308120 * get_iapInfo_7() const { return ___iapInfo_7; }
	inline PayInfo_t1775308120 ** get_address_of_iapInfo_7() { return &___iapInfo_7; }
	inline void set_iapInfo_7(PayInfo_t1775308120 * value)
	{
		___iapInfo_7 = value;
		Il2CppCodeGenWriteBarrier(&___iapInfo_7, value);
	}
};

struct PluginCGame_t2538121058_StaticFields
{
public:
	// System.Action PluginCGame::<>f__am$cache6
	Action_t3771233898 * ___U3CU3Ef__amU24cache6_8;
	// System.Action PluginCGame::<>f__am$cache7
	Action_t3771233898 * ___U3CU3Ef__amU24cache7_9;
	// System.Action PluginCGame::<>f__am$cache8
	Action_t3771233898 * ___U3CU3Ef__amU24cache8_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_8() { return static_cast<int32_t>(offsetof(PluginCGame_t2538121058_StaticFields, ___U3CU3Ef__amU24cache6_8)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache6_8() const { return ___U3CU3Ef__amU24cache6_8; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache6_8() { return &___U3CU3Ef__amU24cache6_8; }
	inline void set_U3CU3Ef__amU24cache6_8(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache6_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_9() { return static_cast<int32_t>(offsetof(PluginCGame_t2538121058_StaticFields, ___U3CU3Ef__amU24cache7_9)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache7_9() const { return ___U3CU3Ef__amU24cache7_9; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache7_9() { return &___U3CU3Ef__amU24cache7_9; }
	inline void set_U3CU3Ef__amU24cache7_9(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache7_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_10() { return static_cast<int32_t>(offsetof(PluginCGame_t2538121058_StaticFields, ___U3CU3Ef__amU24cache8_10)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache8_10() const { return ___U3CU3Ef__amU24cache8_10; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache8_10() { return &___U3CU3Ef__amU24cache8_10; }
	inline void set_U3CU3Ef__amU24cache8_10(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache8_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
