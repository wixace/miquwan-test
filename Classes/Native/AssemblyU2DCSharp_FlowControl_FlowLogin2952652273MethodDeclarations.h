﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlowControl.FlowLogin
struct FlowLogin_t2952652273;

#include "codegen/il2cpp-codegen.h"

// System.Void FlowControl.FlowLogin::.ctor()
extern "C"  void FlowLogin__ctor_m2745567535 (FlowLogin_t2952652273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowLogin::Activate()
extern "C"  void FlowLogin_Activate_m3840863336 (FlowLogin_t2952652273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowLogin::OnComplete()
extern "C"  void FlowLogin_OnComplete_m1393275245 (FlowLogin_t2952652273 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
