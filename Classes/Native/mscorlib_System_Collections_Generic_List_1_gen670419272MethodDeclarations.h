﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>
struct List_1_t670419272;
// System.Collections.Generic.IEnumerable`1<Pathfinding.Voxels.VoxelContour>
struct IEnumerable_1_t2603146677;
// System.Collections.Generic.IEnumerator`1<Pathfinding.Voxels.VoxelContour>
struct IEnumerator_1_t1214098769;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Pathfinding.Voxels.VoxelContour>
struct ICollection_1_t196823707;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Voxels.VoxelContour>
struct ReadOnlyCollection_1_t859311256;
// Pathfinding.Voxels.VoxelContour[]
struct VoxelContourU5BU5D_t3554406569;
// System.Predicate`1<Pathfinding.Voxels.VoxelContour>
struct Predicate_1_t3208257899;
// System.Collections.Generic.IComparer`1<Pathfinding.Voxels.VoxelContour>
struct IComparer_1_t1877247762;
// System.Comparison`1<Pathfinding.Voxels.VoxelContour>
struct Comparison_1_t2313562203;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_VoxelContour3597201016.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat690092042.h"

// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::.ctor()
extern "C"  void List_1__ctor_m1450003073_gshared (List_1_t670419272 * __this, const MethodInfo* method);
#define List_1__ctor_m1450003073(__this, method) ((  void (*) (List_1_t670419272 *, const MethodInfo*))List_1__ctor_m1450003073_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m558857054_gshared (List_1_t670419272 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m558857054(__this, ___collection0, method) ((  void (*) (List_1_t670419272 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m558857054_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m4039664635_gshared (List_1_t670419272 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m4039664635(__this, ___capacity0, method) ((  void (*) (List_1_t670419272 *, int32_t, const MethodInfo*))List_1__ctor_m4039664635_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::.cctor()
extern "C"  void List_1__cctor_m1518326092_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1518326092(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m1518326092_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m179622035_gshared (List_1_t670419272 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m179622035(__this, method) ((  Il2CppObject* (*) (List_1_t670419272 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m179622035_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2577906147_gshared (List_1_t670419272 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m2577906147(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t670419272 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2577906147_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1230116894_gshared (List_1_t670419272 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1230116894(__this, method) ((  Il2CppObject * (*) (List_1_t670419272 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1230116894_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m1931221779_gshared (List_1_t670419272 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m1931221779(__this, ___item0, method) ((  int32_t (*) (List_1_t670419272 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m1931221779_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1320227609_gshared (List_1_t670419272 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1320227609(__this, ___item0, method) ((  bool (*) (List_1_t670419272 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1320227609_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2984415211_gshared (List_1_t670419272 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m2984415211(__this, ___item0, method) ((  int32_t (*) (List_1_t670419272 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m2984415211_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m1975871638_gshared (List_1_t670419272 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1975871638(__this, ___index0, ___item1, method) ((  void (*) (List_1_t670419272 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1975871638_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m398591186_gshared (List_1_t670419272 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m398591186(__this, ___item0, method) ((  void (*) (List_1_t670419272 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m398591186_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2293675994_gshared (List_1_t670419272 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2293675994(__this, method) ((  bool (*) (List_1_t670419272 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2293675994_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m1880146339_gshared (List_1_t670419272 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1880146339(__this, method) ((  bool (*) (List_1_t670419272 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1880146339_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m3621952655_gshared (List_1_t670419272 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m3621952655(__this, method) ((  Il2CppObject * (*) (List_1_t670419272 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m3621952655_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m526862664_gshared (List_1_t670419272 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m526862664(__this, method) ((  bool (*) (List_1_t670419272 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m526862664_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m2549214385_gshared (List_1_t670419272 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m2549214385(__this, method) ((  bool (*) (List_1_t670419272 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2549214385_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2838603350_gshared (List_1_t670419272 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2838603350(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t670419272 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2838603350_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m1051051757_gshared (List_1_t670419272 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m1051051757(__this, ___index0, ___value1, method) ((  void (*) (List_1_t670419272 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m1051051757_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::Add(T)
extern "C"  void List_1_Add_m417607322_gshared (List_1_t670419272 * __this, VoxelContour_t3597201016  ___item0, const MethodInfo* method);
#define List_1_Add_m417607322(__this, ___item0, method) ((  void (*) (List_1_t670419272 *, VoxelContour_t3597201016 , const MethodInfo*))List_1_Add_m417607322_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3760420121_gshared (List_1_t670419272 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3760420121(__this, ___newCount0, method) ((  void (*) (List_1_t670419272 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3760420121_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m2102703406_gshared (List_1_t670419272 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m2102703406(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t670419272 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m2102703406_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2752587799_gshared (List_1_t670419272 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2752587799(__this, ___collection0, method) ((  void (*) (List_1_t670419272 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2752587799_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2013560023_gshared (List_1_t670419272 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2013560023(__this, ___enumerable0, method) ((  void (*) (List_1_t670419272 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2013560023_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1373210464_gshared (List_1_t670419272 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m1373210464(__this, ___collection0, method) ((  void (*) (List_1_t670419272 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m1373210464_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t859311256 * List_1_AsReadOnly_m679998025_gshared (List_1_t670419272 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m679998025(__this, method) ((  ReadOnlyCollection_1_t859311256 * (*) (List_1_t670419272 *, const MethodInfo*))List_1_AsReadOnly_m679998025_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m3749110230_gshared (List_1_t670419272 * __this, VoxelContour_t3597201016  ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m3749110230(__this, ___item0, method) ((  int32_t (*) (List_1_t670419272 *, VoxelContour_t3597201016 , const MethodInfo*))List_1_BinarySearch_m3749110230_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::Clear()
extern "C"  void List_1_Clear_m3151103660_gshared (List_1_t670419272 * __this, const MethodInfo* method);
#define List_1_Clear_m3151103660(__this, method) ((  void (*) (List_1_t670419272 *, const MethodInfo*))List_1_Clear_m3151103660_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::Contains(T)
extern "C"  bool List_1_Contains_m1099184730_gshared (List_1_t670419272 * __this, VoxelContour_t3597201016  ___item0, const MethodInfo* method);
#define List_1_Contains_m1099184730(__this, ___item0, method) ((  bool (*) (List_1_t670419272 *, VoxelContour_t3597201016 , const MethodInfo*))List_1_Contains_m1099184730_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m4229320270_gshared (List_1_t670419272 * __this, VoxelContourU5BU5D_t3554406569* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m4229320270(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t670419272 *, VoxelContourU5BU5D_t3554406569*, int32_t, const MethodInfo*))List_1_CopyTo_m4229320270_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::Find(System.Predicate`1<T>)
extern "C"  VoxelContour_t3597201016  List_1_Find_m4189345946_gshared (List_1_t670419272 * __this, Predicate_1_t3208257899 * ___match0, const MethodInfo* method);
#define List_1_Find_m4189345946(__this, ___match0, method) ((  VoxelContour_t3597201016  (*) (List_1_t670419272 *, Predicate_1_t3208257899 *, const MethodInfo*))List_1_Find_m4189345946_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m2042890325_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3208257899 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m2042890325(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3208257899 *, const MethodInfo*))List_1_CheckMatch_m2042890325_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2340378490_gshared (List_1_t670419272 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3208257899 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m2340378490(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t670419272 *, int32_t, int32_t, Predicate_1_t3208257899 *, const MethodInfo*))List_1_GetIndex_m2340378490_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::GetEnumerator()
extern "C"  Enumerator_t690092042  List_1_GetEnumerator_m1841787543_gshared (List_1_t670419272 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1841787543(__this, method) ((  Enumerator_t690092042  (*) (List_1_t670419272 *, const MethodInfo*))List_1_GetEnumerator_m1841787543_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m830672210_gshared (List_1_t670419272 * __this, VoxelContour_t3597201016  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m830672210(__this, ___item0, method) ((  int32_t (*) (List_1_t670419272 *, VoxelContour_t3597201016 , const MethodInfo*))List_1_IndexOf_m830672210_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m4179228325_gshared (List_1_t670419272 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m4179228325(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t670419272 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m4179228325_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m3975687454_gshared (List_1_t670419272 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m3975687454(__this, ___index0, method) ((  void (*) (List_1_t670419272 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3975687454_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m535797829_gshared (List_1_t670419272 * __this, int32_t ___index0, VoxelContour_t3597201016  ___item1, const MethodInfo* method);
#define List_1_Insert_m535797829(__this, ___index0, ___item1, method) ((  void (*) (List_1_t670419272 *, int32_t, VoxelContour_t3597201016 , const MethodInfo*))List_1_Insert_m535797829_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m63688954_gshared (List_1_t670419272 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m63688954(__this, ___collection0, method) ((  void (*) (List_1_t670419272 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m63688954_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::Remove(T)
extern "C"  bool List_1_Remove_m2539112661_gshared (List_1_t670419272 * __this, VoxelContour_t3597201016  ___item0, const MethodInfo* method);
#define List_1_Remove_m2539112661(__this, ___item0, method) ((  bool (*) (List_1_t670419272 *, VoxelContour_t3597201016 , const MethodInfo*))List_1_Remove_m2539112661_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m1661545365_gshared (List_1_t670419272 * __this, Predicate_1_t3208257899 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m1661545365(__this, ___match0, method) ((  int32_t (*) (List_1_t670419272 *, Predicate_1_t3208257899 *, const MethodInfo*))List_1_RemoveAll_m1661545365_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2704617995_gshared (List_1_t670419272 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m2704617995(__this, ___index0, method) ((  void (*) (List_1_t670419272 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2704617995_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m494508078_gshared (List_1_t670419272 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m494508078(__this, ___index0, ___count1, method) ((  void (*) (List_1_t670419272 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m494508078_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::Reverse()
extern "C"  void List_1_Reverse_m1921360513_gshared (List_1_t670419272 * __this, const MethodInfo* method);
#define List_1_Reverse_m1921360513(__this, method) ((  void (*) (List_1_t670419272 *, const MethodInfo*))List_1_Reverse_m1921360513_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::Sort()
extern "C"  void List_1_Sort_m1809814721_gshared (List_1_t670419272 * __this, const MethodInfo* method);
#define List_1_Sort_m1809814721(__this, method) ((  void (*) (List_1_t670419272 *, const MethodInfo*))List_1_Sort_m1809814721_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3769187907_gshared (List_1_t670419272 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m3769187907(__this, ___comparer0, method) ((  void (*) (List_1_t670419272 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3769187907_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m4028836436_gshared (List_1_t670419272 * __this, Comparison_1_t2313562203 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m4028836436(__this, ___comparison0, method) ((  void (*) (List_1_t670419272 *, Comparison_1_t2313562203 *, const MethodInfo*))List_1_Sort_m4028836436_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::ToArray()
extern "C"  VoxelContourU5BU5D_t3554406569* List_1_ToArray_m4193966272_gshared (List_1_t670419272 * __this, const MethodInfo* method);
#define List_1_ToArray_m4193966272(__this, method) ((  VoxelContourU5BU5D_t3554406569* (*) (List_1_t670419272 *, const MethodInfo*))List_1_ToArray_m4193966272_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2896322458_gshared (List_1_t670419272 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2896322458(__this, method) ((  void (*) (List_1_t670419272 *, const MethodInfo*))List_1_TrimExcess_m2896322458_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m1406667906_gshared (List_1_t670419272 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1406667906(__this, method) ((  int32_t (*) (List_1_t670419272 *, const MethodInfo*))List_1_get_Capacity_m1406667906_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m970608939_gshared (List_1_t670419272 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m970608939(__this, ___value0, method) ((  void (*) (List_1_t670419272 *, int32_t, const MethodInfo*))List_1_set_Capacity_m970608939_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::get_Count()
extern "C"  int32_t List_1_get_Count_m4201050336_gshared (List_1_t670419272 * __this, const MethodInfo* method);
#define List_1_get_Count_m4201050336(__this, method) ((  int32_t (*) (List_1_t670419272 *, const MethodInfo*))List_1_get_Count_m4201050336_gshared)(__this, method)
// T System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::get_Item(System.Int32)
extern "C"  VoxelContour_t3597201016  List_1_get_Item_m4079545909_gshared (List_1_t670419272 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m4079545909(__this, ___index0, method) ((  VoxelContour_t3597201016  (*) (List_1_t670419272 *, int32_t, const MethodInfo*))List_1_get_Item_m4079545909_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m2312131612_gshared (List_1_t670419272 * __this, int32_t ___index0, VoxelContour_t3597201016  ___value1, const MethodInfo* method);
#define List_1_set_Item_m2312131612(__this, ___index0, ___value1, method) ((  void (*) (List_1_t670419272 *, int32_t, VoxelContour_t3597201016 , const MethodInfo*))List_1_set_Item_m2312131612_gshared)(__this, ___index0, ___value1, method)
