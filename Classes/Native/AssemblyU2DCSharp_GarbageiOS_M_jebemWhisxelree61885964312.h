﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_jebemWhisxelree61
struct  M_jebemWhisxelree61_t885964312  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_jebemWhisxelree61::_whooru
	String_t* ____whooru_0;
	// System.Int32 GarbageiOS.M_jebemWhisxelree61::_relkisu
	int32_t ____relkisu_1;
	// System.String GarbageiOS.M_jebemWhisxelree61::_sowxelho
	String_t* ____sowxelho_2;
	// System.UInt32 GarbageiOS.M_jebemWhisxelree61::_gusawjisRamaw
	uint32_t ____gusawjisRamaw_3;
	// System.Single GarbageiOS.M_jebemWhisxelree61::_yekooHowbitras
	float ____yekooHowbitras_4;
	// System.UInt32 GarbageiOS.M_jebemWhisxelree61::_rawjoumal
	uint32_t ____rawjoumal_5;
	// System.String GarbageiOS.M_jebemWhisxelree61::_heargee
	String_t* ____heargee_6;
	// System.Int32 GarbageiOS.M_jebemWhisxelree61::_gartriBarsargi
	int32_t ____gartriBarsargi_7;
	// System.String GarbageiOS.M_jebemWhisxelree61::_damitor
	String_t* ____damitor_8;
	// System.Single GarbageiOS.M_jebemWhisxelree61::_beldaSerousou
	float ____beldaSerousou_9;
	// System.String GarbageiOS.M_jebemWhisxelree61::_sordegairTouremse
	String_t* ____sordegairTouremse_10;
	// System.Int32 GarbageiOS.M_jebemWhisxelree61::_tomisairRezerede
	int32_t ____tomisairRezerede_11;
	// System.Int32 GarbageiOS.M_jebemWhisxelree61::_jocooNowmou
	int32_t ____jocooNowmou_12;
	// System.String GarbageiOS.M_jebemWhisxelree61::_juhe
	String_t* ____juhe_13;
	// System.Int32 GarbageiOS.M_jebemWhisxelree61::_semeKalju
	int32_t ____semeKalju_14;
	// System.Int32 GarbageiOS.M_jebemWhisxelree61::_tuvokir
	int32_t ____tuvokir_15;
	// System.Single GarbageiOS.M_jebemWhisxelree61::_pouseseMayzouchoo
	float ____pouseseMayzouchoo_16;
	// System.String GarbageiOS.M_jebemWhisxelree61::_sallkeeroWhaxawa
	String_t* ____sallkeeroWhaxawa_17;
	// System.Single GarbageiOS.M_jebemWhisxelree61::_sonaiPaltralltay
	float ____sonaiPaltralltay_18;
	// System.String GarbageiOS.M_jebemWhisxelree61::_tedrowrisTuwhirdre
	String_t* ____tedrowrisTuwhirdre_19;
	// System.Boolean GarbageiOS.M_jebemWhisxelree61::_meeda
	bool ____meeda_20;

public:
	inline static int32_t get_offset_of__whooru_0() { return static_cast<int32_t>(offsetof(M_jebemWhisxelree61_t885964312, ____whooru_0)); }
	inline String_t* get__whooru_0() const { return ____whooru_0; }
	inline String_t** get_address_of__whooru_0() { return &____whooru_0; }
	inline void set__whooru_0(String_t* value)
	{
		____whooru_0 = value;
		Il2CppCodeGenWriteBarrier(&____whooru_0, value);
	}

	inline static int32_t get_offset_of__relkisu_1() { return static_cast<int32_t>(offsetof(M_jebemWhisxelree61_t885964312, ____relkisu_1)); }
	inline int32_t get__relkisu_1() const { return ____relkisu_1; }
	inline int32_t* get_address_of__relkisu_1() { return &____relkisu_1; }
	inline void set__relkisu_1(int32_t value)
	{
		____relkisu_1 = value;
	}

	inline static int32_t get_offset_of__sowxelho_2() { return static_cast<int32_t>(offsetof(M_jebemWhisxelree61_t885964312, ____sowxelho_2)); }
	inline String_t* get__sowxelho_2() const { return ____sowxelho_2; }
	inline String_t** get_address_of__sowxelho_2() { return &____sowxelho_2; }
	inline void set__sowxelho_2(String_t* value)
	{
		____sowxelho_2 = value;
		Il2CppCodeGenWriteBarrier(&____sowxelho_2, value);
	}

	inline static int32_t get_offset_of__gusawjisRamaw_3() { return static_cast<int32_t>(offsetof(M_jebemWhisxelree61_t885964312, ____gusawjisRamaw_3)); }
	inline uint32_t get__gusawjisRamaw_3() const { return ____gusawjisRamaw_3; }
	inline uint32_t* get_address_of__gusawjisRamaw_3() { return &____gusawjisRamaw_3; }
	inline void set__gusawjisRamaw_3(uint32_t value)
	{
		____gusawjisRamaw_3 = value;
	}

	inline static int32_t get_offset_of__yekooHowbitras_4() { return static_cast<int32_t>(offsetof(M_jebemWhisxelree61_t885964312, ____yekooHowbitras_4)); }
	inline float get__yekooHowbitras_4() const { return ____yekooHowbitras_4; }
	inline float* get_address_of__yekooHowbitras_4() { return &____yekooHowbitras_4; }
	inline void set__yekooHowbitras_4(float value)
	{
		____yekooHowbitras_4 = value;
	}

	inline static int32_t get_offset_of__rawjoumal_5() { return static_cast<int32_t>(offsetof(M_jebemWhisxelree61_t885964312, ____rawjoumal_5)); }
	inline uint32_t get__rawjoumal_5() const { return ____rawjoumal_5; }
	inline uint32_t* get_address_of__rawjoumal_5() { return &____rawjoumal_5; }
	inline void set__rawjoumal_5(uint32_t value)
	{
		____rawjoumal_5 = value;
	}

	inline static int32_t get_offset_of__heargee_6() { return static_cast<int32_t>(offsetof(M_jebemWhisxelree61_t885964312, ____heargee_6)); }
	inline String_t* get__heargee_6() const { return ____heargee_6; }
	inline String_t** get_address_of__heargee_6() { return &____heargee_6; }
	inline void set__heargee_6(String_t* value)
	{
		____heargee_6 = value;
		Il2CppCodeGenWriteBarrier(&____heargee_6, value);
	}

	inline static int32_t get_offset_of__gartriBarsargi_7() { return static_cast<int32_t>(offsetof(M_jebemWhisxelree61_t885964312, ____gartriBarsargi_7)); }
	inline int32_t get__gartriBarsargi_7() const { return ____gartriBarsargi_7; }
	inline int32_t* get_address_of__gartriBarsargi_7() { return &____gartriBarsargi_7; }
	inline void set__gartriBarsargi_7(int32_t value)
	{
		____gartriBarsargi_7 = value;
	}

	inline static int32_t get_offset_of__damitor_8() { return static_cast<int32_t>(offsetof(M_jebemWhisxelree61_t885964312, ____damitor_8)); }
	inline String_t* get__damitor_8() const { return ____damitor_8; }
	inline String_t** get_address_of__damitor_8() { return &____damitor_8; }
	inline void set__damitor_8(String_t* value)
	{
		____damitor_8 = value;
		Il2CppCodeGenWriteBarrier(&____damitor_8, value);
	}

	inline static int32_t get_offset_of__beldaSerousou_9() { return static_cast<int32_t>(offsetof(M_jebemWhisxelree61_t885964312, ____beldaSerousou_9)); }
	inline float get__beldaSerousou_9() const { return ____beldaSerousou_9; }
	inline float* get_address_of__beldaSerousou_9() { return &____beldaSerousou_9; }
	inline void set__beldaSerousou_9(float value)
	{
		____beldaSerousou_9 = value;
	}

	inline static int32_t get_offset_of__sordegairTouremse_10() { return static_cast<int32_t>(offsetof(M_jebemWhisxelree61_t885964312, ____sordegairTouremse_10)); }
	inline String_t* get__sordegairTouremse_10() const { return ____sordegairTouremse_10; }
	inline String_t** get_address_of__sordegairTouremse_10() { return &____sordegairTouremse_10; }
	inline void set__sordegairTouremse_10(String_t* value)
	{
		____sordegairTouremse_10 = value;
		Il2CppCodeGenWriteBarrier(&____sordegairTouremse_10, value);
	}

	inline static int32_t get_offset_of__tomisairRezerede_11() { return static_cast<int32_t>(offsetof(M_jebemWhisxelree61_t885964312, ____tomisairRezerede_11)); }
	inline int32_t get__tomisairRezerede_11() const { return ____tomisairRezerede_11; }
	inline int32_t* get_address_of__tomisairRezerede_11() { return &____tomisairRezerede_11; }
	inline void set__tomisairRezerede_11(int32_t value)
	{
		____tomisairRezerede_11 = value;
	}

	inline static int32_t get_offset_of__jocooNowmou_12() { return static_cast<int32_t>(offsetof(M_jebemWhisxelree61_t885964312, ____jocooNowmou_12)); }
	inline int32_t get__jocooNowmou_12() const { return ____jocooNowmou_12; }
	inline int32_t* get_address_of__jocooNowmou_12() { return &____jocooNowmou_12; }
	inline void set__jocooNowmou_12(int32_t value)
	{
		____jocooNowmou_12 = value;
	}

	inline static int32_t get_offset_of__juhe_13() { return static_cast<int32_t>(offsetof(M_jebemWhisxelree61_t885964312, ____juhe_13)); }
	inline String_t* get__juhe_13() const { return ____juhe_13; }
	inline String_t** get_address_of__juhe_13() { return &____juhe_13; }
	inline void set__juhe_13(String_t* value)
	{
		____juhe_13 = value;
		Il2CppCodeGenWriteBarrier(&____juhe_13, value);
	}

	inline static int32_t get_offset_of__semeKalju_14() { return static_cast<int32_t>(offsetof(M_jebemWhisxelree61_t885964312, ____semeKalju_14)); }
	inline int32_t get__semeKalju_14() const { return ____semeKalju_14; }
	inline int32_t* get_address_of__semeKalju_14() { return &____semeKalju_14; }
	inline void set__semeKalju_14(int32_t value)
	{
		____semeKalju_14 = value;
	}

	inline static int32_t get_offset_of__tuvokir_15() { return static_cast<int32_t>(offsetof(M_jebemWhisxelree61_t885964312, ____tuvokir_15)); }
	inline int32_t get__tuvokir_15() const { return ____tuvokir_15; }
	inline int32_t* get_address_of__tuvokir_15() { return &____tuvokir_15; }
	inline void set__tuvokir_15(int32_t value)
	{
		____tuvokir_15 = value;
	}

	inline static int32_t get_offset_of__pouseseMayzouchoo_16() { return static_cast<int32_t>(offsetof(M_jebemWhisxelree61_t885964312, ____pouseseMayzouchoo_16)); }
	inline float get__pouseseMayzouchoo_16() const { return ____pouseseMayzouchoo_16; }
	inline float* get_address_of__pouseseMayzouchoo_16() { return &____pouseseMayzouchoo_16; }
	inline void set__pouseseMayzouchoo_16(float value)
	{
		____pouseseMayzouchoo_16 = value;
	}

	inline static int32_t get_offset_of__sallkeeroWhaxawa_17() { return static_cast<int32_t>(offsetof(M_jebemWhisxelree61_t885964312, ____sallkeeroWhaxawa_17)); }
	inline String_t* get__sallkeeroWhaxawa_17() const { return ____sallkeeroWhaxawa_17; }
	inline String_t** get_address_of__sallkeeroWhaxawa_17() { return &____sallkeeroWhaxawa_17; }
	inline void set__sallkeeroWhaxawa_17(String_t* value)
	{
		____sallkeeroWhaxawa_17 = value;
		Il2CppCodeGenWriteBarrier(&____sallkeeroWhaxawa_17, value);
	}

	inline static int32_t get_offset_of__sonaiPaltralltay_18() { return static_cast<int32_t>(offsetof(M_jebemWhisxelree61_t885964312, ____sonaiPaltralltay_18)); }
	inline float get__sonaiPaltralltay_18() const { return ____sonaiPaltralltay_18; }
	inline float* get_address_of__sonaiPaltralltay_18() { return &____sonaiPaltralltay_18; }
	inline void set__sonaiPaltralltay_18(float value)
	{
		____sonaiPaltralltay_18 = value;
	}

	inline static int32_t get_offset_of__tedrowrisTuwhirdre_19() { return static_cast<int32_t>(offsetof(M_jebemWhisxelree61_t885964312, ____tedrowrisTuwhirdre_19)); }
	inline String_t* get__tedrowrisTuwhirdre_19() const { return ____tedrowrisTuwhirdre_19; }
	inline String_t** get_address_of__tedrowrisTuwhirdre_19() { return &____tedrowrisTuwhirdre_19; }
	inline void set__tedrowrisTuwhirdre_19(String_t* value)
	{
		____tedrowrisTuwhirdre_19 = value;
		Il2CppCodeGenWriteBarrier(&____tedrowrisTuwhirdre_19, value);
	}

	inline static int32_t get_offset_of__meeda_20() { return static_cast<int32_t>(offsetof(M_jebemWhisxelree61_t885964312, ____meeda_20)); }
	inline bool get__meeda_20() const { return ____meeda_20; }
	inline bool* get_address_of__meeda_20() { return &____meeda_20; }
	inline void set__meeda_20(bool value)
	{
		____meeda_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
