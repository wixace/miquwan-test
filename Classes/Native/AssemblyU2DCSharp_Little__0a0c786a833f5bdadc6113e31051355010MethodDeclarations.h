﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._0a0c786a833f5bdadc6113e3ed2fc5f8
struct _0a0c786a833f5bdadc6113e3ed2fc5f8_t1051355010;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__0a0c786a833f5bdadc6113e31051355010.h"

// System.Void Little._0a0c786a833f5bdadc6113e3ed2fc5f8::.ctor()
extern "C"  void _0a0c786a833f5bdadc6113e3ed2fc5f8__ctor_m4129820907 (_0a0c786a833f5bdadc6113e3ed2fc5f8_t1051355010 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._0a0c786a833f5bdadc6113e3ed2fc5f8::_0a0c786a833f5bdadc6113e3ed2fc5f8m2(System.Int32)
extern "C"  int32_t _0a0c786a833f5bdadc6113e3ed2fc5f8__0a0c786a833f5bdadc6113e3ed2fc5f8m2_m2498679385 (_0a0c786a833f5bdadc6113e3ed2fc5f8_t1051355010 * __this, int32_t ____0a0c786a833f5bdadc6113e3ed2fc5f8a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._0a0c786a833f5bdadc6113e3ed2fc5f8::_0a0c786a833f5bdadc6113e3ed2fc5f8m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _0a0c786a833f5bdadc6113e3ed2fc5f8__0a0c786a833f5bdadc6113e3ed2fc5f8m_m1636576381 (_0a0c786a833f5bdadc6113e3ed2fc5f8_t1051355010 * __this, int32_t ____0a0c786a833f5bdadc6113e3ed2fc5f8a0, int32_t ____0a0c786a833f5bdadc6113e3ed2fc5f8861, int32_t ____0a0c786a833f5bdadc6113e3ed2fc5f8c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._0a0c786a833f5bdadc6113e3ed2fc5f8::ilo__0a0c786a833f5bdadc6113e3ed2fc5f8m21(Little._0a0c786a833f5bdadc6113e3ed2fc5f8,System.Int32)
extern "C"  int32_t _0a0c786a833f5bdadc6113e3ed2fc5f8_ilo__0a0c786a833f5bdadc6113e3ed2fc5f8m21_m1557627785 (Il2CppObject * __this /* static, unused */, _0a0c786a833f5bdadc6113e3ed2fc5f8_t1051355010 * ____this0, int32_t ____0a0c786a833f5bdadc6113e3ed2fc5f8a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
