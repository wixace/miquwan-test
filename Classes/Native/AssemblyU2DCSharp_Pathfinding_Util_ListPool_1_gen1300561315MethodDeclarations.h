﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Pathfinding.IntRect>
struct List_1_t88276517;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Util.ListPool`1<Pathfinding.IntRect>::.cctor()
extern "C"  void ListPool_1__cctor_m1103718614_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1__cctor_m1103718614(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m1103718614_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<Pathfinding.IntRect>::Claim()
extern "C"  List_1_t88276517 * ListPool_1_Claim_m1160790684_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_Claim_m1160790684(__this /* static, unused */, method) ((  List_1_t88276517 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_Claim_m1160790684_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<Pathfinding.IntRect>::Claim(System.Int32)
extern "C"  List_1_t88276517 * ListPool_1_Claim_m860024301_gshared (Il2CppObject * __this /* static, unused */, int32_t ___capacity0, const MethodInfo* method);
#define ListPool_1_Claim_m860024301(__this /* static, unused */, ___capacity0, method) ((  List_1_t88276517 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))ListPool_1_Claim_m860024301_gshared)(__this /* static, unused */, ___capacity0, method)
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.IntRect>::Warmup(System.Int32,System.Int32)
extern "C"  void ListPool_1_Warmup_m1183603257_gshared (Il2CppObject * __this /* static, unused */, int32_t ___count0, int32_t ___size1, const MethodInfo* method);
#define ListPool_1_Warmup_m1183603257(__this /* static, unused */, ___count0, ___size1, method) ((  void (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))ListPool_1_Warmup_m1183603257_gshared)(__this /* static, unused */, ___count0, ___size1, method)
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.IntRect>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m2094794356_gshared (Il2CppObject * __this /* static, unused */, List_1_t88276517 * ___list0, const MethodInfo* method);
#define ListPool_1_Release_m2094794356(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, List_1_t88276517 *, const MethodInfo*))ListPool_1_Release_m2094794356_gshared)(__this /* static, unused */, ___list0, method)
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.IntRect>::Clear()
extern "C"  void ListPool_1_Clear_m3969013218_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_Clear_m3969013218(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_Clear_m3969013218_gshared)(__this /* static, unused */, method)
// System.Int32 Pathfinding.Util.ListPool`1<Pathfinding.IntRect>::GetSize()
extern "C"  int32_t ListPool_1_GetSize_m1674812862_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_GetSize_m1674812862(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_GetSize_m1674812862_gshared)(__this /* static, unused */, method)
