﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_FindAll_GetDelegate_member14_arg0>c__AnonStorey81`1<System.Object>
struct U3CListA1_FindAll_GetDelegate_member14_arg0U3Ec__AnonStorey81_1_t3940615295;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_FindAll_GetDelegate_member14_arg0>c__AnonStorey81`1<System.Object>::.ctor()
extern "C"  void U3CListA1_FindAll_GetDelegate_member14_arg0U3Ec__AnonStorey81_1__ctor_m3056565102_gshared (U3CListA1_FindAll_GetDelegate_member14_arg0U3Ec__AnonStorey81_1_t3940615295 * __this, const MethodInfo* method);
#define U3CListA1_FindAll_GetDelegate_member14_arg0U3Ec__AnonStorey81_1__ctor_m3056565102(__this, method) ((  void (*) (U3CListA1_FindAll_GetDelegate_member14_arg0U3Ec__AnonStorey81_1_t3940615295 *, const MethodInfo*))U3CListA1_FindAll_GetDelegate_member14_arg0U3Ec__AnonStorey81_1__ctor_m3056565102_gshared)(__this, method)
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_FindAll_GetDelegate_member14_arg0>c__AnonStorey81`1<System.Object>::<>m__A6(T)
extern "C"  bool U3CListA1_FindAll_GetDelegate_member14_arg0U3Ec__AnonStorey81_1_U3CU3Em__A6_m3395208478_gshared (U3CListA1_FindAll_GetDelegate_member14_arg0U3Ec__AnonStorey81_1_t3940615295 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CListA1_FindAll_GetDelegate_member14_arg0U3Ec__AnonStorey81_1_U3CU3Em__A6_m3395208478(__this, ___obj0, method) ((  bool (*) (U3CListA1_FindAll_GetDelegate_member14_arg0U3Ec__AnonStorey81_1_t3940615295 *, Il2CppObject *, const MethodInfo*))U3CListA1_FindAll_GetDelegate_member14_arg0U3Ec__AnonStorey81_1_U3CU3Em__A6_m3395208478_gshared)(__this, ___obj0, method)
