﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginUC
struct PluginUC_t2499992865;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t696267445;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.Object
struct Il2CppObject;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// VersionInfo
struct VersionInfo_t2356638086;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_595048151.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginUC::.ctor()
extern "C"  void PluginUC__ctor_m3655210650 (PluginUC_t2499992865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginUC::Init()
extern "C"  void PluginUC_Init_m207993978 (PluginUC_t2499992865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginUC::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginUC_ReqSDKHttpLogin_m1110036335 (PluginUC_t2499992865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginUC::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginUC_IsLoginSuccess_m1964774777 (PluginUC_t2499992865 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginUC::OpenUserLogin()
extern "C"  void PluginUC_OpenUserLogin_m1749810924 (PluginUC_t2499992865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginUC::GetSign(System.Collections.Generic.Dictionary`2<System.String,System.Object>,System.String)
extern "C"  String_t* PluginUC_GetSign_m3347325763 (PluginUC_t2499992865 * __this, Dictionary_2_t696267445 * ___reqMap0, String_t* ___apiKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginUC::UserPay(CEvent.ZEvent)
extern "C"  void PluginUC_UserPay_m3235066630 (PluginUC_t2499992865 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginUC::UpGameInfo(CEvent.ZEvent)
extern "C"  void PluginUC_UpGameInfo_m4132308790 (PluginUC_t2499992865 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginUC::onInitSucc()
extern "C"  void PluginUC_onInitSucc_m1264057019 (PluginUC_t2499992865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginUC::onInitFailed(System.String)
extern "C"  void PluginUC_onInitFailed_m889154252 (PluginUC_t2499992865 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginUC::onLoginSucc(System.String)
extern "C"  void PluginUC_onLoginSucc_m1276478398 (PluginUC_t2499992865 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginUC::onLoginFailed(System.String)
extern "C"  void PluginUC_onLoginFailed_m424360099 (PluginUC_t2499992865 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginUC::OnLogoutSuccess()
extern "C"  void PluginUC_OnLogoutSuccess_m1507263474 (PluginUC_t2499992865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginUC::onLogoutFailed(System.String)
extern "C"  void PluginUC_onLogoutFailed_m1936227474 (PluginUC_t2499992865 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginUC::onExitSucc()
extern "C"  void PluginUC_onExitSucc_m1155435593 (PluginUC_t2499992865 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginUC::onExitCanceled(System.String)
extern "C"  void PluginUC_onExitCanceled_m4263579938 (PluginUC_t2499992865 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginUC::onCreateOrderSucc(System.String)
extern "C"  void PluginUC_onCreateOrderSucc_m2910530261 (PluginUC_t2499992865 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginUC::onPayUserExit(System.String)
extern "C"  void PluginUC_onPayUserExit_m30420472 (PluginUC_t2499992865 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginUC::OnUserSwitchSuccess(System.String)
extern "C"  void PluginUC_OnUserSwitchSuccess_m3595548645 (PluginUC_t2499992865 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginUC::<GetSign>m__45B(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern "C"  String_t* PluginUC_U3CGetSignU3Em__45B_m2653748901 (Il2CppObject * __this /* static, unused */, KeyValuePair_2_t595048151  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginUC::<GetSign>m__45C(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern "C"  String_t* PluginUC_U3CGetSignU3Em__45C_m1698227622 (Il2CppObject * __this /* static, unused */, KeyValuePair_2_t595048151  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PluginUC::<GetSign>m__45D(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern "C"  Il2CppObject * PluginUC_U3CGetSignU3Em__45D_m3283965013 (Il2CppObject * __this /* static, unused */, KeyValuePair_2_t595048151  ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginUC::<OnLogoutSuccess>m__45E()
extern "C"  void PluginUC_U3COnLogoutSuccessU3Em__45E_m2070117487 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginUC::<OnUserSwitchSuccess>m__45F()
extern "C"  void PluginUC_U3COnUserSwitchSuccessU3Em__45F_m3613721221 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginUC::ilo_get_Instance1()
extern "C"  VersionMgr_t1322950208 * PluginUC_ilo_get_Instance1_m4163735131 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginUC::ilo_get_DeviceID2(PluginsSdkMgr)
extern "C"  String_t* PluginUC_ilo_get_DeviceID2_m1175681146 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginUC::ilo_get_currentVS3(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginUC_ilo_get_currentVS3_m3879144144 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginUC::ilo_Log4(System.Object,System.Boolean)
extern "C"  void PluginUC_ilo_Log4_m3378927158 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginUC::ilo_Logout5(System.Action)
extern "C"  void PluginUC_ilo_Logout5_m1281775789 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginUC::ilo_DispatchEvent6(CEvent.ZEvent)
extern "C"  void PluginUC_ilo_DispatchEvent6_m688141064 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
