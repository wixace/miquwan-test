﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginInFox
struct  PluginInFox_t2544798167  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginInFox::openid
	String_t* ___openid_5;
	// System.String PluginInFox::token
	String_t* ___token_6;
	// System.String PluginInFox::sign
	String_t* ___sign_7;
	// System.String PluginInFox::configId
	String_t* ___configId_8;
	// System.String PluginInFox::APPID
	String_t* ___APPID_9;

public:
	inline static int32_t get_offset_of_openid_5() { return static_cast<int32_t>(offsetof(PluginInFox_t2544798167, ___openid_5)); }
	inline String_t* get_openid_5() const { return ___openid_5; }
	inline String_t** get_address_of_openid_5() { return &___openid_5; }
	inline void set_openid_5(String_t* value)
	{
		___openid_5 = value;
		Il2CppCodeGenWriteBarrier(&___openid_5, value);
	}

	inline static int32_t get_offset_of_token_6() { return static_cast<int32_t>(offsetof(PluginInFox_t2544798167, ___token_6)); }
	inline String_t* get_token_6() const { return ___token_6; }
	inline String_t** get_address_of_token_6() { return &___token_6; }
	inline void set_token_6(String_t* value)
	{
		___token_6 = value;
		Il2CppCodeGenWriteBarrier(&___token_6, value);
	}

	inline static int32_t get_offset_of_sign_7() { return static_cast<int32_t>(offsetof(PluginInFox_t2544798167, ___sign_7)); }
	inline String_t* get_sign_7() const { return ___sign_7; }
	inline String_t** get_address_of_sign_7() { return &___sign_7; }
	inline void set_sign_7(String_t* value)
	{
		___sign_7 = value;
		Il2CppCodeGenWriteBarrier(&___sign_7, value);
	}

	inline static int32_t get_offset_of_configId_8() { return static_cast<int32_t>(offsetof(PluginInFox_t2544798167, ___configId_8)); }
	inline String_t* get_configId_8() const { return ___configId_8; }
	inline String_t** get_address_of_configId_8() { return &___configId_8; }
	inline void set_configId_8(String_t* value)
	{
		___configId_8 = value;
		Il2CppCodeGenWriteBarrier(&___configId_8, value);
	}

	inline static int32_t get_offset_of_APPID_9() { return static_cast<int32_t>(offsetof(PluginInFox_t2544798167, ___APPID_9)); }
	inline String_t* get_APPID_9() const { return ___APPID_9; }
	inline String_t** get_address_of_APPID_9() { return &___APPID_9; }
	inline void set_APPID_9(String_t* value)
	{
		___APPID_9 = value;
		Il2CppCodeGenWriteBarrier(&___APPID_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
