﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIWidget
struct UIWidget_t769069560;
// UIDrawCall/OnRenderCallback
struct OnRenderCallback_t2651898963;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.Material
struct Material_t3870600107;
// UnityEngine.Texture
struct Texture_t2526458961;
// UnityEngine.Shader
struct Shader_t3191267369;
// UnityEngine.Transform
struct Transform_t1659122786;
// UIPanel
struct UIPanel_t295209936;
// BetterList`1<UnityEngine.Vector3>
struct BetterList_1_t1484067282;
// BetterList`1<UnityEngine.Vector2>
struct BetterList_1_t1484067281;
// BetterList`1<UnityEngine.Color32>
struct BetterList_1_t2095821700;
// BetterList`1<UnityEngine.Vector4>
struct BetterList_1_t1484067283;
// UIRect
struct UIRect_t2503437976;
// UnityEngine.Behaviour
struct Behaviour_t200106419;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UIRect/AnchorPoint
struct AnchorPoint_t3581172420;
// UIGeometry
struct UIGeometry_t3586695974;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIDrawCall_OnRenderCallback2651898963.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_UIWidget_Pivot240933195.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Shader3191267369.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "AssemblyU2DCSharp_UIRect2503437976.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"
#include "UnityEngine_UnityEngine_Behaviour200106419.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_UIRect_AnchorPoint3581172420.h"
#include "AssemblyU2DCSharp_UIGeometry3586695974.h"

// System.Void UIWidget::.ctor()
extern "C"  void UIWidget__ctor_m70411171 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIDrawCall/OnRenderCallback UIWidget::get_onRender()
extern "C"  OnRenderCallback_t2651898963 * UIWidget_get_onRender_m4268807263 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::set_onRender(UIDrawCall/OnRenderCallback)
extern "C"  void UIWidget_set_onRender_m3581219820 (UIWidget_t769069560 * __this, OnRenderCallback_t2651898963 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UIWidget::get_drawRegion()
extern "C"  Vector4_t4282066567  UIWidget_get_drawRegion_m382413595 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::set_drawRegion(UnityEngine.Vector4)
extern "C"  void UIWidget_set_drawRegion_m231796482 (UIWidget_t769069560 * __this, Vector4_t4282066567  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UIWidget::get_pivotOffset()
extern "C"  Vector2_t4282066565  UIWidget_get_pivotOffset_m2000981938 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIWidget::get_width()
extern "C"  int32_t UIWidget_get_width_m293857264 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::set_width(System.Int32)
extern "C"  void UIWidget_set_width_m3480390811 (UIWidget_t769069560 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIWidget::get_height()
extern "C"  int32_t UIWidget_get_height_m1023454943 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::set_height(System.Int32)
extern "C"  void UIWidget_set_height_m1838784918 (UIWidget_t769069560 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UIWidget::get_color()
extern "C"  Color_t4194546905  UIWidget_get_color_m2224265652 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::set_color(UnityEngine.Color)
extern "C"  void UIWidget_set_color_m1905035359 (UIWidget_t769069560 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIWidget::get_alpha()
extern "C"  float UIWidget_get_alpha_m3456740746 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::set_alpha(System.Single)
extern "C"  void UIWidget_set_alpha_m3960663305 (UIWidget_t769069560 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidget::get_isVisible()
extern "C"  bool UIWidget_get_isVisible_m4257222444 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidget::get_hasVertices()
extern "C"  bool UIWidget_get_hasVertices_m262926455 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget/Pivot UIWidget::get_rawPivot()
extern "C"  int32_t UIWidget_get_rawPivot_m978044726 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::set_rawPivot(UIWidget/Pivot)
extern "C"  void UIWidget_set_rawPivot_m1762094445 (UIWidget_t769069560 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget/Pivot UIWidget::get_pivot()
extern "C"  int32_t UIWidget_get_pivot_m3551772392 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::set_pivot(UIWidget/Pivot)
extern "C"  void UIWidget_set_pivot_m2063605531 (UIWidget_t769069560 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIWidget::get_depth()
extern "C"  int32_t UIWidget_get_depth_m507722157 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::set_depth(System.Int32)
extern "C"  void UIWidget_set_depth_m1098656472 (UIWidget_t769069560 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIWidget::get_raycastDepth()
extern "C"  int32_t UIWidget_get_raycastDepth_m697183634 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UIWidget::get_localCorners()
extern "C"  Vector3U5BU5D_t215400611* UIWidget_get_localCorners_m4123646229 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UIWidget::get_localSize()
extern "C"  Vector2_t4282066565  UIWidget_get_localSize_m237983625 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIWidget::get_localCenter()
extern "C"  Vector3_t4282066566  UIWidget_get_localCenter_m64081310 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UIWidget::get_worldCorners()
extern "C"  Vector3U5BU5D_t215400611* UIWidget_get_worldCorners_m3779221710 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIWidget::get_worldCenter()
extern "C"  Vector3_t4282066566  UIWidget_get_worldCenter_m3655201477 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UIWidget::get_drawingDimensions()
extern "C"  Vector4_t4282066567  UIWidget_get_drawingDimensions_m3426169802 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UIWidget::get_material()
extern "C"  Material_t3870600107 * UIWidget_get_material_m1008364976 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::set_material(UnityEngine.Material)
extern "C"  void UIWidget_set_material_m1657715431 (UIWidget_t769069560 * __this, Material_t3870600107 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UIWidget::get_mainTexture()
extern "C"  Texture_t2526458961 * UIWidget_get_mainTexture_m1554632171 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::set_mainTexture(UnityEngine.Texture)
extern "C"  void UIWidget_set_mainTexture_m1243757896 (UIWidget_t769069560 * __this, Texture_t2526458961 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Shader UIWidget::get_shader()
extern "C"  Shader_t3191267369 * UIWidget_get_shader_m1849353712 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::set_shader(UnityEngine.Shader)
extern "C"  void UIWidget_set_shader_m1936742439 (UIWidget_t769069560 * __this, Shader_t3191267369 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UIWidget::get_relativeSize()
extern "C"  Vector2_t4282066565  UIWidget_get_relativeSize_m726361522 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidget::get_hasBoxCollider()
extern "C"  bool UIWidget_get_hasBoxCollider_m2767222083 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::SetDimensions(System.Int32,System.Int32)
extern "C"  void UIWidget_SetDimensions_m163939926 (UIWidget_t769069560 * __this, int32_t ___w0, int32_t ___h1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UIWidget::GetSides(UnityEngine.Transform)
extern "C"  Vector3U5BU5D_t215400611* UIWidget_GetSides_m715844286 (UIWidget_t769069560 * __this, Transform_t1659122786 * ___relativeTo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIWidget::CalculateFinalAlpha(System.Int32)
extern "C"  float UIWidget_CalculateFinalAlpha_m3596038484 (UIWidget_t769069560 * __this, int32_t ___frameID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::UpdateFinalAlpha(System.Int32)
extern "C"  void UIWidget_UpdateFinalAlpha_m4090184323 (UIWidget_t769069560 * __this, int32_t ___frameID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::Invalidate(System.Boolean)
extern "C"  void UIWidget_Invalidate_m466609203 (UIWidget_t769069560 * __this, bool ___includeChildren0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIWidget::CalculateCumulativeAlpha(System.Int32)
extern "C"  float UIWidget_CalculateCumulativeAlpha_m2518513923 (UIWidget_t769069560 * __this, int32_t ___frameID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::SetRect(System.Single,System.Single,System.Single,System.Single)
extern "C"  void UIWidget_SetRect_m2259467731 (UIWidget_t769069560 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::ResizeCollider()
extern "C"  void UIWidget_ResizeCollider_m654642185 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIWidget::FullCompareFunc(UIWidget,UIWidget)
extern "C"  int32_t UIWidget_FullCompareFunc_m3896523897 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ___left0, UIWidget_t769069560 * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIWidget::PanelCompareFunc(UIWidget,UIWidget)
extern "C"  int32_t UIWidget_PanelCompareFunc_m1938899026 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ___left0, UIWidget_t769069560 * ___right1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UIWidget::CalculateBounds()
extern "C"  Bounds_t2711641849  UIWidget_CalculateBounds_m2567301067 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UIWidget::CalculateBounds(UnityEngine.Transform)
extern "C"  Bounds_t2711641849  UIWidget_CalculateBounds_m1943782482 (UIWidget_t769069560 * __this, Transform_t1659122786 * ___relativeParent0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::SetDirty()
extern "C"  void UIWidget_SetDirty_m3979225553 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::RemoveFromPanel()
extern "C"  void UIWidget_RemoveFromPanel_m3233223575 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::MarkAsChanged()
extern "C"  void UIWidget_MarkAsChanged_m3551758678 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIPanel UIWidget::CreatePanel()
extern "C"  UIPanel_t295209936 * UIWidget_CreatePanel_m851962502 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::CheckLayer()
extern "C"  void UIWidget_CheckLayer_m388028458 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::ParentHasChanged()
extern "C"  void UIWidget_ParentHasChanged_m3250288453 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::Awake()
extern "C"  void UIWidget_Awake_m308016390 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::OnInit()
extern "C"  void UIWidget_OnInit_m3801346320 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::UpgradeFrom265()
extern "C"  void UIWidget_UpgradeFrom265_m3823217484 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::OnStart()
extern "C"  void UIWidget_OnStart_m2327121348 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::OnAnchor()
extern "C"  void UIWidget_OnAnchor_m1133360565 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::OnUpdate()
extern "C"  void UIWidget_OnUpdate_m3427137225 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::OnApplicationPause(System.Boolean)
extern "C"  void UIWidget_OnApplicationPause_m307910013 (UIWidget_t769069560 * __this, bool ___paused0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::OnDisable()
extern "C"  void UIWidget_OnDisable_m625553034 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::OnDestroy()
extern "C"  void UIWidget_OnDestroy_m2803085084 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidget::UpdateVisibility(System.Boolean,System.Boolean)
extern "C"  bool UIWidget_UpdateVisibility_m143185718 (UIWidget_t769069560 * __this, bool ___visibleByAlpha0, bool ___visibleByPanel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidget::UpdateTransform(System.Int32)
extern "C"  bool UIWidget_UpdateTransform_m2760912577 (UIWidget_t769069560 * __this, int32_t ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidget::UpdateGeometry(System.Int32)
extern "C"  bool UIWidget_UpdateGeometry_m76295649 (UIWidget_t769069560 * __this, int32_t ___frame0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::WriteToBuffers(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>,BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector4>)
extern "C"  void UIWidget_WriteToBuffers_m4077282832 (UIWidget_t769069560 * __this, BetterList_1_t1484067282 * ___v0, BetterList_1_t1484067281 * ___u1, BetterList_1_t2095821700 * ___c2, BetterList_1_t1484067282 * ___n3, BetterList_1_t1484067283 * ___t4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIWidget::get_mult()
extern "C"  float UIWidget_get_mult_m2957111846 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::MakePixelPerfect()
extern "C"  void UIWidget_MakePixelPerfect_m1839593398 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIWidget::get_minWidth()
extern "C"  int32_t UIWidget_get_minWidth_m3490344780 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIWidget::get_minHeight()
extern "C"  int32_t UIWidget_get_minHeight_m1330320131 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UIWidget::get_border()
extern "C"  Vector4_t4282066567  UIWidget_get_border_m4155026927 (UIWidget_t769069560 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::set_border(UnityEngine.Vector4)
extern "C"  void UIWidget_set_border_m2644475438 (UIWidget_t769069560 * __this, Vector4_t4282066567  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::OnFill(BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void UIWidget_OnFill_m620094002 (UIWidget_t769069560 * __this, BetterList_1_t1484067282 * ___verts0, BetterList_1_t1484067281 * ___uvs1, BetterList_1_t2095821700 * ___cols2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::ilo_AdjustWidget1(UIWidget,System.Single,System.Single,System.Single,System.Single)
extern "C"  void UIWidget_ilo_AdjustWidget1_m2475305896 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ___w0, float ___left1, float ___bottom2, float ___right3, float ___top4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::ilo_Invalidate2(UIWidget,System.Boolean)
extern "C"  void UIWidget_ilo_Invalidate2_m2120570064 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, bool ___includeChildren1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::ilo_ResizeCollider3(UIWidget)
extern "C"  void UIWidget_ilo_ResizeCollider3_m3874871713 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UIWidget::ilo_get_worldCorners4(UIWidget)
extern "C"  Vector3U5BU5D_t215400611* UIWidget_ilo_get_worldCorners4_m833249789 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UIWidget::ilo_get_cachedTransform5(UIRect)
extern "C"  Transform_t1659122786 * UIWidget_ilo_get_cachedTransform5_m970281198 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::ilo_RebuildAllDrawCalls6(UIPanel)
extern "C"  void UIWidget_ilo_RebuildAllDrawCalls6_m1143035331 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIPanel UIWidget::ilo_CreatePanel7(UIWidget)
extern "C"  UIPanel_t295209936 * UIWidget_ilo_CreatePanel7_m4070489614 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UIWidget::ilo_get_localCorners8(UIWidget)
extern "C"  Vector3U5BU5D_t215400611* UIWidget_ilo_get_localCorners8_m293866650 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UIWidget::ilo_get_pivotOffset9(UIWidget)
extern "C"  Vector2_t4282066565  UIWidget_ilo_get_pivotOffset9_m4268773988 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UIWidget::ilo_get_material10(UIWidget)
extern "C"  Material_t3870600107 * UIWidget_ilo_get_material10_m48315658 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::ilo_UpdateFinalAlpha11(UIWidget,System.Int32)
extern "C"  void UIWidget_ilo_UpdateFinalAlpha11_m3898829866 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, int32_t ___frameID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIRect UIWidget::ilo_get_parent12(UIRect)
extern "C"  UIRect_t2503437976 * UIWidget_ilo_get_parent12_m2581253749 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIWidget::ilo_CalculateFinalAlpha13(UIRect,System.Int32)
extern "C"  float UIWidget_ilo_CalculateFinalAlpha13_m380790109 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, int32_t ___frameID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIWidget::ilo_CalculateCumulativeAlpha14(UIWidget,System.Int32)
extern "C"  float UIWidget_ilo_CalculateCumulativeAlpha14_m3599915847 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, int32_t ___frameID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIWidget::ilo_get_minWidth15(UIWidget)
extern "C"  int32_t UIWidget_ilo_get_minWidth15_m2513467883 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::ilo_set_width16(UIWidget,System.Int32)
extern "C"  void UIWidget_ilo_set_width16_m554903475 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::ilo_set_height17(UIWidget,System.Int32)
extern "C"  void UIWidget_ilo_set_height17_m3889721905 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIWidget::ilo_CompareFunc18(UIPanel,UIPanel)
extern "C"  int32_t UIWidget_ilo_CompareFunc18_m1673576942 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ___a0, UIPanel_t295209936 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UIWidget::ilo_CalculateBounds19(UIWidget,UnityEngine.Transform)
extern "C"  Bounds_t2711641849  UIWidget_ilo_CalculateBounds19_m1287669599 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, Transform_t1659122786 * ___relativeParent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::ilo_RemoveWidget20(UIPanel,UIWidget)
extern "C"  void UIWidget_ilo_RemoveWidget20_m1283697734 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, UIWidget_t769069560 * ___w1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidget::ilo_GetActive21(UnityEngine.Behaviour)
extern "C"  bool UIWidget_ilo_GetActive21_m1312060023 (Il2CppObject * __this /* static, unused */, Behaviour_t200106419 * ___mb0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidget::ilo_GetActive22(UnityEngine.GameObject)
extern "C"  bool UIWidget_ilo_GetActive22_m2103698286 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::ilo_SetDirty23(UIWidget)
extern "C"  void UIWidget_ilo_SetDirty23_m4159396493 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UIWidget::ilo_get_cachedGameObject24(UIRect)
extern "C"  GameObject_t3674682005 * UIWidget_ilo_get_cachedGameObject24_m69334351 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIPanel UIWidget::ilo_Find25(UnityEngine.Transform,System.Boolean,System.Int32)
extern "C"  UIPanel_t295209936 * UIWidget_ilo_Find25_m157119588 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___trans0, bool ___createIfMissing1, int32_t ___layer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::ilo_ParentHasChanged26(UIRect)
extern "C"  void UIWidget_ilo_ParentHasChanged26_m1182858340 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::ilo_RemoveFromPanel27(UIWidget)
extern "C"  void UIWidget_ilo_RemoveFromPanel27_m2201961233 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UIWidget::ilo_GetSides28(UIRect/AnchorPoint,UnityEngine.Transform)
extern "C"  Vector3U5BU5D_t215400611* UIWidget_ilo_GetSides28_m1375701067 (Il2CppObject * __this /* static, unused */, AnchorPoint_t3581172420 * ____this0, Transform_t1659122786 * ___relativeTo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIWidget::ilo_GetLocalPos29(UIRect,UIRect/AnchorPoint,UnityEngine.Transform)
extern "C"  Vector3_t4282066566  UIWidget_ilo_GetLocalPos29_m2736052279 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ____this0, AnchorPoint_t3581172420 * ___ac1, Transform_t1659122786 * ___trans2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIWidget::ilo_Lerp30(System.Single,System.Single,System.Single)
extern "C"  float UIWidget_ilo_Lerp30_m3708354913 (Il2CppObject * __this /* static, unused */, float ___from0, float ___to1, float ___factor2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::ilo_MarkAsChanged31(UIWidget)
extern "C"  void UIWidget_ilo_MarkAsChanged31_m513794601 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIWidget::ilo_CalculateFinalAlpha32(UIWidget,System.Int32)
extern "C"  float UIWidget_ilo_CalculateFinalAlpha32_m2806458912 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, int32_t ___frameID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Shader UIWidget::ilo_get_shader33(UIWidget)
extern "C"  Shader_t3191267369 * UIWidget_ilo_get_shader33_m3083569163 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidget::ilo_get_hasVertices34(UIGeometry)
extern "C"  bool UIWidget_ilo_get_hasVertices34_m2450185375 (Il2CppObject * __this /* static, unused */, UIGeometry_t3586695974 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidget::ilo_WriteToBuffers35(UIGeometry,BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>,BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector4>)
extern "C"  void UIWidget_ilo_WriteToBuffers35_m555740973 (Il2CppObject * __this /* static, unused */, UIGeometry_t3586695974 * ____this0, BetterList_1_t1484067282 * ___v1, BetterList_1_t1484067281 * ___u2, BetterList_1_t2095821700 * ___c3, BetterList_1_t1484067282 * ___n4, BetterList_1_t1484067283 * ___t5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
