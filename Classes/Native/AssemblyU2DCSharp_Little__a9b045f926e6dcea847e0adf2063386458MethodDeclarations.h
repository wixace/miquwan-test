﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._a9b045f926e6dcea847e0adf351fb6d3
struct _a9b045f926e6dcea847e0adf351fb6d3_t2063386458;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._a9b045f926e6dcea847e0adf351fb6d3::.ctor()
extern "C"  void _a9b045f926e6dcea847e0adf351fb6d3__ctor_m3574072339 (_a9b045f926e6dcea847e0adf351fb6d3_t2063386458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a9b045f926e6dcea847e0adf351fb6d3::_a9b045f926e6dcea847e0adf351fb6d3m2(System.Int32)
extern "C"  int32_t _a9b045f926e6dcea847e0adf351fb6d3__a9b045f926e6dcea847e0adf351fb6d3m2_m3429892953 (_a9b045f926e6dcea847e0adf351fb6d3_t2063386458 * __this, int32_t ____a9b045f926e6dcea847e0adf351fb6d3a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a9b045f926e6dcea847e0adf351fb6d3::_a9b045f926e6dcea847e0adf351fb6d3m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _a9b045f926e6dcea847e0adf351fb6d3__a9b045f926e6dcea847e0adf351fb6d3m_m2249464701 (_a9b045f926e6dcea847e0adf351fb6d3_t2063386458 * __this, int32_t ____a9b045f926e6dcea847e0adf351fb6d3a0, int32_t ____a9b045f926e6dcea847e0adf351fb6d3991, int32_t ____a9b045f926e6dcea847e0adf351fb6d3c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
