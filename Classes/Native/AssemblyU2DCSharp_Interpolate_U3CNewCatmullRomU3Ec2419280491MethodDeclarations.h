﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Interpolate/<NewCatmullRom>c__Iterator25`1<System.Object>
struct U3CNewCatmullRomU3Ec__Iterator25_1_t2419280491;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t1898964319;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void Interpolate/<NewCatmullRom>c__Iterator25`1<System.Object>::.ctor()
extern "C"  void U3CNewCatmullRomU3Ec__Iterator25_1__ctor_m1444366138_gshared (U3CNewCatmullRomU3Ec__Iterator25_1_t2419280491 * __this, const MethodInfo* method);
#define U3CNewCatmullRomU3Ec__Iterator25_1__ctor_m1444366138(__this, method) ((  void (*) (U3CNewCatmullRomU3Ec__Iterator25_1_t2419280491 *, const MethodInfo*))U3CNewCatmullRomU3Ec__Iterator25_1__ctor_m1444366138_gshared)(__this, method)
// UnityEngine.Vector3 Interpolate/<NewCatmullRom>c__Iterator25`1<System.Object>::System.Collections.Generic.IEnumerator<UnityEngine.Vector3>.get_Current()
extern "C"  Vector3_t4282066566  U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_Generic_IEnumeratorU3CUnityEngine_Vector3U3E_get_Current_m3113813623_gshared (U3CNewCatmullRomU3Ec__Iterator25_1_t2419280491 * __this, const MethodInfo* method);
#define U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_Generic_IEnumeratorU3CUnityEngine_Vector3U3E_get_Current_m3113813623(__this, method) ((  Vector3_t4282066566  (*) (U3CNewCatmullRomU3Ec__Iterator25_1_t2419280491 *, const MethodInfo*))U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_Generic_IEnumeratorU3CUnityEngine_Vector3U3E_get_Current_m3113813623_gshared)(__this, method)
// System.Object Interpolate/<NewCatmullRom>c__Iterator25`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_IEnumerator_get_Current_m68169772_gshared (U3CNewCatmullRomU3Ec__Iterator25_1_t2419280491 * __this, const MethodInfo* method);
#define U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_IEnumerator_get_Current_m68169772(__this, method) ((  Il2CppObject * (*) (U3CNewCatmullRomU3Ec__Iterator25_1_t2419280491 *, const MethodInfo*))U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_IEnumerator_get_Current_m68169772_gshared)(__this, method)
// System.Collections.IEnumerator Interpolate/<NewCatmullRom>c__Iterator25`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_IEnumerable_GetEnumerator_m2932605159_gshared (U3CNewCatmullRomU3Ec__Iterator25_1_t2419280491 * __this, const MethodInfo* method);
#define U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_IEnumerable_GetEnumerator_m2932605159(__this, method) ((  Il2CppObject * (*) (U3CNewCatmullRomU3Ec__Iterator25_1_t2419280491 *, const MethodInfo*))U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_IEnumerable_GetEnumerator_m2932605159_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3> Interpolate/<NewCatmullRom>c__Iterator25`1<System.Object>::System.Collections.Generic.IEnumerable<UnityEngine.Vector3>.GetEnumerator()
extern "C"  Il2CppObject* U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_Generic_IEnumerableU3CUnityEngine_Vector3U3E_GetEnumerator_m81390756_gshared (U3CNewCatmullRomU3Ec__Iterator25_1_t2419280491 * __this, const MethodInfo* method);
#define U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_Generic_IEnumerableU3CUnityEngine_Vector3U3E_GetEnumerator_m81390756(__this, method) ((  Il2CppObject* (*) (U3CNewCatmullRomU3Ec__Iterator25_1_t2419280491 *, const MethodInfo*))U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_Generic_IEnumerableU3CUnityEngine_Vector3U3E_GetEnumerator_m81390756_gshared)(__this, method)
// System.Boolean Interpolate/<NewCatmullRom>c__Iterator25`1<System.Object>::MoveNext()
extern "C"  bool U3CNewCatmullRomU3Ec__Iterator25_1_MoveNext_m1180773754_gshared (U3CNewCatmullRomU3Ec__Iterator25_1_t2419280491 * __this, const MethodInfo* method);
#define U3CNewCatmullRomU3Ec__Iterator25_1_MoveNext_m1180773754(__this, method) ((  bool (*) (U3CNewCatmullRomU3Ec__Iterator25_1_t2419280491 *, const MethodInfo*))U3CNewCatmullRomU3Ec__Iterator25_1_MoveNext_m1180773754_gshared)(__this, method)
// System.Void Interpolate/<NewCatmullRom>c__Iterator25`1<System.Object>::Dispose()
extern "C"  void U3CNewCatmullRomU3Ec__Iterator25_1_Dispose_m659705079_gshared (U3CNewCatmullRomU3Ec__Iterator25_1_t2419280491 * __this, const MethodInfo* method);
#define U3CNewCatmullRomU3Ec__Iterator25_1_Dispose_m659705079(__this, method) ((  void (*) (U3CNewCatmullRomU3Ec__Iterator25_1_t2419280491 *, const MethodInfo*))U3CNewCatmullRomU3Ec__Iterator25_1_Dispose_m659705079_gshared)(__this, method)
// System.Void Interpolate/<NewCatmullRom>c__Iterator25`1<System.Object>::Reset()
extern "C"  void U3CNewCatmullRomU3Ec__Iterator25_1_Reset_m3385766375_gshared (U3CNewCatmullRomU3Ec__Iterator25_1_t2419280491 * __this, const MethodInfo* method);
#define U3CNewCatmullRomU3Ec__Iterator25_1_Reset_m3385766375(__this, method) ((  void (*) (U3CNewCatmullRomU3Ec__Iterator25_1_t2419280491 *, const MethodInfo*))U3CNewCatmullRomU3Ec__Iterator25_1_Reset_m3385766375_gshared)(__this, method)
