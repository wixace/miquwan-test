﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SceneMgr
struct SceneMgr_t3584105996;
// System.String
struct String_t;
// checkpointCfg
struct checkpointCfg_t2816107964;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// JSCLevelConfig
struct JSCLevelConfig_t1411099500;
// LevelConfigManager
struct LevelConfigManager_t657947911;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// Mihua.Assets.SubAssetMgr
struct SubAssetMgr_t3564963414;
// System.Object
struct Il2CppObject;
// GlobalGOMgr
struct GlobalGOMgr_t803081773;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// IZUpdate
struct IZUpdate_t3482043738;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_checkpointCfg2816107964.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_LevelConfigManager657947911.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_SceneMgr3584105996.h"
#include "AssemblyU2DCSharp_JSCLevelConfig1411099500.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr3564963414.h"
#include "AssemblyU2DCSharp_GlobalGOMgr803081773.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"

// System.Void SceneMgr::.ctor()
extern "C"  void SceneMgr__ctor_m112373263 (SceneMgr_t3584105996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgr::.cctor()
extern "C"  void SceneMgr__cctor_m3001474942 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgr::set_curSceneName(System.String)
extern "C"  void SceneMgr_set_curSceneName_m1837979609 (SceneMgr_t3584105996 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SceneMgr::get_curSceneName()
extern "C"  String_t* SceneMgr_get_curSceneName_m2937459704 (SceneMgr_t3584105996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgr::set_checkPoint(checkpointCfg)
extern "C"  void SceneMgr_set_checkPoint_m1707638108 (SceneMgr_t3584105996 * __this, checkpointCfg_t2816107964 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// checkpointCfg SceneMgr::get_checkPoint()
extern "C"  checkpointCfg_t2816107964 * SceneMgr_get_checkPoint_m2748451261 (SceneMgr_t3584105996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgr::set_isloading(System.Boolean)
extern "C"  void SceneMgr_set_isloading_m620660025 (SceneMgr_t3584105996 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SceneMgr::get_isloading()
extern "C"  bool SceneMgr_get_isloading_m1107883010 (SceneMgr_t3584105996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgr::Init()
extern "C"  void SceneMgr_Init_m93708901 (SceneMgr_t3584105996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SceneMgr::RequestEnter(System.UInt32)
extern "C"  bool SceneMgr_RequestEnter_m1602305768 (SceneMgr_t3584105996 * __this, uint32_t ___sceneID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgr::LoadScene(System.Int32)
extern "C"  void SceneMgr_LoadScene_m4035823556 (SceneMgr_t3584105996 * __this, int32_t ___sceneID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgr::Logout()
extern "C"  void SceneMgr_Logout_m1621399391 (SceneMgr_t3584105996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgr::LoadComplete(CEvent.ZEvent)
extern "C"  void SceneMgr_LoadComplete_m1964293927 (SceneMgr_t3584105996 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgr::ZUpdate()
extern "C"  void SceneMgr_ZUpdate_m185052496 (SceneMgr_t3584105996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSCLevelConfig SceneMgr::ilo_GetLevelConfigByID1(LevelConfigManager,System.Int32)
extern "C"  JSCLevelConfig_t1411099500 * SceneMgr_ilo_GetLevelConfigByID1_m3703170310 (Il2CppObject * __this /* static, unused */, LevelConfigManager_t657947911 * ____this0, int32_t ___level1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SceneMgr::ilo_Exists2(System.Collections.Generic.List`1<System.String>)
extern "C"  bool SceneMgr_ilo_Exists2_m1276356144 (Il2CppObject * __this /* static, unused */, List_1_t1375417109 * ___assetNames0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SceneMgr::ilo_Exists3(System.String)
extern "C"  bool SceneMgr_ilo_Exists3_m1605081189 (Il2CppObject * __this /* static, unused */, String_t* ___assetName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Assets.SubAssetMgr SceneMgr::ilo_get_SubAssetMgr4()
extern "C"  SubAssetMgr_t3564963414 * SceneMgr_ilo_get_SubAssetMgr4_m1247446855 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgr::ilo_Log5(System.Object,System.Boolean)
extern "C"  void SceneMgr_ilo_Log5_m3400849506 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgr::ilo_set_checkPoint6(SceneMgr,checkpointCfg)
extern "C"  void SceneMgr_ilo_set_checkPoint6_m3780799375 (Il2CppObject * __this /* static, unused */, SceneMgr_t3584105996 * ____this0, checkpointCfg_t2816107964 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// checkpointCfg SceneMgr::ilo_get_checkPoint7(SceneMgr)
extern "C"  checkpointCfg_t2816107964 * SceneMgr_ilo_get_checkPoint7_m3130623325 (Il2CppObject * __this /* static, unused */, SceneMgr_t3584105996 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SceneMgr::ilo_get_resName8(JSCLevelConfig)
extern "C"  String_t* SceneMgr_ilo_get_resName8_m2052050805 (Il2CppObject * __this /* static, unused */, JSCLevelConfig_t1411099500 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgr::ilo_ShowDownassetDialog9(Mihua.Assets.SubAssetMgr)
extern "C"  void SceneMgr_ilo_ShowDownassetDialog9_m4227574485 (Il2CppObject * __this /* static, unused */, SubAssetMgr_t3564963414 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgr::ilo_Reset10(GlobalGOMgr)
extern "C"  void SceneMgr_ilo_Reset10_m1800569053 (Il2CppObject * __this /* static, unused */, GlobalGOMgr_t803081773 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// checkpointCfg SceneMgr::ilo_GetcheckpointCfg11(CSDatacfgManager,System.Int32)
extern "C"  checkpointCfg_t2816107964 * SceneMgr_ilo_GetcheckpointCfg11_m2344172789 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SceneMgr::ilo_get_isloading12(SceneMgr)
extern "C"  bool SceneMgr_ilo_get_isloading12_m4245743684 (Il2CppObject * __this /* static, unused */, SceneMgr_t3584105996 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgr::ilo_RemoveUpdate13(IZUpdate)
extern "C"  void SceneMgr_ilo_RemoveUpdate13_m1565446141 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgr::ilo_RemoveEventListener14(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void SceneMgr_ilo_RemoveEventListener14_m2698870076 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SceneMgr::ilo_DispatchEvent15(CEvent.ZEvent)
extern "C"  void SceneMgr_ilo_DispatchEvent15_m4268686717 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
