﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._335d969307e829d08f086d8855ca9f79
struct _335d969307e829d08f086d8855ca9f79_t1777578141;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._335d969307e829d08f086d8855ca9f79::.ctor()
extern "C"  void _335d969307e829d08f086d8855ca9f79__ctor_m570414640 (_335d969307e829d08f086d8855ca9f79_t1777578141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._335d969307e829d08f086d8855ca9f79::_335d969307e829d08f086d8855ca9f79m2(System.Int32)
extern "C"  int32_t _335d969307e829d08f086d8855ca9f79__335d969307e829d08f086d8855ca9f79m2_m2489640057 (_335d969307e829d08f086d8855ca9f79_t1777578141 * __this, int32_t ____335d969307e829d08f086d8855ca9f79a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._335d969307e829d08f086d8855ca9f79::_335d969307e829d08f086d8855ca9f79m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _335d969307e829d08f086d8855ca9f79__335d969307e829d08f086d8855ca9f79m_m366526045 (_335d969307e829d08f086d8855ca9f79_t1777578141 * __this, int32_t ____335d969307e829d08f086d8855ca9f79a0, int32_t ____335d969307e829d08f086d8855ca9f79211, int32_t ____335d969307e829d08f086d8855ca9f79c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
