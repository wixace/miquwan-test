﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MotionBlur
struct MotionBlur_t1272018269;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void MotionBlur::.ctor()
extern "C"  void MotionBlur__ctor_m3060095378 (MotionBlur_t1272018269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MotionBlur::Start()
extern "C"  void MotionBlur_Start_m2007233170 (MotionBlur_t1272018269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MotionBlur::OnDisable()
extern "C"  void MotionBlur_OnDisable_m4162950393 (MotionBlur_t1272018269 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MotionBlur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void MotionBlur_OnRenderImage_m4048420748 (MotionBlur_t1272018269 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
