﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Collections_ObjectModel_Collection1916378933.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Mail.AttachmentCollection
struct  AttachmentCollection_t2308974981  : public Collection_1_t1916378933
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
