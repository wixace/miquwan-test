﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._86f842b564c3d7f24af8edb0d53e2883
struct _86f842b564c3d7f24af8edb0d53e2883_t2538788951;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__86f842b564c3d7f24af8edb02538788951.h"

// System.Void Little._86f842b564c3d7f24af8edb0d53e2883::.ctor()
extern "C"  void _86f842b564c3d7f24af8edb0d53e2883__ctor_m1025032118 (_86f842b564c3d7f24af8edb0d53e2883_t2538788951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._86f842b564c3d7f24af8edb0d53e2883::_86f842b564c3d7f24af8edb0d53e2883m2(System.Int32)
extern "C"  int32_t _86f842b564c3d7f24af8edb0d53e2883__86f842b564c3d7f24af8edb0d53e2883m2_m3945200185 (_86f842b564c3d7f24af8edb0d53e2883_t2538788951 * __this, int32_t ____86f842b564c3d7f24af8edb0d53e2883a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._86f842b564c3d7f24af8edb0d53e2883::_86f842b564c3d7f24af8edb0d53e2883m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _86f842b564c3d7f24af8edb0d53e2883__86f842b564c3d7f24af8edb0d53e2883m_m3022430365 (_86f842b564c3d7f24af8edb0d53e2883_t2538788951 * __this, int32_t ____86f842b564c3d7f24af8edb0d53e2883a0, int32_t ____86f842b564c3d7f24af8edb0d53e2883151, int32_t ____86f842b564c3d7f24af8edb0d53e2883c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._86f842b564c3d7f24af8edb0d53e2883::ilo__86f842b564c3d7f24af8edb0d53e2883m21(Little._86f842b564c3d7f24af8edb0d53e2883,System.Int32)
extern "C"  int32_t _86f842b564c3d7f24af8edb0d53e2883_ilo__86f842b564c3d7f24af8edb0d53e2883m21_m1628114430 (Il2CppObject * __this /* static, unused */, _86f842b564c3d7f24af8edb0d53e2883_t2538788951 * ____this0, int32_t ____86f842b564c3d7f24af8edb0d53e2883a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
