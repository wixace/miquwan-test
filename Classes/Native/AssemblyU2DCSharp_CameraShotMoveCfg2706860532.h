﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraShotMoveCfg
struct  CameraShotMoveCfg_t2706860532  : public Il2CppObject
{
public:
	// System.Int32 CameraShotMoveCfg::_id
	int32_t ____id_0;
	// System.Int32 CameraShotMoveCfg::_heroOrNpcId
	int32_t ____heroOrNpcId_1;
	// System.Int32 CameraShotMoveCfg::_isHero
	int32_t ____isHero_2;
	// System.Single CameraShotMoveCfg::_posX
	float ____posX_3;
	// System.Single CameraShotMoveCfg::_posY
	float ____posY_4;
	// System.Single CameraShotMoveCfg::_posZ
	float ____posZ_5;
	// ProtoBuf.IExtension CameraShotMoveCfg::extensionObject
	Il2CppObject * ___extensionObject_6;

public:
	inline static int32_t get_offset_of__id_0() { return static_cast<int32_t>(offsetof(CameraShotMoveCfg_t2706860532, ____id_0)); }
	inline int32_t get__id_0() const { return ____id_0; }
	inline int32_t* get_address_of__id_0() { return &____id_0; }
	inline void set__id_0(int32_t value)
	{
		____id_0 = value;
	}

	inline static int32_t get_offset_of__heroOrNpcId_1() { return static_cast<int32_t>(offsetof(CameraShotMoveCfg_t2706860532, ____heroOrNpcId_1)); }
	inline int32_t get__heroOrNpcId_1() const { return ____heroOrNpcId_1; }
	inline int32_t* get_address_of__heroOrNpcId_1() { return &____heroOrNpcId_1; }
	inline void set__heroOrNpcId_1(int32_t value)
	{
		____heroOrNpcId_1 = value;
	}

	inline static int32_t get_offset_of__isHero_2() { return static_cast<int32_t>(offsetof(CameraShotMoveCfg_t2706860532, ____isHero_2)); }
	inline int32_t get__isHero_2() const { return ____isHero_2; }
	inline int32_t* get_address_of__isHero_2() { return &____isHero_2; }
	inline void set__isHero_2(int32_t value)
	{
		____isHero_2 = value;
	}

	inline static int32_t get_offset_of__posX_3() { return static_cast<int32_t>(offsetof(CameraShotMoveCfg_t2706860532, ____posX_3)); }
	inline float get__posX_3() const { return ____posX_3; }
	inline float* get_address_of__posX_3() { return &____posX_3; }
	inline void set__posX_3(float value)
	{
		____posX_3 = value;
	}

	inline static int32_t get_offset_of__posY_4() { return static_cast<int32_t>(offsetof(CameraShotMoveCfg_t2706860532, ____posY_4)); }
	inline float get__posY_4() const { return ____posY_4; }
	inline float* get_address_of__posY_4() { return &____posY_4; }
	inline void set__posY_4(float value)
	{
		____posY_4 = value;
	}

	inline static int32_t get_offset_of__posZ_5() { return static_cast<int32_t>(offsetof(CameraShotMoveCfg_t2706860532, ____posZ_5)); }
	inline float get__posZ_5() const { return ____posZ_5; }
	inline float* get_address_of__posZ_5() { return &____posZ_5; }
	inline void set__posZ_5(float value)
	{
		____posZ_5 = value;
	}

	inline static int32_t get_offset_of_extensionObject_6() { return static_cast<int32_t>(offsetof(CameraShotMoveCfg_t2706860532, ___extensionObject_6)); }
	inline Il2CppObject * get_extensionObject_6() const { return ___extensionObject_6; }
	inline Il2CppObject ** get_address_of_extensionObject_6() { return &___extensionObject_6; }
	inline void set_extensionObject_6(Il2CppObject * value)
	{
		___extensionObject_6 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObject_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
