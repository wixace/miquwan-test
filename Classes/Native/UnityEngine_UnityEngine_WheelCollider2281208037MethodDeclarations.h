﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.WheelCollider
struct WheelCollider_t2281208037;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_JointSpring3746621933.h"
#include "UnityEngine_UnityEngine_WheelFrictionCurve2609054782.h"
#include "UnityEngine_UnityEngine_WheelHit1311777980.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// System.Void UnityEngine.WheelCollider::.ctor()
extern "C"  void WheelCollider__ctor_m1332677194 (WheelCollider_t2281208037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.WheelCollider::get_center()
extern "C"  Vector3_t4282066566  WheelCollider_get_center_m3342104830 (WheelCollider_t2281208037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::set_center(UnityEngine.Vector3)
extern "C"  void WheelCollider_set_center_m1484253997 (WheelCollider_t2281208037 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::INTERNAL_get_center(UnityEngine.Vector3&)
extern "C"  void WheelCollider_INTERNAL_get_center_m3182379643 (WheelCollider_t2281208037 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::INTERNAL_set_center(UnityEngine.Vector3&)
extern "C"  void WheelCollider_INTERNAL_set_center_m535970031 (WheelCollider_t2281208037 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelCollider::get_radius()
extern "C"  float WheelCollider_get_radius_m3068772649 (WheelCollider_t2281208037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::set_radius(System.Single)
extern "C"  void WheelCollider_set_radius_m48892450 (WheelCollider_t2281208037 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelCollider::get_suspensionDistance()
extern "C"  float WheelCollider_get_suspensionDistance_m2464128649 (WheelCollider_t2281208037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::set_suspensionDistance(System.Single)
extern "C"  void WheelCollider_set_suspensionDistance_m611870914 (WheelCollider_t2281208037 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointSpring UnityEngine.WheelCollider::get_suspensionSpring()
extern "C"  JointSpring_t3746621933  WheelCollider_get_suspensionSpring_m3323845210 (WheelCollider_t2281208037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::set_suspensionSpring(UnityEngine.JointSpring)
extern "C"  void WheelCollider_set_suspensionSpring_m4112344913 (WheelCollider_t2281208037 * __this, JointSpring_t3746621933  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::INTERNAL_get_suspensionSpring(UnityEngine.JointSpring&)
extern "C"  void WheelCollider_INTERNAL_get_suspensionSpring_m4100469207 (WheelCollider_t2281208037 * __this, JointSpring_t3746621933 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::INTERNAL_set_suspensionSpring(UnityEngine.JointSpring&)
extern "C"  void WheelCollider_INTERNAL_set_suspensionSpring_m2785203531 (WheelCollider_t2281208037 * __this, JointSpring_t3746621933 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelCollider::get_forceAppPointDistance()
extern "C"  float WheelCollider_get_forceAppPointDistance_m1434346618 (WheelCollider_t2281208037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::set_forceAppPointDistance(System.Single)
extern "C"  void WheelCollider_set_forceAppPointDistance_m3556078705 (WheelCollider_t2281208037 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelCollider::get_mass()
extern "C"  float WheelCollider_get_mass_m2188990603 (WheelCollider_t2281208037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::set_mass(System.Single)
extern "C"  void WheelCollider_set_mass_m2004533760 (WheelCollider_t2281208037 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelCollider::get_wheelDampingRate()
extern "C"  float WheelCollider_get_wheelDampingRate_m1714651774 (WheelCollider_t2281208037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::set_wheelDampingRate(System.Single)
extern "C"  void WheelCollider_set_wheelDampingRate_m375069421 (WheelCollider_t2281208037 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WheelFrictionCurve UnityEngine.WheelCollider::get_forwardFriction()
extern "C"  WheelFrictionCurve_t2609054782  WheelCollider_get_forwardFriction_m4021921564 (WheelCollider_t2281208037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::set_forwardFriction(UnityEngine.WheelFrictionCurve)
extern "C"  void WheelCollider_set_forwardFriction_m3131911225 (WheelCollider_t2281208037 * __this, WheelFrictionCurve_t2609054782  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::INTERNAL_get_forwardFriction(UnityEngine.WheelFrictionCurve&)
extern "C"  void WheelCollider_INTERNAL_get_forwardFriction_m3880234223 (WheelCollider_t2281208037 * __this, WheelFrictionCurve_t2609054782 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::INTERNAL_set_forwardFriction(UnityEngine.WheelFrictionCurve&)
extern "C"  void WheelCollider_INTERNAL_set_forwardFriction_m225372515 (WheelCollider_t2281208037 * __this, WheelFrictionCurve_t2609054782 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WheelFrictionCurve UnityEngine.WheelCollider::get_sidewaysFriction()
extern "C"  WheelFrictionCurve_t2609054782  WheelCollider_get_sidewaysFriction_m265984922 (WheelCollider_t2281208037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::set_sidewaysFriction(UnityEngine.WheelFrictionCurve)
extern "C"  void WheelCollider_set_sidewaysFriction_m68473243 (WheelCollider_t2281208037 * __this, WheelFrictionCurve_t2609054782  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::INTERNAL_get_sidewaysFriction(UnityEngine.WheelFrictionCurve&)
extern "C"  void WheelCollider_INTERNAL_get_sidewaysFriction_m3156235989 (WheelCollider_t2281208037 * __this, WheelFrictionCurve_t2609054782 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::INTERNAL_set_sidewaysFriction(UnityEngine.WheelFrictionCurve&)
extern "C"  void WheelCollider_INTERNAL_set_sidewaysFriction_m1524672737 (WheelCollider_t2281208037 * __this, WheelFrictionCurve_t2609054782 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelCollider::get_motorTorque()
extern "C"  float WheelCollider_get_motorTorque_m2029180362 (WheelCollider_t2281208037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::set_motorTorque(System.Single)
extern "C"  void WheelCollider_set_motorTorque_m3783058721 (WheelCollider_t2281208037 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelCollider::get_brakeTorque()
extern "C"  float WheelCollider_get_brakeTorque_m3939357312 (WheelCollider_t2281208037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::set_brakeTorque(System.Single)
extern "C"  void WheelCollider_set_brakeTorque_m2819145771 (WheelCollider_t2281208037 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelCollider::get_steerAngle()
extern "C"  float WheelCollider_get_steerAngle_m1656360729 (WheelCollider_t2281208037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::set_steerAngle(System.Single)
extern "C"  void WheelCollider_set_steerAngle_m2024038962 (WheelCollider_t2281208037 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WheelCollider::get_isGrounded()
extern "C"  bool WheelCollider_get_isGrounded_m3571622317 (WheelCollider_t2281208037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::ConfigureVehicleSubsteps(System.Single,System.Int32,System.Int32)
extern "C"  void WheelCollider_ConfigureVehicleSubsteps_m4221078788 (WheelCollider_t2281208037 * __this, float ___speedThreshold0, int32_t ___stepsBelowThreshold1, int32_t ___stepsAboveThreshold2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelCollider::get_sprungMass()
extern "C"  float WheelCollider_get_sprungMass_m3174036612 (WheelCollider_t2281208037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WheelCollider::GetGroundHit(UnityEngine.WheelHit&)
extern "C"  bool WheelCollider_GetGroundHit_m3646379857 (WheelCollider_t2281208037 * __this, WheelHit_t1311777980 * ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::GetWorldPose(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void WheelCollider_GetWorldPose_m154559413 (WheelCollider_t2281208037 * __this, Vector3_t4282066566 * ___pos0, Quaternion_t1553702882 * ___quat1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelCollider::get_rpm()
extern "C"  float WheelCollider_get_rpm_m3539352282 (WheelCollider_t2281208037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
