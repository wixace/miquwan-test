﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.DataErrorException
struct DataErrorException_t404948871;

#include "codegen/il2cpp-codegen.h"

// System.Void SevenZip.DataErrorException::.ctor()
extern "C"  void DataErrorException__ctor_m4194023936 (DataErrorException_t404948871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
