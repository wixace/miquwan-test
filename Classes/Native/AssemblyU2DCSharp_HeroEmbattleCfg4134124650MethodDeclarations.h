﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HeroEmbattleCfg
struct HeroEmbattleCfg_t4134124650;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void HeroEmbattleCfg::.ctor()
extern "C"  void HeroEmbattleCfg__ctor_m391799361 (HeroEmbattleCfg_t4134124650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroEmbattleCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void HeroEmbattleCfg_Init_m3303823780 (HeroEmbattleCfg_t4134124650 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
