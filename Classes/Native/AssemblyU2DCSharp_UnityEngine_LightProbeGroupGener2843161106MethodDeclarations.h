﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_LightProbeGroupGenerated
struct UnityEngine_LightProbeGroupGenerated_t2843161106;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_LightProbeGroupGenerated::.ctor()
extern "C"  void UnityEngine_LightProbeGroupGenerated__ctor_m1994140873 (UnityEngine_LightProbeGroupGenerated_t2843161106 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LightProbeGroupGenerated::LightProbeGroup_LightProbeGroup1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LightProbeGroupGenerated_LightProbeGroup_LightProbeGroup1_m2087446725 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightProbeGroupGenerated::LightProbeGroup_probePositions(JSVCall)
extern "C"  void UnityEngine_LightProbeGroupGenerated_LightProbeGroup_probePositions_m1127318840 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightProbeGroupGenerated::__Register()
extern "C"  void UnityEngine_LightProbeGroupGenerated___Register_m1149167006 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine_LightProbeGroupGenerated::<LightProbeGroup_probePositions>m__25E()
extern "C"  Vector3U5BU5D_t215400611* UnityEngine_LightProbeGroupGenerated_U3CLightProbeGroup_probePositionsU3Em__25E_m167066294 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LightProbeGroupGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_LightProbeGroupGenerated_ilo_attachFinalizerObject1_m2914035254 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightProbeGroupGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_LightProbeGroupGenerated_ilo_addJSCSRel2_m4141528966 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LightProbeGroupGenerated::ilo_getArrayLength3(System.Int32)
extern "C"  int32_t UnityEngine_LightProbeGroupGenerated_ilo_getArrayLength3_m1425965473 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LightProbeGroupGenerated::ilo_getElement4(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_LightProbeGroupGenerated_ilo_getElement4_m2489649554 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
