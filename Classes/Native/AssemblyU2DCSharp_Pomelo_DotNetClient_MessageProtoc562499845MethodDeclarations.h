﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pomelo.DotNetClient.MessageProtocol
struct MessageProtocol_t562499845;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;
// Pomelo.DotNetClient.Message
struct Message_t686303821;
// Pomelo.Protobuf.Protobuf
struct Protobuf_t1750100383;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_MessageProtoc562499845.h"
#include "AssemblyU2DCSharp_Pomelo_Protobuf_Protobuf1750100383.h"

// System.Void Pomelo.DotNetClient.MessageProtocol::.ctor(Newtonsoft.Json.Linq.JObject,Newtonsoft.Json.Linq.JObject,Newtonsoft.Json.Linq.JObject)
extern "C"  void MessageProtocol__ctor_m1481595287 (MessageProtocol_t562499845 * __this, JObject_t1798544199 * ___dict0, JObject_t1798544199 * ___serverProtos1, JObject_t1798544199 * ___clientProtos2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.DotNetClient.MessageProtocol::encode(System.String,Newtonsoft.Json.Linq.JObject)
extern "C"  ByteU5BU5D_t4260760469* MessageProtocol_encode_m2114150653 (MessageProtocol_t562499845 * __this, String_t* ___route0, JObject_t1798544199 * ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.DotNetClient.MessageProtocol::encode(System.String,System.UInt32,Newtonsoft.Json.Linq.JObject)
extern "C"  ByteU5BU5D_t4260760469* MessageProtocol_encode_m429751049 (MessageProtocol_t562499845 * __this, String_t* ___route0, uint32_t ___id1, JObject_t1798544199 * ___msg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pomelo.DotNetClient.Message Pomelo.DotNetClient.MessageProtocol::decode(System.Byte[])
extern "C"  Message_t686303821 * MessageProtocol_decode_m622755045 (MessageProtocol_t562499845 * __this, ByteU5BU5D_t4260760469* ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.MessageProtocol::writeInt(System.Int32,System.UInt32,System.Byte[])
extern "C"  void MessageProtocol_writeInt_m1216752885 (MessageProtocol_t562499845 * __this, int32_t ___offset0, uint32_t ___value1, ByteU5BU5D_t4260760469* ___bytes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.MessageProtocol::writeShort(System.Int32,System.UInt16,System.Byte[])
extern "C"  void MessageProtocol_writeShort_m3549321596 (MessageProtocol_t562499845 * __this, int32_t ___offset0, uint16_t ___value1, ByteU5BU5D_t4260760469* ___bytes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 Pomelo.DotNetClient.MessageProtocol::readShort(System.Int32,System.Byte[])
extern "C"  uint16_t MessageProtocol_readShort_m2175125460 (MessageProtocol_t562499845 * __this, int32_t ___offset0, ByteU5BU5D_t4260760469* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pomelo.DotNetClient.MessageProtocol::byteLength(System.String)
extern "C"  int32_t MessageProtocol_byteLength_m3924025989 (MessageProtocol_t562499845 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.MessageProtocol::writeBytes(System.Byte[],System.Int32,System.Byte[])
extern "C"  void MessageProtocol_writeBytes_m2035069274 (MessageProtocol_t562499845 * __this, ByteU5BU5D_t4260760469* ___source0, int32_t ___offset1, ByteU5BU5D_t4260760469* ___target2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pomelo.DotNetClient.MessageProtocol::ilo_byteLength1(Pomelo.DotNetClient.MessageProtocol,System.String)
extern "C"  int32_t MessageProtocol_ilo_byteLength1_m2045224967 (Il2CppObject * __this /* static, unused */, MessageProtocol_t562499845 * ____this0, String_t* ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.MessageProtocol::ilo_writeBytes2(Pomelo.DotNetClient.MessageProtocol,System.Byte[],System.Int32,System.Byte[])
extern "C"  void MessageProtocol_ilo_writeBytes2_m2857059905 (Il2CppObject * __this /* static, unused */, MessageProtocol_t562499845 * ____this0, ByteU5BU5D_t4260760469* ___source1, int32_t ___offset2, ByteU5BU5D_t4260760469* ___target3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.MessageProtocol::ilo_writeShort3(Pomelo.DotNetClient.MessageProtocol,System.Int32,System.UInt16,System.Byte[])
extern "C"  void MessageProtocol_ilo_writeShort3_m3435934114 (Il2CppObject * __this /* static, unused */, MessageProtocol_t562499845 * ____this0, int32_t ___offset1, uint16_t ___value2, ByteU5BU5D_t4260760469* ___bytes3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.DotNetClient.MessageProtocol::ilo_encode4(Pomelo.Protobuf.Protobuf,System.String,Newtonsoft.Json.Linq.JObject)
extern "C"  ByteU5BU5D_t4260760469* MessageProtocol_ilo_encode4_m3619116026 (Il2CppObject * __this /* static, unused */, Protobuf_t1750100383 * ____this0, String_t* ___route1, JObject_t1798544199 * ___msg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject Pomelo.DotNetClient.MessageProtocol::ilo_decode5(Pomelo.Protobuf.Protobuf,System.String,System.Byte[])
extern "C"  JObject_t1798544199 * MessageProtocol_ilo_decode5_m1679888747 (Il2CppObject * __this /* static, unused */, Protobuf_t1750100383 * ____this0, String_t* ___route1, ByteU5BU5D_t4260760469* ___buffer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
