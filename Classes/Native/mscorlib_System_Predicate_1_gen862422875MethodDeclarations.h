﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen3781873254MethodDeclarations.h"

// System.Void System.Predicate`1<CameraAniGroup>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m1634762643(__this, ___object0, ___method1, method) ((  void (*) (Predicate_1_t862422875 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m231416842_gshared)(__this, ___object0, ___method1, method)
// System.Boolean System.Predicate`1<CameraAniGroup>::Invoke(T)
#define Predicate_1_Invoke_m1281677711(__this, ___obj0, method) ((  bool (*) (Predicate_1_t862422875 *, CameraAniGroup_t1251365992 *, const MethodInfo*))Predicate_1_Invoke_m1328901537_gshared)(__this, ___obj0, method)
// System.IAsyncResult System.Predicate`1<CameraAniGroup>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m3419515678(__this, ___obj0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (Predicate_1_t862422875 *, CameraAniGroup_t1251365992 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Predicate_1_BeginInvoke_m2038073176_gshared)(__this, ___obj0, ___callback1, ___object2, method)
// System.Boolean System.Predicate`1<CameraAniGroup>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m3637791141(__this, ___result0, method) ((  bool (*) (Predicate_1_t862422875 *, Il2CppObject *, const MethodInfo*))Predicate_1_EndInvoke_m3970497007_gshared)(__this, ___result0, method)
