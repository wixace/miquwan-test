﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.Object
struct Il2CppObject;
// PluginTlog
struct PluginTlog_t1606437955;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginTlog/<SendLog>c__Iterator35
struct  U3CSendLogU3Ec__Iterator35_t481847745  : public Il2CppObject
{
public:
	// System.String PluginTlog/<SendLog>c__Iterator35::<url>__0
	String_t* ___U3CurlU3E__0_0;
	// System.String PluginTlog/<SendLog>c__Iterator35::jsonStr
	String_t* ___jsonStr_1;
	// System.Byte[] PluginTlog/<SendLog>c__Iterator35::<body>__1
	ByteU5BU5D_t4260760469* ___U3CbodyU3E__1_2;
	// System.Byte[] PluginTlog/<SendLog>c__Iterator35::<hashInfo>__2
	ByteU5BU5D_t4260760469* ___U3ChashInfoU3E__2_3;
	// System.String PluginTlog/<SendLog>c__Iterator35::<hash64>__3
	String_t* ___U3Chash64U3E__3_4;
	// UnityEngine.WWW PluginTlog/<SendLog>c__Iterator35::<www>__4
	WWW_t3134621005 * ___U3CwwwU3E__4_5;
	// System.Int32 PluginTlog/<SendLog>c__Iterator35::$PC
	int32_t ___U24PC_6;
	// System.Object PluginTlog/<SendLog>c__Iterator35::$current
	Il2CppObject * ___U24current_7;
	// System.String PluginTlog/<SendLog>c__Iterator35::<$>jsonStr
	String_t* ___U3CU24U3EjsonStr_8;
	// PluginTlog PluginTlog/<SendLog>c__Iterator35::<>f__this
	PluginTlog_t1606437955 * ___U3CU3Ef__this_9;

public:
	inline static int32_t get_offset_of_U3CurlU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSendLogU3Ec__Iterator35_t481847745, ___U3CurlU3E__0_0)); }
	inline String_t* get_U3CurlU3E__0_0() const { return ___U3CurlU3E__0_0; }
	inline String_t** get_address_of_U3CurlU3E__0_0() { return &___U3CurlU3E__0_0; }
	inline void set_U3CurlU3E__0_0(String_t* value)
	{
		___U3CurlU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CurlU3E__0_0, value);
	}

	inline static int32_t get_offset_of_jsonStr_1() { return static_cast<int32_t>(offsetof(U3CSendLogU3Ec__Iterator35_t481847745, ___jsonStr_1)); }
	inline String_t* get_jsonStr_1() const { return ___jsonStr_1; }
	inline String_t** get_address_of_jsonStr_1() { return &___jsonStr_1; }
	inline void set_jsonStr_1(String_t* value)
	{
		___jsonStr_1 = value;
		Il2CppCodeGenWriteBarrier(&___jsonStr_1, value);
	}

	inline static int32_t get_offset_of_U3CbodyU3E__1_2() { return static_cast<int32_t>(offsetof(U3CSendLogU3Ec__Iterator35_t481847745, ___U3CbodyU3E__1_2)); }
	inline ByteU5BU5D_t4260760469* get_U3CbodyU3E__1_2() const { return ___U3CbodyU3E__1_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CbodyU3E__1_2() { return &___U3CbodyU3E__1_2; }
	inline void set_U3CbodyU3E__1_2(ByteU5BU5D_t4260760469* value)
	{
		___U3CbodyU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbodyU3E__1_2, value);
	}

	inline static int32_t get_offset_of_U3ChashInfoU3E__2_3() { return static_cast<int32_t>(offsetof(U3CSendLogU3Ec__Iterator35_t481847745, ___U3ChashInfoU3E__2_3)); }
	inline ByteU5BU5D_t4260760469* get_U3ChashInfoU3E__2_3() const { return ___U3ChashInfoU3E__2_3; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3ChashInfoU3E__2_3() { return &___U3ChashInfoU3E__2_3; }
	inline void set_U3ChashInfoU3E__2_3(ByteU5BU5D_t4260760469* value)
	{
		___U3ChashInfoU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3ChashInfoU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U3Chash64U3E__3_4() { return static_cast<int32_t>(offsetof(U3CSendLogU3Ec__Iterator35_t481847745, ___U3Chash64U3E__3_4)); }
	inline String_t* get_U3Chash64U3E__3_4() const { return ___U3Chash64U3E__3_4; }
	inline String_t** get_address_of_U3Chash64U3E__3_4() { return &___U3Chash64U3E__3_4; }
	inline void set_U3Chash64U3E__3_4(String_t* value)
	{
		___U3Chash64U3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3Chash64U3E__3_4, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__4_5() { return static_cast<int32_t>(offsetof(U3CSendLogU3Ec__Iterator35_t481847745, ___U3CwwwU3E__4_5)); }
	inline WWW_t3134621005 * get_U3CwwwU3E__4_5() const { return ___U3CwwwU3E__4_5; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3E__4_5() { return &___U3CwwwU3E__4_5; }
	inline void set_U3CwwwU3E__4_5(WWW_t3134621005 * value)
	{
		___U3CwwwU3E__4_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__4_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CSendLogU3Ec__Iterator35_t481847745, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CSendLogU3Ec__Iterator35_t481847745, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EjsonStr_8() { return static_cast<int32_t>(offsetof(U3CSendLogU3Ec__Iterator35_t481847745, ___U3CU24U3EjsonStr_8)); }
	inline String_t* get_U3CU24U3EjsonStr_8() const { return ___U3CU24U3EjsonStr_8; }
	inline String_t** get_address_of_U3CU24U3EjsonStr_8() { return &___U3CU24U3EjsonStr_8; }
	inline void set_U3CU24U3EjsonStr_8(String_t* value)
	{
		___U3CU24U3EjsonStr_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EjsonStr_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_9() { return static_cast<int32_t>(offsetof(U3CSendLogU3Ec__Iterator35_t481847745, ___U3CU3Ef__this_9)); }
	inline PluginTlog_t1606437955 * get_U3CU3Ef__this_9() const { return ___U3CU3Ef__this_9; }
	inline PluginTlog_t1606437955 ** get_address_of_U3CU3Ef__this_9() { return &___U3CU3Ef__this_9; }
	inline void set_U3CU3Ef__this_9(PluginTlog_t1606437955 * value)
	{
		___U3CU3Ef__this_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
