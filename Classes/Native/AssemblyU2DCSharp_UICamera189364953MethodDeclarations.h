﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICamera
struct UICamera_t189364953;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Camera
struct Camera_t2727095145;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.Rigidbody
struct Rigidbody_t3346577219;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1743771669;
// UICamera/MouseOrTouch
struct MouseOrTouch_t2057376333;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UIWidget/HitCheck
struct HitCheck_t3889696652;
// UIWidget
struct UIWidget_t769069560;
// UIPanel
struct UIPanel_t295209936;
// UICamera/GetKeyStateFunc
struct GetKeyStateFunc_t2745734774;
// UICamera/OnScreenResize
struct OnScreenResize_t3828578773;
// UICamera/BoolDelegate
struct BoolDelegate_t2094540325;
// UICamera/KeyCodeDelegate
struct KeyCodeDelegate_t4120455995;
// UICamera/VoidDelegate
struct VoidDelegate_t3135042255;
// UICamera/ObjectDelegate
struct ObjectDelegate_t3703364602;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_UICamera_ControlScheme2923531404.h"
#include "AssemblyU2DCSharp_UICamera189364953.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_UICamera_MouseOrTouch2057376333.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_UICamera_DepthEntry1145614469.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_UIWidget_HitCheck3889696652.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"
#include "AssemblyU2DCSharp_UICamera_GetKeyStateFunc2745734774.h"
#include "AssemblyU2DCSharp_UICamera_OnScreenResize3828578773.h"
#include "AssemblyU2DCSharp_UICamera_BoolDelegate2094540325.h"
#include "AssemblyU2DCSharp_UICamera_KeyCodeDelegate4120455995.h"
#include "AssemblyU2DCSharp_UICamera_VoidDelegate3135042255.h"
#include "AssemblyU2DCSharp_UICamera_ObjectDelegate3703364602.h"

// System.Void UICamera::.ctor()
extern "C"  void UICamera__ctor_m2978047970 (UICamera_t189364953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::.cctor()
extern "C"  void UICamera__cctor_m1643077643 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::get_stickyPress()
extern "C"  bool UICamera_get_stickyPress_m902490973 (UICamera_t189364953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UICamera::get_lastEventPosition()
extern "C"  Vector2_t4282066565  UICamera_get_lastEventPosition_m1966259817 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::set_lastEventPosition(UnityEngine.Vector2)
extern "C"  void UICamera_set_lastEventPosition_m1843170890 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray UICamera::get_currentRay()
extern "C"  Ray_t3134616544  UICamera_get_currentRay_m1943112348 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UICamera::get_genericEventHandler()
extern "C"  GameObject_t3674682005 * UICamera_get_genericEventHandler_m1372983657 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::set_genericEventHandler(UnityEngine.GameObject)
extern "C"  void UICamera_set_genericEventHandler_m1583245378 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::get_handlesEvents()
extern "C"  bool UICamera_get_handlesEvents_m3964417223 (UICamera_t189364953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UICamera::get_cachedCamera()
extern "C"  Camera_t2727095145 * UICamera_get_cachedCamera_m269881363 (UICamera_t189364953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::get_isOverUI()
extern "C"  bool UICamera_get_isOverUI_m3999030001 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UICamera::get_selectedObject()
extern "C"  GameObject_t3674682005 * UICamera_get_selectedObject_m1709372474 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::set_selectedObject(UnityEngine.GameObject)
extern "C"  void UICamera_set_selectedObject_m734101361 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::IsPressed(UnityEngine.GameObject)
extern "C"  bool UICamera_IsPressed_m2263685468 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::SetSelection(UnityEngine.GameObject,UICamera/ControlScheme)
extern "C"  void UICamera_SetSelection_m879288790 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, int32_t ___scheme1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UICamera::ChangeSelection()
extern "C"  Il2CppObject * UICamera_ChangeSelection_m2272367540 (UICamera_t189364953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera::get_touchCount()
extern "C"  int32_t UICamera_get_touchCount_m700725161 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera::get_dragCount()
extern "C"  int32_t UICamera_get_dragCount_m938745316 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UICamera::get_mainCamera()
extern "C"  Camera_t2727095145 * UICamera_get_mainCamera_m1601227882 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera UICamera::get_eventHandler()
extern "C"  UICamera_t189364953 * UICamera_get_eventHandler_m643934687 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera::CompareFunc(UICamera,UICamera)
extern "C"  int32_t UICamera_CompareFunc_m2639586507 (Il2CppObject * __this /* static, unused */, UICamera_t189364953 * ___a0, UICamera_t189364953 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UICamera::FindRootRigidbody(UnityEngine.Transform)
extern "C"  Rigidbody_t3346577219 * UICamera_FindRootRigidbody_m2948444808 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___trans0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody2D UICamera::FindRootRigidbody2D(UnityEngine.Transform)
extern "C"  Rigidbody2D_t1743771669 * UICamera_FindRootRigidbody2D_m139188388 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___trans0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::Raycast(UICamera/MouseOrTouch)
extern "C"  void UICamera_Raycast_m4053376572 (Il2CppObject * __this /* static, unused */, MouseOrTouch_t2057376333 * ___touch0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::Raycast(UnityEngine.Vector3)
extern "C"  bool UICamera_Raycast_m2420302724 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___inPos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::IsVisible(UnityEngine.Vector3,UnityEngine.GameObject)
extern "C"  bool UICamera_IsVisible_m1362530233 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___worldPoint0, GameObject_t3674682005 * ___go1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::IsVisible(UICamera/DepthEntry&)
extern "C"  bool UICamera_IsVisible_m1299075475 (Il2CppObject * __this /* static, unused */, DepthEntry_t1145614469 * ___de0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::IsHighlighted(UnityEngine.GameObject)
extern "C"  bool UICamera_IsHighlighted_m1305001485 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera UICamera::FindCameraForLayer(System.Int32)
extern "C"  UICamera_t189364953 * UICamera_FindCameraForLayer_m2152131997 (Il2CppObject * __this /* static, unused */, int32_t ___layer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera::GetDirection(UnityEngine.KeyCode,UnityEngine.KeyCode)
extern "C"  int32_t UICamera_GetDirection_m2450741211 (Il2CppObject * __this /* static, unused */, int32_t ___up0, int32_t ___down1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera::GetDirection(UnityEngine.KeyCode,UnityEngine.KeyCode,UnityEngine.KeyCode,UnityEngine.KeyCode)
extern "C"  int32_t UICamera_GetDirection_m285642313 (Il2CppObject * __this /* static, unused */, int32_t ___up00, int32_t ___up11, int32_t ___down02, int32_t ___down13, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera::GetDirection(System.String)
extern "C"  int32_t UICamera_GetDirection_m759218409 (Il2CppObject * __this /* static, unused */, String_t* ___axis0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::Notify(UnityEngine.GameObject,System.String,System.Object)
extern "C"  void UICamera_Notify_m1535613613 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, String_t* ___funcName1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/MouseOrTouch UICamera::GetMouse(System.Int32)
extern "C"  MouseOrTouch_t2057376333 * UICamera_GetMouse_m1121080520 (Il2CppObject * __this /* static, unused */, int32_t ___button0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/MouseOrTouch UICamera::GetTouch(System.Int32)
extern "C"  MouseOrTouch_t2057376333 * UICamera_GetTouch_m3606012386 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::RemoveTouch(System.Int32)
extern "C"  void UICamera_RemoveTouch_m2980431084 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::Awake()
extern "C"  void UICamera_Awake_m3215653189 (UICamera_t189364953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::OnEnable()
extern "C"  void UICamera_OnEnable_m4254186596 (UICamera_t189364953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::OnDisable()
extern "C"  void UICamera_OnDisable_m3471702857 (UICamera_t189364953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::Start()
extern "C"  void UICamera_Start_m1925185762 (UICamera_t189364953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::Update()
extern "C"  void UICamera_Update_m3852035947 (UICamera_t189364953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::LateUpdate()
extern "C"  void UICamera_LateUpdate_m3569582001 (UICamera_t189364953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ProcessMouse()
extern "C"  void UICamera_ProcessMouse_m2917197336 (UICamera_t189364953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ProcessTouches()
extern "C"  void UICamera_ProcessTouches_m2887277344 (UICamera_t189364953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ProcessFakeTouches()
extern "C"  void UICamera_ProcessFakeTouches_m3498912331 (UICamera_t189364953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ProcessOthers()
extern "C"  void UICamera_ProcessOthers_m3482327090 (UICamera_t189364953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ProcessTouch(System.Boolean,System.Boolean)
extern "C"  void UICamera_ProcessTouch_m3860306996 (UICamera_t189364953 * __this, bool ___pressed0, bool ___unpressed1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ShowTooltip(System.Boolean)
extern "C"  void UICamera_ShowTooltip_m3052573053 (UICamera_t189364953 * __this, bool ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::OnApplicationPause()
extern "C"  void UICamera_OnApplicationPause_m2276564135 (UICamera_t189364953 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera::<Raycast>m__3BA(UICamera/DepthEntry,UICamera/DepthEntry)
extern "C"  int32_t UICamera_U3CRaycastU3Em__3BA_m758129196 (Il2CppObject * __this /* static, unused */, DepthEntry_t1145614469  ___r10, DepthEntry_t1145614469  ___r21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera::<Raycast>m__3BB(UICamera/DepthEntry,UICamera/DepthEntry)
extern "C"  int32_t UICamera_U3CRaycastU3Em__3BB_m882202443 (Il2CppObject * __this /* static, unused */, DepthEntry_t1145614469  ___r10, DepthEntry_t1145614469  ___r21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::ilo_get_isOverUI1(UICamera/MouseOrTouch)
extern "C"  bool UICamera_ilo_get_isOverUI1_m1495548534 (Il2CppObject * __this /* static, unused */, MouseOrTouch_t2057376333 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ilo_SetSelection2(UnityEngine.GameObject,UICamera/ControlScheme)
extern "C"  void UICamera_ilo_SetSelection2_m3430895949 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, int32_t ___scheme1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UICamera::ilo_ChangeSelection3(UICamera)
extern "C"  Il2CppObject * UICamera_ilo_ChangeSelection3_m2651757659 (Il2CppObject * __this /* static, unused */, UICamera_t189364953 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UICamera::ilo_get_cachedCamera4(UICamera)
extern "C"  Camera_t2727095145 * UICamera_ilo_get_cachedCamera4_m3953336471 (Il2CppObject * __this /* static, unused */, UICamera_t189364953 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::ilo_Invoke5(UIWidget/HitCheck,UnityEngine.Vector3)
extern "C"  bool UICamera_ilo_Invoke5_m3042388035 (Il2CppObject * __this /* static, unused */, HitCheck_t3889696652 * ____this0, Vector3_t4282066566  ___worldPos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::ilo_IsVisible6(UICamera/DepthEntry&)
extern "C"  bool UICamera_ilo_IsVisible6_m3007138294 (Il2CppObject * __this /* static, unused */, DepthEntry_t1145614469 * ___de0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::ilo_get_isVisible7(UIWidget)
extern "C"  bool UICamera_ilo_get_isVisible7_m2990443529 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::ilo_IsVisible8(UnityEngine.Vector3,UnityEngine.GameObject)
extern "C"  bool UICamera_ilo_IsVisible8_m996888798 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___worldPoint0, GameObject_t3674682005 * ___go1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera::ilo_CalculateRaycastDepth9(UnityEngine.GameObject)
extern "C"  int32_t UICamera_ilo_CalculateRaycastDepth9_m1425510196 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::ilo_IsVisible10(UIPanel,UnityEngine.Vector3)
extern "C"  bool UICamera_ilo_IsVisible10_m1846716725 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, Vector3_t4282066566  ___worldPos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::ilo_Invoke11(UICamera/GetKeyStateFunc,UnityEngine.KeyCode)
extern "C"  bool UICamera_ilo_Invoke11_m3185584162 (Il2CppObject * __this /* static, unused */, GetKeyStateFunc_t2745734774 * ____this0, int32_t ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::ilo_GetActive12(UnityEngine.GameObject)
extern "C"  bool UICamera_ilo_GetActive12_m2212550798 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ilo_ProcessTouches13(UICamera)
extern "C"  void UICamera_ilo_ProcessTouches13_m842258492 (Il2CppObject * __this /* static, unused */, UICamera_t189364953 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ilo_ProcessOthers14(UICamera)
extern "C"  void UICamera_ilo_ProcessOthers14_m2851518281 (Il2CppObject * __this /* static, unused */, UICamera_t189364953 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ilo_Notify15(UnityEngine.GameObject,System.String,System.Object)
extern "C"  void UICamera_ilo_Notify15_m2167370628 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, String_t* ___funcName1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UICamera::ilo_get_time16()
extern "C"  float UICamera_ilo_get_time16_m4096810812 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::ilo_get_handlesEvents17(UICamera)
extern "C"  bool UICamera_ilo_get_handlesEvents17_m3292991841 (Il2CppObject * __this /* static, unused */, UICamera_t189364953 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ilo_Broadcast18(System.String)
extern "C"  void UICamera_ilo_Broadcast18_m4145285517 (Il2CppObject * __this /* static, unused */, String_t* ___funcName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ilo_Invoke19(UICamera/OnScreenResize)
extern "C"  void UICamera_ilo_Invoke19_m1586861160 (Il2CppObject * __this /* static, unused */, OnScreenResize_t3828578773 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICamera::ilo_Raycast20(UnityEngine.Vector3)
extern "C"  bool UICamera_ilo_Raycast20_m3210890617 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___inPos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ilo_ShowTooltip21(UICamera,System.Boolean)
extern "C"  void UICamera_ilo_ShowTooltip21_m1742834020 (Il2CppObject * __this /* static, unused */, UICamera_t189364953 * ____this0, bool ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ilo_Invoke22(UICamera/BoolDelegate,UnityEngine.GameObject,System.Boolean)
extern "C"  void UICamera_ilo_Invoke22_m2764252223 (Il2CppObject * __this /* static, unused */, BoolDelegate_t2094540325 * ____this0, GameObject_t3674682005 * ___go1, bool ___state2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/MouseOrTouch UICamera::ilo_GetTouch23(System.Int32)
extern "C"  MouseOrTouch_t2057376333 * UICamera_ilo_GetTouch23_m2592507926 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ilo_ProcessTouch24(UICamera,System.Boolean,System.Boolean)
extern "C"  void UICamera_ilo_ProcessTouch24_m1184015246 (Il2CppObject * __this /* static, unused */, UICamera_t189364953 * ____this0, bool ___pressed1, bool ___unpressed2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera::ilo_GetDirection25(UnityEngine.KeyCode,UnityEngine.KeyCode)
extern "C"  int32_t UICamera_ilo_GetDirection25_m946166501 (Il2CppObject * __this /* static, unused */, int32_t ___up0, int32_t ___down1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICamera::ilo_GetDirection26(System.String)
extern "C"  int32_t UICamera_ilo_GetDirection26_m2394249810 (Il2CppObject * __this /* static, unused */, String_t* ___axis0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ilo_Invoke27(UICamera/KeyCodeDelegate,UnityEngine.GameObject,UnityEngine.KeyCode)
extern "C"  void UICamera_ilo_Invoke27_m1254042206 (Il2CppObject * __this /* static, unused */, KeyCodeDelegate_t4120455995 * ____this0, GameObject_t3674682005 * ___go1, int32_t ___key2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ilo_Invoke28(UICamera/VoidDelegate,UnityEngine.GameObject)
extern "C"  void UICamera_ilo_Invoke28_m3790788526 (Il2CppObject * __this /* static, unused */, VoidDelegate_t3135042255 * ____this0, GameObject_t3674682005 * ___go1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ilo_Invoke29(UICamera/ObjectDelegate,UnityEngine.GameObject,UnityEngine.GameObject)
extern "C"  void UICamera_ilo_Invoke29_m648952548 (Il2CppObject * __this /* static, unused */, ObjectDelegate_t3703364602 * ____this0, GameObject_t3674682005 * ___go1, GameObject_t3674682005 * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICamera::ilo_RemoveTouch30(System.Int32)
extern "C"  void UICamera_ilo_RemoveTouch30_m1091319926 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
