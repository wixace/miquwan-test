﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SwipeGesture
struct SwipeGesture_t529355983;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_FingerGestures_SwipeDirection1218055201.h"

// System.Void SwipeGesture::.ctor()
extern "C"  void SwipeGesture__ctor_m928749228 (SwipeGesture_t529355983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 SwipeGesture::get_Move()
extern "C"  Vector2_t4282066565  SwipeGesture_get_Move_m2475572877 (SwipeGesture_t529355983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeGesture::set_Move(UnityEngine.Vector2)
extern "C"  void SwipeGesture_set_Move_m1343855188 (SwipeGesture_t529355983 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SwipeGesture::get_Velocity()
extern "C"  float SwipeGesture_get_Velocity_m2330737418 (SwipeGesture_t529355983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeGesture::set_Velocity(System.Single)
extern "C"  void SwipeGesture_set_Velocity_m3261503289 (SwipeGesture_t529355983 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/SwipeDirection SwipeGesture::get_Direction()
extern "C"  int32_t SwipeGesture_get_Direction_m3277007246 (SwipeGesture_t529355983 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SwipeGesture::set_Direction(FingerGestures/SwipeDirection)
extern "C"  void SwipeGesture_set_Direction_m3550768389 (SwipeGesture_t529355983 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
