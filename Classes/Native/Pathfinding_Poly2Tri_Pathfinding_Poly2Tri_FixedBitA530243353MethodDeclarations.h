﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator1
struct U3CEnumerateU3Ec__Iterator1_t530243353;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Boolean>
struct IEnumerator_1_t2388663767;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator1::.ctor()
extern "C"  void U3CEnumerateU3Ec__Iterator1__ctor_m4172945681 (U3CEnumerateU3Ec__Iterator1_t530243353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator1::System.Collections.Generic.IEnumerator<bool>.get_Current()
extern "C"  bool U3CEnumerateU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CboolU3E_get_Current_m877570709 (U3CEnumerateU3Ec__Iterator1_t530243353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEnumerateU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m2547457461 (U3CEnumerateU3Ec__Iterator1_t530243353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Pathfinding.Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator1::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CEnumerateU3Ec__Iterator1_System_Collections_IEnumerable_GetEnumerator_m430644878 (U3CEnumerateU3Ec__Iterator1_t530243353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Boolean> Pathfinding.Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator1::System.Collections.Generic.IEnumerable<bool>.GetEnumerator()
extern "C"  Il2CppObject* U3CEnumerateU3Ec__Iterator1_System_Collections_Generic_IEnumerableU3CboolU3E_GetEnumerator_m2073272258 (U3CEnumerateU3Ec__Iterator1_t530243353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator1::MoveNext()
extern "C"  bool U3CEnumerateU3Ec__Iterator1_MoveNext_m3244669857 (U3CEnumerateU3Ec__Iterator1_t530243353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator1::Dispose()
extern "C"  void U3CEnumerateU3Ec__Iterator1_Dispose_m2894595342 (U3CEnumerateU3Ec__Iterator1_t530243353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.FixedBitArray3/<Enumerate>c__Iterator1::Reset()
extern "C"  void U3CEnumerateU3Ec__Iterator1_Reset_m1819378622 (U3CEnumerateU3Ec__Iterator1_t530243353 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
