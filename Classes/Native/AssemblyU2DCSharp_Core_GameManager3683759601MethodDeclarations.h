﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Core.GameManager
struct GameManager_t3683759601;
// Core.RpsStatistics
struct RpsStatistics_t1047797582;
// UnityEngine.Sprite
struct Sprite_t3199167241;
// Core.RpsHandler
struct RpsHandler_t115594153;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Core_RpsChoice52797132.h"
#include "AssemblyU2DCSharp_Core_RpsStatistics1047797582.h"
#include "AssemblyU2DCSharp_Core_GameManager3683759601.h"
#include "AssemblyU2DCSharp_Core_RpsHandler115594153.h"

// System.Void Core.GameManager::.ctor()
extern "C"  void GameManager__ctor_m3354140815 (GameManager_t3683759601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Core.GameManager::Start()
extern "C"  void GameManager_Start_m2301278607 (GameManager_t3683759601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Core.GameManager::MakeChoice(Core.RpsChoice)
extern "C"  void GameManager_MakeChoice_m2357787261 (GameManager_t3683759601 * __this, int32_t ___choice0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Core.GameManager::Choose()
extern "C"  void GameManager_Choose_m3137922604 (GameManager_t3683759601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Core.GameManager::Restart()
extern "C"  void GameManager_Restart_m4187840764 (GameManager_t3683759601 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Core.GameManager::OnRpsResultChanged(Core.RpsStatistics)
extern "C"  void GameManager_OnRpsResultChanged_m3775800941 (GameManager_t3683759601 * __this, RpsStatistics_t1047797582 * ___rpsStatistics0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite Core.GameManager::GetSprite(Core.RpsChoice)
extern "C"  Sprite_t3199167241 * GameManager_GetSprite_m3708931840 (GameManager_t3683759601 * __this, int32_t ___choice0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Sprite Core.GameManager::ilo_GetSprite1(Core.GameManager,Core.RpsChoice)
extern "C"  Sprite_t3199167241 * GameManager_ilo_GetSprite1_m2838272268 (Il2CppObject * __this /* static, unused */, GameManager_t3683759601 * ____this0, int32_t ___choice1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Core.GameManager::ilo_Restart2(Core.RpsHandler)
extern "C"  void GameManager_ilo_Restart2_m2109002435 (Il2CppObject * __this /* static, unused */, RpsHandler_t115594153 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
