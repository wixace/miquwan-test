﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2284652049MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m475443477(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1700550115 *, Dictionary_2_t383226723 *, const MethodInfo*))Enumerator__ctor_m2839623054_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1102763692(__this, method) ((  Il2CppObject * (*) (Enumerator_t1700550115 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4140471197_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1031715968(__this, method) ((  void (*) (Enumerator_t1700550115 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3207216743_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m251473033(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1700550115 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1172003166_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1543594632(__this, method) ((  Il2CppObject * (*) (Enumerator_t1700550115 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m2853299897_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2679160602(__this, method) ((  Il2CppObject * (*) (Enumerator_t1700550115 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2880502539_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::MoveNext()
#define Enumerator_MoveNext_m2840074019(__this, method) ((  bool (*) (Enumerator_t1700550115 *, const MethodInfo*))Enumerator_MoveNext_m14324631_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::get_Current()
#define Enumerator_get_Current_m3591792412(__this, method) ((  KeyValuePair_2_t282007429  (*) (Enumerator_t1700550115 *, const MethodInfo*))Enumerator_get_Current_m2428217029_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m4157316153(__this, method) ((  Int2_t1974045593  (*) (Enumerator_t1700550115 *, const MethodInfo*))Enumerator_get_CurrentKey_m1838538208_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m2480265629(__this, method) ((  ProceduralTile_t3586714437 * (*) (Enumerator_t1700550115 *, const MethodInfo*))Enumerator_get_CurrentValue_m231599392_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::Reset()
#define Enumerator_Reset_m4164975591(__this, method) ((  void (*) (Enumerator_t1700550115 *, const MethodInfo*))Enumerator_Reset_m1210538720_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::VerifyState()
#define Enumerator_VerifyState_m1418396336(__this, method) ((  void (*) (Enumerator_t1700550115 *, const MethodInfo*))Enumerator_VerifyState_m3141731049_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m24150552(__this, method) ((  void (*) (Enumerator_t1700550115 *, const MethodInfo*))Enumerator_VerifyCurrent_m2586400785_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::Dispose()
#define Enumerator_Dispose_m2155452151(__this, method) ((  void (*) (Enumerator_t1700550115 *, const MethodInfo*))Enumerator_Dispose_m1915001776_gshared)(__this, method)
