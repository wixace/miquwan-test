﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"

// System.Void Loader.PathUtil::.cctor()
extern "C"  void PathUtil__cctor_m1421031524 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader.PathUtil::Init()
extern "C"  void PathUtil_Init_m1884241099 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Loader.PathUtil::StreamingAssetsPath(System.String)
extern "C"  String_t* PathUtil_StreamingAssetsPath_m3431132878 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Loader.PathUtil::PersistentDataPath(System.String)
extern "C"  String_t* PathUtil_PersistentDataPath_m516449444 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Loader.PathUtil::CheckPath(System.String)
extern "C"  String_t* PathUtil_CheckPath_m1683770411 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Loader.PathUtil::JoinPath(System.String[])
extern "C"  String_t* PathUtil_JoinPath_m3575016281 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t4054002952* ___arg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Loader.PathUtil::JoinPath(System.String,System.String)
extern "C"  String_t* PathUtil_JoinPath_m3597356023 (Il2CppObject * __this /* static, unused */, String_t* ___pathA0, String_t* ___pathB1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Loader.PathUtil::GetFilePathWithoutExtension(System.String)
extern "C"  String_t* PathUtil_GetFilePathWithoutExtension_m1038436330 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Loader.PathUtil::ServerPath(System.String)
extern "C"  String_t* PathUtil_ServerPath_m1326091778 (Il2CppObject * __this /* static, unused */, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Loader.PathUtil::GetPersistentDataPath()
extern "C"  String_t* PathUtil_GetPersistentDataPath_m3318009414 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Loader.PathUtil::ilo_get_Item1(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * PathUtil_ilo_get_Item1_m2923875264 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Loader.PathUtil::ilo_ContainsKey2(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  bool PathUtil_ilo_ContainsKey2_m4241928920 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
