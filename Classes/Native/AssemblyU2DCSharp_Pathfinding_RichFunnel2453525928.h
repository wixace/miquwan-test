﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>
struct List_1_t2994434301;
// Pathfinding.IFunnelGraph
struct IFunnelGraph_t2367181445;
// Pathfinding.RichPath
struct RichPath_t1926198167;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "AssemblyU2DCSharp_Pathfinding_RichPathPart2520985130.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_RichFunnel_FunnelSimp683755508.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RichFunnel
struct  RichFunnel_t2453525928  : public RichPathPart_t2520985130
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Pathfinding.RichFunnel::left
	List_1_t1355284822 * ___left_0;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Pathfinding.RichFunnel::right
	List_1_t1355284822 * ___right_1;
	// System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode> Pathfinding.RichFunnel::nodes
	List_1_t2994434301 * ___nodes_2;
	// UnityEngine.Vector3 Pathfinding.RichFunnel::exactStart
	Vector3_t4282066566  ___exactStart_3;
	// UnityEngine.Vector3 Pathfinding.RichFunnel::exactEnd
	Vector3_t4282066566  ___exactEnd_4;
	// Pathfinding.IFunnelGraph Pathfinding.RichFunnel::graph
	Il2CppObject * ___graph_5;
	// System.Int32 Pathfinding.RichFunnel::currentNode
	int32_t ___currentNode_6;
	// UnityEngine.Vector3 Pathfinding.RichFunnel::currentPosition
	Vector3_t4282066566  ___currentPosition_7;
	// System.Int32 Pathfinding.RichFunnel::tmpCounter
	int32_t ___tmpCounter_8;
	// Pathfinding.RichPath Pathfinding.RichFunnel::path
	RichPath_t1926198167 * ___path_9;
	// System.Int32[] Pathfinding.RichFunnel::triBuffer
	Int32U5BU5D_t3230847821* ___triBuffer_10;
	// Pathfinding.RichFunnel/FunnelSimplification Pathfinding.RichFunnel::funnelSimplificationMode
	int32_t ___funnelSimplificationMode_11;

public:
	inline static int32_t get_offset_of_left_0() { return static_cast<int32_t>(offsetof(RichFunnel_t2453525928, ___left_0)); }
	inline List_1_t1355284822 * get_left_0() const { return ___left_0; }
	inline List_1_t1355284822 ** get_address_of_left_0() { return &___left_0; }
	inline void set_left_0(List_1_t1355284822 * value)
	{
		___left_0 = value;
		Il2CppCodeGenWriteBarrier(&___left_0, value);
	}

	inline static int32_t get_offset_of_right_1() { return static_cast<int32_t>(offsetof(RichFunnel_t2453525928, ___right_1)); }
	inline List_1_t1355284822 * get_right_1() const { return ___right_1; }
	inline List_1_t1355284822 ** get_address_of_right_1() { return &___right_1; }
	inline void set_right_1(List_1_t1355284822 * value)
	{
		___right_1 = value;
		Il2CppCodeGenWriteBarrier(&___right_1, value);
	}

	inline static int32_t get_offset_of_nodes_2() { return static_cast<int32_t>(offsetof(RichFunnel_t2453525928, ___nodes_2)); }
	inline List_1_t2994434301 * get_nodes_2() const { return ___nodes_2; }
	inline List_1_t2994434301 ** get_address_of_nodes_2() { return &___nodes_2; }
	inline void set_nodes_2(List_1_t2994434301 * value)
	{
		___nodes_2 = value;
		Il2CppCodeGenWriteBarrier(&___nodes_2, value);
	}

	inline static int32_t get_offset_of_exactStart_3() { return static_cast<int32_t>(offsetof(RichFunnel_t2453525928, ___exactStart_3)); }
	inline Vector3_t4282066566  get_exactStart_3() const { return ___exactStart_3; }
	inline Vector3_t4282066566 * get_address_of_exactStart_3() { return &___exactStart_3; }
	inline void set_exactStart_3(Vector3_t4282066566  value)
	{
		___exactStart_3 = value;
	}

	inline static int32_t get_offset_of_exactEnd_4() { return static_cast<int32_t>(offsetof(RichFunnel_t2453525928, ___exactEnd_4)); }
	inline Vector3_t4282066566  get_exactEnd_4() const { return ___exactEnd_4; }
	inline Vector3_t4282066566 * get_address_of_exactEnd_4() { return &___exactEnd_4; }
	inline void set_exactEnd_4(Vector3_t4282066566  value)
	{
		___exactEnd_4 = value;
	}

	inline static int32_t get_offset_of_graph_5() { return static_cast<int32_t>(offsetof(RichFunnel_t2453525928, ___graph_5)); }
	inline Il2CppObject * get_graph_5() const { return ___graph_5; }
	inline Il2CppObject ** get_address_of_graph_5() { return &___graph_5; }
	inline void set_graph_5(Il2CppObject * value)
	{
		___graph_5 = value;
		Il2CppCodeGenWriteBarrier(&___graph_5, value);
	}

	inline static int32_t get_offset_of_currentNode_6() { return static_cast<int32_t>(offsetof(RichFunnel_t2453525928, ___currentNode_6)); }
	inline int32_t get_currentNode_6() const { return ___currentNode_6; }
	inline int32_t* get_address_of_currentNode_6() { return &___currentNode_6; }
	inline void set_currentNode_6(int32_t value)
	{
		___currentNode_6 = value;
	}

	inline static int32_t get_offset_of_currentPosition_7() { return static_cast<int32_t>(offsetof(RichFunnel_t2453525928, ___currentPosition_7)); }
	inline Vector3_t4282066566  get_currentPosition_7() const { return ___currentPosition_7; }
	inline Vector3_t4282066566 * get_address_of_currentPosition_7() { return &___currentPosition_7; }
	inline void set_currentPosition_7(Vector3_t4282066566  value)
	{
		___currentPosition_7 = value;
	}

	inline static int32_t get_offset_of_tmpCounter_8() { return static_cast<int32_t>(offsetof(RichFunnel_t2453525928, ___tmpCounter_8)); }
	inline int32_t get_tmpCounter_8() const { return ___tmpCounter_8; }
	inline int32_t* get_address_of_tmpCounter_8() { return &___tmpCounter_8; }
	inline void set_tmpCounter_8(int32_t value)
	{
		___tmpCounter_8 = value;
	}

	inline static int32_t get_offset_of_path_9() { return static_cast<int32_t>(offsetof(RichFunnel_t2453525928, ___path_9)); }
	inline RichPath_t1926198167 * get_path_9() const { return ___path_9; }
	inline RichPath_t1926198167 ** get_address_of_path_9() { return &___path_9; }
	inline void set_path_9(RichPath_t1926198167 * value)
	{
		___path_9 = value;
		Il2CppCodeGenWriteBarrier(&___path_9, value);
	}

	inline static int32_t get_offset_of_triBuffer_10() { return static_cast<int32_t>(offsetof(RichFunnel_t2453525928, ___triBuffer_10)); }
	inline Int32U5BU5D_t3230847821* get_triBuffer_10() const { return ___triBuffer_10; }
	inline Int32U5BU5D_t3230847821** get_address_of_triBuffer_10() { return &___triBuffer_10; }
	inline void set_triBuffer_10(Int32U5BU5D_t3230847821* value)
	{
		___triBuffer_10 = value;
		Il2CppCodeGenWriteBarrier(&___triBuffer_10, value);
	}

	inline static int32_t get_offset_of_funnelSimplificationMode_11() { return static_cast<int32_t>(offsetof(RichFunnel_t2453525928, ___funnelSimplificationMode_11)); }
	inline int32_t get_funnelSimplificationMode_11() const { return ___funnelSimplificationMode_11; }
	inline int32_t* get_address_of_funnelSimplificationMode_11() { return &___funnelSimplificationMode_11; }
	inline void set_funnelSimplificationMode_11(int32_t value)
	{
		___funnelSimplificationMode_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
