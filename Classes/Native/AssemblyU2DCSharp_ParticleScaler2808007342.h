﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticleScaler
struct  ParticleScaler_t2808007342  : public MonoBehaviour_t667441552
{
public:
	// System.Single ParticleScaler::particleScale
	float ___particleScale_2;
	// System.Boolean ParticleScaler::alsoScaleGameobject
	bool ___alsoScaleGameobject_3;
	// System.Single ParticleScaler::prevScale
	float ___prevScale_4;

public:
	inline static int32_t get_offset_of_particleScale_2() { return static_cast<int32_t>(offsetof(ParticleScaler_t2808007342, ___particleScale_2)); }
	inline float get_particleScale_2() const { return ___particleScale_2; }
	inline float* get_address_of_particleScale_2() { return &___particleScale_2; }
	inline void set_particleScale_2(float value)
	{
		___particleScale_2 = value;
	}

	inline static int32_t get_offset_of_alsoScaleGameobject_3() { return static_cast<int32_t>(offsetof(ParticleScaler_t2808007342, ___alsoScaleGameobject_3)); }
	inline bool get_alsoScaleGameobject_3() const { return ___alsoScaleGameobject_3; }
	inline bool* get_address_of_alsoScaleGameobject_3() { return &___alsoScaleGameobject_3; }
	inline void set_alsoScaleGameobject_3(bool value)
	{
		___alsoScaleGameobject_3 = value;
	}

	inline static int32_t get_offset_of_prevScale_4() { return static_cast<int32_t>(offsetof(ParticleScaler_t2808007342, ___prevScale_4)); }
	inline float get_prevScale_4() const { return ___prevScale_4; }
	inline float* get_address_of_prevScale_4() { return &___prevScale_4; }
	inline void set_prevScale_4(float value)
	{
		___prevScale_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
