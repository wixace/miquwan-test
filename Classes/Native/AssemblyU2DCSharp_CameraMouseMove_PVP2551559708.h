﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.Animator
struct Animator_t2776330603;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraMouseMove_PVP
struct  CameraMouseMove_PVP_t2551559708  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Camera CameraMouseMove_PVP::mCamera
	Camera_t2727095145 * ___mCamera_2;
	// UnityEngine.Animator CameraMouseMove_PVP::animator
	Animator_t2776330603 * ___animator_3;
	// System.Int32 CameraMouseMove_PVP::cameraMoveSpeed
	int32_t ___cameraMoveSpeed_4;
	// System.String CameraMouseMove_PVP::camera_limit
	String_t* ___camera_limit_5;
	// System.Boolean CameraMouseMove_PVP::CanMoveY
	bool ___CanMoveY_6;
	// System.Boolean CameraMouseMove_PVP::CanMoveZ
	bool ___CanMoveZ_7;
	// System.Boolean CameraMouseMove_PVP::CanMoveCamera
	bool ___CanMoveCamera_8;
	// System.Single CameraMouseMove_PVP::originalPositionX_min
	float ___originalPositionX_min_9;
	// System.Single CameraMouseMove_PVP::originalPositionX_max
	float ___originalPositionX_max_10;
	// System.Single CameraMouseMove_PVP::originalPositionY_min
	float ___originalPositionY_min_11;
	// System.Single CameraMouseMove_PVP::originalPositionY_max
	float ___originalPositionY_max_12;
	// System.Single CameraMouseMove_PVP::originalPositionZ_min
	float ___originalPositionZ_min_13;
	// System.Single CameraMouseMove_PVP::originalPositionZ_max
	float ___originalPositionZ_max_14;
	// UnityEngine.Vector3 CameraMouseMove_PVP::cameraScreenPoint
	Vector3_t4282066566  ___cameraScreenPoint_15;
	// UnityEngine.Vector3 CameraMouseMove_PVP::beginMouseWorld
	Vector3_t4282066566  ___beginMouseWorld_16;
	// UnityEngine.Vector3 CameraMouseMove_PVP::currentMouseWorldPoint
	Vector3_t4282066566  ___currentMouseWorldPoint_17;
	// UnityEngine.Vector3 CameraMouseMove_PVP::offsetPosition
	Vector3_t4282066566  ___offsetPosition_18;
	// UnityEngine.Vector3 CameraMouseMove_PVP::targetPosition
	Vector3_t4282066566  ___targetPosition_19;

public:
	inline static int32_t get_offset_of_mCamera_2() { return static_cast<int32_t>(offsetof(CameraMouseMove_PVP_t2551559708, ___mCamera_2)); }
	inline Camera_t2727095145 * get_mCamera_2() const { return ___mCamera_2; }
	inline Camera_t2727095145 ** get_address_of_mCamera_2() { return &___mCamera_2; }
	inline void set_mCamera_2(Camera_t2727095145 * value)
	{
		___mCamera_2 = value;
		Il2CppCodeGenWriteBarrier(&___mCamera_2, value);
	}

	inline static int32_t get_offset_of_animator_3() { return static_cast<int32_t>(offsetof(CameraMouseMove_PVP_t2551559708, ___animator_3)); }
	inline Animator_t2776330603 * get_animator_3() const { return ___animator_3; }
	inline Animator_t2776330603 ** get_address_of_animator_3() { return &___animator_3; }
	inline void set_animator_3(Animator_t2776330603 * value)
	{
		___animator_3 = value;
		Il2CppCodeGenWriteBarrier(&___animator_3, value);
	}

	inline static int32_t get_offset_of_cameraMoveSpeed_4() { return static_cast<int32_t>(offsetof(CameraMouseMove_PVP_t2551559708, ___cameraMoveSpeed_4)); }
	inline int32_t get_cameraMoveSpeed_4() const { return ___cameraMoveSpeed_4; }
	inline int32_t* get_address_of_cameraMoveSpeed_4() { return &___cameraMoveSpeed_4; }
	inline void set_cameraMoveSpeed_4(int32_t value)
	{
		___cameraMoveSpeed_4 = value;
	}

	inline static int32_t get_offset_of_camera_limit_5() { return static_cast<int32_t>(offsetof(CameraMouseMove_PVP_t2551559708, ___camera_limit_5)); }
	inline String_t* get_camera_limit_5() const { return ___camera_limit_5; }
	inline String_t** get_address_of_camera_limit_5() { return &___camera_limit_5; }
	inline void set_camera_limit_5(String_t* value)
	{
		___camera_limit_5 = value;
		Il2CppCodeGenWriteBarrier(&___camera_limit_5, value);
	}

	inline static int32_t get_offset_of_CanMoveY_6() { return static_cast<int32_t>(offsetof(CameraMouseMove_PVP_t2551559708, ___CanMoveY_6)); }
	inline bool get_CanMoveY_6() const { return ___CanMoveY_6; }
	inline bool* get_address_of_CanMoveY_6() { return &___CanMoveY_6; }
	inline void set_CanMoveY_6(bool value)
	{
		___CanMoveY_6 = value;
	}

	inline static int32_t get_offset_of_CanMoveZ_7() { return static_cast<int32_t>(offsetof(CameraMouseMove_PVP_t2551559708, ___CanMoveZ_7)); }
	inline bool get_CanMoveZ_7() const { return ___CanMoveZ_7; }
	inline bool* get_address_of_CanMoveZ_7() { return &___CanMoveZ_7; }
	inline void set_CanMoveZ_7(bool value)
	{
		___CanMoveZ_7 = value;
	}

	inline static int32_t get_offset_of_CanMoveCamera_8() { return static_cast<int32_t>(offsetof(CameraMouseMove_PVP_t2551559708, ___CanMoveCamera_8)); }
	inline bool get_CanMoveCamera_8() const { return ___CanMoveCamera_8; }
	inline bool* get_address_of_CanMoveCamera_8() { return &___CanMoveCamera_8; }
	inline void set_CanMoveCamera_8(bool value)
	{
		___CanMoveCamera_8 = value;
	}

	inline static int32_t get_offset_of_originalPositionX_min_9() { return static_cast<int32_t>(offsetof(CameraMouseMove_PVP_t2551559708, ___originalPositionX_min_9)); }
	inline float get_originalPositionX_min_9() const { return ___originalPositionX_min_9; }
	inline float* get_address_of_originalPositionX_min_9() { return &___originalPositionX_min_9; }
	inline void set_originalPositionX_min_9(float value)
	{
		___originalPositionX_min_9 = value;
	}

	inline static int32_t get_offset_of_originalPositionX_max_10() { return static_cast<int32_t>(offsetof(CameraMouseMove_PVP_t2551559708, ___originalPositionX_max_10)); }
	inline float get_originalPositionX_max_10() const { return ___originalPositionX_max_10; }
	inline float* get_address_of_originalPositionX_max_10() { return &___originalPositionX_max_10; }
	inline void set_originalPositionX_max_10(float value)
	{
		___originalPositionX_max_10 = value;
	}

	inline static int32_t get_offset_of_originalPositionY_min_11() { return static_cast<int32_t>(offsetof(CameraMouseMove_PVP_t2551559708, ___originalPositionY_min_11)); }
	inline float get_originalPositionY_min_11() const { return ___originalPositionY_min_11; }
	inline float* get_address_of_originalPositionY_min_11() { return &___originalPositionY_min_11; }
	inline void set_originalPositionY_min_11(float value)
	{
		___originalPositionY_min_11 = value;
	}

	inline static int32_t get_offset_of_originalPositionY_max_12() { return static_cast<int32_t>(offsetof(CameraMouseMove_PVP_t2551559708, ___originalPositionY_max_12)); }
	inline float get_originalPositionY_max_12() const { return ___originalPositionY_max_12; }
	inline float* get_address_of_originalPositionY_max_12() { return &___originalPositionY_max_12; }
	inline void set_originalPositionY_max_12(float value)
	{
		___originalPositionY_max_12 = value;
	}

	inline static int32_t get_offset_of_originalPositionZ_min_13() { return static_cast<int32_t>(offsetof(CameraMouseMove_PVP_t2551559708, ___originalPositionZ_min_13)); }
	inline float get_originalPositionZ_min_13() const { return ___originalPositionZ_min_13; }
	inline float* get_address_of_originalPositionZ_min_13() { return &___originalPositionZ_min_13; }
	inline void set_originalPositionZ_min_13(float value)
	{
		___originalPositionZ_min_13 = value;
	}

	inline static int32_t get_offset_of_originalPositionZ_max_14() { return static_cast<int32_t>(offsetof(CameraMouseMove_PVP_t2551559708, ___originalPositionZ_max_14)); }
	inline float get_originalPositionZ_max_14() const { return ___originalPositionZ_max_14; }
	inline float* get_address_of_originalPositionZ_max_14() { return &___originalPositionZ_max_14; }
	inline void set_originalPositionZ_max_14(float value)
	{
		___originalPositionZ_max_14 = value;
	}

	inline static int32_t get_offset_of_cameraScreenPoint_15() { return static_cast<int32_t>(offsetof(CameraMouseMove_PVP_t2551559708, ___cameraScreenPoint_15)); }
	inline Vector3_t4282066566  get_cameraScreenPoint_15() const { return ___cameraScreenPoint_15; }
	inline Vector3_t4282066566 * get_address_of_cameraScreenPoint_15() { return &___cameraScreenPoint_15; }
	inline void set_cameraScreenPoint_15(Vector3_t4282066566  value)
	{
		___cameraScreenPoint_15 = value;
	}

	inline static int32_t get_offset_of_beginMouseWorld_16() { return static_cast<int32_t>(offsetof(CameraMouseMove_PVP_t2551559708, ___beginMouseWorld_16)); }
	inline Vector3_t4282066566  get_beginMouseWorld_16() const { return ___beginMouseWorld_16; }
	inline Vector3_t4282066566 * get_address_of_beginMouseWorld_16() { return &___beginMouseWorld_16; }
	inline void set_beginMouseWorld_16(Vector3_t4282066566  value)
	{
		___beginMouseWorld_16 = value;
	}

	inline static int32_t get_offset_of_currentMouseWorldPoint_17() { return static_cast<int32_t>(offsetof(CameraMouseMove_PVP_t2551559708, ___currentMouseWorldPoint_17)); }
	inline Vector3_t4282066566  get_currentMouseWorldPoint_17() const { return ___currentMouseWorldPoint_17; }
	inline Vector3_t4282066566 * get_address_of_currentMouseWorldPoint_17() { return &___currentMouseWorldPoint_17; }
	inline void set_currentMouseWorldPoint_17(Vector3_t4282066566  value)
	{
		___currentMouseWorldPoint_17 = value;
	}

	inline static int32_t get_offset_of_offsetPosition_18() { return static_cast<int32_t>(offsetof(CameraMouseMove_PVP_t2551559708, ___offsetPosition_18)); }
	inline Vector3_t4282066566  get_offsetPosition_18() const { return ___offsetPosition_18; }
	inline Vector3_t4282066566 * get_address_of_offsetPosition_18() { return &___offsetPosition_18; }
	inline void set_offsetPosition_18(Vector3_t4282066566  value)
	{
		___offsetPosition_18 = value;
	}

	inline static int32_t get_offset_of_targetPosition_19() { return static_cast<int32_t>(offsetof(CameraMouseMove_PVP_t2551559708, ___targetPosition_19)); }
	inline Vector3_t4282066566  get_targetPosition_19() const { return ___targetPosition_19; }
	inline Vector3_t4282066566 * get_address_of_targetPosition_19() { return &___targetPosition_19; }
	inline void set_targetPosition_19(Vector3_t4282066566  value)
	{
		___targetPosition_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
