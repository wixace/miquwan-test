﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIUVAni
struct UIUVAni_t299456871;
// UITexture
struct UITexture_t3903132647;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UITexture3903132647.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

// System.Void UIUVAni::.ctor()
extern "C"  void UIUVAni__ctor_m1984985572 (UIUVAni_t299456871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIUVAni::Start()
extern "C"  void UIUVAni_Start_m932123364 (UIUVAni_t299456871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIUVAni::Update()
extern "C"  void UIUVAni_Update_m3131872681 (UIUVAni_t299456871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIUVAni::ilo_set_uvRect1(UITexture,UnityEngine.Rect)
extern "C"  void UIUVAni_ilo_set_uvRect1_m3678927262 (Il2CppObject * __this /* static, unused */, UITexture_t3903132647 * ____this0, Rect_t4241904616  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
