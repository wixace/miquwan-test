﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_Thread3745804690MethodDeclarations.h"

// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Func`2<System.Object,System.Object>>::.ctor(System.Func`2<TKey,TValue>)
#define ThreadSafeStore_2__ctor_m738586306(__this, ___creator0, method) ((  void (*) (ThreadSafeStore_2_t4054519640 *, Func_2_t1364512968 *, const MethodInfo*))ThreadSafeStore_2__ctor_m1318482509_gshared)(__this, ___creator0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Func`2<System.Object,System.Object>>::Get(TKey)
#define ThreadSafeStore_2_Get_m1349815730(__this, ___key0, method) ((  Func_2_t184564025 * (*) (ThreadSafeStore_2_t4054519640 *, TypeConvertKey_t866134174 , const MethodInfo*))ThreadSafeStore_2_Get_m1258554877_gshared)(__this, ___key0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Utilities.ConvertUtils/TypeConvertKey,System.Func`2<System.Object,System.Object>>::AddValue(TKey)
#define ThreadSafeStore_2_AddValue_m2467558432(__this, ___key0, method) ((  Func_2_t184564025 * (*) (ThreadSafeStore_2_t4054519640 *, TypeConvertKey_t866134174 , const MethodInfo*))ThreadSafeStore_2_AddValue_m527102645_gshared)(__this, ___key0, method)
