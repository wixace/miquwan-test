﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraAnimation
struct CameraAnimation_t1209985471;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraAnimation::.ctor()
extern "C"  void CameraAnimation__ctor_m3867829388 (CameraAnimation_t1209985471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraAnimation::PlayAnimationEnd()
extern "C"  void CameraAnimation_PlayAnimationEnd_m653319683 (CameraAnimation_t1209985471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
