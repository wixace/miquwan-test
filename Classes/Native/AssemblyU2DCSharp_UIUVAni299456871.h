﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UITexture
struct UITexture_t3903132647;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "AssemblyU2DCSharp_UIUVAni_Axial844386293.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIUVAni
struct  UIUVAni_t299456871  : public MonoBehaviour_t667441552
{
public:
	// UITexture UIUVAni::ui
	UITexture_t3903132647 * ___ui_2;
	// UnityEngine.Rect UIUVAni::rect
	Rect_t4241904616  ___rect_3;
	// System.Single UIUVAni::curTime
	float ___curTime_4;
	// UIUVAni/Axial UIUVAni::axial
	int32_t ___axial_5;
	// System.Single UIUVAni::max
	float ___max_6;
	// System.Single UIUVAni::min
	float ___min_7;
	// System.Single UIUVAni::time
	float ___time_8;
	// System.Single UIUVAni::gapTime
	float ___gapTime_9;
	// System.Boolean UIUVAni::isGap
	bool ___isGap_10;

public:
	inline static int32_t get_offset_of_ui_2() { return static_cast<int32_t>(offsetof(UIUVAni_t299456871, ___ui_2)); }
	inline UITexture_t3903132647 * get_ui_2() const { return ___ui_2; }
	inline UITexture_t3903132647 ** get_address_of_ui_2() { return &___ui_2; }
	inline void set_ui_2(UITexture_t3903132647 * value)
	{
		___ui_2 = value;
		Il2CppCodeGenWriteBarrier(&___ui_2, value);
	}

	inline static int32_t get_offset_of_rect_3() { return static_cast<int32_t>(offsetof(UIUVAni_t299456871, ___rect_3)); }
	inline Rect_t4241904616  get_rect_3() const { return ___rect_3; }
	inline Rect_t4241904616 * get_address_of_rect_3() { return &___rect_3; }
	inline void set_rect_3(Rect_t4241904616  value)
	{
		___rect_3 = value;
	}

	inline static int32_t get_offset_of_curTime_4() { return static_cast<int32_t>(offsetof(UIUVAni_t299456871, ___curTime_4)); }
	inline float get_curTime_4() const { return ___curTime_4; }
	inline float* get_address_of_curTime_4() { return &___curTime_4; }
	inline void set_curTime_4(float value)
	{
		___curTime_4 = value;
	}

	inline static int32_t get_offset_of_axial_5() { return static_cast<int32_t>(offsetof(UIUVAni_t299456871, ___axial_5)); }
	inline int32_t get_axial_5() const { return ___axial_5; }
	inline int32_t* get_address_of_axial_5() { return &___axial_5; }
	inline void set_axial_5(int32_t value)
	{
		___axial_5 = value;
	}

	inline static int32_t get_offset_of_max_6() { return static_cast<int32_t>(offsetof(UIUVAni_t299456871, ___max_6)); }
	inline float get_max_6() const { return ___max_6; }
	inline float* get_address_of_max_6() { return &___max_6; }
	inline void set_max_6(float value)
	{
		___max_6 = value;
	}

	inline static int32_t get_offset_of_min_7() { return static_cast<int32_t>(offsetof(UIUVAni_t299456871, ___min_7)); }
	inline float get_min_7() const { return ___min_7; }
	inline float* get_address_of_min_7() { return &___min_7; }
	inline void set_min_7(float value)
	{
		___min_7 = value;
	}

	inline static int32_t get_offset_of_time_8() { return static_cast<int32_t>(offsetof(UIUVAni_t299456871, ___time_8)); }
	inline float get_time_8() const { return ___time_8; }
	inline float* get_address_of_time_8() { return &___time_8; }
	inline void set_time_8(float value)
	{
		___time_8 = value;
	}

	inline static int32_t get_offset_of_gapTime_9() { return static_cast<int32_t>(offsetof(UIUVAni_t299456871, ___gapTime_9)); }
	inline float get_gapTime_9() const { return ___gapTime_9; }
	inline float* get_address_of_gapTime_9() { return &___gapTime_9; }
	inline void set_gapTime_9(float value)
	{
		___gapTime_9 = value;
	}

	inline static int32_t get_offset_of_isGap_10() { return static_cast<int32_t>(offsetof(UIUVAni_t299456871, ___isGap_10)); }
	inline bool get_isGap_10() const { return ___isGap_10; }
	inline bool* get_address_of_isGap_10() { return &___isGap_10; }
	inline void set_isGap_10(bool value)
	{
		___isGap_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
