﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen3164635835.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsMode3080509312.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Nullable`1<SharpKit.JavaScript.JsMode>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2297815667_gshared (Nullable_1_t3164635835 * __this, int32_t ___value0, const MethodInfo* method);
#define Nullable_1__ctor_m2297815667(__this, ___value0, method) ((  void (*) (Nullable_1_t3164635835 *, int32_t, const MethodInfo*))Nullable_1__ctor_m2297815667_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<SharpKit.JavaScript.JsMode>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2124201516_gshared (Nullable_1_t3164635835 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m2124201516(__this, method) ((  bool (*) (Nullable_1_t3164635835 *, const MethodInfo*))Nullable_1_get_HasValue_m2124201516_gshared)(__this, method)
// T System.Nullable`1<SharpKit.JavaScript.JsMode>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m1502216177_gshared (Nullable_1_t3164635835 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m1502216177(__this, method) ((  int32_t (*) (Nullable_1_t3164635835 *, const MethodInfo*))Nullable_1_get_Value_m1502216177_gshared)(__this, method)
// System.Boolean System.Nullable`1<SharpKit.JavaScript.JsMode>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1568672511_gshared (Nullable_1_t3164635835 * __this, Il2CppObject * ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m1568672511(__this, ___other0, method) ((  bool (*) (Nullable_1_t3164635835 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1568672511_gshared)(__this, ___other0, method)
// System.Boolean System.Nullable`1<SharpKit.JavaScript.JsMode>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m185465696_gshared (Nullable_1_t3164635835 * __this, Nullable_1_t3164635835  ___other0, const MethodInfo* method);
#define Nullable_1_Equals_m185465696(__this, ___other0, method) ((  bool (*) (Nullable_1_t3164635835 *, Nullable_1_t3164635835 , const MethodInfo*))Nullable_1_Equals_m185465696_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<SharpKit.JavaScript.JsMode>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1595715031_gshared (Nullable_1_t3164635835 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m1595715031(__this, method) ((  int32_t (*) (Nullable_1_t3164635835 *, const MethodInfo*))Nullable_1_GetHashCode_m1595715031_gshared)(__this, method)
// T System.Nullable`1<SharpKit.JavaScript.JsMode>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3171265427_gshared (Nullable_1_t3164635835 * __this, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m3171265427(__this, method) ((  int32_t (*) (Nullable_1_t3164635835 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m3171265427_gshared)(__this, method)
// T System.Nullable`1<SharpKit.JavaScript.JsMode>::GetValueOrDefault(T)
extern "C"  int32_t Nullable_1_GetValueOrDefault_m500914066_gshared (Nullable_1_t3164635835 * __this, int32_t ___defaultValue0, const MethodInfo* method);
#define Nullable_1_GetValueOrDefault_m500914066(__this, ___defaultValue0, method) ((  int32_t (*) (Nullable_1_t3164635835 *, int32_t, const MethodInfo*))Nullable_1_GetValueOrDefault_m500914066_gshared)(__this, ___defaultValue0, method)
// System.String System.Nullable`1<SharpKit.JavaScript.JsMode>::ToString()
extern "C"  String_t* Nullable_1_ToString_m134921505_gshared (Nullable_1_t3164635835 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m134921505(__this, method) ((  String_t* (*) (Nullable_1_t3164635835 *, const MethodInfo*))Nullable_1_ToString_m134921505_gshared)(__this, method)
