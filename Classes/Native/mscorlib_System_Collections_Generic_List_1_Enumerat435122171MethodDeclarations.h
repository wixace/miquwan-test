﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<Pathfinding.Int2>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m791038778(__this, ___l0, method) ((  void (*) (Enumerator_t435122171 *, List_1_t415449401 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<Pathfinding.Int2>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3070307544(__this, method) ((  void (*) (Enumerator_t435122171 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<Pathfinding.Int2>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2065707972(__this, method) ((  Il2CppObject * (*) (Enumerator_t435122171 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<Pathfinding.Int2>>::Dispose()
#define Enumerator_Dispose_m2583021471(__this, method) ((  void (*) (Enumerator_t435122171 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<Pathfinding.Int2>>::VerifyState()
#define Enumerator_VerifyState_m4256079704(__this, method) ((  void (*) (Enumerator_t435122171 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<Pathfinding.Int2>>::MoveNext()
#define Enumerator_MoveNext_m1811837508(__this, method) ((  bool (*) (Enumerator_t435122171 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.List`1<Pathfinding.Int2>>::get_Current()
#define Enumerator_get_Current_m1164328975(__this, method) ((  List_1_t3342231145 * (*) (Enumerator_t435122171 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
