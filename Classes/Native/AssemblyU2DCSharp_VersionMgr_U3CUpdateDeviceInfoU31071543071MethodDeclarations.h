﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VersionMgr/<UpdateDeviceInfo>c__AnonStorey15F
struct U3CUpdateDeviceInfoU3Ec__AnonStorey15F_t1071543071;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void VersionMgr/<UpdateDeviceInfo>c__AnonStorey15F::.ctor()
extern "C"  void U3CUpdateDeviceInfoU3Ec__AnonStorey15F__ctor_m1232363820 (U3CUpdateDeviceInfoU3Ec__AnonStorey15F_t1071543071 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VersionMgr/<UpdateDeviceInfo>c__AnonStorey15F::<>m__3FA(System.Boolean,System.String)
extern "C"  void U3CUpdateDeviceInfoU3Ec__AnonStorey15F_U3CU3Em__3FA_m1011250990 (U3CUpdateDeviceInfoU3Ec__AnonStorey15F_t1071543071 * __this, bool ___ret0, String_t* ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
