﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIDraggableCameraGenerated
struct UIDraggableCameraGenerated_t3488526673;
// JSVCall
struct JSVCall_t3708497963;
// UIDraggableCamera
struct UIDraggableCamera_t1776763358;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_UIDraggableCamera1776763358.h"

// System.Void UIDraggableCameraGenerated::.ctor()
extern "C"  void UIDraggableCameraGenerated__ctor_m903717482 (UIDraggableCameraGenerated_t3488526673 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDraggableCameraGenerated::UIDraggableCamera_UIDraggableCamera1(JSVCall,System.Int32)
extern "C"  bool UIDraggableCameraGenerated_UIDraggableCamera_UIDraggableCamera1_m2723475922 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDraggableCameraGenerated::UIDraggableCamera_rootForBounds(JSVCall)
extern "C"  void UIDraggableCameraGenerated_UIDraggableCamera_rootForBounds_m3293152420 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDraggableCameraGenerated::UIDraggableCamera_scale(JSVCall)
extern "C"  void UIDraggableCameraGenerated_UIDraggableCamera_scale_m2597288758 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDraggableCameraGenerated::UIDraggableCamera_scrollWheelFactor(JSVCall)
extern "C"  void UIDraggableCameraGenerated_UIDraggableCamera_scrollWheelFactor_m295446371 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDraggableCameraGenerated::UIDraggableCamera_dragEffect(JSVCall)
extern "C"  void UIDraggableCameraGenerated_UIDraggableCamera_dragEffect_m3763430343 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDraggableCameraGenerated::UIDraggableCamera_smoothDragStart(JSVCall)
extern "C"  void UIDraggableCameraGenerated_UIDraggableCamera_smoothDragStart_m2859731776 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDraggableCameraGenerated::UIDraggableCamera_momentumAmount(JSVCall)
extern "C"  void UIDraggableCameraGenerated_UIDraggableCamera_momentumAmount_m2005807612 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDraggableCameraGenerated::UIDraggableCamera_currentMomentum(JSVCall)
extern "C"  void UIDraggableCameraGenerated_UIDraggableCamera_currentMomentum_m3323195055 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDraggableCameraGenerated::UIDraggableCamera_ConstrainToBounds__Boolean(JSVCall,System.Int32)
extern "C"  bool UIDraggableCameraGenerated_UIDraggableCamera_ConstrainToBounds__Boolean_m2012456896 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDraggableCameraGenerated::UIDraggableCamera_Drag__Vector2(JSVCall,System.Int32)
extern "C"  bool UIDraggableCameraGenerated_UIDraggableCamera_Drag__Vector2_m3445158814 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDraggableCameraGenerated::UIDraggableCamera_Press__Boolean(JSVCall,System.Int32)
extern "C"  bool UIDraggableCameraGenerated_UIDraggableCamera_Press__Boolean_m2489259172 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDraggableCameraGenerated::UIDraggableCamera_Scroll__Single(JSVCall,System.Int32)
extern "C"  bool UIDraggableCameraGenerated_UIDraggableCamera_Scroll__Single_m2815927156 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDraggableCameraGenerated::__Register()
extern "C"  void UIDraggableCameraGenerated___Register_m2010827933 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDraggableCameraGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UIDraggableCameraGenerated_ilo_attachFinalizerObject1_m2270656949 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDraggableCameraGenerated::ilo_setVector2S2(System.Int32,UnityEngine.Vector2)
extern "C"  void UIDraggableCameraGenerated_ilo_setVector2S2_m4022968766 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDraggableCameraGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UIDraggableCameraGenerated_ilo_setSingle3_m203642572 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIDraggableCameraGenerated::ilo_getEnum4(System.Int32)
extern "C"  int32_t UIDraggableCameraGenerated_ilo_getEnum4_m524368681 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDraggableCameraGenerated::ilo_getBooleanS5(System.Int32)
extern "C"  bool UIDraggableCameraGenerated_ilo_getBooleanS5_m3322599206 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDraggableCameraGenerated::ilo_ConstrainToBounds6(UIDraggableCamera,System.Boolean)
extern "C"  bool UIDraggableCameraGenerated_ilo_ConstrainToBounds6_m4251824217 (Il2CppObject * __this /* static, unused */, UIDraggableCamera_t1776763358 * ____this0, bool ___immediate1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDraggableCameraGenerated::ilo_setBooleanS7(System.Int32,System.Boolean)
extern "C"  void UIDraggableCameraGenerated_ilo_setBooleanS7_m324397845 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UIDraggableCameraGenerated::ilo_getVector2S8(System.Int32)
extern "C"  Vector2_t4282066565  UIDraggableCameraGenerated_ilo_getVector2S8_m2355616503 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDraggableCameraGenerated::ilo_Press9(UIDraggableCamera,System.Boolean)
extern "C"  void UIDraggableCameraGenerated_ilo_Press9_m2487184076 (Il2CppObject * __this /* static, unused */, UIDraggableCamera_t1776763358 * ____this0, bool ___isPressed1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIDraggableCameraGenerated::ilo_getSingle10(System.Int32)
extern "C"  float UIDraggableCameraGenerated_ilo_getSingle10_m1804383543 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
