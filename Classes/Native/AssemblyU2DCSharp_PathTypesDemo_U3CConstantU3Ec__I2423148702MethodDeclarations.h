﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PathTypesDemo/<Constant>c__Iterator14
struct U3CConstantU3Ec__Iterator14_t2423148702;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PathTypesDemo/<Constant>c__Iterator14::.ctor()
extern "C"  void U3CConstantU3Ec__Iterator14__ctor_m4202146445 (U3CConstantU3Ec__Iterator14_t2423148702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PathTypesDemo/<Constant>c__Iterator14::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CConstantU3Ec__Iterator14_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m738970277 (U3CConstantU3Ec__Iterator14_t2423148702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PathTypesDemo/<Constant>c__Iterator14::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CConstantU3Ec__Iterator14_System_Collections_IEnumerator_get_Current_m3920214073 (U3CConstantU3Ec__Iterator14_t2423148702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PathTypesDemo/<Constant>c__Iterator14::MoveNext()
extern "C"  bool U3CConstantU3Ec__Iterator14_MoveNext_m2362016583 (U3CConstantU3Ec__Iterator14_t2423148702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathTypesDemo/<Constant>c__Iterator14::Dispose()
extern "C"  void U3CConstantU3Ec__Iterator14_Dispose_m891758474 (U3CConstantU3Ec__Iterator14_t2423148702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PathTypesDemo/<Constant>c__Iterator14::Reset()
extern "C"  void U3CConstantU3Ec__Iterator14_Reset_m1848579386 (U3CConstantU3Ec__Iterator14_t2423148702 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
