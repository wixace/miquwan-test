﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Events.UnityEvent
struct UnityEvent_t1266085011;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.RpsTrigger
struct  RpsTrigger_t2657866263  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Events.UnityEvent Core.RpsTrigger::Event
	UnityEvent_t1266085011 * ___Event_2;
	// System.Boolean Core.RpsTrigger::_canInvoke
	bool ____canInvoke_3;

public:
	inline static int32_t get_offset_of_Event_2() { return static_cast<int32_t>(offsetof(RpsTrigger_t2657866263, ___Event_2)); }
	inline UnityEvent_t1266085011 * get_Event_2() const { return ___Event_2; }
	inline UnityEvent_t1266085011 ** get_address_of_Event_2() { return &___Event_2; }
	inline void set_Event_2(UnityEvent_t1266085011 * value)
	{
		___Event_2 = value;
		Il2CppCodeGenWriteBarrier(&___Event_2, value);
	}

	inline static int32_t get_offset_of__canInvoke_3() { return static_cast<int32_t>(offsetof(RpsTrigger_t2657866263, ____canInvoke_3)); }
	inline bool get__canInvoke_3() const { return ____canInvoke_3; }
	inline bool* get_address_of__canInvoke_3() { return &____canInvoke_3; }
	inline void set__canInvoke_3(bool value)
	{
		____canInvoke_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
