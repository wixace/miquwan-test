﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_TrackedReferenceGenerated
struct UnityEngine_TrackedReferenceGenerated_t3581032166;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_TrackedReferenceGenerated::.ctor()
extern "C"  void UnityEngine_TrackedReferenceGenerated__ctor_m1495160133 (UnityEngine_TrackedReferenceGenerated_t3581032166 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TrackedReferenceGenerated::TrackedReference_Equals__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TrackedReferenceGenerated_TrackedReference_Equals__Object_m2100960131 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TrackedReferenceGenerated::TrackedReference_GetHashCode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TrackedReferenceGenerated_TrackedReference_GetHashCode_m115483502 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TrackedReferenceGenerated::TrackedReference_op_Equality__TrackedReference__TrackedReference(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TrackedReferenceGenerated_TrackedReference_op_Equality__TrackedReference__TrackedReference_m1505418551 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TrackedReferenceGenerated::TrackedReference_op_Implicit__TrackedReference_to_Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TrackedReferenceGenerated_TrackedReference_op_Implicit__TrackedReference_to_Boolean_m2638399972 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TrackedReferenceGenerated::TrackedReference_op_Inequality__TrackedReference__TrackedReference(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TrackedReferenceGenerated_TrackedReference_op_Inequality__TrackedReference__TrackedReference_m1510996636 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TrackedReferenceGenerated::__Register()
extern "C"  void UnityEngine_TrackedReferenceGenerated___Register_m2431560354 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_TrackedReferenceGenerated::ilo_getObject1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_TrackedReferenceGenerated_ilo_getObject1_m2934318784 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
