﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUILayoutGenerated/<GUILayout_Window_GetDelegate_member103_arg2>c__AnonStoreyF5
struct U3CGUILayout_Window_GetDelegate_member103_arg2U3Ec__AnonStoreyF5_t2451639283;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_GUILayoutGenerated/<GUILayout_Window_GetDelegate_member103_arg2>c__AnonStoreyF5::.ctor()
extern "C"  void U3CGUILayout_Window_GetDelegate_member103_arg2U3Ec__AnonStoreyF5__ctor_m1090461912 (U3CGUILayout_Window_GetDelegate_member103_arg2U3Ec__AnonStoreyF5_t2451639283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUILayoutGenerated/<GUILayout_Window_GetDelegate_member103_arg2>c__AnonStoreyF5::<>m__23C(System.Int32)
extern "C"  void U3CGUILayout_Window_GetDelegate_member103_arg2U3Ec__AnonStoreyF5_U3CU3Em__23C_m556888788 (U3CGUILayout_Window_GetDelegate_member103_arg2U3Ec__AnonStoreyF5_t2451639283 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
