﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_zalvi38
struct M_zalvi38_t1995763005;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_zalvi381995763005.h"

// System.Void GarbageiOS.M_zalvi38::.ctor()
extern "C"  void M_zalvi38__ctor_m4218875590 (M_zalvi38_t1995763005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zalvi38::M_wurnorgair0(System.String[],System.Int32)
extern "C"  void M_zalvi38_M_wurnorgair0_m1633783545 (M_zalvi38_t1995763005 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zalvi38::M_salma1(System.String[],System.Int32)
extern "C"  void M_zalvi38_M_salma1_m1398409798 (M_zalvi38_t1995763005 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zalvi38::M_jerleati2(System.String[],System.Int32)
extern "C"  void M_zalvi38_M_jerleati2_m708779701 (M_zalvi38_t1995763005 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zalvi38::M_kurma3(System.String[],System.Int32)
extern "C"  void M_zalvi38_M_kurma3_m1390373790 (M_zalvi38_t1995763005 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zalvi38::M_palwulirRaldregall4(System.String[],System.Int32)
extern "C"  void M_zalvi38_M_palwulirRaldregall4_m2432011117 (M_zalvi38_t1995763005 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zalvi38::ilo_M_wurnorgair01(GarbageiOS.M_zalvi38,System.String[],System.Int32)
extern "C"  void M_zalvi38_ilo_M_wurnorgair01_m3882697838 (Il2CppObject * __this /* static, unused */, M_zalvi38_t1995763005 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zalvi38::ilo_M_salma12(GarbageiOS.M_zalvi38,System.String[],System.Int32)
extern "C"  void M_zalvi38_ilo_M_salma12_m2604051680 (Il2CppObject * __this /* static, unused */, M_zalvi38_t1995763005 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_zalvi38::ilo_M_jerleati23(GarbageiOS.M_zalvi38,System.String[],System.Int32)
extern "C"  void M_zalvi38_ilo_M_jerleati23_m25631528 (Il2CppObject * __this /* static, unused */, M_zalvi38_t1995763005 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
