﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIDrawCallGenerated/<UIDrawCall_onRender_GetDelegate_member13_arg0>c__AnonStoreyB4
struct U3CUIDrawCall_onRender_GetDelegate_member13_arg0U3Ec__AnonStoreyB4_t1002407853;
// UnityEngine.Material
struct Material_t3870600107;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"

// System.Void UIDrawCallGenerated/<UIDrawCall_onRender_GetDelegate_member13_arg0>c__AnonStoreyB4::.ctor()
extern "C"  void U3CUIDrawCall_onRender_GetDelegate_member13_arg0U3Ec__AnonStoreyB4__ctor_m3854764942 (U3CUIDrawCall_onRender_GetDelegate_member13_arg0U3Ec__AnonStoreyB4_t1002407853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDrawCallGenerated/<UIDrawCall_onRender_GetDelegate_member13_arg0>c__AnonStoreyB4::<>m__12C(UnityEngine.Material)
extern "C"  void U3CUIDrawCall_onRender_GetDelegate_member13_arg0U3Ec__AnonStoreyB4_U3CU3Em__12C_m3480513039 (U3CUIDrawCall_onRender_GetDelegate_member13_arg0U3Ec__AnonStoreyB4_t1002407853 * __this, Material_t3870600107 * ___mat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
