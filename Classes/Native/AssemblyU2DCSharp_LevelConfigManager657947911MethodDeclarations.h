﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LevelConfigManager
struct LevelConfigManager_t657947911;
// JSCLevelConfig
struct JSCLevelConfig_t1411099500;
// System.Collections.Generic.List`1<JSCMapPathPointInfoConfig>
struct List_1_t4071866601;
// JSCLevelMonsterConfig
struct JSCLevelMonsterConfig_t1924079698;
// JSCLevelHeroConfig
struct JSCLevelHeroConfig_t1953226502;
// System.Object
struct Il2CppObject;
// JSCMapPathConfig
struct JSCMapPathConfig_t3426118729;
// System.Collections.Generic.List`1<JSCLevelMonsterConfig>
struct List_1_t3292265250;
// JSCWaveNpcConfig
struct JSCWaveNpcConfig_t4221504656;
// System.Collections.Generic.List`1<JSCWaveNpcConfig>
struct List_1_t1294722912;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSCMapPathConfig3426118729.h"
#include "AssemblyU2DCSharp_JSCLevelConfig1411099500.h"
#include "AssemblyU2DCSharp_JSCWaveNpcConfig4221504656.h"
#include "AssemblyU2DCSharp_LevelConfigManager657947911.h"

// System.Void LevelConfigManager::.ctor()
extern "C"  void LevelConfigManager__ctor_m1762474868 (LevelConfigManager_t657947911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelConfigManager::.cctor()
extern "C"  void LevelConfigManager__cctor_m2615017145 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LevelConfigManager::get_inited()
extern "C"  bool LevelConfigManager_get_inited_m519964508 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelConfigManager::set_inited(System.Boolean)
extern "C"  void LevelConfigManager_set_inited_m2255633811 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LevelConfigManager LevelConfigManager::get_instance()
extern "C"  LevelConfigManager_t657947911 * LevelConfigManager_get_instance_m744241380 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelConfigManager::InitLevelConfig()
extern "C"  void LevelConfigManager_InitLevelConfig_m796957320 (LevelConfigManager_t657947911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSCLevelConfig LevelConfigManager::GetLevelConfigByID(System.Int32)
extern "C"  JSCLevelConfig_t1411099500 * LevelConfigManager_GetLevelConfigByID_m2031576564 (LevelConfigManager_t657947911 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<JSCMapPathPointInfoConfig> LevelConfigManager::GetMapathPointInfos(System.Int32)
extern "C"  List_1_t4071866601 * LevelConfigManager_GetMapathPointInfos_m3836106157 (LevelConfigManager_t657947911 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSCLevelMonsterConfig LevelConfigManager::GetMonsterConfig(System.Int32,System.Int32,System.Int32)
extern "C"  JSCLevelMonsterConfig_t1924079698 * LevelConfigManager_GetMonsterConfig_m2838571144 (LevelConfigManager_t657947911 * __this, int32_t ___level0, int32_t ___round1, int32_t ___index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSCLevelHeroConfig LevelConfigManager::GetHeroConfig(System.Int32)
extern "C"  JSCLevelHeroConfig_t1953226502 * LevelConfigManager_GetHeroConfig_m2421383454 (LevelConfigManager_t657947911 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSCLevelHeroConfig LevelConfigManager::GetGuideHeroConfig(System.Int32)
extern "C"  JSCLevelHeroConfig_t1953226502 * LevelConfigManager_GetGuideHeroConfig_m3464824666 (LevelConfigManager_t657947911 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LevelConfigManager::GetTotalMonsters(System.Int32,System.Int32)
extern "C"  int32_t LevelConfigManager_GetTotalMonsters_m84333729 (LevelConfigManager_t657947911 * __this, int32_t ___level0, int32_t ___round1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LevelConfigManager::GetTotalRounds(System.Int32)
extern "C"  int32_t LevelConfigManager_GetTotalRounds_m2839160450 (LevelConfigManager_t657947911 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LevelConfigManager::GetTotalOtherRounds(System.Int32)
extern "C"  int32_t LevelConfigManager_GetTotalOtherRounds_m840516860 (LevelConfigManager_t657947911 * __this, int32_t ___level0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelConfigManager::ilo_Debug1(System.Object,System.Boolean,System.Int32)
extern "C"  void LevelConfigManager_ilo_Debug1_m3970730285 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, int32_t ___user2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LevelConfigManager::ilo_get_id2(JSCMapPathConfig)
extern "C"  int32_t LevelConfigManager_ilo_get_id2_m3648447030 (Il2CppObject * __this /* static, unused */, JSCMapPathConfig_t3426118729 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LevelConfigManager::ilo_set_inited3(System.Boolean)
extern "C"  void LevelConfigManager_ilo_set_inited3_m1054685917 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LevelConfigManager::ilo_get_mapPathId4(JSCLevelConfig)
extern "C"  int32_t LevelConfigManager_ilo_get_mapPathId4_m4248814310 (Il2CppObject * __this /* static, unused */, JSCLevelConfig_t1411099500 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<JSCMapPathPointInfoConfig> LevelConfigManager::ilo_get_pathPoints5(JSCMapPathConfig)
extern "C"  List_1_t4071866601 * LevelConfigManager_ilo_get_pathPoints5_m312005632 (Il2CppObject * __this /* static, unused */, JSCMapPathConfig_t3426118729 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<JSCLevelMonsterConfig> LevelConfigManager::ilo_get_wave6(JSCWaveNpcConfig)
extern "C"  List_1_t3292265250 * LevelConfigManager_ilo_get_wave6_m284987728 (Il2CppObject * __this /* static, unused */, JSCWaveNpcConfig_t4221504656 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSCLevelConfig LevelConfigManager::ilo_GetLevelConfigByID7(LevelConfigManager,System.Int32)
extern "C"  JSCLevelConfig_t1411099500 * LevelConfigManager_ilo_GetLevelConfigByID7_m3159394747 (Il2CppObject * __this /* static, unused */, LevelConfigManager_t657947911 * ____this0, int32_t ___level1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<JSCWaveNpcConfig> LevelConfigManager::ilo_get_monsters8(JSCLevelConfig)
extern "C"  List_1_t1294722912 * LevelConfigManager_ilo_get_monsters8_m1020180102 (Il2CppObject * __this /* static, unused */, JSCLevelConfig_t1411099500 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
