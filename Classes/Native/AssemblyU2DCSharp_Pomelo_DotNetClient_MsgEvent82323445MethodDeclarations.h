﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pomelo.DotNetClient.MsgEvent
struct MsgEvent_t82323445;

#include "codegen/il2cpp-codegen.h"

// System.Void Pomelo.DotNetClient.MsgEvent::.ctor()
extern "C"  void MsgEvent__ctor_m1892642855 (MsgEvent_t82323445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.MsgEvent::Dispose()
extern "C"  void MsgEvent_Dispose_m1956900516 (MsgEvent_t82323445 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
