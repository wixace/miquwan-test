﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_BoxColliderGenerated
struct UnityEngine_BoxColliderGenerated_t2070989528;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_BoxColliderGenerated::.ctor()
extern "C"  void UnityEngine_BoxColliderGenerated__ctor_m2937052867 (UnityEngine_BoxColliderGenerated_t2070989528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoxColliderGenerated::BoxCollider_BoxCollider1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoxColliderGenerated_BoxCollider_BoxCollider1_m3913077271 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoxColliderGenerated::BoxCollider_center(JSVCall)
extern "C"  void UnityEngine_BoxColliderGenerated_BoxCollider_center_m441955921 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoxColliderGenerated::BoxCollider_size(JSVCall)
extern "C"  void UnityEngine_BoxColliderGenerated_BoxCollider_size_m4240794661 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoxColliderGenerated::__Register()
extern "C"  void UnityEngine_BoxColliderGenerated___Register_m3901579236 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoxColliderGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_BoxColliderGenerated_ilo_attachFinalizerObject1_m119182716 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoxColliderGenerated::ilo_setVector3S2(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_BoxColliderGenerated_ilo_setVector3S2_m2883738469 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_BoxColliderGenerated::ilo_getVector3S3(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_BoxColliderGenerated_ilo_getVector3S3_m1512127769 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
