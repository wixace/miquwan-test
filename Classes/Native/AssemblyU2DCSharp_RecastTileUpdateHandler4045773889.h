﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.RecastGraph
struct RecastGraph_t2197443166;
// System.Boolean[]
struct BooleanU5BU5D_t3456302923;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RecastTileUpdateHandler
struct  RecastTileUpdateHandler_t4045773889  : public MonoBehaviour_t667441552
{
public:
	// Pathfinding.RecastGraph RecastTileUpdateHandler::graph
	RecastGraph_t2197443166 * ___graph_2;
	// System.Boolean[] RecastTileUpdateHandler::dirtyTiles
	BooleanU5BU5D_t3456302923* ___dirtyTiles_3;
	// System.Boolean RecastTileUpdateHandler::anyDirtyTiles
	bool ___anyDirtyTiles_4;
	// System.Single RecastTileUpdateHandler::earliestDirty
	float ___earliestDirty_5;
	// System.Single RecastTileUpdateHandler::maxThrottlingDelay
	float ___maxThrottlingDelay_6;

public:
	inline static int32_t get_offset_of_graph_2() { return static_cast<int32_t>(offsetof(RecastTileUpdateHandler_t4045773889, ___graph_2)); }
	inline RecastGraph_t2197443166 * get_graph_2() const { return ___graph_2; }
	inline RecastGraph_t2197443166 ** get_address_of_graph_2() { return &___graph_2; }
	inline void set_graph_2(RecastGraph_t2197443166 * value)
	{
		___graph_2 = value;
		Il2CppCodeGenWriteBarrier(&___graph_2, value);
	}

	inline static int32_t get_offset_of_dirtyTiles_3() { return static_cast<int32_t>(offsetof(RecastTileUpdateHandler_t4045773889, ___dirtyTiles_3)); }
	inline BooleanU5BU5D_t3456302923* get_dirtyTiles_3() const { return ___dirtyTiles_3; }
	inline BooleanU5BU5D_t3456302923** get_address_of_dirtyTiles_3() { return &___dirtyTiles_3; }
	inline void set_dirtyTiles_3(BooleanU5BU5D_t3456302923* value)
	{
		___dirtyTiles_3 = value;
		Il2CppCodeGenWriteBarrier(&___dirtyTiles_3, value);
	}

	inline static int32_t get_offset_of_anyDirtyTiles_4() { return static_cast<int32_t>(offsetof(RecastTileUpdateHandler_t4045773889, ___anyDirtyTiles_4)); }
	inline bool get_anyDirtyTiles_4() const { return ___anyDirtyTiles_4; }
	inline bool* get_address_of_anyDirtyTiles_4() { return &___anyDirtyTiles_4; }
	inline void set_anyDirtyTiles_4(bool value)
	{
		___anyDirtyTiles_4 = value;
	}

	inline static int32_t get_offset_of_earliestDirty_5() { return static_cast<int32_t>(offsetof(RecastTileUpdateHandler_t4045773889, ___earliestDirty_5)); }
	inline float get_earliestDirty_5() const { return ___earliestDirty_5; }
	inline float* get_address_of_earliestDirty_5() { return &___earliestDirty_5; }
	inline void set_earliestDirty_5(float value)
	{
		___earliestDirty_5 = value;
	}

	inline static int32_t get_offset_of_maxThrottlingDelay_6() { return static_cast<int32_t>(offsetof(RecastTileUpdateHandler_t4045773889, ___maxThrottlingDelay_6)); }
	inline float get_maxThrottlingDelay_6() const { return ___maxThrottlingDelay_6; }
	inline float* get_address_of_maxThrottlingDelay_6() { return &___maxThrottlingDelay_6; }
	inline void set_maxThrottlingDelay_6(float value)
	{
		___maxThrottlingDelay_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
