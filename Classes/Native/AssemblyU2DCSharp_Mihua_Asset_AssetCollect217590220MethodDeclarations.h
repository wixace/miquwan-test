﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// checkpointCfg
struct checkpointCfg_t2816107964;
// LevelConfigManager
struct LevelConfigManager_t657947911;
// JSCLevelConfig
struct JSCLevelConfig_t1411099500;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// System.Collections.Generic.List`1<JSCLevelMonsterConfig>
struct List_1_t3292265250;
// JSCWaveNpcConfig
struct JSCWaveNpcConfig_t4221504656;
// Newtonsoft.Json.Linq.JArray
struct JArray_t3394795039;
// JSCLevelHeroConfig
struct JSCLevelHeroConfig_t1953226502;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// JSCLevelMonsterConfig
struct JSCLevelMonsterConfig_t1924079698;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_checkpointCfg2816107964.h"
#include "AssemblyU2DCSharp_LevelConfigManager657947911.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "AssemblyU2DCSharp_JSCWaveNpcConfig4221504656.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JArray3394795039.h"
#include "AssemblyU2DCSharp_JSCLevelConfig1411099500.h"
#include "AssemblyU2DCSharp_JSCLevelHeroConfig1953226502.h"
#include "AssemblyU2DCSharp_JSCLevelMonsterConfig1924079698.h"

// System.Void Mihua.Asset.AssetCollect::.cctor()
extern "C"  void AssetCollect__cctor_m1012584832 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetCollect::AddUIAssets(System.String)
extern "C"  void AssetCollect_AddUIAssets_m2599503263 (Il2CppObject * __this /* static, unused */, String_t* ___assetPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetCollect::AddAssets(System.String)
extern "C"  void AssetCollect_AddAssets_m2488446195 (Il2CppObject * __this /* static, unused */, String_t* ___assetPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> Mihua.Asset.AssetCollect::AllAssets(checkpointCfg)
extern "C"  List_1_t1375417109 * AssetCollect_AllAssets_m4168659992 (Il2CppObject * __this /* static, unused */, checkpointCfg_t2816107964 * ___checkPoint0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetCollect::AnalysisAseets(System.String,System.Collections.Generic.List`1<System.String>)
extern "C"  void AssetCollect_AnalysisAseets_m4144127068 (Il2CppObject * __this /* static, unused */, String_t* ___arr0, List_1_t1375417109 * ___list1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetCollect::AddAssetsByJArray(System.String)
extern "C"  void AssetCollect_AddAssetsByJArray_m2893516493 (Il2CppObject * __this /* static, unused */, String_t* ___arr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua.Asset.AssetCollect::NpcAseetsCs(System.Int32)
extern "C"  String_t* AssetCollect_NpcAseetsCs_m779974747 (Il2CppObject * __this /* static, unused */, int32_t ___mapID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetCollect::NextAssets(System.Collections.Generic.List`1<System.String>)
extern "C"  void AssetCollect_NextAssets_m1321640033 (Il2CppObject * __this /* static, unused */, List_1_t1375417109 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetCollect::UIAssets(System.Collections.Generic.List`1<System.String>)
extern "C"  void AssetCollect_UIAssets_m2701216354 (Il2CppObject * __this /* static, unused */, List_1_t1375417109 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetCollect::SkillAssets(System.Collections.Generic.List`1<System.String>)
extern "C"  void AssetCollect_SkillAssets_m471589843 (Il2CppObject * __this /* static, unused */, List_1_t1375417109 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetCollect::ilo_AnalysisAseets1(System.String,System.Collections.Generic.List`1<System.String>)
extern "C"  void AssetCollect_ilo_AnalysisAseets1_m663084820 (Il2CppObject * __this /* static, unused */, String_t* ___arr0, List_1_t1375417109 * ___list1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetCollect::ilo_NextAssets2(System.Collections.Generic.List`1<System.String>)
extern "C"  void AssetCollect_ilo_NextAssets2_m3539467496 (Il2CppObject * __this /* static, unused */, List_1_t1375417109 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LevelConfigManager Mihua.Asset.AssetCollect::ilo_get_instance3()
extern "C"  LevelConfigManager_t657947911 * AssetCollect_ilo_get_instance3_m698351671 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSCLevelConfig Mihua.Asset.AssetCollect::ilo_GetLevelConfigByID4(LevelConfigManager,System.Int32)
extern "C"  JSCLevelConfig_t1411099500 * AssetCollect_ilo_GetLevelConfigByID4_m3058079077 (Il2CppObject * __this /* static, unused */, LevelConfigManager_t657947911 * ____this0, int32_t ___level1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetCollect::ilo_Add5(Newtonsoft.Json.Linq.JObject,System.String,Newtonsoft.Json.Linq.JToken)
extern "C"  void AssetCollect_ilo_Add5_m3533299790 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, JToken_t3412245951 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<JSCLevelMonsterConfig> Mihua.Asset.AssetCollect::ilo_get_wave6(JSCWaveNpcConfig)
extern "C"  List_1_t3292265250 * AssetCollect_ilo_get_wave6_m3702149353 (Il2CppObject * __this /* static, unused */, JSCWaveNpcConfig_t4221504656 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Asset.AssetCollect::ilo_Add7(Newtonsoft.Json.Linq.JArray,Newtonsoft.Json.Linq.JToken)
extern "C"  void AssetCollect_ilo_Add7_m3606226350 (Il2CppObject * __this /* static, unused */, JArray_t3394795039 * ____this0, JToken_t3412245951 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSCLevelHeroConfig Mihua.Asset.AssetCollect::ilo_get_guideHero8(JSCLevelConfig)
extern "C"  JSCLevelHeroConfig_t1953226502 * AssetCollect_ilo_get_guideHero8_m2950252000 (Il2CppObject * __this /* static, unused */, JSCLevelConfig_t1411099500 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> Mihua.Asset.AssetCollect::ilo_get_idList9(JSCLevelHeroConfig)
extern "C"  List_1_t2522024052 * AssetCollect_ilo_get_idList9_m2070398055 (Il2CppObject * __this /* static, unused */, JSCLevelHeroConfig_t1953226502 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mihua.Asset.AssetCollect::ilo_get_id10(JSCLevelMonsterConfig)
extern "C"  int32_t AssetCollect_ilo_get_id10_m2007582277 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
