﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va142873305MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m3717022214(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3803011808 *, Dictionary_2_t807438799 *, const MethodInfo*))ValueCollection__ctor_m203935594_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m4284326252(__this, ___item0, method) ((  void (*) (ValueCollection_t3803011808 *, OnMoveCallBackFun_t3535987578 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2924074888_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2522162229(__this, method) ((  void (*) (ValueCollection_t3803011808 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1579731025_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3676867514(__this, ___item0, method) ((  bool (*) (ValueCollection_t3803011808 *, OnMoveCallBackFun_t3535987578 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1877017374_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m272976607(__this, ___item0, method) ((  bool (*) (ValueCollection_t3803011808 *, OnMoveCallBackFun_t3535987578 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1723616067_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1190594819(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3803011808 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m663527199_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3100100345(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3803011808 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3764516885_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m752554164(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3803011808 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2066971856_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1952043373(__this, method) ((  bool (*) (ValueCollection_t3803011808 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m152193233_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m560272077(__this, method) ((  bool (*) (ValueCollection_t3803011808 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2158657585_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3153945657(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3803011808 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2764646045_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m2161587405(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3803011808 *, OnMoveCallBackFunU5BU5D_t500157791*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m487491889_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3607399600(__this, method) ((  Enumerator_t3034239503  (*) (ValueCollection_t3803011808 *, const MethodInfo*))ValueCollection_GetEnumerator_m119970324_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::get_Count()
#define ValueCollection_get_Count_m2857113491(__this, method) ((  int32_t (*) (ValueCollection_t3803011808 *, const MethodInfo*))ValueCollection_get_Count_m1354876151_gshared)(__this, method)
