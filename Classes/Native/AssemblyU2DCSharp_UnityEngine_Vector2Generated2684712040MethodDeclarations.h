﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Vector2Generated
struct UnityEngine_Vector2Generated_t2684712040;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine_Vector2Generated::.ctor()
extern "C"  void UnityEngine_Vector2Generated__ctor_m777241395 (UnityEngine_Vector2Generated_t2684712040 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector2Generated::.cctor()
extern "C"  void UnityEngine_Vector2Generated__cctor_m2137550554 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_Vector21(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_Vector21_m160993479 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_Vector22(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_Vector22_m1405757960 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector2Generated::Vector2_kEpsilon(JSVCall)
extern "C"  void UnityEngine_Vector2Generated_Vector2_kEpsilon_m1113385543 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector2Generated::Vector2_x(JSVCall)
extern "C"  void UnityEngine_Vector2Generated_Vector2_x_m3955070382 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector2Generated::Vector2_y(JSVCall)
extern "C"  void UnityEngine_Vector2Generated_Vector2_y_m3758556877 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector2Generated::Vector2_Item_Int32(JSVCall)
extern "C"  void UnityEngine_Vector2Generated_Vector2_Item_Int32_m4063911876 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector2Generated::Vector2_normalized(JSVCall)
extern "C"  void UnityEngine_Vector2Generated_Vector2_normalized_m3544744655 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector2Generated::Vector2_magnitude(JSVCall)
extern "C"  void UnityEngine_Vector2Generated_Vector2_magnitude_m3832409878 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector2Generated::Vector2_sqrMagnitude(JSVCall)
extern "C"  void UnityEngine_Vector2Generated_Vector2_sqrMagnitude_m2450175178 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector2Generated::Vector2_zero(JSVCall)
extern "C"  void UnityEngine_Vector2Generated_Vector2_zero_m937420350 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector2Generated::Vector2_one(JSVCall)
extern "C"  void UnityEngine_Vector2Generated_Vector2_one_m2600717184 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector2Generated::Vector2_up(JSVCall)
extern "C"  void UnityEngine_Vector2Generated_Vector2_up_m2851811243 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector2Generated::Vector2_down(JSVCall)
extern "C"  void UnityEngine_Vector2Generated_Vector2_down_m4189837700 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector2Generated::Vector2_left(JSVCall)
extern "C"  void UnityEngine_Vector2Generated_Vector2_left_m4151074751 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector2Generated::Vector2_right(JSVCall)
extern "C"  void UnityEngine_Vector2Generated_Vector2_right_m3056704650 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_Equals__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_Equals__Object_m2757096963 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_GetHashCode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_GetHashCode_m2086164718 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_Normalize(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_Normalize_m2316442730 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_Scale__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_Scale__Vector2_m4250907658 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_Set__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_Set__Single__Single_m850431375 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_SqrMagnitude(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_SqrMagnitude_m1274533665 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_ToString__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_ToString__String_m2901541442 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_ToString_m2935670001 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_Angle__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_Angle__Vector2__Vector2_m2652502736 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_ClampMagnitude__Vector2__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_ClampMagnitude__Vector2__Single_m1719680063 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_Distance__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_Distance__Vector2__Vector2_m3496520282 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_Dot__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_Dot__Vector2__Vector2_m4051381318 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_Lerp__Vector2__Vector2__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_Lerp__Vector2__Vector2__Single_m2310972196 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_LerpUnclamped__Vector2__Vector2__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_LerpUnclamped__Vector2__Vector2__Single_m4053579119 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_Max__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_Max__Vector2__Vector2_m3418346849 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_Min__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_Min__Vector2__Vector2_m2292699215 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_MoveTowards__Vector2__Vector2__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_MoveTowards__Vector2__Vector2__Single_m4194196240 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_op_Addition__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_op_Addition__Vector2__Vector2_m3180846551 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_op_Division__Vector2__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_op_Division__Vector2__Single_m4154787569 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_op_Equality__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_op_Equality__Vector2__Vector2_m811155925 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_op_Implicit__Vector3_to_Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_op_Implicit__Vector3_to_Vector2_m1510091098 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_op_Implicit__Vector2_to_Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_op_Implicit__Vector2_to_Vector3_m1729363580 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_op_Inequality__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_op_Inequality__Vector2__Vector2_m3561815674 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_op_Multiply__Vector2__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_op_Multiply__Vector2__Single_m2013981082 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_op_Multiply__Single__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_op_Multiply__Single__Vector2_m393452810 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_op_Subtraction__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_op_Subtraction__Vector2__Vector2_m1646933307 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_op_UnaryNegation__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_op_UnaryNegation__Vector2_m1673557206 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_Reflect__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_Reflect__Vector2__Vector2_m2105283514 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_Scale__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_Scale__Vector2__Vector2_m3182856263 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_SmoothDamp__Vector2__Vector2__Vector2__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_SmoothDamp__Vector2__Vector2__Vector2__Single__Single__Single_m3454524470 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_SmoothDamp__Vector2__Vector2__Vector2__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_SmoothDamp__Vector2__Vector2__Vector2__Single__Single_m786160366 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_SmoothDamp__Vector2__Vector2__Vector2__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_SmoothDamp__Vector2__Vector2__Vector2__Single_m2302022054 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::Vector2_SqrMagnitude__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_Vector2_SqrMagnitude__Vector2_m1462693648 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector2Generated::__Register()
extern "C"  void UnityEngine_Vector2Generated___Register_m1261879668 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Vector2Generated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_Vector2Generated_ilo_getObject1_m2175501503 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_ilo_attachFinalizerObject2_m407855949 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector2Generated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UnityEngine_Vector2Generated_ilo_addJSCSRel3_m562873713 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector2Generated::ilo_setSingle4(System.Int32,System.Single)
extern "C"  void UnityEngine_Vector2Generated_ilo_setSingle4_m1709989348 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_Vector2Generated::ilo_getSingle5(System.Int32)
extern "C"  float UnityEngine_Vector2Generated_ilo_getSingle5_m3239668440 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector2Generated::ilo_changeJSObj6(System.Int32,System.Object)
extern "C"  void UnityEngine_Vector2Generated_ilo_changeJSObj6_m149548187 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector2Generated::ilo_setVector2S7(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_Vector2Generated_ilo_setVector2S7_m614071322 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector2Generated::ilo_Vector2_GetHashCode8(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector2Generated_ilo_Vector2_GetHashCode8_m3141994367 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_Vector2Generated::ilo_getStringS9(System.Int32)
extern "C"  String_t* UnityEngine_Vector2Generated_ilo_getStringS9_m963414585 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector2Generated::ilo_setStringS10(System.Int32,System.String)
extern "C"  void UnityEngine_Vector2Generated_ilo_setStringS10_m1918443664 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_Vector2Generated::ilo_getVector2S11(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_Vector2Generated_ilo_getVector2S11_m404515118 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
