﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.UriDecorator
struct UriDecorator_t4260167625;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// ProtoBuf.Serializers.IProtoSerializer
struct IProtoSerializer_t3033312651;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"

// System.Void ProtoBuf.Serializers.UriDecorator::.ctor(ProtoBuf.Meta.TypeModel,ProtoBuf.Serializers.IProtoSerializer)
extern "C"  void UriDecorator__ctor_m3695658024 (UriDecorator_t4260167625 * __this, TypeModel_t2730011105 * ___model0, Il2CppObject * ___tail1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.UriDecorator::.cctor()
extern "C"  void UriDecorator__cctor_m1733743105 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.UriDecorator::get_ExpectedType()
extern "C"  Type_t * UriDecorator_get_ExpectedType_m4030586073 (UriDecorator_t4260167625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.UriDecorator::get_RequiresOldValue()
extern "C"  bool UriDecorator_get_RequiresOldValue_m1886873925 (UriDecorator_t4260167625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.UriDecorator::get_ReturnsValue()
extern "C"  bool UriDecorator_get_ReturnsValue_m335332955 (UriDecorator_t4260167625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.UriDecorator::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void UriDecorator_Write_m2422529443 (UriDecorator_t4260167625 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.UriDecorator::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * UriDecorator_Read_m19365507 (UriDecorator_t4260167625 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
