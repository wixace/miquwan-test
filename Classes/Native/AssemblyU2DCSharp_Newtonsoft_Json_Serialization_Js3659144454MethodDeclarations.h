﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader
struct JsonSerializerInternalReader_t3659144454;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Serialization.JsonContract
struct JsonContract_t1328848902;
// System.Type
struct Type_t;
// Newtonsoft.Json.Serialization.JsonSerializerProxy
struct JsonSerializerProxy_t3893567258;
// Newtonsoft.Json.Serialization.JsonFormatterConverter
struct JsonFormatterConverter_t4063403306;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Serialization.JsonProperty
struct JsonProperty_t902655177;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t2159686854;
// Newtonsoft.Json.Serialization.JsonArrayContract
struct JsonArrayContract_t145179369;
// System.String
struct String_t;
// System.Globalization.CultureInfo
struct CultureInfo_t1065375142;
// Newtonsoft.Json.Serialization.JsonDictionaryContract
struct JsonDictionaryContract_t989352188;
// Newtonsoft.Json.Utilities.IWrappedDictionary
struct IWrappedDictionary_t1790279202;
// Newtonsoft.Json.Utilities.IWrappedCollection
struct IWrappedCollection_t3896884266;
// Newtonsoft.Json.Serialization.JsonISerializableContract
struct JsonISerializableContract_t624170136;
// Newtonsoft.Json.Serialization.JObjectContract
struct JObjectContract_t2867848225;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t4136801618;
// System.Collections.Generic.IDictionary`2<Newtonsoft.Json.Serialization.JsonProperty,System.Object>
struct IDictionary_2_t1553015650;
// System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct Dictionary_2_t696655424;
// System.Reflection.ParameterInfo
struct ParameterInfo_t2235474049;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_t507353639;
// Newtonsoft.Json.Linq.JRaw
struct JRaw_t225059758;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// Newtonsoft.Json.Linq.JTokenWriter
struct JTokenWriter_t3702364242;
// Newtonsoft.Json.Serialization.IValueProvider
struct IValueProvider_t175677893;
// Newtonsoft.Json.Serialization.IReferenceResolver
struct IReferenceResolver_t425424564;
// Newtonsoft.Json.Serialization.JsonSerializerInternalBase
struct JsonSerializerInternalBase_t2068678036;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_t2137423328;
// System.Action`2<System.Object,System.Object>
struct Action_2_t4293064463;
// System.Func`1<System.Object>
struct Func_1_t1001010649;
// Newtonsoft.Json.Serialization.JsonPropertyCollection
struct JsonPropertyCollection_t717767559;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer251850770.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js1328848902.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso902655177.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonConverter2159686854.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Globalization_CultureInfo1065375142.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_DefaultValueHand1569448045.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso989352188.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso145179369.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso624170136.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_JO2867848225.h"
#include "mscorlib_System_Reflection_ConstructorInfo4136801618.h"
#include "mscorlib_System_Reflection_ParameterInfo2235474049.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2892329490.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken4173078175.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js3659144454.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenWrite3702364242.h"
#include "mscorlib_System_Nullable_1_gen2443451997.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2068678036.h"
#include "mscorlib_System_Nullable_1_gen140208118.h"
#include "mscorlib_System_Nullable_1_gen2838778904.h"
#include "mscorlib_System_Nullable_1_gen1653574568.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"

// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::.ctor(Newtonsoft.Json.JsonSerializer)
extern "C"  void JsonSerializerInternalReader__ctor_m3803103007 (JsonSerializerInternalReader_t3659144454 * __this, JsonSerializer_t251850770 * ___serializer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::Populate(Newtonsoft.Json.JsonReader,System.Object)
extern "C"  void JsonSerializerInternalReader_Populate_m3134576366 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, Il2CppObject * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetContractSafe(System.Type)
extern "C"  JsonContract_t1328848902 * JsonSerializerInternalReader_GetContractSafe_m2847704936 (JsonSerializerInternalReader_t3659144454 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetContractSafe(System.Type,System.Object)
extern "C"  JsonContract_t1328848902 * JsonSerializerInternalReader_GetContractSafe_m1151188022 (JsonSerializerInternalReader_t3659144454 * __this, Type_t * ___type0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::Deserialize(Newtonsoft.Json.JsonReader,System.Type)
extern "C"  Il2CppObject * JsonSerializerInternalReader_Deserialize_m2498181577 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonSerializerProxy Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetInternalSerializer()
extern "C"  JsonSerializerProxy_t3893567258 * JsonSerializerInternalReader_GetInternalSerializer_m484459679 (JsonSerializerInternalReader_t3659144454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonFormatterConverter Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetFormatterConverter()
extern "C"  JsonFormatterConverter_t4063403306 * JsonSerializerInternalReader_GetFormatterConverter_m2127903372 (JsonSerializerInternalReader_t3659144454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateJToken(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  JToken_t3412245951 * JsonSerializerInternalReader_CreateJToken_m3172053069 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, JsonContract_t1328848902 * ___contract1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateJObject(Newtonsoft.Json.JsonReader)
extern "C"  JToken_t3412245951 * JsonSerializerInternalReader_CreateJObject_m3341452782 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateValueProperty(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonProperty,System.Object,System.Boolean,System.Object)
extern "C"  Il2CppObject * JsonSerializerInternalReader_CreateValueProperty_m699751592 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, JsonProperty_t902655177 * ___property1, Il2CppObject * ___target2, bool ___gottenCurrentValue3, Il2CppObject * ___currentValue4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateValueNonProperty(Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  Il2CppObject * JsonSerializerInternalReader_CreateValueNonProperty_m3821777518 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, JsonContract_t1328848902 * ___contract2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateValueInternal(Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern "C"  Il2CppObject * JsonSerializerInternalReader_CreateValueInternal_m4181586183 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, JsonContract_t1328848902 * ___contract2, JsonProperty_t902655177 * ___member3, Il2CppObject * ___existingValue4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonSerializerInternalReader::GetConverter(Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.JsonConverter)
extern "C"  JsonConverter_t2159686854 * JsonSerializerInternalReader_GetConverter_m44426892 (JsonSerializerInternalReader_t3659144454 * __this, JsonContract_t1328848902 * ___contract0, JsonConverter_t2159686854 * ___memberConverter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateObject(Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern "C"  Il2CppObject * JsonSerializerInternalReader_CreateObject_m4292628100 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, JsonContract_t1328848902 * ___contract2, JsonProperty_t902655177 * ___member3, Il2CppObject * ___existingValue4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonArrayContract Newtonsoft.Json.Serialization.JsonSerializerInternalReader::EnsureArrayContract(System.Type,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  JsonArrayContract_t145179369 * JsonSerializerInternalReader_EnsureArrayContract_m507392118 (JsonSerializerInternalReader_t3659144454 * __this, Type_t * ___objectType0, JsonContract_t1328848902 * ___contract1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CheckedRead(Newtonsoft.Json.JsonReader)
extern "C"  void JsonSerializerInternalReader_CheckedRead_m2978886089 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateList(Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object,System.String)
extern "C"  Il2CppObject * JsonSerializerInternalReader_CreateList_m3376612831 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___objectType1, JsonContract_t1328848902 * ___contract2, JsonProperty_t902655177 * ___member3, Il2CppObject * ___existingValue4, String_t* ___reference5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::HasDefinedType(System.Type)
extern "C"  bool JsonSerializerInternalReader_HasDefinedType_m1072819558 (JsonSerializerInternalReader_t3659144454 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::EnsureType(System.Object,System.Globalization.CultureInfo,System.Type)
extern "C"  Il2CppObject * JsonSerializerInternalReader_EnsureType_m2085624120 (JsonSerializerInternalReader_t3659144454 * __this, Il2CppObject * ___value0, CultureInfo_t1065375142 * ___culture1, Type_t * ___targetType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonSerializerInternalReader::FormatValueForPrint(System.Object)
extern "C"  String_t* JsonSerializerInternalReader_FormatValueForPrint_m2626955181 (JsonSerializerInternalReader_t3659144454 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::SetPropertyValue(Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader,System.Object)
extern "C"  void JsonSerializerInternalReader_SetPropertyValue_m2999701588 (JsonSerializerInternalReader_t3659144454 * __this, JsonProperty_t902655177 * ___property0, JsonReader_t816925123 * ___reader1, Il2CppObject * ___target2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::HasFlag(Newtonsoft.Json.DefaultValueHandling,Newtonsoft.Json.DefaultValueHandling)
extern "C"  bool JsonSerializerInternalReader_HasFlag_m2754275272 (JsonSerializerInternalReader_t3659144454 * __this, int32_t ___value0, int32_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ShouldSetPropertyValue(Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern "C"  bool JsonSerializerInternalReader_ShouldSetPropertyValue_m987587829 (JsonSerializerInternalReader_t3659144454 * __this, JsonProperty_t902655177 * ___property0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateAndPopulateDictionary(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonDictionaryContract,System.String)
extern "C"  Il2CppObject * JsonSerializerInternalReader_CreateAndPopulateDictionary_m4034315917 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, JsonDictionaryContract_t989352188 * ___contract1, String_t* ___id2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::PopulateDictionary(Newtonsoft.Json.Utilities.IWrappedDictionary,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonDictionaryContract,System.String)
extern "C"  Il2CppObject * JsonSerializerInternalReader_PopulateDictionary_m564122905 (JsonSerializerInternalReader_t3659144454 * __this, Il2CppObject * ___dictionary0, JsonReader_t816925123 * ___reader1, JsonDictionaryContract_t989352188 * ___contract2, String_t* ___id3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateAndPopulateList(Newtonsoft.Json.JsonReader,System.String,Newtonsoft.Json.Serialization.JsonArrayContract)
extern "C"  Il2CppObject * JsonSerializerInternalReader_CreateAndPopulateList_m2200321362 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, String_t* ___reference1, JsonArrayContract_t145179369 * ___contract2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ReadForTypeArrayHack(Newtonsoft.Json.JsonReader,System.Type)
extern "C"  bool JsonSerializerInternalReader_ReadForTypeArrayHack_m3853733130 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::PopulateList(Newtonsoft.Json.Utilities.IWrappedCollection,Newtonsoft.Json.JsonReader,System.String,Newtonsoft.Json.Serialization.JsonArrayContract)
extern "C"  Il2CppObject * JsonSerializerInternalReader_PopulateList_m3527589118 (JsonSerializerInternalReader_t3659144454 * __this, Il2CppObject * ___wrappedList0, JsonReader_t816925123 * ___reader1, String_t* ___reference2, JsonArrayContract_t145179369 * ___contract3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateISerializable(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonISerializableContract,System.String)
extern "C"  Il2CppObject * JsonSerializerInternalReader_CreateISerializable_m1336685036 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, JsonISerializableContract_t624170136 * ___contract1, String_t* ___id2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateAndPopulateObject(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JObjectContract,System.String)
extern "C"  Il2CppObject * JsonSerializerInternalReader_CreateAndPopulateObject_m4139799611 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, JObjectContract_t2867848225 * ___contract1, String_t* ___id2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::CreateObjectFromNonDefaultConstructor(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JObjectContract,System.Reflection.ConstructorInfo,System.String)
extern "C"  Il2CppObject * JsonSerializerInternalReader_CreateObjectFromNonDefaultConstructor_m3484501440 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, JObjectContract_t2867848225 * ___contract1, ConstructorInfo_t4136801618 * ___constructorInfo2, String_t* ___id3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<Newtonsoft.Json.Serialization.JsonProperty,System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ResolvePropertyAndConstructorValues(Newtonsoft.Json.Serialization.JObjectContract,Newtonsoft.Json.JsonReader,System.Type)
extern "C"  Il2CppObject* JsonSerializerInternalReader_ResolvePropertyAndConstructorValues_m269940262 (JsonSerializerInternalReader_t3659144454 * __this, JObjectContract_t2867848225 * ___contract0, JsonReader_t816925123 * ___reader1, Type_t * ___objectType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ReadForType(Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.JsonConverter)
extern "C"  bool JsonSerializerInternalReader_ReadForType_m4241498817 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, Type_t * ___t1, JsonConverter_t2159686854 * ___propertyConverter2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::PopulateObject(System.Object,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JObjectContract,System.String)
extern "C"  Il2CppObject * JsonSerializerInternalReader_PopulateObject_m411908070 (JsonSerializerInternalReader_t3659144454 * __this, Il2CppObject * ___newObject0, JsonReader_t816925123 * ___reader1, JObjectContract_t2867848225 * ___contract2, String_t* ___id3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::SetPropertyPresence(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonProperty,System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>)
extern "C"  void JsonSerializerInternalReader_SetPropertyPresence_m310205921 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, JsonProperty_t902655177 * ___property1, Dictionary_2_t696655424 * ___requiredProperties2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::HandleError(Newtonsoft.Json.JsonReader,System.Int32)
extern "C"  void JsonSerializerInternalReader_HandleError_m4186217003 (JsonSerializerInternalReader_t3659144454 * __this, JsonReader_t816925123 * ___reader0, int32_t ___initialDepth1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ParameterInfo Newtonsoft.Json.Serialization.JsonSerializerInternalReader::<CreateObjectFromNonDefaultConstructor>m__38A(System.Reflection.ParameterInfo)
extern "C"  ParameterInfo_t2235474049 * JsonSerializerInternalReader_U3CCreateObjectFromNonDefaultConstructorU3Em__38A_m2390824555 (Il2CppObject * __this /* static, unused */, ParameterInfo_t2235474049 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::<CreateObjectFromNonDefaultConstructor>m__38B(System.Reflection.ParameterInfo)
extern "C"  Il2CppObject * JsonSerializerInternalReader_U3CCreateObjectFromNonDefaultConstructorU3Em__38B_m1520194751 (Il2CppObject * __this /* static, unused */, ParameterInfo_t2235474049 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonSerializerInternalReader::<PopulateObject>m__38E(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  JsonProperty_t902655177 * JsonSerializerInternalReader_U3CPopulateObjectU3Em__38E_m1561552991 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence Newtonsoft.Json.Serialization.JsonSerializerInternalReader::<PopulateObject>m__38F(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  int32_t JsonSerializerInternalReader_U3CPopulateObjectU3Em__38F_m1497421748 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_ArgumentNotNull1(System.Object,System.String)
extern "C"  void JsonSerializerInternalReader_ilo_ArgumentNotNull1_m2911289299 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, String_t* ___parameterName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_FormatWith2(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* JsonSerializerInternalReader_ilo_FormatWith2_m2023691273 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonToken Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_TokenType3(Newtonsoft.Json.JsonReader)
extern "C"  int32_t JsonSerializerInternalReader_ilo_get_TokenType3_m985644105 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_ResolveContract4(Newtonsoft.Json.Serialization.IContractResolver,System.Type)
extern "C"  JsonContract_t1328848902 * JsonSerializerInternalReader_ilo_ResolveContract4_m2107679464 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_ContractResolver5(Newtonsoft.Json.JsonSerializer)
extern "C"  Il2CppObject * JsonSerializerInternalReader_ilo_get_ContractResolver5_m2797981561 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonSerializerProxy Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_GetInternalSerializer6(Newtonsoft.Json.Serialization.JsonSerializerInternalReader)
extern "C"  JsonSerializerProxy_t3893567258 * JsonSerializerInternalReader_ilo_GetInternalSerializer6_m1255269279 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JRaw Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_Create7(Newtonsoft.Json.JsonReader)
extern "C"  JRaw_t225059758 * JsonSerializerInternalReader_ilo_Create7_m2352307352 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_WriteToken8(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonReader)
extern "C"  void JsonSerializerInternalReader_ilo_WriteToken8_m2493853951 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, JsonReader_t816925123 * ___reader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_Token9(Newtonsoft.Json.Linq.JTokenWriter)
extern "C"  JToken_t3412245951 * JsonSerializerInternalReader_ilo_get_Token9_m2110848986 (Il2CppObject * __this /* static, unused */, JTokenWriter_t3702364242 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_Depth10(Newtonsoft.Json.JsonReader)
extern "C"  int32_t JsonSerializerInternalReader_ilo_get_Depth10_m4266771780 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_WriteToken11(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonReader,System.Int32)
extern "C"  void JsonSerializerInternalReader_ilo_WriteToken11_m969475364 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, JsonReader_t816925123 * ___reader1, int32_t ___initialDepth2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_WriteEndObject12(Newtonsoft.Json.JsonWriter)
extern "C"  void JsonSerializerInternalReader_ilo_WriteEndObject12_m2607137525 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_PropertyType13(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Type_t * JsonSerializerInternalReader_ilo_get_PropertyType13_m1734640045 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_GetContractSafe14(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,System.Type,System.Object)
extern "C"  JsonContract_t1328848902 * JsonSerializerInternalReader_ilo_GetContractSafe14_m916059093 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, Type_t * ___type1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IValueProvider Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_ValueProvider15(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Il2CppObject * JsonSerializerInternalReader_ilo_get_ValueProvider15_m150513749 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_GetValue16(Newtonsoft.Json.Serialization.IValueProvider,System.Object)
extern "C"  Il2CppObject * JsonSerializerInternalReader_ilo_GetValue16_m1962791072 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_CreateValueInternal17(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern "C"  Il2CppObject * JsonSerializerInternalReader_ilo_CreateValueInternal17_m397214453 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, JsonReader_t816925123 * ___reader1, Type_t * ___objectType2, JsonContract_t1328848902 * ___contract3, JsonProperty_t902655177 * ___member4, Il2CppObject * ___existingValue5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_CanRead18(Newtonsoft.Json.JsonConverter)
extern "C"  bool JsonSerializerInternalReader_ilo_get_CanRead18_m1358857414 (Il2CppObject * __this /* static, unused */, JsonConverter_t2159686854 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_CreateObject19(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern "C"  Il2CppObject * JsonSerializerInternalReader_ilo_CreateObject19_m3291313744 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, JsonReader_t816925123 * ___reader1, Type_t * ___objectType2, JsonContract_t1328848902 * ___contract3, JsonProperty_t902655177 * ___member4, Il2CppObject * ___existingValue5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_CreateList20(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,System.Object,System.String)
extern "C"  Il2CppObject * JsonSerializerInternalReader_ilo_CreateList20_m1947599287 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, JsonReader_t816925123 * ___reader1, Type_t * ___objectType2, JsonContract_t1328848902 * ___contract3, JsonProperty_t902655177 * ___member4, Il2CppObject * ___existingValue5, String_t* ___reference6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_Value21(Newtonsoft.Json.JsonReader)
extern "C"  Il2CppObject * JsonSerializerInternalReader_ilo_get_Value21_m1200002827 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_GetMatchingConverter22(Newtonsoft.Json.JsonSerializer,System.Type)
extern "C"  JsonConverter_t2159686854 * JsonSerializerInternalReader_ilo_GetMatchingConverter22_m1387519018 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_ResolveReference23(Newtonsoft.Json.Serialization.IReferenceResolver,System.Object,System.String)
extern "C"  Il2CppObject * JsonSerializerInternalReader_ilo_ResolveReference23_m381844717 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___context1, String_t* ___reference2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_TypeNameHandling24(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Nullable_1_t2443451997  JsonSerializerInternalReader_ilo_get_TypeNameHandling24_m1096968013 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonSerializer Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_Serializer25(Newtonsoft.Json.Serialization.JsonSerializerInternalBase)
extern "C"  JsonSerializer_t251850770 * JsonSerializerInternalReader_ilo_get_Serializer25_m936898202 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalBase_t2068678036 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.SerializationBinder Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_Binder26(Newtonsoft.Json.JsonSerializer)
extern "C"  SerializationBinder_t2137423328 * JsonSerializerInternalReader_ilo_get_Binder26_m3063540455 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_HasDefinedType27(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,System.Type)
extern "C"  bool JsonSerializerInternalReader_ilo_HasDefinedType27_m2743211825 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_CreateAndPopulateDictionary28(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonDictionaryContract,System.String)
extern "C"  Il2CppObject * JsonSerializerInternalReader_ilo_CreateAndPopulateDictionary28_m190210737 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, JsonReader_t816925123 * ___reader1, JsonDictionaryContract_t989352188 * ___contract2, String_t* ___id3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_CreateAndPopulateObject29(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JObjectContract,System.String)
extern "C"  Il2CppObject * JsonSerializerInternalReader_ilo_CreateAndPopulateObject29_m227018436 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, JsonReader_t816925123 * ___reader1, JObjectContract_t2867848225 * ___contract2, String_t* ___id3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_CreateISerializable30(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonISerializableContract,System.String)
extern "C"  Il2CppObject * JsonSerializerInternalReader_ilo_CreateISerializable30_m2330102351 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, JsonReader_t816925123 * ___reader1, JsonISerializableContract_t624170136 * ___contract2, String_t* ___id3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonArrayContract Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_EnsureArrayContract31(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,System.Type,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  JsonArrayContract_t145179369 * JsonSerializerInternalReader_ilo_EnsureArrayContract31_m1346199730 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, Type_t * ___objectType1, JsonContract_t1328848902 * ___contract2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_CreateAndPopulateList32(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,Newtonsoft.Json.JsonReader,System.String,Newtonsoft.Json.Serialization.JsonArrayContract)
extern "C"  Il2CppObject * JsonSerializerInternalReader_ilo_CreateAndPopulateList32_m2119544549 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, JsonReader_t816925123 * ___reader1, String_t* ___reference2, JsonArrayContract_t145179369 * ___contract3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_CreateJToken33(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  JToken_t3412245951 * JsonSerializerInternalReader_ilo_CreateJToken33_m887897861 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, JsonReader_t816925123 * ___reader1, JsonContract_t1328848902 * ___contract2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_Skip34(Newtonsoft.Json.JsonReader)
extern "C"  void JsonSerializerInternalReader_ilo_Skip34_m1972893321 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling> Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_ObjectCreationHandling35(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Nullable_1_t140208118  JsonSerializerInternalReader_ilo_get_ObjectCreationHandling35_m2379495135 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_InheritsGenericDefinition36(System.Type,System.Type)
extern "C"  bool JsonSerializerInternalReader_ilo_InheritsGenericDefinition36_m4138190194 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, Type_t * ___genericClassDefinition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_NullValueHandling37(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Nullable_1_t2838778904  JsonSerializerInternalReader_ilo_get_NullValueHandling37_m3944639813 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling> Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_DefaultValueHandling38(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Nullable_1_t1653574568  JsonSerializerInternalReader_ilo_get_DefaultValueHandling38_m3069172422 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_DefaultValue39(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Il2CppObject * JsonSerializerInternalReader_ilo_get_DefaultValue39_m747736973 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_ShouldSetPropertyValue40(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern "C"  bool JsonSerializerInternalReader_ilo_ShouldSetPropertyValue40_m2131159083 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, JsonProperty_t902655177 * ___property1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`2<System.Object,System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_SetIsSpecified41(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Action_2_t4293064463 * JsonSerializerInternalReader_ilo_get_SetIsSpecified41_m2708325085 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_ValueEquals42(System.Object,System.Object)
extern "C"  bool JsonSerializerInternalReader_ilo_ValueEquals42_m3271331759 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___objA0, Il2CppObject * ___objB1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_DefaultCreator43(Newtonsoft.Json.Serialization.JsonContract)
extern "C"  Func_1_t1001010649 * JsonSerializerInternalReader_ilo_get_DefaultCreator43_m2323233444 (Il2CppObject * __this /* static, unused */, JsonContract_t1328848902 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_UnderlyingType44(Newtonsoft.Json.Serialization.JsonContract)
extern "C"  Type_t * JsonSerializerInternalReader_ilo_get_UnderlyingType44_m2569738806 (Il2CppObject * __this /* static, unused */, JsonContract_t1328848902 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_UnderlyingDictionary45(Newtonsoft.Json.Utilities.IWrappedDictionary)
extern "C"  Il2CppObject * JsonSerializerInternalReader_ilo_get_UnderlyingDictionary45_m3916184912 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_InvokeOnDeserializing46(Newtonsoft.Json.Serialization.JsonContract,System.Object,System.Runtime.Serialization.StreamingContext)
extern "C"  void JsonSerializerInternalReader_ilo_InvokeOnDeserializing46_m2086818349 (Il2CppObject * __this /* static, unused */, JsonContract_t1328848902 * ____this0, Il2CppObject * ___o1, StreamingContext_t2761351129  ___context2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_EnsureType47(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,System.Object,System.Globalization.CultureInfo,System.Type)
extern "C"  Il2CppObject * JsonSerializerInternalReader_ilo_EnsureType47_m3670186543 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, Il2CppObject * ___value1, CultureInfo_t1065375142 * ___culture2, Type_t * ___targetType3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_DictionaryValueType48(Newtonsoft.Json.Serialization.JsonDictionaryContract)
extern "C"  Type_t * JsonSerializerInternalReader_ilo_get_DictionaryValueType48_m878166316 (Il2CppObject * __this /* static, unused */, JsonDictionaryContract_t989352188 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_Context49(Newtonsoft.Json.JsonSerializer)
extern "C"  StreamingContext_t2761351129  JsonSerializerInternalReader_ilo_get_Context49_m1095738274 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_ReadForType50(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.JsonConverter)
extern "C"  bool JsonSerializerInternalReader_ilo_ReadForType50_m1182955298 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, JsonReader_t816925123 * ___reader1, Type_t * ___t2, JsonConverter_t2159686854 * ___propertyConverter3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_UnderlyingCollection51(Newtonsoft.Json.Utilities.IWrappedCollection)
extern "C"  Il2CppObject * JsonSerializerInternalReader_ilo_get_UnderlyingCollection51_m4008630059 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_AddReference52(Newtonsoft.Json.Serialization.IReferenceResolver,System.Object,System.String,System.Object)
extern "C"  void JsonSerializerInternalReader_ilo_AddReference52_m2411400973 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___context1, String_t* ___reference2, Il2CppObject * ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_CollectionItemType53(Newtonsoft.Json.Serialization.JsonArrayContract)
extern "C"  Type_t * JsonSerializerInternalReader_ilo_get_CollectionItemType53_m2328086701 (Il2CppObject * __this /* static, unused */, JsonArrayContract_t145179369 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_GetContractSafe54(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,System.Type)
extern "C"  JsonContract_t1328848902 * JsonSerializerInternalReader_ilo_GetContractSafe54_m3544634819 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_CreateValueNonProperty55(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,Newtonsoft.Json.JsonReader,System.Type,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  Il2CppObject * JsonSerializerInternalReader_ilo_CreateValueNonProperty55_m2743566014 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, JsonReader_t816925123 * ___reader1, Type_t * ___objectType2, JsonContract_t1328848902 * ___contract3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonFormatterConverter Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_GetFormatterConverter56(Newtonsoft.Json.Serialization.JsonSerializerInternalReader)
extern "C"  JsonFormatterConverter_t4063403306 * JsonSerializerInternalReader_ilo_GetFormatterConverter56_m4121249901 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_Read57(Newtonsoft.Json.JsonReader)
extern "C"  bool JsonSerializerInternalReader_ilo_Read57_m2558332629 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_InvokeOnDeserialized58(Newtonsoft.Json.Serialization.JsonContract,System.Object,System.Runtime.Serialization.StreamingContext)
extern "C"  void JsonSerializerInternalReader_ilo_InvokeOnDeserialized58_m8576565 (Il2CppObject * __this /* static, unused */, JsonContract_t1328848902 * ____this0, Il2CppObject * ___o1, StreamingContext_t2761351129  ___context2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_OverrideConstructor59(Newtonsoft.Json.Serialization.JObjectContract)
extern "C"  ConstructorInfo_t4136801618 * JsonSerializerInternalReader_ilo_get_OverrideConstructor59_m3024750615 (Il2CppObject * __this /* static, unused */, JObjectContract_t2867848225 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_CreateObjectFromNonDefaultConstructor60(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JObjectContract,System.Reflection.ConstructorInfo,System.String)
extern "C"  Il2CppObject * JsonSerializerInternalReader_ilo_CreateObjectFromNonDefaultConstructor60_m646779548 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, JsonReader_t816925123 * ___reader1, JObjectContract_t2867848225 * ___contract2, ConstructorInfo_t4136801618 * ___constructorInfo3, String_t* ___id4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_PopulateObject61(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,System.Object,Newtonsoft.Json.JsonReader,Newtonsoft.Json.Serialization.JObjectContract,System.String)
extern "C"  Il2CppObject * JsonSerializerInternalReader_ilo_PopulateObject61_m3913045283 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, Il2CppObject * ___newObject1, JsonReader_t816925123 * ___reader2, JObjectContract_t2867848225 * ___contract3, String_t* ___id4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_ConstructorParameters62(Newtonsoft.Json.Serialization.JObjectContract)
extern "C"  JsonPropertyCollection_t717767559 * JsonSerializerInternalReader_ilo_get_ConstructorParameters62_m1335040681 (Il2CppObject * __this /* static, unused */, JObjectContract_t2867848225 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_Converter63(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  JsonConverter_t2159686854 * JsonSerializerInternalReader_ilo_get_Converter63_m1665729997 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_Ignored64(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  bool JsonSerializerInternalReader_ilo_get_Ignored64_m2428370588 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_GetConverter65(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.JsonConverter)
extern "C"  JsonConverter_t2159686854 * JsonSerializerInternalReader_ilo_GetConverter65_m2178184205 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, JsonContract_t1328848902 * ___contract1, JsonConverter_t2159686854 * ___memberConverter2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_ReadAsBytes66(Newtonsoft.Json.JsonReader)
extern "C"  ByteU5BU5D_t4260760469* JsonSerializerInternalReader_ilo_ReadAsBytes66_m4242884882 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_Properties67(Newtonsoft.Json.Serialization.JObjectContract)
extern "C"  JsonPropertyCollection_t717767559 * JsonSerializerInternalReader_ilo_get_Properties67_m4202413365 (Il2CppObject * __this /* static, unused */, JObjectContract_t2867848225 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_SetPropertyValue68(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonReader,System.Object)
extern "C"  void JsonSerializerInternalReader_ilo_SetPropertyValue68_m2062702198 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, JsonProperty_t902655177 * ___property1, JsonReader_t816925123 * ___reader2, Il2CppObject * ___target3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_HandleError69(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,Newtonsoft.Json.JsonReader,System.Int32)
extern "C"  void JsonSerializerInternalReader_ilo_HandleError69_m1026530496 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, JsonReader_t816925123 * ___reader1, int32_t ___initialDepth2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_HasFlag70(Newtonsoft.Json.Serialization.JsonSerializerInternalReader,Newtonsoft.Json.DefaultValueHandling,Newtonsoft.Json.DefaultValueHandling)
extern "C"  bool JsonSerializerInternalReader_ilo_HasFlag70_m4080907223 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalReader_t3659144454 * ____this0, int32_t ___value1, int32_t ___flag2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_SetValue71(Newtonsoft.Json.Serialization.IValueProvider,System.Object,System.Object)
extern "C"  void JsonSerializerInternalReader_ilo_SetValue71_m13437474 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___target1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_get_PropertyName72(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  String_t* JsonSerializerInternalReader_ilo_get_PropertyName72_m1140307904 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalReader::ilo_ClearErrorContext73(Newtonsoft.Json.Serialization.JsonSerializerInternalBase)
extern "C"  void JsonSerializerInternalReader_ilo_ClearErrorContext73_m3530620366 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalBase_t2068678036 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
