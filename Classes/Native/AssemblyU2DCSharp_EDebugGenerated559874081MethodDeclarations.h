﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EDebugGenerated
struct EDebugGenerated_t559874081;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void EDebugGenerated::.ctor()
extern "C"  void EDebugGenerated__ctor_m1226857578 (EDebugGenerated_t559874081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EDebugGenerated::EDebug_EDebug1(JSVCall,System.Int32)
extern "C"  bool EDebugGenerated_EDebug_EDebug1_m1266265288 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebugGenerated::EDebug_CurrentLogLevels(JSVCall)
extern "C"  void EDebugGenerated_EDebug_CurrentLogLevels_m3109197716 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebugGenerated::EDebug_DebugFilterStr(JSVCall)
extern "C"  void EDebugGenerated_EDebug_DebugFilterStr_m141865192 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EDebugGenerated::EDebug_Critical__Object__Boolean(JSVCall,System.Int32)
extern "C"  bool EDebugGenerated_EDebug_Critical__Object__Boolean_m1891893647 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EDebugGenerated::EDebug_Debug__String__Object__Boolean(JSVCall,System.Int32)
extern "C"  bool EDebugGenerated_EDebug_Debug__String__Object__Boolean_m4042630850 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EDebugGenerated::EDebug_Debug__Object__Boolean__Int32(JSVCall,System.Int32)
extern "C"  bool EDebugGenerated_EDebug_Debug__Object__Boolean__Int32_m221101821 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EDebugGenerated::EDebug_Except__Exception__Object(JSVCall,System.Int32)
extern "C"  bool EDebugGenerated_EDebug_Except__Exception__Object_m3228584986 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EDebugGenerated::EDebug_Log__Object__Boolean(JSVCall,System.Int32)
extern "C"  bool EDebugGenerated_EDebug_Log__Object__Boolean_m3049187810 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EDebugGenerated::EDebug_LogError__Object__Boolean(JSVCall,System.Int32)
extern "C"  bool EDebugGenerated_EDebug_LogError__Object__Boolean_m2695943274 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EDebugGenerated::EDebug_LogWarning__Object__Boolean(JSVCall,System.Int32)
extern "C"  bool EDebugGenerated_EDebug_LogWarning__Object__Boolean_m491332182 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EDebugGenerated::EDebug_Release(JSVCall,System.Int32)
extern "C"  bool EDebugGenerated_EDebug_Release_m2493348044 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EDebugGenerated::EDebug_UploadLogFile__String__String(JSVCall,System.Int32)
extern "C"  bool EDebugGenerated_EDebug_UploadLogFile__String__String_m2416680198 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebugGenerated::__Register()
extern "C"  void EDebugGenerated___Register_m2201768605 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EDebugGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t EDebugGenerated_ilo_getObject1_m2859707404 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EDebugGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool EDebugGenerated_ilo_attachFinalizerObject2_m1650305294 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebugGenerated::ilo_setEnum3(System.Int32,System.Int32)
extern "C"  void EDebugGenerated_ilo_setEnum3_m403460297 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 EDebugGenerated::ilo_getEnum4(System.Int32)
extern "C"  int32_t EDebugGenerated_ilo_getEnum4_m467097293 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebugGenerated::ilo_setStringS5(System.Int32,System.String)
extern "C"  void EDebugGenerated_ilo_setStringS5_m2950157367 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EDebugGenerated::ilo_getWhatever6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * EDebugGenerated_ilo_getWhatever6_m76657597 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebugGenerated::ilo_Debug7(System.String,System.Object,System.Boolean)
extern "C"  void EDebugGenerated_ilo_Debug7_m1681422654 (Il2CppObject * __this /* static, unused */, String_t* ___filter0, Il2CppObject * ___message1, bool ___isShowStack2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebugGenerated::ilo_Debug8(System.Object,System.Boolean,System.Int32)
extern "C"  void EDebugGenerated_ilo_Debug8_m538922300 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, int32_t ___user2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean EDebugGenerated::ilo_getBooleanS9(System.Int32)
extern "C"  bool EDebugGenerated_ilo_getBooleanS9_m2521449282 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EDebugGenerated::ilo_getObject10(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * EDebugGenerated_ilo_getObject10_m699326053 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebugGenerated::ilo_Log11(System.Object,System.Boolean)
extern "C"  void EDebugGenerated_ilo_Log11_m2446929858 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebugGenerated::ilo_LogError12(System.Object,System.Boolean)
extern "C"  void EDebugGenerated_ilo_LogError12_m3065310075 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebugGenerated::ilo_Release13()
extern "C"  void EDebugGenerated_ilo_Release13_m3755466910 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EDebugGenerated::ilo_UploadLogFile14(System.String,System.String)
extern "C"  void EDebugGenerated_ilo_UploadLogFile14_m3913100903 (Il2CppObject * __this /* static, unused */, String_t* ___date0, String_t* ___toAddress1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String EDebugGenerated::ilo_getStringS15(System.Int32)
extern "C"  String_t* EDebugGenerated_ilo_getStringS15_m58567051 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
