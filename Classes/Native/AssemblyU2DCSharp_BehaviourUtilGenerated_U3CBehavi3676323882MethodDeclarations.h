﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtilGenerated/<BehaviourUtil_DelayCall_GetDelegate_member2_arg1>c__AnonStorey45`3<System.Object,System.Object,System.Object>
struct U3CBehaviourUtil_DelayCall_GetDelegate_member2_arg1U3Ec__AnonStorey45_3_t3676323882;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void BehaviourUtilGenerated/<BehaviourUtil_DelayCall_GetDelegate_member2_arg1>c__AnonStorey45`3<System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CBehaviourUtil_DelayCall_GetDelegate_member2_arg1U3Ec__AnonStorey45_3__ctor_m1591908721_gshared (U3CBehaviourUtil_DelayCall_GetDelegate_member2_arg1U3Ec__AnonStorey45_3_t3676323882 * __this, const MethodInfo* method);
#define U3CBehaviourUtil_DelayCall_GetDelegate_member2_arg1U3Ec__AnonStorey45_3__ctor_m1591908721(__this, method) ((  void (*) (U3CBehaviourUtil_DelayCall_GetDelegate_member2_arg1U3Ec__AnonStorey45_3_t3676323882 *, const MethodInfo*))U3CBehaviourUtil_DelayCall_GetDelegate_member2_arg1U3Ec__AnonStorey45_3__ctor_m1591908721_gshared)(__this, method)
// System.Void BehaviourUtilGenerated/<BehaviourUtil_DelayCall_GetDelegate_member2_arg1>c__AnonStorey45`3<System.Object,System.Object,System.Object>::<>m__4(T1,T2,T3)
extern "C"  void U3CBehaviourUtil_DelayCall_GetDelegate_member2_arg1U3Ec__AnonStorey45_3_U3CU3Em__4_m3807648382_gshared (U3CBehaviourUtil_DelayCall_GetDelegate_member2_arg1U3Ec__AnonStorey45_3_t3676323882 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, Il2CppObject * ___arg32, const MethodInfo* method);
#define U3CBehaviourUtil_DelayCall_GetDelegate_member2_arg1U3Ec__AnonStorey45_3_U3CU3Em__4_m3807648382(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (U3CBehaviourUtil_DelayCall_GetDelegate_member2_arg1U3Ec__AnonStorey45_3_t3676323882 *, Il2CppObject *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CBehaviourUtil_DelayCall_GetDelegate_member2_arg1U3Ec__AnonStorey45_3_U3CU3Em__4_m3807648382_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
