﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginQuWan
struct  PluginQuWan_t2552410765  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginQuWan::loginToken
	String_t* ___loginToken_2;
	// System.String PluginQuWan::time
	String_t* ___time_3;
	// System.String PluginQuWan::userId
	String_t* ___userId_4;
	// System.String PluginQuWan::channelId
	String_t* ___channelId_5;
	// System.Boolean PluginQuWan::isOut
	bool ___isOut_6;
	// System.String PluginQuWan::configId
	String_t* ___configId_7;

public:
	inline static int32_t get_offset_of_loginToken_2() { return static_cast<int32_t>(offsetof(PluginQuWan_t2552410765, ___loginToken_2)); }
	inline String_t* get_loginToken_2() const { return ___loginToken_2; }
	inline String_t** get_address_of_loginToken_2() { return &___loginToken_2; }
	inline void set_loginToken_2(String_t* value)
	{
		___loginToken_2 = value;
		Il2CppCodeGenWriteBarrier(&___loginToken_2, value);
	}

	inline static int32_t get_offset_of_time_3() { return static_cast<int32_t>(offsetof(PluginQuWan_t2552410765, ___time_3)); }
	inline String_t* get_time_3() const { return ___time_3; }
	inline String_t** get_address_of_time_3() { return &___time_3; }
	inline void set_time_3(String_t* value)
	{
		___time_3 = value;
		Il2CppCodeGenWriteBarrier(&___time_3, value);
	}

	inline static int32_t get_offset_of_userId_4() { return static_cast<int32_t>(offsetof(PluginQuWan_t2552410765, ___userId_4)); }
	inline String_t* get_userId_4() const { return ___userId_4; }
	inline String_t** get_address_of_userId_4() { return &___userId_4; }
	inline void set_userId_4(String_t* value)
	{
		___userId_4 = value;
		Il2CppCodeGenWriteBarrier(&___userId_4, value);
	}

	inline static int32_t get_offset_of_channelId_5() { return static_cast<int32_t>(offsetof(PluginQuWan_t2552410765, ___channelId_5)); }
	inline String_t* get_channelId_5() const { return ___channelId_5; }
	inline String_t** get_address_of_channelId_5() { return &___channelId_5; }
	inline void set_channelId_5(String_t* value)
	{
		___channelId_5 = value;
		Il2CppCodeGenWriteBarrier(&___channelId_5, value);
	}

	inline static int32_t get_offset_of_isOut_6() { return static_cast<int32_t>(offsetof(PluginQuWan_t2552410765, ___isOut_6)); }
	inline bool get_isOut_6() const { return ___isOut_6; }
	inline bool* get_address_of_isOut_6() { return &___isOut_6; }
	inline void set_isOut_6(bool value)
	{
		___isOut_6 = value;
	}

	inline static int32_t get_offset_of_configId_7() { return static_cast<int32_t>(offsetof(PluginQuWan_t2552410765, ___configId_7)); }
	inline String_t* get_configId_7() const { return ___configId_7; }
	inline String_t** get_address_of_configId_7() { return &___configId_7; }
	inline void set_configId_7(String_t* value)
	{
		___configId_7 = value;
		Il2CppCodeGenWriteBarrier(&___configId_7, value);
	}
};

struct PluginQuWan_t2552410765_StaticFields
{
public:
	// System.Action PluginQuWan::<>f__am$cache6
	Action_t3771233898 * ___U3CU3Ef__amU24cache6_8;
	// System.Action PluginQuWan::<>f__am$cache7
	Action_t3771233898 * ___U3CU3Ef__amU24cache7_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_8() { return static_cast<int32_t>(offsetof(PluginQuWan_t2552410765_StaticFields, ___U3CU3Ef__amU24cache6_8)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache6_8() const { return ___U3CU3Ef__amU24cache6_8; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache6_8() { return &___U3CU3Ef__amU24cache6_8; }
	inline void set_U3CU3Ef__amU24cache6_8(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache6_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_9() { return static_cast<int32_t>(offsetof(PluginQuWan_t2552410765_StaticFields, ___U3CU3Ef__amU24cache7_9)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache7_9() const { return ___U3CU3Ef__amU24cache7_9; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache7_9() { return &___U3CU3Ef__amU24cache7_9; }
	inline void set_U3CU3Ef__amU24cache7_9(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache7_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
