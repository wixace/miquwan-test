﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ParticleAnimatorGenerated
struct UnityEngine_ParticleAnimatorGenerated_t2311695386;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_ParticleAnimatorGenerated::.ctor()
extern "C"  void UnityEngine_ParticleAnimatorGenerated__ctor_m3326442129 (UnityEngine_ParticleAnimatorGenerated_t2311695386 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ParticleAnimatorGenerated::ParticleAnimator_ParticleAnimator1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ParticleAnimatorGenerated_ParticleAnimator_ParticleAnimator1_m3459257185 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleAnimatorGenerated::ParticleAnimator_doesAnimateColor(JSVCall)
extern "C"  void UnityEngine_ParticleAnimatorGenerated_ParticleAnimator_doesAnimateColor_m2148581931 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleAnimatorGenerated::ParticleAnimator_worldRotationAxis(JSVCall)
extern "C"  void UnityEngine_ParticleAnimatorGenerated_ParticleAnimator_worldRotationAxis_m1137047381 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleAnimatorGenerated::ParticleAnimator_localRotationAxis(JSVCall)
extern "C"  void UnityEngine_ParticleAnimatorGenerated_ParticleAnimator_localRotationAxis_m1868842268 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleAnimatorGenerated::ParticleAnimator_sizeGrow(JSVCall)
extern "C"  void UnityEngine_ParticleAnimatorGenerated_ParticleAnimator_sizeGrow_m1176084658 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleAnimatorGenerated::ParticleAnimator_rndForce(JSVCall)
extern "C"  void UnityEngine_ParticleAnimatorGenerated_ParticleAnimator_rndForce_m1730755363 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleAnimatorGenerated::ParticleAnimator_force(JSVCall)
extern "C"  void UnityEngine_ParticleAnimatorGenerated_ParticleAnimator_force_m4009596571 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleAnimatorGenerated::ParticleAnimator_damping(JSVCall)
extern "C"  void UnityEngine_ParticleAnimatorGenerated_ParticleAnimator_damping_m554591844 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleAnimatorGenerated::ParticleAnimator_autodestruct(JSVCall)
extern "C"  void UnityEngine_ParticleAnimatorGenerated_ParticleAnimator_autodestruct_m2483652929 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleAnimatorGenerated::ParticleAnimator_colorAnimation(JSVCall)
extern "C"  void UnityEngine_ParticleAnimatorGenerated_ParticleAnimator_colorAnimation_m1258223525 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleAnimatorGenerated::__Register()
extern "C"  void UnityEngine_ParticleAnimatorGenerated___Register_m3911644374 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine_ParticleAnimatorGenerated::<ParticleAnimator_colorAnimation>m__282()
extern "C"  ColorU5BU5D_t2441545636* UnityEngine_ParticleAnimatorGenerated_U3CParticleAnimator_colorAnimationU3Em__282_m2390907930 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleAnimatorGenerated::ilo_setBooleanS1(System.Int32,System.Boolean)
extern "C"  void UnityEngine_ParticleAnimatorGenerated_ilo_setBooleanS1_m2318755906 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleAnimatorGenerated::ilo_setVector3S2(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_ParticleAnimatorGenerated_ilo_setVector3S2_m60205783 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_ParticleAnimatorGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_ParticleAnimatorGenerated_ilo_getSingle3_m2168520608 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ParticleAnimatorGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_ParticleAnimatorGenerated_ilo_setObject4_m804521216 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ParticleAnimatorGenerated::ilo_setArrayS5(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_ParticleAnimatorGenerated_ilo_setArrayS5_m192965014 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ParticleAnimatorGenerated::ilo_getObject6(System.Int32)
extern "C"  int32_t UnityEngine_ParticleAnimatorGenerated_ilo_getObject6_m2724552202 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
