﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSArenaInterServiceData
struct CSArenaInterServiceData_t1260466202;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void CSArenaInterServiceData::.ctor()
extern "C"  void CSArenaInterServiceData__ctor_m4105743505 (CSArenaInterServiceData_t1260466202 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSArenaInterServiceData::LoadData(System.String)
extern "C"  void CSArenaInterServiceData_LoadData_m1777604767 (CSArenaInterServiceData_t1260466202 * __this, String_t* ___jsonStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CSArenaInterServiceData::ilo_DeserializeObject1(System.String)
extern "C"  Il2CppObject * CSArenaInterServiceData_ilo_DeserializeObject1_m1443133758 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
