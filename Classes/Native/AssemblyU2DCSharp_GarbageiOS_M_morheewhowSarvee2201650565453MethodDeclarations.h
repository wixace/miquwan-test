﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_morheewhowSarvee220
struct M_morheewhowSarvee220_t1650565453;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_morheewhowSarvee2201650565453.h"

// System.Void GarbageiOS.M_morheewhowSarvee220::.ctor()
extern "C"  void M_morheewhowSarvee220__ctor_m902391990 (M_morheewhowSarvee220_t1650565453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_morheewhowSarvee220::M_qarmaw0(System.String[],System.Int32)
extern "C"  void M_morheewhowSarvee220_M_qarmaw0_m3268818312 (M_morheewhowSarvee220_t1650565453 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_morheewhowSarvee220::M_cisearteFijem1(System.String[],System.Int32)
extern "C"  void M_morheewhowSarvee220_M_cisearteFijem1_m1539701107 (M_morheewhowSarvee220_t1650565453 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_morheewhowSarvee220::M_hipur2(System.String[],System.Int32)
extern "C"  void M_morheewhowSarvee220_M_hipur2_m94165725 (M_morheewhowSarvee220_t1650565453 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_morheewhowSarvee220::M_raisal3(System.String[],System.Int32)
extern "C"  void M_morheewhowSarvee220_M_raisal3_m205880872 (M_morheewhowSarvee220_t1650565453 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_morheewhowSarvee220::ilo_M_hipur21(GarbageiOS.M_morheewhowSarvee220,System.String[],System.Int32)
extern "C"  void M_morheewhowSarvee220_ilo_M_hipur21_m3839134056 (Il2CppObject * __this /* static, unused */, M_morheewhowSarvee220_t1650565453 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
