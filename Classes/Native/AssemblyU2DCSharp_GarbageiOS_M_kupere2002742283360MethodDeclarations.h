﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_kupere200
struct M_kupere200_t2742283360;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_kupere200::.ctor()
extern "C"  void M_kupere200__ctor_m306520323 (M_kupere200_t2742283360 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kupere200::M_mirsteajouCemtayster0(System.String[],System.Int32)
extern "C"  void M_kupere200_M_mirsteajouCemtayster0_m934465322 (M_kupere200_t2742283360 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kupere200::M_nerpe1(System.String[],System.Int32)
extern "C"  void M_kupere200_M_nerpe1_m569541067 (M_kupere200_t2742283360 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
