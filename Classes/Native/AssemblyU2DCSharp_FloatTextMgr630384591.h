﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>
struct Dictionary_2_t3981672550;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FloatTextMgr
struct  FloatTextMgr_t630384591  : public Il2CppObject
{
public:
	// UnityEngine.GameObject FloatTextMgr::FloatTextControl
	GameObject_t3674682005 * ___FloatTextControl_1;
	// System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>> FloatTextMgr::CacheFloatTextDic
	Dictionary_2_t3981672550 * ___CacheFloatTextDic_2;

public:
	inline static int32_t get_offset_of_FloatTextControl_1() { return static_cast<int32_t>(offsetof(FloatTextMgr_t630384591, ___FloatTextControl_1)); }
	inline GameObject_t3674682005 * get_FloatTextControl_1() const { return ___FloatTextControl_1; }
	inline GameObject_t3674682005 ** get_address_of_FloatTextControl_1() { return &___FloatTextControl_1; }
	inline void set_FloatTextControl_1(GameObject_t3674682005 * value)
	{
		___FloatTextControl_1 = value;
		Il2CppCodeGenWriteBarrier(&___FloatTextControl_1, value);
	}

	inline static int32_t get_offset_of_CacheFloatTextDic_2() { return static_cast<int32_t>(offsetof(FloatTextMgr_t630384591, ___CacheFloatTextDic_2)); }
	inline Dictionary_2_t3981672550 * get_CacheFloatTextDic_2() const { return ___CacheFloatTextDic_2; }
	inline Dictionary_2_t3981672550 ** get_address_of_CacheFloatTextDic_2() { return &___CacheFloatTextDic_2; }
	inline void set_CacheFloatTextDic_2(Dictionary_2_t3981672550 * value)
	{
		___CacheFloatTextDic_2 = value;
		Il2CppCodeGenWriteBarrier(&___CacheFloatTextDic_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
