﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._77badf244cd82ec6cf120b8dfe053739
struct _77badf244cd82ec6cf120b8dfe053739_t2465429845;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._77badf244cd82ec6cf120b8dfe053739::.ctor()
extern "C"  void _77badf244cd82ec6cf120b8dfe053739__ctor_m2634701944 (_77badf244cd82ec6cf120b8dfe053739_t2465429845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._77badf244cd82ec6cf120b8dfe053739::_77badf244cd82ec6cf120b8dfe053739m2(System.Int32)
extern "C"  int32_t _77badf244cd82ec6cf120b8dfe053739__77badf244cd82ec6cf120b8dfe053739m2_m1473669497 (_77badf244cd82ec6cf120b8dfe053739_t2465429845 * __this, int32_t ____77badf244cd82ec6cf120b8dfe053739a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._77badf244cd82ec6cf120b8dfe053739::_77badf244cd82ec6cf120b8dfe053739m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _77badf244cd82ec6cf120b8dfe053739__77badf244cd82ec6cf120b8dfe053739m_m1270274397 (_77badf244cd82ec6cf120b8dfe053739_t2465429845 * __this, int32_t ____77badf244cd82ec6cf120b8dfe053739a0, int32_t ____77badf244cd82ec6cf120b8dfe053739771, int32_t ____77badf244cd82ec6cf120b8dfe053739c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
