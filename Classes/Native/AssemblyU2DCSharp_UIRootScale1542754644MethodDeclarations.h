﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIRootScale
struct UIRootScale_t1542754644;
// UIWidget
struct UIWidget_t769069560;
// CEvent.ZEvent
struct ZEvent_t3638018500;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIRootScale1542754644.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"

// System.Void UIRootScale::.ctor()
extern "C"  void UIRootScale__ctor_m3940755607 (UIRootScale_t1542754644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootScale::Awake()
extern "C"  void UIRootScale_Awake_m4178360826 (UIRootScale_t1542754644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootScale::OnDestroy()
extern "C"  void UIRootScale_OnDestroy_m2368789008 (UIRootScale_t1542754644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootScale::Start()
extern "C"  void UIRootScale_Start_m2887893399 (UIRootScale_t1542754644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootScale::ScreenSizeChanged()
extern "C"  void UIRootScale_ScreenSizeChanged_m3209483612 (UIRootScale_t1542754644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootScale::Update()
extern "C"  void UIRootScale_Update_m3631201622 (UIRootScale_t1542754644 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootScale::ilo_Update1(UIRootScale)
extern "C"  void UIRootScale_ilo_Update1_m2677188212 (Il2CppObject * __this /* static, unused */, UIRootScale_t1542754644 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UIRootScale::ilo_get_localSize2(UIWidget)
extern "C"  Vector2_t4282066565  UIRootScale_ilo_get_localSize2_m2050013532 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIRootScale::ilo_DispatchEvent3(CEvent.ZEvent)
extern "C"  void UIRootScale_ilo_DispatchEvent3_m2957900072 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
