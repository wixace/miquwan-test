﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginAnqu
struct  PluginAnqu_t1605873924  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginAnqu::userId
	String_t* ___userId_2;
	// System.String PluginAnqu::sessiond
	String_t* ___sessiond_3;
	// System.String PluginAnqu::configId
	String_t* ___configId_4;

public:
	inline static int32_t get_offset_of_userId_2() { return static_cast<int32_t>(offsetof(PluginAnqu_t1605873924, ___userId_2)); }
	inline String_t* get_userId_2() const { return ___userId_2; }
	inline String_t** get_address_of_userId_2() { return &___userId_2; }
	inline void set_userId_2(String_t* value)
	{
		___userId_2 = value;
		Il2CppCodeGenWriteBarrier(&___userId_2, value);
	}

	inline static int32_t get_offset_of_sessiond_3() { return static_cast<int32_t>(offsetof(PluginAnqu_t1605873924, ___sessiond_3)); }
	inline String_t* get_sessiond_3() const { return ___sessiond_3; }
	inline String_t** get_address_of_sessiond_3() { return &___sessiond_3; }
	inline void set_sessiond_3(String_t* value)
	{
		___sessiond_3 = value;
		Il2CppCodeGenWriteBarrier(&___sessiond_3, value);
	}

	inline static int32_t get_offset_of_configId_4() { return static_cast<int32_t>(offsetof(PluginAnqu_t1605873924, ___configId_4)); }
	inline String_t* get_configId_4() const { return ___configId_4; }
	inline String_t** get_address_of_configId_4() { return &___configId_4; }
	inline void set_configId_4(String_t* value)
	{
		___configId_4 = value;
		Il2CppCodeGenWriteBarrier(&___configId_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
