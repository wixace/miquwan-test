﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GrayscaleEffect
struct GrayscaleEffect_t967569688;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void GrayscaleEffect::.ctor()
extern "C"  void GrayscaleEffect__ctor_m2179105823 (GrayscaleEffect_t967569688 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GrayscaleEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void GrayscaleEffect_OnRenderImage_m2200279263 (GrayscaleEffect_t967569688 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
