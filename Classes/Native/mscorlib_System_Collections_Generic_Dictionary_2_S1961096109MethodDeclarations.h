﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int2,System.Int32>
struct ShimEnumerator_t1961096109;
// System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>
struct Dictionary_2_t2245318082;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int2,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m2395012472_gshared (ShimEnumerator_t1961096109 * __this, Dictionary_2_t2245318082 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m2395012472(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1961096109 *, Dictionary_2_t2245318082 *, const MethodInfo*))ShimEnumerator__ctor_m2395012472_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int2,System.Int32>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1012655145_gshared (ShimEnumerator_t1961096109 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1012655145(__this, method) ((  bool (*) (ShimEnumerator_t1961096109 *, const MethodInfo*))ShimEnumerator_MoveNext_m1012655145_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int2,System.Int32>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m3194089355_gshared (ShimEnumerator_t1961096109 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m3194089355(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1961096109 *, const MethodInfo*))ShimEnumerator_get_Entry_m3194089355_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int2,System.Int32>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3787548326_gshared (ShimEnumerator_t1961096109 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m3787548326(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1961096109 *, const MethodInfo*))ShimEnumerator_get_Key_m3787548326_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int2,System.Int32>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3045077944_gshared (ShimEnumerator_t1961096109 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m3045077944(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1961096109 *, const MethodInfo*))ShimEnumerator_get_Value_m3045077944_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int2,System.Int32>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2871866432_gshared (ShimEnumerator_t1961096109 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2871866432(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1961096109 *, const MethodInfo*))ShimEnumerator_get_Current_m2871866432_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int2,System.Int32>::Reset()
extern "C"  void ShimEnumerator_Reset_m3828671434_gshared (ShimEnumerator_t1961096109 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m3828671434(__this, method) ((  void (*) (ShimEnumerator_t1961096109 *, const MethodInfo*))ShimEnumerator_Reset_m3828671434_gshared)(__this, method)
