﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlotFightTalk
struct PlotFightTalk_t789215451;
// UnityEngine.Transform
struct Transform_t1659122786;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// CEvent.ZEvent
struct ZEvent_t3638018500;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"

// System.Void PlotFightTalk::.ctor()
extern "C"  void PlotFightTalk__ctor_m1791667952 (PlotFightTalk_t789215451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalk::Awake()
extern "C"  void PlotFightTalk_Awake_m2029273171 (PlotFightTalk_t789215451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalk::Init(UnityEngine.Transform,System.Int32,System.String,System.Single)
extern "C"  void PlotFightTalk_Init_m3374613055 (PlotFightTalk_t789215451 * __this, Transform_t1659122786 * ___targe0, int32_t ___id1, String_t* ___file2, float ___left3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalk::Hid()
extern "C"  void PlotFightTalk_Hid_m2171286033 (PlotFightTalk_t789215451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalk::Show()
extern "C"  void PlotFightTalk_Show_m3199758353 (PlotFightTalk_t789215451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalk::Update()
extern "C"  void PlotFightTalk_Update_m1433993757 (PlotFightTalk_t789215451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalk::Clear()
extern "C"  void PlotFightTalk_Clear_m3492768539 (PlotFightTalk_t789215451 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalk::ilo_ScaleTo1(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern "C"  void PlotFightTalk_ilo_ScaleTo1_m124293091 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___target0, Vector3_t4282066566  ___scale1, float ___time2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalk::ilo_DispatchEvent2(CEvent.ZEvent)
extern "C"  void PlotFightTalk_ilo_DispatchEvent2_m131527778 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
