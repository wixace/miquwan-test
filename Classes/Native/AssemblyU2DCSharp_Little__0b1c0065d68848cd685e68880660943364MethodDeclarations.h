﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._0b1c0065d68848cd685e688804c19acf
struct _0b1c0065d68848cd685e688804c19acf_t660943364;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._0b1c0065d68848cd685e688804c19acf::.ctor()
extern "C"  void _0b1c0065d68848cd685e688804c19acf__ctor_m1060894889 (_0b1c0065d68848cd685e688804c19acf_t660943364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._0b1c0065d68848cd685e688804c19acf::_0b1c0065d68848cd685e688804c19acfm2(System.Int32)
extern "C"  int32_t _0b1c0065d68848cd685e688804c19acf__0b1c0065d68848cd685e688804c19acfm2_m1005302553 (_0b1c0065d68848cd685e688804c19acf_t660943364 * __this, int32_t ____0b1c0065d68848cd685e688804c19acfa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._0b1c0065d68848cd685e688804c19acf::_0b1c0065d68848cd685e688804c19acfm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _0b1c0065d68848cd685e688804c19acf__0b1c0065d68848cd685e688804c19acfm_m3653870525 (_0b1c0065d68848cd685e688804c19acf_t660943364 * __this, int32_t ____0b1c0065d68848cd685e688804c19acfa0, int32_t ____0b1c0065d68848cd685e688804c19acf801, int32_t ____0b1c0065d68848cd685e688804c19acfc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
