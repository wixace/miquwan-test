﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginQuick
struct PluginQuick_t2552428122;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Action
struct Action_t3771233898;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginQuick2552428122.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_FloatTextMgr630384591.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"

// System.Void PluginQuick::.ctor()
extern "C"  void PluginQuick__ctor_m902761041 (PluginQuick_t2552428122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::Init()
extern "C"  void PluginQuick_Init_m2335962595 (PluginQuick_t2552428122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginQuick::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginQuick_ReqSDKHttpLogin_m4051572354 (PluginQuick_t2552428122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginQuick::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginQuick_IsLoginSuccess_m2205950458 (PluginQuick_t2552428122 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::OpenUserLogin()
extern "C"  void PluginQuick_OpenUserLogin_m3330624419 (PluginQuick_t2552428122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::CreatRole(CEvent.ZEvent)
extern "C"  void PluginQuick_CreatRole_m2388413933 (PluginQuick_t2552428122 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::RoleEnterGame(CEvent.ZEvent)
extern "C"  void PluginQuick_RoleEnterGame_m4062156248 (PluginQuick_t2552428122 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginQuick_RoleUpgrade_m3884000870 (PluginQuick_t2552428122 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::UserPay(CEvent.ZEvent)
extern "C"  void PluginQuick_UserPay_m3150150255 (PluginQuick_t2552428122 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::OnInitSuccess(System.String)
extern "C"  void PluginQuick_OnInitSuccess_m2407057919 (PluginQuick_t2552428122 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::OnInitFail(System.String)
extern "C"  void PluginQuick_OnInitFail_m48103330 (PluginQuick_t2552428122 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::OnLoginSuccess(System.String)
extern "C"  void PluginQuick_OnLoginSuccess_m1994063510 (PluginQuick_t2552428122 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::OnLoginFail(System.String)
extern "C"  void PluginQuick_OnLoginFail_m121039467 (PluginQuick_t2552428122 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::OnLogoutSuccess(System.String)
extern "C"  void PluginQuick_OnLogoutSuccess_m1689969337 (PluginQuick_t2552428122 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::OnPaySuccess(System.String)
extern "C"  void PluginQuick_OnPaySuccess_m3967553109 (PluginQuick_t2552428122 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::OnPayFail(System.String)
extern "C"  void PluginQuick_OnPayFail_m4092267276 (PluginQuick_t2552428122 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::OnPayCancel(System.String)
extern "C"  void PluginQuick_OnPayCancel_m622666224 (PluginQuick_t2552428122 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::OnUserSwitchSuccess(System.String)
extern "C"  void PluginQuick_OnUserSwitchSuccess_m332081614 (PluginQuick_t2552428122 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::OnUserSwitchFail(System.String)
extern "C"  void PluginQuick_OnUserSwitchFail_m2140228147 (PluginQuick_t2552428122 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::OnExitGameSuccess(System.String)
extern "C"  void PluginQuick_OnExitGameSuccess_m2423281311 (PluginQuick_t2552428122 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::Quick_Exit()
extern "C"  void PluginQuick_Quick_Exit_m2116257699 (PluginQuick_t2552428122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::initSdk(System.String,System.String)
extern "C"  void PluginQuick_initSdk_m4020481413 (Il2CppObject * __this /* static, unused */, String_t* ___productKey0, String_t* ___productCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::login()
extern "C"  void PluginQuick_login_m424775864 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::updateRoleInfo(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginQuick_updateRoleInfo_m1226313666 (Il2CppObject * __this /* static, unused */, String_t* ___serverName0, String_t* ___roleName1, String_t* ___serverId2, String_t* ___roleId3, String_t* ___balance4, String_t* ___vipLv5, String_t* ___roleLv6, String_t* ___partyName7, String_t* ___isCreate8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginQuick_pay_m3159995795 (Il2CppObject * __this /* static, unused */, String_t* ___goodId0, String_t* ___goodName1, String_t* ___orderId2, String_t* ___count3, String_t* ___price4, String_t* ___cbUrl5, String_t* ___eArgs6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::<OnLogoutSuccess>m__450()
extern "C"  void PluginQuick_U3COnLogoutSuccessU3Em__450_m2062107025 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::ilo_Log1(System.Object,System.Boolean)
extern "C"  void PluginQuick_ilo_Log1_m3621409500 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginQuick::ilo_get_isSdkLogin2(VersionMgr)
extern "C"  bool PluginQuick_ilo_get_isSdkLogin2_m3497790784 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginQuick::ilo_get_PluginsSdkMgr3()
extern "C"  PluginsSdkMgr_t3884624670 * PluginQuick_ilo_get_PluginsSdkMgr3_m1776440303 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginQuick::ilo_get_Instance4()
extern "C"  VersionMgr_t1322950208 * PluginQuick_ilo_get_Instance4_m508038191 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::ilo_OpenUserLogin5(PluginQuick)
extern "C"  void PluginQuick_ilo_OpenUserLogin5_m2685918641 (Il2CppObject * __this /* static, unused */, PluginQuick_t2552428122 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::ilo_updateRoleInfo6(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginQuick_ilo_updateRoleInfo6_m1405706333 (Il2CppObject * __this /* static, unused */, String_t* ___serverName0, String_t* ___roleName1, String_t* ___serverId2, String_t* ___roleId3, String_t* ___balance4, String_t* ___vipLv5, String_t* ___roleLv6, String_t* ___partyName7, String_t* ___isCreate8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::ilo_pay7(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginQuick_ilo_pay7_m2714605205 (Il2CppObject * __this /* static, unused */, String_t* ___goodId0, String_t* ___goodName1, String_t* ___orderId2, String_t* ___count3, String_t* ___price4, String_t* ___cbUrl5, String_t* ___eArgs6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::ilo_ReqSDKHttpLogin8(PluginsSdkMgr)
extern "C"  void PluginQuick_ilo_ReqSDKHttpLogin8_m118405883 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::ilo_Logout9(System.Action)
extern "C"  void PluginQuick_ilo_Logout9_m3286907666 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr PluginQuick::ilo_get_FloatTextMgr10()
extern "C"  FloatTextMgr_t630384591 * PluginQuick_ilo_get_FloatTextMgr10_m544012239 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::ilo_DispatchEvent11(CEvent.ZEvent)
extern "C"  void PluginQuick_ilo_DispatchEvent11_m3960836223 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQuick::ilo_FloatText12(FloatTextMgr,FLOAT_TEXT_ID,System.Int32,System.Object[])
extern "C"  void PluginQuick_ilo_FloatText12_m1306688042 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, int32_t ___textId1, int32_t ___offsetY2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
