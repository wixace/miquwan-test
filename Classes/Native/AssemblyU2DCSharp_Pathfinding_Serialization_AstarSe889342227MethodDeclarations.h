﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Serialization.AstarSerializer/<SerializeExtraInfo>c__AnonStorey10C
struct U3CSerializeExtraInfoU3Ec__AnonStorey10C_t889342227;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.Serialization.AstarSerializer/<SerializeExtraInfo>c__AnonStorey10C::.ctor()
extern "C"  void U3CSerializeExtraInfoU3Ec__AnonStorey10C__ctor_m333203688 (U3CSerializeExtraInfoU3Ec__AnonStorey10C_t889342227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Serialization.AstarSerializer/<SerializeExtraInfo>c__AnonStorey10C::<>m__33F(Pathfinding.GraphNode)
extern "C"  bool U3CSerializeExtraInfoU3Ec__AnonStorey10C_U3CU3Em__33F_m3960000353 (U3CSerializeExtraInfoU3Ec__AnonStorey10C_t889342227 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
