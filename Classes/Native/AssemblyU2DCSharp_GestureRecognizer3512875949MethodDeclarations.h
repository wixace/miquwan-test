﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GestureRecognizer
struct GestureRecognizer_t3512875949;
// FingerGestures/Finger
struct Finger_t182428197;
// FingerGestures/IFingerList
struct IFingerList_t1026994100;
// System.String
struct String_t;
// FingerGestures
struct FingerGestures_t2907604723;
// FingerClusterManager
struct FingerClusterManager_t3376029756;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GestureResetMode2954327145.h"
#include "AssemblyU2DCSharp_FingerGestures_Finger182428197.h"
#include "AssemblyU2DCSharp_GestureRecognizer3512875949.h"

// System.Void GestureRecognizer::.ctor()
extern "C"  void GestureRecognizer__ctor_m929383518 (GestureRecognizer_t3512875949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GestureRecognizer::.cctor()
extern "C"  void GestureRecognizer__cctor_m2558989071 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GestureRecognizer::get_RequiredFingerCount()
extern "C"  int32_t GestureRecognizer_get_RequiredFingerCount_m3358385640 (GestureRecognizer_t3512875949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GestureRecognizer::set_RequiredFingerCount(System.Int32)
extern "C"  void GestureRecognizer_set_RequiredFingerCount_m299452727 (GestureRecognizer_t3512875949 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GestureRecognizer::get_SupportFingerClustering()
extern "C"  bool GestureRecognizer_get_SupportFingerClustering_m2139587623 (GestureRecognizer_t3512875949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GestureResetMode GestureRecognizer::GetDefaultResetMode()
extern "C"  int32_t GestureRecognizer_GetDefaultResetMode_m347079351 (GestureRecognizer_t3512875949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GestureRecognizer::Awake()
extern "C"  void GestureRecognizer_Awake_m1166988737 (GestureRecognizer_t3512875949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GestureRecognizer::OnEnable()
extern "C"  void GestureRecognizer_OnEnable_m3976773224 (GestureRecognizer_t3512875949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GestureRecognizer::OnDisable()
extern "C"  void GestureRecognizer_OnDisable_m3461822917 (GestureRecognizer_t3512875949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GestureRecognizer::Acquire(FingerGestures/Finger)
extern "C"  void GestureRecognizer_Acquire_m3930661979 (GestureRecognizer_t3512875949 * __this, Finger_t182428197 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GestureRecognizer::Release(FingerGestures/Finger)
extern "C"  bool GestureRecognizer_Release_m3368580566 (GestureRecognizer_t3512875949 * __this, Finger_t182428197 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GestureRecognizer::Start()
extern "C"  void GestureRecognizer_Start_m4171488606 (GestureRecognizer_t3512875949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GestureRecognizer::Young(FingerGestures/IFingerList)
extern "C"  bool GestureRecognizer_Young_m1506319700 (GestureRecognizer_t3512875949 * __this, Il2CppObject * ___touches0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GestureRecognizer::ToPixels(System.Single)
extern "C"  float GestureRecognizer_ToPixels_m928444553 (GestureRecognizer_t3512875949 * __this, float ___distance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GestureRecognizer::ToSqrPixels(System.Single)
extern "C"  float GestureRecognizer_ToSqrPixels_m889397885 (GestureRecognizer_t3512875949 * __this, float ___distance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GestureRecognizer::ilo_GetDefaultEventMessageName1(GestureRecognizer)
extern "C"  String_t* GestureRecognizer_ilo_GetDefaultEventMessageName1_m777134763 (Il2CppObject * __this /* static, unused */, GestureRecognizer_t3512875949 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GestureResetMode GestureRecognizer::ilo_GetDefaultResetMode2(GestureRecognizer)
extern "C"  int32_t GestureRecognizer_ilo_GetDefaultResetMode2_m535550453 (Il2CppObject * __this /* static, unused */, GestureRecognizer_t3512875949 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures GestureRecognizer::ilo_get_Instance3()
extern "C"  FingerGestures_t2907604723 * GestureRecognizer_ilo_get_Instance3_m1074966184 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerClusterManager GestureRecognizer::ilo_get_DefaultClusterManager4()
extern "C"  FingerClusterManager_t3376029756 * GestureRecognizer_ilo_get_DefaultClusterManager4_m3876340993 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GestureRecognizer::ilo_get_StarTime5(FingerGestures/Finger)
extern "C"  float GestureRecognizer_ilo_get_StarTime5_m98538315 (Il2CppObject * __this /* static, unused */, Finger_t182428197 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
