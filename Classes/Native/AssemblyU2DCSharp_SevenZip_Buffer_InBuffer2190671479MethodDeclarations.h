﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Buffer.InBuffer
struct InBuffer_t2190671479;
// System.IO.Stream
struct Stream_t1561764144;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_SevenZip_Buffer_InBuffer2190671479.h"

// System.Void SevenZip.Buffer.InBuffer::.ctor(System.UInt32)
extern "C"  void InBuffer__ctor_m4027529016 (InBuffer_t2190671479 * __this, uint32_t ___bufferSize0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Buffer.InBuffer::Init(System.IO.Stream)
extern "C"  void InBuffer_Init_m1428970409 (InBuffer_t2190671479 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.Buffer.InBuffer::ReadBlock()
extern "C"  bool InBuffer_ReadBlock_m4128234307 (InBuffer_t2190671479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Buffer.InBuffer::ReleaseStream()
extern "C"  void InBuffer_ReleaseStream_m2692794311 (InBuffer_t2190671479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.Buffer.InBuffer::ReadByte(System.Byte)
extern "C"  bool InBuffer_ReadByte_m2881324663 (InBuffer_t2190671479 * __this, uint8_t ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte SevenZip.Buffer.InBuffer::ReadByte()
extern "C"  uint8_t InBuffer_ReadByte_m88503500 (InBuffer_t2190671479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 SevenZip.Buffer.InBuffer::GetProcessedSize()
extern "C"  uint64_t InBuffer_GetProcessedSize_m692188823 (InBuffer_t2190671479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.Buffer.InBuffer::ilo_ReadBlock1(SevenZip.Buffer.InBuffer)
extern "C"  bool InBuffer_ilo_ReadBlock1_m2162249098 (Il2CppObject * __this /* static, unused */, InBuffer_t2190671479 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
