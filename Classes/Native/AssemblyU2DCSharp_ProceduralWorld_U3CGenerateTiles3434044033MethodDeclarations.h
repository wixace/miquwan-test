﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProceduralWorld/<GenerateTiles>c__Iterator11
struct U3CGenerateTilesU3Ec__Iterator11_t3434044033;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void ProceduralWorld/<GenerateTiles>c__Iterator11::.ctor()
extern "C"  void U3CGenerateTilesU3Ec__Iterator11__ctor_m3872487738 (U3CGenerateTilesU3Ec__Iterator11_t3434044033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProceduralWorld/<GenerateTiles>c__Iterator11::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CGenerateTilesU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m645704034 (U3CGenerateTilesU3Ec__Iterator11_t3434044033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProceduralWorld/<GenerateTiles>c__Iterator11::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGenerateTilesU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m2510401782 (U3CGenerateTilesU3Ec__Iterator11_t3434044033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProceduralWorld/<GenerateTiles>c__Iterator11::MoveNext()
extern "C"  bool U3CGenerateTilesU3Ec__Iterator11_MoveNext_m1891712098 (U3CGenerateTilesU3Ec__Iterator11_t3434044033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProceduralWorld/<GenerateTiles>c__Iterator11::Dispose()
extern "C"  void U3CGenerateTilesU3Ec__Iterator11_Dispose_m1917320951 (U3CGenerateTilesU3Ec__Iterator11_t3434044033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProceduralWorld/<GenerateTiles>c__Iterator11::Reset()
extern "C"  void U3CGenerateTilesU3Ec__Iterator11_Reset_m1518920679 (U3CGenerateTilesU3Ec__Iterator11_t3434044033 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
