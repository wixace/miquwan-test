﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PetsCfg
struct  PetsCfg_t988016752  : public CsCfgBase_t69924517
{
public:
	// System.Int32 PetsCfg::id
	int32_t ___id_0;
	// System.String PetsCfg::name
	String_t* ___name_1;
	// System.String PetsCfg::resName
	String_t* ___resName_2;
	// System.String PetsCfg::icon
	String_t* ___icon_3;
	// System.Int32 PetsCfg::intel
	int32_t ___intel_4;
	// System.Int32 PetsCfg::quatily
	int32_t ___quatily_5;
	// System.Int32 PetsCfg::star_lv
	int32_t ___star_lv_6;
	// System.Int32 PetsCfg::star_start_id
	int32_t ___star_start_id_7;
	// System.Int32 PetsCfg::upgrade_start_id
	int32_t ___upgrade_start_id_8;
	// System.Int32 PetsCfg::train_level
	int32_t ___train_level_9;
	// System.Single PetsCfg::scale
	float ___scale_10;
	// System.Single PetsCfg::scale_team
	float ___scale_team_11;
	// System.Int32 PetsCfg::compose_chipId
	int32_t ___compose_chipId_12;
	// System.Int32 PetsCfg::compose_chip_num
	int32_t ___compose_chip_num_13;
	// System.Int32 PetsCfg::level
	int32_t ___level_14;
	// System.Int32 PetsCfg::attack
	int32_t ___attack_15;
	// System.Int32 PetsCfg::hp
	int32_t ___hp_16;
	// System.Int32 PetsCfg::def_phy
	int32_t ___def_phy_17;
	// System.Int32 PetsCfg::def_mag
	int32_t ___def_mag_18;
	// System.String PetsCfg::talent_skill
	String_t* ___talent_skill_19;
	// System.String PetsCfg::specail_att
	String_t* ___specail_att_20;
	// System.String PetsCfg::with_eage
	String_t* ___with_eage_21;
	// System.String PetsCfg::with_eage_att
	String_t* ___with_eage_att_22;
	// System.Int32 PetsCfg::with_eage_number
	int32_t ___with_eage_number_23;
	// System.String PetsCfg::await_animation
	String_t* ___await_animation_24;
	// System.String PetsCfg::offset_pos
	String_t* ___offset_pos_25;
	// System.String PetsCfg::offset_pos_compose
	String_t* ___offset_pos_compose_26;
	// System.String PetsCfg::offset_pos_team
	String_t* ___offset_pos_team_27;
	// System.String PetsCfg::offset_rot_team
	String_t* ___offset_rot_team_28;
	// System.String PetsCfg::offset_pos_upStar
	String_t* ___offset_pos_upStar_29;
	// System.String PetsCfg::offset_rat
	String_t* ___offset_rat_30;
	// System.String PetsCfg::tipresume
	String_t* ___tipresume_31;
	// System.String PetsCfg::des
	String_t* ___des_32;
	// System.Int32 PetsCfg::pet_resolve
	int32_t ___pet_resolve_33;
	// System.Int32 PetsCfg::propId
	int32_t ___propId_34;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_resName_2() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___resName_2)); }
	inline String_t* get_resName_2() const { return ___resName_2; }
	inline String_t** get_address_of_resName_2() { return &___resName_2; }
	inline void set_resName_2(String_t* value)
	{
		___resName_2 = value;
		Il2CppCodeGenWriteBarrier(&___resName_2, value);
	}

	inline static int32_t get_offset_of_icon_3() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___icon_3)); }
	inline String_t* get_icon_3() const { return ___icon_3; }
	inline String_t** get_address_of_icon_3() { return &___icon_3; }
	inline void set_icon_3(String_t* value)
	{
		___icon_3 = value;
		Il2CppCodeGenWriteBarrier(&___icon_3, value);
	}

	inline static int32_t get_offset_of_intel_4() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___intel_4)); }
	inline int32_t get_intel_4() const { return ___intel_4; }
	inline int32_t* get_address_of_intel_4() { return &___intel_4; }
	inline void set_intel_4(int32_t value)
	{
		___intel_4 = value;
	}

	inline static int32_t get_offset_of_quatily_5() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___quatily_5)); }
	inline int32_t get_quatily_5() const { return ___quatily_5; }
	inline int32_t* get_address_of_quatily_5() { return &___quatily_5; }
	inline void set_quatily_5(int32_t value)
	{
		___quatily_5 = value;
	}

	inline static int32_t get_offset_of_star_lv_6() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___star_lv_6)); }
	inline int32_t get_star_lv_6() const { return ___star_lv_6; }
	inline int32_t* get_address_of_star_lv_6() { return &___star_lv_6; }
	inline void set_star_lv_6(int32_t value)
	{
		___star_lv_6 = value;
	}

	inline static int32_t get_offset_of_star_start_id_7() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___star_start_id_7)); }
	inline int32_t get_star_start_id_7() const { return ___star_start_id_7; }
	inline int32_t* get_address_of_star_start_id_7() { return &___star_start_id_7; }
	inline void set_star_start_id_7(int32_t value)
	{
		___star_start_id_7 = value;
	}

	inline static int32_t get_offset_of_upgrade_start_id_8() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___upgrade_start_id_8)); }
	inline int32_t get_upgrade_start_id_8() const { return ___upgrade_start_id_8; }
	inline int32_t* get_address_of_upgrade_start_id_8() { return &___upgrade_start_id_8; }
	inline void set_upgrade_start_id_8(int32_t value)
	{
		___upgrade_start_id_8 = value;
	}

	inline static int32_t get_offset_of_train_level_9() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___train_level_9)); }
	inline int32_t get_train_level_9() const { return ___train_level_9; }
	inline int32_t* get_address_of_train_level_9() { return &___train_level_9; }
	inline void set_train_level_9(int32_t value)
	{
		___train_level_9 = value;
	}

	inline static int32_t get_offset_of_scale_10() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___scale_10)); }
	inline float get_scale_10() const { return ___scale_10; }
	inline float* get_address_of_scale_10() { return &___scale_10; }
	inline void set_scale_10(float value)
	{
		___scale_10 = value;
	}

	inline static int32_t get_offset_of_scale_team_11() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___scale_team_11)); }
	inline float get_scale_team_11() const { return ___scale_team_11; }
	inline float* get_address_of_scale_team_11() { return &___scale_team_11; }
	inline void set_scale_team_11(float value)
	{
		___scale_team_11 = value;
	}

	inline static int32_t get_offset_of_compose_chipId_12() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___compose_chipId_12)); }
	inline int32_t get_compose_chipId_12() const { return ___compose_chipId_12; }
	inline int32_t* get_address_of_compose_chipId_12() { return &___compose_chipId_12; }
	inline void set_compose_chipId_12(int32_t value)
	{
		___compose_chipId_12 = value;
	}

	inline static int32_t get_offset_of_compose_chip_num_13() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___compose_chip_num_13)); }
	inline int32_t get_compose_chip_num_13() const { return ___compose_chip_num_13; }
	inline int32_t* get_address_of_compose_chip_num_13() { return &___compose_chip_num_13; }
	inline void set_compose_chip_num_13(int32_t value)
	{
		___compose_chip_num_13 = value;
	}

	inline static int32_t get_offset_of_level_14() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___level_14)); }
	inline int32_t get_level_14() const { return ___level_14; }
	inline int32_t* get_address_of_level_14() { return &___level_14; }
	inline void set_level_14(int32_t value)
	{
		___level_14 = value;
	}

	inline static int32_t get_offset_of_attack_15() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___attack_15)); }
	inline int32_t get_attack_15() const { return ___attack_15; }
	inline int32_t* get_address_of_attack_15() { return &___attack_15; }
	inline void set_attack_15(int32_t value)
	{
		___attack_15 = value;
	}

	inline static int32_t get_offset_of_hp_16() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___hp_16)); }
	inline int32_t get_hp_16() const { return ___hp_16; }
	inline int32_t* get_address_of_hp_16() { return &___hp_16; }
	inline void set_hp_16(int32_t value)
	{
		___hp_16 = value;
	}

	inline static int32_t get_offset_of_def_phy_17() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___def_phy_17)); }
	inline int32_t get_def_phy_17() const { return ___def_phy_17; }
	inline int32_t* get_address_of_def_phy_17() { return &___def_phy_17; }
	inline void set_def_phy_17(int32_t value)
	{
		___def_phy_17 = value;
	}

	inline static int32_t get_offset_of_def_mag_18() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___def_mag_18)); }
	inline int32_t get_def_mag_18() const { return ___def_mag_18; }
	inline int32_t* get_address_of_def_mag_18() { return &___def_mag_18; }
	inline void set_def_mag_18(int32_t value)
	{
		___def_mag_18 = value;
	}

	inline static int32_t get_offset_of_talent_skill_19() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___talent_skill_19)); }
	inline String_t* get_talent_skill_19() const { return ___talent_skill_19; }
	inline String_t** get_address_of_talent_skill_19() { return &___talent_skill_19; }
	inline void set_talent_skill_19(String_t* value)
	{
		___talent_skill_19 = value;
		Il2CppCodeGenWriteBarrier(&___talent_skill_19, value);
	}

	inline static int32_t get_offset_of_specail_att_20() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___specail_att_20)); }
	inline String_t* get_specail_att_20() const { return ___specail_att_20; }
	inline String_t** get_address_of_specail_att_20() { return &___specail_att_20; }
	inline void set_specail_att_20(String_t* value)
	{
		___specail_att_20 = value;
		Il2CppCodeGenWriteBarrier(&___specail_att_20, value);
	}

	inline static int32_t get_offset_of_with_eage_21() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___with_eage_21)); }
	inline String_t* get_with_eage_21() const { return ___with_eage_21; }
	inline String_t** get_address_of_with_eage_21() { return &___with_eage_21; }
	inline void set_with_eage_21(String_t* value)
	{
		___with_eage_21 = value;
		Il2CppCodeGenWriteBarrier(&___with_eage_21, value);
	}

	inline static int32_t get_offset_of_with_eage_att_22() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___with_eage_att_22)); }
	inline String_t* get_with_eage_att_22() const { return ___with_eage_att_22; }
	inline String_t** get_address_of_with_eage_att_22() { return &___with_eage_att_22; }
	inline void set_with_eage_att_22(String_t* value)
	{
		___with_eage_att_22 = value;
		Il2CppCodeGenWriteBarrier(&___with_eage_att_22, value);
	}

	inline static int32_t get_offset_of_with_eage_number_23() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___with_eage_number_23)); }
	inline int32_t get_with_eage_number_23() const { return ___with_eage_number_23; }
	inline int32_t* get_address_of_with_eage_number_23() { return &___with_eage_number_23; }
	inline void set_with_eage_number_23(int32_t value)
	{
		___with_eage_number_23 = value;
	}

	inline static int32_t get_offset_of_await_animation_24() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___await_animation_24)); }
	inline String_t* get_await_animation_24() const { return ___await_animation_24; }
	inline String_t** get_address_of_await_animation_24() { return &___await_animation_24; }
	inline void set_await_animation_24(String_t* value)
	{
		___await_animation_24 = value;
		Il2CppCodeGenWriteBarrier(&___await_animation_24, value);
	}

	inline static int32_t get_offset_of_offset_pos_25() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___offset_pos_25)); }
	inline String_t* get_offset_pos_25() const { return ___offset_pos_25; }
	inline String_t** get_address_of_offset_pos_25() { return &___offset_pos_25; }
	inline void set_offset_pos_25(String_t* value)
	{
		___offset_pos_25 = value;
		Il2CppCodeGenWriteBarrier(&___offset_pos_25, value);
	}

	inline static int32_t get_offset_of_offset_pos_compose_26() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___offset_pos_compose_26)); }
	inline String_t* get_offset_pos_compose_26() const { return ___offset_pos_compose_26; }
	inline String_t** get_address_of_offset_pos_compose_26() { return &___offset_pos_compose_26; }
	inline void set_offset_pos_compose_26(String_t* value)
	{
		___offset_pos_compose_26 = value;
		Il2CppCodeGenWriteBarrier(&___offset_pos_compose_26, value);
	}

	inline static int32_t get_offset_of_offset_pos_team_27() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___offset_pos_team_27)); }
	inline String_t* get_offset_pos_team_27() const { return ___offset_pos_team_27; }
	inline String_t** get_address_of_offset_pos_team_27() { return &___offset_pos_team_27; }
	inline void set_offset_pos_team_27(String_t* value)
	{
		___offset_pos_team_27 = value;
		Il2CppCodeGenWriteBarrier(&___offset_pos_team_27, value);
	}

	inline static int32_t get_offset_of_offset_rot_team_28() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___offset_rot_team_28)); }
	inline String_t* get_offset_rot_team_28() const { return ___offset_rot_team_28; }
	inline String_t** get_address_of_offset_rot_team_28() { return &___offset_rot_team_28; }
	inline void set_offset_rot_team_28(String_t* value)
	{
		___offset_rot_team_28 = value;
		Il2CppCodeGenWriteBarrier(&___offset_rot_team_28, value);
	}

	inline static int32_t get_offset_of_offset_pos_upStar_29() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___offset_pos_upStar_29)); }
	inline String_t* get_offset_pos_upStar_29() const { return ___offset_pos_upStar_29; }
	inline String_t** get_address_of_offset_pos_upStar_29() { return &___offset_pos_upStar_29; }
	inline void set_offset_pos_upStar_29(String_t* value)
	{
		___offset_pos_upStar_29 = value;
		Il2CppCodeGenWriteBarrier(&___offset_pos_upStar_29, value);
	}

	inline static int32_t get_offset_of_offset_rat_30() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___offset_rat_30)); }
	inline String_t* get_offset_rat_30() const { return ___offset_rat_30; }
	inline String_t** get_address_of_offset_rat_30() { return &___offset_rat_30; }
	inline void set_offset_rat_30(String_t* value)
	{
		___offset_rat_30 = value;
		Il2CppCodeGenWriteBarrier(&___offset_rat_30, value);
	}

	inline static int32_t get_offset_of_tipresume_31() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___tipresume_31)); }
	inline String_t* get_tipresume_31() const { return ___tipresume_31; }
	inline String_t** get_address_of_tipresume_31() { return &___tipresume_31; }
	inline void set_tipresume_31(String_t* value)
	{
		___tipresume_31 = value;
		Il2CppCodeGenWriteBarrier(&___tipresume_31, value);
	}

	inline static int32_t get_offset_of_des_32() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___des_32)); }
	inline String_t* get_des_32() const { return ___des_32; }
	inline String_t** get_address_of_des_32() { return &___des_32; }
	inline void set_des_32(String_t* value)
	{
		___des_32 = value;
		Il2CppCodeGenWriteBarrier(&___des_32, value);
	}

	inline static int32_t get_offset_of_pet_resolve_33() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___pet_resolve_33)); }
	inline int32_t get_pet_resolve_33() const { return ___pet_resolve_33; }
	inline int32_t* get_address_of_pet_resolve_33() { return &___pet_resolve_33; }
	inline void set_pet_resolve_33(int32_t value)
	{
		___pet_resolve_33 = value;
	}

	inline static int32_t get_offset_of_propId_34() { return static_cast<int32_t>(offsetof(PetsCfg_t988016752, ___propId_34)); }
	inline int32_t get_propId_34() const { return ___propId_34; }
	inline int32_t* get_address_of_propId_34() { return &___propId_34; }
	inline void set_propId_34(int32_t value)
	{
		___propId_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
