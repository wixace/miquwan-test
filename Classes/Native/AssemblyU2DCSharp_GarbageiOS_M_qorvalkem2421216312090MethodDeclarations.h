﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_qorvalkem242
struct M_qorvalkem242_t1216312090;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_qorvalkem2421216312090.h"

// System.Void GarbageiOS.M_qorvalkem242::.ctor()
extern "C"  void M_qorvalkem242__ctor_m1436851289 (M_qorvalkem242_t1216312090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_qorvalkem242::M_debowfaw0(System.String[],System.Int32)
extern "C"  void M_qorvalkem242_M_debowfaw0_m203265753 (M_qorvalkem242_t1216312090 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_qorvalkem242::M_baroja1(System.String[],System.Int32)
extern "C"  void M_qorvalkem242_M_baroja1_m4064618490 (M_qorvalkem242_t1216312090 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_qorvalkem242::M_hejoo2(System.String[],System.Int32)
extern "C"  void M_qorvalkem242_M_hejoo2_m1746088825 (M_qorvalkem242_t1216312090 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_qorvalkem242::M_fomar3(System.String[],System.Int32)
extern "C"  void M_qorvalkem242_M_fomar3_m119065714 (M_qorvalkem242_t1216312090 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_qorvalkem242::M_sairjalmaFisto4(System.String[],System.Int32)
extern "C"  void M_qorvalkem242_M_sairjalmaFisto4_m142996503 (M_qorvalkem242_t1216312090 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_qorvalkem242::M_rirxaiweJorjear5(System.String[],System.Int32)
extern "C"  void M_qorvalkem242_M_rirxaiweJorjear5_m2340360323 (M_qorvalkem242_t1216312090 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_qorvalkem242::ilo_M_fomar31(GarbageiOS.M_qorvalkem242,System.String[],System.Int32)
extern "C"  void M_qorvalkem242_ilo_M_fomar31_m798847554 (Il2CppObject * __this /* static, unused */, M_qorvalkem242_t1216312090 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
