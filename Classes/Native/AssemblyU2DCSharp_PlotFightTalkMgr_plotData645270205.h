﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// StoryCfg
struct StoryCfg_t1782371151;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlotFightTalkMgr/plotData
struct  plotData_t645270205  : public Il2CppObject
{
public:
	// System.Int32 PlotFightTalkMgr/plotData::term
	int32_t ___term_0;
	// StoryCfg PlotFightTalkMgr/plotData::cfg
	StoryCfg_t1782371151 * ___cfg_1;
	// System.Int32 PlotFightTalkMgr/plotData::<curCount>k__BackingField
	int32_t ___U3CcurCountU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_term_0() { return static_cast<int32_t>(offsetof(plotData_t645270205, ___term_0)); }
	inline int32_t get_term_0() const { return ___term_0; }
	inline int32_t* get_address_of_term_0() { return &___term_0; }
	inline void set_term_0(int32_t value)
	{
		___term_0 = value;
	}

	inline static int32_t get_offset_of_cfg_1() { return static_cast<int32_t>(offsetof(plotData_t645270205, ___cfg_1)); }
	inline StoryCfg_t1782371151 * get_cfg_1() const { return ___cfg_1; }
	inline StoryCfg_t1782371151 ** get_address_of_cfg_1() { return &___cfg_1; }
	inline void set_cfg_1(StoryCfg_t1782371151 * value)
	{
		___cfg_1 = value;
		Il2CppCodeGenWriteBarrier(&___cfg_1, value);
	}

	inline static int32_t get_offset_of_U3CcurCountU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(plotData_t645270205, ___U3CcurCountU3Ek__BackingField_2)); }
	inline int32_t get_U3CcurCountU3Ek__BackingField_2() const { return ___U3CcurCountU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CcurCountU3Ek__BackingField_2() { return &___U3CcurCountU3Ek__BackingField_2; }
	inline void set_U3CcurCountU3Ek__BackingField_2(int32_t value)
	{
		___U3CcurCountU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
