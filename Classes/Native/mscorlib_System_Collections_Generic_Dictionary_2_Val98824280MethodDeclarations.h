﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Single>
struct Dictionary_2_t2166990872;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Val98824280.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3906139946_gshared (Enumerator_t98824280 * __this, Dictionary_2_t2166990872 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3906139946(__this, ___host0, method) ((  void (*) (Enumerator_t98824280 *, Dictionary_2_t2166990872 *, const MethodInfo*))Enumerator__ctor_m3906139946_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3123287735_gshared (Enumerator_t98824280 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3123287735(__this, method) ((  Il2CppObject * (*) (Enumerator_t98824280 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3123287735_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Single>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1435710283_gshared (Enumerator_t98824280 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1435710283(__this, method) ((  void (*) (Enumerator_t98824280 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1435710283_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Single>::Dispose()
extern "C"  void Enumerator_Dispose_m3642244172_gshared (Enumerator_t98824280 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3642244172(__this, method) ((  void (*) (Enumerator_t98824280 *, const MethodInfo*))Enumerator_Dispose_m3642244172_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Single>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1562675895_gshared (Enumerator_t98824280 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1562675895(__this, method) ((  bool (*) (Enumerator_t98824280 *, const MethodInfo*))Enumerator_MoveNext_m1562675895_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Single>::get_Current()
extern "C"  float Enumerator_get_Current_m225874027_gshared (Enumerator_t98824280 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m225874027(__this, method) ((  float (*) (Enumerator_t98824280 *, const MethodInfo*))Enumerator_get_Current_m225874027_gshared)(__this, method)
