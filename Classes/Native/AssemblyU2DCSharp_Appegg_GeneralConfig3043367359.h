﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Appegg_HybridType3509539841.h"
#include "AssemblyU2DCSharp_Appegg_AppType99119152.h"
#include "AssemblyU2DCSharp_Appegg_DataPath3703791194.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Appegg/GeneralConfig
struct  GeneralConfig_t3043367359  : public Il2CppObject
{
public:
	// Appegg/HybridType Appegg/GeneralConfig::HybridType
	int32_t ___HybridType_0;
	// Appegg/AppType Appegg/GeneralConfig::AppType
	int32_t ___AppType_1;
	// Appegg/DataPath Appegg/GeneralConfig::DataPath
	int32_t ___DataPath_2;

public:
	inline static int32_t get_offset_of_HybridType_0() { return static_cast<int32_t>(offsetof(GeneralConfig_t3043367359, ___HybridType_0)); }
	inline int32_t get_HybridType_0() const { return ___HybridType_0; }
	inline int32_t* get_address_of_HybridType_0() { return &___HybridType_0; }
	inline void set_HybridType_0(int32_t value)
	{
		___HybridType_0 = value;
	}

	inline static int32_t get_offset_of_AppType_1() { return static_cast<int32_t>(offsetof(GeneralConfig_t3043367359, ___AppType_1)); }
	inline int32_t get_AppType_1() const { return ___AppType_1; }
	inline int32_t* get_address_of_AppType_1() { return &___AppType_1; }
	inline void set_AppType_1(int32_t value)
	{
		___AppType_1 = value;
	}

	inline static int32_t get_offset_of_DataPath_2() { return static_cast<int32_t>(offsetof(GeneralConfig_t3043367359, ___DataPath_2)); }
	inline int32_t get_DataPath_2() const { return ___DataPath_2; }
	inline int32_t* get_address_of_DataPath_2() { return &___DataPath_2; }
	inline void set_DataPath_2(int32_t value)
	{
		___DataPath_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
