﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// RVOExampleAgent
struct RVOExampleAgent_t1174908390;
// System.Object
struct Il2CppObject;
// RVOAgentPlacer
struct RVOAgentPlacer_t2570441765;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RVOAgentPlacer/<Start>c__IteratorF
struct  U3CStartU3Ec__IteratorF_t3884464099  : public Il2CppObject
{
public:
	// System.Int32 RVOAgentPlacer/<Start>c__IteratorF::<i>__0
	int32_t ___U3CiU3E__0_0;
	// System.Single RVOAgentPlacer/<Start>c__IteratorF::<angle>__1
	float ___U3CangleU3E__1_1;
	// UnityEngine.Vector3 RVOAgentPlacer/<Start>c__IteratorF::<pos>__2
	Vector3_t4282066566  ___U3CposU3E__2_2;
	// UnityEngine.Vector3 RVOAgentPlacer/<Start>c__IteratorF::<antipodal>__3
	Vector3_t4282066566  ___U3CantipodalU3E__3_3;
	// UnityEngine.GameObject RVOAgentPlacer/<Start>c__IteratorF::<go>__4
	GameObject_t3674682005 * ___U3CgoU3E__4_4;
	// RVOExampleAgent RVOAgentPlacer/<Start>c__IteratorF::<ag>__5
	RVOExampleAgent_t1174908390 * ___U3CagU3E__5_5;
	// System.Int32 RVOAgentPlacer/<Start>c__IteratorF::$PC
	int32_t ___U24PC_6;
	// System.Object RVOAgentPlacer/<Start>c__IteratorF::$current
	Il2CppObject * ___U24current_7;
	// RVOAgentPlacer RVOAgentPlacer/<Start>c__IteratorF::<>f__this
	RVOAgentPlacer_t2570441765 * ___U3CU3Ef__this_8;

public:
	inline static int32_t get_offset_of_U3CiU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__IteratorF_t3884464099, ___U3CiU3E__0_0)); }
	inline int32_t get_U3CiU3E__0_0() const { return ___U3CiU3E__0_0; }
	inline int32_t* get_address_of_U3CiU3E__0_0() { return &___U3CiU3E__0_0; }
	inline void set_U3CiU3E__0_0(int32_t value)
	{
		___U3CiU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CangleU3E__1_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__IteratorF_t3884464099, ___U3CangleU3E__1_1)); }
	inline float get_U3CangleU3E__1_1() const { return ___U3CangleU3E__1_1; }
	inline float* get_address_of_U3CangleU3E__1_1() { return &___U3CangleU3E__1_1; }
	inline void set_U3CangleU3E__1_1(float value)
	{
		___U3CangleU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CposU3E__2_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__IteratorF_t3884464099, ___U3CposU3E__2_2)); }
	inline Vector3_t4282066566  get_U3CposU3E__2_2() const { return ___U3CposU3E__2_2; }
	inline Vector3_t4282066566 * get_address_of_U3CposU3E__2_2() { return &___U3CposU3E__2_2; }
	inline void set_U3CposU3E__2_2(Vector3_t4282066566  value)
	{
		___U3CposU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CantipodalU3E__3_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__IteratorF_t3884464099, ___U3CantipodalU3E__3_3)); }
	inline Vector3_t4282066566  get_U3CantipodalU3E__3_3() const { return ___U3CantipodalU3E__3_3; }
	inline Vector3_t4282066566 * get_address_of_U3CantipodalU3E__3_3() { return &___U3CantipodalU3E__3_3; }
	inline void set_U3CantipodalU3E__3_3(Vector3_t4282066566  value)
	{
		___U3CantipodalU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U3CgoU3E__4_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__IteratorF_t3884464099, ___U3CgoU3E__4_4)); }
	inline GameObject_t3674682005 * get_U3CgoU3E__4_4() const { return ___U3CgoU3E__4_4; }
	inline GameObject_t3674682005 ** get_address_of_U3CgoU3E__4_4() { return &___U3CgoU3E__4_4; }
	inline void set_U3CgoU3E__4_4(GameObject_t3674682005 * value)
	{
		___U3CgoU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CgoU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U3CagU3E__5_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__IteratorF_t3884464099, ___U3CagU3E__5_5)); }
	inline RVOExampleAgent_t1174908390 * get_U3CagU3E__5_5() const { return ___U3CagU3E__5_5; }
	inline RVOExampleAgent_t1174908390 ** get_address_of_U3CagU3E__5_5() { return &___U3CagU3E__5_5; }
	inline void set_U3CagU3E__5_5(RVOExampleAgent_t1174908390 * value)
	{
		___U3CagU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CagU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__IteratorF_t3884464099, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__IteratorF_t3884464099, ___U24current_7)); }
	inline Il2CppObject * get_U24current_7() const { return ___U24current_7; }
	inline Il2CppObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Il2CppObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_8() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__IteratorF_t3884464099, ___U3CU3Ef__this_8)); }
	inline RVOAgentPlacer_t2570441765 * get_U3CU3Ef__this_8() const { return ___U3CU3Ef__this_8; }
	inline RVOAgentPlacer_t2570441765 ** get_address_of_U3CU3Ef__this_8() { return &___U3CU3Ef__this_8; }
	inline void set_U3CU3Ef__this_8(RVOAgentPlacer_t2570441765 * value)
	{
		___U3CU3Ef__this_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
