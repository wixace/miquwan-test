﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_PackageType3964380262.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pomelo.DotNetClient.Package
struct  Package_t3219283596  : public Il2CppObject
{
public:
	// Pomelo.DotNetClient.PackageType Pomelo.DotNetClient.Package::type
	int32_t ___type_0;
	// System.Int32 Pomelo.DotNetClient.Package::length
	int32_t ___length_1;
	// System.Byte[] Pomelo.DotNetClient.Package::body
	ByteU5BU5D_t4260760469* ___body_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(Package_t3219283596, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(Package_t3219283596, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}

	inline static int32_t get_offset_of_body_2() { return static_cast<int32_t>(offsetof(Package_t3219283596, ___body_2)); }
	inline ByteU5BU5D_t4260760469* get_body_2() const { return ___body_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_body_2() { return &___body_2; }
	inline void set_body_2(ByteU5BU5D_t4260760469* value)
	{
		___body_2 = value;
		Il2CppCodeGenWriteBarrier(&___body_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
