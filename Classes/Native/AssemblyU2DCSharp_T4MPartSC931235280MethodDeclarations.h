﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// T4MPartSC
struct T4MPartSC_t931235280;

#include "codegen/il2cpp-codegen.h"

// System.Void T4MPartSC::.ctor()
extern "C"  void T4MPartSC__ctor_m3391822235 (T4MPartSC_t931235280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
