﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Vector4Generated
struct UnityEngine_Vector4Generated_t2291685030;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine_Vector4Generated::.ctor()
extern "C"  void UnityEngine_Vector4Generated__ctor_m3383264949 (UnityEngine_Vector4Generated_t2291685030 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector4Generated::.cctor()
extern "C"  void UnityEngine_Vector4Generated__cctor_m1319902104 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_Vector41(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_Vector41_m4115626625 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_Vector42(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_Vector42_m1065423810 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_Vector43(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_Vector43_m2310188291 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_Vector44(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_Vector44_m3554952772 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector4Generated::Vector4_kEpsilon(JSVCall)
extern "C"  void UnityEngine_Vector4Generated_Vector4_kEpsilon_m83973323 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector4Generated::Vector4_x(JSVCall)
extern "C"  void UnityEngine_Vector4Generated_Vector4_x_m4235868074 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector4Generated::Vector4_y(JSVCall)
extern "C"  void UnityEngine_Vector4Generated_Vector4_y_m4039354569 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector4Generated::Vector4_z(JSVCall)
extern "C"  void UnityEngine_Vector4Generated_Vector4_z_m3842841064 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector4Generated::Vector4_w(JSVCall)
extern "C"  void UnityEngine_Vector4Generated_Vector4_w_m137414283 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector4Generated::Vector4_Item_Int32(JSVCall)
extern "C"  void UnityEngine_Vector4Generated_Vector4_Item_Int32_m2641246536 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector4Generated::Vector4_normalized(JSVCall)
extern "C"  void UnityEngine_Vector4Generated_Vector4_normalized_m2122079315 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector4Generated::Vector4_magnitude(JSVCall)
extern "C"  void UnityEngine_Vector4Generated_Vector4_magnitude_m1985402130 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector4Generated::Vector4_sqrMagnitude(JSVCall)
extern "C"  void UnityEngine_Vector4Generated_Vector4_sqrMagnitude_m1068383566 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector4Generated::Vector4_zero(JSVCall)
extern "C"  void UnityEngine_Vector4Generated_Vector4_zero_m3880137410 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector4Generated::Vector4_one(JSVCall)
extern "C"  void UnityEngine_Vector4Generated_Vector4_one_m1864359548 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_Equals__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_Equals__Object_m1768412415 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_GetHashCode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_GetHashCode_m1686059890 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_Normalize(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_Normalize_m215470062 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_Scale__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_Scale__Vector4_m1456784776 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_Set__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_Set__Single__Single__Single__Single_m1857027235 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_SqrMagnitude(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_SqrMagnitude_m1756185885 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_ToString__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_ToString__String_m1963463230 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_ToString_m2729349357 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_Distance__Vector4__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_Distance__Vector4__Vector4_m2438701462 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_Dot__Vector4__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_Dot__Vector4__Vector4_m2996792330 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_Lerp__Vector4__Vector4__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_Lerp__Vector4__Vector4__Single_m2593684576 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_LerpUnclamped__Vector4__Vector4__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_LerpUnclamped__Vector4__Vector4__Single_m1908137011 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_Magnitude__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_Magnitude__Vector4_m4019923970 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_Max__Vector4__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_Max__Vector4__Vector4_m2363757861 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_Min__Vector4__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_Min__Vector4__Vector4_m1238110227 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_MoveTowards__Vector4__Vector4__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_MoveTowards__Vector4__Vector4__Single_m1430835924 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_Normalize__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_Normalize__Vector4_m2046345061 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_op_Addition__Vector4__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_op_Addition__Vector4__Vector4_m1126717851 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_op_Division__Vector4__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_op_Division__Vector4__Single_m4008217967 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_op_Equality__Vector4__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_op_Equality__Vector4__Vector4_m3051994521 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_op_Implicit__Vector3_to_Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_op_Implicit__Vector3_to_Vector4_m3475362912 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_op_Implicit__Vector4_to_Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_op_Implicit__Vector4_to_Vector3_m3256090430 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_op_Implicit__Vector2_to_Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_op_Implicit__Vector2_to_Vector4_m2449870913 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_op_Implicit__Vector4_to_Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_op_Implicit__Vector4_to_Vector2_m2011325949 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_op_Inequality__Vector4__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_op_Inequality__Vector4__Vector4_m1506879806 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_op_Multiply__Vector4__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_op_Multiply__Vector4__Single_m1867411480 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_op_Multiply__Single__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_op_Multiply__Single__Vector4_m2034812296 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_op_Subtraction__Vector4__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_op_Subtraction__Vector4__Vector4_m1044152183 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_op_UnaryNegation__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_op_UnaryNegation__Vector4_m2931413724 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_Project__Vector4__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_Project__Vector4__Vector4_m1120000474 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_Scale__Vector4__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_Scale__Vector4__Vector4_m3907876619 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::Vector4_SqrMagnitude__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_Vector4_SqrMagnitude__Vector4_m133345046 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector4Generated::__Register()
extern "C"  void UnityEngine_Vector4Generated___Register_m1127866162 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Vector4Generated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_Vector4Generated_ilo_getObject1_m803694205 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector4Generated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_Vector4Generated_ilo_addJSCSRel2_m1256557426 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Vector4Generated::ilo_attachFinalizerObject3(System.Int32)
extern "C"  bool UnityEngine_Vector4Generated_ilo_attachFinalizerObject3_m3453747788 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_Vector4Generated::ilo_getSingle4(System.Int32)
extern "C"  float UnityEngine_Vector4Generated_ilo_getSingle4_m360309333 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector4Generated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void UnityEngine_Vector4Generated_ilo_setSingle5_m3279110051 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector4Generated::ilo_changeJSObj6(System.Int32,System.Object)
extern "C"  void UnityEngine_Vector4Generated_ilo_changeJSObj6_m190222937 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Vector4Generated::ilo_setObject7(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_Vector4Generated_ilo_setObject7_m655405947 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector4Generated::ilo_setBooleanS8(System.Int32,System.Boolean)
extern "C"  void UnityEngine_Vector4Generated_ilo_setBooleanS8_m1070262975 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_Vector4Generated::ilo_getObject9(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_Vector4Generated_ilo_getObject9_m3898844706 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector4Generated::ilo_setVector3S10(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_Vector4Generated_ilo_setVector3S10_m477018152 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Vector4Generated::ilo_setVector2S11(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_Vector4Generated_ilo_setVector2S11_m569695019 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
