﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_bairbiyou51
struct  M_bairbiyou51_t716023212  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_bairbiyou51::_jege
	bool ____jege_0;
	// System.Int32 GarbageiOS.M_bairbiyou51::_lehutu
	int32_t ____lehutu_1;
	// System.Int32 GarbageiOS.M_bairbiyou51::_dijou
	int32_t ____dijou_2;
	// System.UInt32 GarbageiOS.M_bairbiyou51::_ceeserejisWallhiyai
	uint32_t ____ceeserejisWallhiyai_3;
	// System.Boolean GarbageiOS.M_bairbiyou51::_risweretalSaqowdo
	bool ____risweretalSaqowdo_4;

public:
	inline static int32_t get_offset_of__jege_0() { return static_cast<int32_t>(offsetof(M_bairbiyou51_t716023212, ____jege_0)); }
	inline bool get__jege_0() const { return ____jege_0; }
	inline bool* get_address_of__jege_0() { return &____jege_0; }
	inline void set__jege_0(bool value)
	{
		____jege_0 = value;
	}

	inline static int32_t get_offset_of__lehutu_1() { return static_cast<int32_t>(offsetof(M_bairbiyou51_t716023212, ____lehutu_1)); }
	inline int32_t get__lehutu_1() const { return ____lehutu_1; }
	inline int32_t* get_address_of__lehutu_1() { return &____lehutu_1; }
	inline void set__lehutu_1(int32_t value)
	{
		____lehutu_1 = value;
	}

	inline static int32_t get_offset_of__dijou_2() { return static_cast<int32_t>(offsetof(M_bairbiyou51_t716023212, ____dijou_2)); }
	inline int32_t get__dijou_2() const { return ____dijou_2; }
	inline int32_t* get_address_of__dijou_2() { return &____dijou_2; }
	inline void set__dijou_2(int32_t value)
	{
		____dijou_2 = value;
	}

	inline static int32_t get_offset_of__ceeserejisWallhiyai_3() { return static_cast<int32_t>(offsetof(M_bairbiyou51_t716023212, ____ceeserejisWallhiyai_3)); }
	inline uint32_t get__ceeserejisWallhiyai_3() const { return ____ceeserejisWallhiyai_3; }
	inline uint32_t* get_address_of__ceeserejisWallhiyai_3() { return &____ceeserejisWallhiyai_3; }
	inline void set__ceeserejisWallhiyai_3(uint32_t value)
	{
		____ceeserejisWallhiyai_3 = value;
	}

	inline static int32_t get_offset_of__risweretalSaqowdo_4() { return static_cast<int32_t>(offsetof(M_bairbiyou51_t716023212, ____risweretalSaqowdo_4)); }
	inline bool get__risweretalSaqowdo_4() const { return ____risweretalSaqowdo_4; }
	inline bool* get_address_of__risweretalSaqowdo_4() { return &____risweretalSaqowdo_4; }
	inline void set__risweretalSaqowdo_4(bool value)
	{
		____risweretalSaqowdo_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
