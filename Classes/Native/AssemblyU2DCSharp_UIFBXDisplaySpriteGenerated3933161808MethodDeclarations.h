﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIFBXDisplaySpriteGenerated
struct UIFBXDisplaySpriteGenerated_t3933161808;
// JSVCall
struct JSVCall_t3708497963;
// UIFBXDisplaySprite
struct UIFBXDisplaySprite_t2233392895;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_UIFBXDisplaySprite2233392895.h"
#include "AssemblyU2DCSharp_UIModelDisplayType1891752679.h"

// System.Void UIFBXDisplaySpriteGenerated::.ctor()
extern "C"  void UIFBXDisplaySpriteGenerated__ctor_m731831323 (UIFBXDisplaySpriteGenerated_t3933161808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIFBXDisplaySpriteGenerated::UIFBXDisplaySprite_UIFBXDisplaySprite1(JSVCall,System.Int32)
extern "C"  bool UIFBXDisplaySpriteGenerated_UIFBXDisplaySprite_UIFBXDisplaySprite1_m1116753367 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFBXDisplaySpriteGenerated::UIFBXDisplaySprite_mainTexture_(JSVCall)
extern "C"  void UIFBXDisplaySpriteGenerated_UIFBXDisplaySprite_mainTexture__m2444126225 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFBXDisplaySpriteGenerated::UIFBXDisplaySprite_mainTexture(JSVCall)
extern "C"  void UIFBXDisplaySpriteGenerated_UIFBXDisplaySprite_mainTexture_m3453862972 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIFBXDisplaySpriteGenerated::UIFBXDisplaySprite_Bind__UIModelDisplayType(JSVCall,System.Int32)
extern "C"  bool UIFBXDisplaySpriteGenerated_UIFBXDisplaySprite_Bind__UIModelDisplayType_m1646997505 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIFBXDisplaySpriteGenerated::UIFBXDisplaySprite_RefreshDrawCall(JSVCall,System.Int32)
extern "C"  bool UIFBXDisplaySpriteGenerated_UIFBXDisplaySprite_RefreshDrawCall_m2731719202 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIFBXDisplaySpriteGenerated::UIFBXDisplaySprite_UnBind(JSVCall,System.Int32)
extern "C"  bool UIFBXDisplaySpriteGenerated_UIFBXDisplaySprite_UnBind_m413888883 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIFBXDisplaySpriteGenerated::__Register()
extern "C"  void UIFBXDisplaySpriteGenerated___Register_m1694431628 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIFBXDisplaySpriteGenerated::ilo_Bind1(UIFBXDisplaySprite,UIModelDisplayType)
extern "C"  bool UIFBXDisplaySpriteGenerated_ilo_Bind1_m2575182416 (Il2CppObject * __this /* static, unused */, UIFBXDisplaySprite_t2233392895 * ____this0, int32_t ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
