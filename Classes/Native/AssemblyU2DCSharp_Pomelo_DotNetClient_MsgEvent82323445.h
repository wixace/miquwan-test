﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pomelo.DotNetClient.MsgEvent
struct  MsgEvent_t82323445  : public Il2CppObject
{
public:
	// System.String Pomelo.DotNetClient.MsgEvent::msgString
	String_t* ___msgString_0;
	// System.String Pomelo.DotNetClient.MsgEvent::route
	String_t* ___route_1;
	// System.UInt32 Pomelo.DotNetClient.MsgEvent::id
	uint32_t ___id_2;
	// System.UInt32 Pomelo.DotNetClient.MsgEvent::code
	uint32_t ___code_3;
	// System.Int32 Pomelo.DotNetClient.MsgEvent::msgType
	int32_t ___msgType_4;

public:
	inline static int32_t get_offset_of_msgString_0() { return static_cast<int32_t>(offsetof(MsgEvent_t82323445, ___msgString_0)); }
	inline String_t* get_msgString_0() const { return ___msgString_0; }
	inline String_t** get_address_of_msgString_0() { return &___msgString_0; }
	inline void set_msgString_0(String_t* value)
	{
		___msgString_0 = value;
		Il2CppCodeGenWriteBarrier(&___msgString_0, value);
	}

	inline static int32_t get_offset_of_route_1() { return static_cast<int32_t>(offsetof(MsgEvent_t82323445, ___route_1)); }
	inline String_t* get_route_1() const { return ___route_1; }
	inline String_t** get_address_of_route_1() { return &___route_1; }
	inline void set_route_1(String_t* value)
	{
		___route_1 = value;
		Il2CppCodeGenWriteBarrier(&___route_1, value);
	}

	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(MsgEvent_t82323445, ___id_2)); }
	inline uint32_t get_id_2() const { return ___id_2; }
	inline uint32_t* get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(uint32_t value)
	{
		___id_2 = value;
	}

	inline static int32_t get_offset_of_code_3() { return static_cast<int32_t>(offsetof(MsgEvent_t82323445, ___code_3)); }
	inline uint32_t get_code_3() const { return ___code_3; }
	inline uint32_t* get_address_of_code_3() { return &___code_3; }
	inline void set_code_3(uint32_t value)
	{
		___code_3 = value;
	}

	inline static int32_t get_offset_of_msgType_4() { return static_cast<int32_t>(offsetof(MsgEvent_t82323445, ___msgType_4)); }
	inline int32_t get_msgType_4() const { return ___msgType_4; }
	inline int32_t* get_address_of_msgType_4() { return &___msgType_4; }
	inline void set_msgType_4(int32_t value)
	{
		___msgType_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
