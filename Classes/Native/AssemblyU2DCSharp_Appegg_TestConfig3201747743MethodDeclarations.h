﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Appegg/TestConfig
struct TestConfig_t3201747743;

#include "codegen/il2cpp-codegen.h"

// System.Void Appegg/TestConfig::.ctor()
extern "C"  void TestConfig__ctor_m4216714540 (TestConfig_t3201747743 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
