﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.EuclideanEmbedding/<RecalculatePivots>c__AnonStorey11C
struct U3CRecalculatePivotsU3Ec__AnonStorey11C_t2905139494;
// Pathfinding.EuclideanEmbedding
struct EuclideanEmbedding_t908139023;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.EuclideanEmbedding/<RecalculatePivots>c__AnonStorey11B
struct  U3CRecalculatePivotsU3Ec__AnonStorey11B_t2905139493  : public Il2CppObject
{
public:
	// System.Int32 Pathfinding.EuclideanEmbedding/<RecalculatePivots>c__AnonStorey11B::n
	int32_t ___n_0;
	// Pathfinding.EuclideanEmbedding/<RecalculatePivots>c__AnonStorey11C Pathfinding.EuclideanEmbedding/<RecalculatePivots>c__AnonStorey11B::<>f__ref$284
	U3CRecalculatePivotsU3Ec__AnonStorey11C_t2905139494 * ___U3CU3Ef__refU24284_1;
	// Pathfinding.EuclideanEmbedding Pathfinding.EuclideanEmbedding/<RecalculatePivots>c__AnonStorey11B::<>f__this
	EuclideanEmbedding_t908139023 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_n_0() { return static_cast<int32_t>(offsetof(U3CRecalculatePivotsU3Ec__AnonStorey11B_t2905139493, ___n_0)); }
	inline int32_t get_n_0() const { return ___n_0; }
	inline int32_t* get_address_of_n_0() { return &___n_0; }
	inline void set_n_0(int32_t value)
	{
		___n_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU24284_1() { return static_cast<int32_t>(offsetof(U3CRecalculatePivotsU3Ec__AnonStorey11B_t2905139493, ___U3CU3Ef__refU24284_1)); }
	inline U3CRecalculatePivotsU3Ec__AnonStorey11C_t2905139494 * get_U3CU3Ef__refU24284_1() const { return ___U3CU3Ef__refU24284_1; }
	inline U3CRecalculatePivotsU3Ec__AnonStorey11C_t2905139494 ** get_address_of_U3CU3Ef__refU24284_1() { return &___U3CU3Ef__refU24284_1; }
	inline void set_U3CU3Ef__refU24284_1(U3CRecalculatePivotsU3Ec__AnonStorey11C_t2905139494 * value)
	{
		___U3CU3Ef__refU24284_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__refU24284_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CRecalculatePivotsU3Ec__AnonStorey11B_t2905139493, ___U3CU3Ef__this_2)); }
	inline EuclideanEmbedding_t908139023 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline EuclideanEmbedding_t908139023 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(EuclideanEmbedding_t908139023 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
