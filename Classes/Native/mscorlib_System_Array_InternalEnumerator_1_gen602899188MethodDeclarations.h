﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2953159047MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<System.Int32[][]>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m722577828(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t602899188 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2616641763_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Int32[][]>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2546942332(__this, method) ((  void (*) (InternalEnumerator_1_t602899188 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2224260061_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Int32[][]>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m975556776(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t602899188 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m390763987_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int32[][]>::Dispose()
#define InternalEnumerator_1_Dispose_m1443115131(__this, method) ((  void (*) (InternalEnumerator_1_t602899188 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2760671866_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int32[][]>::MoveNext()
#define InternalEnumerator_1_MoveNext_m32686440(__this, method) ((  bool (*) (InternalEnumerator_1_t602899188 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3716548237_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int32[][]>::get_Current()
#define InternalEnumerator_1_get_Current_m558055723(__this, method) ((  Int32U5BU5DU5BU5D_t1820556512* (*) (InternalEnumerator_1_t602899188 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2178852364_gshared)(__this, method)
