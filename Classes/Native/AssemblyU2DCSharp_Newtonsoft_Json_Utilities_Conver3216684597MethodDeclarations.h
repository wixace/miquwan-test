﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ConvertUtils/<CreateCastConverter>c__AnonStorey13A
struct U3CCreateCastConverterU3Ec__AnonStorey13A_t3216684597;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Utilities.ConvertUtils/<CreateCastConverter>c__AnonStorey13A::.ctor()
extern "C"  void U3CCreateCastConverterU3Ec__AnonStorey13A__ctor_m3432097798 (U3CCreateCastConverterU3Ec__AnonStorey13A_t3216684597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ConvertUtils/<CreateCastConverter>c__AnonStorey13A::<>m__396(System.Object)
extern "C"  Il2CppObject * U3CCreateCastConverterU3Ec__AnonStorey13A_U3CU3Em__396_m3791157724 (U3CCreateCastConverterU3Ec__AnonStorey13A_t3216684597 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
