﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuffState
struct BuffState_t2048909278;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_BuffEnum_EBuffState2159802479.h"

// System.Void BuffState::.ctor()
extern "C"  void BuffState__ctor_m3340119885 (BuffState_t2048909278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffState::Setup(System.Int32,BuffEnum.EBuffState,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void BuffState_Setup_m3277525541 (BuffState_t2048909278 * __this, int32_t ____buffID0, uint64_t ____buffState1, int32_t ____para12, int32_t ____para23, int32_t ____para34, int32_t ____para45, int32_t ____srcID6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffState::Reset()
extern "C"  void BuffState_Reset_m986552826 (BuffState_t2048909278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffState::Cleanup()
extern "C"  void BuffState_Cleanup_m4183400335 (BuffState_t2048909278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BuffState BuffState::Clone()
extern "C"  BuffState_t2048909278 * BuffState_Clone_m838352253 (BuffState_t2048909278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
