﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RVO.Line
struct Line_t2379010364;
struct Line_t2379010364_marshaled_pinvoke;
struct Line_t2379010364_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct Line_t2379010364;
struct Line_t2379010364_marshaled_pinvoke;

extern "C" void Line_t2379010364_marshal_pinvoke(const Line_t2379010364& unmarshaled, Line_t2379010364_marshaled_pinvoke& marshaled);
extern "C" void Line_t2379010364_marshal_pinvoke_back(const Line_t2379010364_marshaled_pinvoke& marshaled, Line_t2379010364& unmarshaled);
extern "C" void Line_t2379010364_marshal_pinvoke_cleanup(Line_t2379010364_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Line_t2379010364;
struct Line_t2379010364_marshaled_com;

extern "C" void Line_t2379010364_marshal_com(const Line_t2379010364& unmarshaled, Line_t2379010364_marshaled_com& marshaled);
extern "C" void Line_t2379010364_marshal_com_back(const Line_t2379010364_marshaled_com& marshaled, Line_t2379010364& unmarshaled);
extern "C" void Line_t2379010364_marshal_com_cleanup(Line_t2379010364_marshaled_com& marshaled);
