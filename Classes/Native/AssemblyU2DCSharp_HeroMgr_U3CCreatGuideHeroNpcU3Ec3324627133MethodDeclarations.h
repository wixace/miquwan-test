﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HeroMgr/<CreatGuideHeroNpc>c__AnonStorey153
struct U3CCreatGuideHeroNpcU3Ec__AnonStorey153_t3324627133;

#include "codegen/il2cpp-codegen.h"

// System.Void HeroMgr/<CreatGuideHeroNpc>c__AnonStorey153::.ctor()
extern "C"  void U3CCreatGuideHeroNpcU3Ec__AnonStorey153__ctor_m552965966 (U3CCreatGuideHeroNpcU3Ec__AnonStorey153_t3324627133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgr/<CreatGuideHeroNpc>c__AnonStorey153::<>m__3D1()
extern "C"  void U3CCreatGuideHeroNpcU3Ec__AnonStorey153_U3CU3Em__3D1_m746759691 (U3CCreatGuideHeroNpcU3Ec__AnonStorey153_t3324627133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
