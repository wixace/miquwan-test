﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>
struct ReadOnlyCollection_1_t3531123129;
// System.Collections.Generic.IList`1<Pathfinding.Int2>
struct IList_1_t373725500;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.Int2[]
struct Int2U5BU5D_t30096868;
// System.Collections.Generic.IEnumerator`1<Pathfinding.Int2>
struct IEnumerator_1_t3885910642;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m3501303299_gshared (ReadOnlyCollection_1_t3531123129 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m3501303299(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t3531123129 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m3501303299_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m696065645_gshared (ReadOnlyCollection_1_t3531123129 * __this, Int2_t1974045593  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m696065645(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t3531123129 *, Int2_t1974045593 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m696065645_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2109889533_gshared (ReadOnlyCollection_1_t3531123129 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2109889533(__this, method) ((  void (*) (ReadOnlyCollection_1_t3531123129 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2109889533_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m84688148_gshared (ReadOnlyCollection_1_t3531123129 * __this, int32_t ___index0, Int2_t1974045593  ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m84688148(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t3531123129 *, int32_t, Int2_t1974045593 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m84688148_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2447741482_gshared (ReadOnlyCollection_1_t3531123129 * __this, Int2_t1974045593  ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2447741482(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t3531123129 *, Int2_t1974045593 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m2447741482_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2253508314_gshared (ReadOnlyCollection_1_t3531123129 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2253508314(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3531123129 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m2253508314_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  Int2_t1974045593  ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3110560192_gshared (ReadOnlyCollection_1_t3531123129 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3110560192(__this, ___index0, method) ((  Int2_t1974045593  (*) (ReadOnlyCollection_1_t3531123129 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3110560192_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1590528235_gshared (ReadOnlyCollection_1_t3531123129 * __this, int32_t ___index0, Int2_t1974045593  ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1590528235(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3531123129 *, int32_t, Int2_t1974045593 , const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1590528235_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1442531365_gshared (ReadOnlyCollection_1_t3531123129 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1442531365(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3531123129 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1442531365_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m4285487410_gshared (ReadOnlyCollection_1_t3531123129 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m4285487410(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3531123129 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m4285487410_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2271911745_gshared (ReadOnlyCollection_1_t3531123129 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2271911745(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3531123129 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m2271911745_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2930781468_gshared (ReadOnlyCollection_1_t3531123129 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m2930781468(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3531123129 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m2930781468_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m914206784_gshared (ReadOnlyCollection_1_t3531123129 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m914206784(__this, method) ((  void (*) (ReadOnlyCollection_1_t3531123129 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m914206784_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m2171893028_gshared (ReadOnlyCollection_1_t3531123129 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m2171893028(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3531123129 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m2171893028_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m27030900_gshared (ReadOnlyCollection_1_t3531123129 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m27030900(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3531123129 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m27030900_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m140325863_gshared (ReadOnlyCollection_1_t3531123129 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m140325863(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3531123129 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m140325863_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m892152289_gshared (ReadOnlyCollection_1_t3531123129 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m892152289(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t3531123129 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m892152289_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3472272887_gshared (ReadOnlyCollection_1_t3531123129 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3472272887(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t3531123129 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3472272887_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3472826296_gshared (ReadOnlyCollection_1_t3531123129 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3472826296(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3531123129 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m3472826296_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4144462186_gshared (ReadOnlyCollection_1_t3531123129 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4144462186(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3531123129 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m4144462186_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2025050003_gshared (ReadOnlyCollection_1_t3531123129 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2025050003(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3531123129 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2025050003_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3844468998_gshared (ReadOnlyCollection_1_t3531123129 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3844468998(__this, method) ((  bool (*) (ReadOnlyCollection_1_t3531123129 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m3844468998_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m2459089009_gshared (ReadOnlyCollection_1_t3531123129 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2459089009(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t3531123129 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m2459089009_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m2323120638_gshared (ReadOnlyCollection_1_t3531123129 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2323120638(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t3531123129 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m2323120638_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m2217873775_gshared (ReadOnlyCollection_1_t3531123129 * __this, Int2_t1974045593  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m2217873775(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t3531123129 *, Int2_t1974045593 , const MethodInfo*))ReadOnlyCollection_1_Contains_m2217873775_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m2572331165_gshared (ReadOnlyCollection_1_t3531123129 * __this, Int2U5BU5D_t30096868* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m2572331165(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t3531123129 *, Int2U5BU5D_t30096868*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m2572331165_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m1642655174_gshared (ReadOnlyCollection_1_t3531123129 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1642655174(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t3531123129 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1642655174_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m3908121129_gshared (ReadOnlyCollection_1_t3531123129 * __this, Int2_t1974045593  ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m3908121129(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t3531123129 *, Int2_t1974045593 , const MethodInfo*))ReadOnlyCollection_1_IndexOf_m3908121129_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m709978226_gshared (ReadOnlyCollection_1_t3531123129 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m709978226(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t3531123129 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m709978226_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>::get_Item(System.Int32)
extern "C"  Int2_t1974045593  ReadOnlyCollection_1_get_Item_m591389568_gshared (ReadOnlyCollection_1_t3531123129 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m591389568(__this, ___index0, method) ((  Int2_t1974045593  (*) (ReadOnlyCollection_1_t3531123129 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m591389568_gshared)(__this, ___index0, method)
