﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_TextMeshGenerated
struct UnityEngine_TextMeshGenerated_t408055181;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine_TextMeshGenerated::.ctor()
extern "C"  void UnityEngine_TextMeshGenerated__ctor_m3414626942 (UnityEngine_TextMeshGenerated_t408055181 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextMeshGenerated::TextMesh_TextMesh1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextMeshGenerated_TextMesh_TextMesh1_m2359561332 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextMeshGenerated::TextMesh_text(JSVCall)
extern "C"  void UnityEngine_TextMeshGenerated_TextMesh_text_m2933949497 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextMeshGenerated::TextMesh_font(JSVCall)
extern "C"  void UnityEngine_TextMeshGenerated_TextMesh_font_m532128599 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextMeshGenerated::TextMesh_fontSize(JSVCall)
extern "C"  void UnityEngine_TextMeshGenerated_TextMesh_fontSize_m549786582 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextMeshGenerated::TextMesh_fontStyle(JSVCall)
extern "C"  void UnityEngine_TextMeshGenerated_TextMesh_fontStyle_m2677684196 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextMeshGenerated::TextMesh_offsetZ(JSVCall)
extern "C"  void UnityEngine_TextMeshGenerated_TextMesh_offsetZ_m3666958847 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextMeshGenerated::TextMesh_alignment(JSVCall)
extern "C"  void UnityEngine_TextMeshGenerated_TextMesh_alignment_m197831107 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextMeshGenerated::TextMesh_anchor(JSVCall)
extern "C"  void UnityEngine_TextMeshGenerated_TextMesh_anchor_m3144904625 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextMeshGenerated::TextMesh_characterSize(JSVCall)
extern "C"  void UnityEngine_TextMeshGenerated_TextMesh_characterSize_m3268455612 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextMeshGenerated::TextMesh_lineSpacing(JSVCall)
extern "C"  void UnityEngine_TextMeshGenerated_TextMesh_lineSpacing_m31321975 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextMeshGenerated::TextMesh_tabSize(JSVCall)
extern "C"  void UnityEngine_TextMeshGenerated_TextMesh_tabSize_m1150568400 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextMeshGenerated::TextMesh_richText(JSVCall)
extern "C"  void UnityEngine_TextMeshGenerated_TextMesh_richText_m582873277 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextMeshGenerated::TextMesh_color(JSVCall)
extern "C"  void UnityEngine_TextMeshGenerated_TextMesh_color_m4279217219 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextMeshGenerated::__Register()
extern "C"  void UnityEngine_TextMeshGenerated___Register_m1152928009 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextMeshGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_TextMeshGenerated_ilo_attachFinalizerObject1_m654378553 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextMeshGenerated::ilo_setStringS2(System.Int32,System.String)
extern "C"  void UnityEngine_TextMeshGenerated_ilo_setStringS2_m3569902280 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_TextMeshGenerated::ilo_getStringS3(System.Int32)
extern "C"  String_t* UnityEngine_TextMeshGenerated_ilo_getStringS3_m3317881188 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_TextMeshGenerated::ilo_getInt324(System.Int32)
extern "C"  int32_t UnityEngine_TextMeshGenerated_ilo_getInt324_m2136662628 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextMeshGenerated::ilo_setEnum5(System.Int32,System.Int32)
extern "C"  void UnityEngine_TextMeshGenerated_ilo_setEnum5_m2560083355 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_TextMeshGenerated::ilo_getEnum6(System.Int32)
extern "C"  int32_t UnityEngine_TextMeshGenerated_ilo_getEnum6_m3402724667 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextMeshGenerated::ilo_setSingle7(System.Int32,System.Single)
extern "C"  void UnityEngine_TextMeshGenerated_ilo_setSingle7_m4038320188 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_TextMeshGenerated::ilo_getSingle8(System.Int32)
extern "C"  float UnityEngine_TextMeshGenerated_ilo_getSingle8_m2312082712 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
