﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Blur
struct Blur_t2073735;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void Blur::.ctor()
extern "C"  void Blur__ctor_m812308445 (Blur_t2073735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Blur::CheckResources()
extern "C"  bool Blur_CheckResources_m42718442 (Blur_t2073735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blur::OnDisable()
extern "C"  void Blur_OnDisable_m3680040388 (Blur_t2073735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blur::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void Blur_OnRenderImage_m3512620641 (Blur_t2073735 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Blur::Main()
extern "C"  void Blur_Main_m4236665152 (Blur_t2073735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
