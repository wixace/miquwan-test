﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_lesayqeFasairba158
struct M_lesayqeFasairba158_t225446925;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_lesayqeFasairba158::.ctor()
extern "C"  void M_lesayqeFasairba158__ctor_m1962809030 (M_lesayqeFasairba158_t225446925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lesayqeFasairba158::M_lelzicaw0(System.String[],System.Int32)
extern "C"  void M_lesayqeFasairba158_M_lelzicaw0_m3740653122 (M_lesayqeFasairba158_t225446925 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lesayqeFasairba158::M_rejoWhallhou1(System.String[],System.Int32)
extern "C"  void M_lesayqeFasairba158_M_rejoWhallhou1_m1000854084 (M_lesayqeFasairba158_t225446925 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
