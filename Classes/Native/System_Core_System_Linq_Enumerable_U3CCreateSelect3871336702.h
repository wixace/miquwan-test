﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3176762032;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Object
struct Il2CppObject;
// System.Func`2<System.Object,System.Collections.Generic.IEnumerable`1<System.Object>>
struct Func_2_t3485476982;
// System.Func`3<System.Object,System.Object,System.Object>
struct Func_3_t3956694567;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3<System.Object,System.Object,System.Object>
struct  U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702  : public Il2CppObject
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3::source
	Il2CppObject* ___source_0;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3::<$s_97>__0
	Il2CppObject* ___U3CU24s_97U3E__0_1;
	// TSource System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3::<element>__1
	Il2CppObject * ___U3CelementU3E__1_2;
	// System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TCollection>> System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3::collectionSelector
	Func_2_t3485476982 * ___collectionSelector_3;
	// System.Collections.Generic.IEnumerator`1<TCollection> System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3::<$s_98>__2
	Il2CppObject* ___U3CU24s_98U3E__2_4;
	// TCollection System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3::<collection>__3
	Il2CppObject * ___U3CcollectionU3E__3_5;
	// System.Func`3<TSource,TCollection,TResult> System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3::selector
	Func_3_t3956694567 * ___selector_6;
	// System.Int32 System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3::$PC
	int32_t ___U24PC_7;
	// TResult System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3::$current
	Il2CppObject * ___U24current_8;
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3::<$>source
	Il2CppObject* ___U3CU24U3Esource_9;
	// System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TCollection>> System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3::<$>collectionSelector
	Func_2_t3485476982 * ___U3CU24U3EcollectionSelector_10;
	// System.Func`3<TSource,TCollection,TResult> System.Linq.Enumerable/<CreateSelectManyIterator>c__Iterator14`3::<$>selector
	Func_3_t3956694567 * ___U3CU24U3Eselector_11;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702, ___source_0)); }
	inline Il2CppObject* get_source_0() const { return ___source_0; }
	inline Il2CppObject** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(Il2CppObject* value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier(&___source_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_97U3E__0_1() { return static_cast<int32_t>(offsetof(U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702, ___U3CU24s_97U3E__0_1)); }
	inline Il2CppObject* get_U3CU24s_97U3E__0_1() const { return ___U3CU24s_97U3E__0_1; }
	inline Il2CppObject** get_address_of_U3CU24s_97U3E__0_1() { return &___U3CU24s_97U3E__0_1; }
	inline void set_U3CU24s_97U3E__0_1(Il2CppObject* value)
	{
		___U3CU24s_97U3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_97U3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CelementU3E__1_2() { return static_cast<int32_t>(offsetof(U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702, ___U3CelementU3E__1_2)); }
	inline Il2CppObject * get_U3CelementU3E__1_2() const { return ___U3CelementU3E__1_2; }
	inline Il2CppObject ** get_address_of_U3CelementU3E__1_2() { return &___U3CelementU3E__1_2; }
	inline void set_U3CelementU3E__1_2(Il2CppObject * value)
	{
		___U3CelementU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CelementU3E__1_2, value);
	}

	inline static int32_t get_offset_of_collectionSelector_3() { return static_cast<int32_t>(offsetof(U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702, ___collectionSelector_3)); }
	inline Func_2_t3485476982 * get_collectionSelector_3() const { return ___collectionSelector_3; }
	inline Func_2_t3485476982 ** get_address_of_collectionSelector_3() { return &___collectionSelector_3; }
	inline void set_collectionSelector_3(Func_2_t3485476982 * value)
	{
		___collectionSelector_3 = value;
		Il2CppCodeGenWriteBarrier(&___collectionSelector_3, value);
	}

	inline static int32_t get_offset_of_U3CU24s_98U3E__2_4() { return static_cast<int32_t>(offsetof(U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702, ___U3CU24s_98U3E__2_4)); }
	inline Il2CppObject* get_U3CU24s_98U3E__2_4() const { return ___U3CU24s_98U3E__2_4; }
	inline Il2CppObject** get_address_of_U3CU24s_98U3E__2_4() { return &___U3CU24s_98U3E__2_4; }
	inline void set_U3CU24s_98U3E__2_4(Il2CppObject* value)
	{
		___U3CU24s_98U3E__2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_98U3E__2_4, value);
	}

	inline static int32_t get_offset_of_U3CcollectionU3E__3_5() { return static_cast<int32_t>(offsetof(U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702, ___U3CcollectionU3E__3_5)); }
	inline Il2CppObject * get_U3CcollectionU3E__3_5() const { return ___U3CcollectionU3E__3_5; }
	inline Il2CppObject ** get_address_of_U3CcollectionU3E__3_5() { return &___U3CcollectionU3E__3_5; }
	inline void set_U3CcollectionU3E__3_5(Il2CppObject * value)
	{
		___U3CcollectionU3E__3_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcollectionU3E__3_5, value);
	}

	inline static int32_t get_offset_of_selector_6() { return static_cast<int32_t>(offsetof(U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702, ___selector_6)); }
	inline Func_3_t3956694567 * get_selector_6() const { return ___selector_6; }
	inline Func_3_t3956694567 ** get_address_of_selector_6() { return &___selector_6; }
	inline void set_selector_6(Func_3_t3956694567 * value)
	{
		___selector_6 = value;
		Il2CppCodeGenWriteBarrier(&___selector_6, value);
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702, ___U24current_8)); }
	inline Il2CppObject * get_U24current_8() const { return ___U24current_8; }
	inline Il2CppObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(Il2CppObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_8, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Esource_9() { return static_cast<int32_t>(offsetof(U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702, ___U3CU24U3Esource_9)); }
	inline Il2CppObject* get_U3CU24U3Esource_9() const { return ___U3CU24U3Esource_9; }
	inline Il2CppObject** get_address_of_U3CU24U3Esource_9() { return &___U3CU24U3Esource_9; }
	inline void set_U3CU24U3Esource_9(Il2CppObject* value)
	{
		___U3CU24U3Esource_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Esource_9, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EcollectionSelector_10() { return static_cast<int32_t>(offsetof(U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702, ___U3CU24U3EcollectionSelector_10)); }
	inline Func_2_t3485476982 * get_U3CU24U3EcollectionSelector_10() const { return ___U3CU24U3EcollectionSelector_10; }
	inline Func_2_t3485476982 ** get_address_of_U3CU24U3EcollectionSelector_10() { return &___U3CU24U3EcollectionSelector_10; }
	inline void set_U3CU24U3EcollectionSelector_10(Func_2_t3485476982 * value)
	{
		___U3CU24U3EcollectionSelector_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EcollectionSelector_10, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eselector_11() { return static_cast<int32_t>(offsetof(U3CCreateSelectManyIteratorU3Ec__Iterator14_3_t3871336702, ___U3CU24U3Eselector_11)); }
	inline Func_3_t3956694567 * get_U3CU24U3Eselector_11() const { return ___U3CU24U3Eselector_11; }
	inline Func_3_t3956694567 ** get_address_of_U3CU24U3Eselector_11() { return &___U3CU24U3Eselector_11; }
	inline void set_U3CU24U3Eselector_11(Func_3_t3956694567 * value)
	{
		___U3CU24U3Eselector_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eselector_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
