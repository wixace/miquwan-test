﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21199798016MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<CombatEntity,HatredCtrl/stValue>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1382802744(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3657452364 *, CombatEntity_t684137495 *, stValue_t3425945410 , const MethodInfo*))KeyValuePair_2__ctor_m923468223_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<CombatEntity,HatredCtrl/stValue>::get_Key()
#define KeyValuePair_2_get_Key_m4150296368(__this, method) ((  CombatEntity_t684137495 * (*) (KeyValuePair_2_t3657452364 *, const MethodInfo*))KeyValuePair_2_get_Key_m2179442505_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<CombatEntity,HatredCtrl/stValue>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1608978929(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3657452364 *, CombatEntity_t684137495 *, const MethodInfo*))KeyValuePair_2_set_Key_m2806775050_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<CombatEntity,HatredCtrl/stValue>::get_Value()
#define KeyValuePair_2_get_Value_m142340798(__this, method) ((  stValue_t3425945410  (*) (KeyValuePair_2_t3657452364 *, const MethodInfo*))KeyValuePair_2_get_Value_m4264011977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<CombatEntity,HatredCtrl/stValue>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m93717873(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3657452364 *, stValue_t3425945410 , const MethodInfo*))KeyValuePair_2_set_Value_m3958225930_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<CombatEntity,HatredCtrl/stValue>::ToString()
#define KeyValuePair_2_ToString_m842259767(__this, method) ((  String_t* (*) (KeyValuePair_2_t3657452364 *, const MethodInfo*))KeyValuePair_2_ToString_m284841432_gshared)(__this, method)
