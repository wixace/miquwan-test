﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_raysur70
struct  M_raysur70_t3191115919  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_raysur70::_sagurni
	String_t* ____sagurni_0;
	// System.Single GarbageiOS.M_raysur70::_sisdarchi
	float ____sisdarchi_1;
	// System.Single GarbageiOS.M_raysur70::_deenowTrersar
	float ____deenowTrersar_2;
	// System.Single GarbageiOS.M_raysur70::_powelhereCistaicere
	float ____powelhereCistaicere_3;
	// System.String GarbageiOS.M_raysur70::_mumiWeagur
	String_t* ____mumiWeagur_4;
	// System.Boolean GarbageiOS.M_raysur70::_sayriTairseachow
	bool ____sayriTairseachow_5;
	// System.Boolean GarbageiOS.M_raysur70::_joumayChousur
	bool ____joumayChousur_6;
	// System.Int32 GarbageiOS.M_raysur70::_tego
	int32_t ____tego_7;
	// System.UInt32 GarbageiOS.M_raysur70::_tralti
	uint32_t ____tralti_8;
	// System.Single GarbageiOS.M_raysur70::_jarrekoo
	float ____jarrekoo_9;
	// System.Int32 GarbageiOS.M_raysur70::_fojinu
	int32_t ____fojinu_10;
	// System.Boolean GarbageiOS.M_raysur70::_nekair
	bool ____nekair_11;
	// System.String GarbageiOS.M_raysur70::_bemcemelTorca
	String_t* ____bemcemelTorca_12;
	// System.Boolean GarbageiOS.M_raysur70::_jisdralcer
	bool ____jisdralcer_13;
	// System.Boolean GarbageiOS.M_raysur70::_hadriRifirgay
	bool ____hadriRifirgay_14;
	// System.Single GarbageiOS.M_raysur70::_waspefi
	float ____waspefi_15;
	// System.Int32 GarbageiOS.M_raysur70::_deba
	int32_t ____deba_16;
	// System.String GarbageiOS.M_raysur70::_nawlalcaRemha
	String_t* ____nawlalcaRemha_17;
	// System.Boolean GarbageiOS.M_raysur70::_kawgempere
	bool ____kawgempere_18;
	// System.Single GarbageiOS.M_raysur70::_ralniti
	float ____ralniti_19;
	// System.UInt32 GarbageiOS.M_raysur70::_deatrou
	uint32_t ____deatrou_20;
	// System.Single GarbageiOS.M_raysur70::_burchalwiSumergoo
	float ____burchalwiSumergoo_21;
	// System.Int32 GarbageiOS.M_raysur70::_hirea
	int32_t ____hirea_22;
	// System.Single GarbageiOS.M_raysur70::_qowjadereHilutra
	float ____qowjadereHilutra_23;
	// System.Int32 GarbageiOS.M_raysur70::_gota
	int32_t ____gota_24;
	// System.Boolean GarbageiOS.M_raysur70::_magainel
	bool ____magainel_25;
	// System.Int32 GarbageiOS.M_raysur70::_neretea
	int32_t ____neretea_26;
	// System.Single GarbageiOS.M_raysur70::_drerekajiDalljerno
	float ____drerekajiDalljerno_27;
	// System.Int32 GarbageiOS.M_raysur70::_karsayhisWhefeebe
	int32_t ____karsayhisWhefeebe_28;
	// System.Single GarbageiOS.M_raysur70::_morsemsaGallwo
	float ____morsemsaGallwo_29;
	// System.Int32 GarbageiOS.M_raysur70::_keva
	int32_t ____keva_30;
	// System.Int32 GarbageiOS.M_raysur70::_rouyooter
	int32_t ____rouyooter_31;
	// System.String GarbageiOS.M_raysur70::_rarterYirvu
	String_t* ____rarterYirvu_32;
	// System.UInt32 GarbageiOS.M_raysur70::_tisal
	uint32_t ____tisal_33;
	// System.Int32 GarbageiOS.M_raysur70::_wislemai
	int32_t ____wislemai_34;
	// System.String GarbageiOS.M_raysur70::_xalcawu
	String_t* ____xalcawu_35;
	// System.Single GarbageiOS.M_raysur70::_stuchatir
	float ____stuchatir_36;
	// System.Int32 GarbageiOS.M_raysur70::_jeeras
	int32_t ____jeeras_37;
	// System.UInt32 GarbageiOS.M_raysur70::_qerweCardou
	uint32_t ____qerweCardou_38;
	// System.Int32 GarbageiOS.M_raysur70::_silamor
	int32_t ____silamor_39;

public:
	inline static int32_t get_offset_of__sagurni_0() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____sagurni_0)); }
	inline String_t* get__sagurni_0() const { return ____sagurni_0; }
	inline String_t** get_address_of__sagurni_0() { return &____sagurni_0; }
	inline void set__sagurni_0(String_t* value)
	{
		____sagurni_0 = value;
		Il2CppCodeGenWriteBarrier(&____sagurni_0, value);
	}

	inline static int32_t get_offset_of__sisdarchi_1() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____sisdarchi_1)); }
	inline float get__sisdarchi_1() const { return ____sisdarchi_1; }
	inline float* get_address_of__sisdarchi_1() { return &____sisdarchi_1; }
	inline void set__sisdarchi_1(float value)
	{
		____sisdarchi_1 = value;
	}

	inline static int32_t get_offset_of__deenowTrersar_2() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____deenowTrersar_2)); }
	inline float get__deenowTrersar_2() const { return ____deenowTrersar_2; }
	inline float* get_address_of__deenowTrersar_2() { return &____deenowTrersar_2; }
	inline void set__deenowTrersar_2(float value)
	{
		____deenowTrersar_2 = value;
	}

	inline static int32_t get_offset_of__powelhereCistaicere_3() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____powelhereCistaicere_3)); }
	inline float get__powelhereCistaicere_3() const { return ____powelhereCistaicere_3; }
	inline float* get_address_of__powelhereCistaicere_3() { return &____powelhereCistaicere_3; }
	inline void set__powelhereCistaicere_3(float value)
	{
		____powelhereCistaicere_3 = value;
	}

	inline static int32_t get_offset_of__mumiWeagur_4() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____mumiWeagur_4)); }
	inline String_t* get__mumiWeagur_4() const { return ____mumiWeagur_4; }
	inline String_t** get_address_of__mumiWeagur_4() { return &____mumiWeagur_4; }
	inline void set__mumiWeagur_4(String_t* value)
	{
		____mumiWeagur_4 = value;
		Il2CppCodeGenWriteBarrier(&____mumiWeagur_4, value);
	}

	inline static int32_t get_offset_of__sayriTairseachow_5() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____sayriTairseachow_5)); }
	inline bool get__sayriTairseachow_5() const { return ____sayriTairseachow_5; }
	inline bool* get_address_of__sayriTairseachow_5() { return &____sayriTairseachow_5; }
	inline void set__sayriTairseachow_5(bool value)
	{
		____sayriTairseachow_5 = value;
	}

	inline static int32_t get_offset_of__joumayChousur_6() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____joumayChousur_6)); }
	inline bool get__joumayChousur_6() const { return ____joumayChousur_6; }
	inline bool* get_address_of__joumayChousur_6() { return &____joumayChousur_6; }
	inline void set__joumayChousur_6(bool value)
	{
		____joumayChousur_6 = value;
	}

	inline static int32_t get_offset_of__tego_7() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____tego_7)); }
	inline int32_t get__tego_7() const { return ____tego_7; }
	inline int32_t* get_address_of__tego_7() { return &____tego_7; }
	inline void set__tego_7(int32_t value)
	{
		____tego_7 = value;
	}

	inline static int32_t get_offset_of__tralti_8() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____tralti_8)); }
	inline uint32_t get__tralti_8() const { return ____tralti_8; }
	inline uint32_t* get_address_of__tralti_8() { return &____tralti_8; }
	inline void set__tralti_8(uint32_t value)
	{
		____tralti_8 = value;
	}

	inline static int32_t get_offset_of__jarrekoo_9() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____jarrekoo_9)); }
	inline float get__jarrekoo_9() const { return ____jarrekoo_9; }
	inline float* get_address_of__jarrekoo_9() { return &____jarrekoo_9; }
	inline void set__jarrekoo_9(float value)
	{
		____jarrekoo_9 = value;
	}

	inline static int32_t get_offset_of__fojinu_10() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____fojinu_10)); }
	inline int32_t get__fojinu_10() const { return ____fojinu_10; }
	inline int32_t* get_address_of__fojinu_10() { return &____fojinu_10; }
	inline void set__fojinu_10(int32_t value)
	{
		____fojinu_10 = value;
	}

	inline static int32_t get_offset_of__nekair_11() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____nekair_11)); }
	inline bool get__nekair_11() const { return ____nekair_11; }
	inline bool* get_address_of__nekair_11() { return &____nekair_11; }
	inline void set__nekair_11(bool value)
	{
		____nekair_11 = value;
	}

	inline static int32_t get_offset_of__bemcemelTorca_12() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____bemcemelTorca_12)); }
	inline String_t* get__bemcemelTorca_12() const { return ____bemcemelTorca_12; }
	inline String_t** get_address_of__bemcemelTorca_12() { return &____bemcemelTorca_12; }
	inline void set__bemcemelTorca_12(String_t* value)
	{
		____bemcemelTorca_12 = value;
		Il2CppCodeGenWriteBarrier(&____bemcemelTorca_12, value);
	}

	inline static int32_t get_offset_of__jisdralcer_13() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____jisdralcer_13)); }
	inline bool get__jisdralcer_13() const { return ____jisdralcer_13; }
	inline bool* get_address_of__jisdralcer_13() { return &____jisdralcer_13; }
	inline void set__jisdralcer_13(bool value)
	{
		____jisdralcer_13 = value;
	}

	inline static int32_t get_offset_of__hadriRifirgay_14() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____hadriRifirgay_14)); }
	inline bool get__hadriRifirgay_14() const { return ____hadriRifirgay_14; }
	inline bool* get_address_of__hadriRifirgay_14() { return &____hadriRifirgay_14; }
	inline void set__hadriRifirgay_14(bool value)
	{
		____hadriRifirgay_14 = value;
	}

	inline static int32_t get_offset_of__waspefi_15() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____waspefi_15)); }
	inline float get__waspefi_15() const { return ____waspefi_15; }
	inline float* get_address_of__waspefi_15() { return &____waspefi_15; }
	inline void set__waspefi_15(float value)
	{
		____waspefi_15 = value;
	}

	inline static int32_t get_offset_of__deba_16() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____deba_16)); }
	inline int32_t get__deba_16() const { return ____deba_16; }
	inline int32_t* get_address_of__deba_16() { return &____deba_16; }
	inline void set__deba_16(int32_t value)
	{
		____deba_16 = value;
	}

	inline static int32_t get_offset_of__nawlalcaRemha_17() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____nawlalcaRemha_17)); }
	inline String_t* get__nawlalcaRemha_17() const { return ____nawlalcaRemha_17; }
	inline String_t** get_address_of__nawlalcaRemha_17() { return &____nawlalcaRemha_17; }
	inline void set__nawlalcaRemha_17(String_t* value)
	{
		____nawlalcaRemha_17 = value;
		Il2CppCodeGenWriteBarrier(&____nawlalcaRemha_17, value);
	}

	inline static int32_t get_offset_of__kawgempere_18() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____kawgempere_18)); }
	inline bool get__kawgempere_18() const { return ____kawgempere_18; }
	inline bool* get_address_of__kawgempere_18() { return &____kawgempere_18; }
	inline void set__kawgempere_18(bool value)
	{
		____kawgempere_18 = value;
	}

	inline static int32_t get_offset_of__ralniti_19() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____ralniti_19)); }
	inline float get__ralniti_19() const { return ____ralniti_19; }
	inline float* get_address_of__ralniti_19() { return &____ralniti_19; }
	inline void set__ralniti_19(float value)
	{
		____ralniti_19 = value;
	}

	inline static int32_t get_offset_of__deatrou_20() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____deatrou_20)); }
	inline uint32_t get__deatrou_20() const { return ____deatrou_20; }
	inline uint32_t* get_address_of__deatrou_20() { return &____deatrou_20; }
	inline void set__deatrou_20(uint32_t value)
	{
		____deatrou_20 = value;
	}

	inline static int32_t get_offset_of__burchalwiSumergoo_21() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____burchalwiSumergoo_21)); }
	inline float get__burchalwiSumergoo_21() const { return ____burchalwiSumergoo_21; }
	inline float* get_address_of__burchalwiSumergoo_21() { return &____burchalwiSumergoo_21; }
	inline void set__burchalwiSumergoo_21(float value)
	{
		____burchalwiSumergoo_21 = value;
	}

	inline static int32_t get_offset_of__hirea_22() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____hirea_22)); }
	inline int32_t get__hirea_22() const { return ____hirea_22; }
	inline int32_t* get_address_of__hirea_22() { return &____hirea_22; }
	inline void set__hirea_22(int32_t value)
	{
		____hirea_22 = value;
	}

	inline static int32_t get_offset_of__qowjadereHilutra_23() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____qowjadereHilutra_23)); }
	inline float get__qowjadereHilutra_23() const { return ____qowjadereHilutra_23; }
	inline float* get_address_of__qowjadereHilutra_23() { return &____qowjadereHilutra_23; }
	inline void set__qowjadereHilutra_23(float value)
	{
		____qowjadereHilutra_23 = value;
	}

	inline static int32_t get_offset_of__gota_24() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____gota_24)); }
	inline int32_t get__gota_24() const { return ____gota_24; }
	inline int32_t* get_address_of__gota_24() { return &____gota_24; }
	inline void set__gota_24(int32_t value)
	{
		____gota_24 = value;
	}

	inline static int32_t get_offset_of__magainel_25() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____magainel_25)); }
	inline bool get__magainel_25() const { return ____magainel_25; }
	inline bool* get_address_of__magainel_25() { return &____magainel_25; }
	inline void set__magainel_25(bool value)
	{
		____magainel_25 = value;
	}

	inline static int32_t get_offset_of__neretea_26() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____neretea_26)); }
	inline int32_t get__neretea_26() const { return ____neretea_26; }
	inline int32_t* get_address_of__neretea_26() { return &____neretea_26; }
	inline void set__neretea_26(int32_t value)
	{
		____neretea_26 = value;
	}

	inline static int32_t get_offset_of__drerekajiDalljerno_27() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____drerekajiDalljerno_27)); }
	inline float get__drerekajiDalljerno_27() const { return ____drerekajiDalljerno_27; }
	inline float* get_address_of__drerekajiDalljerno_27() { return &____drerekajiDalljerno_27; }
	inline void set__drerekajiDalljerno_27(float value)
	{
		____drerekajiDalljerno_27 = value;
	}

	inline static int32_t get_offset_of__karsayhisWhefeebe_28() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____karsayhisWhefeebe_28)); }
	inline int32_t get__karsayhisWhefeebe_28() const { return ____karsayhisWhefeebe_28; }
	inline int32_t* get_address_of__karsayhisWhefeebe_28() { return &____karsayhisWhefeebe_28; }
	inline void set__karsayhisWhefeebe_28(int32_t value)
	{
		____karsayhisWhefeebe_28 = value;
	}

	inline static int32_t get_offset_of__morsemsaGallwo_29() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____morsemsaGallwo_29)); }
	inline float get__morsemsaGallwo_29() const { return ____morsemsaGallwo_29; }
	inline float* get_address_of__morsemsaGallwo_29() { return &____morsemsaGallwo_29; }
	inline void set__morsemsaGallwo_29(float value)
	{
		____morsemsaGallwo_29 = value;
	}

	inline static int32_t get_offset_of__keva_30() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____keva_30)); }
	inline int32_t get__keva_30() const { return ____keva_30; }
	inline int32_t* get_address_of__keva_30() { return &____keva_30; }
	inline void set__keva_30(int32_t value)
	{
		____keva_30 = value;
	}

	inline static int32_t get_offset_of__rouyooter_31() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____rouyooter_31)); }
	inline int32_t get__rouyooter_31() const { return ____rouyooter_31; }
	inline int32_t* get_address_of__rouyooter_31() { return &____rouyooter_31; }
	inline void set__rouyooter_31(int32_t value)
	{
		____rouyooter_31 = value;
	}

	inline static int32_t get_offset_of__rarterYirvu_32() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____rarterYirvu_32)); }
	inline String_t* get__rarterYirvu_32() const { return ____rarterYirvu_32; }
	inline String_t** get_address_of__rarterYirvu_32() { return &____rarterYirvu_32; }
	inline void set__rarterYirvu_32(String_t* value)
	{
		____rarterYirvu_32 = value;
		Il2CppCodeGenWriteBarrier(&____rarterYirvu_32, value);
	}

	inline static int32_t get_offset_of__tisal_33() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____tisal_33)); }
	inline uint32_t get__tisal_33() const { return ____tisal_33; }
	inline uint32_t* get_address_of__tisal_33() { return &____tisal_33; }
	inline void set__tisal_33(uint32_t value)
	{
		____tisal_33 = value;
	}

	inline static int32_t get_offset_of__wislemai_34() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____wislemai_34)); }
	inline int32_t get__wislemai_34() const { return ____wislemai_34; }
	inline int32_t* get_address_of__wislemai_34() { return &____wislemai_34; }
	inline void set__wislemai_34(int32_t value)
	{
		____wislemai_34 = value;
	}

	inline static int32_t get_offset_of__xalcawu_35() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____xalcawu_35)); }
	inline String_t* get__xalcawu_35() const { return ____xalcawu_35; }
	inline String_t** get_address_of__xalcawu_35() { return &____xalcawu_35; }
	inline void set__xalcawu_35(String_t* value)
	{
		____xalcawu_35 = value;
		Il2CppCodeGenWriteBarrier(&____xalcawu_35, value);
	}

	inline static int32_t get_offset_of__stuchatir_36() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____stuchatir_36)); }
	inline float get__stuchatir_36() const { return ____stuchatir_36; }
	inline float* get_address_of__stuchatir_36() { return &____stuchatir_36; }
	inline void set__stuchatir_36(float value)
	{
		____stuchatir_36 = value;
	}

	inline static int32_t get_offset_of__jeeras_37() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____jeeras_37)); }
	inline int32_t get__jeeras_37() const { return ____jeeras_37; }
	inline int32_t* get_address_of__jeeras_37() { return &____jeeras_37; }
	inline void set__jeeras_37(int32_t value)
	{
		____jeeras_37 = value;
	}

	inline static int32_t get_offset_of__qerweCardou_38() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____qerweCardou_38)); }
	inline uint32_t get__qerweCardou_38() const { return ____qerweCardou_38; }
	inline uint32_t* get_address_of__qerweCardou_38() { return &____qerweCardou_38; }
	inline void set__qerweCardou_38(uint32_t value)
	{
		____qerweCardou_38 = value;
	}

	inline static int32_t get_offset_of__silamor_39() { return static_cast<int32_t>(offsetof(M_raysur70_t3191115919, ____silamor_39)); }
	inline int32_t get__silamor_39() const { return ____silamor_39; }
	inline int32_t* get_address_of__silamor_39() { return &____silamor_39; }
	inline void set__silamor_39(int32_t value)
	{
		____silamor_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
