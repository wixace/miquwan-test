﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_lorgayLete69
struct M_lorgayLete69_t3973278861;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_lorgayLete693973278861.h"

// System.Void GarbageiOS.M_lorgayLete69::.ctor()
extern "C"  void M_lorgayLete69__ctor_m179737670 (M_lorgayLete69_t3973278861 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lorgayLete69::M_geldiSavexi0(System.String[],System.Int32)
extern "C"  void M_lorgayLete69_M_geldiSavexi0_m2415946230 (M_lorgayLete69_t3973278861 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lorgayLete69::M_kohurHafeartis1(System.String[],System.Int32)
extern "C"  void M_lorgayLete69_M_kohurHafeartis1_m3552893318 (M_lorgayLete69_t3973278861 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lorgayLete69::M_sisgormaSairsalir2(System.String[],System.Int32)
extern "C"  void M_lorgayLete69_M_sisgormaSairsalir2_m1116665034 (M_lorgayLete69_t3973278861 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lorgayLete69::M_perhecaVaynabou3(System.String[],System.Int32)
extern "C"  void M_lorgayLete69_M_perhecaVaynabou3_m2260048187 (M_lorgayLete69_t3973278861 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lorgayLete69::M_resor4(System.String[],System.Int32)
extern "C"  void M_lorgayLete69_M_resor4_m1713759960 (M_lorgayLete69_t3973278861 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lorgayLete69::M_galemyaMasrear5(System.String[],System.Int32)
extern "C"  void M_lorgayLete69_M_galemyaMasrear5_m1899665789 (M_lorgayLete69_t3973278861 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lorgayLete69::M_velamairSaldridras6(System.String[],System.Int32)
extern "C"  void M_lorgayLete69_M_velamairSaldridras6_m1567579265 (M_lorgayLete69_t3973278861 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lorgayLete69::M_nurkage7(System.String[],System.Int32)
extern "C"  void M_lorgayLete69_M_nurkage7_m3528397855 (M_lorgayLete69_t3973278861 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_lorgayLete69::ilo_M_galemyaMasrear51(GarbageiOS.M_lorgayLete69,System.String[],System.Int32)
extern "C"  void M_lorgayLete69_ilo_M_galemyaMasrear51_m2367671120 (Il2CppObject * __this /* static, unused */, M_lorgayLete69_t3973278861 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
