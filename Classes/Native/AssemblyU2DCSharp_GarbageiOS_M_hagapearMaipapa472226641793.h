﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_hagapearMaipapa47
struct  M_hagapearMaipapa47_t2226641793  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_hagapearMaipapa47::_fewewhelJallmi
	int32_t ____fewewhelJallmi_0;
	// System.String GarbageiOS.M_hagapearMaipapa47::_cemjiNorcefo
	String_t* ____cemjiNorcefo_1;
	// System.Boolean GarbageiOS.M_hagapearMaipapa47::_kuwayrall
	bool ____kuwayrall_2;
	// System.Int32 GarbageiOS.M_hagapearMaipapa47::_paiseyou
	int32_t ____paiseyou_3;
	// System.UInt32 GarbageiOS.M_hagapearMaipapa47::_zenairfer
	uint32_t ____zenairfer_4;
	// System.String GarbageiOS.M_hagapearMaipapa47::_dreargoujouLelu
	String_t* ____dreargoujouLelu_5;
	// System.String GarbageiOS.M_hagapearMaipapa47::_laivayfaw
	String_t* ____laivayfaw_6;
	// System.Int32 GarbageiOS.M_hagapearMaipapa47::_cesepo
	int32_t ____cesepo_7;
	// System.UInt32 GarbageiOS.M_hagapearMaipapa47::_kijouha
	uint32_t ____kijouha_8;
	// System.Boolean GarbageiOS.M_hagapearMaipapa47::_nasniDorcairdu
	bool ____nasniDorcairdu_9;
	// System.UInt32 GarbageiOS.M_hagapearMaipapa47::_pairpu
	uint32_t ____pairpu_10;
	// System.String GarbageiOS.M_hagapearMaipapa47::_whufemse
	String_t* ____whufemse_11;
	// System.Single GarbageiOS.M_hagapearMaipapa47::_sadrairqirZokee
	float ____sadrairqirZokee_12;
	// System.String GarbageiOS.M_hagapearMaipapa47::_dousteeSacis
	String_t* ____dousteeSacis_13;
	// System.Int32 GarbageiOS.M_hagapearMaipapa47::_cuyouDewaipall
	int32_t ____cuyouDewaipall_14;
	// System.String GarbageiOS.M_hagapearMaipapa47::_belsterDafer
	String_t* ____belsterDafer_15;
	// System.String GarbageiOS.M_hagapearMaipapa47::_temerMesu
	String_t* ____temerMesu_16;
	// System.Boolean GarbageiOS.M_hagapearMaipapa47::_whornouqel
	bool ____whornouqel_17;
	// System.Boolean GarbageiOS.M_hagapearMaipapa47::_budasnereYeezearjel
	bool ____budasnereYeezearjel_18;
	// System.UInt32 GarbageiOS.M_hagapearMaipapa47::_ride
	uint32_t ____ride_19;
	// System.Int32 GarbageiOS.M_hagapearMaipapa47::_jojorle
	int32_t ____jojorle_20;
	// System.UInt32 GarbageiOS.M_hagapearMaipapa47::_whoofeGesaswaw
	uint32_t ____whoofeGesaswaw_21;
	// System.UInt32 GarbageiOS.M_hagapearMaipapa47::_xewasdal
	uint32_t ____xewasdal_22;
	// System.UInt32 GarbageiOS.M_hagapearMaipapa47::_dike
	uint32_t ____dike_23;
	// System.String GarbageiOS.M_hagapearMaipapa47::_trisrayar
	String_t* ____trisrayar_24;
	// System.Int32 GarbageiOS.M_hagapearMaipapa47::_bemkaneJisjear
	int32_t ____bemkaneJisjear_25;
	// System.Single GarbageiOS.M_hagapearMaipapa47::_xardaButeesor
	float ____xardaButeesor_26;
	// System.Boolean GarbageiOS.M_hagapearMaipapa47::_jaqe
	bool ____jaqe_27;
	// System.String GarbageiOS.M_hagapearMaipapa47::_haroo
	String_t* ____haroo_28;
	// System.Single GarbageiOS.M_hagapearMaipapa47::_lusa
	float ____lusa_29;

public:
	inline static int32_t get_offset_of__fewewhelJallmi_0() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____fewewhelJallmi_0)); }
	inline int32_t get__fewewhelJallmi_0() const { return ____fewewhelJallmi_0; }
	inline int32_t* get_address_of__fewewhelJallmi_0() { return &____fewewhelJallmi_0; }
	inline void set__fewewhelJallmi_0(int32_t value)
	{
		____fewewhelJallmi_0 = value;
	}

	inline static int32_t get_offset_of__cemjiNorcefo_1() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____cemjiNorcefo_1)); }
	inline String_t* get__cemjiNorcefo_1() const { return ____cemjiNorcefo_1; }
	inline String_t** get_address_of__cemjiNorcefo_1() { return &____cemjiNorcefo_1; }
	inline void set__cemjiNorcefo_1(String_t* value)
	{
		____cemjiNorcefo_1 = value;
		Il2CppCodeGenWriteBarrier(&____cemjiNorcefo_1, value);
	}

	inline static int32_t get_offset_of__kuwayrall_2() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____kuwayrall_2)); }
	inline bool get__kuwayrall_2() const { return ____kuwayrall_2; }
	inline bool* get_address_of__kuwayrall_2() { return &____kuwayrall_2; }
	inline void set__kuwayrall_2(bool value)
	{
		____kuwayrall_2 = value;
	}

	inline static int32_t get_offset_of__paiseyou_3() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____paiseyou_3)); }
	inline int32_t get__paiseyou_3() const { return ____paiseyou_3; }
	inline int32_t* get_address_of__paiseyou_3() { return &____paiseyou_3; }
	inline void set__paiseyou_3(int32_t value)
	{
		____paiseyou_3 = value;
	}

	inline static int32_t get_offset_of__zenairfer_4() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____zenairfer_4)); }
	inline uint32_t get__zenairfer_4() const { return ____zenairfer_4; }
	inline uint32_t* get_address_of__zenairfer_4() { return &____zenairfer_4; }
	inline void set__zenairfer_4(uint32_t value)
	{
		____zenairfer_4 = value;
	}

	inline static int32_t get_offset_of__dreargoujouLelu_5() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____dreargoujouLelu_5)); }
	inline String_t* get__dreargoujouLelu_5() const { return ____dreargoujouLelu_5; }
	inline String_t** get_address_of__dreargoujouLelu_5() { return &____dreargoujouLelu_5; }
	inline void set__dreargoujouLelu_5(String_t* value)
	{
		____dreargoujouLelu_5 = value;
		Il2CppCodeGenWriteBarrier(&____dreargoujouLelu_5, value);
	}

	inline static int32_t get_offset_of__laivayfaw_6() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____laivayfaw_6)); }
	inline String_t* get__laivayfaw_6() const { return ____laivayfaw_6; }
	inline String_t** get_address_of__laivayfaw_6() { return &____laivayfaw_6; }
	inline void set__laivayfaw_6(String_t* value)
	{
		____laivayfaw_6 = value;
		Il2CppCodeGenWriteBarrier(&____laivayfaw_6, value);
	}

	inline static int32_t get_offset_of__cesepo_7() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____cesepo_7)); }
	inline int32_t get__cesepo_7() const { return ____cesepo_7; }
	inline int32_t* get_address_of__cesepo_7() { return &____cesepo_7; }
	inline void set__cesepo_7(int32_t value)
	{
		____cesepo_7 = value;
	}

	inline static int32_t get_offset_of__kijouha_8() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____kijouha_8)); }
	inline uint32_t get__kijouha_8() const { return ____kijouha_8; }
	inline uint32_t* get_address_of__kijouha_8() { return &____kijouha_8; }
	inline void set__kijouha_8(uint32_t value)
	{
		____kijouha_8 = value;
	}

	inline static int32_t get_offset_of__nasniDorcairdu_9() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____nasniDorcairdu_9)); }
	inline bool get__nasniDorcairdu_9() const { return ____nasniDorcairdu_9; }
	inline bool* get_address_of__nasniDorcairdu_9() { return &____nasniDorcairdu_9; }
	inline void set__nasniDorcairdu_9(bool value)
	{
		____nasniDorcairdu_9 = value;
	}

	inline static int32_t get_offset_of__pairpu_10() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____pairpu_10)); }
	inline uint32_t get__pairpu_10() const { return ____pairpu_10; }
	inline uint32_t* get_address_of__pairpu_10() { return &____pairpu_10; }
	inline void set__pairpu_10(uint32_t value)
	{
		____pairpu_10 = value;
	}

	inline static int32_t get_offset_of__whufemse_11() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____whufemse_11)); }
	inline String_t* get__whufemse_11() const { return ____whufemse_11; }
	inline String_t** get_address_of__whufemse_11() { return &____whufemse_11; }
	inline void set__whufemse_11(String_t* value)
	{
		____whufemse_11 = value;
		Il2CppCodeGenWriteBarrier(&____whufemse_11, value);
	}

	inline static int32_t get_offset_of__sadrairqirZokee_12() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____sadrairqirZokee_12)); }
	inline float get__sadrairqirZokee_12() const { return ____sadrairqirZokee_12; }
	inline float* get_address_of__sadrairqirZokee_12() { return &____sadrairqirZokee_12; }
	inline void set__sadrairqirZokee_12(float value)
	{
		____sadrairqirZokee_12 = value;
	}

	inline static int32_t get_offset_of__dousteeSacis_13() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____dousteeSacis_13)); }
	inline String_t* get__dousteeSacis_13() const { return ____dousteeSacis_13; }
	inline String_t** get_address_of__dousteeSacis_13() { return &____dousteeSacis_13; }
	inline void set__dousteeSacis_13(String_t* value)
	{
		____dousteeSacis_13 = value;
		Il2CppCodeGenWriteBarrier(&____dousteeSacis_13, value);
	}

	inline static int32_t get_offset_of__cuyouDewaipall_14() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____cuyouDewaipall_14)); }
	inline int32_t get__cuyouDewaipall_14() const { return ____cuyouDewaipall_14; }
	inline int32_t* get_address_of__cuyouDewaipall_14() { return &____cuyouDewaipall_14; }
	inline void set__cuyouDewaipall_14(int32_t value)
	{
		____cuyouDewaipall_14 = value;
	}

	inline static int32_t get_offset_of__belsterDafer_15() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____belsterDafer_15)); }
	inline String_t* get__belsterDafer_15() const { return ____belsterDafer_15; }
	inline String_t** get_address_of__belsterDafer_15() { return &____belsterDafer_15; }
	inline void set__belsterDafer_15(String_t* value)
	{
		____belsterDafer_15 = value;
		Il2CppCodeGenWriteBarrier(&____belsterDafer_15, value);
	}

	inline static int32_t get_offset_of__temerMesu_16() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____temerMesu_16)); }
	inline String_t* get__temerMesu_16() const { return ____temerMesu_16; }
	inline String_t** get_address_of__temerMesu_16() { return &____temerMesu_16; }
	inline void set__temerMesu_16(String_t* value)
	{
		____temerMesu_16 = value;
		Il2CppCodeGenWriteBarrier(&____temerMesu_16, value);
	}

	inline static int32_t get_offset_of__whornouqel_17() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____whornouqel_17)); }
	inline bool get__whornouqel_17() const { return ____whornouqel_17; }
	inline bool* get_address_of__whornouqel_17() { return &____whornouqel_17; }
	inline void set__whornouqel_17(bool value)
	{
		____whornouqel_17 = value;
	}

	inline static int32_t get_offset_of__budasnereYeezearjel_18() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____budasnereYeezearjel_18)); }
	inline bool get__budasnereYeezearjel_18() const { return ____budasnereYeezearjel_18; }
	inline bool* get_address_of__budasnereYeezearjel_18() { return &____budasnereYeezearjel_18; }
	inline void set__budasnereYeezearjel_18(bool value)
	{
		____budasnereYeezearjel_18 = value;
	}

	inline static int32_t get_offset_of__ride_19() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____ride_19)); }
	inline uint32_t get__ride_19() const { return ____ride_19; }
	inline uint32_t* get_address_of__ride_19() { return &____ride_19; }
	inline void set__ride_19(uint32_t value)
	{
		____ride_19 = value;
	}

	inline static int32_t get_offset_of__jojorle_20() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____jojorle_20)); }
	inline int32_t get__jojorle_20() const { return ____jojorle_20; }
	inline int32_t* get_address_of__jojorle_20() { return &____jojorle_20; }
	inline void set__jojorle_20(int32_t value)
	{
		____jojorle_20 = value;
	}

	inline static int32_t get_offset_of__whoofeGesaswaw_21() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____whoofeGesaswaw_21)); }
	inline uint32_t get__whoofeGesaswaw_21() const { return ____whoofeGesaswaw_21; }
	inline uint32_t* get_address_of__whoofeGesaswaw_21() { return &____whoofeGesaswaw_21; }
	inline void set__whoofeGesaswaw_21(uint32_t value)
	{
		____whoofeGesaswaw_21 = value;
	}

	inline static int32_t get_offset_of__xewasdal_22() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____xewasdal_22)); }
	inline uint32_t get__xewasdal_22() const { return ____xewasdal_22; }
	inline uint32_t* get_address_of__xewasdal_22() { return &____xewasdal_22; }
	inline void set__xewasdal_22(uint32_t value)
	{
		____xewasdal_22 = value;
	}

	inline static int32_t get_offset_of__dike_23() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____dike_23)); }
	inline uint32_t get__dike_23() const { return ____dike_23; }
	inline uint32_t* get_address_of__dike_23() { return &____dike_23; }
	inline void set__dike_23(uint32_t value)
	{
		____dike_23 = value;
	}

	inline static int32_t get_offset_of__trisrayar_24() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____trisrayar_24)); }
	inline String_t* get__trisrayar_24() const { return ____trisrayar_24; }
	inline String_t** get_address_of__trisrayar_24() { return &____trisrayar_24; }
	inline void set__trisrayar_24(String_t* value)
	{
		____trisrayar_24 = value;
		Il2CppCodeGenWriteBarrier(&____trisrayar_24, value);
	}

	inline static int32_t get_offset_of__bemkaneJisjear_25() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____bemkaneJisjear_25)); }
	inline int32_t get__bemkaneJisjear_25() const { return ____bemkaneJisjear_25; }
	inline int32_t* get_address_of__bemkaneJisjear_25() { return &____bemkaneJisjear_25; }
	inline void set__bemkaneJisjear_25(int32_t value)
	{
		____bemkaneJisjear_25 = value;
	}

	inline static int32_t get_offset_of__xardaButeesor_26() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____xardaButeesor_26)); }
	inline float get__xardaButeesor_26() const { return ____xardaButeesor_26; }
	inline float* get_address_of__xardaButeesor_26() { return &____xardaButeesor_26; }
	inline void set__xardaButeesor_26(float value)
	{
		____xardaButeesor_26 = value;
	}

	inline static int32_t get_offset_of__jaqe_27() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____jaqe_27)); }
	inline bool get__jaqe_27() const { return ____jaqe_27; }
	inline bool* get_address_of__jaqe_27() { return &____jaqe_27; }
	inline void set__jaqe_27(bool value)
	{
		____jaqe_27 = value;
	}

	inline static int32_t get_offset_of__haroo_28() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____haroo_28)); }
	inline String_t* get__haroo_28() const { return ____haroo_28; }
	inline String_t** get_address_of__haroo_28() { return &____haroo_28; }
	inline void set__haroo_28(String_t* value)
	{
		____haroo_28 = value;
		Il2CppCodeGenWriteBarrier(&____haroo_28, value);
	}

	inline static int32_t get_offset_of__lusa_29() { return static_cast<int32_t>(offsetof(M_hagapearMaipapa47_t2226641793, ____lusa_29)); }
	inline float get__lusa_29() const { return ____lusa_29; }
	inline float* get_address_of__lusa_29() { return &____lusa_29; }
	inline void set__lusa_29(float value)
	{
		____lusa_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
