﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUIGenerated/<GUI_ModalWindow_GetDelegate_member56_arg2>c__AnonStoreyEA
struct U3CGUI_ModalWindow_GetDelegate_member56_arg2U3Ec__AnonStoreyEA_t1503834578;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_GUIGenerated/<GUI_ModalWindow_GetDelegate_member56_arg2>c__AnonStoreyEA::.ctor()
extern "C"  void U3CGUI_ModalWindow_GetDelegate_member56_arg2U3Ec__AnonStoreyEA__ctor_m3194359257 (U3CGUI_ModalWindow_GetDelegate_member56_arg2U3Ec__AnonStoreyEA_t1503834578 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated/<GUI_ModalWindow_GetDelegate_member56_arg2>c__AnonStoreyEA::<>m__1BE(System.Int32)
extern "C"  void U3CGUI_ModalWindow_GetDelegate_member56_arg2U3Ec__AnonStoreyEA_U3CU3Em__1BE_m4053645893 (U3CGUI_ModalWindow_GetDelegate_member56_arg2U3Ec__AnonStoreyEA_t1503834578 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
