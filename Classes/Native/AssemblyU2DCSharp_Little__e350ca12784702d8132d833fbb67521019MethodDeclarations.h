﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._e350ca12784702d8132d833fbb3fbb8c
struct _e350ca12784702d8132d833fbb3fbb8c_t67521019;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__e350ca12784702d8132d833fbb67521019.h"

// System.Void Little._e350ca12784702d8132d833fbb3fbb8c::.ctor()
extern "C"  void _e350ca12784702d8132d833fbb3fbb8c__ctor_m1405150866 (_e350ca12784702d8132d833fbb3fbb8c_t67521019 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e350ca12784702d8132d833fbb3fbb8c::_e350ca12784702d8132d833fbb3fbb8cm2(System.Int32)
extern "C"  int32_t _e350ca12784702d8132d833fbb3fbb8c__e350ca12784702d8132d833fbb3fbb8cm2_m956200889 (_e350ca12784702d8132d833fbb3fbb8c_t67521019 * __this, int32_t ____e350ca12784702d8132d833fbb3fbb8ca0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e350ca12784702d8132d833fbb3fbb8c::_e350ca12784702d8132d833fbb3fbb8cm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _e350ca12784702d8132d833fbb3fbb8c__e350ca12784702d8132d833fbb3fbb8cm_m3765392157 (_e350ca12784702d8132d833fbb3fbb8c_t67521019 * __this, int32_t ____e350ca12784702d8132d833fbb3fbb8ca0, int32_t ____e350ca12784702d8132d833fbb3fbb8c631, int32_t ____e350ca12784702d8132d833fbb3fbb8cc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e350ca12784702d8132d833fbb3fbb8c::ilo__e350ca12784702d8132d833fbb3fbb8cm21(Little._e350ca12784702d8132d833fbb3fbb8c,System.Int32)
extern "C"  int32_t _e350ca12784702d8132d833fbb3fbb8c_ilo__e350ca12784702d8132d833fbb3fbb8cm21_m3595804706 (Il2CppObject * __this /* static, unused */, _e350ca12784702d8132d833fbb3fbb8c_t67521019 * ____this0, int32_t ____e350ca12784702d8132d833fbb3fbb8ca1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
