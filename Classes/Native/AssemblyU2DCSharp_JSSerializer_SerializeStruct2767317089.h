﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// JSSerializer/SerializeStruct
struct SerializeStruct_t2767317089;
// System.Collections.Generic.List`1<JSSerializer/SerializeStruct>
struct List_1_t4135502641;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSSerializer_SerializeStruct_STy1913504511.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSSerializer/SerializeStruct
struct  SerializeStruct_t2767317089  : public Il2CppObject
{
public:
	// JSSerializer/SerializeStruct/SType JSSerializer/SerializeStruct::type
	int32_t ___type_0;
	// System.String JSSerializer/SerializeStruct::name
	String_t* ___name_1;
	// System.String JSSerializer/SerializeStruct::typeName
	String_t* ___typeName_2;
	// System.Int32 JSSerializer/SerializeStruct::__id
	int32_t _____id_3;
	// JSSerializer/SerializeStruct JSSerializer/SerializeStruct::father
	SerializeStruct_t2767317089 * ___father_4;
	// System.Collections.Generic.List`1<JSSerializer/SerializeStruct> JSSerializer/SerializeStruct::lstChildren
	List_1_t4135502641 * ___lstChildren_5;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(SerializeStruct_t2767317089, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(SerializeStruct_t2767317089, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_typeName_2() { return static_cast<int32_t>(offsetof(SerializeStruct_t2767317089, ___typeName_2)); }
	inline String_t* get_typeName_2() const { return ___typeName_2; }
	inline String_t** get_address_of_typeName_2() { return &___typeName_2; }
	inline void set_typeName_2(String_t* value)
	{
		___typeName_2 = value;
		Il2CppCodeGenWriteBarrier(&___typeName_2, value);
	}

	inline static int32_t get_offset_of___id_3() { return static_cast<int32_t>(offsetof(SerializeStruct_t2767317089, _____id_3)); }
	inline int32_t get___id_3() const { return _____id_3; }
	inline int32_t* get_address_of___id_3() { return &_____id_3; }
	inline void set___id_3(int32_t value)
	{
		_____id_3 = value;
	}

	inline static int32_t get_offset_of_father_4() { return static_cast<int32_t>(offsetof(SerializeStruct_t2767317089, ___father_4)); }
	inline SerializeStruct_t2767317089 * get_father_4() const { return ___father_4; }
	inline SerializeStruct_t2767317089 ** get_address_of_father_4() { return &___father_4; }
	inline void set_father_4(SerializeStruct_t2767317089 * value)
	{
		___father_4 = value;
		Il2CppCodeGenWriteBarrier(&___father_4, value);
	}

	inline static int32_t get_offset_of_lstChildren_5() { return static_cast<int32_t>(offsetof(SerializeStruct_t2767317089, ___lstChildren_5)); }
	inline List_1_t4135502641 * get_lstChildren_5() const { return ___lstChildren_5; }
	inline List_1_t4135502641 ** get_address_of_lstChildren_5() { return &___lstChildren_5; }
	inline void set_lstChildren_5(List_1_t4135502641 * value)
	{
		___lstChildren_5 = value;
		Il2CppCodeGenWriteBarrier(&___lstChildren_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
