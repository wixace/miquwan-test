﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;
// Effect2DSound
struct Effect2DSound_t1148696332;
// BGSound
struct BGSound_t558445514;
// System.Collections.Generic.Dictionary`2<SoundTypeID,ISound>
struct Dictionary_2_t47618509;
// System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>
struct Dictionary_2_t3590202184;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundMgr
struct  SoundMgr_t1807284905  : public Il2CppObject
{
public:
	// System.Boolean SoundMgr::_mute
	bool ____mute_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> SoundMgr::sceneBGDic
	Dictionary_2_t1974256870 * ___sceneBGDic_2;
	// Effect2DSound SoundMgr::sound2DMgr
	Effect2DSound_t1148696332 * ___sound2DMgr_3;
	// System.Boolean SoundMgr::sound2DChatType
	bool ___sound2DChatType_4;
	// BGSound SoundMgr::soundBGMgr
	BGSound_t558445514 * ___soundBGMgr_5;
	// System.Boolean SoundMgr::soundBgChatType
	bool ___soundBgChatType_6;
	// System.Collections.Generic.Dictionary`2<SoundTypeID,ISound> SoundMgr::soundList
	Dictionary_2_t47618509 * ___soundList_7;
	// System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus> SoundMgr::soundStatus
	Dictionary_2_t3590202184 * ___soundStatus_8;
	// System.Collections.Generic.List`1<System.Int32> SoundMgr::randomReviewBGList
	List_1_t2522024052 * ___randomReviewBGList_9;

public:
	inline static int32_t get_offset_of__mute_1() { return static_cast<int32_t>(offsetof(SoundMgr_t1807284905, ____mute_1)); }
	inline bool get__mute_1() const { return ____mute_1; }
	inline bool* get_address_of__mute_1() { return &____mute_1; }
	inline void set__mute_1(bool value)
	{
		____mute_1 = value;
	}

	inline static int32_t get_offset_of_sceneBGDic_2() { return static_cast<int32_t>(offsetof(SoundMgr_t1807284905, ___sceneBGDic_2)); }
	inline Dictionary_2_t1974256870 * get_sceneBGDic_2() const { return ___sceneBGDic_2; }
	inline Dictionary_2_t1974256870 ** get_address_of_sceneBGDic_2() { return &___sceneBGDic_2; }
	inline void set_sceneBGDic_2(Dictionary_2_t1974256870 * value)
	{
		___sceneBGDic_2 = value;
		Il2CppCodeGenWriteBarrier(&___sceneBGDic_2, value);
	}

	inline static int32_t get_offset_of_sound2DMgr_3() { return static_cast<int32_t>(offsetof(SoundMgr_t1807284905, ___sound2DMgr_3)); }
	inline Effect2DSound_t1148696332 * get_sound2DMgr_3() const { return ___sound2DMgr_3; }
	inline Effect2DSound_t1148696332 ** get_address_of_sound2DMgr_3() { return &___sound2DMgr_3; }
	inline void set_sound2DMgr_3(Effect2DSound_t1148696332 * value)
	{
		___sound2DMgr_3 = value;
		Il2CppCodeGenWriteBarrier(&___sound2DMgr_3, value);
	}

	inline static int32_t get_offset_of_sound2DChatType_4() { return static_cast<int32_t>(offsetof(SoundMgr_t1807284905, ___sound2DChatType_4)); }
	inline bool get_sound2DChatType_4() const { return ___sound2DChatType_4; }
	inline bool* get_address_of_sound2DChatType_4() { return &___sound2DChatType_4; }
	inline void set_sound2DChatType_4(bool value)
	{
		___sound2DChatType_4 = value;
	}

	inline static int32_t get_offset_of_soundBGMgr_5() { return static_cast<int32_t>(offsetof(SoundMgr_t1807284905, ___soundBGMgr_5)); }
	inline BGSound_t558445514 * get_soundBGMgr_5() const { return ___soundBGMgr_5; }
	inline BGSound_t558445514 ** get_address_of_soundBGMgr_5() { return &___soundBGMgr_5; }
	inline void set_soundBGMgr_5(BGSound_t558445514 * value)
	{
		___soundBGMgr_5 = value;
		Il2CppCodeGenWriteBarrier(&___soundBGMgr_5, value);
	}

	inline static int32_t get_offset_of_soundBgChatType_6() { return static_cast<int32_t>(offsetof(SoundMgr_t1807284905, ___soundBgChatType_6)); }
	inline bool get_soundBgChatType_6() const { return ___soundBgChatType_6; }
	inline bool* get_address_of_soundBgChatType_6() { return &___soundBgChatType_6; }
	inline void set_soundBgChatType_6(bool value)
	{
		___soundBgChatType_6 = value;
	}

	inline static int32_t get_offset_of_soundList_7() { return static_cast<int32_t>(offsetof(SoundMgr_t1807284905, ___soundList_7)); }
	inline Dictionary_2_t47618509 * get_soundList_7() const { return ___soundList_7; }
	inline Dictionary_2_t47618509 ** get_address_of_soundList_7() { return &___soundList_7; }
	inline void set_soundList_7(Dictionary_2_t47618509 * value)
	{
		___soundList_7 = value;
		Il2CppCodeGenWriteBarrier(&___soundList_7, value);
	}

	inline static int32_t get_offset_of_soundStatus_8() { return static_cast<int32_t>(offsetof(SoundMgr_t1807284905, ___soundStatus_8)); }
	inline Dictionary_2_t3590202184 * get_soundStatus_8() const { return ___soundStatus_8; }
	inline Dictionary_2_t3590202184 ** get_address_of_soundStatus_8() { return &___soundStatus_8; }
	inline void set_soundStatus_8(Dictionary_2_t3590202184 * value)
	{
		___soundStatus_8 = value;
		Il2CppCodeGenWriteBarrier(&___soundStatus_8, value);
	}

	inline static int32_t get_offset_of_randomReviewBGList_9() { return static_cast<int32_t>(offsetof(SoundMgr_t1807284905, ___randomReviewBGList_9)); }
	inline List_1_t2522024052 * get_randomReviewBGList_9() const { return ___randomReviewBGList_9; }
	inline List_1_t2522024052 ** get_address_of_randomReviewBGList_9() { return &___randomReviewBGList_9; }
	inline void set_randomReviewBGList_9(List_1_t2522024052 * value)
	{
		___randomReviewBGList_9 = value;
		Il2CppCodeGenWriteBarrier(&___randomReviewBGList_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
