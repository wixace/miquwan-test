﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>
struct Dictionary_2_t2163003329;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2777939383.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3773320896_gshared (Enumerator_t2777939383 * __this, Dictionary_2_t2163003329 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3773320896(__this, ___host0, method) ((  void (*) (Enumerator_t2777939383 *, Dictionary_2_t2163003329 *, const MethodInfo*))Enumerator__ctor_m3773320896_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3852236705_gshared (Enumerator_t2777939383 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3852236705(__this, method) ((  Il2CppObject * (*) (Enumerator_t2777939383 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3852236705_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3980233205_gshared (Enumerator_t2777939383 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3980233205(__this, method) ((  void (*) (Enumerator_t2777939383 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3980233205_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::Dispose()
extern "C"  void Enumerator_Dispose_m2121090402_gshared (Enumerator_t2777939383 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2121090402(__this, method) ((  void (*) (Enumerator_t2777939383 *, const MethodInfo*))Enumerator_Dispose_m2121090402_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1697622497_gshared (Enumerator_t2777939383 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1697622497(__this, method) ((  bool (*) (Enumerator_t2777939383 *, const MethodInfo*))Enumerator_MoveNext_m1697622497_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m1216140243_gshared (Enumerator_t2777939383 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1216140243(__this, method) ((  Il2CppObject * (*) (Enumerator_t2777939383 *, const MethodInfo*))Enumerator_get_Current_m1216140243_gshared)(__this, method)
