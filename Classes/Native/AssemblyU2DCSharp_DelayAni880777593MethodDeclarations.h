﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DelayAni
struct DelayAni_t880777593;

#include "codegen/il2cpp-codegen.h"

// System.Void DelayAni::.ctor()
extern "C"  void DelayAni__ctor_m3459497282 (DelayAni_t880777593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DelayAni::Start()
extern "C"  void DelayAni_Start_m2406635074 (DelayAni_t880777593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
