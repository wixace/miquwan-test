﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_peregoo176
struct M_peregoo176_t2304884001;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_peregoo176::.ctor()
extern "C"  void M_peregoo176__ctor_m1968292914 (M_peregoo176_t2304884001 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_peregoo176::M_naiwoonou0(System.String[],System.Int32)
extern "C"  void M_peregoo176_M_naiwoonou0_m1772956664 (M_peregoo176_t2304884001 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
