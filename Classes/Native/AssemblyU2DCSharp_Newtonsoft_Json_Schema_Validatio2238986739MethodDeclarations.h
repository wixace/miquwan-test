﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Schema.ValidationEventHandler
struct ValidationEventHandler_t2238986739;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Schema.ValidationEventArgs
struct ValidationEventArgs_t623166840;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_Validation623166840.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void Newtonsoft.Json.Schema.ValidationEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void ValidationEventHandler__ctor_m1622391640 (ValidationEventHandler_t2238986739 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.ValidationEventHandler::Invoke(System.Object,Newtonsoft.Json.Schema.ValidationEventArgs)
extern "C"  void ValidationEventHandler_Invoke_m3029449502 (ValidationEventHandler_t2238986739 * __this, Il2CppObject * ___sender0, ValidationEventArgs_t623166840 * ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Newtonsoft.Json.Schema.ValidationEventHandler::BeginInvoke(System.Object,Newtonsoft.Json.Schema.ValidationEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ValidationEventHandler_BeginInvoke_m2093764985 (ValidationEventHandler_t2238986739 * __this, Il2CppObject * ___sender0, ValidationEventArgs_t623166840 * ___e1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.ValidationEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void ValidationEventHandler_EndInvoke_m1511692648 (ValidationEventHandler_t2238986739 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
