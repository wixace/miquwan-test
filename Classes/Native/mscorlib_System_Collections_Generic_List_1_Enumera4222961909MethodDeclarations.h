﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Poly2Tri.DelaunayTriangle>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m268272117(__this, ___l0, method) ((  void (*) (Enumerator_t4222961909 *, List_1_t4203289139 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Poly2Tri.DelaunayTriangle>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2599120189(__this, method) ((  void (*) (Enumerator_t4222961909 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.Poly2Tri.DelaunayTriangle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1347746729(__this, method) ((  Il2CppObject * (*) (Enumerator_t4222961909 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Poly2Tri.DelaunayTriangle>::Dispose()
#define Enumerator_Dispose_m1526603034(__this, method) ((  void (*) (Enumerator_t4222961909 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Poly2Tri.DelaunayTriangle>::VerifyState()
#define Enumerator_VerifyState_m2940845907(__this, method) ((  void (*) (Enumerator_t4222961909 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.Poly2Tri.DelaunayTriangle>::MoveNext()
#define Enumerator_MoveNext_m422834345(__this, method) ((  bool (*) (Enumerator_t4222961909 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.Poly2Tri.DelaunayTriangle>::get_Current()
#define Enumerator_get_Current_m536087114(__this, method) ((  DelaunayTriangle_t2835103587 * (*) (Enumerator_t4222961909 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
