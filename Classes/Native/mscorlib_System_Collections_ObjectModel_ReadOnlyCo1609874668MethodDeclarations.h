﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>
struct ReadOnlyCollection_1_t1609874668;
// System.Collections.Generic.IList`1<Core.RpsChoice>
struct IList_1_t2747444335;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Core.RpsChoice[]
struct RpsChoiceU5BU5D_t3806589061;
// System.Collections.Generic.IEnumerator`1<Core.RpsChoice>
struct IEnumerator_1_t1964662181;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Core_RpsChoice52797132.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m441383081_gshared (ReadOnlyCollection_1_t1609874668 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m441383081(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1609874668 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m441383081_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3913592339_gshared (ReadOnlyCollection_1_t1609874668 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3913592339(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1609874668 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3913592339_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3068969239_gshared (ReadOnlyCollection_1_t1609874668 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3068969239(__this, method) ((  void (*) (ReadOnlyCollection_1_t1609874668 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3068969239_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1144583482_gshared (ReadOnlyCollection_1_t1609874668 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1144583482(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1609874668 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1144583482_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3340718404_gshared (ReadOnlyCollection_1_t1609874668 * __this, int32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3340718404(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1609874668 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m3340718404_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3313403648_gshared (ReadOnlyCollection_1_t1609874668 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3313403648(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1609874668 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3313403648_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3651202598_gshared (ReadOnlyCollection_1_t1609874668 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3651202598(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1609874668 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3651202598_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2242695057_gshared (ReadOnlyCollection_1_t1609874668 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2242695057(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1609874668 *, int32_t, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2242695057_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m201013195_gshared (ReadOnlyCollection_1_t1609874668 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m201013195(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1609874668 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m201013195_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2187255000_gshared (ReadOnlyCollection_1_t1609874668 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2187255000(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1609874668 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m2187255000_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m576725735_gshared (ReadOnlyCollection_1_t1609874668 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m576725735(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1609874668 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m576725735_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m1321174198_gshared (ReadOnlyCollection_1_t1609874668 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m1321174198(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1609874668 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1321174198_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m481643494_gshared (ReadOnlyCollection_1_t1609874668 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m481643494(__this, method) ((  void (*) (ReadOnlyCollection_1_t1609874668 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m481643494_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m3301350474_gshared (ReadOnlyCollection_1_t1609874668 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3301350474(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1609874668 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m3301350474_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3567415310_gshared (ReadOnlyCollection_1_t1609874668 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3567415310(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1609874668 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3567415310_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2932310145_gshared (ReadOnlyCollection_1_t1609874668 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2932310145(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1609874668 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2932310145_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m2361420551_gshared (ReadOnlyCollection_1_t1609874668 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m2361420551(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1609874668 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2361420551_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1774948753_gshared (ReadOnlyCollection_1_t1609874668 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1774948753(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1609874668 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1774948753_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m70835922_gshared (ReadOnlyCollection_1_t1609874668 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m70835922(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1609874668 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m70835922_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m882603204_gshared (ReadOnlyCollection_1_t1609874668 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m882603204(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1609874668 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m882603204_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2447625017_gshared (ReadOnlyCollection_1_t1609874668 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2447625017(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1609874668 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2447625017_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1641343136_gshared (ReadOnlyCollection_1_t1609874668 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1641343136(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1609874668 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1641343136_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m3138733387_gshared (ReadOnlyCollection_1_t1609874668 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m3138733387(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1609874668 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3138733387_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m1065455640_gshared (ReadOnlyCollection_1_t1609874668 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m1065455640(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1609874668 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m1065455640_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m853308041_gshared (ReadOnlyCollection_1_t1609874668 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m853308041(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1609874668 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m853308041_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m3646638659_gshared (ReadOnlyCollection_1_t1609874668 * __this, RpsChoiceU5BU5D_t3806589061* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m3646638659(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1609874668 *, RpsChoiceU5BU5D_t3806589061*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m3646638659_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m2122354784_gshared (ReadOnlyCollection_1_t1609874668 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m2122354784(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1609874668 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m2122354784_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m2230949711_gshared (ReadOnlyCollection_1_t1609874668 * __this, int32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m2230949711(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1609874668 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m2230949711_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m257271820_gshared (ReadOnlyCollection_1_t1609874668 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m257271820(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1609874668 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m257271820_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Core.RpsChoice>::get_Item(System.Int32)
extern "C"  int32_t ReadOnlyCollection_1_get_Item_m171092710_gshared (ReadOnlyCollection_1_t1609874668 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m171092710(__this, ___index0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1609874668 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m171092710_gshared)(__this, ___index0, method)
