﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.QuadtreeNode
struct QuadtreeNode_t1423301757;
// Pathfinding.QuadtreeNodeHolder
struct QuadtreeNodeHolder_t2134093193;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.QuadtreeNodeHolder
struct  QuadtreeNodeHolder_t2134093193  : public Il2CppObject
{
public:
	// Pathfinding.QuadtreeNode Pathfinding.QuadtreeNodeHolder::node
	QuadtreeNode_t1423301757 * ___node_0;
	// Pathfinding.QuadtreeNodeHolder Pathfinding.QuadtreeNodeHolder::c0
	QuadtreeNodeHolder_t2134093193 * ___c0_1;
	// Pathfinding.QuadtreeNodeHolder Pathfinding.QuadtreeNodeHolder::c1
	QuadtreeNodeHolder_t2134093193 * ___c1_2;
	// Pathfinding.QuadtreeNodeHolder Pathfinding.QuadtreeNodeHolder::c2
	QuadtreeNodeHolder_t2134093193 * ___c2_3;
	// Pathfinding.QuadtreeNodeHolder Pathfinding.QuadtreeNodeHolder::c3
	QuadtreeNodeHolder_t2134093193 * ___c3_4;

public:
	inline static int32_t get_offset_of_node_0() { return static_cast<int32_t>(offsetof(QuadtreeNodeHolder_t2134093193, ___node_0)); }
	inline QuadtreeNode_t1423301757 * get_node_0() const { return ___node_0; }
	inline QuadtreeNode_t1423301757 ** get_address_of_node_0() { return &___node_0; }
	inline void set_node_0(QuadtreeNode_t1423301757 * value)
	{
		___node_0 = value;
		Il2CppCodeGenWriteBarrier(&___node_0, value);
	}

	inline static int32_t get_offset_of_c0_1() { return static_cast<int32_t>(offsetof(QuadtreeNodeHolder_t2134093193, ___c0_1)); }
	inline QuadtreeNodeHolder_t2134093193 * get_c0_1() const { return ___c0_1; }
	inline QuadtreeNodeHolder_t2134093193 ** get_address_of_c0_1() { return &___c0_1; }
	inline void set_c0_1(QuadtreeNodeHolder_t2134093193 * value)
	{
		___c0_1 = value;
		Il2CppCodeGenWriteBarrier(&___c0_1, value);
	}

	inline static int32_t get_offset_of_c1_2() { return static_cast<int32_t>(offsetof(QuadtreeNodeHolder_t2134093193, ___c1_2)); }
	inline QuadtreeNodeHolder_t2134093193 * get_c1_2() const { return ___c1_2; }
	inline QuadtreeNodeHolder_t2134093193 ** get_address_of_c1_2() { return &___c1_2; }
	inline void set_c1_2(QuadtreeNodeHolder_t2134093193 * value)
	{
		___c1_2 = value;
		Il2CppCodeGenWriteBarrier(&___c1_2, value);
	}

	inline static int32_t get_offset_of_c2_3() { return static_cast<int32_t>(offsetof(QuadtreeNodeHolder_t2134093193, ___c2_3)); }
	inline QuadtreeNodeHolder_t2134093193 * get_c2_3() const { return ___c2_3; }
	inline QuadtreeNodeHolder_t2134093193 ** get_address_of_c2_3() { return &___c2_3; }
	inline void set_c2_3(QuadtreeNodeHolder_t2134093193 * value)
	{
		___c2_3 = value;
		Il2CppCodeGenWriteBarrier(&___c2_3, value);
	}

	inline static int32_t get_offset_of_c3_4() { return static_cast<int32_t>(offsetof(QuadtreeNodeHolder_t2134093193, ___c3_4)); }
	inline QuadtreeNodeHolder_t2134093193 * get_c3_4() const { return ___c3_4; }
	inline QuadtreeNodeHolder_t2134093193 ** get_address_of_c3_4() { return &___c3_4; }
	inline void set_c3_4(QuadtreeNodeHolder_t2134093193 * value)
	{
		___c3_4 = value;
		Il2CppCodeGenWriteBarrier(&___c3_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
