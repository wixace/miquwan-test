﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlotFightTalkMgr
struct PlotFightTalkMgr_t866599741;
// CombatEntity
struct CombatEntity_t684137495;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// PlotFightTalkMgr/plotData
struct plotData_t645270205;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// PlotFightTalkMgr/PlotTermParam
struct PlotTermParam_t2297548846;
// GlobalGOMgr
struct GlobalGOMgr_t803081773;
// PlotFightTalk
struct PlotFightTalk_t789215451;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_PlotFightTalkMgr_plotData645270205.h"
#include "AssemblyU2DCSharp_PlotFightTalkMgr_PlotTermParam2297548846.h"
#include "AssemblyU2DCSharp_PlotFightTalkMgr866599741.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_PlotFightTalk789215451.h"
#include "mscorlib_System_String7231557.h"

// System.Void PlotFightTalkMgr::.ctor(CombatEntity)
extern "C"  void PlotFightTalkMgr__ctor_m581928775 (PlotFightTalkMgr_t866599741 * __this, CombatEntity_t684137495 * ___unit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr::IZUpdate.ZUpdate()
extern "C"  void PlotFightTalkMgr_IZUpdate_ZUpdate_m4049495893 (PlotFightTalkMgr_t866599741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject PlotFightTalkMgr::get_parentGO()
extern "C"  GameObject_t3674682005 * PlotFightTalkMgr_get_parentGO_m3755862582 (PlotFightTalkMgr_t866599741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr::OnTrigger(CEvent.ZEvent)
extern "C"  void PlotFightTalkMgr_OnTrigger_m350533222 (PlotFightTalkMgr_t866599741 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr::Play(PlotFightTalkMgr/plotData)
extern "C"  void PlotFightTalkMgr_Play_m1904522907 (PlotFightTalkMgr_t866599741 * __this, plotData_t645270205 * ___data0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlotFightTalkMgr::IsHaveNpc(System.Collections.Generic.List`1<System.Int32>,System.Int32[])
extern "C"  bool PlotFightTalkMgr_IsHaveNpc_m51012683 (PlotFightTalkMgr_t866599741 * __this, List_1_t2522024052 * ___npcs0, Int32U5BU5D_t3230847821* ___haveNpcs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlotFightTalkMgr::IsNoHaveNpc(System.Collections.Generic.List`1<System.Int32>,System.Int32[])
extern "C"  bool PlotFightTalkMgr_IsNoHaveNpc_m615823370 (PlotFightTalkMgr_t866599741 * __this, List_1_t2522024052 * ___npcs0, Int32U5BU5D_t3230847821* ___haveNpcs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr::Clear()
extern "C"  void PlotFightTalkMgr_Clear_m3020813353 (PlotFightTalkMgr_t866599741 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlotFightTalkMgr::SpliteTermValue(PlotFightTalkMgr/PlotTermParam,PlotFightTalkMgr/plotData)
extern "C"  int32_t PlotFightTalkMgr_SpliteTermValue_m3774855241 (PlotFightTalkMgr_t866599741 * __this, PlotTermParam_t2297548846 * ___plotTerm0, plotData_t645270205 * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr::ilo_Play1(PlotFightTalkMgr,PlotFightTalkMgr/plotData)
extern "C"  void PlotFightTalkMgr_ilo_Play1_m3247547064 (Il2CppObject * __this /* static, unused */, PlotFightTalkMgr_t866599741 * ____this0, plotData_t645270205 * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GlobalGOMgr PlotFightTalkMgr::ilo_get_GlobalGOMgr2()
extern "C"  GlobalGOMgr_t803081773 * PlotFightTalkMgr_ilo_get_GlobalGOMgr2_m602480205 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlotFightTalkMgr::ilo_get_isHide3(CombatEntity)
extern "C"  bool PlotFightTalkMgr_ilo_get_isHide3_m1573978492 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlotFightTalkMgr::ilo_SpliteTermValue4(PlotFightTalkMgr,PlotFightTalkMgr/PlotTermParam,PlotFightTalkMgr/plotData)
extern "C"  int32_t PlotFightTalkMgr_ilo_SpliteTermValue4_m3128139491 (Il2CppObject * __this /* static, unused */, PlotFightTalkMgr_t866599741 * ____this0, PlotTermParam_t2297548846 * ___plotTerm1, plotData_t645270205 * ___data2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlotFightTalkMgr::ilo_get_curCount5(PlotFightTalkMgr/plotData)
extern "C"  int32_t PlotFightTalkMgr_ilo_get_curCount5_m2852764861 (Il2CppObject * __this /* static, unused */, plotData_t645270205 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlotFightTalkMgr::ilo_get_isDeath6(PlotFightTalkMgr/plotData)
extern "C"  bool PlotFightTalkMgr_ilo_get_isDeath6_m3659057203 (Il2CppObject * __this /* static, unused */, plotData_t645270205 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject PlotFightTalkMgr::ilo_Instantiate7(UnityEngine.GameObject)
extern "C"  GameObject_t3674682005 * PlotFightTalkMgr_ilo_Instantiate7_m2281641907 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject PlotFightTalkMgr::ilo_get_parentGO8(PlotFightTalkMgr)
extern "C"  GameObject_t3674682005 * PlotFightTalkMgr_ilo_get_parentGO8_m4275109748 (Il2CppObject * __this /* static, unused */, PlotFightTalkMgr_t866599741 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr::ilo_Clear9(PlotFightTalk)
extern "C"  void PlotFightTalkMgr_ilo_Clear9_m1075611346 (Il2CppObject * __this /* static, unused */, PlotFightTalk_t789215451 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] PlotFightTalkMgr::ilo_GetArticle_Int10(System.String)
extern "C"  Int32U5BU5D_t3230847821* PlotFightTalkMgr_ilo_GetArticle_Int10_m40526062 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlotFightTalkMgr::ilo_IsNoHaveNpc11(PlotFightTalkMgr,System.Collections.Generic.List`1<System.Int32>,System.Int32[])
extern "C"  bool PlotFightTalkMgr_ilo_IsNoHaveNpc11_m1686566324 (Il2CppObject * __this /* static, unused */, PlotFightTalkMgr_t866599741 * ____this0, List_1_t2522024052 * ___npcs1, Int32U5BU5D_t3230847821* ___haveNpcs2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlotFightTalkMgr::ilo_get_id12(CombatEntity)
extern "C"  int32_t PlotFightTalkMgr_ilo_get_id12_m2251589045 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlotFightTalkMgr::ilo_get_elite13(CombatEntity)
extern "C"  bool PlotFightTalkMgr_ilo_get_elite13_m1245998282 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
