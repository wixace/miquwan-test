﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3962901666MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m2266259534(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3378799732 *, Dictionary_2_t383226723 *, const MethodInfo*))ValueCollection__ctor_m1322936629_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m81107492(__this, ___item0, method) ((  void (*) (ValueCollection_t3378799732 *, ProceduralTile_t3586714437 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m189225053_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m1616263405(__this, method) ((  void (*) (ValueCollection_t3378799732 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3762944934_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m41753986(__this, ___item0, method) ((  bool (*) (ValueCollection_t3378799732 *, ProceduralTile_t3586714437 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3159435437_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m363048615(__this, ___item0, method) ((  bool (*) (ValueCollection_t3378799732 *, ProceduralTile_t3586714437 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m214337682_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m635443067(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3378799732 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2368026598_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2548061617(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3378799732 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3972973866_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3376786156(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3378799732 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3407700665_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2611897141(__this, method) ((  bool (*) (ValueCollection_t3378799732 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1434611296_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4092596373(__this, method) ((  bool (*) (ValueCollection_t3378799732 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4000531520_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3953094465(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3378799732 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1893444914_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1116930325(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3378799732 *, ProceduralTileU5BU5D_t1428483592*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m637061180_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::GetEnumerator()
#define ValueCollection_GetEnumerator_m4104224312(__this, method) ((  Enumerator_t2610027427  (*) (ValueCollection_t3378799732 *, const MethodInfo*))ValueCollection_GetEnumerator_m2862149669_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::get_Count()
#define ValueCollection_get_Count_m2468720347(__this, method) ((  int32_t (*) (ValueCollection_t3378799732 *, const MethodInfo*))ValueCollection_get_Count_m3583443578_gshared)(__this, method)
