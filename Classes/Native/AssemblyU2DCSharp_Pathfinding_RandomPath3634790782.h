﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.PathNode
struct PathNode_t417131581;
// System.Random
struct Random_t4255898871;

#include "AssemblyU2DCSharp_Pathfinding_ABPath1187561148.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RandomPath
struct  RandomPath_t3634790782  : public ABPath_t1187561148
{
public:
	// System.Int32 Pathfinding.RandomPath::searchLength
	int32_t ___searchLength_52;
	// System.Int32 Pathfinding.RandomPath::spread
	int32_t ___spread_53;
	// System.Boolean Pathfinding.RandomPath::uniform
	bool ___uniform_54;
	// System.Single Pathfinding.RandomPath::aimStrength
	float ___aimStrength_55;
	// Pathfinding.PathNode Pathfinding.RandomPath::chosenNodeR
	PathNode_t417131581 * ___chosenNodeR_56;
	// Pathfinding.PathNode Pathfinding.RandomPath::maxGScoreNodeR
	PathNode_t417131581 * ___maxGScoreNodeR_57;
	// System.Int32 Pathfinding.RandomPath::maxGScore
	int32_t ___maxGScore_58;
	// UnityEngine.Vector3 Pathfinding.RandomPath::aim
	Vector3_t4282066566  ___aim_59;
	// System.Int32 Pathfinding.RandomPath::nodesEvaluatedRep
	int32_t ___nodesEvaluatedRep_60;
	// System.Random Pathfinding.RandomPath::rnd
	Random_t4255898871 * ___rnd_61;

public:
	inline static int32_t get_offset_of_searchLength_52() { return static_cast<int32_t>(offsetof(RandomPath_t3634790782, ___searchLength_52)); }
	inline int32_t get_searchLength_52() const { return ___searchLength_52; }
	inline int32_t* get_address_of_searchLength_52() { return &___searchLength_52; }
	inline void set_searchLength_52(int32_t value)
	{
		___searchLength_52 = value;
	}

	inline static int32_t get_offset_of_spread_53() { return static_cast<int32_t>(offsetof(RandomPath_t3634790782, ___spread_53)); }
	inline int32_t get_spread_53() const { return ___spread_53; }
	inline int32_t* get_address_of_spread_53() { return &___spread_53; }
	inline void set_spread_53(int32_t value)
	{
		___spread_53 = value;
	}

	inline static int32_t get_offset_of_uniform_54() { return static_cast<int32_t>(offsetof(RandomPath_t3634790782, ___uniform_54)); }
	inline bool get_uniform_54() const { return ___uniform_54; }
	inline bool* get_address_of_uniform_54() { return &___uniform_54; }
	inline void set_uniform_54(bool value)
	{
		___uniform_54 = value;
	}

	inline static int32_t get_offset_of_aimStrength_55() { return static_cast<int32_t>(offsetof(RandomPath_t3634790782, ___aimStrength_55)); }
	inline float get_aimStrength_55() const { return ___aimStrength_55; }
	inline float* get_address_of_aimStrength_55() { return &___aimStrength_55; }
	inline void set_aimStrength_55(float value)
	{
		___aimStrength_55 = value;
	}

	inline static int32_t get_offset_of_chosenNodeR_56() { return static_cast<int32_t>(offsetof(RandomPath_t3634790782, ___chosenNodeR_56)); }
	inline PathNode_t417131581 * get_chosenNodeR_56() const { return ___chosenNodeR_56; }
	inline PathNode_t417131581 ** get_address_of_chosenNodeR_56() { return &___chosenNodeR_56; }
	inline void set_chosenNodeR_56(PathNode_t417131581 * value)
	{
		___chosenNodeR_56 = value;
		Il2CppCodeGenWriteBarrier(&___chosenNodeR_56, value);
	}

	inline static int32_t get_offset_of_maxGScoreNodeR_57() { return static_cast<int32_t>(offsetof(RandomPath_t3634790782, ___maxGScoreNodeR_57)); }
	inline PathNode_t417131581 * get_maxGScoreNodeR_57() const { return ___maxGScoreNodeR_57; }
	inline PathNode_t417131581 ** get_address_of_maxGScoreNodeR_57() { return &___maxGScoreNodeR_57; }
	inline void set_maxGScoreNodeR_57(PathNode_t417131581 * value)
	{
		___maxGScoreNodeR_57 = value;
		Il2CppCodeGenWriteBarrier(&___maxGScoreNodeR_57, value);
	}

	inline static int32_t get_offset_of_maxGScore_58() { return static_cast<int32_t>(offsetof(RandomPath_t3634790782, ___maxGScore_58)); }
	inline int32_t get_maxGScore_58() const { return ___maxGScore_58; }
	inline int32_t* get_address_of_maxGScore_58() { return &___maxGScore_58; }
	inline void set_maxGScore_58(int32_t value)
	{
		___maxGScore_58 = value;
	}

	inline static int32_t get_offset_of_aim_59() { return static_cast<int32_t>(offsetof(RandomPath_t3634790782, ___aim_59)); }
	inline Vector3_t4282066566  get_aim_59() const { return ___aim_59; }
	inline Vector3_t4282066566 * get_address_of_aim_59() { return &___aim_59; }
	inline void set_aim_59(Vector3_t4282066566  value)
	{
		___aim_59 = value;
	}

	inline static int32_t get_offset_of_nodesEvaluatedRep_60() { return static_cast<int32_t>(offsetof(RandomPath_t3634790782, ___nodesEvaluatedRep_60)); }
	inline int32_t get_nodesEvaluatedRep_60() const { return ___nodesEvaluatedRep_60; }
	inline int32_t* get_address_of_nodesEvaluatedRep_60() { return &___nodesEvaluatedRep_60; }
	inline void set_nodesEvaluatedRep_60(int32_t value)
	{
		___nodesEvaluatedRep_60 = value;
	}

	inline static int32_t get_offset_of_rnd_61() { return static_cast<int32_t>(offsetof(RandomPath_t3634790782, ___rnd_61)); }
	inline Random_t4255898871 * get_rnd_61() const { return ___rnd_61; }
	inline Random_t4255898871 ** get_address_of_rnd_61() { return &___rnd_61; }
	inline void set_rnd_61(Random_t4255898871 * value)
	{
		___rnd_61 = value;
		Il2CppCodeGenWriteBarrier(&___rnd_61, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
