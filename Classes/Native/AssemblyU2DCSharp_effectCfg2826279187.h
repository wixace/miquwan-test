﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// effectCfg
struct  effectCfg_t2826279187  : public CsCfgBase_t69924517
{
public:
	// System.Int32 effectCfg::id
	int32_t ___id_0;
	// System.String effectCfg::srcname
	String_t* ___srcname_1;
	// System.Int32 effectCfg::type
	int32_t ___type_2;
	// System.Boolean effectCfg::loop
	bool ___loop_3;
	// System.Single effectCfg::lifetime
	float ___lifetime_4;
	// System.Single effectCfg::zoom_rate
	float ___zoom_rate_5;
	// System.String effectCfg::offset
	String_t* ___offset_6;
	// System.String effectCfg::binding
	String_t* ___binding_7;
	// System.Int32 effectCfg::target
	int32_t ___target_8;
	// System.Boolean effectCfg::isflow
	bool ___isflow_9;
	// System.Int32 effectCfg::isparabola
	int32_t ___isparabola_10;
	// System.Single effectCfg::parabolaHight
	float ___parabolaHight_11;
	// System.Single effectCfg::showtime
	float ___showtime_12;
	// System.Boolean effectCfg::isoffset
	bool ___isoffset_13;
	// System.Boolean effectCfg::isline
	bool ___isline_14;
	// System.Int32 effectCfg::common_attack
	int32_t ___common_attack_15;
	// System.Boolean effectCfg::isTrail
	bool ___isTrail_16;
	// System.Single effectCfg::trailtime
	float ___trailtime_17;
	// System.Single effectCfg::clear_delayTime
	float ___clear_delayTime_18;
	// System.Boolean effectCfg::scale
	bool ___scale_19;
	// System.Int32 effectCfg::sound_id
	int32_t ___sound_id_20;
	// System.Single effectCfg::sound_delay
	float ___sound_delay_21;
	// System.Boolean effectCfg::isShowInBlack
	bool ___isShowInBlack_22;
	// System.Int32 effectCfg::hero_play_speed
	int32_t ___hero_play_speed_23;
	// System.Int32 effectCfg::MultipleType
	int32_t ___MultipleType_24;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_srcname_1() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___srcname_1)); }
	inline String_t* get_srcname_1() const { return ___srcname_1; }
	inline String_t** get_address_of_srcname_1() { return &___srcname_1; }
	inline void set_srcname_1(String_t* value)
	{
		___srcname_1 = value;
		Il2CppCodeGenWriteBarrier(&___srcname_1, value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_loop_3() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___loop_3)); }
	inline bool get_loop_3() const { return ___loop_3; }
	inline bool* get_address_of_loop_3() { return &___loop_3; }
	inline void set_loop_3(bool value)
	{
		___loop_3 = value;
	}

	inline static int32_t get_offset_of_lifetime_4() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___lifetime_4)); }
	inline float get_lifetime_4() const { return ___lifetime_4; }
	inline float* get_address_of_lifetime_4() { return &___lifetime_4; }
	inline void set_lifetime_4(float value)
	{
		___lifetime_4 = value;
	}

	inline static int32_t get_offset_of_zoom_rate_5() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___zoom_rate_5)); }
	inline float get_zoom_rate_5() const { return ___zoom_rate_5; }
	inline float* get_address_of_zoom_rate_5() { return &___zoom_rate_5; }
	inline void set_zoom_rate_5(float value)
	{
		___zoom_rate_5 = value;
	}

	inline static int32_t get_offset_of_offset_6() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___offset_6)); }
	inline String_t* get_offset_6() const { return ___offset_6; }
	inline String_t** get_address_of_offset_6() { return &___offset_6; }
	inline void set_offset_6(String_t* value)
	{
		___offset_6 = value;
		Il2CppCodeGenWriteBarrier(&___offset_6, value);
	}

	inline static int32_t get_offset_of_binding_7() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___binding_7)); }
	inline String_t* get_binding_7() const { return ___binding_7; }
	inline String_t** get_address_of_binding_7() { return &___binding_7; }
	inline void set_binding_7(String_t* value)
	{
		___binding_7 = value;
		Il2CppCodeGenWriteBarrier(&___binding_7, value);
	}

	inline static int32_t get_offset_of_target_8() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___target_8)); }
	inline int32_t get_target_8() const { return ___target_8; }
	inline int32_t* get_address_of_target_8() { return &___target_8; }
	inline void set_target_8(int32_t value)
	{
		___target_8 = value;
	}

	inline static int32_t get_offset_of_isflow_9() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___isflow_9)); }
	inline bool get_isflow_9() const { return ___isflow_9; }
	inline bool* get_address_of_isflow_9() { return &___isflow_9; }
	inline void set_isflow_9(bool value)
	{
		___isflow_9 = value;
	}

	inline static int32_t get_offset_of_isparabola_10() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___isparabola_10)); }
	inline int32_t get_isparabola_10() const { return ___isparabola_10; }
	inline int32_t* get_address_of_isparabola_10() { return &___isparabola_10; }
	inline void set_isparabola_10(int32_t value)
	{
		___isparabola_10 = value;
	}

	inline static int32_t get_offset_of_parabolaHight_11() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___parabolaHight_11)); }
	inline float get_parabolaHight_11() const { return ___parabolaHight_11; }
	inline float* get_address_of_parabolaHight_11() { return &___parabolaHight_11; }
	inline void set_parabolaHight_11(float value)
	{
		___parabolaHight_11 = value;
	}

	inline static int32_t get_offset_of_showtime_12() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___showtime_12)); }
	inline float get_showtime_12() const { return ___showtime_12; }
	inline float* get_address_of_showtime_12() { return &___showtime_12; }
	inline void set_showtime_12(float value)
	{
		___showtime_12 = value;
	}

	inline static int32_t get_offset_of_isoffset_13() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___isoffset_13)); }
	inline bool get_isoffset_13() const { return ___isoffset_13; }
	inline bool* get_address_of_isoffset_13() { return &___isoffset_13; }
	inline void set_isoffset_13(bool value)
	{
		___isoffset_13 = value;
	}

	inline static int32_t get_offset_of_isline_14() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___isline_14)); }
	inline bool get_isline_14() const { return ___isline_14; }
	inline bool* get_address_of_isline_14() { return &___isline_14; }
	inline void set_isline_14(bool value)
	{
		___isline_14 = value;
	}

	inline static int32_t get_offset_of_common_attack_15() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___common_attack_15)); }
	inline int32_t get_common_attack_15() const { return ___common_attack_15; }
	inline int32_t* get_address_of_common_attack_15() { return &___common_attack_15; }
	inline void set_common_attack_15(int32_t value)
	{
		___common_attack_15 = value;
	}

	inline static int32_t get_offset_of_isTrail_16() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___isTrail_16)); }
	inline bool get_isTrail_16() const { return ___isTrail_16; }
	inline bool* get_address_of_isTrail_16() { return &___isTrail_16; }
	inline void set_isTrail_16(bool value)
	{
		___isTrail_16 = value;
	}

	inline static int32_t get_offset_of_trailtime_17() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___trailtime_17)); }
	inline float get_trailtime_17() const { return ___trailtime_17; }
	inline float* get_address_of_trailtime_17() { return &___trailtime_17; }
	inline void set_trailtime_17(float value)
	{
		___trailtime_17 = value;
	}

	inline static int32_t get_offset_of_clear_delayTime_18() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___clear_delayTime_18)); }
	inline float get_clear_delayTime_18() const { return ___clear_delayTime_18; }
	inline float* get_address_of_clear_delayTime_18() { return &___clear_delayTime_18; }
	inline void set_clear_delayTime_18(float value)
	{
		___clear_delayTime_18 = value;
	}

	inline static int32_t get_offset_of_scale_19() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___scale_19)); }
	inline bool get_scale_19() const { return ___scale_19; }
	inline bool* get_address_of_scale_19() { return &___scale_19; }
	inline void set_scale_19(bool value)
	{
		___scale_19 = value;
	}

	inline static int32_t get_offset_of_sound_id_20() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___sound_id_20)); }
	inline int32_t get_sound_id_20() const { return ___sound_id_20; }
	inline int32_t* get_address_of_sound_id_20() { return &___sound_id_20; }
	inline void set_sound_id_20(int32_t value)
	{
		___sound_id_20 = value;
	}

	inline static int32_t get_offset_of_sound_delay_21() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___sound_delay_21)); }
	inline float get_sound_delay_21() const { return ___sound_delay_21; }
	inline float* get_address_of_sound_delay_21() { return &___sound_delay_21; }
	inline void set_sound_delay_21(float value)
	{
		___sound_delay_21 = value;
	}

	inline static int32_t get_offset_of_isShowInBlack_22() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___isShowInBlack_22)); }
	inline bool get_isShowInBlack_22() const { return ___isShowInBlack_22; }
	inline bool* get_address_of_isShowInBlack_22() { return &___isShowInBlack_22; }
	inline void set_isShowInBlack_22(bool value)
	{
		___isShowInBlack_22 = value;
	}

	inline static int32_t get_offset_of_hero_play_speed_23() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___hero_play_speed_23)); }
	inline int32_t get_hero_play_speed_23() const { return ___hero_play_speed_23; }
	inline int32_t* get_address_of_hero_play_speed_23() { return &___hero_play_speed_23; }
	inline void set_hero_play_speed_23(int32_t value)
	{
		___hero_play_speed_23 = value;
	}

	inline static int32_t get_offset_of_MultipleType_24() { return static_cast<int32_t>(offsetof(effectCfg_t2826279187, ___MultipleType_24)); }
	inline int32_t get_MultipleType_24() const { return ___MultipleType_24; }
	inline int32_t* get_address_of_MultipleType_24() { return &___MultipleType_24; }
	inline void set_MultipleType_24(int32_t value)
	{
		___MultipleType_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
