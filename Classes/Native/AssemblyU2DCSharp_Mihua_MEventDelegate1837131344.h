﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Mihua.MEventDelegate/RequestDelegate
struct RequestDelegate_t1822766829;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mihua.MEventDelegate
struct  MEventDelegate_t1837131344  : public Il2CppObject
{
public:
	// Mihua.MEventDelegate/RequestDelegate Mihua.MEventDelegate::requestDelegate
	RequestDelegate_t1822766829 * ___requestDelegate_0;

public:
	inline static int32_t get_offset_of_requestDelegate_0() { return static_cast<int32_t>(offsetof(MEventDelegate_t1837131344, ___requestDelegate_0)); }
	inline RequestDelegate_t1822766829 * get_requestDelegate_0() const { return ___requestDelegate_0; }
	inline RequestDelegate_t1822766829 ** get_address_of_requestDelegate_0() { return &___requestDelegate_0; }
	inline void set_requestDelegate_0(RequestDelegate_t1822766829 * value)
	{
		___requestDelegate_0 = value;
		Il2CppCodeGenWriteBarrier(&___requestDelegate_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
