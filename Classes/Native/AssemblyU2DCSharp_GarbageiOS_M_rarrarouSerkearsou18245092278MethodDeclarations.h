﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_rarrarouSerkearsou181
struct M_rarrarouSerkearsou181_t245092278;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_rarrarouSerkearsou18245092278.h"

// System.Void GarbageiOS.M_rarrarouSerkearsou181::.ctor()
extern "C"  void M_rarrarouSerkearsou181__ctor_m102709869 (M_rarrarouSerkearsou181_t245092278 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rarrarouSerkearsou181::M_rorwirSigoohur0(System.String[],System.Int32)
extern "C"  void M_rarrarouSerkearsou181_M_rorwirSigoohur0_m103252289 (M_rarrarouSerkearsou181_t245092278 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rarrarouSerkearsou181::M_decel1(System.String[],System.Int32)
extern "C"  void M_rarrarouSerkearsou181_M_decel1_m2127017128 (M_rarrarouSerkearsou181_t245092278 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rarrarouSerkearsou181::M_bisbalgeXuhoupir2(System.String[],System.Int32)
extern "C"  void M_rarrarouSerkearsou181_M_bisbalgeXuhoupir2_m940024251 (M_rarrarouSerkearsou181_t245092278 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rarrarouSerkearsou181::ilo_M_rorwirSigoohur01(GarbageiOS.M_rarrarouSerkearsou181,System.String[],System.Int32)
extern "C"  void M_rarrarouSerkearsou181_ilo_M_rorwirSigoohur01_m3134604463 (Il2CppObject * __this /* static, unused */, M_rarrarouSerkearsou181_t245092278 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rarrarouSerkearsou181::ilo_M_decel12(GarbageiOS.M_rarrarouSerkearsou181,System.String[],System.Int32)
extern "C"  void M_rarrarouSerkearsou181_ilo_M_decel12_m3788551739 (Il2CppObject * __this /* static, unused */, M_rarrarouSerkearsou181_t245092278 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
