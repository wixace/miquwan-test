﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3000372391.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_ExtraMesh4218029715.h"

// System.Void System.Array/InternalEnumerator`1<Pathfinding.Voxels.ExtraMesh>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2366006647_gshared (InternalEnumerator_1_t3000372391 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2366006647(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3000372391 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2366006647_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m190207689_gshared (InternalEnumerator_1_t3000372391 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m190207689(__this, method) ((  void (*) (InternalEnumerator_1_t3000372391 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m190207689_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Pathfinding.Voxels.ExtraMesh>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3167293365_gshared (InternalEnumerator_1_t3000372391 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3167293365(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3000372391 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3167293365_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.Voxels.ExtraMesh>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2164721678_gshared (InternalEnumerator_1_t3000372391 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2164721678(__this, method) ((  void (*) (InternalEnumerator_1_t3000372391 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2164721678_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.Voxels.ExtraMesh>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m773697333_gshared (InternalEnumerator_1_t3000372391 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m773697333(__this, method) ((  bool (*) (InternalEnumerator_1_t3000372391 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m773697333_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Pathfinding.Voxels.ExtraMesh>::get_Current()
extern "C"  ExtraMesh_t4218029715  InternalEnumerator_1_get_Current_m1167364286_gshared (InternalEnumerator_1_t3000372391 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1167364286(__this, method) ((  ExtraMesh_t4218029715  (*) (InternalEnumerator_1_t3000372391 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1167364286_gshared)(__this, method)
