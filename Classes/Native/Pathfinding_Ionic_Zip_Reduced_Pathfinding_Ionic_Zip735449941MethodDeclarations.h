﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Ionic.Zip.SetCompressionCallback
struct SetCompressionCallback_t735449941;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "Pathfinding_Ionic_Zip_Reduced_Pathfinding_Ionic_Zl3197845446.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void Pathfinding.Ionic.Zip.SetCompressionCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void SetCompressionCallback__ctor_m1641792390 (SetCompressionCallback_t735449941 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Ionic.Zlib.CompressionLevel Pathfinding.Ionic.Zip.SetCompressionCallback::Invoke(System.String,System.String)
extern "C"  int32_t SetCompressionCallback_Invoke_m2982684086 (SetCompressionCallback_t735449941 * __this, String_t* ___localFileName0, String_t* ___fileNameInArchive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult Pathfinding.Ionic.Zip.SetCompressionCallback::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * SetCompressionCallback_BeginInvoke_m4252911787 (SetCompressionCallback_t735449941 * __this, String_t* ___localFileName0, String_t* ___fileNameInArchive1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Ionic.Zlib.CompressionLevel Pathfinding.Ionic.Zip.SetCompressionCallback::EndInvoke(System.IAsyncResult)
extern "C"  int32_t SetCompressionCallback_EndInvoke_m2622668862 (SetCompressionCallback_t735449941 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
