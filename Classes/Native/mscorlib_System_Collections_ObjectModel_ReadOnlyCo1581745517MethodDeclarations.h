﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>
struct ReadOnlyCollection_1_t1581745517;
// System.Collections.Generic.IList`1<System.UInt32>
struct IList_1_t2719315184;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.UInt32[]
struct UInt32U5BU5D_t3230734560;
// System.Collections.Generic.IEnumerator`1<System.UInt32>
struct IEnumerator_1_t1936533030;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::.ctor(System.Collections.Generic.IList`1<T>)
extern "C"  void ReadOnlyCollection_1__ctor_m2378256824_gshared (ReadOnlyCollection_1_t1581745517 * __this, Il2CppObject* ___list0, const MethodInfo* method);
#define ReadOnlyCollection_1__ctor_m2378256824(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t1581745517 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m2378256824_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m984999842_gshared (ReadOnlyCollection_1_t1581745517 * __this, uint32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m984999842(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t1581745517 *, uint32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m984999842_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.Generic.ICollection<T>.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2476915048_gshared (ReadOnlyCollection_1_t1581745517 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2476915048(__this, method) ((  void (*) (ReadOnlyCollection_1_t1581745517 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m2476915048_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1408681993_gshared (ReadOnlyCollection_1_t1581745517 * __this, int32_t ___index0, uint32_t ___item1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1408681993(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t1581745517 *, int32_t, uint32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m1408681993_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.Generic.ICollection<T>.Remove(T)
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m479366929_gshared (ReadOnlyCollection_1_t1581745517 * __this, uint32_t ___item0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m479366929(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t1581745517 *, uint32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m479366929_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3577502159_gshared (ReadOnlyCollection_1_t1581745517 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3577502159(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1581745517 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m3577502159_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
extern "C"  uint32_t ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1446903315_gshared (ReadOnlyCollection_1_t1581745517 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1446903315(__this, ___index0, method) ((  uint32_t (*) (ReadOnlyCollection_1_t1581745517 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m1446903315_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
extern "C"  void ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2638293664_gshared (ReadOnlyCollection_1_t1581745517 * __this, int32_t ___index0, uint32_t ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2638293664(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1581745517 *, int32_t, uint32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m2638293664_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m371870046_gshared (ReadOnlyCollection_1_t1581745517 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m371870046(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1581745517 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m371870046_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3005290599_gshared (ReadOnlyCollection_1_t1581745517 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3005290599(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1581745517 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3005290599_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m512806818_gshared (ReadOnlyCollection_1_t1581745517 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m512806818(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1581745517 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m512806818_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_Add_m2077241871_gshared (ReadOnlyCollection_1_t1581745517 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Add_m2077241871(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1581745517 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m2077241871_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.Clear()
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Clear_m412189557_gshared (ReadOnlyCollection_1_t1581745517 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m412189557(__this, method) ((  void (*) (ReadOnlyCollection_1_t1581745517 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m412189557_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.Contains(System.Object)
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_Contains_m3474979229_gshared (ReadOnlyCollection_1_t1581745517 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3474979229(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1581745517 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m3474979229_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2222639335_gshared (ReadOnlyCollection_1_t1581745517 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2222639335(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1581745517 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2222639335_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Insert_m2529429394_gshared (ReadOnlyCollection_1_t1581745517 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m2529429394(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1581745517 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2529429394_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.Remove(System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_Remove_m3976248406_gshared (ReadOnlyCollection_1_t1581745517 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m3976248406(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t1581745517 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m3976248406_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.RemoveAt(System.Int32)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m295004706_gshared (ReadOnlyCollection_1_t1581745517 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m295004706(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t1581745517 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m295004706_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1504451743_gshared (ReadOnlyCollection_1_t1581745517 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1504451743(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1581745517 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1504451743_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2213340299_gshared (ReadOnlyCollection_1_t1581745517 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2213340299(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1581745517 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m2213340299_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3163972300_gshared (ReadOnlyCollection_1_t1581745517 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3163972300(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1581745517 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m3163972300_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1802998445_gshared (ReadOnlyCollection_1_t1581745517 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1802998445(__this, method) ((  bool (*) (ReadOnlyCollection_1_t1581745517 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m1802998445_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * ReadOnlyCollection_1_System_Collections_IList_get_Item_m2076827474_gshared (ReadOnlyCollection_1_t1581745517 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m2076827474(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t1581745517 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m2076827474_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void ReadOnlyCollection_1_System_Collections_IList_set_Item_m444110569_gshared (ReadOnlyCollection_1_t1581745517 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m444110569(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t1581745517 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m444110569_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::Contains(T)
extern "C"  bool ReadOnlyCollection_1_Contains_m3614654806_gshared (ReadOnlyCollection_1_t1581745517 * __this, uint32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_Contains_m3614654806(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t1581745517 *, uint32_t, const MethodInfo*))ReadOnlyCollection_1_Contains_m3614654806_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::CopyTo(T[],System.Int32)
extern "C"  void ReadOnlyCollection_1_CopyTo_m1267957970_gshared (ReadOnlyCollection_1_t1581745517 * __this, UInt32U5BU5D_t3230734560* ___array0, int32_t ___index1, const MethodInfo* method);
#define ReadOnlyCollection_1_CopyTo_m1267957970(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t1581745517 *, UInt32U5BU5D_t3230734560*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m1267957970_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::GetEnumerator()
extern "C"  Il2CppObject* ReadOnlyCollection_1_GetEnumerator_m1010255673_gshared (ReadOnlyCollection_1_t1581745517 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_GetEnumerator_m1010255673(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t1581745517 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m1010255673_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::IndexOf(T)
extern "C"  int32_t ReadOnlyCollection_1_IndexOf_m911816406_gshared (ReadOnlyCollection_1_t1581745517 * __this, uint32_t ___value0, const MethodInfo* method);
#define ReadOnlyCollection_1_IndexOf_m911816406(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t1581745517 *, uint32_t, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m911816406_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::get_Count()
extern "C"  int32_t ReadOnlyCollection_1_get_Count_m2313812325_gshared (ReadOnlyCollection_1_t1581745517 * __this, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Count_m2313812325(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t1581745517 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m2313812325_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<System.UInt32>::get_Item(System.Int32)
extern "C"  uint32_t ReadOnlyCollection_1_get_Item_m2234756435_gshared (ReadOnlyCollection_1_t1581745517 * __this, int32_t ___index0, const MethodInfo* method);
#define ReadOnlyCollection_1_get_Item_m2234756435(__this, ___index0, method) ((  uint32_t (*) (ReadOnlyCollection_1_t1581745517 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2234756435_gshared)(__this, ___index0, method)
