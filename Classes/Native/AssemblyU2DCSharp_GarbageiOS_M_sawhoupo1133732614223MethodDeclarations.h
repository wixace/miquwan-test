﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_sawhoupo113
struct M_sawhoupo113_t3732614223;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_sawhoupo1133732614223.h"

// System.Void GarbageiOS.M_sawhoupo113::.ctor()
extern "C"  void M_sawhoupo113__ctor_m1422571508 (M_sawhoupo113_t3732614223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sawhoupo113::M_yenay0(System.String[],System.Int32)
extern "C"  void M_sawhoupo113_M_yenay0_m2199312751 (M_sawhoupo113_t3732614223 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sawhoupo113::M_warurNallmaljel1(System.String[],System.Int32)
extern "C"  void M_sawhoupo113_M_warurNallmaljel1_m3140418649 (M_sawhoupo113_t3732614223 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sawhoupo113::M_sallte2(System.String[],System.Int32)
extern "C"  void M_sawhoupo113_M_sallte2_m430340106 (M_sawhoupo113_t3732614223 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sawhoupo113::M_whearcha3(System.String[],System.Int32)
extern "C"  void M_sawhoupo113_M_whearcha3_m329618739 (M_sawhoupo113_t3732614223 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sawhoupo113::ilo_M_yenay01(GarbageiOS.M_sawhoupo113,System.String[],System.Int32)
extern "C"  void M_sawhoupo113_ilo_M_yenay01_m3102031612 (Il2CppObject * __this /* static, unused */, M_sawhoupo113_t3732614223 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sawhoupo113::ilo_M_sallte22(GarbageiOS.M_sawhoupo113,System.String[],System.Int32)
extern "C"  void M_sawhoupo113_ilo_M_sallte22_m4260183472 (Il2CppObject * __this /* static, unused */, M_sawhoupo113_t3732614223 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
