﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va863609042MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1156207225(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3808955512 *, Dictionary_2_t813382503 *, const MethodInfo*))ValueCollection__ctor_m852717223_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m45299353(__this, ___item0, method) ((  void (*) (ValueCollection_t3808955512 *, KeyValuePair_2_t4287931429 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2252266667_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2046187490(__this, method) ((  void (*) (ValueCollection_t3808955512 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m962320116_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2174991149(__this, ___item0, method) ((  bool (*) (ValueCollection_t3808955512 *, KeyValuePair_2_t4287931429 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3760656475_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m4070292242(__this, ___item0, method) ((  bool (*) (ValueCollection_t3808955512 *, KeyValuePair_2_t4287931429 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m179209152_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1734664688(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3808955512 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1370173186_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1968309862(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3808955512 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3879294712_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3793320929(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3808955512 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m435170547_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m450167008(__this, method) ((  bool (*) (ValueCollection_t3808955512 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2035832334_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3240990400(__this, method) ((  bool (*) (ValueCollection_t3808955512 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m664060142_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3936902956(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3808955512 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m1334723546_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m2336587904(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3808955512 *, KeyValuePair_2U5BU5D_t204068264*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m515277102_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3328397923(__this, method) ((  Enumerator_t3040183207  (*) (ValueCollection_t3808955512 *, const MethodInfo*))ValueCollection_GetEnumerator_m3179477905_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Count()
#define ValueCollection_get_Count_m2962940166(__this, method) ((  int32_t (*) (ValueCollection_t3808955512 *, const MethodInfo*))ValueCollection_get_Count_m4201271604_gshared)(__this, method)
