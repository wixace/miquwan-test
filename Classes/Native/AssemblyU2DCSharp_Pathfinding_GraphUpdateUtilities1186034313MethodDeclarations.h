﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.GraphNode
struct GraphNode_t23612370;
// System.Collections.Generic.List`1<Pathfinding.GraphNode>
struct List_1_t1391797922;
// Pathfinding.GraphUpdateObject
struct GraphUpdateObject_t430843704;
// OnVoidDelegate
struct OnVoidDelegate_t2787120856;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphUpdateObject430843704.h"
#include "AssemblyU2DCSharp_OnVoidDelegate2787120856.h"

// System.Boolean Pathfinding.GraphUpdateUtilities::IsPathPossible(Pathfinding.GraphNode,Pathfinding.GraphNode)
extern "C"  bool GraphUpdateUtilities_IsPathPossible_m4023485912 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ___n10, GraphNode_t23612370 * ___n21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GraphUpdateUtilities::IsPathPossible(System.Collections.Generic.List`1<Pathfinding.GraphNode>)
extern "C"  bool GraphUpdateUtilities_IsPathPossible_m2262080388 (Il2CppObject * __this /* static, unused */, List_1_t1391797922 * ___nodes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GraphUpdateUtilities::UpdateGraphsNoBlock(Pathfinding.GraphUpdateObject,Pathfinding.GraphNode,Pathfinding.GraphNode,System.Boolean)
extern "C"  bool GraphUpdateUtilities_UpdateGraphsNoBlock_m2857058481 (Il2CppObject * __this /* static, unused */, GraphUpdateObject_t430843704 * ___guo0, GraphNode_t23612370 * ___node11, GraphNode_t23612370 * ___node22, bool ___alwaysRevert3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GraphUpdateUtilities::UpdateGraphsNoBlock(Pathfinding.GraphUpdateObject,System.Collections.Generic.List`1<Pathfinding.GraphNode>,System.Boolean)
extern "C"  bool GraphUpdateUtilities_UpdateGraphsNoBlock_m2666730733 (Il2CppObject * __this /* static, unused */, GraphUpdateObject_t430843704 * ___guo0, List_1_t1391797922 * ___nodes1, bool ___alwaysRevert2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.GraphUpdateUtilities::ilo_get_Walkable1(Pathfinding.GraphNode)
extern "C"  bool GraphUpdateUtilities_ilo_get_Walkable1_m3267231314 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.GraphUpdateUtilities::ilo_get_Area2(Pathfinding.GraphNode)
extern "C"  uint32_t GraphUpdateUtilities_ilo_get_Area2_m1207608258 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphUpdateUtilities::ilo_RegisterSafeUpdate3(OnVoidDelegate)
extern "C"  void GraphUpdateUtilities_ilo_RegisterSafeUpdate3_m2485032907 (Il2CppObject * __this /* static, unused */, OnVoidDelegate_t2787120856 * ___callback0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
