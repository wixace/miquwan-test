﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginGameFan
struct PluginGameFan_t3898102510;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// Mihua.SDK.EnterGameInfo
struct EnterGameInfo_t1897532954;
// System.Action
struct Action_t3771233898;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_PluginGameFan3898102510.h"
#include "AssemblyU2DCSharp_FloatTextMgr630384591.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"

// System.Void PluginGameFan::.ctor()
extern "C"  void PluginGameFan__ctor_m2212854333 (PluginGameFan_t3898102510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::Init()
extern "C"  void PluginGameFan_Init_m300013687 (PluginGameFan_t3898102510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginGameFan::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginGameFan_ReqSDKHttpLogin_m564259822 (PluginGameFan_t3898102510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginGameFan::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginGameFan_IsLoginSuccess_m1087473038 (PluginGameFan_t3898102510 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::OpenUserLogin()
extern "C"  void PluginGameFan_OpenUserLogin_m4191358863 (PluginGameFan_t3898102510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::UserPay(CEvent.ZEvent)
extern "C"  void PluginGameFan_UserPay_m4036605187 (PluginGameFan_t3898102510 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginGameFan_SignCallBack_m3105573916 (PluginGameFan_t3898102510 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginGameFan::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginGameFan_BuildOrderParam2WWWForm_m552468102 (PluginGameFan_t3898102510 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::CreateRole(CEvent.ZEvent)
extern "C"  void PluginGameFan_CreateRole_m1850942242 (PluginGameFan_t3898102510 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::EnterGame(CEvent.ZEvent)
extern "C"  void PluginGameFan_EnterGame_m3038663126 (PluginGameFan_t3898102510 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginGameFan_RoleUpgrade_m4207933178 (PluginGameFan_t3898102510 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::OnLoginSuccess(System.String)
extern "C"  void PluginGameFan_OnLoginSuccess_m1469937026 (PluginGameFan_t3898102510 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::OnLogoutSuccess(System.String)
extern "C"  void PluginGameFan_OnLogoutSuccess_m2621917517 (PluginGameFan_t3898102510 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::OnLogout(System.String)
extern "C"  void PluginGameFan_OnLogout_m2319598226 (PluginGameFan_t3898102510 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::initSdk(System.String)
extern "C"  void PluginGameFan_initSdk_m3282944925 (PluginGameFan_t3898102510 * __this, String_t* ___gameId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::login()
extern "C"  void PluginGameFan_login_m1734869156 (PluginGameFan_t3898102510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::logout()
extern "C"  void PluginGameFan_logout_m2247158993 (PluginGameFan_t3898102510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::updateRoleInfo(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginGameFan_updateRoleInfo_m3297096758 (PluginGameFan_t3898102510 * __this, String_t* ___dataType0, String_t* ___serverID1, String_t* ___serverName2, String_t* ___roleID3, String_t* ___roleName4, String_t* ___roleLevel5, String_t* ___roleVip6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginGameFan_pay_m262844191 (PluginGameFan_t3898102510 * __this, String_t* ___roleId0, String_t* ___userId1, String_t* ___serverId2, String_t* ___amount3, String_t* ___serverName4, String_t* ___orderId5, String_t* ___productName6, String_t* ___productDes7, String_t* ___sign8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::<OnLogoutSuccess>m__420()
extern "C"  void PluginGameFan_U3COnLogoutSuccessU3Em__420_m686993760 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::<OnLogout>m__421()
extern "C"  void PluginGameFan_U3COnLogoutU3Em__421_m945945926 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginGameFan_ilo_AddEventListener1_m2814714647 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginGameFan::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginGameFan_ilo_get_Instance2_m2593757017 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::ilo_initSdk3(PluginGameFan,System.String)
extern "C"  void PluginGameFan_ilo_initSdk3_m2740491773 (Il2CppObject * __this /* static, unused */, PluginGameFan_t3898102510 * ____this0, String_t* ___gameId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::ilo_FloatText4(FloatTextMgr,FLOAT_TEXT_ID,System.Int32,System.Object[])
extern "C"  void PluginGameFan_ilo_FloatText4_m3027150443 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, int32_t ___textId1, int32_t ___offsetY2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginGameFan::ilo_BuildOrderParam2WWWForm5(PluginGameFan,Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginGameFan_ilo_BuildOrderParam2WWWForm5_m1684327162 (Il2CppObject * __this /* static, unused */, PluginGameFan_t3898102510 * ____this0, PayInfo_t1775308120 * ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::ilo_Log6(System.Object,System.Boolean)
extern "C"  void PluginGameFan_ilo_Log6_m666685109 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken PluginGameFan::ilo_get_Item7(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * PluginGameFan_ilo_get_Item7_m248323674 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.EnterGameInfo PluginGameFan::ilo_Parse8(System.Object[])
extern "C"  EnterGameInfo_t1897532954 * PluginGameFan_ilo_Parse8_m1589118616 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::ilo_updateRoleInfo9(PluginGameFan,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginGameFan_ilo_updateRoleInfo9_m645892308 (Il2CppObject * __this /* static, unused */, PluginGameFan_t3898102510 * ____this0, String_t* ___dataType1, String_t* ___serverID2, String_t* ___serverName3, String_t* ___roleID4, String_t* ___roleName5, String_t* ___roleLevel6, String_t* ___roleVip7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::ilo_logout10(PluginGameFan)
extern "C"  void PluginGameFan_ilo_logout10_m2115290817 (Il2CppObject * __this /* static, unused */, PluginGameFan_t3898102510 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::ilo_Logout11(System.Action)
extern "C"  void PluginGameFan_ilo_Logout11_m613548409 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGameFan::ilo_ReqSDKHttpLogin12(PluginsSdkMgr)
extern "C"  void PluginGameFan_ilo_ReqSDKHttpLogin12_m161437446 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
