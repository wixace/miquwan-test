﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.MeshRenderer
struct MeshRenderer_t2804666580;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GhostItem
struct  GhostItem_t218864162  : public MonoBehaviour_t667441552
{
public:
	// System.Single GhostItem::duration
	float ___duration_2;
	// System.Single GhostItem::deleteTime
	float ___deleteTime_3;
	// UnityEngine.MeshRenderer GhostItem::meshRenderer
	MeshRenderer_t2804666580 * ___meshRenderer_4;

public:
	inline static int32_t get_offset_of_duration_2() { return static_cast<int32_t>(offsetof(GhostItem_t218864162, ___duration_2)); }
	inline float get_duration_2() const { return ___duration_2; }
	inline float* get_address_of_duration_2() { return &___duration_2; }
	inline void set_duration_2(float value)
	{
		___duration_2 = value;
	}

	inline static int32_t get_offset_of_deleteTime_3() { return static_cast<int32_t>(offsetof(GhostItem_t218864162, ___deleteTime_3)); }
	inline float get_deleteTime_3() const { return ___deleteTime_3; }
	inline float* get_address_of_deleteTime_3() { return &___deleteTime_3; }
	inline void set_deleteTime_3(float value)
	{
		___deleteTime_3 = value;
	}

	inline static int32_t get_offset_of_meshRenderer_4() { return static_cast<int32_t>(offsetof(GhostItem_t218864162, ___meshRenderer_4)); }
	inline MeshRenderer_t2804666580 * get_meshRenderer_4() const { return ___meshRenderer_4; }
	inline MeshRenderer_t2804666580 ** get_address_of_meshRenderer_4() { return &___meshRenderer_4; }
	inline void set_meshRenderer_4(MeshRenderer_t2804666580 * value)
	{
		___meshRenderer_4 = value;
		Il2CppCodeGenWriteBarrier(&___meshRenderer_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
