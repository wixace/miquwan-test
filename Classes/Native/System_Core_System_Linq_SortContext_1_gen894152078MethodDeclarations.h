﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.SortContext`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct SortContext_1_t894152078;

#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Linq_SortDirection313822039.h"

// System.Void System.Linq.SortContext`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Linq.SortDirection,System.Linq.SortContext`1<TElement>)
extern "C"  void SortContext_1__ctor_m2350296668_gshared (SortContext_1_t894152078 * __this, int32_t ___direction0, SortContext_1_t894152078 * ___child_context1, const MethodInfo* method);
#define SortContext_1__ctor_m2350296668(__this, ___direction0, ___child_context1, method) ((  void (*) (SortContext_1_t894152078 *, int32_t, SortContext_1_t894152078 *, const MethodInfo*))SortContext_1__ctor_m2350296668_gshared)(__this, ___direction0, ___child_context1, method)
