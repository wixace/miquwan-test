﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_LightGenerated
struct UnityEngine_LightGenerated_t4146417793;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_LightGenerated::.ctor()
extern "C"  void UnityEngine_LightGenerated__ctor_m913250618 (UnityEngine_LightGenerated_t4146417793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LightGenerated::Light_Light1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LightGenerated_Light_Light1_m1875099474 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::Light_type(JSVCall)
extern "C"  void UnityEngine_LightGenerated_Light_type_m2161227514 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::Light_color(JSVCall)
extern "C"  void UnityEngine_LightGenerated_Light_color_m3220960213 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::Light_intensity(JSVCall)
extern "C"  void UnityEngine_LightGenerated_Light_intensity_m593067013 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::Light_bounceIntensity(JSVCall)
extern "C"  void UnityEngine_LightGenerated_Light_bounceIntensity_m1652741933 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::Light_shadows(JSVCall)
extern "C"  void UnityEngine_LightGenerated_Light_shadows_m3478165861 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::Light_shadowStrength(JSVCall)
extern "C"  void UnityEngine_LightGenerated_Light_shadowStrength_m545220371 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::Light_shadowBias(JSVCall)
extern "C"  void UnityEngine_LightGenerated_Light_shadowBias_m3518980763 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::Light_shadowNormalBias(JSVCall)
extern "C"  void UnityEngine_LightGenerated_Light_shadowNormalBias_m887704180 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::Light_shadowNearPlane(JSVCall)
extern "C"  void UnityEngine_LightGenerated_Light_shadowNearPlane_m1834970628 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::Light_range(JSVCall)
extern "C"  void UnityEngine_LightGenerated_Light_range_m4048772923 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::Light_spotAngle(JSVCall)
extern "C"  void UnityEngine_LightGenerated_Light_spotAngle_m630237447 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::Light_cookieSize(JSVCall)
extern "C"  void UnityEngine_LightGenerated_Light_cookieSize_m4087722895 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::Light_cookie(JSVCall)
extern "C"  void UnityEngine_LightGenerated_Light_cookie_m231147408 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::Light_flare(JSVCall)
extern "C"  void UnityEngine_LightGenerated_Light_flare_m2623372234 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::Light_renderMode(JSVCall)
extern "C"  void UnityEngine_LightGenerated_Light_renderMode_m3012401723 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::Light_alreadyLightmapped(JSVCall)
extern "C"  void UnityEngine_LightGenerated_Light_alreadyLightmapped_m3727113283 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::Light_cullingMask(JSVCall)
extern "C"  void UnityEngine_LightGenerated_Light_cullingMask_m2301008284 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::Light_commandBufferCount(JSVCall)
extern "C"  void UnityEngine_LightGenerated_Light_commandBufferCount_m3593997456 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LightGenerated::Light_AddCommandBuffer__LightEvent__CommandBuffer(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LightGenerated_Light_AddCommandBuffer__LightEvent__CommandBuffer_m1524863912 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LightGenerated::Light_GetCommandBuffers__LightEvent(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LightGenerated_Light_GetCommandBuffers__LightEvent_m583102381 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LightGenerated::Light_RemoveAllCommandBuffers(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LightGenerated_Light_RemoveAllCommandBuffers_m2112394160 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LightGenerated::Light_RemoveCommandBuffer__LightEvent__CommandBuffer(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LightGenerated_Light_RemoveCommandBuffer__LightEvent__CommandBuffer_m2474731479 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LightGenerated::Light_RemoveCommandBuffers__LightEvent(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LightGenerated_Light_RemoveCommandBuffers__LightEvent_m1389476199 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_LightGenerated::Light_GetLights__LightType__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_LightGenerated_Light_GetLights__LightType__Int32_m3760396508 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::__Register()
extern "C"  void UnityEngine_LightGenerated___Register_m3904051149 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LightGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_LightGenerated_ilo_getObject1_m531747992 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LightGenerated::ilo_getEnum2(System.Int32)
extern "C"  int32_t UnityEngine_LightGenerated_ilo_getEnum2_m2745654743 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_LightGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_LightGenerated_ilo_setObject3_m2440780946 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::ilo_setSingle4(System.Int32,System.Single)
extern "C"  void UnityEngine_LightGenerated_ilo_setSingle4_m3130774141 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_LightGenerated::ilo_getSingle5(System.Int32)
extern "C"  float UnityEngine_LightGenerated_ilo_getSingle5_m195490353 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_LightGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_LightGenerated_ilo_getObject6_m837782842 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::ilo_setBooleanS7(System.Int32,System.Boolean)
extern "C"  void UnityEngine_LightGenerated_ilo_setBooleanS7_m3790063077 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_LightGenerated::ilo_moveSaveID2Arr8(System.Int32)
extern "C"  void UnityEngine_LightGenerated_ilo_moveSaveID2Arr8_m2531457686 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
