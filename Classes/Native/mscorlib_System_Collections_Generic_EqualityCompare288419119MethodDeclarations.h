﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<PushType>
struct DefaultComparer_t288419119;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PushType1840642452.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<PushType>::.ctor()
extern "C"  void DefaultComparer__ctor_m1116639104_gshared (DefaultComparer_t288419119 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1116639104(__this, method) ((  void (*) (DefaultComparer_t288419119 *, const MethodInfo*))DefaultComparer__ctor_m1116639104_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<PushType>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2328632179_gshared (DefaultComparer_t288419119 * __this, int32_t ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2328632179(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t288419119 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m2328632179_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<PushType>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m596483477_gshared (DefaultComparer_t288419119 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m596483477(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t288419119 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m596483477_gshared)(__this, ___x0, ___y1, method)
