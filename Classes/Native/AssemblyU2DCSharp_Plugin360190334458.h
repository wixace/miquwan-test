﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Plugin360
struct  Plugin360_t190334458  : public MonoBehaviour_t667441552
{
public:
	// System.String Plugin360::token
	String_t* ___token_2;

public:
	inline static int32_t get_offset_of_token_2() { return static_cast<int32_t>(offsetof(Plugin360_t190334458, ___token_2)); }
	inline String_t* get_token_2() const { return ___token_2; }
	inline String_t** get_address_of_token_2() { return &___token_2; }
	inline void set_token_2(String_t* value)
	{
		___token_2 = value;
		Il2CppCodeGenWriteBarrier(&___token_2, value);
	}
};

struct Plugin360_t190334458_StaticFields
{
public:
	// System.Action Plugin360::<>f__am$cache1
	Action_t3771233898 * ___U3CU3Ef__amU24cache1_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(Plugin360_t190334458_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
