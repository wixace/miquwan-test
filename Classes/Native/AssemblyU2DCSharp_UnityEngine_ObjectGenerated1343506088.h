﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MethodID
struct MethodID_t3916401116;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine_ObjectGenerated
struct  UnityEngine_ObjectGenerated_t1343506088  : public Il2CppObject
{
public:

public:
};

struct UnityEngine_ObjectGenerated_t1343506088_StaticFields
{
public:
	// MethodID UnityEngine_ObjectGenerated::methodID12
	MethodID_t3916401116 * ___methodID12_0;
	// MethodID UnityEngine_ObjectGenerated::methodID14
	MethodID_t3916401116 * ___methodID14_1;
	// MethodID UnityEngine_ObjectGenerated::methodID17
	MethodID_t3916401116 * ___methodID17_2;

public:
	inline static int32_t get_offset_of_methodID12_0() { return static_cast<int32_t>(offsetof(UnityEngine_ObjectGenerated_t1343506088_StaticFields, ___methodID12_0)); }
	inline MethodID_t3916401116 * get_methodID12_0() const { return ___methodID12_0; }
	inline MethodID_t3916401116 ** get_address_of_methodID12_0() { return &___methodID12_0; }
	inline void set_methodID12_0(MethodID_t3916401116 * value)
	{
		___methodID12_0 = value;
		Il2CppCodeGenWriteBarrier(&___methodID12_0, value);
	}

	inline static int32_t get_offset_of_methodID14_1() { return static_cast<int32_t>(offsetof(UnityEngine_ObjectGenerated_t1343506088_StaticFields, ___methodID14_1)); }
	inline MethodID_t3916401116 * get_methodID14_1() const { return ___methodID14_1; }
	inline MethodID_t3916401116 ** get_address_of_methodID14_1() { return &___methodID14_1; }
	inline void set_methodID14_1(MethodID_t3916401116 * value)
	{
		___methodID14_1 = value;
		Il2CppCodeGenWriteBarrier(&___methodID14_1, value);
	}

	inline static int32_t get_offset_of_methodID17_2() { return static_cast<int32_t>(offsetof(UnityEngine_ObjectGenerated_t1343506088_StaticFields, ___methodID17_2)); }
	inline MethodID_t3916401116 * get_methodID17_2() const { return ___methodID17_2; }
	inline MethodID_t3916401116 ** get_address_of_methodID17_2() { return &___methodID17_2; }
	inline void set_methodID17_2(MethodID_t3916401116 * value)
	{
		___methodID17_2 = value;
		Il2CppCodeGenWriteBarrier(&___methodID17_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
