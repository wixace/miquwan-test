﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_foose213
struct M_foose213_t3005548172;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_foose2133005548172.h"

// System.Void GarbageiOS.M_foose213::.ctor()
extern "C"  void M_foose213__ctor_m809205031 (M_foose213_t3005548172 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_foose213::M_fapairCismelsoo0(System.String[],System.Int32)
extern "C"  void M_foose213_M_fapairCismelsoo0_m449312671 (M_foose213_t3005548172 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_foose213::M_tasayco1(System.String[],System.Int32)
extern "C"  void M_foose213_M_tasayco1_m3063327725 (M_foose213_t3005548172 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_foose213::M_daifugallJusel2(System.String[],System.Int32)
extern "C"  void M_foose213_M_daifugallJusel2_m1711500962 (M_foose213_t3005548172 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_foose213::M_mojirster3(System.String[],System.Int32)
extern "C"  void M_foose213_M_mojirster3_m2262403354 (M_foose213_t3005548172 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_foose213::M_derxacas4(System.String[],System.Int32)
extern "C"  void M_foose213_M_derxacas4_m553603619 (M_foose213_t3005548172 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_foose213::M_hearcawkear5(System.String[],System.Int32)
extern "C"  void M_foose213_M_hearcawkear5_m1021988613 (M_foose213_t3005548172 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_foose213::M_sasrurpePugiskas6(System.String[],System.Int32)
extern "C"  void M_foose213_M_sasrurpePugiskas6_m2132428048 (M_foose213_t3005548172 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_foose213::ilo_M_daifugallJusel21(GarbageiOS.M_foose213,System.String[],System.Int32)
extern "C"  void M_foose213_ilo_M_daifugallJusel21_m2208848458 (Il2CppObject * __this /* static, unused */, M_foose213_t3005548172 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_foose213::ilo_M_mojirster32(GarbageiOS.M_foose213,System.String[],System.Int32)
extern "C"  void M_foose213_ilo_M_mojirster32_m4000002701 (Il2CppObject * __this /* static, unused */, M_foose213_t3005548172 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_foose213::ilo_M_derxacas43(GarbageiOS.M_foose213,System.String[],System.Int32)
extern "C"  void M_foose213_ilo_M_derxacas43_m171944683 (Il2CppObject * __this /* static, unused */, M_foose213_t3005548172 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
