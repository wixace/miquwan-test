﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Vector2>
struct U3CGetEnumeratorU3Ec__Iterator29_t1734063026;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Vector2>::.ctor()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29__ctor_m529383041_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1734063026 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29__ctor_m529383041(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t1734063026 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29__ctor_m529383041_gshared)(__this, method)
// T BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Vector2>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C"  Vector2_t4282066565  U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1958545578_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1734063026 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1958545578(__this, method) ((  Vector2_t4282066565  (*) (U3CGetEnumeratorU3Ec__Iterator29_t1734063026 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m1958545578_gshared)(__this, method)
// System.Object BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m1515825359_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1734063026 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m1515825359(__this, method) ((  Il2CppObject * (*) (U3CGetEnumeratorU3Ec__Iterator29_t1734063026 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_System_Collections_IEnumerator_get_Current_m1515825359_gshared)(__this, method)
// System.Boolean BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Vector2>::MoveNext()
extern "C"  bool U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m2575521979_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1734063026 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m2575521979(__this, method) ((  bool (*) (U3CGetEnumeratorU3Ec__Iterator29_t1734063026 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_MoveNext_m2575521979_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Vector2>::Dispose()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29_Dispose_m1829244542_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1734063026 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_Dispose_m1829244542(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t1734063026 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_Dispose_m1829244542_gshared)(__this, method)
// System.Void BetterList`1/<GetEnumerator>c__Iterator29<UnityEngine.Vector2>::Reset()
extern "C"  void U3CGetEnumeratorU3Ec__Iterator29_Reset_m2470783278_gshared (U3CGetEnumeratorU3Ec__Iterator29_t1734063026 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator29_Reset_m2470783278(__this, method) ((  void (*) (U3CGetEnumeratorU3Ec__Iterator29_t1734063026 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator29_Reset_m2470783278_gshared)(__this, method)
