﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_cejaswur73
struct  M_cejaswur73_t603143238  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_cejaswur73::_feajiLecoowhea
	int32_t ____feajiLecoowhea_0;
	// System.Int32 GarbageiOS.M_cejaswur73::_wabooree
	int32_t ____wabooree_1;
	// System.Int32 GarbageiOS.M_cejaswur73::_feyiWure
	int32_t ____feyiWure_2;
	// System.Int32 GarbageiOS.M_cejaswur73::_trewha
	int32_t ____trewha_3;
	// System.UInt32 GarbageiOS.M_cejaswur73::_xearnai
	uint32_t ____xearnai_4;
	// System.UInt32 GarbageiOS.M_cejaswur73::_dimerheNemi
	uint32_t ____dimerheNemi_5;
	// System.Single GarbageiOS.M_cejaswur73::_beabou
	float ____beabou_6;
	// System.String GarbageiOS.M_cejaswur73::_remtone
	String_t* ____remtone_7;
	// System.Int32 GarbageiOS.M_cejaswur73::_mayhayciXaltearis
	int32_t ____mayhayciXaltearis_8;
	// System.String GarbageiOS.M_cejaswur73::_telqeJeenorea
	String_t* ____telqeJeenorea_9;
	// System.UInt32 GarbageiOS.M_cejaswur73::_birsall
	uint32_t ____birsall_10;
	// System.UInt32 GarbageiOS.M_cejaswur73::_chayki
	uint32_t ____chayki_11;
	// System.Boolean GarbageiOS.M_cejaswur73::_nisirSeagal
	bool ____nisirSeagal_12;
	// System.UInt32 GarbageiOS.M_cejaswur73::_cibu
	uint32_t ____cibu_13;

public:
	inline static int32_t get_offset_of__feajiLecoowhea_0() { return static_cast<int32_t>(offsetof(M_cejaswur73_t603143238, ____feajiLecoowhea_0)); }
	inline int32_t get__feajiLecoowhea_0() const { return ____feajiLecoowhea_0; }
	inline int32_t* get_address_of__feajiLecoowhea_0() { return &____feajiLecoowhea_0; }
	inline void set__feajiLecoowhea_0(int32_t value)
	{
		____feajiLecoowhea_0 = value;
	}

	inline static int32_t get_offset_of__wabooree_1() { return static_cast<int32_t>(offsetof(M_cejaswur73_t603143238, ____wabooree_1)); }
	inline int32_t get__wabooree_1() const { return ____wabooree_1; }
	inline int32_t* get_address_of__wabooree_1() { return &____wabooree_1; }
	inline void set__wabooree_1(int32_t value)
	{
		____wabooree_1 = value;
	}

	inline static int32_t get_offset_of__feyiWure_2() { return static_cast<int32_t>(offsetof(M_cejaswur73_t603143238, ____feyiWure_2)); }
	inline int32_t get__feyiWure_2() const { return ____feyiWure_2; }
	inline int32_t* get_address_of__feyiWure_2() { return &____feyiWure_2; }
	inline void set__feyiWure_2(int32_t value)
	{
		____feyiWure_2 = value;
	}

	inline static int32_t get_offset_of__trewha_3() { return static_cast<int32_t>(offsetof(M_cejaswur73_t603143238, ____trewha_3)); }
	inline int32_t get__trewha_3() const { return ____trewha_3; }
	inline int32_t* get_address_of__trewha_3() { return &____trewha_3; }
	inline void set__trewha_3(int32_t value)
	{
		____trewha_3 = value;
	}

	inline static int32_t get_offset_of__xearnai_4() { return static_cast<int32_t>(offsetof(M_cejaswur73_t603143238, ____xearnai_4)); }
	inline uint32_t get__xearnai_4() const { return ____xearnai_4; }
	inline uint32_t* get_address_of__xearnai_4() { return &____xearnai_4; }
	inline void set__xearnai_4(uint32_t value)
	{
		____xearnai_4 = value;
	}

	inline static int32_t get_offset_of__dimerheNemi_5() { return static_cast<int32_t>(offsetof(M_cejaswur73_t603143238, ____dimerheNemi_5)); }
	inline uint32_t get__dimerheNemi_5() const { return ____dimerheNemi_5; }
	inline uint32_t* get_address_of__dimerheNemi_5() { return &____dimerheNemi_5; }
	inline void set__dimerheNemi_5(uint32_t value)
	{
		____dimerheNemi_5 = value;
	}

	inline static int32_t get_offset_of__beabou_6() { return static_cast<int32_t>(offsetof(M_cejaswur73_t603143238, ____beabou_6)); }
	inline float get__beabou_6() const { return ____beabou_6; }
	inline float* get_address_of__beabou_6() { return &____beabou_6; }
	inline void set__beabou_6(float value)
	{
		____beabou_6 = value;
	}

	inline static int32_t get_offset_of__remtone_7() { return static_cast<int32_t>(offsetof(M_cejaswur73_t603143238, ____remtone_7)); }
	inline String_t* get__remtone_7() const { return ____remtone_7; }
	inline String_t** get_address_of__remtone_7() { return &____remtone_7; }
	inline void set__remtone_7(String_t* value)
	{
		____remtone_7 = value;
		Il2CppCodeGenWriteBarrier(&____remtone_7, value);
	}

	inline static int32_t get_offset_of__mayhayciXaltearis_8() { return static_cast<int32_t>(offsetof(M_cejaswur73_t603143238, ____mayhayciXaltearis_8)); }
	inline int32_t get__mayhayciXaltearis_8() const { return ____mayhayciXaltearis_8; }
	inline int32_t* get_address_of__mayhayciXaltearis_8() { return &____mayhayciXaltearis_8; }
	inline void set__mayhayciXaltearis_8(int32_t value)
	{
		____mayhayciXaltearis_8 = value;
	}

	inline static int32_t get_offset_of__telqeJeenorea_9() { return static_cast<int32_t>(offsetof(M_cejaswur73_t603143238, ____telqeJeenorea_9)); }
	inline String_t* get__telqeJeenorea_9() const { return ____telqeJeenorea_9; }
	inline String_t** get_address_of__telqeJeenorea_9() { return &____telqeJeenorea_9; }
	inline void set__telqeJeenorea_9(String_t* value)
	{
		____telqeJeenorea_9 = value;
		Il2CppCodeGenWriteBarrier(&____telqeJeenorea_9, value);
	}

	inline static int32_t get_offset_of__birsall_10() { return static_cast<int32_t>(offsetof(M_cejaswur73_t603143238, ____birsall_10)); }
	inline uint32_t get__birsall_10() const { return ____birsall_10; }
	inline uint32_t* get_address_of__birsall_10() { return &____birsall_10; }
	inline void set__birsall_10(uint32_t value)
	{
		____birsall_10 = value;
	}

	inline static int32_t get_offset_of__chayki_11() { return static_cast<int32_t>(offsetof(M_cejaswur73_t603143238, ____chayki_11)); }
	inline uint32_t get__chayki_11() const { return ____chayki_11; }
	inline uint32_t* get_address_of__chayki_11() { return &____chayki_11; }
	inline void set__chayki_11(uint32_t value)
	{
		____chayki_11 = value;
	}

	inline static int32_t get_offset_of__nisirSeagal_12() { return static_cast<int32_t>(offsetof(M_cejaswur73_t603143238, ____nisirSeagal_12)); }
	inline bool get__nisirSeagal_12() const { return ____nisirSeagal_12; }
	inline bool* get_address_of__nisirSeagal_12() { return &____nisirSeagal_12; }
	inline void set__nisirSeagal_12(bool value)
	{
		____nisirSeagal_12 = value;
	}

	inline static int32_t get_offset_of__cibu_13() { return static_cast<int32_t>(offsetof(M_cejaswur73_t603143238, ____cibu_13)); }
	inline uint32_t get__cibu_13() const { return ____cibu_13; }
	inline uint32_t* get_address_of__cibu_13() { return &____cibu_13; }
	inline void set__cibu_13(uint32_t value)
	{
		____cibu_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
