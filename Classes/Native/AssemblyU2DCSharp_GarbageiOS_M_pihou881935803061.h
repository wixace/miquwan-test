﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_pihou88
struct  M_pihou88_t1935803061  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_pihou88::_seleeRoji
	float ____seleeRoji_0;
	// System.Int32 GarbageiOS.M_pihou88::_bearuder
	int32_t ____bearuder_1;
	// System.Int32 GarbageiOS.M_pihou88::_jaircearqereRolaymere
	int32_t ____jaircearqereRolaymere_2;
	// System.Boolean GarbageiOS.M_pihou88::_meldaChepoudra
	bool ____meldaChepoudra_3;
	// System.String GarbageiOS.M_pihou88::_mimouber
	String_t* ____mimouber_4;
	// System.Boolean GarbageiOS.M_pihou88::_jadoZouzedi
	bool ____jadoZouzedi_5;
	// System.String GarbageiOS.M_pihou88::_nowbee
	String_t* ____nowbee_6;
	// System.Single GarbageiOS.M_pihou88::_yisfoo
	float ____yisfoo_7;
	// System.Int32 GarbageiOS.M_pihou88::_talmi
	int32_t ____talmi_8;
	// System.Single GarbageiOS.M_pihou88::_caytrurdea
	float ____caytrurdea_9;
	// System.UInt32 GarbageiOS.M_pihou88::_tairyel
	uint32_t ____tairyel_10;
	// System.Single GarbageiOS.M_pihou88::_tooballKebaike
	float ____tooballKebaike_11;
	// System.UInt32 GarbageiOS.M_pihou88::_muhotor
	uint32_t ____muhotor_12;
	// System.String GarbageiOS.M_pihou88::_zearhairChearjirwir
	String_t* ____zearhairChearjirwir_13;
	// System.Boolean GarbageiOS.M_pihou88::_feveJeldeeyou
	bool ____feveJeldeeyou_14;
	// System.Int32 GarbageiOS.M_pihou88::_trewimall
	int32_t ____trewimall_15;
	// System.String GarbageiOS.M_pihou88::_waxiResou
	String_t* ____waxiResou_16;
	// System.UInt32 GarbageiOS.M_pihou88::_saimee
	uint32_t ____saimee_17;
	// System.Boolean GarbageiOS.M_pihou88::_dralltanea
	bool ____dralltanea_18;

public:
	inline static int32_t get_offset_of__seleeRoji_0() { return static_cast<int32_t>(offsetof(M_pihou88_t1935803061, ____seleeRoji_0)); }
	inline float get__seleeRoji_0() const { return ____seleeRoji_0; }
	inline float* get_address_of__seleeRoji_0() { return &____seleeRoji_0; }
	inline void set__seleeRoji_0(float value)
	{
		____seleeRoji_0 = value;
	}

	inline static int32_t get_offset_of__bearuder_1() { return static_cast<int32_t>(offsetof(M_pihou88_t1935803061, ____bearuder_1)); }
	inline int32_t get__bearuder_1() const { return ____bearuder_1; }
	inline int32_t* get_address_of__bearuder_1() { return &____bearuder_1; }
	inline void set__bearuder_1(int32_t value)
	{
		____bearuder_1 = value;
	}

	inline static int32_t get_offset_of__jaircearqereRolaymere_2() { return static_cast<int32_t>(offsetof(M_pihou88_t1935803061, ____jaircearqereRolaymere_2)); }
	inline int32_t get__jaircearqereRolaymere_2() const { return ____jaircearqereRolaymere_2; }
	inline int32_t* get_address_of__jaircearqereRolaymere_2() { return &____jaircearqereRolaymere_2; }
	inline void set__jaircearqereRolaymere_2(int32_t value)
	{
		____jaircearqereRolaymere_2 = value;
	}

	inline static int32_t get_offset_of__meldaChepoudra_3() { return static_cast<int32_t>(offsetof(M_pihou88_t1935803061, ____meldaChepoudra_3)); }
	inline bool get__meldaChepoudra_3() const { return ____meldaChepoudra_3; }
	inline bool* get_address_of__meldaChepoudra_3() { return &____meldaChepoudra_3; }
	inline void set__meldaChepoudra_3(bool value)
	{
		____meldaChepoudra_3 = value;
	}

	inline static int32_t get_offset_of__mimouber_4() { return static_cast<int32_t>(offsetof(M_pihou88_t1935803061, ____mimouber_4)); }
	inline String_t* get__mimouber_4() const { return ____mimouber_4; }
	inline String_t** get_address_of__mimouber_4() { return &____mimouber_4; }
	inline void set__mimouber_4(String_t* value)
	{
		____mimouber_4 = value;
		Il2CppCodeGenWriteBarrier(&____mimouber_4, value);
	}

	inline static int32_t get_offset_of__jadoZouzedi_5() { return static_cast<int32_t>(offsetof(M_pihou88_t1935803061, ____jadoZouzedi_5)); }
	inline bool get__jadoZouzedi_5() const { return ____jadoZouzedi_5; }
	inline bool* get_address_of__jadoZouzedi_5() { return &____jadoZouzedi_5; }
	inline void set__jadoZouzedi_5(bool value)
	{
		____jadoZouzedi_5 = value;
	}

	inline static int32_t get_offset_of__nowbee_6() { return static_cast<int32_t>(offsetof(M_pihou88_t1935803061, ____nowbee_6)); }
	inline String_t* get__nowbee_6() const { return ____nowbee_6; }
	inline String_t** get_address_of__nowbee_6() { return &____nowbee_6; }
	inline void set__nowbee_6(String_t* value)
	{
		____nowbee_6 = value;
		Il2CppCodeGenWriteBarrier(&____nowbee_6, value);
	}

	inline static int32_t get_offset_of__yisfoo_7() { return static_cast<int32_t>(offsetof(M_pihou88_t1935803061, ____yisfoo_7)); }
	inline float get__yisfoo_7() const { return ____yisfoo_7; }
	inline float* get_address_of__yisfoo_7() { return &____yisfoo_7; }
	inline void set__yisfoo_7(float value)
	{
		____yisfoo_7 = value;
	}

	inline static int32_t get_offset_of__talmi_8() { return static_cast<int32_t>(offsetof(M_pihou88_t1935803061, ____talmi_8)); }
	inline int32_t get__talmi_8() const { return ____talmi_8; }
	inline int32_t* get_address_of__talmi_8() { return &____talmi_8; }
	inline void set__talmi_8(int32_t value)
	{
		____talmi_8 = value;
	}

	inline static int32_t get_offset_of__caytrurdea_9() { return static_cast<int32_t>(offsetof(M_pihou88_t1935803061, ____caytrurdea_9)); }
	inline float get__caytrurdea_9() const { return ____caytrurdea_9; }
	inline float* get_address_of__caytrurdea_9() { return &____caytrurdea_9; }
	inline void set__caytrurdea_9(float value)
	{
		____caytrurdea_9 = value;
	}

	inline static int32_t get_offset_of__tairyel_10() { return static_cast<int32_t>(offsetof(M_pihou88_t1935803061, ____tairyel_10)); }
	inline uint32_t get__tairyel_10() const { return ____tairyel_10; }
	inline uint32_t* get_address_of__tairyel_10() { return &____tairyel_10; }
	inline void set__tairyel_10(uint32_t value)
	{
		____tairyel_10 = value;
	}

	inline static int32_t get_offset_of__tooballKebaike_11() { return static_cast<int32_t>(offsetof(M_pihou88_t1935803061, ____tooballKebaike_11)); }
	inline float get__tooballKebaike_11() const { return ____tooballKebaike_11; }
	inline float* get_address_of__tooballKebaike_11() { return &____tooballKebaike_11; }
	inline void set__tooballKebaike_11(float value)
	{
		____tooballKebaike_11 = value;
	}

	inline static int32_t get_offset_of__muhotor_12() { return static_cast<int32_t>(offsetof(M_pihou88_t1935803061, ____muhotor_12)); }
	inline uint32_t get__muhotor_12() const { return ____muhotor_12; }
	inline uint32_t* get_address_of__muhotor_12() { return &____muhotor_12; }
	inline void set__muhotor_12(uint32_t value)
	{
		____muhotor_12 = value;
	}

	inline static int32_t get_offset_of__zearhairChearjirwir_13() { return static_cast<int32_t>(offsetof(M_pihou88_t1935803061, ____zearhairChearjirwir_13)); }
	inline String_t* get__zearhairChearjirwir_13() const { return ____zearhairChearjirwir_13; }
	inline String_t** get_address_of__zearhairChearjirwir_13() { return &____zearhairChearjirwir_13; }
	inline void set__zearhairChearjirwir_13(String_t* value)
	{
		____zearhairChearjirwir_13 = value;
		Il2CppCodeGenWriteBarrier(&____zearhairChearjirwir_13, value);
	}

	inline static int32_t get_offset_of__feveJeldeeyou_14() { return static_cast<int32_t>(offsetof(M_pihou88_t1935803061, ____feveJeldeeyou_14)); }
	inline bool get__feveJeldeeyou_14() const { return ____feveJeldeeyou_14; }
	inline bool* get_address_of__feveJeldeeyou_14() { return &____feveJeldeeyou_14; }
	inline void set__feveJeldeeyou_14(bool value)
	{
		____feveJeldeeyou_14 = value;
	}

	inline static int32_t get_offset_of__trewimall_15() { return static_cast<int32_t>(offsetof(M_pihou88_t1935803061, ____trewimall_15)); }
	inline int32_t get__trewimall_15() const { return ____trewimall_15; }
	inline int32_t* get_address_of__trewimall_15() { return &____trewimall_15; }
	inline void set__trewimall_15(int32_t value)
	{
		____trewimall_15 = value;
	}

	inline static int32_t get_offset_of__waxiResou_16() { return static_cast<int32_t>(offsetof(M_pihou88_t1935803061, ____waxiResou_16)); }
	inline String_t* get__waxiResou_16() const { return ____waxiResou_16; }
	inline String_t** get_address_of__waxiResou_16() { return &____waxiResou_16; }
	inline void set__waxiResou_16(String_t* value)
	{
		____waxiResou_16 = value;
		Il2CppCodeGenWriteBarrier(&____waxiResou_16, value);
	}

	inline static int32_t get_offset_of__saimee_17() { return static_cast<int32_t>(offsetof(M_pihou88_t1935803061, ____saimee_17)); }
	inline uint32_t get__saimee_17() const { return ____saimee_17; }
	inline uint32_t* get_address_of__saimee_17() { return &____saimee_17; }
	inline void set__saimee_17(uint32_t value)
	{
		____saimee_17 = value;
	}

	inline static int32_t get_offset_of__dralltanea_18() { return static_cast<int32_t>(offsetof(M_pihou88_t1935803061, ____dralltanea_18)); }
	inline bool get__dralltanea_18() const { return ____dralltanea_18; }
	inline bool* get_address_of__dralltanea_18() { return &____dralltanea_18; }
	inline void set__dralltanea_18(bool value)
	{
		____dralltanea_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
