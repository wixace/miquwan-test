﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// UnityEngine.Rendering.SphericalHarmonicsL2
struct SphericalHarmonicsL2_t3881980607;
struct SphericalHarmonicsL2_t3881980607_marshaled_pinvoke;
struct SphericalHarmonicsL2_t3881980607_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmoni3881980607.h"
#include "mscorlib_System_Object4170816371.h"

// System.Int32 UnityEngine.Rendering.SphericalHarmonicsL2::GetHashCode()
extern "C"  int32_t SphericalHarmonicsL2_GetHashCode_m4110797003 (SphericalHarmonicsL2_t3881980607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rendering.SphericalHarmonicsL2::Equals(System.Object)
extern "C"  bool SphericalHarmonicsL2_Equals_m2524805223 (SphericalHarmonicsL2_t3881980607 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Rendering.SphericalHarmonicsL2::op_Equality(UnityEngine.Rendering.SphericalHarmonicsL2,UnityEngine.Rendering.SphericalHarmonicsL2)
extern "C"  bool SphericalHarmonicsL2_op_Equality_m3170363866 (Il2CppObject * __this /* static, unused */, SphericalHarmonicsL2_t3881980607  ___lhs0, SphericalHarmonicsL2_t3881980607  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct SphericalHarmonicsL2_t3881980607;
struct SphericalHarmonicsL2_t3881980607_marshaled_pinvoke;

extern "C" void SphericalHarmonicsL2_t3881980607_marshal_pinvoke(const SphericalHarmonicsL2_t3881980607& unmarshaled, SphericalHarmonicsL2_t3881980607_marshaled_pinvoke& marshaled);
extern "C" void SphericalHarmonicsL2_t3881980607_marshal_pinvoke_back(const SphericalHarmonicsL2_t3881980607_marshaled_pinvoke& marshaled, SphericalHarmonicsL2_t3881980607& unmarshaled);
extern "C" void SphericalHarmonicsL2_t3881980607_marshal_pinvoke_cleanup(SphericalHarmonicsL2_t3881980607_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct SphericalHarmonicsL2_t3881980607;
struct SphericalHarmonicsL2_t3881980607_marshaled_com;

extern "C" void SphericalHarmonicsL2_t3881980607_marshal_com(const SphericalHarmonicsL2_t3881980607& unmarshaled, SphericalHarmonicsL2_t3881980607_marshaled_com& marshaled);
extern "C" void SphericalHarmonicsL2_t3881980607_marshal_com_back(const SphericalHarmonicsL2_t3881980607_marshaled_com& marshaled, SphericalHarmonicsL2_t3881980607& unmarshaled);
extern "C" void SphericalHarmonicsL2_t3881980607_marshal_com_cleanup(SphericalHarmonicsL2_t3881980607_marshaled_com& marshaled);
