﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Schema.JsonSchemaGenerator
struct JsonSchemaGenerator_t2856272772;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_t507353639;
// Newtonsoft.Json.Schema.JsonSchema
struct JsonSchema_t460567603;
// Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema
struct TypeSchema_t486414904;
// System.Type
struct Type_t;
// Newtonsoft.Json.Schema.JsonSchemaResolver
struct JsonSchemaResolver_t2728657753;
// System.String
struct String_t;
// Newtonsoft.Json.Serialization.JObjectContract
struct JObjectContract_t2867848225;
// Newtonsoft.Json.Serialization.JsonISerializableContract
struct JsonISerializableContract_t624170136;
// Newtonsoft.Json.JsonContainerAttribute
struct JsonContainerAttribute_t1917602971;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Newtonsoft.Json.Serialization.JsonContract
struct JsonContract_t1328848902;
// Newtonsoft.Json.JsonArrayAttribute
struct JsonArrayAttribute_t2333618243;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchema>
struct IList_1_t3155214806;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken>
struct IList_1_t1811925858;
// Newtonsoft.Json.Serialization.JsonProperty
struct JsonProperty_t902655177;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_Undefined3185178219.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema486414904.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem2728657753.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Required3921306327.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem2115415821.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_DefaultValueHand1569448045.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_JO2867848225.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso624170136.h"
#include "mscorlib_System_Nullable_1_gen2199542344.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem2856272772.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonContainerAtt1917602971.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema460567603.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonArrayAttribu2333618243.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso902655177.h"
#include "mscorlib_System_Nullable_1_gen2838778904.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::.ctor()
extern "C"  void JsonSchemaGenerator__ctor_m223066953 (JsonSchemaGenerator_t2856272772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.UndefinedSchemaIdHandling Newtonsoft.Json.Schema.JsonSchemaGenerator::get_UndefinedSchemaIdHandling()
extern "C"  int32_t JsonSchemaGenerator_get_UndefinedSchemaIdHandling_m2528940267 (JsonSchemaGenerator_t2856272772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::set_UndefinedSchemaIdHandling(Newtonsoft.Json.Schema.UndefinedSchemaIdHandling)
extern "C"  void JsonSchemaGenerator_set_UndefinedSchemaIdHandling_m4239230850 (JsonSchemaGenerator_t2856272772 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.Schema.JsonSchemaGenerator::get_ContractResolver()
extern "C"  Il2CppObject * JsonSchemaGenerator_get_ContractResolver_m2629544257 (JsonSchemaGenerator_t2856272772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::set_ContractResolver(Newtonsoft.Json.Serialization.IContractResolver)
extern "C"  void JsonSchemaGenerator_set_ContractResolver_m396477236 (JsonSchemaGenerator_t2856272772 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaGenerator::get_CurrentSchema()
extern "C"  JsonSchema_t460567603 * JsonSchemaGenerator_get_CurrentSchema_m3951968518 (JsonSchemaGenerator_t2856272772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::Push(Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema)
extern "C"  void JsonSchemaGenerator_Push_m2208542501 (JsonSchemaGenerator_t2856272772 * __this, TypeSchema_t486414904 * ___typeSchema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema Newtonsoft.Json.Schema.JsonSchemaGenerator::Pop()
extern "C"  TypeSchema_t486414904 * JsonSchemaGenerator_Pop_m404386493 (JsonSchemaGenerator_t2856272772 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaGenerator::Generate(System.Type)
extern "C"  JsonSchema_t460567603 * JsonSchemaGenerator_Generate_m2336631927 (JsonSchemaGenerator_t2856272772 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaGenerator::Generate(System.Type,Newtonsoft.Json.Schema.JsonSchemaResolver)
extern "C"  JsonSchema_t460567603 * JsonSchemaGenerator_Generate_m1262763932 (JsonSchemaGenerator_t2856272772 * __this, Type_t * ___type0, JsonSchemaResolver_t2728657753 * ___resolver1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaGenerator::Generate(System.Type,System.Boolean)
extern "C"  JsonSchema_t460567603 * JsonSchemaGenerator_Generate_m3089812006 (JsonSchemaGenerator_t2856272772 * __this, Type_t * ___type0, bool ___rootSchemaNullable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaGenerator::Generate(System.Type,Newtonsoft.Json.Schema.JsonSchemaResolver,System.Boolean)
extern "C"  JsonSchema_t460567603 * JsonSchemaGenerator_Generate_m2851694689 (JsonSchemaGenerator_t2856272772 * __this, Type_t * ___type0, JsonSchemaResolver_t2728657753 * ___resolver1, bool ___rootSchemaNullable2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaGenerator::GetTitle(System.Type)
extern "C"  String_t* JsonSchemaGenerator_GetTitle_m2279448985 (JsonSchemaGenerator_t2856272772 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaGenerator::GetDescription(System.Type)
extern "C"  String_t* JsonSchemaGenerator_GetDescription_m1391924149 (JsonSchemaGenerator_t2856272772 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaGenerator::GetTypeId(System.Type,System.Boolean)
extern "C"  String_t* JsonSchemaGenerator_GetTypeId_m2231528499 (JsonSchemaGenerator_t2856272772 * __this, Type_t * ___type0, bool ___explicitOnly1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaGenerator::GenerateInternal(System.Type,Newtonsoft.Json.Required,System.Boolean)
extern "C"  JsonSchema_t460567603 * JsonSchemaGenerator_GenerateInternal_m2491174181 (JsonSchemaGenerator_t2856272772 * __this, Type_t * ___type0, int32_t ___valueRequired1, bool ___required2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaType Newtonsoft.Json.Schema.JsonSchemaGenerator::AddNullType(Newtonsoft.Json.Schema.JsonSchemaType,Newtonsoft.Json.Required)
extern "C"  int32_t JsonSchemaGenerator_AddNullType_m3203789932 (JsonSchemaGenerator_t2856272772 * __this, int32_t ___type0, int32_t ___valueRequired1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchemaGenerator::HasFlag(Newtonsoft.Json.DefaultValueHandling,Newtonsoft.Json.DefaultValueHandling)
extern "C"  bool JsonSchemaGenerator_HasFlag_m3628958477 (JsonSchemaGenerator_t2856272772 * __this, int32_t ___value0, int32_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::GenerateObjectSchema(System.Type,Newtonsoft.Json.Serialization.JObjectContract)
extern "C"  void JsonSchemaGenerator_GenerateObjectSchema_m2980564301 (JsonSchemaGenerator_t2856272772 * __this, Type_t * ___type0, JObjectContract_t2867848225 * ___contract1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::GenerateISerializableContract(System.Type,Newtonsoft.Json.Serialization.JsonISerializableContract)
extern "C"  void JsonSchemaGenerator_GenerateISerializableContract_m3261479610 (JsonSchemaGenerator_t2856272772 * __this, Type_t * ___type0, JsonISerializableContract_t624170136 * ___contract1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchemaGenerator::HasFlag(System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>,Newtonsoft.Json.Schema.JsonSchemaType)
extern "C"  bool JsonSchemaGenerator_HasFlag_m218382716 (Il2CppObject * __this /* static, unused */, Nullable_1_t2199542344  ___value0, int32_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaType Newtonsoft.Json.Schema.JsonSchemaGenerator::GetJsonSchemaType(System.Type,Newtonsoft.Json.Required)
extern "C"  int32_t JsonSchemaGenerator_GetJsonSchemaType_m3509475073 (JsonSchemaGenerator_t2856272772 * __this, Type_t * ___type0, int32_t ___valueRequired1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_get_Schema1(Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema)
extern "C"  JsonSchema_t460567603 * JsonSchemaGenerator_ilo_get_Schema1_m2730101873 (Il2CppObject * __this /* static, unused */, TypeSchema_t486414904 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_Generate2(Newtonsoft.Json.Schema.JsonSchemaGenerator,System.Type,Newtonsoft.Json.Schema.JsonSchemaResolver,System.Boolean)
extern "C"  JsonSchema_t460567603 * JsonSchemaGenerator_ilo_Generate2_m1955018264 (Il2CppObject * __this /* static, unused */, JsonSchemaGenerator_t2856272772 * ____this0, Type_t * ___type1, JsonSchemaResolver_t2728657753 * ___resolver2, bool ___rootSchemaNullable3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_GenerateInternal3(Newtonsoft.Json.Schema.JsonSchemaGenerator,System.Type,Newtonsoft.Json.Required,System.Boolean)
extern "C"  JsonSchema_t460567603 * JsonSchemaGenerator_ilo_GenerateInternal3_m2445016335 (Il2CppObject * __this /* static, unused */, JsonSchemaGenerator_t2856272772 * ____this0, Type_t * ___type1, int32_t ___valueRequired2, bool ___required3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonContainerAttribute Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_GetJsonContainerAttribute4(System.Type)
extern "C"  JsonContainerAttribute_t1917602971 * JsonSchemaGenerator_ilo_GetJsonContainerAttribute4_m4000520107 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_get_Id5(Newtonsoft.Json.JsonContainerAttribute)
extern "C"  String_t* JsonSchemaGenerator_ilo_get_Id5_m1375513194 (Il2CppObject * __this /* static, unused */, JsonContainerAttribute_t1917602971 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_GetSchema6(Newtonsoft.Json.Schema.JsonSchemaResolver,System.String)
extern "C"  JsonSchema_t460567603 * JsonSchemaGenerator_ilo_GetSchema6_m1544095432 (Il2CppObject * __this /* static, unused */, JsonSchemaResolver_t2728657753 * ____this0, String_t* ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_HasFlag7(System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>,Newtonsoft.Json.Schema.JsonSchemaType)
extern "C"  bool JsonSchemaGenerator_ilo_HasFlag7_m1428211254 (Il2CppObject * __this /* static, unused */, Nullable_1_t2199542344  ___value0, int32_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_set_Type8(Newtonsoft.Json.Schema.JsonSchema,System.Nullable`1<Newtonsoft.Json.Schema.JsonSchemaType>)
extern "C"  void JsonSchemaGenerator_ilo_set_Type8_m894076604 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, Nullable_1_t2199542344  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_get_Required9(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  Nullable_1_t560925241  JsonSchemaGenerator_ilo_get_Required9_m3256889493 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_set_Required10(Newtonsoft.Json.Schema.JsonSchema,System.Nullable`1<System.Boolean>)
extern "C"  void JsonSchemaGenerator_ilo_set_Required10_m868331978 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, Nullable_1_t560925241  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_FormatWith11(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* JsonSchemaGenerator_ilo_FormatWith11_m3631118796 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_ResolveContract12(Newtonsoft.Json.Serialization.IContractResolver,System.Type)
extern "C"  JsonContract_t1328848902 * JsonSchemaGenerator_ilo_ResolveContract12_m3044857492 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_Push13(Newtonsoft.Json.Schema.JsonSchemaGenerator,Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema)
extern "C"  void JsonSchemaGenerator_ilo_Push13_m765576812 (Il2CppObject * __this /* static, unused */, JsonSchemaGenerator_t2856272772 * ____this0, TypeSchema_t486414904 * ___typeSchema1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_GetTitle14(Newtonsoft.Json.Schema.JsonSchemaGenerator,System.Type)
extern "C"  String_t* JsonSchemaGenerator_ilo_GetTitle14_m4200463447 (Il2CppObject * __this /* static, unused */, JsonSchemaGenerator_t2856272772 * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_set_Title15(Newtonsoft.Json.Schema.JsonSchema,System.String)
extern "C"  void JsonSchemaGenerator_ilo_set_Title15_m3720768954 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_get_CurrentSchema16(Newtonsoft.Json.Schema.JsonSchemaGenerator)
extern "C"  JsonSchema_t460567603 * JsonSchemaGenerator_ilo_get_CurrentSchema16_m2457710662 (Il2CppObject * __this /* static, unused */, JsonSchemaGenerator_t2856272772 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaType Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_AddNullType17(Newtonsoft.Json.Schema.JsonSchemaGenerator,Newtonsoft.Json.Schema.JsonSchemaType,Newtonsoft.Json.Required)
extern "C"  int32_t JsonSchemaGenerator_ilo_AddNullType17_m916659983 (Il2CppObject * __this /* static, unused */, JsonSchemaGenerator_t2856272772 * ____this0, int32_t ___type1, int32_t ___valueRequired2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_GetDictionaryKeyValueTypes18(System.Type,System.Type&,System.Type&)
extern "C"  void JsonSchemaGenerator_ilo_GetDictionaryKeyValueTypes18_m2081528073 (Il2CppObject * __this /* static, unused */, Type_t * ___dictionaryType0, Type_t ** ___keyType1, Type_t ** ___valueType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_set_AdditionalProperties19(Newtonsoft.Json.Schema.JsonSchema,Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void JsonSchemaGenerator_ilo_set_AdditionalProperties19_m617552335 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, JsonSchema_t460567603 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_get_AllowNullItems20(Newtonsoft.Json.JsonArrayAttribute)
extern "C"  bool JsonSchemaGenerator_ilo_get_AllowNullItems20_m2333420377 (Il2CppObject * __this /* static, unused */, JsonArrayAttribute_t2333618243 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_GetCollectionItemType21(System.Type)
extern "C"  Type_t * JsonSchemaGenerator_ilo_GetCollectionItemType21_m3406716511 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchema> Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_get_Items22(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  Il2CppObject* JsonSchemaGenerator_ilo_get_Items22_m1280371753 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaType Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_GetJsonSchemaType23(Newtonsoft.Json.Schema.JsonSchemaGenerator,System.Type,Newtonsoft.Json.Required)
extern "C"  int32_t JsonSchemaGenerator_ilo_GetJsonSchemaType23_m2166112411 (Il2CppObject * __this /* static, unused */, JsonSchemaGenerator_t2856272772 * ____this0, Type_t * ___type1, int32_t ___valueRequired2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_set_Enum24(Newtonsoft.Json.Schema.JsonSchema,System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken>)
extern "C"  void JsonSchemaGenerator_ilo_set_Enum24_m1982990598 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, Il2CppObject* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_GenerateObjectSchema25(Newtonsoft.Json.Schema.JsonSchemaGenerator,System.Type,Newtonsoft.Json.Serialization.JObjectContract)
extern "C"  void JsonSchemaGenerator_ilo_GenerateObjectSchema25_m2432675275 (Il2CppObject * __this /* static, unused */, JsonSchemaGenerator_t2856272772 * ____this0, Type_t * ___type1, JObjectContract_t2867848225 * ___contract2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_set_Id26(Newtonsoft.Json.Schema.JsonSchema,System.String)
extern "C"  void JsonSchemaGenerator_ilo_set_Id26_m1103413667 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_GenerateISerializableContract27(Newtonsoft.Json.Schema.JsonSchemaGenerator,System.Type,Newtonsoft.Json.Serialization.JsonISerializableContract)
extern "C"  void JsonSchemaGenerator_ilo_GenerateISerializableContract27_m3416545196 (Il2CppObject * __this /* static, unused */, JsonSchemaGenerator_t2856272772 * ____this0, Type_t * ___type1, JsonISerializableContract_t624170136 * ___contract2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_Pop28(Newtonsoft.Json.Schema.JsonSchemaGenerator)
extern "C"  TypeSchema_t486414904 * JsonSchemaGenerator_ilo_Pop28_m455695710 (Il2CppObject * __this /* static, unused */, JsonSchemaGenerator_t2856272772 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_get_Ignored29(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  bool JsonSchemaGenerator_ilo_get_Ignored29_m1624299584 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_get_NullValueHandling30(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Nullable_1_t2838778904  JsonSchemaGenerator_ilo_get_NullValueHandling30_m635168153 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_HasFlag31(Newtonsoft.Json.Schema.JsonSchemaGenerator,Newtonsoft.Json.DefaultValueHandling,Newtonsoft.Json.DefaultValueHandling)
extern "C"  bool JsonSchemaGenerator_ilo_HasFlag31_m325722642 (Il2CppObject * __this /* static, unused */, JsonSchemaGenerator_t2856272772 * ____this0, int32_t ___value1, int32_t ___flag2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_get_DefaultValue32(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Il2CppObject * JsonSchemaGenerator_ilo_get_DefaultValue32_m1965466955 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_FromObject33(System.Object)
extern "C"  JToken_t3412245951 * JsonSchemaGenerator_ilo_FromObject33_m2055246866 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_get_PropertyName34(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  String_t* JsonSchemaGenerator_ilo_get_PropertyName34_m270600587 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_set_AllowAdditionalProperties35(Newtonsoft.Json.Schema.JsonSchema,System.Boolean)
extern "C"  void JsonSchemaGenerator_ilo_set_AllowAdditionalProperties35_m631385928 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Schema.JsonSchemaGenerator::ilo_IsNullable36(System.Type)
extern "C"  bool JsonSchemaGenerator_ilo_IsNullable36_m2457831465 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
