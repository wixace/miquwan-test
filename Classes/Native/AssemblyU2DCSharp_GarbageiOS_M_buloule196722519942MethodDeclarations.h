﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_buloule196
struct M_buloule196_t722519942;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_buloule196722519942.h"

// System.Void GarbageiOS.M_buloule196::.ctor()
extern "C"  void M_buloule196__ctor_m2280102765 (M_buloule196_t722519942 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_buloule196::M_teaka0(System.String[],System.Int32)
extern "C"  void M_buloule196_M_teaka0_m3657044618 (M_buloule196_t722519942 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_buloule196::M_jirchawmeTearjenay1(System.String[],System.Int32)
extern "C"  void M_buloule196_M_jirchawmeTearjenay1_m2346106238 (M_buloule196_t722519942 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_buloule196::M_dormapayDupis2(System.String[],System.Int32)
extern "C"  void M_buloule196_M_dormapayDupis2_m4147589110 (M_buloule196_t722519942 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_buloule196::M_tepeaJurzair3(System.String[],System.Int32)
extern "C"  void M_buloule196_M_tepeaJurzair3_m1831454535 (M_buloule196_t722519942 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_buloule196::M_higallve4(System.String[],System.Int32)
extern "C"  void M_buloule196_M_higallve4_m3830421402 (M_buloule196_t722519942 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_buloule196::M_geesa5(System.String[],System.Int32)
extern "C"  void M_buloule196_M_geesa5_m1049431936 (M_buloule196_t722519942 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_buloule196::M_rowselgee6(System.String[],System.Int32)
extern "C"  void M_buloule196_M_rowselgee6_m3446606415 (M_buloule196_t722519942 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_buloule196::ilo_M_jirchawmeTearjenay11(GarbageiOS.M_buloule196,System.String[],System.Int32)
extern "C"  void M_buloule196_ilo_M_jirchawmeTearjenay11_m3224657768 (Il2CppObject * __this /* static, unused */, M_buloule196_t722519942 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
