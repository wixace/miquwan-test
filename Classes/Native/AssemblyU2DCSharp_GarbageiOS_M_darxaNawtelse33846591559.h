﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_darxaNawtelse33
struct  M_darxaNawtelse33_t846591559  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_darxaNawtelse33::_salchem
	uint32_t ____salchem_0;
	// System.String GarbageiOS.M_darxaNawtelse33::_kuhear
	String_t* ____kuhear_1;
	// System.String GarbageiOS.M_darxaNawtelse33::_cotalni
	String_t* ____cotalni_2;
	// System.Int32 GarbageiOS.M_darxaNawtelse33::_coniki
	int32_t ____coniki_3;
	// System.String GarbageiOS.M_darxaNawtelse33::_sejapi
	String_t* ____sejapi_4;
	// System.Single GarbageiOS.M_darxaNawtelse33::_derjouKermou
	float ____derjouKermou_5;
	// System.Single GarbageiOS.M_darxaNawtelse33::_ralltrawRewamal
	float ____ralltrawRewamal_6;
	// System.String GarbageiOS.M_darxaNawtelse33::_cirepel
	String_t* ____cirepel_7;
	// System.Boolean GarbageiOS.M_darxaNawtelse33::_tive
	bool ____tive_8;
	// System.Single GarbageiOS.M_darxaNawtelse33::_tairlou
	float ____tairlou_9;
	// System.String GarbageiOS.M_darxaNawtelse33::_wawelgeDeharsas
	String_t* ____wawelgeDeharsas_10;

public:
	inline static int32_t get_offset_of__salchem_0() { return static_cast<int32_t>(offsetof(M_darxaNawtelse33_t846591559, ____salchem_0)); }
	inline uint32_t get__salchem_0() const { return ____salchem_0; }
	inline uint32_t* get_address_of__salchem_0() { return &____salchem_0; }
	inline void set__salchem_0(uint32_t value)
	{
		____salchem_0 = value;
	}

	inline static int32_t get_offset_of__kuhear_1() { return static_cast<int32_t>(offsetof(M_darxaNawtelse33_t846591559, ____kuhear_1)); }
	inline String_t* get__kuhear_1() const { return ____kuhear_1; }
	inline String_t** get_address_of__kuhear_1() { return &____kuhear_1; }
	inline void set__kuhear_1(String_t* value)
	{
		____kuhear_1 = value;
		Il2CppCodeGenWriteBarrier(&____kuhear_1, value);
	}

	inline static int32_t get_offset_of__cotalni_2() { return static_cast<int32_t>(offsetof(M_darxaNawtelse33_t846591559, ____cotalni_2)); }
	inline String_t* get__cotalni_2() const { return ____cotalni_2; }
	inline String_t** get_address_of__cotalni_2() { return &____cotalni_2; }
	inline void set__cotalni_2(String_t* value)
	{
		____cotalni_2 = value;
		Il2CppCodeGenWriteBarrier(&____cotalni_2, value);
	}

	inline static int32_t get_offset_of__coniki_3() { return static_cast<int32_t>(offsetof(M_darxaNawtelse33_t846591559, ____coniki_3)); }
	inline int32_t get__coniki_3() const { return ____coniki_3; }
	inline int32_t* get_address_of__coniki_3() { return &____coniki_3; }
	inline void set__coniki_3(int32_t value)
	{
		____coniki_3 = value;
	}

	inline static int32_t get_offset_of__sejapi_4() { return static_cast<int32_t>(offsetof(M_darxaNawtelse33_t846591559, ____sejapi_4)); }
	inline String_t* get__sejapi_4() const { return ____sejapi_4; }
	inline String_t** get_address_of__sejapi_4() { return &____sejapi_4; }
	inline void set__sejapi_4(String_t* value)
	{
		____sejapi_4 = value;
		Il2CppCodeGenWriteBarrier(&____sejapi_4, value);
	}

	inline static int32_t get_offset_of__derjouKermou_5() { return static_cast<int32_t>(offsetof(M_darxaNawtelse33_t846591559, ____derjouKermou_5)); }
	inline float get__derjouKermou_5() const { return ____derjouKermou_5; }
	inline float* get_address_of__derjouKermou_5() { return &____derjouKermou_5; }
	inline void set__derjouKermou_5(float value)
	{
		____derjouKermou_5 = value;
	}

	inline static int32_t get_offset_of__ralltrawRewamal_6() { return static_cast<int32_t>(offsetof(M_darxaNawtelse33_t846591559, ____ralltrawRewamal_6)); }
	inline float get__ralltrawRewamal_6() const { return ____ralltrawRewamal_6; }
	inline float* get_address_of__ralltrawRewamal_6() { return &____ralltrawRewamal_6; }
	inline void set__ralltrawRewamal_6(float value)
	{
		____ralltrawRewamal_6 = value;
	}

	inline static int32_t get_offset_of__cirepel_7() { return static_cast<int32_t>(offsetof(M_darxaNawtelse33_t846591559, ____cirepel_7)); }
	inline String_t* get__cirepel_7() const { return ____cirepel_7; }
	inline String_t** get_address_of__cirepel_7() { return &____cirepel_7; }
	inline void set__cirepel_7(String_t* value)
	{
		____cirepel_7 = value;
		Il2CppCodeGenWriteBarrier(&____cirepel_7, value);
	}

	inline static int32_t get_offset_of__tive_8() { return static_cast<int32_t>(offsetof(M_darxaNawtelse33_t846591559, ____tive_8)); }
	inline bool get__tive_8() const { return ____tive_8; }
	inline bool* get_address_of__tive_8() { return &____tive_8; }
	inline void set__tive_8(bool value)
	{
		____tive_8 = value;
	}

	inline static int32_t get_offset_of__tairlou_9() { return static_cast<int32_t>(offsetof(M_darxaNawtelse33_t846591559, ____tairlou_9)); }
	inline float get__tairlou_9() const { return ____tairlou_9; }
	inline float* get_address_of__tairlou_9() { return &____tairlou_9; }
	inline void set__tairlou_9(float value)
	{
		____tairlou_9 = value;
	}

	inline static int32_t get_offset_of__wawelgeDeharsas_10() { return static_cast<int32_t>(offsetof(M_darxaNawtelse33_t846591559, ____wawelgeDeharsas_10)); }
	inline String_t* get__wawelgeDeharsas_10() const { return ____wawelgeDeharsas_10; }
	inline String_t** get_address_of__wawelgeDeharsas_10() { return &____wawelgeDeharsas_10; }
	inline void set__wawelgeDeharsas_10(String_t* value)
	{
		____wawelgeDeharsas_10 = value;
		Il2CppCodeGenWriteBarrier(&____wawelgeDeharsas_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
