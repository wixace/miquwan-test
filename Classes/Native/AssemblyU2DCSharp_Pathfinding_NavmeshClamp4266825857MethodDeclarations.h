﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.NavmeshClamp
struct NavmeshClamp_t4266825857;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.IRaycastableGraph
struct IRaycastableGraph_t2032416694;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_NNInfo1570625892.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphHitInfo3082627369.h"

// System.Void Pathfinding.NavmeshClamp::.ctor()
extern "C"  void NavmeshClamp__ctor_m4173098758 (NavmeshClamp_t4266825857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshClamp::LateUpdate()
extern "C"  void NavmeshClamp_LateUpdate_m163794701 (NavmeshClamp_t4266825857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.NNInfo Pathfinding.NavmeshClamp::ilo_GetNearest1(AstarPath,UnityEngine.Vector3)
extern "C"  NNInfo_t1570625892  NavmeshClamp_ilo_GetNearest1_m2179722168 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, Vector3_t4282066566  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavmeshClamp::ilo_Linecast2(Pathfinding.IRaycastableGraph,UnityEngine.Vector3,UnityEngine.Vector3,Pathfinding.GraphNode,Pathfinding.GraphHitInfo&)
extern "C"  bool NavmeshClamp_ilo_Linecast2_m148996987 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Vector3_t4282066566  ___start1, Vector3_t4282066566  ___end2, GraphNode_t23612370 * ___hint3, GraphHitInfo_t3082627369 * ___hit4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
