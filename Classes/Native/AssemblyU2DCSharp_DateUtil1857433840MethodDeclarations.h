﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DayOfWeek1779421117.h"
#include "mscorlib_System_TimeSpan413522987.h"

// System.Void DateUtil::.cctor()
extern "C"  void DateUtil__cctor_m1711056226 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DateUtil::get_NowDay()
extern "C"  int32_t DateUtil_get_NowDay_m1756726262 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DateUtil::get_NowHour()
extern "C"  int32_t DateUtil_get_NowHour_m3046304428 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DateUtil::get_NowMillisecond()
extern "C"  int32_t DateUtil_get_NowMillisecond_m2678737051 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DateUtil::get_NowMinute()
extern "C"  int32_t DateUtil_get_NowMinute_m1524578588 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DateUtil::get_NowMonth()
extern "C"  int32_t DateUtil_get_NowMonth_m82364314 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DateUtil::get_NowSecond()
extern "C"  int32_t DateUtil_get_NowSecond_m3815817084 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 DateUtil::get_NowTicks()
extern "C"  int64_t DateUtil_get_NowTicks_m469645969 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DateUtil::get_NowYear()
extern "C"  int32_t DateUtil_get_NowYear_m3523168965 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DateUtil::GetDay(System.Double)
extern "C"  int32_t DateUtil_GetDay_m628780213 (Il2CppObject * __this /* static, unused */, double ___timer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DateUtil::GetHour(System.Double)
extern "C"  int32_t DateUtil_GetHour_m491399053 (Il2CppObject * __this /* static, unused */, double ___timer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DateUtil::GetMinute(System.Double)
extern "C"  int32_t DateUtil_GetMinute_m2396704093 (Il2CppObject * __this /* static, unused */, double ___timer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DateUtil::GetSecond(System.Double)
extern "C"  int32_t DateUtil_GetSecond_m77505277 (Il2CppObject * __this /* static, unused */, double ___timer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DayOfWeek DateUtil::GetDayOfWeek(System.Double)
extern "C"  int32_t DateUtil_GetDayOfWeek_m3819253155 (Il2CppObject * __this /* static, unused */, double ___timer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double DateUtil::GetAllDaySeconds(System.Double)
extern "C"  double DateUtil_GetAllDaySeconds_m144721624 (Il2CppObject * __this /* static, unused */, double ___timer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double DateUtil::GetTimeSpanTotalSeconds(System.Double,System.Double)
extern "C"  double DateUtil_GetTimeSpanTotalSeconds_m4222430096 (Il2CppObject * __this /* static, unused */, double ___timer10, double ___timer21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan DateUtil::GetTimeSpan(System.Double,System.Double)
extern "C"  TimeSpan_t413522987  DateUtil_GetTimeSpan_m2764370117 (Il2CppObject * __this /* static, unused */, double ___timer10, double ___timer21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DateUtil::GetDaysInMonth(System.Double)
extern "C"  int32_t DateUtil_GetDaysInMonth_m3145049933 (Il2CppObject * __this /* static, unused */, double ___timer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String DateUtil::ToShortDateString(System.Double)
extern "C"  String_t* DateUtil_ToShortDateString_m1639217276 (Il2CppObject * __this /* static, unused */, double ___timer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
