﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIButtonRotationGenerated
struct UIButtonRotationGenerated_t3862266219;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UIButtonRotationGenerated::.ctor()
extern "C"  void UIButtonRotationGenerated__ctor_m2123508832 (UIButtonRotationGenerated_t3862266219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonRotationGenerated::UIButtonRotation_UIButtonRotation1(JSVCall,System.Int32)
extern "C"  bool UIButtonRotationGenerated_UIButtonRotation_UIButtonRotation1_m547537554 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonRotationGenerated::UIButtonRotation_tweenTarget(JSVCall)
extern "C"  void UIButtonRotationGenerated_UIButtonRotation_tweenTarget_m3606178274 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonRotationGenerated::UIButtonRotation_hover(JSVCall)
extern "C"  void UIButtonRotationGenerated_UIButtonRotation_hover_m909999746 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonRotationGenerated::UIButtonRotation_pressed(JSVCall)
extern "C"  void UIButtonRotationGenerated_UIButtonRotation_pressed_m2270867548 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonRotationGenerated::UIButtonRotation_duration(JSVCall)
extern "C"  void UIButtonRotationGenerated_UIButtonRotation_duration_m1451578554 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonRotationGenerated::__Register()
extern "C"  void UIButtonRotationGenerated___Register_m3948678247 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonRotationGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UIButtonRotationGenerated_ilo_attachFinalizerObject1_m1850701463 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonRotationGenerated::ilo_setVector3S2(System.Int32,UnityEngine.Vector3)
extern "C"  void UIButtonRotationGenerated_ilo_setVector3S2_m2781777320 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIButtonRotationGenerated::ilo_getVector3S3(System.Int32)
extern "C"  Vector3_t4282066566  UIButtonRotationGenerated_ilo_getVector3S3_m602437928 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonRotationGenerated::ilo_setSingle4(System.Int32,System.Single)
extern "C"  void UIButtonRotationGenerated_ilo_setSingle4_m438975895 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
