﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIButtonColor
struct UIButtonColor_t1546108957;
// UIWidget
struct UIWidget_t769069560;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// TweenColor
struct TweenColor_t2922258392;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIButtonColor_State1650987007.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_UIButtonColor1546108957.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_TweenColor2922258392.h"

// System.Void UIButtonColor::.ctor()
extern "C"  void UIButtonColor__ctor_m127693294 (UIButtonColor_t1546108957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIButtonColor/State UIButtonColor::get_state()
extern "C"  int32_t UIButtonColor_get_state_m1820626312 (UIButtonColor_t1546108957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColor::set_state(UIButtonColor/State)
extern "C"  void UIButtonColor_set_state_m46403507 (UIButtonColor_t1546108957 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UIButtonColor::get_defaultColor()
extern "C"  Color_t4194546905  UIButtonColor_get_defaultColor_m769768378 (UIButtonColor_t1546108957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColor::set_defaultColor(UnityEngine.Color)
extern "C"  void UIButtonColor_set_defaultColor_m76540465 (UIButtonColor_t1546108957 * __this, Color_t4194546905  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonColor::get_isEnabled()
extern "C"  bool UIButtonColor_get_isEnabled_m3369123182 (UIButtonColor_t1546108957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColor::set_isEnabled(System.Boolean)
extern "C"  void UIButtonColor_set_isEnabled_m4104216509 (UIButtonColor_t1546108957 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColor::ResetDefaultColor()
extern "C"  void UIButtonColor_ResetDefaultColor_m275733213 (UIButtonColor_t1546108957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColor::Start()
extern "C"  void UIButtonColor_Start_m3369798382 (UIButtonColor_t1546108957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColor::OnInit()
extern "C"  void UIButtonColor_OnInit_m1282124837 (UIButtonColor_t1546108957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColor::OnEnable()
extern "C"  void UIButtonColor_OnEnable_m841475800 (UIButtonColor_t1546108957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColor::OnDisable()
extern "C"  void UIButtonColor_OnDisable_m756883285 (UIButtonColor_t1546108957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColor::OnHover(System.Boolean)
extern "C"  void UIButtonColor_OnHover_m697468256 (UIButtonColor_t1546108957 * __this, bool ___isOver0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColor::OnPress(System.Boolean)
extern "C"  void UIButtonColor_OnPress_m2912121447 (UIButtonColor_t1546108957 * __this, bool ___isPressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColor::OnDragOver()
extern "C"  void UIButtonColor_OnDragOver_m2090916669 (UIButtonColor_t1546108957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColor::OnDragOut()
extern "C"  void UIButtonColor_OnDragOut_m621620583 (UIButtonColor_t1546108957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColor::OnSelect(System.Boolean)
extern "C"  void UIButtonColor_OnSelect_m4003184584 (UIButtonColor_t1546108957 * __this, bool ___isSelected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColor::SetState(UIButtonColor/State,System.Boolean)
extern "C"  void UIButtonColor_SetState_m972443567 (UIButtonColor_t1546108957 * __this, int32_t ___state0, bool ___instant1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColor::UpdateColor(System.Boolean)
extern "C"  void UIButtonColor_UpdateColor_m227044189 (UIButtonColor_t1546108957 * __this, bool ___instant0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColor::ilo_SetState1(UIButtonColor,UIButtonColor/State,System.Boolean)
extern "C"  void UIButtonColor_ilo_SetState1_m731751478 (Il2CppObject * __this /* static, unused */, UIButtonColor_t1546108957 * ____this0, int32_t ___state1, bool ___instant2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColor::ilo_OnInit2(UIButtonColor)
extern "C"  void UIButtonColor_ilo_OnInit2_m1442885369 (Il2CppObject * __this /* static, unused */, UIButtonColor_t1546108957 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColor::ilo_set_defaultColor3(UIButtonColor,UnityEngine.Color)
extern "C"  void UIButtonColor_ilo_set_defaultColor3_m2099001832 (Il2CppObject * __this /* static, unused */, UIButtonColor_t1546108957 * ____this0, Color_t4194546905  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UIButtonColor::ilo_get_color4(UIWidget)
extern "C"  Color_t4194546905  UIButtonColor_ilo_get_color4_m169378180 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonColor::ilo_IsHighlighted5(UnityEngine.GameObject)
extern "C"  bool UIButtonColor_ilo_IsHighlighted5_m2583250969 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColor::ilo_OnHover6(UIButtonColor,System.Boolean)
extern "C"  void UIButtonColor_ilo_OnHover6_m3974965418 (Il2CppObject * __this /* static, unused */, UIButtonColor_t1546108957 * ____this0, bool ___isOver1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonColor::ilo_set_value7(TweenColor,UnityEngine.Color)
extern "C"  void UIButtonColor_ilo_set_value7_m3291005012 (Il2CppObject * __this /* static, unused */, TweenColor_t2922258392 * ____this0, Color_t4194546905  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TweenColor UIButtonColor::ilo_Begin8(UnityEngine.GameObject,System.Single,UnityEngine.Color)
extern "C"  TweenColor_t2922258392 * UIButtonColor_ilo_Begin8_m2493853040 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, float ___duration1, Color_t4194546905  ___color2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
