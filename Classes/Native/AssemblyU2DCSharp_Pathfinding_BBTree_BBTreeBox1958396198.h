﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.MeshNode
struct MeshNode_t3005053445;

#include "mscorlib_System_ValueType1744280289.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.BBTree/BBTreeBox
struct  BBTreeBox_t1958396198 
{
public:
	// UnityEngine.Rect Pathfinding.BBTree/BBTreeBox::rect
	Rect_t4241904616  ___rect_0;
	// Pathfinding.MeshNode Pathfinding.BBTree/BBTreeBox::node
	MeshNode_t3005053445 * ___node_1;
	// System.Int32 Pathfinding.BBTree/BBTreeBox::left
	int32_t ___left_2;
	// System.Int32 Pathfinding.BBTree/BBTreeBox::right
	int32_t ___right_3;

public:
	inline static int32_t get_offset_of_rect_0() { return static_cast<int32_t>(offsetof(BBTreeBox_t1958396198, ___rect_0)); }
	inline Rect_t4241904616  get_rect_0() const { return ___rect_0; }
	inline Rect_t4241904616 * get_address_of_rect_0() { return &___rect_0; }
	inline void set_rect_0(Rect_t4241904616  value)
	{
		___rect_0 = value;
	}

	inline static int32_t get_offset_of_node_1() { return static_cast<int32_t>(offsetof(BBTreeBox_t1958396198, ___node_1)); }
	inline MeshNode_t3005053445 * get_node_1() const { return ___node_1; }
	inline MeshNode_t3005053445 ** get_address_of_node_1() { return &___node_1; }
	inline void set_node_1(MeshNode_t3005053445 * value)
	{
		___node_1 = value;
		Il2CppCodeGenWriteBarrier(&___node_1, value);
	}

	inline static int32_t get_offset_of_left_2() { return static_cast<int32_t>(offsetof(BBTreeBox_t1958396198, ___left_2)); }
	inline int32_t get_left_2() const { return ___left_2; }
	inline int32_t* get_address_of_left_2() { return &___left_2; }
	inline void set_left_2(int32_t value)
	{
		___left_2 = value;
	}

	inline static int32_t get_offset_of_right_3() { return static_cast<int32_t>(offsetof(BBTreeBox_t1958396198, ___right_3)); }
	inline int32_t get_right_3() const { return ___right_3; }
	inline int32_t* get_address_of_right_3() { return &___right_3; }
	inline void set_right_3(int32_t value)
	{
		___right_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Pathfinding.BBTree/BBTreeBox
struct BBTreeBox_t1958396198_marshaled_pinvoke
{
	Rect_t4241904616_marshaled_pinvoke ___rect_0;
	MeshNode_t3005053445 * ___node_1;
	int32_t ___left_2;
	int32_t ___right_3;
};
// Native definition for marshalling of: Pathfinding.BBTree/BBTreeBox
struct BBTreeBox_t1958396198_marshaled_com
{
	Rect_t4241904616_marshaled_com ___rect_0;
	MeshNode_t3005053445 * ___node_1;
	int32_t ___left_2;
	int32_t ___right_3;
};
