﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_sermemtereRoupu2
struct M_sermemtereRoupu2_t3656819710;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_sermemtereRoupu23656819710.h"

// System.Void GarbageiOS.M_sermemtereRoupu2::.ctor()
extern "C"  void M_sermemtereRoupu2__ctor_m2318628853 (M_sermemtereRoupu2_t3656819710 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sermemtereRoupu2::M_jeapi0(System.String[],System.Int32)
extern "C"  void M_sermemtereRoupu2_M_jeapi0_m1259674697 (M_sermemtereRoupu2_t3656819710 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sermemtereRoupu2::M_vairbiKowri1(System.String[],System.Int32)
extern "C"  void M_sermemtereRoupu2_M_vairbiKowri1_m4083389466 (M_sermemtereRoupu2_t3656819710 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sermemtereRoupu2::M_cadearou2(System.String[],System.Int32)
extern "C"  void M_sermemtereRoupu2_M_cadearou2_m99656884 (M_sermemtereRoupu2_t3656819710 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sermemtereRoupu2::ilo_M_jeapi01(GarbageiOS.M_sermemtereRoupu2,System.String[],System.Int32)
extern "C"  void M_sermemtereRoupu2_ilo_M_jeapi01_m3602302319 (Il2CppObject * __this /* static, unused */, M_sermemtereRoupu2_t3656819710 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sermemtereRoupu2::ilo_M_vairbiKowri12(GarbageiOS.M_sermemtereRoupu2,System.String[],System.Int32)
extern "C"  void M_sermemtereRoupu2_ilo_M_vairbiKowri12_m3736026687 (Il2CppObject * __this /* static, unused */, M_sermemtereRoupu2_t3656819710 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
