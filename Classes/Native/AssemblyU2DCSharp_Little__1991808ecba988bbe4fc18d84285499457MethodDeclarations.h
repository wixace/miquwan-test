﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._1991808ecba988bbe4fc18d849aed17f
struct _1991808ecba988bbe4fc18d849aed17f_t285499457;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._1991808ecba988bbe4fc18d849aed17f::.ctor()
extern "C"  void _1991808ecba988bbe4fc18d849aed17f__ctor_m542146316 (_1991808ecba988bbe4fc18d849aed17f_t285499457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1991808ecba988bbe4fc18d849aed17f::_1991808ecba988bbe4fc18d849aed17fm2(System.Int32)
extern "C"  int32_t _1991808ecba988bbe4fc18d849aed17f__1991808ecba988bbe4fc18d849aed17fm2_m4131627513 (_1991808ecba988bbe4fc18d849aed17f_t285499457 * __this, int32_t ____1991808ecba988bbe4fc18d849aed17fa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1991808ecba988bbe4fc18d849aed17f::_1991808ecba988bbe4fc18d849aed17fm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _1991808ecba988bbe4fc18d849aed17f__1991808ecba988bbe4fc18d849aed17fm_m1220686045 (_1991808ecba988bbe4fc18d849aed17f_t285499457 * __this, int32_t ____1991808ecba988bbe4fc18d849aed17fa0, int32_t ____1991808ecba988bbe4fc18d849aed17f01, int32_t ____1991808ecba988bbe4fc18d849aed17fc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
