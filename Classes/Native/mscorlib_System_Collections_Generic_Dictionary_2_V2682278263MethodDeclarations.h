﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3122611053MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m173256423(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2682278263 *, Dictionary_2_t3981672550 *, const MethodInfo*))ValueCollection__ctor_m2663874790_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m429233259(__this, ___item0, method) ((  void (*) (ValueCollection_t2682278263 *, List_1_t3730483581 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1617954444_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m642018996(__this, method) ((  void (*) (ValueCollection_t2682278263 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2309429589_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1870118239(__this, ___item0, method) ((  bool (*) (ValueCollection_t2682278263 *, List_1_t3730483581 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2786704666_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3515785668(__this, ___item0, method) ((  bool (*) (ValueCollection_t2682278263 *, List_1_t3730483581 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m915625023_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1261230324(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2682278263 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3096645987_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m1662467768(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2682278263 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m4135703577_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1038457799(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2682278263 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3062241876_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m145294098(__this, method) ((  bool (*) (ValueCollection_t2682278263 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1061880525_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3906851826(__this, method) ((  bool (*) (ValueCollection_t2682278263 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2995606573_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3734429284(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2682278263 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2989546841_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m3052974446(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2682278263 *, List_1U5BU5D_t3336706032*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m4149263277_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1780772055(__this, method) ((  Enumerator_t1913505958  (*) (ValueCollection_t2682278263 *, const MethodInfo*))ValueCollection_GetEnumerator_m1771510608_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::get_Count()
#define ValueCollection_get_Count_m2844378156(__this, method) ((  int32_t (*) (ValueCollection_t2682278263 *, const MethodInfo*))ValueCollection_get_Count_m3494312307_gshared)(__this, method)
