﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>
struct SortedDictionary_2_t3586408994;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary3450288660.h"

// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3706688083_gshared (Enumerator_t3450288660 * __this, SortedDictionary_2_t3586408994 * ___dic0, const MethodInfo* method);
#define Enumerator__ctor_m3706688083(__this, ___dic0, method) ((  void (*) (Enumerator_t3450288660 *, SortedDictionary_2_t3586408994 *, const MethodInfo*))Enumerator__ctor_m3706688083_gshared)(__this, ___dic0, method)
// System.Object System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m906732779_gshared (Enumerator_t3450288660 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m906732779(__this, method) ((  Il2CppObject * (*) (Enumerator_t3450288660 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m906732779_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3518822527_gshared (Enumerator_t3450288660 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3518822527(__this, method) ((  void (*) (Enumerator_t3450288660 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3518822527_gshared)(__this, method)
// TValue System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2728540235_gshared (Enumerator_t3450288660 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2728540235(__this, method) ((  int32_t (*) (Enumerator_t3450288660 *, const MethodInfo*))Enumerator_get_Current_m2728540235_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m78176983_gshared (Enumerator_t3450288660 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m78176983(__this, method) ((  bool (*) (Enumerator_t3450288660 *, const MethodInfo*))Enumerator_MoveNext_m78176983_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/ValueCollection/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m3374328472_gshared (Enumerator_t3450288660 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3374328472(__this, method) ((  void (*) (Enumerator_t3450288660 *, const MethodInfo*))Enumerator_Dispose_m3374328472_gshared)(__this, method)
