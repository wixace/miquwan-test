﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_calme46
struct M_calme46_t3057742888;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_calme463057742888.h"

// System.Void GarbageiOS.M_calme46::.ctor()
extern "C"  void M_calme46__ctor_m3796631611 (M_calme46_t3057742888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_calme46::M_zirpeJapea0(System.String[],System.Int32)
extern "C"  void M_calme46_M_zirpeJapea0_m4032428369 (M_calme46_t3057742888 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_calme46::M_reeqasDrassorwu1(System.String[],System.Int32)
extern "C"  void M_calme46_M_reeqasDrassorwu1_m1212144480 (M_calme46_t3057742888 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_calme46::M_raru2(System.String[],System.Int32)
extern "C"  void M_calme46_M_raru2_m597550590 (M_calme46_t3057742888 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_calme46::ilo_M_zirpeJapea01(GarbageiOS.M_calme46,System.String[],System.Int32)
extern "C"  void M_calme46_ilo_M_zirpeJapea01_m3286569969 (Il2CppObject * __this /* static, unused */, M_calme46_t3057742888 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_calme46::ilo_M_reeqasDrassorwu12(GarbageiOS.M_calme46,System.String[],System.Int32)
extern "C"  void M_calme46_ilo_M_reeqasDrassorwu12_m3866728165 (Il2CppObject * __this /* static, unused */, M_calme46_t3057742888 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
