﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RichFunnel/<Update>c__AnonStorey101
struct U3CUpdateU3Ec__AnonStorey101_t941073431;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.RichFunnel/<Update>c__AnonStorey101::.ctor()
extern "C"  void U3CUpdateU3Ec__AnonStorey101__ctor_m3658231092 (U3CUpdateU3Ec__AnonStorey101_t941073431 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichFunnel/<Update>c__AnonStorey101::<>m__32E(Pathfinding.GraphNode)
extern "C"  void U3CUpdateU3Ec__AnonStorey101_U3CU3Em__32E_m1615327489 (U3CUpdateU3Ec__AnonStorey101_t941073431 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
