﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DistanceWatcher
struct DistanceWatcher_t2121543655;

#include "codegen/il2cpp-codegen.h"

// System.Void DistanceWatcher::.ctor()
extern "C"  void DistanceWatcher__ctor_m1077775204 (DistanceWatcher_t2121543655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DistanceWatcher::Update()
extern "C"  void DistanceWatcher_Update_m778155049 (DistanceWatcher_t2121543655 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
