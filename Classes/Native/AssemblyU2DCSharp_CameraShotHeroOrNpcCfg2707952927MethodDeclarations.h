﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraShotHeroOrNpcCfg
struct CameraShotHeroOrNpcCfg_t2707952927;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraShotHeroOrNpcCfg::.ctor()
extern "C"  void CameraShotHeroOrNpcCfg__ctor_m2915070556 (CameraShotHeroOrNpcCfg_t2707952927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension CameraShotHeroOrNpcCfg::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * CameraShotHeroOrNpcCfg_ProtoBuf_IExtensible_GetExtensionObject_m3460787116 (CameraShotHeroOrNpcCfg_t2707952927 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotHeroOrNpcCfg::get_id()
extern "C"  int32_t CameraShotHeroOrNpcCfg_get_id_m2971666746 (CameraShotHeroOrNpcCfg_t2707952927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotHeroOrNpcCfg::set_id(System.Int32)
extern "C"  void CameraShotHeroOrNpcCfg_set_id_m571145969 (CameraShotHeroOrNpcCfg_t2707952927 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotHeroOrNpcCfg::get_npcType()
extern "C"  int32_t CameraShotHeroOrNpcCfg_get_npcType_m111706334 (CameraShotHeroOrNpcCfg_t2707952927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotHeroOrNpcCfg::set_npcType(System.Int32)
extern "C"  void CameraShotHeroOrNpcCfg_set_npcType_m2594521481 (CameraShotHeroOrNpcCfg_t2707952927 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotHeroOrNpcCfg::get_npcRoundIndex()
extern "C"  int32_t CameraShotHeroOrNpcCfg_get_npcRoundIndex_m946378952 (CameraShotHeroOrNpcCfg_t2707952927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotHeroOrNpcCfg::set_npcRoundIndex(System.Int32)
extern "C"  void CameraShotHeroOrNpcCfg_set_npcRoundIndex_m797023987 (CameraShotHeroOrNpcCfg_t2707952927 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotHeroOrNpcCfg::get_npcId()
extern "C"  int32_t CameraShotHeroOrNpcCfg_get_npcId_m1599764287 (CameraShotHeroOrNpcCfg_t2707952927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotHeroOrNpcCfg::set_npcId(System.Int32)
extern "C"  void CameraShotHeroOrNpcCfg_set_npcId_m3142450410 (CameraShotHeroOrNpcCfg_t2707952927 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
