﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RichAI/<SearchPaths>c__Iterator5
struct U3CSearchPathsU3Ec__Iterator5_t2343119703;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.RichAI/<SearchPaths>c__Iterator5::.ctor()
extern "C"  void U3CSearchPathsU3Ec__Iterator5__ctor_m3511040292 (U3CSearchPathsU3Ec__Iterator5_t2343119703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.RichAI/<SearchPaths>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSearchPathsU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1025705912 (U3CSearchPathsU3Ec__Iterator5_t2343119703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.RichAI/<SearchPaths>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSearchPathsU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m2081127244 (U3CSearchPathsU3Ec__Iterator5_t2343119703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RichAI/<SearchPaths>c__Iterator5::MoveNext()
extern "C"  bool U3CSearchPathsU3Ec__Iterator5_MoveNext_m1493859384 (U3CSearchPathsU3Ec__Iterator5_t2343119703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichAI/<SearchPaths>c__Iterator5::Dispose()
extern "C"  void U3CSearchPathsU3Ec__Iterator5_Dispose_m2458676321 (U3CSearchPathsU3Ec__Iterator5_t2343119703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RichAI/<SearchPaths>c__Iterator5::Reset()
extern "C"  void U3CSearchPathsU3Ec__Iterator5_Reset_m1157473233 (U3CSearchPathsU3Ec__Iterator5_t2343119703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
