﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Interpolate/<NewBezier>c__Iterator24`1<System.Object>
struct U3CNewBezierU3Ec__Iterator24_1_t516803767;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t1898964319;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void Interpolate/<NewBezier>c__Iterator24`1<System.Object>::.ctor()
extern "C"  void U3CNewBezierU3Ec__Iterator24_1__ctor_m2847280742_gshared (U3CNewBezierU3Ec__Iterator24_1_t516803767 * __this, const MethodInfo* method);
#define U3CNewBezierU3Ec__Iterator24_1__ctor_m2847280742(__this, method) ((  void (*) (U3CNewBezierU3Ec__Iterator24_1_t516803767 *, const MethodInfo*))U3CNewBezierU3Ec__Iterator24_1__ctor_m2847280742_gshared)(__this, method)
// UnityEngine.Vector3 Interpolate/<NewBezier>c__Iterator24`1<System.Object>::System.Collections.Generic.IEnumerator<UnityEngine.Vector3>.get_Current()
extern "C"  Vector3_t4282066566  U3CNewBezierU3Ec__Iterator24_1_System_Collections_Generic_IEnumeratorU3CUnityEngine_Vector3U3E_get_Current_m531013539_gshared (U3CNewBezierU3Ec__Iterator24_1_t516803767 * __this, const MethodInfo* method);
#define U3CNewBezierU3Ec__Iterator24_1_System_Collections_Generic_IEnumeratorU3CUnityEngine_Vector3U3E_get_Current_m531013539(__this, method) ((  Vector3_t4282066566  (*) (U3CNewBezierU3Ec__Iterator24_1_t516803767 *, const MethodInfo*))U3CNewBezierU3Ec__Iterator24_1_System_Collections_Generic_IEnumeratorU3CUnityEngine_Vector3U3E_get_Current_m531013539_gshared)(__this, method)
// System.Object Interpolate/<NewBezier>c__Iterator24`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CNewBezierU3Ec__Iterator24_1_System_Collections_IEnumerator_get_Current_m4075962880_gshared (U3CNewBezierU3Ec__Iterator24_1_t516803767 * __this, const MethodInfo* method);
#define U3CNewBezierU3Ec__Iterator24_1_System_Collections_IEnumerator_get_Current_m4075962880(__this, method) ((  Il2CppObject * (*) (U3CNewBezierU3Ec__Iterator24_1_t516803767 *, const MethodInfo*))U3CNewBezierU3Ec__Iterator24_1_System_Collections_IEnumerator_get_Current_m4075962880_gshared)(__this, method)
// System.Collections.IEnumerator Interpolate/<NewBezier>c__Iterator24`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CNewBezierU3Ec__Iterator24_1_System_Collections_IEnumerable_GetEnumerator_m598137915_gshared (U3CNewBezierU3Ec__Iterator24_1_t516803767 * __this, const MethodInfo* method);
#define U3CNewBezierU3Ec__Iterator24_1_System_Collections_IEnumerable_GetEnumerator_m598137915(__this, method) ((  Il2CppObject * (*) (U3CNewBezierU3Ec__Iterator24_1_t516803767 *, const MethodInfo*))U3CNewBezierU3Ec__Iterator24_1_System_Collections_IEnumerable_GetEnumerator_m598137915_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3> Interpolate/<NewBezier>c__Iterator24`1<System.Object>::System.Collections.Generic.IEnumerable<UnityEngine.Vector3>.GetEnumerator()
extern "C"  Il2CppObject* U3CNewBezierU3Ec__Iterator24_1_System_Collections_Generic_IEnumerableU3CUnityEngine_Vector3U3E_GetEnumerator_m4286694992_gshared (U3CNewBezierU3Ec__Iterator24_1_t516803767 * __this, const MethodInfo* method);
#define U3CNewBezierU3Ec__Iterator24_1_System_Collections_Generic_IEnumerableU3CUnityEngine_Vector3U3E_GetEnumerator_m4286694992(__this, method) ((  Il2CppObject* (*) (U3CNewBezierU3Ec__Iterator24_1_t516803767 *, const MethodInfo*))U3CNewBezierU3Ec__Iterator24_1_System_Collections_Generic_IEnumerableU3CUnityEngine_Vector3U3E_GetEnumerator_m4286694992_gshared)(__this, method)
// System.Boolean Interpolate/<NewBezier>c__Iterator24`1<System.Object>::MoveNext()
extern "C"  bool U3CNewBezierU3Ec__Iterator24_1_MoveNext_m1382927566_gshared (U3CNewBezierU3Ec__Iterator24_1_t516803767 * __this, const MethodInfo* method);
#define U3CNewBezierU3Ec__Iterator24_1_MoveNext_m1382927566(__this, method) ((  bool (*) (U3CNewBezierU3Ec__Iterator24_1_t516803767 *, const MethodInfo*))U3CNewBezierU3Ec__Iterator24_1_MoveNext_m1382927566_gshared)(__this, method)
// System.Void Interpolate/<NewBezier>c__Iterator24`1<System.Object>::Dispose()
extern "C"  void U3CNewBezierU3Ec__Iterator24_1_Dispose_m240908579_gshared (U3CNewBezierU3Ec__Iterator24_1_t516803767 * __this, const MethodInfo* method);
#define U3CNewBezierU3Ec__Iterator24_1_Dispose_m240908579(__this, method) ((  void (*) (U3CNewBezierU3Ec__Iterator24_1_t516803767 *, const MethodInfo*))U3CNewBezierU3Ec__Iterator24_1_Dispose_m240908579_gshared)(__this, method)
// System.Void Interpolate/<NewBezier>c__Iterator24`1<System.Object>::Reset()
extern "C"  void U3CNewBezierU3Ec__Iterator24_1_Reset_m493713683_gshared (U3CNewBezierU3Ec__Iterator24_1_t516803767 * __this, const MethodInfo* method);
#define U3CNewBezierU3Ec__Iterator24_1_Reset_m493713683(__this, method) ((  void (*) (U3CNewBezierU3Ec__Iterator24_1_t516803767 *, const MethodInfo*))U3CNewBezierU3Ec__Iterator24_1_Reset_m493713683_gshared)(__this, method)
