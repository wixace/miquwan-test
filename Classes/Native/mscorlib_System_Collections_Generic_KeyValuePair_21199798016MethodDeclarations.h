﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21199798016.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m923468223_gshared (KeyValuePair_2_t1199798016 * __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m923468223(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1199798016 *, Il2CppObject *, stValue_t3425945410 , const MethodInfo*))KeyValuePair_2__ctor_m923468223_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2179442505_gshared (KeyValuePair_2_t1199798016 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2179442505(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1199798016 *, const MethodInfo*))KeyValuePair_2_get_Key_m2179442505_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2806775050_gshared (KeyValuePair_2_t1199798016 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m2806775050(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1199798016 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m2806775050_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>::get_Value()
extern "C"  stValue_t3425945410  KeyValuePair_2_get_Value_m4264011977_gshared (KeyValuePair_2_t1199798016 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m4264011977(__this, method) ((  stValue_t3425945410  (*) (KeyValuePair_2_t1199798016 *, const MethodInfo*))KeyValuePair_2_get_Value_m4264011977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3958225930_gshared (KeyValuePair_2_t1199798016 * __this, stValue_t3425945410  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m3958225930(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1199798016 *, stValue_t3425945410 , const MethodInfo*))KeyValuePair_2_set_Value_m3958225930_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m284841432_gshared (KeyValuePair_2_t1199798016 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m284841432(__this, method) ((  String_t* (*) (KeyValuePair_2_t1199798016 *, const MethodInfo*))KeyValuePair_2_ToString_m284841432_gshared)(__this, method)
