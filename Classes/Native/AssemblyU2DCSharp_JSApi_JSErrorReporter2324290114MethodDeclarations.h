﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSApi/JSErrorReporter
struct JSErrorReporter_t2324290114;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void JSApi/JSErrorReporter::.ctor(System.Object,System.IntPtr)
extern "C"  void JSErrorReporter__ctor_m70121753 (JSErrorReporter_t2324290114 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi/JSErrorReporter::Invoke(System.IntPtr,System.String,System.IntPtr)
extern "C"  int32_t JSErrorReporter_Invoke_m1319350745 (JSErrorReporter_t2324290114 * __this, IntPtr_t ___cx0, String_t* ___message1, IntPtr_t ___report2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult JSApi/JSErrorReporter::BeginInvoke(System.IntPtr,System.String,System.IntPtr,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * JSErrorReporter_BeginInvoke_m3382039200 (JSErrorReporter_t2324290114 * __this, IntPtr_t ___cx0, String_t* ___message1, IntPtr_t ___report2, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___object4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi/JSErrorReporter::EndInvoke(System.IAsyncResult)
extern "C"  int32_t JSErrorReporter_EndInvoke_m4098026075 (JSErrorReporter_t2324290114 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
