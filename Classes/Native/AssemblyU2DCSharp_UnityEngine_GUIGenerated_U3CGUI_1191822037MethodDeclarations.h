﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUIGenerated/<GUI_Window_GetDelegate_member109_arg2>c__AnonStoreyF1
struct U3CGUI_Window_GetDelegate_member109_arg2U3Ec__AnonStoreyF1_t1191822037;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_GUIGenerated/<GUI_Window_GetDelegate_member109_arg2>c__AnonStoreyF1::.ctor()
extern "C"  void U3CGUI_Window_GetDelegate_member109_arg2U3Ec__AnonStoreyF1__ctor_m3131581494 (U3CGUI_Window_GetDelegate_member109_arg2U3Ec__AnonStoreyF1_t1191822037 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated/<GUI_Window_GetDelegate_member109_arg2>c__AnonStoreyF1::<>m__1D8(System.Int32)
extern "C"  void U3CGUI_Window_GetDelegate_member109_arg2U3Ec__AnonStoreyF1_U3CU3Em__1D8_m1462359225 (U3CGUI_Window_GetDelegate_member109_arg2U3Ec__AnonStoreyF1_t1191822037 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
