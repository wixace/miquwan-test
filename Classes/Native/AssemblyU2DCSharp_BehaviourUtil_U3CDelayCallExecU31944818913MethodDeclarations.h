﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Int32,System.Object,UnityEngine.Vector3>
struct U3CDelayCallExecU3Ec__Iterator41_3_t1944818913;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Int32,System.Object,UnityEngine.Vector3>::.ctor()
extern "C"  void U3CDelayCallExecU3Ec__Iterator41_3__ctor_m1448987838_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t1944818913 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3__ctor_m1448987838(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator41_3_t1944818913 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3__ctor_m1448987838_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Int32,System.Object,UnityEngine.Vector3>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1975706206_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t1944818913 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1975706206(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator41_3_t1944818913 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1975706206_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Int32,System.Object,UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_IEnumerator_get_Current_m1930519026_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t1944818913 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_IEnumerator_get_Current_m1930519026(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator41_3_t1944818913 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_IEnumerator_get_Current_m1930519026_gshared)(__this, method)
// System.Boolean BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Int32,System.Object,UnityEngine.Vector3>::MoveNext()
extern "C"  bool U3CDelayCallExecU3Ec__Iterator41_3_MoveNext_m1847611998_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t1944818913 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_MoveNext_m1847611998(__this, method) ((  bool (*) (U3CDelayCallExecU3Ec__Iterator41_3_t1944818913 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_MoveNext_m1847611998_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Int32,System.Object,UnityEngine.Vector3>::Dispose()
extern "C"  void U3CDelayCallExecU3Ec__Iterator41_3_Dispose_m806191483_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t1944818913 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_Dispose_m806191483(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator41_3_t1944818913 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_Dispose_m806191483_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Int32,System.Object,UnityEngine.Vector3>::Reset()
extern "C"  void U3CDelayCallExecU3Ec__Iterator41_3_Reset_m3390388075_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t1944818913 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_Reset_m3390388075(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator41_3_t1944818913 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_Reset_m3390388075_gshared)(__this, method)
