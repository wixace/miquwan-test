﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.UnitStateBvr
struct  UnitStateBvr_t181606353  : public IBehavior_t770859129
{
public:
	// System.Single Entity.Behavior.UnitStateBvr::doTime
	float ___doTime_2;
	// System.Boolean Entity.Behavior.UnitStateBvr::isLock
	bool ___isLock_3;

public:
	inline static int32_t get_offset_of_doTime_2() { return static_cast<int32_t>(offsetof(UnitStateBvr_t181606353, ___doTime_2)); }
	inline float get_doTime_2() const { return ___doTime_2; }
	inline float* get_address_of_doTime_2() { return &___doTime_2; }
	inline void set_doTime_2(float value)
	{
		___doTime_2 = value;
	}

	inline static int32_t get_offset_of_isLock_3() { return static_cast<int32_t>(offsetof(UnitStateBvr_t181606353, ___isLock_3)); }
	inline bool get_isLock_3() const { return ___isLock_3; }
	inline bool* get_address_of_isLock_3() { return &___isLock_3; }
	inline void set_isLock_3(bool value)
	{
		___isLock_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
