﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnitStateFlash
struct UnitStateFlash_t2186009251;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// CombatEntity
struct CombatEntity_t684137495;
// TargetMgr
struct TargetMgr_t1188374183;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UnitStateID1002198824.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_TargetMgr1188374183.h"

// System.Void UnitStateFlash::.ctor()
extern "C"  void UnitStateFlash__ctor_m1564621144 (UnitStateFlash_t2186009251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnitStateID UnitStateFlash::get_type()
extern "C"  uint8_t UnitStateFlash_get_type_m2998448826 (UnitStateFlash_t2186009251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFlash::SetParams(System.Object[])
extern "C"  void UnitStateFlash_SetParams_m844119412 (UnitStateFlash_t2186009251 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFlash::EnterState()
extern "C"  void UnitStateFlash_EnterState_m716545797 (UnitStateFlash_t2186009251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFlash::DoEnter()
extern "C"  void UnitStateFlash_DoEnter_m4199034627 (UnitStateFlash_t2186009251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFlash::OnPathComplete()
extern "C"  void UnitStateFlash_OnPathComplete_m1638515689 (UnitStateFlash_t2186009251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFlash::LeaveState()
extern "C"  void UnitStateFlash_LeaveState_m2502921798 (UnitStateFlash_t2186009251 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFlash::Update(System.Single)
extern "C"  void UnitStateFlash_Update_m3403565846 (UnitStateFlash_t2186009251 * __this, float ___deltatime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFlash::ilo_set_forceShift1(CombatEntity,System.Boolean)
extern "C"  void UnitStateFlash_ilo_set_forceShift1_m1906559220 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnitStateFlash::ilo_get_isDeath2(CombatEntity)
extern "C"  bool UnitStateFlash_ilo_get_isDeath2_m3348981197 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnitStateFlash::ilo_GetTargetPointInConnect3(TargetMgr,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  UnitStateFlash_ilo_GetTargetPointInConnect3_m1215377793 (Il2CppObject * __this /* static, unused */, TargetMgr_t1188374183 * ____this0, Vector3_t4282066566  ___startPoint1, Vector3_t4282066566  ___endPoint2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl UnitStateFlash::ilo_get_bvrCtrl4(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * UnitStateFlash_ilo_get_bvrCtrl4_m1007307866 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFlash::ilo_set_isMovement5(CombatEntity,System.Boolean)
extern "C"  void UnitStateFlash_ilo_set_isMovement5_m3889400722 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnitStateFlash::ilo_LookAt6(CombatEntity,UnityEngine.Vector3)
extern "C"  void UnitStateFlash_ilo_LookAt6_m3267180419 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, Vector3_t4282066566  ___pos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
