﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// iOSUIScreenEffect
struct iOSUIScreenEffect_t1658945310;
// UnityEngine.Material
struct Material_t3870600107;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionInfo
struct VersionInfo_t2356638086;
// VersionMgr
struct VersionMgr_t1322950208;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_iOSUIScreenEffect1658945310.h"

// System.Void iOSUIScreenEffect::.ctor()
extern "C"  void iOSUIScreenEffect__ctor_m4056698893 (iOSUIScreenEffect_t1658945310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material iOSUIScreenEffect::get_material()
extern "C"  Material_t3870600107 * iOSUIScreenEffect_get_material_m3955445672 (iOSUIScreenEffect_t1658945310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iOSUIScreenEffect::Awake()
extern "C"  void iOSUIScreenEffect_Awake_m4294304112 (iOSUIScreenEffect_t1658945310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iOSUIScreenEffect::OnSceneLoadOK(CEvent.ZEvent)
extern "C"  void iOSUIScreenEffect_OnSceneLoadOK_m2587366849 (iOSUIScreenEffect_t1658945310 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iOSUIScreenEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void iOSUIScreenEffect_OnRenderImage_m1734869041 (iOSUIScreenEffect_t1658945310 * __this, RenderTexture_t1963041563 * ___sourceTexture0, RenderTexture_t1963041563 * ___destTexture1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iOSUIScreenEffect::ilo_Log1(System.Object,System.Boolean)
extern "C"  void iOSUIScreenEffect_ilo_Log1_m3772994976 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iOSUIScreenEffect::ilo_AddEventListener2(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void iOSUIScreenEffect_ilo_AddEventListener2_m1842641254 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo iOSUIScreenEffect::ilo_get_currentVS3(VersionMgr)
extern "C"  VersionInfo_t2356638086 * iOSUIScreenEffect_ilo_get_currentVS3_m1327973123 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void iOSUIScreenEffect::ilo_RemoveEventListener4(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void iOSUIScreenEffect_ilo_RemoveEventListener4_m1512889519 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material iOSUIScreenEffect::ilo_get_material5(iOSUIScreenEffect)
extern "C"  Material_t3870600107 * iOSUIScreenEffect_ilo_get_material5_m3715570232 (Il2CppObject * __this /* static, unused */, iOSUIScreenEffect_t1658945310 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
