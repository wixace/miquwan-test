﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>
struct Dictionary_2_t2048431866;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4275232570.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<SoundTypeID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m4237321959_gshared (Enumerator_t4275232570 * __this, Dictionary_2_t2048431866 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m4237321959(__this, ___host0, method) ((  void (*) (Enumerator_t4275232570 *, Dictionary_2_t2048431866 *, const MethodInfo*))Enumerator__ctor_m4237321959_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<SoundTypeID,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m131871578_gshared (Enumerator_t4275232570 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m131871578(__this, method) ((  Il2CppObject * (*) (Enumerator_t4275232570 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m131871578_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<SoundTypeID,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1029883758_gshared (Enumerator_t4275232570 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1029883758(__this, method) ((  void (*) (Enumerator_t4275232570 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1029883758_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<SoundTypeID,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3727377737_gshared (Enumerator_t4275232570 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3727377737(__this, method) ((  void (*) (Enumerator_t4275232570 *, const MethodInfo*))Enumerator_Dispose_m3727377737_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<SoundTypeID,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1132755162_gshared (Enumerator_t4275232570 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1132755162(__this, method) ((  bool (*) (Enumerator_t4275232570 *, const MethodInfo*))Enumerator_MoveNext_m1132755162_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<SoundTypeID,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2143848_gshared (Enumerator_t4275232570 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2143848(__this, method) ((  Il2CppObject * (*) (Enumerator_t4275232570 *, const MethodInfo*))Enumerator_get_Current_m2143848_gshared)(__this, method)
