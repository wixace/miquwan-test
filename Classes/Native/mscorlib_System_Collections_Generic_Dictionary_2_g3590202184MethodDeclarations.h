﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>
struct Dictionary_2_t3590202184;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t1944872904;
// System.Collections.Generic.IDictionary`2<System.Int32,SoundStatus>
struct IDictionary_2_t3168075529;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t2048428487;
// System.Collections.Generic.ICollection`1<SoundStatus>
struct ICollection_1_t192561636;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>[]
struct KeyValuePair_2U5BU5D_t1889225647;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>>
struct IEnumerator_1_t1105880643;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,SoundStatus>
struct KeyCollection_t921994339;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,SoundStatus>
struct ValueCollection_t2290807897;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23488982890.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_SoundStatus3592938945.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En612558280.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::.ctor()
extern "C"  void Dictionary_2__ctor_m1034127606_gshared (Dictionary_2_t3590202184 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1034127606(__this, method) ((  void (*) (Dictionary_2_t3590202184 *, const MethodInfo*))Dictionary_2__ctor_m1034127606_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m723642934_gshared (Dictionary_2_t3590202184 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m723642934(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3590202184 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m723642934_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m973820345_gshared (Dictionary_2_t3590202184 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m973820345(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t3590202184 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m973820345_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m1964143632_gshared (Dictionary_2_t3590202184 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m1964143632(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3590202184 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1964143632_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3383742436_gshared (Dictionary_2_t3590202184 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m3383742436(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t3590202184 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3383742436_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m619162112_gshared (Dictionary_2_t3590202184 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m619162112(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3590202184 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m619162112_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1659016569_gshared (Dictionary_2_t3590202184 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1659016569(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3590202184 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1659016569_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3140391701_gshared (Dictionary_2_t3590202184 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3140391701(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3590202184 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3140391701_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m3823262909_gshared (Dictionary_2_t3590202184 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m3823262909(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3590202184 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m3823262909_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m31204971_gshared (Dictionary_2_t3590202184 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m31204971(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3590202184 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m31204971_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3057909310_gshared (Dictionary_2_t3590202184 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3057909310(__this, method) ((  bool (*) (Dictionary_2_t3590202184 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3057909310_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2630861051_gshared (Dictionary_2_t3590202184 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2630861051(__this, method) ((  bool (*) (Dictionary_2_t3590202184 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m2630861051_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m4116270495_gshared (Dictionary_2_t3590202184 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m4116270495(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3590202184 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m4116270495_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m1906953294_gshared (Dictionary_2_t3590202184 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m1906953294(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3590202184 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m1906953294_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m293960003_gshared (Dictionary_2_t3590202184 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m293960003(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3590202184 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m293960003_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m125024143_gshared (Dictionary_2_t3590202184 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m125024143(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3590202184 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m125024143_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m1072341644_gshared (Dictionary_2_t3590202184 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m1072341644(__this, ___key0, method) ((  void (*) (Dictionary_2_t3590202184 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m1072341644_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1431878181_gshared (Dictionary_2_t3590202184 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1431878181(__this, method) ((  bool (*) (Dictionary_2_t3590202184 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1431878181_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2762644247_gshared (Dictionary_2_t3590202184 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2762644247(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3590202184 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2762644247_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3209087529_gshared (Dictionary_2_t3590202184 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3209087529(__this, method) ((  bool (*) (Dictionary_2_t3590202184 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3209087529_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3471481122_gshared (Dictionary_2_t3590202184 * __this, KeyValuePair_2_t3488982890  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3471481122(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3590202184 *, KeyValuePair_2_t3488982890 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3471481122_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m527398052_gshared (Dictionary_2_t3590202184 * __this, KeyValuePair_2_t3488982890  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m527398052(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3590202184 *, KeyValuePair_2_t3488982890 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m527398052_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2593408134_gshared (Dictionary_2_t3590202184 * __this, KeyValuePair_2U5BU5D_t1889225647* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2593408134(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3590202184 *, KeyValuePair_2U5BU5D_t1889225647*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2593408134_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m886731977_gshared (Dictionary_2_t3590202184 * __this, KeyValuePair_2_t3488982890  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m886731977(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3590202184 *, KeyValuePair_2_t3488982890 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m886731977_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m1318570213_gshared (Dictionary_2_t3590202184 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1318570213(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3590202184 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1318570213_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m691129012_gshared (Dictionary_2_t3590202184 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m691129012(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3590202184 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m691129012_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m866845355_gshared (Dictionary_2_t3590202184 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m866845355(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3590202184 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m866845355_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1879338232_gshared (Dictionary_2_t3590202184 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1879338232(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3590202184 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1879338232_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m2289635551_gshared (Dictionary_2_t3590202184 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m2289635551(__this, method) ((  int32_t (*) (Dictionary_2_t3590202184 *, const MethodInfo*))Dictionary_2_get_Count_m2289635551_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::get_Item(TKey)
extern "C"  int32_t Dictionary_2_get_Item_m773076462_gshared (Dictionary_2_t3590202184 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m773076462(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t3590202184 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m773076462_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1732001895_gshared (Dictionary_2_t3590202184 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1732001895(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3590202184 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_set_Item_m1732001895_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m927007927_gshared (Dictionary_2_t3590202184 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m927007927(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3590202184 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m927007927_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m422165472_gshared (Dictionary_2_t3590202184 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m422165472(__this, ___size0, method) ((  void (*) (Dictionary_2_t3590202184 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m422165472_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m260460508_gshared (Dictionary_2_t3590202184 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m260460508(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3590202184 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m260460508_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t3488982890  Dictionary_2_make_pair_m2671632752_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m2671632752(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3488982890  (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_make_pair_m2671632752_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m1605525582_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m1605525582(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_key_m1605525582_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::pick_value(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_value_m542562730_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m542562730(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_value_m542562730_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m983802995_gshared (Dictionary_2_t3590202184 * __this, KeyValuePair_2U5BU5D_t1889225647* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m983802995(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3590202184 *, KeyValuePair_2U5BU5D_t1889225647*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m983802995_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::Resize()
extern "C"  void Dictionary_2_Resize_m2777737945_gshared (Dictionary_2_t3590202184 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m2777737945(__this, method) ((  void (*) (Dictionary_2_t3590202184 *, const MethodInfo*))Dictionary_2_Resize_m2777737945_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m4223923056_gshared (Dictionary_2_t3590202184 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m4223923056(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3590202184 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_Add_m4223923056_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::Clear()
extern "C"  void Dictionary_2_Clear_m3589955946_gshared (Dictionary_2_t3590202184 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m3589955946(__this, method) ((  void (*) (Dictionary_2_t3590202184 *, const MethodInfo*))Dictionary_2_Clear_m3589955946_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2075691065_gshared (Dictionary_2_t3590202184 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2075691065(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3590202184 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2075691065_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m1669844756_gshared (Dictionary_2_t3590202184 * __this, int32_t ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1669844756(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3590202184 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m1669844756_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m1071575389_gshared (Dictionary_2_t3590202184 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1071575389(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3590202184 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m1071575389_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m337465639_gshared (Dictionary_2_t3590202184 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m337465639(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3590202184 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m337465639_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m367867740_gshared (Dictionary_2_t3590202184 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m367867740(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3590202184 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m367867740_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m3474794861_gshared (Dictionary_2_t3590202184 * __this, int32_t ___key0, int32_t* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m3474794861(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3590202184 *, int32_t, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m3474794861_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::get_Keys()
extern "C"  KeyCollection_t921994339 * Dictionary_2_get_Keys_m4169644366_gshared (Dictionary_2_t3590202184 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m4169644366(__this, method) ((  KeyCollection_t921994339 * (*) (Dictionary_2_t3590202184 *, const MethodInfo*))Dictionary_2_get_Keys_m4169644366_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::get_Values()
extern "C"  ValueCollection_t2290807897 * Dictionary_2_get_Values_m322525290_gshared (Dictionary_2_t3590202184 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m322525290(__this, method) ((  ValueCollection_t2290807897 * (*) (Dictionary_2_t3590202184 *, const MethodInfo*))Dictionary_2_get_Values_m322525290_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m1055384489_gshared (Dictionary_2_t3590202184 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1055384489(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t3590202184 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1055384489_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::ToTValue(System.Object)
extern "C"  int32_t Dictionary_2_ToTValue_m3149844165_gshared (Dictionary_2_t3590202184 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m3149844165(__this, ___value0, method) ((  int32_t (*) (Dictionary_2_t3590202184 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m3149844165_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m2208662847_gshared (Dictionary_2_t3590202184 * __this, KeyValuePair_2_t3488982890  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m2208662847(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3590202184 *, KeyValuePair_2_t3488982890 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m2208662847_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::GetEnumerator()
extern "C"  Enumerator_t612558280  Dictionary_2_GetEnumerator_m1483081738_gshared (Dictionary_2_t3590202184 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1483081738(__this, method) ((  Enumerator_t612558280  (*) (Dictionary_2_t3590202184 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1483081738_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,SoundStatus>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m4178590209_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m4178590209(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m4178590209_gshared)(__this /* static, unused */, ___key0, ___value1, method)
