﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Util.TileHandler
struct TileHandler_t1505605502;
// System.Collections.Generic.List`1<UnityEngine.Bounds>
struct List_1_t4079827401;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.TileHandlerHelper
struct  TileHandlerHelper_t3135677676  : public MonoBehaviour_t667441552
{
public:
	// Pathfinding.Util.TileHandler Pathfinding.TileHandlerHelper::handler
	TileHandler_t1505605502 * ___handler_2;
	// System.Single Pathfinding.TileHandlerHelper::updateInterval
	float ___updateInterval_3;
	// System.Single Pathfinding.TileHandlerHelper::lastUpdateTime
	float ___lastUpdateTime_4;
	// System.Collections.Generic.List`1<UnityEngine.Bounds> Pathfinding.TileHandlerHelper::forcedReloadBounds
	List_1_t4079827401 * ___forcedReloadBounds_5;

public:
	inline static int32_t get_offset_of_handler_2() { return static_cast<int32_t>(offsetof(TileHandlerHelper_t3135677676, ___handler_2)); }
	inline TileHandler_t1505605502 * get_handler_2() const { return ___handler_2; }
	inline TileHandler_t1505605502 ** get_address_of_handler_2() { return &___handler_2; }
	inline void set_handler_2(TileHandler_t1505605502 * value)
	{
		___handler_2 = value;
		Il2CppCodeGenWriteBarrier(&___handler_2, value);
	}

	inline static int32_t get_offset_of_updateInterval_3() { return static_cast<int32_t>(offsetof(TileHandlerHelper_t3135677676, ___updateInterval_3)); }
	inline float get_updateInterval_3() const { return ___updateInterval_3; }
	inline float* get_address_of_updateInterval_3() { return &___updateInterval_3; }
	inline void set_updateInterval_3(float value)
	{
		___updateInterval_3 = value;
	}

	inline static int32_t get_offset_of_lastUpdateTime_4() { return static_cast<int32_t>(offsetof(TileHandlerHelper_t3135677676, ___lastUpdateTime_4)); }
	inline float get_lastUpdateTime_4() const { return ___lastUpdateTime_4; }
	inline float* get_address_of_lastUpdateTime_4() { return &___lastUpdateTime_4; }
	inline void set_lastUpdateTime_4(float value)
	{
		___lastUpdateTime_4 = value;
	}

	inline static int32_t get_offset_of_forcedReloadBounds_5() { return static_cast<int32_t>(offsetof(TileHandlerHelper_t3135677676, ___forcedReloadBounds_5)); }
	inline List_1_t4079827401 * get_forcedReloadBounds_5() const { return ___forcedReloadBounds_5; }
	inline List_1_t4079827401 ** get_address_of_forcedReloadBounds_5() { return &___forcedReloadBounds_5; }
	inline void set_forcedReloadBounds_5(List_1_t4079827401 * value)
	{
		___forcedReloadBounds_5 = value;
		Il2CppCodeGenWriteBarrier(&___forcedReloadBounds_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
