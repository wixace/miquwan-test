﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_skill_uieffect
struct Float_skill_uieffect_t2262955958;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;
// FloatTextUnit
struct FloatTextUnit_t2362298029;
// System.String
struct String_t;
// Mihua.Asset.OnLoadAsset
struct OnLoadAsset_t4181543125;
// UIEffect
struct UIEffect_t251031877;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_Asset_OnLoadAsset4181543125.h"
#include "AssemblyU2DCSharp_UIEffect251031877.h"

// System.Void Float_skill_uieffect::.ctor()
extern "C"  void Float_skill_uieffect__ctor_m1170386853 (Float_skill_uieffect_t2262955958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_skill_uieffect::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_skill_uieffect_OnAwake_m1838756961 (Float_skill_uieffect_t2262955958 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_skill_uieffect::OnDestroy()
extern "C"  void Float_skill_uieffect_OnDestroy_m3485084190 (Float_skill_uieffect_t2262955958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_skill_uieffect::FloatID()
extern "C"  int32_t Float_skill_uieffect_FloatID_m2389833265 (Float_skill_uieffect_t2262955958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_skill_uieffect::Init()
extern "C"  void Float_skill_uieffect_Init_m3730069007 (Float_skill_uieffect_t2262955958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_skill_uieffect::Death()
extern "C"  void Float_skill_uieffect_Death_m3555449399 (Float_skill_uieffect_t2262955958 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_skill_uieffect::SetFile(System.Object[])
extern "C"  void Float_skill_uieffect_SetFile_m3738472401 (Float_skill_uieffect_t2262955958 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_skill_uieffect::<SetFile>m__3E1(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  void Float_skill_uieffect_U3CSetFileU3Em__3E1_m1033265464 (Float_skill_uieffect_t2262955958 * __this, AssetOperation_t778728221 * ___ao0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_skill_uieffect::ilo_OnDestroy1(FloatTextUnit)
extern "C"  void Float_skill_uieffect_ilo_OnDestroy1_m2986080125 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Asset.ABLoadOperation.AssetOperation Float_skill_uieffect::ilo_LoadAssetAsync2(System.String,Mihua.Asset.OnLoadAsset)
extern "C"  AssetOperation_t778728221 * Float_skill_uieffect_ilo_LoadAssetAsync2_m3669411371 (Il2CppObject * __this /* static, unused */, String_t* ___assetName0, OnLoadAsset_t4181543125 * ___onLoadAsset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_skill_uieffect::ilo_ChangeParent3(UnityEngine.GameObject,UnityEngine.GameObject,System.Boolean)
extern "C"  void Float_skill_uieffect_ilo_ChangeParent3_m2741068320 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, GameObject_t3674682005 * ___parent1, bool ___resetMat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_skill_uieffect::ilo_Init4(UIEffect)
extern "C"  void Float_skill_uieffect_ilo_Init4_m1462044335 (Il2CppObject * __this /* static, unused */, UIEffect_t251031877 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
