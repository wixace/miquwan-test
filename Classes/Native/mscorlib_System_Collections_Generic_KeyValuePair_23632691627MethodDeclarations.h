﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Scripts.JSBindingClasses.JsRepresentClass>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m920615252(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3632691627 *, String_t*, JsRepresentClass_t2913492551 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,Scripts.JSBindingClasses.JsRepresentClass>::get_Key()
#define KeyValuePair_2_get_Key_m2292276116(__this, method) ((  String_t* (*) (KeyValuePair_2_t3632691627 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Scripts.JSBindingClasses.JsRepresentClass>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1914068053(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3632691627 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,Scripts.JSBindingClasses.JsRepresentClass>::get_Value()
#define KeyValuePair_2_get_Value_m1230266424(__this, method) ((  JsRepresentClass_t2913492551 * (*) (KeyValuePair_2_t3632691627 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,Scripts.JSBindingClasses.JsRepresentClass>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2157018581(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3632691627 *, JsRepresentClass_t2913492551 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,Scripts.JSBindingClasses.JsRepresentClass>::ToString()
#define KeyValuePair_2_ToString_m2574734419(__this, method) ((  String_t* (*) (KeyValuePair_2_t3632691627 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
