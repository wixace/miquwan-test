﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DragRigidbody/$DragObject$58/$
struct U24_t418036571;
// DragRigidbody
struct DragRigidbody_t2531437401;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScriptU2Dfirstpass_DragRigidbody2531437401.h"

// System.Void DragRigidbody/$DragObject$58/$::.ctor(System.Single,DragRigidbody)
extern "C"  void U24__ctor_m4141945653 (U24_t418036571 * __this, float ___distance0, DragRigidbody_t2531437401 * ___self_1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DragRigidbody/$DragObject$58/$::MoveNext()
extern "C"  bool U24_MoveNext_m3111963941 (U24_t418036571 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
