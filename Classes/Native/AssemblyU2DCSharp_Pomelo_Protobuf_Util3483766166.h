﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String[]
struct StringU5BU5D_t4054002952;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pomelo.Protobuf.Util
struct  Util_t3483766166  : public Il2CppObject
{
public:
	// System.String[] Pomelo.Protobuf.Util::types
	StringU5BU5D_t4054002952* ___types_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Pomelo.Protobuf.Util::typeMap
	Dictionary_2_t1974256870 * ___typeMap_1;

public:
	inline static int32_t get_offset_of_types_0() { return static_cast<int32_t>(offsetof(Util_t3483766166, ___types_0)); }
	inline StringU5BU5D_t4054002952* get_types_0() const { return ___types_0; }
	inline StringU5BU5D_t4054002952** get_address_of_types_0() { return &___types_0; }
	inline void set_types_0(StringU5BU5D_t4054002952* value)
	{
		___types_0 = value;
		Il2CppCodeGenWriteBarrier(&___types_0, value);
	}

	inline static int32_t get_offset_of_typeMap_1() { return static_cast<int32_t>(offsetof(Util_t3483766166, ___typeMap_1)); }
	inline Dictionary_2_t1974256870 * get_typeMap_1() const { return ___typeMap_1; }
	inline Dictionary_2_t1974256870 ** get_address_of_typeMap_1() { return &___typeMap_1; }
	inline void set_typeMap_1(Dictionary_2_t1974256870 * value)
	{
		___typeMap_1 = value;
		Il2CppCodeGenWriteBarrier(&___typeMap_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
