﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._0a62ee45ea1c0830dcf23cc9c34830a8
struct _0a62ee45ea1c0830dcf23cc9c34830a8_t4117012819;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._0a62ee45ea1c0830dcf23cc9c34830a8::.ctor()
extern "C"  void _0a62ee45ea1c0830dcf23cc9c34830a8__ctor_m4208153146 (_0a62ee45ea1c0830dcf23cc9c34830a8_t4117012819 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._0a62ee45ea1c0830dcf23cc9c34830a8::_0a62ee45ea1c0830dcf23cc9c34830a8m2(System.Int32)
extern "C"  int32_t _0a62ee45ea1c0830dcf23cc9c34830a8__0a62ee45ea1c0830dcf23cc9c34830a8m2_m2684385465 (_0a62ee45ea1c0830dcf23cc9c34830a8_t4117012819 * __this, int32_t ____0a62ee45ea1c0830dcf23cc9c34830a8a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._0a62ee45ea1c0830dcf23cc9c34830a8::_0a62ee45ea1c0830dcf23cc9c34830a8m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _0a62ee45ea1c0830dcf23cc9c34830a8__0a62ee45ea1c0830dcf23cc9c34830a8m_m3640685085 (_0a62ee45ea1c0830dcf23cc9c34830a8_t4117012819 * __this, int32_t ____0a62ee45ea1c0830dcf23cc9c34830a8a0, int32_t ____0a62ee45ea1c0830dcf23cc9c34830a8651, int32_t ____0a62ee45ea1c0830dcf23cc9c34830a8c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
