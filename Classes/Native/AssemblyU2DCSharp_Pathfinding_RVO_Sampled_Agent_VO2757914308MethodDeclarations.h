﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RVO.Sampled.Agent/VO
struct VO_t2757914308;
struct VO_t2757914308_marshaled_pinvoke;
struct VO_t2757914308_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_Sampled_Agent_VO2757914308.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void Pathfinding.RVO.Sampled.Agent/VO::.ctor(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C"  void VO__ctor_m2168030096 (VO_t2757914308 * __this, Vector2_t4282066565  ___offset0, Vector2_t4282066565  ___p01, Vector2_t4282066565  ___dir2, float ___weightFactor3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent/VO::.ctor(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern "C"  void VO__ctor_m741216120 (VO_t2757914308 * __this, Vector2_t4282066565  ___offset0, Vector2_t4282066565  ___p11, Vector2_t4282066565  ___p22, Vector2_t4282066565  ___tang13, Vector2_t4282066565  ___tang24, float ___weightFactor5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Sampled.Agent/VO::.ctor(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.Vector2,System.Single,System.Single)
extern "C"  void VO__ctor_m3369181978 (VO_t2757914308 * __this, Vector2_t4282066565  ___center0, Vector2_t4282066565  ___offset1, float ___radius2, Vector2_t4282066565  ___sideChooser3, float ___inverseDt4, float ___weightFactor5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.RVO.Sampled.Agent/VO::Left(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool VO_Left_m4147358362 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___a0, Vector2_t4282066565  ___dir1, Vector2_t4282066565  ___p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RVO.Sampled.Agent/VO::Det(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float VO_Det_m178725286 (Il2CppObject * __this /* static, unused */, Vector2_t4282066565  ___a0, Vector2_t4282066565  ___dir1, Vector2_t4282066565  ___p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Pathfinding.RVO.Sampled.Agent/VO::Sample(UnityEngine.Vector2,System.Single&)
extern "C"  Vector2_t4282066565  VO_Sample_m360781791 (VO_t2757914308 * __this, Vector2_t4282066565  ___p0, float* ___weight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.RVO.Sampled.Agent/VO::ScalarSample(UnityEngine.Vector2)
extern "C"  float VO_ScalarSample_m1576405323 (VO_t2757914308 * __this, Vector2_t4282066565  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct VO_t2757914308;
struct VO_t2757914308_marshaled_pinvoke;

extern "C" void VO_t2757914308_marshal_pinvoke(const VO_t2757914308& unmarshaled, VO_t2757914308_marshaled_pinvoke& marshaled);
extern "C" void VO_t2757914308_marshal_pinvoke_back(const VO_t2757914308_marshaled_pinvoke& marshaled, VO_t2757914308& unmarshaled);
extern "C" void VO_t2757914308_marshal_pinvoke_cleanup(VO_t2757914308_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct VO_t2757914308;
struct VO_t2757914308_marshaled_com;

extern "C" void VO_t2757914308_marshal_com(const VO_t2757914308& unmarshaled, VO_t2757914308_marshaled_com& marshaled);
extern "C" void VO_t2757914308_marshal_com_back(const VO_t2757914308_marshaled_com& marshaled, VO_t2757914308& unmarshaled);
extern "C" void VO_t2757914308_marshal_com_cleanup(VO_t2757914308_marshaled_com& marshaled);
