﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_nirfoufeNoupir225
struct M_nirfoufeNoupir225_t2250845276;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_nirfoufeNoupir2252250845276.h"

// System.Void GarbageiOS.M_nirfoufeNoupir225::.ctor()
extern "C"  void M_nirfoufeNoupir225__ctor_m3058730119 (M_nirfoufeNoupir225_t2250845276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nirfoufeNoupir225::M_joqainall0(System.String[],System.Int32)
extern "C"  void M_nirfoufeNoupir225_M_joqainall0_m2563253007 (M_nirfoufeNoupir225_t2250845276 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nirfoufeNoupir225::M_mace1(System.String[],System.Int32)
extern "C"  void M_nirfoufeNoupir225_M_mace1_m1530025029 (M_nirfoufeNoupir225_t2250845276 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nirfoufeNoupir225::M_powrearyouSouwemu2(System.String[],System.Int32)
extern "C"  void M_nirfoufeNoupir225_M_powrearyouSouwemu2_m877389932 (M_nirfoufeNoupir225_t2250845276 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nirfoufeNoupir225::M_reseNesa3(System.String[],System.Int32)
extern "C"  void M_nirfoufeNoupir225_M_reseNesa3_m3664112275 (M_nirfoufeNoupir225_t2250845276 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_nirfoufeNoupir225::ilo_M_mace11(GarbageiOS.M_nirfoufeNoupir225,System.String[],System.Int32)
extern "C"  void M_nirfoufeNoupir225_ilo_M_mace11_m1950393241 (Il2CppObject * __this /* static, unused */, M_nirfoufeNoupir225_t2250845276 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
