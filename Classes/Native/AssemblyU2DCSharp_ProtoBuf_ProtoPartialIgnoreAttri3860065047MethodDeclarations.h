﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.ProtoPartialIgnoreAttribute
struct ProtoPartialIgnoreAttribute_t3860065047;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ProtoBuf.ProtoPartialIgnoreAttribute::.ctor(System.String)
extern "C"  void ProtoPartialIgnoreAttribute__ctor_m3062514421 (ProtoPartialIgnoreAttribute_t3860065047 * __this, String_t* ___memberName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.ProtoPartialIgnoreAttribute::get_MemberName()
extern "C"  String_t* ProtoPartialIgnoreAttribute_get_MemberName_m322736680 (ProtoPartialIgnoreAttribute_t3860065047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
