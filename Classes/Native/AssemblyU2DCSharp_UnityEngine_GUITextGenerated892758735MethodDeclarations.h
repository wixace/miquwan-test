﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUITextGenerated
struct UnityEngine_GUITextGenerated_t892758735;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_GUITextGenerated::.ctor()
extern "C"  void UnityEngine_GUITextGenerated__ctor_m1264849068 (UnityEngine_GUITextGenerated_t892758735 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUITextGenerated::GUIText_GUIText1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUITextGenerated_GUIText_GUIText1_m25700732 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextGenerated::GUIText_text(JSVCall)
extern "C"  void UnityEngine_GUITextGenerated_GUIText_text_m4245675275 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextGenerated::GUIText_material(JSVCall)
extern "C"  void UnityEngine_GUITextGenerated_GUIText_material_m1718795601 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextGenerated::GUIText_pixelOffset(JSVCall)
extern "C"  void UnityEngine_GUITextGenerated_GUIText_pixelOffset_m983661723 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextGenerated::GUIText_font(JSVCall)
extern "C"  void UnityEngine_GUITextGenerated_GUIText_font_m1843854377 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextGenerated::GUIText_alignment(JSVCall)
extern "C"  void UnityEngine_GUITextGenerated_GUIText_alignment_m3553362993 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextGenerated::GUIText_anchor(JSVCall)
extern "C"  void UnityEngine_GUITextGenerated_GUIText_anchor_m992992259 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextGenerated::GUIText_lineSpacing(JSVCall)
extern "C"  void UnityEngine_GUITextGenerated_GUIText_lineSpacing_m3471992421 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextGenerated::GUIText_tabSize(JSVCall)
extern "C"  void UnityEngine_GUITextGenerated_GUIText_tabSize_m3160761790 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextGenerated::GUIText_fontSize(JSVCall)
extern "C"  void UnityEngine_GUITextGenerated_GUIText_fontSize_m2736239528 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextGenerated::GUIText_fontStyle(JSVCall)
extern "C"  void UnityEngine_GUITextGenerated_GUIText_fontStyle_m1738248786 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextGenerated::GUIText_richText(JSVCall)
extern "C"  void UnityEngine_GUITextGenerated_GUIText_richText_m2769326223 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextGenerated::GUIText_color(JSVCall)
extern "C"  void UnityEngine_GUITextGenerated_GUIText_color_m1993043377 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextGenerated::__Register()
extern "C"  void UnityEngine_GUITextGenerated___Register_m197719963 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextGenerated::ilo_setStringS1(System.Int32,System.String)
extern "C"  void UnityEngine_GUITextGenerated_ilo_setStringS1_m2035711349 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_GUITextGenerated::ilo_getVector2S2(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_GUITextGenerated_ilo_getVector2S2_m4166978095 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUITextGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_GUITextGenerated_ilo_setObject3_m3583466720 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_GUITextGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_GUITextGenerated_ilo_getObject4_m3638343750 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextGenerated::ilo_setEnum5(System.Int32,System.Int32)
extern "C"  void UnityEngine_GUITextGenerated_ilo_setEnum5_m1371573193 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUITextGenerated::ilo_getEnum6(System.Int32)
extern "C"  int32_t UnityEngine_GUITextGenerated_ilo_getEnum6_m1914927785 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextGenerated::ilo_setSingle7(System.Int32,System.Single)
extern "C"  void UnityEngine_GUITextGenerated_ilo_setSingle7_m547504974 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_GUITextGenerated::ilo_getSingle8(System.Int32)
extern "C"  float UnityEngine_GUITextGenerated_ilo_getSingle8_m1240641986 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUITextGenerated::ilo_setInt329(System.Int32,System.Int32)
extern "C"  void UnityEngine_GUITextGenerated_ilo_setInt329_m2163049378 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
