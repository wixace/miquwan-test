﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_t1659122786;
// UILabel
struct UILabel_t291504320;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlotFightTalk
struct  PlotFightTalk_t789215451  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 PlotFightTalk::ID
	int32_t ___ID_4;
	// UnityEngine.Vector3 PlotFightTalk::offset
	Vector3_t4282066566  ___offset_5;
	// UnityEngine.Transform PlotFightTalk::_targe
	Transform_t1659122786 * ____targe_6;
	// UILabel PlotFightTalk::_label_file
	UILabel_t291504320 * ____label_file_7;
	// System.Single PlotFightTalk::_birst_time
	float ____birst_time_8;
	// System.Boolean PlotFightTalk::_isFirst
	bool ____isFirst_9;
	// System.Single PlotFightTalk::_leftTime
	float ____leftTime_10;

public:
	inline static int32_t get_offset_of_ID_4() { return static_cast<int32_t>(offsetof(PlotFightTalk_t789215451, ___ID_4)); }
	inline int32_t get_ID_4() const { return ___ID_4; }
	inline int32_t* get_address_of_ID_4() { return &___ID_4; }
	inline void set_ID_4(int32_t value)
	{
		___ID_4 = value;
	}

	inline static int32_t get_offset_of_offset_5() { return static_cast<int32_t>(offsetof(PlotFightTalk_t789215451, ___offset_5)); }
	inline Vector3_t4282066566  get_offset_5() const { return ___offset_5; }
	inline Vector3_t4282066566 * get_address_of_offset_5() { return &___offset_5; }
	inline void set_offset_5(Vector3_t4282066566  value)
	{
		___offset_5 = value;
	}

	inline static int32_t get_offset_of__targe_6() { return static_cast<int32_t>(offsetof(PlotFightTalk_t789215451, ____targe_6)); }
	inline Transform_t1659122786 * get__targe_6() const { return ____targe_6; }
	inline Transform_t1659122786 ** get_address_of__targe_6() { return &____targe_6; }
	inline void set__targe_6(Transform_t1659122786 * value)
	{
		____targe_6 = value;
		Il2CppCodeGenWriteBarrier(&____targe_6, value);
	}

	inline static int32_t get_offset_of__label_file_7() { return static_cast<int32_t>(offsetof(PlotFightTalk_t789215451, ____label_file_7)); }
	inline UILabel_t291504320 * get__label_file_7() const { return ____label_file_7; }
	inline UILabel_t291504320 ** get_address_of__label_file_7() { return &____label_file_7; }
	inline void set__label_file_7(UILabel_t291504320 * value)
	{
		____label_file_7 = value;
		Il2CppCodeGenWriteBarrier(&____label_file_7, value);
	}

	inline static int32_t get_offset_of__birst_time_8() { return static_cast<int32_t>(offsetof(PlotFightTalk_t789215451, ____birst_time_8)); }
	inline float get__birst_time_8() const { return ____birst_time_8; }
	inline float* get_address_of__birst_time_8() { return &____birst_time_8; }
	inline void set__birst_time_8(float value)
	{
		____birst_time_8 = value;
	}

	inline static int32_t get_offset_of__isFirst_9() { return static_cast<int32_t>(offsetof(PlotFightTalk_t789215451, ____isFirst_9)); }
	inline bool get__isFirst_9() const { return ____isFirst_9; }
	inline bool* get_address_of__isFirst_9() { return &____isFirst_9; }
	inline void set__isFirst_9(bool value)
	{
		____isFirst_9 = value;
	}

	inline static int32_t get_offset_of__leftTime_10() { return static_cast<int32_t>(offsetof(PlotFightTalk_t789215451, ____leftTime_10)); }
	inline float get__leftTime_10() const { return ____leftTime_10; }
	inline float* get_address_of__leftTime_10() { return &____leftTime_10; }
	inline void set__leftTime_10(float value)
	{
		____leftTime_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
