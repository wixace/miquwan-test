﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.UInt32[]
struct UInt32U5BU5D_t3230734560;

#include "AssemblyU2DCSharp_SevenZip_Compression_LZ_InWindow3196891427.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SevenZip.Compression.LZ.BinTree
struct  BinTree_t161206647  : public InWindow_t3196891427
{
public:
	// System.UInt32 SevenZip.Compression.LZ.BinTree::_cyclicBufferPos
	uint32_t ____cyclicBufferPos_18;
	// System.UInt32 SevenZip.Compression.LZ.BinTree::_cyclicBufferSize
	uint32_t ____cyclicBufferSize_19;
	// System.UInt32 SevenZip.Compression.LZ.BinTree::_matchMaxLen
	uint32_t ____matchMaxLen_20;
	// System.UInt32[] SevenZip.Compression.LZ.BinTree::_son
	UInt32U5BU5D_t3230734560* ____son_21;
	// System.UInt32[] SevenZip.Compression.LZ.BinTree::_hash
	UInt32U5BU5D_t3230734560* ____hash_22;
	// System.UInt32 SevenZip.Compression.LZ.BinTree::_cutValue
	uint32_t ____cutValue_23;
	// System.UInt32 SevenZip.Compression.LZ.BinTree::_hashMask
	uint32_t ____hashMask_24;
	// System.UInt32 SevenZip.Compression.LZ.BinTree::_hashSizeSum
	uint32_t ____hashSizeSum_25;
	// System.Boolean SevenZip.Compression.LZ.BinTree::HASH_ARRAY
	bool ___HASH_ARRAY_26;
	// System.UInt32 SevenZip.Compression.LZ.BinTree::kNumHashDirectBytes
	uint32_t ___kNumHashDirectBytes_27;
	// System.UInt32 SevenZip.Compression.LZ.BinTree::kMinMatchCheck
	uint32_t ___kMinMatchCheck_28;
	// System.UInt32 SevenZip.Compression.LZ.BinTree::kFixHashSize
	uint32_t ___kFixHashSize_29;

public:
	inline static int32_t get_offset_of__cyclicBufferPos_18() { return static_cast<int32_t>(offsetof(BinTree_t161206647, ____cyclicBufferPos_18)); }
	inline uint32_t get__cyclicBufferPos_18() const { return ____cyclicBufferPos_18; }
	inline uint32_t* get_address_of__cyclicBufferPos_18() { return &____cyclicBufferPos_18; }
	inline void set__cyclicBufferPos_18(uint32_t value)
	{
		____cyclicBufferPos_18 = value;
	}

	inline static int32_t get_offset_of__cyclicBufferSize_19() { return static_cast<int32_t>(offsetof(BinTree_t161206647, ____cyclicBufferSize_19)); }
	inline uint32_t get__cyclicBufferSize_19() const { return ____cyclicBufferSize_19; }
	inline uint32_t* get_address_of__cyclicBufferSize_19() { return &____cyclicBufferSize_19; }
	inline void set__cyclicBufferSize_19(uint32_t value)
	{
		____cyclicBufferSize_19 = value;
	}

	inline static int32_t get_offset_of__matchMaxLen_20() { return static_cast<int32_t>(offsetof(BinTree_t161206647, ____matchMaxLen_20)); }
	inline uint32_t get__matchMaxLen_20() const { return ____matchMaxLen_20; }
	inline uint32_t* get_address_of__matchMaxLen_20() { return &____matchMaxLen_20; }
	inline void set__matchMaxLen_20(uint32_t value)
	{
		____matchMaxLen_20 = value;
	}

	inline static int32_t get_offset_of__son_21() { return static_cast<int32_t>(offsetof(BinTree_t161206647, ____son_21)); }
	inline UInt32U5BU5D_t3230734560* get__son_21() const { return ____son_21; }
	inline UInt32U5BU5D_t3230734560** get_address_of__son_21() { return &____son_21; }
	inline void set__son_21(UInt32U5BU5D_t3230734560* value)
	{
		____son_21 = value;
		Il2CppCodeGenWriteBarrier(&____son_21, value);
	}

	inline static int32_t get_offset_of__hash_22() { return static_cast<int32_t>(offsetof(BinTree_t161206647, ____hash_22)); }
	inline UInt32U5BU5D_t3230734560* get__hash_22() const { return ____hash_22; }
	inline UInt32U5BU5D_t3230734560** get_address_of__hash_22() { return &____hash_22; }
	inline void set__hash_22(UInt32U5BU5D_t3230734560* value)
	{
		____hash_22 = value;
		Il2CppCodeGenWriteBarrier(&____hash_22, value);
	}

	inline static int32_t get_offset_of__cutValue_23() { return static_cast<int32_t>(offsetof(BinTree_t161206647, ____cutValue_23)); }
	inline uint32_t get__cutValue_23() const { return ____cutValue_23; }
	inline uint32_t* get_address_of__cutValue_23() { return &____cutValue_23; }
	inline void set__cutValue_23(uint32_t value)
	{
		____cutValue_23 = value;
	}

	inline static int32_t get_offset_of__hashMask_24() { return static_cast<int32_t>(offsetof(BinTree_t161206647, ____hashMask_24)); }
	inline uint32_t get__hashMask_24() const { return ____hashMask_24; }
	inline uint32_t* get_address_of__hashMask_24() { return &____hashMask_24; }
	inline void set__hashMask_24(uint32_t value)
	{
		____hashMask_24 = value;
	}

	inline static int32_t get_offset_of__hashSizeSum_25() { return static_cast<int32_t>(offsetof(BinTree_t161206647, ____hashSizeSum_25)); }
	inline uint32_t get__hashSizeSum_25() const { return ____hashSizeSum_25; }
	inline uint32_t* get_address_of__hashSizeSum_25() { return &____hashSizeSum_25; }
	inline void set__hashSizeSum_25(uint32_t value)
	{
		____hashSizeSum_25 = value;
	}

	inline static int32_t get_offset_of_HASH_ARRAY_26() { return static_cast<int32_t>(offsetof(BinTree_t161206647, ___HASH_ARRAY_26)); }
	inline bool get_HASH_ARRAY_26() const { return ___HASH_ARRAY_26; }
	inline bool* get_address_of_HASH_ARRAY_26() { return &___HASH_ARRAY_26; }
	inline void set_HASH_ARRAY_26(bool value)
	{
		___HASH_ARRAY_26 = value;
	}

	inline static int32_t get_offset_of_kNumHashDirectBytes_27() { return static_cast<int32_t>(offsetof(BinTree_t161206647, ___kNumHashDirectBytes_27)); }
	inline uint32_t get_kNumHashDirectBytes_27() const { return ___kNumHashDirectBytes_27; }
	inline uint32_t* get_address_of_kNumHashDirectBytes_27() { return &___kNumHashDirectBytes_27; }
	inline void set_kNumHashDirectBytes_27(uint32_t value)
	{
		___kNumHashDirectBytes_27 = value;
	}

	inline static int32_t get_offset_of_kMinMatchCheck_28() { return static_cast<int32_t>(offsetof(BinTree_t161206647, ___kMinMatchCheck_28)); }
	inline uint32_t get_kMinMatchCheck_28() const { return ___kMinMatchCheck_28; }
	inline uint32_t* get_address_of_kMinMatchCheck_28() { return &___kMinMatchCheck_28; }
	inline void set_kMinMatchCheck_28(uint32_t value)
	{
		___kMinMatchCheck_28 = value;
	}

	inline static int32_t get_offset_of_kFixHashSize_29() { return static_cast<int32_t>(offsetof(BinTree_t161206647, ___kFixHashSize_29)); }
	inline uint32_t get_kFixHashSize_29() const { return ___kFixHashSize_29; }
	inline uint32_t* get_address_of_kFixHashSize_29() { return &___kFixHashSize_29; }
	inline void set_kFixHashSize_29(uint32_t value)
	{
		___kFixHashSize_29 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
