﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_roujasdu241
struct  M_roujasdu241_t1680661114  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_roujasdu241::_sasrawrooYorrall
	float ____sasrawrooYorrall_0;
	// System.Int32 GarbageiOS.M_roujasdu241::_towrelsu
	int32_t ____towrelsu_1;
	// System.Boolean GarbageiOS.M_roujasdu241::_drijistou
	bool ____drijistou_2;
	// System.Boolean GarbageiOS.M_roujasdu241::_fircowSawpe
	bool ____fircowSawpe_3;
	// System.Int32 GarbageiOS.M_roujasdu241::_kerheevaw
	int32_t ____kerheevaw_4;
	// System.UInt32 GarbageiOS.M_roujasdu241::_suwe
	uint32_t ____suwe_5;
	// System.UInt32 GarbageiOS.M_roujasdu241::_rallgis
	uint32_t ____rallgis_6;
	// System.Int32 GarbageiOS.M_roujasdu241::_masairdai
	int32_t ____masairdai_7;
	// System.Boolean GarbageiOS.M_roujasdu241::_delterdirMosawpere
	bool ____delterdirMosawpere_8;
	// System.Boolean GarbageiOS.M_roujasdu241::_nurmorsas
	bool ____nurmorsas_9;
	// System.Boolean GarbageiOS.M_roujasdu241::_kirbowJeqa
	bool ____kirbowJeqa_10;
	// System.Boolean GarbageiOS.M_roujasdu241::_meca
	bool ____meca_11;

public:
	inline static int32_t get_offset_of__sasrawrooYorrall_0() { return static_cast<int32_t>(offsetof(M_roujasdu241_t1680661114, ____sasrawrooYorrall_0)); }
	inline float get__sasrawrooYorrall_0() const { return ____sasrawrooYorrall_0; }
	inline float* get_address_of__sasrawrooYorrall_0() { return &____sasrawrooYorrall_0; }
	inline void set__sasrawrooYorrall_0(float value)
	{
		____sasrawrooYorrall_0 = value;
	}

	inline static int32_t get_offset_of__towrelsu_1() { return static_cast<int32_t>(offsetof(M_roujasdu241_t1680661114, ____towrelsu_1)); }
	inline int32_t get__towrelsu_1() const { return ____towrelsu_1; }
	inline int32_t* get_address_of__towrelsu_1() { return &____towrelsu_1; }
	inline void set__towrelsu_1(int32_t value)
	{
		____towrelsu_1 = value;
	}

	inline static int32_t get_offset_of__drijistou_2() { return static_cast<int32_t>(offsetof(M_roujasdu241_t1680661114, ____drijistou_2)); }
	inline bool get__drijistou_2() const { return ____drijistou_2; }
	inline bool* get_address_of__drijistou_2() { return &____drijistou_2; }
	inline void set__drijistou_2(bool value)
	{
		____drijistou_2 = value;
	}

	inline static int32_t get_offset_of__fircowSawpe_3() { return static_cast<int32_t>(offsetof(M_roujasdu241_t1680661114, ____fircowSawpe_3)); }
	inline bool get__fircowSawpe_3() const { return ____fircowSawpe_3; }
	inline bool* get_address_of__fircowSawpe_3() { return &____fircowSawpe_3; }
	inline void set__fircowSawpe_3(bool value)
	{
		____fircowSawpe_3 = value;
	}

	inline static int32_t get_offset_of__kerheevaw_4() { return static_cast<int32_t>(offsetof(M_roujasdu241_t1680661114, ____kerheevaw_4)); }
	inline int32_t get__kerheevaw_4() const { return ____kerheevaw_4; }
	inline int32_t* get_address_of__kerheevaw_4() { return &____kerheevaw_4; }
	inline void set__kerheevaw_4(int32_t value)
	{
		____kerheevaw_4 = value;
	}

	inline static int32_t get_offset_of__suwe_5() { return static_cast<int32_t>(offsetof(M_roujasdu241_t1680661114, ____suwe_5)); }
	inline uint32_t get__suwe_5() const { return ____suwe_5; }
	inline uint32_t* get_address_of__suwe_5() { return &____suwe_5; }
	inline void set__suwe_5(uint32_t value)
	{
		____suwe_5 = value;
	}

	inline static int32_t get_offset_of__rallgis_6() { return static_cast<int32_t>(offsetof(M_roujasdu241_t1680661114, ____rallgis_6)); }
	inline uint32_t get__rallgis_6() const { return ____rallgis_6; }
	inline uint32_t* get_address_of__rallgis_6() { return &____rallgis_6; }
	inline void set__rallgis_6(uint32_t value)
	{
		____rallgis_6 = value;
	}

	inline static int32_t get_offset_of__masairdai_7() { return static_cast<int32_t>(offsetof(M_roujasdu241_t1680661114, ____masairdai_7)); }
	inline int32_t get__masairdai_7() const { return ____masairdai_7; }
	inline int32_t* get_address_of__masairdai_7() { return &____masairdai_7; }
	inline void set__masairdai_7(int32_t value)
	{
		____masairdai_7 = value;
	}

	inline static int32_t get_offset_of__delterdirMosawpere_8() { return static_cast<int32_t>(offsetof(M_roujasdu241_t1680661114, ____delterdirMosawpere_8)); }
	inline bool get__delterdirMosawpere_8() const { return ____delterdirMosawpere_8; }
	inline bool* get_address_of__delterdirMosawpere_8() { return &____delterdirMosawpere_8; }
	inline void set__delterdirMosawpere_8(bool value)
	{
		____delterdirMosawpere_8 = value;
	}

	inline static int32_t get_offset_of__nurmorsas_9() { return static_cast<int32_t>(offsetof(M_roujasdu241_t1680661114, ____nurmorsas_9)); }
	inline bool get__nurmorsas_9() const { return ____nurmorsas_9; }
	inline bool* get_address_of__nurmorsas_9() { return &____nurmorsas_9; }
	inline void set__nurmorsas_9(bool value)
	{
		____nurmorsas_9 = value;
	}

	inline static int32_t get_offset_of__kirbowJeqa_10() { return static_cast<int32_t>(offsetof(M_roujasdu241_t1680661114, ____kirbowJeqa_10)); }
	inline bool get__kirbowJeqa_10() const { return ____kirbowJeqa_10; }
	inline bool* get_address_of__kirbowJeqa_10() { return &____kirbowJeqa_10; }
	inline void set__kirbowJeqa_10(bool value)
	{
		____kirbowJeqa_10 = value;
	}

	inline static int32_t get_offset_of__meca_11() { return static_cast<int32_t>(offsetof(M_roujasdu241_t1680661114, ____meca_11)); }
	inline bool get__meca_11() const { return ____meca_11; }
	inline bool* get_address_of__meca_11() { return &____meca_11; }
	inline void set__meca_11(bool value)
	{
		____meca_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
