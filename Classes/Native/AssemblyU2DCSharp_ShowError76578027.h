﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ShowError
struct ShowError_t76578027;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// UnityEngine.GUIStyle
struct GUIStyle_t2990928826;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShowError
struct  ShowError_t76578027  : public MonoBehaviour_t667441552
{
public:
	// System.Collections.Generic.List`1<System.String> ShowError::mLines
	List_1_t1375417109 * ___mLines_4;
	// UnityEngine.GUIStyle ShowError::style
	GUIStyle_t2990928826 * ___style_5;

public:
	inline static int32_t get_offset_of_mLines_4() { return static_cast<int32_t>(offsetof(ShowError_t76578027, ___mLines_4)); }
	inline List_1_t1375417109 * get_mLines_4() const { return ___mLines_4; }
	inline List_1_t1375417109 ** get_address_of_mLines_4() { return &___mLines_4; }
	inline void set_mLines_4(List_1_t1375417109 * value)
	{
		___mLines_4 = value;
		Il2CppCodeGenWriteBarrier(&___mLines_4, value);
	}

	inline static int32_t get_offset_of_style_5() { return static_cast<int32_t>(offsetof(ShowError_t76578027, ___style_5)); }
	inline GUIStyle_t2990928826 * get_style_5() const { return ___style_5; }
	inline GUIStyle_t2990928826 ** get_address_of_style_5() { return &___style_5; }
	inline void set_style_5(GUIStyle_t2990928826 * value)
	{
		___style_5 = value;
		Il2CppCodeGenWriteBarrier(&___style_5, value);
	}
};

struct ShowError_t76578027_StaticFields
{
public:
	// System.Boolean ShowError::_isShow
	bool ____isShow_2;
	// ShowError ShowError::_showError
	ShowError_t76578027 * ____showError_3;

public:
	inline static int32_t get_offset_of__isShow_2() { return static_cast<int32_t>(offsetof(ShowError_t76578027_StaticFields, ____isShow_2)); }
	inline bool get__isShow_2() const { return ____isShow_2; }
	inline bool* get_address_of__isShow_2() { return &____isShow_2; }
	inline void set__isShow_2(bool value)
	{
		____isShow_2 = value;
	}

	inline static int32_t get_offset_of__showError_3() { return static_cast<int32_t>(offsetof(ShowError_t76578027_StaticFields, ____showError_3)); }
	inline ShowError_t76578027 * get__showError_3() const { return ____showError_3; }
	inline ShowError_t76578027 ** get_address_of__showError_3() { return &____showError_3; }
	inline void set__showError_3(ShowError_t76578027 * value)
	{
		____showError_3 = value;
		Il2CppCodeGenWriteBarrier(&____showError_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
