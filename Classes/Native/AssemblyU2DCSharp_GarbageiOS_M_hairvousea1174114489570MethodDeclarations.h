﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_hairvousea117
struct M_hairvousea117_t4114489570;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_hairvousea1174114489570.h"

// System.Void GarbageiOS.M_hairvousea117::.ctor()
extern "C"  void M_hairvousea117__ctor_m1688015553 (M_hairvousea117_t4114489570 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hairvousea117::M_vurca0(System.String[],System.Int32)
extern "C"  void M_hairvousea117_M_vurca0_m982729611 (M_hairvousea117_t4114489570 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hairvousea117::M_traikouhuWhorkas1(System.String[],System.Int32)
extern "C"  void M_hairvousea117_M_traikouhuWhorkas1_m315358148 (M_hairvousea117_t4114489570 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hairvousea117::M_heedoTelwiter2(System.String[],System.Int32)
extern "C"  void M_hairvousea117_M_heedoTelwiter2_m1203172087 (M_hairvousea117_t4114489570 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hairvousea117::M_toorerebiSoosaw3(System.String[],System.Int32)
extern "C"  void M_hairvousea117_M_toorerebiSoosaw3_m2023725608 (M_hairvousea117_t4114489570 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hairvousea117::ilo_M_vurca01(GarbageiOS.M_hairvousea117,System.String[],System.Int32)
extern "C"  void M_hairvousea117_ilo_M_vurca01_m669579947 (Il2CppObject * __this /* static, unused */, M_hairvousea117_t4114489570 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hairvousea117::ilo_M_traikouhuWhorkas12(GarbageiOS.M_hairvousea117,System.String[],System.Int32)
extern "C"  void M_hairvousea117_ilo_M_traikouhuWhorkas12_m4050406333 (Il2CppObject * __this /* static, unused */, M_hairvousea117_t4114489570 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hairvousea117::ilo_M_heedoTelwiter23(GarbageiOS.M_hairvousea117,System.String[],System.Int32)
extern "C"  void M_hairvousea117_ilo_M_heedoTelwiter23_m3124271957 (Il2CppObject * __this /* static, unused */, M_hairvousea117_t4114489570 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
