﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIPanelGenerated/<UIPanel_onClipMove_GetDelegate_member15_arg0>c__AnonStoreyC9
struct U3CUIPanel_onClipMove_GetDelegate_member15_arg0U3Ec__AnonStoreyC9_t680376146;
// UIPanel
struct UIPanel_t295209936;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"

// System.Void UIPanelGenerated/<UIPanel_onClipMove_GetDelegate_member15_arg0>c__AnonStoreyC9::.ctor()
extern "C"  void U3CUIPanel_onClipMove_GetDelegate_member15_arg0U3Ec__AnonStoreyC9__ctor_m2573169033 (U3CUIPanel_onClipMove_GetDelegate_member15_arg0U3Ec__AnonStoreyC9_t680376146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated/<UIPanel_onClipMove_GetDelegate_member15_arg0>c__AnonStoreyC9::<>m__157(UIPanel)
extern "C"  void U3CUIPanel_onClipMove_GetDelegate_member15_arg0U3Ec__AnonStoreyC9_U3CU3Em__157_m3765548383 (U3CUIPanel_onClipMove_GetDelegate_member15_arg0U3Ec__AnonStoreyC9_t680376146 * __this, UIPanel_t295209936 * ___panel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
