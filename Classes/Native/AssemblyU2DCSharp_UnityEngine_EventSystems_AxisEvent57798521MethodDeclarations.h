﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_AxisEventDataGenerated
struct UnityEngine_EventSystems_AxisEventDataGenerated_t57798521;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine_EventSystems_AxisEventDataGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_AxisEventDataGenerated__ctor_m1010012178 (UnityEngine_EventSystems_AxisEventDataGenerated_t57798521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_AxisEventDataGenerated::AxisEventData_AxisEventData1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_AxisEventDataGenerated_AxisEventData_AxisEventData1_m1840320952 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_AxisEventDataGenerated::AxisEventData_moveVector(JSVCall)
extern "C"  void UnityEngine_EventSystems_AxisEventDataGenerated_AxisEventData_moveVector_m317198213 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_AxisEventDataGenerated::AxisEventData_moveDir(JSVCall)
extern "C"  void UnityEngine_EventSystems_AxisEventDataGenerated_AxisEventData_moveDir_m3988278007 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_AxisEventDataGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_AxisEventDataGenerated___Register_m2849974261 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_EventSystems_AxisEventDataGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_EventSystems_AxisEventDataGenerated_ilo_getObject1_m784435556 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_AxisEventDataGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_EventSystems_AxisEventDataGenerated_ilo_attachFinalizerObject2_m292772454 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_AxisEventDataGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UnityEngine_EventSystems_AxisEventDataGenerated_ilo_addJSCSRel3_m3349663440 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_EventSystems_AxisEventDataGenerated::ilo_getVector2S4(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_EventSystems_AxisEventDataGenerated_ilo_getVector2S4_m2371128053 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_AxisEventDataGenerated::ilo_setEnum5(System.Int32,System.Int32)
extern "C"  void UnityEngine_EventSystems_AxisEventDataGenerated_ilo_setEnum5_m1892217647 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_EventSystems_AxisEventDataGenerated::ilo_getEnum6(System.Int32)
extern "C"  int32_t UnityEngine_EventSystems_AxisEventDataGenerated_ilo_getEnum6_m333676199 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
