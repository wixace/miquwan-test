﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Schema.JsonSchemaModelBuilder
struct JsonSchemaModelBuilder_t875629317;
// Newtonsoft.Json.Schema.JsonSchemaModel
struct JsonSchemaModel_t3035618138;
// Newtonsoft.Json.Schema.JsonSchema
struct JsonSchema_t460567603;
// Newtonsoft.Json.Schema.JsonSchemaNode
struct JsonSchemaNode_t2115227093;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchema>
struct IDictionary_2_t858859318;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>
struct IDictionary_2_t2513518808;
// System.String
struct String_t;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Schema.JsonSchema>
struct IEnumerable_1_t3761480560;
// System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>
struct Dictionary_2_t2935645463;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchema>
struct IList_1_t3155214806;
// System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaNode>
struct List_1_t3483412645;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchema>
struct ReadOnlyCollection_1_t2017645139;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaModel>
struct IDictionary_2_t3433909853;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchemaModel>
struct IList_1_t1435298045;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema460567603.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem2115227093.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema875629317.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchem3035618138.h"

// System.Void Newtonsoft.Json.Schema.JsonSchemaModelBuilder::.ctor()
extern "C"  void JsonSchemaModelBuilder__ctor_m797138360 (JsonSchemaModelBuilder_t875629317 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaModel Newtonsoft.Json.Schema.JsonSchemaModelBuilder::Build(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  JsonSchemaModel_t3035618138 * JsonSchemaModelBuilder_Build_m160806552 (JsonSchemaModelBuilder_t875629317 * __this, JsonSchema_t460567603 * ___schema0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaNode Newtonsoft.Json.Schema.JsonSchemaModelBuilder::AddSchema(Newtonsoft.Json.Schema.JsonSchemaNode,Newtonsoft.Json.Schema.JsonSchema)
extern "C"  JsonSchemaNode_t2115227093 * JsonSchemaModelBuilder_AddSchema_m1464289778 (JsonSchemaModelBuilder_t875629317 * __this, JsonSchemaNode_t2115227093 * ___existingNode0, JsonSchema_t460567603 * ___schema1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModelBuilder::AddProperties(System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchema>,System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>)
extern "C"  void JsonSchemaModelBuilder_AddProperties_m3507987280 (JsonSchemaModelBuilder_t875629317 * __this, Il2CppObject* ___source0, Il2CppObject* ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModelBuilder::AddProperty(System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode>,System.String,Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void JsonSchemaModelBuilder_AddProperty_m3887368252 (JsonSchemaModelBuilder_t875629317 * __this, Il2CppObject* ___target0, String_t* ___propertyName1, JsonSchema_t460567603 * ___schema2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModelBuilder::AddItem(Newtonsoft.Json.Schema.JsonSchemaNode,System.Int32,Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void JsonSchemaModelBuilder_AddItem_m3196034109 (JsonSchemaModelBuilder_t875629317 * __this, JsonSchemaNode_t2115227093 * ___parentNode0, int32_t ___index1, JsonSchema_t460567603 * ___schema2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModelBuilder::AddAdditionalProperties(Newtonsoft.Json.Schema.JsonSchemaNode,Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void JsonSchemaModelBuilder_AddAdditionalProperties_m907721673 (JsonSchemaModelBuilder_t875629317 * __this, JsonSchemaNode_t2115227093 * ___parentNode0, JsonSchema_t460567603 * ___schema1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaModel Newtonsoft.Json.Schema.JsonSchemaModelBuilder::BuildNodeModel(Newtonsoft.Json.Schema.JsonSchemaNode)
extern "C"  JsonSchemaModel_t3035618138 * JsonSchemaModelBuilder_BuildNodeModel_m405771615 (JsonSchemaModelBuilder_t875629317 * __this, JsonSchemaNode_t2115227093 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaModel Newtonsoft.Json.Schema.JsonSchemaModelBuilder::ilo_BuildNodeModel1(Newtonsoft.Json.Schema.JsonSchemaModelBuilder,Newtonsoft.Json.Schema.JsonSchemaNode)
extern "C"  JsonSchemaModel_t3035618138 * JsonSchemaModelBuilder_ilo_BuildNodeModel1_m2808712952 (Il2CppObject * __this /* static, unused */, JsonSchemaModelBuilder_t875629317 * ____this0, JsonSchemaNode_t2115227093 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Schema.JsonSchemaModelBuilder::ilo_GetId2(System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Schema.JsonSchema>)
extern "C"  String_t* JsonSchemaModelBuilder_ilo_GetId2_m49586319 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___schemata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaNode Newtonsoft.Json.Schema.JsonSchemaModelBuilder::ilo_Combine3(Newtonsoft.Json.Schema.JsonSchemaNode,Newtonsoft.Json.Schema.JsonSchema)
extern "C"  JsonSchemaNode_t2115227093 * JsonSchemaModelBuilder_ilo_Combine3_m373973975 (Il2CppObject * __this /* static, unused */, JsonSchemaNode_t2115227093 * ____this0, JsonSchema_t460567603 * ___schema1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchema> Newtonsoft.Json.Schema.JsonSchemaModelBuilder::ilo_get_Properties4(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  Il2CppObject* JsonSchemaModelBuilder_ilo_get_Properties4_m1067315286 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaNode> Newtonsoft.Json.Schema.JsonSchemaModelBuilder::ilo_get_Properties5(Newtonsoft.Json.Schema.JsonSchemaNode)
extern "C"  Dictionary_2_t2935645463 * JsonSchemaModelBuilder_ilo_get_Properties5_m2481692018 (Il2CppObject * __this /* static, unused */, JsonSchemaNode_t2115227093 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchema> Newtonsoft.Json.Schema.JsonSchemaModelBuilder::ilo_get_Items6(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  Il2CppObject* JsonSchemaModelBuilder_ilo_get_Items6_m190701942 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaModelBuilder::ilo_get_AdditionalProperties7(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  JsonSchema_t460567603 * JsonSchemaModelBuilder_ilo_get_AdditionalProperties7_m813774632 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModelBuilder::ilo_AddAdditionalProperties8(Newtonsoft.Json.Schema.JsonSchemaModelBuilder,Newtonsoft.Json.Schema.JsonSchemaNode,Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void JsonSchemaModelBuilder_ilo_AddAdditionalProperties8_m2768973207 (Il2CppObject * __this /* static, unused */, JsonSchemaModelBuilder_t875629317 * ____this0, JsonSchemaNode_t2115227093 * ___parentNode1, JsonSchema_t460567603 * ___schema2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaModelBuilder::ilo_get_Extends9(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  JsonSchema_t460567603 * JsonSchemaModelBuilder_ilo_get_Extends9_m470362235 (Il2CppObject * __this /* static, unused */, JsonSchema_t460567603 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaNode Newtonsoft.Json.Schema.JsonSchemaModelBuilder::ilo_AddSchema10(Newtonsoft.Json.Schema.JsonSchemaModelBuilder,Newtonsoft.Json.Schema.JsonSchemaNode,Newtonsoft.Json.Schema.JsonSchema)
extern "C"  JsonSchemaNode_t2115227093 * JsonSchemaModelBuilder_ilo_AddSchema10_m1267439967 (Il2CppObject * __this /* static, unused */, JsonSchemaModelBuilder_t875629317 * ____this0, JsonSchemaNode_t2115227093 * ___existingNode1, JsonSchema_t460567603 * ___schema2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Newtonsoft.Json.Schema.JsonSchemaNode> Newtonsoft.Json.Schema.JsonSchemaModelBuilder::ilo_get_Items11(Newtonsoft.Json.Schema.JsonSchemaNode)
extern "C"  List_1_t3483412645 * JsonSchemaModelBuilder_ilo_get_Items11_m2288636177 (Il2CppObject * __this /* static, unused */, JsonSchemaNode_t2115227093 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaNode Newtonsoft.Json.Schema.JsonSchemaModelBuilder::ilo_get_AdditionalProperties12(Newtonsoft.Json.Schema.JsonSchemaNode)
extern "C"  JsonSchemaNode_t2115227093 * JsonSchemaModelBuilder_ilo_get_AdditionalProperties12_m1223294588 (Il2CppObject * __this /* static, unused */, JsonSchemaNode_t2115227093 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModelBuilder::ilo_set_AdditionalProperties13(Newtonsoft.Json.Schema.JsonSchemaNode,Newtonsoft.Json.Schema.JsonSchemaNode)
extern "C"  void JsonSchemaModelBuilder_ilo_set_AdditionalProperties13_m1405879360 (Il2CppObject * __this /* static, unused */, JsonSchemaNode_t2115227093 * ____this0, JsonSchemaNode_t2115227093 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Newtonsoft.Json.Schema.JsonSchema> Newtonsoft.Json.Schema.JsonSchemaModelBuilder::ilo_get_Schemas14(Newtonsoft.Json.Schema.JsonSchemaNode)
extern "C"  ReadOnlyCollection_1_t2017645139 * JsonSchemaModelBuilder_ilo_get_Schemas14_m1403253743 (Il2CppObject * __this /* static, unused */, JsonSchemaNode_t2115227093 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchemaModel Newtonsoft.Json.Schema.JsonSchemaModelBuilder::ilo_Create15(System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchema>)
extern "C"  JsonSchemaModel_t3035618138 * JsonSchemaModelBuilder_ilo_Create15_m1814527320 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___schemata0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Schema.JsonSchemaModel> Newtonsoft.Json.Schema.JsonSchemaModelBuilder::ilo_get_PatternProperties16(Newtonsoft.Json.Schema.JsonSchemaModel)
extern "C"  Il2CppObject* JsonSchemaModelBuilder_ilo_get_PatternProperties16_m180538391 (Il2CppObject * __this /* static, unused */, JsonSchemaModel_t3035618138 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaModelBuilder::ilo_set_Items17(Newtonsoft.Json.Schema.JsonSchemaModel,System.Collections.Generic.IList`1<Newtonsoft.Json.Schema.JsonSchemaModel>)
extern "C"  void JsonSchemaModelBuilder_ilo_set_Items17_m2183002079 (Il2CppObject * __this /* static, unused */, JsonSchemaModel_t3035618138 * ____this0, Il2CppObject* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
