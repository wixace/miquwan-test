﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlyCam
struct  FlyCam_t2107453052  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 FlyCam::currentWaypoint
	int32_t ___currentWaypoint_2;
	// System.Single FlyCam::rotateSpeed
	float ___rotateSpeed_3;
	// System.Single FlyCam::moveSpeed
	float ___moveSpeed_4;
	// System.Single FlyCam::magnitudeMax
	float ___magnitudeMax_5;

public:
	inline static int32_t get_offset_of_currentWaypoint_2() { return static_cast<int32_t>(offsetof(FlyCam_t2107453052, ___currentWaypoint_2)); }
	inline int32_t get_currentWaypoint_2() const { return ___currentWaypoint_2; }
	inline int32_t* get_address_of_currentWaypoint_2() { return &___currentWaypoint_2; }
	inline void set_currentWaypoint_2(int32_t value)
	{
		___currentWaypoint_2 = value;
	}

	inline static int32_t get_offset_of_rotateSpeed_3() { return static_cast<int32_t>(offsetof(FlyCam_t2107453052, ___rotateSpeed_3)); }
	inline float get_rotateSpeed_3() const { return ___rotateSpeed_3; }
	inline float* get_address_of_rotateSpeed_3() { return &___rotateSpeed_3; }
	inline void set_rotateSpeed_3(float value)
	{
		___rotateSpeed_3 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_4() { return static_cast<int32_t>(offsetof(FlyCam_t2107453052, ___moveSpeed_4)); }
	inline float get_moveSpeed_4() const { return ___moveSpeed_4; }
	inline float* get_address_of_moveSpeed_4() { return &___moveSpeed_4; }
	inline void set_moveSpeed_4(float value)
	{
		___moveSpeed_4 = value;
	}

	inline static int32_t get_offset_of_magnitudeMax_5() { return static_cast<int32_t>(offsetof(FlyCam_t2107453052, ___magnitudeMax_5)); }
	inline float get_magnitudeMax_5() const { return ___magnitudeMax_5; }
	inline float* get_address_of_magnitudeMax_5() { return &___magnitudeMax_5; }
	inline void set_magnitudeMax_5(float value)
	{
		___magnitudeMax_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
