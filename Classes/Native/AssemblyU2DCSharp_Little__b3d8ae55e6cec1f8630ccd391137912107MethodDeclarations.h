﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._b3d8ae55e6cec1f8630ccd39449765bd
struct _b3d8ae55e6cec1f8630ccd39449765bd_t1137912107;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__b3d8ae55e6cec1f8630ccd391137912107.h"

// System.Void Little._b3d8ae55e6cec1f8630ccd39449765bd::.ctor()
extern "C"  void _b3d8ae55e6cec1f8630ccd39449765bd__ctor_m3770948962 (_b3d8ae55e6cec1f8630ccd39449765bd_t1137912107 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b3d8ae55e6cec1f8630ccd39449765bd::_b3d8ae55e6cec1f8630ccd39449765bdm2(System.Int32)
extern "C"  int32_t _b3d8ae55e6cec1f8630ccd39449765bd__b3d8ae55e6cec1f8630ccd39449765bdm2_m617654713 (_b3d8ae55e6cec1f8630ccd39449765bd_t1137912107 * __this, int32_t ____b3d8ae55e6cec1f8630ccd39449765bda0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b3d8ae55e6cec1f8630ccd39449765bd::_b3d8ae55e6cec1f8630ccd39449765bdm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _b3d8ae55e6cec1f8630ccd39449765bd__b3d8ae55e6cec1f8630ccd39449765bdm_m4145635613 (_b3d8ae55e6cec1f8630ccd39449765bd_t1137912107 * __this, int32_t ____b3d8ae55e6cec1f8630ccd39449765bda0, int32_t ____b3d8ae55e6cec1f8630ccd39449765bd861, int32_t ____b3d8ae55e6cec1f8630ccd39449765bdc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b3d8ae55e6cec1f8630ccd39449765bd::ilo__b3d8ae55e6cec1f8630ccd39449765bdm21(Little._b3d8ae55e6cec1f8630ccd39449765bd,System.Int32)
extern "C"  int32_t _b3d8ae55e6cec1f8630ccd39449765bd_ilo__b3d8ae55e6cec1f8630ccd39449765bdm21_m2045900114 (Il2CppObject * __this /* static, unused */, _b3d8ae55e6cec1f8630ccd39449765bd_t1137912107 * ____this0, int32_t ____b3d8ae55e6cec1f8630ccd39449765bda1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
