﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameObjectVisitor/<FindComponentInChildren>c__AnonStorey165`1<System.Object>
struct  U3CFindComponentInChildrenU3Ec__AnonStorey165_1_t2237766626  : public Il2CppObject
{
public:
	// T GameObjectVisitor/<FindComponentInChildren>c__AnonStorey165`1::ret
	Il2CppObject * ___ret_0;

public:
	inline static int32_t get_offset_of_ret_0() { return static_cast<int32_t>(offsetof(U3CFindComponentInChildrenU3Ec__AnonStorey165_1_t2237766626, ___ret_0)); }
	inline Il2CppObject * get_ret_0() const { return ___ret_0; }
	inline Il2CppObject ** get_address_of_ret_0() { return &___ret_0; }
	inline void set_ret_0(Il2CppObject * value)
	{
		___ret_0 = value;
		Il2CppCodeGenWriteBarrier(&___ret_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
