﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TBHoverChangeScale
struct TBHoverChangeScale_t1442219820;
// FingerHoverEvent
struct FingerHoverEvent_t1767068455;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FingerHoverEvent1767068455.h"
#include "AssemblyU2DCSharp_FingerHoverPhase1776806408.h"

// System.Void TBHoverChangeScale::.ctor()
extern "C"  void TBHoverChangeScale__ctor_m2385955567 (TBHoverChangeScale_t1442219820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBHoverChangeScale::Start()
extern "C"  void TBHoverChangeScale_Start_m1333093359 (TBHoverChangeScale_t1442219820 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBHoverChangeScale::OnFingerHover(FingerHoverEvent)
extern "C"  void TBHoverChangeScale_OnFingerHover_m780349626 (TBHoverChangeScale_t1442219820 * __this, FingerHoverEvent_t1767068455 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerHoverPhase TBHoverChangeScale::ilo_get_Phase1(FingerHoverEvent)
extern "C"  int32_t TBHoverChangeScale_ilo_get_Phase1_m254120053 (Il2CppObject * __this /* static, unused */, FingerHoverEvent_t1767068455 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
