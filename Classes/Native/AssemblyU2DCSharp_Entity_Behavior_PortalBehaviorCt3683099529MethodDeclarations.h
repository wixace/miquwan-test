﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.PortalBehaviorCtrl
struct PortalBehaviorCtrl_t3683099529;
// CombatEntity
struct CombatEntity_t684137495;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"

// System.Void Entity.Behavior.PortalBehaviorCtrl::.ctor(CombatEntity)
extern "C"  void PortalBehaviorCtrl__ctor_m1294092426 (PortalBehaviorCtrl_t3683099529 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.PortalBehaviorCtrl::Init()
extern "C"  void PortalBehaviorCtrl_Init_m3334229107 (PortalBehaviorCtrl_t3683099529 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.PortalBehaviorCtrl::TranBehavior(Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void PortalBehaviorCtrl_TranBehavior_m944960797 (PortalBehaviorCtrl_t3683099529 * __this, uint8_t ___id0, ObjectU5BU5D_t1108656482* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.PortalBehaviorCtrl::ilo_AddBehavior1(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.IBehavior)
extern "C"  void PortalBehaviorCtrl_ilo_AddBehavior1_m1231408971 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, IBehavior_t770859129 * ___behavior1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.PortalBehaviorCtrl::ilo_set_curBehavior2(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.IBehavior)
extern "C"  void PortalBehaviorCtrl_ilo_set_curBehavior2_m1414505676 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, IBehavior_t770859129 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehavior Entity.Behavior.PortalBehaviorCtrl::ilo_get_curBehavior3(Entity.Behavior.IBehaviorCtrl)
extern "C"  IBehavior_t770859129 * PortalBehaviorCtrl_ilo_get_curBehavior3_m2018479316 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.PortalBehaviorCtrl::ilo_set_entity4(Entity.Behavior.IBehavior,CombatEntity)
extern "C"  void PortalBehaviorCtrl_ilo_set_entity4_m952594279 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, CombatEntity_t684137495 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
