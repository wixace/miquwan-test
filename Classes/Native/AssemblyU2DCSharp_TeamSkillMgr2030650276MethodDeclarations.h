﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TeamSkillMgr
struct TeamSkillMgr_t2030650276;
// TeamSkill
struct TeamSkill_t1816898004;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// CSGameDataMgr
struct CSGameDataMgr_t2623305516;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// System.Collections.Generic.List`1<CSHeroUnit>
struct List_1_t837576702;
// CSHeroData
struct CSHeroData_t3763839828;
// CSHeroUnit
struct CSHeroUnit_t3764358446;
// IZUpdate
struct IZUpdate_t3482043738;
// Hero
struct Hero_t2245658;
// FightCtrl
struct FightCtrl_t648967803;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// HeroMgr
struct HeroMgr_t2475965342;
// CombatEntity
struct CombatEntity_t684137495;
// skillCfg
struct skillCfg_t2142425171;
// System.Collections.Generic.List`1<Hero>
struct List_1_t1370431210;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_TeamSkillMgr2030650276.h"
#include "AssemblyU2DCSharp_CSHeroData3763839828.h"
#include "AssemblyU2DCSharp_HERO_ELEMENT2692579223.h"
#include "AssemblyU2DCSharp_CSHeroUnit3764358446.h"
#include "AssemblyU2DCSharp_Hero2245658.h"
#include "AssemblyU2DCSharp_FightCtrl648967803.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_FightEnum_EFightRst3627658870.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"
#include "AssemblyU2DCSharp_HeroMgr2475965342.h"
#include "System_Core_System_Action3771233898.h"

// System.Void TeamSkillMgr::.ctor()
extern "C"  void TeamSkillMgr__ctor_m3766912887 (TeamSkillMgr_t2030650276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TeamSkillMgr::get_rageRate()
extern "C"  float TeamSkillMgr_get_rageRate_m3376474287 (TeamSkillMgr_t2030650276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TeamSkillMgr::get_selectid()
extern "C"  int32_t TeamSkillMgr_get_selectid_m1900446139 (TeamSkillMgr_t2030650276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::set_selectid(System.Int32)
extern "C"  void TeamSkillMgr_set_selectid_m1300553714 (TeamSkillMgr_t2030650276 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TeamSkill TeamSkillMgr::get_curSelectSkill()
extern "C"  TeamSkill_t1816898004 * TeamSkillMgr_get_curSelectSkill_m153766378 (TeamSkillMgr_t2030650276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TeamSkillMgr::get_isUseingSkill()
extern "C"  bool TeamSkillMgr_get_isUseingSkill_m907144996 (TeamSkillMgr_t2030650276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TeamSkillMgr::get_isCanUse()
extern "C"  bool TeamSkillMgr_get_isCanUse_m186976555 (TeamSkillMgr_t2030650276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::set_isCanUse(System.Boolean)
extern "C"  void TeamSkillMgr_set_isCanUse_m3687113250 (TeamSkillMgr_t2030650276 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TeamSkillMgr::get_damage()
extern "C"  float TeamSkillMgr_get_damage_m362793489 (TeamSkillMgr_t2030650276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TeamSkillMgr::get_progresslei()
extern "C"  float TeamSkillMgr_get_progresslei_m1559187523 (TeamSkillMgr_t2030650276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TeamSkillMgr::get_progress()
extern "C"  float TeamSkillMgr_get_progress_m2564977935 (TeamSkillMgr_t2030650276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TeamSkillMgr::isTeamSkillAndType(System.Int32)
extern "C"  int32_t TeamSkillMgr_isTeamSkillAndType_m1711695699 (TeamSkillMgr_t2030650276 * __this, int32_t ___skillID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TeamSkillMgr::GetCurSkill(System.Int32)
extern "C"  int32_t TeamSkillMgr_GetCurSkill_m775598783 (TeamSkillMgr_t2030650276 * __this, int32_t ___checkPointID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::Clear(CEvent.ZEvent)
extern "C"  void TeamSkillMgr_Clear_m1416349721 (TeamSkillMgr_t2030650276 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::Init()
extern "C"  void TeamSkillMgr_Init_m1319975933 (TeamSkillMgr_t2030650276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TeamSkillMgr::UseSkill()
extern "C"  bool TeamSkillMgr_UseSkill_m2103799339 (TeamSkillMgr_t2030650276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::AddHeroSkill()
extern "C"  void TeamSkillMgr_AddHeroSkill_m3958150211 (TeamSkillMgr_t2030650276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::DelHeroSkill()
extern "C"  void TeamSkillMgr_DelHeroSkill_m3180733081 (TeamSkillMgr_t2030650276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::UseHeroSkill()
extern "C"  void TeamSkillMgr_UseHeroSkill_m2459917213 (TeamSkillMgr_t2030650276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TeamSkillMgr::SelectSkill(System.Int32)
extern "C"  bool TeamSkillMgr_SelectSkill_m1596234791 (TeamSkillMgr_t2030650276 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::AddRage()
extern "C"  void TeamSkillMgr_AddRage_m3242568611 (TeamSkillMgr_t2030650276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::AddRage(System.Int32)
extern "C"  void TeamSkillMgr_AddRage_m2065171572 (TeamSkillMgr_t2030650276 * __this, int32_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::AddExp()
extern "C"  void TeamSkillMgr_AddExp_m4249705129 (TeamSkillMgr_t2030650276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::OnGameWin()
extern "C"  void TeamSkillMgr_OnGameWin_m4145344384 (TeamSkillMgr_t2030650276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::ZUpdate()
extern "C"  void TeamSkillMgr_ZUpdate_m3209350328 (TeamSkillMgr_t2030650276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::OnUseSkillEnd_Break()
extern "C"  void TeamSkillMgr_OnUseSkillEnd_Break_m706629511 (TeamSkillMgr_t2030650276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::OnUseSkillEnd()
extern "C"  void TeamSkillMgr_OnUseSkillEnd_m3736956967 (TeamSkillMgr_t2030650276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::<OnUseSkillEnd>m__3E8()
extern "C"  void TeamSkillMgr_U3COnUseSkillEndU3Em__3E8_m2980814870 (TeamSkillMgr_t2030650276 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr TeamSkillMgr::ilo_get_FloatTextMgr1()
extern "C"  FloatTextMgr_t630384591 * TeamSkillMgr_ilo_get_FloatTextMgr1_m1832635117 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TeamSkill TeamSkillMgr::ilo_get_curSelectSkill2(TeamSkillMgr)
extern "C"  TeamSkill_t1816898004 * TeamSkillMgr_ilo_get_curSelectSkill2_m3357782515 (Il2CppObject * __this /* static, unused */, TeamSkillMgr_t2030650276 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSGameDataMgr TeamSkillMgr::ilo_get_CSGameDataMgr3()
extern "C"  CSGameDataMgr_t2623305516 * TeamSkillMgr_ilo_get_CSGameDataMgr3_m3914536695 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSDatacfgManager TeamSkillMgr::ilo_get_CfgDataMgr4()
extern "C"  CSDatacfgManager_t1565254243 * TeamSkillMgr_ilo_get_CfgDataMgr4_m777730185 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSHeroUnit> TeamSkillMgr::ilo_GetFightingHeroUntis5(CSHeroData,System.Int32)
extern "C"  List_1_t837576702 * TeamSkillMgr_ilo_GetFightingHeroUntis5_m2603017400 (Il2CppObject * __this /* static, unused */, CSHeroData_t3763839828 * ____this0, int32_t ___fuctionType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HERO_ELEMENT TeamSkillMgr::ilo_get_HeroElement6(CSHeroUnit)
extern "C"  int32_t TeamSkillMgr_ilo_get_HeroElement6_m951034581 (Il2CppObject * __this /* static, unused */, CSHeroUnit_t3764358446 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::ilo_RemoveUpdate7(IZUpdate)
extern "C"  void TeamSkillMgr_ilo_RemoveUpdate7_m1323505618 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::ilo_StopCoroutine8(System.UInt32)
extern "C"  void TeamSkillMgr_ilo_StopCoroutine8_m1975863448 (Il2CppObject * __this /* static, unused */, uint32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::ilo_DispatchEvent9(CEvent.ZEvent)
extern "C"  void TeamSkillMgr_ilo_DispatchEvent9_m1152496450 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TeamSkillMgr::ilo_get_isCaptain10(Hero)
extern "C"  bool TeamSkillMgr_ilo_get_isCaptain10_m1211067428 (Il2CppObject * __this /* static, unused */, Hero_t2245658 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::ilo_TeamAddSkill11(FightCtrl,System.Int32[])
extern "C"  void TeamSkillMgr_ilo_TeamAddSkill11_m684840107 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, Int32U5BU5D_t3230847821* ___skillIDS1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr TeamSkillMgr::ilo_get_instance12()
extern "C"  HeroMgr_t2475965342 * TeamSkillMgr_ilo_get_instance12_m3543185460 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightCtrl TeamSkillMgr::ilo_get_fightCtrl13(CombatEntity)
extern "C"  FightCtrl_t648967803 * TeamSkillMgr_ilo_get_fightCtrl13_m819844743 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::ilo_DelSkill14(FightCtrl,System.Int32[])
extern "C"  void TeamSkillMgr_ilo_DelSkill14_m2302176327 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, Int32U5BU5D_t3230847821* ___skillIDS1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightEnum.EFightRst TeamSkillMgr::ilo_UseSkillByID15(FightCtrl,System.Int32)
extern "C"  int32_t TeamSkillMgr_ilo_UseSkillByID15_m3392869192 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, int32_t ___skillID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TeamSkillMgr::ilo_get_selectid16(TeamSkillMgr)
extern "C"  int32_t TeamSkillMgr_ilo_get_selectid16_m779479343 (Il2CppObject * __this /* static, unused */, TeamSkillMgr_t2030650276 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::ilo_AddHeroSkill17(TeamSkillMgr)
extern "C"  void TeamSkillMgr_ilo_AddHeroSkill17_m8867576 (Il2CppObject * __this /* static, unused */, TeamSkillMgr_t2030650276 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// skillCfg TeamSkillMgr::ilo_GetskillCfg18(CSDatacfgManager,System.Int32)
extern "C"  skillCfg_t2142425171 * TeamSkillMgr_ilo_GetskillCfg18_m2241637618 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::ilo_UseHeroSkill19(TeamSkillMgr)
extern "C"  void TeamSkillMgr_ilo_UseHeroSkill19_m3794535764 (Il2CppObject * __this /* static, unused */, TeamSkillMgr_t2030650276 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Hero> TeamSkillMgr::ilo_GetAllHeros20(HeroMgr)
extern "C"  List_1_t1370431210 * TeamSkillMgr_ilo_GetAllHeros20_m55184883 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillMgr::ilo_OnUseSkillEnd21(TeamSkillMgr)
extern "C"  void TeamSkillMgr_ilo_OnUseSkillEnd21_m4069877551 (Il2CppObject * __this /* static, unused */, TeamSkillMgr_t2030650276 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TeamSkillMgr::ilo_get_atkSpeed22(CombatEntity)
extern "C"  float TeamSkillMgr_ilo_get_atkSpeed22_m195543981 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 TeamSkillMgr::ilo_DelayCall23(System.Single,System.Action)
extern "C"  uint32_t TeamSkillMgr_ilo_DelayCall23_m190751417 (Il2CppObject * __this /* static, unused */, float ___delaytime0, Action_t3771233898 * ___act1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
