﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraShotConfig
struct CameraShotConfig_t493527041;
// ProtoBuf.IExtension
struct IExtension_t1606339106;
// System.Collections.Generic.List`1<CameraShotsCfg>
struct List_1_t3108632096;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraShotConfig::.ctor()
extern "C"  void CameraShotConfig__ctor_m489158586 (CameraShotConfig_t493527041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension CameraShotConfig::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * CameraShotConfig_ProtoBuf_IExtensible_GetExtensionObject_m1743677834 (CameraShotConfig_t493527041 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotConfig::get_id()
extern "C"  int32_t CameraShotConfig_get_id_m2328711708 (CameraShotConfig_t493527041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotConfig::set_id(System.Int32)
extern "C"  void CameraShotConfig_set_id_m1530110547 (CameraShotConfig_t493527041 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CameraShotsCfg> CameraShotConfig::get_cameraShots()
extern "C"  List_1_t3108632096 * CameraShotConfig_get_cameraShots_m2250148704 (CameraShotConfig_t493527041 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
