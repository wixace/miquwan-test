﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.WebCamTexture
struct WebCamTexture_t1290350902;
// System.String
struct String_t;
// UnityEngine.WebCamDevice[]
struct WebCamDeviceU5BU5D_t3721690872;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_WebCamTexture1290350902.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Void UnityEngine.WebCamTexture::.ctor(System.String,System.Int32,System.Int32,System.Int32)
extern "C"  void WebCamTexture__ctor_m1622497486 (WebCamTexture_t1290350902 * __this, String_t* ___deviceName0, int32_t ___requestedWidth1, int32_t ___requestedHeight2, int32_t ___requestedFPS3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::.ctor(System.String,System.Int32,System.Int32)
extern "C"  void WebCamTexture__ctor_m3779632809 (WebCamTexture_t1290350902 * __this, String_t* ___deviceName0, int32_t ___requestedWidth1, int32_t ___requestedHeight2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::.ctor(System.String)
extern "C"  void WebCamTexture__ctor_m3625407113 (WebCamTexture_t1290350902 * __this, String_t* ___deviceName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::.ctor(System.Int32,System.Int32,System.Int32)
extern "C"  void WebCamTexture__ctor_m4204927434 (WebCamTexture_t1290350902 * __this, int32_t ___requestedWidth0, int32_t ___requestedHeight1, int32_t ___requestedFPS2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::.ctor(System.Int32,System.Int32)
extern "C"  void WebCamTexture__ctor_m1800008749 (WebCamTexture_t1290350902 * __this, int32_t ___requestedWidth0, int32_t ___requestedHeight1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::.ctor()
extern "C"  void WebCamTexture__ctor_m755637529 (WebCamTexture_t1290350902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::Internal_CreateWebCamTexture(UnityEngine.WebCamTexture,System.String,System.Int32,System.Int32,System.Int32)
extern "C"  void WebCamTexture_Internal_CreateWebCamTexture_m384251711 (Il2CppObject * __this /* static, unused */, WebCamTexture_t1290350902 * ___self0, String_t* ___scriptingDevice1, int32_t ___requestedWidth2, int32_t ___requestedHeight3, int32_t ___maxFramerate4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::Play()
extern "C"  void WebCamTexture_Play_m2252445503 (WebCamTexture_t1290350902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Play(UnityEngine.WebCamTexture)
extern "C"  void WebCamTexture_INTERNAL_CALL_Play_m3478980075 (Il2CppObject * __this /* static, unused */, WebCamTexture_t1290350902 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::Pause()
extern "C"  void WebCamTexture_Pause_m809763501 (WebCamTexture_t1290350902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Pause(UnityEngine.WebCamTexture)
extern "C"  void WebCamTexture_INTERNAL_CALL_Pause_m4232847003 (Il2CppObject * __this /* static, unused */, WebCamTexture_t1290350902 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::Stop()
extern "C"  void WebCamTexture_Stop_m2346129549 (WebCamTexture_t1290350902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_Stop(UnityEngine.WebCamTexture)
extern "C"  void WebCamTexture_INTERNAL_CALL_Stop_m3733059677 (Il2CppObject * __this /* static, unused */, WebCamTexture_t1290350902 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WebCamTexture::get_isPlaying()
extern "C"  bool WebCamTexture_get_isPlaying_m1109067512 (WebCamTexture_t1290350902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.WebCamTexture::get_deviceName()
extern "C"  String_t* WebCamTexture_get_deviceName_m1248945202 (WebCamTexture_t1290350902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::set_deviceName(System.String)
extern "C"  void WebCamTexture_set_deviceName_m1209257657 (WebCamTexture_t1290350902 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WebCamTexture::get_requestedFPS()
extern "C"  float WebCamTexture_get_requestedFPS_m3374681667 (WebCamTexture_t1290350902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::set_requestedFPS(System.Single)
extern "C"  void WebCamTexture_set_requestedFPS_m644257608 (WebCamTexture_t1290350902 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.WebCamTexture::get_requestedWidth()
extern "C"  int32_t WebCamTexture_get_requestedWidth_m3260948908 (WebCamTexture_t1290350902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::set_requestedWidth(System.Int32)
extern "C"  void WebCamTexture_set_requestedWidth_m3687101425 (WebCamTexture_t1290350902 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.WebCamTexture::get_requestedHeight()
extern "C"  int32_t WebCamTexture_get_requestedHeight_m2808982691 (WebCamTexture_t1290350902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::set_requestedHeight(System.Int32)
extern "C"  void WebCamTexture_set_requestedHeight_m3951846656 (WebCamTexture_t1290350902 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WebCamDevice[] UnityEngine.WebCamTexture::get_devices()
extern "C"  WebCamDeviceU5BU5D_t3721690872* WebCamTexture_get_devices_m3738445840 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.WebCamTexture::GetPixel(System.Int32,System.Int32)
extern "C"  Color_t4194546905  WebCamTexture_GetPixel_m2084592498 (WebCamTexture_t1290350902 * __this, int32_t ___x0, int32_t ___y1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WebCamTexture::INTERNAL_CALL_GetPixel(UnityEngine.WebCamTexture,System.Int32,System.Int32,UnityEngine.Color&)
extern "C"  void WebCamTexture_INTERNAL_CALL_GetPixel_m3477001501 (Il2CppObject * __this /* static, unused */, WebCamTexture_t1290350902 * ___self0, int32_t ___x1, int32_t ___y2, Color_t4194546905 * ___value3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine.WebCamTexture::GetPixels()
extern "C"  ColorU5BU5D_t2441545636* WebCamTexture_GetPixels_m2104673983 (WebCamTexture_t1290350902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine.WebCamTexture::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  ColorU5BU5D_t2441545636* WebCamTexture_GetPixels_m3429338087 (WebCamTexture_t1290350902 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32[] UnityEngine.WebCamTexture::GetPixels32(UnityEngine.Color32[])
extern "C"  Color32U5BU5D_t2960766953* WebCamTexture_GetPixels32_m2023060102 (WebCamTexture_t1290350902 * __this, Color32U5BU5D_t2960766953* ___colors0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32[] UnityEngine.WebCamTexture::GetPixels32()
extern "C"  Color32U5BU5D_t2960766953* WebCamTexture_GetPixels32_m3040478367 (WebCamTexture_t1290350902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.WebCamTexture::get_videoRotationAngle()
extern "C"  int32_t WebCamTexture_get_videoRotationAngle_m4198752238 (WebCamTexture_t1290350902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WebCamTexture::get_videoVerticallyMirrored()
extern "C"  bool WebCamTexture_get_videoVerticallyMirrored_m1911394608 (WebCamTexture_t1290350902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WebCamTexture::get_didUpdateThisFrame()
extern "C"  bool WebCamTexture_get_didUpdateThisFrame_m2572205429 (WebCamTexture_t1290350902 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
