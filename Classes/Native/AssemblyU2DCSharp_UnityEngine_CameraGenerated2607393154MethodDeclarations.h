﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_CameraGenerated
struct UnityEngine_CameraGenerated_t2607393154;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t1945583101;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// UnityEngine.RenderBuffer[]
struct RenderBufferU5BU5D_t1970987103;
// UnityEngine.Camera[]
struct CameraU5BU5D_t2716570836;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_CameraGenerated::.ctor()
extern "C"  void UnityEngine_CameraGenerated__ctor_m883463209 (UnityEngine_CameraGenerated_t2607393154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_Camera1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_Camera1_m1646790473 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera/CameraCallback UnityEngine_CameraGenerated::Camera_onPreCull_GetDelegate_member0_arg0(CSRepresentedObject)
extern "C"  CameraCallback_t1945583101 * UnityEngine_CameraGenerated_Camera_onPreCull_GetDelegate_member0_arg0_m979758933 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_onPreCull(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_onPreCull_m2564130960 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera/CameraCallback UnityEngine_CameraGenerated::Camera_onPreRender_GetDelegate_member1_arg0(CSRepresentedObject)
extern "C"  CameraCallback_t1945583101 * UnityEngine_CameraGenerated_Camera_onPreRender_GetDelegate_member1_arg0_m2819682746 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_onPreRender(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_onPreRender_m2555467244 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera/CameraCallback UnityEngine_CameraGenerated::Camera_onPostRender_GetDelegate_member2_arg0(CSRepresentedObject)
extern "C"  CameraCallback_t1945583101 * UnityEngine_CameraGenerated_Camera_onPostRender_GetDelegate_member2_arg0_m3762155970 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_onPostRender(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_onPostRender_m1708062065 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_fieldOfView(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_fieldOfView_m765574576 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_nearClipPlane(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_nearClipPlane_m3457443458 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_farClipPlane(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_farClipPlane_m3012848497 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_renderingPath(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_renderingPath_m1183264245 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_actualRenderingPath(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_actualRenderingPath_m4191071523 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_hdr(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_hdr_m1479275888 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_orthographicSize(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_orthographicSize_m3122428469 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_orthographic(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_orthographic_m2353467702 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_opaqueSortMode(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_opaqueSortMode_m1175801348 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_transparencySortMode(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_transparencySortMode_m3151139757 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_depth(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_depth_m2625186883 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_aspect(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_aspect_m2436162062 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_cullingMask(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_cullingMask_m3068595946 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_eventMask(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_eventMask_m2603533760 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_backgroundColor(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_backgroundColor_m551963761 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_rect(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_rect_m4204139010 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_pixelRect(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_pixelRect_m2325817692 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_targetTexture(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_targetTexture_m1035835548 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_pixelWidth(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_pixelWidth_m788484230 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_pixelHeight(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_pixelHeight_m3623618297 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_cameraToWorldMatrix(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_cameraToWorldMatrix_m4055054259 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_worldToCameraMatrix(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_worldToCameraMatrix_m3105361779 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_projectionMatrix(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_projectionMatrix_m523203190 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_velocity(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_velocity_m4139812649 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_clearFlags(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_clearFlags_m1200886892 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_stereoEnabled(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_stereoEnabled_m1044178333 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_stereoSeparation(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_stereoSeparation_m3197117384 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_stereoConvergence(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_stereoConvergence_m4155219309 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_cameraType(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_cameraType_m4021225031 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_stereoMirrorMode(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_stereoMirrorMode_m1758476428 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_targetDisplay(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_targetDisplay_m1442480341 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_main(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_main_m4142130637 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_current(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_current_m746970701 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_allCameras(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_allCameras_m1658759417 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_allCamerasCount(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_allCamerasCount_m1852055012 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_useOcclusionCulling(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_useOcclusionCulling_m2921899100 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_layerCullDistances(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_layerCullDistances_m474511819 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_layerCullSpherical(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_layerCullSpherical_m904948076 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_depthTextureMode(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_depthTextureMode_m2192074539 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_clearStencilAfterLightingPass(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_clearStencilAfterLightingPass_m4262433948 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::Camera_commandBufferCount(JSVCall)
extern "C"  void UnityEngine_CameraGenerated_Camera_commandBufferCount_m1083548546 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_AddCommandBuffer__CameraEvent__CommandBuffer(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_AddCommandBuffer__CameraEvent__CommandBuffer_m195237029 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_CalculateObliqueMatrix__Vector4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_CalculateObliqueMatrix__Vector4_m127571490 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_CopyFrom__Camera(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_CopyFrom__Camera_m142711529 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_GetCommandBuffers__CameraEvent(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_GetCommandBuffers__CameraEvent_m2125922972 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_RemoveAllCommandBuffers(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_RemoveAllCommandBuffers_m1227591458 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_RemoveCommandBuffer__CameraEvent__CommandBuffer(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_RemoveCommandBuffer__CameraEvent__CommandBuffer_m2373913594 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_RemoveCommandBuffers__CameraEvent(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_RemoveCommandBuffers__CameraEvent_m2328503494 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_Render(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_Render_m3850333979 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_RenderDontRestore(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_RenderDontRestore_m3795037860 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_RenderToCubemap__RenderTexture__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_RenderToCubemap__RenderTexture__Int32_m336668988 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_RenderToCubemap__Cubemap__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_RenderToCubemap__Cubemap__Int32_m2957613978 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_RenderToCubemap__RenderTexture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_RenderToCubemap__RenderTexture_m1461751700 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_RenderToCubemap__Cubemap(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_RenderToCubemap__Cubemap_m978250486 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_RenderWithShader__Shader__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_RenderWithShader__Shader__String_m179222844 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_ResetAspect(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_ResetAspect_m902528996 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_ResetFieldOfView(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_ResetFieldOfView_m2280775596 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_ResetProjectionMatrix(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_ResetProjectionMatrix_m2906317500 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_ResetReplacementShader(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_ResetReplacementShader_m3448711949 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_ResetStereoProjectionMatrices(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_ResetStereoProjectionMatrices_m3230926061 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_ResetStereoViewMatrices(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_ResetStereoViewMatrices_m3063600387 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_ResetWorldToCameraMatrix(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_ResetWorldToCameraMatrix_m3466816969 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_ScreenPointToRay__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_ScreenPointToRay__Vector3_m3184834562 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_ScreenToViewportPoint__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_ScreenToViewportPoint__Vector3_m3475468594 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_ScreenToWorldPoint__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_ScreenToWorldPoint__Vector3_m2802394824 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_SetReplacementShader__Shader__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_SetReplacementShader__Shader__String_m3649703440 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_SetStereoProjectionMatrices__Matrix4x4__Matrix4x4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_SetStereoProjectionMatrices__Matrix4x4__Matrix4x4_m3386775840 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_SetStereoViewMatrices__Matrix4x4__Matrix4x4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_SetStereoViewMatrices__Matrix4x4__Matrix4x4_m1532673782 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_SetTargetBuffers__RenderBuffer_Array__RenderBuffer(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_SetTargetBuffers__RenderBuffer_Array__RenderBuffer_m2462695243 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_SetTargetBuffers__RenderBuffer__RenderBuffer(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_SetTargetBuffers__RenderBuffer__RenderBuffer_m4152000145 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_ViewportPointToRay__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_ViewportPointToRay__Vector3_m703272456 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_ViewportToScreenPoint__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_ViewportToScreenPoint__Vector3_m393794802 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_ViewportToWorldPoint__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_ViewportToWorldPoint__Vector3_m1728060238 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_WorldToScreenPoint__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_WorldToScreenPoint__Vector3_m3896553590 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_WorldToViewportPoint__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_WorldToViewportPoint__Vector3_m2966942064 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_GetAllCameras__Camera_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_GetAllCameras__Camera_Array_m140784511 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::Camera_SetupCurrent__Camera(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_Camera_SetupCurrent__Camera_m1638224806 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::__Register()
extern "C"  void UnityEngine_CameraGenerated___Register_m2905348670 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera/CameraCallback UnityEngine_CameraGenerated::<Camera_onPreCull>m__191()
extern "C"  CameraCallback_t1945583101 * UnityEngine_CameraGenerated_U3CCamera_onPreCullU3Em__191_m754255015 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera/CameraCallback UnityEngine_CameraGenerated::<Camera_onPreRender>m__193()
extern "C"  CameraCallback_t1945583101 * UnityEngine_CameraGenerated_U3CCamera_onPreRenderU3Em__193_m1363230981 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera/CameraCallback UnityEngine_CameraGenerated::<Camera_onPostRender>m__195()
extern "C"  CameraCallback_t1945583101 * UnityEngine_CameraGenerated_U3CCamera_onPostRenderU3Em__195_m1750174792 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] UnityEngine_CameraGenerated::<Camera_layerCullDistances>m__196()
extern "C"  SingleU5BU5D_t2316563989* UnityEngine_CameraGenerated_U3CCamera_layerCullDistancesU3Em__196_m3450322839 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RenderBuffer[] UnityEngine_CameraGenerated::<Camera_SetTargetBuffers__RenderBuffer_Array__RenderBuffer>m__197()
extern "C"  RenderBufferU5BU5D_t1970987103* UnityEngine_CameraGenerated_U3CCamera_SetTargetBuffers__RenderBuffer_Array__RenderBufferU3Em__197_m4061717041 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera[] UnityEngine_CameraGenerated::<Camera_GetAllCameras__Camera_Array>m__198()
extern "C"  CameraU5BU5D_t2716570836* UnityEngine_CameraGenerated_U3CCamera_GetAllCameras__Camera_ArrayU3Em__198_m3559595801 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CameraGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_CameraGenerated_ilo_setObject1_m3690610213 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_CameraGenerated::ilo_getSingle2(System.Int32)
extern "C"  float UnityEngine_CameraGenerated_ilo_getSingle2_m385432519 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_CameraGenerated_ilo_setSingle3_m139842733 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::ilo_getBooleanS4(System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_ilo_getBooleanS4_m3113934686 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CameraGenerated::ilo_getEnum5(System.Int32)
extern "C"  int32_t UnityEngine_CameraGenerated_ilo_getEnum5_m428204015 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::ilo_setEnum6(System.Int32,System.Int32)
extern "C"  void UnityEngine_CameraGenerated_ilo_setEnum6_m4083212965 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_CameraGenerated::ilo_getObject7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_CameraGenerated_ilo_getObject7_m2898727202 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::ilo_setVector3S8(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_CameraGenerated_ilo_setVector3S8_m3507552453 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::ilo_setBooleanS9(System.Int32,System.Boolean)
extern "C"  void UnityEngine_CameraGenerated_ilo_setBooleanS9_m2356538578 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CameraGenerated::ilo_getInt3210(System.Int32)
extern "C"  int32_t UnityEngine_CameraGenerated_ilo_getInt3210_m110661576 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CameraGenerated::ilo_setArrayS11(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_CameraGenerated_ilo_setArrayS11_m143222375 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_CameraGenerated::ilo_getStringS12(System.Int32)
extern "C"  String_t* UnityEngine_CameraGenerated_ilo_getStringS12_m1740019881 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_CameraGenerated::ilo_getVector3S13(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_CameraGenerated_ilo_getVector3S13_m163776698 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CameraGenerated::ilo_isFunctionS14(System.Int32)
extern "C"  bool UnityEngine_CameraGenerated_ilo_isFunctionS14_m1646772781 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject UnityEngine_CameraGenerated::ilo_getFunctionS15(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * UnityEngine_CameraGenerated_ilo_getFunctionS15_m1844249579 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera/CameraCallback UnityEngine_CameraGenerated::ilo_Camera_onPreRender_GetDelegate_member1_arg016(CSRepresentedObject)
extern "C"  CameraCallback_t1945583101 * UnityEngine_CameraGenerated_ilo_Camera_onPreRender_GetDelegate_member1_arg016_m1282372712 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CameraGenerated::ilo_getObject17(System.Int32)
extern "C"  int32_t UnityEngine_CameraGenerated_ilo_getObject17_m162953806 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CameraGenerated::ilo_getArrayLength18(System.Int32)
extern "C"  int32_t UnityEngine_CameraGenerated_ilo_getArrayLength18_m671918369 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
