﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_mene129
struct  M_mene129_t3458920393  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_mene129::_stelhawe
	String_t* ____stelhawe_0;
	// System.String GarbageiOS.M_mene129::_meakalltere
	String_t* ____meakalltere_1;
	// System.Single GarbageiOS.M_mene129::_jouwurCinaygow
	float ____jouwurCinaygow_2;
	// System.Boolean GarbageiOS.M_mene129::_veltornuNesair
	bool ____veltornuNesair_3;
	// System.Boolean GarbageiOS.M_mene129::_kayjorDalkoupair
	bool ____kayjorDalkoupair_4;
	// System.Single GarbageiOS.M_mene129::_sardar
	float ____sardar_5;
	// System.Single GarbageiOS.M_mene129::_nedairPouvego
	float ____nedairPouvego_6;
	// System.Single GarbageiOS.M_mene129::_pijeenai
	float ____pijeenai_7;
	// System.Single GarbageiOS.M_mene129::_karyawtarMarkou
	float ____karyawtarMarkou_8;
	// System.UInt32 GarbageiOS.M_mene129::_yurremBeedoras
	uint32_t ____yurremBeedoras_9;

public:
	inline static int32_t get_offset_of__stelhawe_0() { return static_cast<int32_t>(offsetof(M_mene129_t3458920393, ____stelhawe_0)); }
	inline String_t* get__stelhawe_0() const { return ____stelhawe_0; }
	inline String_t** get_address_of__stelhawe_0() { return &____stelhawe_0; }
	inline void set__stelhawe_0(String_t* value)
	{
		____stelhawe_0 = value;
		Il2CppCodeGenWriteBarrier(&____stelhawe_0, value);
	}

	inline static int32_t get_offset_of__meakalltere_1() { return static_cast<int32_t>(offsetof(M_mene129_t3458920393, ____meakalltere_1)); }
	inline String_t* get__meakalltere_1() const { return ____meakalltere_1; }
	inline String_t** get_address_of__meakalltere_1() { return &____meakalltere_1; }
	inline void set__meakalltere_1(String_t* value)
	{
		____meakalltere_1 = value;
		Il2CppCodeGenWriteBarrier(&____meakalltere_1, value);
	}

	inline static int32_t get_offset_of__jouwurCinaygow_2() { return static_cast<int32_t>(offsetof(M_mene129_t3458920393, ____jouwurCinaygow_2)); }
	inline float get__jouwurCinaygow_2() const { return ____jouwurCinaygow_2; }
	inline float* get_address_of__jouwurCinaygow_2() { return &____jouwurCinaygow_2; }
	inline void set__jouwurCinaygow_2(float value)
	{
		____jouwurCinaygow_2 = value;
	}

	inline static int32_t get_offset_of__veltornuNesair_3() { return static_cast<int32_t>(offsetof(M_mene129_t3458920393, ____veltornuNesair_3)); }
	inline bool get__veltornuNesair_3() const { return ____veltornuNesair_3; }
	inline bool* get_address_of__veltornuNesair_3() { return &____veltornuNesair_3; }
	inline void set__veltornuNesair_3(bool value)
	{
		____veltornuNesair_3 = value;
	}

	inline static int32_t get_offset_of__kayjorDalkoupair_4() { return static_cast<int32_t>(offsetof(M_mene129_t3458920393, ____kayjorDalkoupair_4)); }
	inline bool get__kayjorDalkoupair_4() const { return ____kayjorDalkoupair_4; }
	inline bool* get_address_of__kayjorDalkoupair_4() { return &____kayjorDalkoupair_4; }
	inline void set__kayjorDalkoupair_4(bool value)
	{
		____kayjorDalkoupair_4 = value;
	}

	inline static int32_t get_offset_of__sardar_5() { return static_cast<int32_t>(offsetof(M_mene129_t3458920393, ____sardar_5)); }
	inline float get__sardar_5() const { return ____sardar_5; }
	inline float* get_address_of__sardar_5() { return &____sardar_5; }
	inline void set__sardar_5(float value)
	{
		____sardar_5 = value;
	}

	inline static int32_t get_offset_of__nedairPouvego_6() { return static_cast<int32_t>(offsetof(M_mene129_t3458920393, ____nedairPouvego_6)); }
	inline float get__nedairPouvego_6() const { return ____nedairPouvego_6; }
	inline float* get_address_of__nedairPouvego_6() { return &____nedairPouvego_6; }
	inline void set__nedairPouvego_6(float value)
	{
		____nedairPouvego_6 = value;
	}

	inline static int32_t get_offset_of__pijeenai_7() { return static_cast<int32_t>(offsetof(M_mene129_t3458920393, ____pijeenai_7)); }
	inline float get__pijeenai_7() const { return ____pijeenai_7; }
	inline float* get_address_of__pijeenai_7() { return &____pijeenai_7; }
	inline void set__pijeenai_7(float value)
	{
		____pijeenai_7 = value;
	}

	inline static int32_t get_offset_of__karyawtarMarkou_8() { return static_cast<int32_t>(offsetof(M_mene129_t3458920393, ____karyawtarMarkou_8)); }
	inline float get__karyawtarMarkou_8() const { return ____karyawtarMarkou_8; }
	inline float* get_address_of__karyawtarMarkou_8() { return &____karyawtarMarkou_8; }
	inline void set__karyawtarMarkou_8(float value)
	{
		____karyawtarMarkou_8 = value;
	}

	inline static int32_t get_offset_of__yurremBeedoras_9() { return static_cast<int32_t>(offsetof(M_mene129_t3458920393, ____yurremBeedoras_9)); }
	inline uint32_t get__yurremBeedoras_9() const { return ____yurremBeedoras_9; }
	inline uint32_t* get_address_of__yurremBeedoras_9() { return &____yurremBeedoras_9; }
	inline void set__yurremBeedoras_9(uint32_t value)
	{
		____yurremBeedoras_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
