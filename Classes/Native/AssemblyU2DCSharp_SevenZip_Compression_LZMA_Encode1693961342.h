﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.UInt32[]
struct UInt32U5BU5D_t3230734560;
// SevenZip.Compression.LZMA.Encoder/Optimal[]
struct OptimalU5BU5D_t591851880;
// SevenZip.Compression.LZ.IMatchFinder
struct IMatchFinder_t333938384;
// SevenZip.Compression.RangeCoder.Encoder
struct Encoder_t2248006694;
// SevenZip.Compression.RangeCoder.BitEncoder[]
struct BitEncoderU5BU5D_t2970556732;
// SevenZip.Compression.RangeCoder.BitTreeEncoder[]
struct BitTreeEncoderU5BU5D_t1594753010;
// SevenZip.Compression.LZMA.Encoder/LenPriceTableEncoder
struct LenPriceTableEncoder_t2154457791;
// SevenZip.Compression.LZMA.Encoder/LiteralEncoder
struct LiteralEncoder_t1395322410;
// System.IO.Stream
struct Stream_t1561764144;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Base_St344225277.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_3875796003.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Encode3313976957.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SevenZip.Compression.LZMA.Encoder
struct  Encoder_t1693961342  : public Il2CppObject
{
public:
	// SevenZip.Compression.LZMA.Base/State SevenZip.Compression.LZMA.Encoder::_state
	State_t344225277  ____state_7;
	// System.Byte SevenZip.Compression.LZMA.Encoder::_previousByte
	uint8_t ____previousByte_8;
	// System.UInt32[] SevenZip.Compression.LZMA.Encoder::_repDistances
	UInt32U5BU5D_t3230734560* ____repDistances_9;
	// SevenZip.Compression.LZMA.Encoder/Optimal[] SevenZip.Compression.LZMA.Encoder::_optimum
	OptimalU5BU5D_t591851880* ____optimum_10;
	// SevenZip.Compression.LZ.IMatchFinder SevenZip.Compression.LZMA.Encoder::_matchFinder
	Il2CppObject * ____matchFinder_11;
	// SevenZip.Compression.RangeCoder.Encoder SevenZip.Compression.LZMA.Encoder::_rangeEncoder
	Encoder_t2248006694 * ____rangeEncoder_12;
	// SevenZip.Compression.RangeCoder.BitEncoder[] SevenZip.Compression.LZMA.Encoder::_isMatch
	BitEncoderU5BU5D_t2970556732* ____isMatch_13;
	// SevenZip.Compression.RangeCoder.BitEncoder[] SevenZip.Compression.LZMA.Encoder::_isRep
	BitEncoderU5BU5D_t2970556732* ____isRep_14;
	// SevenZip.Compression.RangeCoder.BitEncoder[] SevenZip.Compression.LZMA.Encoder::_isRepG0
	BitEncoderU5BU5D_t2970556732* ____isRepG0_15;
	// SevenZip.Compression.RangeCoder.BitEncoder[] SevenZip.Compression.LZMA.Encoder::_isRepG1
	BitEncoderU5BU5D_t2970556732* ____isRepG1_16;
	// SevenZip.Compression.RangeCoder.BitEncoder[] SevenZip.Compression.LZMA.Encoder::_isRepG2
	BitEncoderU5BU5D_t2970556732* ____isRepG2_17;
	// SevenZip.Compression.RangeCoder.BitEncoder[] SevenZip.Compression.LZMA.Encoder::_isRep0Long
	BitEncoderU5BU5D_t2970556732* ____isRep0Long_18;
	// SevenZip.Compression.RangeCoder.BitTreeEncoder[] SevenZip.Compression.LZMA.Encoder::_posSlotEncoder
	BitTreeEncoderU5BU5D_t1594753010* ____posSlotEncoder_19;
	// SevenZip.Compression.RangeCoder.BitEncoder[] SevenZip.Compression.LZMA.Encoder::_posEncoders
	BitEncoderU5BU5D_t2970556732* ____posEncoders_20;
	// SevenZip.Compression.RangeCoder.BitTreeEncoder SevenZip.Compression.LZMA.Encoder::_posAlignEncoder
	BitTreeEncoder_t3875796003  ____posAlignEncoder_21;
	// SevenZip.Compression.LZMA.Encoder/LenPriceTableEncoder SevenZip.Compression.LZMA.Encoder::_lenEncoder
	LenPriceTableEncoder_t2154457791 * ____lenEncoder_22;
	// SevenZip.Compression.LZMA.Encoder/LenPriceTableEncoder SevenZip.Compression.LZMA.Encoder::_repMatchLenEncoder
	LenPriceTableEncoder_t2154457791 * ____repMatchLenEncoder_23;
	// SevenZip.Compression.LZMA.Encoder/LiteralEncoder SevenZip.Compression.LZMA.Encoder::_literalEncoder
	LiteralEncoder_t1395322410 * ____literalEncoder_24;
	// System.UInt32[] SevenZip.Compression.LZMA.Encoder::_matchDistances
	UInt32U5BU5D_t3230734560* ____matchDistances_25;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder::_numFastBytes
	uint32_t ____numFastBytes_26;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder::_longestMatchLength
	uint32_t ____longestMatchLength_27;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder::_numDistancePairs
	uint32_t ____numDistancePairs_28;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder::_additionalOffset
	uint32_t ____additionalOffset_29;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder::_optimumEndIndex
	uint32_t ____optimumEndIndex_30;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder::_optimumCurrentIndex
	uint32_t ____optimumCurrentIndex_31;
	// System.Boolean SevenZip.Compression.LZMA.Encoder::_longestMatchWasFound
	bool ____longestMatchWasFound_32;
	// System.UInt32[] SevenZip.Compression.LZMA.Encoder::_posSlotPrices
	UInt32U5BU5D_t3230734560* ____posSlotPrices_33;
	// System.UInt32[] SevenZip.Compression.LZMA.Encoder::_distancesPrices
	UInt32U5BU5D_t3230734560* ____distancesPrices_34;
	// System.UInt32[] SevenZip.Compression.LZMA.Encoder::_alignPrices
	UInt32U5BU5D_t3230734560* ____alignPrices_35;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder::_alignPriceCount
	uint32_t ____alignPriceCount_36;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder::_distTableSize
	uint32_t ____distTableSize_37;
	// System.Int32 SevenZip.Compression.LZMA.Encoder::_posStateBits
	int32_t ____posStateBits_38;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder::_posStateMask
	uint32_t ____posStateMask_39;
	// System.Int32 SevenZip.Compression.LZMA.Encoder::_numLiteralPosStateBits
	int32_t ____numLiteralPosStateBits_40;
	// System.Int32 SevenZip.Compression.LZMA.Encoder::_numLiteralContextBits
	int32_t ____numLiteralContextBits_41;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder::_dictionarySize
	uint32_t ____dictionarySize_42;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder::_dictionarySizePrev
	uint32_t ____dictionarySizePrev_43;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder::_numFastBytesPrev
	uint32_t ____numFastBytesPrev_44;
	// System.Int64 SevenZip.Compression.LZMA.Encoder::nowPos64
	int64_t ___nowPos64_45;
	// System.Boolean SevenZip.Compression.LZMA.Encoder::_finished
	bool ____finished_46;
	// System.IO.Stream SevenZip.Compression.LZMA.Encoder::_inStream
	Stream_t1561764144 * ____inStream_47;
	// SevenZip.Compression.LZMA.Encoder/EMatchFinderType SevenZip.Compression.LZMA.Encoder::_matchFinderType
	int32_t ____matchFinderType_48;
	// System.Boolean SevenZip.Compression.LZMA.Encoder::_writeEndMark
	bool ____writeEndMark_49;
	// System.Boolean SevenZip.Compression.LZMA.Encoder::_needReleaseMFStream
	bool ____needReleaseMFStream_50;
	// System.UInt32[] SevenZip.Compression.LZMA.Encoder::reps
	UInt32U5BU5D_t3230734560* ___reps_51;
	// System.UInt32[] SevenZip.Compression.LZMA.Encoder::repLens
	UInt32U5BU5D_t3230734560* ___repLens_52;
	// System.Byte[] SevenZip.Compression.LZMA.Encoder::properties
	ByteU5BU5D_t4260760469* ___properties_53;
	// System.UInt32[] SevenZip.Compression.LZMA.Encoder::tempPrices
	UInt32U5BU5D_t3230734560* ___tempPrices_54;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder::_matchPriceCount
	uint32_t ____matchPriceCount_55;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder::_trainSize
	uint32_t ____trainSize_57;

public:
	inline static int32_t get_offset_of__state_7() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____state_7)); }
	inline State_t344225277  get__state_7() const { return ____state_7; }
	inline State_t344225277 * get_address_of__state_7() { return &____state_7; }
	inline void set__state_7(State_t344225277  value)
	{
		____state_7 = value;
	}

	inline static int32_t get_offset_of__previousByte_8() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____previousByte_8)); }
	inline uint8_t get__previousByte_8() const { return ____previousByte_8; }
	inline uint8_t* get_address_of__previousByte_8() { return &____previousByte_8; }
	inline void set__previousByte_8(uint8_t value)
	{
		____previousByte_8 = value;
	}

	inline static int32_t get_offset_of__repDistances_9() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____repDistances_9)); }
	inline UInt32U5BU5D_t3230734560* get__repDistances_9() const { return ____repDistances_9; }
	inline UInt32U5BU5D_t3230734560** get_address_of__repDistances_9() { return &____repDistances_9; }
	inline void set__repDistances_9(UInt32U5BU5D_t3230734560* value)
	{
		____repDistances_9 = value;
		Il2CppCodeGenWriteBarrier(&____repDistances_9, value);
	}

	inline static int32_t get_offset_of__optimum_10() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____optimum_10)); }
	inline OptimalU5BU5D_t591851880* get__optimum_10() const { return ____optimum_10; }
	inline OptimalU5BU5D_t591851880** get_address_of__optimum_10() { return &____optimum_10; }
	inline void set__optimum_10(OptimalU5BU5D_t591851880* value)
	{
		____optimum_10 = value;
		Il2CppCodeGenWriteBarrier(&____optimum_10, value);
	}

	inline static int32_t get_offset_of__matchFinder_11() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____matchFinder_11)); }
	inline Il2CppObject * get__matchFinder_11() const { return ____matchFinder_11; }
	inline Il2CppObject ** get_address_of__matchFinder_11() { return &____matchFinder_11; }
	inline void set__matchFinder_11(Il2CppObject * value)
	{
		____matchFinder_11 = value;
		Il2CppCodeGenWriteBarrier(&____matchFinder_11, value);
	}

	inline static int32_t get_offset_of__rangeEncoder_12() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____rangeEncoder_12)); }
	inline Encoder_t2248006694 * get__rangeEncoder_12() const { return ____rangeEncoder_12; }
	inline Encoder_t2248006694 ** get_address_of__rangeEncoder_12() { return &____rangeEncoder_12; }
	inline void set__rangeEncoder_12(Encoder_t2248006694 * value)
	{
		____rangeEncoder_12 = value;
		Il2CppCodeGenWriteBarrier(&____rangeEncoder_12, value);
	}

	inline static int32_t get_offset_of__isMatch_13() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____isMatch_13)); }
	inline BitEncoderU5BU5D_t2970556732* get__isMatch_13() const { return ____isMatch_13; }
	inline BitEncoderU5BU5D_t2970556732** get_address_of__isMatch_13() { return &____isMatch_13; }
	inline void set__isMatch_13(BitEncoderU5BU5D_t2970556732* value)
	{
		____isMatch_13 = value;
		Il2CppCodeGenWriteBarrier(&____isMatch_13, value);
	}

	inline static int32_t get_offset_of__isRep_14() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____isRep_14)); }
	inline BitEncoderU5BU5D_t2970556732* get__isRep_14() const { return ____isRep_14; }
	inline BitEncoderU5BU5D_t2970556732** get_address_of__isRep_14() { return &____isRep_14; }
	inline void set__isRep_14(BitEncoderU5BU5D_t2970556732* value)
	{
		____isRep_14 = value;
		Il2CppCodeGenWriteBarrier(&____isRep_14, value);
	}

	inline static int32_t get_offset_of__isRepG0_15() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____isRepG0_15)); }
	inline BitEncoderU5BU5D_t2970556732* get__isRepG0_15() const { return ____isRepG0_15; }
	inline BitEncoderU5BU5D_t2970556732** get_address_of__isRepG0_15() { return &____isRepG0_15; }
	inline void set__isRepG0_15(BitEncoderU5BU5D_t2970556732* value)
	{
		____isRepG0_15 = value;
		Il2CppCodeGenWriteBarrier(&____isRepG0_15, value);
	}

	inline static int32_t get_offset_of__isRepG1_16() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____isRepG1_16)); }
	inline BitEncoderU5BU5D_t2970556732* get__isRepG1_16() const { return ____isRepG1_16; }
	inline BitEncoderU5BU5D_t2970556732** get_address_of__isRepG1_16() { return &____isRepG1_16; }
	inline void set__isRepG1_16(BitEncoderU5BU5D_t2970556732* value)
	{
		____isRepG1_16 = value;
		Il2CppCodeGenWriteBarrier(&____isRepG1_16, value);
	}

	inline static int32_t get_offset_of__isRepG2_17() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____isRepG2_17)); }
	inline BitEncoderU5BU5D_t2970556732* get__isRepG2_17() const { return ____isRepG2_17; }
	inline BitEncoderU5BU5D_t2970556732** get_address_of__isRepG2_17() { return &____isRepG2_17; }
	inline void set__isRepG2_17(BitEncoderU5BU5D_t2970556732* value)
	{
		____isRepG2_17 = value;
		Il2CppCodeGenWriteBarrier(&____isRepG2_17, value);
	}

	inline static int32_t get_offset_of__isRep0Long_18() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____isRep0Long_18)); }
	inline BitEncoderU5BU5D_t2970556732* get__isRep0Long_18() const { return ____isRep0Long_18; }
	inline BitEncoderU5BU5D_t2970556732** get_address_of__isRep0Long_18() { return &____isRep0Long_18; }
	inline void set__isRep0Long_18(BitEncoderU5BU5D_t2970556732* value)
	{
		____isRep0Long_18 = value;
		Il2CppCodeGenWriteBarrier(&____isRep0Long_18, value);
	}

	inline static int32_t get_offset_of__posSlotEncoder_19() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____posSlotEncoder_19)); }
	inline BitTreeEncoderU5BU5D_t1594753010* get__posSlotEncoder_19() const { return ____posSlotEncoder_19; }
	inline BitTreeEncoderU5BU5D_t1594753010** get_address_of__posSlotEncoder_19() { return &____posSlotEncoder_19; }
	inline void set__posSlotEncoder_19(BitTreeEncoderU5BU5D_t1594753010* value)
	{
		____posSlotEncoder_19 = value;
		Il2CppCodeGenWriteBarrier(&____posSlotEncoder_19, value);
	}

	inline static int32_t get_offset_of__posEncoders_20() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____posEncoders_20)); }
	inline BitEncoderU5BU5D_t2970556732* get__posEncoders_20() const { return ____posEncoders_20; }
	inline BitEncoderU5BU5D_t2970556732** get_address_of__posEncoders_20() { return &____posEncoders_20; }
	inline void set__posEncoders_20(BitEncoderU5BU5D_t2970556732* value)
	{
		____posEncoders_20 = value;
		Il2CppCodeGenWriteBarrier(&____posEncoders_20, value);
	}

	inline static int32_t get_offset_of__posAlignEncoder_21() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____posAlignEncoder_21)); }
	inline BitTreeEncoder_t3875796003  get__posAlignEncoder_21() const { return ____posAlignEncoder_21; }
	inline BitTreeEncoder_t3875796003 * get_address_of__posAlignEncoder_21() { return &____posAlignEncoder_21; }
	inline void set__posAlignEncoder_21(BitTreeEncoder_t3875796003  value)
	{
		____posAlignEncoder_21 = value;
	}

	inline static int32_t get_offset_of__lenEncoder_22() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____lenEncoder_22)); }
	inline LenPriceTableEncoder_t2154457791 * get__lenEncoder_22() const { return ____lenEncoder_22; }
	inline LenPriceTableEncoder_t2154457791 ** get_address_of__lenEncoder_22() { return &____lenEncoder_22; }
	inline void set__lenEncoder_22(LenPriceTableEncoder_t2154457791 * value)
	{
		____lenEncoder_22 = value;
		Il2CppCodeGenWriteBarrier(&____lenEncoder_22, value);
	}

	inline static int32_t get_offset_of__repMatchLenEncoder_23() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____repMatchLenEncoder_23)); }
	inline LenPriceTableEncoder_t2154457791 * get__repMatchLenEncoder_23() const { return ____repMatchLenEncoder_23; }
	inline LenPriceTableEncoder_t2154457791 ** get_address_of__repMatchLenEncoder_23() { return &____repMatchLenEncoder_23; }
	inline void set__repMatchLenEncoder_23(LenPriceTableEncoder_t2154457791 * value)
	{
		____repMatchLenEncoder_23 = value;
		Il2CppCodeGenWriteBarrier(&____repMatchLenEncoder_23, value);
	}

	inline static int32_t get_offset_of__literalEncoder_24() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____literalEncoder_24)); }
	inline LiteralEncoder_t1395322410 * get__literalEncoder_24() const { return ____literalEncoder_24; }
	inline LiteralEncoder_t1395322410 ** get_address_of__literalEncoder_24() { return &____literalEncoder_24; }
	inline void set__literalEncoder_24(LiteralEncoder_t1395322410 * value)
	{
		____literalEncoder_24 = value;
		Il2CppCodeGenWriteBarrier(&____literalEncoder_24, value);
	}

	inline static int32_t get_offset_of__matchDistances_25() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____matchDistances_25)); }
	inline UInt32U5BU5D_t3230734560* get__matchDistances_25() const { return ____matchDistances_25; }
	inline UInt32U5BU5D_t3230734560** get_address_of__matchDistances_25() { return &____matchDistances_25; }
	inline void set__matchDistances_25(UInt32U5BU5D_t3230734560* value)
	{
		____matchDistances_25 = value;
		Il2CppCodeGenWriteBarrier(&____matchDistances_25, value);
	}

	inline static int32_t get_offset_of__numFastBytes_26() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____numFastBytes_26)); }
	inline uint32_t get__numFastBytes_26() const { return ____numFastBytes_26; }
	inline uint32_t* get_address_of__numFastBytes_26() { return &____numFastBytes_26; }
	inline void set__numFastBytes_26(uint32_t value)
	{
		____numFastBytes_26 = value;
	}

	inline static int32_t get_offset_of__longestMatchLength_27() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____longestMatchLength_27)); }
	inline uint32_t get__longestMatchLength_27() const { return ____longestMatchLength_27; }
	inline uint32_t* get_address_of__longestMatchLength_27() { return &____longestMatchLength_27; }
	inline void set__longestMatchLength_27(uint32_t value)
	{
		____longestMatchLength_27 = value;
	}

	inline static int32_t get_offset_of__numDistancePairs_28() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____numDistancePairs_28)); }
	inline uint32_t get__numDistancePairs_28() const { return ____numDistancePairs_28; }
	inline uint32_t* get_address_of__numDistancePairs_28() { return &____numDistancePairs_28; }
	inline void set__numDistancePairs_28(uint32_t value)
	{
		____numDistancePairs_28 = value;
	}

	inline static int32_t get_offset_of__additionalOffset_29() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____additionalOffset_29)); }
	inline uint32_t get__additionalOffset_29() const { return ____additionalOffset_29; }
	inline uint32_t* get_address_of__additionalOffset_29() { return &____additionalOffset_29; }
	inline void set__additionalOffset_29(uint32_t value)
	{
		____additionalOffset_29 = value;
	}

	inline static int32_t get_offset_of__optimumEndIndex_30() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____optimumEndIndex_30)); }
	inline uint32_t get__optimumEndIndex_30() const { return ____optimumEndIndex_30; }
	inline uint32_t* get_address_of__optimumEndIndex_30() { return &____optimumEndIndex_30; }
	inline void set__optimumEndIndex_30(uint32_t value)
	{
		____optimumEndIndex_30 = value;
	}

	inline static int32_t get_offset_of__optimumCurrentIndex_31() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____optimumCurrentIndex_31)); }
	inline uint32_t get__optimumCurrentIndex_31() const { return ____optimumCurrentIndex_31; }
	inline uint32_t* get_address_of__optimumCurrentIndex_31() { return &____optimumCurrentIndex_31; }
	inline void set__optimumCurrentIndex_31(uint32_t value)
	{
		____optimumCurrentIndex_31 = value;
	}

	inline static int32_t get_offset_of__longestMatchWasFound_32() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____longestMatchWasFound_32)); }
	inline bool get__longestMatchWasFound_32() const { return ____longestMatchWasFound_32; }
	inline bool* get_address_of__longestMatchWasFound_32() { return &____longestMatchWasFound_32; }
	inline void set__longestMatchWasFound_32(bool value)
	{
		____longestMatchWasFound_32 = value;
	}

	inline static int32_t get_offset_of__posSlotPrices_33() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____posSlotPrices_33)); }
	inline UInt32U5BU5D_t3230734560* get__posSlotPrices_33() const { return ____posSlotPrices_33; }
	inline UInt32U5BU5D_t3230734560** get_address_of__posSlotPrices_33() { return &____posSlotPrices_33; }
	inline void set__posSlotPrices_33(UInt32U5BU5D_t3230734560* value)
	{
		____posSlotPrices_33 = value;
		Il2CppCodeGenWriteBarrier(&____posSlotPrices_33, value);
	}

	inline static int32_t get_offset_of__distancesPrices_34() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____distancesPrices_34)); }
	inline UInt32U5BU5D_t3230734560* get__distancesPrices_34() const { return ____distancesPrices_34; }
	inline UInt32U5BU5D_t3230734560** get_address_of__distancesPrices_34() { return &____distancesPrices_34; }
	inline void set__distancesPrices_34(UInt32U5BU5D_t3230734560* value)
	{
		____distancesPrices_34 = value;
		Il2CppCodeGenWriteBarrier(&____distancesPrices_34, value);
	}

	inline static int32_t get_offset_of__alignPrices_35() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____alignPrices_35)); }
	inline UInt32U5BU5D_t3230734560* get__alignPrices_35() const { return ____alignPrices_35; }
	inline UInt32U5BU5D_t3230734560** get_address_of__alignPrices_35() { return &____alignPrices_35; }
	inline void set__alignPrices_35(UInt32U5BU5D_t3230734560* value)
	{
		____alignPrices_35 = value;
		Il2CppCodeGenWriteBarrier(&____alignPrices_35, value);
	}

	inline static int32_t get_offset_of__alignPriceCount_36() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____alignPriceCount_36)); }
	inline uint32_t get__alignPriceCount_36() const { return ____alignPriceCount_36; }
	inline uint32_t* get_address_of__alignPriceCount_36() { return &____alignPriceCount_36; }
	inline void set__alignPriceCount_36(uint32_t value)
	{
		____alignPriceCount_36 = value;
	}

	inline static int32_t get_offset_of__distTableSize_37() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____distTableSize_37)); }
	inline uint32_t get__distTableSize_37() const { return ____distTableSize_37; }
	inline uint32_t* get_address_of__distTableSize_37() { return &____distTableSize_37; }
	inline void set__distTableSize_37(uint32_t value)
	{
		____distTableSize_37 = value;
	}

	inline static int32_t get_offset_of__posStateBits_38() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____posStateBits_38)); }
	inline int32_t get__posStateBits_38() const { return ____posStateBits_38; }
	inline int32_t* get_address_of__posStateBits_38() { return &____posStateBits_38; }
	inline void set__posStateBits_38(int32_t value)
	{
		____posStateBits_38 = value;
	}

	inline static int32_t get_offset_of__posStateMask_39() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____posStateMask_39)); }
	inline uint32_t get__posStateMask_39() const { return ____posStateMask_39; }
	inline uint32_t* get_address_of__posStateMask_39() { return &____posStateMask_39; }
	inline void set__posStateMask_39(uint32_t value)
	{
		____posStateMask_39 = value;
	}

	inline static int32_t get_offset_of__numLiteralPosStateBits_40() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____numLiteralPosStateBits_40)); }
	inline int32_t get__numLiteralPosStateBits_40() const { return ____numLiteralPosStateBits_40; }
	inline int32_t* get_address_of__numLiteralPosStateBits_40() { return &____numLiteralPosStateBits_40; }
	inline void set__numLiteralPosStateBits_40(int32_t value)
	{
		____numLiteralPosStateBits_40 = value;
	}

	inline static int32_t get_offset_of__numLiteralContextBits_41() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____numLiteralContextBits_41)); }
	inline int32_t get__numLiteralContextBits_41() const { return ____numLiteralContextBits_41; }
	inline int32_t* get_address_of__numLiteralContextBits_41() { return &____numLiteralContextBits_41; }
	inline void set__numLiteralContextBits_41(int32_t value)
	{
		____numLiteralContextBits_41 = value;
	}

	inline static int32_t get_offset_of__dictionarySize_42() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____dictionarySize_42)); }
	inline uint32_t get__dictionarySize_42() const { return ____dictionarySize_42; }
	inline uint32_t* get_address_of__dictionarySize_42() { return &____dictionarySize_42; }
	inline void set__dictionarySize_42(uint32_t value)
	{
		____dictionarySize_42 = value;
	}

	inline static int32_t get_offset_of__dictionarySizePrev_43() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____dictionarySizePrev_43)); }
	inline uint32_t get__dictionarySizePrev_43() const { return ____dictionarySizePrev_43; }
	inline uint32_t* get_address_of__dictionarySizePrev_43() { return &____dictionarySizePrev_43; }
	inline void set__dictionarySizePrev_43(uint32_t value)
	{
		____dictionarySizePrev_43 = value;
	}

	inline static int32_t get_offset_of__numFastBytesPrev_44() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____numFastBytesPrev_44)); }
	inline uint32_t get__numFastBytesPrev_44() const { return ____numFastBytesPrev_44; }
	inline uint32_t* get_address_of__numFastBytesPrev_44() { return &____numFastBytesPrev_44; }
	inline void set__numFastBytesPrev_44(uint32_t value)
	{
		____numFastBytesPrev_44 = value;
	}

	inline static int32_t get_offset_of_nowPos64_45() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ___nowPos64_45)); }
	inline int64_t get_nowPos64_45() const { return ___nowPos64_45; }
	inline int64_t* get_address_of_nowPos64_45() { return &___nowPos64_45; }
	inline void set_nowPos64_45(int64_t value)
	{
		___nowPos64_45 = value;
	}

	inline static int32_t get_offset_of__finished_46() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____finished_46)); }
	inline bool get__finished_46() const { return ____finished_46; }
	inline bool* get_address_of__finished_46() { return &____finished_46; }
	inline void set__finished_46(bool value)
	{
		____finished_46 = value;
	}

	inline static int32_t get_offset_of__inStream_47() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____inStream_47)); }
	inline Stream_t1561764144 * get__inStream_47() const { return ____inStream_47; }
	inline Stream_t1561764144 ** get_address_of__inStream_47() { return &____inStream_47; }
	inline void set__inStream_47(Stream_t1561764144 * value)
	{
		____inStream_47 = value;
		Il2CppCodeGenWriteBarrier(&____inStream_47, value);
	}

	inline static int32_t get_offset_of__matchFinderType_48() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____matchFinderType_48)); }
	inline int32_t get__matchFinderType_48() const { return ____matchFinderType_48; }
	inline int32_t* get_address_of__matchFinderType_48() { return &____matchFinderType_48; }
	inline void set__matchFinderType_48(int32_t value)
	{
		____matchFinderType_48 = value;
	}

	inline static int32_t get_offset_of__writeEndMark_49() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____writeEndMark_49)); }
	inline bool get__writeEndMark_49() const { return ____writeEndMark_49; }
	inline bool* get_address_of__writeEndMark_49() { return &____writeEndMark_49; }
	inline void set__writeEndMark_49(bool value)
	{
		____writeEndMark_49 = value;
	}

	inline static int32_t get_offset_of__needReleaseMFStream_50() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____needReleaseMFStream_50)); }
	inline bool get__needReleaseMFStream_50() const { return ____needReleaseMFStream_50; }
	inline bool* get_address_of__needReleaseMFStream_50() { return &____needReleaseMFStream_50; }
	inline void set__needReleaseMFStream_50(bool value)
	{
		____needReleaseMFStream_50 = value;
	}

	inline static int32_t get_offset_of_reps_51() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ___reps_51)); }
	inline UInt32U5BU5D_t3230734560* get_reps_51() const { return ___reps_51; }
	inline UInt32U5BU5D_t3230734560** get_address_of_reps_51() { return &___reps_51; }
	inline void set_reps_51(UInt32U5BU5D_t3230734560* value)
	{
		___reps_51 = value;
		Il2CppCodeGenWriteBarrier(&___reps_51, value);
	}

	inline static int32_t get_offset_of_repLens_52() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ___repLens_52)); }
	inline UInt32U5BU5D_t3230734560* get_repLens_52() const { return ___repLens_52; }
	inline UInt32U5BU5D_t3230734560** get_address_of_repLens_52() { return &___repLens_52; }
	inline void set_repLens_52(UInt32U5BU5D_t3230734560* value)
	{
		___repLens_52 = value;
		Il2CppCodeGenWriteBarrier(&___repLens_52, value);
	}

	inline static int32_t get_offset_of_properties_53() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ___properties_53)); }
	inline ByteU5BU5D_t4260760469* get_properties_53() const { return ___properties_53; }
	inline ByteU5BU5D_t4260760469** get_address_of_properties_53() { return &___properties_53; }
	inline void set_properties_53(ByteU5BU5D_t4260760469* value)
	{
		___properties_53 = value;
		Il2CppCodeGenWriteBarrier(&___properties_53, value);
	}

	inline static int32_t get_offset_of_tempPrices_54() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ___tempPrices_54)); }
	inline UInt32U5BU5D_t3230734560* get_tempPrices_54() const { return ___tempPrices_54; }
	inline UInt32U5BU5D_t3230734560** get_address_of_tempPrices_54() { return &___tempPrices_54; }
	inline void set_tempPrices_54(UInt32U5BU5D_t3230734560* value)
	{
		___tempPrices_54 = value;
		Il2CppCodeGenWriteBarrier(&___tempPrices_54, value);
	}

	inline static int32_t get_offset_of__matchPriceCount_55() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____matchPriceCount_55)); }
	inline uint32_t get__matchPriceCount_55() const { return ____matchPriceCount_55; }
	inline uint32_t* get_address_of__matchPriceCount_55() { return &____matchPriceCount_55; }
	inline void set__matchPriceCount_55(uint32_t value)
	{
		____matchPriceCount_55 = value;
	}

	inline static int32_t get_offset_of__trainSize_57() { return static_cast<int32_t>(offsetof(Encoder_t1693961342, ____trainSize_57)); }
	inline uint32_t get__trainSize_57() const { return ____trainSize_57; }
	inline uint32_t* get_address_of__trainSize_57() { return &____trainSize_57; }
	inline void set__trainSize_57(uint32_t value)
	{
		____trainSize_57 = value;
	}
};

struct Encoder_t1693961342_StaticFields
{
public:
	// System.Byte[] SevenZip.Compression.LZMA.Encoder::g_FastPos
	ByteU5BU5D_t4260760469* ___g_FastPos_6;
	// System.String[] SevenZip.Compression.LZMA.Encoder::kMatchFinderIDs
	StringU5BU5D_t4054002952* ___kMatchFinderIDs_56;

public:
	inline static int32_t get_offset_of_g_FastPos_6() { return static_cast<int32_t>(offsetof(Encoder_t1693961342_StaticFields, ___g_FastPos_6)); }
	inline ByteU5BU5D_t4260760469* get_g_FastPos_6() const { return ___g_FastPos_6; }
	inline ByteU5BU5D_t4260760469** get_address_of_g_FastPos_6() { return &___g_FastPos_6; }
	inline void set_g_FastPos_6(ByteU5BU5D_t4260760469* value)
	{
		___g_FastPos_6 = value;
		Il2CppCodeGenWriteBarrier(&___g_FastPos_6, value);
	}

	inline static int32_t get_offset_of_kMatchFinderIDs_56() { return static_cast<int32_t>(offsetof(Encoder_t1693961342_StaticFields, ___kMatchFinderIDs_56)); }
	inline StringU5BU5D_t4054002952* get_kMatchFinderIDs_56() const { return ___kMatchFinderIDs_56; }
	inline StringU5BU5D_t4054002952** get_address_of_kMatchFinderIDs_56() { return &___kMatchFinderIDs_56; }
	inline void set_kMatchFinderIDs_56(StringU5BU5D_t4054002952* value)
	{
		___kMatchFinderIDs_56 = value;
		Il2CppCodeGenWriteBarrier(&___kMatchFinderIDs_56, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
