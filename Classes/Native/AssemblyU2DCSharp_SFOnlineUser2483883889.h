﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SFOnlineUser
struct  SFOnlineUser_t2483883889  : public Il2CppObject
{
public:
	// System.Int64 SFOnlineUser::id
	int64_t ___id_0;
	// System.String SFOnlineUser::channelId
	String_t* ___channelId_1;
	// System.String SFOnlineUser::channelUserId
	String_t* ___channelUserId_2;
	// System.String SFOnlineUser::userName
	String_t* ___userName_3;
	// System.String SFOnlineUser::token
	String_t* ___token_4;
	// System.String SFOnlineUser::productCode
	String_t* ___productCode_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(SFOnlineUser_t2483883889, ___id_0)); }
	inline int64_t get_id_0() const { return ___id_0; }
	inline int64_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int64_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_channelId_1() { return static_cast<int32_t>(offsetof(SFOnlineUser_t2483883889, ___channelId_1)); }
	inline String_t* get_channelId_1() const { return ___channelId_1; }
	inline String_t** get_address_of_channelId_1() { return &___channelId_1; }
	inline void set_channelId_1(String_t* value)
	{
		___channelId_1 = value;
		Il2CppCodeGenWriteBarrier(&___channelId_1, value);
	}

	inline static int32_t get_offset_of_channelUserId_2() { return static_cast<int32_t>(offsetof(SFOnlineUser_t2483883889, ___channelUserId_2)); }
	inline String_t* get_channelUserId_2() const { return ___channelUserId_2; }
	inline String_t** get_address_of_channelUserId_2() { return &___channelUserId_2; }
	inline void set_channelUserId_2(String_t* value)
	{
		___channelUserId_2 = value;
		Il2CppCodeGenWriteBarrier(&___channelUserId_2, value);
	}

	inline static int32_t get_offset_of_userName_3() { return static_cast<int32_t>(offsetof(SFOnlineUser_t2483883889, ___userName_3)); }
	inline String_t* get_userName_3() const { return ___userName_3; }
	inline String_t** get_address_of_userName_3() { return &___userName_3; }
	inline void set_userName_3(String_t* value)
	{
		___userName_3 = value;
		Il2CppCodeGenWriteBarrier(&___userName_3, value);
	}

	inline static int32_t get_offset_of_token_4() { return static_cast<int32_t>(offsetof(SFOnlineUser_t2483883889, ___token_4)); }
	inline String_t* get_token_4() const { return ___token_4; }
	inline String_t** get_address_of_token_4() { return &___token_4; }
	inline void set_token_4(String_t* value)
	{
		___token_4 = value;
		Il2CppCodeGenWriteBarrier(&___token_4, value);
	}

	inline static int32_t get_offset_of_productCode_5() { return static_cast<int32_t>(offsetof(SFOnlineUser_t2483883889, ___productCode_5)); }
	inline String_t* get_productCode_5() const { return ___productCode_5; }
	inline String_t** get_address_of_productCode_5() { return &___productCode_5; }
	inline void set_productCode_5(String_t* value)
	{
		___productCode_5 = value;
		Il2CppCodeGenWriteBarrier(&___productCode_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
