﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UILabel
struct UILabel_t291504320;
// UITexture
struct UITexture_t3903132647;
// UILabel[]
struct UILabelU5BU5D_t1494885441;
// TweenScale
struct TweenScale_t2936666559;
// TweenAlpha
struct TweenAlpha_t2920325587;
// TweenPosition[]
struct TweenPositionU5BU5D_t2332880541;
// TweenAlpha[]
struct TweenAlphaU5BU5D_t2326787458;
// TweenPosition
struct TweenPosition_t3684358292;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Float_equip_streng_master
struct  Float_equip_streng_master_t3807538562  : public FloatTextUnit_t2362298029
{
public:
	// UnityEngine.GameObject Float_equip_streng_master::Obj_value_left_go
	GameObject_t3674682005 * ___Obj_value_left_go_3;
	// UnityEngine.GameObject Float_equip_streng_master::Obj_value_right_go
	GameObject_t3674682005 * ___Obj_value_right_go_4;
	// UILabel Float_equip_streng_master::Label_tile_txt
	UILabel_t291504320 * ___Label_tile_txt_5;
	// UILabel Float_equip_streng_master::Label_tile_1_txt
	UILabel_t291504320 * ___Label_tile_1_txt_6;
	// UILabel Float_equip_streng_master::Label_tile_2_txt
	UILabel_t291504320 * ___Label_tile_2_txt_7;
	// UnityEngine.GameObject Float_equip_streng_master::Obj_add_value_go
	GameObject_t3674682005 * ___Obj_add_value_go_8;
	// UnityEngine.GameObject Float_equip_streng_master::MasterTile_go
	GameObject_t3674682005 * ___MasterTile_go_9;
	// UnityEngine.GameObject Float_equip_streng_master::MasterTileEffect_go
	GameObject_t3674682005 * ___MasterTileEffect_go_10;
	// UnityEngine.GameObject Float_equip_streng_master::bg_go
	GameObject_t3674682005 * ___bg_go_11;
	// UnityEngine.GameObject Float_equip_streng_master::Left_go
	GameObject_t3674682005 * ___Left_go_12;
	// UnityEngine.GameObject Float_equip_streng_master::ArrowLeft_go
	GameObject_t3674682005 * ___ArrowLeft_go_13;
	// UnityEngine.GameObject Float_equip_streng_master::Right_go
	GameObject_t3674682005 * ___Right_go_14;
	// UnityEngine.GameObject Float_equip_streng_master::Arrow_go
	GameObject_t3674682005 * ___Arrow_go_15;
	// UnityEngine.GameObject Float_equip_streng_master::ArrowUp_go
	GameObject_t3674682005 * ___ArrowUp_go_16;
	// UnityEngine.GameObject Float_equip_streng_master::Obj_effect_go
	GameObject_t3674682005 * ___Obj_effect_go_17;
	// UITexture Float_equip_streng_master::Texture_tex
	UITexture_t3903132647 * ___Texture_tex_18;
	// UILabel[] Float_equip_streng_master::Label_Right_value
	UILabelU5BU5D_t1494885441* ___Label_Right_value_19;
	// UILabel[] Float_equip_streng_master::Label_Left_value
	UILabelU5BU5D_t1494885441* ___Label_Left_value_20;
	// UILabel[] Float_equip_streng_master::Label_Add_value
	UILabelU5BU5D_t1494885441* ___Label_Add_value_21;
	// TweenScale Float_equip_streng_master::BGTs
	TweenScale_t2936666559 * ___BGTs_22;
	// TweenScale Float_equip_streng_master::MasterTileTs
	TweenScale_t2936666559 * ___MasterTileTs_23;
	// TweenAlpha Float_equip_streng_master::MasterTileTa
	TweenAlpha_t2920325587 * ___MasterTileTa_24;
	// TweenAlpha Float_equip_streng_master::TileTa
	TweenAlpha_t2920325587 * ___TileTa_25;
	// TweenAlpha Float_equip_streng_master::LeftTextureTa
	TweenAlpha_t2920325587 * ___LeftTextureTa_26;
	// TweenAlpha Float_equip_streng_master::RightTextureTa
	TweenAlpha_t2920325587 * ___RightTextureTa_27;
	// TweenAlpha Float_equip_streng_master::ArrowTa
	TweenAlpha_t2920325587 * ___ArrowTa_28;
	// TweenAlpha Float_equip_streng_master::Tile01Ta
	TweenAlpha_t2920325587 * ___Tile01Ta_29;
	// TweenAlpha Float_equip_streng_master::Tile02Ta
	TweenAlpha_t2920325587 * ___Tile02Ta_30;
	// TweenPosition[] Float_equip_streng_master::LeftValueTp
	TweenPositionU5BU5D_t2332880541* ___LeftValueTp_31;
	// TweenAlpha[] Float_equip_streng_master::LeftValueTa
	TweenAlphaU5BU5D_t2326787458* ___LeftValueTa_32;
	// TweenPosition[] Float_equip_streng_master::ArrowLeftTp
	TweenPositionU5BU5D_t2332880541* ___ArrowLeftTp_33;
	// TweenAlpha[] Float_equip_streng_master::ArrowLeftTa
	TweenAlphaU5BU5D_t2326787458* ___ArrowLeftTa_34;
	// TweenPosition[] Float_equip_streng_master::RightValueTp
	TweenPositionU5BU5D_t2332880541* ___RightValueTp_35;
	// TweenAlpha[] Float_equip_streng_master::RightValueTa
	TweenAlphaU5BU5D_t2326787458* ___RightValueTa_36;
	// TweenAlpha Float_equip_streng_master::ArrowUpTa
	TweenAlpha_t2920325587 * ___ArrowUpTa_37;
	// TweenPosition Float_equip_streng_master::ArrowUpTp
	TweenPosition_t3684358292 * ___ArrowUpTp_38;
	// TweenAlpha Float_equip_streng_master::AddValueTa
	TweenAlpha_t2920325587 * ___AddValueTa_39;
	// TweenPosition Float_equip_streng_master::AddValueTp
	TweenPosition_t3684358292 * ___AddValueTp_40;
	// UnityEngine.Transform Float_equip_streng_master::ArrowUp_tr
	Transform_t1659122786 * ___ArrowUp_tr_41;
	// System.Int32 Float_equip_streng_master::masterLv
	int32_t ___masterLv_42;
	// System.Boolean Float_equip_streng_master::isCanClose
	bool ___isCanClose_43;
	// System.UInt32 Float_equip_streng_master::delayID
	uint32_t ___delayID_44;

public:
	inline static int32_t get_offset_of_Obj_value_left_go_3() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___Obj_value_left_go_3)); }
	inline GameObject_t3674682005 * get_Obj_value_left_go_3() const { return ___Obj_value_left_go_3; }
	inline GameObject_t3674682005 ** get_address_of_Obj_value_left_go_3() { return &___Obj_value_left_go_3; }
	inline void set_Obj_value_left_go_3(GameObject_t3674682005 * value)
	{
		___Obj_value_left_go_3 = value;
		Il2CppCodeGenWriteBarrier(&___Obj_value_left_go_3, value);
	}

	inline static int32_t get_offset_of_Obj_value_right_go_4() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___Obj_value_right_go_4)); }
	inline GameObject_t3674682005 * get_Obj_value_right_go_4() const { return ___Obj_value_right_go_4; }
	inline GameObject_t3674682005 ** get_address_of_Obj_value_right_go_4() { return &___Obj_value_right_go_4; }
	inline void set_Obj_value_right_go_4(GameObject_t3674682005 * value)
	{
		___Obj_value_right_go_4 = value;
		Il2CppCodeGenWriteBarrier(&___Obj_value_right_go_4, value);
	}

	inline static int32_t get_offset_of_Label_tile_txt_5() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___Label_tile_txt_5)); }
	inline UILabel_t291504320 * get_Label_tile_txt_5() const { return ___Label_tile_txt_5; }
	inline UILabel_t291504320 ** get_address_of_Label_tile_txt_5() { return &___Label_tile_txt_5; }
	inline void set_Label_tile_txt_5(UILabel_t291504320 * value)
	{
		___Label_tile_txt_5 = value;
		Il2CppCodeGenWriteBarrier(&___Label_tile_txt_5, value);
	}

	inline static int32_t get_offset_of_Label_tile_1_txt_6() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___Label_tile_1_txt_6)); }
	inline UILabel_t291504320 * get_Label_tile_1_txt_6() const { return ___Label_tile_1_txt_6; }
	inline UILabel_t291504320 ** get_address_of_Label_tile_1_txt_6() { return &___Label_tile_1_txt_6; }
	inline void set_Label_tile_1_txt_6(UILabel_t291504320 * value)
	{
		___Label_tile_1_txt_6 = value;
		Il2CppCodeGenWriteBarrier(&___Label_tile_1_txt_6, value);
	}

	inline static int32_t get_offset_of_Label_tile_2_txt_7() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___Label_tile_2_txt_7)); }
	inline UILabel_t291504320 * get_Label_tile_2_txt_7() const { return ___Label_tile_2_txt_7; }
	inline UILabel_t291504320 ** get_address_of_Label_tile_2_txt_7() { return &___Label_tile_2_txt_7; }
	inline void set_Label_tile_2_txt_7(UILabel_t291504320 * value)
	{
		___Label_tile_2_txt_7 = value;
		Il2CppCodeGenWriteBarrier(&___Label_tile_2_txt_7, value);
	}

	inline static int32_t get_offset_of_Obj_add_value_go_8() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___Obj_add_value_go_8)); }
	inline GameObject_t3674682005 * get_Obj_add_value_go_8() const { return ___Obj_add_value_go_8; }
	inline GameObject_t3674682005 ** get_address_of_Obj_add_value_go_8() { return &___Obj_add_value_go_8; }
	inline void set_Obj_add_value_go_8(GameObject_t3674682005 * value)
	{
		___Obj_add_value_go_8 = value;
		Il2CppCodeGenWriteBarrier(&___Obj_add_value_go_8, value);
	}

	inline static int32_t get_offset_of_MasterTile_go_9() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___MasterTile_go_9)); }
	inline GameObject_t3674682005 * get_MasterTile_go_9() const { return ___MasterTile_go_9; }
	inline GameObject_t3674682005 ** get_address_of_MasterTile_go_9() { return &___MasterTile_go_9; }
	inline void set_MasterTile_go_9(GameObject_t3674682005 * value)
	{
		___MasterTile_go_9 = value;
		Il2CppCodeGenWriteBarrier(&___MasterTile_go_9, value);
	}

	inline static int32_t get_offset_of_MasterTileEffect_go_10() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___MasterTileEffect_go_10)); }
	inline GameObject_t3674682005 * get_MasterTileEffect_go_10() const { return ___MasterTileEffect_go_10; }
	inline GameObject_t3674682005 ** get_address_of_MasterTileEffect_go_10() { return &___MasterTileEffect_go_10; }
	inline void set_MasterTileEffect_go_10(GameObject_t3674682005 * value)
	{
		___MasterTileEffect_go_10 = value;
		Il2CppCodeGenWriteBarrier(&___MasterTileEffect_go_10, value);
	}

	inline static int32_t get_offset_of_bg_go_11() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___bg_go_11)); }
	inline GameObject_t3674682005 * get_bg_go_11() const { return ___bg_go_11; }
	inline GameObject_t3674682005 ** get_address_of_bg_go_11() { return &___bg_go_11; }
	inline void set_bg_go_11(GameObject_t3674682005 * value)
	{
		___bg_go_11 = value;
		Il2CppCodeGenWriteBarrier(&___bg_go_11, value);
	}

	inline static int32_t get_offset_of_Left_go_12() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___Left_go_12)); }
	inline GameObject_t3674682005 * get_Left_go_12() const { return ___Left_go_12; }
	inline GameObject_t3674682005 ** get_address_of_Left_go_12() { return &___Left_go_12; }
	inline void set_Left_go_12(GameObject_t3674682005 * value)
	{
		___Left_go_12 = value;
		Il2CppCodeGenWriteBarrier(&___Left_go_12, value);
	}

	inline static int32_t get_offset_of_ArrowLeft_go_13() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___ArrowLeft_go_13)); }
	inline GameObject_t3674682005 * get_ArrowLeft_go_13() const { return ___ArrowLeft_go_13; }
	inline GameObject_t3674682005 ** get_address_of_ArrowLeft_go_13() { return &___ArrowLeft_go_13; }
	inline void set_ArrowLeft_go_13(GameObject_t3674682005 * value)
	{
		___ArrowLeft_go_13 = value;
		Il2CppCodeGenWriteBarrier(&___ArrowLeft_go_13, value);
	}

	inline static int32_t get_offset_of_Right_go_14() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___Right_go_14)); }
	inline GameObject_t3674682005 * get_Right_go_14() const { return ___Right_go_14; }
	inline GameObject_t3674682005 ** get_address_of_Right_go_14() { return &___Right_go_14; }
	inline void set_Right_go_14(GameObject_t3674682005 * value)
	{
		___Right_go_14 = value;
		Il2CppCodeGenWriteBarrier(&___Right_go_14, value);
	}

	inline static int32_t get_offset_of_Arrow_go_15() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___Arrow_go_15)); }
	inline GameObject_t3674682005 * get_Arrow_go_15() const { return ___Arrow_go_15; }
	inline GameObject_t3674682005 ** get_address_of_Arrow_go_15() { return &___Arrow_go_15; }
	inline void set_Arrow_go_15(GameObject_t3674682005 * value)
	{
		___Arrow_go_15 = value;
		Il2CppCodeGenWriteBarrier(&___Arrow_go_15, value);
	}

	inline static int32_t get_offset_of_ArrowUp_go_16() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___ArrowUp_go_16)); }
	inline GameObject_t3674682005 * get_ArrowUp_go_16() const { return ___ArrowUp_go_16; }
	inline GameObject_t3674682005 ** get_address_of_ArrowUp_go_16() { return &___ArrowUp_go_16; }
	inline void set_ArrowUp_go_16(GameObject_t3674682005 * value)
	{
		___ArrowUp_go_16 = value;
		Il2CppCodeGenWriteBarrier(&___ArrowUp_go_16, value);
	}

	inline static int32_t get_offset_of_Obj_effect_go_17() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___Obj_effect_go_17)); }
	inline GameObject_t3674682005 * get_Obj_effect_go_17() const { return ___Obj_effect_go_17; }
	inline GameObject_t3674682005 ** get_address_of_Obj_effect_go_17() { return &___Obj_effect_go_17; }
	inline void set_Obj_effect_go_17(GameObject_t3674682005 * value)
	{
		___Obj_effect_go_17 = value;
		Il2CppCodeGenWriteBarrier(&___Obj_effect_go_17, value);
	}

	inline static int32_t get_offset_of_Texture_tex_18() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___Texture_tex_18)); }
	inline UITexture_t3903132647 * get_Texture_tex_18() const { return ___Texture_tex_18; }
	inline UITexture_t3903132647 ** get_address_of_Texture_tex_18() { return &___Texture_tex_18; }
	inline void set_Texture_tex_18(UITexture_t3903132647 * value)
	{
		___Texture_tex_18 = value;
		Il2CppCodeGenWriteBarrier(&___Texture_tex_18, value);
	}

	inline static int32_t get_offset_of_Label_Right_value_19() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___Label_Right_value_19)); }
	inline UILabelU5BU5D_t1494885441* get_Label_Right_value_19() const { return ___Label_Right_value_19; }
	inline UILabelU5BU5D_t1494885441** get_address_of_Label_Right_value_19() { return &___Label_Right_value_19; }
	inline void set_Label_Right_value_19(UILabelU5BU5D_t1494885441* value)
	{
		___Label_Right_value_19 = value;
		Il2CppCodeGenWriteBarrier(&___Label_Right_value_19, value);
	}

	inline static int32_t get_offset_of_Label_Left_value_20() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___Label_Left_value_20)); }
	inline UILabelU5BU5D_t1494885441* get_Label_Left_value_20() const { return ___Label_Left_value_20; }
	inline UILabelU5BU5D_t1494885441** get_address_of_Label_Left_value_20() { return &___Label_Left_value_20; }
	inline void set_Label_Left_value_20(UILabelU5BU5D_t1494885441* value)
	{
		___Label_Left_value_20 = value;
		Il2CppCodeGenWriteBarrier(&___Label_Left_value_20, value);
	}

	inline static int32_t get_offset_of_Label_Add_value_21() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___Label_Add_value_21)); }
	inline UILabelU5BU5D_t1494885441* get_Label_Add_value_21() const { return ___Label_Add_value_21; }
	inline UILabelU5BU5D_t1494885441** get_address_of_Label_Add_value_21() { return &___Label_Add_value_21; }
	inline void set_Label_Add_value_21(UILabelU5BU5D_t1494885441* value)
	{
		___Label_Add_value_21 = value;
		Il2CppCodeGenWriteBarrier(&___Label_Add_value_21, value);
	}

	inline static int32_t get_offset_of_BGTs_22() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___BGTs_22)); }
	inline TweenScale_t2936666559 * get_BGTs_22() const { return ___BGTs_22; }
	inline TweenScale_t2936666559 ** get_address_of_BGTs_22() { return &___BGTs_22; }
	inline void set_BGTs_22(TweenScale_t2936666559 * value)
	{
		___BGTs_22 = value;
		Il2CppCodeGenWriteBarrier(&___BGTs_22, value);
	}

	inline static int32_t get_offset_of_MasterTileTs_23() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___MasterTileTs_23)); }
	inline TweenScale_t2936666559 * get_MasterTileTs_23() const { return ___MasterTileTs_23; }
	inline TweenScale_t2936666559 ** get_address_of_MasterTileTs_23() { return &___MasterTileTs_23; }
	inline void set_MasterTileTs_23(TweenScale_t2936666559 * value)
	{
		___MasterTileTs_23 = value;
		Il2CppCodeGenWriteBarrier(&___MasterTileTs_23, value);
	}

	inline static int32_t get_offset_of_MasterTileTa_24() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___MasterTileTa_24)); }
	inline TweenAlpha_t2920325587 * get_MasterTileTa_24() const { return ___MasterTileTa_24; }
	inline TweenAlpha_t2920325587 ** get_address_of_MasterTileTa_24() { return &___MasterTileTa_24; }
	inline void set_MasterTileTa_24(TweenAlpha_t2920325587 * value)
	{
		___MasterTileTa_24 = value;
		Il2CppCodeGenWriteBarrier(&___MasterTileTa_24, value);
	}

	inline static int32_t get_offset_of_TileTa_25() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___TileTa_25)); }
	inline TweenAlpha_t2920325587 * get_TileTa_25() const { return ___TileTa_25; }
	inline TweenAlpha_t2920325587 ** get_address_of_TileTa_25() { return &___TileTa_25; }
	inline void set_TileTa_25(TweenAlpha_t2920325587 * value)
	{
		___TileTa_25 = value;
		Il2CppCodeGenWriteBarrier(&___TileTa_25, value);
	}

	inline static int32_t get_offset_of_LeftTextureTa_26() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___LeftTextureTa_26)); }
	inline TweenAlpha_t2920325587 * get_LeftTextureTa_26() const { return ___LeftTextureTa_26; }
	inline TweenAlpha_t2920325587 ** get_address_of_LeftTextureTa_26() { return &___LeftTextureTa_26; }
	inline void set_LeftTextureTa_26(TweenAlpha_t2920325587 * value)
	{
		___LeftTextureTa_26 = value;
		Il2CppCodeGenWriteBarrier(&___LeftTextureTa_26, value);
	}

	inline static int32_t get_offset_of_RightTextureTa_27() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___RightTextureTa_27)); }
	inline TweenAlpha_t2920325587 * get_RightTextureTa_27() const { return ___RightTextureTa_27; }
	inline TweenAlpha_t2920325587 ** get_address_of_RightTextureTa_27() { return &___RightTextureTa_27; }
	inline void set_RightTextureTa_27(TweenAlpha_t2920325587 * value)
	{
		___RightTextureTa_27 = value;
		Il2CppCodeGenWriteBarrier(&___RightTextureTa_27, value);
	}

	inline static int32_t get_offset_of_ArrowTa_28() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___ArrowTa_28)); }
	inline TweenAlpha_t2920325587 * get_ArrowTa_28() const { return ___ArrowTa_28; }
	inline TweenAlpha_t2920325587 ** get_address_of_ArrowTa_28() { return &___ArrowTa_28; }
	inline void set_ArrowTa_28(TweenAlpha_t2920325587 * value)
	{
		___ArrowTa_28 = value;
		Il2CppCodeGenWriteBarrier(&___ArrowTa_28, value);
	}

	inline static int32_t get_offset_of_Tile01Ta_29() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___Tile01Ta_29)); }
	inline TweenAlpha_t2920325587 * get_Tile01Ta_29() const { return ___Tile01Ta_29; }
	inline TweenAlpha_t2920325587 ** get_address_of_Tile01Ta_29() { return &___Tile01Ta_29; }
	inline void set_Tile01Ta_29(TweenAlpha_t2920325587 * value)
	{
		___Tile01Ta_29 = value;
		Il2CppCodeGenWriteBarrier(&___Tile01Ta_29, value);
	}

	inline static int32_t get_offset_of_Tile02Ta_30() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___Tile02Ta_30)); }
	inline TweenAlpha_t2920325587 * get_Tile02Ta_30() const { return ___Tile02Ta_30; }
	inline TweenAlpha_t2920325587 ** get_address_of_Tile02Ta_30() { return &___Tile02Ta_30; }
	inline void set_Tile02Ta_30(TweenAlpha_t2920325587 * value)
	{
		___Tile02Ta_30 = value;
		Il2CppCodeGenWriteBarrier(&___Tile02Ta_30, value);
	}

	inline static int32_t get_offset_of_LeftValueTp_31() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___LeftValueTp_31)); }
	inline TweenPositionU5BU5D_t2332880541* get_LeftValueTp_31() const { return ___LeftValueTp_31; }
	inline TweenPositionU5BU5D_t2332880541** get_address_of_LeftValueTp_31() { return &___LeftValueTp_31; }
	inline void set_LeftValueTp_31(TweenPositionU5BU5D_t2332880541* value)
	{
		___LeftValueTp_31 = value;
		Il2CppCodeGenWriteBarrier(&___LeftValueTp_31, value);
	}

	inline static int32_t get_offset_of_LeftValueTa_32() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___LeftValueTa_32)); }
	inline TweenAlphaU5BU5D_t2326787458* get_LeftValueTa_32() const { return ___LeftValueTa_32; }
	inline TweenAlphaU5BU5D_t2326787458** get_address_of_LeftValueTa_32() { return &___LeftValueTa_32; }
	inline void set_LeftValueTa_32(TweenAlphaU5BU5D_t2326787458* value)
	{
		___LeftValueTa_32 = value;
		Il2CppCodeGenWriteBarrier(&___LeftValueTa_32, value);
	}

	inline static int32_t get_offset_of_ArrowLeftTp_33() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___ArrowLeftTp_33)); }
	inline TweenPositionU5BU5D_t2332880541* get_ArrowLeftTp_33() const { return ___ArrowLeftTp_33; }
	inline TweenPositionU5BU5D_t2332880541** get_address_of_ArrowLeftTp_33() { return &___ArrowLeftTp_33; }
	inline void set_ArrowLeftTp_33(TweenPositionU5BU5D_t2332880541* value)
	{
		___ArrowLeftTp_33 = value;
		Il2CppCodeGenWriteBarrier(&___ArrowLeftTp_33, value);
	}

	inline static int32_t get_offset_of_ArrowLeftTa_34() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___ArrowLeftTa_34)); }
	inline TweenAlphaU5BU5D_t2326787458* get_ArrowLeftTa_34() const { return ___ArrowLeftTa_34; }
	inline TweenAlphaU5BU5D_t2326787458** get_address_of_ArrowLeftTa_34() { return &___ArrowLeftTa_34; }
	inline void set_ArrowLeftTa_34(TweenAlphaU5BU5D_t2326787458* value)
	{
		___ArrowLeftTa_34 = value;
		Il2CppCodeGenWriteBarrier(&___ArrowLeftTa_34, value);
	}

	inline static int32_t get_offset_of_RightValueTp_35() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___RightValueTp_35)); }
	inline TweenPositionU5BU5D_t2332880541* get_RightValueTp_35() const { return ___RightValueTp_35; }
	inline TweenPositionU5BU5D_t2332880541** get_address_of_RightValueTp_35() { return &___RightValueTp_35; }
	inline void set_RightValueTp_35(TweenPositionU5BU5D_t2332880541* value)
	{
		___RightValueTp_35 = value;
		Il2CppCodeGenWriteBarrier(&___RightValueTp_35, value);
	}

	inline static int32_t get_offset_of_RightValueTa_36() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___RightValueTa_36)); }
	inline TweenAlphaU5BU5D_t2326787458* get_RightValueTa_36() const { return ___RightValueTa_36; }
	inline TweenAlphaU5BU5D_t2326787458** get_address_of_RightValueTa_36() { return &___RightValueTa_36; }
	inline void set_RightValueTa_36(TweenAlphaU5BU5D_t2326787458* value)
	{
		___RightValueTa_36 = value;
		Il2CppCodeGenWriteBarrier(&___RightValueTa_36, value);
	}

	inline static int32_t get_offset_of_ArrowUpTa_37() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___ArrowUpTa_37)); }
	inline TweenAlpha_t2920325587 * get_ArrowUpTa_37() const { return ___ArrowUpTa_37; }
	inline TweenAlpha_t2920325587 ** get_address_of_ArrowUpTa_37() { return &___ArrowUpTa_37; }
	inline void set_ArrowUpTa_37(TweenAlpha_t2920325587 * value)
	{
		___ArrowUpTa_37 = value;
		Il2CppCodeGenWriteBarrier(&___ArrowUpTa_37, value);
	}

	inline static int32_t get_offset_of_ArrowUpTp_38() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___ArrowUpTp_38)); }
	inline TweenPosition_t3684358292 * get_ArrowUpTp_38() const { return ___ArrowUpTp_38; }
	inline TweenPosition_t3684358292 ** get_address_of_ArrowUpTp_38() { return &___ArrowUpTp_38; }
	inline void set_ArrowUpTp_38(TweenPosition_t3684358292 * value)
	{
		___ArrowUpTp_38 = value;
		Il2CppCodeGenWriteBarrier(&___ArrowUpTp_38, value);
	}

	inline static int32_t get_offset_of_AddValueTa_39() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___AddValueTa_39)); }
	inline TweenAlpha_t2920325587 * get_AddValueTa_39() const { return ___AddValueTa_39; }
	inline TweenAlpha_t2920325587 ** get_address_of_AddValueTa_39() { return &___AddValueTa_39; }
	inline void set_AddValueTa_39(TweenAlpha_t2920325587 * value)
	{
		___AddValueTa_39 = value;
		Il2CppCodeGenWriteBarrier(&___AddValueTa_39, value);
	}

	inline static int32_t get_offset_of_AddValueTp_40() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___AddValueTp_40)); }
	inline TweenPosition_t3684358292 * get_AddValueTp_40() const { return ___AddValueTp_40; }
	inline TweenPosition_t3684358292 ** get_address_of_AddValueTp_40() { return &___AddValueTp_40; }
	inline void set_AddValueTp_40(TweenPosition_t3684358292 * value)
	{
		___AddValueTp_40 = value;
		Il2CppCodeGenWriteBarrier(&___AddValueTp_40, value);
	}

	inline static int32_t get_offset_of_ArrowUp_tr_41() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___ArrowUp_tr_41)); }
	inline Transform_t1659122786 * get_ArrowUp_tr_41() const { return ___ArrowUp_tr_41; }
	inline Transform_t1659122786 ** get_address_of_ArrowUp_tr_41() { return &___ArrowUp_tr_41; }
	inline void set_ArrowUp_tr_41(Transform_t1659122786 * value)
	{
		___ArrowUp_tr_41 = value;
		Il2CppCodeGenWriteBarrier(&___ArrowUp_tr_41, value);
	}

	inline static int32_t get_offset_of_masterLv_42() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___masterLv_42)); }
	inline int32_t get_masterLv_42() const { return ___masterLv_42; }
	inline int32_t* get_address_of_masterLv_42() { return &___masterLv_42; }
	inline void set_masterLv_42(int32_t value)
	{
		___masterLv_42 = value;
	}

	inline static int32_t get_offset_of_isCanClose_43() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___isCanClose_43)); }
	inline bool get_isCanClose_43() const { return ___isCanClose_43; }
	inline bool* get_address_of_isCanClose_43() { return &___isCanClose_43; }
	inline void set_isCanClose_43(bool value)
	{
		___isCanClose_43 = value;
	}

	inline static int32_t get_offset_of_delayID_44() { return static_cast<int32_t>(offsetof(Float_equip_streng_master_t3807538562, ___delayID_44)); }
	inline uint32_t get_delayID_44() const { return ___delayID_44; }
	inline uint32_t* get_address_of_delayID_44() { return &___delayID_44; }
	inline void set_delayID_44(uint32_t value)
	{
		___delayID_44 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
