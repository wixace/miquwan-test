﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CombatEntity
struct CombatEntity_t684137495;
// System.Collections.Generic.Dictionary`2<CombatEntity,HatredCtrl/stValue>
struct Dictionary_2_t3758671658;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HatredCtrl
struct  HatredCtrl_t891253697  : public Il2CppObject
{
public:
	// System.Single HatredCtrl::_curTime
	float ____curTime_4;
	// CombatEntity HatredCtrl::_entity
	CombatEntity_t684137495 * ____entity_5;
	// System.Collections.Generic.Dictionary`2<CombatEntity,HatredCtrl/stValue> HatredCtrl::hatredDic
	Dictionary_2_t3758671658 * ___hatredDic_6;

public:
	inline static int32_t get_offset_of__curTime_4() { return static_cast<int32_t>(offsetof(HatredCtrl_t891253697, ____curTime_4)); }
	inline float get__curTime_4() const { return ____curTime_4; }
	inline float* get_address_of__curTime_4() { return &____curTime_4; }
	inline void set__curTime_4(float value)
	{
		____curTime_4 = value;
	}

	inline static int32_t get_offset_of__entity_5() { return static_cast<int32_t>(offsetof(HatredCtrl_t891253697, ____entity_5)); }
	inline CombatEntity_t684137495 * get__entity_5() const { return ____entity_5; }
	inline CombatEntity_t684137495 ** get_address_of__entity_5() { return &____entity_5; }
	inline void set__entity_5(CombatEntity_t684137495 * value)
	{
		____entity_5 = value;
		Il2CppCodeGenWriteBarrier(&____entity_5, value);
	}

	inline static int32_t get_offset_of_hatredDic_6() { return static_cast<int32_t>(offsetof(HatredCtrl_t891253697, ___hatredDic_6)); }
	inline Dictionary_2_t3758671658 * get_hatredDic_6() const { return ___hatredDic_6; }
	inline Dictionary_2_t3758671658 ** get_address_of_hatredDic_6() { return &___hatredDic_6; }
	inline void set_hatredDic_6(Dictionary_2_t3758671658 * value)
	{
		___hatredDic_6 = value;
		Il2CppCodeGenWriteBarrier(&___hatredDic_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
