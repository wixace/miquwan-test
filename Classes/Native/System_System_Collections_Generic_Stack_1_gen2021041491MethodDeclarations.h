﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen2974409999MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<Mihua.Utils.WaitForSeconds>::.ctor()
#define Stack_1__ctor_m2439292494(__this, method) ((  void (*) (Stack_1_t2021041491 *, const MethodInfo*))Stack_1__ctor_m2725689112_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Mihua.Utils.WaitForSeconds>::.ctor(System.Int32)
#define Stack_1__ctor_m1410684575(__this, ___count0, method) ((  void (*) (Stack_1_t2021041491 *, int32_t, const MethodInfo*))Stack_1__ctor_m3186788457_gshared)(__this, ___count0, method)
// System.Boolean System.Collections.Generic.Stack`1<Mihua.Utils.WaitForSeconds>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m1189028572(__this, method) ((  bool (*) (Stack_1_t2021041491 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m1582336274_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<Mihua.Utils.WaitForSeconds>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m3967947164(__this, method) ((  Il2CppObject * (*) (Stack_1_t2021041491 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m2938343088_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Mihua.Utils.WaitForSeconds>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m262423222(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t2021041491 *, Il2CppArray *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m3277353260_gshared)(__this, ___dest0, ___idx1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<Mihua.Utils.WaitForSeconds>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m1143007520(__this, method) ((  Il2CppObject* (*) (Stack_1_t2021041491 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m625377314_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<Mihua.Utils.WaitForSeconds>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m2546560325(__this, method) ((  Il2CppObject * (*) (Stack_1_t2021041491 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m4095051687_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Mihua.Utils.WaitForSeconds>::Clear()
#define Stack_1_Clear_m4140393081(__this, method) ((  void (*) (Stack_1_t2021041491 *, const MethodInfo*))Stack_1_Clear_m131822403_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<Mihua.Utils.WaitForSeconds>::Contains(T)
#define Stack_1_Contains_m3087992979(__this, ___t0, method) ((  bool (*) (Stack_1_t2021041491 *, WaitForSeconds_t3217447863 *, const MethodInfo*))Stack_1_Contains_m328948937_gshared)(__this, ___t0, method)
// T System.Collections.Generic.Stack`1<Mihua.Utils.WaitForSeconds>::Peek()
#define Stack_1_Peek_m1701088976(__this, method) ((  WaitForSeconds_t3217447863 * (*) (Stack_1_t2021041491 *, const MethodInfo*))Stack_1_Peek_m3418768488_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<Mihua.Utils.WaitForSeconds>::Pop()
#define Stack_1_Pop_m470822238(__this, method) ((  WaitForSeconds_t3217447863 * (*) (Stack_1_t2021041491 *, const MethodInfo*))Stack_1_Pop_m4267009222_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Mihua.Utils.WaitForSeconds>::Push(T)
#define Stack_1_Push_m3063769486(__this, ___t0, method) ((  void (*) (Stack_1_t2021041491 *, WaitForSeconds_t3217447863 *, const MethodInfo*))Stack_1_Push_m3350166104_gshared)(__this, ___t0, method)
// System.Int32 System.Collections.Generic.Stack`1<Mihua.Utils.WaitForSeconds>::get_Count()
#define Stack_1_get_Count_m1828569378(__this, method) ((  int32_t (*) (Stack_1_t2021041491 *, const MethodInfo*))Stack_1_get_Count_m3631765324_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<Mihua.Utils.WaitForSeconds>::GetEnumerator()
#define Stack_1_GetEnumerator_m2015124932(__this, method) ((  Enumerator_t1578827517  (*) (Stack_1_t2021041491 *, const MethodInfo*))Stack_1_GetEnumerator_m202302354_gshared)(__this, method)
