﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlowControl.FlowLoadCFG
struct FlowLoadCFG_t2572974950;
// FlowControl.FlowBase
struct FlowBase_t3680091731;
// ConfigAssetMgr
struct ConfigAssetMgr_t4036193930;
// LoadingBoardMgr
struct LoadingBoardMgr_t303632014;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FlowControl_FlowBase3680091731.h"
#include "AssemblyU2DCSharp_ConfigAssetMgr4036193930.h"
#include "AssemblyU2DCSharp_LoadingBoardMgr303632014.h"
#include "mscorlib_System_String7231557.h"

// System.Void FlowControl.FlowLoadCFG::.ctor()
extern "C"  void FlowLoadCFG__ctor_m991374682 (FlowLoadCFG_t2572974950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowLoadCFG::Activate()
extern "C"  void FlowLoadCFG_Activate_m1548670045 (FlowLoadCFG_t2572974950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowLoadCFG::FlowUpdate(System.Single)
extern "C"  void FlowLoadCFG_FlowUpdate_m2779322250 (FlowLoadCFG_t2572974950 * __this, float ___duringTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowLoadCFG::OnComplete()
extern "C"  void FlowLoadCFG_OnComplete_m1913745442 (FlowLoadCFG_t2572974950 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowLoadCFG::ilo_Activate1(FlowControl.FlowBase)
extern "C"  void FlowLoadCFG_ilo_Activate1_m1533468709 (Il2CppObject * __this /* static, unused */, FlowBase_t3680091731 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FlowControl.FlowLoadCFG::ilo_get_isLoaded2(ConfigAssetMgr)
extern "C"  bool FlowLoadCFG_ilo_get_isLoaded2_m128915337 (Il2CppObject * __this /* static, unused */, ConfigAssetMgr_t4036193930 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowLoadCFG::ilo_set_isComplete3(FlowControl.FlowBase,System.Boolean)
extern "C"  void FlowLoadCFG_ilo_set_isComplete3_m833265315 (Il2CppObject * __this /* static, unused */, FlowBase_t3680091731 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LoadingBoardMgr FlowControl.FlowLoadCFG::ilo_get_Instance4()
extern "C"  LoadingBoardMgr_t303632014 * FlowLoadCFG_ilo_get_Instance4_m2113083968 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowLoadCFG::ilo_SetPercent5(LoadingBoardMgr,System.Single,System.String)
extern "C"  void FlowLoadCFG_ilo_SetPercent5_m1804917166 (Il2CppObject * __this /* static, unused */, LoadingBoardMgr_t303632014 * ____this0, float ___value1, String_t* ___msg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
