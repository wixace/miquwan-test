﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CallJSApi
struct CallJSApi_t937490515;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void CallJSApi::.ctor()
extern "C"  void CallJSApi__ctor_m2057357944 (CallJSApi_t937490515 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CallJSApi::.cctor()
extern "C"  void CallJSApi__cctor_m3166457909 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CallJSApi::CallJsFun(System.String,System.Object[])
extern "C"  void CallJSApi_CallJsFun_m1264944480 (Il2CppObject * __this /* static, unused */, String_t* ___funName0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CallJSApi::CallLoadJsObjToCs()
extern "C"  void CallJSApi_CallLoadJsObjToCs_m743480107 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CallJSApi::ilo_CallJSFunctionName1(JSVCall,System.Int32,System.String,System.Object[])
extern "C"  bool CallJSApi_ilo_CallJSFunctionName1_m3830427094 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ____this0, int32_t ___jsObjID1, String_t* ___funName2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
