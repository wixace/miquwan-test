﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// CaptureScreenshotMgr
struct CaptureScreenshotMgr_t3951887692;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CaptureScreenshotMgr/<CutImage>c__Iterator31
struct  U3CCutImageU3Ec__Iterator31_t1349992387  : public Il2CppObject
{
public:
	// UnityEngine.Texture2D CaptureScreenshotMgr/<CutImage>c__Iterator31::<tex>__0
	Texture2D_t3884108195 * ___U3CtexU3E__0_0;
	// System.Byte[] CaptureScreenshotMgr/<CutImage>c__Iterator31::<byt>__1
	ByteU5BU5D_t4260760469* ___U3CbytU3E__1_1;
	// System.String CaptureScreenshotMgr/<CutImage>c__Iterator31::name
	String_t* ___name_2;
	// System.Int32 CaptureScreenshotMgr/<CutImage>c__Iterator31::$PC
	int32_t ___U24PC_3;
	// System.Object CaptureScreenshotMgr/<CutImage>c__Iterator31::$current
	Il2CppObject * ___U24current_4;
	// System.String CaptureScreenshotMgr/<CutImage>c__Iterator31::<$>name
	String_t* ___U3CU24U3Ename_5;
	// CaptureScreenshotMgr CaptureScreenshotMgr/<CutImage>c__Iterator31::<>f__this
	CaptureScreenshotMgr_t3951887692 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_U3CtexU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCutImageU3Ec__Iterator31_t1349992387, ___U3CtexU3E__0_0)); }
	inline Texture2D_t3884108195 * get_U3CtexU3E__0_0() const { return ___U3CtexU3E__0_0; }
	inline Texture2D_t3884108195 ** get_address_of_U3CtexU3E__0_0() { return &___U3CtexU3E__0_0; }
	inline void set_U3CtexU3E__0_0(Texture2D_t3884108195 * value)
	{
		___U3CtexU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CtexU3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CbytU3E__1_1() { return static_cast<int32_t>(offsetof(U3CCutImageU3Ec__Iterator31_t1349992387, ___U3CbytU3E__1_1)); }
	inline ByteU5BU5D_t4260760469* get_U3CbytU3E__1_1() const { return ___U3CbytU3E__1_1; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CbytU3E__1_1() { return &___U3CbytU3E__1_1; }
	inline void set_U3CbytU3E__1_1(ByteU5BU5D_t4260760469* value)
	{
		___U3CbytU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbytU3E__1_1, value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(U3CCutImageU3Ec__Iterator31_t1349992387, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCutImageU3Ec__Iterator31_t1349992387, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCutImageU3Ec__Iterator31_t1349992387, ___U24current_4)); }
	inline Il2CppObject * get_U24current_4() const { return ___U24current_4; }
	inline Il2CppObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(Il2CppObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_4, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ename_5() { return static_cast<int32_t>(offsetof(U3CCutImageU3Ec__Iterator31_t1349992387, ___U3CU24U3Ename_5)); }
	inline String_t* get_U3CU24U3Ename_5() const { return ___U3CU24U3Ename_5; }
	inline String_t** get_address_of_U3CU24U3Ename_5() { return &___U3CU24U3Ename_5; }
	inline void set_U3CU24U3Ename_5(String_t* value)
	{
		___U3CU24U3Ename_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ename_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CCutImageU3Ec__Iterator31_t1349992387, ___U3CU3Ef__this_6)); }
	inline CaptureScreenshotMgr_t3951887692 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline CaptureScreenshotMgr_t3951887692 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(CaptureScreenshotMgr_t3951887692 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
