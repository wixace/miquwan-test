﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua_Assets_AssetUtilGenerated
struct Mihua_Assets_AssetUtilGenerated_t2931096850;
// JSVCall
struct JSVCall_t3708497963;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;
// Mihua.Assets.AssetUtil
struct AssetUtil_t30958784;
// System.Collections.Generic.List`1<FileInfoRes>
struct List_1_t2585245350;
// System.IO.MemoryStream
struct MemoryStream_t418716369;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_Assets_AssetUtil30958784.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void Mihua_Assets_AssetUtilGenerated::.ctor()
extern "C"  void Mihua_Assets_AssetUtilGenerated__ctor_m1402900121 (Mihua_Assets_AssetUtilGenerated_t2931096850 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_AssetUtilGenerated::AssetUtil_NETWORK_CHANGE(JSVCall)
extern "C"  void Mihua_Assets_AssetUtilGenerated_AssetUtil_NETWORK_CHANGE_m3749005486 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_AssetUtilGenerated::AssetUtil_IsWifiNetwork(JSVCall)
extern "C"  void Mihua_Assets_AssetUtilGenerated_AssetUtil_IsWifiNetwork_m4033864334 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Assets_AssetUtilGenerated::AssetUtil_ZUpdate(JSVCall,System.Int32)
extern "C"  bool Mihua_Assets_AssetUtilGenerated_AssetUtil_ZUpdate_m484334481 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Assets_AssetUtilGenerated::AssetUtil_Exists__String(JSVCall,System.Int32)
extern "C"  bool Mihua_Assets_AssetUtilGenerated_AssetUtil_Exists__String_m1701749313 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Assets_AssetUtilGenerated::AssetUtil_GetNeedLoadList(JSVCall,System.Int32)
extern "C"  bool Mihua_Assets_AssetUtilGenerated_AssetUtil_GetNeedLoadList_m3846405310 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Assets_AssetUtilGenerated::AssetUtil_LoadDataFromStreamPath__String(JSVCall,System.Int32)
extern "C"  bool Mihua_Assets_AssetUtilGenerated_AssetUtil_LoadDataFromStreamPath__String_m3635834244 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Assets_AssetUtilGenerated::AssetUtil_SaveCacheBundleFile__String__Byte_Array(JSVCall,System.Int32)
extern "C"  bool Mihua_Assets_AssetUtilGenerated_AssetUtil_SaveCacheBundleFile__String__Byte_Array_m3040809924 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua_Assets_AssetUtilGenerated::AssetUtil_SetAssetCfg__FilelistT1_FileInfoRes(JSVCall,System.Int32)
extern "C"  bool Mihua_Assets_AssetUtilGenerated_AssetUtil_SetAssetCfg__FilelistT1_FileInfoRes_m1159802066 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_AssetUtilGenerated::__Register()
extern "C"  void Mihua_Assets_AssetUtilGenerated___Register_m4194379214 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mihua_Assets_AssetUtilGenerated::<AssetUtil_SaveCacheBundleFile__String__Byte_Array>m__7E()
extern "C"  ByteU5BU5D_t4260760469* Mihua_Assets_AssetUtilGenerated_U3CAssetUtil_SaveCacheBundleFile__String__Byte_ArrayU3Em__7E_m810804587 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_AssetUtilGenerated::ilo_setStringS1(System.Int32,System.String)
extern "C"  void Mihua_Assets_AssetUtilGenerated_ilo_setStringS1_m2436188386 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_AssetUtilGenerated::ilo_ZUpdate2(Mihua.Assets.AssetUtil)
extern "C"  void Mihua_Assets_AssetUtilGenerated_ilo_ZUpdate2_m2706284464 (Il2CppObject * __this /* static, unused */, AssetUtil_t30958784 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua_Assets_AssetUtilGenerated::ilo_getStringS3(System.Int32)
extern "C"  String_t* Mihua_Assets_AssetUtilGenerated_ilo_getStringS3_m3369345663 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<FileInfoRes> Mihua_Assets_AssetUtilGenerated::ilo_GetNeedLoadList4()
extern "C"  List_1_t2585245350 * Mihua_Assets_AssetUtilGenerated_ilo_GetNeedLoadList4_m1120941355 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IO.MemoryStream Mihua_Assets_AssetUtilGenerated::ilo_LoadDataFromStreamPath5(System.String)
extern "C"  MemoryStream_t418716369 * Mihua_Assets_AssetUtilGenerated_ilo_LoadDataFromStreamPath5_m3727812149 (Il2CppObject * __this /* static, unused */, String_t* ___fileName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua_Assets_AssetUtilGenerated::ilo_SaveCacheBundleFile6(System.String,System.Byte[])
extern "C"  void Mihua_Assets_AssetUtilGenerated_ilo_SaveCacheBundleFile6_m370621588 (Il2CppObject * __this /* static, unused */, String_t* ___assetBundleName0, ByteU5BU5D_t4260760469* ___p11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mihua_Assets_AssetUtilGenerated::ilo_getObject7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * Mihua_Assets_AssetUtilGenerated_ilo_getObject7_m2596743986 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Mihua_Assets_AssetUtilGenerated::ilo_getElement8(System.Int32,System.Int32)
extern "C"  int32_t Mihua_Assets_AssetUtilGenerated_ilo_getElement8_m783758946 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Mihua_Assets_AssetUtilGenerated::ilo_getByte9(System.Int32)
extern "C"  uint8_t Mihua_Assets_AssetUtilGenerated_ilo_getByte9_m3064332510 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
