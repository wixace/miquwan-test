﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_SkeletonBoneGenerated
struct UnityEngine_SkeletonBoneGenerated_t1799855990;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_SkeletonBoneGenerated::.ctor()
extern "C"  void UnityEngine_SkeletonBoneGenerated__ctor_m783434421 (UnityEngine_SkeletonBoneGenerated_t1799855990 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkeletonBoneGenerated::.cctor()
extern "C"  void UnityEngine_SkeletonBoneGenerated__cctor_m2329534360 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SkeletonBoneGenerated::SkeletonBone_SkeletonBone1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SkeletonBoneGenerated_SkeletonBone_SkeletonBone1_m4108698685 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkeletonBoneGenerated::SkeletonBone_name(JSVCall)
extern "C"  void UnityEngine_SkeletonBoneGenerated_SkeletonBone_name_m4108671035 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkeletonBoneGenerated::SkeletonBone_position(JSVCall)
extern "C"  void UnityEngine_SkeletonBoneGenerated_SkeletonBone_position_m370432861 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkeletonBoneGenerated::SkeletonBone_rotation(JSVCall)
extern "C"  void UnityEngine_SkeletonBoneGenerated_SkeletonBone_rotation_m39138472 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkeletonBoneGenerated::SkeletonBone_scale(JSVCall)
extern "C"  void UnityEngine_SkeletonBoneGenerated_SkeletonBone_scale_m26876572 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkeletonBoneGenerated::SkeletonBone_transformModified(JSVCall)
extern "C"  void UnityEngine_SkeletonBoneGenerated_SkeletonBone_transformModified_m4175251953 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkeletonBoneGenerated::__Register()
extern "C"  void UnityEngine_SkeletonBoneGenerated___Register_m1793434418 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_SkeletonBoneGenerated::ilo_getStringS1(System.Int32)
extern "C"  String_t* UnityEngine_SkeletonBoneGenerated_ilo_getStringS1_m3347486809 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkeletonBoneGenerated::ilo_changeJSObj2(System.Int32,System.Object)
extern "C"  void UnityEngine_SkeletonBoneGenerated_ilo_changeJSObj2_m1303561301 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkeletonBoneGenerated::ilo_setVector3S3(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_SkeletonBoneGenerated_ilo_setVector3S3_m2450813940 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_SkeletonBoneGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_SkeletonBoneGenerated_ilo_getObject4_m3387956435 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_SkeletonBoneGenerated::ilo_getVector3S5(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_SkeletonBoneGenerated_ilo_getVector3S5_m253346229 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
