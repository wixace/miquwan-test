﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIButtonKeys
struct UIButtonKeys_t2405408250;
// UIKeyNavigation
struct UIKeyNavigation_t1837256607;
// UnityEngine.Object
struct Object_t3071478659;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIButtonKeys2405408250.h"
#include "AssemblyU2DCSharp_UIKeyNavigation1837256607.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"

// System.Void UIButtonKeys::.ctor()
extern "C"  void UIButtonKeys__ctor_m1835101665 (UIButtonKeys_t2405408250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonKeys::OnEnable()
extern "C"  void UIButtonKeys_OnEnable_m946569733 (UIButtonKeys_t2405408250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonKeys::Upgrade()
extern "C"  void UIButtonKeys_Upgrade_m3425947291 (UIButtonKeys_t2405408250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonKeys::ilo_Upgrade1(UIButtonKeys)
extern "C"  void UIButtonKeys_ilo_Upgrade1_m2172886609 (Il2CppObject * __this /* static, unused */, UIButtonKeys_t2405408250 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonKeys::ilo_OnEnable2(UIKeyNavigation)
extern "C"  void UIButtonKeys_ilo_OnEnable2_m3467265815 (Il2CppObject * __this /* static, unused */, UIKeyNavigation_t1837256607 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonKeys::ilo_SetDirty3(UnityEngine.Object)
extern "C"  void UIButtonKeys_ilo_SetDirty3_m4035734489 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
