﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AsyncOperationGenerated
struct UnityEngine_AsyncOperationGenerated_t2311094556;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_AsyncOperationGenerated::.ctor()
extern "C"  void UnityEngine_AsyncOperationGenerated__ctor_m2953069007 (UnityEngine_AsyncOperationGenerated_t2311094556 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AsyncOperationGenerated::AsyncOperation_AsyncOperation1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AsyncOperationGenerated_AsyncOperation_AsyncOperation1_m426580515 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AsyncOperationGenerated::AsyncOperation_isDone(JSVCall)
extern "C"  void UnityEngine_AsyncOperationGenerated_AsyncOperation_isDone_m3893396954 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AsyncOperationGenerated::AsyncOperation_progress(JSVCall)
extern "C"  void UnityEngine_AsyncOperationGenerated_AsyncOperation_progress_m1847703705 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AsyncOperationGenerated::AsyncOperation_priority(JSVCall)
extern "C"  void UnityEngine_AsyncOperationGenerated_AsyncOperation_priority_m3478091522 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AsyncOperationGenerated::AsyncOperation_allowSceneActivation(JSVCall)
extern "C"  void UnityEngine_AsyncOperationGenerated_AsyncOperation_allowSceneActivation_m1555982349 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AsyncOperationGenerated::__Register()
extern "C"  void UnityEngine_AsyncOperationGenerated___Register_m1683555416 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AsyncOperationGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_AsyncOperationGenerated_ilo_attachFinalizerObject1_m286489608 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AsyncOperationGenerated::ilo_setInt322(System.Int32,System.Int32)
extern "C"  void UnityEngine_AsyncOperationGenerated_ilo_setInt322_m175640390 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
