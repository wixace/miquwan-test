﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FloatTextMgr
struct FloatTextMgr_t630384591;
// FloatTextUnit
struct FloatTextUnit_t2362298029;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;
// System.String
struct String_t;
// Mihua.Asset.OnLoadAsset
struct OnLoadAsset_t4181543125;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_Asset_OnLoadAsset4181543125.h"
#include "AssemblyU2DCSharp_FloatTextMgr630384591.h"

// System.Void FloatTextMgr::.ctor()
extern "C"  void FloatTextMgr__ctor_m1391158188 (FloatTextMgr_t630384591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextUnit FloatTextMgr::GetFloatTextUnit(FLOAT_TEXT_ID)
extern "C"  FloatTextUnit_t2362298029 * FloatTextMgr_GetFloatTextUnit_m2556955635 (FloatTextMgr_t630384591 * __this, int32_t ___textId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextMgr::FloatText(FLOAT_TEXT_ID,UnityEngine.Vector3,System.Object[])
extern "C"  void FloatTextMgr_FloatText_m2467938100 (FloatTextMgr_t630384591 * __this, int32_t ___textId0, Vector3_t4282066566  ___offsetPos1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextMgr::FloatText(FLOAT_TEXT_ID,System.Int32,System.Object[])
extern "C"  void FloatTextMgr_FloatText_m1487838638 (FloatTextMgr_t630384591 * __this, int32_t ___textId0, int32_t ___offsetY1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextMgr::ClearTeamTips()
extern "C"  void FloatTextMgr_ClearTeamTips_m1804623852 (FloatTextMgr_t630384591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextMgr::Clear()
extern "C"  void FloatTextMgr_Clear_m3092258775 (FloatTextMgr_t630384591 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.Asset.ABLoadOperation.AssetOperation FloatTextMgr::ilo_LoadAssetAsync1(System.String,Mihua.Asset.OnLoadAsset)
extern "C"  AssetOperation_t778728221 * FloatTextMgr_ilo_LoadAssetAsync1_m689552869 (Il2CppObject * __this /* static, unused */, String_t* ___assetName0, OnLoadAsset_t4181543125 * ___onLoadAsset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTextMgr::ilo_FloatText2(FloatTextMgr,FLOAT_TEXT_ID,UnityEngine.Vector3,System.Object[])
extern "C"  void FloatTextMgr_ilo_FloatText2_m4108209030 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, int32_t ___textId1, Vector3_t4282066566  ___offsetPos2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
