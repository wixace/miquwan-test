﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Mihua.Utils.ObjectPool`1<System.Object>::.cctor()
extern "C"  void ObjectPool_1__cctor_m2632395647_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ObjectPool_1__cctor_m2632395647(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1__cctor_m2632395647_gshared)(__this /* static, unused */, method)
// System.Void Mihua.Utils.ObjectPool`1<System.Object>::Recycle(T)
extern "C"  void ObjectPool_1_Recycle_m1987265311_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method);
#define ObjectPool_1_Recycle_m1987265311(__this /* static, unused */, ___obj0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))ObjectPool_1_Recycle_m1987265311_gshared)(__this /* static, unused */, ___obj0, method)
// System.Int32 Mihua.Utils.ObjectPool`1<System.Object>::get_maxCount()
extern "C"  int32_t ObjectPool_1_get_maxCount_m713746044_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ObjectPool_1_get_maxCount_m713746044(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1_get_maxCount_m713746044_gshared)(__this /* static, unused */, method)
// System.Void Mihua.Utils.ObjectPool`1<System.Object>::set_maxCount(System.Int32)
extern "C"  void ObjectPool_1_set_maxCount_m1492204559_gshared (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method);
#define ObjectPool_1_set_maxCount_m1492204559(__this /* static, unused */, ___value0, method) ((  void (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))ObjectPool_1_set_maxCount_m1492204559_gshared)(__this /* static, unused */, ___value0, method)
// System.Int32 Mihua.Utils.ObjectPool`1<System.Object>::GetTotalCreated()
extern "C"  int32_t ObjectPool_1_GetTotalCreated_m1929750740_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ObjectPool_1_GetTotalCreated_m1929750740(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1_GetTotalCreated_m1929750740_gshared)(__this /* static, unused */, method)
// System.Int32 Mihua.Utils.ObjectPool`1<System.Object>::GetSize()
extern "C"  int32_t ObjectPool_1_GetSize_m2045490577_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ObjectPool_1_GetSize_m2045490577(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1_GetSize_m2045490577_gshared)(__this /* static, unused */, method)
// T Mihua.Utils.ObjectPool`1<System.Object>::GetObj()
extern "C"  Il2CppObject * ObjectPool_1_GetObj_m2492700728_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ObjectPool_1_GetObj_m2492700728(__this /* static, unused */, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1_GetObj_m2492700728_gshared)(__this /* static, unused */, method)
// System.Void Mihua.Utils.ObjectPool`1<System.Object>::Cleanup()
extern "C"  void ObjectPool_1_Cleanup_m433760752_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ObjectPool_1_Cleanup_m433760752(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1_Cleanup_m433760752_gshared)(__this /* static, unused */, method)
