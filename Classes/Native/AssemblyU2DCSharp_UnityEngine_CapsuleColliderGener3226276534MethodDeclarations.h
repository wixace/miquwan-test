﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_CapsuleColliderGenerated
struct UnityEngine_CapsuleColliderGenerated_t3226276534;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_CapsuleColliderGenerated::.ctor()
extern "C"  void UnityEngine_CapsuleColliderGenerated__ctor_m28230821 (UnityEngine_CapsuleColliderGenerated_t3226276534 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CapsuleColliderGenerated::CapsuleCollider_CapsuleCollider1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CapsuleColliderGenerated_CapsuleCollider_CapsuleCollider1_m2130946225 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CapsuleColliderGenerated::CapsuleCollider_center(JSVCall)
extern "C"  void UnityEngine_CapsuleColliderGenerated_CapsuleCollider_center_m2160620693 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CapsuleColliderGenerated::CapsuleCollider_radius(JSVCall)
extern "C"  void UnityEngine_CapsuleColliderGenerated_CapsuleCollider_radius_m3657484536 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CapsuleColliderGenerated::CapsuleCollider_height(JSVCall)
extern "C"  void UnityEngine_CapsuleColliderGenerated_CapsuleCollider_height_m3703438339 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CapsuleColliderGenerated::CapsuleCollider_direction(JSVCall)
extern "C"  void UnityEngine_CapsuleColliderGenerated_CapsuleCollider_direction_m1705138851 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CapsuleColliderGenerated::__Register()
extern "C"  void UnityEngine_CapsuleColliderGenerated___Register_m3491551042 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
