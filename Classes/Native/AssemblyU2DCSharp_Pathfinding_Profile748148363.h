﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Diagnostics.Stopwatch
struct Stopwatch_t3420517611;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Profile
struct  Profile_t748148363  : public Il2CppObject
{
public:
	// System.String Pathfinding.Profile::name
	String_t* ___name_1;
	// System.Diagnostics.Stopwatch Pathfinding.Profile::w
	Stopwatch_t3420517611 * ___w_2;
	// System.Int32 Pathfinding.Profile::counter
	int32_t ___counter_3;
	// System.Int64 Pathfinding.Profile::mem
	int64_t ___mem_4;
	// System.Int64 Pathfinding.Profile::smem
	int64_t ___smem_5;
	// System.Int32 Pathfinding.Profile::control
	int32_t ___control_6;
	// System.Boolean Pathfinding.Profile::dontCountFirst
	bool ___dontCountFirst_7;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(Profile_t748148363, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier(&___name_1, value);
	}

	inline static int32_t get_offset_of_w_2() { return static_cast<int32_t>(offsetof(Profile_t748148363, ___w_2)); }
	inline Stopwatch_t3420517611 * get_w_2() const { return ___w_2; }
	inline Stopwatch_t3420517611 ** get_address_of_w_2() { return &___w_2; }
	inline void set_w_2(Stopwatch_t3420517611 * value)
	{
		___w_2 = value;
		Il2CppCodeGenWriteBarrier(&___w_2, value);
	}

	inline static int32_t get_offset_of_counter_3() { return static_cast<int32_t>(offsetof(Profile_t748148363, ___counter_3)); }
	inline int32_t get_counter_3() const { return ___counter_3; }
	inline int32_t* get_address_of_counter_3() { return &___counter_3; }
	inline void set_counter_3(int32_t value)
	{
		___counter_3 = value;
	}

	inline static int32_t get_offset_of_mem_4() { return static_cast<int32_t>(offsetof(Profile_t748148363, ___mem_4)); }
	inline int64_t get_mem_4() const { return ___mem_4; }
	inline int64_t* get_address_of_mem_4() { return &___mem_4; }
	inline void set_mem_4(int64_t value)
	{
		___mem_4 = value;
	}

	inline static int32_t get_offset_of_smem_5() { return static_cast<int32_t>(offsetof(Profile_t748148363, ___smem_5)); }
	inline int64_t get_smem_5() const { return ___smem_5; }
	inline int64_t* get_address_of_smem_5() { return &___smem_5; }
	inline void set_smem_5(int64_t value)
	{
		___smem_5 = value;
	}

	inline static int32_t get_offset_of_control_6() { return static_cast<int32_t>(offsetof(Profile_t748148363, ___control_6)); }
	inline int32_t get_control_6() const { return ___control_6; }
	inline int32_t* get_address_of_control_6() { return &___control_6; }
	inline void set_control_6(int32_t value)
	{
		___control_6 = value;
	}

	inline static int32_t get_offset_of_dontCountFirst_7() { return static_cast<int32_t>(offsetof(Profile_t748148363, ___dontCountFirst_7)); }
	inline bool get_dontCountFirst_7() const { return ___dontCountFirst_7; }
	inline bool* get_address_of_dontCountFirst_7() { return &___dontCountFirst_7; }
	inline void set_dontCountFirst_7(bool value)
	{
		___dontCountFirst_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
