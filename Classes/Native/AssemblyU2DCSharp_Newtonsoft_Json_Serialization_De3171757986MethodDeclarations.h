﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.DefaultContractResolver/<SetIsSpecifiedActions>c__AnonStorey133
struct U3CSetIsSpecifiedActionsU3Ec__AnonStorey133_t3171757986;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver/<SetIsSpecifiedActions>c__AnonStorey133::.ctor()
extern "C"  void U3CSetIsSpecifiedActionsU3Ec__AnonStorey133__ctor_m4078016521 (U3CSetIsSpecifiedActionsU3Ec__AnonStorey133_t3171757986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver/<SetIsSpecifiedActions>c__AnonStorey133::<>m__388(System.Object)
extern "C"  bool U3CSetIsSpecifiedActionsU3Ec__AnonStorey133_U3CU3Em__388_m2088835301 (U3CSetIsSpecifiedActionsU3Ec__AnonStorey133_t3171757986 * __this, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
