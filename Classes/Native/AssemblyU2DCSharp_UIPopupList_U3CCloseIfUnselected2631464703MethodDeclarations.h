﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIPopupList/<CloseIfUnselected>c__Iterator28
struct U3CCloseIfUnselectedU3Ec__Iterator28_t2631464703;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UIPopupList/<CloseIfUnselected>c__Iterator28::.ctor()
extern "C"  void U3CCloseIfUnselectedU3Ec__Iterator28__ctor_m836569724 (U3CCloseIfUnselectedU3Ec__Iterator28_t2631464703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIPopupList/<CloseIfUnselected>c__Iterator28::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CCloseIfUnselectedU3Ec__Iterator28_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2048941408 (U3CCloseIfUnselectedU3Ec__Iterator28_t2631464703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIPopupList/<CloseIfUnselected>c__Iterator28::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCloseIfUnselectedU3Ec__Iterator28_System_Collections_IEnumerator_get_Current_m3346604276 (U3CCloseIfUnselectedU3Ec__Iterator28_t2631464703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPopupList/<CloseIfUnselected>c__Iterator28::MoveNext()
extern "C"  bool U3CCloseIfUnselectedU3Ec__Iterator28_MoveNext_m2279476192 (U3CCloseIfUnselectedU3Ec__Iterator28_t2631464703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList/<CloseIfUnselected>c__Iterator28::Dispose()
extern "C"  void U3CCloseIfUnselectedU3Ec__Iterator28_Dispose_m682903481 (U3CCloseIfUnselectedU3Ec__Iterator28_t2631464703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPopupList/<CloseIfUnselected>c__Iterator28::Reset()
extern "C"  void U3CCloseIfUnselectedU3Ec__Iterator28_Reset_m2777969961 (U3CCloseIfUnselectedU3Ec__Iterator28_t2631464703 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
