﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Runtime_Serialization_ISurrogateSelectorGenerated
struct System_Runtime_Serialization_ISurrogateSelectorGenerated_t1860555259;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void System_Runtime_Serialization_ISurrogateSelectorGenerated::.ctor()
extern "C"  void System_Runtime_Serialization_ISurrogateSelectorGenerated__ctor_m1006266112 (System_Runtime_Serialization_ISurrogateSelectorGenerated_t1860555259 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Runtime_Serialization_ISurrogateSelectorGenerated::ISurrogateSelector_ChainSelector__ISurrogateSelector(JSVCall,System.Int32)
extern "C"  bool System_Runtime_Serialization_ISurrogateSelectorGenerated_ISurrogateSelector_ChainSelector__ISurrogateSelector_m3261724229 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Runtime_Serialization_ISurrogateSelectorGenerated::ISurrogateSelector_GetNextSelector(JSVCall,System.Int32)
extern "C"  bool System_Runtime_Serialization_ISurrogateSelectorGenerated_ISurrogateSelector_GetNextSelector_m504083327 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Runtime_Serialization_ISurrogateSelectorGenerated::ISurrogateSelector_GetSurrogate__Type__StreamingContext__ISurrogateSelector(JSVCall,System.Int32)
extern "C"  bool System_Runtime_Serialization_ISurrogateSelectorGenerated_ISurrogateSelector_GetSurrogate__Type__StreamingContext__ISurrogateSelector_m3951722306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Runtime_Serialization_ISurrogateSelectorGenerated::__Register()
extern "C"  void System_Runtime_Serialization_ISurrogateSelectorGenerated___Register_m1494185415 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Runtime_Serialization_ISurrogateSelectorGenerated::ilo_getObject1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_Runtime_Serialization_ISurrogateSelectorGenerated_ilo_getObject1_m1342326255 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Runtime_Serialization_ISurrogateSelectorGenerated::ilo_incArgIndex2()
extern "C"  int32_t System_Runtime_Serialization_ISurrogateSelectorGenerated_ilo_incArgIndex2_m2635480829 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Runtime_Serialization_ISurrogateSelectorGenerated::ilo_setArgIndex3(System.Int32)
extern "C"  void System_Runtime_Serialization_ISurrogateSelectorGenerated_ilo_setArgIndex3_m1121643005 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
