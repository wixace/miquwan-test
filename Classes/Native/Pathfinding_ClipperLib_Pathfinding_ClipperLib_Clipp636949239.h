﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Pathfinding.ClipperLib.OutRec>
struct List_1_t319023540;
// Pathfinding.ClipperLib.Scanbeam
struct Scanbeam_t1885114670;
// Pathfinding.ClipperLib.TEdge
struct TEdge_t3950806139;
// Pathfinding.ClipperLib.IntersectNode
struct IntersectNode_t4106323947;
// System.Collections.Generic.List`1<Pathfinding.ClipperLib.Join>
struct List_1_t1043336060;
// System.Action`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>
struct Action_1_t795160571;

#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Clipp512275816.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Clip3693001772.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Poly3967424203.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.ClipperLib.Clipper
struct  Clipper_t636949239  : public ClipperBase_t512275816
{
public:
	// System.Collections.Generic.List`1<Pathfinding.ClipperLib.OutRec> Pathfinding.ClipperLib.Clipper::m_PolyOuts
	List_1_t319023540 * ___m_PolyOuts_15;
	// Pathfinding.ClipperLib.ClipType Pathfinding.ClipperLib.Clipper::m_ClipType
	int32_t ___m_ClipType_16;
	// Pathfinding.ClipperLib.Scanbeam Pathfinding.ClipperLib.Clipper::m_Scanbeam
	Scanbeam_t1885114670 * ___m_Scanbeam_17;
	// Pathfinding.ClipperLib.TEdge Pathfinding.ClipperLib.Clipper::m_ActiveEdges
	TEdge_t3950806139 * ___m_ActiveEdges_18;
	// Pathfinding.ClipperLib.TEdge Pathfinding.ClipperLib.Clipper::m_SortedEdges
	TEdge_t3950806139 * ___m_SortedEdges_19;
	// Pathfinding.ClipperLib.IntersectNode Pathfinding.ClipperLib.Clipper::m_IntersectNodes
	IntersectNode_t4106323947 * ___m_IntersectNodes_20;
	// System.Boolean Pathfinding.ClipperLib.Clipper::m_ExecuteLocked
	bool ___m_ExecuteLocked_21;
	// Pathfinding.ClipperLib.PolyFillType Pathfinding.ClipperLib.Clipper::m_ClipFillType
	int32_t ___m_ClipFillType_22;
	// Pathfinding.ClipperLib.PolyFillType Pathfinding.ClipperLib.Clipper::m_SubjFillType
	int32_t ___m_SubjFillType_23;
	// System.Collections.Generic.List`1<Pathfinding.ClipperLib.Join> Pathfinding.ClipperLib.Clipper::m_Joins
	List_1_t1043336060 * ___m_Joins_24;
	// System.Collections.Generic.List`1<Pathfinding.ClipperLib.Join> Pathfinding.ClipperLib.Clipper::m_GhostJoins
	List_1_t1043336060 * ___m_GhostJoins_25;
	// System.Boolean Pathfinding.ClipperLib.Clipper::m_UsingPolyTree
	bool ___m_UsingPolyTree_26;
	// System.Boolean Pathfinding.ClipperLib.Clipper::<ReverseSolution>k__BackingField
	bool ___U3CReverseSolutionU3Ek__BackingField_27;
	// System.Boolean Pathfinding.ClipperLib.Clipper::<StrictlySimple>k__BackingField
	bool ___U3CStrictlySimpleU3Ek__BackingField_28;

public:
	inline static int32_t get_offset_of_m_PolyOuts_15() { return static_cast<int32_t>(offsetof(Clipper_t636949239, ___m_PolyOuts_15)); }
	inline List_1_t319023540 * get_m_PolyOuts_15() const { return ___m_PolyOuts_15; }
	inline List_1_t319023540 ** get_address_of_m_PolyOuts_15() { return &___m_PolyOuts_15; }
	inline void set_m_PolyOuts_15(List_1_t319023540 * value)
	{
		___m_PolyOuts_15 = value;
		Il2CppCodeGenWriteBarrier(&___m_PolyOuts_15, value);
	}

	inline static int32_t get_offset_of_m_ClipType_16() { return static_cast<int32_t>(offsetof(Clipper_t636949239, ___m_ClipType_16)); }
	inline int32_t get_m_ClipType_16() const { return ___m_ClipType_16; }
	inline int32_t* get_address_of_m_ClipType_16() { return &___m_ClipType_16; }
	inline void set_m_ClipType_16(int32_t value)
	{
		___m_ClipType_16 = value;
	}

	inline static int32_t get_offset_of_m_Scanbeam_17() { return static_cast<int32_t>(offsetof(Clipper_t636949239, ___m_Scanbeam_17)); }
	inline Scanbeam_t1885114670 * get_m_Scanbeam_17() const { return ___m_Scanbeam_17; }
	inline Scanbeam_t1885114670 ** get_address_of_m_Scanbeam_17() { return &___m_Scanbeam_17; }
	inline void set_m_Scanbeam_17(Scanbeam_t1885114670 * value)
	{
		___m_Scanbeam_17 = value;
		Il2CppCodeGenWriteBarrier(&___m_Scanbeam_17, value);
	}

	inline static int32_t get_offset_of_m_ActiveEdges_18() { return static_cast<int32_t>(offsetof(Clipper_t636949239, ___m_ActiveEdges_18)); }
	inline TEdge_t3950806139 * get_m_ActiveEdges_18() const { return ___m_ActiveEdges_18; }
	inline TEdge_t3950806139 ** get_address_of_m_ActiveEdges_18() { return &___m_ActiveEdges_18; }
	inline void set_m_ActiveEdges_18(TEdge_t3950806139 * value)
	{
		___m_ActiveEdges_18 = value;
		Il2CppCodeGenWriteBarrier(&___m_ActiveEdges_18, value);
	}

	inline static int32_t get_offset_of_m_SortedEdges_19() { return static_cast<int32_t>(offsetof(Clipper_t636949239, ___m_SortedEdges_19)); }
	inline TEdge_t3950806139 * get_m_SortedEdges_19() const { return ___m_SortedEdges_19; }
	inline TEdge_t3950806139 ** get_address_of_m_SortedEdges_19() { return &___m_SortedEdges_19; }
	inline void set_m_SortedEdges_19(TEdge_t3950806139 * value)
	{
		___m_SortedEdges_19 = value;
		Il2CppCodeGenWriteBarrier(&___m_SortedEdges_19, value);
	}

	inline static int32_t get_offset_of_m_IntersectNodes_20() { return static_cast<int32_t>(offsetof(Clipper_t636949239, ___m_IntersectNodes_20)); }
	inline IntersectNode_t4106323947 * get_m_IntersectNodes_20() const { return ___m_IntersectNodes_20; }
	inline IntersectNode_t4106323947 ** get_address_of_m_IntersectNodes_20() { return &___m_IntersectNodes_20; }
	inline void set_m_IntersectNodes_20(IntersectNode_t4106323947 * value)
	{
		___m_IntersectNodes_20 = value;
		Il2CppCodeGenWriteBarrier(&___m_IntersectNodes_20, value);
	}

	inline static int32_t get_offset_of_m_ExecuteLocked_21() { return static_cast<int32_t>(offsetof(Clipper_t636949239, ___m_ExecuteLocked_21)); }
	inline bool get_m_ExecuteLocked_21() const { return ___m_ExecuteLocked_21; }
	inline bool* get_address_of_m_ExecuteLocked_21() { return &___m_ExecuteLocked_21; }
	inline void set_m_ExecuteLocked_21(bool value)
	{
		___m_ExecuteLocked_21 = value;
	}

	inline static int32_t get_offset_of_m_ClipFillType_22() { return static_cast<int32_t>(offsetof(Clipper_t636949239, ___m_ClipFillType_22)); }
	inline int32_t get_m_ClipFillType_22() const { return ___m_ClipFillType_22; }
	inline int32_t* get_address_of_m_ClipFillType_22() { return &___m_ClipFillType_22; }
	inline void set_m_ClipFillType_22(int32_t value)
	{
		___m_ClipFillType_22 = value;
	}

	inline static int32_t get_offset_of_m_SubjFillType_23() { return static_cast<int32_t>(offsetof(Clipper_t636949239, ___m_SubjFillType_23)); }
	inline int32_t get_m_SubjFillType_23() const { return ___m_SubjFillType_23; }
	inline int32_t* get_address_of_m_SubjFillType_23() { return &___m_SubjFillType_23; }
	inline void set_m_SubjFillType_23(int32_t value)
	{
		___m_SubjFillType_23 = value;
	}

	inline static int32_t get_offset_of_m_Joins_24() { return static_cast<int32_t>(offsetof(Clipper_t636949239, ___m_Joins_24)); }
	inline List_1_t1043336060 * get_m_Joins_24() const { return ___m_Joins_24; }
	inline List_1_t1043336060 ** get_address_of_m_Joins_24() { return &___m_Joins_24; }
	inline void set_m_Joins_24(List_1_t1043336060 * value)
	{
		___m_Joins_24 = value;
		Il2CppCodeGenWriteBarrier(&___m_Joins_24, value);
	}

	inline static int32_t get_offset_of_m_GhostJoins_25() { return static_cast<int32_t>(offsetof(Clipper_t636949239, ___m_GhostJoins_25)); }
	inline List_1_t1043336060 * get_m_GhostJoins_25() const { return ___m_GhostJoins_25; }
	inline List_1_t1043336060 ** get_address_of_m_GhostJoins_25() { return &___m_GhostJoins_25; }
	inline void set_m_GhostJoins_25(List_1_t1043336060 * value)
	{
		___m_GhostJoins_25 = value;
		Il2CppCodeGenWriteBarrier(&___m_GhostJoins_25, value);
	}

	inline static int32_t get_offset_of_m_UsingPolyTree_26() { return static_cast<int32_t>(offsetof(Clipper_t636949239, ___m_UsingPolyTree_26)); }
	inline bool get_m_UsingPolyTree_26() const { return ___m_UsingPolyTree_26; }
	inline bool* get_address_of_m_UsingPolyTree_26() { return &___m_UsingPolyTree_26; }
	inline void set_m_UsingPolyTree_26(bool value)
	{
		___m_UsingPolyTree_26 = value;
	}

	inline static int32_t get_offset_of_U3CReverseSolutionU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(Clipper_t636949239, ___U3CReverseSolutionU3Ek__BackingField_27)); }
	inline bool get_U3CReverseSolutionU3Ek__BackingField_27() const { return ___U3CReverseSolutionU3Ek__BackingField_27; }
	inline bool* get_address_of_U3CReverseSolutionU3Ek__BackingField_27() { return &___U3CReverseSolutionU3Ek__BackingField_27; }
	inline void set_U3CReverseSolutionU3Ek__BackingField_27(bool value)
	{
		___U3CReverseSolutionU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3CStrictlySimpleU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(Clipper_t636949239, ___U3CStrictlySimpleU3Ek__BackingField_28)); }
	inline bool get_U3CStrictlySimpleU3Ek__BackingField_28() const { return ___U3CStrictlySimpleU3Ek__BackingField_28; }
	inline bool* get_address_of_U3CStrictlySimpleU3Ek__BackingField_28() { return &___U3CStrictlySimpleU3Ek__BackingField_28; }
	inline void set_U3CStrictlySimpleU3Ek__BackingField_28(bool value)
	{
		___U3CStrictlySimpleU3Ek__BackingField_28 = value;
	}
};

struct Clipper_t636949239_StaticFields
{
public:
	// System.Action`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>> Pathfinding.ClipperLib.Clipper::<>f__am$cacheE
	Action_1_t795160571 * ___U3CU3Ef__amU24cacheE_29;
	// System.Action`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>> Pathfinding.ClipperLib.Clipper::<>f__am$cacheF
	Action_1_t795160571 * ___U3CU3Ef__amU24cacheF_30;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheE_29() { return static_cast<int32_t>(offsetof(Clipper_t636949239_StaticFields, ___U3CU3Ef__amU24cacheE_29)); }
	inline Action_1_t795160571 * get_U3CU3Ef__amU24cacheE_29() const { return ___U3CU3Ef__amU24cacheE_29; }
	inline Action_1_t795160571 ** get_address_of_U3CU3Ef__amU24cacheE_29() { return &___U3CU3Ef__amU24cacheE_29; }
	inline void set_U3CU3Ef__amU24cacheE_29(Action_1_t795160571 * value)
	{
		___U3CU3Ef__amU24cacheE_29 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheE_29, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheF_30() { return static_cast<int32_t>(offsetof(Clipper_t636949239_StaticFields, ___U3CU3Ef__amU24cacheF_30)); }
	inline Action_1_t795160571 * get_U3CU3Ef__amU24cacheF_30() const { return ___U3CU3Ef__amU24cacheF_30; }
	inline Action_1_t795160571 ** get_address_of_U3CU3Ef__amU24cacheF_30() { return &___U3CU3Ef__amU24cacheF_30; }
	inline void set_U3CU3Ef__amU24cacheF_30(Action_1_t795160571 * value)
	{
		___U3CU3Ef__amU24cacheF_30 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cacheF_30, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
