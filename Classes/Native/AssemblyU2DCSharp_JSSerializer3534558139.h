﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1015136018;
// System.Collections.Generic.List`1<JSComponent>
struct List_1_t3011080324;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSSerializer
struct  JSSerializer_t3534558139  : public MonoBehaviour_t667441552
{
public:
	// System.String JSSerializer::jsClassName
	String_t* ___jsClassName_2;
	// System.String[] JSSerializer::arrString
	StringU5BU5D_t4054002952* ___arrString_3;
	// UnityEngine.Object[] JSSerializer::arrObject
	ObjectU5BU5D_t1015136018* ___arrObject_4;
	// System.Int32 JSSerializer::arrStringIndex
	int32_t ___arrStringIndex_5;
	// System.Boolean JSSerializer::dataSerialized
	bool ___dataSerialized_6;
	// System.Collections.Generic.List`1<JSComponent> JSSerializer::waitSerialize
	List_1_t3011080324 * ___waitSerialize_7;

public:
	inline static int32_t get_offset_of_jsClassName_2() { return static_cast<int32_t>(offsetof(JSSerializer_t3534558139, ___jsClassName_2)); }
	inline String_t* get_jsClassName_2() const { return ___jsClassName_2; }
	inline String_t** get_address_of_jsClassName_2() { return &___jsClassName_2; }
	inline void set_jsClassName_2(String_t* value)
	{
		___jsClassName_2 = value;
		Il2CppCodeGenWriteBarrier(&___jsClassName_2, value);
	}

	inline static int32_t get_offset_of_arrString_3() { return static_cast<int32_t>(offsetof(JSSerializer_t3534558139, ___arrString_3)); }
	inline StringU5BU5D_t4054002952* get_arrString_3() const { return ___arrString_3; }
	inline StringU5BU5D_t4054002952** get_address_of_arrString_3() { return &___arrString_3; }
	inline void set_arrString_3(StringU5BU5D_t4054002952* value)
	{
		___arrString_3 = value;
		Il2CppCodeGenWriteBarrier(&___arrString_3, value);
	}

	inline static int32_t get_offset_of_arrObject_4() { return static_cast<int32_t>(offsetof(JSSerializer_t3534558139, ___arrObject_4)); }
	inline ObjectU5BU5D_t1015136018* get_arrObject_4() const { return ___arrObject_4; }
	inline ObjectU5BU5D_t1015136018** get_address_of_arrObject_4() { return &___arrObject_4; }
	inline void set_arrObject_4(ObjectU5BU5D_t1015136018* value)
	{
		___arrObject_4 = value;
		Il2CppCodeGenWriteBarrier(&___arrObject_4, value);
	}

	inline static int32_t get_offset_of_arrStringIndex_5() { return static_cast<int32_t>(offsetof(JSSerializer_t3534558139, ___arrStringIndex_5)); }
	inline int32_t get_arrStringIndex_5() const { return ___arrStringIndex_5; }
	inline int32_t* get_address_of_arrStringIndex_5() { return &___arrStringIndex_5; }
	inline void set_arrStringIndex_5(int32_t value)
	{
		___arrStringIndex_5 = value;
	}

	inline static int32_t get_offset_of_dataSerialized_6() { return static_cast<int32_t>(offsetof(JSSerializer_t3534558139, ___dataSerialized_6)); }
	inline bool get_dataSerialized_6() const { return ___dataSerialized_6; }
	inline bool* get_address_of_dataSerialized_6() { return &___dataSerialized_6; }
	inline void set_dataSerialized_6(bool value)
	{
		___dataSerialized_6 = value;
	}

	inline static int32_t get_offset_of_waitSerialize_7() { return static_cast<int32_t>(offsetof(JSSerializer_t3534558139, ___waitSerialize_7)); }
	inline List_1_t3011080324 * get_waitSerialize_7() const { return ___waitSerialize_7; }
	inline List_1_t3011080324 ** get_address_of_waitSerialize_7() { return &___waitSerialize_7; }
	inline void set_waitSerialize_7(List_1_t3011080324 * value)
	{
		___waitSerialize_7 = value;
		Il2CppCodeGenWriteBarrier(&___waitSerialize_7, value);
	}
};

struct JSSerializer_t3534558139_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> JSSerializer::<>f__switch$map0
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map0_8;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_8() { return static_cast<int32_t>(offsetof(JSSerializer_t3534558139_StaticFields, ___U3CU3Ef__switchU24map0_8)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map0_8() const { return ___U3CU3Ef__switchU24map0_8; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map0_8() { return &___U3CU3Ef__switchU24map0_8; }
	inline void set_U3CU3Ef__switchU24map0_8(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map0_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map0_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
