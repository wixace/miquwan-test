﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V2331461444MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m946717202(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3560552897 *, Dictionary_2_t564979888 *, const MethodInfo*))ValueCollection__ctor_m1378915496_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1839656928(__this, ___item0, method) ((  void (*) (ValueCollection_t3560552897 *, List_1_t1104940528 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3516166794_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m198167721(__this, method) ((  void (*) (ValueCollection_t3560552897 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3513594963_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m330953226(__this, ___item0, method) ((  bool (*) (ValueCollection_t3560552897 *, List_1_t1104940528 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1298725212_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3281782063(__this, ___item0, method) ((  bool (*) (ValueCollection_t3560552897 *, List_1_t1104940528 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1302903041_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1024578921(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3560552897 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m506325281_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m2863754861(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3560552897 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3017329047_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1655658940(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3560552897 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m157092562_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2901096381(__this, method) ((  bool (*) (ValueCollection_t3560552897 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3868868367_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2929815837(__this, method) ((  bool (*) (ValueCollection_t3560552897 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3081986927_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m3559695951(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3560552897 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2148847707_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m2653884889(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3560552897 *, List_1U5BU5D_t1953614929*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2863754991_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::GetEnumerator()
#define ValueCollection_GetEnumerator_m2624987906(__this, method) ((  Enumerator_t2791780592  (*) (ValueCollection_t3560552897 *, const MethodInfo*))ValueCollection_GetEnumerator_m1092043858_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::get_Count()
#define ValueCollection_get_Count_m4207446871(__this, method) ((  int32_t (*) (ValueCollection_t3560552897 *, const MethodInfo*))ValueCollection_get_Count_m1269644853_gshared)(__this, method)
