﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1347443627MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.AnimationClip,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m633074958(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t129947640 *, Dictionary_2_t1429341927 *, const MethodInfo*))ValueCollection__ctor_m2824870125_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.AnimationClip,System.Boolean>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1552444772(__this, ___item0, method) ((  void (*) (ValueCollection_t129947640 *, bool, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m653316517_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.AnimationClip,System.Boolean>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3740576813(__this, method) ((  void (*) (ValueCollection_t129947640 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3589495022_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.AnimationClip,System.Boolean>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m4034217350(__this, ___item0, method) ((  bool (*) (ValueCollection_t129947640 *, bool, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1506710757_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.AnimationClip,System.Boolean>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m617482155(__this, ___item0, method) ((  bool (*) (ValueCollection_t129947640 *, bool, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m883008202_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.AnimationClip,System.Boolean>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2657395245(__this, method) ((  Il2CppObject* (*) (ValueCollection_t129947640 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4243354478_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.AnimationClip,System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3732388081(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t129947640 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2558142578_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.AnimationClip,System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3138567616(__this, method) ((  Il2CppObject * (*) (ValueCollection_t129947640 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2848139905_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.AnimationClip,System.Boolean>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2309393209(__this, method) ((  bool (*) (ValueCollection_t129947640 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m4076853912_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.AnimationClip,System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m251731609(__this, method) ((  bool (*) (ValueCollection_t129947640 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4249306232_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.AnimationClip,System.Boolean>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m454679179(__this, method) ((  Il2CppObject * (*) (ValueCollection_t129947640 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2240931882_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.AnimationClip,System.Boolean>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m2389403093(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t129947640 *, BooleanU5BU5D_t3456302923*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2147895796_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.AnimationClip,System.Boolean>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3985369534(__this, method) ((  Enumerator_t3656142631  (*) (ValueCollection_t129947640 *, const MethodInfo*))ValueCollection_GetEnumerator_m387880093_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.AnimationClip,System.Boolean>::get_Count()
#define ValueCollection_get_Count_m180425043(__this, method) ((  int32_t (*) (ValueCollection_t129947640 *, const MethodInfo*))ValueCollection_get_Count_m971561266_gshared)(__this, method)
