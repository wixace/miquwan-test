﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory
struct LateBoundReflectionDelegateFactory_t1043944210;
// Newtonsoft.Json.Utilities.ReflectionDelegateFactory
struct ReflectionDelegateFactory_t1590616920;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"

// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::.ctor()
extern "C"  void LateBoundReflectionDelegateFactory__ctor_m2007574752 (LateBoundReflectionDelegateFactory_t1043944210 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::.cctor()
extern "C"  void LateBoundReflectionDelegateFactory__cctor_m1623178957 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.ReflectionDelegateFactory Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::get_Instance()
extern "C"  ReflectionDelegateFactory_t1590616920 * LateBoundReflectionDelegateFactory_get_Instance_m3158424212 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory::ilo_ArgumentNotNull1(System.Object,System.String)
extern "C"  void LateBoundReflectionDelegateFactory_ilo_ArgumentNotNull1_m1134607557 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, String_t* ___parameterName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
