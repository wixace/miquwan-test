﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MethodID
struct MethodID_t3916401116;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine_ScriptableObjectGenerated
struct  UnityEngine_ScriptableObjectGenerated_t2200492355  : public Il2CppObject
{
public:

public:
};

struct UnityEngine_ScriptableObjectGenerated_t2200492355_StaticFields
{
public:
	// MethodID UnityEngine_ScriptableObjectGenerated::methodID2
	MethodID_t3916401116 * ___methodID2_0;

public:
	inline static int32_t get_offset_of_methodID2_0() { return static_cast<int32_t>(offsetof(UnityEngine_ScriptableObjectGenerated_t2200492355_StaticFields, ___methodID2_0)); }
	inline MethodID_t3916401116 * get_methodID2_0() const { return ___methodID2_0; }
	inline MethodID_t3916401116 ** get_address_of_methodID2_0() { return &___methodID2_0; }
	inline void set_methodID2_0(MethodID_t3916401116 * value)
	{
		___methodID2_0 = value;
		Il2CppCodeGenWriteBarrier(&___methodID2_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
