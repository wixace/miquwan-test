﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._fcafb63bd94da05ea4966ac4d0a99955
struct _fcafb63bd94da05ea4966ac4d0a99955_t1851570322;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__fcafb63bd94da05ea4966ac41851570322.h"

// System.Void Little._fcafb63bd94da05ea4966ac4d0a99955::.ctor()
extern "C"  void _fcafb63bd94da05ea4966ac4d0a99955__ctor_m3491230171 (_fcafb63bd94da05ea4966ac4d0a99955_t1851570322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._fcafb63bd94da05ea4966ac4d0a99955::_fcafb63bd94da05ea4966ac4d0a99955m2(System.Int32)
extern "C"  int32_t _fcafb63bd94da05ea4966ac4d0a99955__fcafb63bd94da05ea4966ac4d0a99955m2_m401389657 (_fcafb63bd94da05ea4966ac4d0a99955_t1851570322 * __this, int32_t ____fcafb63bd94da05ea4966ac4d0a99955a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._fcafb63bd94da05ea4966ac4d0a99955::_fcafb63bd94da05ea4966ac4d0a99955m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _fcafb63bd94da05ea4966ac4d0a99955__fcafb63bd94da05ea4966ac4d0a99955m_m2245854845 (_fcafb63bd94da05ea4966ac4d0a99955_t1851570322 * __this, int32_t ____fcafb63bd94da05ea4966ac4d0a99955a0, int32_t ____fcafb63bd94da05ea4966ac4d0a99955761, int32_t ____fcafb63bd94da05ea4966ac4d0a99955c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._fcafb63bd94da05ea4966ac4d0a99955::ilo__fcafb63bd94da05ea4966ac4d0a99955m21(Little._fcafb63bd94da05ea4966ac4d0a99955,System.Int32)
extern "C"  int32_t _fcafb63bd94da05ea4966ac4d0a99955_ilo__fcafb63bd94da05ea4966ac4d0a99955m21_m2655987353 (Il2CppObject * __this /* static, unused */, _fcafb63bd94da05ea4966ac4d0a99955_t1851570322 * ____this0, int32_t ____fcafb63bd94da05ea4966ac4d0a99955a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
