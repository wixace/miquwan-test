﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSharpGeneratedBase
struct CSharpGeneratedBase_t950833881;

#include "codegen/il2cpp-codegen.h"

// System.Void CSharpGeneratedBase::.ctor()
extern "C"  void CSharpGeneratedBase__ctor_m1605805234 (CSharpGeneratedBase_t950833881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSharpGeneratedBase::Register()
extern "C"  bool CSharpGeneratedBase_Register_m669902465 (CSharpGeneratedBase_t950833881 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
