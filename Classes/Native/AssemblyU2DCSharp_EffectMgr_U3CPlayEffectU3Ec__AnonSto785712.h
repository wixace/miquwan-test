﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CombatEntity
struct CombatEntity_t684137495;
// effectCfg
struct effectCfg_t2826279187;
// EffectCtrl
struct EffectCtrl_t3708787644;
// EffectMgr
struct EffectMgr_t535289511;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectMgr/<PlayEffect>c__AnonStorey156
struct  U3CPlayEffectU3Ec__AnonStorey156_t785712  : public Il2CppObject
{
public:
	// CombatEntity EffectMgr/<PlayEffect>c__AnonStorey156::entity
	CombatEntity_t684137495 * ___entity_0;
	// System.Int32 EffectMgr/<PlayEffect>c__AnonStorey156::id
	int32_t ___id_1;
	// effectCfg EffectMgr/<PlayEffect>c__AnonStorey156::effectCfg
	effectCfg_t2826279187 * ___effectCfg_2;
	// CombatEntity EffectMgr/<PlayEffect>c__AnonStorey156::srcTf
	CombatEntity_t684137495 * ___srcTf_3;
	// EffectCtrl EffectMgr/<PlayEffect>c__AnonStorey156::effectObj
	EffectCtrl_t3708787644 * ___effectObj_4;
	// System.Single EffectMgr/<PlayEffect>c__AnonStorey156::lifetime
	float ___lifetime_5;
	// UnityEngine.Vector3 EffectMgr/<PlayEffect>c__AnonStorey156::pos
	Vector3_t4282066566  ___pos_6;
	// CombatEntity EffectMgr/<PlayEffect>c__AnonStorey156::host
	CombatEntity_t684137495 * ___host_7;
	// System.Boolean EffectMgr/<PlayEffect>c__AnonStorey156::isShowInBlack
	bool ___isShowInBlack_8;
	// System.Single EffectMgr/<PlayEffect>c__AnonStorey156::playSpeed
	float ___playSpeed_9;
	// EffectMgr EffectMgr/<PlayEffect>c__AnonStorey156::<>f__this
	EffectMgr_t535289511 * ___U3CU3Ef__this_10;

public:
	inline static int32_t get_offset_of_entity_0() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__AnonStorey156_t785712, ___entity_0)); }
	inline CombatEntity_t684137495 * get_entity_0() const { return ___entity_0; }
	inline CombatEntity_t684137495 ** get_address_of_entity_0() { return &___entity_0; }
	inline void set_entity_0(CombatEntity_t684137495 * value)
	{
		___entity_0 = value;
		Il2CppCodeGenWriteBarrier(&___entity_0, value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__AnonStorey156_t785712, ___id_1)); }
	inline int32_t get_id_1() const { return ___id_1; }
	inline int32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int32_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_effectCfg_2() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__AnonStorey156_t785712, ___effectCfg_2)); }
	inline effectCfg_t2826279187 * get_effectCfg_2() const { return ___effectCfg_2; }
	inline effectCfg_t2826279187 ** get_address_of_effectCfg_2() { return &___effectCfg_2; }
	inline void set_effectCfg_2(effectCfg_t2826279187 * value)
	{
		___effectCfg_2 = value;
		Il2CppCodeGenWriteBarrier(&___effectCfg_2, value);
	}

	inline static int32_t get_offset_of_srcTf_3() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__AnonStorey156_t785712, ___srcTf_3)); }
	inline CombatEntity_t684137495 * get_srcTf_3() const { return ___srcTf_3; }
	inline CombatEntity_t684137495 ** get_address_of_srcTf_3() { return &___srcTf_3; }
	inline void set_srcTf_3(CombatEntity_t684137495 * value)
	{
		___srcTf_3 = value;
		Il2CppCodeGenWriteBarrier(&___srcTf_3, value);
	}

	inline static int32_t get_offset_of_effectObj_4() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__AnonStorey156_t785712, ___effectObj_4)); }
	inline EffectCtrl_t3708787644 * get_effectObj_4() const { return ___effectObj_4; }
	inline EffectCtrl_t3708787644 ** get_address_of_effectObj_4() { return &___effectObj_4; }
	inline void set_effectObj_4(EffectCtrl_t3708787644 * value)
	{
		___effectObj_4 = value;
		Il2CppCodeGenWriteBarrier(&___effectObj_4, value);
	}

	inline static int32_t get_offset_of_lifetime_5() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__AnonStorey156_t785712, ___lifetime_5)); }
	inline float get_lifetime_5() const { return ___lifetime_5; }
	inline float* get_address_of_lifetime_5() { return &___lifetime_5; }
	inline void set_lifetime_5(float value)
	{
		___lifetime_5 = value;
	}

	inline static int32_t get_offset_of_pos_6() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__AnonStorey156_t785712, ___pos_6)); }
	inline Vector3_t4282066566  get_pos_6() const { return ___pos_6; }
	inline Vector3_t4282066566 * get_address_of_pos_6() { return &___pos_6; }
	inline void set_pos_6(Vector3_t4282066566  value)
	{
		___pos_6 = value;
	}

	inline static int32_t get_offset_of_host_7() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__AnonStorey156_t785712, ___host_7)); }
	inline CombatEntity_t684137495 * get_host_7() const { return ___host_7; }
	inline CombatEntity_t684137495 ** get_address_of_host_7() { return &___host_7; }
	inline void set_host_7(CombatEntity_t684137495 * value)
	{
		___host_7 = value;
		Il2CppCodeGenWriteBarrier(&___host_7, value);
	}

	inline static int32_t get_offset_of_isShowInBlack_8() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__AnonStorey156_t785712, ___isShowInBlack_8)); }
	inline bool get_isShowInBlack_8() const { return ___isShowInBlack_8; }
	inline bool* get_address_of_isShowInBlack_8() { return &___isShowInBlack_8; }
	inline void set_isShowInBlack_8(bool value)
	{
		___isShowInBlack_8 = value;
	}

	inline static int32_t get_offset_of_playSpeed_9() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__AnonStorey156_t785712, ___playSpeed_9)); }
	inline float get_playSpeed_9() const { return ___playSpeed_9; }
	inline float* get_address_of_playSpeed_9() { return &___playSpeed_9; }
	inline void set_playSpeed_9(float value)
	{
		___playSpeed_9 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_10() { return static_cast<int32_t>(offsetof(U3CPlayEffectU3Ec__AnonStorey156_t785712, ___U3CU3Ef__this_10)); }
	inline EffectMgr_t535289511 * get_U3CU3Ef__this_10() const { return ___U3CU3Ef__this_10; }
	inline EffectMgr_t535289511 ** get_address_of_U3CU3Ef__this_10() { return &___U3CU3Ef__this_10; }
	inline void set_U3CU3Ef__this_10(EffectMgr_t535289511 * value)
	{
		___U3CU3Ef__this_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
