﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_jelbirsa7
struct M_jelbirsa7_t3085780943;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_jelbirsa7::.ctor()
extern "C"  void M_jelbirsa7__ctor_m4244255348 (M_jelbirsa7_t3085780943 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jelbirsa7::M_nokiTeseeme0(System.String[],System.Int32)
extern "C"  void M_jelbirsa7_M_nokiTeseeme0_m3524911918 (M_jelbirsa7_t3085780943 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
