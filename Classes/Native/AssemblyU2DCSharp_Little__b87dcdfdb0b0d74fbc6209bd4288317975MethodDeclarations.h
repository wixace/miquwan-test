﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._b87dcdfdb0b0d74fbc6209bd6bb9c531
struct _b87dcdfdb0b0d74fbc6209bd6bb9c531_t4288317975;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._b87dcdfdb0b0d74fbc6209bd6bb9c531::.ctor()
extern "C"  void _b87dcdfdb0b0d74fbc6209bd6bb9c531__ctor_m855442934 (_b87dcdfdb0b0d74fbc6209bd6bb9c531_t4288317975 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b87dcdfdb0b0d74fbc6209bd6bb9c531::_b87dcdfdb0b0d74fbc6209bd6bb9c531m2(System.Int32)
extern "C"  int32_t _b87dcdfdb0b0d74fbc6209bd6bb9c531__b87dcdfdb0b0d74fbc6209bd6bb9c531m2_m1692115513 (_b87dcdfdb0b0d74fbc6209bd6bb9c531_t4288317975 * __this, int32_t ____b87dcdfdb0b0d74fbc6209bd6bb9c531a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b87dcdfdb0b0d74fbc6209bd6bb9c531::_b87dcdfdb0b0d74fbc6209bd6bb9c531m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _b87dcdfdb0b0d74fbc6209bd6bb9c531__b87dcdfdb0b0d74fbc6209bd6bb9c531m_m1803483293 (_b87dcdfdb0b0d74fbc6209bd6bb9c531_t4288317975 * __this, int32_t ____b87dcdfdb0b0d74fbc6209bd6bb9c531a0, int32_t ____b87dcdfdb0b0d74fbc6209bd6bb9c531221, int32_t ____b87dcdfdb0b0d74fbc6209bd6bb9c531c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
