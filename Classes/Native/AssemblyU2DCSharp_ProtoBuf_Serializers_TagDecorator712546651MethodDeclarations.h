﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.TagDecorator
struct TagDecorator_t712546651;
// ProtoBuf.Serializers.IProtoSerializer
struct IProtoSerializer_t3033312651;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.SerializationContext
struct SerializationContext_t3997850667;
// System.Type
struct Type_t;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// ProtoBuf.Serializers.IProtoTypeSerializer
struct IProtoTypeSerializer_t321624293;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_WireType2355646059.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel_Callback2866957669.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_SerializationContext3997850667.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"

// System.Void ProtoBuf.Serializers.TagDecorator::.ctor(System.Int32,ProtoBuf.WireType,System.Boolean,ProtoBuf.Serializers.IProtoSerializer)
extern "C"  void TagDecorator__ctor_m2586808179 (TagDecorator_t712546651 * __this, int32_t ___fieldNumber0, int32_t ___wireType1, bool ___strict2, Il2CppObject * ___tail3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.TagDecorator::HasCallbacks(ProtoBuf.Meta.TypeModel/CallbackType)
extern "C"  bool TagDecorator_HasCallbacks_m1708779525 (TagDecorator_t712546651 * __this, int32_t ___callbackType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.TagDecorator::CanCreateInstance()
extern "C"  bool TagDecorator_CanCreateInstance_m2238958989 (TagDecorator_t712546651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.TagDecorator::CreateInstance(ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * TagDecorator_CreateInstance_m3040717118 (TagDecorator_t712546651 * __this, ProtoReader_t3962509489 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.TagDecorator::Callback(System.Object,ProtoBuf.Meta.TypeModel/CallbackType,ProtoBuf.SerializationContext)
extern "C"  void TagDecorator_Callback_m2226168582 (TagDecorator_t712546651 * __this, Il2CppObject * ___value0, int32_t ___callbackType1, SerializationContext_t3997850667 * ___context2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.TagDecorator::get_ExpectedType()
extern "C"  Type_t * TagDecorator_get_ExpectedType_m54458731 (TagDecorator_t712546651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.TagDecorator::get_RequiresOldValue()
extern "C"  bool TagDecorator_get_RequiresOldValue_m2912152791 (TagDecorator_t712546651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.TagDecorator::get_ReturnsValue()
extern "C"  bool TagDecorator_get_ReturnsValue_m654172909 (TagDecorator_t712546651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.TagDecorator::get_NeedsHint()
extern "C"  bool TagDecorator_get_NeedsHint_m1823208455 (TagDecorator_t712546651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.TagDecorator::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * TagDecorator_Read_m712311189 (TagDecorator_t712546651 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.TagDecorator::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void TagDecorator_Write_m2429009105 (TagDecorator_t712546651 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.TagDecorator::ilo_HasCallbacks1(ProtoBuf.Serializers.IProtoTypeSerializer,ProtoBuf.Meta.TypeModel/CallbackType)
extern "C"  bool TagDecorator_ilo_HasCallbacks1_m3860777856 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, int32_t ___callbackType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.TagDecorator::ilo_get_ExpectedType2(ProtoBuf.Serializers.IProtoSerializer)
extern "C"  Type_t * TagDecorator_ilo_get_ExpectedType2_m2383559099 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.TagDecorator::ilo_get_RequiresOldValue3(ProtoBuf.Serializers.IProtoSerializer)
extern "C"  bool TagDecorator_ilo_get_RequiresOldValue3_m2491553926 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.TagDecorator::ilo_Hint4(ProtoBuf.ProtoReader,ProtoBuf.WireType)
extern "C"  void TagDecorator_ilo_Hint4_m678352788 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, int32_t ___wireType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.TagDecorator::ilo_Read5(ProtoBuf.Serializers.IProtoSerializer,System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * TagDecorator_ilo_Read5_m3379169560 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___value1, ProtoReader_t3962509489 * ___source2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.TagDecorator::ilo_WriteFieldHeader6(System.Int32,ProtoBuf.WireType,ProtoBuf.ProtoWriter)
extern "C"  void TagDecorator_ilo_WriteFieldHeader6_m242589056 (Il2CppObject * __this /* static, unused */, int32_t ___fieldNumber0, int32_t ___wireType1, ProtoWriter_t4117914721 * ___writer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
