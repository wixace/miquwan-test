﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MihuaPayMgr
struct MihuaPayMgr_t1240399912;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// SceneMgr
struct SceneMgr_t3584105996;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// System.Object
struct Il2CppObject;
// ByteReadArray
struct ByteReadArray_t751193627;
// MHIAPMgr.PayInfo
struct PayInfo_t444430236;
// Mihua.Net.UrlLoader
struct UrlLoader_t2490729496;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ByteReadArray751193627.h"
#include "AssemblyU2DCSharp_MHIAPMgr_PayInfo444430236.h"
#include "AssemblyU2DCSharp_MihuaPayMgr1240399912.h"
#include "AssemblyU2DCSharp_Mihua_Net_UrlLoader2490729496.h"

// System.Void MihuaPayMgr::.ctor()
extern "C"  void MihuaPayMgr__ctor_m4053149763 (MihuaPayMgr_t1240399912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MihuaPayMgr::EnterGame(CEvent.ZEvent)
extern "C"  void MihuaPayMgr_EnterGame_m545960080 (MihuaPayMgr_t1240399912 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MihuaPayMgr::AddPayInfo(System.String)
extern "C"  void MihuaPayMgr_AddPayInfo_m3898927564 (MihuaPayMgr_t1240399912 * __this, String_t* ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MihuaPayMgr::Read()
extern "C"  void MihuaPayMgr_Read_m1993948343 (MihuaPayMgr_t1240399912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MihuaPayMgr::Save()
extern "C"  void MihuaPayMgr_Save_m2019509982 (MihuaPayMgr_t1240399912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MihuaPayMgr::RemovePayInfo(System.String)
extern "C"  void MihuaPayMgr_RemovePayInfo_m3732230319 (MihuaPayMgr_t1240399912 * __this, String_t* ___orderId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MihuaPayMgr::OnResponse(System.String,System.Byte[])
extern "C"  void MihuaPayMgr_OnResponse_m212252676 (MihuaPayMgr_t1240399912 * __this, String_t* ___orderId0, ByteU5BU5D_t4260760469* ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MihuaPayMgr::ZUpdate()
extern "C"  void MihuaPayMgr_ZUpdate_m3405081220 (MihuaPayMgr_t1240399912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MihuaPayMgr::DownNext()
extern "C"  void MihuaPayMgr_DownNext_m1800161814 (MihuaPayMgr_t1240399912 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SceneMgr MihuaPayMgr::ilo_get_SceneMgr1()
extern "C"  SceneMgr_t3584105996 * MihuaPayMgr_ilo_get_SceneMgr1_m2088534555 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MihuaPayMgr::ilo_RemoveEventListener2(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void MihuaPayMgr_ilo_RemoveEventListener2_m2073920295 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MihuaPayMgr::ilo_Log3(System.Object,System.Boolean)
extern "C"  void MihuaPayMgr_ilo_Log3_m3252520748 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MihuaPayMgr::ilo_ReadUTF4(ByteReadArray)
extern "C"  String_t* MihuaPayMgr_ilo_ReadUTF4_m610573059 (Il2CppObject * __this /* static, unused */, ByteReadArray_t751193627 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MihuaPayMgr::ilo_Parse5(MHIAPMgr.PayInfo,System.String)
extern "C"  void MihuaPayMgr_ilo_Parse5_m3491064847 (Il2CppObject * __this /* static, unused */, PayInfo_t444430236 * ____this0, String_t* ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MihuaPayMgr::ilo_RemovePayInfo6(MihuaPayMgr,System.String)
extern "C"  void MihuaPayMgr_ilo_RemovePayInfo6_m126424206 (Il2CppObject * __this /* static, unused */, MihuaPayMgr_t1240399912 * ____this0, String_t* ___orderId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MihuaPayMgr::ilo_get_error7(Mihua.Net.UrlLoader)
extern "C"  String_t* MihuaPayMgr_ilo_get_error7_m3900974382 (Il2CppObject * __this /* static, unused */, UrlLoader_t2490729496 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String MihuaPayMgr::ilo_get_url8(Mihua.Net.UrlLoader)
extern "C"  String_t* MihuaPayMgr_ilo_get_url8_m4133075380 (Il2CppObject * __this /* static, unused */, UrlLoader_t2490729496 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MihuaPayMgr::ilo_DownNext9(MihuaPayMgr)
extern "C"  void MihuaPayMgr_ilo_DownNext9_m1780801816 (Il2CppObject * __this /* static, unused */, MihuaPayMgr_t1240399912 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MihuaPayMgr::ilo_CanRequest10(MHIAPMgr.PayInfo)
extern "C"  bool MihuaPayMgr_ilo_CanRequest10_m3466447291 (Il2CppObject * __this /* static, unused */, PayInfo_t444430236 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
