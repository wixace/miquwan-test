﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._bc0ca2d692d3c0671f951eef729f723d
struct _bc0ca2d692d3c0671f951eef729f723d_t1238051104;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._bc0ca2d692d3c0671f951eef729f723d::.ctor()
extern "C"  void _bc0ca2d692d3c0671f951eef729f723d__ctor_m2460238093 (_bc0ca2d692d3c0671f951eef729f723d_t1238051104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._bc0ca2d692d3c0671f951eef729f723d::_bc0ca2d692d3c0671f951eef729f723dm2(System.Int32)
extern "C"  int32_t _bc0ca2d692d3c0671f951eef729f723d__bc0ca2d692d3c0671f951eef729f723dm2_m842366361 (_bc0ca2d692d3c0671f951eef729f723d_t1238051104 * __this, int32_t ____bc0ca2d692d3c0671f951eef729f723da0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._bc0ca2d692d3c0671f951eef729f723d::_bc0ca2d692d3c0671f951eef729f723dm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _bc0ca2d692d3c0671f951eef729f723d__bc0ca2d692d3c0671f951eef729f723dm_m3219171645 (_bc0ca2d692d3c0671f951eef729f723d_t1238051104 * __this, int32_t ____bc0ca2d692d3c0671f951eef729f723da0, int32_t ____bc0ca2d692d3c0671f951eef729f723d901, int32_t ____bc0ca2d692d3c0671f951eef729f723dc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
