﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.RVO.Simulator/Worker
struct Worker_t3927454006;
// Pathfinding.RVO.Simulator
struct Simulator_t2705969170;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_Simulator2705969170.h"

// System.Void Pathfinding.RVO.Simulator/Worker::.ctor(Pathfinding.RVO.Simulator)
extern "C"  void Worker__ctor_m1550988566 (Worker_t3927454006 * __this, Simulator_t2705969170 * ___sim0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator/Worker::Execute(System.Int32)
extern "C"  void Worker_Execute_m4180250185 (Worker_t3927454006 * __this, int32_t ___task0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator/Worker::WaitOne()
extern "C"  void Worker_WaitOne_m682978100 (Worker_t3927454006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator/Worker::Terminate()
extern "C"  void Worker_Terminate_m2718687332 (Worker_t3927454006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.RVO.Simulator/Worker::Run()
extern "C"  void Worker_Run_m2137723790 (Worker_t3927454006 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
