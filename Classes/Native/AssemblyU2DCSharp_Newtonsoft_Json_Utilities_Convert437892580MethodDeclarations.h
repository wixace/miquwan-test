﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.ConvertUtils/<TryConvertOrCast>c__AnonStorey13D`1<System.Object>
struct U3CTryConvertOrCastU3Ec__AnonStorey13D_1_t437892580;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Utilities.ConvertUtils/<TryConvertOrCast>c__AnonStorey13D`1<System.Object>::.ctor()
extern "C"  void U3CTryConvertOrCastU3Ec__AnonStorey13D_1__ctor_m2456385675_gshared (U3CTryConvertOrCastU3Ec__AnonStorey13D_1_t437892580 * __this, const MethodInfo* method);
#define U3CTryConvertOrCastU3Ec__AnonStorey13D_1__ctor_m2456385675(__this, method) ((  void (*) (U3CTryConvertOrCastU3Ec__AnonStorey13D_1_t437892580 *, const MethodInfo*))U3CTryConvertOrCastU3Ec__AnonStorey13D_1__ctor_m2456385675_gshared)(__this, method)
// T Newtonsoft.Json.Utilities.ConvertUtils/<TryConvertOrCast>c__AnonStorey13D`1<System.Object>::<>m__399()
extern "C"  Il2CppObject * U3CTryConvertOrCastU3Ec__AnonStorey13D_1_U3CU3Em__399_m2928679392_gshared (U3CTryConvertOrCastU3Ec__AnonStorey13D_1_t437892580 * __this, const MethodInfo* method);
#define U3CTryConvertOrCastU3Ec__AnonStorey13D_1_U3CU3Em__399_m2928679392(__this, method) ((  Il2CppObject * (*) (U3CTryConvertOrCastU3Ec__AnonStorey13D_1_t437892580 *, const MethodInfo*))U3CTryConvertOrCastU3Ec__AnonStorey13D_1_U3CU3Em__399_m2928679392_gshared)(__this, method)
