﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SkillGenerated
struct SkillGenerated_t2072823390;
// JSVCall
struct JSVCall_t3708497963;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// Skill
struct Skill_t79944241;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_Skill79944241.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void SkillGenerated::.ctor()
extern "C"  void SkillGenerated__ctor_m2826915837 (SkillGenerated_t2072823390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SkillGenerated::Skill_Skill1(JSVCall,System.Int32)
extern "C"  bool SkillGenerated_Skill_Skill1_m3296994745 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SkillGenerated::Skill_Skill2(JSVCall,System.Int32)
extern "C"  bool SkillGenerated_Skill_Skill2_m246791930 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_index(JSVCall)
extern "C"  void SkillGenerated_Skill_index_m3025023688 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_mTplID(JSVCall)
extern "C"  void SkillGenerated_Skill_mTplID_m980880852 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_isActive(JSVCall)
extern "C"  void SkillGenerated_Skill_isActive_m3628139618 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_isAddedAnger(JSVCall)
extern "C"  void SkillGenerated_Skill_isAddedAnger_m751184033 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_mLastUseTime(JSVCall)
extern "C"  void SkillGenerated_Skill_mLastUseTime_m1566956929 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_mpJson(JSVCall)
extern "C"  void SkillGenerated_Skill_mpJson_m3944792423 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_action(JSVCall)
extern "C"  void SkillGenerated_Skill_action_m287515836 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_actionTime(JSVCall)
extern "C"  void SkillGenerated_Skill_actionTime_m3808251247 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_castingEffect(JSVCall)
extern "C"  void SkillGenerated_Skill_castingEffect_m710204966 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_castingEffectTime(JSVCall)
extern "C"  void SkillGenerated_Skill_castingEffectTime_m813711833 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_castingEffectPlaySpeed(JSVCall)
extern "C"  void SkillGenerated_Skill_castingEffectPlaySpeed_m1055324403 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_effect(JSVCall)
extern "C"  void SkillGenerated_Skill_effect_m3006889921 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_effectTime(JSVCall)
extern "C"  void SkillGenerated_Skill_effectTime_m2360647156 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_effcetEvent(JSVCall)
extern "C"  void SkillGenerated_Skill_effcetEvent_m3098455725 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_effectPlaySpeed(JSVCall)
extern "C"  void SkillGenerated_Skill_effectPlaySpeed_m2401170872 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_hitEffect(JSVCall)
extern "C"  void SkillGenerated_Skill_hitEffect_m2292067030 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_hitEffectTime(JSVCall)
extern "C"  void SkillGenerated_Skill_hitEffectTime_m2062792329 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_zoominoffset(JSVCall)
extern "C"  void SkillGenerated_Skill_zoominoffset_m367126215 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_sound(JSVCall)
extern "C"  void SkillGenerated_Skill_sound_m513050411 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_soundTime(JSVCall)
extern "C"  void SkillGenerated_Skill_soundTime_m990503006 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_skillLevel(JSVCall)
extern "C"  void SkillGenerated_Skill_skillLevel_m3749617119 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_damage_add_per(JSVCall)
extern "C"  void SkillGenerated_Skill_damage_add_per_m4050705251 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_damage_add_value(JSVCall)
extern "C"  void SkillGenerated_Skill_damage_add_value_m3145263887 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_buffHitPer(JSVCall)
extern "C"  void SkillGenerated_Skill_buffHitPer_m734300693 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_buffid(JSVCall)
extern "C"  void SkillGenerated_Skill_buffid_m572314948 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_buffprob(JSVCall)
extern "C"  void SkillGenerated_Skill_buffprob_m2770658826 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_buffevent(JSVCall)
extern "C"  void SkillGenerated_Skill_buffevent_m1620976403 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_bufftarget(JSVCall)
extern "C"  void SkillGenerated_Skill_bufftarget_m1259286798 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_buffbreak(JSVCall)
extern "C"  void SkillGenerated_Skill_buffbreak_m3670449742 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_isProgressBar(JSVCall)
extern "C"  void SkillGenerated_Skill_isProgressBar_m3061852094 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_breakskill(JSVCall)
extern "C"  void SkillGenerated_Skill_breakskill_m4152049600 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_summonid(JSVCall)
extern "C"  void SkillGenerated_Skill_summonid_m6956278 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_summonTime(JSVCall)
extern "C"  void SkillGenerated_Skill_summonTime_m2094823588 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_summonAttackper(JSVCall)
extern "C"  void SkillGenerated_Skill_summonAttackper_m1142701798 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_callAttributeKey1(JSVCall)
extern "C"  void SkillGenerated_Skill_callAttributeKey1_m3857652394 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_callAttributeValue1(JSVCall)
extern "C"  void SkillGenerated_Skill_callAttributeValue1_m2091946428 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_callAttributeKey2(JSVCall)
extern "C"  void SkillGenerated_Skill_callAttributeKey2_m3661138889 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_callAttributeValue2(JSVCall)
extern "C"  void SkillGenerated_Skill_callAttributeValue2_m1895432923 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_isSputter(JSVCall)
extern "C"  void SkillGenerated_Skill_isSputter_m1940412255 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_sputter_radius(JSVCall)
extern "C"  void SkillGenerated_Skill_sputter_radius_m1724299910 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_sputter_num(JSVCall)
extern "C"  void SkillGenerated_Skill_sputter_num_m795440302 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_sputter_hurt(JSVCall)
extern "C"  void SkillGenerated_Skill_sputter_hurt_m3171181609 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_effectCount(JSVCall)
extern "C"  void SkillGenerated_Skill_effectCount_m3366470684 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_CD(JSVCall)
extern "C"  void SkillGenerated_Skill_CD_m1138954481 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_isFingdPos(JSVCall)
extern "C"  void SkillGenerated_Skill_isFingdPos_m1101103772 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_activeSpacing(JSVCall)
extern "C"  void SkillGenerated_Skill_activeSpacing_m3450972573 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_isThump(JSVCall)
extern "C"  void SkillGenerated_Skill_isThump_m155459776 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_thumpAddValue(JSVCall)
extern "C"  void SkillGenerated_Skill_thumpAddValue_m2805211782 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::Skill_isInCD(JSVCall)
extern "C"  void SkillGenerated_Skill_isInCD_m1760947106 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SkillGenerated::Skill_CheckUseDistance__Vector3__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool SkillGenerated_Skill_CheckUseDistance__Vector3__Vector3__Single_m481963637 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SkillGenerated::Skill_isInBlackByEffectID__Int32(JSVCall,System.Int32)
extern "C"  bool SkillGenerated_Skill_isInBlackByEffectID__Int32_m747275572 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SkillGenerated::Skill_OnUseing(JSVCall,System.Int32)
extern "C"  bool SkillGenerated_Skill_OnUseing_m4159904755 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SkillGenerated::Skill_RudeceCD__Single(JSVCall,System.Int32)
extern "C"  bool SkillGenerated_Skill_RudeceCD__Single_m2237670408 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::__Register()
extern "C"  void SkillGenerated___Register_m3004539626 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] SkillGenerated::<Skill_activeSpacing>m__98()
extern "C"  SingleU5BU5D_t2316563989* SkillGenerated_U3CSkill_activeSpacingU3Em__98_m2532400818 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SkillGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t SkillGenerated_ilo_getObject1_m2293344949 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SkillGenerated::ilo_getUInt322(System.Int32)
extern "C"  uint32_t SkillGenerated_ilo_getUInt322_m4174586985 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SkillGenerated::ilo_getInt323(System.Int32)
extern "C"  int32_t SkillGenerated_ilo_getInt323_m2658962630 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SkillGenerated::ilo_get_index4(Skill)
extern "C"  int32_t SkillGenerated_ilo_get_index4_m3967687886 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void SkillGenerated_ilo_setBooleanS5_m2525133610 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::ilo_set_isAddedAnger6(Skill,System.Boolean)
extern "C"  void SkillGenerated_ilo_set_isAddedAnger6_m3783743084 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SkillGenerated::ilo_setObject7(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t SkillGenerated_ilo_setObject7_m2902202291 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] SkillGenerated::ilo_get_action8(Skill)
extern "C"  StringU5BU5D_t4054002952* SkillGenerated_ilo_get_action8_m318359233 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::ilo_moveSaveID2Arr9(System.Int32)
extern "C"  void SkillGenerated_ilo_moveSaveID2Arr9_m2971065818 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::ilo_setSingle10(System.Int32,System.Single)
extern "C"  void SkillGenerated_ilo_setSingle10_m1093272903 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::ilo_setArrayS11(System.Int32,System.Int32,System.Boolean)
extern "C"  void SkillGenerated_ilo_setArrayS11_m684721467 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] SkillGenerated::ilo_get_castingEffectPlaySpeed12(Skill)
extern "C"  SingleU5BU5D_t2316563989* SkillGenerated_ilo_get_castingEffectPlaySpeed12_m3103446036 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::ilo_setInt3213(System.Int32,System.Int32)
extern "C"  void SkillGenerated_ilo_setInt3213_m1085458814 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SkillGenerated::ilo_get_hitEffect14(Skill)
extern "C"  int32_t SkillGenerated_ilo_get_hitEffect14_m1708760009 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] SkillGenerated::ilo_get_soundTime15(Skill)
extern "C"  SingleU5BU5D_t2316563989* SkillGenerated_ilo_get_soundTime15_m3744285132 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::ilo_setUInt3216(System.Int32,System.UInt32)
extern "C"  void SkillGenerated_ilo_setUInt3216_m1618785965 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SkillGenerated::ilo_get_damage_add_value17(Skill)
extern "C"  int32_t SkillGenerated_ilo_get_damage_add_value17_m4210577319 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] SkillGenerated::ilo_get_buffid18(Skill)
extern "C"  Int32U5BU5D_t3230847821* SkillGenerated_ilo_get_buffid18_m748771997 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkillGenerated::ilo_setEnum19(System.Int32,System.Int32)
extern "C"  void SkillGenerated_ilo_setEnum19_m3067873331 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] SkillGenerated::ilo_get_summonid20(Skill)
extern "C"  Int32U5BU5D_t3230847821* SkillGenerated_ilo_get_summonid20_m2461999320 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SkillGenerated::ilo_get_callAttributeKey221(Skill)
extern "C"  String_t* SkillGenerated_ilo_get_callAttributeKey221_m2675536373 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SkillGenerated::ilo_get_CD22(Skill)
extern "C"  float SkillGenerated_ilo_get_CD22_m1424590769 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SkillGenerated::ilo_get_isFingdPos23(Skill)
extern "C"  bool SkillGenerated_ilo_get_isFingdPos23_m3970403667 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SkillGenerated::ilo_getBooleanS24(System.Int32)
extern "C"  bool SkillGenerated_ilo_getBooleanS24_m3062310696 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SkillGenerated::ilo_get_thumpAddValue25(Skill)
extern "C"  float SkillGenerated_ilo_get_thumpAddValue25_m33804183 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SkillGenerated::ilo_getSingle26(System.Int32)
extern "C"  float SkillGenerated_ilo_getSingle26_m66874351 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 SkillGenerated::ilo_getVector3S27(System.Int32)
extern "C"  Vector3_t4282066566  SkillGenerated_ilo_getVector3S27_m981445693 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 SkillGenerated::ilo_getElement28(System.Int32,System.Int32)
extern "C"  int32_t SkillGenerated_ilo_getElement28_m928039186 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
