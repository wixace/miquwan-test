﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// buffCfg
struct buffCfg_t227963665;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// CombatEntity
struct CombatEntity_t684137495;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Buff
struct  Buff_t2081907  : public Il2CppObject
{
public:
	// System.Single Buff::endTime
	float ___endTime_0;
	// System.Int32 Buff::id
	int32_t ___id_1;
	// System.Boolean Buff::handleEff
	bool ___handleEff_2;
	// System.Boolean Buff::isFrist
	bool ___isFrist_3;
	// buffCfg Buff::buffDB
	buffCfg_t227963665 * ___buffDB_4;
	// System.String[] Buff::effectNumText
	StringU5BU5D_t4054002952* ___effectNumText_5;
	// System.Int32[] Buff::effectNumPath
	Int32U5BU5D_t3230847821* ___effectNumPath_6;
	// System.Boolean Buff::isAwaken
	bool ___isAwaken_7;
	// System.Int32 Buff::awakenLv
	int32_t ___awakenLv_8;
	// System.Single Buff::awakenEndtime
	float ___awakenEndtime_9;
	// System.Int32 Buff::awakenAddValue01
	int32_t ___awakenAddValue01_10;
	// System.Int32 Buff::awakenAddValue02
	int32_t ___awakenAddValue02_11;
	// System.Int32 Buff::awakenAddValue03
	int32_t ___awakenAddValue03_12;
	// System.Int32 Buff::awakenAddValue04
	int32_t ___awakenAddValue04_13;
	// CombatEntity Buff::src
	CombatEntity_t684137495 * ___src_14;

public:
	inline static int32_t get_offset_of_endTime_0() { return static_cast<int32_t>(offsetof(Buff_t2081907, ___endTime_0)); }
	inline float get_endTime_0() const { return ___endTime_0; }
	inline float* get_address_of_endTime_0() { return &___endTime_0; }
	inline void set_endTime_0(float value)
	{
		___endTime_0 = value;
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(Buff_t2081907, ___id_1)); }
	inline int32_t get_id_1() const { return ___id_1; }
	inline int32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int32_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_handleEff_2() { return static_cast<int32_t>(offsetof(Buff_t2081907, ___handleEff_2)); }
	inline bool get_handleEff_2() const { return ___handleEff_2; }
	inline bool* get_address_of_handleEff_2() { return &___handleEff_2; }
	inline void set_handleEff_2(bool value)
	{
		___handleEff_2 = value;
	}

	inline static int32_t get_offset_of_isFrist_3() { return static_cast<int32_t>(offsetof(Buff_t2081907, ___isFrist_3)); }
	inline bool get_isFrist_3() const { return ___isFrist_3; }
	inline bool* get_address_of_isFrist_3() { return &___isFrist_3; }
	inline void set_isFrist_3(bool value)
	{
		___isFrist_3 = value;
	}

	inline static int32_t get_offset_of_buffDB_4() { return static_cast<int32_t>(offsetof(Buff_t2081907, ___buffDB_4)); }
	inline buffCfg_t227963665 * get_buffDB_4() const { return ___buffDB_4; }
	inline buffCfg_t227963665 ** get_address_of_buffDB_4() { return &___buffDB_4; }
	inline void set_buffDB_4(buffCfg_t227963665 * value)
	{
		___buffDB_4 = value;
		Il2CppCodeGenWriteBarrier(&___buffDB_4, value);
	}

	inline static int32_t get_offset_of_effectNumText_5() { return static_cast<int32_t>(offsetof(Buff_t2081907, ___effectNumText_5)); }
	inline StringU5BU5D_t4054002952* get_effectNumText_5() const { return ___effectNumText_5; }
	inline StringU5BU5D_t4054002952** get_address_of_effectNumText_5() { return &___effectNumText_5; }
	inline void set_effectNumText_5(StringU5BU5D_t4054002952* value)
	{
		___effectNumText_5 = value;
		Il2CppCodeGenWriteBarrier(&___effectNumText_5, value);
	}

	inline static int32_t get_offset_of_effectNumPath_6() { return static_cast<int32_t>(offsetof(Buff_t2081907, ___effectNumPath_6)); }
	inline Int32U5BU5D_t3230847821* get_effectNumPath_6() const { return ___effectNumPath_6; }
	inline Int32U5BU5D_t3230847821** get_address_of_effectNumPath_6() { return &___effectNumPath_6; }
	inline void set_effectNumPath_6(Int32U5BU5D_t3230847821* value)
	{
		___effectNumPath_6 = value;
		Il2CppCodeGenWriteBarrier(&___effectNumPath_6, value);
	}

	inline static int32_t get_offset_of_isAwaken_7() { return static_cast<int32_t>(offsetof(Buff_t2081907, ___isAwaken_7)); }
	inline bool get_isAwaken_7() const { return ___isAwaken_7; }
	inline bool* get_address_of_isAwaken_7() { return &___isAwaken_7; }
	inline void set_isAwaken_7(bool value)
	{
		___isAwaken_7 = value;
	}

	inline static int32_t get_offset_of_awakenLv_8() { return static_cast<int32_t>(offsetof(Buff_t2081907, ___awakenLv_8)); }
	inline int32_t get_awakenLv_8() const { return ___awakenLv_8; }
	inline int32_t* get_address_of_awakenLv_8() { return &___awakenLv_8; }
	inline void set_awakenLv_8(int32_t value)
	{
		___awakenLv_8 = value;
	}

	inline static int32_t get_offset_of_awakenEndtime_9() { return static_cast<int32_t>(offsetof(Buff_t2081907, ___awakenEndtime_9)); }
	inline float get_awakenEndtime_9() const { return ___awakenEndtime_9; }
	inline float* get_address_of_awakenEndtime_9() { return &___awakenEndtime_9; }
	inline void set_awakenEndtime_9(float value)
	{
		___awakenEndtime_9 = value;
	}

	inline static int32_t get_offset_of_awakenAddValue01_10() { return static_cast<int32_t>(offsetof(Buff_t2081907, ___awakenAddValue01_10)); }
	inline int32_t get_awakenAddValue01_10() const { return ___awakenAddValue01_10; }
	inline int32_t* get_address_of_awakenAddValue01_10() { return &___awakenAddValue01_10; }
	inline void set_awakenAddValue01_10(int32_t value)
	{
		___awakenAddValue01_10 = value;
	}

	inline static int32_t get_offset_of_awakenAddValue02_11() { return static_cast<int32_t>(offsetof(Buff_t2081907, ___awakenAddValue02_11)); }
	inline int32_t get_awakenAddValue02_11() const { return ___awakenAddValue02_11; }
	inline int32_t* get_address_of_awakenAddValue02_11() { return &___awakenAddValue02_11; }
	inline void set_awakenAddValue02_11(int32_t value)
	{
		___awakenAddValue02_11 = value;
	}

	inline static int32_t get_offset_of_awakenAddValue03_12() { return static_cast<int32_t>(offsetof(Buff_t2081907, ___awakenAddValue03_12)); }
	inline int32_t get_awakenAddValue03_12() const { return ___awakenAddValue03_12; }
	inline int32_t* get_address_of_awakenAddValue03_12() { return &___awakenAddValue03_12; }
	inline void set_awakenAddValue03_12(int32_t value)
	{
		___awakenAddValue03_12 = value;
	}

	inline static int32_t get_offset_of_awakenAddValue04_13() { return static_cast<int32_t>(offsetof(Buff_t2081907, ___awakenAddValue04_13)); }
	inline int32_t get_awakenAddValue04_13() const { return ___awakenAddValue04_13; }
	inline int32_t* get_address_of_awakenAddValue04_13() { return &___awakenAddValue04_13; }
	inline void set_awakenAddValue04_13(int32_t value)
	{
		___awakenAddValue04_13 = value;
	}

	inline static int32_t get_offset_of_src_14() { return static_cast<int32_t>(offsetof(Buff_t2081907, ___src_14)); }
	inline CombatEntity_t684137495 * get_src_14() const { return ___src_14; }
	inline CombatEntity_t684137495 ** get_address_of_src_14() { return &___src_14; }
	inline void set_src_14(CombatEntity_t684137495 * value)
	{
		___src_14 = value;
		Il2CppCodeGenWriteBarrier(&___src_14, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
