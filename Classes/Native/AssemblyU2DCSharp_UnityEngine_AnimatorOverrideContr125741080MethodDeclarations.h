﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AnimatorOverrideControllerGenerated
struct UnityEngine_AnimatorOverrideControllerGenerated_t125741080;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.AnimationClipPair[]
struct AnimationClipPairU5BU5D_t3767720461;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_AnimatorOverrideControllerGenerated::.ctor()
extern "C"  void UnityEngine_AnimatorOverrideControllerGenerated__ctor_m2291341907 (UnityEngine_AnimatorOverrideControllerGenerated_t125741080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimatorOverrideControllerGenerated::AnimatorOverrideController_AnimatorOverrideController1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimatorOverrideControllerGenerated_AnimatorOverrideController_AnimatorOverrideController1_m2134000287 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorOverrideControllerGenerated::AnimatorOverrideController_runtimeAnimatorController(JSVCall)
extern "C"  void UnityEngine_AnimatorOverrideControllerGenerated_AnimatorOverrideController_runtimeAnimatorController_m842519787 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorOverrideControllerGenerated::AnimatorOverrideController_Item_String(JSVCall)
extern "C"  void UnityEngine_AnimatorOverrideControllerGenerated_AnimatorOverrideController_Item_String_m3353906249 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorOverrideControllerGenerated::AnimatorOverrideController_Item_AnimationClip(JSVCall)
extern "C"  void UnityEngine_AnimatorOverrideControllerGenerated_AnimatorOverrideController_Item_AnimationClip_m156377310 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorOverrideControllerGenerated::AnimatorOverrideController_clips(JSVCall)
extern "C"  void UnityEngine_AnimatorOverrideControllerGenerated_AnimatorOverrideController_clips_m3300059811 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimatorOverrideControllerGenerated::__Register()
extern "C"  void UnityEngine_AnimatorOverrideControllerGenerated___Register_m747691604 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AnimationClipPair[] UnityEngine_AnimatorOverrideControllerGenerated::<AnimatorOverrideController_clips>m__17C()
extern "C"  AnimationClipPairU5BU5D_t3767720461* UnityEngine_AnimatorOverrideControllerGenerated_U3CAnimatorOverrideController_clipsU3Em__17C_m543267574 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimatorOverrideControllerGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_AnimatorOverrideControllerGenerated_ilo_getObject1_m3796935107 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_AnimatorOverrideControllerGenerated::ilo_getStringS2(System.Int32)
extern "C"  String_t* UnityEngine_AnimatorOverrideControllerGenerated_ilo_getStringS2_m3715118712 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_AnimatorOverrideControllerGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_AnimatorOverrideControllerGenerated_ilo_getObject3_m543267892 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimatorOverrideControllerGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_AnimatorOverrideControllerGenerated_ilo_setObject4_m3059540990 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimatorOverrideControllerGenerated::ilo_getArrayLength5(System.Int32)
extern "C"  int32_t UnityEngine_AnimatorOverrideControllerGenerated_ilo_getArrayLength5_m1935479689 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
