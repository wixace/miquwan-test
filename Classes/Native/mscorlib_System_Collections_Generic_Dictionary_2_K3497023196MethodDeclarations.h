﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3365640714MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1165806842(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3497023196 *, Dictionary_2_t1870263745 *, const MethodInfo*))KeyCollection__ctor_m3352204200_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.String>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3968260124(__this, ___item0, method) ((  void (*) (KeyCollection_t3497023196 *, uint16_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m912465070_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.String>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3785118867(__this, method) ((  void (*) (KeyCollection_t3497023196 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1149539749_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.String>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m135874670(__this, ___item0, method) ((  bool (*) (KeyCollection_t3497023196 *, uint16_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2322272028_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.String>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2606300499(__this, ___item0, method) ((  bool (*) (KeyCollection_t3497023196 *, uint16_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3542652801_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.String>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1615405903(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3497023196 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m674247265_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.String>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m190641285(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3497023196 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1545742999_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.String>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m851651392(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3497023196 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4109246930_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.String>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m312630351(__this, method) ((  bool (*) (KeyCollection_t3497023196 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1201483645_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.String>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2221779905(__this, method) ((  bool (*) (KeyCollection_t3497023196 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1305477231_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.String>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3504044845(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3497023196 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2500749915_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.String>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m814521263(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3497023196 *, UInt16U5BU5D_t801649474*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2041879133_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.String>::GetEnumerator()
#define KeyCollection_GetEnumerator_m2346096658(__this, method) ((  Enumerator_t2485199799  (*) (KeyCollection_t3497023196 *, const MethodInfo*))KeyCollection_GetEnumerator_m2071243712_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.String>::get_Count()
#define KeyCollection_get_Count_m1254278791(__this, method) ((  int32_t (*) (KeyCollection_t3497023196 *, const MethodInfo*))KeyCollection_get_Count_m2917785909_gshared)(__this, method)
