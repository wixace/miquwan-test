﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_beceeJetrunaw18
struct  M_beceeJetrunaw18_t3351917487  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_beceeJetrunaw18::_cawjahi
	int32_t ____cawjahi_0;
	// System.String GarbageiOS.M_beceeJetrunaw18::_bowwhooFoboudu
	String_t* ____bowwhooFoboudu_1;
	// System.UInt32 GarbageiOS.M_beceeJetrunaw18::_warwowgear
	uint32_t ____warwowgear_2;
	// System.UInt32 GarbageiOS.M_beceeJetrunaw18::_baheetem
	uint32_t ____baheetem_3;
	// System.String GarbageiOS.M_beceeJetrunaw18::_tenorcirCajoumur
	String_t* ____tenorcirCajoumur_4;
	// System.Boolean GarbageiOS.M_beceeJetrunaw18::_jorwe
	bool ____jorwe_5;
	// System.Boolean GarbageiOS.M_beceeJetrunaw18::_sada
	bool ____sada_6;
	// System.Int32 GarbageiOS.M_beceeJetrunaw18::_mearmee
	int32_t ____mearmee_7;
	// System.String GarbageiOS.M_beceeJetrunaw18::_mawhumeaJesasur
	String_t* ____mawhumeaJesasur_8;
	// System.Boolean GarbageiOS.M_beceeJetrunaw18::_koudrairHaisa
	bool ____koudrairHaisa_9;
	// System.Single GarbageiOS.M_beceeJetrunaw18::_teape
	float ____teape_10;
	// System.Boolean GarbageiOS.M_beceeJetrunaw18::_recoHafa
	bool ____recoHafa_11;
	// System.Int32 GarbageiOS.M_beceeJetrunaw18::_sawneviReha
	int32_t ____sawneviReha_12;
	// System.Boolean GarbageiOS.M_beceeJetrunaw18::_hererzearSurpoxe
	bool ____hererzearSurpoxe_13;
	// System.Int32 GarbageiOS.M_beceeJetrunaw18::_risaBallwoo
	int32_t ____risaBallwoo_14;
	// System.Single GarbageiOS.M_beceeJetrunaw18::_vulusas
	float ____vulusas_15;
	// System.String GarbageiOS.M_beceeJetrunaw18::_wixiPairbal
	String_t* ____wixiPairbal_16;

public:
	inline static int32_t get_offset_of__cawjahi_0() { return static_cast<int32_t>(offsetof(M_beceeJetrunaw18_t3351917487, ____cawjahi_0)); }
	inline int32_t get__cawjahi_0() const { return ____cawjahi_0; }
	inline int32_t* get_address_of__cawjahi_0() { return &____cawjahi_0; }
	inline void set__cawjahi_0(int32_t value)
	{
		____cawjahi_0 = value;
	}

	inline static int32_t get_offset_of__bowwhooFoboudu_1() { return static_cast<int32_t>(offsetof(M_beceeJetrunaw18_t3351917487, ____bowwhooFoboudu_1)); }
	inline String_t* get__bowwhooFoboudu_1() const { return ____bowwhooFoboudu_1; }
	inline String_t** get_address_of__bowwhooFoboudu_1() { return &____bowwhooFoboudu_1; }
	inline void set__bowwhooFoboudu_1(String_t* value)
	{
		____bowwhooFoboudu_1 = value;
		Il2CppCodeGenWriteBarrier(&____bowwhooFoboudu_1, value);
	}

	inline static int32_t get_offset_of__warwowgear_2() { return static_cast<int32_t>(offsetof(M_beceeJetrunaw18_t3351917487, ____warwowgear_2)); }
	inline uint32_t get__warwowgear_2() const { return ____warwowgear_2; }
	inline uint32_t* get_address_of__warwowgear_2() { return &____warwowgear_2; }
	inline void set__warwowgear_2(uint32_t value)
	{
		____warwowgear_2 = value;
	}

	inline static int32_t get_offset_of__baheetem_3() { return static_cast<int32_t>(offsetof(M_beceeJetrunaw18_t3351917487, ____baheetem_3)); }
	inline uint32_t get__baheetem_3() const { return ____baheetem_3; }
	inline uint32_t* get_address_of__baheetem_3() { return &____baheetem_3; }
	inline void set__baheetem_3(uint32_t value)
	{
		____baheetem_3 = value;
	}

	inline static int32_t get_offset_of__tenorcirCajoumur_4() { return static_cast<int32_t>(offsetof(M_beceeJetrunaw18_t3351917487, ____tenorcirCajoumur_4)); }
	inline String_t* get__tenorcirCajoumur_4() const { return ____tenorcirCajoumur_4; }
	inline String_t** get_address_of__tenorcirCajoumur_4() { return &____tenorcirCajoumur_4; }
	inline void set__tenorcirCajoumur_4(String_t* value)
	{
		____tenorcirCajoumur_4 = value;
		Il2CppCodeGenWriteBarrier(&____tenorcirCajoumur_4, value);
	}

	inline static int32_t get_offset_of__jorwe_5() { return static_cast<int32_t>(offsetof(M_beceeJetrunaw18_t3351917487, ____jorwe_5)); }
	inline bool get__jorwe_5() const { return ____jorwe_5; }
	inline bool* get_address_of__jorwe_5() { return &____jorwe_5; }
	inline void set__jorwe_5(bool value)
	{
		____jorwe_5 = value;
	}

	inline static int32_t get_offset_of__sada_6() { return static_cast<int32_t>(offsetof(M_beceeJetrunaw18_t3351917487, ____sada_6)); }
	inline bool get__sada_6() const { return ____sada_6; }
	inline bool* get_address_of__sada_6() { return &____sada_6; }
	inline void set__sada_6(bool value)
	{
		____sada_6 = value;
	}

	inline static int32_t get_offset_of__mearmee_7() { return static_cast<int32_t>(offsetof(M_beceeJetrunaw18_t3351917487, ____mearmee_7)); }
	inline int32_t get__mearmee_7() const { return ____mearmee_7; }
	inline int32_t* get_address_of__mearmee_7() { return &____mearmee_7; }
	inline void set__mearmee_7(int32_t value)
	{
		____mearmee_7 = value;
	}

	inline static int32_t get_offset_of__mawhumeaJesasur_8() { return static_cast<int32_t>(offsetof(M_beceeJetrunaw18_t3351917487, ____mawhumeaJesasur_8)); }
	inline String_t* get__mawhumeaJesasur_8() const { return ____mawhumeaJesasur_8; }
	inline String_t** get_address_of__mawhumeaJesasur_8() { return &____mawhumeaJesasur_8; }
	inline void set__mawhumeaJesasur_8(String_t* value)
	{
		____mawhumeaJesasur_8 = value;
		Il2CppCodeGenWriteBarrier(&____mawhumeaJesasur_8, value);
	}

	inline static int32_t get_offset_of__koudrairHaisa_9() { return static_cast<int32_t>(offsetof(M_beceeJetrunaw18_t3351917487, ____koudrairHaisa_9)); }
	inline bool get__koudrairHaisa_9() const { return ____koudrairHaisa_9; }
	inline bool* get_address_of__koudrairHaisa_9() { return &____koudrairHaisa_9; }
	inline void set__koudrairHaisa_9(bool value)
	{
		____koudrairHaisa_9 = value;
	}

	inline static int32_t get_offset_of__teape_10() { return static_cast<int32_t>(offsetof(M_beceeJetrunaw18_t3351917487, ____teape_10)); }
	inline float get__teape_10() const { return ____teape_10; }
	inline float* get_address_of__teape_10() { return &____teape_10; }
	inline void set__teape_10(float value)
	{
		____teape_10 = value;
	}

	inline static int32_t get_offset_of__recoHafa_11() { return static_cast<int32_t>(offsetof(M_beceeJetrunaw18_t3351917487, ____recoHafa_11)); }
	inline bool get__recoHafa_11() const { return ____recoHafa_11; }
	inline bool* get_address_of__recoHafa_11() { return &____recoHafa_11; }
	inline void set__recoHafa_11(bool value)
	{
		____recoHafa_11 = value;
	}

	inline static int32_t get_offset_of__sawneviReha_12() { return static_cast<int32_t>(offsetof(M_beceeJetrunaw18_t3351917487, ____sawneviReha_12)); }
	inline int32_t get__sawneviReha_12() const { return ____sawneviReha_12; }
	inline int32_t* get_address_of__sawneviReha_12() { return &____sawneviReha_12; }
	inline void set__sawneviReha_12(int32_t value)
	{
		____sawneviReha_12 = value;
	}

	inline static int32_t get_offset_of__hererzearSurpoxe_13() { return static_cast<int32_t>(offsetof(M_beceeJetrunaw18_t3351917487, ____hererzearSurpoxe_13)); }
	inline bool get__hererzearSurpoxe_13() const { return ____hererzearSurpoxe_13; }
	inline bool* get_address_of__hererzearSurpoxe_13() { return &____hererzearSurpoxe_13; }
	inline void set__hererzearSurpoxe_13(bool value)
	{
		____hererzearSurpoxe_13 = value;
	}

	inline static int32_t get_offset_of__risaBallwoo_14() { return static_cast<int32_t>(offsetof(M_beceeJetrunaw18_t3351917487, ____risaBallwoo_14)); }
	inline int32_t get__risaBallwoo_14() const { return ____risaBallwoo_14; }
	inline int32_t* get_address_of__risaBallwoo_14() { return &____risaBallwoo_14; }
	inline void set__risaBallwoo_14(int32_t value)
	{
		____risaBallwoo_14 = value;
	}

	inline static int32_t get_offset_of__vulusas_15() { return static_cast<int32_t>(offsetof(M_beceeJetrunaw18_t3351917487, ____vulusas_15)); }
	inline float get__vulusas_15() const { return ____vulusas_15; }
	inline float* get_address_of__vulusas_15() { return &____vulusas_15; }
	inline void set__vulusas_15(float value)
	{
		____vulusas_15 = value;
	}

	inline static int32_t get_offset_of__wixiPairbal_16() { return static_cast<int32_t>(offsetof(M_beceeJetrunaw18_t3351917487, ____wixiPairbal_16)); }
	inline String_t* get__wixiPairbal_16() const { return ____wixiPairbal_16; }
	inline String_t** get_address_of__wixiPairbal_16() { return &____wixiPairbal_16; }
	inline void set__wixiPairbal_16(String_t* value)
	{
		____wixiPairbal_16 = value;
		Il2CppCodeGenWriteBarrier(&____wixiPairbal_16, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
