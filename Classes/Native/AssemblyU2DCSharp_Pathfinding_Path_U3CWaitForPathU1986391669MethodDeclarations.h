﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Path/<WaitForPath>c__IteratorE
struct U3CWaitForPathU3Ec__IteratorE_t1986391669;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Path/<WaitForPath>c__IteratorE::.ctor()
extern "C"  void U3CWaitForPathU3Ec__IteratorE__ctor_m4156276166 (U3CWaitForPathU3Ec__IteratorE_t1986391669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.Path/<WaitForPath>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CWaitForPathU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m928398742 (U3CWaitForPathU3Ec__IteratorE_t1986391669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pathfinding.Path/<WaitForPath>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CWaitForPathU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m2624728362 (U3CWaitForPathU3Ec__IteratorE_t1986391669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Path/<WaitForPath>c__IteratorE::MoveNext()
extern "C"  bool U3CWaitForPathU3Ec__IteratorE_MoveNext_m3239655766 (U3CWaitForPathU3Ec__IteratorE_t1986391669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path/<WaitForPath>c__IteratorE::Dispose()
extern "C"  void U3CWaitForPathU3Ec__IteratorE_Dispose_m4055060611 (U3CWaitForPathU3Ec__IteratorE_t1986391669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Path/<WaitForPath>c__IteratorE::Reset()
extern "C"  void U3CWaitForPathU3Ec__IteratorE_Reset_m1802709107 (U3CWaitForPathU3Ec__IteratorE_t1986391669 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
