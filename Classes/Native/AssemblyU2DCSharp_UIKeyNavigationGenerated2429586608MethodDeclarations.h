﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIKeyNavigationGenerated
struct UIKeyNavigationGenerated_t2429586608;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UIKeyNavigationGenerated::.ctor()
extern "C"  void UIKeyNavigationGenerated__ctor_m4140582891 (UIKeyNavigationGenerated_t2429586608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIKeyNavigationGenerated::UIKeyNavigation_UIKeyNavigation1(JSVCall,System.Int32)
extern "C"  bool UIKeyNavigationGenerated_UIKeyNavigation_UIKeyNavigation1_m4100475439 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyNavigationGenerated::UIKeyNavigation_list(JSVCall)
extern "C"  void UIKeyNavigationGenerated_UIKeyNavigation_list_m1116680240 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyNavigationGenerated::UIKeyNavigation_constraint(JSVCall)
extern "C"  void UIKeyNavigationGenerated_UIKeyNavigation_constraint_m2893151185 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyNavigationGenerated::UIKeyNavigation_onUp(JSVCall)
extern "C"  void UIKeyNavigationGenerated_UIKeyNavigation_onUp_m4036872052 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyNavigationGenerated::UIKeyNavigation_onDown(JSVCall)
extern "C"  void UIKeyNavigationGenerated_UIKeyNavigation_onDown_m571974413 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyNavigationGenerated::UIKeyNavigation_onLeft(JSVCall)
extern "C"  void UIKeyNavigationGenerated_UIKeyNavigation_onLeft_m533211464 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyNavigationGenerated::UIKeyNavigation_onRight(JSVCall)
extern "C"  void UIKeyNavigationGenerated_UIKeyNavigation_onRight_m2572092449 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyNavigationGenerated::UIKeyNavigation_onClick(JSVCall)
extern "C"  void UIKeyNavigationGenerated_UIKeyNavigation_onClick_m3106937397 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyNavigationGenerated::UIKeyNavigation_startsSelected(JSVCall)
extern "C"  void UIKeyNavigationGenerated_UIKeyNavigation_startsSelected_m1319867522 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyNavigationGenerated::__Register()
extern "C"  void UIKeyNavigationGenerated___Register_m2272030652 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIKeyNavigationGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UIKeyNavigationGenerated_ilo_attachFinalizerObject1_m1596515668 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIKeyNavigationGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIKeyNavigationGenerated_ilo_getObject2_m1411142309 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyNavigationGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void UIKeyNavigationGenerated_ilo_setBooleanS3_m2548742490 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
