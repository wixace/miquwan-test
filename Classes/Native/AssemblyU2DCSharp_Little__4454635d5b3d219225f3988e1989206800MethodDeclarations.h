﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._4454635d5b3d219225f3988edf52eb86
struct _4454635d5b3d219225f3988edf52eb86_t1989206800;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._4454635d5b3d219225f3988edf52eb86::.ctor()
extern "C"  void _4454635d5b3d219225f3988edf52eb86__ctor_m186168605 (_4454635d5b3d219225f3988edf52eb86_t1989206800 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._4454635d5b3d219225f3988edf52eb86::_4454635d5b3d219225f3988edf52eb86m2(System.Int32)
extern "C"  int32_t _4454635d5b3d219225f3988edf52eb86__4454635d5b3d219225f3988edf52eb86m2_m929281945 (_4454635d5b3d219225f3988edf52eb86_t1989206800 * __this, int32_t ____4454635d5b3d219225f3988edf52eb86a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._4454635d5b3d219225f3988edf52eb86::_4454635d5b3d219225f3988edf52eb86m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _4454635d5b3d219225f3988edf52eb86__4454635d5b3d219225f3988edf52eb86m_m2269622077 (_4454635d5b3d219225f3988edf52eb86_t1989206800 * __this, int32_t ____4454635d5b3d219225f3988edf52eb86a0, int32_t ____4454635d5b3d219225f3988edf52eb86221, int32_t ____4454635d5b3d219225f3988edf52eb86c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
