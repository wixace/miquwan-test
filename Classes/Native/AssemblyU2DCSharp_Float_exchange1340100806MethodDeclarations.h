﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_exchange
struct Float_exchange_t1340100806;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// FloatTextUnit
struct FloatTextUnit_t2362298029;
// EventDelegate/Callback
struct Callback_t1094463061;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_UITweener_Method1078127180.h"

// System.Void Float_exchange::.ctor()
extern "C"  void Float_exchange__ctor_m3596417685 (Float_exchange_t1340100806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_exchange::FloatID()
extern "C"  int32_t Float_exchange_FloatID_m2456599137 (Float_exchange_t1340100806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_exchange::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_exchange_OnAwake_m868069201 (Float_exchange_t1340100806 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_exchange::OnDestroy()
extern "C"  void Float_exchange_OnDestroy_m2740288782 (Float_exchange_t1340100806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_exchange::Init()
extern "C"  void Float_exchange_Init_m483192095 (Float_exchange_t1340100806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_exchange::SetFile(System.Object[])
extern "C"  void Float_exchange_SetFile_m3382412001 (Float_exchange_t1340100806 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_exchange::ilo_OnAwake1(FloatTextUnit,UnityEngine.GameObject)
extern "C"  void Float_exchange_ilo_OnAwake1_m1980022406 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, GameObject_t3674682005 * ___viewGO1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_exchange::ilo_MoveScale2(FloatTextUnit,System.Single,System.Single,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void Float_exchange_ilo_MoveScale2_m327003663 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, float ___from1, float ___to2, float ___duration3, float ___delay4, Callback_t1094463061 * ___call5, GameObject_t3674682005 * ___go6, int32_t ___method7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_exchange::ilo_Play3(FloatTextUnit)
extern "C"  void Float_exchange_ilo_Play3_m1476914726 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
