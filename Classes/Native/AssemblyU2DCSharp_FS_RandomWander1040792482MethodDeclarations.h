﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FS_RandomWander
struct FS_RandomWander_t1040792482;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FS_RandomWander1040792482.h"

// System.Void FS_RandomWander::.ctor()
extern "C"  void FS_RandomWander__ctor_m3519709705 (FS_RandomWander_t1040792482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_RandomWander::Awake()
extern "C"  void FS_RandomWander_Awake_m3757314924 (FS_RandomWander_t1040792482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_RandomWander::FixedUpdate()
extern "C"  void FS_RandomWander_FixedUpdate_m1854076100 (FS_RandomWander_t1040792482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FS_RandomWander::NewHeading()
extern "C"  Il2CppObject * FS_RandomWander_NewHeading_m2654560533 (FS_RandomWander_t1040792482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_RandomWander::NewHeadingRoutine()
extern "C"  void FS_RandomWander_NewHeadingRoutine_m3518631881 (FS_RandomWander_t1040792482 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator FS_RandomWander::ilo_NewHeading1(FS_RandomWander)
extern "C"  Il2CppObject * FS_RandomWander_ilo_NewHeading1_m1509253029 (Il2CppObject * __this /* static, unused */, FS_RandomWander_t1040792482 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_RandomWander::ilo_NewHeadingRoutine2(FS_RandomWander)
extern "C"  void FS_RandomWander_ilo_NewHeadingRoutine2_m2544670642 (Il2CppObject * __this /* static, unused */, FS_RandomWander_t1040792482 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
