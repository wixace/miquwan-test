﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CombatEntity
struct CombatEntity_t684137495;
// Monster
struct Monster_t2901270458;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Monster/<OnTriggerFun>c__AnonStorey152
struct  U3COnTriggerFunU3Ec__AnonStorey152_t3929677528  : public Il2CppObject
{
public:
	// CombatEntity Monster/<OnTriggerFun>c__AnonStorey152::main
	CombatEntity_t684137495 * ___main_0;
	// Monster Monster/<OnTriggerFun>c__AnonStorey152::<>f__this
	Monster_t2901270458 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_main_0() { return static_cast<int32_t>(offsetof(U3COnTriggerFunU3Ec__AnonStorey152_t3929677528, ___main_0)); }
	inline CombatEntity_t684137495 * get_main_0() const { return ___main_0; }
	inline CombatEntity_t684137495 ** get_address_of_main_0() { return &___main_0; }
	inline void set_main_0(CombatEntity_t684137495 * value)
	{
		___main_0 = value;
		Il2CppCodeGenWriteBarrier(&___main_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3COnTriggerFunU3Ec__AnonStorey152_t3929677528, ___U3CU3Ef__this_1)); }
	inline Monster_t2901270458 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline Monster_t2901270458 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(Monster_t2901270458 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
