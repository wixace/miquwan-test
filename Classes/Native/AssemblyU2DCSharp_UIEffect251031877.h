﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UIWidget
struct UIWidget_t769069560;
// System.Collections.Generic.List`1<UnityEngine.Material>
struct List_1_t943818363;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEffect
struct  UIEffect_t251031877  : public MonoBehaviour_t667441552
{
public:
	// UIWidget UIEffect::uiWid
	UIWidget_t769069560 * ___uiWid_2;
	// System.Collections.Generic.List`1<UnityEngine.Material> UIEffect::matList
	List_1_t943818363 * ___matList_3;

public:
	inline static int32_t get_offset_of_uiWid_2() { return static_cast<int32_t>(offsetof(UIEffect_t251031877, ___uiWid_2)); }
	inline UIWidget_t769069560 * get_uiWid_2() const { return ___uiWid_2; }
	inline UIWidget_t769069560 ** get_address_of_uiWid_2() { return &___uiWid_2; }
	inline void set_uiWid_2(UIWidget_t769069560 * value)
	{
		___uiWid_2 = value;
		Il2CppCodeGenWriteBarrier(&___uiWid_2, value);
	}

	inline static int32_t get_offset_of_matList_3() { return static_cast<int32_t>(offsetof(UIEffect_t251031877, ___matList_3)); }
	inline List_1_t943818363 * get_matList_3() const { return ___matList_3; }
	inline List_1_t943818363 ** get_address_of_matList_3() { return &___matList_3; }
	inline void set_matList_3(List_1_t943818363 * value)
	{
		___matList_3 = value;
		Il2CppCodeGenWriteBarrier(&___matList_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
