﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_MatchTargetWeightMaskGenerated
struct UnityEngine_MatchTargetWeightMaskGenerated_t497864765;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_MatchTargetWeightMaskGenerated::.ctor()
extern "C"  void UnityEngine_MatchTargetWeightMaskGenerated__ctor_m2231524606 (UnityEngine_MatchTargetWeightMaskGenerated_t497864765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MatchTargetWeightMaskGenerated::.cctor()
extern "C"  void UnityEngine_MatchTargetWeightMaskGenerated__cctor_m4270657135 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MatchTargetWeightMaskGenerated::MatchTargetWeightMask_MatchTargetWeightMask1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MatchTargetWeightMaskGenerated_MatchTargetWeightMask_MatchTargetWeightMask1_m881523590 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MatchTargetWeightMaskGenerated::MatchTargetWeightMask_MatchTargetWeightMask2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MatchTargetWeightMaskGenerated_MatchTargetWeightMask_MatchTargetWeightMask2_m2126288071 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MatchTargetWeightMaskGenerated::MatchTargetWeightMask_positionXYZWeight(JSVCall)
extern "C"  void UnityEngine_MatchTargetWeightMaskGenerated_MatchTargetWeightMask_positionXYZWeight_m64827144 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MatchTargetWeightMaskGenerated::MatchTargetWeightMask_rotationWeight(JSVCall)
extern "C"  void UnityEngine_MatchTargetWeightMaskGenerated_MatchTargetWeightMask_rotationWeight_m772528166 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MatchTargetWeightMaskGenerated::__Register()
extern "C"  void UnityEngine_MatchTargetWeightMaskGenerated___Register_m1629982345 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MatchTargetWeightMaskGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_MatchTargetWeightMaskGenerated_ilo_getObject1_m323188564 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MatchTargetWeightMaskGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_MatchTargetWeightMaskGenerated_ilo_attachFinalizerObject2_m2394088418 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_MatchTargetWeightMaskGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_MatchTargetWeightMaskGenerated_ilo_getSingle3_m725910635 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MatchTargetWeightMaskGenerated::ilo_setVector3S4(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_MatchTargetWeightMaskGenerated_ilo_setVector3S4_m1757078988 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MatchTargetWeightMaskGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void UnityEngine_MatchTargetWeightMaskGenerated_ilo_setSingle5_m3231759546 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MatchTargetWeightMaskGenerated::ilo_changeJSObj6(System.Int32,System.Object)
extern "C"  void UnityEngine_MatchTargetWeightMaskGenerated_ilo_changeJSObj6_m1931027888 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
