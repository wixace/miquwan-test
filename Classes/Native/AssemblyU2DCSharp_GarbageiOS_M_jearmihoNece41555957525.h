﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_jearmihoNece41
struct  M_jearmihoNece41_t555957525  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_jearmihoNece41::_herewiDairmou
	String_t* ____herewiDairmou_0;
	// System.String GarbageiOS.M_jearmihoNece41::_rouhowPayko
	String_t* ____rouhowPayko_1;
	// System.String GarbageiOS.M_jearmihoNece41::_wowmojuTerair
	String_t* ____wowmojuTerair_2;
	// System.Int32 GarbageiOS.M_jearmihoNece41::_sowsigaJalall
	int32_t ____sowsigaJalall_3;
	// System.Int32 GarbageiOS.M_jearmihoNece41::_stalzawnurYiwhairtrall
	int32_t ____stalzawnurYiwhairtrall_4;
	// System.String GarbageiOS.M_jearmihoNece41::_nerestawair
	String_t* ____nerestawair_5;
	// System.Int32 GarbageiOS.M_jearmihoNece41::_stearcimeTima
	int32_t ____stearcimeTima_6;
	// System.Boolean GarbageiOS.M_jearmihoNece41::_wurtadeSteehiwou
	bool ____wurtadeSteehiwou_7;
	// System.Single GarbageiOS.M_jearmihoNece41::_nairdre
	float ____nairdre_8;
	// System.Boolean GarbageiOS.M_jearmihoNece41::_yewhasmoo
	bool ____yewhasmoo_9;
	// System.String GarbageiOS.M_jearmihoNece41::_cheetrijear
	String_t* ____cheetrijear_10;
	// System.Single GarbageiOS.M_jearmihoNece41::_jerekalltaiWefa
	float ____jerekalltaiWefa_11;
	// System.Single GarbageiOS.M_jearmihoNece41::_sifumou
	float ____sifumou_12;
	// System.Single GarbageiOS.M_jearmihoNece41::_jawxeLearlai
	float ____jawxeLearlai_13;
	// System.String GarbageiOS.M_jearmihoNece41::_ralljeeQeltemgee
	String_t* ____ralljeeQeltemgee_14;
	// System.Single GarbageiOS.M_jearmihoNece41::_yaga
	float ____yaga_15;
	// System.Single GarbageiOS.M_jearmihoNece41::_zerelaNekuka
	float ____zerelaNekuka_16;
	// System.Boolean GarbageiOS.M_jearmihoNece41::_cursujair
	bool ____cursujair_17;
	// System.Boolean GarbageiOS.M_jearmihoNece41::_rape
	bool ____rape_18;
	// System.Boolean GarbageiOS.M_jearmihoNece41::_pirouWhinee
	bool ____pirouWhinee_19;
	// System.Int32 GarbageiOS.M_jearmihoNece41::_coudrairLise
	int32_t ____coudrairLise_20;
	// System.String GarbageiOS.M_jearmihoNece41::_chisa
	String_t* ____chisa_21;

public:
	inline static int32_t get_offset_of__herewiDairmou_0() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____herewiDairmou_0)); }
	inline String_t* get__herewiDairmou_0() const { return ____herewiDairmou_0; }
	inline String_t** get_address_of__herewiDairmou_0() { return &____herewiDairmou_0; }
	inline void set__herewiDairmou_0(String_t* value)
	{
		____herewiDairmou_0 = value;
		Il2CppCodeGenWriteBarrier(&____herewiDairmou_0, value);
	}

	inline static int32_t get_offset_of__rouhowPayko_1() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____rouhowPayko_1)); }
	inline String_t* get__rouhowPayko_1() const { return ____rouhowPayko_1; }
	inline String_t** get_address_of__rouhowPayko_1() { return &____rouhowPayko_1; }
	inline void set__rouhowPayko_1(String_t* value)
	{
		____rouhowPayko_1 = value;
		Il2CppCodeGenWriteBarrier(&____rouhowPayko_1, value);
	}

	inline static int32_t get_offset_of__wowmojuTerair_2() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____wowmojuTerair_2)); }
	inline String_t* get__wowmojuTerair_2() const { return ____wowmojuTerair_2; }
	inline String_t** get_address_of__wowmojuTerair_2() { return &____wowmojuTerair_2; }
	inline void set__wowmojuTerair_2(String_t* value)
	{
		____wowmojuTerair_2 = value;
		Il2CppCodeGenWriteBarrier(&____wowmojuTerair_2, value);
	}

	inline static int32_t get_offset_of__sowsigaJalall_3() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____sowsigaJalall_3)); }
	inline int32_t get__sowsigaJalall_3() const { return ____sowsigaJalall_3; }
	inline int32_t* get_address_of__sowsigaJalall_3() { return &____sowsigaJalall_3; }
	inline void set__sowsigaJalall_3(int32_t value)
	{
		____sowsigaJalall_3 = value;
	}

	inline static int32_t get_offset_of__stalzawnurYiwhairtrall_4() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____stalzawnurYiwhairtrall_4)); }
	inline int32_t get__stalzawnurYiwhairtrall_4() const { return ____stalzawnurYiwhairtrall_4; }
	inline int32_t* get_address_of__stalzawnurYiwhairtrall_4() { return &____stalzawnurYiwhairtrall_4; }
	inline void set__stalzawnurYiwhairtrall_4(int32_t value)
	{
		____stalzawnurYiwhairtrall_4 = value;
	}

	inline static int32_t get_offset_of__nerestawair_5() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____nerestawair_5)); }
	inline String_t* get__nerestawair_5() const { return ____nerestawair_5; }
	inline String_t** get_address_of__nerestawair_5() { return &____nerestawair_5; }
	inline void set__nerestawair_5(String_t* value)
	{
		____nerestawair_5 = value;
		Il2CppCodeGenWriteBarrier(&____nerestawair_5, value);
	}

	inline static int32_t get_offset_of__stearcimeTima_6() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____stearcimeTima_6)); }
	inline int32_t get__stearcimeTima_6() const { return ____stearcimeTima_6; }
	inline int32_t* get_address_of__stearcimeTima_6() { return &____stearcimeTima_6; }
	inline void set__stearcimeTima_6(int32_t value)
	{
		____stearcimeTima_6 = value;
	}

	inline static int32_t get_offset_of__wurtadeSteehiwou_7() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____wurtadeSteehiwou_7)); }
	inline bool get__wurtadeSteehiwou_7() const { return ____wurtadeSteehiwou_7; }
	inline bool* get_address_of__wurtadeSteehiwou_7() { return &____wurtadeSteehiwou_7; }
	inline void set__wurtadeSteehiwou_7(bool value)
	{
		____wurtadeSteehiwou_7 = value;
	}

	inline static int32_t get_offset_of__nairdre_8() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____nairdre_8)); }
	inline float get__nairdre_8() const { return ____nairdre_8; }
	inline float* get_address_of__nairdre_8() { return &____nairdre_8; }
	inline void set__nairdre_8(float value)
	{
		____nairdre_8 = value;
	}

	inline static int32_t get_offset_of__yewhasmoo_9() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____yewhasmoo_9)); }
	inline bool get__yewhasmoo_9() const { return ____yewhasmoo_9; }
	inline bool* get_address_of__yewhasmoo_9() { return &____yewhasmoo_9; }
	inline void set__yewhasmoo_9(bool value)
	{
		____yewhasmoo_9 = value;
	}

	inline static int32_t get_offset_of__cheetrijear_10() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____cheetrijear_10)); }
	inline String_t* get__cheetrijear_10() const { return ____cheetrijear_10; }
	inline String_t** get_address_of__cheetrijear_10() { return &____cheetrijear_10; }
	inline void set__cheetrijear_10(String_t* value)
	{
		____cheetrijear_10 = value;
		Il2CppCodeGenWriteBarrier(&____cheetrijear_10, value);
	}

	inline static int32_t get_offset_of__jerekalltaiWefa_11() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____jerekalltaiWefa_11)); }
	inline float get__jerekalltaiWefa_11() const { return ____jerekalltaiWefa_11; }
	inline float* get_address_of__jerekalltaiWefa_11() { return &____jerekalltaiWefa_11; }
	inline void set__jerekalltaiWefa_11(float value)
	{
		____jerekalltaiWefa_11 = value;
	}

	inline static int32_t get_offset_of__sifumou_12() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____sifumou_12)); }
	inline float get__sifumou_12() const { return ____sifumou_12; }
	inline float* get_address_of__sifumou_12() { return &____sifumou_12; }
	inline void set__sifumou_12(float value)
	{
		____sifumou_12 = value;
	}

	inline static int32_t get_offset_of__jawxeLearlai_13() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____jawxeLearlai_13)); }
	inline float get__jawxeLearlai_13() const { return ____jawxeLearlai_13; }
	inline float* get_address_of__jawxeLearlai_13() { return &____jawxeLearlai_13; }
	inline void set__jawxeLearlai_13(float value)
	{
		____jawxeLearlai_13 = value;
	}

	inline static int32_t get_offset_of__ralljeeQeltemgee_14() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____ralljeeQeltemgee_14)); }
	inline String_t* get__ralljeeQeltemgee_14() const { return ____ralljeeQeltemgee_14; }
	inline String_t** get_address_of__ralljeeQeltemgee_14() { return &____ralljeeQeltemgee_14; }
	inline void set__ralljeeQeltemgee_14(String_t* value)
	{
		____ralljeeQeltemgee_14 = value;
		Il2CppCodeGenWriteBarrier(&____ralljeeQeltemgee_14, value);
	}

	inline static int32_t get_offset_of__yaga_15() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____yaga_15)); }
	inline float get__yaga_15() const { return ____yaga_15; }
	inline float* get_address_of__yaga_15() { return &____yaga_15; }
	inline void set__yaga_15(float value)
	{
		____yaga_15 = value;
	}

	inline static int32_t get_offset_of__zerelaNekuka_16() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____zerelaNekuka_16)); }
	inline float get__zerelaNekuka_16() const { return ____zerelaNekuka_16; }
	inline float* get_address_of__zerelaNekuka_16() { return &____zerelaNekuka_16; }
	inline void set__zerelaNekuka_16(float value)
	{
		____zerelaNekuka_16 = value;
	}

	inline static int32_t get_offset_of__cursujair_17() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____cursujair_17)); }
	inline bool get__cursujair_17() const { return ____cursujair_17; }
	inline bool* get_address_of__cursujair_17() { return &____cursujair_17; }
	inline void set__cursujair_17(bool value)
	{
		____cursujair_17 = value;
	}

	inline static int32_t get_offset_of__rape_18() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____rape_18)); }
	inline bool get__rape_18() const { return ____rape_18; }
	inline bool* get_address_of__rape_18() { return &____rape_18; }
	inline void set__rape_18(bool value)
	{
		____rape_18 = value;
	}

	inline static int32_t get_offset_of__pirouWhinee_19() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____pirouWhinee_19)); }
	inline bool get__pirouWhinee_19() const { return ____pirouWhinee_19; }
	inline bool* get_address_of__pirouWhinee_19() { return &____pirouWhinee_19; }
	inline void set__pirouWhinee_19(bool value)
	{
		____pirouWhinee_19 = value;
	}

	inline static int32_t get_offset_of__coudrairLise_20() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____coudrairLise_20)); }
	inline int32_t get__coudrairLise_20() const { return ____coudrairLise_20; }
	inline int32_t* get_address_of__coudrairLise_20() { return &____coudrairLise_20; }
	inline void set__coudrairLise_20(int32_t value)
	{
		____coudrairLise_20 = value;
	}

	inline static int32_t get_offset_of__chisa_21() { return static_cast<int32_t>(offsetof(M_jearmihoNece41_t555957525, ____chisa_21)); }
	inline String_t* get__chisa_21() const { return ____chisa_21; }
	inline String_t** get_address_of__chisa_21() { return &____chisa_21; }
	inline void set__chisa_21(String_t* value)
	{
		____chisa_21 = value;
		Il2CppCodeGenWriteBarrier(&____chisa_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
