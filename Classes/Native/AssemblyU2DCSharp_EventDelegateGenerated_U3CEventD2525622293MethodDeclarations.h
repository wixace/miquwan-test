﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EventDelegateGenerated/<EventDelegate_Add_GetDelegate_member7_arg1>c__AnonStorey5E
struct U3CEventDelegate_Add_GetDelegate_member7_arg1U3Ec__AnonStorey5E_t2525622293;

#include "codegen/il2cpp-codegen.h"

// System.Void EventDelegateGenerated/<EventDelegate_Add_GetDelegate_member7_arg1>c__AnonStorey5E::.ctor()
extern "C"  void U3CEventDelegate_Add_GetDelegate_member7_arg1U3Ec__AnonStorey5E__ctor_m570660902 (U3CEventDelegate_Add_GetDelegate_member7_arg1U3Ec__AnonStorey5E_t2525622293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventDelegateGenerated/<EventDelegate_Add_GetDelegate_member7_arg1>c__AnonStorey5E::<>m__48()
extern "C"  void U3CEventDelegate_Add_GetDelegate_member7_arg1U3Ec__AnonStorey5E_U3CU3Em__48_m957449971 (U3CEventDelegate_Add_GetDelegate_member7_arg1U3Ec__AnonStorey5E_t2525622293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
