﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_hootallhowNuhouxow214
struct M_hootallhowNuhouxow214_t3661699681;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_hootallhowNuhouxow23661699681.h"

// System.Void GarbageiOS.M_hootallhowNuhouxow214::.ctor()
extern "C"  void M_hootallhowNuhouxow214__ctor_m3957047330 (M_hootallhowNuhouxow214_t3661699681 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hootallhowNuhouxow214::M_teebe0(System.String[],System.Int32)
extern "C"  void M_hootallhowNuhouxow214_M_teebe0_m3007908644 (M_hootallhowNuhouxow214_t3661699681 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hootallhowNuhouxow214::M_rairowCamay1(System.String[],System.Int32)
extern "C"  void M_hootallhowNuhouxow214_M_rairowCamay1_m3149340117 (M_hootallhowNuhouxow214_t3661699681 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hootallhowNuhouxow214::M_lasiRenerenem2(System.String[],System.Int32)
extern "C"  void M_hootallhowNuhouxow214_M_lasiRenerenem2_m1994096079 (M_hootallhowNuhouxow214_t3661699681 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hootallhowNuhouxow214::M_henallCismou3(System.String[],System.Int32)
extern "C"  void M_hootallhowNuhouxow214_M_henallCismou3_m2180689634 (M_hootallhowNuhouxow214_t3661699681 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hootallhowNuhouxow214::M_goodearfeGirwhallda4(System.String[],System.Int32)
extern "C"  void M_hootallhowNuhouxow214_M_goodearfeGirwhallda4_m2909777034 (M_hootallhowNuhouxow214_t3661699681 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hootallhowNuhouxow214::M_mepur5(System.String[],System.Int32)
extern "C"  void M_hootallhowNuhouxow214_M_mepur5_m3488483723 (M_hootallhowNuhouxow214_t3661699681 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hootallhowNuhouxow214::M_tearsor6(System.String[],System.Int32)
extern "C"  void M_hootallhowNuhouxow214_M_tearsor6_m2092492717 (M_hootallhowNuhouxow214_t3661699681 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hootallhowNuhouxow214::M_soxeaWaicaysi7(System.String[],System.Int32)
extern "C"  void M_hootallhowNuhouxow214_M_soxeaWaicaysi7_m543035288 (M_hootallhowNuhouxow214_t3661699681 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hootallhowNuhouxow214::M_leecelkasJotrall8(System.String[],System.Int32)
extern "C"  void M_hootallhowNuhouxow214_M_leecelkasJotrall8_m1479096286 (M_hootallhowNuhouxow214_t3661699681 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hootallhowNuhouxow214::ilo_M_lasiRenerenem21(GarbageiOS.M_hootallhowNuhouxow214,System.String[],System.Int32)
extern "C"  void M_hootallhowNuhouxow214_ilo_M_lasiRenerenem21_m1343261870 (Il2CppObject * __this /* static, unused */, M_hootallhowNuhouxow214_t3661699681 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hootallhowNuhouxow214::ilo_M_goodearfeGirwhallda42(GarbageiOS.M_hootallhowNuhouxow214,System.String[],System.Int32)
extern "C"  void M_hootallhowNuhouxow214_ilo_M_goodearfeGirwhallda42_m171343816 (Il2CppObject * __this /* static, unused */, M_hootallhowNuhouxow214_t3661699681 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
