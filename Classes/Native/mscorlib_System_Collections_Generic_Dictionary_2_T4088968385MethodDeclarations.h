﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T2949220623MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m760570096(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t4088968385 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m641310834_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3748996300(__this, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t4088968385 *, TriangulationPoint_t3810082933 *, int32_t, const MethodInfo*))Transform_1_Invoke_m3922456586_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m687771563(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t4088968385 *, TriangulationPoint_t3810082933 *, int32_t, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m2253318505_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m2095498302(__this, ___result0, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t4088968385 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m2079925824_gshared)(__this, ___result0, method)
