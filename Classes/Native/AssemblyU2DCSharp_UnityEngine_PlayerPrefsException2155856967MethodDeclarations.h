﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_PlayerPrefsExceptionGenerated
struct UnityEngine_PlayerPrefsExceptionGenerated_t2155856967;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_PlayerPrefsExceptionGenerated::.ctor()
extern "C"  void UnityEngine_PlayerPrefsExceptionGenerated__ctor_m1556648196 (UnityEngine_PlayerPrefsExceptionGenerated_t2155856967 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlayerPrefsExceptionGenerated::PlayerPrefsException_PlayerPrefsException1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlayerPrefsExceptionGenerated_PlayerPrefsException_PlayerPrefsException1_m3817532270 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PlayerPrefsExceptionGenerated::__Register()
extern "C"  void UnityEngine_PlayerPrefsExceptionGenerated___Register_m996077123 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlayerPrefsExceptionGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_PlayerPrefsExceptionGenerated_ilo_attachFinalizerObject1_m3441696371 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PlayerPrefsExceptionGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_PlayerPrefsExceptionGenerated_ilo_addJSCSRel2_m1829952065 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
