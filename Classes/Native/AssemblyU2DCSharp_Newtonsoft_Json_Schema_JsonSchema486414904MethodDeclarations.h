﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema
struct TypeSchema_t486414904;
// System.Type
struct Type_t;
// Newtonsoft.Json.Schema.JsonSchema
struct JsonSchema_t460567603;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Schema_JsonSchema460567603.h"

// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema::.ctor(System.Type,Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void TypeSchema__ctor_m3366031153 (TypeSchema_t486414904 * __this, Type_t * ___type0, JsonSchema_t460567603 * ___schema1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema::get_Type()
extern "C"  Type_t * TypeSchema_get_Type_m2755959450 (TypeSchema_t486414904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema::set_Type(System.Type)
extern "C"  void TypeSchema_set_Type_m226775697 (TypeSchema_t486414904 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Schema.JsonSchema Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema::get_Schema()
extern "C"  JsonSchema_t460567603 * TypeSchema_get_Schema_m1275362409 (TypeSchema_t486414904 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Schema.JsonSchemaGenerator/TypeSchema::set_Schema(Newtonsoft.Json.Schema.JsonSchema)
extern "C"  void TypeSchema_set_Schema_m2796549538 (TypeSchema_t486414904 * __this, JsonSchema_t460567603 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
