﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_mirsi131
struct  M_mirsi131_t1376269555  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_mirsi131::_fariniYihem
	bool ____fariniYihem_0;
	// System.UInt32 GarbageiOS.M_mirsi131::_fearvearmiNosisdral
	uint32_t ____fearvearmiNosisdral_1;
	// System.String GarbageiOS.M_mirsi131::_canotaiPorjo
	String_t* ____canotaiPorjo_2;
	// System.Single GarbageiOS.M_mirsi131::_sejaircai
	float ____sejaircai_3;
	// System.Single GarbageiOS.M_mirsi131::_royir
	float ____royir_4;
	// System.Int32 GarbageiOS.M_mirsi131::_cokeaTirtea
	int32_t ____cokeaTirtea_5;
	// System.Int32 GarbageiOS.M_mirsi131::_seyeexelCidor
	int32_t ____seyeexelCidor_6;
	// System.UInt32 GarbageiOS.M_mirsi131::_hearmalHersego
	uint32_t ____hearmalHersego_7;
	// System.Single GarbageiOS.M_mirsi131::_faryuNarsair
	float ____faryuNarsair_8;
	// System.Boolean GarbageiOS.M_mirsi131::_tabiTaiser
	bool ____tabiTaiser_9;
	// System.Single GarbageiOS.M_mirsi131::_drolem
	float ____drolem_10;
	// System.Single GarbageiOS.M_mirsi131::_fudisTerkereyere
	float ____fudisTerkereyere_11;
	// System.UInt32 GarbageiOS.M_mirsi131::_talgooperQejem
	uint32_t ____talgooperQejem_12;
	// System.Int32 GarbageiOS.M_mirsi131::_kayki
	int32_t ____kayki_13;
	// System.Boolean GarbageiOS.M_mirsi131::_stawnouChaspawir
	bool ____stawnouChaspawir_14;
	// System.Single GarbageiOS.M_mirsi131::_drepai
	float ____drepai_15;
	// System.Int32 GarbageiOS.M_mirsi131::_boukou
	int32_t ____boukou_16;
	// System.String GarbageiOS.M_mirsi131::_cawirlair
	String_t* ____cawirlair_17;
	// System.Single GarbageiOS.M_mirsi131::_legurHearpeejaw
	float ____legurHearpeejaw_18;
	// System.String GarbageiOS.M_mirsi131::_nasnere
	String_t* ____nasnere_19;
	// System.Int32 GarbageiOS.M_mirsi131::_whoutellir
	int32_t ____whoutellir_20;
	// System.UInt32 GarbageiOS.M_mirsi131::_fawbismou
	uint32_t ____fawbismou_21;
	// System.Single GarbageiOS.M_mirsi131::_sedris
	float ____sedris_22;
	// System.UInt32 GarbageiOS.M_mirsi131::_walreekeTordorpo
	uint32_t ____walreekeTordorpo_23;
	// System.String GarbageiOS.M_mirsi131::_vaypem
	String_t* ____vaypem_24;
	// System.Boolean GarbageiOS.M_mirsi131::_guwiceVaceemi
	bool ____guwiceVaceemi_25;
	// System.Int32 GarbageiOS.M_mirsi131::_sineryeTise
	int32_t ____sineryeTise_26;
	// System.Boolean GarbageiOS.M_mirsi131::_sakiswhe
	bool ____sakiswhe_27;
	// System.String GarbageiOS.M_mirsi131::_xooratre
	String_t* ____xooratre_28;
	// System.String GarbageiOS.M_mirsi131::_yalcurjemTouluzoo
	String_t* ____yalcurjemTouluzoo_29;
	// System.String GarbageiOS.M_mirsi131::_sirkiste
	String_t* ____sirkiste_30;
	// System.String GarbageiOS.M_mirsi131::_kapem
	String_t* ____kapem_31;
	// System.String GarbageiOS.M_mirsi131::_tehiSenur
	String_t* ____tehiSenur_32;
	// System.Int32 GarbageiOS.M_mirsi131::_peardere
	int32_t ____peardere_33;
	// System.String GarbageiOS.M_mirsi131::_yemfookairCedumaw
	String_t* ____yemfookairCedumaw_34;
	// System.UInt32 GarbageiOS.M_mirsi131::_cairpu
	uint32_t ____cairpu_35;
	// System.Int32 GarbageiOS.M_mirsi131::_dorra
	int32_t ____dorra_36;
	// System.Int32 GarbageiOS.M_mirsi131::_falnorleeChisti
	int32_t ____falnorleeChisti_37;
	// System.Single GarbageiOS.M_mirsi131::_whiscel
	float ____whiscel_38;

public:
	inline static int32_t get_offset_of__fariniYihem_0() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____fariniYihem_0)); }
	inline bool get__fariniYihem_0() const { return ____fariniYihem_0; }
	inline bool* get_address_of__fariniYihem_0() { return &____fariniYihem_0; }
	inline void set__fariniYihem_0(bool value)
	{
		____fariniYihem_0 = value;
	}

	inline static int32_t get_offset_of__fearvearmiNosisdral_1() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____fearvearmiNosisdral_1)); }
	inline uint32_t get__fearvearmiNosisdral_1() const { return ____fearvearmiNosisdral_1; }
	inline uint32_t* get_address_of__fearvearmiNosisdral_1() { return &____fearvearmiNosisdral_1; }
	inline void set__fearvearmiNosisdral_1(uint32_t value)
	{
		____fearvearmiNosisdral_1 = value;
	}

	inline static int32_t get_offset_of__canotaiPorjo_2() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____canotaiPorjo_2)); }
	inline String_t* get__canotaiPorjo_2() const { return ____canotaiPorjo_2; }
	inline String_t** get_address_of__canotaiPorjo_2() { return &____canotaiPorjo_2; }
	inline void set__canotaiPorjo_2(String_t* value)
	{
		____canotaiPorjo_2 = value;
		Il2CppCodeGenWriteBarrier(&____canotaiPorjo_2, value);
	}

	inline static int32_t get_offset_of__sejaircai_3() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____sejaircai_3)); }
	inline float get__sejaircai_3() const { return ____sejaircai_3; }
	inline float* get_address_of__sejaircai_3() { return &____sejaircai_3; }
	inline void set__sejaircai_3(float value)
	{
		____sejaircai_3 = value;
	}

	inline static int32_t get_offset_of__royir_4() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____royir_4)); }
	inline float get__royir_4() const { return ____royir_4; }
	inline float* get_address_of__royir_4() { return &____royir_4; }
	inline void set__royir_4(float value)
	{
		____royir_4 = value;
	}

	inline static int32_t get_offset_of__cokeaTirtea_5() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____cokeaTirtea_5)); }
	inline int32_t get__cokeaTirtea_5() const { return ____cokeaTirtea_5; }
	inline int32_t* get_address_of__cokeaTirtea_5() { return &____cokeaTirtea_5; }
	inline void set__cokeaTirtea_5(int32_t value)
	{
		____cokeaTirtea_5 = value;
	}

	inline static int32_t get_offset_of__seyeexelCidor_6() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____seyeexelCidor_6)); }
	inline int32_t get__seyeexelCidor_6() const { return ____seyeexelCidor_6; }
	inline int32_t* get_address_of__seyeexelCidor_6() { return &____seyeexelCidor_6; }
	inline void set__seyeexelCidor_6(int32_t value)
	{
		____seyeexelCidor_6 = value;
	}

	inline static int32_t get_offset_of__hearmalHersego_7() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____hearmalHersego_7)); }
	inline uint32_t get__hearmalHersego_7() const { return ____hearmalHersego_7; }
	inline uint32_t* get_address_of__hearmalHersego_7() { return &____hearmalHersego_7; }
	inline void set__hearmalHersego_7(uint32_t value)
	{
		____hearmalHersego_7 = value;
	}

	inline static int32_t get_offset_of__faryuNarsair_8() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____faryuNarsair_8)); }
	inline float get__faryuNarsair_8() const { return ____faryuNarsair_8; }
	inline float* get_address_of__faryuNarsair_8() { return &____faryuNarsair_8; }
	inline void set__faryuNarsair_8(float value)
	{
		____faryuNarsair_8 = value;
	}

	inline static int32_t get_offset_of__tabiTaiser_9() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____tabiTaiser_9)); }
	inline bool get__tabiTaiser_9() const { return ____tabiTaiser_9; }
	inline bool* get_address_of__tabiTaiser_9() { return &____tabiTaiser_9; }
	inline void set__tabiTaiser_9(bool value)
	{
		____tabiTaiser_9 = value;
	}

	inline static int32_t get_offset_of__drolem_10() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____drolem_10)); }
	inline float get__drolem_10() const { return ____drolem_10; }
	inline float* get_address_of__drolem_10() { return &____drolem_10; }
	inline void set__drolem_10(float value)
	{
		____drolem_10 = value;
	}

	inline static int32_t get_offset_of__fudisTerkereyere_11() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____fudisTerkereyere_11)); }
	inline float get__fudisTerkereyere_11() const { return ____fudisTerkereyere_11; }
	inline float* get_address_of__fudisTerkereyere_11() { return &____fudisTerkereyere_11; }
	inline void set__fudisTerkereyere_11(float value)
	{
		____fudisTerkereyere_11 = value;
	}

	inline static int32_t get_offset_of__talgooperQejem_12() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____talgooperQejem_12)); }
	inline uint32_t get__talgooperQejem_12() const { return ____talgooperQejem_12; }
	inline uint32_t* get_address_of__talgooperQejem_12() { return &____talgooperQejem_12; }
	inline void set__talgooperQejem_12(uint32_t value)
	{
		____talgooperQejem_12 = value;
	}

	inline static int32_t get_offset_of__kayki_13() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____kayki_13)); }
	inline int32_t get__kayki_13() const { return ____kayki_13; }
	inline int32_t* get_address_of__kayki_13() { return &____kayki_13; }
	inline void set__kayki_13(int32_t value)
	{
		____kayki_13 = value;
	}

	inline static int32_t get_offset_of__stawnouChaspawir_14() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____stawnouChaspawir_14)); }
	inline bool get__stawnouChaspawir_14() const { return ____stawnouChaspawir_14; }
	inline bool* get_address_of__stawnouChaspawir_14() { return &____stawnouChaspawir_14; }
	inline void set__stawnouChaspawir_14(bool value)
	{
		____stawnouChaspawir_14 = value;
	}

	inline static int32_t get_offset_of__drepai_15() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____drepai_15)); }
	inline float get__drepai_15() const { return ____drepai_15; }
	inline float* get_address_of__drepai_15() { return &____drepai_15; }
	inline void set__drepai_15(float value)
	{
		____drepai_15 = value;
	}

	inline static int32_t get_offset_of__boukou_16() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____boukou_16)); }
	inline int32_t get__boukou_16() const { return ____boukou_16; }
	inline int32_t* get_address_of__boukou_16() { return &____boukou_16; }
	inline void set__boukou_16(int32_t value)
	{
		____boukou_16 = value;
	}

	inline static int32_t get_offset_of__cawirlair_17() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____cawirlair_17)); }
	inline String_t* get__cawirlair_17() const { return ____cawirlair_17; }
	inline String_t** get_address_of__cawirlair_17() { return &____cawirlair_17; }
	inline void set__cawirlair_17(String_t* value)
	{
		____cawirlair_17 = value;
		Il2CppCodeGenWriteBarrier(&____cawirlair_17, value);
	}

	inline static int32_t get_offset_of__legurHearpeejaw_18() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____legurHearpeejaw_18)); }
	inline float get__legurHearpeejaw_18() const { return ____legurHearpeejaw_18; }
	inline float* get_address_of__legurHearpeejaw_18() { return &____legurHearpeejaw_18; }
	inline void set__legurHearpeejaw_18(float value)
	{
		____legurHearpeejaw_18 = value;
	}

	inline static int32_t get_offset_of__nasnere_19() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____nasnere_19)); }
	inline String_t* get__nasnere_19() const { return ____nasnere_19; }
	inline String_t** get_address_of__nasnere_19() { return &____nasnere_19; }
	inline void set__nasnere_19(String_t* value)
	{
		____nasnere_19 = value;
		Il2CppCodeGenWriteBarrier(&____nasnere_19, value);
	}

	inline static int32_t get_offset_of__whoutellir_20() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____whoutellir_20)); }
	inline int32_t get__whoutellir_20() const { return ____whoutellir_20; }
	inline int32_t* get_address_of__whoutellir_20() { return &____whoutellir_20; }
	inline void set__whoutellir_20(int32_t value)
	{
		____whoutellir_20 = value;
	}

	inline static int32_t get_offset_of__fawbismou_21() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____fawbismou_21)); }
	inline uint32_t get__fawbismou_21() const { return ____fawbismou_21; }
	inline uint32_t* get_address_of__fawbismou_21() { return &____fawbismou_21; }
	inline void set__fawbismou_21(uint32_t value)
	{
		____fawbismou_21 = value;
	}

	inline static int32_t get_offset_of__sedris_22() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____sedris_22)); }
	inline float get__sedris_22() const { return ____sedris_22; }
	inline float* get_address_of__sedris_22() { return &____sedris_22; }
	inline void set__sedris_22(float value)
	{
		____sedris_22 = value;
	}

	inline static int32_t get_offset_of__walreekeTordorpo_23() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____walreekeTordorpo_23)); }
	inline uint32_t get__walreekeTordorpo_23() const { return ____walreekeTordorpo_23; }
	inline uint32_t* get_address_of__walreekeTordorpo_23() { return &____walreekeTordorpo_23; }
	inline void set__walreekeTordorpo_23(uint32_t value)
	{
		____walreekeTordorpo_23 = value;
	}

	inline static int32_t get_offset_of__vaypem_24() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____vaypem_24)); }
	inline String_t* get__vaypem_24() const { return ____vaypem_24; }
	inline String_t** get_address_of__vaypem_24() { return &____vaypem_24; }
	inline void set__vaypem_24(String_t* value)
	{
		____vaypem_24 = value;
		Il2CppCodeGenWriteBarrier(&____vaypem_24, value);
	}

	inline static int32_t get_offset_of__guwiceVaceemi_25() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____guwiceVaceemi_25)); }
	inline bool get__guwiceVaceemi_25() const { return ____guwiceVaceemi_25; }
	inline bool* get_address_of__guwiceVaceemi_25() { return &____guwiceVaceemi_25; }
	inline void set__guwiceVaceemi_25(bool value)
	{
		____guwiceVaceemi_25 = value;
	}

	inline static int32_t get_offset_of__sineryeTise_26() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____sineryeTise_26)); }
	inline int32_t get__sineryeTise_26() const { return ____sineryeTise_26; }
	inline int32_t* get_address_of__sineryeTise_26() { return &____sineryeTise_26; }
	inline void set__sineryeTise_26(int32_t value)
	{
		____sineryeTise_26 = value;
	}

	inline static int32_t get_offset_of__sakiswhe_27() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____sakiswhe_27)); }
	inline bool get__sakiswhe_27() const { return ____sakiswhe_27; }
	inline bool* get_address_of__sakiswhe_27() { return &____sakiswhe_27; }
	inline void set__sakiswhe_27(bool value)
	{
		____sakiswhe_27 = value;
	}

	inline static int32_t get_offset_of__xooratre_28() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____xooratre_28)); }
	inline String_t* get__xooratre_28() const { return ____xooratre_28; }
	inline String_t** get_address_of__xooratre_28() { return &____xooratre_28; }
	inline void set__xooratre_28(String_t* value)
	{
		____xooratre_28 = value;
		Il2CppCodeGenWriteBarrier(&____xooratre_28, value);
	}

	inline static int32_t get_offset_of__yalcurjemTouluzoo_29() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____yalcurjemTouluzoo_29)); }
	inline String_t* get__yalcurjemTouluzoo_29() const { return ____yalcurjemTouluzoo_29; }
	inline String_t** get_address_of__yalcurjemTouluzoo_29() { return &____yalcurjemTouluzoo_29; }
	inline void set__yalcurjemTouluzoo_29(String_t* value)
	{
		____yalcurjemTouluzoo_29 = value;
		Il2CppCodeGenWriteBarrier(&____yalcurjemTouluzoo_29, value);
	}

	inline static int32_t get_offset_of__sirkiste_30() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____sirkiste_30)); }
	inline String_t* get__sirkiste_30() const { return ____sirkiste_30; }
	inline String_t** get_address_of__sirkiste_30() { return &____sirkiste_30; }
	inline void set__sirkiste_30(String_t* value)
	{
		____sirkiste_30 = value;
		Il2CppCodeGenWriteBarrier(&____sirkiste_30, value);
	}

	inline static int32_t get_offset_of__kapem_31() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____kapem_31)); }
	inline String_t* get__kapem_31() const { return ____kapem_31; }
	inline String_t** get_address_of__kapem_31() { return &____kapem_31; }
	inline void set__kapem_31(String_t* value)
	{
		____kapem_31 = value;
		Il2CppCodeGenWriteBarrier(&____kapem_31, value);
	}

	inline static int32_t get_offset_of__tehiSenur_32() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____tehiSenur_32)); }
	inline String_t* get__tehiSenur_32() const { return ____tehiSenur_32; }
	inline String_t** get_address_of__tehiSenur_32() { return &____tehiSenur_32; }
	inline void set__tehiSenur_32(String_t* value)
	{
		____tehiSenur_32 = value;
		Il2CppCodeGenWriteBarrier(&____tehiSenur_32, value);
	}

	inline static int32_t get_offset_of__peardere_33() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____peardere_33)); }
	inline int32_t get__peardere_33() const { return ____peardere_33; }
	inline int32_t* get_address_of__peardere_33() { return &____peardere_33; }
	inline void set__peardere_33(int32_t value)
	{
		____peardere_33 = value;
	}

	inline static int32_t get_offset_of__yemfookairCedumaw_34() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____yemfookairCedumaw_34)); }
	inline String_t* get__yemfookairCedumaw_34() const { return ____yemfookairCedumaw_34; }
	inline String_t** get_address_of__yemfookairCedumaw_34() { return &____yemfookairCedumaw_34; }
	inline void set__yemfookairCedumaw_34(String_t* value)
	{
		____yemfookairCedumaw_34 = value;
		Il2CppCodeGenWriteBarrier(&____yemfookairCedumaw_34, value);
	}

	inline static int32_t get_offset_of__cairpu_35() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____cairpu_35)); }
	inline uint32_t get__cairpu_35() const { return ____cairpu_35; }
	inline uint32_t* get_address_of__cairpu_35() { return &____cairpu_35; }
	inline void set__cairpu_35(uint32_t value)
	{
		____cairpu_35 = value;
	}

	inline static int32_t get_offset_of__dorra_36() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____dorra_36)); }
	inline int32_t get__dorra_36() const { return ____dorra_36; }
	inline int32_t* get_address_of__dorra_36() { return &____dorra_36; }
	inline void set__dorra_36(int32_t value)
	{
		____dorra_36 = value;
	}

	inline static int32_t get_offset_of__falnorleeChisti_37() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____falnorleeChisti_37)); }
	inline int32_t get__falnorleeChisti_37() const { return ____falnorleeChisti_37; }
	inline int32_t* get_address_of__falnorleeChisti_37() { return &____falnorleeChisti_37; }
	inline void set__falnorleeChisti_37(int32_t value)
	{
		____falnorleeChisti_37 = value;
	}

	inline static int32_t get_offset_of__whiscel_38() { return static_cast<int32_t>(offsetof(M_mirsi131_t1376269555, ____whiscel_38)); }
	inline float get__whiscel_38() const { return ____whiscel_38; }
	inline float* get_address_of__whiscel_38() { return &____whiscel_38; }
	inline void set__whiscel_38(float value)
	{
		____whiscel_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
