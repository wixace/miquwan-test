﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._4c156da0f8ed5e8f3bb838288be9dea5
struct _4c156da0f8ed5e8f3bb838288be9dea5_t3489769627;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__4c156da0f8ed5e8f3bb838283489769627.h"

// System.Void Little._4c156da0f8ed5e8f3bb838288be9dea5::.ctor()
extern "C"  void _4c156da0f8ed5e8f3bb838288be9dea5__ctor_m2006318066 (_4c156da0f8ed5e8f3bb838288be9dea5_t3489769627 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._4c156da0f8ed5e8f3bb838288be9dea5::_4c156da0f8ed5e8f3bb838288be9dea5m2(System.Int32)
extern "C"  int32_t _4c156da0f8ed5e8f3bb838288be9dea5__4c156da0f8ed5e8f3bb838288be9dea5m2_m1490875321 (_4c156da0f8ed5e8f3bb838288be9dea5_t3489769627 * __this, int32_t ____4c156da0f8ed5e8f3bb838288be9dea5a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._4c156da0f8ed5e8f3bb838288be9dea5::_4c156da0f8ed5e8f3bb838288be9dea5m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _4c156da0f8ed5e8f3bb838288be9dea5__4c156da0f8ed5e8f3bb838288be9dea5m_m4204025629 (_4c156da0f8ed5e8f3bb838288be9dea5_t3489769627 * __this, int32_t ____4c156da0f8ed5e8f3bb838288be9dea5a0, int32_t ____4c156da0f8ed5e8f3bb838288be9dea5851, int32_t ____4c156da0f8ed5e8f3bb838288be9dea5c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._4c156da0f8ed5e8f3bb838288be9dea5::ilo__4c156da0f8ed5e8f3bb838288be9dea5m21(Little._4c156da0f8ed5e8f3bb838288be9dea5,System.Int32)
extern "C"  int32_t _4c156da0f8ed5e8f3bb838288be9dea5_ilo__4c156da0f8ed5e8f3bb838288be9dea5m21_m1529262786 (Il2CppObject * __this /* static, unused */, _4c156da0f8ed5e8f3bb838288be9dea5_t3489769627 * ____this0, int32_t ____4c156da0f8ed5e8f3bb838288be9dea5a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
