﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// guideCfg
struct guideCfg_t2981043144;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSGuideVO
struct  CSGuideVO_t4116883493  : public Il2CppObject
{
public:
	// guideCfg CSGuideVO::cfg
	guideCfg_t2981043144 * ___cfg_0;
	// System.Int32 CSGuideVO::guideId
	int32_t ___guideId_1;
	// System.Boolean CSGuideVO::isFinished
	bool ___isFinished_2;

public:
	inline static int32_t get_offset_of_cfg_0() { return static_cast<int32_t>(offsetof(CSGuideVO_t4116883493, ___cfg_0)); }
	inline guideCfg_t2981043144 * get_cfg_0() const { return ___cfg_0; }
	inline guideCfg_t2981043144 ** get_address_of_cfg_0() { return &___cfg_0; }
	inline void set_cfg_0(guideCfg_t2981043144 * value)
	{
		___cfg_0 = value;
		Il2CppCodeGenWriteBarrier(&___cfg_0, value);
	}

	inline static int32_t get_offset_of_guideId_1() { return static_cast<int32_t>(offsetof(CSGuideVO_t4116883493, ___guideId_1)); }
	inline int32_t get_guideId_1() const { return ___guideId_1; }
	inline int32_t* get_address_of_guideId_1() { return &___guideId_1; }
	inline void set_guideId_1(int32_t value)
	{
		___guideId_1 = value;
	}

	inline static int32_t get_offset_of_isFinished_2() { return static_cast<int32_t>(offsetof(CSGuideVO_t4116883493, ___isFinished_2)); }
	inline bool get_isFinished_2() const { return ___isFinished_2; }
	inline bool* get_address_of_isFinished_2() { return &___isFinished_2; }
	inline void set_isFinished_2(bool value)
	{
		___isFinished_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
