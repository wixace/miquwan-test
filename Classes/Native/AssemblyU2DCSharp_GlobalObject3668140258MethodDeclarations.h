﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"

// System.Void GlobalObject::.cctor()
extern "C"  void GlobalObject__cctor_m3024837332 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject GlobalObject::get_GlobalObj()
extern "C"  GameObject_t3674682005 * GlobalObject_get_GlobalObj_m1737100909 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject GlobalObject::ilo_get_GlobalObj1()
extern "C"  GameObject_t3674682005 * GlobalObject_ilo_get_GlobalObj1_m3178766777 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
