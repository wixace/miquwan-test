﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AIObject
struct AIObject_t37280135;
// npcAIConditionCfg
struct npcAIConditionCfg_t930197170;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AIEventInfo
struct  AIEventInfo_t4031722272  : public Il2CppObject
{
public:
	// AIObject AIEventInfo::aiObj
	AIObject_t37280135 * ___aiObj_0;
	// npcAIConditionCfg AIEventInfo::condition
	npcAIConditionCfg_t930197170 * ___condition_1;

public:
	inline static int32_t get_offset_of_aiObj_0() { return static_cast<int32_t>(offsetof(AIEventInfo_t4031722272, ___aiObj_0)); }
	inline AIObject_t37280135 * get_aiObj_0() const { return ___aiObj_0; }
	inline AIObject_t37280135 ** get_address_of_aiObj_0() { return &___aiObj_0; }
	inline void set_aiObj_0(AIObject_t37280135 * value)
	{
		___aiObj_0 = value;
		Il2CppCodeGenWriteBarrier(&___aiObj_0, value);
	}

	inline static int32_t get_offset_of_condition_1() { return static_cast<int32_t>(offsetof(AIEventInfo_t4031722272, ___condition_1)); }
	inline npcAIConditionCfg_t930197170 * get_condition_1() const { return ___condition_1; }
	inline npcAIConditionCfg_t930197170 ** get_address_of_condition_1() { return &___condition_1; }
	inline void set_condition_1(npcAIConditionCfg_t930197170 * value)
	{
		___condition_1 = value;
		Il2CppCodeGenWriteBarrier(&___condition_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
