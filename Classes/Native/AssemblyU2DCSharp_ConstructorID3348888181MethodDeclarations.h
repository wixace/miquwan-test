﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ConstructorID
struct ConstructorID_t3348888181;
// System.String[]
struct StringU5BU5D_t4054002952;
// TypeFlag[]
struct TypeFlagU5BU5D_t693965763;

#include "codegen/il2cpp-codegen.h"

// System.Void ConstructorID::.ctor(System.String[],TypeFlag[])
extern "C"  void ConstructorID__ctor_m1608662772 (ConstructorID_t3348888181 * __this, StringU5BU5D_t4054002952* ___paramTypes0, TypeFlagU5BU5D_t693965763* ___paramFlags1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
