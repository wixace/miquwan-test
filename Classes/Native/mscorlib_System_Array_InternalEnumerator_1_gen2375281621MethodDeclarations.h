﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2375281621.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_SoundStatus3592938945.h"

// System.Void System.Array/InternalEnumerator`1<SoundStatus>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m473538822_gshared (InternalEnumerator_1_t2375281621 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m473538822(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2375281621 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m473538822_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<SoundStatus>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2165439194_gshared (InternalEnumerator_1_t2375281621 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2165439194(__this, method) ((  void (*) (InternalEnumerator_1_t2375281621 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2165439194_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<SoundStatus>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3576071248_gshared (InternalEnumerator_1_t2375281621 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3576071248(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2375281621 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3576071248_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<SoundStatus>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2436209245_gshared (InternalEnumerator_1_t2375281621 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2436209245(__this, method) ((  void (*) (InternalEnumerator_1_t2375281621 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2436209245_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<SoundStatus>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3152806794_gshared (InternalEnumerator_1_t2375281621 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3152806794(__this, method) ((  bool (*) (InternalEnumerator_1_t2375281621 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3152806794_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<SoundStatus>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m3063543983_gshared (InternalEnumerator_1_t2375281621 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3063543983(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2375281621 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3063543983_gshared)(__this, method)
