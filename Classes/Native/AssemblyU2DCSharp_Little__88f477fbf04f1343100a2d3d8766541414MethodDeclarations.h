﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._88f477fbf04f1343100a2d3d8910b962
struct _88f477fbf04f1343100a2d3d8910b962_t766541414;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._88f477fbf04f1343100a2d3d8910b962::.ctor()
extern "C"  void _88f477fbf04f1343100a2d3d8910b962__ctor_m1862498951 (_88f477fbf04f1343100a2d3d8910b962_t766541414 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._88f477fbf04f1343100a2d3d8910b962::_88f477fbf04f1343100a2d3d8910b962m2(System.Int32)
extern "C"  int32_t _88f477fbf04f1343100a2d3d8910b962__88f477fbf04f1343100a2d3d8910b962m2_m3921299417 (_88f477fbf04f1343100a2d3d8910b962_t766541414 * __this, int32_t ____88f477fbf04f1343100a2d3d8910b962a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._88f477fbf04f1343100a2d3d8910b962::_88f477fbf04f1343100a2d3d8910b962m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _88f477fbf04f1343100a2d3d8910b962__88f477fbf04f1343100a2d3d8910b962m_m3066554109 (_88f477fbf04f1343100a2d3d8910b962_t766541414 * __this, int32_t ____88f477fbf04f1343100a2d3d8910b962a0, int32_t ____88f477fbf04f1343100a2d3d8910b962431, int32_t ____88f477fbf04f1343100a2d3d8910b962c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
