﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct KeyCollection_t2394160841;
// System.Collections.Generic.Dictionary`2<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct Dictionary_2_t767401390;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K1382337444.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m548590012_gshared (KeyCollection_t2394160841 * __this, Dictionary_2_t767401390 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m548590012(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2394160841 *, Dictionary_2_t767401390 *, const MethodInfo*))KeyCollection__ctor_m548590012_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1248682522_gshared (KeyCollection_t2394160841 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1248682522(__this, ___item0, method) ((  void (*) (KeyCollection_t2394160841 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1248682522_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m586761745_gshared (KeyCollection_t2394160841 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m586761745(__this, method) ((  void (*) (KeyCollection_t2394160841 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m586761745_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m992638128_gshared (KeyCollection_t2394160841 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m992638128(__this, ___item0, method) ((  bool (*) (KeyCollection_t2394160841 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m992638128_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m636244501_gshared (KeyCollection_t2394160841 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m636244501(__this, ___item0, method) ((  bool (*) (KeyCollection_t2394160841 *, Il2CppObject *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m636244501_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1085981709_gshared (KeyCollection_t2394160841 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1085981709(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2394160841 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1085981709_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3763706115_gshared (KeyCollection_t2394160841 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m3763706115(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2394160841 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3763706115_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2450043070_gshared (KeyCollection_t2394160841 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2450043070(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2394160841 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2450043070_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3323559953_gshared (KeyCollection_t2394160841 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3323559953(__this, method) ((  bool (*) (KeyCollection_t2394160841 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3323559953_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m38287875_gshared (KeyCollection_t2394160841 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m38287875(__this, method) ((  bool (*) (KeyCollection_t2394160841 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m38287875_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m4130558511_gshared (KeyCollection_t2394160841 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m4130558511(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2394160841 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m4130558511_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2541103217_gshared (KeyCollection_t2394160841 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2541103217(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2394160841 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2541103217_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::GetEnumerator()
extern "C"  Enumerator_t1382337444  KeyCollection_GetEnumerator_m1104063124_gshared (KeyCollection_t2394160841 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m1104063124(__this, method) ((  Enumerator_t1382337444  (*) (KeyCollection_t2394160841 *, const MethodInfo*))KeyCollection_GetEnumerator_m1104063124_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1964097609_gshared (KeyCollection_t2394160841 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m1964097609(__this, method) ((  int32_t (*) (KeyCollection_t2394160841 *, const MethodInfo*))KeyCollection_get_Count_m1964097609_gshared)(__this, method)
