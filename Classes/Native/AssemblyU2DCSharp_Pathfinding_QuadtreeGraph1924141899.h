﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.QuadtreeNodeHolder
struct QuadtreeNodeHolder_t2134093193;
// System.Collections.BitArray
struct BitArray_t3577534870;

#include "AssemblyU2DCSharp_Pathfinding_NavGraph1254319713.h"
#include "UnityEngine_UnityEngine_LayerMask3236759763.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.QuadtreeGraph
struct  QuadtreeGraph_t1924141899  : public NavGraph_t1254319713
{
public:
	// System.Int32 Pathfinding.QuadtreeGraph::editorWidthLog2
	int32_t ___editorWidthLog2_11;
	// System.Int32 Pathfinding.QuadtreeGraph::editorHeightLog2
	int32_t ___editorHeightLog2_12;
	// UnityEngine.LayerMask Pathfinding.QuadtreeGraph::layerMask
	LayerMask_t3236759763  ___layerMask_13;
	// System.Single Pathfinding.QuadtreeGraph::nodeSize
	float ___nodeSize_14;
	// System.Int32 Pathfinding.QuadtreeGraph::minDepth
	int32_t ___minDepth_15;
	// Pathfinding.QuadtreeNodeHolder Pathfinding.QuadtreeGraph::root
	QuadtreeNodeHolder_t2134093193 * ___root_16;
	// UnityEngine.Vector3 Pathfinding.QuadtreeGraph::center
	Vector3_t4282066566  ___center_17;
	// System.Collections.BitArray Pathfinding.QuadtreeGraph::map
	BitArray_t3577534870 * ___map_18;
	// System.Int32 Pathfinding.QuadtreeGraph::<Width>k__BackingField
	int32_t ___U3CWidthU3Ek__BackingField_19;
	// System.Int32 Pathfinding.QuadtreeGraph::<Height>k__BackingField
	int32_t ___U3CHeightU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_editorWidthLog2_11() { return static_cast<int32_t>(offsetof(QuadtreeGraph_t1924141899, ___editorWidthLog2_11)); }
	inline int32_t get_editorWidthLog2_11() const { return ___editorWidthLog2_11; }
	inline int32_t* get_address_of_editorWidthLog2_11() { return &___editorWidthLog2_11; }
	inline void set_editorWidthLog2_11(int32_t value)
	{
		___editorWidthLog2_11 = value;
	}

	inline static int32_t get_offset_of_editorHeightLog2_12() { return static_cast<int32_t>(offsetof(QuadtreeGraph_t1924141899, ___editorHeightLog2_12)); }
	inline int32_t get_editorHeightLog2_12() const { return ___editorHeightLog2_12; }
	inline int32_t* get_address_of_editorHeightLog2_12() { return &___editorHeightLog2_12; }
	inline void set_editorHeightLog2_12(int32_t value)
	{
		___editorHeightLog2_12 = value;
	}

	inline static int32_t get_offset_of_layerMask_13() { return static_cast<int32_t>(offsetof(QuadtreeGraph_t1924141899, ___layerMask_13)); }
	inline LayerMask_t3236759763  get_layerMask_13() const { return ___layerMask_13; }
	inline LayerMask_t3236759763 * get_address_of_layerMask_13() { return &___layerMask_13; }
	inline void set_layerMask_13(LayerMask_t3236759763  value)
	{
		___layerMask_13 = value;
	}

	inline static int32_t get_offset_of_nodeSize_14() { return static_cast<int32_t>(offsetof(QuadtreeGraph_t1924141899, ___nodeSize_14)); }
	inline float get_nodeSize_14() const { return ___nodeSize_14; }
	inline float* get_address_of_nodeSize_14() { return &___nodeSize_14; }
	inline void set_nodeSize_14(float value)
	{
		___nodeSize_14 = value;
	}

	inline static int32_t get_offset_of_minDepth_15() { return static_cast<int32_t>(offsetof(QuadtreeGraph_t1924141899, ___minDepth_15)); }
	inline int32_t get_minDepth_15() const { return ___minDepth_15; }
	inline int32_t* get_address_of_minDepth_15() { return &___minDepth_15; }
	inline void set_minDepth_15(int32_t value)
	{
		___minDepth_15 = value;
	}

	inline static int32_t get_offset_of_root_16() { return static_cast<int32_t>(offsetof(QuadtreeGraph_t1924141899, ___root_16)); }
	inline QuadtreeNodeHolder_t2134093193 * get_root_16() const { return ___root_16; }
	inline QuadtreeNodeHolder_t2134093193 ** get_address_of_root_16() { return &___root_16; }
	inline void set_root_16(QuadtreeNodeHolder_t2134093193 * value)
	{
		___root_16 = value;
		Il2CppCodeGenWriteBarrier(&___root_16, value);
	}

	inline static int32_t get_offset_of_center_17() { return static_cast<int32_t>(offsetof(QuadtreeGraph_t1924141899, ___center_17)); }
	inline Vector3_t4282066566  get_center_17() const { return ___center_17; }
	inline Vector3_t4282066566 * get_address_of_center_17() { return &___center_17; }
	inline void set_center_17(Vector3_t4282066566  value)
	{
		___center_17 = value;
	}

	inline static int32_t get_offset_of_map_18() { return static_cast<int32_t>(offsetof(QuadtreeGraph_t1924141899, ___map_18)); }
	inline BitArray_t3577534870 * get_map_18() const { return ___map_18; }
	inline BitArray_t3577534870 ** get_address_of_map_18() { return &___map_18; }
	inline void set_map_18(BitArray_t3577534870 * value)
	{
		___map_18 = value;
		Il2CppCodeGenWriteBarrier(&___map_18, value);
	}

	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(QuadtreeGraph_t1924141899, ___U3CWidthU3Ek__BackingField_19)); }
	inline int32_t get_U3CWidthU3Ek__BackingField_19() const { return ___U3CWidthU3Ek__BackingField_19; }
	inline int32_t* get_address_of_U3CWidthU3Ek__BackingField_19() { return &___U3CWidthU3Ek__BackingField_19; }
	inline void set_U3CWidthU3Ek__BackingField_19(int32_t value)
	{
		___U3CWidthU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(QuadtreeGraph_t1924141899, ___U3CHeightU3Ek__BackingField_20)); }
	inline int32_t get_U3CHeightU3Ek__BackingField_20() const { return ___U3CHeightU3Ek__BackingField_20; }
	inline int32_t* get_address_of_U3CHeightU3Ek__BackingField_20() { return &___U3CHeightU3Ek__BackingField_20; }
	inline void set_U3CHeightU3Ek__BackingField_20(int32_t value)
	{
		___U3CHeightU3Ek__BackingField_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
