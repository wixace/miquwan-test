﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._61ce11f8be181b0cbd58a8e6669fa6be
struct _61ce11f8be181b0cbd58a8e6669fa6be_t1407465448;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__61ce11f8be181b0cbd58a8e61407465448.h"

// System.Void Little._61ce11f8be181b0cbd58a8e6669fa6be::.ctor()
extern "C"  void _61ce11f8be181b0cbd58a8e6669fa6be__ctor_m2990232389 (_61ce11f8be181b0cbd58a8e6669fa6be_t1407465448 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._61ce11f8be181b0cbd58a8e6669fa6be::_61ce11f8be181b0cbd58a8e6669fa6bem2(System.Int32)
extern "C"  int32_t _61ce11f8be181b0cbd58a8e6669fa6be__61ce11f8be181b0cbd58a8e6669fa6bem2_m2887305369 (_61ce11f8be181b0cbd58a8e6669fa6be_t1407465448 * __this, int32_t ____61ce11f8be181b0cbd58a8e6669fa6bea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._61ce11f8be181b0cbd58a8e6669fa6be::_61ce11f8be181b0cbd58a8e6669fa6bem(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _61ce11f8be181b0cbd58a8e6669fa6be__61ce11f8be181b0cbd58a8e6669fa6bem_m3333930557 (_61ce11f8be181b0cbd58a8e6669fa6be_t1407465448 * __this, int32_t ____61ce11f8be181b0cbd58a8e6669fa6bea0, int32_t ____61ce11f8be181b0cbd58a8e6669fa6be141, int32_t ____61ce11f8be181b0cbd58a8e6669fa6bec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._61ce11f8be181b0cbd58a8e6669fa6be::ilo__61ce11f8be181b0cbd58a8e6669fa6bem21(Little._61ce11f8be181b0cbd58a8e6669fa6be,System.Int32)
extern "C"  int32_t _61ce11f8be181b0cbd58a8e6669fa6be_ilo__61ce11f8be181b0cbd58a8e6669fa6bem21_m2171572911 (Il2CppObject * __this /* static, unused */, _61ce11f8be181b0cbd58a8e6669fa6be_t1407465448 * ____this0, int32_t ____61ce11f8be181b0cbd58a8e6669fa6bea1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
