﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Object,System.Object>
struct U3CDelayCallExecU3Ec__Iterator40_2_t1092759312;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Object,System.Object>::.ctor()
extern "C"  void U3CDelayCallExecU3Ec__Iterator40_2__ctor_m323182344_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t1092759312 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2__ctor_m323182344(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator40_2_t1092759312 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2__ctor_m323182344_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Object,System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1594323850_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t1092759312 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1594323850(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator40_2_t1092759312 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1594323850_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_IEnumerator_get_Current_m1528154910_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t1092759312 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_IEnumerator_get_Current_m1528154910(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator40_2_t1092759312 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_System_Collections_IEnumerator_get_Current_m1528154910_gshared)(__this, method)
// System.Boolean BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Object,System.Object>::MoveNext()
extern "C"  bool U3CDelayCallExecU3Ec__Iterator40_2_MoveNext_m4342508_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t1092759312 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_MoveNext_m4342508(__this, method) ((  bool (*) (U3CDelayCallExecU3Ec__Iterator40_2_t1092759312 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_MoveNext_m4342508_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Object,System.Object>::Dispose()
extern "C"  void U3CDelayCallExecU3Ec__Iterator40_2_Dispose_m1238870341_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t1092759312 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_Dispose_m1238870341(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator40_2_t1092759312 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_Dispose_m1238870341_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator40`2<System.Object,System.Object>::Reset()
extern "C"  void U3CDelayCallExecU3Ec__Iterator40_2_Reset_m2264582581_gshared (U3CDelayCallExecU3Ec__Iterator40_2_t1092759312 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator40_2_Reset_m2264582581(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator40_2_t1092759312 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator40_2_Reset_m2264582581_gshared)(__this, method)
