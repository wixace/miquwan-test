﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t3870600107;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MonsterDeadFadeTimed
struct  MonsterDeadFadeTimed_t1196879997  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Material MonsterDeadFadeTimed::m_Mat
	Material_t3870600107 * ___m_Mat_2;
	// System.Single MonsterDeadFadeTimed::m_fTime
	float ___m_fTime_3;
	// System.Single MonsterDeadFadeTimed::fadeSpeed
	float ___fadeSpeed_4;

public:
	inline static int32_t get_offset_of_m_Mat_2() { return static_cast<int32_t>(offsetof(MonsterDeadFadeTimed_t1196879997, ___m_Mat_2)); }
	inline Material_t3870600107 * get_m_Mat_2() const { return ___m_Mat_2; }
	inline Material_t3870600107 ** get_address_of_m_Mat_2() { return &___m_Mat_2; }
	inline void set_m_Mat_2(Material_t3870600107 * value)
	{
		___m_Mat_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Mat_2, value);
	}

	inline static int32_t get_offset_of_m_fTime_3() { return static_cast<int32_t>(offsetof(MonsterDeadFadeTimed_t1196879997, ___m_fTime_3)); }
	inline float get_m_fTime_3() const { return ___m_fTime_3; }
	inline float* get_address_of_m_fTime_3() { return &___m_fTime_3; }
	inline void set_m_fTime_3(float value)
	{
		___m_fTime_3 = value;
	}

	inline static int32_t get_offset_of_fadeSpeed_4() { return static_cast<int32_t>(offsetof(MonsterDeadFadeTimed_t1196879997, ___fadeSpeed_4)); }
	inline float get_fadeSpeed_4() const { return ___fadeSpeed_4; }
	inline float* get_address_of_fadeSpeed_4() { return &___fadeSpeed_4; }
	inline void set_fadeSpeed_4(float value)
	{
		___fadeSpeed_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
