﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// AMihuaPay
struct AMihuaPay_t1319045905;
// WMihuaPay
struct WMihuaPay_t209753383;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_PluginMihua_BigPayType3266541836.h"
#include "AssemblyU2DCSharp_PluginMihua_PayType917897752.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginMihua
struct  PluginMihua_t2548376133  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginMihua::password
	String_t* ___password_4;
	// System.String PluginMihua::username
	String_t* ___username_5;
	// PluginMihua/BigPayType PluginMihua::bigPayType
	int32_t ___bigPayType_6;
	// PluginMihua/PayType PluginMihua::allPayType
	int32_t ___allPayType_7;
	// AMihuaPay PluginMihua::aliSDK
	AMihuaPay_t1319045905 * ___aliSDK_8;
	// WMihuaPay PluginMihua::wxhSDK
	WMihuaPay_t209753383 * ___wxhSDK_9;
	// Mihua.SDK.PayInfo PluginMihua::payInfo
	PayInfo_t1775308120 * ___payInfo_10;
	// System.Int32 PluginMihua::creditNum
	int32_t ___creditNum_11;
	// System.Int32 PluginMihua::rechargeCount
	int32_t ___rechargeCount_12;

public:
	inline static int32_t get_offset_of_password_4() { return static_cast<int32_t>(offsetof(PluginMihua_t2548376133, ___password_4)); }
	inline String_t* get_password_4() const { return ___password_4; }
	inline String_t** get_address_of_password_4() { return &___password_4; }
	inline void set_password_4(String_t* value)
	{
		___password_4 = value;
		Il2CppCodeGenWriteBarrier(&___password_4, value);
	}

	inline static int32_t get_offset_of_username_5() { return static_cast<int32_t>(offsetof(PluginMihua_t2548376133, ___username_5)); }
	inline String_t* get_username_5() const { return ___username_5; }
	inline String_t** get_address_of_username_5() { return &___username_5; }
	inline void set_username_5(String_t* value)
	{
		___username_5 = value;
		Il2CppCodeGenWriteBarrier(&___username_5, value);
	}

	inline static int32_t get_offset_of_bigPayType_6() { return static_cast<int32_t>(offsetof(PluginMihua_t2548376133, ___bigPayType_6)); }
	inline int32_t get_bigPayType_6() const { return ___bigPayType_6; }
	inline int32_t* get_address_of_bigPayType_6() { return &___bigPayType_6; }
	inline void set_bigPayType_6(int32_t value)
	{
		___bigPayType_6 = value;
	}

	inline static int32_t get_offset_of_allPayType_7() { return static_cast<int32_t>(offsetof(PluginMihua_t2548376133, ___allPayType_7)); }
	inline int32_t get_allPayType_7() const { return ___allPayType_7; }
	inline int32_t* get_address_of_allPayType_7() { return &___allPayType_7; }
	inline void set_allPayType_7(int32_t value)
	{
		___allPayType_7 = value;
	}

	inline static int32_t get_offset_of_aliSDK_8() { return static_cast<int32_t>(offsetof(PluginMihua_t2548376133, ___aliSDK_8)); }
	inline AMihuaPay_t1319045905 * get_aliSDK_8() const { return ___aliSDK_8; }
	inline AMihuaPay_t1319045905 ** get_address_of_aliSDK_8() { return &___aliSDK_8; }
	inline void set_aliSDK_8(AMihuaPay_t1319045905 * value)
	{
		___aliSDK_8 = value;
		Il2CppCodeGenWriteBarrier(&___aliSDK_8, value);
	}

	inline static int32_t get_offset_of_wxhSDK_9() { return static_cast<int32_t>(offsetof(PluginMihua_t2548376133, ___wxhSDK_9)); }
	inline WMihuaPay_t209753383 * get_wxhSDK_9() const { return ___wxhSDK_9; }
	inline WMihuaPay_t209753383 ** get_address_of_wxhSDK_9() { return &___wxhSDK_9; }
	inline void set_wxhSDK_9(WMihuaPay_t209753383 * value)
	{
		___wxhSDK_9 = value;
		Il2CppCodeGenWriteBarrier(&___wxhSDK_9, value);
	}

	inline static int32_t get_offset_of_payInfo_10() { return static_cast<int32_t>(offsetof(PluginMihua_t2548376133, ___payInfo_10)); }
	inline PayInfo_t1775308120 * get_payInfo_10() const { return ___payInfo_10; }
	inline PayInfo_t1775308120 ** get_address_of_payInfo_10() { return &___payInfo_10; }
	inline void set_payInfo_10(PayInfo_t1775308120 * value)
	{
		___payInfo_10 = value;
		Il2CppCodeGenWriteBarrier(&___payInfo_10, value);
	}

	inline static int32_t get_offset_of_creditNum_11() { return static_cast<int32_t>(offsetof(PluginMihua_t2548376133, ___creditNum_11)); }
	inline int32_t get_creditNum_11() const { return ___creditNum_11; }
	inline int32_t* get_address_of_creditNum_11() { return &___creditNum_11; }
	inline void set_creditNum_11(int32_t value)
	{
		___creditNum_11 = value;
	}

	inline static int32_t get_offset_of_rechargeCount_12() { return static_cast<int32_t>(offsetof(PluginMihua_t2548376133, ___rechargeCount_12)); }
	inline int32_t get_rechargeCount_12() const { return ___rechargeCount_12; }
	inline int32_t* get_address_of_rechargeCount_12() { return &___rechargeCount_12; }
	inline void set_rechargeCount_12(int32_t value)
	{
		___rechargeCount_12 = value;
	}
};

struct PluginMihua_t2548376133_StaticFields
{
public:
	// System.String PluginMihua::appKey
	String_t* ___appKey_3;

public:
	inline static int32_t get_offset_of_appKey_3() { return static_cast<int32_t>(offsetof(PluginMihua_t2548376133_StaticFields, ___appKey_3)); }
	inline String_t* get_appKey_3() const { return ___appKey_3; }
	inline String_t** get_address_of_appKey_3() { return &___appKey_3; }
	inline void set_appKey_3(String_t* value)
	{
		___appKey_3 = value;
		Il2CppCodeGenWriteBarrier(&___appKey_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
