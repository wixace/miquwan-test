﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._58bf98b1bfde38ebc0139dd99d1c5349
struct _58bf98b1bfde38ebc0139dd99d1c5349_t41371364;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__58bf98b1bfde38ebc0139dd99d41371364.h"

// System.Void Little._58bf98b1bfde38ebc0139dd99d1c5349::.ctor()
extern "C"  void _58bf98b1bfde38ebc0139dd99d1c5349__ctor_m897802185 (_58bf98b1bfde38ebc0139dd99d1c5349_t41371364 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._58bf98b1bfde38ebc0139dd99d1c5349::_58bf98b1bfde38ebc0139dd99d1c5349m2(System.Int32)
extern "C"  int32_t _58bf98b1bfde38ebc0139dd99d1c5349__58bf98b1bfde38ebc0139dd99d1c5349m2_m3057420057 (_58bf98b1bfde38ebc0139dd99d1c5349_t41371364 * __this, int32_t ____58bf98b1bfde38ebc0139dd99d1c5349a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._58bf98b1bfde38ebc0139dd99d1c5349::_58bf98b1bfde38ebc0139dd99d1c5349m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _58bf98b1bfde38ebc0139dd99d1c5349__58bf98b1bfde38ebc0139dd99d1c5349m_m2454671293 (_58bf98b1bfde38ebc0139dd99d1c5349_t41371364 * __this, int32_t ____58bf98b1bfde38ebc0139dd99d1c5349a0, int32_t ____58bf98b1bfde38ebc0139dd99d1c5349611, int32_t ____58bf98b1bfde38ebc0139dd99d1c5349c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._58bf98b1bfde38ebc0139dd99d1c5349::ilo__58bf98b1bfde38ebc0139dd99d1c5349m21(Little._58bf98b1bfde38ebc0139dd99d1c5349,System.Int32)
extern "C"  int32_t _58bf98b1bfde38ebc0139dd99d1c5349_ilo__58bf98b1bfde38ebc0139dd99d1c5349m21_m3155129643 (Il2CppObject * __this /* static, unused */, _58bf98b1bfde38ebc0139dd99d1c5349_t41371364 * ____this0, int32_t ____58bf98b1bfde38ebc0139dd99d1c5349a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
