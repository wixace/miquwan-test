﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StoryMgr
struct StoryMgr_t1782380803;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// StoryMgr/StoryVO
struct StoryVO_t3225531714;
// SceneMgr
struct SceneMgr_t3584105996;
// checkpointCfg
struct checkpointCfg_t2816107964;
// System.Collections.Generic.Dictionary`2<System.Int32,demoStoryCfg>
struct Dictionary_2_t1990425529;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// GlobalGOMgr
struct GlobalGOMgr_t803081773;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;
// CombatEntity
struct CombatEntity_t684137495;
// GameMgr
struct GameMgr_t1469029542;
// JSCLevelMonsterConfig
struct JSCLevelMonsterConfig_t1924079698;
// Monster
struct Monster_t2901270458;
// NpcMgr
struct NpcMgr_t2339534743;
// BuffCtrl
struct BuffCtrl_t2836564350;
// FightCtrl
struct FightCtrl_t648967803;
// CameraHelper
struct CameraHelper_t3196871507;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_StoryMgr_StoryVO3225531714.h"
#include "AssemblyU2DCSharp_SceneMgr3584105996.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_StoryMgr1782380803.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "AssemblyU2DCSharp_JSCLevelMonsterConfig1924079698.h"
#include "AssemblyU2DCSharp_NpcMgr2339534743.h"
#include "AssemblyU2DCSharp_NpcMgr_EditorNpcType2400575222.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Monster2901270458.h"
#include "AssemblyU2DCSharp_BuffCtrl2836564350.h"
#include "AssemblyU2DCSharp_FightCtrl648967803.h"
#include "AssemblyU2DCSharp_CameraHelper3196871507.h"

// System.Void StoryMgr::.ctor()
extern "C"  void StoryMgr__ctor_m2913483000 (StoryMgr_t1782380803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::OnEnterScene(CEvent.ZEvent)
extern "C"  void StoryMgr_OnEnterScene_m3706563964 (StoryMgr_t1782380803 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean StoryMgr::Init()
extern "C"  bool StoryMgr_Init_m3510951184 (StoryMgr_t1782380803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::OnStart()
extern "C"  void StoryMgr_OnStart_m2919948761 (StoryMgr_t1782380803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::JumpAllStory(CEvent.ZEvent)
extern "C"  void StoryMgr_JumpAllStory_m1025095501 (StoryMgr_t1782380803 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::OnChangeCamTarComplete(CEvent.ZEvent)
extern "C"  void StoryMgr_OnChangeCamTarComplete_m960772785 (StoryMgr_t1782380803 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::OnStartAniComplete(CEvent.ZEvent)
extern "C"  void StoryMgr_OnStartAniComplete_m1300956733 (StoryMgr_t1782380803 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::OnMoveCameraComplete(CEvent.ZEvent)
extern "C"  void StoryMgr_OnMoveCameraComplete_m3001998593 (StoryMgr_t1782380803 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::OnPlotComplete(CEvent.ZEvent)
extern "C"  void StoryMgr_OnPlotComplete_m595885718 (StoryMgr_t1782380803 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::OnGuideComplete(CEvent.ZEvent)
extern "C"  void StoryMgr_OnGuideComplete_m3120577775 (StoryMgr_t1782380803 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::OnWaitComplete(CEvent.ZEvent)
extern "C"  void StoryMgr_OnWaitComplete_m2269352834 (StoryMgr_t1782380803 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::OnComplete()
extern "C"  void StoryMgr_OnComplete_m3305961284 (StoryMgr_t1782380803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::Execute()
extern "C"  void StoryMgr_Execute_m2389875467 (StoryMgr_t1782380803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::OnEnd()
extern "C"  void StoryMgr_OnEnd_m2407805330 (StoryMgr_t1782380803 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 StoryMgr::<Init>m__3E6(StoryMgr/StoryVO,StoryMgr/StoryVO)
extern "C"  int32_t StoryMgr_U3CInitU3Em__3E6_m3238819843 (Il2CppObject * __this /* static, unused */, StoryVO_t3225531714 * ___a0, StoryVO_t3225531714 * ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::<Execute>m__3E7()
extern "C"  void StoryMgr_U3CExecuteU3Em__3E7_m1341057171 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SceneMgr StoryMgr::ilo_get_SceneMgr1()
extern "C"  SceneMgr_t3584105996 * StoryMgr_ilo_get_SceneMgr1_m1345808526 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// checkpointCfg StoryMgr::ilo_get_checkPoint2(SceneMgr)
extern "C"  checkpointCfg_t2816107964 * StoryMgr_ilo_get_checkPoint2_m3877146049 (Il2CppObject * __this /* static, unused */, SceneMgr_t3584105996 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.Int32,demoStoryCfg> StoryMgr::ilo_GetdemoStoryCfgDic3(CSDatacfgManager)
extern "C"  Dictionary_2_t1990425529 * StoryMgr_ilo_GetdemoStoryCfgDic3_m3115844500 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::ilo_AddEventListener4(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void StoryMgr_ilo_AddEventListener4_m1954376537 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GlobalGOMgr StoryMgr::ilo_get_GlobalGOMgr5()
extern "C"  GlobalGOMgr_t803081773 * StoryMgr_ilo_get_GlobalGOMgr5_m2251111254 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::ilo_CallJsFun6(System.String,System.Object[])
extern "C"  void StoryMgr_ilo_CallJsFun6_m2890781009 (Il2CppObject * __this /* static, unused */, String_t* ___funName0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::ilo_RemoveEventListener7(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void StoryMgr_ilo_RemoveEventListener7_m453529911 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::ilo_OnComplete8(StoryMgr)
extern "C"  void StoryMgr_ilo_OnComplete8_m241771744 (Il2CppObject * __this /* static, unused */, StoryMgr_t1782380803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::ilo_Log9(System.Object,System.Boolean)
extern "C"  void StoryMgr_ilo_Log9_m3474674013 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity StoryMgr::ilo_GetEntityByID10(GameMgr,System.Boolean,System.Int32)
extern "C"  CombatEntity_t684137495 * StoryMgr_ilo_GetEntityByID10_m3336598751 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, bool ___isHero1, int32_t ___id2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::ilo_LogWarning11(System.Object,System.Boolean)
extern "C"  void StoryMgr_ilo_LogWarning11_m2652767872 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::ilo_set_z12(JSCLevelMonsterConfig,System.Single)
extern "C"  void StoryMgr_ilo_set_z12_m3985476868 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::ilo_set_rot13(JSCLevelMonsterConfig,System.Single)
extern "C"  void StoryMgr_ilo_set_rot13_m1602971366 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::ilo_set_id14(JSCLevelMonsterConfig,System.Int32)
extern "C"  void StoryMgr_ilo_set_id14_m4065393617 (Il2CppObject * __this /* static, unused */, JSCLevelMonsterConfig_t1924079698 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Monster StoryMgr::ilo_CreatNpc15(NpcMgr,JSCLevelMonsterConfig,NpcMgr/EditorNpcType,System.Int32,CombatEntity,System.Boolean,System.Boolean)
extern "C"  Monster_t2901270458 * StoryMgr_ilo_CreatNpc15_m784766321 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, JSCLevelMonsterConfig_t1924079698 * ___lconfig1, int32_t ___type2, int32_t ___index3, CombatEntity_t684137495 * ___SummonEntity4, bool ____isSummon5, bool ___isFindPos6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::ilo_set_atkTarget16(CombatEntity,CombatEntity)
extern "C"  void StoryMgr_ilo_set_atkTarget16_m1373450098 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, CombatEntity_t684137495 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::ilo_OnTriggerFun17(Monster,CombatEntity,System.Int32)
extern "C"  void StoryMgr_ilo_OnTriggerFun17_m3901682933 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, CombatEntity_t684137495 * ___main1, int32_t ___effectId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BuffCtrl StoryMgr::ilo_get_buffCtrl18(CombatEntity)
extern "C"  BuffCtrl_t2836564350 * StoryMgr_ilo_get_buffCtrl18_m3126817845 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::ilo_AddBuff19(BuffCtrl,System.Int32)
extern "C"  void StoryMgr_ilo_AddBuff19_m1137672822 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::ilo_set_IsBirthEffect20(CombatEntity,System.Boolean)
extern "C"  void StoryMgr_ilo_set_IsBirthEffect20_m1885930122 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::ilo_OnTriggerFun21(CombatEntity,System.Int32)
extern "C"  void StoryMgr_ilo_OnTriggerFun21_m3730124298 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___effectId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::ilo_UnLockNpcBehaviourIdle22(GameMgr)
extern "C"  void StoryMgr_ilo_UnLockNpcBehaviourIdle22_m3742388185 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::ilo_ForceAddSkill23(FightCtrl,System.Int32)
extern "C"  void StoryMgr_ilo_ForceAddSkill23_m977035295 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, int32_t ___skillID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::ilo_LockView24(CameraHelper,System.Boolean,System.Int32,System.Single,System.Single,System.Single,System.Single,System.String)
extern "C"  void StoryMgr_ilo_LockView24_m560624200 (Il2CppObject * __this /* static, unused */, CameraHelper_t3196871507 * ____this0, bool ___isHero1, int32_t ___id2, float ___fov3, float ___dis4, float ___rx5, float ___ry6, String_t* ___objName7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void StoryMgr::ilo_ScreenShake25(CameraHelper,System.Single,System.Single)
extern "C"  void StoryMgr_ilo_ScreenShake25_m967088791 (Il2CppObject * __this /* static, unused */, CameraHelper_t3196871507 * ____this0, float ____fps1, float ____shakeTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
