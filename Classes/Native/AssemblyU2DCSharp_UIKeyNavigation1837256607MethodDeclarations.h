﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIKeyNavigation
struct UIKeyNavigation_t1837256607;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UIButton
struct UIButton_t179429094;
// UnityEngine.Behaviour
struct Behaviour_t200106419;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"
#include "AssemblyU2DCSharp_UIButton179429094.h"
#include "AssemblyU2DCSharp_UIKeyNavigation1837256607.h"
#include "UnityEngine_UnityEngine_Behaviour200106419.h"

// System.Void UIKeyNavigation::.ctor()
extern "C"  void UIKeyNavigation__ctor_m3664485036 (UIKeyNavigation_t1837256607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyNavigation::.cctor()
extern "C"  void UIKeyNavigation__cctor_m1447790209 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyNavigation::OnEnable()
extern "C"  void UIKeyNavigation_OnEnable_m1266556250 (UIKeyNavigation_t1837256607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyNavigation::OnDisable()
extern "C"  void UIKeyNavigation_OnDisable_m1049475347 (UIKeyNavigation_t1837256607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UIKeyNavigation::GetLeft()
extern "C"  GameObject_t3674682005 * UIKeyNavigation_GetLeft_m3827253564 (UIKeyNavigation_t1837256607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UIKeyNavigation::GetRight()
extern "C"  GameObject_t3674682005 * UIKeyNavigation_GetRight_m3825953961 (UIKeyNavigation_t1837256607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UIKeyNavigation::GetUp()
extern "C"  GameObject_t3674682005 * UIKeyNavigation_GetUp_m2980792272 (UIKeyNavigation_t1837256607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UIKeyNavigation::GetDown()
extern "C"  GameObject_t3674682005 * UIKeyNavigation_GetDown_m3607956247 (UIKeyNavigation_t1837256607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UIKeyNavigation::Get(UnityEngine.Vector3,System.Boolean)
extern "C"  GameObject_t3674682005 * UIKeyNavigation_Get_m767854105 (UIKeyNavigation_t1837256607 * __this, Vector3_t4282066566  ___myDir0, bool ___horizontal1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIKeyNavigation::GetCenter(UnityEngine.GameObject)
extern "C"  Vector3_t4282066566  UIKeyNavigation_GetCenter_m1591038087 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyNavigation::OnKey(UnityEngine.KeyCode)
extern "C"  void UIKeyNavigation_OnKey_m1333240243 (UIKeyNavigation_t1837256607 * __this, int32_t ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyNavigation::OnClick()
extern "C"  void UIKeyNavigation_OnClick_m1541142067 (UIKeyNavigation_t1837256607 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UIKeyNavigation::ilo_get_selectedObject1()
extern "C"  GameObject_t3674682005 * UIKeyNavigation_ilo_get_selectedObject1_m2400463642 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyNavigation::ilo_set_selectedObject2(UnityEngine.GameObject)
extern "C"  void UIKeyNavigation_ilo_set_selectedObject2_m4104517226 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIKeyNavigation::ilo_GetActive3(UnityEngine.GameObject)
extern "C"  bool UIKeyNavigation_ilo_GetActive3_m1503107782 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIKeyNavigation::ilo_get_isEnabled4(UIButton)
extern "C"  bool UIKeyNavigation_ilo_get_isEnabled4_m827035639 (Il2CppObject * __this /* static, unused */, UIButton_t179429094 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIKeyNavigation::ilo_GetCenter5(UnityEngine.GameObject)
extern "C"  Vector3_t4282066566  UIKeyNavigation_ilo_GetCenter5_m1040679123 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UIKeyNavigation::ilo_GetUp6(UIKeyNavigation)
extern "C"  GameObject_t3674682005 * UIKeyNavigation_ilo_GetUp6_m3614942552 (Il2CppObject * __this /* static, unused */, UIKeyNavigation_t1837256607 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UIKeyNavigation::ilo_GetDown7(UIKeyNavigation)
extern "C"  GameObject_t3674682005 * UIKeyNavigation_ilo_GetDown7_m1385248126 (Il2CppObject * __this /* static, unused */, UIKeyNavigation_t1837256607 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIKeyNavigation::ilo_GetActive8(UnityEngine.Behaviour)
extern "C"  bool UIKeyNavigation_ilo_GetActive8_m59361625 (Il2CppObject * __this /* static, unused */, Behaviour_t200106419 * ___mb0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
