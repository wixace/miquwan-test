//
//  ToUnityUtil.h
//  AnquSDKDemo
//
//  Created by XYG on 2017/8/18.
//  Copyright © 2017年 anqu. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


#ifndef ToUnityUtil_h
#define ToUnityUtil_h

typedef void (^warningCallBack)(BOOL isOk);

@interface ToUnityUtil : NSObject<UIAlertViewDelegate>
+(ToUnityUtil*) instance;

-(void)showWarningBox:(NSString*) strTitle message:(NSString*) strText leftBtnTxt:(NSString*) leftTxt rightBtnTxt:(NSString*) rightTxt callBack:(warningCallBack)callBack;

-(void)showWarningBox:(NSString*) strTitle message:(NSString*) strText leftBtnTxt:(NSString*) leftTxt callBack:(warningCallBack)callBack;

@end

NSString *_CreateNSString(const char *);

void SendMsg2Unity(const char *,const char *);

void _showWarningBox1(char* strTitle ,char* strText,char* leftBtnTxt);

void _showWarningBox2(char* strTitle ,char* strText,char* leftBtnTxt,char* rightBtnTxt);
#endif /* ToUnityUtil_h */
