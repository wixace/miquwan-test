﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginFengqi
struct  PluginFengqi_t1486316547  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginFengqi::token
	String_t* ___token_2;
	// System.String PluginFengqi::gid
	String_t* ___gid_3;
	// System.String PluginFengqi::ptid
	String_t* ___ptid_4;
	// Mihua.SDK.PayInfo PluginFengqi::payInfo
	PayInfo_t1775308120 * ___payInfo_5;
	// System.String PluginFengqi::configId
	String_t* ___configId_6;

public:
	inline static int32_t get_offset_of_token_2() { return static_cast<int32_t>(offsetof(PluginFengqi_t1486316547, ___token_2)); }
	inline String_t* get_token_2() const { return ___token_2; }
	inline String_t** get_address_of_token_2() { return &___token_2; }
	inline void set_token_2(String_t* value)
	{
		___token_2 = value;
		Il2CppCodeGenWriteBarrier(&___token_2, value);
	}

	inline static int32_t get_offset_of_gid_3() { return static_cast<int32_t>(offsetof(PluginFengqi_t1486316547, ___gid_3)); }
	inline String_t* get_gid_3() const { return ___gid_3; }
	inline String_t** get_address_of_gid_3() { return &___gid_3; }
	inline void set_gid_3(String_t* value)
	{
		___gid_3 = value;
		Il2CppCodeGenWriteBarrier(&___gid_3, value);
	}

	inline static int32_t get_offset_of_ptid_4() { return static_cast<int32_t>(offsetof(PluginFengqi_t1486316547, ___ptid_4)); }
	inline String_t* get_ptid_4() const { return ___ptid_4; }
	inline String_t** get_address_of_ptid_4() { return &___ptid_4; }
	inline void set_ptid_4(String_t* value)
	{
		___ptid_4 = value;
		Il2CppCodeGenWriteBarrier(&___ptid_4, value);
	}

	inline static int32_t get_offset_of_payInfo_5() { return static_cast<int32_t>(offsetof(PluginFengqi_t1486316547, ___payInfo_5)); }
	inline PayInfo_t1775308120 * get_payInfo_5() const { return ___payInfo_5; }
	inline PayInfo_t1775308120 ** get_address_of_payInfo_5() { return &___payInfo_5; }
	inline void set_payInfo_5(PayInfo_t1775308120 * value)
	{
		___payInfo_5 = value;
		Il2CppCodeGenWriteBarrier(&___payInfo_5, value);
	}

	inline static int32_t get_offset_of_configId_6() { return static_cast<int32_t>(offsetof(PluginFengqi_t1486316547, ___configId_6)); }
	inline String_t* get_configId_6() const { return ___configId_6; }
	inline String_t** get_address_of_configId_6() { return &___configId_6; }
	inline void set_configId_6(String_t* value)
	{
		___configId_6 = value;
		Il2CppCodeGenWriteBarrier(&___configId_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
