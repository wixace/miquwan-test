﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.NavMeshHit
struct NavMeshHit_t1624713351;
struct NavMeshHit_t1624713351_marshaled_pinvoke;
struct NavMeshHit_t1624713351_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_NavMeshHit1624713351.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// UnityEngine.Vector3 UnityEngine.NavMeshHit::get_position()
extern "C"  Vector3_t4282066566  NavMeshHit_get_position_m3829439174 (NavMeshHit_t1624713351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshHit::set_position(UnityEngine.Vector3)
extern "C"  void NavMeshHit_set_position_m3977903257 (NavMeshHit_t1624713351 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.NavMeshHit::get_normal()
extern "C"  Vector3_t4282066566  NavMeshHit_get_normal_m524144132 (NavMeshHit_t1624713351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshHit::set_normal(UnityEngine.Vector3)
extern "C"  void NavMeshHit_set_normal_m351628827 (NavMeshHit_t1624713351 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.NavMeshHit::get_distance()
extern "C"  float NavMeshHit_get_distance_m311503268 (NavMeshHit_t1624713351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshHit::set_distance(System.Single)
extern "C"  void NavMeshHit_set_distance_m2567567903 (NavMeshHit_t1624713351 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.NavMeshHit::get_mask()
extern "C"  int32_t NavMeshHit_get_mask_m3047510607 (NavMeshHit_t1624713351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshHit::set_mask(System.Int32)
extern "C"  void NavMeshHit_set_mask_m1944975380 (NavMeshHit_t1624713351 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshHit::get_hit()
extern "C"  bool NavMeshHit_get_hit_m3075741836 (NavMeshHit_t1624713351 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshHit::set_hit(System.Boolean)
extern "C"  void NavMeshHit_set_hit_m3860651957 (NavMeshHit_t1624713351 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct NavMeshHit_t1624713351;
struct NavMeshHit_t1624713351_marshaled_pinvoke;

extern "C" void NavMeshHit_t1624713351_marshal_pinvoke(const NavMeshHit_t1624713351& unmarshaled, NavMeshHit_t1624713351_marshaled_pinvoke& marshaled);
extern "C" void NavMeshHit_t1624713351_marshal_pinvoke_back(const NavMeshHit_t1624713351_marshaled_pinvoke& marshaled, NavMeshHit_t1624713351& unmarshaled);
extern "C" void NavMeshHit_t1624713351_marshal_pinvoke_cleanup(NavMeshHit_t1624713351_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct NavMeshHit_t1624713351;
struct NavMeshHit_t1624713351_marshaled_com;

extern "C" void NavMeshHit_t1624713351_marshal_com(const NavMeshHit_t1624713351& unmarshaled, NavMeshHit_t1624713351_marshaled_com& marshaled);
extern "C" void NavMeshHit_t1624713351_marshal_com_back(const NavMeshHit_t1624713351_marshaled_com& marshaled, NavMeshHit_t1624713351& unmarshaled);
extern "C" void NavMeshHit_t1624713351_marshal_com_cleanup(NavMeshHit_t1624713351_marshaled_com& marshaled);
