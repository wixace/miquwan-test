﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JProperty
struct JProperty_t2616415645;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken>
struct IList_1_t1811925858;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// Newtonsoft.Json.JsonConverter[]
struct JsonConverterU5BU5D_t638349667;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// Newtonsoft.Json.Linq.JContainer
struct JContainer_t3364442311;
// System.IFormatProvider
struct IFormatProvider_t192740775;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JProperty2616415645.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType3916897561.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JContainer3364442311.h"

// System.Void Newtonsoft.Json.Linq.JProperty::.ctor(Newtonsoft.Json.Linq.JProperty)
extern "C"  void JProperty__ctor_m1736115663 (JProperty_t2616415645 * __this, JProperty_t2616415645 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty::.ctor(System.String)
extern "C"  void JProperty__ctor_m3440048749 (JProperty_t2616415645 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty::.ctor(System.String,System.Object[])
extern "C"  void JProperty__ctor_m1031411417 (JProperty_t2616415645 * __this, String_t* ___name0, ObjectU5BU5D_t1108656482* ___content1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty::.ctor(System.String,System.Object)
extern "C"  void JProperty__ctor_m3647996539 (JProperty_t2616415645 * __this, String_t* ___name0, Il2CppObject * ___content1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JProperty::get_ChildrenTokens()
extern "C"  Il2CppObject* JProperty_get_ChildrenTokens_m1382921243 (JProperty_t2616415645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JProperty::get_Name()
extern "C"  String_t* JProperty_get_Name_m2764667942 (JProperty_t2616415645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty::get_Value()
extern "C"  JToken_t3412245951 * JProperty_get_Value_m3171771760 (JProperty_t2616415645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty::set_Value(Newtonsoft.Json.Linq.JToken)
extern "C"  void JProperty_set_Value_m637899939 (JProperty_t2616415645 * __this, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty::GetItem(System.Int32)
extern "C"  JToken_t3412245951 * JProperty_GetItem_m4286853890 (JProperty_t2616415645 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty::SetItem(System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JProperty_SetItem_m982472797 (JProperty_t2616415645 * __this, int32_t ___index0, JToken_t3412245951 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JProperty::RemoveItem(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JProperty_RemoveItem_m3702404048 (JProperty_t2616415645 * __this, JToken_t3412245951 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty::RemoveItemAt(System.Int32)
extern "C"  void JProperty_RemoveItemAt_m2778348426 (JProperty_t2616415645 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty::InsertItem(System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JProperty_InsertItem_m3814345872 (JProperty_t2616415645 * __this, int32_t ___index0, JToken_t3412245951 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JProperty::ContainsItem(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JProperty_ContainsItem_m2786010645 (JProperty_t2616415645 * __this, JToken_t3412245951 * ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty::ClearItems()
extern "C"  void JProperty_ClearItems_m2824977954 (JProperty_t2616415645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JProperty::DeepEquals(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JProperty_DeepEquals_m4287289916 (JProperty_t2616415645 * __this, JToken_t3412245951 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty::CloneToken()
extern "C"  JToken_t3412245951 * JProperty_CloneToken_m931887702 (JProperty_t2616415645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JProperty::get_Type()
extern "C"  int32_t JProperty_get_Type_m1975905699 (JProperty_t2616415645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty::WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
extern "C"  void JProperty_WriteTo_m3392406980 (JProperty_t2616415645 * __this, JsonWriter_t972330355 * ___writer0, JsonConverterU5BU5D_t638349667* ___converters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JProperty::GetDeepHashCode()
extern "C"  int32_t JProperty_GetDeepHashCode_m705211746 (JProperty_t2616415645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JProperty Newtonsoft.Json.Linq.JProperty::Load(Newtonsoft.Json.JsonReader)
extern "C"  JProperty_t2616415645 * JProperty_Load_m416865356 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty::ilo_CheckReentrancy1(Newtonsoft.Json.Linq.JContainer)
extern "C"  void JProperty_ilo_CheckReentrancy1_m3392751060 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty::ilo_InsertItem2(Newtonsoft.Json.Linq.JProperty,System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JProperty_ilo_InsertItem2_m367928859 (Il2CppObject * __this /* static, unused */, JProperty_t2616415645 * ____this0, int32_t ___index1, JToken_t3412245951 * ___item2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JProperty::ilo_IsTokenUnchanged3(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern "C"  bool JProperty_ilo_IsTokenUnchanged3_m1389557243 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___currentValue0, JToken_t3412245951 * ___newValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JProperty::ilo_get_Parent4(Newtonsoft.Json.Linq.JToken)
extern "C"  JContainer_t3364442311 * JProperty_ilo_get_Parent4_m1783494828 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty::ilo_SetItem5(Newtonsoft.Json.Linq.JContainer,System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JProperty_ilo_SetItem5_m4015894395 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, int32_t ___index1, JToken_t3412245951 * ___item2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JProperty::ilo_FormatWith6(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* JProperty_ilo_FormatWith6_m1679219092 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty::ilo_get_Value7(Newtonsoft.Json.Linq.JProperty)
extern "C"  JToken_t3412245951 * JProperty_ilo_get_Value7_m50502390 (Il2CppObject * __this /* static, unused */, JProperty_t2616415645 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JProperty::ilo_WriteTo8(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
extern "C"  void JProperty_ilo_WriteTo8_m354897587 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, JsonWriter_t972330355 * ___writer1, JsonConverterU5BU5D_t638349667* ___converters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JProperty::ilo_GetDeepHashCode9(Newtonsoft.Json.Linq.JToken)
extern "C"  int32_t JProperty_ilo_GetDeepHashCode9_m71711358 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JProperty::ilo_Read10(Newtonsoft.Json.JsonReader)
extern "C"  bool JProperty_ilo_Read10_m1957648555 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
