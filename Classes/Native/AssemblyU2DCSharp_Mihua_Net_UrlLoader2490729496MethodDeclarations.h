﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.Net.UrlLoader
struct UrlLoader_t2490729496;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// UnityEngine.WWW
struct WWW_t3134621005;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// UnityEngine.AssetBundle
struct AssetBundle_t2070959688;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_WWWForm461342257.h"
#include "UnityEngine_UnityEngine_WWW3134621005.h"
#include "AssemblyU2DCSharp_Mihua_Net_UrlLoader2490729496.h"

// System.Void Mihua.Net.UrlLoader::.ctor(System.String)
extern "C"  void UrlLoader__ctor_m909976722 (UrlLoader_t2490729496 * __this, String_t* ___url0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Net.UrlLoader::.ctor(System.String,UnityEngine.WWWForm)
extern "C"  void UrlLoader__ctor_m3822330586 (UrlLoader_t2490729496 * __this, String_t* ___url0, WWWForm_t461342257 * ___form1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW Mihua.Net.UrlLoader::get_www()
extern "C"  WWW_t3134621005 * UrlLoader_get_www_m2408568303 (UrlLoader_t2490729496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Net.UrlLoader::set_www(UnityEngine.WWW)
extern "C"  void UrlLoader_set_www_m713787722 (UrlLoader_t2490729496 * __this, WWW_t3134621005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua.Net.UrlLoader::get_url()
extern "C"  String_t* UrlLoader_get_url_m948330711 (UrlLoader_t2490729496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Mihua.Net.UrlLoader::get_error()
extern "C"  String_t* UrlLoader_get_error_m3800284528 (UrlLoader_t2490729496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Mihua.Net.UrlLoader::get_progress()
extern "C"  float UrlLoader_get_progress_m2168131294 (UrlLoader_t2490729496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Mihua.Net.UrlLoader::get_bytes()
extern "C"  ByteU5BU5D_t4260760469* UrlLoader_get_bytes_m778445786 (UrlLoader_t2490729496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundle Mihua.Net.UrlLoader::get_assetBundle()
extern "C"  AssetBundle_t2070959688 * UrlLoader_get_assetBundle_m3018133967 (UrlLoader_t2490729496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Net.UrlLoader::get_isDone()
extern "C"  bool UrlLoader_get_isDone_m1482123221 (UrlLoader_t2490729496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Net.UrlLoader::get_isTimeout()
extern "C"  bool UrlLoader_get_isTimeout_m128089360 (UrlLoader_t2490729496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.Net.UrlLoader::Dispose()
extern "C"  void UrlLoader_Dispose_m1846558893 (UrlLoader_t2490729496 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWW Mihua.Net.UrlLoader::ilo_get_www1(Mihua.Net.UrlLoader)
extern "C"  WWW_t3134621005 * UrlLoader_ilo_get_www1_m1096910688 (Il2CppObject * __this /* static, unused */, UrlLoader_t2490729496 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.Net.UrlLoader::ilo_get_isTimeout2(Mihua.Net.UrlLoader)
extern "C"  bool UrlLoader_ilo_get_isTimeout2_m3365691488 (Il2CppObject * __this /* static, unused */, UrlLoader_t2490729496 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
