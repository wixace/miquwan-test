﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonSerializerInternalWriter
struct JsonSerializerInternalWriter_t3814549686;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1244034627;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Serialization.JsonSerializerProxy
struct JsonSerializerProxy_t3893567258;
// Newtonsoft.Json.Serialization.JsonContract
struct JsonContract_t1328848902;
// Newtonsoft.Json.Serialization.JsonPrimitiveContract
struct JsonPrimitiveContract_t554063095;
// Newtonsoft.Json.Serialization.JsonProperty
struct JsonProperty_t902655177;
// System.Type
struct Type_t;
// System.String
struct String_t;
// Newtonsoft.Json.Serialization.JsonStringContract
struct JsonStringContract_t4087007991;
// Newtonsoft.Json.Serialization.JObjectContract
struct JObjectContract_t2867848225;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t2159686854;
// Newtonsoft.Json.Utilities.IWrappedCollection
struct IWrappedCollection_t3896884266;
// Newtonsoft.Json.Serialization.JsonArrayContract
struct JsonArrayContract_t145179369;
// System.Runtime.Serialization.ISerializable
struct ISerializable_t867484142;
// Newtonsoft.Json.Serialization.JsonISerializableContract
struct JsonISerializableContract_t624170136;
// Newtonsoft.Json.Utilities.IWrappedDictionary
struct IWrappedDictionary_t1790279202;
// Newtonsoft.Json.Serialization.JsonDictionaryContract
struct JsonDictionaryContract_t989352188;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_t507353639;
// Newtonsoft.Json.Serialization.JsonSerializerInternalBase
struct JsonSerializerInternalBase_t2068678036;
// Newtonsoft.Json.Serialization.IReferenceResolver
struct IReferenceResolver_t425424564;
// System.ComponentModel.TypeConverter
struct TypeConverter_t1753450284;
// Newtonsoft.Json.Serialization.JsonPropertyCollection
struct JsonPropertyCollection_t717767559;
// System.Exception
struct Exception_t3991598821;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_t2137423328;
// System.Predicate`1<System.Object>
struct Predicate_1_t3781873254;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer251850770.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso554063095.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso902655177.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js1328848902.h"
#include "mscorlib_System_Nullable_1_gen2845787645.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js4087007991.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_JO2867848225.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_DefaultValueHand1569448045.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_PreserveReferenc4230591217.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_TypeNameHandling2359325474.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonConverter2159686854.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso145179369.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso624170136.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso989352188.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js3814549686.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js2068678036.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_NullValueHandlin2754652381.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_ReferenceLoopHan2761661122.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"
#include "mscorlib_System_Exception3991598821.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_F3005881063.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2137423328.h"

// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::.ctor(Newtonsoft.Json.JsonSerializer)
extern "C"  void JsonSerializerInternalWriter__ctor_m1848814959 (JsonSerializerInternalWriter_t3814549686 * __this, JsonSerializer_t251850770 * ___serializer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::get_SerializeStack()
extern "C"  List_1_t1244034627 * JsonSerializerInternalWriter_get_SerializeStack_m1412853750 (JsonSerializerInternalWriter_t3814549686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::Serialize(Newtonsoft.Json.JsonWriter,System.Object)
extern "C"  void JsonSerializerInternalWriter_Serialize_m2835988858 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___jsonWriter0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonSerializerProxy Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetInternalSerializer()
extern "C"  JsonSerializerProxy_t3893567258 * JsonSerializerInternalWriter_GetInternalSerializer_m3854597871 (JsonSerializerInternalWriter_t3814549686 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetContractSafe(System.Object)
extern "C"  JsonContract_t1328848902 * JsonSerializerInternalWriter_GetContractSafe_m4065236339 (JsonSerializerInternalWriter_t3814549686 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializePrimitive(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonPrimitiveContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonSerializerInternalWriter_SerializePrimitive_m3054056572 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonPrimitiveContract_t554063095 * ___contract2, JsonProperty_t902655177 * ___member3, JsonContract_t1328848902 * ___collectionValueContract4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeValue(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonSerializerInternalWriter_SerializeValue_m4265338791 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonContract_t1328848902 * ___valueContract2, JsonProperty_t902655177 * ___member3, JsonContract_t1328848902 * ___collectionValueContract4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldWriteReference(System.Object,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  bool JsonSerializerInternalWriter_ShouldWriteReference_m2495927838 (JsonSerializerInternalWriter_t3814549686 * __this, Il2CppObject * ___value0, JsonProperty_t902655177 * ___property1, JsonContract_t1328848902 * ___contract2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteMemberInfoProperty(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonSerializerInternalWriter_WriteMemberInfoProperty_m1842446617 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___memberValue1, JsonProperty_t902655177 * ___property2, JsonContract_t1328848902 * ___contract3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::CheckForCircularReference(System.Object,System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  bool JsonSerializerInternalWriter_CheckForCircularReference_m3431206558 (JsonSerializerInternalWriter_t3814549686 * __this, Il2CppObject * ___value0, Nullable_1_t2845787645  ___referenceLoopHandling1, JsonContract_t1328848902 * ___contract2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteReference(Newtonsoft.Json.JsonWriter,System.Object)
extern "C"  void JsonSerializerInternalWriter_WriteReference_m540678128 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::TryConvertToString(System.Object,System.Type,System.String&)
extern "C"  bool JsonSerializerInternalWriter_TryConvertToString_m4077103143 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, Type_t * ___type1, String_t** ___s2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeString(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonStringContract)
extern "C"  void JsonSerializerInternalWriter_SerializeString_m1686419901 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonStringContract_t4087007991 * ___contract2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeObject(Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonSerializerInternalWriter_SerializeObject_m2466846720 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JObjectContract_t2867848225 * ___contract2, JsonProperty_t902655177 * ___member3, JsonContract_t1328848902 * ___collectionValueContract4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::WriteTypeProperty(Newtonsoft.Json.JsonWriter,System.Type)
extern "C"  void JsonSerializerInternalWriter_WriteTypeProperty_m2339993005 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HasFlag(Newtonsoft.Json.DefaultValueHandling,Newtonsoft.Json.DefaultValueHandling)
extern "C"  bool JsonSerializerInternalWriter_HasFlag_m2358115192 (JsonSerializerInternalWriter_t3814549686 * __this, int32_t ___value0, int32_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HasFlag(Newtonsoft.Json.PreserveReferencesHandling,Newtonsoft.Json.PreserveReferencesHandling)
extern "C"  bool JsonSerializerInternalWriter_HasFlag_m3934316280 (JsonSerializerInternalWriter_t3814549686 * __this, int32_t ___value0, int32_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HasFlag(Newtonsoft.Json.TypeNameHandling,Newtonsoft.Json.TypeNameHandling)
extern "C"  bool JsonSerializerInternalWriter_HasFlag_m180899544 (JsonSerializerInternalWriter_t3814549686 * __this, int32_t ___value0, int32_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeConvertable(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter,System.Object,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonSerializerInternalWriter_SerializeConvertable_m1388543327 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, JsonConverter_t2159686854 * ___converter1, Il2CppObject * ___value2, JsonContract_t1328848902 * ___contract3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeList(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Utilities.IWrappedCollection,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonSerializerInternalWriter_SerializeList_m3696374760 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___values1, JsonArrayContract_t145179369 * ___contract2, JsonProperty_t902655177 * ___member3, JsonContract_t1328848902 * ___collectionValueContract4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeISerializable(Newtonsoft.Json.JsonWriter,System.Runtime.Serialization.ISerializable,Newtonsoft.Json.Serialization.JsonISerializableContract)
extern "C"  void JsonSerializerInternalWriter_SerializeISerializable_m3673091936 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___value1, JsonISerializableContract_t624170136 * ___contract2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldWriteType(Newtonsoft.Json.TypeNameHandling,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  bool JsonSerializerInternalWriter_ShouldWriteType_m2384757453 (JsonSerializerInternalWriter_t3814549686 * __this, int32_t ___typeNameHandlingFlag0, JsonContract_t1328848902 * ___contract1, JsonProperty_t902655177 * ___member2, JsonContract_t1328848902 * ___collectionValueContract3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::SerializeDictionary(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Utilities.IWrappedDictionary,Newtonsoft.Json.Serialization.JsonDictionaryContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonSerializerInternalWriter_SerializeDictionary_m3859124877 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, Il2CppObject * ___values1, JsonDictionaryContract_t989352188 * ___contract2, JsonProperty_t902655177 * ___member3, JsonContract_t1328848902 * ___collectionValueContract4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::GetPropertyName(System.Collections.DictionaryEntry)
extern "C"  String_t* JsonSerializerInternalWriter_GetPropertyName_m2239834123 (JsonSerializerInternalWriter_t3814549686 * __this, DictionaryEntry_t1751606614  ___entry0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::HandleError(Newtonsoft.Json.JsonWriter,System.Int32)
extern "C"  void JsonSerializerInternalWriter_HandleError_m3631831435 (JsonSerializerInternalWriter_t3814549686 * __this, JsonWriter_t972330355 * ___writer0, int32_t ___initialDepth1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ShouldSerialize(Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern "C"  bool JsonSerializerInternalWriter_ShouldSerialize_m1285789587 (JsonSerializerInternalWriter_t3814549686 * __this, JsonProperty_t902655177 * ___property0, Il2CppObject * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::IsSpecified(Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern "C"  bool JsonSerializerInternalWriter_IsSpecified_m3458256652 (JsonSerializerInternalWriter_t3814549686 * __this, JsonProperty_t902655177 * ___property0, Il2CppObject * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_GetContractSafe1(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter,System.Object)
extern "C"  JsonContract_t1328848902 * JsonSerializerInternalWriter_ilo_GetContractSafe1_m826359062 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalWriter_t3814549686 * ____this0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_SerializeValue2(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter,Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonSerializerInternalWriter_ilo_SerializeValue2_m3475529669 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalWriter_t3814549686 * ____this0, JsonWriter_t972330355 * ___writer1, Il2CppObject * ___value2, JsonContract_t1328848902 * ___valueContract3, JsonProperty_t902655177 * ___member4, JsonContract_t1328848902 * ___collectionValueContract5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_ResolveContract3(Newtonsoft.Json.Serialization.IContractResolver,System.Type)
extern "C"  JsonContract_t1328848902 * JsonSerializerInternalWriter_ilo_ResolveContract3_m1131800985 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_WriteTypeProperty4(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter,Newtonsoft.Json.JsonWriter,System.Type)
extern "C"  void JsonSerializerInternalWriter_ilo_WriteTypeProperty4_m1569501639 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalWriter_t3814549686 * ____this0, JsonWriter_t972330355 * ___writer1, Type_t * ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonSerializer Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_Serializer5(Newtonsoft.Json.Serialization.JsonSerializerInternalBase)
extern "C"  JsonSerializer_t251850770 * JsonSerializerInternalWriter_ilo_get_Serializer5_m1220781594 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalBase_t2068678036 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_SerializeConvertable6(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter,Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter,System.Object,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonSerializerInternalWriter_ilo_SerializeConvertable6_m2895019201 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalWriter_t3814549686 * ____this0, JsonWriter_t972330355 * ___writer1, JsonConverter_t2159686854 * ___converter2, Il2CppObject * ___value3, JsonContract_t1328848902 * ___contract4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_SerializePrimitive7(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter,Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonPrimitiveContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonSerializerInternalWriter_ilo_SerializePrimitive7_m3711854841 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalWriter_t3814549686 * ____this0, JsonWriter_t972330355 * ___writer1, Il2CppObject * ___value2, JsonPrimitiveContract_t554063095 * ___contract3, JsonProperty_t902655177 * ___member4, JsonContract_t1328848902 * ___collectionValueContract5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_SerializeObject8(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter,Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JObjectContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonSerializerInternalWriter_ilo_SerializeObject8_m1648692438 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalWriter_t3814549686 * ____this0, JsonWriter_t972330355 * ___writer1, Il2CppObject * ___value2, JObjectContract_t2867848225 * ___contract3, JsonProperty_t902655177 * ___member4, JsonContract_t1328848902 * ___collectionValueContract5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_SerializeList9(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter,Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Utilities.IWrappedCollection,Newtonsoft.Json.Serialization.JsonArrayContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonSerializerInternalWriter_ilo_SerializeList9_m2005779923 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalWriter_t3814549686 * ____this0, JsonWriter_t972330355 * ___writer1, Il2CppObject * ___values2, JsonArrayContract_t145179369 * ___contract3, JsonProperty_t902655177 * ___member4, JsonContract_t1328848902 * ___collectionValueContract5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.PreserveReferencesHandling Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_PreserveReferencesHandling10(Newtonsoft.Json.JsonSerializer)
extern "C"  int32_t JsonSerializerInternalWriter_ilo_get_PreserveReferencesHandling10_m512446394 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_IsReferenced11(Newtonsoft.Json.Serialization.IReferenceResolver,System.Object,System.Object)
extern "C"  bool JsonSerializerInternalWriter_ilo_IsReferenced11_m2954522961 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___context1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_PropertyName12(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  String_t* JsonSerializerInternalWriter_ilo_get_PropertyName12_m3490372022 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.NullValueHandling Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_NullValueHandling13(Newtonsoft.Json.JsonSerializer)
extern "C"  int32_t JsonSerializerInternalWriter_ilo_get_NullValueHandling13_m3923144937 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_ValueEquals14(System.Object,System.Object)
extern "C"  bool JsonSerializerInternalWriter_ilo_ValueEquals14_m3684088282 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___objA0, Il2CppObject * ___objB1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_WritePropertyName15(Newtonsoft.Json.JsonWriter,System.String)
extern "C"  void JsonSerializerInternalWriter_ilo_WritePropertyName15_m2645666200 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_WriteReference16(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter,Newtonsoft.Json.JsonWriter,System.Object)
extern "C"  void JsonSerializerInternalWriter_ilo_WriteReference16_m3735546167 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalWriter_t3814549686 * ____this0, JsonWriter_t972330355 * ___writer1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_ReferenceLoopHandling17(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Nullable_1_t2845787645  JsonSerializerInternalWriter_ilo_get_ReferenceLoopHandling17_m2433937207 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_SerializeStack18(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter)
extern "C"  List_1_t1244034627 * JsonSerializerInternalWriter_ilo_get_SerializeStack18_m1958594163 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalWriter_t3814549686 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.ReferenceLoopHandling Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_ReferenceLoopHandling19(Newtonsoft.Json.JsonSerializer)
extern "C"  int32_t JsonSerializerInternalWriter_ilo_get_ReferenceLoopHandling19_m2499616249 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IReferenceResolver Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_ReferenceResolver20(Newtonsoft.Json.JsonSerializer)
extern "C"  Il2CppObject * JsonSerializerInternalWriter_ilo_get_ReferenceResolver20_m2132865630 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_WriteEndObject21(Newtonsoft.Json.JsonWriter)
extern "C"  void JsonSerializerInternalWriter_ilo_WriteEndObject21_m916556227 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.TypeConverter Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_GetConverter22(System.Type)
extern "C"  TypeConverter_t1753450284 * JsonSerializerInternalWriter_ilo_GetConverter22_m1284550086 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_InvokeOnSerializing23(Newtonsoft.Json.Serialization.JsonContract,System.Object,System.Runtime.Serialization.StreamingContext)
extern "C"  void JsonSerializerInternalWriter_ilo_InvokeOnSerializing23_m923928413 (Il2CppObject * __this /* static, unused */, JsonContract_t1328848902 * ____this0, Il2CppObject * ___o1, StreamingContext_t2761351129  ___context2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_TryConvertToString24(System.Object,System.Type,System.String&)
extern "C"  bool JsonSerializerInternalWriter_ilo_TryConvertToString24_m990634780 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, Type_t * ___type1, String_t** ___s2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_WriteValue25(Newtonsoft.Json.JsonWriter,System.String)
extern "C"  void JsonSerializerInternalWriter_ilo_WriteValue25_m3721790266 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_Context26(Newtonsoft.Json.JsonSerializer)
extern "C"  StreamingContext_t2761351129  JsonSerializerInternalWriter_ilo_get_Context26_m92921265 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_WriteStartObject27(Newtonsoft.Json.JsonWriter)
extern "C"  void JsonSerializerInternalWriter_ilo_WriteStartObject27_m3832002192 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_IsReference28(Newtonsoft.Json.Serialization.JsonContract)
extern "C"  Nullable_1_t560925241  JsonSerializerInternalWriter_ilo_get_IsReference28_m783742481 (Il2CppObject * __this /* static, unused */, JsonContract_t1328848902 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_ShouldWriteType29(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter,Newtonsoft.Json.TypeNameHandling,Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  bool JsonSerializerInternalWriter_ilo_ShouldWriteType29_m1564745466 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalWriter_t3814549686 * ____this0, int32_t ___typeNameHandlingFlag1, JsonContract_t1328848902 * ___contract2, JsonProperty_t902655177 * ___member3, JsonContract_t1328848902 * ___collectionValueContract4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_Properties30(Newtonsoft.Json.Serialization.JObjectContract)
extern "C"  JsonPropertyCollection_t717767559 * JsonSerializerInternalWriter_ilo_get_Properties30_m511870057 (Il2CppObject * __this /* static, unused */, JObjectContract_t2867848225 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_Ignored31(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  bool JsonSerializerInternalWriter_ilo_get_Ignored31_m3181841548 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_ShouldSerialize32(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter,Newtonsoft.Json.Serialization.JsonProperty,System.Object)
extern "C"  bool JsonSerializerInternalWriter_ilo_ShouldSerialize32_m2143668288 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalWriter_t3814549686 * ____this0, JsonProperty_t902655177 * ___property1, Il2CppObject * ___target2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_WriteMemberInfoProperty33(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter,Newtonsoft.Json.JsonWriter,System.Object,Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void JsonSerializerInternalWriter_ilo_WriteMemberInfoProperty33_m1556707455 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalWriter_t3814549686 * ____this0, JsonWriter_t972330355 * ___writer1, Il2CppObject * ___memberValue2, JsonProperty_t902655177 * ___property3, JsonContract_t1328848902 * ___contract4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_IsErrorHandled34(Newtonsoft.Json.Serialization.JsonSerializerInternalBase,System.Object,Newtonsoft.Json.Serialization.JsonContract,System.Object,System.Exception)
extern "C"  bool JsonSerializerInternalWriter_ilo_IsErrorHandled34_m3678770350 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalBase_t2068678036 * ____this0, Il2CppObject * ___currentObject1, JsonContract_t1328848902 * ___contract2, Il2CppObject * ___keyValue3, Exception_t3991598821 * ___ex4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_TypeNameAssemblyFormat35(Newtonsoft.Json.JsonSerializer)
extern "C"  int32_t JsonSerializerInternalWriter_ilo_get_TypeNameAssemblyFormat35_m1346866305 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.SerializationBinder Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_Binder36(Newtonsoft.Json.JsonSerializer)
extern "C"  SerializationBinder_t2137423328 * JsonSerializerInternalWriter_ilo_get_Binder36_m3080236726 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_GetTypeName37(System.Type,System.Runtime.Serialization.Formatters.FormatterAssemblyStyle,System.Runtime.Serialization.SerializationBinder)
extern "C"  String_t* JsonSerializerInternalWriter_ilo_GetTypeName37_m3885512064 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, int32_t ___assemblyFormat1, SerializationBinder_t2137423328 * ___binder2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_UnderlyingCollection38(Newtonsoft.Json.Utilities.IWrappedCollection)
extern "C"  Il2CppObject * JsonSerializerInternalWriter_ilo_get_UnderlyingCollection38_m404240100 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_HasFlag39(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter,Newtonsoft.Json.PreserveReferencesHandling,Newtonsoft.Json.PreserveReferencesHandling)
extern "C"  bool JsonSerializerInternalWriter_ilo_HasFlag39_m2879670212 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalWriter_t3814549686 * ____this0, int32_t ___value1, int32_t ___flag2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_WriteStartArray40(Newtonsoft.Json.JsonWriter)
extern "C"  void JsonSerializerInternalWriter_ilo_WriteStartArray40_m1042137931 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_CheckForCircularReference41(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter,System.Object,System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  bool JsonSerializerInternalWriter_ilo_CheckForCircularReference41_m1964132843 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalWriter_t3814549686 * ____this0, Il2CppObject * ___value1, Nullable_1_t2845787645  ___referenceLoopHandling2, JsonContract_t1328848902 * ___contract3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_HandleError42(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter,Newtonsoft.Json.JsonWriter,System.Int32)
extern "C"  void JsonSerializerInternalWriter_ilo_HandleError42_m2680537931 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalWriter_t3814549686 * ____this0, JsonWriter_t972330355 * ___writer1, int32_t ___initialDepth2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_WriteEndArray43(Newtonsoft.Json.JsonWriter)
extern "C"  void JsonSerializerInternalWriter_ilo_WriteEndArray43_m2607355765 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_InvokeOnSerialized44(Newtonsoft.Json.Serialization.JsonContract,System.Object,System.Runtime.Serialization.StreamingContext)
extern "C"  void JsonSerializerInternalWriter_ilo_InvokeOnSerialized44_m2871667329 (Il2CppObject * __this /* static, unused */, JsonContract_t1328848902 * ____this0, Il2CppObject * ___o1, StreamingContext_t2761351129  ___context2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_UnderlyingType45(Newtonsoft.Json.Serialization.JsonContract)
extern "C"  Type_t * JsonSerializerInternalWriter_ilo_get_UnderlyingType45_m1650336871 (Il2CppObject * __this /* static, unused */, JsonContract_t1328848902 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.TypeNameHandling Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_TypeNameHandling46(Newtonsoft.Json.JsonSerializer)
extern "C"  int32_t JsonSerializerInternalWriter_ilo_get_TypeNameHandling46_m1213559997 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_PropertyType47(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Type_t * JsonSerializerInternalWriter_ilo_get_PropertyType47_m3811485758 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_ContractResolver48(Newtonsoft.Json.JsonSerializer)
extern "C"  Il2CppObject * JsonSerializerInternalWriter_ilo_get_ContractResolver48_m471971860 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_UnderlyingDictionary49(Newtonsoft.Json.Utilities.IWrappedDictionary)
extern "C"  Il2CppObject * JsonSerializerInternalWriter_ilo_get_UnderlyingDictionary49_m633105924 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_GetReference50(Newtonsoft.Json.Serialization.IReferenceResolver,System.Object,System.Object)
extern "C"  String_t* JsonSerializerInternalWriter_ilo_GetReference50_m3369020109 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___context1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_DictionaryValueType51(Newtonsoft.Json.Serialization.JsonDictionaryContract)
extern "C"  Type_t * JsonSerializerInternalWriter_ilo_get_DictionaryValueType51_m943661460 (Il2CppObject * __this /* static, unused */, JsonDictionaryContract_t989352188 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_GetPropertyName52(Newtonsoft.Json.Serialization.JsonSerializerInternalWriter,System.Collections.DictionaryEntry)
extern "C"  String_t* JsonSerializerInternalWriter_ilo_GetPropertyName52_m87432902 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalWriter_t3814549686 * ____this0, DictionaryEntry_t1751606614  ___entry1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_ClearErrorContext53(Newtonsoft.Json.Serialization.JsonSerializerInternalBase)
extern "C"  void JsonSerializerInternalWriter_ilo_ClearErrorContext53_m545070432 (Il2CppObject * __this /* static, unused */, JsonSerializerInternalBase_t2068678036 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::ilo_get_ShouldSerialize54(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Predicate_1_t3781873254 * JsonSerializerInternalWriter_ilo_get_ShouldSerialize54_m999498972 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
