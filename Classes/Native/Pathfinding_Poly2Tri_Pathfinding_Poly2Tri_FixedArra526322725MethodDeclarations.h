﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t3176762032;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_FixedArra526322725.h"
#include "mscorlib_System_Object4170816371.h"

// System.Collections.IEnumerator Pathfinding.Poly2Tri.FixedArray3`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * FixedArray3_1_System_Collections_IEnumerable_GetEnumerator_m4035815537_gshared (FixedArray3_1_t526322725 * __this, const MethodInfo* method);
#define FixedArray3_1_System_Collections_IEnumerable_GetEnumerator_m4035815537(__this, method) ((  Il2CppObject * (*) (FixedArray3_1_t526322725 *, const MethodInfo*))FixedArray3_1_System_Collections_IEnumerable_GetEnumerator_m4035815537_gshared)(__this, method)
// T Pathfinding.Poly2Tri.FixedArray3`1<System.Object>::get_Item(System.Int32)
extern "C"  Il2CppObject * FixedArray3_1_get_Item_m2156685124_gshared (FixedArray3_1_t526322725 * __this, int32_t ___index0, const MethodInfo* method);
#define FixedArray3_1_get_Item_m2156685124(__this, ___index0, method) ((  Il2CppObject * (*) (FixedArray3_1_t526322725 *, int32_t, const MethodInfo*))FixedArray3_1_get_Item_m2156685124_gshared)(__this, ___index0, method)
// System.Void Pathfinding.Poly2Tri.FixedArray3`1<System.Object>::set_Item(System.Int32,T)
extern "C"  void FixedArray3_1_set_Item_m851048943_gshared (FixedArray3_1_t526322725 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define FixedArray3_1_set_Item_m851048943(__this, ___index0, ___value1, method) ((  void (*) (FixedArray3_1_t526322725 *, int32_t, Il2CppObject *, const MethodInfo*))FixedArray3_1_set_Item_m851048943_gshared)(__this, ___index0, ___value1, method)
// System.Boolean Pathfinding.Poly2Tri.FixedArray3`1<System.Object>::Contains(T)
extern "C"  bool FixedArray3_1_Contains_m872077375_gshared (FixedArray3_1_t526322725 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define FixedArray3_1_Contains_m872077375(__this, ___value0, method) ((  bool (*) (FixedArray3_1_t526322725 *, Il2CppObject *, const MethodInfo*))FixedArray3_1_Contains_m872077375_gshared)(__this, ___value0, method)
// System.Int32 Pathfinding.Poly2Tri.FixedArray3`1<System.Object>::IndexOf(T)
extern "C"  int32_t FixedArray3_1_IndexOf_m548310489_gshared (FixedArray3_1_t526322725 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define FixedArray3_1_IndexOf_m548310489(__this, ___value0, method) ((  int32_t (*) (FixedArray3_1_t526322725 *, Il2CppObject *, const MethodInfo*))FixedArray3_1_IndexOf_m548310489_gshared)(__this, ___value0, method)
// System.Void Pathfinding.Poly2Tri.FixedArray3`1<System.Object>::Clear()
extern "C"  void FixedArray3_1_Clear_m1266883641_gshared (FixedArray3_1_t526322725 * __this, const MethodInfo* method);
#define FixedArray3_1_Clear_m1266883641(__this, method) ((  void (*) (FixedArray3_1_t526322725 *, const MethodInfo*))FixedArray3_1_Clear_m1266883641_gshared)(__this, method)
// System.Collections.Generic.IEnumerable`1<T> Pathfinding.Poly2Tri.FixedArray3`1<System.Object>::Enumerate()
extern "C"  Il2CppObject* FixedArray3_1_Enumerate_m2474609968_gshared (FixedArray3_1_t526322725 * __this, const MethodInfo* method);
#define FixedArray3_1_Enumerate_m2474609968(__this, method) ((  Il2CppObject* (*) (FixedArray3_1_t526322725 *, const MethodInfo*))FixedArray3_1_Enumerate_m2474609968_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<T> Pathfinding.Poly2Tri.FixedArray3`1<System.Object>::GetEnumerator()
extern "C"  Il2CppObject* FixedArray3_1_GetEnumerator_m983331714_gshared (FixedArray3_1_t526322725 * __this, const MethodInfo* method);
#define FixedArray3_1_GetEnumerator_m983331714(__this, method) ((  Il2CppObject* (*) (FixedArray3_1_t526322725 *, const MethodInfo*))FixedArray3_1_GetEnumerator_m983331714_gshared)(__this, method)
