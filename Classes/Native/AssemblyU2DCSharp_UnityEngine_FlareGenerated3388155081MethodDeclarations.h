﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_FlareGenerated
struct UnityEngine_FlareGenerated_t3388155081;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_FlareGenerated::.ctor()
extern "C"  void UnityEngine_FlareGenerated__ctor_m3231159794 (UnityEngine_FlareGenerated_t3388155081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_FlareGenerated::Flare_Flare1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_FlareGenerated_Flare_Flare1_m617391658 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_FlareGenerated::__Register()
extern "C"  void UnityEngine_FlareGenerated___Register_m1249103381 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_FlareGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_FlareGenerated_ilo_addJSCSRel1_m291937198 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
