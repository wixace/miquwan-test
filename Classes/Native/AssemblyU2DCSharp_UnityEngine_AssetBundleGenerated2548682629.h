﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MethodID
struct MethodID_t3916401116;
// JSDataExchangeMgr/DGetV`1<System.Byte[]>
struct DGetV_1_t4138411810;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine_AssetBundleGenerated
struct  UnityEngine_AssetBundleGenerated_t2548682629  : public Il2CppObject
{
public:

public:
};

struct UnityEngine_AssetBundleGenerated_t2548682629_StaticFields
{
public:
	// MethodID UnityEngine_AssetBundleGenerated::methodID4
	MethodID_t3916401116 * ___methodID4_0;
	// MethodID UnityEngine_AssetBundleGenerated::methodID7
	MethodID_t3916401116 * ___methodID7_1;
	// MethodID UnityEngine_AssetBundleGenerated::methodID10
	MethodID_t3916401116 * ___methodID10_2;
	// MethodID UnityEngine_AssetBundleGenerated::methodID13
	MethodID_t3916401116 * ___methodID13_3;
	// MethodID UnityEngine_AssetBundleGenerated::methodID16
	MethodID_t3916401116 * ___methodID16_4;
	// MethodID UnityEngine_AssetBundleGenerated::methodID19
	MethodID_t3916401116 * ___methodID19_5;
	// JSDataExchangeMgr/DGetV`1<System.Byte[]> UnityEngine_AssetBundleGenerated::<>f__am$cache6
	DGetV_1_t4138411810 * ___U3CU3Ef__amU24cache6_6;
	// JSDataExchangeMgr/DGetV`1<System.Byte[]> UnityEngine_AssetBundleGenerated::<>f__am$cache7
	DGetV_1_t4138411810 * ___U3CU3Ef__amU24cache7_7;
	// JSDataExchangeMgr/DGetV`1<System.Byte[]> UnityEngine_AssetBundleGenerated::<>f__am$cache8
	DGetV_1_t4138411810 * ___U3CU3Ef__amU24cache8_8;
	// JSDataExchangeMgr/DGetV`1<System.Byte[]> UnityEngine_AssetBundleGenerated::<>f__am$cache9
	DGetV_1_t4138411810 * ___U3CU3Ef__amU24cache9_9;

public:
	inline static int32_t get_offset_of_methodID4_0() { return static_cast<int32_t>(offsetof(UnityEngine_AssetBundleGenerated_t2548682629_StaticFields, ___methodID4_0)); }
	inline MethodID_t3916401116 * get_methodID4_0() const { return ___methodID4_0; }
	inline MethodID_t3916401116 ** get_address_of_methodID4_0() { return &___methodID4_0; }
	inline void set_methodID4_0(MethodID_t3916401116 * value)
	{
		___methodID4_0 = value;
		Il2CppCodeGenWriteBarrier(&___methodID4_0, value);
	}

	inline static int32_t get_offset_of_methodID7_1() { return static_cast<int32_t>(offsetof(UnityEngine_AssetBundleGenerated_t2548682629_StaticFields, ___methodID7_1)); }
	inline MethodID_t3916401116 * get_methodID7_1() const { return ___methodID7_1; }
	inline MethodID_t3916401116 ** get_address_of_methodID7_1() { return &___methodID7_1; }
	inline void set_methodID7_1(MethodID_t3916401116 * value)
	{
		___methodID7_1 = value;
		Il2CppCodeGenWriteBarrier(&___methodID7_1, value);
	}

	inline static int32_t get_offset_of_methodID10_2() { return static_cast<int32_t>(offsetof(UnityEngine_AssetBundleGenerated_t2548682629_StaticFields, ___methodID10_2)); }
	inline MethodID_t3916401116 * get_methodID10_2() const { return ___methodID10_2; }
	inline MethodID_t3916401116 ** get_address_of_methodID10_2() { return &___methodID10_2; }
	inline void set_methodID10_2(MethodID_t3916401116 * value)
	{
		___methodID10_2 = value;
		Il2CppCodeGenWriteBarrier(&___methodID10_2, value);
	}

	inline static int32_t get_offset_of_methodID13_3() { return static_cast<int32_t>(offsetof(UnityEngine_AssetBundleGenerated_t2548682629_StaticFields, ___methodID13_3)); }
	inline MethodID_t3916401116 * get_methodID13_3() const { return ___methodID13_3; }
	inline MethodID_t3916401116 ** get_address_of_methodID13_3() { return &___methodID13_3; }
	inline void set_methodID13_3(MethodID_t3916401116 * value)
	{
		___methodID13_3 = value;
		Il2CppCodeGenWriteBarrier(&___methodID13_3, value);
	}

	inline static int32_t get_offset_of_methodID16_4() { return static_cast<int32_t>(offsetof(UnityEngine_AssetBundleGenerated_t2548682629_StaticFields, ___methodID16_4)); }
	inline MethodID_t3916401116 * get_methodID16_4() const { return ___methodID16_4; }
	inline MethodID_t3916401116 ** get_address_of_methodID16_4() { return &___methodID16_4; }
	inline void set_methodID16_4(MethodID_t3916401116 * value)
	{
		___methodID16_4 = value;
		Il2CppCodeGenWriteBarrier(&___methodID16_4, value);
	}

	inline static int32_t get_offset_of_methodID19_5() { return static_cast<int32_t>(offsetof(UnityEngine_AssetBundleGenerated_t2548682629_StaticFields, ___methodID19_5)); }
	inline MethodID_t3916401116 * get_methodID19_5() const { return ___methodID19_5; }
	inline MethodID_t3916401116 ** get_address_of_methodID19_5() { return &___methodID19_5; }
	inline void set_methodID19_5(MethodID_t3916401116 * value)
	{
		___methodID19_5 = value;
		Il2CppCodeGenWriteBarrier(&___methodID19_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(UnityEngine_AssetBundleGenerated_t2548682629_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline DGetV_1_t4138411810 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline DGetV_1_t4138411810 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(DGetV_1_t4138411810 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(UnityEngine_AssetBundleGenerated_t2548682629_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline DGetV_1_t4138411810 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline DGetV_1_t4138411810 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(DGetV_1_t4138411810 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_8() { return static_cast<int32_t>(offsetof(UnityEngine_AssetBundleGenerated_t2548682629_StaticFields, ___U3CU3Ef__amU24cache8_8)); }
	inline DGetV_1_t4138411810 * get_U3CU3Ef__amU24cache8_8() const { return ___U3CU3Ef__amU24cache8_8; }
	inline DGetV_1_t4138411810 ** get_address_of_U3CU3Ef__amU24cache8_8() { return &___U3CU3Ef__amU24cache8_8; }
	inline void set_U3CU3Ef__amU24cache8_8(DGetV_1_t4138411810 * value)
	{
		___U3CU3Ef__amU24cache8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_9() { return static_cast<int32_t>(offsetof(UnityEngine_AssetBundleGenerated_t2548682629_StaticFields, ___U3CU3Ef__amU24cache9_9)); }
	inline DGetV_1_t4138411810 * get_U3CU3Ef__amU24cache9_9() const { return ___U3CU3Ef__amU24cache9_9; }
	inline DGetV_1_t4138411810 ** get_address_of_U3CU3Ef__amU24cache9_9() { return &___U3CU3Ef__amU24cache9_9; }
	inline void set_U3CU3Ef__amU24cache9_9(DGetV_1_t4138411810 * value)
	{
		___U3CU3Ef__amU24cache9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
