﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_jasuse121
struct M_jasuse121_t2879802277;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_jasuse1212879802277.h"

// System.Void GarbageiOS.M_jasuse121::.ctor()
extern "C"  void M_jasuse121__ctor_m991502174 (M_jasuse121_t2879802277 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jasuse121::M_vichaijoDeebairnall0(System.String[],System.Int32)
extern "C"  void M_jasuse121_M_vichaijoDeebairnall0_m831266309 (M_jasuse121_t2879802277 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jasuse121::M_masjener1(System.String[],System.Int32)
extern "C"  void M_jasuse121_M_masjener1_m648767345 (M_jasuse121_t2879802277 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jasuse121::M_kedo2(System.String[],System.Int32)
extern "C"  void M_jasuse121_M_kedo2_m4116852942 (M_jasuse121_t2879802277 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jasuse121::M_bocir3(System.String[],System.Int32)
extern "C"  void M_jasuse121_M_bocir3_m2269675043 (M_jasuse121_t2879802277 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jasuse121::M_heyairje4(System.String[],System.Int32)
extern "C"  void M_jasuse121_M_heyairje4_m2013713740 (M_jasuse121_t2879802277 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jasuse121::ilo_M_bocir31(GarbageiOS.M_jasuse121,System.String[],System.Int32)
extern "C"  void M_jasuse121_ilo_M_bocir31_m1266253254 (Il2CppObject * __this /* static, unused */, M_jasuse121_t2879802277 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
