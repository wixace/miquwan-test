﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_GuidGenerated
struct System_GuidGenerated_t3982104310;
// JSVCall
struct JSVCall_t3708497963;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_String7231557.h"

// System.Void System_GuidGenerated::.ctor()
extern "C"  void System_GuidGenerated__ctor_m2639902821 (System_GuidGenerated_t3982104310 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_GuidGenerated::.cctor()
extern "C"  void System_GuidGenerated__cctor_m4045479912 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_GuidGenerated::Guid_Guid1(JSVCall,System.Int32)
extern "C"  bool System_GuidGenerated_Guid_Guid1_m98045557 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_GuidGenerated::Guid_Guid2(JSVCall,System.Int32)
extern "C"  bool System_GuidGenerated_Guid_Guid2_m1342810038 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_GuidGenerated::Guid_Guid3(JSVCall,System.Int32)
extern "C"  bool System_GuidGenerated_Guid_Guid3_m2587574519 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_GuidGenerated::Guid_Guid4(JSVCall,System.Int32)
extern "C"  bool System_GuidGenerated_Guid_Guid4_m3832339000 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_GuidGenerated::Guid_Guid5(JSVCall,System.Int32)
extern "C"  bool System_GuidGenerated_Guid_Guid5_m782136185 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_GuidGenerated::Guid_Guid6(JSVCall,System.Int32)
extern "C"  bool System_GuidGenerated_Guid_Guid6_m2026900666 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_GuidGenerated::Guid_Empty(JSVCall)
extern "C"  void System_GuidGenerated_Guid_Empty_m3863762161 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_GuidGenerated::Guid_CompareTo__Guid(JSVCall,System.Int32)
extern "C"  bool System_GuidGenerated_Guid_CompareTo__Guid_m1740370614 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_GuidGenerated::Guid_CompareTo__Object(JSVCall,System.Int32)
extern "C"  bool System_GuidGenerated_Guid_CompareTo__Object_m1353542636 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_GuidGenerated::Guid_Equals__Guid(JSVCall,System.Int32)
extern "C"  bool System_GuidGenerated_Guid_Equals__Guid_m3179025917 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_GuidGenerated::Guid_Equals__Object(JSVCall,System.Int32)
extern "C"  bool System_GuidGenerated_Guid_Equals__Object_m921819507 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_GuidGenerated::Guid_GetHashCode(JSVCall,System.Int32)
extern "C"  bool System_GuidGenerated_Guid_GetHashCode_m1204071294 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_GuidGenerated::Guid_ToByteArray(JSVCall,System.Int32)
extern "C"  bool System_GuidGenerated_Guid_ToByteArray_m4004112931 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_GuidGenerated::Guid_ToString__String__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_GuidGenerated_Guid_ToString__String__IFormatProvider_m1271318433 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_GuidGenerated::Guid_ToString__String(JSVCall,System.Int32)
extern "C"  bool System_GuidGenerated_Guid_ToString__String_m136497586 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_GuidGenerated::Guid_ToString(JSVCall,System.Int32)
extern "C"  bool System_GuidGenerated_Guid_ToString_m3479305313 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_GuidGenerated::Guid_NewGuid(JSVCall,System.Int32)
extern "C"  bool System_GuidGenerated_Guid_NewGuid_m1868003894 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_GuidGenerated::Guid_op_Equality__Guid__Guid(JSVCall,System.Int32)
extern "C"  bool System_GuidGenerated_Guid_op_Equality__Guid__Guid_m3477876567 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_GuidGenerated::Guid_op_Inequality__Guid__Guid(JSVCall,System.Int32)
extern "C"  bool System_GuidGenerated_Guid_op_Inequality__Guid__Guid_m4161480636 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_GuidGenerated::__Register()
extern "C"  void System_GuidGenerated___Register_m3757325186 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_GuidGenerated::<Guid_Guid2>m__C1()
extern "C"  ByteU5BU5D_t4260760469* System_GuidGenerated_U3CGuid_Guid2U3Em__C1_m157266981 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_GuidGenerated::<Guid_Guid4>m__C2()
extern "C"  ByteU5BU5D_t4260760469* System_GuidGenerated_U3CGuid_Guid4U3Em__C2_m837326312 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_GuidGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t System_GuidGenerated_ilo_getObject1_m1760268493 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_GuidGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool System_GuidGenerated_ilo_attachFinalizerObject2_m2286768219 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System_GuidGenerated::ilo_getInt163(System.Int32)
extern "C"  int16_t System_GuidGenerated_ilo_getInt163_m3796926830 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_GuidGenerated::ilo_addJSCSRel4(System.Int32,System.Object)
extern "C"  void System_GuidGenerated_ilo_addJSCSRel4_m3482545700 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System_GuidGenerated::ilo_getByte5(System.Int32)
extern "C"  uint8_t System_GuidGenerated_ilo_getByte5_m1531064358 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System_GuidGenerated::ilo_getUInt326(System.Int32)
extern "C"  uint32_t System_GuidGenerated_ilo_getUInt326_m4004107717 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System_GuidGenerated::ilo_getUInt167(System.Int32)
extern "C"  uint16_t System_GuidGenerated_ilo_getUInt167_m3395682810 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_GuidGenerated::ilo_getObject8(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_GuidGenerated_ilo_getObject8_m1118975857 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_GuidGenerated::ilo_changeJSObj9(System.Int32,System.Object)
extern "C"  void System_GuidGenerated_ilo_changeJSObj9_m2533055532 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_GuidGenerated::ilo_setInt3210(System.Int32,System.Int32)
extern "C"  void System_GuidGenerated_ilo_setInt3210_m2190344649 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_GuidGenerated::ilo_setStringS11(System.Int32,System.String)
extern "C"  void System_GuidGenerated_ilo_setStringS11_m2915747615 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System_GuidGenerated::ilo_getStringS12(System.Int32)
extern "C"  String_t* System_GuidGenerated_ilo_getStringS12_m1817877395 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_GuidGenerated::ilo_getElement13(System.Int32,System.Int32)
extern "C"  int32_t System_GuidGenerated_ilo_getElement13_m328588958 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
