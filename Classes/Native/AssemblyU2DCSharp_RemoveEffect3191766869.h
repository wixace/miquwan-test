﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RemoveEffect
struct  RemoveEffect_t3191766869  : public ZEvent_t3638018500
{
public:
	// System.Int32 RemoveEffect::id
	int32_t ___id_6;
	// System.Int32 RemoveEffect::instanceID
	int32_t ___instanceID_7;

public:
	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(RemoveEffect_t3191766869, ___id_6)); }
	inline int32_t get_id_6() const { return ___id_6; }
	inline int32_t* get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(int32_t value)
	{
		___id_6 = value;
	}

	inline static int32_t get_offset_of_instanceID_7() { return static_cast<int32_t>(offsetof(RemoveEffect_t3191766869, ___instanceID_7)); }
	inline int32_t get_instanceID_7() const { return ___instanceID_7; }
	inline int32_t* get_address_of_instanceID_7() { return &___instanceID_7; }
	inline void set_instanceID_7(int32_t value)
	{
		___instanceID_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
