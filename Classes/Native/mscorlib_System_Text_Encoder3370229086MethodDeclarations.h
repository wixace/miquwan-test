﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.Encoder
struct Encoder_t3370229086;
// System.Text.EncoderFallback
struct EncoderFallback_t1901620832;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t671205824;
// System.Char[]
struct CharU5BU5D_t3324145743;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Text_EncoderFallback1901620832.h"

// System.Void System.Text.Encoder::.ctor()
extern "C"  void Encoder__ctor_m1621202458 (Encoder_t3370229086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallback System.Text.Encoder::get_Fallback()
extern "C"  EncoderFallback_t1901620832 * Encoder_get_Fallback_m1277786878 (Encoder_t3370229086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Encoder::set_Fallback(System.Text.EncoderFallback)
extern "C"  void Encoder_set_Fallback_m4149313261 (Encoder_t3370229086 * __this, EncoderFallback_t1901620832 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.EncoderFallbackBuffer System.Text.Encoder::get_FallbackBuffer()
extern "C"  EncoderFallbackBuffer_t671205824 * Encoder_get_FallbackBuffer_m721809406 (Encoder_t3370229086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Encoder::Reset()
extern "C"  void Encoder_Reset_m3562602695 (Encoder_t3370229086 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.Encoder::Convert(System.Char[],System.Int32,System.Int32,System.Byte[],System.Int32,System.Int32,System.Boolean,System.Int32&,System.Int32&,System.Boolean&)
extern "C"  void Encoder_Convert_m1257062083 (Encoder_t3370229086 * __this, CharU5BU5D_t3324145743* ___chars0, int32_t ___charIndex1, int32_t ___charCount2, ByteU5BU5D_t4260760469* ___bytes3, int32_t ___byteIndex4, int32_t ___byteCount5, bool ___flush6, int32_t* ___charsUsed7, int32_t* ___bytesUsed8, bool* ___completed9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
