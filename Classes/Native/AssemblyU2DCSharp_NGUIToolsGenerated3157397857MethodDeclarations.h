﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NGUIToolsGenerated
struct NGUIToolsGenerated_t3157397857;
// JSVCall
struct JSVCall_t3708497963;
// UIWidget[]
struct UIWidgetU5BU5D_t4236988201;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type
struct Type_t;
// MethodID
struct MethodID_t3916401116;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UIPanel
struct UIPanel_t295209936;
// UnityEngine.Object
struct Object_t3071478659;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityEngine.AudioSource
struct AudioSource_t1740077639;
// UnityEngine.AudioClip
struct AudioClip_t794140988;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_MethodID3916401116.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_AudioClip794140988.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"

// System.Void NGUIToolsGenerated::.ctor()
extern "C"  void NGUIToolsGenerated__ctor_m613084250 (NGUIToolsGenerated_t3157397857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::.cctor()
extern "C"  void NGUIToolsGenerated__cctor_m1343646355 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::NGUITools_soundVolume(JSVCall)
extern "C"  void NGUIToolsGenerated_NGUITools_soundVolume_m3742014455 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::NGUITools_fileAccess(JSVCall)
extern "C"  void NGUIToolsGenerated_NGUITools_fileAccess_m3494192876 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::NGUITools_clipboard(JSVCall)
extern "C"  void NGUIToolsGenerated_NGUITools_clipboard_m1621422218 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::NGUITools_screenSize(JSVCall)
extern "C"  void NGUIToolsGenerated_NGUITools_screenSize_m3467739391 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_AddChild__GameObject__Boolean(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_AddChild__GameObject__Boolean_m625879679 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_AddChild__GameObject__GameObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_AddChild__GameObject__GameObject_m1444041500 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_AddChildT1__GameObject__Boolean(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_AddChildT1__GameObject__Boolean_m694171202 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_AddChild__GameObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_AddChild__GameObject_m659877643 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_AddChildT1__GameObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_AddChildT1__GameObject_m1233718248 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_AddMissingComponentT1__GameObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_AddMissingComponentT1__GameObject_m1656786505 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_AddSprite__GameObject__UIAtlas__String(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_AddSprite__GameObject__UIAtlas__String_m3371675728 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_AddWidgetT1__GameObject__Int32(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_AddWidgetT1__GameObject__Int32_m798695162 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_AddWidgetT1__GameObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_AddWidgetT1__GameObject_m1349546902 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_AddWidgetCollider__GameObject__Boolean(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_AddWidgetCollider__GameObject__Boolean_m973818237 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_AddWidgetCollider__GameObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_AddWidgetCollider__GameObject_m286218829 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_AdjustDepth__GameObject__Int32(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_AdjustDepth__GameObject__Int32_m1025771048 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_ApplyPMA__Color(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_ApplyPMA__Color_m67442384 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_BringForward__GameObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_BringForward__GameObject_m175989219 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_Broadcast__String__Object(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_Broadcast__String__Object_m1219103604 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_Broadcast__String(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_Broadcast__String_m1404634485 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_CalculateNextDepth__GameObject__Boolean(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_CalculateNextDepth__GameObject__Boolean_m389421776 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_CalculateNextDepth__GameObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_CalculateNextDepth__GameObject_m293285786 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_CalculateRaycastDepth__GameObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_CalculateRaycastDepth__GameObject_m250997396 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_CreateUI__Transform__Boolean__Int32(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_CreateUI__Transform__Boolean__Int32_m4211911525 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_CreateUI__Boolean__Int32(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_CreateUI__Boolean__Int32_m3613378613 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_CreateUI__Boolean(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_CreateUI__Boolean_m4102022843 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_Destroy__UEObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_Destroy__UEObject_m2678195116 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_DestroyImmediate__UEObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_DestroyImmediate__UEObject_m2010454181 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_ExecuteT1__GameObject__String(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_ExecuteT1__GameObject__String_m2667881879 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_ExecuteAllT1__GameObject__String(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_ExecuteAllT1__GameObject__String_m3470555690 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_FindActiveT1(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_FindActiveT1_m1816404859 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_FindCameraForLayer__Int32(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_FindCameraForLayer__Int32_m1116265611 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_FindInParentsT1__GameObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_FindInParentsT1__GameObject_m2722380732 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_FindInParentsT1__Transform(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_FindInParentsT1__Transform_m1424189603 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_GetActive__GameObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_GetActive__GameObject_m2331991088 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_GetActive__Behaviour(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_GetActive__Behaviour_m1723028544 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_GetFuncName__Object__String(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_GetFuncName__Object__String_m4291881496 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_GetHierarchy__GameObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_GetHierarchy__GameObject_m278299023 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_GetRoot__GameObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_GetRoot__GameObject_m1133963532 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_GetSides__Camera__Single__Transform(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_GetSides__Camera__Single__Transform_m2286252220 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_GetSides__Camera__Single(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_GetSides__Camera__Single_m723047730 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_GetSides__Camera__Transform(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_GetSides__Camera__Transform_m2169590532 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_GetSides__Camera(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_GetSides__Camera_m832802794 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_GetTypeName__UEObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_GetTypeName__UEObject_m1517475085 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_GetTypeNameT1(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_GetTypeNameT1_m1660224539 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_GetWorldCorners__Camera__Single__Transform(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_GetWorldCorners__Camera__Single__Transform_m178579932 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_GetWorldCorners__Camera__Transform(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_GetWorldCorners__Camera__Transform_m2932599844 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_GetWorldCorners__Camera__Single(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_GetWorldCorners__Camera__Single_m2779369490 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_GetWorldCorners__Camera(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_GetWorldCorners__Camera_m3805113034 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_ImmediatelyCreateDrawCalls__GameObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_ImmediatelyCreateDrawCalls__GameObject_m3545346055 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_IsChild__Transform__Transform(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_IsChild__Transform__Transform_m2412924853 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_Load__String(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_Load__String_m102691702 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_MakePixelPerfect__Transform(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_MakePixelPerfect__Transform_m4241112570 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_MarkParentAsChanged__GameObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_MarkParentAsChanged__GameObject_m60902911 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_NormalizeDepths(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_NormalizeDepths_m3677131616 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_NormalizePanelDepths(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_NormalizePanelDepths_m706077286 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_NormalizeWidgetDepths__GameObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_NormalizeWidgetDepths__GameObject_m743784213 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_NormalizeWidgetDepths__UIWidget_Array(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_NormalizeWidgetDepths__UIWidget_Array_m2814994966 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_NormalizeWidgetDepths(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_NormalizeWidgetDepths_m1063733380 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_PlaySound__AudioClip__Single__Single(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_PlaySound__AudioClip__Single__Single_m698995002 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_PlaySound__AudioClip__Single(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_PlaySound__AudioClip__Single_m97850354 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_PlaySound__AudioClip(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_PlaySound__AudioClip_m2565508778 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_PushBack__GameObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_PushBack__GameObject_m3204261809 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_RandomRange__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_RandomRange__Int32__Int32_m3915258525 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_RegisterUndo__UEObject__String(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_RegisterUndo__UEObject__String_m4047960646 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_Round__Vector3(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_Round__Vector3_m3798740033 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_Save__String__Byte_Array(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_Save__String__Byte_Array_m3460646543 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_SetActive__GameObject__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_SetActive__GameObject__Boolean__Boolean_m4256916860 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_SetActive__GameObject__Boolean(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_SetActive__GameObject__Boolean_m4208128878 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_SetActiveChildren__GameObject__Boolean(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_SetActiveChildren__GameObject__Boolean_m4055772655 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_SetActiveSelf__GameObject__Boolean(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_SetActiveSelf__GameObject__Boolean_m83476866 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_SetChildLayer__Transform__Int32(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_SetChildLayer__Transform__Int32_m4063420508 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_SetDirty__UEObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_SetDirty__UEObject_m785897054 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_SetLayer__GameObject__Int32(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_SetLayer__GameObject__Int32_m1612562257 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_UpdateWidgetCollider__GameObject__Boolean(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_UpdateWidgetCollider__GameObject__Boolean_m4241395001 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_UpdateWidgetCollider__BoxCollider2D__Boolean(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_UpdateWidgetCollider__BoxCollider2D__Boolean_m1636392983 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_UpdateWidgetCollider__BoxCollider__Boolean(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_UpdateWidgetCollider__BoxCollider__Boolean_m544820393 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::NGUITools_UpdateWidgetCollider__GameObject(JSVCall,System.Int32)
extern "C"  bool NGUIToolsGenerated_NGUITools_UpdateWidgetCollider__GameObject_m3265901329 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::__Register()
extern "C"  void NGUIToolsGenerated___Register_m3634247853 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget[] NGUIToolsGenerated::<NGUITools_NormalizeWidgetDepths__UIWidget_Array>m__88()
extern "C"  UIWidgetU5BU5D_t4236988201* NGUIToolsGenerated_U3CNGUITools_NormalizeWidgetDepths__UIWidget_ArrayU3Em__88_m4205278818 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] NGUIToolsGenerated::<NGUITools_Save__String__Byte_Array>m__89()
extern "C"  ByteU5BU5D_t4260760469* NGUIToolsGenerated_U3CNGUITools_Save__String__Byte_ArrayU3Em__89_m2808522939 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NGUIToolsGenerated::ilo_getSingle1(System.Int32)
extern "C"  float NGUIToolsGenerated_ilo_getSingle1_m2165454349 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_set_soundVolume2(System.Single)
extern "C"  void NGUIToolsGenerated_ilo_set_soundVolume2_m1025402888 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void NGUIToolsGenerated_ilo_setBooleanS3_m1291424393 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String NGUIToolsGenerated::ilo_get_clipboard4()
extern "C"  String_t* NGUIToolsGenerated_ilo_get_clipboard4_m3627474247 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String NGUIToolsGenerated::ilo_getStringS5(System.Int32)
extern "C"  String_t* NGUIToolsGenerated_ilo_getStringS5_m4000058204 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 NGUIToolsGenerated::ilo_get_screenSize6()
extern "C"  Vector2_t4282066565  NGUIToolsGenerated_ilo_get_screenSize6_m2632996970 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_setVector2S7(System.Int32,UnityEngine.Vector2)
extern "C"  void NGUIToolsGenerated_ilo_setVector2S7_m2699509907 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object NGUIToolsGenerated::ilo_getObject8(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * NGUIToolsGenerated_ilo_getObject8_m2578330396 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NGUIToolsGenerated::ilo_setObject9(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t NGUIToolsGenerated_ilo_setObject9_m3160543480 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIToolsGenerated::ilo_getBooleanS10(System.Int32)
extern "C"  bool NGUIToolsGenerated_ilo_getBooleanS10_m2489642914 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo NGUIToolsGenerated::ilo_makeGenericMethod11(System.Type,MethodID,System.Int32)
extern "C"  MethodInfo_t * NGUIToolsGenerated_ilo_makeGenericMethod11_m505417793 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MethodID_t3916401116 * ___methodID1, int32_t ___TCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NGUIToolsGenerated::ilo_getInt3212(System.Int32)
extern "C"  int32_t NGUIToolsGenerated_ilo_getInt3212_m4194562325 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_AddWidgetCollider13(UnityEngine.GameObject,System.Boolean)
extern "C"  void NGUIToolsGenerated_ilo_AddWidgetCollider13_m3732411685 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, bool ___considerInactive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object NGUIToolsGenerated::ilo_getWhatever14(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * NGUIToolsGenerated_ilo_getWhatever14_m510944626 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_Broadcast15(System.String,System.Object)
extern "C"  void NGUIToolsGenerated_ilo_Broadcast15_m2053930310 (Il2CppObject * __this /* static, unused */, String_t* ___funcName0, Il2CppObject * ___param1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_Broadcast16(System.String)
extern "C"  void NGUIToolsGenerated_ilo_Broadcast16_m1429029463 (Il2CppObject * __this /* static, unused */, String_t* ___funcName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_setInt3217(System.Int32,System.Int32)
extern "C"  void NGUIToolsGenerated_ilo_setInt3217_m785984535 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NGUIToolsGenerated::ilo_CalculateRaycastDepth18(UnityEngine.GameObject)
extern "C"  int32_t NGUIToolsGenerated_ilo_CalculateRaycastDepth18_m2385540310 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIPanel NGUIToolsGenerated::ilo_CreateUI19(System.Boolean)
extern "C"  UIPanel_t295209936 * NGUIToolsGenerated_ilo_CreateUI19_m1153674767 (Il2CppObject * __this /* static, unused */, bool ___advanced3D0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_Destroy20(UnityEngine.Object)
extern "C"  void NGUIToolsGenerated_ilo_Destroy20_m4153006759 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_moveSaveID2Arr21(System.Int32)
extern "C"  void NGUIToolsGenerated_ilo_moveSaveID2Arr21_m1300503461 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String NGUIToolsGenerated::ilo_GetFuncName22(System.Object,System.String)
extern "C"  String_t* NGUIToolsGenerated_ilo_GetFuncName22_m1433125097 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, String_t* ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_setStringS23(System.Int32,System.String)
extern "C"  void NGUIToolsGenerated_ilo_setStringS23_m1841941739 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_setArrayS24(System.Int32,System.Int32,System.Boolean)
extern "C"  void NGUIToolsGenerated_ilo_setArrayS24_m2927044090 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] NGUIToolsGenerated::ilo_GetSides25(UnityEngine.Camera,System.Single)
extern "C"  Vector3U5BU5D_t215400611* NGUIToolsGenerated_ilo_GetSides25_m268879175 (Il2CppObject * __this /* static, unused */, Camera_t2727095145 * ___cam0, float ___depth1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_setVector3S26(System.Int32,UnityEngine.Vector3)
extern "C"  void NGUIToolsGenerated_ilo_setVector3S26_m536785202 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] NGUIToolsGenerated::ilo_GetSides27(UnityEngine.Camera)
extern "C"  Vector3U5BU5D_t215400611* NGUIToolsGenerated_ilo_GetSides27_m3718060260 (Il2CppObject * __this /* static, unused */, Camera_t2727095145 * ___cam0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_setStringS28(System.Int32,System.Object)
extern "C"  void NGUIToolsGenerated_ilo_setStringS28_m1234593282 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] NGUIToolsGenerated::ilo_GetWorldCorners29(UnityEngine.Camera,System.Single)
extern "C"  Vector3U5BU5D_t215400611* NGUIToolsGenerated_ilo_GetWorldCorners29_m1430650079 (Il2CppObject * __this /* static, unused */, Camera_t2727095145 * ___cam0, float ___depth1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_MarkParentAsChanged30(UnityEngine.GameObject)
extern "C"  void NGUIToolsGenerated_ilo_MarkParentAsChanged30_m1833294789 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_NormalizePanelDepths31()
extern "C"  void NGUIToolsGenerated_ilo_NormalizePanelDepths31_m3113392482 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_NormalizeWidgetDepths32(UIWidget[])
extern "C"  void NGUIToolsGenerated_ilo_NormalizeWidgetDepths32_m4228250507 (Il2CppObject * __this /* static, unused */, UIWidgetU5BU5D_t4236988201* ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioSource NGUIToolsGenerated::ilo_PlaySound33(UnityEngine.AudioClip,System.Single,System.Single)
extern "C"  AudioSource_t1740077639 * NGUIToolsGenerated_ilo_PlaySound33_m3353097030 (Il2CppObject * __this /* static, unused */, AudioClip_t794140988 * ___clip0, float ___volume1, float ___pitch2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_PushBack34(UnityEngine.GameObject)
extern "C"  void NGUIToolsGenerated_ilo_PushBack34_m876149527 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_RegisterUndo35(UnityEngine.Object,System.String)
extern "C"  void NGUIToolsGenerated_ilo_RegisterUndo35_m1560073516 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_SetActive36(UnityEngine.GameObject,System.Boolean,System.Boolean)
extern "C"  void NGUIToolsGenerated_ilo_SetActive36_m2056195304 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, bool ___state1, bool ___compatibilityMode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_SetActiveChildren37(UnityEngine.GameObject,System.Boolean)
extern "C"  void NGUIToolsGenerated_ilo_SetActiveChildren37_m1607829397 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_SetChildLayer38(UnityEngine.Transform,System.Int32)
extern "C"  void NGUIToolsGenerated_ilo_SetChildLayer38_m2277181723 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___t0, int32_t ___layer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_SetDirty39(UnityEngine.Object)
extern "C"  void NGUIToolsGenerated_ilo_SetDirty39_m2717754013 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_SetLayer40(UnityEngine.GameObject,System.Int32)
extern "C"  void NGUIToolsGenerated_ilo_SetLayer40_m1517537591 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, int32_t ___layer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_UpdateWidgetCollider41(UnityEngine.GameObject,System.Boolean)
extern "C"  void NGUIToolsGenerated_ilo_UpdateWidgetCollider41_m1191411082 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, bool ___considerInactive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIToolsGenerated::ilo_UpdateWidgetCollider42(UnityEngine.GameObject)
extern "C"  void NGUIToolsGenerated_ilo_UpdateWidgetCollider42_m1143073428 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NGUIToolsGenerated::ilo_getObject43(System.Int32)
extern "C"  int32_t NGUIToolsGenerated_ilo_getObject43_m4259018620 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NGUIToolsGenerated::ilo_getArrayLength44(System.Int32)
extern "C"  int32_t NGUIToolsGenerated_ilo_getArrayLength44_m3909165637 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte NGUIToolsGenerated::ilo_getByte45(System.Int32)
extern "C"  uint8_t NGUIToolsGenerated_ilo_getByte45_m2926449161 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
