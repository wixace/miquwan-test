﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RadiusModifier
struct RadiusModifier_t3948604841;
// Pathfinding.Path
struct Path_t1974241691;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_ModifierData4046421655.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_RadiusModifier_TangentType1819309977.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_RadiusModifier3948604841.h"

// System.Void RadiusModifier::.ctor()
extern "C"  void RadiusModifier__ctor_m3794873618 (RadiusModifier_t3948604841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ModifierData RadiusModifier::get_input()
extern "C"  int32_t RadiusModifier_get_input_m1781545503 (RadiusModifier_t3948604841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ModifierData RadiusModifier::get_output()
extern "C"  int32_t RadiusModifier_get_output_m3287823470 (RadiusModifier_t3948604841 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RadiusModifier::CalculateCircleInner(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Single&,System.Single&)
extern "C"  bool RadiusModifier_CalculateCircleInner_m3542132112 (RadiusModifier_t3948604841 * __this, Vector3_t4282066566  ___p10, Vector3_t4282066566  ___p21, float ___r12, float ___r23, float* ___a4, float* ___sigma5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RadiusModifier::CalculateCircleOuter(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Single&,System.Single&)
extern "C"  bool RadiusModifier_CalculateCircleOuter_m1435459915 (RadiusModifier_t3948604841 * __this, Vector3_t4282066566  ___p10, Vector3_t4282066566  ___p21, float ___r12, float ___r23, float* ___a4, float* ___sigma5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// RadiusModifier/TangentType RadiusModifier::CalculateTangentType(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  int32_t RadiusModifier_CalculateTangentType_m3583503139 (RadiusModifier_t3948604841 * __this, Vector3_t4282066566  ___p10, Vector3_t4282066566  ___p21, Vector3_t4282066566  ___p32, Vector3_t4282066566  ___p43, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// RadiusModifier/TangentType RadiusModifier::CalculateTangentTypeSimple(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  int32_t RadiusModifier_CalculateTangentTypeSimple_m2045096830 (RadiusModifier_t3948604841 * __this, Vector3_t4282066566  ___p10, Vector3_t4282066566  ___p21, Vector3_t4282066566  ___p32, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadiusModifier::DrawCircle(UnityEngine.Vector3,System.Single,UnityEngine.Color,System.Single,System.Single)
extern "C"  void RadiusModifier_DrawCircle_m3754572226 (RadiusModifier_t3948604841 * __this, Vector3_t4282066566  ___p10, float ___rad1, Color_t4194546905  ___col2, float ___start3, float ___end4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RadiusModifier::Apply(Pathfinding.Path,Pathfinding.ModifierData)
extern "C"  void RadiusModifier_Apply_m1132244684 (RadiusModifier_t3948604841 * __this, Path_t1974241691 * ___p0, int32_t ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Vector3> RadiusModifier::Apply(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  List_1_t1355284822 * RadiusModifier_Apply_m252908193 (RadiusModifier_t3948604841 * __this, List_1_t1355284822 * ___vs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<UnityEngine.Vector3> RadiusModifier::ilo_Apply1(RadiusModifier,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  List_1_t1355284822 * RadiusModifier_ilo_Apply1_m1456187850 (Il2CppObject * __this /* static, unused */, RadiusModifier_t3948604841 * ____this0, List_1_t1355284822 * ___vs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// RadiusModifier/TangentType RadiusModifier::ilo_CalculateTangentType2(RadiusModifier,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  int32_t RadiusModifier_ilo_CalculateTangentType2_m2870163925 (Il2CppObject * __this /* static, unused */, RadiusModifier_t3948604841 * ____this0, Vector3_t4282066566  ___p11, Vector3_t4282066566  ___p22, Vector3_t4282066566  ___p33, Vector3_t4282066566  ___p44, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RadiusModifier::ilo_CalculateCircleInner3(RadiusModifier,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Single&,System.Single&)
extern "C"  bool RadiusModifier_ilo_CalculateCircleInner3_m641329027 (Il2CppObject * __this /* static, unused */, RadiusModifier_t3948604841 * ____this0, Vector3_t4282066566  ___p11, Vector3_t4282066566  ___p22, float ___r13, float ___r24, float* ___a5, float* ___sigma6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean RadiusModifier::ilo_CalculateCircleOuter4(RadiusModifier,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Single&,System.Single&)
extern "C"  bool RadiusModifier_ilo_CalculateCircleOuter4_m3345244927 (Il2CppObject * __this /* static, unused */, RadiusModifier_t3948604841 * ____this0, Vector3_t4282066566  ___p11, Vector3_t4282066566  ___p22, float ___r13, float ___r24, float* ___a5, float* ___sigma6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
