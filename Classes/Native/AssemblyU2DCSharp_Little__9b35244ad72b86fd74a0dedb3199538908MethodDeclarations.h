﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._9b35244ad72b86fd74a0dedbdfc4fae6
struct _9b35244ad72b86fd74a0dedbdfc4fae6_t3199538908;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__9b35244ad72b86fd74a0dedb3199538908.h"

// System.Void Little._9b35244ad72b86fd74a0dedbdfc4fae6::.ctor()
extern "C"  void _9b35244ad72b86fd74a0dedbdfc4fae6__ctor_m4152624849 (_9b35244ad72b86fd74a0dedbdfc4fae6_t3199538908 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._9b35244ad72b86fd74a0dedbdfc4fae6::_9b35244ad72b86fd74a0dedbdfc4fae6m2(System.Int32)
extern "C"  int32_t _9b35244ad72b86fd74a0dedbdfc4fae6__9b35244ad72b86fd74a0dedbdfc4fae6m2_m4074161177 (_9b35244ad72b86fd74a0dedbdfc4fae6_t3199538908 * __this, int32_t ____9b35244ad72b86fd74a0dedbdfc4fae6a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._9b35244ad72b86fd74a0dedbdfc4fae6::_9b35244ad72b86fd74a0dedbdfc4fae6m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _9b35244ad72b86fd74a0dedbdfc4fae6__9b35244ad72b86fd74a0dedbdfc4fae6m_m2680402621 (_9b35244ad72b86fd74a0dedbdfc4fae6_t3199538908 * __this, int32_t ____9b35244ad72b86fd74a0dedbdfc4fae6a0, int32_t ____9b35244ad72b86fd74a0dedbdfc4fae6811, int32_t ____9b35244ad72b86fd74a0dedbdfc4fae6c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._9b35244ad72b86fd74a0dedbdfc4fae6::ilo__9b35244ad72b86fd74a0dedbdfc4fae6m21(Little._9b35244ad72b86fd74a0dedbdfc4fae6,System.Int32)
extern "C"  int32_t _9b35244ad72b86fd74a0dedbdfc4fae6_ilo__9b35244ad72b86fd74a0dedbdfc4fae6m21_m326315043 (Il2CppObject * __this /* static, unused */, _9b35244ad72b86fd74a0dedbdfc4fae6_t3199538908 * ____this0, int32_t ____9b35244ad72b86fd74a0dedbdfc4fae6a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
