﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.BasicList/MatchPredicate
struct MatchPredicate_t1402151099;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ProtoBuf.Meta.BasicList/MatchPredicate::.ctor(System.Object,System.IntPtr)
extern "C"  void MatchPredicate__ctor_m3900157410 (MatchPredicate_t1402151099 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.BasicList/MatchPredicate::Invoke(System.Object,System.Object)
extern "C"  bool MatchPredicate_Invoke_m2206755826 (MatchPredicate_t1402151099 * __this, Il2CppObject * ___value0, Il2CppObject * ___ctx1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ProtoBuf.Meta.BasicList/MatchPredicate::BeginInvoke(System.Object,System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * MatchPredicate_BeginInvoke_m95469483 (MatchPredicate_t1402151099 * __this, Il2CppObject * ___value0, Il2CppObject * ___ctx1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.BasicList/MatchPredicate::EndInvoke(System.IAsyncResult)
extern "C"  bool MatchPredicate_EndInvoke_m1599264934 (MatchPredicate_t1402151099 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
