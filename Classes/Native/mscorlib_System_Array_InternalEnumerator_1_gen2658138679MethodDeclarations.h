﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2658138679.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_3875796003.h"

// System.Void System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitTreeEncoder>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2901234118_gshared (InternalEnumerator_1_t2658138679 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2901234118(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2658138679 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2901234118_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitTreeEncoder>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3527479834_gshared (InternalEnumerator_1_t2658138679 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3527479834(__this, method) ((  void (*) (InternalEnumerator_1_t2658138679 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3527479834_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitTreeEncoder>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1667814790_gshared (InternalEnumerator_1_t2658138679 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1667814790(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2658138679 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1667814790_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitTreeEncoder>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3840621341_gshared (InternalEnumerator_1_t2658138679 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3840621341(__this, method) ((  void (*) (InternalEnumerator_1_t2658138679 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3840621341_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitTreeEncoder>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2086906502_gshared (InternalEnumerator_1_t2658138679 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2086906502(__this, method) ((  bool (*) (InternalEnumerator_1_t2658138679 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2086906502_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<SevenZip.Compression.RangeCoder.BitTreeEncoder>::get_Current()
extern "C"  BitTreeEncoder_t3875796003  InternalEnumerator_1_get_Current_m3365479053_gshared (InternalEnumerator_1_t2658138679 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3365479053(__this, method) ((  BitTreeEncoder_t3875796003  (*) (InternalEnumerator_1_t2658138679 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3365479053_gshared)(__this, method)
