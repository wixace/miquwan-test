﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._63f5b8bb190cd99dc046ac1cad426fcc
struct _63f5b8bb190cd99dc046ac1cad426fcc_t1509306122;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._63f5b8bb190cd99dc046ac1cad426fcc::.ctor()
extern "C"  void _63f5b8bb190cd99dc046ac1cad426fcc__ctor_m1026572899 (_63f5b8bb190cd99dc046ac1cad426fcc_t1509306122 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._63f5b8bb190cd99dc046ac1cad426fcc::_63f5b8bb190cd99dc046ac1cad426fccm2(System.Int32)
extern "C"  int32_t _63f5b8bb190cd99dc046ac1cad426fcc__63f5b8bb190cd99dc046ac1cad426fccm2_m2582734169 (_63f5b8bb190cd99dc046ac1cad426fcc_t1509306122 * __this, int32_t ____63f5b8bb190cd99dc046ac1cad426fcca0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._63f5b8bb190cd99dc046ac1cad426fcc::_63f5b8bb190cd99dc046ac1cad426fccm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _63f5b8bb190cd99dc046ac1cad426fcc__63f5b8bb190cd99dc046ac1cad426fccm_m1837431165 (_63f5b8bb190cd99dc046ac1cad426fcc_t1509306122 * __this, int32_t ____63f5b8bb190cd99dc046ac1cad426fcca0, int32_t ____63f5b8bb190cd99dc046ac1cad426fcc341, int32_t ____63f5b8bb190cd99dc046ac1cad426fccc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
