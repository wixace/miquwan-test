﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_murleNasjaldoo86
struct  M_murleNasjaldoo86_t1266022906  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_murleNasjaldoo86::_cerzalPepalsi
	float ____cerzalPepalsi_0;
	// System.Int32 GarbageiOS.M_murleNasjaldoo86::_doumou
	int32_t ____doumou_1;
	// System.Single GarbageiOS.M_murleNasjaldoo86::_ladroRedouse
	float ____ladroRedouse_2;
	// System.Int32 GarbageiOS.M_murleNasjaldoo86::_tardreaHabo
	int32_t ____tardreaHabo_3;
	// System.Boolean GarbageiOS.M_murleNasjaldoo86::_teresalllor
	bool ____teresalllor_4;
	// System.Boolean GarbageiOS.M_murleNasjaldoo86::_hirurha
	bool ____hirurha_5;
	// System.String GarbageiOS.M_murleNasjaldoo86::_berewaho
	String_t* ____berewaho_6;
	// System.String GarbageiOS.M_murleNasjaldoo86::_hallwihay
	String_t* ____hallwihay_7;
	// System.Single GarbageiOS.M_murleNasjaldoo86::_fijerToojemur
	float ____fijerToojemur_8;
	// System.UInt32 GarbageiOS.M_murleNasjaldoo86::_riguwaForhel
	uint32_t ____riguwaForhel_9;
	// System.String GarbageiOS.M_murleNasjaldoo86::_kowgecallSawfi
	String_t* ____kowgecallSawfi_10;
	// System.Single GarbageiOS.M_murleNasjaldoo86::_medrallboo
	float ____medrallboo_11;
	// System.String GarbageiOS.M_murleNasjaldoo86::_lanawstouNemai
	String_t* ____lanawstouNemai_12;
	// System.Boolean GarbageiOS.M_murleNasjaldoo86::_femou
	bool ____femou_13;
	// System.Int32 GarbageiOS.M_murleNasjaldoo86::_cairlalDiralray
	int32_t ____cairlalDiralray_14;
	// System.String GarbageiOS.M_murleNasjaldoo86::_birlurgir
	String_t* ____birlurgir_15;
	// System.String GarbageiOS.M_murleNasjaldoo86::_sepea
	String_t* ____sepea_16;
	// System.UInt32 GarbageiOS.M_murleNasjaldoo86::_cisvaiBallsar
	uint32_t ____cisvaiBallsar_17;
	// System.Int32 GarbageiOS.M_murleNasjaldoo86::_lowsas
	int32_t ____lowsas_18;
	// System.UInt32 GarbageiOS.M_murleNasjaldoo86::_sure
	uint32_t ____sure_19;
	// System.Boolean GarbageiOS.M_murleNasjaldoo86::_zerucar
	bool ____zerucar_20;
	// System.UInt32 GarbageiOS.M_murleNasjaldoo86::_lisza
	uint32_t ____lisza_21;
	// System.Int32 GarbageiOS.M_murleNasjaldoo86::_taslootrawMasatea
	int32_t ____taslootrawMasatea_22;
	// System.String GarbageiOS.M_murleNasjaldoo86::_jurchesa
	String_t* ____jurchesa_23;
	// System.String GarbageiOS.M_murleNasjaldoo86::_vuyaldis
	String_t* ____vuyaldis_24;
	// System.Int32 GarbageiOS.M_murleNasjaldoo86::_memstowDouti
	int32_t ____memstowDouti_25;
	// System.Int32 GarbageiOS.M_murleNasjaldoo86::_whikalChaywhalaw
	int32_t ____whikalChaywhalaw_26;

public:
	inline static int32_t get_offset_of__cerzalPepalsi_0() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____cerzalPepalsi_0)); }
	inline float get__cerzalPepalsi_0() const { return ____cerzalPepalsi_0; }
	inline float* get_address_of__cerzalPepalsi_0() { return &____cerzalPepalsi_0; }
	inline void set__cerzalPepalsi_0(float value)
	{
		____cerzalPepalsi_0 = value;
	}

	inline static int32_t get_offset_of__doumou_1() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____doumou_1)); }
	inline int32_t get__doumou_1() const { return ____doumou_1; }
	inline int32_t* get_address_of__doumou_1() { return &____doumou_1; }
	inline void set__doumou_1(int32_t value)
	{
		____doumou_1 = value;
	}

	inline static int32_t get_offset_of__ladroRedouse_2() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____ladroRedouse_2)); }
	inline float get__ladroRedouse_2() const { return ____ladroRedouse_2; }
	inline float* get_address_of__ladroRedouse_2() { return &____ladroRedouse_2; }
	inline void set__ladroRedouse_2(float value)
	{
		____ladroRedouse_2 = value;
	}

	inline static int32_t get_offset_of__tardreaHabo_3() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____tardreaHabo_3)); }
	inline int32_t get__tardreaHabo_3() const { return ____tardreaHabo_3; }
	inline int32_t* get_address_of__tardreaHabo_3() { return &____tardreaHabo_3; }
	inline void set__tardreaHabo_3(int32_t value)
	{
		____tardreaHabo_3 = value;
	}

	inline static int32_t get_offset_of__teresalllor_4() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____teresalllor_4)); }
	inline bool get__teresalllor_4() const { return ____teresalllor_4; }
	inline bool* get_address_of__teresalllor_4() { return &____teresalllor_4; }
	inline void set__teresalllor_4(bool value)
	{
		____teresalllor_4 = value;
	}

	inline static int32_t get_offset_of__hirurha_5() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____hirurha_5)); }
	inline bool get__hirurha_5() const { return ____hirurha_5; }
	inline bool* get_address_of__hirurha_5() { return &____hirurha_5; }
	inline void set__hirurha_5(bool value)
	{
		____hirurha_5 = value;
	}

	inline static int32_t get_offset_of__berewaho_6() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____berewaho_6)); }
	inline String_t* get__berewaho_6() const { return ____berewaho_6; }
	inline String_t** get_address_of__berewaho_6() { return &____berewaho_6; }
	inline void set__berewaho_6(String_t* value)
	{
		____berewaho_6 = value;
		Il2CppCodeGenWriteBarrier(&____berewaho_6, value);
	}

	inline static int32_t get_offset_of__hallwihay_7() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____hallwihay_7)); }
	inline String_t* get__hallwihay_7() const { return ____hallwihay_7; }
	inline String_t** get_address_of__hallwihay_7() { return &____hallwihay_7; }
	inline void set__hallwihay_7(String_t* value)
	{
		____hallwihay_7 = value;
		Il2CppCodeGenWriteBarrier(&____hallwihay_7, value);
	}

	inline static int32_t get_offset_of__fijerToojemur_8() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____fijerToojemur_8)); }
	inline float get__fijerToojemur_8() const { return ____fijerToojemur_8; }
	inline float* get_address_of__fijerToojemur_8() { return &____fijerToojemur_8; }
	inline void set__fijerToojemur_8(float value)
	{
		____fijerToojemur_8 = value;
	}

	inline static int32_t get_offset_of__riguwaForhel_9() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____riguwaForhel_9)); }
	inline uint32_t get__riguwaForhel_9() const { return ____riguwaForhel_9; }
	inline uint32_t* get_address_of__riguwaForhel_9() { return &____riguwaForhel_9; }
	inline void set__riguwaForhel_9(uint32_t value)
	{
		____riguwaForhel_9 = value;
	}

	inline static int32_t get_offset_of__kowgecallSawfi_10() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____kowgecallSawfi_10)); }
	inline String_t* get__kowgecallSawfi_10() const { return ____kowgecallSawfi_10; }
	inline String_t** get_address_of__kowgecallSawfi_10() { return &____kowgecallSawfi_10; }
	inline void set__kowgecallSawfi_10(String_t* value)
	{
		____kowgecallSawfi_10 = value;
		Il2CppCodeGenWriteBarrier(&____kowgecallSawfi_10, value);
	}

	inline static int32_t get_offset_of__medrallboo_11() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____medrallboo_11)); }
	inline float get__medrallboo_11() const { return ____medrallboo_11; }
	inline float* get_address_of__medrallboo_11() { return &____medrallboo_11; }
	inline void set__medrallboo_11(float value)
	{
		____medrallboo_11 = value;
	}

	inline static int32_t get_offset_of__lanawstouNemai_12() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____lanawstouNemai_12)); }
	inline String_t* get__lanawstouNemai_12() const { return ____lanawstouNemai_12; }
	inline String_t** get_address_of__lanawstouNemai_12() { return &____lanawstouNemai_12; }
	inline void set__lanawstouNemai_12(String_t* value)
	{
		____lanawstouNemai_12 = value;
		Il2CppCodeGenWriteBarrier(&____lanawstouNemai_12, value);
	}

	inline static int32_t get_offset_of__femou_13() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____femou_13)); }
	inline bool get__femou_13() const { return ____femou_13; }
	inline bool* get_address_of__femou_13() { return &____femou_13; }
	inline void set__femou_13(bool value)
	{
		____femou_13 = value;
	}

	inline static int32_t get_offset_of__cairlalDiralray_14() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____cairlalDiralray_14)); }
	inline int32_t get__cairlalDiralray_14() const { return ____cairlalDiralray_14; }
	inline int32_t* get_address_of__cairlalDiralray_14() { return &____cairlalDiralray_14; }
	inline void set__cairlalDiralray_14(int32_t value)
	{
		____cairlalDiralray_14 = value;
	}

	inline static int32_t get_offset_of__birlurgir_15() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____birlurgir_15)); }
	inline String_t* get__birlurgir_15() const { return ____birlurgir_15; }
	inline String_t** get_address_of__birlurgir_15() { return &____birlurgir_15; }
	inline void set__birlurgir_15(String_t* value)
	{
		____birlurgir_15 = value;
		Il2CppCodeGenWriteBarrier(&____birlurgir_15, value);
	}

	inline static int32_t get_offset_of__sepea_16() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____sepea_16)); }
	inline String_t* get__sepea_16() const { return ____sepea_16; }
	inline String_t** get_address_of__sepea_16() { return &____sepea_16; }
	inline void set__sepea_16(String_t* value)
	{
		____sepea_16 = value;
		Il2CppCodeGenWriteBarrier(&____sepea_16, value);
	}

	inline static int32_t get_offset_of__cisvaiBallsar_17() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____cisvaiBallsar_17)); }
	inline uint32_t get__cisvaiBallsar_17() const { return ____cisvaiBallsar_17; }
	inline uint32_t* get_address_of__cisvaiBallsar_17() { return &____cisvaiBallsar_17; }
	inline void set__cisvaiBallsar_17(uint32_t value)
	{
		____cisvaiBallsar_17 = value;
	}

	inline static int32_t get_offset_of__lowsas_18() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____lowsas_18)); }
	inline int32_t get__lowsas_18() const { return ____lowsas_18; }
	inline int32_t* get_address_of__lowsas_18() { return &____lowsas_18; }
	inline void set__lowsas_18(int32_t value)
	{
		____lowsas_18 = value;
	}

	inline static int32_t get_offset_of__sure_19() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____sure_19)); }
	inline uint32_t get__sure_19() const { return ____sure_19; }
	inline uint32_t* get_address_of__sure_19() { return &____sure_19; }
	inline void set__sure_19(uint32_t value)
	{
		____sure_19 = value;
	}

	inline static int32_t get_offset_of__zerucar_20() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____zerucar_20)); }
	inline bool get__zerucar_20() const { return ____zerucar_20; }
	inline bool* get_address_of__zerucar_20() { return &____zerucar_20; }
	inline void set__zerucar_20(bool value)
	{
		____zerucar_20 = value;
	}

	inline static int32_t get_offset_of__lisza_21() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____lisza_21)); }
	inline uint32_t get__lisza_21() const { return ____lisza_21; }
	inline uint32_t* get_address_of__lisza_21() { return &____lisza_21; }
	inline void set__lisza_21(uint32_t value)
	{
		____lisza_21 = value;
	}

	inline static int32_t get_offset_of__taslootrawMasatea_22() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____taslootrawMasatea_22)); }
	inline int32_t get__taslootrawMasatea_22() const { return ____taslootrawMasatea_22; }
	inline int32_t* get_address_of__taslootrawMasatea_22() { return &____taslootrawMasatea_22; }
	inline void set__taslootrawMasatea_22(int32_t value)
	{
		____taslootrawMasatea_22 = value;
	}

	inline static int32_t get_offset_of__jurchesa_23() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____jurchesa_23)); }
	inline String_t* get__jurchesa_23() const { return ____jurchesa_23; }
	inline String_t** get_address_of__jurchesa_23() { return &____jurchesa_23; }
	inline void set__jurchesa_23(String_t* value)
	{
		____jurchesa_23 = value;
		Il2CppCodeGenWriteBarrier(&____jurchesa_23, value);
	}

	inline static int32_t get_offset_of__vuyaldis_24() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____vuyaldis_24)); }
	inline String_t* get__vuyaldis_24() const { return ____vuyaldis_24; }
	inline String_t** get_address_of__vuyaldis_24() { return &____vuyaldis_24; }
	inline void set__vuyaldis_24(String_t* value)
	{
		____vuyaldis_24 = value;
		Il2CppCodeGenWriteBarrier(&____vuyaldis_24, value);
	}

	inline static int32_t get_offset_of__memstowDouti_25() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____memstowDouti_25)); }
	inline int32_t get__memstowDouti_25() const { return ____memstowDouti_25; }
	inline int32_t* get_address_of__memstowDouti_25() { return &____memstowDouti_25; }
	inline void set__memstowDouti_25(int32_t value)
	{
		____memstowDouti_25 = value;
	}

	inline static int32_t get_offset_of__whikalChaywhalaw_26() { return static_cast<int32_t>(offsetof(M_murleNasjaldoo86_t1266022906, ____whikalChaywhalaw_26)); }
	inline int32_t get__whikalChaywhalaw_26() const { return ____whikalChaywhalaw_26; }
	inline int32_t* get_address_of__whikalChaywhalaw_26() { return &____whikalChaywhalaw_26; }
	inline void set__whikalChaywhalaw_26(int32_t value)
	{
		____whikalChaywhalaw_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
