﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectAdjust
struct  EffectAdjust_t3544776864  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Transform EffectAdjust::tr
	Transform_t1659122786 * ___tr_2;
	// UnityEngine.RaycastHit EffectAdjust::hit
	RaycastHit_t4003175726  ___hit_3;
	// UnityEngine.Vector3 EffectAdjust::beforPos
	Vector3_t4282066566  ___beforPos_4;

public:
	inline static int32_t get_offset_of_tr_2() { return static_cast<int32_t>(offsetof(EffectAdjust_t3544776864, ___tr_2)); }
	inline Transform_t1659122786 * get_tr_2() const { return ___tr_2; }
	inline Transform_t1659122786 ** get_address_of_tr_2() { return &___tr_2; }
	inline void set_tr_2(Transform_t1659122786 * value)
	{
		___tr_2 = value;
		Il2CppCodeGenWriteBarrier(&___tr_2, value);
	}

	inline static int32_t get_offset_of_hit_3() { return static_cast<int32_t>(offsetof(EffectAdjust_t3544776864, ___hit_3)); }
	inline RaycastHit_t4003175726  get_hit_3() const { return ___hit_3; }
	inline RaycastHit_t4003175726 * get_address_of_hit_3() { return &___hit_3; }
	inline void set_hit_3(RaycastHit_t4003175726  value)
	{
		___hit_3 = value;
	}

	inline static int32_t get_offset_of_beforPos_4() { return static_cast<int32_t>(offsetof(EffectAdjust_t3544776864, ___beforPos_4)); }
	inline Vector3_t4282066566  get_beforPos_4() const { return ___beforPos_4; }
	inline Vector3_t4282066566 * get_address_of_beforPos_4() { return &___beforPos_4; }
	inline void set_beforPos_4(Vector3_t4282066566  value)
	{
		___beforPos_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
