﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginXiaoNiu
struct  PluginXiaoNiu_t2024052712  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginXiaoNiu::sessionId
	String_t* ___sessionId_2;
	// System.String PluginXiaoNiu::userid
	String_t* ___userid_3;
	// System.String PluginXiaoNiu::switchaccount
	String_t* ___switchaccount_4;
	// System.Boolean PluginXiaoNiu::isOut
	bool ___isOut_5;
	// System.String PluginXiaoNiu::configId
	String_t* ___configId_6;
	// System.String PluginXiaoNiu::appId
	String_t* ___appId_7;
	// Mihua.SDK.PayInfo PluginXiaoNiu::payInfo
	PayInfo_t1775308120 * ___payInfo_8;

public:
	inline static int32_t get_offset_of_sessionId_2() { return static_cast<int32_t>(offsetof(PluginXiaoNiu_t2024052712, ___sessionId_2)); }
	inline String_t* get_sessionId_2() const { return ___sessionId_2; }
	inline String_t** get_address_of_sessionId_2() { return &___sessionId_2; }
	inline void set_sessionId_2(String_t* value)
	{
		___sessionId_2 = value;
		Il2CppCodeGenWriteBarrier(&___sessionId_2, value);
	}

	inline static int32_t get_offset_of_userid_3() { return static_cast<int32_t>(offsetof(PluginXiaoNiu_t2024052712, ___userid_3)); }
	inline String_t* get_userid_3() const { return ___userid_3; }
	inline String_t** get_address_of_userid_3() { return &___userid_3; }
	inline void set_userid_3(String_t* value)
	{
		___userid_3 = value;
		Il2CppCodeGenWriteBarrier(&___userid_3, value);
	}

	inline static int32_t get_offset_of_switchaccount_4() { return static_cast<int32_t>(offsetof(PluginXiaoNiu_t2024052712, ___switchaccount_4)); }
	inline String_t* get_switchaccount_4() const { return ___switchaccount_4; }
	inline String_t** get_address_of_switchaccount_4() { return &___switchaccount_4; }
	inline void set_switchaccount_4(String_t* value)
	{
		___switchaccount_4 = value;
		Il2CppCodeGenWriteBarrier(&___switchaccount_4, value);
	}

	inline static int32_t get_offset_of_isOut_5() { return static_cast<int32_t>(offsetof(PluginXiaoNiu_t2024052712, ___isOut_5)); }
	inline bool get_isOut_5() const { return ___isOut_5; }
	inline bool* get_address_of_isOut_5() { return &___isOut_5; }
	inline void set_isOut_5(bool value)
	{
		___isOut_5 = value;
	}

	inline static int32_t get_offset_of_configId_6() { return static_cast<int32_t>(offsetof(PluginXiaoNiu_t2024052712, ___configId_6)); }
	inline String_t* get_configId_6() const { return ___configId_6; }
	inline String_t** get_address_of_configId_6() { return &___configId_6; }
	inline void set_configId_6(String_t* value)
	{
		___configId_6 = value;
		Il2CppCodeGenWriteBarrier(&___configId_6, value);
	}

	inline static int32_t get_offset_of_appId_7() { return static_cast<int32_t>(offsetof(PluginXiaoNiu_t2024052712, ___appId_7)); }
	inline String_t* get_appId_7() const { return ___appId_7; }
	inline String_t** get_address_of_appId_7() { return &___appId_7; }
	inline void set_appId_7(String_t* value)
	{
		___appId_7 = value;
		Il2CppCodeGenWriteBarrier(&___appId_7, value);
	}

	inline static int32_t get_offset_of_payInfo_8() { return static_cast<int32_t>(offsetof(PluginXiaoNiu_t2024052712, ___payInfo_8)); }
	inline PayInfo_t1775308120 * get_payInfo_8() const { return ___payInfo_8; }
	inline PayInfo_t1775308120 ** get_address_of_payInfo_8() { return &___payInfo_8; }
	inline void set_payInfo_8(PayInfo_t1775308120 * value)
	{
		___payInfo_8 = value;
		Il2CppCodeGenWriteBarrier(&___payInfo_8, value);
	}
};

struct PluginXiaoNiu_t2024052712_StaticFields
{
public:
	// System.Action PluginXiaoNiu::<>f__am$cache7
	Action_t3771233898 * ___U3CU3Ef__amU24cache7_9;
	// System.Action PluginXiaoNiu::<>f__am$cache8
	Action_t3771233898 * ___U3CU3Ef__amU24cache8_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_9() { return static_cast<int32_t>(offsetof(PluginXiaoNiu_t2024052712_StaticFields, ___U3CU3Ef__amU24cache7_9)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache7_9() const { return ___U3CU3Ef__amU24cache7_9; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache7_9() { return &___U3CU3Ef__amU24cache7_9; }
	inline void set_U3CU3Ef__amU24cache7_9(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache7_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_10() { return static_cast<int32_t>(offsetof(PluginXiaoNiu_t2024052712_StaticFields, ___U3CU3Ef__amU24cache8_10)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache8_10() const { return ___U3CU3Ef__amU24cache8_10; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache8_10() { return &___U3CU3Ef__amU24cache8_10; }
	inline void set_U3CU3Ef__amU24cache8_10(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache8_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
