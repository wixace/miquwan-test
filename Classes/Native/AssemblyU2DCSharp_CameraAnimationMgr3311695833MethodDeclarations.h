﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraAnimationMgr
struct CameraAnimationMgr_t3311695833;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CameraAnimationMgr3311695833.h"

// System.Void CameraAnimationMgr::.ctor()
extern "C"  void CameraAnimationMgr__ctor_m421022946 (CameraAnimationMgr_t3311695833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraAnimationMgr::set_IsPlaying(System.Boolean)
extern "C"  void CameraAnimationMgr_set_IsPlaying_m3126457982 (CameraAnimationMgr_t3311695833 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraAnimationMgr::get_IsPlaying()
extern "C"  bool CameraAnimationMgr_get_IsPlaying_m343835399 (CameraAnimationMgr_t3311695833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraAnimationMgr::OnAwake(UnityEngine.GameObject,System.String)
extern "C"  void CameraAnimationMgr_OnAwake_m75327066 (CameraAnimationMgr_t3311695833 * __this, GameObject_t3674682005 * ___go0, String_t* ___aniName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraAnimationMgr::Play()
extern "C"  void CameraAnimationMgr_Play_m4181314134 (CameraAnimationMgr_t3311695833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraAnimationMgr::Stop()
extern "C"  void CameraAnimationMgr_Stop_m4274998180 (CameraAnimationMgr_t3311695833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraAnimationMgr::OnDestroy()
extern "C"  void CameraAnimationMgr_OnDestroy_m2555699419 (CameraAnimationMgr_t3311695833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraAnimationMgr::SetCameraState(System.Boolean)
extern "C"  void CameraAnimationMgr_SetCameraState_m3304976259 (CameraAnimationMgr_t3311695833 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraAnimationMgr::ilo_SetCameraState1(CameraAnimationMgr,System.Boolean)
extern "C"  void CameraAnimationMgr_ilo_SetCameraState1_m2601688226 (Il2CppObject * __this /* static, unused */, CameraAnimationMgr_t3311695833 * ____this0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraAnimationMgr::ilo_set_IsPlaying2(CameraAnimationMgr,System.Boolean)
extern "C"  void CameraAnimationMgr_ilo_set_IsPlaying2_m2045616182 (Il2CppObject * __this /* static, unused */, CameraAnimationMgr_t3311695833 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
