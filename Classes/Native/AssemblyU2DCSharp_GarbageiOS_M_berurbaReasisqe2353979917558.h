﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_berurbaReasisqe235
struct  M_berurbaReasisqe235_t3979917558  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_berurbaReasisqe235::_temzaiHeerasmaw
	String_t* ____temzaiHeerasmaw_0;
	// System.UInt32 GarbageiOS.M_berurbaReasisqe235::_heagaw
	uint32_t ____heagaw_1;
	// System.UInt32 GarbageiOS.M_berurbaReasisqe235::_jutaiSaldrel
	uint32_t ____jutaiSaldrel_2;
	// System.Boolean GarbageiOS.M_berurbaReasisqe235::_fowwalWosow
	bool ____fowwalWosow_3;
	// System.Int32 GarbageiOS.M_berurbaReasisqe235::_mayheTairka
	int32_t ____mayheTairka_4;
	// System.String GarbageiOS.M_berurbaReasisqe235::_rairnaw
	String_t* ____rairnaw_5;
	// System.UInt32 GarbageiOS.M_berurbaReasisqe235::_wheakaiWajilu
	uint32_t ____wheakaiWajilu_6;
	// System.Boolean GarbageiOS.M_berurbaReasisqe235::_kapePale
	bool ____kapePale_7;
	// System.UInt32 GarbageiOS.M_berurbaReasisqe235::_gogear
	uint32_t ____gogear_8;
	// System.Int32 GarbageiOS.M_berurbaReasisqe235::_mirdeefi
	int32_t ____mirdeefi_9;
	// System.UInt32 GarbageiOS.M_berurbaReasisqe235::_forniQija
	uint32_t ____forniQija_10;
	// System.String GarbageiOS.M_berurbaReasisqe235::_gereseduLetapa
	String_t* ____gereseduLetapa_11;
	// System.Single GarbageiOS.M_berurbaReasisqe235::_tisor
	float ____tisor_12;
	// System.String GarbageiOS.M_berurbaReasisqe235::_hikijarSenaw
	String_t* ____hikijarSenaw_13;
	// System.Boolean GarbageiOS.M_berurbaReasisqe235::_jobea
	bool ____jobea_14;
	// System.Single GarbageiOS.M_berurbaReasisqe235::_dupawhawNosorrou
	float ____dupawhawNosorrou_15;
	// System.String GarbageiOS.M_berurbaReasisqe235::_noukalDuce
	String_t* ____noukalDuce_16;
	// System.Single GarbageiOS.M_berurbaReasisqe235::_fere
	float ____fere_17;
	// System.String GarbageiOS.M_berurbaReasisqe235::_berecha
	String_t* ____berecha_18;
	// System.Single GarbageiOS.M_berurbaReasisqe235::_mili
	float ____mili_19;
	// System.Int32 GarbageiOS.M_berurbaReasisqe235::_zearmerLaci
	int32_t ____zearmerLaci_20;
	// System.Single GarbageiOS.M_berurbaReasisqe235::_chedenooKairrufa
	float ____chedenooKairrufa_21;
	// System.Single GarbageiOS.M_berurbaReasisqe235::_sofuce
	float ____sofuce_22;
	// System.String GarbageiOS.M_berurbaReasisqe235::_senairHerdee
	String_t* ____senairHerdee_23;
	// System.Int32 GarbageiOS.M_berurbaReasisqe235::_hacudiStowcallsa
	int32_t ____hacudiStowcallsa_24;
	// System.UInt32 GarbageiOS.M_berurbaReasisqe235::_ceakairKiba
	uint32_t ____ceakairKiba_25;
	// System.Int32 GarbageiOS.M_berurbaReasisqe235::_suborDarjou
	int32_t ____suborDarjou_26;
	// System.Single GarbageiOS.M_berurbaReasisqe235::_lordraitral
	float ____lordraitral_27;
	// System.String GarbageiOS.M_berurbaReasisqe235::_hearpa
	String_t* ____hearpa_28;
	// System.String GarbageiOS.M_berurbaReasisqe235::_wujallgi
	String_t* ____wujallgi_29;
	// System.Boolean GarbageiOS.M_berurbaReasisqe235::_hairar
	bool ____hairar_30;
	// System.Int32 GarbageiOS.M_berurbaReasisqe235::_saysu
	int32_t ____saysu_31;
	// System.Single GarbageiOS.M_berurbaReasisqe235::_mecallxear
	float ____mecallxear_32;
	// System.UInt32 GarbageiOS.M_berurbaReasisqe235::_telri
	uint32_t ____telri_33;
	// System.String GarbageiOS.M_berurbaReasisqe235::_tapearea
	String_t* ____tapearea_34;
	// System.Single GarbageiOS.M_berurbaReasisqe235::_sirse
	float ____sirse_35;
	// System.Single GarbageiOS.M_berurbaReasisqe235::_sercho
	float ____sercho_36;
	// System.UInt32 GarbageiOS.M_berurbaReasisqe235::_lurdofeaPihor
	uint32_t ____lurdofeaPihor_37;
	// System.Single GarbageiOS.M_berurbaReasisqe235::_rorpesee
	float ____rorpesee_38;
	// System.Int32 GarbageiOS.M_berurbaReasisqe235::_hirpavou
	int32_t ____hirpavou_39;
	// System.Single GarbageiOS.M_berurbaReasisqe235::_sokeakelXerer
	float ____sokeakelXerer_40;
	// System.Int32 GarbageiOS.M_berurbaReasisqe235::_hahemow
	int32_t ____hahemow_41;

public:
	inline static int32_t get_offset_of__temzaiHeerasmaw_0() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____temzaiHeerasmaw_0)); }
	inline String_t* get__temzaiHeerasmaw_0() const { return ____temzaiHeerasmaw_0; }
	inline String_t** get_address_of__temzaiHeerasmaw_0() { return &____temzaiHeerasmaw_0; }
	inline void set__temzaiHeerasmaw_0(String_t* value)
	{
		____temzaiHeerasmaw_0 = value;
		Il2CppCodeGenWriteBarrier(&____temzaiHeerasmaw_0, value);
	}

	inline static int32_t get_offset_of__heagaw_1() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____heagaw_1)); }
	inline uint32_t get__heagaw_1() const { return ____heagaw_1; }
	inline uint32_t* get_address_of__heagaw_1() { return &____heagaw_1; }
	inline void set__heagaw_1(uint32_t value)
	{
		____heagaw_1 = value;
	}

	inline static int32_t get_offset_of__jutaiSaldrel_2() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____jutaiSaldrel_2)); }
	inline uint32_t get__jutaiSaldrel_2() const { return ____jutaiSaldrel_2; }
	inline uint32_t* get_address_of__jutaiSaldrel_2() { return &____jutaiSaldrel_2; }
	inline void set__jutaiSaldrel_2(uint32_t value)
	{
		____jutaiSaldrel_2 = value;
	}

	inline static int32_t get_offset_of__fowwalWosow_3() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____fowwalWosow_3)); }
	inline bool get__fowwalWosow_3() const { return ____fowwalWosow_3; }
	inline bool* get_address_of__fowwalWosow_3() { return &____fowwalWosow_3; }
	inline void set__fowwalWosow_3(bool value)
	{
		____fowwalWosow_3 = value;
	}

	inline static int32_t get_offset_of__mayheTairka_4() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____mayheTairka_4)); }
	inline int32_t get__mayheTairka_4() const { return ____mayheTairka_4; }
	inline int32_t* get_address_of__mayheTairka_4() { return &____mayheTairka_4; }
	inline void set__mayheTairka_4(int32_t value)
	{
		____mayheTairka_4 = value;
	}

	inline static int32_t get_offset_of__rairnaw_5() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____rairnaw_5)); }
	inline String_t* get__rairnaw_5() const { return ____rairnaw_5; }
	inline String_t** get_address_of__rairnaw_5() { return &____rairnaw_5; }
	inline void set__rairnaw_5(String_t* value)
	{
		____rairnaw_5 = value;
		Il2CppCodeGenWriteBarrier(&____rairnaw_5, value);
	}

	inline static int32_t get_offset_of__wheakaiWajilu_6() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____wheakaiWajilu_6)); }
	inline uint32_t get__wheakaiWajilu_6() const { return ____wheakaiWajilu_6; }
	inline uint32_t* get_address_of__wheakaiWajilu_6() { return &____wheakaiWajilu_6; }
	inline void set__wheakaiWajilu_6(uint32_t value)
	{
		____wheakaiWajilu_6 = value;
	}

	inline static int32_t get_offset_of__kapePale_7() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____kapePale_7)); }
	inline bool get__kapePale_7() const { return ____kapePale_7; }
	inline bool* get_address_of__kapePale_7() { return &____kapePale_7; }
	inline void set__kapePale_7(bool value)
	{
		____kapePale_7 = value;
	}

	inline static int32_t get_offset_of__gogear_8() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____gogear_8)); }
	inline uint32_t get__gogear_8() const { return ____gogear_8; }
	inline uint32_t* get_address_of__gogear_8() { return &____gogear_8; }
	inline void set__gogear_8(uint32_t value)
	{
		____gogear_8 = value;
	}

	inline static int32_t get_offset_of__mirdeefi_9() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____mirdeefi_9)); }
	inline int32_t get__mirdeefi_9() const { return ____mirdeefi_9; }
	inline int32_t* get_address_of__mirdeefi_9() { return &____mirdeefi_9; }
	inline void set__mirdeefi_9(int32_t value)
	{
		____mirdeefi_9 = value;
	}

	inline static int32_t get_offset_of__forniQija_10() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____forniQija_10)); }
	inline uint32_t get__forniQija_10() const { return ____forniQija_10; }
	inline uint32_t* get_address_of__forniQija_10() { return &____forniQija_10; }
	inline void set__forniQija_10(uint32_t value)
	{
		____forniQija_10 = value;
	}

	inline static int32_t get_offset_of__gereseduLetapa_11() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____gereseduLetapa_11)); }
	inline String_t* get__gereseduLetapa_11() const { return ____gereseduLetapa_11; }
	inline String_t** get_address_of__gereseduLetapa_11() { return &____gereseduLetapa_11; }
	inline void set__gereseduLetapa_11(String_t* value)
	{
		____gereseduLetapa_11 = value;
		Il2CppCodeGenWriteBarrier(&____gereseduLetapa_11, value);
	}

	inline static int32_t get_offset_of__tisor_12() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____tisor_12)); }
	inline float get__tisor_12() const { return ____tisor_12; }
	inline float* get_address_of__tisor_12() { return &____tisor_12; }
	inline void set__tisor_12(float value)
	{
		____tisor_12 = value;
	}

	inline static int32_t get_offset_of__hikijarSenaw_13() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____hikijarSenaw_13)); }
	inline String_t* get__hikijarSenaw_13() const { return ____hikijarSenaw_13; }
	inline String_t** get_address_of__hikijarSenaw_13() { return &____hikijarSenaw_13; }
	inline void set__hikijarSenaw_13(String_t* value)
	{
		____hikijarSenaw_13 = value;
		Il2CppCodeGenWriteBarrier(&____hikijarSenaw_13, value);
	}

	inline static int32_t get_offset_of__jobea_14() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____jobea_14)); }
	inline bool get__jobea_14() const { return ____jobea_14; }
	inline bool* get_address_of__jobea_14() { return &____jobea_14; }
	inline void set__jobea_14(bool value)
	{
		____jobea_14 = value;
	}

	inline static int32_t get_offset_of__dupawhawNosorrou_15() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____dupawhawNosorrou_15)); }
	inline float get__dupawhawNosorrou_15() const { return ____dupawhawNosorrou_15; }
	inline float* get_address_of__dupawhawNosorrou_15() { return &____dupawhawNosorrou_15; }
	inline void set__dupawhawNosorrou_15(float value)
	{
		____dupawhawNosorrou_15 = value;
	}

	inline static int32_t get_offset_of__noukalDuce_16() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____noukalDuce_16)); }
	inline String_t* get__noukalDuce_16() const { return ____noukalDuce_16; }
	inline String_t** get_address_of__noukalDuce_16() { return &____noukalDuce_16; }
	inline void set__noukalDuce_16(String_t* value)
	{
		____noukalDuce_16 = value;
		Il2CppCodeGenWriteBarrier(&____noukalDuce_16, value);
	}

	inline static int32_t get_offset_of__fere_17() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____fere_17)); }
	inline float get__fere_17() const { return ____fere_17; }
	inline float* get_address_of__fere_17() { return &____fere_17; }
	inline void set__fere_17(float value)
	{
		____fere_17 = value;
	}

	inline static int32_t get_offset_of__berecha_18() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____berecha_18)); }
	inline String_t* get__berecha_18() const { return ____berecha_18; }
	inline String_t** get_address_of__berecha_18() { return &____berecha_18; }
	inline void set__berecha_18(String_t* value)
	{
		____berecha_18 = value;
		Il2CppCodeGenWriteBarrier(&____berecha_18, value);
	}

	inline static int32_t get_offset_of__mili_19() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____mili_19)); }
	inline float get__mili_19() const { return ____mili_19; }
	inline float* get_address_of__mili_19() { return &____mili_19; }
	inline void set__mili_19(float value)
	{
		____mili_19 = value;
	}

	inline static int32_t get_offset_of__zearmerLaci_20() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____zearmerLaci_20)); }
	inline int32_t get__zearmerLaci_20() const { return ____zearmerLaci_20; }
	inline int32_t* get_address_of__zearmerLaci_20() { return &____zearmerLaci_20; }
	inline void set__zearmerLaci_20(int32_t value)
	{
		____zearmerLaci_20 = value;
	}

	inline static int32_t get_offset_of__chedenooKairrufa_21() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____chedenooKairrufa_21)); }
	inline float get__chedenooKairrufa_21() const { return ____chedenooKairrufa_21; }
	inline float* get_address_of__chedenooKairrufa_21() { return &____chedenooKairrufa_21; }
	inline void set__chedenooKairrufa_21(float value)
	{
		____chedenooKairrufa_21 = value;
	}

	inline static int32_t get_offset_of__sofuce_22() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____sofuce_22)); }
	inline float get__sofuce_22() const { return ____sofuce_22; }
	inline float* get_address_of__sofuce_22() { return &____sofuce_22; }
	inline void set__sofuce_22(float value)
	{
		____sofuce_22 = value;
	}

	inline static int32_t get_offset_of__senairHerdee_23() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____senairHerdee_23)); }
	inline String_t* get__senairHerdee_23() const { return ____senairHerdee_23; }
	inline String_t** get_address_of__senairHerdee_23() { return &____senairHerdee_23; }
	inline void set__senairHerdee_23(String_t* value)
	{
		____senairHerdee_23 = value;
		Il2CppCodeGenWriteBarrier(&____senairHerdee_23, value);
	}

	inline static int32_t get_offset_of__hacudiStowcallsa_24() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____hacudiStowcallsa_24)); }
	inline int32_t get__hacudiStowcallsa_24() const { return ____hacudiStowcallsa_24; }
	inline int32_t* get_address_of__hacudiStowcallsa_24() { return &____hacudiStowcallsa_24; }
	inline void set__hacudiStowcallsa_24(int32_t value)
	{
		____hacudiStowcallsa_24 = value;
	}

	inline static int32_t get_offset_of__ceakairKiba_25() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____ceakairKiba_25)); }
	inline uint32_t get__ceakairKiba_25() const { return ____ceakairKiba_25; }
	inline uint32_t* get_address_of__ceakairKiba_25() { return &____ceakairKiba_25; }
	inline void set__ceakairKiba_25(uint32_t value)
	{
		____ceakairKiba_25 = value;
	}

	inline static int32_t get_offset_of__suborDarjou_26() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____suborDarjou_26)); }
	inline int32_t get__suborDarjou_26() const { return ____suborDarjou_26; }
	inline int32_t* get_address_of__suborDarjou_26() { return &____suborDarjou_26; }
	inline void set__suborDarjou_26(int32_t value)
	{
		____suborDarjou_26 = value;
	}

	inline static int32_t get_offset_of__lordraitral_27() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____lordraitral_27)); }
	inline float get__lordraitral_27() const { return ____lordraitral_27; }
	inline float* get_address_of__lordraitral_27() { return &____lordraitral_27; }
	inline void set__lordraitral_27(float value)
	{
		____lordraitral_27 = value;
	}

	inline static int32_t get_offset_of__hearpa_28() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____hearpa_28)); }
	inline String_t* get__hearpa_28() const { return ____hearpa_28; }
	inline String_t** get_address_of__hearpa_28() { return &____hearpa_28; }
	inline void set__hearpa_28(String_t* value)
	{
		____hearpa_28 = value;
		Il2CppCodeGenWriteBarrier(&____hearpa_28, value);
	}

	inline static int32_t get_offset_of__wujallgi_29() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____wujallgi_29)); }
	inline String_t* get__wujallgi_29() const { return ____wujallgi_29; }
	inline String_t** get_address_of__wujallgi_29() { return &____wujallgi_29; }
	inline void set__wujallgi_29(String_t* value)
	{
		____wujallgi_29 = value;
		Il2CppCodeGenWriteBarrier(&____wujallgi_29, value);
	}

	inline static int32_t get_offset_of__hairar_30() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____hairar_30)); }
	inline bool get__hairar_30() const { return ____hairar_30; }
	inline bool* get_address_of__hairar_30() { return &____hairar_30; }
	inline void set__hairar_30(bool value)
	{
		____hairar_30 = value;
	}

	inline static int32_t get_offset_of__saysu_31() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____saysu_31)); }
	inline int32_t get__saysu_31() const { return ____saysu_31; }
	inline int32_t* get_address_of__saysu_31() { return &____saysu_31; }
	inline void set__saysu_31(int32_t value)
	{
		____saysu_31 = value;
	}

	inline static int32_t get_offset_of__mecallxear_32() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____mecallxear_32)); }
	inline float get__mecallxear_32() const { return ____mecallxear_32; }
	inline float* get_address_of__mecallxear_32() { return &____mecallxear_32; }
	inline void set__mecallxear_32(float value)
	{
		____mecallxear_32 = value;
	}

	inline static int32_t get_offset_of__telri_33() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____telri_33)); }
	inline uint32_t get__telri_33() const { return ____telri_33; }
	inline uint32_t* get_address_of__telri_33() { return &____telri_33; }
	inline void set__telri_33(uint32_t value)
	{
		____telri_33 = value;
	}

	inline static int32_t get_offset_of__tapearea_34() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____tapearea_34)); }
	inline String_t* get__tapearea_34() const { return ____tapearea_34; }
	inline String_t** get_address_of__tapearea_34() { return &____tapearea_34; }
	inline void set__tapearea_34(String_t* value)
	{
		____tapearea_34 = value;
		Il2CppCodeGenWriteBarrier(&____tapearea_34, value);
	}

	inline static int32_t get_offset_of__sirse_35() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____sirse_35)); }
	inline float get__sirse_35() const { return ____sirse_35; }
	inline float* get_address_of__sirse_35() { return &____sirse_35; }
	inline void set__sirse_35(float value)
	{
		____sirse_35 = value;
	}

	inline static int32_t get_offset_of__sercho_36() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____sercho_36)); }
	inline float get__sercho_36() const { return ____sercho_36; }
	inline float* get_address_of__sercho_36() { return &____sercho_36; }
	inline void set__sercho_36(float value)
	{
		____sercho_36 = value;
	}

	inline static int32_t get_offset_of__lurdofeaPihor_37() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____lurdofeaPihor_37)); }
	inline uint32_t get__lurdofeaPihor_37() const { return ____lurdofeaPihor_37; }
	inline uint32_t* get_address_of__lurdofeaPihor_37() { return &____lurdofeaPihor_37; }
	inline void set__lurdofeaPihor_37(uint32_t value)
	{
		____lurdofeaPihor_37 = value;
	}

	inline static int32_t get_offset_of__rorpesee_38() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____rorpesee_38)); }
	inline float get__rorpesee_38() const { return ____rorpesee_38; }
	inline float* get_address_of__rorpesee_38() { return &____rorpesee_38; }
	inline void set__rorpesee_38(float value)
	{
		____rorpesee_38 = value;
	}

	inline static int32_t get_offset_of__hirpavou_39() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____hirpavou_39)); }
	inline int32_t get__hirpavou_39() const { return ____hirpavou_39; }
	inline int32_t* get_address_of__hirpavou_39() { return &____hirpavou_39; }
	inline void set__hirpavou_39(int32_t value)
	{
		____hirpavou_39 = value;
	}

	inline static int32_t get_offset_of__sokeakelXerer_40() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____sokeakelXerer_40)); }
	inline float get__sokeakelXerer_40() const { return ____sokeakelXerer_40; }
	inline float* get_address_of__sokeakelXerer_40() { return &____sokeakelXerer_40; }
	inline void set__sokeakelXerer_40(float value)
	{
		____sokeakelXerer_40 = value;
	}

	inline static int32_t get_offset_of__hahemow_41() { return static_cast<int32_t>(offsetof(M_berurbaReasisqe235_t3979917558, ____hahemow_41)); }
	inline int32_t get__hahemow_41() const { return ____hahemow_41; }
	inline int32_t* get_address_of__hahemow_41() { return &____hahemow_41; }
	inline void set__hahemow_41(int32_t value)
	{
		____hahemow_41 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
