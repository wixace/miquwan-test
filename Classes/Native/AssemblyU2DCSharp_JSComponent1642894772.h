﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_JSSerializer3534558139.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSComponent
struct  JSComponent_t1642894772  : public JSSerializer_t3534558139
{
public:
	// System.Int32 JSComponent::jsObjID
	int32_t ___jsObjID_9;
	// System.Int32 JSComponent::idAwake
	int32_t ___idAwake_10;
	// System.Int32 JSComponent::idStart
	int32_t ___idStart_11;
	// System.Int32 JSComponent::idOnDestroy
	int32_t ___idOnDestroy_12;
	// System.Int32 JSComponent::idStartSinking
	int32_t ___idStartSinking_13;
	// System.Int32 JSComponent::idRestartLevel
	int32_t ___idRestartLevel_14;
	// System.Int32 JSComponent::jsState
	int32_t ___jsState_15;

public:
	inline static int32_t get_offset_of_jsObjID_9() { return static_cast<int32_t>(offsetof(JSComponent_t1642894772, ___jsObjID_9)); }
	inline int32_t get_jsObjID_9() const { return ___jsObjID_9; }
	inline int32_t* get_address_of_jsObjID_9() { return &___jsObjID_9; }
	inline void set_jsObjID_9(int32_t value)
	{
		___jsObjID_9 = value;
	}

	inline static int32_t get_offset_of_idAwake_10() { return static_cast<int32_t>(offsetof(JSComponent_t1642894772, ___idAwake_10)); }
	inline int32_t get_idAwake_10() const { return ___idAwake_10; }
	inline int32_t* get_address_of_idAwake_10() { return &___idAwake_10; }
	inline void set_idAwake_10(int32_t value)
	{
		___idAwake_10 = value;
	}

	inline static int32_t get_offset_of_idStart_11() { return static_cast<int32_t>(offsetof(JSComponent_t1642894772, ___idStart_11)); }
	inline int32_t get_idStart_11() const { return ___idStart_11; }
	inline int32_t* get_address_of_idStart_11() { return &___idStart_11; }
	inline void set_idStart_11(int32_t value)
	{
		___idStart_11 = value;
	}

	inline static int32_t get_offset_of_idOnDestroy_12() { return static_cast<int32_t>(offsetof(JSComponent_t1642894772, ___idOnDestroy_12)); }
	inline int32_t get_idOnDestroy_12() const { return ___idOnDestroy_12; }
	inline int32_t* get_address_of_idOnDestroy_12() { return &___idOnDestroy_12; }
	inline void set_idOnDestroy_12(int32_t value)
	{
		___idOnDestroy_12 = value;
	}

	inline static int32_t get_offset_of_idStartSinking_13() { return static_cast<int32_t>(offsetof(JSComponent_t1642894772, ___idStartSinking_13)); }
	inline int32_t get_idStartSinking_13() const { return ___idStartSinking_13; }
	inline int32_t* get_address_of_idStartSinking_13() { return &___idStartSinking_13; }
	inline void set_idStartSinking_13(int32_t value)
	{
		___idStartSinking_13 = value;
	}

	inline static int32_t get_offset_of_idRestartLevel_14() { return static_cast<int32_t>(offsetof(JSComponent_t1642894772, ___idRestartLevel_14)); }
	inline int32_t get_idRestartLevel_14() const { return ___idRestartLevel_14; }
	inline int32_t* get_address_of_idRestartLevel_14() { return &___idRestartLevel_14; }
	inline void set_idRestartLevel_14(int32_t value)
	{
		___idRestartLevel_14 = value;
	}

	inline static int32_t get_offset_of_jsState_15() { return static_cast<int32_t>(offsetof(JSComponent_t1642894772, ___jsState_15)); }
	inline int32_t get_jsState_15() const { return ___jsState_15; }
	inline int32_t* get_address_of_jsState_15() { return &___jsState_15; }
	inline void set_jsState_15(int32_t value)
	{
		___jsState_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
