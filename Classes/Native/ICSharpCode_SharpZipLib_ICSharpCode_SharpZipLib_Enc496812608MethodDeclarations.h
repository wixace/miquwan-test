﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Encryption.ZipAESTransform
struct ZipAESTransform_t496812608;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"

// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern "C"  int32_t ZipAESTransform_TransformBlock_m201067254 (ZipAESTransform_t496812608 * __this, ByteU5BU5D_t4260760469* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, ByteU5BU5D_t4260760469* ___outputBuffer3, int32_t ___outputOffset4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::GetAuthCode()
extern "C"  ByteU5BU5D_t4260760469* ZipAESTransform_GetAuthCode_m1195602789 (ZipAESTransform_t496812608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern "C"  ByteU5BU5D_t4260760469* ZipAESTransform_TransformFinalBlock_m3809853644 (ZipAESTransform_t496812608 * __this, ByteU5BU5D_t4260760469* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::get_CanReuseTransform()
extern "C"  bool ZipAESTransform_get_CanReuseTransform_m2202231249 (ZipAESTransform_t496812608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::Dispose()
extern "C"  void ZipAESTransform_Dispose_m1286558275 (ZipAESTransform_t496812608 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
