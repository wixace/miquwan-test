﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Text_EncodingGenerated
struct System_Text_EncodingGenerated_t369736410;
// JSVCall
struct JSVCall_t3708497963;
// System.Char[]
struct CharU5BU5D_t3324145743;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"

// System.Void System_Text_EncodingGenerated::.ctor()
extern "C"  void System_Text_EncodingGenerated__ctor_m3704450001 (System_Text_EncodingGenerated_t369736410 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::Encoding_IsReadOnly(JSVCall)
extern "C"  void System_Text_EncodingGenerated_Encoding_IsReadOnly_m1280877856 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::Encoding_IsSingleByte(JSVCall)
extern "C"  void System_Text_EncodingGenerated_Encoding_IsSingleByte_m3163255506 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::Encoding_DecoderFallback(JSVCall)
extern "C"  void System_Text_EncodingGenerated_Encoding_DecoderFallback_m1150551290 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::Encoding_EncoderFallback(JSVCall)
extern "C"  void System_Text_EncodingGenerated_Encoding_EncoderFallback_m3696490274 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::Encoding_BodyName(JSVCall)
extern "C"  void System_Text_EncodingGenerated_Encoding_BodyName_m3814127135 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::Encoding_CodePage(JSVCall)
extern "C"  void System_Text_EncodingGenerated_Encoding_CodePage_m3998674992 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::Encoding_EncodingName(JSVCall)
extern "C"  void System_Text_EncodingGenerated_Encoding_EncodingName_m4047451918 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::Encoding_HeaderName(JSVCall)
extern "C"  void System_Text_EncodingGenerated_Encoding_HeaderName_m2235836820 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::Encoding_IsBrowserDisplay(JSVCall)
extern "C"  void System_Text_EncodingGenerated_Encoding_IsBrowserDisplay_m223628552 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::Encoding_IsBrowserSave(JSVCall)
extern "C"  void System_Text_EncodingGenerated_Encoding_IsBrowserSave_m1224583461 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::Encoding_IsMailNewsDisplay(JSVCall)
extern "C"  void System_Text_EncodingGenerated_Encoding_IsMailNewsDisplay_m325339410 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::Encoding_IsMailNewsSave(JSVCall)
extern "C"  void System_Text_EncodingGenerated_Encoding_IsMailNewsSave_m2747021659 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::Encoding_WebName(JSVCall)
extern "C"  void System_Text_EncodingGenerated_Encoding_WebName_m2091721313 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::Encoding_WindowsCodePage(JSVCall)
extern "C"  void System_Text_EncodingGenerated_Encoding_WindowsCodePage_m3386496673 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::Encoding_ASCII(JSVCall)
extern "C"  void System_Text_EncodingGenerated_Encoding_ASCII_m1500364239 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::Encoding_BigEndianUnicode(JSVCall)
extern "C"  void System_Text_EncodingGenerated_Encoding_BigEndianUnicode_m2635534698 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::Encoding_Default(JSVCall)
extern "C"  void System_Text_EncodingGenerated_Encoding_Default_m1330914911 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::Encoding_UTF7(JSVCall)
extern "C"  void System_Text_EncodingGenerated_Encoding_UTF7_m124884060 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::Encoding_UTF8(JSVCall)
extern "C"  void System_Text_EncodingGenerated_Encoding_UTF8_m4223337851 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::Encoding_Unicode(JSVCall)
extern "C"  void System_Text_EncodingGenerated_Encoding_Unicode_m3485266243 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::Encoding_UTF32(JSVCall)
extern "C"  void System_Text_EncodingGenerated_Encoding_UTF32_m1176039034 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_Clone(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_Clone_m2725916992 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_Equals__Object(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_Equals__Object_m2473943101 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetByteCount__Char_Array__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetByteCount__Char_Array__Int32__Int32_m1638338880 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetByteCount__Char_Array(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetByteCount__Char_Array_m3629357888 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetByteCount__String(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetByteCount__String_m4292500481 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetBytes__String__Int32__Int32__Byte_Array__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetBytes__String__Int32__Int32__Byte_Array__Int32_m2019606249 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetBytes__Char_Array__Int32__Int32__Byte_Array__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetBytes__Char_Array__Int32__Int32__Byte_Array__Int32_m1105216906 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetBytes__Char_Array__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetBytes__Char_Array__Int32__Int32_m1660686532 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetBytes__Char_Array(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetBytes__Char_Array_m3439088068 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetBytes__String(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetBytes__String_m3343578245 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetCharCount__Byte_Array__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetCharCount__Byte_Array__Int32__Int32_m321995492 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetCharCount__Byte_Array(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetCharCount__Byte_Array_m193313764 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32_m506714968 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetChars__Byte_Array__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetChars__Byte_Array__Int32__Int32_m2240629608 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetChars__Byte_Array(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetChars__Byte_Array_m1615412072 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetDecoder(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetDecoder_m648372045 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetEncoder(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetEncoder_m3343812133 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetHashCode(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetHashCode_m3254364404 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetMaxByteCount__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetMaxByteCount__Int32_m1960212756 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetMaxCharCount__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetMaxCharCount__Int32_m542414818 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetPreamble(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetPreamble_m1078429445 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetString__Byte_Array__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetString__Byte_Array__Int32__Int32_m2675949740 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetString__Byte_Array(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetString__Byte_Array_m4031543212 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_IsAlwaysNormalized__NormalizationForm(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_IsAlwaysNormalized__NormalizationForm_m237754164 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_IsAlwaysNormalized(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_IsAlwaysNormalized_m4221389679 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_Convert__Encoding__Encoding__Byte_Array__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_Convert__Encoding__Encoding__Byte_Array__Int32__Int32_m1181525566 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_Convert__Encoding__Encoding__Byte_Array(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_Convert__Encoding__Encoding__Byte_Array_m3953042622 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetEncoding__Int32__EncoderFallback__DecoderFallback(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetEncoding__Int32__EncoderFallback__DecoderFallback_m792291788 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetEncoding__String__EncoderFallback__DecoderFallback(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetEncoding__String__EncoderFallback__DecoderFallback_m602163301 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetEncoding__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetEncoding__Int32_m697986468 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetEncoding__String(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetEncoding__String_m1658831485 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_EncodingGenerated::Encoding_GetEncodings(JSVCall,System.Int32)
extern "C"  bool System_Text_EncodingGenerated_Encoding_GetEncodings_m3872652681 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::__Register()
extern "C"  void System_Text_EncodingGenerated___Register_m2082720662 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System_Text_EncodingGenerated::<Encoding_GetByteCount__Char_Array__Int32__Int32>m__D4()
extern "C"  CharU5BU5D_t3324145743* System_Text_EncodingGenerated_U3CEncoding_GetByteCount__Char_Array__Int32__Int32U3Em__D4_m3606533447 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System_Text_EncodingGenerated::<Encoding_GetByteCount__Char_Array>m__D5()
extern "C"  CharU5BU5D_t3324145743* System_Text_EncodingGenerated_U3CEncoding_GetByteCount__Char_ArrayU3Em__D5_m2473540872 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_EncodingGenerated::<Encoding_GetBytes__String__Int32__Int32__Byte_Array__Int32>m__D6()
extern "C"  ByteU5BU5D_t4260760469* System_Text_EncodingGenerated_U3CEncoding_GetBytes__String__Int32__Int32__Byte_Array__Int32U3Em__D6_m2573694736 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System_Text_EncodingGenerated::<Encoding_GetBytes__Char_Array__Int32__Int32__Byte_Array__Int32>m__D7()
extern "C"  CharU5BU5D_t3324145743* System_Text_EncodingGenerated_U3CEncoding_GetBytes__Char_Array__Int32__Int32__Byte_Array__Int32U3Em__D7_m1398055332 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_EncodingGenerated::<Encoding_GetBytes__Char_Array__Int32__Int32__Byte_Array__Int32>m__D8()
extern "C"  ByteU5BU5D_t4260760469* System_Text_EncodingGenerated_U3CEncoding_GetBytes__Char_Array__Int32__Int32__Byte_Array__Int32U3Em__D8_m3208837107 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System_Text_EncodingGenerated::<Encoding_GetBytes__Char_Array__Int32__Int32>m__D9()
extern "C"  CharU5BU5D_t3324145743* System_Text_EncodingGenerated_U3CEncoding_GetBytes__Char_Array__Int32__Int32U3Em__D9_m2058766992 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System_Text_EncodingGenerated::<Encoding_GetBytes__Char_Array>m__DA()
extern "C"  CharU5BU5D_t3324145743* System_Text_EncodingGenerated_U3CEncoding_GetBytes__Char_ArrayU3Em__DA_m2755347352 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_EncodingGenerated::<Encoding_GetCharCount__Byte_Array__Int32__Int32>m__DB()
extern "C"  ByteU5BU5D_t4260760469* System_Text_EncodingGenerated_U3CEncoding_GetCharCount__Byte_Array__Int32__Int32U3Em__DB_m1518226859 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_EncodingGenerated::<Encoding_GetCharCount__Byte_Array>m__DC()
extern "C"  ByteU5BU5D_t4260760469* System_Text_EncodingGenerated_U3CEncoding_GetCharCount__Byte_ArrayU3Em__DC_m3332899820 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_EncodingGenerated::<Encoding_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32>m__DD()
extern "C"  ByteU5BU5D_t4260760469* System_Text_EncodingGenerated_U3CEncoding_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32U3Em__DD_m3144866637 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System_Text_EncodingGenerated::<Encoding_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32>m__DE()
extern "C"  CharU5BU5D_t3324145743* System_Text_EncodingGenerated_U3CEncoding_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32U3Em__DE_m1334086784 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_EncodingGenerated::<Encoding_GetChars__Byte_Array__Int32__Int32>m__DF()
extern "C"  ByteU5BU5D_t4260760469* System_Text_EncodingGenerated_U3CEncoding_GetChars__Byte_Array__Int32__Int32U3Em__DF_m4286411059 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_EncodingGenerated::<Encoding_GetChars__Byte_Array>m__E0()
extern "C"  ByteU5BU5D_t4260760469* System_Text_EncodingGenerated_U3CEncoding_GetChars__Byte_ArrayU3Em__E0_m2333326460 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_EncodingGenerated::<Encoding_GetString__Byte_Array__Int32__Int32>m__E1()
extern "C"  ByteU5BU5D_t4260760469* System_Text_EncodingGenerated_U3CEncoding_GetString__Byte_Array__Int32__Int32U3Em__E1_m2677371437 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_EncodingGenerated::<Encoding_GetString__Byte_Array>m__E2()
extern "C"  ByteU5BU5D_t4260760469* System_Text_EncodingGenerated_U3CEncoding_GetString__Byte_ArrayU3Em__E2_m3480183662 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_EncodingGenerated::<Encoding_Convert__Encoding__Encoding__Byte_Array__Int32__Int32>m__E3()
extern "C"  ByteU5BU5D_t4260760469* System_Text_EncodingGenerated_U3CEncoding_Convert__Encoding__Encoding__Byte_Array__Int32__Int32U3Em__E3_m603997505 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_EncodingGenerated::<Encoding_Convert__Encoding__Encoding__Byte_Array>m__E4()
extern "C"  ByteU5BU5D_t4260760469* System_Text_EncodingGenerated_U3CEncoding_Convert__Encoding__Encoding__Byte_ArrayU3Em__E4_m3790208514 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Text_EncodingGenerated::ilo_getObject1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_Text_EncodingGenerated_ilo_getObject1_m3107497396 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::ilo_setBooleanS2(System.Int32,System.Boolean)
extern "C"  void System_Text_EncodingGenerated_ilo_setBooleanS2_m2905281825 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void System_Text_EncodingGenerated_ilo_setInt323_m4228691171 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_EncodingGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_Text_EncodingGenerated_ilo_setObject4_m3217534400 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_EncodingGenerated::ilo_getInt325(System.Int32)
extern "C"  int32_t System_Text_EncodingGenerated_ilo_getInt325_m3463298680 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System_Text_EncodingGenerated::ilo_getStringS6(System.Int32)
extern "C"  String_t* System_Text_EncodingGenerated_ilo_getStringS6_m1484218682 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::ilo_setByte7(System.Int32,System.Byte)
extern "C"  void System_Text_EncodingGenerated_ilo_setByte7_m376531785 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint8_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::ilo_setArrayS8(System.Int32,System.Int32,System.Boolean)
extern "C"  void System_Text_EncodingGenerated_ilo_setArrayS8_m527068313 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::ilo_moveSaveID2Arr9(System.Int32)
extern "C"  void System_Text_EncodingGenerated_ilo_moveSaveID2Arr9_m63641774 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_EncodingGenerated::ilo_setStringS10(System.Int32,System.String)
extern "C"  void System_Text_EncodingGenerated_ilo_setStringS10_m3544523442 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_EncodingGenerated::ilo_getEnum11(System.Int32)
extern "C"  int32_t System_Text_EncodingGenerated_ilo_getEnum11_m4059368562 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_EncodingGenerated::ilo_getElement12(System.Int32,System.Int32)
extern "C"  int32_t System_Text_EncodingGenerated_ilo_getElement12_m114601351 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_EncodingGenerated::ilo_getArrayLength13(System.Int32)
extern "C"  int32_t System_Text_EncodingGenerated_ilo_getArrayLength13_m1041821876 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_EncodingGenerated::ilo_getObject14(System.Int32)
extern "C"  int32_t System_Text_EncodingGenerated_ilo_getObject14_m3345447347 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System_Text_EncodingGenerated::ilo_getByte15(System.Int32)
extern "C"  uint8_t System_Text_EncodingGenerated_ilo_getByte15_m634681083 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System_Text_EncodingGenerated::ilo_getChar16(System.Int32)
extern "C"  int16_t System_Text_EncodingGenerated_ilo_getChar16_m3976970374 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
