﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Poly2Tri.DTSweepContext
struct DTSweepContext_t543731303;
// Pathfinding.Poly2Tri.AdvancingFrontNode
struct AdvancingFrontNode_t688967424;
// Pathfinding.Poly2Tri.TriangulationPoint
struct TriangulationPoint_t3810082933;
// Pathfinding.Poly2Tri.DTSweepConstraint
struct DTSweepConstraint_t3360279023;
// Pathfinding.Poly2Tri.DelaunayTriangle
struct DelaunayTriangle_t2835103587;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_DTSweepCo543731303.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Advancing688967424.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3810082933.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_DTSweepC3360279023.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Delaunay2835103587.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Orientat3899194662.h"

// System.Void Pathfinding.Poly2Tri.DTSweep::Triangulate(Pathfinding.Poly2Tri.DTSweepContext)
extern "C"  void DTSweep_Triangulate_m2688155415 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::Sweep(Pathfinding.Poly2Tri.DTSweepContext)
extern "C"  void DTSweep_Sweep_m212349143 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::FinalizationConvexHull(Pathfinding.Poly2Tri.DTSweepContext)
extern "C"  void DTSweep_FinalizationConvexHull_m4213943769 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::TurnAdvancingFrontConvex(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.AdvancingFrontNode,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweep_TurnAdvancingFrontConvex_m2832606575 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, AdvancingFrontNode_t688967424 * ___b1, AdvancingFrontNode_t688967424 * ___c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::FinalizationPolygon(Pathfinding.Poly2Tri.DTSweepContext)
extern "C"  void DTSweep_FinalizationPolygon_m2007995799 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.DTSweep::PointEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  AdvancingFrontNode_t688967424 * DTSweep_PointEvent_m1518293845 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, TriangulationPoint_t3810082933 * ___point1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.DTSweep::NewFrontTriangle(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  AdvancingFrontNode_t688967424 * DTSweep_NewFrontTriangle_m128967791 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, TriangulationPoint_t3810082933 * ___point1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::EdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DTSweepConstraint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweep_EdgeEvent_m1794910471 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DTSweepConstraint_t3360279023 * ___edge1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::FillEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DTSweepConstraint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweep_FillEdgeEvent_m3097164868 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DTSweepConstraint_t3360279023 * ___edge1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::FillRightConcaveEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DTSweepConstraint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweep_FillRightConcaveEdgeEvent_m868064561 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DTSweepConstraint_t3360279023 * ___edge1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::FillRightConvexEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DTSweepConstraint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweep_FillRightConvexEdgeEvent_m226090063 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DTSweepConstraint_t3360279023 * ___edge1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::FillRightBelowEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DTSweepConstraint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweep_FillRightBelowEdgeEvent_m2212243855 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DTSweepConstraint_t3360279023 * ___edge1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::FillRightAboveEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DTSweepConstraint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweep_FillRightAboveEdgeEvent_m470772131 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DTSweepConstraint_t3360279023 * ___edge1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::FillLeftConvexEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DTSweepConstraint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweep_FillLeftConvexEdgeEvent_m2487214550 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DTSweepConstraint_t3360279023 * ___edge1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::FillLeftConcaveEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DTSweepConstraint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweep_FillLeftConcaveEdgeEvent_m2243446922 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DTSweepConstraint_t3360279023 * ___edge1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::FillLeftBelowEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DTSweepConstraint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweep_FillLeftBelowEdgeEvent_m3809204008 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DTSweepConstraint_t3360279023 * ___edge1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::FillLeftAboveEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DTSweepConstraint,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweep_FillLeftAboveEdgeEvent_m2067732284 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DTSweepConstraint_t3360279023 * ___edge1, AdvancingFrontNode_t688967424 * ___node2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Poly2Tri.DTSweep::IsEdgeSideOfTriangle(Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  bool DTSweep_IsEdgeSideOfTriangle_m3418112580 (Il2CppObject * __this /* static, unused */, DelaunayTriangle_t2835103587 * ___triangle0, TriangulationPoint_t3810082933 * ___ep1, TriangulationPoint_t3810082933 * ___eq2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::EdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  void DTSweep_EdgeEvent_m1498452168 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, TriangulationPoint_t3810082933 * ___ep1, TriangulationPoint_t3810082933 * ___eq2, DelaunayTriangle_t2835103587 * ___triangle3, TriangulationPoint_t3810082933 * ___point4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::FlipEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  void DTSweep_FlipEdgeEvent_m1349562325 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, TriangulationPoint_t3810082933 * ___ep1, TriangulationPoint_t3810082933 * ___eq2, DelaunayTriangle_t2835103587 * ___t3, TriangulationPoint_t3810082933 * ___p4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.TriangulationPoint Pathfinding.Poly2Tri.DTSweep::NextFlipPoint(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  TriangulationPoint_t3810082933 * DTSweep_NextFlipPoint_m2493387054 (Il2CppObject * __this /* static, unused */, TriangulationPoint_t3810082933 * ___ep0, TriangulationPoint_t3810082933 * ___eq1, DelaunayTriangle_t2835103587 * ___ot2, TriangulationPoint_t3810082933 * ___op3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.DelaunayTriangle Pathfinding.Poly2Tri.DTSweep::NextFlipTriangle(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.Orientation,Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  DelaunayTriangle_t2835103587 * DTSweep_NextFlipTriangle_m3763013819 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, int32_t ___o1, DelaunayTriangle_t2835103587 * ___t2, DelaunayTriangle_t2835103587 * ___ot3, TriangulationPoint_t3810082933 * ___p4, TriangulationPoint_t3810082933 * ___op5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::FlipScanEdgeEvent(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  void DTSweep_FlipScanEdgeEvent_m1758040592 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, TriangulationPoint_t3810082933 * ___ep1, TriangulationPoint_t3810082933 * ___eq2, DelaunayTriangle_t2835103587 * ___flipTriangle3, DelaunayTriangle_t2835103587 * ___t4, TriangulationPoint_t3810082933 * ___p5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::FillAdvancingFront(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweep_FillAdvancingFront_m2175963311 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, AdvancingFrontNode_t688967424 * ___n1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::FillBasin(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweep_FillBasin_m2083514350 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, AdvancingFrontNode_t688967424 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::FillBasinReq(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweep_FillBasinReq_m472506964 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, AdvancingFrontNode_t688967424 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Poly2Tri.DTSweep::IsShallow(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  bool DTSweep_IsShallow_m3574832752 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, AdvancingFrontNode_t688967424 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Pathfinding.Poly2Tri.DTSweep::HoleAngle(Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  double DTSweep_HoleAngle_m3245728986 (Il2CppObject * __this /* static, unused */, AdvancingFrontNode_t688967424 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Pathfinding.Poly2Tri.DTSweep::BasinAngle(Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  double DTSweep_BasinAngle_m1054115749 (Il2CppObject * __this /* static, unused */, AdvancingFrontNode_t688967424 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::Fill(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.AdvancingFrontNode)
extern "C"  void DTSweep_Fill_m2383500537 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, AdvancingFrontNode_t688967424 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Poly2Tri.DTSweep::Legalize(Pathfinding.Poly2Tri.DTSweepContext,Pathfinding.Poly2Tri.DelaunayTriangle)
extern "C"  bool DTSweep_Legalize_m2173221096 (Il2CppObject * __this /* static, unused */, DTSweepContext_t543731303 * ___tcx0, DelaunayTriangle_t2835103587 * ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.Poly2Tri.DTSweep::RotateTrianglePair(Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.DelaunayTriangle,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  void DTSweep_RotateTrianglePair_m2948867544 (Il2CppObject * __this /* static, unused */, DelaunayTriangle_t2835103587 * ___t0, TriangulationPoint_t3810082933 * ___p1, DelaunayTriangle_t2835103587 * ___ot2, TriangulationPoint_t3810082933 * ___op3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
