﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_sischo139
struct  M_sischo139_t30009290  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_sischo139::_sistiSaircarru
	String_t* ____sistiSaircarru_0;
	// System.Int32 GarbageiOS.M_sischo139::_karloo
	int32_t ____karloo_1;
	// System.String GarbageiOS.M_sischo139::_fasu
	String_t* ____fasu_2;
	// System.UInt32 GarbageiOS.M_sischo139::_soumeKawhairmi
	uint32_t ____soumeKawhairmi_3;
	// System.Single GarbageiOS.M_sischo139::_weasawnoKoolere
	float ____weasawnoKoolere_4;
	// System.Boolean GarbageiOS.M_sischo139::_koucaSearnouka
	bool ____koucaSearnouka_5;
	// System.String GarbageiOS.M_sischo139::_camosaw
	String_t* ____camosaw_6;
	// System.Boolean GarbageiOS.M_sischo139::_jereToukiwe
	bool ____jereToukiwe_7;
	// System.String GarbageiOS.M_sischo139::_ratetalWhewe
	String_t* ____ratetalWhewe_8;
	// System.Int32 GarbageiOS.M_sischo139::_zemjall
	int32_t ____zemjall_9;
	// System.Int32 GarbageiOS.M_sischo139::_noudroRewurmea
	int32_t ____noudroRewurmea_10;
	// System.Single GarbageiOS.M_sischo139::_jurnar
	float ____jurnar_11;
	// System.Single GarbageiOS.M_sischo139::_wapere
	float ____wapere_12;
	// System.Int32 GarbageiOS.M_sischo139::_lepisou
	int32_t ____lepisou_13;
	// System.Single GarbageiOS.M_sischo139::_jeemolo
	float ____jeemolo_14;
	// System.Boolean GarbageiOS.M_sischo139::_cogall
	bool ____cogall_15;
	// System.Single GarbageiOS.M_sischo139::_zoumowsaRakar
	float ____zoumowsaRakar_16;
	// System.Boolean GarbageiOS.M_sischo139::_mashapur
	bool ____mashapur_17;
	// System.UInt32 GarbageiOS.M_sischo139::_difo
	uint32_t ____difo_18;
	// System.Int32 GarbageiOS.M_sischo139::_gowrai
	int32_t ____gowrai_19;
	// System.String GarbageiOS.M_sischo139::_texasga
	String_t* ____texasga_20;
	// System.String GarbageiOS.M_sischo139::_bawtoDerecai
	String_t* ____bawtoDerecai_21;
	// System.UInt32 GarbageiOS.M_sischo139::_perrereSuseartaw
	uint32_t ____perrereSuseartaw_22;
	// System.Single GarbageiOS.M_sischo139::_wanisselRuderebo
	float ____wanisselRuderebo_23;
	// System.UInt32 GarbageiOS.M_sischo139::_xawmisarMuselhel
	uint32_t ____xawmisarMuselhel_24;
	// System.Single GarbageiOS.M_sischo139::_cusaiSastou
	float ____cusaiSastou_25;
	// System.Boolean GarbageiOS.M_sischo139::_malna
	bool ____malna_26;
	// System.Single GarbageiOS.M_sischo139::_ficormeKawisla
	float ____ficormeKawisla_27;
	// System.UInt32 GarbageiOS.M_sischo139::_whawge
	uint32_t ____whawge_28;
	// System.Single GarbageiOS.M_sischo139::_ralzissereDurteacair
	float ____ralzissereDurteacair_29;
	// System.String GarbageiOS.M_sischo139::_tairlirmowBiszi
	String_t* ____tairlirmowBiszi_30;
	// System.Boolean GarbageiOS.M_sischo139::_cerhaw
	bool ____cerhaw_31;
	// System.Int32 GarbageiOS.M_sischo139::_teremereje
	int32_t ____teremereje_32;
	// System.Boolean GarbageiOS.M_sischo139::_stirfeDayhe
	bool ____stirfeDayhe_33;
	// System.Boolean GarbageiOS.M_sischo139::_bowsture
	bool ____bowsture_34;
	// System.Single GarbageiOS.M_sischo139::_sourealeLooge
	float ____sourealeLooge_35;
	// System.UInt32 GarbageiOS.M_sischo139::_jasu
	uint32_t ____jasu_36;
	// System.Boolean GarbageiOS.M_sischo139::_makur
	bool ____makur_37;

public:
	inline static int32_t get_offset_of__sistiSaircarru_0() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____sistiSaircarru_0)); }
	inline String_t* get__sistiSaircarru_0() const { return ____sistiSaircarru_0; }
	inline String_t** get_address_of__sistiSaircarru_0() { return &____sistiSaircarru_0; }
	inline void set__sistiSaircarru_0(String_t* value)
	{
		____sistiSaircarru_0 = value;
		Il2CppCodeGenWriteBarrier(&____sistiSaircarru_0, value);
	}

	inline static int32_t get_offset_of__karloo_1() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____karloo_1)); }
	inline int32_t get__karloo_1() const { return ____karloo_1; }
	inline int32_t* get_address_of__karloo_1() { return &____karloo_1; }
	inline void set__karloo_1(int32_t value)
	{
		____karloo_1 = value;
	}

	inline static int32_t get_offset_of__fasu_2() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____fasu_2)); }
	inline String_t* get__fasu_2() const { return ____fasu_2; }
	inline String_t** get_address_of__fasu_2() { return &____fasu_2; }
	inline void set__fasu_2(String_t* value)
	{
		____fasu_2 = value;
		Il2CppCodeGenWriteBarrier(&____fasu_2, value);
	}

	inline static int32_t get_offset_of__soumeKawhairmi_3() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____soumeKawhairmi_3)); }
	inline uint32_t get__soumeKawhairmi_3() const { return ____soumeKawhairmi_3; }
	inline uint32_t* get_address_of__soumeKawhairmi_3() { return &____soumeKawhairmi_3; }
	inline void set__soumeKawhairmi_3(uint32_t value)
	{
		____soumeKawhairmi_3 = value;
	}

	inline static int32_t get_offset_of__weasawnoKoolere_4() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____weasawnoKoolere_4)); }
	inline float get__weasawnoKoolere_4() const { return ____weasawnoKoolere_4; }
	inline float* get_address_of__weasawnoKoolere_4() { return &____weasawnoKoolere_4; }
	inline void set__weasawnoKoolere_4(float value)
	{
		____weasawnoKoolere_4 = value;
	}

	inline static int32_t get_offset_of__koucaSearnouka_5() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____koucaSearnouka_5)); }
	inline bool get__koucaSearnouka_5() const { return ____koucaSearnouka_5; }
	inline bool* get_address_of__koucaSearnouka_5() { return &____koucaSearnouka_5; }
	inline void set__koucaSearnouka_5(bool value)
	{
		____koucaSearnouka_5 = value;
	}

	inline static int32_t get_offset_of__camosaw_6() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____camosaw_6)); }
	inline String_t* get__camosaw_6() const { return ____camosaw_6; }
	inline String_t** get_address_of__camosaw_6() { return &____camosaw_6; }
	inline void set__camosaw_6(String_t* value)
	{
		____camosaw_6 = value;
		Il2CppCodeGenWriteBarrier(&____camosaw_6, value);
	}

	inline static int32_t get_offset_of__jereToukiwe_7() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____jereToukiwe_7)); }
	inline bool get__jereToukiwe_7() const { return ____jereToukiwe_7; }
	inline bool* get_address_of__jereToukiwe_7() { return &____jereToukiwe_7; }
	inline void set__jereToukiwe_7(bool value)
	{
		____jereToukiwe_7 = value;
	}

	inline static int32_t get_offset_of__ratetalWhewe_8() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____ratetalWhewe_8)); }
	inline String_t* get__ratetalWhewe_8() const { return ____ratetalWhewe_8; }
	inline String_t** get_address_of__ratetalWhewe_8() { return &____ratetalWhewe_8; }
	inline void set__ratetalWhewe_8(String_t* value)
	{
		____ratetalWhewe_8 = value;
		Il2CppCodeGenWriteBarrier(&____ratetalWhewe_8, value);
	}

	inline static int32_t get_offset_of__zemjall_9() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____zemjall_9)); }
	inline int32_t get__zemjall_9() const { return ____zemjall_9; }
	inline int32_t* get_address_of__zemjall_9() { return &____zemjall_9; }
	inline void set__zemjall_9(int32_t value)
	{
		____zemjall_9 = value;
	}

	inline static int32_t get_offset_of__noudroRewurmea_10() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____noudroRewurmea_10)); }
	inline int32_t get__noudroRewurmea_10() const { return ____noudroRewurmea_10; }
	inline int32_t* get_address_of__noudroRewurmea_10() { return &____noudroRewurmea_10; }
	inline void set__noudroRewurmea_10(int32_t value)
	{
		____noudroRewurmea_10 = value;
	}

	inline static int32_t get_offset_of__jurnar_11() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____jurnar_11)); }
	inline float get__jurnar_11() const { return ____jurnar_11; }
	inline float* get_address_of__jurnar_11() { return &____jurnar_11; }
	inline void set__jurnar_11(float value)
	{
		____jurnar_11 = value;
	}

	inline static int32_t get_offset_of__wapere_12() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____wapere_12)); }
	inline float get__wapere_12() const { return ____wapere_12; }
	inline float* get_address_of__wapere_12() { return &____wapere_12; }
	inline void set__wapere_12(float value)
	{
		____wapere_12 = value;
	}

	inline static int32_t get_offset_of__lepisou_13() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____lepisou_13)); }
	inline int32_t get__lepisou_13() const { return ____lepisou_13; }
	inline int32_t* get_address_of__lepisou_13() { return &____lepisou_13; }
	inline void set__lepisou_13(int32_t value)
	{
		____lepisou_13 = value;
	}

	inline static int32_t get_offset_of__jeemolo_14() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____jeemolo_14)); }
	inline float get__jeemolo_14() const { return ____jeemolo_14; }
	inline float* get_address_of__jeemolo_14() { return &____jeemolo_14; }
	inline void set__jeemolo_14(float value)
	{
		____jeemolo_14 = value;
	}

	inline static int32_t get_offset_of__cogall_15() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____cogall_15)); }
	inline bool get__cogall_15() const { return ____cogall_15; }
	inline bool* get_address_of__cogall_15() { return &____cogall_15; }
	inline void set__cogall_15(bool value)
	{
		____cogall_15 = value;
	}

	inline static int32_t get_offset_of__zoumowsaRakar_16() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____zoumowsaRakar_16)); }
	inline float get__zoumowsaRakar_16() const { return ____zoumowsaRakar_16; }
	inline float* get_address_of__zoumowsaRakar_16() { return &____zoumowsaRakar_16; }
	inline void set__zoumowsaRakar_16(float value)
	{
		____zoumowsaRakar_16 = value;
	}

	inline static int32_t get_offset_of__mashapur_17() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____mashapur_17)); }
	inline bool get__mashapur_17() const { return ____mashapur_17; }
	inline bool* get_address_of__mashapur_17() { return &____mashapur_17; }
	inline void set__mashapur_17(bool value)
	{
		____mashapur_17 = value;
	}

	inline static int32_t get_offset_of__difo_18() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____difo_18)); }
	inline uint32_t get__difo_18() const { return ____difo_18; }
	inline uint32_t* get_address_of__difo_18() { return &____difo_18; }
	inline void set__difo_18(uint32_t value)
	{
		____difo_18 = value;
	}

	inline static int32_t get_offset_of__gowrai_19() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____gowrai_19)); }
	inline int32_t get__gowrai_19() const { return ____gowrai_19; }
	inline int32_t* get_address_of__gowrai_19() { return &____gowrai_19; }
	inline void set__gowrai_19(int32_t value)
	{
		____gowrai_19 = value;
	}

	inline static int32_t get_offset_of__texasga_20() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____texasga_20)); }
	inline String_t* get__texasga_20() const { return ____texasga_20; }
	inline String_t** get_address_of__texasga_20() { return &____texasga_20; }
	inline void set__texasga_20(String_t* value)
	{
		____texasga_20 = value;
		Il2CppCodeGenWriteBarrier(&____texasga_20, value);
	}

	inline static int32_t get_offset_of__bawtoDerecai_21() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____bawtoDerecai_21)); }
	inline String_t* get__bawtoDerecai_21() const { return ____bawtoDerecai_21; }
	inline String_t** get_address_of__bawtoDerecai_21() { return &____bawtoDerecai_21; }
	inline void set__bawtoDerecai_21(String_t* value)
	{
		____bawtoDerecai_21 = value;
		Il2CppCodeGenWriteBarrier(&____bawtoDerecai_21, value);
	}

	inline static int32_t get_offset_of__perrereSuseartaw_22() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____perrereSuseartaw_22)); }
	inline uint32_t get__perrereSuseartaw_22() const { return ____perrereSuseartaw_22; }
	inline uint32_t* get_address_of__perrereSuseartaw_22() { return &____perrereSuseartaw_22; }
	inline void set__perrereSuseartaw_22(uint32_t value)
	{
		____perrereSuseartaw_22 = value;
	}

	inline static int32_t get_offset_of__wanisselRuderebo_23() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____wanisselRuderebo_23)); }
	inline float get__wanisselRuderebo_23() const { return ____wanisselRuderebo_23; }
	inline float* get_address_of__wanisselRuderebo_23() { return &____wanisselRuderebo_23; }
	inline void set__wanisselRuderebo_23(float value)
	{
		____wanisselRuderebo_23 = value;
	}

	inline static int32_t get_offset_of__xawmisarMuselhel_24() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____xawmisarMuselhel_24)); }
	inline uint32_t get__xawmisarMuselhel_24() const { return ____xawmisarMuselhel_24; }
	inline uint32_t* get_address_of__xawmisarMuselhel_24() { return &____xawmisarMuselhel_24; }
	inline void set__xawmisarMuselhel_24(uint32_t value)
	{
		____xawmisarMuselhel_24 = value;
	}

	inline static int32_t get_offset_of__cusaiSastou_25() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____cusaiSastou_25)); }
	inline float get__cusaiSastou_25() const { return ____cusaiSastou_25; }
	inline float* get_address_of__cusaiSastou_25() { return &____cusaiSastou_25; }
	inline void set__cusaiSastou_25(float value)
	{
		____cusaiSastou_25 = value;
	}

	inline static int32_t get_offset_of__malna_26() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____malna_26)); }
	inline bool get__malna_26() const { return ____malna_26; }
	inline bool* get_address_of__malna_26() { return &____malna_26; }
	inline void set__malna_26(bool value)
	{
		____malna_26 = value;
	}

	inline static int32_t get_offset_of__ficormeKawisla_27() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____ficormeKawisla_27)); }
	inline float get__ficormeKawisla_27() const { return ____ficormeKawisla_27; }
	inline float* get_address_of__ficormeKawisla_27() { return &____ficormeKawisla_27; }
	inline void set__ficormeKawisla_27(float value)
	{
		____ficormeKawisla_27 = value;
	}

	inline static int32_t get_offset_of__whawge_28() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____whawge_28)); }
	inline uint32_t get__whawge_28() const { return ____whawge_28; }
	inline uint32_t* get_address_of__whawge_28() { return &____whawge_28; }
	inline void set__whawge_28(uint32_t value)
	{
		____whawge_28 = value;
	}

	inline static int32_t get_offset_of__ralzissereDurteacair_29() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____ralzissereDurteacair_29)); }
	inline float get__ralzissereDurteacair_29() const { return ____ralzissereDurteacair_29; }
	inline float* get_address_of__ralzissereDurteacair_29() { return &____ralzissereDurteacair_29; }
	inline void set__ralzissereDurteacair_29(float value)
	{
		____ralzissereDurteacair_29 = value;
	}

	inline static int32_t get_offset_of__tairlirmowBiszi_30() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____tairlirmowBiszi_30)); }
	inline String_t* get__tairlirmowBiszi_30() const { return ____tairlirmowBiszi_30; }
	inline String_t** get_address_of__tairlirmowBiszi_30() { return &____tairlirmowBiszi_30; }
	inline void set__tairlirmowBiszi_30(String_t* value)
	{
		____tairlirmowBiszi_30 = value;
		Il2CppCodeGenWriteBarrier(&____tairlirmowBiszi_30, value);
	}

	inline static int32_t get_offset_of__cerhaw_31() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____cerhaw_31)); }
	inline bool get__cerhaw_31() const { return ____cerhaw_31; }
	inline bool* get_address_of__cerhaw_31() { return &____cerhaw_31; }
	inline void set__cerhaw_31(bool value)
	{
		____cerhaw_31 = value;
	}

	inline static int32_t get_offset_of__teremereje_32() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____teremereje_32)); }
	inline int32_t get__teremereje_32() const { return ____teremereje_32; }
	inline int32_t* get_address_of__teremereje_32() { return &____teremereje_32; }
	inline void set__teremereje_32(int32_t value)
	{
		____teremereje_32 = value;
	}

	inline static int32_t get_offset_of__stirfeDayhe_33() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____stirfeDayhe_33)); }
	inline bool get__stirfeDayhe_33() const { return ____stirfeDayhe_33; }
	inline bool* get_address_of__stirfeDayhe_33() { return &____stirfeDayhe_33; }
	inline void set__stirfeDayhe_33(bool value)
	{
		____stirfeDayhe_33 = value;
	}

	inline static int32_t get_offset_of__bowsture_34() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____bowsture_34)); }
	inline bool get__bowsture_34() const { return ____bowsture_34; }
	inline bool* get_address_of__bowsture_34() { return &____bowsture_34; }
	inline void set__bowsture_34(bool value)
	{
		____bowsture_34 = value;
	}

	inline static int32_t get_offset_of__sourealeLooge_35() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____sourealeLooge_35)); }
	inline float get__sourealeLooge_35() const { return ____sourealeLooge_35; }
	inline float* get_address_of__sourealeLooge_35() { return &____sourealeLooge_35; }
	inline void set__sourealeLooge_35(float value)
	{
		____sourealeLooge_35 = value;
	}

	inline static int32_t get_offset_of__jasu_36() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____jasu_36)); }
	inline uint32_t get__jasu_36() const { return ____jasu_36; }
	inline uint32_t* get_address_of__jasu_36() { return &____jasu_36; }
	inline void set__jasu_36(uint32_t value)
	{
		____jasu_36 = value;
	}

	inline static int32_t get_offset_of__makur_37() { return static_cast<int32_t>(offsetof(M_sischo139_t30009290, ____makur_37)); }
	inline bool get__makur_37() const { return ____makur_37; }
	inline bool* get_address_of__makur_37() { return &____makur_37; }
	inline void set__makur_37(bool value)
	{
		____makur_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
