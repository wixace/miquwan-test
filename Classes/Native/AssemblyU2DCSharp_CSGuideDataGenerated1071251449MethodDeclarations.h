﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSGuideDataGenerated
struct CSGuideDataGenerated_t1071251449;
// JSVCall
struct JSVCall_t3708497963;
// CSGuideVO[]
struct CSGuideVOU5BU5D_t2582593960;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void CSGuideDataGenerated::.ctor()
extern "C"  void CSGuideDataGenerated__ctor_m1586250434 (CSGuideDataGenerated_t1071251449 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSGuideDataGenerated::CSGuideData_CSGuideData1(JSVCall,System.Int32)
extern "C"  bool CSGuideDataGenerated_CSGuideData_CSGuideData1_m1609329418 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGuideDataGenerated::CSGuideData_guideList(JSVCall)
extern "C"  void CSGuideDataGenerated_CSGuideData_guideList_m2117888982 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGuideDataGenerated::CSGuideData_finishList(JSVCall)
extern "C"  void CSGuideDataGenerated_CSGuideData_finishList_m2775163723 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSGuideDataGenerated::CSGuideData_AssinGuideVOListByNpcId(JSVCall,System.Int32)
extern "C"  bool CSGuideDataGenerated_CSGuideData_AssinGuideVOListByNpcId_m1663483915 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSGuideDataGenerated::CSGuideData_CanTriggerNpcHpGuide__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool CSGuideDataGenerated_CSGuideData_CanTriggerNpcHpGuide__Int32__Int32_m3576718218 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSGuideDataGenerated::CSGuideData_LoadData__String(JSVCall,System.Int32)
extern "C"  bool CSGuideDataGenerated_CSGuideData_LoadData__String_m1548145840 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSGuideDataGenerated::CSGuideData_UnLoadData(JSVCall,System.Int32)
extern "C"  bool CSGuideDataGenerated_CSGuideData_UnLoadData_m4103438040 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGuideDataGenerated::__Register()
extern "C"  void CSGuideDataGenerated___Register_m695903045 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSGuideVO[] CSGuideDataGenerated::<CSGuideData_guideList>m__23()
extern "C"  CSGuideVOU5BU5D_t2582593960* CSGuideDataGenerated_U3CCSGuideData_guideListU3Em__23_m3166535955 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSGuideDataGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool CSGuideDataGenerated_ilo_attachFinalizerObject1_m801299293 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGuideDataGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void CSGuideDataGenerated_ilo_addJSCSRel2_m2837513471 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSGuideDataGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t CSGuideDataGenerated_ilo_setObject3_m448510602 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CSGuideDataGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * CSGuideDataGenerated_ilo_getObject4_m1067056752 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSGuideDataGenerated::ilo_getInt325(System.Int32)
extern "C"  int32_t CSGuideDataGenerated_ilo_getInt325_m984814093 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSGuideDataGenerated::ilo_getObject6(System.Int32)
extern "C"  int32_t CSGuideDataGenerated_ilo_getObject6_m1903240661 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSGuideDataGenerated::ilo_getArrayLength7(System.Int32)
extern "C"  int32_t CSGuideDataGenerated_ilo_getArrayLength7_m3828608862 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
