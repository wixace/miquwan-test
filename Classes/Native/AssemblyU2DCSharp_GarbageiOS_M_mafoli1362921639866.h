﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_mafoli136
struct  M_mafoli136_t2921639866  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_mafoli136::_roulornas
	float ____roulornas_0;
	// System.Single GarbageiOS.M_mafoli136::_kouperSorcirsa
	float ____kouperSorcirsa_1;
	// System.Boolean GarbageiOS.M_mafoli136::_raysimas
	bool ____raysimas_2;
	// System.String GarbageiOS.M_mafoli136::_pisjuMure
	String_t* ____pisjuMure_3;
	// System.String GarbageiOS.M_mafoli136::_baballJegemem
	String_t* ____baballJegemem_4;
	// System.Int32 GarbageiOS.M_mafoli136::_kipowBeko
	int32_t ____kipowBeko_5;
	// System.UInt32 GarbageiOS.M_mafoli136::_towzerecu
	uint32_t ____towzerecu_6;
	// System.Boolean GarbageiOS.M_mafoli136::_tuheevi
	bool ____tuheevi_7;
	// System.Int32 GarbageiOS.M_mafoli136::_dearra
	int32_t ____dearra_8;
	// System.String GarbageiOS.M_mafoli136::_wobeabe
	String_t* ____wobeabe_9;
	// System.Int32 GarbageiOS.M_mafoli136::_yeesaJacurqa
	int32_t ____yeesaJacurqa_10;
	// System.Int32 GarbageiOS.M_mafoli136::_nebaheGeqer
	int32_t ____nebaheGeqer_11;
	// System.Int32 GarbageiOS.M_mafoli136::_nurzall
	int32_t ____nurzall_12;
	// System.Boolean GarbageiOS.M_mafoli136::_cairwallSemra
	bool ____cairwallSemra_13;
	// System.Int32 GarbageiOS.M_mafoli136::_lamirTousairwho
	int32_t ____lamirTousairwho_14;
	// System.Int32 GarbageiOS.M_mafoli136::_soupaw
	int32_t ____soupaw_15;
	// System.String GarbageiOS.M_mafoli136::_soniSilir
	String_t* ____soniSilir_16;
	// System.Int32 GarbageiOS.M_mafoli136::_gohu
	int32_t ____gohu_17;
	// System.Boolean GarbageiOS.M_mafoli136::_turhalce
	bool ____turhalce_18;
	// System.UInt32 GarbageiOS.M_mafoli136::_pastairJirarpas
	uint32_t ____pastairJirarpas_19;
	// System.UInt32 GarbageiOS.M_mafoli136::_jorlabou
	uint32_t ____jorlabou_20;
	// System.Int32 GarbageiOS.M_mafoli136::_vigeCallmay
	int32_t ____vigeCallmay_21;

public:
	inline static int32_t get_offset_of__roulornas_0() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____roulornas_0)); }
	inline float get__roulornas_0() const { return ____roulornas_0; }
	inline float* get_address_of__roulornas_0() { return &____roulornas_0; }
	inline void set__roulornas_0(float value)
	{
		____roulornas_0 = value;
	}

	inline static int32_t get_offset_of__kouperSorcirsa_1() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____kouperSorcirsa_1)); }
	inline float get__kouperSorcirsa_1() const { return ____kouperSorcirsa_1; }
	inline float* get_address_of__kouperSorcirsa_1() { return &____kouperSorcirsa_1; }
	inline void set__kouperSorcirsa_1(float value)
	{
		____kouperSorcirsa_1 = value;
	}

	inline static int32_t get_offset_of__raysimas_2() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____raysimas_2)); }
	inline bool get__raysimas_2() const { return ____raysimas_2; }
	inline bool* get_address_of__raysimas_2() { return &____raysimas_2; }
	inline void set__raysimas_2(bool value)
	{
		____raysimas_2 = value;
	}

	inline static int32_t get_offset_of__pisjuMure_3() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____pisjuMure_3)); }
	inline String_t* get__pisjuMure_3() const { return ____pisjuMure_3; }
	inline String_t** get_address_of__pisjuMure_3() { return &____pisjuMure_3; }
	inline void set__pisjuMure_3(String_t* value)
	{
		____pisjuMure_3 = value;
		Il2CppCodeGenWriteBarrier(&____pisjuMure_3, value);
	}

	inline static int32_t get_offset_of__baballJegemem_4() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____baballJegemem_4)); }
	inline String_t* get__baballJegemem_4() const { return ____baballJegemem_4; }
	inline String_t** get_address_of__baballJegemem_4() { return &____baballJegemem_4; }
	inline void set__baballJegemem_4(String_t* value)
	{
		____baballJegemem_4 = value;
		Il2CppCodeGenWriteBarrier(&____baballJegemem_4, value);
	}

	inline static int32_t get_offset_of__kipowBeko_5() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____kipowBeko_5)); }
	inline int32_t get__kipowBeko_5() const { return ____kipowBeko_5; }
	inline int32_t* get_address_of__kipowBeko_5() { return &____kipowBeko_5; }
	inline void set__kipowBeko_5(int32_t value)
	{
		____kipowBeko_5 = value;
	}

	inline static int32_t get_offset_of__towzerecu_6() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____towzerecu_6)); }
	inline uint32_t get__towzerecu_6() const { return ____towzerecu_6; }
	inline uint32_t* get_address_of__towzerecu_6() { return &____towzerecu_6; }
	inline void set__towzerecu_6(uint32_t value)
	{
		____towzerecu_6 = value;
	}

	inline static int32_t get_offset_of__tuheevi_7() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____tuheevi_7)); }
	inline bool get__tuheevi_7() const { return ____tuheevi_7; }
	inline bool* get_address_of__tuheevi_7() { return &____tuheevi_7; }
	inline void set__tuheevi_7(bool value)
	{
		____tuheevi_7 = value;
	}

	inline static int32_t get_offset_of__dearra_8() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____dearra_8)); }
	inline int32_t get__dearra_8() const { return ____dearra_8; }
	inline int32_t* get_address_of__dearra_8() { return &____dearra_8; }
	inline void set__dearra_8(int32_t value)
	{
		____dearra_8 = value;
	}

	inline static int32_t get_offset_of__wobeabe_9() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____wobeabe_9)); }
	inline String_t* get__wobeabe_9() const { return ____wobeabe_9; }
	inline String_t** get_address_of__wobeabe_9() { return &____wobeabe_9; }
	inline void set__wobeabe_9(String_t* value)
	{
		____wobeabe_9 = value;
		Il2CppCodeGenWriteBarrier(&____wobeabe_9, value);
	}

	inline static int32_t get_offset_of__yeesaJacurqa_10() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____yeesaJacurqa_10)); }
	inline int32_t get__yeesaJacurqa_10() const { return ____yeesaJacurqa_10; }
	inline int32_t* get_address_of__yeesaJacurqa_10() { return &____yeesaJacurqa_10; }
	inline void set__yeesaJacurqa_10(int32_t value)
	{
		____yeesaJacurqa_10 = value;
	}

	inline static int32_t get_offset_of__nebaheGeqer_11() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____nebaheGeqer_11)); }
	inline int32_t get__nebaheGeqer_11() const { return ____nebaheGeqer_11; }
	inline int32_t* get_address_of__nebaheGeqer_11() { return &____nebaheGeqer_11; }
	inline void set__nebaheGeqer_11(int32_t value)
	{
		____nebaheGeqer_11 = value;
	}

	inline static int32_t get_offset_of__nurzall_12() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____nurzall_12)); }
	inline int32_t get__nurzall_12() const { return ____nurzall_12; }
	inline int32_t* get_address_of__nurzall_12() { return &____nurzall_12; }
	inline void set__nurzall_12(int32_t value)
	{
		____nurzall_12 = value;
	}

	inline static int32_t get_offset_of__cairwallSemra_13() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____cairwallSemra_13)); }
	inline bool get__cairwallSemra_13() const { return ____cairwallSemra_13; }
	inline bool* get_address_of__cairwallSemra_13() { return &____cairwallSemra_13; }
	inline void set__cairwallSemra_13(bool value)
	{
		____cairwallSemra_13 = value;
	}

	inline static int32_t get_offset_of__lamirTousairwho_14() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____lamirTousairwho_14)); }
	inline int32_t get__lamirTousairwho_14() const { return ____lamirTousairwho_14; }
	inline int32_t* get_address_of__lamirTousairwho_14() { return &____lamirTousairwho_14; }
	inline void set__lamirTousairwho_14(int32_t value)
	{
		____lamirTousairwho_14 = value;
	}

	inline static int32_t get_offset_of__soupaw_15() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____soupaw_15)); }
	inline int32_t get__soupaw_15() const { return ____soupaw_15; }
	inline int32_t* get_address_of__soupaw_15() { return &____soupaw_15; }
	inline void set__soupaw_15(int32_t value)
	{
		____soupaw_15 = value;
	}

	inline static int32_t get_offset_of__soniSilir_16() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____soniSilir_16)); }
	inline String_t* get__soniSilir_16() const { return ____soniSilir_16; }
	inline String_t** get_address_of__soniSilir_16() { return &____soniSilir_16; }
	inline void set__soniSilir_16(String_t* value)
	{
		____soniSilir_16 = value;
		Il2CppCodeGenWriteBarrier(&____soniSilir_16, value);
	}

	inline static int32_t get_offset_of__gohu_17() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____gohu_17)); }
	inline int32_t get__gohu_17() const { return ____gohu_17; }
	inline int32_t* get_address_of__gohu_17() { return &____gohu_17; }
	inline void set__gohu_17(int32_t value)
	{
		____gohu_17 = value;
	}

	inline static int32_t get_offset_of__turhalce_18() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____turhalce_18)); }
	inline bool get__turhalce_18() const { return ____turhalce_18; }
	inline bool* get_address_of__turhalce_18() { return &____turhalce_18; }
	inline void set__turhalce_18(bool value)
	{
		____turhalce_18 = value;
	}

	inline static int32_t get_offset_of__pastairJirarpas_19() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____pastairJirarpas_19)); }
	inline uint32_t get__pastairJirarpas_19() const { return ____pastairJirarpas_19; }
	inline uint32_t* get_address_of__pastairJirarpas_19() { return &____pastairJirarpas_19; }
	inline void set__pastairJirarpas_19(uint32_t value)
	{
		____pastairJirarpas_19 = value;
	}

	inline static int32_t get_offset_of__jorlabou_20() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____jorlabou_20)); }
	inline uint32_t get__jorlabou_20() const { return ____jorlabou_20; }
	inline uint32_t* get_address_of__jorlabou_20() { return &____jorlabou_20; }
	inline void set__jorlabou_20(uint32_t value)
	{
		____jorlabou_20 = value;
	}

	inline static int32_t get_offset_of__vigeCallmay_21() { return static_cast<int32_t>(offsetof(M_mafoli136_t2921639866, ____vigeCallmay_21)); }
	inline int32_t get__vigeCallmay_21() const { return ____vigeCallmay_21; }
	inline int32_t* get_address_of__vigeCallmay_21() { return &____vigeCallmay_21; }
	inline void set__vigeCallmay_21(int32_t value)
	{
		____vigeCallmay_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
