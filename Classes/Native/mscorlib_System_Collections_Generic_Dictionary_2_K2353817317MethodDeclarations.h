﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>
struct Dictionary_2_t1738881263;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2353817317.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.UInt16,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2581742355_gshared (Enumerator_t2353817317 * __this, Dictionary_2_t1738881263 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m2581742355(__this, ___host0, method) ((  void (*) (Enumerator_t2353817317 *, Dictionary_2_t1738881263 *, const MethodInfo*))Enumerator__ctor_m2581742355_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.UInt16,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m2328560814_gshared (Enumerator_t2353817317 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m2328560814(__this, method) ((  Il2CppObject * (*) (Enumerator_t2353817317 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2328560814_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.UInt16,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m181282754_gshared (Enumerator_t2353817317 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m181282754(__this, method) ((  void (*) (Enumerator_t2353817317 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m181282754_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.UInt16,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m827442293_gshared (Enumerator_t2353817317 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m827442293(__this, method) ((  void (*) (Enumerator_t2353817317 *, const MethodInfo*))Enumerator_Dispose_m827442293_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.UInt16,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1429069614_gshared (Enumerator_t2353817317 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1429069614(__this, method) ((  bool (*) (Enumerator_t2353817317 *, const MethodInfo*))Enumerator_MoveNext_m1429069614_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.UInt16,System.Object>::get_Current()
extern "C"  uint16_t Enumerator_get_Current_m644043110_gshared (Enumerator_t2353817317 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m644043110(__this, method) ((  uint16_t (*) (Enumerator_t2353817317 *, const MethodInfo*))Enumerator_get_Current_m644043110_gshared)(__this, method)
