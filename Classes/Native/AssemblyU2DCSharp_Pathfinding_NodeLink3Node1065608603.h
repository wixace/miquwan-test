﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.NodeLink3
struct NodeLink3_t1645404665;

#include "AssemblyU2DCSharp_Pathfinding_PointNode2761813780.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.NodeLink3Node
struct  NodeLink3Node_t1065608603  : public PointNode_t2761813780
{
public:
	// Pathfinding.NodeLink3 Pathfinding.NodeLink3Node::link
	NodeLink3_t1645404665 * ___link_19;
	// UnityEngine.Vector3 Pathfinding.NodeLink3Node::portalA
	Vector3_t4282066566  ___portalA_20;
	// UnityEngine.Vector3 Pathfinding.NodeLink3Node::portalB
	Vector3_t4282066566  ___portalB_21;

public:
	inline static int32_t get_offset_of_link_19() { return static_cast<int32_t>(offsetof(NodeLink3Node_t1065608603, ___link_19)); }
	inline NodeLink3_t1645404665 * get_link_19() const { return ___link_19; }
	inline NodeLink3_t1645404665 ** get_address_of_link_19() { return &___link_19; }
	inline void set_link_19(NodeLink3_t1645404665 * value)
	{
		___link_19 = value;
		Il2CppCodeGenWriteBarrier(&___link_19, value);
	}

	inline static int32_t get_offset_of_portalA_20() { return static_cast<int32_t>(offsetof(NodeLink3Node_t1065608603, ___portalA_20)); }
	inline Vector3_t4282066566  get_portalA_20() const { return ___portalA_20; }
	inline Vector3_t4282066566 * get_address_of_portalA_20() { return &___portalA_20; }
	inline void set_portalA_20(Vector3_t4282066566  value)
	{
		___portalA_20 = value;
	}

	inline static int32_t get_offset_of_portalB_21() { return static_cast<int32_t>(offsetof(NodeLink3Node_t1065608603, ___portalB_21)); }
	inline Vector3_t4282066566  get_portalB_21() const { return ___portalB_21; }
	inline Vector3_t4282066566 * get_address_of_portalB_21() { return &___portalB_21; }
	inline void set_portalB_21(Vector3_t4282066566  value)
	{
		___portalB_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
