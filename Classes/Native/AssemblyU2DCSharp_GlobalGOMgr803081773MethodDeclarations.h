﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GlobalGOMgr
struct GlobalGOMgr_t803081773;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UIWidget
struct UIWidget_t769069560;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_GlobalGOMgr803081773.h"

// System.Void GlobalGOMgr::.ctor()
extern "C"  void GlobalGOMgr__ctor_m304484318 (GlobalGOMgr_t803081773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalGOMgr::get_IsClick2D()
extern "C"  bool GlobalGOMgr_get_IsClick2D_m3279432855 (GlobalGOMgr_t803081773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalGOMgr::get_IsClick3D()
extern "C"  bool GlobalGOMgr_get_IsClick3D_m3279462646 (GlobalGOMgr_t803081773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgr::Init()
extern "C"  void GlobalGOMgr_Init_m2455210678 (GlobalGOMgr_t803081773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgr::SceneLoadCompleteInit(System.Int32)
extern "C"  void GlobalGOMgr_SceneLoadCompleteInit_m1409714600 (GlobalGOMgr_t803081773 * __this, int32_t ___bigType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgr::InitOverEffectData()
extern "C"  void GlobalGOMgr_InitOverEffectData_m3870180133 (GlobalGOMgr_t803081773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgr::Lock2DTouchEvent(System.String)
extern "C"  void GlobalGOMgr_Lock2DTouchEvent_m170215524 (GlobalGOMgr_t803081773 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgr::UnLock2DTouchEvent(System.String)
extern "C"  void GlobalGOMgr_UnLock2DTouchEvent_m3340815307 (GlobalGOMgr_t803081773 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgr::UnLock2DTouchEvent(System.Object)
extern "C"  void GlobalGOMgr_UnLock2DTouchEvent_m3562925661 (GlobalGOMgr_t803081773 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgr::ForeUnLock2DTouchEvent()
extern "C"  void GlobalGOMgr_ForeUnLock2DTouchEvent_m3255423219 (GlobalGOMgr_t803081773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgr::Reduction2DTouchEvent()
extern "C"  void GlobalGOMgr_Reduction2DTouchEvent_m3209335676 (GlobalGOMgr_t803081773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgr::Lock3DTouchEvent(System.String)
extern "C"  void GlobalGOMgr_Lock3DTouchEvent_m1131829541 (GlobalGOMgr_t803081773 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgr::UnLock3DTouchEvent(System.String)
extern "C"  void GlobalGOMgr_UnLock3DTouchEvent_m7462028 (GlobalGOMgr_t803081773 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgr::UnLock3DTouchEvent(System.Object)
extern "C"  void GlobalGOMgr_UnLock3DTouchEvent_m229572382 (GlobalGOMgr_t803081773 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgr::ClearLock()
extern "C"  void GlobalGOMgr_ClearLock_m523967124 (GlobalGOMgr_t803081773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgr::ClickControl(System.Boolean)
extern "C"  void GlobalGOMgr_ClickControl_m1926607954 (GlobalGOMgr_t803081773 * __this, bool ___isClick0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgr::PlayOverEffect()
extern "C"  void GlobalGOMgr_PlayOverEffect_m687290111 (GlobalGOMgr_t803081773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgr::Reset()
extern "C"  void GlobalGOMgr_Reset_m2245884555 (GlobalGOMgr_t803081773 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject GlobalGOMgr::ilo_Instantiate1(UnityEngine.GameObject)
extern "C"  GameObject_t3674682005 * GlobalGOMgr_ilo_Instantiate1_m2597272515 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgr::ilo_set_alpha2(UIWidget,System.Single)
extern "C"  void GlobalGOMgr_ilo_set_alpha2_m3177192759 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GlobalGOMgr::ilo_get_IsClick2D3(GlobalGOMgr)
extern "C"  bool GlobalGOMgr_ilo_get_IsClick2D3_m2363324276 (Il2CppObject * __this /* static, unused */, GlobalGOMgr_t803081773 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgr::ilo_UnLock2DTouchEvent4(GlobalGOMgr,System.String)
extern "C"  void GlobalGOMgr_ilo_UnLock2DTouchEvent4_m972126933 (Il2CppObject * __this /* static, unused */, GlobalGOMgr_t803081773 * ____this0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalGOMgr::ilo_UnLock3DTouchEvent5(GlobalGOMgr,System.String)
extern "C"  void GlobalGOMgr_ilo_UnLock3DTouchEvent5_m2178970387 (Il2CppObject * __this /* static, unused */, GlobalGOMgr_t803081773 * ____this0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
