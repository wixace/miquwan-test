﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MethodID
struct MethodID_t3916401116;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine_ResourcesGenerated
struct  UnityEngine_ResourcesGenerated_t700159058  : public Il2CppObject
{
public:

public:
};

struct UnityEngine_ResourcesGenerated_t700159058_StaticFields
{
public:
	// MethodID UnityEngine_ResourcesGenerated::methodID1
	MethodID_t3916401116 * ___methodID1_0;
	// MethodID UnityEngine_ResourcesGenerated::methodID3
	MethodID_t3916401116 * ___methodID3_1;
	// MethodID UnityEngine_ResourcesGenerated::methodID6
	MethodID_t3916401116 * ___methodID6_2;
	// MethodID UnityEngine_ResourcesGenerated::methodID9
	MethodID_t3916401116 * ___methodID9_3;
	// MethodID UnityEngine_ResourcesGenerated::methodID12
	MethodID_t3916401116 * ___methodID12_4;

public:
	inline static int32_t get_offset_of_methodID1_0() { return static_cast<int32_t>(offsetof(UnityEngine_ResourcesGenerated_t700159058_StaticFields, ___methodID1_0)); }
	inline MethodID_t3916401116 * get_methodID1_0() const { return ___methodID1_0; }
	inline MethodID_t3916401116 ** get_address_of_methodID1_0() { return &___methodID1_0; }
	inline void set_methodID1_0(MethodID_t3916401116 * value)
	{
		___methodID1_0 = value;
		Il2CppCodeGenWriteBarrier(&___methodID1_0, value);
	}

	inline static int32_t get_offset_of_methodID3_1() { return static_cast<int32_t>(offsetof(UnityEngine_ResourcesGenerated_t700159058_StaticFields, ___methodID3_1)); }
	inline MethodID_t3916401116 * get_methodID3_1() const { return ___methodID3_1; }
	inline MethodID_t3916401116 ** get_address_of_methodID3_1() { return &___methodID3_1; }
	inline void set_methodID3_1(MethodID_t3916401116 * value)
	{
		___methodID3_1 = value;
		Il2CppCodeGenWriteBarrier(&___methodID3_1, value);
	}

	inline static int32_t get_offset_of_methodID6_2() { return static_cast<int32_t>(offsetof(UnityEngine_ResourcesGenerated_t700159058_StaticFields, ___methodID6_2)); }
	inline MethodID_t3916401116 * get_methodID6_2() const { return ___methodID6_2; }
	inline MethodID_t3916401116 ** get_address_of_methodID6_2() { return &___methodID6_2; }
	inline void set_methodID6_2(MethodID_t3916401116 * value)
	{
		___methodID6_2 = value;
		Il2CppCodeGenWriteBarrier(&___methodID6_2, value);
	}

	inline static int32_t get_offset_of_methodID9_3() { return static_cast<int32_t>(offsetof(UnityEngine_ResourcesGenerated_t700159058_StaticFields, ___methodID9_3)); }
	inline MethodID_t3916401116 * get_methodID9_3() const { return ___methodID9_3; }
	inline MethodID_t3916401116 ** get_address_of_methodID9_3() { return &___methodID9_3; }
	inline void set_methodID9_3(MethodID_t3916401116 * value)
	{
		___methodID9_3 = value;
		Il2CppCodeGenWriteBarrier(&___methodID9_3, value);
	}

	inline static int32_t get_offset_of_methodID12_4() { return static_cast<int32_t>(offsetof(UnityEngine_ResourcesGenerated_t700159058_StaticFields, ___methodID12_4)); }
	inline MethodID_t3916401116 * get_methodID12_4() const { return ___methodID12_4; }
	inline MethodID_t3916401116 ** get_address_of_methodID12_4() { return &___methodID12_4; }
	inline void set_methodID12_4(MethodID_t3916401116 * value)
	{
		___methodID12_4 = value;
		Il2CppCodeGenWriteBarrier(&___methodID12_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
