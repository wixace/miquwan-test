﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Object,System.Object,System.Object,System.Object>
struct U3CDelayCallExecU3Ec__Iterator42_4_t809237648;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C"  void U3CDelayCallExecU3Ec__Iterator42_4__ctor_m370869488_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t809237648 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4__ctor_m370869488(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator42_4_t809237648 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4__ctor_m370869488_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Object,System.Object,System.Object,System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2984235042_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t809237648 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2984235042(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator42_4_t809237648 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2984235042_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Object,System.Object,System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_IEnumerator_get_Current_m2343196598_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t809237648 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_IEnumerator_get_Current_m2343196598(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator42_4_t809237648 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4_System_Collections_IEnumerator_get_Current_m2343196598_gshared)(__this, method)
// System.Boolean BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Object,System.Object,System.Object,System.Object>::MoveNext()
extern "C"  bool U3CDelayCallExecU3Ec__Iterator42_4_MoveNext_m1937279492_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t809237648 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4_MoveNext_m1937279492(__this, method) ((  bool (*) (U3CDelayCallExecU3Ec__Iterator42_4_t809237648 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4_MoveNext_m1937279492_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Object,System.Object,System.Object,System.Object>::Dispose()
extern "C"  void U3CDelayCallExecU3Ec__Iterator42_4_Dispose_m4116542765_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t809237648 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4_Dispose_m4116542765(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator42_4_t809237648 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4_Dispose_m4116542765_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator42`4<System.Object,System.Object,System.Object,System.Object>::Reset()
extern "C"  void U3CDelayCallExecU3Ec__Iterator42_4_Reset_m2312269725_gshared (U3CDelayCallExecU3Ec__Iterator42_4_t809237648 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator42_4_Reset_m2312269725(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator42_4_t809237648 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator42_4_Reset_m2312269725_gshared)(__this, method)
