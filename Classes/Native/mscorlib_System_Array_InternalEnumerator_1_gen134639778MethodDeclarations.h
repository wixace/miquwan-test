﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen134639778.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21352297102.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3502657972_gshared (InternalEnumerator_1_t134639778 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3502657972(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t134639778 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3502657972_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1013383020_gshared (InternalEnumerator_1_t134639778 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1013383020(__this, method) ((  void (*) (InternalEnumerator_1_t134639778 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1013383020_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1803084962_gshared (InternalEnumerator_1_t134639778 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1803084962(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t134639778 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1803084962_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m534293643_gshared (InternalEnumerator_1_t134639778 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m534293643(__this, method) ((  void (*) (InternalEnumerator_1_t134639778 *, const MethodInfo*))InternalEnumerator_1_Dispose_m534293643_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m607805340_gshared (InternalEnumerator_1_t134639778 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m607805340(__this, method) ((  bool (*) (InternalEnumerator_1_t134639778 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m607805340_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t1352297102  InternalEnumerator_1_get_Current_m743419933_gshared (InternalEnumerator_1_t134639778 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m743419933(__this, method) ((  KeyValuePair_2_t1352297102  (*) (InternalEnumerator_1_t134639778 *, const MethodInfo*))InternalEnumerator_1_get_Current_m743419933_gshared)(__this, method)
