﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.Dictionary`2<System.String,asset_sharedCfg>
struct Dictionary_2_t2721732210;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Object>
struct Dictionary_2_t3891897029;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AssetBundle>
struct Dictionary_2_t2891378058;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.Collections.Generic.Dictionary`2<System.String,System.String[]>
struct Dictionary_2_t579454026;
// System.Collections.Generic.List`1<Mihua.Asset.ABLoadOperation.AssetOperation>
struct List_1_t2146913773;
// UnityEngine.AsyncOperation
struct AsyncOperation_t3699081103;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>
struct Dictionary_2_t813382503;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_Mihua_Asset_AssetMgr_LogMode3545913684.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mihua.Asset.AssetMgr
struct  AssetMgr_t543080634  : public MonoBehaviour_t667441552
{
public:

public:
};

struct AssetMgr_t543080634_StaticFields
{
public:
	// System.Single Mihua.Asset.AssetMgr::maxTime
	float ___maxTime_2;
	// Mihua.Asset.AssetMgr/LogMode Mihua.Asset.AssetMgr::m_LogMode
	int32_t ___m_LogMode_3;
	// System.Collections.Generic.List`1<System.String> Mihua.Asset.AssetMgr::keysToRemove
	List_1_t1375417109 * ___keysToRemove_4;
	// System.Collections.Generic.Dictionary`2<System.String,asset_sharedCfg> Mihua.Asset.AssetMgr::m_assetShared
	Dictionary_2_t2721732210 * ___m_assetShared_5;
	// System.Collections.Generic.List`1<System.String> Mihua.Asset.AssetMgr::nextNeedAssetList
	List_1_t1375417109 * ___nextNeedAssetList_6;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Object> Mihua.Asset.AssetMgr::m_CacheDic
	Dictionary_2_t3891897029 * ___m_CacheDic_7;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.AssetBundle> Mihua.Asset.AssetMgr::m_LoadedAssetBundles
	Dictionary_2_t2891378058 * ___m_LoadedAssetBundles_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Mihua.Asset.AssetMgr::m_DownloadingErrors
	Dictionary_2_t827649927 * ___m_DownloadingErrors_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.String[]> Mihua.Asset.AssetMgr::m_Dependencies
	Dictionary_2_t579454026 * ___m_Dependencies_10;
	// System.Collections.Generic.List`1<Mihua.Asset.ABLoadOperation.AssetOperation> Mihua.Asset.AssetMgr::m_InProgressOperations
	List_1_t2146913773 * ___m_InProgressOperations_11;
	// UnityEngine.AsyncOperation Mihua.Asset.AssetMgr::unloadAO
	AsyncOperation_t3699081103 * ___unloadAO_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>> Mihua.Asset.AssetMgr::m_assetsIndex
	Dictionary_2_t813382503 * ___m_assetsIndex_13;
	// System.Collections.Generic.List`1<System.String> Mihua.Asset.AssetMgr::assetFileName
	List_1_t1375417109 * ___assetFileName_14;
	// System.Int32 Mihua.Asset.AssetMgr::maxLoadingCount
	int32_t ___maxLoadingCount_15;
	// System.Boolean Mihua.Asset.AssetMgr::isClearAll
	bool ___isClearAll_16;
	// System.Boolean Mihua.Asset.AssetMgr::<unloading>k__BackingField
	bool ___U3CunloadingU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_maxTime_2() { return static_cast<int32_t>(offsetof(AssetMgr_t543080634_StaticFields, ___maxTime_2)); }
	inline float get_maxTime_2() const { return ___maxTime_2; }
	inline float* get_address_of_maxTime_2() { return &___maxTime_2; }
	inline void set_maxTime_2(float value)
	{
		___maxTime_2 = value;
	}

	inline static int32_t get_offset_of_m_LogMode_3() { return static_cast<int32_t>(offsetof(AssetMgr_t543080634_StaticFields, ___m_LogMode_3)); }
	inline int32_t get_m_LogMode_3() const { return ___m_LogMode_3; }
	inline int32_t* get_address_of_m_LogMode_3() { return &___m_LogMode_3; }
	inline void set_m_LogMode_3(int32_t value)
	{
		___m_LogMode_3 = value;
	}

	inline static int32_t get_offset_of_keysToRemove_4() { return static_cast<int32_t>(offsetof(AssetMgr_t543080634_StaticFields, ___keysToRemove_4)); }
	inline List_1_t1375417109 * get_keysToRemove_4() const { return ___keysToRemove_4; }
	inline List_1_t1375417109 ** get_address_of_keysToRemove_4() { return &___keysToRemove_4; }
	inline void set_keysToRemove_4(List_1_t1375417109 * value)
	{
		___keysToRemove_4 = value;
		Il2CppCodeGenWriteBarrier(&___keysToRemove_4, value);
	}

	inline static int32_t get_offset_of_m_assetShared_5() { return static_cast<int32_t>(offsetof(AssetMgr_t543080634_StaticFields, ___m_assetShared_5)); }
	inline Dictionary_2_t2721732210 * get_m_assetShared_5() const { return ___m_assetShared_5; }
	inline Dictionary_2_t2721732210 ** get_address_of_m_assetShared_5() { return &___m_assetShared_5; }
	inline void set_m_assetShared_5(Dictionary_2_t2721732210 * value)
	{
		___m_assetShared_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_assetShared_5, value);
	}

	inline static int32_t get_offset_of_nextNeedAssetList_6() { return static_cast<int32_t>(offsetof(AssetMgr_t543080634_StaticFields, ___nextNeedAssetList_6)); }
	inline List_1_t1375417109 * get_nextNeedAssetList_6() const { return ___nextNeedAssetList_6; }
	inline List_1_t1375417109 ** get_address_of_nextNeedAssetList_6() { return &___nextNeedAssetList_6; }
	inline void set_nextNeedAssetList_6(List_1_t1375417109 * value)
	{
		___nextNeedAssetList_6 = value;
		Il2CppCodeGenWriteBarrier(&___nextNeedAssetList_6, value);
	}

	inline static int32_t get_offset_of_m_CacheDic_7() { return static_cast<int32_t>(offsetof(AssetMgr_t543080634_StaticFields, ___m_CacheDic_7)); }
	inline Dictionary_2_t3891897029 * get_m_CacheDic_7() const { return ___m_CacheDic_7; }
	inline Dictionary_2_t3891897029 ** get_address_of_m_CacheDic_7() { return &___m_CacheDic_7; }
	inline void set_m_CacheDic_7(Dictionary_2_t3891897029 * value)
	{
		___m_CacheDic_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_CacheDic_7, value);
	}

	inline static int32_t get_offset_of_m_LoadedAssetBundles_8() { return static_cast<int32_t>(offsetof(AssetMgr_t543080634_StaticFields, ___m_LoadedAssetBundles_8)); }
	inline Dictionary_2_t2891378058 * get_m_LoadedAssetBundles_8() const { return ___m_LoadedAssetBundles_8; }
	inline Dictionary_2_t2891378058 ** get_address_of_m_LoadedAssetBundles_8() { return &___m_LoadedAssetBundles_8; }
	inline void set_m_LoadedAssetBundles_8(Dictionary_2_t2891378058 * value)
	{
		___m_LoadedAssetBundles_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_LoadedAssetBundles_8, value);
	}

	inline static int32_t get_offset_of_m_DownloadingErrors_9() { return static_cast<int32_t>(offsetof(AssetMgr_t543080634_StaticFields, ___m_DownloadingErrors_9)); }
	inline Dictionary_2_t827649927 * get_m_DownloadingErrors_9() const { return ___m_DownloadingErrors_9; }
	inline Dictionary_2_t827649927 ** get_address_of_m_DownloadingErrors_9() { return &___m_DownloadingErrors_9; }
	inline void set_m_DownloadingErrors_9(Dictionary_2_t827649927 * value)
	{
		___m_DownloadingErrors_9 = value;
		Il2CppCodeGenWriteBarrier(&___m_DownloadingErrors_9, value);
	}

	inline static int32_t get_offset_of_m_Dependencies_10() { return static_cast<int32_t>(offsetof(AssetMgr_t543080634_StaticFields, ___m_Dependencies_10)); }
	inline Dictionary_2_t579454026 * get_m_Dependencies_10() const { return ___m_Dependencies_10; }
	inline Dictionary_2_t579454026 ** get_address_of_m_Dependencies_10() { return &___m_Dependencies_10; }
	inline void set_m_Dependencies_10(Dictionary_2_t579454026 * value)
	{
		___m_Dependencies_10 = value;
		Il2CppCodeGenWriteBarrier(&___m_Dependencies_10, value);
	}

	inline static int32_t get_offset_of_m_InProgressOperations_11() { return static_cast<int32_t>(offsetof(AssetMgr_t543080634_StaticFields, ___m_InProgressOperations_11)); }
	inline List_1_t2146913773 * get_m_InProgressOperations_11() const { return ___m_InProgressOperations_11; }
	inline List_1_t2146913773 ** get_address_of_m_InProgressOperations_11() { return &___m_InProgressOperations_11; }
	inline void set_m_InProgressOperations_11(List_1_t2146913773 * value)
	{
		___m_InProgressOperations_11 = value;
		Il2CppCodeGenWriteBarrier(&___m_InProgressOperations_11, value);
	}

	inline static int32_t get_offset_of_unloadAO_12() { return static_cast<int32_t>(offsetof(AssetMgr_t543080634_StaticFields, ___unloadAO_12)); }
	inline AsyncOperation_t3699081103 * get_unloadAO_12() const { return ___unloadAO_12; }
	inline AsyncOperation_t3699081103 ** get_address_of_unloadAO_12() { return &___unloadAO_12; }
	inline void set_unloadAO_12(AsyncOperation_t3699081103 * value)
	{
		___unloadAO_12 = value;
		Il2CppCodeGenWriteBarrier(&___unloadAO_12, value);
	}

	inline static int32_t get_offset_of_m_assetsIndex_13() { return static_cast<int32_t>(offsetof(AssetMgr_t543080634_StaticFields, ___m_assetsIndex_13)); }
	inline Dictionary_2_t813382503 * get_m_assetsIndex_13() const { return ___m_assetsIndex_13; }
	inline Dictionary_2_t813382503 ** get_address_of_m_assetsIndex_13() { return &___m_assetsIndex_13; }
	inline void set_m_assetsIndex_13(Dictionary_2_t813382503 * value)
	{
		___m_assetsIndex_13 = value;
		Il2CppCodeGenWriteBarrier(&___m_assetsIndex_13, value);
	}

	inline static int32_t get_offset_of_assetFileName_14() { return static_cast<int32_t>(offsetof(AssetMgr_t543080634_StaticFields, ___assetFileName_14)); }
	inline List_1_t1375417109 * get_assetFileName_14() const { return ___assetFileName_14; }
	inline List_1_t1375417109 ** get_address_of_assetFileName_14() { return &___assetFileName_14; }
	inline void set_assetFileName_14(List_1_t1375417109 * value)
	{
		___assetFileName_14 = value;
		Il2CppCodeGenWriteBarrier(&___assetFileName_14, value);
	}

	inline static int32_t get_offset_of_maxLoadingCount_15() { return static_cast<int32_t>(offsetof(AssetMgr_t543080634_StaticFields, ___maxLoadingCount_15)); }
	inline int32_t get_maxLoadingCount_15() const { return ___maxLoadingCount_15; }
	inline int32_t* get_address_of_maxLoadingCount_15() { return &___maxLoadingCount_15; }
	inline void set_maxLoadingCount_15(int32_t value)
	{
		___maxLoadingCount_15 = value;
	}

	inline static int32_t get_offset_of_isClearAll_16() { return static_cast<int32_t>(offsetof(AssetMgr_t543080634_StaticFields, ___isClearAll_16)); }
	inline bool get_isClearAll_16() const { return ___isClearAll_16; }
	inline bool* get_address_of_isClearAll_16() { return &___isClearAll_16; }
	inline void set_isClearAll_16(bool value)
	{
		___isClearAll_16 = value;
	}

	inline static int32_t get_offset_of_U3CunloadingU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(AssetMgr_t543080634_StaticFields, ___U3CunloadingU3Ek__BackingField_17)); }
	inline bool get_U3CunloadingU3Ek__BackingField_17() const { return ___U3CunloadingU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CunloadingU3Ek__BackingField_17() { return &___U3CunloadingU3Ek__BackingField_17; }
	inline void set_U3CunloadingU3Ek__BackingField_17(bool value)
	{
		___U3CunloadingU3Ek__BackingField_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
