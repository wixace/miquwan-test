﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TeamSkillGenerated
struct TeamSkillGenerated_t2300632603;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void TeamSkillGenerated::.ctor()
extern "C"  void TeamSkillGenerated__ctor_m115200224 (TeamSkillGenerated_t2300632603 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TeamSkillGenerated::TeamSkill_TeamSkill1(JSVCall,System.Int32)
extern "C"  bool TeamSkillGenerated_TeamSkill_TeamSkill1_m3173626032 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillGenerated::TeamSkill_mpJson(JSVCall)
extern "C"  void TeamSkillGenerated_TeamSkill_mpJson_m1950231597 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillGenerated::TeamSkill_skillIDArr(JSVCall)
extern "C"  void TeamSkillGenerated_TeamSkill_skillIDArr_m3217981443 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillGenerated::TeamSkill_upnumberArr(JSVCall)
extern "C"  void TeamSkillGenerated_TeamSkill_upnumberArr_m1935420375 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillGenerated::TeamSkill_endSkillIDArr(JSVCall)
extern "C"  void TeamSkillGenerated_TeamSkill_endSkillIDArr_m1293762820 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillGenerated::TeamSkill_captainEndSkillIDArr(JSVCall)
extern "C"  void TeamSkillGenerated_TeamSkill_captainEndSkillIDArr_m3571495148 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillGenerated::TeamSkill_UIEffectArr(JSVCall)
extern "C"  void TeamSkillGenerated_TeamSkill_UIEffectArr_m4038781432 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillGenerated::TeamSkill_SectionArr(JSVCall)
extern "C"  void TeamSkillGenerated_TeamSkill_SectionArr_m847476252 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillGenerated::TeamSkill_ReducetimeArr(JSVCall)
extern "C"  void TeamSkillGenerated_TeamSkill_ReducetimeArr_m1246435718 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillGenerated::TeamSkill_GradeArr(JSVCall)
extern "C"  void TeamSkillGenerated_TeamSkill_GradeArr_m4220602510 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillGenerated::TeamSkill_id(JSVCall)
extern "C"  void TeamSkillGenerated_TeamSkill_id_m4100029245 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillGenerated::TeamSkill_isSelect(JSVCall)
extern "C"  void TeamSkillGenerated_TeamSkill_isSelect_m1431139442 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillGenerated::__Register()
extern "C"  void TeamSkillGenerated___Register_m2401449959 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TeamSkillGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool TeamSkillGenerated_ilo_attachFinalizerObject1_m1852007935 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TeamSkillGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * TeamSkillGenerated_ilo_getObject2_m4030757840 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void TeamSkillGenerated_ilo_setInt323_m1899589044 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillGenerated::ilo_setArrayS4(System.Int32,System.Int32,System.Boolean)
extern "C"  void TeamSkillGenerated_ilo_setArrayS4_m433039462 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TeamSkillGenerated::ilo_moveSaveID2Arr5(System.Int32)
extern "C"  void TeamSkillGenerated_ilo_moveSaveID2Arr5_m2848559097 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
