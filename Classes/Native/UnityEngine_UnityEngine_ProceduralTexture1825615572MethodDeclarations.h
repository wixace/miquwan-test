﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ProceduralTexture
struct ProceduralTexture_t1825615572;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ProceduralOutputType915118908.h"
#include "UnityEngine_UnityEngine_TextureFormat4189619560.h"

// System.Void UnityEngine.ProceduralTexture::.ctor()
extern "C"  void ProceduralTexture__ctor_m1558202811 (ProceduralTexture_t1825615572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ProceduralOutputType UnityEngine.ProceduralTexture::GetProceduralOutputType()
extern "C"  int32_t ProceduralTexture_GetProceduralOutputType_m3237672359 (ProceduralTexture_t1825615572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ProceduralTexture::get_hasAlpha()
extern "C"  bool ProceduralTexture_get_hasAlpha_m1639996752 (ProceduralTexture_t1825615572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TextureFormat UnityEngine.ProceduralTexture::get_format()
extern "C"  int32_t ProceduralTexture_get_format_m3906021809 (ProceduralTexture_t1825615572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32[] UnityEngine.ProceduralTexture::GetPixels32(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  Color32U5BU5D_t2960766953* ProceduralTexture_GetPixels32_m2621807269 (ProceduralTexture_t1825615572 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
