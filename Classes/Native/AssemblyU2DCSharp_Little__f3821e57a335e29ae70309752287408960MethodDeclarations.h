﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._f3821e57a335e29ae70309757b69bf2e
struct _f3821e57a335e29ae70309757b69bf2e_t2287408960;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._f3821e57a335e29ae70309757b69bf2e::.ctor()
extern "C"  void _f3821e57a335e29ae70309757b69bf2e__ctor_m1355953901 (_f3821e57a335e29ae70309757b69bf2e_t2287408960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f3821e57a335e29ae70309757b69bf2e::_f3821e57a335e29ae70309757b69bf2em2(System.Int32)
extern "C"  int32_t _f3821e57a335e29ae70309757b69bf2e__f3821e57a335e29ae70309757b69bf2em2_m3935160729 (_f3821e57a335e29ae70309757b69bf2e_t2287408960 * __this, int32_t ____f3821e57a335e29ae70309757b69bf2ea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f3821e57a335e29ae70309757b69bf2e::_f3821e57a335e29ae70309757b69bf2em(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _f3821e57a335e29ae70309757b69bf2e__f3821e57a335e29ae70309757b69bf2em_m523623741 (_f3821e57a335e29ae70309757b69bf2e_t2287408960 * __this, int32_t ____f3821e57a335e29ae70309757b69bf2ea0, int32_t ____f3821e57a335e29ae70309757b69bf2e341, int32_t ____f3821e57a335e29ae70309757b69bf2ec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
