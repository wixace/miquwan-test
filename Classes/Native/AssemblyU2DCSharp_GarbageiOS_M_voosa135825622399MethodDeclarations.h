﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_voosa135
struct M_voosa135_t825622399;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_voosa135::.ctor()
extern "C"  void M_voosa135__ctor_m2339890580 (M_voosa135_t825622399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_voosa135::M_vimirtu0(System.String[],System.Int32)
extern "C"  void M_voosa135_M_vimirtu0_m1240079461 (M_voosa135_t825622399 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_voosa135::M_derpou1(System.String[],System.Int32)
extern "C"  void M_voosa135_M_derpou1_m4023696195 (M_voosa135_t825622399 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_voosa135::M_jale2(System.String[],System.Int32)
extern "C"  void M_voosa135_M_jale2_m2193638809 (M_voosa135_t825622399 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
