﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_t2137423328;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t4136801618;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Collections.Generic.List`1<System.Reflection.MemberInfo>
struct List_1_t1068734154;
// System.Type[]
struct TypeU5BU5D_t3339007067;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Collections.Generic.IList`1<System.Type>
struct IList_1_t1262825681;
// System.Func`3<System.Type,System.Collections.Generic.IList`1<System.Object>,System.Object>
struct Func_3_t1067326363;
// System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo>
struct IEnumerable_1_t2978998927;
// System.Collections.Generic.IList`1<System.Reflection.MemberInfo>
struct IList_1_t2395195805;
// System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo>
struct IEnumerable_1_t4225181682;
// System.Collections.Generic.IList`1<System.Reflection.PropertyInfo>
struct IList_1_t3618915928;
// <>__AnonType0`2<System.Int32,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>
struct U3CU3E__AnonType0_2_t3677950232;
// System.Linq.IGrouping`2<System.String,System.Reflection.MemberInfo>
struct IGrouping_2_t3411231652;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t2570496278;
// System.Reflection.ParameterInfo
struct ParameterInfo_t2235474049;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Collections.IList
struct IList_t1751339649;
// System.Collections.IEnumerable
struct IEnumerable_t3464557803;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_PropertyInfo924268725.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_F3005881063.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2137423328.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "mscorlib_System_Reflection_BindingFlags1523912596.h"
#include "mscorlib_System_Nullable_1_gen1237965023.h"
#include "mscorlib_System_Reflection_ParameterInfo2235474049.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266.h"

// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsVirtual(System.Reflection.PropertyInfo)
extern "C"  bool ReflectionUtils_IsVirtual_m4052492746 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___propertyInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Utilities.ReflectionUtils::GetObjectType(System.Object)
extern "C"  Type_t * ReflectionUtils_GetObjectType_m1052889583 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.ReflectionUtils::GetTypeName(System.Type,System.Runtime.Serialization.Formatters.FormatterAssemblyStyle)
extern "C"  String_t* ReflectionUtils_GetTypeName_m1435858548 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, int32_t ___assemblyFormat1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.ReflectionUtils::GetTypeName(System.Type,System.Runtime.Serialization.Formatters.FormatterAssemblyStyle,System.Runtime.Serialization.SerializationBinder)
extern "C"  String_t* ReflectionUtils_GetTypeName_m2297345675 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, int32_t ___assemblyFormat1, SerializationBinder_t2137423328 * ___binder2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.ReflectionUtils::RemoveAssemblyDetails(System.String)
extern "C"  String_t* ReflectionUtils_RemoveAssemblyDetails_m3352817099 (Il2CppObject * __this /* static, unused */, String_t* ___fullyQualifiedTypeName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsInstantiatableType(System.Type)
extern "C"  bool ReflectionUtils_IsInstantiatableType_m437710062 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::HasDefaultConstructor(System.Type)
extern "C"  bool ReflectionUtils_HasDefaultConstructor_m2181615766 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::HasDefaultConstructor(System.Type,System.Boolean)
extern "C"  bool ReflectionUtils_HasDefaultConstructor_m3891627047 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, bool ___nonPublic1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo Newtonsoft.Json.Utilities.ReflectionUtils::GetDefaultConstructor(System.Type)
extern "C"  ConstructorInfo_t4136801618 * ReflectionUtils_GetDefaultConstructor_m1759291423 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo Newtonsoft.Json.Utilities.ReflectionUtils::GetDefaultConstructor(System.Type,System.Boolean)
extern "C"  ConstructorInfo_t4136801618 * ReflectionUtils_GetDefaultConstructor_m1495828862 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, bool ___nonPublic1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsNullable(System.Type)
extern "C"  bool ReflectionUtils_IsNullable_m2193505948 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsNullableType(System.Type)
extern "C"  bool ReflectionUtils_IsNullableType_m640088962 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Utilities.ReflectionUtils::EnsureNotNullableType(System.Type)
extern "C"  Type_t * ReflectionUtils_EnsureNotNullableType_m3375354803 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsUnitializedValue(System.Object)
extern "C"  bool ReflectionUtils_IsUnitializedValue_m3385755367 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ReflectionUtils::CreateUnitializedValue(System.Type)
extern "C"  Il2CppObject * ReflectionUtils_CreateUnitializedValue_m1606058939 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsPropertyIndexed(System.Reflection.PropertyInfo)
extern "C"  bool ReflectionUtils_IsPropertyIndexed_m1974805275 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___property0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::ImplementsGenericDefinition(System.Type,System.Type)
extern "C"  bool ReflectionUtils_ImplementsGenericDefinition_m1837579874 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, Type_t * ___genericInterfaceDefinition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::ImplementsGenericDefinition(System.Type,System.Type,System.Type&)
extern "C"  bool ReflectionUtils_ImplementsGenericDefinition_m907478999 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, Type_t * ___genericInterfaceDefinition1, Type_t ** ___implementingType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::AssignableToTypeName(System.Type,System.String,System.Type&)
extern "C"  bool ReflectionUtils_AssignableToTypeName_m3749652063 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, String_t* ___fullTypeName1, Type_t ** ___match2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::AssignableToTypeName(System.Type,System.String)
extern "C"  bool ReflectionUtils_AssignableToTypeName_m2678596314 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, String_t* ___fullTypeName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::InheritsGenericDefinition(System.Type,System.Type)
extern "C"  bool ReflectionUtils_InheritsGenericDefinition_m1885766602 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, Type_t * ___genericClassDefinition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::InheritsGenericDefinition(System.Type,System.Type,System.Type&)
extern "C"  bool ReflectionUtils_InheritsGenericDefinition_m2668219247 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, Type_t * ___genericClassDefinition1, Type_t ** ___implementingType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::InheritsGenericDefinitionInternal(System.Type,System.Type,System.Type&)
extern "C"  bool ReflectionUtils_InheritsGenericDefinitionInternal_m571389836 (Il2CppObject * __this /* static, unused */, Type_t * ___currentType0, Type_t * ___genericClassDefinition1, Type_t ** ___implementingType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Utilities.ReflectionUtils::GetCollectionItemType(System.Type)
extern "C"  Type_t * ReflectionUtils_GetCollectionItemType_m2672508994 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ReflectionUtils::GetDictionaryKeyValueTypes(System.Type,System.Type&,System.Type&)
extern "C"  void ReflectionUtils_GetDictionaryKeyValueTypes_m1361820184 (Il2CppObject * __this /* static, unused */, Type_t * ___dictionaryType0, Type_t ** ___keyType1, Type_t ** ___valueType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Utilities.ReflectionUtils::GetDictionaryValueType(System.Type)
extern "C"  Type_t * ReflectionUtils_GetDictionaryValueType_m890456078 (Il2CppObject * __this /* static, unused */, Type_t * ___dictionaryType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Utilities.ReflectionUtils::GetDictionaryKeyType(System.Type)
extern "C"  Type_t * ReflectionUtils_GetDictionaryKeyType_m1016342816 (Il2CppObject * __this /* static, unused */, Type_t * ___dictionaryType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Utilities.ReflectionUtils::GetMemberUnderlyingType(System.Reflection.MemberInfo)
extern "C"  Type_t * ReflectionUtils_GetMemberUnderlyingType_m2208268337 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsIndexedProperty(System.Reflection.MemberInfo)
extern "C"  bool ReflectionUtils_IsIndexedProperty_m2795771328 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsIndexedProperty(System.Reflection.PropertyInfo)
extern "C"  bool ReflectionUtils_IsIndexedProperty_m925551525 (Il2CppObject * __this /* static, unused */, PropertyInfo_t * ___property0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ReflectionUtils::GetMemberValue(System.Reflection.MemberInfo,System.Object)
extern "C"  Il2CppObject * ReflectionUtils_GetMemberValue_m2642429908 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, Il2CppObject * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ReflectionUtils::SetMemberValue(System.Reflection.MemberInfo,System.Object,System.Object)
extern "C"  void ReflectionUtils_SetMemberValue_m738515179 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, Il2CppObject * ___target1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::CanReadMemberValue(System.Reflection.MemberInfo,System.Boolean)
extern "C"  bool ReflectionUtils_CanReadMemberValue_m697335974 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, bool ___nonPublic1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::CanSetMemberValue(System.Reflection.MemberInfo,System.Boolean,System.Boolean)
extern "C"  bool ReflectionUtils_CanSetMemberValue_m46089833 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, bool ___nonPublic1, bool ___canSetReadOnly2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Reflection.MemberInfo> Newtonsoft.Json.Utilities.ReflectionUtils::GetFieldsAndProperties(System.Type,System.Reflection.BindingFlags)
extern "C"  List_1_t1068734154 * ReflectionUtils_GetFieldsAndProperties_m3424261769 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, int32_t ___bindingAttr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsOverridenGenericMember(System.Reflection.MemberInfo,System.Reflection.BindingFlags)
extern "C"  bool ReflectionUtils_IsOverridenGenericMember_m1085028257 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___memberInfo0, int32_t ___bindingAttr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.ReflectionUtils::GetNameAndAssessmblyName(System.Type)
extern "C"  String_t* ReflectionUtils_GetNameAndAssessmblyName_m3815925327 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Utilities.ReflectionUtils::MakeGenericType(System.Type,System.Type[])
extern "C"  Type_t * ReflectionUtils_MakeGenericType_m4192478737 (Il2CppObject * __this /* static, unused */, Type_t * ___genericTypeDefinition0, TypeU5BU5D_t3339007067* ___innerTypes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ReflectionUtils::CreateGeneric(System.Type,System.Type,System.Object[])
extern "C"  Il2CppObject * ReflectionUtils_CreateGeneric_m1616286028 (Il2CppObject * __this /* static, unused */, Type_t * ___genericTypeDefinition0, Type_t * ___innerType1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ReflectionUtils::CreateGeneric(System.Type,System.Collections.Generic.IList`1<System.Type>,System.Object[])
extern "C"  Il2CppObject * ReflectionUtils_CreateGeneric_m952862189 (Il2CppObject * __this /* static, unused */, Type_t * ___genericTypeDefinition0, Il2CppObject* ___innerTypes1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ReflectionUtils::CreateGeneric(System.Type,System.Collections.Generic.IList`1<System.Type>,System.Func`3<System.Type,System.Collections.Generic.IList`1<System.Object>,System.Object>,System.Object[])
extern "C"  Il2CppObject * ReflectionUtils_CreateGeneric_m3903140063 (Il2CppObject * __this /* static, unused */, Type_t * ___genericTypeDefinition0, Il2CppObject* ___innerTypes1, Func_3_t1067326363 * ___instanceCreator2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::IsCompatibleValue(System.Object,System.Type)
extern "C"  bool ReflectionUtils_IsCompatibleValue_m2629950564 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ReflectionUtils::CreateInstance(System.Type,System.Object[])
extern "C"  Il2CppObject * ReflectionUtils_CreateInstance_m2555227011 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, ObjectU5BU5D_t1108656482* ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ReflectionUtils::SplitFullyQualifiedTypeName(System.String,System.String&,System.String&)
extern "C"  void ReflectionUtils_SplitFullyQualifiedTypeName_m2376734293 (Il2CppObject * __this /* static, unused */, String_t* ___fullyQualifiedTypeName0, String_t** ___typeName1, String_t** ___assemblyName2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.Utilities.ReflectionUtils::GetAssemblyDelimiterIndex(System.String)
extern "C"  Nullable_1_t1237965023  ReflectionUtils_GetAssemblyDelimiterIndex_m905170288 (Il2CppObject * __this /* static, unused */, String_t* ___fullyQualifiedTypeName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MemberInfo Newtonsoft.Json.Utilities.ReflectionUtils::GetMemberInfoFromType(System.Type,System.Reflection.MemberInfo)
extern "C"  MemberInfo_t * ReflectionUtils_GetMemberInfoFromType_m225072048 (Il2CppObject * __this /* static, unused */, Type_t * ___targetType0, MemberInfo_t * ___memberInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo> Newtonsoft.Json.Utilities.ReflectionUtils::GetFields(System.Type,System.Reflection.BindingFlags)
extern "C"  Il2CppObject* ReflectionUtils_GetFields_m2113888898 (Il2CppObject * __this /* static, unused */, Type_t * ___targetType0, int32_t ___bindingAttr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ReflectionUtils::GetChildPrivateFields(System.Collections.Generic.IList`1<System.Reflection.MemberInfo>,System.Type,System.Reflection.BindingFlags)
extern "C"  void ReflectionUtils_GetChildPrivateFields_m2207215002 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___initialFields0, Type_t * ___targetType1, int32_t ___bindingAttr2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo> Newtonsoft.Json.Utilities.ReflectionUtils::GetProperties(System.Type,System.Reflection.BindingFlags)
extern "C"  Il2CppObject* ReflectionUtils_GetProperties_m2166755899 (Il2CppObject * __this /* static, unused */, Type_t * ___targetType0, int32_t ___bindingAttr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.BindingFlags Newtonsoft.Json.Utilities.ReflectionUtils::RemoveFlag(System.Reflection.BindingFlags,System.Reflection.BindingFlags)
extern "C"  int32_t ReflectionUtils_RemoveFlag_m3325226855 (Il2CppObject * __this /* static, unused */, int32_t ___bindingAttr0, int32_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ReflectionUtils::GetChildPrivateProperties(System.Collections.Generic.IList`1<System.Reflection.PropertyInfo>,System.Type,System.Reflection.BindingFlags)
extern "C"  void ReflectionUtils_GetChildPrivateProperties_m954813957 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___initialProperties0, Type_t * ___targetType1, int32_t ___bindingAttr2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.ReflectionUtils::<GetFieldsAndProperties>m__3AE(System.Reflection.MemberInfo)
extern "C"  String_t* ReflectionUtils_U3CGetFieldsAndPropertiesU3Em__3AE_m2380932456 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// <>__AnonType0`2<System.Int32,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>> Newtonsoft.Json.Utilities.ReflectionUtils::<GetFieldsAndProperties>m__3AF(System.Linq.IGrouping`2<System.String,System.Reflection.MemberInfo>)
extern "C"  U3CU3E__AnonType0_2_t3677950232 * ReflectionUtils_U3CGetFieldsAndPropertiesU3Em__3AF_m995596308 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___g0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ReflectionUtils::<CreateGeneric>m__3B1(System.Type,System.Collections.Generic.IList`1<System.Object>)
extern "C"  Il2CppObject * ReflectionUtils_U3CCreateGenericU3Em__3B1_m2634934605 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, Il2CppObject* ___a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Utilities.ReflectionUtils::<GetMemberInfoFromType>m__3B2(System.Reflection.ParameterInfo)
extern "C"  Type_t * ReflectionUtils_U3CGetMemberInfoFromTypeU3Em__3B2_m3877034811 (Il2CppObject * __this /* static, unused */, ParameterInfo_t2235474049 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::<GetChildPrivateFields>m__3B3(System.Reflection.FieldInfo)
extern "C"  bool ReflectionUtils_U3CGetChildPrivateFieldsU3Em__3B3_m2326243619 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___f0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ReflectionUtils::ilo_ArgumentNotNull1(System.Object,System.String)
extern "C"  void ReflectionUtils_ilo_ArgumentNotNull1_m3557268003 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, String_t* ___parameterName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::ilo_HasDefaultConstructor2(System.Type,System.Boolean)
extern "C"  bool ReflectionUtils_ilo_HasDefaultConstructor2_m2197866888 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, bool ___nonPublic1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::ilo_IsNullableType3(System.Type)
extern "C"  bool ReflectionUtils_ilo_IsNullableType3_m3655681870 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.ReflectionUtils::ilo_CreateUnitializedValue4(System.Type)
extern "C"  Il2CppObject * ReflectionUtils_ilo_CreateUnitializedValue4_m4261586260 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Utilities.ReflectionUtils::ilo_FormatWith5(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* ReflectionUtils_ilo_FormatWith5_m2377063426 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.ReflectionUtils::ilo_InheritsGenericDefinitionInternal6(System.Type,System.Type,System.Type&)
extern "C"  bool ReflectionUtils_ilo_InheritsGenericDefinitionInternal6_m185218617 (Il2CppObject * __this /* static, unused */, Type_t * ___currentType0, Type_t * ___genericClassDefinition1, Type_t ** ___implementingType2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Reflection.MemberInfo> Newtonsoft.Json.Utilities.ReflectionUtils::ilo_GetFieldsAndProperties7(System.Type,System.Reflection.BindingFlags)
extern "C"  List_1_t1068734154 * ReflectionUtils_ilo_GetFieldsAndProperties7_m1516530699 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, int32_t ___bindingAttr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo> Newtonsoft.Json.Utilities.ReflectionUtils::ilo_GetFields8(System.Type,System.Reflection.BindingFlags)
extern "C"  Il2CppObject* ReflectionUtils_ilo_GetFields8_m227216697 (Il2CppObject * __this /* static, unused */, Type_t * ___targetType0, int32_t ___bindingAttr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo> Newtonsoft.Json.Utilities.ReflectionUtils::ilo_GetProperties9(System.Type,System.Reflection.BindingFlags)
extern "C"  Il2CppObject* ReflectionUtils_ilo_GetProperties9_m2622860577 (Il2CppObject * __this /* static, unused */, Type_t * ___targetType0, int32_t ___bindingAttr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ReflectionUtils::ilo_AddRange10(System.Collections.IList,System.Collections.IEnumerable)
extern "C"  void ReflectionUtils_ilo_AddRange10_m2102322248 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___initial0, Il2CppObject * ___collection1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ReflectionUtils::ilo_GetChildPrivateFields11(System.Collections.Generic.IList`1<System.Reflection.MemberInfo>,System.Type,System.Reflection.BindingFlags)
extern "C"  void ReflectionUtils_ilo_GetChildPrivateFields11_m936791533 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___initialFields0, Type_t * ___targetType1, int32_t ___bindingAttr2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.BindingFlags Newtonsoft.Json.Utilities.ReflectionUtils::ilo_RemoveFlag12(System.Reflection.BindingFlags,System.Reflection.BindingFlags)
extern "C"  int32_t ReflectionUtils_ilo_RemoveFlag12_m3513709459 (Il2CppObject * __this /* static, unused */, int32_t ___bindingAttr0, int32_t ___flag1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.ReflectionUtils::ilo_GetChildPrivateProperties13(System.Collections.Generic.IList`1<System.Reflection.PropertyInfo>,System.Type,System.Reflection.BindingFlags)
extern "C"  void ReflectionUtils_ilo_GetChildPrivateProperties13_m2983611990 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___initialProperties0, Type_t * ___targetType1, int32_t ___bindingAttr2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
