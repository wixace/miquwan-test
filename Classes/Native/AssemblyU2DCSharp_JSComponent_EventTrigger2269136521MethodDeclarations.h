﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSComponent_EventTrigger
struct JSComponent_EventTrigger_t2269136521;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t1848751023;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t2054899105;
// UnityEngine.EventSystems.AxisEventData
struct AxisEventData_t3355659985;
// JSComponent
struct JSComponent_t1642894772;
// System.String
struct String_t;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEve1848751023.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_BaseEventD2054899105.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_AxisEventD3355659985.h"
#include "AssemblyU2DCSharp_JSComponent1642894772.h"
#include "mscorlib_System_String7231557.h"

// System.Void JSComponent_EventTrigger::.ctor()
extern "C"  void JSComponent_EventTrigger__ctor_m4064121394 (JSComponent_EventTrigger_t2269136521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::initMemberFunction()
extern "C"  void JSComponent_EventTrigger_initMemberFunction_m857943092 (JSComponent_EventTrigger_t2269136521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::Update()
extern "C"  void JSComponent_EventTrigger_Update_m3160573723 (JSComponent_EventTrigger_t2269136521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::LateUpdate()
extern "C"  void JSComponent_EventTrigger_LateUpdate_m3717547873 (JSComponent_EventTrigger_t2269136521 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void JSComponent_EventTrigger_OnBeginDrag_m38075984 (JSComponent_EventTrigger_t2269136521 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::OnCancel(UnityEngine.EventSystems.BaseEventData)
extern "C"  void JSComponent_EventTrigger_OnCancel_m3659395429 (JSComponent_EventTrigger_t2269136521 * __this, BaseEventData_t2054899105 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::OnDeselect(UnityEngine.EventSystems.BaseEventData)
extern "C"  void JSComponent_EventTrigger_OnDeselect_m2003094696 (JSComponent_EventTrigger_t2269136521 * __this, BaseEventData_t2054899105 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void JSComponent_EventTrigger_OnDrag_m2312432537 (JSComponent_EventTrigger_t2269136521 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::OnDrop(UnityEngine.EventSystems.PointerEventData)
extern "C"  void JSComponent_EventTrigger_OnDrop_m3639558910 (JSComponent_EventTrigger_t2269136521 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void JSComponent_EventTrigger_OnEndDrag_m2358108126 (JSComponent_EventTrigger_t2269136521 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
extern "C"  void JSComponent_EventTrigger_OnInitializePotentialDrag_m982386155 (JSComponent_EventTrigger_t2269136521 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::OnMove(UnityEngine.EventSystems.AxisEventData)
extern "C"  void JSComponent_EventTrigger_OnMove_m2321616012 (JSComponent_EventTrigger_t2269136521 * __this, AxisEventData_t3355659985 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern "C"  void JSComponent_EventTrigger_OnPointerClick_m3126061858 (JSComponent_EventTrigger_t2269136521 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C"  void JSComponent_EventTrigger_OnPointerDown_m2670905870 (JSComponent_EventTrigger_t2269136521 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern "C"  void JSComponent_EventTrigger_OnPointerEnter_m2595813330 (JSComponent_EventTrigger_t2269136521 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern "C"  void JSComponent_EventTrigger_OnPointerExit_m3063988242 (JSComponent_EventTrigger_t2269136521 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C"  void JSComponent_EventTrigger_OnPointerUp_m584398645 (JSComponent_EventTrigger_t2269136521 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::OnScroll(UnityEngine.EventSystems.PointerEventData)
extern "C"  void JSComponent_EventTrigger_OnScroll_m3488833632 (JSComponent_EventTrigger_t2269136521 * __this, PointerEventData_t1848751023 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::OnSelect(UnityEngine.EventSystems.BaseEventData)
extern "C"  void JSComponent_EventTrigger_OnSelect_m1996148199 (JSComponent_EventTrigger_t2269136521 * __this, BaseEventData_t2054899105 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::OnSubmit(UnityEngine.EventSystems.BaseEventData)
extern "C"  void JSComponent_EventTrigger_OnSubmit_m3660942595 (JSComponent_EventTrigger_t2269136521 * __this, BaseEventData_t2054899105 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::OnUpdateSelected(UnityEngine.EventSystems.BaseEventData)
extern "C"  void JSComponent_EventTrigger_OnUpdateSelected_m394647407 (JSComponent_EventTrigger_t2269136521 * __this, BaseEventData_t2054899105 * ___eventData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::ilo_initMemberFunction1(JSComponent)
extern "C"  void JSComponent_EventTrigger_ilo_initMemberFunction1_m1749147890 (Il2CppObject * __this /* static, unused */, JSComponent_t1642894772 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSComponent_EventTrigger::ilo_getObjFunction2(System.Int32,System.String)
extern "C"  int32_t JSComponent_EventTrigger_ilo_getObjFunction2_m3491393973 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___fname1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSComponent_EventTrigger::ilo_callIfExist3(JSComponent,System.Int32,System.Object[])
extern "C"  void JSComponent_EventTrigger_ilo_callIfExist3_m1775515585 (Il2CppObject * __this /* static, unused */, JSComponent_t1642894772 * ____this0, int32_t ___funID1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
