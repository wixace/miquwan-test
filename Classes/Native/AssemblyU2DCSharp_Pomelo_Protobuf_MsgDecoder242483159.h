﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// Pomelo.Protobuf.Util
struct Util_t3483766166;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pomelo.Protobuf.MsgDecoder
struct  MsgDecoder_t242483159  : public Il2CppObject
{
public:
	// Newtonsoft.Json.Linq.JObject Pomelo.Protobuf.MsgDecoder::<protos>k__BackingField
	JObject_t1798544199 * ___U3CprotosU3Ek__BackingField_0;
	// System.Int32 Pomelo.Protobuf.MsgDecoder::<offset>k__BackingField
	int32_t ___U3CoffsetU3Ek__BackingField_1;
	// System.Byte[] Pomelo.Protobuf.MsgDecoder::<buffer>k__BackingField
	ByteU5BU5D_t4260760469* ___U3CbufferU3Ek__BackingField_2;
	// Pomelo.Protobuf.Util Pomelo.Protobuf.MsgDecoder::<util>k__BackingField
	Util_t3483766166 * ___U3CutilU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CprotosU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MsgDecoder_t242483159, ___U3CprotosU3Ek__BackingField_0)); }
	inline JObject_t1798544199 * get_U3CprotosU3Ek__BackingField_0() const { return ___U3CprotosU3Ek__BackingField_0; }
	inline JObject_t1798544199 ** get_address_of_U3CprotosU3Ek__BackingField_0() { return &___U3CprotosU3Ek__BackingField_0; }
	inline void set_U3CprotosU3Ek__BackingField_0(JObject_t1798544199 * value)
	{
		___U3CprotosU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CprotosU3Ek__BackingField_0, value);
	}

	inline static int32_t get_offset_of_U3CoffsetU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MsgDecoder_t242483159, ___U3CoffsetU3Ek__BackingField_1)); }
	inline int32_t get_U3CoffsetU3Ek__BackingField_1() const { return ___U3CoffsetU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CoffsetU3Ek__BackingField_1() { return &___U3CoffsetU3Ek__BackingField_1; }
	inline void set_U3CoffsetU3Ek__BackingField_1(int32_t value)
	{
		___U3CoffsetU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CbufferU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MsgDecoder_t242483159, ___U3CbufferU3Ek__BackingField_2)); }
	inline ByteU5BU5D_t4260760469* get_U3CbufferU3Ek__BackingField_2() const { return ___U3CbufferU3Ek__BackingField_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_U3CbufferU3Ek__BackingField_2() { return &___U3CbufferU3Ek__BackingField_2; }
	inline void set_U3CbufferU3Ek__BackingField_2(ByteU5BU5D_t4260760469* value)
	{
		___U3CbufferU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CbufferU3Ek__BackingField_2, value);
	}

	inline static int32_t get_offset_of_U3CutilU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(MsgDecoder_t242483159, ___U3CutilU3Ek__BackingField_3)); }
	inline Util_t3483766166 * get_U3CutilU3Ek__BackingField_3() const { return ___U3CutilU3Ek__BackingField_3; }
	inline Util_t3483766166 ** get_address_of_U3CutilU3Ek__BackingField_3() { return &___U3CutilU3Ek__BackingField_3; }
	inline void set_U3CutilU3Ek__BackingField_3(Util_t3483766166 * value)
	{
		___U3CutilU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CutilU3Ek__BackingField_3, value);
	}
};

struct MsgDecoder_t242483159_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Pomelo.Protobuf.MsgDecoder::<>f__switch$map1F
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map1F_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Pomelo.Protobuf.MsgDecoder::<>f__switch$map20
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map20_5;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map1F_4() { return static_cast<int32_t>(offsetof(MsgDecoder_t242483159_StaticFields, ___U3CU3Ef__switchU24map1F_4)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map1F_4() const { return ___U3CU3Ef__switchU24map1F_4; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map1F_4() { return &___U3CU3Ef__switchU24map1F_4; }
	inline void set_U3CU3Ef__switchU24map1F_4(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map1F_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map1F_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map20_5() { return static_cast<int32_t>(offsetof(MsgDecoder_t242483159_StaticFields, ___U3CU3Ef__switchU24map20_5)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map20_5() const { return ___U3CU3Ef__switchU24map20_5; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map20_5() { return &___U3CU3Ef__switchU24map20_5; }
	inline void set_U3CU3Ef__switchU24map20_5(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map20_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map20_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
