﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Gesture
struct Gesture_t1589572905;
// Gesture/EventHandler
struct EventHandler_t2814654102;
// FingerGestures/FingerList
struct FingerList_t1886137443;
// GestureRecognizer
struct GestureRecognizer_t3512875949;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// ScreenRaycaster
struct ScreenRaycaster_t4188861866;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Gesture_EventHandler2814654102.h"
#include "AssemblyU2DCSharp_FingerGestures_FingerList1886137443.h"
#include "AssemblyU2DCSharp_GestureRecognizer3512875949.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_GestureRecognitionState3604239971.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_ScreenRaycastData1110901127.h"
#include "AssemblyU2DCSharp_ScreenRaycaster4188861866.h"
#include "AssemblyU2DCSharp_Gesture1589572905.h"

// System.Void Gesture::.ctor()
extern "C"  void Gesture__ctor_m1915811938 (Gesture_t1589572905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gesture::add_OnStateChanged(Gesture/EventHandler)
extern "C"  void Gesture_add_OnStateChanged_m1220715596 (Gesture_t1589572905 * __this, EventHandler_t2814654102 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gesture::remove_OnStateChanged(Gesture/EventHandler)
extern "C"  void Gesture_remove_OnStateChanged_m533623847 (Gesture_t1589572905 * __this, EventHandler_t2814654102 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/FingerList Gesture::get_Fingers()
extern "C"  FingerList_t1886137443 * Gesture_get_Fingers_m497292433 (Gesture_t1589572905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gesture::set_Fingers(FingerGestures/FingerList)
extern "C"  void Gesture_set_Fingers_m394850946 (Gesture_t1589572905 * __this, FingerList_t1886137443 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GestureRecognizer Gesture::get_Recognizer()
extern "C"  GestureRecognizer_t3512875949 * Gesture_get_Recognizer_m3443234761 (Gesture_t1589572905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gesture::set_Recognizer(GestureRecognizer)
extern "C"  void Gesture_set_Recognizer_m532011202 (Gesture_t1589572905 * __this, GestureRecognizer_t3512875949 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Gesture::get_StartTime()
extern "C"  float Gesture_get_StartTime_m3223758450 (Gesture_t1589572905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gesture::set_StartTime(System.Single)
extern "C"  void Gesture_set_StartTime_m3721071161 (Gesture_t1589572905 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Gesture::get_StartPosition()
extern "C"  Vector2_t4282066565  Gesture_get_StartPosition_m285519837 (Gesture_t1589572905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gesture::set_StartPosition(UnityEngine.Vector2)
extern "C"  void Gesture_set_StartPosition_m1839633132 (Gesture_t1589572905 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Gesture::get_Position()
extern "C"  Vector2_t4282066565  Gesture_get_Position_m451484505 (Gesture_t1589572905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gesture::set_Position(UnityEngine.Vector2)
extern "C"  void Gesture_set_Position_m1053063218 (Gesture_t1589572905 * __this, Vector2_t4282066565  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GestureRecognitionState Gesture::get_State()
extern "C"  int32_t Gesture_get_State_m3157652344 (Gesture_t1589572905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gesture::set_State(GestureRecognitionState)
extern "C"  void Gesture_set_State_m3236968059 (Gesture_t1589572905 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GestureRecognitionState Gesture::get_PreviousState()
extern "C"  int32_t Gesture_get_PreviousState_m3646314113 (Gesture_t1589572905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Gesture::get_ElapsedTime()
extern "C"  float Gesture_get_ElapsedTime_m1051477900 (Gesture_t1589572905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Gesture::get_StartSelection()
extern "C"  GameObject_t3674682005 * Gesture_get_StartSelection_m988830432 (Gesture_t1589572905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gesture::set_StartSelection(UnityEngine.GameObject)
extern "C"  void Gesture_set_StartSelection_m3674641569 (Gesture_t1589572905 * __this, GameObject_t3674682005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Gesture::get_Selection()
extern "C"  GameObject_t3674682005 * Gesture_get_Selection_m2085766488 (Gesture_t1589572905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gesture::set_Selection(UnityEngine.GameObject)
extern "C"  void Gesture_set_Selection_m1954834151 (Gesture_t1589572905 * __this, GameObject_t3674682005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ScreenRaycastData Gesture::get_Raycast()
extern "C"  ScreenRaycastData_t1110901127  Gesture_get_Raycast_m1292292716 (Gesture_t1589572905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gesture::set_Raycast(ScreenRaycastData)
extern "C"  void Gesture_set_Raycast_m1646983999 (Gesture_t1589572905 * __this, ScreenRaycastData_t1110901127  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Gesture::PickObject(ScreenRaycaster,UnityEngine.Vector2)
extern "C"  GameObject_t3674682005 * Gesture_PickObject_m2988082767 (Gesture_t1589572905 * __this, ScreenRaycaster_t4188861866 * ___raycaster0, Vector2_t4282066565  ___screenPos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gesture::PickStartSelection(ScreenRaycaster)
extern "C"  void Gesture_PickStartSelection_m2468673915 (Gesture_t1589572905 * __this, ScreenRaycaster_t4188861866 * ___raycaster0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gesture::PickSelection(ScreenRaycaster)
extern "C"  void Gesture_PickSelection_m4111963517 (Gesture_t1589572905 * __this, ScreenRaycaster_t4188861866 * ___raycaster0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Gesture::op_Implicit(Gesture)
extern "C"  bool Gesture_op_Implicit_m1153246354 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Gesture::ilo_Raycast1(ScreenRaycaster,UnityEngine.Vector2,ScreenRaycastData&)
extern "C"  bool Gesture_ilo_Raycast1_m2904712884 (Il2CppObject * __this /* static, unused */, ScreenRaycaster_t4188861866 * ____this0, Vector2_t4282066565  ___screenPos1, ScreenRaycastData_t1110901127 * ___hitData2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject Gesture::ilo_get_GameObject2(ScreenRaycastData&)
extern "C"  GameObject_t3674682005 * Gesture_ilo_get_GameObject2_m1992200859 (Il2CppObject * __this /* static, unused */, ScreenRaycastData_t1110901127 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 Gesture::ilo_get_Position3(Gesture)
extern "C"  Vector2_t4282066565  Gesture_ilo_get_Position3_m1081369280 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Gesture::ilo_set_Selection4(Gesture,UnityEngine.GameObject)
extern "C"  void Gesture_ilo_set_Selection4_m1197780335 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, GameObject_t3674682005 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
