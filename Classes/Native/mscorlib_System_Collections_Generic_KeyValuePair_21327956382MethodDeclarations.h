﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,JSMgr/JS_CS_Rel>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2437517053(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1327956382 *, Il2CppObject *, JS_CS_Rel_t3554103776 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,JSMgr/JS_CS_Rel>::get_Key()
#define KeyValuePair_2_get_Key_m3711681483(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1327956382 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,JSMgr/JS_CS_Rel>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2648547596(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1327956382 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,JSMgr/JS_CS_Rel>::get_Value()
#define KeyValuePair_2_get_Value_m479742255(__this, method) ((  JS_CS_Rel_t3554103776 * (*) (KeyValuePair_2_t1327956382 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,JSMgr/JS_CS_Rel>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m959024908(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1327956382 *, JS_CS_Rel_t3554103776 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,JSMgr/JS_CS_Rel>::ToString()
#define KeyValuePair_2_ToString_m1591322812(__this, method) ((  String_t* (*) (KeyValuePair_2_t1327956382 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
