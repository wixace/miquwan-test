﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Boss
struct Boss_t2076557;

#include "codegen/il2cpp-codegen.h"

// System.Void Boss::.ctor()
extern "C"  void Boss__ctor_m1896352174 (Boss_t2076557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boss::.cctor()
extern "C"  void Boss__cctor_m2470246335 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boss::AfterAttack()
extern "C"  void Boss_AfterAttack_m1198650576 (Boss_t2076557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Boss::Dead()
extern "C"  void Boss_Dead_m2354850106 (Boss_t2076557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
