﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Poly2Tri.PolygonPoint
struct PolygonPoint_t2222827754;

#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3810082933.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Poly2Tri.PolygonPoint
struct  PolygonPoint_t2222827754  : public TriangulationPoint_t3810082933
{
public:
	// Pathfinding.Poly2Tri.PolygonPoint Pathfinding.Poly2Tri.PolygonPoint::<Next>k__BackingField
	PolygonPoint_t2222827754 * ___U3CNextU3Ek__BackingField_3;
	// Pathfinding.Poly2Tri.PolygonPoint Pathfinding.Poly2Tri.PolygonPoint::<Previous>k__BackingField
	PolygonPoint_t2222827754 * ___U3CPreviousU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CNextU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PolygonPoint_t2222827754, ___U3CNextU3Ek__BackingField_3)); }
	inline PolygonPoint_t2222827754 * get_U3CNextU3Ek__BackingField_3() const { return ___U3CNextU3Ek__BackingField_3; }
	inline PolygonPoint_t2222827754 ** get_address_of_U3CNextU3Ek__BackingField_3() { return &___U3CNextU3Ek__BackingField_3; }
	inline void set_U3CNextU3Ek__BackingField_3(PolygonPoint_t2222827754 * value)
	{
		___U3CNextU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CNextU3Ek__BackingField_3, value);
	}

	inline static int32_t get_offset_of_U3CPreviousU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PolygonPoint_t2222827754, ___U3CPreviousU3Ek__BackingField_4)); }
	inline PolygonPoint_t2222827754 * get_U3CPreviousU3Ek__BackingField_4() const { return ___U3CPreviousU3Ek__BackingField_4; }
	inline PolygonPoint_t2222827754 ** get_address_of_U3CPreviousU3Ek__BackingField_4() { return &___U3CPreviousU3Ek__BackingField_4; }
	inline void set_U3CPreviousU3Ek__BackingField_4(PolygonPoint_t2222827754 * value)
	{
		___U3CPreviousU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CPreviousU3Ek__BackingField_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
