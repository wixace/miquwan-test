﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.WeakReference>
struct Dictionary_2_t3019897867;
// System.Collections.Generic.List`1<Blood>
struct List_1_t1432465578;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "AssemblyU2DCSharp_Singleton_1_gen3958700543.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BloodMgr
struct  BloodMgr_t3705885150  : public Singleton_1_t3958700543
{
public:
	// System.Collections.Generic.List`1<Blood> BloodMgr::bloodList
	List_1_t1432465578 * ___bloodList_2;
	// UnityEngine.GameObject BloodMgr::_parentGO
	GameObject_t3674682005 * ____parentGO_3;

public:
	inline static int32_t get_offset_of_bloodList_2() { return static_cast<int32_t>(offsetof(BloodMgr_t3705885150, ___bloodList_2)); }
	inline List_1_t1432465578 * get_bloodList_2() const { return ___bloodList_2; }
	inline List_1_t1432465578 ** get_address_of_bloodList_2() { return &___bloodList_2; }
	inline void set_bloodList_2(List_1_t1432465578 * value)
	{
		___bloodList_2 = value;
		Il2CppCodeGenWriteBarrier(&___bloodList_2, value);
	}

	inline static int32_t get_offset_of__parentGO_3() { return static_cast<int32_t>(offsetof(BloodMgr_t3705885150, ____parentGO_3)); }
	inline GameObject_t3674682005 * get__parentGO_3() const { return ____parentGO_3; }
	inline GameObject_t3674682005 ** get_address_of__parentGO_3() { return &____parentGO_3; }
	inline void set__parentGO_3(GameObject_t3674682005 * value)
	{
		____parentGO_3 = value;
		Il2CppCodeGenWriteBarrier(&____parentGO_3, value);
	}
};

struct BloodMgr_t3705885150_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.WeakReference> BloodMgr::debugList
	Dictionary_2_t3019897867 * ___debugList_1;

public:
	inline static int32_t get_offset_of_debugList_1() { return static_cast<int32_t>(offsetof(BloodMgr_t3705885150_StaticFields, ___debugList_1)); }
	inline Dictionary_2_t3019897867 * get_debugList_1() const { return ___debugList_1; }
	inline Dictionary_2_t3019897867 ** get_address_of_debugList_1() { return &___debugList_1; }
	inline void set_debugList_1(Dictionary_2_t3019897867 * value)
	{
		___debugList_1 = value;
		Il2CppCodeGenWriteBarrier(&___debugList_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
