﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SharpKit.JavaScript.JsFunc`3<System.Object,System.Object,System.Object>
struct JsFunc_3_t2543207488;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void SharpKit.JavaScript.JsFunc`3<System.Object,System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void JsFunc_3__ctor_m3119373905_gshared (JsFunc_3_t2543207488 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define JsFunc_3__ctor_m3119373905(__this, ___object0, ___method1, method) ((  void (*) (JsFunc_3_t2543207488 *, Il2CppObject *, IntPtr_t, const MethodInfo*))JsFunc_3__ctor_m3119373905_gshared)(__this, ___object0, ___method1, method)
// TResult SharpKit.JavaScript.JsFunc`3<System.Object,System.Object,System.Object>::Invoke(T1,T2)
extern "C"  Il2CppObject * JsFunc_3_Invoke_m366452956_gshared (JsFunc_3_t2543207488 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method);
#define JsFunc_3_Invoke_m366452956(__this, ___arg10, ___arg21, method) ((  Il2CppObject * (*) (JsFunc_3_t2543207488 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))JsFunc_3_Invoke_m366452956_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult SharpKit.JavaScript.JsFunc`3<System.Object,System.Object,System.Object>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * JsFunc_3_BeginInvoke_m1181892065_gshared (JsFunc_3_t2543207488 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method);
#define JsFunc_3_BeginInvoke_m1181892065(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (JsFunc_3_t2543207488 *, Il2CppObject *, Il2CppObject *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))JsFunc_3_BeginInvoke_m1181892065_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// TResult SharpKit.JavaScript.JsFunc`3<System.Object,System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * JsFunc_3_EndInvoke_m3327674751_gshared (JsFunc_3_t2543207488 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define JsFunc_3_EndInvoke_m3327674751(__this, ___result0, method) ((  Il2CppObject * (*) (JsFunc_3_t2543207488 *, Il2CppObject *, const MethodInfo*))JsFunc_3_EndInvoke_m3327674751_gshared)(__this, ___result0, method)
