﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Pathfinding.Int2>
struct List_1_t3342231145;
// System.Collections.Generic.IEnumerable`1<Pathfinding.Int2>
struct IEnumerable_1_t979991254;
// System.Collections.Generic.IEnumerator`1<Pathfinding.Int2>
struct IEnumerator_1_t3885910642;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<Pathfinding.Int2>
struct ICollection_1_t2868635580;
// System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.Int2>
struct ReadOnlyCollection_1_t3531123129;
// Pathfinding.Int2[]
struct Int2U5BU5D_t30096868;
// System.Predicate`1<Pathfinding.Int2>
struct Predicate_1_t1585102476;
// System.Collections.Generic.IComparer`1<Pathfinding.Int2>
struct IComparer_1_t254092339;
// System.Comparison`1<Pathfinding.Int2>
struct Comparison_1_t690406780;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera3361903915.h"

// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::.ctor()
extern "C"  void List_1__ctor_m1321104321_gshared (List_1_t3342231145 * __this, const MethodInfo* method);
#define List_1__ctor_m1321104321(__this, method) ((  void (*) (List_1_t3342231145 *, const MethodInfo*))List_1__ctor_m1321104321_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m408488478_gshared (List_1_t3342231145 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m408488478(__this, ___collection0, method) ((  void (*) (List_1_t3342231145 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m408488478_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3841953682_gshared (List_1_t3342231145 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m3841953682(__this, ___capacity0, method) ((  void (*) (List_1_t3342231145 *, int32_t, const MethodInfo*))List_1__ctor_m3841953682_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::.cctor()
extern "C"  void List_1__cctor_m1817432076_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m1817432076(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m1817432076_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Pathfinding.Int2>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m758374795_gshared (List_1_t3342231145 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m758374795(__this, method) ((  Il2CppObject* (*) (List_1_t3342231145 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m758374795_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m644905123_gshared (List_1_t3342231145 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m644905123(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3342231145 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m644905123_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Pathfinding.Int2>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1203847026_gshared (List_1_t3342231145 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1203847026(__this, method) ((  Il2CppObject * (*) (List_1_t3342231145 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1203847026_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Int2>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m429619211_gshared (List_1_t3342231145 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m429619211(__this, ___item0, method) ((  int32_t (*) (List_1_t3342231145 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m429619211_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Int2>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m370633237_gshared (List_1_t3342231145 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m370633237(__this, ___item0, method) ((  bool (*) (List_1_t3342231145 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m370633237_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Int2>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m519745763_gshared (List_1_t3342231145 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m519745763(__this, ___item0, method) ((  int32_t (*) (List_1_t3342231145 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m519745763_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m1286631894_gshared (List_1_t3342231145 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1286631894(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3342231145 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1286631894_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m3915445138_gshared (List_1_t3342231145 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m3915445138(__this, ___item0, method) ((  void (*) (List_1_t3342231145 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m3915445138_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Int2>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m768181206_gshared (List_1_t3342231145 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m768181206(__this, method) ((  bool (*) (List_1_t3342231145 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m768181206_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Int2>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3463788839_gshared (List_1_t3342231145 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3463788839(__this, method) ((  bool (*) (List_1_t3342231145 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3463788839_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.Int2>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1626945753_gshared (List_1_t3342231145 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1626945753(__this, method) ((  Il2CppObject * (*) (List_1_t3342231145 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1626945753_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Int2>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m2888404292_gshared (List_1_t3342231145 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m2888404292(__this, method) ((  bool (*) (List_1_t3342231145 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2888404292_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Int2>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m270088501_gshared (List_1_t3342231145 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m270088501(__this, method) ((  bool (*) (List_1_t3342231145 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m270088501_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.Int2>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1192384544_gshared (List_1_t3342231145 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1192384544(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t3342231145 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1192384544_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m116621357_gshared (List_1_t3342231145 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m116621357(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3342231145 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m116621357_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::Add(T)
extern "C"  void List_1_Add_m3772998824_gshared (List_1_t3342231145 * __this, Int2_t1974045593  ___item0, const MethodInfo* method);
#define List_1_Add_m3772998824(__this, ___item0, method) ((  void (*) (List_1_t3342231145 *, Int2_t1974045593 , const MethodInfo*))List_1_Add_m3772998824_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m2415739865_gshared (List_1_t3342231145 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m2415739865(__this, ___newCount0, method) ((  void (*) (List_1_t3342231145 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m2415739865_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m1412050542_gshared (List_1_t3342231145 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m1412050542(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t3342231145 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1412050542_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2868901591_gshared (List_1_t3342231145 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2868901591(__this, ___collection0, method) ((  void (*) (List_1_t3342231145 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2868901591_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m2129873815_gshared (List_1_t3342231145 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m2129873815(__this, ___enumerable0, method) ((  void (*) (List_1_t3342231145 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m2129873815_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m1393852576_gshared (List_1_t3342231145 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m1393852576(__this, ___collection0, method) ((  void (*) (List_1_t3342231145 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m1393852576_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Pathfinding.Int2>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t3531123129 * List_1_AsReadOnly_m77117541_gshared (List_1_t3342231145 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m77117541(__this, method) ((  ReadOnlyCollection_1_t3531123129 * (*) (List_1_t3342231145 *, const MethodInfo*))List_1_AsReadOnly_m77117541_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Int2>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m1267786446_gshared (List_1_t3342231145 * __this, Int2_t1974045593  ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m1267786446(__this, ___item0, method) ((  int32_t (*) (List_1_t3342231145 *, Int2_t1974045593 , const MethodInfo*))List_1_BinarySearch_m1267786446_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::Clear()
extern "C"  void List_1_Clear_m3022204908_gshared (List_1_t3342231145 * __this, const MethodInfo* method);
#define List_1_Clear_m3022204908(__this, method) ((  void (*) (List_1_t3342231145 *, const MethodInfo*))List_1_Clear_m3022204908_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Int2>::Contains(T)
extern "C"  bool List_1_Contains_m1236895198_gshared (List_1_t3342231145 * __this, Int2_t1974045593  ___item0, const MethodInfo* method);
#define List_1_Contains_m1236895198(__this, ___item0, method) ((  bool (*) (List_1_t3342231145 *, Int2_t1974045593 , const MethodInfo*))List_1_Contains_m1236895198_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m294964494_gshared (List_1_t3342231145 * __this, Int2U5BU5D_t30096868* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m294964494(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t3342231145 *, Int2U5BU5D_t30096868*, int32_t, const MethodInfo*))List_1_CopyTo_m294964494_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Pathfinding.Int2>::Find(System.Predicate`1<T>)
extern "C"  Int2_t1974045593  List_1_Find_m2501136696_gshared (List_1_t3342231145 * __this, Predicate_1_t1585102476 * ___match0, const MethodInfo* method);
#define List_1_Find_m2501136696(__this, ___match0, method) ((  Int2_t1974045593  (*) (List_1_t3342231145 *, Predicate_1_t1585102476 *, const MethodInfo*))List_1_Find_m2501136696_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m247996821_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t1585102476 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m247996821(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1585102476 *, const MethodInfo*))List_1_CheckMatch_m247996821_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Int2>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1316790898_gshared (List_1_t3342231145 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t1585102476 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1316790898(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t3342231145 *, int32_t, int32_t, Predicate_1_t1585102476 *, const MethodInfo*))List_1_GetIndex_m1316790898_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Pathfinding.Int2>::GetEnumerator()
extern "C"  Enumerator_t3361903915  List_1_GetEnumerator_m1276454939_gshared (List_1_t3342231145 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m1276454939(__this, method) ((  Enumerator_t3361903915  (*) (List_1_t3342231145 *, const MethodInfo*))List_1_GetEnumerator_m1276454939_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Int2>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2935640410_gshared (List_1_t3342231145 * __this, Int2_t1974045593  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2935640410(__this, ___item0, method) ((  int32_t (*) (List_1_t3342231145 *, Int2_t1974045593 , const MethodInfo*))List_1_IndexOf_m2935640410_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2458760549_gshared (List_1_t3342231145 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2458760549(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t3342231145 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2458760549_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m41331678_gshared (List_1_t3342231145 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m41331678(__this, ___index0, method) ((  void (*) (List_1_t3342231145 *, int32_t, const MethodInfo*))List_1_CheckIndex_m41331678_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m478072581_gshared (List_1_t3342231145 * __this, int32_t ___index0, Int2_t1974045593  ___item1, const MethodInfo* method);
#define List_1_Insert_m478072581(__this, ___index0, ___item1, method) ((  void (*) (List_1_t3342231145 *, int32_t, Int2_t1974045593 , const MethodInfo*))List_1_Insert_m478072581_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m172093370_gshared (List_1_t3342231145 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m172093370(__this, ___collection0, method) ((  void (*) (List_1_t3342231145 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m172093370_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.Int2>::Remove(T)
extern "C"  bool List_1_Remove_m2360485209_gshared (List_1_t3342231145 * __this, Int2_t1974045593  ___item0, const MethodInfo* method);
#define List_1_Remove_m2360485209(__this, ___item0, method) ((  bool (*) (List_1_t3342231145 *, Int2_t1974045593 , const MethodInfo*))List_1_Remove_m2360485209_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Int2>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m3682744733_gshared (List_1_t3342231145 * __this, Predicate_1_t1585102476 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m3682744733(__this, ___match0, method) ((  int32_t (*) (List_1_t3342231145 *, Predicate_1_t1585102476 *, const MethodInfo*))List_1_RemoveAll_m3682744733_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m2646892747_gshared (List_1_t3342231145 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m2646892747(__this, ___index0, method) ((  void (*) (List_1_t3342231145 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2646892747_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m559105774_gshared (List_1_t3342231145 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m559105774(__this, ___index0, ___count1, method) ((  void (*) (List_1_t3342231145 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m559105774_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::Reverse()
extern "C"  void List_1_Reverse_m2603711425_gshared (List_1_t3342231145 * __this, const MethodInfo* method);
#define List_1_Reverse_m2603711425(__this, method) ((  void (*) (List_1_t3342231145 *, const MethodInfo*))List_1_Reverse_m2603711425_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::Sort()
extern "C"  void List_1_Sort_m2082751361_gshared (List_1_t3342231145 * __this, const MethodInfo* method);
#define List_1_Sort_m2082751361(__this, method) ((  void (*) (List_1_t3342231145 *, const MethodInfo*))List_1_Sort_m2082751361_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m1122510723_gshared (List_1_t3342231145 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m1122510723(__this, ___comparer0, method) ((  void (*) (List_1_t3342231145 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m1122510723_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m281299220_gshared (List_1_t3342231145 * __this, Comparison_1_t690406780 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m281299220(__this, ___comparison0, method) ((  void (*) (List_1_t3342231145 *, Comparison_1_t690406780 *, const MethodInfo*))List_1_Sort_m281299220_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Pathfinding.Int2>::ToArray()
extern "C"  Int2U5BU5D_t30096868* List_1_ToArray_m2911272282_gshared (List_1_t3342231145 * __this, const MethodInfo* method);
#define List_1_ToArray_m2911272282(__this, method) ((  Int2U5BU5D_t30096868* (*) (List_1_t3342231145 *, const MethodInfo*))List_1_ToArray_m2911272282_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2732129882_gshared (List_1_t3342231145 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2732129882(__this, method) ((  void (*) (List_1_t3342231145 *, const MethodInfo*))List_1_TrimExcess_m2732129882_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Int2>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m4236119178_gshared (List_1_t3342231145 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m4236119178(__this, method) ((  int32_t (*) (List_1_t3342231145 *, const MethodInfo*))List_1_get_Capacity_m4236119178_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3920895979_gshared (List_1_t3342231145 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3920895979(__this, ___value0, method) ((  void (*) (List_1_t3342231145 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3920895979_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.Int2>::get_Count()
extern "C"  int32_t List_1_get_Count_m627847009_gshared (List_1_t3342231145 * __this, const MethodInfo* method);
#define List_1_get_Count_m627847009(__this, method) ((  int32_t (*) (List_1_t3342231145 *, const MethodInfo*))List_1_get_Count_m627847009_gshared)(__this, method)
// T System.Collections.Generic.List`1<Pathfinding.Int2>::get_Item(System.Int32)
extern "C"  Int2_t1974045593  List_1_get_Item_m3080291019_gshared (List_1_t3342231145 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3080291019(__this, ___index0, method) ((  Int2_t1974045593  (*) (List_1_t3342231145 *, int32_t, const MethodInfo*))List_1_get_Item_m3080291019_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.Int2>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1675846300_gshared (List_1_t3342231145 * __this, int32_t ___index0, Int2_t1974045593  ___value1, const MethodInfo* method);
#define List_1_set_Item_m1675846300(__this, ___index0, ___value1, method) ((  void (*) (List_1_t3342231145 *, int32_t, Int2_t1974045593 , const MethodInfo*))List_1_set_Item_m1675846300_gshared)(__this, ___index0, ___value1, method)
