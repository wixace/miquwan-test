﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VortexEffect
struct VortexEffect_t1608244223;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void VortexEffect::.ctor()
extern "C"  void VortexEffect__ctor_m4273669680 (VortexEffect_t1608244223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VortexEffect::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void VortexEffect_OnRenderImage_m1926518830 (VortexEffect_t1608244223 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
