﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Text_DecoderGenerated
struct System_Text_DecoderGenerated_t4123399309;
// JSVCall
struct JSVCall_t3708497963;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Char[]
struct CharU5BU5D_t3324145743;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void System_Text_DecoderGenerated::.ctor()
extern "C"  void System_Text_DecoderGenerated__ctor_m2246343854 (System_Text_DecoderGenerated_t4123399309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_DecoderGenerated::Decoder_Fallback(JSVCall)
extern "C"  void System_Text_DecoderGenerated_Decoder_Fallback_m3042398612 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_DecoderGenerated::Decoder_FallbackBuffer(JSVCall)
extern "C"  void System_Text_DecoderGenerated_Decoder_FallbackBuffer_m3671947956 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_DecoderGenerated::Decoder_Convert__Byte_Array__Int32__Int32__Char_Array__Int32__Int32__Boolean__Int32__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool System_Text_DecoderGenerated_Decoder_Convert__Byte_Array__Int32__Int32__Char_Array__Int32__Int32__Boolean__Int32__Int32__Boolean_m2891836050 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_DecoderGenerated::Decoder_GetCharCount__Byte_Array__Int32__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool System_Text_DecoderGenerated_Decoder_GetCharCount__Byte_Array__Int32__Int32__Boolean_m3031818288 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_DecoderGenerated::Decoder_GetCharCount__Byte_Array__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_DecoderGenerated_Decoder_GetCharCount__Byte_Array__Int32__Int32_m3355827770 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_DecoderGenerated::Decoder_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool System_Text_DecoderGenerated_Decoder_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32__Boolean_m1446854184 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_DecoderGenerated::Decoder_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32(JSVCall,System.Int32)
extern "C"  bool System_Text_DecoderGenerated_Decoder_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32_m3208014146 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Text_DecoderGenerated::Decoder_Reset(JSVCall,System.Int32)
extern "C"  bool System_Text_DecoderGenerated_Decoder_Reset_m3358972316 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_DecoderGenerated::__Register()
extern "C"  void System_Text_DecoderGenerated___Register_m2954214617 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_DecoderGenerated::<Decoder_Convert__Byte_Array__Int32__Int32__Char_Array__Int32__Int32__Boolean__Int32__Int32__Boolean>m__C7()
extern "C"  ByteU5BU5D_t4260760469* System_Text_DecoderGenerated_U3CDecoder_Convert__Byte_Array__Int32__Int32__Char_Array__Int32__Int32__Boolean__Int32__Int32__BooleanU3Em__C7_m4203655057 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System_Text_DecoderGenerated::<Decoder_Convert__Byte_Array__Int32__Int32__Char_Array__Int32__Int32__Boolean__Int32__Int32__Boolean>m__C8()
extern "C"  CharU5BU5D_t3324145743* System_Text_DecoderGenerated_U3CDecoder_Convert__Byte_Array__Int32__Int32__Char_Array__Int32__Int32__Boolean__Int32__Int32__BooleanU3Em__C8_m3301576644 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_DecoderGenerated::<Decoder_GetCharCount__Byte_Array__Int32__Int32__Boolean>m__C9()
extern "C"  ByteU5BU5D_t4260760469* System_Text_DecoderGenerated_U3CDecoder_GetCharCount__Byte_Array__Int32__Int32__BooleanU3Em__C9_m1793665841 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_DecoderGenerated::<Decoder_GetCharCount__Byte_Array__Int32__Int32>m__CA()
extern "C"  ByteU5BU5D_t4260760469* System_Text_DecoderGenerated_U3CDecoder_GetCharCount__Byte_Array__Int32__Int32U3Em__CA_m3763281003 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_DecoderGenerated::<Decoder_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32__Boolean>m__CB()
extern "C"  ByteU5BU5D_t4260760469* System_Text_DecoderGenerated_U3CDecoder_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32__BooleanU3Em__CB_m3464223898 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System_Text_DecoderGenerated::<Decoder_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32__Boolean>m__CC()
extern "C"  CharU5BU5D_t3324145743* System_Text_DecoderGenerated_U3CDecoder_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32__BooleanU3Em__CC_m585338537 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_Text_DecoderGenerated::<Decoder_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32>m__CD()
extern "C"  ByteU5BU5D_t4260760469* System_Text_DecoderGenerated_U3CDecoder_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32U3Em__CD_m4224615438 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] System_Text_DecoderGenerated::<Decoder_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32>m__CE()
extern "C"  CharU5BU5D_t3324145743* System_Text_DecoderGenerated_U3CDecoder_GetChars__Byte_Array__Int32__Int32__Char_Array__Int32U3Em__CE_m3471894977 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_DecoderGenerated::ilo_incArgIndex1()
extern "C"  int32_t System_Text_DecoderGenerated_ilo_incArgIndex1_m3298684366 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_DecoderGenerated::ilo_setArgIndex2(System.Int32)
extern "C"  void System_Text_DecoderGenerated_ilo_setArgIndex2_m2174543438 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_DecoderGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void System_Text_DecoderGenerated_ilo_setBooleanS3_m1660437469 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_DecoderGenerated::ilo_getInt324(System.Int32)
extern "C"  int32_t System_Text_DecoderGenerated_ilo_getInt324_m1569579448 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Text_DecoderGenerated::ilo_setInt325(System.Int32,System.Int32)
extern "C"  void System_Text_DecoderGenerated_ilo_setInt325_m3022389860 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_DecoderGenerated::ilo_getArrayLength6(System.Int32)
extern "C"  int32_t System_Text_DecoderGenerated_ilo_getArrayLength6_m771661321 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System_Text_DecoderGenerated::ilo_getByte7(System.Int32)
extern "C"  uint8_t System_Text_DecoderGenerated_ilo_getByte7_m10753471 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System_Text_DecoderGenerated::ilo_getChar8(System.Int32)
extern "C"  int16_t System_Text_DecoderGenerated_ilo_getChar8_m3878338542 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_DecoderGenerated::ilo_getObject9(System.Int32)
extern "C"  int32_t System_Text_DecoderGenerated_ilo_getObject9_m2568251436 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Text_DecoderGenerated::ilo_getElement10(System.Int32,System.Int32)
extern "C"  int32_t System_Text_DecoderGenerated_ilo_getElement10_m3977054794 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
