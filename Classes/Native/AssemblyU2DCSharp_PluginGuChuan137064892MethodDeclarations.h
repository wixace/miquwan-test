﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginGuChuan
struct PluginGuChuan_t137064892;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PluginGuChuan137064892.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void PluginGuChuan::.ctor()
extern "C"  void PluginGuChuan__ctor_m2949456687 (PluginGuChuan_t137064892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::Init()
extern "C"  void PluginGuChuan_Init_m46680389 (PluginGuChuan_t137064892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginGuChuan::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginGuChuan_ReqSDKHttpLogin_m2923990624 (PluginGuChuan_t137064892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginGuChuan::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginGuChuan_IsLoginSuccess_m3762678876 (PluginGuChuan_t137064892 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::OpenUserLogin()
extern "C"  void PluginGuChuan_OpenUserLogin_m1615046273 (PluginGuChuan_t137064892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::UserPay(CEvent.ZEvent)
extern "C"  void PluginGuChuan_UserPay_m2404436433 (PluginGuChuan_t137064892 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginGuChuan::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginGuChuan_BuildOrderParam2WWWForm_m1416142932 (PluginGuChuan_t137064892 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::CreateRole(CEvent.ZEvent)
extern "C"  void PluginGuChuan_CreateRole_m1236349844 (PluginGuChuan_t137064892 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::EnterGame(CEvent.ZEvent)
extern "C"  void PluginGuChuan_EnterGame_m2187553572 (PluginGuChuan_t137064892 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginGuChuan_RoleUpgrade_m2335438024 (PluginGuChuan_t137064892 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::OnLoginSuccess(System.String)
extern "C"  void PluginGuChuan_OnLoginSuccess_m1002044660 (PluginGuChuan_t137064892 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::OnLogoutSuccess(System.String)
extern "C"  void PluginGuChuan_OnLogoutSuccess_m1002156059 (PluginGuChuan_t137064892 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::OnLogout(System.String)
extern "C"  void PluginGuChuan_OnLogout_m3261974404 (PluginGuChuan_t137064892 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::initSdk(System.String,System.String)
extern "C"  void PluginGuChuan_initSdk_m607742311 (PluginGuChuan_t137064892 * __this, String_t* ___appid0, String_t* ___sourceid1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::login()
extern "C"  void PluginGuChuan_login_m2471471510 (PluginGuChuan_t137064892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::logout()
extern "C"  void PluginGuChuan_logout_m3606995487 (PluginGuChuan_t137064892 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::creatRole(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginGuChuan_creatRole_m3938928286 (PluginGuChuan_t137064892 * __this, String_t* ___role_id0, String_t* ___role_name1, String_t* ___role_level2, String_t* ___gold3, String_t* ___vipLv4, String_t* ___server_id5, String_t* ___server_name6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::roleUpgrade(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginGuChuan_roleUpgrade_m3685840919 (PluginGuChuan_t137064892 * __this, String_t* ___role_id0, String_t* ___role_name1, String_t* ___role_level2, String_t* ___gold3, String_t* ___vipLv4, String_t* ___server_id5, String_t* ___server_name6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::pay(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginGuChuan_pay_m1684727549 (PluginGuChuan_t137064892 * __this, String_t* ___productId0, String_t* ___productName1, String_t* ___price2, String_t* ___orderId3, String_t* ___extra4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::<OnLogoutSuccess>m__423()
extern "C"  void PluginGuChuan_U3COnLogoutSuccessU3Em__423_m72404245 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::<OnLogout>m__424()
extern "C"  void PluginGuChuan_U3COnLogoutU3Em__424_m1083159639 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginGuChuan_ilo_AddEventListener1_m3174966245 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::ilo_initSdk2(PluginGuChuan,System.String,System.String)
extern "C"  void PluginGuChuan_ilo_initSdk2_m884707070 (Il2CppObject * __this /* static, unused */, PluginGuChuan_t137064892 * ____this0, String_t* ___appid1, String_t* ___sourceid2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginGuChuan::ilo_get_Instance3()
extern "C"  VersionMgr_t1322950208 * PluginGuChuan_ilo_get_Instance3_m2552326412 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginGuChuan::ilo_get_PluginsSdkMgr4()
extern "C"  PluginsSdkMgr_t3884624670 * PluginGuChuan_ilo_get_PluginsSdkMgr4_m2543654866 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginGuChuan::ilo_get_isSdkLogin5(VersionMgr)
extern "C"  bool PluginGuChuan_ilo_get_isSdkLogin5_m3631522465 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::ilo_login6(PluginGuChuan)
extern "C"  void PluginGuChuan_ilo_login6_m1656890017 (Il2CppObject * __this /* static, unused */, PluginGuChuan_t137064892 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.PayInfo PluginGuChuan::ilo_Parse7(System.Object[])
extern "C"  PayInfo_t1775308120 * PluginGuChuan_ilo_Parse7_m2681206445 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___payOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginGuChuan::ilo_GetProductID8(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginGuChuan_ilo_GetProductID8_m621657861 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::ilo_pay9(PluginGuChuan,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginGuChuan_ilo_pay9_m1470838569 (Il2CppObject * __this /* static, unused */, PluginGuChuan_t137064892 * ____this0, String_t* ___productId1, String_t* ___productName2, String_t* ___price3, String_t* ___orderId4, String_t* ___extra5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::ilo_creatRole10(PluginGuChuan,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginGuChuan_ilo_creatRole10_m3037971970 (Il2CppObject * __this /* static, unused */, PluginGuChuan_t137064892 * ____this0, String_t* ___role_id1, String_t* ___role_name2, String_t* ___role_level3, String_t* ___gold4, String_t* ___vipLv5, String_t* ___server_id6, String_t* ___server_name7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::ilo_ReqSDKHttpLogin11(PluginsSdkMgr)
extern "C"  void PluginGuChuan_ilo_ReqSDKHttpLogin11_m2908818805 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginGuChuan::ilo_Log12(System.Object,System.Boolean)
extern "C"  void PluginGuChuan_ilo_Log12_m265972424 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
