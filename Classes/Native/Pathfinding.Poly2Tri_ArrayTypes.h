﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// Pathfinding.Poly2Tri.TriangulationPoint
struct TriangulationPoint_t3810082933;
// Pathfinding.Poly2Tri.DelaunayTriangle
struct DelaunayTriangle_t2835103587;
// Pathfinding.Poly2Tri.Polygon
struct Polygon_t1047479568;
// Pathfinding.Poly2Tri.PolygonPoint
struct PolygonPoint_t2222827754;
// Pathfinding.Poly2Tri.DTSweepConstraint
struct DTSweepConstraint_t3360279023;

#include "mscorlib_System_Array1146569071.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3810082933.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Delaunay2835103587.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Polygon1047479568.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_PolygonP2222827754.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_DTSweepC3360279023.h"

#pragma once
// Pathfinding.Poly2Tri.TriangulationPoint[]
struct TriangulationPointU5BU5D_t4289080856  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TriangulationPoint_t3810082933 * m_Items[1];

public:
	inline TriangulationPoint_t3810082933 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TriangulationPoint_t3810082933 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TriangulationPoint_t3810082933 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.Poly2Tri.DelaunayTriangle[]
struct DelaunayTriangleU5BU5D_t3935625138  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DelaunayTriangle_t2835103587 * m_Items[1];

public:
	inline DelaunayTriangle_t2835103587 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DelaunayTriangle_t2835103587 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DelaunayTriangle_t2835103587 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.Poly2Tri.Polygon[]
struct PolygonU5BU5D_t75773105  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Polygon_t1047479568 * m_Items[1];

public:
	inline Polygon_t1047479568 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Polygon_t1047479568 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Polygon_t1047479568 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.Poly2Tri.PolygonPoint[]
struct PolygonPointU5BU5D_t1760007215  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PolygonPoint_t2222827754 * m_Items[1];

public:
	inline PolygonPoint_t2222827754 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PolygonPoint_t2222827754 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PolygonPoint_t2222827754 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.Poly2Tri.DTSweepConstraint[]
struct DTSweepConstraintU5BU5D_t1615789430  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DTSweepConstraint_t3360279023 * m_Items[1];

public:
	inline DTSweepConstraint_t3360279023 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline DTSweepConstraint_t3360279023 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, DTSweepConstraint_t3360279023 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
