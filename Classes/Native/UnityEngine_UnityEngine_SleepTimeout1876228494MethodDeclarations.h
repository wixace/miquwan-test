﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.SleepTimeout
struct SleepTimeout_t1876228494;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.SleepTimeout::.ctor()
extern "C"  void SleepTimeout__ctor_m457701667 (SleepTimeout_t1876228494 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
