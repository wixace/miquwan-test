﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._167c8098c7d6a54a509bf88d4308b1db
struct _167c8098c7d6a54a509bf88d4308b1db_t3666518910;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._167c8098c7d6a54a509bf88d4308b1db::.ctor()
extern "C"  void _167c8098c7d6a54a509bf88d4308b1db__ctor_m463835247 (_167c8098c7d6a54a509bf88d4308b1db_t3666518910 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._167c8098c7d6a54a509bf88d4308b1db::_167c8098c7d6a54a509bf88d4308b1dbm2(System.Int32)
extern "C"  int32_t _167c8098c7d6a54a509bf88d4308b1db__167c8098c7d6a54a509bf88d4308b1dbm2_m2736484569 (_167c8098c7d6a54a509bf88d4308b1db_t3666518910 * __this, int32_t ____167c8098c7d6a54a509bf88d4308b1dba0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._167c8098c7d6a54a509bf88d4308b1db::_167c8098c7d6a54a509bf88d4308b1dbm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _167c8098c7d6a54a509bf88d4308b1db__167c8098c7d6a54a509bf88d4308b1dbm_m1792466429 (_167c8098c7d6a54a509bf88d4308b1db_t3666518910 * __this, int32_t ____167c8098c7d6a54a509bf88d4308b1dba0, int32_t ____167c8098c7d6a54a509bf88d4308b1db861, int32_t ____167c8098c7d6a54a509bf88d4308b1dbc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
