﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2E`1<System.Object>
struct U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;

#include "codegen/il2cpp-codegen.h"

// System.Void ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2E`1<System.Object>::.ctor()
extern "C"  void U3CGetExtendedValuesU3Ec__Iterator2E_1__ctor_m1543935562_gshared (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * __this, const MethodInfo* method);
#define U3CGetExtendedValuesU3Ec__Iterator2E_1__ctor_m1543935562(__this, method) ((  void (*) (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 *, const MethodInfo*))U3CGetExtendedValuesU3Ec__Iterator2E_1__ctor_m1543935562_gshared)(__this, method)
// TValue ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2E`1<System.Object>::System.Collections.Generic.IEnumerator<TValue>.get_Current()
extern "C"  Il2CppObject * U3CGetExtendedValuesU3Ec__Iterator2E_1_System_Collections_Generic_IEnumeratorU3CTValueU3E_get_Current_m1377785279_gshared (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * __this, const MethodInfo* method);
#define U3CGetExtendedValuesU3Ec__Iterator2E_1_System_Collections_Generic_IEnumeratorU3CTValueU3E_get_Current_m1377785279(__this, method) ((  Il2CppObject * (*) (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 *, const MethodInfo*))U3CGetExtendedValuesU3Ec__Iterator2E_1_System_Collections_Generic_IEnumeratorU3CTValueU3E_get_Current_m1377785279_gshared)(__this, method)
// System.Object ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2E`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CGetExtendedValuesU3Ec__Iterator2E_1_System_Collections_IEnumerator_get_Current_m925193500_gshared (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * __this, const MethodInfo* method);
#define U3CGetExtendedValuesU3Ec__Iterator2E_1_System_Collections_IEnumerator_get_Current_m925193500(__this, method) ((  Il2CppObject * (*) (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 *, const MethodInfo*))U3CGetExtendedValuesU3Ec__Iterator2E_1_System_Collections_IEnumerator_get_Current_m925193500_gshared)(__this, method)
// System.Collections.IEnumerator ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2E`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CGetExtendedValuesU3Ec__Iterator2E_1_System_Collections_IEnumerable_GetEnumerator_m1009020887_gshared (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * __this, const MethodInfo* method);
#define U3CGetExtendedValuesU3Ec__Iterator2E_1_System_Collections_IEnumerable_GetEnumerator_m1009020887(__this, method) ((  Il2CppObject * (*) (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 *, const MethodInfo*))U3CGetExtendedValuesU3Ec__Iterator2E_1_System_Collections_IEnumerable_GetEnumerator_m1009020887_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TValue> ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2E`1<System.Object>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
extern "C"  Il2CppObject* U3CGetExtendedValuesU3Ec__Iterator2E_1_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2968560516_gshared (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * __this, const MethodInfo* method);
#define U3CGetExtendedValuesU3Ec__Iterator2E_1_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2968560516(__this, method) ((  Il2CppObject* (*) (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 *, const MethodInfo*))U3CGetExtendedValuesU3Ec__Iterator2E_1_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2968560516_gshared)(__this, method)
// System.Boolean ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2E`1<System.Object>::MoveNext()
extern "C"  bool U3CGetExtendedValuesU3Ec__Iterator2E_1_MoveNext_m1975364714_gshared (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * __this, const MethodInfo* method);
#define U3CGetExtendedValuesU3Ec__Iterator2E_1_MoveNext_m1975364714(__this, method) ((  bool (*) (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 *, const MethodInfo*))U3CGetExtendedValuesU3Ec__Iterator2E_1_MoveNext_m1975364714_gshared)(__this, method)
// System.Void ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2E`1<System.Object>::Dispose()
extern "C"  void U3CGetExtendedValuesU3Ec__Iterator2E_1_Dispose_m1856641031_gshared (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * __this, const MethodInfo* method);
#define U3CGetExtendedValuesU3Ec__Iterator2E_1_Dispose_m1856641031(__this, method) ((  void (*) (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 *, const MethodInfo*))U3CGetExtendedValuesU3Ec__Iterator2E_1_Dispose_m1856641031_gshared)(__this, method)
// System.Void ProtoBuf.ExtensibleUtil/<GetExtendedValues>c__Iterator2E`1<System.Object>::Reset()
extern "C"  void U3CGetExtendedValuesU3Ec__Iterator2E_1_Reset_m3485335799_gshared (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 * __this, const MethodInfo* method);
#define U3CGetExtendedValuesU3Ec__Iterator2E_1_Reset_m3485335799(__this, method) ((  void (*) (U3CGetExtendedValuesU3Ec__Iterator2E_1_t2058630907 *, const MethodInfo*))U3CGetExtendedValuesU3Ec__Iterator2E_1_Reset_m3485335799_gshared)(__this, method)
