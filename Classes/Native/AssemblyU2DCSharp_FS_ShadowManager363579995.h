﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FS_ShadowManager
struct FS_ShadowManager_t363579995;
// System.Collections.Hashtable
struct Hashtable_t1407064410;
// UnityEngine.Plane[]
struct PlaneU5BU5D_t1447812263;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FS_ShadowManager
struct  FS_ShadowManager_t363579995  : public MonoBehaviour_t667441552
{
public:
	// System.Collections.Hashtable FS_ShadowManager::shadowMeshes
	Hashtable_t1407064410 * ___shadowMeshes_3;
	// System.Collections.Hashtable FS_ShadowManager::shadowMeshesStatic
	Hashtable_t1407064410 * ___shadowMeshesStatic_4;
	// System.Int32 FS_ShadowManager::frameCalcedFustrum
	int32_t ___frameCalcedFustrum_5;
	// UnityEngine.Plane[] FS_ShadowManager::fustrumPlanes
	PlaneU5BU5D_t1447812263* ___fustrumPlanes_6;

public:
	inline static int32_t get_offset_of_shadowMeshes_3() { return static_cast<int32_t>(offsetof(FS_ShadowManager_t363579995, ___shadowMeshes_3)); }
	inline Hashtable_t1407064410 * get_shadowMeshes_3() const { return ___shadowMeshes_3; }
	inline Hashtable_t1407064410 ** get_address_of_shadowMeshes_3() { return &___shadowMeshes_3; }
	inline void set_shadowMeshes_3(Hashtable_t1407064410 * value)
	{
		___shadowMeshes_3 = value;
		Il2CppCodeGenWriteBarrier(&___shadowMeshes_3, value);
	}

	inline static int32_t get_offset_of_shadowMeshesStatic_4() { return static_cast<int32_t>(offsetof(FS_ShadowManager_t363579995, ___shadowMeshesStatic_4)); }
	inline Hashtable_t1407064410 * get_shadowMeshesStatic_4() const { return ___shadowMeshesStatic_4; }
	inline Hashtable_t1407064410 ** get_address_of_shadowMeshesStatic_4() { return &___shadowMeshesStatic_4; }
	inline void set_shadowMeshesStatic_4(Hashtable_t1407064410 * value)
	{
		___shadowMeshesStatic_4 = value;
		Il2CppCodeGenWriteBarrier(&___shadowMeshesStatic_4, value);
	}

	inline static int32_t get_offset_of_frameCalcedFustrum_5() { return static_cast<int32_t>(offsetof(FS_ShadowManager_t363579995, ___frameCalcedFustrum_5)); }
	inline int32_t get_frameCalcedFustrum_5() const { return ___frameCalcedFustrum_5; }
	inline int32_t* get_address_of_frameCalcedFustrum_5() { return &___frameCalcedFustrum_5; }
	inline void set_frameCalcedFustrum_5(int32_t value)
	{
		___frameCalcedFustrum_5 = value;
	}

	inline static int32_t get_offset_of_fustrumPlanes_6() { return static_cast<int32_t>(offsetof(FS_ShadowManager_t363579995, ___fustrumPlanes_6)); }
	inline PlaneU5BU5D_t1447812263* get_fustrumPlanes_6() const { return ___fustrumPlanes_6; }
	inline PlaneU5BU5D_t1447812263** get_address_of_fustrumPlanes_6() { return &___fustrumPlanes_6; }
	inline void set_fustrumPlanes_6(PlaneU5BU5D_t1447812263* value)
	{
		___fustrumPlanes_6 = value;
		Il2CppCodeGenWriteBarrier(&___fustrumPlanes_6, value);
	}
};

struct FS_ShadowManager_t363579995_StaticFields
{
public:
	// FS_ShadowManager FS_ShadowManager::_manager
	FS_ShadowManager_t363579995 * ____manager_2;

public:
	inline static int32_t get_offset_of__manager_2() { return static_cast<int32_t>(offsetof(FS_ShadowManager_t363579995_StaticFields, ____manager_2)); }
	inline FS_ShadowManager_t363579995 * get__manager_2() const { return ____manager_2; }
	inline FS_ShadowManager_t363579995 ** get_address_of__manager_2() { return &____manager_2; }
	inline void set__manager_2(FS_ShadowManager_t363579995 * value)
	{
		____manager_2 = value;
		Il2CppCodeGenWriteBarrier(&____manager_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
