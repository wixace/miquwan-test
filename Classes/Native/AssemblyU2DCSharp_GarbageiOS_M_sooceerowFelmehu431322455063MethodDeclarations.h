﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_sooceerowFelmehu43
struct M_sooceerowFelmehu43_t1322455063;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_sooceerowFelmehu431322455063.h"

// System.Void GarbageiOS.M_sooceerowFelmehu43::.ctor()
extern "C"  void M_sooceerowFelmehu43__ctor_m3281911292 (M_sooceerowFelmehu43_t1322455063 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sooceerowFelmehu43::M_jihaysiCellemke0(System.String[],System.Int32)
extern "C"  void M_sooceerowFelmehu43_M_jihaysiCellemke0_m4258075910 (M_sooceerowFelmehu43_t1322455063 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sooceerowFelmehu43::M_jeetusaPorha1(System.String[],System.Int32)
extern "C"  void M_sooceerowFelmehu43_M_jeetusaPorha1_m865884509 (M_sooceerowFelmehu43_t1322455063 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sooceerowFelmehu43::M_hereji2(System.String[],System.Int32)
extern "C"  void M_sooceerowFelmehu43_M_hereji2_m1425027618 (M_sooceerowFelmehu43_t1322455063 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sooceerowFelmehu43::ilo_M_jihaysiCellemke01(GarbageiOS.M_sooceerowFelmehu43,System.String[],System.Int32)
extern "C"  void M_sooceerowFelmehu43_ilo_M_jihaysiCellemke01_m674227627 (Il2CppObject * __this /* static, unused */, M_sooceerowFelmehu43_t1322455063 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sooceerowFelmehu43::ilo_M_jeetusaPorha12(GarbageiOS.M_sooceerowFelmehu43,System.String[],System.Int32)
extern "C"  void M_sooceerowFelmehu43_ilo_M_jeetusaPorha12_m3751391291 (Il2CppObject * __this /* static, unused */, M_sooceerowFelmehu43_t1322455063 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
