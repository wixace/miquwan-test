﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_IConvertibleGenerated
struct System_IConvertibleGenerated_t3092011651;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_IConvertibleGenerated::.ctor()
extern "C"  void System_IConvertibleGenerated__ctor_m3291384696 (System_IConvertibleGenerated_t3092011651 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IConvertibleGenerated::IConvertible_GetTypeCode(JSVCall,System.Int32)
extern "C"  bool System_IConvertibleGenerated_IConvertible_GetTypeCode_m1521186634 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IConvertibleGenerated::IConvertible_ToBoolean__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_IConvertibleGenerated_IConvertible_ToBoolean__IFormatProvider_m1817377945 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IConvertibleGenerated::IConvertible_ToByte__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_IConvertibleGenerated_IConvertible_ToByte__IFormatProvider_m2029650139 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IConvertibleGenerated::IConvertible_ToChar__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_IConvertibleGenerated_IConvertible_ToChar__IFormatProvider_m3710186189 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IConvertibleGenerated::IConvertible_ToDateTime__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_IConvertibleGenerated_IConvertible_ToDateTime__IFormatProvider_m320240232 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IConvertibleGenerated::IConvertible_ToDecimal__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_IConvertibleGenerated_IConvertible_ToDecimal__IFormatProvider_m2400127312 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IConvertibleGenerated::IConvertible_ToDouble__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_IConvertibleGenerated_IConvertible_ToDouble__IFormatProvider_m2048825778 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IConvertibleGenerated::IConvertible_ToInt16__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_IConvertibleGenerated_IConvertible_ToInt16__IFormatProvider_m2594606253 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IConvertibleGenerated::IConvertible_ToInt32__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_IConvertibleGenerated_IConvertible_ToInt32__IFormatProvider_m1466900275 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IConvertibleGenerated::IConvertible_ToInt64__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_IConvertibleGenerated_IConvertible_ToInt64__IFormatProvider_m2878046708 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IConvertibleGenerated::IConvertible_ToSByte__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_IConvertibleGenerated_IConvertible_ToSByte__IFormatProvider_m3255313670 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IConvertibleGenerated::IConvertible_ToSingle__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_IConvertibleGenerated_IConvertible_ToSingle__IFormatProvider_m3626994011 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IConvertibleGenerated::IConvertible_ToString__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_IConvertibleGenerated_IConvertible_ToString__IFormatProvider_m2745462130 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IConvertibleGenerated::IConvertible_ToType__Type__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_IConvertibleGenerated_IConvertible_ToType__Type__IFormatProvider_m2420973743 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IConvertibleGenerated::IConvertible_ToUInt16__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_IConvertibleGenerated_IConvertible_ToUInt16__IFormatProvider_m2828956388 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IConvertibleGenerated::IConvertible_ToUInt32__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_IConvertibleGenerated_IConvertible_ToUInt32__IFormatProvider_m1701250410 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IConvertibleGenerated::IConvertible_ToUInt64__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_IConvertibleGenerated_IConvertible_ToUInt64__IFormatProvider_m3112396843 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IConvertibleGenerated::__Register()
extern "C"  void System_IConvertibleGenerated___Register_m211246159 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_IConvertibleGenerated::ilo_getObject1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_IConvertibleGenerated_ilo_getObject1_m2817637367 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_IConvertibleGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_IConvertibleGenerated_ilo_setObject2_m3117671251 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IConvertibleGenerated::ilo_setDouble3(System.Int32,System.Double)
extern "C"  void System_IConvertibleGenerated_ilo_setDouble3_m1656167852 (Il2CppObject * __this /* static, unused */, int32_t ___e0, double ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IConvertibleGenerated::ilo_setInt164(System.Int32,System.Int16)
extern "C"  void System_IConvertibleGenerated_ilo_setInt164_m2449187131 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int16_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IConvertibleGenerated::ilo_setSByte5(System.Int32,System.SByte)
extern "C"  void System_IConvertibleGenerated_ilo_setSByte5_m1056866554 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int8_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IConvertibleGenerated::ilo_setSingle6(System.Int32,System.Single)
extern "C"  void System_IConvertibleGenerated_ilo_setSingle6_m3179712641 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
