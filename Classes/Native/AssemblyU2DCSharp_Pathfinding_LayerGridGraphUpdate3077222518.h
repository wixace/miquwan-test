﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Pathfinding_GraphUpdateObject430843704.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.LayerGridGraphUpdate
struct  LayerGridGraphUpdate_t3077222518  : public GraphUpdateObject_t430843704
{
public:
	// System.Boolean Pathfinding.LayerGridGraphUpdate::recalculateNodes
	bool ___recalculateNodes_16;
	// System.Boolean Pathfinding.LayerGridGraphUpdate::preserveExistingNodes
	bool ___preserveExistingNodes_17;

public:
	inline static int32_t get_offset_of_recalculateNodes_16() { return static_cast<int32_t>(offsetof(LayerGridGraphUpdate_t3077222518, ___recalculateNodes_16)); }
	inline bool get_recalculateNodes_16() const { return ___recalculateNodes_16; }
	inline bool* get_address_of_recalculateNodes_16() { return &___recalculateNodes_16; }
	inline void set_recalculateNodes_16(bool value)
	{
		___recalculateNodes_16 = value;
	}

	inline static int32_t get_offset_of_preserveExistingNodes_17() { return static_cast<int32_t>(offsetof(LayerGridGraphUpdate_t3077222518, ___preserveExistingNodes_17)); }
	inline bool get_preserveExistingNodes_17() const { return ___preserveExistingNodes_17; }
	inline bool* get_address_of_preserveExistingNodes_17() { return &___preserveExistingNodes_17; }
	inline void set_preserveExistingNodes_17(bool value)
	{
		___preserveExistingNodes_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
