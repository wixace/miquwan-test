﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo>
struct Dictionary_2_t2126409608;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProductsCfgMgr
struct  ProductsCfgMgr_t2493714872  : public Il2CppObject
{
public:
	// System.String ProductsCfgMgr::url
	String_t* ___url_0;
	// System.Collections.Generic.Dictionary`2<System.String,ProductsCfgMgr/ProductInfo> ProductsCfgMgr::infoDic
	Dictionary_2_t2126409608 * ___infoDic_1;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(ProductsCfgMgr_t2493714872, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_infoDic_1() { return static_cast<int32_t>(offsetof(ProductsCfgMgr_t2493714872, ___infoDic_1)); }
	inline Dictionary_2_t2126409608 * get_infoDic_1() const { return ___infoDic_1; }
	inline Dictionary_2_t2126409608 ** get_address_of_infoDic_1() { return &___infoDic_1; }
	inline void set_infoDic_1(Dictionary_2_t2126409608 * value)
	{
		___infoDic_1 = value;
		Il2CppCodeGenWriteBarrier(&___infoDic_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
