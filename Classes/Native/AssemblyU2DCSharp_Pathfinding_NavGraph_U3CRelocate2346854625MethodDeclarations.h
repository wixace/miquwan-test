﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.NavGraph/<RelocateNodes>c__AnonStorey114
struct U3CRelocateNodesU3Ec__AnonStorey114_t2346854625;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.NavGraph/<RelocateNodes>c__AnonStorey114::.ctor()
extern "C"  void U3CRelocateNodesU3Ec__AnonStorey114__ctor_m2284928730 (U3CRelocateNodesU3Ec__AnonStorey114_t2346854625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavGraph/<RelocateNodes>c__AnonStorey114::<>m__347(Pathfinding.GraphNode)
extern "C"  bool U3CRelocateNodesU3Ec__AnonStorey114_U3CU3Em__347_m2724750403 (U3CRelocateNodesU3Ec__AnonStorey114_t2346854625 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
