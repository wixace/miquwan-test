﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MethodID
struct MethodID_t3916401116;
// JSDataExchangeMgr/DGetV`1<GameObjectVisitor/condition>
struct DGetV_1_t3711178326;
// JSDataExchangeMgr/DGetV`1<System.Action`1<UnityEngine.GameObject>>
struct DGetV_1_t3948149482;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameObjectVisitorGenerated
struct  GameObjectVisitorGenerated_t1770478802  : public Il2CppObject
{
public:

public:
};

struct GameObjectVisitorGenerated_t1770478802_StaticFields
{
public:
	// MethodID GameObjectVisitorGenerated::methodID4
	MethodID_t3916401116 * ___methodID4_0;
	// JSDataExchangeMgr/DGetV`1<GameObjectVisitor/condition> GameObjectVisitorGenerated::<>f__am$cache1
	DGetV_1_t3711178326 * ___U3CU3Ef__amU24cache1_1;
	// JSDataExchangeMgr/DGetV`1<GameObjectVisitor/condition> GameObjectVisitorGenerated::<>f__am$cache2
	DGetV_1_t3711178326 * ___U3CU3Ef__amU24cache2_2;
	// JSDataExchangeMgr/DGetV`1<System.Action`1<UnityEngine.GameObject>> GameObjectVisitorGenerated::<>f__am$cache3
	DGetV_1_t3948149482 * ___U3CU3Ef__amU24cache3_3;
	// JSDataExchangeMgr/DGetV`1<System.Action`1<UnityEngine.GameObject>> GameObjectVisitorGenerated::<>f__am$cache4
	DGetV_1_t3948149482 * ___U3CU3Ef__amU24cache4_4;

public:
	inline static int32_t get_offset_of_methodID4_0() { return static_cast<int32_t>(offsetof(GameObjectVisitorGenerated_t1770478802_StaticFields, ___methodID4_0)); }
	inline MethodID_t3916401116 * get_methodID4_0() const { return ___methodID4_0; }
	inline MethodID_t3916401116 ** get_address_of_methodID4_0() { return &___methodID4_0; }
	inline void set_methodID4_0(MethodID_t3916401116 * value)
	{
		___methodID4_0 = value;
		Il2CppCodeGenWriteBarrier(&___methodID4_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(GameObjectVisitorGenerated_t1770478802_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline DGetV_1_t3711178326 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline DGetV_1_t3711178326 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(DGetV_1_t3711178326 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(GameObjectVisitorGenerated_t1770478802_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline DGetV_1_t3711178326 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline DGetV_1_t3711178326 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(DGetV_1_t3711178326 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_3() { return static_cast<int32_t>(offsetof(GameObjectVisitorGenerated_t1770478802_StaticFields, ___U3CU3Ef__amU24cache3_3)); }
	inline DGetV_1_t3948149482 * get_U3CU3Ef__amU24cache3_3() const { return ___U3CU3Ef__amU24cache3_3; }
	inline DGetV_1_t3948149482 ** get_address_of_U3CU3Ef__amU24cache3_3() { return &___U3CU3Ef__amU24cache3_3; }
	inline void set_U3CU3Ef__amU24cache3_3(DGetV_1_t3948149482 * value)
	{
		___U3CU3Ef__amU24cache3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(GameObjectVisitorGenerated_t1770478802_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline DGetV_1_t3948149482 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline DGetV_1_t3948149482 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(DGetV_1_t3948149482 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
