﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSApi/CSEntry
struct CSEntry_t1794636260;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void JSApi/CSEntry::.ctor(System.Object,System.IntPtr)
extern "C"  void CSEntry__ctor_m1412958139 (CSEntry_t1794636260 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi/CSEntry::Invoke(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  int32_t CSEntry_Invoke_m640888728 (CSEntry_t1794636260 * __this, int32_t ___op0, int32_t ___slot1, int32_t ___index2, int32_t ___bStatic3, int32_t ___argc4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult JSApi/CSEntry::BeginInvoke(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * CSEntry_BeginInvoke_m177115479 (CSEntry_t1794636260 * __this, int32_t ___op0, int32_t ___slot1, int32_t ___index2, int32_t ___bStatic3, int32_t ___argc4, AsyncCallback_t1369114871 * ___callback5, Il2CppObject * ___object6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSApi/CSEntry::EndInvoke(System.IAsyncResult)
extern "C"  int32_t CSEntry_EndInvoke_m1851288573 (CSEntry_t1794636260 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
