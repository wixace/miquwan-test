﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_normal
struct Float_normal_t2052312138;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// UILabel
struct UILabel_t291504320;
// System.String
struct String_t;
// UIWidget
struct UIWidget_t769069560;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"

// System.Void Float_normal::.ctor()
extern "C"  void Float_normal__ctor_m506747281 (Float_normal_t2052312138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_normal::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_normal_OnAwake_m40294989 (Float_normal_t2052312138 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_normal::OnDestroy()
extern "C"  void Float_normal_OnDestroy_m1647115786 (Float_normal_t2052312138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_normal::FloatID()
extern "C"  int32_t Float_normal_FloatID_m3548052509 (Float_normal_t2052312138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_normal::Init()
extern "C"  void Float_normal_Init_m3847208611 (Float_normal_t2052312138 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_normal::SetFile(System.Object[])
extern "C"  void Float_normal_SetFile_m1531345765 (Float_normal_t2052312138 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_normal::ilo_set_text1(UILabel,System.String)
extern "C"  void Float_normal_ilo_set_text1_m1138907851 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Float_normal::ilo_get_text2(UILabel)
extern "C"  String_t* Float_normal_ilo_get_text2_m3196175645 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Float_normal::ilo_get_height3(UIWidget)
extern "C"  int32_t Float_normal_ilo_get_height3_m4151175801 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_normal::ilo_SetDimensions4(UIWidget,System.Int32,System.Int32)
extern "C"  void Float_normal_ilo_SetDimensions4_m1247036403 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, int32_t ___w1, int32_t ___h2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
