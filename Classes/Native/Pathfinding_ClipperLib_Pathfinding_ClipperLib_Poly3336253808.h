﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.ClipperLib.PolyNode
struct PolyNode_t3336253808;
// System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>
struct List_1_t399344435;
// System.Collections.Generic.List`1<Pathfinding.ClipperLib.PolyNode>
struct List_1_t409472064;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.ClipperLib.PolyNode
struct  PolyNode_t3336253808  : public Il2CppObject
{
public:
	// Pathfinding.ClipperLib.PolyNode Pathfinding.ClipperLib.PolyNode::m_Parent
	PolyNode_t3336253808 * ___m_Parent_0;
	// System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint> Pathfinding.ClipperLib.PolyNode::m_polygon
	List_1_t399344435 * ___m_polygon_1;
	// System.Int32 Pathfinding.ClipperLib.PolyNode::m_Index
	int32_t ___m_Index_2;
	// System.Collections.Generic.List`1<Pathfinding.ClipperLib.PolyNode> Pathfinding.ClipperLib.PolyNode::m_Childs
	List_1_t409472064 * ___m_Childs_3;
	// System.Boolean Pathfinding.ClipperLib.PolyNode::<IsOpen>k__BackingField
	bool ___U3CIsOpenU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_m_Parent_0() { return static_cast<int32_t>(offsetof(PolyNode_t3336253808, ___m_Parent_0)); }
	inline PolyNode_t3336253808 * get_m_Parent_0() const { return ___m_Parent_0; }
	inline PolyNode_t3336253808 ** get_address_of_m_Parent_0() { return &___m_Parent_0; }
	inline void set_m_Parent_0(PolyNode_t3336253808 * value)
	{
		___m_Parent_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_Parent_0, value);
	}

	inline static int32_t get_offset_of_m_polygon_1() { return static_cast<int32_t>(offsetof(PolyNode_t3336253808, ___m_polygon_1)); }
	inline List_1_t399344435 * get_m_polygon_1() const { return ___m_polygon_1; }
	inline List_1_t399344435 ** get_address_of_m_polygon_1() { return &___m_polygon_1; }
	inline void set_m_polygon_1(List_1_t399344435 * value)
	{
		___m_polygon_1 = value;
		Il2CppCodeGenWriteBarrier(&___m_polygon_1, value);
	}

	inline static int32_t get_offset_of_m_Index_2() { return static_cast<int32_t>(offsetof(PolyNode_t3336253808, ___m_Index_2)); }
	inline int32_t get_m_Index_2() const { return ___m_Index_2; }
	inline int32_t* get_address_of_m_Index_2() { return &___m_Index_2; }
	inline void set_m_Index_2(int32_t value)
	{
		___m_Index_2 = value;
	}

	inline static int32_t get_offset_of_m_Childs_3() { return static_cast<int32_t>(offsetof(PolyNode_t3336253808, ___m_Childs_3)); }
	inline List_1_t409472064 * get_m_Childs_3() const { return ___m_Childs_3; }
	inline List_1_t409472064 ** get_address_of_m_Childs_3() { return &___m_Childs_3; }
	inline void set_m_Childs_3(List_1_t409472064 * value)
	{
		___m_Childs_3 = value;
		Il2CppCodeGenWriteBarrier(&___m_Childs_3, value);
	}

	inline static int32_t get_offset_of_U3CIsOpenU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PolyNode_t3336253808, ___U3CIsOpenU3Ek__BackingField_4)); }
	inline bool get_U3CIsOpenU3Ek__BackingField_4() const { return ___U3CIsOpenU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsOpenU3Ek__BackingField_4() { return &___U3CIsOpenU3Ek__BackingField_4; }
	inline void set_U3CIsOpenU3Ek__BackingField_4(bool value)
	{
		___U3CIsOpenU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
