﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.SDK.EnterGameInfo
struct EnterGameInfo_t1897532954;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"

// System.Void Mihua.SDK.EnterGameInfo::.ctor()
extern "C"  void EnterGameInfo__ctor_m3898820125 (EnterGameInfo_t1897532954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.EnterGameInfo Mihua.SDK.EnterGameInfo::Parse(System.Object[])
extern "C"  EnterGameInfo_t1897532954 * EnterGameInfo_Parse_m680786527 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
