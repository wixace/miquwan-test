﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioSource
struct AudioSource_t1740077639;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundObj
struct  SoundObj_t1807286664  : public Il2CppObject
{
public:
	// UnityEngine.AudioSource SoundObj::audioSource
	AudioSource_t1740077639 * ___audioSource_0;
	// System.Int32 SoundObj::id
	int32_t ___id_1;
	// System.Int32 SoundObj::replacetype
	int32_t ___replacetype_2;

public:
	inline static int32_t get_offset_of_audioSource_0() { return static_cast<int32_t>(offsetof(SoundObj_t1807286664, ___audioSource_0)); }
	inline AudioSource_t1740077639 * get_audioSource_0() const { return ___audioSource_0; }
	inline AudioSource_t1740077639 ** get_address_of_audioSource_0() { return &___audioSource_0; }
	inline void set_audioSource_0(AudioSource_t1740077639 * value)
	{
		___audioSource_0 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_0, value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(SoundObj_t1807286664, ___id_1)); }
	inline int32_t get_id_1() const { return ___id_1; }
	inline int32_t* get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(int32_t value)
	{
		___id_1 = value;
	}

	inline static int32_t get_offset_of_replacetype_2() { return static_cast<int32_t>(offsetof(SoundObj_t1807286664, ___replacetype_2)); }
	inline int32_t get_replacetype_2() const { return ___replacetype_2; }
	inline int32_t* get_address_of_replacetype_2() { return &___replacetype_2; }
	inline void set_replacetype_2(int32_t value)
	{
		___replacetype_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
