﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSCampfightData
struct CSCampfightData_t179152873;
// System.String
struct String_t;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Collections.Generic.List`1<CSPlusAtt>
struct List_1_t341533415;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// Newtonsoft.Json.Linq.JArray
struct JArray_t3394795039;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CSCampfightData179152873.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JArray3394795039.h"

// System.Void CSCampfightData::.ctor()
extern "C"  void CSCampfightData__ctor_m3116234146 (CSCampfightData_t179152873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCampfightData::LoadData(System.String)
extern "C"  void CSCampfightData_LoadData_m1351557040 (CSCampfightData_t179152873 * __this, String_t* ___jsonStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCampfightData::UnLoadData()
extern "C"  void CSCampfightData_UnLoadData_m372262859 (CSCampfightData_t179152873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCampfightData::Clear()
extern "C"  void CSCampfightData_Clear_m522367437 (CSCampfightData_t179152873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] CSCampfightData::GetMonsterIds()
extern "C"  Int32U5BU5D_t3230847821* CSCampfightData_GetMonsterIds_m907156640 (CSCampfightData_t179152873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSCampfightData::GetMonsterLevel(System.Int32,System.Boolean)
extern "C"  int32_t CSCampfightData_GetMonsterLevel_m1490078846 (CSCampfightData_t179152873 * __this, int32_t ___id0, bool ___isView1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPlusAtt> CSCampfightData::GetAttPlus()
extern "C"  List_1_t341533415 * CSCampfightData_GetAttPlus_m2251413743 (CSCampfightData_t179152873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPlusAtt> CSCampfightData::GetEnemyAttPlus()
extern "C"  List_1_t341533415 * CSCampfightData_GetEnemyAttPlus_m3904157825 (CSCampfightData_t179152873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPlusAtt> CSCampfightData::GetAttPlus_fightfor()
extern "C"  List_1_t341533415 * CSCampfightData_GetAttPlus_fightfor_m9073387 (CSCampfightData_t179152873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CSPlusAtt> CSCampfightData::GetEnemyAttPlus_fightfor()
extern "C"  List_1_t341533415 * CSCampfightData_GetEnemyAttPlus_fightfor_m2612328345 (CSCampfightData_t179152873 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSCampfightData::ilo_Clear1(CSCampfightData)
extern "C"  void CSCampfightData_ilo_Clear1_m3057201968 (Il2CppObject * __this /* static, unused */, CSCampfightData_t179152873 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken CSCampfightData::ilo_get_Item2(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * CSCampfightData_ilo_get_Item2_m2451260218 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken CSCampfightData::ilo_get_Item3(Newtonsoft.Json.Linq.JArray,System.Int32)
extern "C"  JToken_t3412245951 * CSCampfightData_ilo_get_Item3_m3353777500 (Il2CppObject * __this /* static, unused */, JArray_t3394795039 * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
