﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RecastGraph/CapsuleCache
struct  CapsuleCache_t2032660306  : public Il2CppObject
{
public:
	// System.Int32 Pathfinding.RecastGraph/CapsuleCache::rows
	int32_t ___rows_0;
	// System.Single Pathfinding.RecastGraph/CapsuleCache::height
	float ___height_1;
	// UnityEngine.Vector3[] Pathfinding.RecastGraph/CapsuleCache::verts
	Vector3U5BU5D_t215400611* ___verts_2;
	// System.Int32[] Pathfinding.RecastGraph/CapsuleCache::tris
	Int32U5BU5D_t3230847821* ___tris_3;

public:
	inline static int32_t get_offset_of_rows_0() { return static_cast<int32_t>(offsetof(CapsuleCache_t2032660306, ___rows_0)); }
	inline int32_t get_rows_0() const { return ___rows_0; }
	inline int32_t* get_address_of_rows_0() { return &___rows_0; }
	inline void set_rows_0(int32_t value)
	{
		___rows_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(CapsuleCache_t2032660306, ___height_1)); }
	inline float get_height_1() const { return ___height_1; }
	inline float* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(float value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_verts_2() { return static_cast<int32_t>(offsetof(CapsuleCache_t2032660306, ___verts_2)); }
	inline Vector3U5BU5D_t215400611* get_verts_2() const { return ___verts_2; }
	inline Vector3U5BU5D_t215400611** get_address_of_verts_2() { return &___verts_2; }
	inline void set_verts_2(Vector3U5BU5D_t215400611* value)
	{
		___verts_2 = value;
		Il2CppCodeGenWriteBarrier(&___verts_2, value);
	}

	inline static int32_t get_offset_of_tris_3() { return static_cast<int32_t>(offsetof(CapsuleCache_t2032660306, ___tris_3)); }
	inline Int32U5BU5D_t3230847821* get_tris_3() const { return ___tris_3; }
	inline Int32U5BU5D_t3230847821** get_address_of_tris_3() { return &___tris_3; }
	inline void set_tris_3(Int32U5BU5D_t3230847821* value)
	{
		___tris_3 = value;
		Il2CppCodeGenWriteBarrier(&___tris_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
