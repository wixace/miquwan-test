﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._301b7f65ab5137f09469f2274d37df38
struct _301b7f65ab5137f09469f2274d37df38_t1148155434;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__301b7f65ab5137f09469f2271148155434.h"

// System.Void Little._301b7f65ab5137f09469f2274d37df38::.ctor()
extern "C"  void _301b7f65ab5137f09469f2274d37df38__ctor_m4066705219 (_301b7f65ab5137f09469f2274d37df38_t1148155434 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._301b7f65ab5137f09469f2274d37df38::_301b7f65ab5137f09469f2274d37df38m2(System.Int32)
extern "C"  int32_t _301b7f65ab5137f09469f2274d37df38__301b7f65ab5137f09469f2274d37df38m2_m1057165657 (_301b7f65ab5137f09469f2274d37df38_t1148155434 * __this, int32_t ____301b7f65ab5137f09469f2274d37df38a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._301b7f65ab5137f09469f2274d37df38::_301b7f65ab5137f09469f2274d37df38m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _301b7f65ab5137f09469f2274d37df38__301b7f65ab5137f09469f2274d37df38m_m4050439549 (_301b7f65ab5137f09469f2274d37df38_t1148155434 * __this, int32_t ____301b7f65ab5137f09469f2274d37df38a0, int32_t ____301b7f65ab5137f09469f2274d37df38821, int32_t ____301b7f65ab5137f09469f2274d37df38c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._301b7f65ab5137f09469f2274d37df38::ilo__301b7f65ab5137f09469f2274d37df38m21(Little._301b7f65ab5137f09469f2274d37df38,System.Int32)
extern "C"  int32_t _301b7f65ab5137f09469f2274d37df38_ilo__301b7f65ab5137f09469f2274d37df38m21_m398352177 (Il2CppObject * __this /* static, unused */, _301b7f65ab5137f09469f2274d37df38_t1148155434 * ____this0, int32_t ____301b7f65ab5137f09469f2274d37df38a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
