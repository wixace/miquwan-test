﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StoryMgr/StoryVO
struct StoryVO_t3225531714;
// demoStoryCfg
struct demoStoryCfg_t1993162290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_demoStoryCfg1993162290.h"

// System.Void StoryMgr/StoryVO::.ctor(demoStoryCfg)
extern "C"  void StoryVO__ctor_m4207614887 (StoryVO_t3225531714 * __this, demoStoryCfg_t1993162290 * ___cfg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
