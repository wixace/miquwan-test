﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._b26c5659036d427bf6ca2235acbdc6b3
struct _b26c5659036d427bf6ca2235acbdc6b3_t3019852941;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._b26c5659036d427bf6ca2235acbdc6b3::.ctor()
extern "C"  void _b26c5659036d427bf6ca2235acbdc6b3__ctor_m1890363456 (_b26c5659036d427bf6ca2235acbdc6b3_t3019852941 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b26c5659036d427bf6ca2235acbdc6b3::_b26c5659036d427bf6ca2235acbdc6b3m2(System.Int32)
extern "C"  int32_t _b26c5659036d427bf6ca2235acbdc6b3__b26c5659036d427bf6ca2235acbdc6b3m2_m431480441 (_b26c5659036d427bf6ca2235acbdc6b3_t3019852941 * __this, int32_t ____b26c5659036d427bf6ca2235acbdc6b3a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._b26c5659036d427bf6ca2235acbdc6b3::_b26c5659036d427bf6ca2235acbdc6b3m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _b26c5659036d427bf6ca2235acbdc6b3__b26c5659036d427bf6ca2235acbdc6b3m_m200475741 (_b26c5659036d427bf6ca2235acbdc6b3_t3019852941 * __this, int32_t ____b26c5659036d427bf6ca2235acbdc6b3a0, int32_t ____b26c5659036d427bf6ca2235acbdc6b3741, int32_t ____b26c5659036d427bf6ca2235acbdc6b3c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
