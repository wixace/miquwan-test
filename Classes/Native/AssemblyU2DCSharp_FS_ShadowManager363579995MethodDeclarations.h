﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FS_ShadowManager
struct FS_ShadowManager_t363579995;
// FS_ShadowSimple
struct FS_ShadowSimple_t4208748868;
// FS_MeshKey
struct FS_MeshKey_t116578208;
// UnityEngine.Plane[]
struct PlaneU5BU5D_t1447812263;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FS_ShadowSimple4208748868.h"
#include "AssemblyU2DCSharp_FS_MeshKey116578208.h"

// System.Void FS_ShadowManager::.ctor()
extern "C"  void FS_ShadowManager__ctor_m3429356192 (FS_ShadowManager_t363579995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowManager::Start()
extern "C"  void FS_ShadowManager_Start_m2376493984 (FS_ShadowManager_t363579995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowManager::OnApplicationQuit()
extern "C"  void FS_ShadowManager_OnApplicationQuit_m3827912734 (FS_ShadowManager_t363579995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FS_ShadowManager FS_ShadowManager::Manager()
extern "C"  FS_ShadowManager_t363579995 * FS_ShadowManager_Manager_m849679081 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FS_ShadowManager::registerGeometry(FS_ShadowSimple,FS_MeshKey)
extern "C"  void FS_ShadowManager_registerGeometry_m734446833 (FS_ShadowManager_t363579995 * __this, FS_ShadowSimple_t4208748868 * ___s0, FS_MeshKey_t116578208 * ___meshKey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Plane[] FS_ShadowManager::getCameraFustrumPlanes()
extern "C"  PlaneU5BU5D_t1447812263* FS_ShadowManager_getCameraFustrumPlanes_m3243833350 (FS_ShadowManager_t363579995 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
