﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_2_gen119199768MethodDeclarations.h"

// System.Void System.Action`2<System.String,System.Single>::.ctor(System.Object,System.IntPtr)
#define Action_2__ctor_m3848183695(__this, ___object0, ___method1, method) ((  void (*) (Action_2_t3064546238 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_2__ctor_m4284393137_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`2<System.String,System.Single>::Invoke(T1,T2)
#define Action_2_Invoke_m317623852(__this, ___arg10, ___arg21, method) ((  void (*) (Action_2_t3064546238 *, String_t*, float, const MethodInfo*))Action_2_Invoke_m1544981722_gshared)(__this, ___arg10, ___arg21, method)
// System.IAsyncResult System.Action`2<System.String,System.Single>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define Action_2_BeginInvoke_m3659415883(__this, ___arg10, ___arg21, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Action_2_t3064546238 *, String_t*, float, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_2_BeginInvoke_m801995257_gshared)(__this, ___arg10, ___arg21, ___callback2, ___object3, method)
// System.Void System.Action`2<System.String,System.Single>::EndInvoke(System.IAsyncResult)
#define Action_2_EndInvoke_m1408214575(__this, ___result0, method) ((  void (*) (Action_2_t3064546238 *, Il2CppObject *, const MethodInfo*))Action_2_EndInvoke_m4146796865_gshared)(__this, ___result0, method)
