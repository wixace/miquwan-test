﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._e61d8ce101f35e44df5f1672613d9d0d
struct _e61d8ce101f35e44df5f1672613d9d0d_t2746409392;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._e61d8ce101f35e44df5f1672613d9d0d::.ctor()
extern "C"  void _e61d8ce101f35e44df5f1672613d9d0d__ctor_m2671454845 (_e61d8ce101f35e44df5f1672613d9d0d_t2746409392 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e61d8ce101f35e44df5f1672613d9d0d::_e61d8ce101f35e44df5f1672613d9d0dm2(System.Int32)
extern "C"  int32_t _e61d8ce101f35e44df5f1672613d9d0d__e61d8ce101f35e44df5f1672613d9d0dm2_m549335961 (_e61d8ce101f35e44df5f1672613d9d0d_t2746409392 * __this, int32_t ____e61d8ce101f35e44df5f1672613d9d0da0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e61d8ce101f35e44df5f1672613d9d0d::_e61d8ce101f35e44df5f1672613d9d0dm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _e61d8ce101f35e44df5f1672613d9d0d__e61d8ce101f35e44df5f1672613d9d0dm_m997241661 (_e61d8ce101f35e44df5f1672613d9d0d_t2746409392 * __this, int32_t ____e61d8ce101f35e44df5f1672613d9d0da0, int32_t ____e61d8ce101f35e44df5f1672613d9d0d831, int32_t ____e61d8ce101f35e44df5f1672613d9d0dc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
