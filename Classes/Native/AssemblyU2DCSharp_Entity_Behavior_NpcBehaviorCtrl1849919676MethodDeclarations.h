﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.NpcBehaviorCtrl
struct NpcBehaviorCtrl_t1849919676;
// CombatEntity
struct CombatEntity_t684137495;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"

// System.Void Entity.Behavior.NpcBehaviorCtrl::.ctor(CombatEntity)
extern "C"  void NpcBehaviorCtrl__ctor_m4093779975 (NpcBehaviorCtrl_t1849919676 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.NpcBehaviorCtrl::Init()
extern "C"  void NpcBehaviorCtrl_Init_m3949137366 (NpcBehaviorCtrl_t1849919676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.NpcBehaviorCtrl::ilo_AddBehavior1(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.IBehavior)
extern "C"  void NpcBehaviorCtrl_ilo_AddBehavior1_m3033270024 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, IBehavior_t770859129 * ___behavior1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
