﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JJCCoefficientCfg
struct  JJCCoefficientCfg_t2755051730  : public CsCfgBase_t69924517
{
public:
	// System.Int32 JJCCoefficientCfg::id
	int32_t ___id_0;
	// System.Int32 JJCCoefficientCfg::attack
	int32_t ___attack_1;
	// System.Int32 JJCCoefficientCfg::phy_def
	int32_t ___phy_def_2;
	// System.Int32 JJCCoefficientCfg::mag_def
	int32_t ___mag_def_3;
	// System.Int32 JJCCoefficientCfg::hp
	int32_t ___hp_4;
	// System.Int32 JJCCoefficientCfg::fighting
	int32_t ___fighting_5;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(JJCCoefficientCfg_t2755051730, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_attack_1() { return static_cast<int32_t>(offsetof(JJCCoefficientCfg_t2755051730, ___attack_1)); }
	inline int32_t get_attack_1() const { return ___attack_1; }
	inline int32_t* get_address_of_attack_1() { return &___attack_1; }
	inline void set_attack_1(int32_t value)
	{
		___attack_1 = value;
	}

	inline static int32_t get_offset_of_phy_def_2() { return static_cast<int32_t>(offsetof(JJCCoefficientCfg_t2755051730, ___phy_def_2)); }
	inline int32_t get_phy_def_2() const { return ___phy_def_2; }
	inline int32_t* get_address_of_phy_def_2() { return &___phy_def_2; }
	inline void set_phy_def_2(int32_t value)
	{
		___phy_def_2 = value;
	}

	inline static int32_t get_offset_of_mag_def_3() { return static_cast<int32_t>(offsetof(JJCCoefficientCfg_t2755051730, ___mag_def_3)); }
	inline int32_t get_mag_def_3() const { return ___mag_def_3; }
	inline int32_t* get_address_of_mag_def_3() { return &___mag_def_3; }
	inline void set_mag_def_3(int32_t value)
	{
		___mag_def_3 = value;
	}

	inline static int32_t get_offset_of_hp_4() { return static_cast<int32_t>(offsetof(JJCCoefficientCfg_t2755051730, ___hp_4)); }
	inline int32_t get_hp_4() const { return ___hp_4; }
	inline int32_t* get_address_of_hp_4() { return &___hp_4; }
	inline void set_hp_4(int32_t value)
	{
		___hp_4 = value;
	}

	inline static int32_t get_offset_of_fighting_5() { return static_cast<int32_t>(offsetof(JJCCoefficientCfg_t2755051730, ___fighting_5)); }
	inline int32_t get_fighting_5() const { return ___fighting_5; }
	inline int32_t* get_address_of_fighting_5() { return &___fighting_5; }
	inline void set_fighting_5(int32_t value)
	{
		___fighting_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
