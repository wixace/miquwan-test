﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UvTexAnimator
struct UvTexAnimator_t1307591341;
// UITexture
struct UITexture_t3903132647;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UITexture3903132647.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

// System.Void UvTexAnimator::.ctor()
extern "C"  void UvTexAnimator__ctor_m1268109662 (UvTexAnimator_t1307591341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UvTexAnimator::Start()
extern "C"  void UvTexAnimator_Start_m215247454 (UvTexAnimator_t1307591341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UvTexAnimator::Update()
extern "C"  void UvTexAnimator_Update_m2383555951 (UvTexAnimator_t1307591341 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UvTexAnimator::ilo_set_uvRect1(UITexture,UnityEngine.Rect)
extern "C"  void UvTexAnimator_ilo_set_uvRect1_m2565381144 (Il2CppObject * __this /* static, unused */, UITexture_t3903132647 * ____this0, Rect_t4241904616  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
