﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.ProtoAfterSerializationAttribute
struct ProtoAfterSerializationAttribute_t2299005580;

#include "codegen/il2cpp-codegen.h"

// System.Void ProtoBuf.ProtoAfterSerializationAttribute::.ctor()
extern "C"  void ProtoAfterSerializationAttribute__ctor_m2030611496 (ProtoAfterSerializationAttribute_t2299005580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
