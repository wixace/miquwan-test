﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>
struct Dictionary_2_t127038044;
// System.Collections.Generic.IEqualityComparer`1<FLOAT_TEXT_ID>
struct IEqualityComparer_1_t2990380174;
// System.Collections.Generic.IDictionary`2<FLOAT_TEXT_ID,System.Object>
struct IDictionary_2_t3999878685;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<FLOAT_TEXT_ID>
struct ICollection_1_t3093935757;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<FLOAT_TEXT_ID,System.Object>[]
struct KeyValuePair_2U5BU5D_t504207819;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<FLOAT_TEXT_ID,System.Object>>
struct IEnumerator_1_t1937683799;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<FLOAT_TEXT_ID,System.Object>
struct KeyCollection_t1753797495;
// System.Collections.Generic.Dictionary`2/ValueCollection<FLOAT_TEXT_ID,System.Object>
struct ValueCollection_t3122611053;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_g25818750.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1444361436.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m1970740427_gshared (Dictionary_2_t127038044 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1970740427(__this, method) ((  void (*) (Dictionary_2_t127038044 *, const MethodInfo*))Dictionary_2__ctor_m1970740427_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3055481794_gshared (Dictionary_2_t127038044 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3055481794(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t127038044 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3055481794_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m246380973_gshared (Dictionary_2_t127038044 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m246380973(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t127038044 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m246380973_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2283862428_gshared (Dictionary_2_t127038044 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m2283862428(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t127038044 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2283862428_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2785720432_gshared (Dictionary_2_t127038044 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2785720432(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t127038044 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2785720432_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m3086956428_gshared (Dictionary_2_t127038044 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m3086956428(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t127038044 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m3086956428_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m84494883_gshared (Dictionary_2_t127038044 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m84494883(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t127038044 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m84494883_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3802058915_gshared (Dictionary_2_t127038044 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3802058915(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t127038044 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3802058915_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m2651933137_gshared (Dictionary_2_t127038044 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m2651933137(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t127038044 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m2651933137_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m3959692927_gshared (Dictionary_2_t127038044 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m3959692927(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t127038044 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m3959692927_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m690399222_gshared (Dictionary_2_t127038044 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m690399222(__this, method) ((  bool (*) (Dictionary_2_t127038044 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m690399222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3801415747_gshared (Dictionary_2_t127038044 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3801415747(__this, method) ((  bool (*) (Dictionary_2_t127038044 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3801415747_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m1033400925_gshared (Dictionary_2_t127038044 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1033400925(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t127038044 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1033400925_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m544260162_gshared (Dictionary_2_t127038044 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m544260162(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t127038044 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m544260162_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m1552604367_gshared (Dictionary_2_t127038044 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1552604367(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t127038044 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1552604367_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m1265514311_gshared (Dictionary_2_t127038044 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1265514311(__this, ___key0, method) ((  bool (*) (Dictionary_2_t127038044 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1265514311_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m887094912_gshared (Dictionary_2_t127038044 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m887094912(__this, ___key0, method) ((  void (*) (Dictionary_2_t127038044 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m887094912_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2891781485_gshared (Dictionary_2_t127038044 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2891781485(__this, method) ((  bool (*) (Dictionary_2_t127038044 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2891781485_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3190917273_gshared (Dictionary_2_t127038044 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3190917273(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t127038044 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3190917273_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3694459761_gshared (Dictionary_2_t127038044 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3694459761(__this, method) ((  bool (*) (Dictionary_2_t127038044 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3694459761_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3891948310_gshared (Dictionary_2_t127038044 * __this, KeyValuePair_2_t25818750  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3891948310(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t127038044 *, KeyValuePair_2_t25818750 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3891948310_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2643129836_gshared (Dictionary_2_t127038044 * __this, KeyValuePair_2_t25818750  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2643129836(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t127038044 *, KeyValuePair_2_t25818750 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2643129836_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1310130554_gshared (Dictionary_2_t127038044 * __this, KeyValuePair_2U5BU5D_t504207819* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1310130554(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t127038044 *, KeyValuePair_2U5BU5D_t504207819*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1310130554_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1291167761_gshared (Dictionary_2_t127038044 * __this, KeyValuePair_2_t25818750  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1291167761(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t127038044 *, KeyValuePair_2_t25818750 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1291167761_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m1681839833_gshared (Dictionary_2_t127038044 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m1681839833(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t127038044 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1681839833_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m39598356_gshared (Dictionary_2_t127038044 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m39598356(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t127038044 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m39598356_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2619058961_gshared (Dictionary_2_t127038044 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2619058961(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t127038044 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2619058961_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1614585004_gshared (Dictionary_2_t127038044 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1614585004(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t127038044 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1614585004_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m2017330867_gshared (Dictionary_2_t127038044 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m2017330867(__this, method) ((  int32_t (*) (Dictionary_2_t127038044 *, const MethodInfo*))Dictionary_2_get_Count_m2017330867_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m225447128_gshared (Dictionary_2_t127038044 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m225447128(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t127038044 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m225447128_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m969380683_gshared (Dictionary_2_t127038044 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m969380683(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t127038044 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m969380683_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m966794307_gshared (Dictionary_2_t127038044 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m966794307(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t127038044 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m966794307_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2722942420_gshared (Dictionary_2_t127038044 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2722942420(__this, ___size0, method) ((  void (*) (Dictionary_2_t127038044 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2722942420_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m2526602960_gshared (Dictionary_2_t127038044 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m2526602960(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t127038044 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2526602960_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t25818750  Dictionary_2_make_pair_m2209175068_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m2209175068(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t25818750  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m2209175068_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m2250582618_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m2250582618(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m2250582618_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m3081424538_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m3081424538(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m3081424538_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m1459479295_gshared (Dictionary_2_t127038044 * __this, KeyValuePair_2U5BU5D_t504207819* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m1459479295(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t127038044 *, KeyValuePair_2U5BU5D_t504207819*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1459479295_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m1021207757_gshared (Dictionary_2_t127038044 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1021207757(__this, method) ((  void (*) (Dictionary_2_t127038044 *, const MethodInfo*))Dictionary_2_Resize_m1021207757_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m3943451722_gshared (Dictionary_2_t127038044 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m3943451722(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t127038044 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m3943451722_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m3671841014_gshared (Dictionary_2_t127038044 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m3671841014(__this, method) ((  void (*) (Dictionary_2_t127038044 *, const MethodInfo*))Dictionary_2_Clear_m3671841014_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2716258908_gshared (Dictionary_2_t127038044 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2716258908(__this, ___key0, method) ((  bool (*) (Dictionary_2_t127038044 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2716258908_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m3348731996_gshared (Dictionary_2_t127038044 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m3348731996(__this, ___value0, method) ((  bool (*) (Dictionary_2_t127038044 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m3348731996_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m2458268393_gshared (Dictionary_2_t127038044 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m2458268393(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t127038044 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m2458268393_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m2799251227_gshared (Dictionary_2_t127038044 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m2799251227(__this, ___sender0, method) ((  void (*) (Dictionary_2_t127038044 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2799251227_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m723410196_gshared (Dictionary_2_t127038044 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m723410196(__this, ___key0, method) ((  bool (*) (Dictionary_2_t127038044 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m723410196_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m3608743605_gshared (Dictionary_2_t127038044 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m3608743605(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t127038044 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m3608743605_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::get_Keys()
extern "C"  KeyCollection_t1753797495 * Dictionary_2_get_Keys_m2511387754_gshared (Dictionary_2_t127038044 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m2511387754(__this, method) ((  KeyCollection_t1753797495 * (*) (Dictionary_2_t127038044 *, const MethodInfo*))Dictionary_2_get_Keys_m2511387754_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::get_Values()
extern "C"  ValueCollection_t3122611053 * Dictionary_2_get_Values_m638292586_gshared (Dictionary_2_t127038044 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m638292586(__this, method) ((  ValueCollection_t3122611053 * (*) (Dictionary_2_t127038044 *, const MethodInfo*))Dictionary_2_get_Values_m638292586_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m1700441525_gshared (Dictionary_2_t127038044 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1700441525(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t127038044 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1700441525_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m1393738677_gshared (Dictionary_2_t127038044 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1393738677(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t127038044 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1393738677_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m516745975_gshared (Dictionary_2_t127038044 * __this, KeyValuePair_2_t25818750  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m516745975(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t127038044 *, KeyValuePair_2_t25818750 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m516745975_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::GetEnumerator()
extern "C"  Enumerator_t1444361436  Dictionary_2_GetEnumerator_m632969680_gshared (Dictionary_2_t127038044 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m632969680(__this, method) ((  Enumerator_t1444361436  (*) (Dictionary_2_t127038044 *, const MethodInfo*))Dictionary_2_GetEnumerator_m632969680_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m1056878111_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m1056878111(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1056878111_gshared)(__this /* static, unused */, ___key0, ___value1, method)
