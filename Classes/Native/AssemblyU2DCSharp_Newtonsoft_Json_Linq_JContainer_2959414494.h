﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerator_1_t1029143704;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JContainer
struct JContainer_t3364442311;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator1C
struct  U3CDescendantsU3Ec__Iterator1C_t2959414494  : public Il2CppObject
{
public:
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator1C::<$s_103>__0
	Il2CppObject* ___U3CU24s_103U3E__0_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator1C::<o>__1
	JToken_t3412245951 * ___U3CoU3E__1_1;
	// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator1C::<c>__2
	JContainer_t3364442311 * ___U3CcU3E__2_2;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator1C::<$s_104>__3
	Il2CppObject* ___U3CU24s_104U3E__3_3;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator1C::<d>__4
	JToken_t3412245951 * ___U3CdU3E__4_4;
	// System.Int32 Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator1C::$PC
	int32_t ___U24PC_5;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator1C::$current
	JToken_t3412245951 * ___U24current_6;
	// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JContainer/<Descendants>c__Iterator1C::<>f__this
	JContainer_t3364442311 * ___U3CU3Ef__this_7;

public:
	inline static int32_t get_offset_of_U3CU24s_103U3E__0_0() { return static_cast<int32_t>(offsetof(U3CDescendantsU3Ec__Iterator1C_t2959414494, ___U3CU24s_103U3E__0_0)); }
	inline Il2CppObject* get_U3CU24s_103U3E__0_0() const { return ___U3CU24s_103U3E__0_0; }
	inline Il2CppObject** get_address_of_U3CU24s_103U3E__0_0() { return &___U3CU24s_103U3E__0_0; }
	inline void set_U3CU24s_103U3E__0_0(Il2CppObject* value)
	{
		___U3CU24s_103U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_103U3E__0_0, value);
	}

	inline static int32_t get_offset_of_U3CoU3E__1_1() { return static_cast<int32_t>(offsetof(U3CDescendantsU3Ec__Iterator1C_t2959414494, ___U3CoU3E__1_1)); }
	inline JToken_t3412245951 * get_U3CoU3E__1_1() const { return ___U3CoU3E__1_1; }
	inline JToken_t3412245951 ** get_address_of_U3CoU3E__1_1() { return &___U3CoU3E__1_1; }
	inline void set_U3CoU3E__1_1(JToken_t3412245951 * value)
	{
		___U3CoU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CoU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CcU3E__2_2() { return static_cast<int32_t>(offsetof(U3CDescendantsU3Ec__Iterator1C_t2959414494, ___U3CcU3E__2_2)); }
	inline JContainer_t3364442311 * get_U3CcU3E__2_2() const { return ___U3CcU3E__2_2; }
	inline JContainer_t3364442311 ** get_address_of_U3CcU3E__2_2() { return &___U3CcU3E__2_2; }
	inline void set_U3CcU3E__2_2(JContainer_t3364442311 * value)
	{
		___U3CcU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcU3E__2_2, value);
	}

	inline static int32_t get_offset_of_U3CU24s_104U3E__3_3() { return static_cast<int32_t>(offsetof(U3CDescendantsU3Ec__Iterator1C_t2959414494, ___U3CU24s_104U3E__3_3)); }
	inline Il2CppObject* get_U3CU24s_104U3E__3_3() const { return ___U3CU24s_104U3E__3_3; }
	inline Il2CppObject** get_address_of_U3CU24s_104U3E__3_3() { return &___U3CU24s_104U3E__3_3; }
	inline void set_U3CU24s_104U3E__3_3(Il2CppObject* value)
	{
		___U3CU24s_104U3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_104U3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CdU3E__4_4() { return static_cast<int32_t>(offsetof(U3CDescendantsU3Ec__Iterator1C_t2959414494, ___U3CdU3E__4_4)); }
	inline JToken_t3412245951 * get_U3CdU3E__4_4() const { return ___U3CdU3E__4_4; }
	inline JToken_t3412245951 ** get_address_of_U3CdU3E__4_4() { return &___U3CdU3E__4_4; }
	inline void set_U3CdU3E__4_4(JToken_t3412245951 * value)
	{
		___U3CdU3E__4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdU3E__4_4, value);
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CDescendantsU3Ec__Iterator1C_t2959414494, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CDescendantsU3Ec__Iterator1C_t2959414494, ___U24current_6)); }
	inline JToken_t3412245951 * get_U24current_6() const { return ___U24current_6; }
	inline JToken_t3412245951 ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(JToken_t3412245951 * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_7() { return static_cast<int32_t>(offsetof(U3CDescendantsU3Ec__Iterator1C_t2959414494, ___U3CU3Ef__this_7)); }
	inline JContainer_t3364442311 * get_U3CU3Ef__this_7() const { return ___U3CU3Ef__this_7; }
	inline JContainer_t3364442311 ** get_address_of_U3CU3Ef__this_7() { return &___U3CU3Ef__this_7; }
	inline void set_U3CU3Ef__this_7(JContainer_t3364442311 * value)
	{
		___U3CU3Ef__this_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
