﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._970fc18c67bfe8e43e784f420354e666
struct _970fc18c67bfe8e43e784f420354e666_t3633191267;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__970fc18c67bfe8e43e784f423633191267.h"

// System.Void Little._970fc18c67bfe8e43e784f420354e666::.ctor()
extern "C"  void _970fc18c67bfe8e43e784f420354e666__ctor_m1197993002 (_970fc18c67bfe8e43e784f420354e666_t3633191267 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._970fc18c67bfe8e43e784f420354e666::_970fc18c67bfe8e43e784f420354e666m2(System.Int32)
extern "C"  int32_t _970fc18c67bfe8e43e784f420354e666__970fc18c67bfe8e43e784f420354e666m2_m401931961 (_970fc18c67bfe8e43e784f420354e666_t3633191267 * __this, int32_t ____970fc18c67bfe8e43e784f420354e666a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._970fc18c67bfe8e43e784f420354e666::_970fc18c67bfe8e43e784f420354e666m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _970fc18c67bfe8e43e784f420354e666__970fc18c67bfe8e43e784f420354e666m_m1024371741 (_970fc18c67bfe8e43e784f420354e666_t3633191267 * __this, int32_t ____970fc18c67bfe8e43e784f420354e666a0, int32_t ____970fc18c67bfe8e43e784f420354e666291, int32_t ____970fc18c67bfe8e43e784f420354e666c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._970fc18c67bfe8e43e784f420354e666::ilo__970fc18c67bfe8e43e784f420354e666m21(Little._970fc18c67bfe8e43e784f420354e666,System.Int32)
extern "C"  int32_t _970fc18c67bfe8e43e784f420354e666_ilo__970fc18c67bfe8e43e784f420354e666m21_m821531786 (Il2CppObject * __this /* static, unused */, _970fc18c67bfe8e43e784f420354e666_t3633191267 * ____this0, int32_t ____970fc18c67bfe8e43e784f420354e666a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
