﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2770839788MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,Pathfinding.PointNode>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3405465465(__this, ___dictionary0, method) ((  void (*) (Enumerator_t1361837197 *, Dictionary_2_t44513805 *, const MethodInfo*))Enumerator__ctor_m2853155855_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m844301330(__this, method) ((  Il2CppObject * (*) (Enumerator_t1361837197 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2332623676_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3768086428(__this, method) ((  void (*) (Enumerator_t1361837197 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3227554118_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2851001043(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t1361837197 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2126232061_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m113208174(__this, method) ((  Il2CppObject * (*) (Enumerator_t1361837197 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1182786328_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,Pathfinding.PointNode>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2467309184(__this, method) ((  Il2CppObject * (*) (Enumerator_t1361837197 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3834731434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,Pathfinding.PointNode>::MoveNext()
#define Enumerator_MoveNext_m367301708(__this, method) ((  bool (*) (Enumerator_t1361837197 *, const MethodInfo*))Enumerator_MoveNext_m4054555382_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,Pathfinding.PointNode>::get_Current()
#define Enumerator_get_Current_m1272571888(__this, method) ((  KeyValuePair_2_t4238261807  (*) (Enumerator_t1361837197 *, const MethodInfo*))Enumerator_get_Current_m2779016966_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,Pathfinding.PointNode>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m2950735061(__this, method) ((  Int3_t1974045594  (*) (Enumerator_t1361837197 *, const MethodInfo*))Enumerator_get_CurrentKey_m2864030207_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,Pathfinding.PointNode>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m106839253(__this, method) ((  PointNode_t2761813780 * (*) (Enumerator_t1361837197 *, const MethodInfo*))Enumerator_get_CurrentValue_m2181899647_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,Pathfinding.PointNode>::Reset()
#define Enumerator_Reset_m3172449355(__this, method) ((  void (*) (Enumerator_t1361837197 *, const MethodInfo*))Enumerator_Reset_m3428296673_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,Pathfinding.PointNode>::VerifyState()
#define Enumerator_VerifyState_m350316564(__this, method) ((  void (*) (Enumerator_t1361837197 *, const MethodInfo*))Enumerator_VerifyState_m3492530986_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,Pathfinding.PointNode>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m96673404(__this, method) ((  void (*) (Enumerator_t1361837197 *, const MethodInfo*))Enumerator_VerifyCurrent_m402723858_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,Pathfinding.PointNode>::Dispose()
#define Enumerator_Dispose_m1820479067(__this, method) ((  void (*) (Enumerator_t1361837197 *, const MethodInfo*))Enumerator_Dispose_m2876615793_gshared)(__this, method)
