﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::.ctor()
#define List_1__ctor_m1922342217(__this, method) ((  void (*) (List_1_t415449401 *, const MethodInfo*))List_1__ctor_m1239231859_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m2503687062(__this, ___collection0, method) ((  void (*) (List_1_t415449401 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::.ctor(System.Int32)
#define List_1__ctor_m2843454746(__this, ___capacity0, method) ((  void (*) (List_1_t415449401 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::.cctor()
#define List_1__cctor_m3275937668(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m329500507(__this, method) ((  Il2CppObject* (*) (List_1_t415449401 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m2382542875(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t415449401 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3484831062(__this, method) ((  Il2CppObject * (*) (List_1_t415449401 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m2641596379(__this, ___item0, method) ((  int32_t (*) (List_1_t415449401 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m2082761809(__this, ___item0, method) ((  bool (*) (List_1_t415449401 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m1180852403(__this, ___item0, method) ((  int32_t (*) (List_1_t415449401 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m3206702942(__this, ___index0, ___item1, method) ((  void (*) (List_1_t415449401 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m3561398538(__this, ___item0, method) ((  void (*) (List_1_t415449401 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3600149266(__this, method) ((  bool (*) (List_1_t415449401 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m2454469995(__this, method) ((  bool (*) (List_1_t415449401 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m3514291671(__this, method) ((  Il2CppObject * (*) (List_1_t415449401 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m3619941504(__this, method) ((  bool (*) (List_1_t415449401 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m1817707129(__this, method) ((  bool (*) (List_1_t415449401 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m2455612830(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t415449401 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m2763928501(__this, ___index0, ___value1, method) ((  void (*) (List_1_t415449401 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::Add(T)
#define List_1_Add_m2823099926(__this, ___item0, method) ((  void (*) (List_1_t415449401 *, List_1_t3342231145 *, const MethodInfo*))List_1_Add_m933592675_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m3115736401(__this, ___newCount0, method) ((  void (*) (List_1_t415449401 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m20773366(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t415449401 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m590371457_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m3207934031(__this, ___collection0, method) ((  void (*) (List_1_t415449401 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m2468906255(__this, ___enumerable0, method) ((  void (*) (List_1_t415449401 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m695155752(__this, ___collection0, method) ((  void (*) (List_1_t415449401 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::AsReadOnly()
#define List_1_AsReadOnly_m1959260033(__this, method) ((  ReadOnlyCollection_1_t604341385 * (*) (List_1_t415449401 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::BinarySearch(T)
#define List_1_BinarySearch_m3005993630(__this, ___item0, method) ((  int32_t (*) (List_1_t415449401 *, List_1_t3342231145 *, const MethodInfo*))List_1_BinarySearch_m2761349225_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::Clear()
#define List_1_Clear_m3623442804(__this, method) ((  void (*) (List_1_t415449401 *, const MethodInfo*))List_1_Clear_m2940332446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::Contains(T)
#define List_1_Contains_m3632078370(__this, ___item0, method) ((  bool (*) (List_1_t415449401 *, List_1_t3342231145 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m523625606(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t415449401 *, List_1U5BU5D_t1457655252*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::Find(System.Predicate`1<T>)
#define List_1_Find_m4171955426(__this, ___match0, method) ((  List_1_t3342231145 * (*) (List_1_t415449401 *, Predicate_1_t2953288028 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m3789418781(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2953288028 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m4045852226(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t415449401 *, int32_t, int32_t, Predicate_1_t2953288028 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::GetEnumerator()
#define List_1_GetEnumerator_m687354463(__this, method) ((  Enumerator_t435122171  (*) (List_1_t415449401 *, const MethodInfo*))List_1_GetEnumerator_m1919240000_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::IndexOf(T)
#define List_1_IndexOf_m3818700682(__this, ___item0, method) ((  int32_t (*) (List_1_t415449401 *, List_1_t3342231145 *, const MethodInfo*))List_1_IndexOf_m2776798407_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m356242653(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t415449401 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m269992790(__this, ___index0, method) ((  void (*) (List_1_t415449401 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::Insert(System.Int32,T)
#define List_1_Insert_m1139762301(__this, ___index0, ___item1, method) ((  void (*) (List_1_t415449401 *, int32_t, List_1_t3342231145 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m3859721010(__this, ___collection0, method) ((  void (*) (List_1_t415449401 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::Remove(T)
#define List_1_Remove_m3980852893(__this, ___item0, method) ((  bool (*) (List_1_t415449401 *, List_1_t3342231145 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m1137370061(__this, ___match0, method) ((  int32_t (*) (List_1_t415449401 *, Predicate_1_t2953288028 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m3308582467(__this, ___index0, method) ((  void (*) (List_1_t415449401 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2208877785_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m379186278(__this, ___index0, ___count1, method) ((  void (*) (List_1_t415449401 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m856857915_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::Reverse()
#define List_1_Reverse_m572744521(__this, method) ((  void (*) (List_1_t415449401 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::Sort()
#define List_1_Sort_m3349072121(__this, method) ((  void (*) (List_1_t415449401 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m3031968011(__this, ___comparer0, method) ((  void (*) (List_1_t415449401 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m2963072140(__this, ___comparison0, method) ((  void (*) (List_1_t415449401 *, Comparison_1_t2058592332 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::ToArray()
#define List_1_ToArray_m193509896(__this, method) ((  List_1U5BU5D_t1457655252* (*) (List_1_t415449401 *, const MethodInfo*))List_1_ToArray_m1449333879_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::TrimExcess()
#define List_1_TrimExcess_m1401391570(__this, method) ((  void (*) (List_1_t415449401 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::get_Capacity()
#define List_1_get_Capacity_m551412410(__this, method) ((  int32_t (*) (List_1_t415449401 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m325925219(__this, ___value0, method) ((  void (*) (List_1_t415449401 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::get_Count()
#define List_1_get_Count_m2232911665(__this, method) ((  int32_t (*) (List_1_t415449401 *, const MethodInfo*))List_1_get_Count_m3549598589_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::get_Item(System.Int32)
#define List_1_get_Item_m473528455(__this, ___index0, method) ((  List_1_t3342231145 * (*) (List_1_t415449401 *, int32_t, const MethodInfo*))List_1_get_Item_m404118264_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.Int2>>::set_Item(System.Int32,T)
#define List_1_set_Item_m1904507412(__this, ___index0, ___value1, method) ((  void (*) (List_1_t415449401 *, int32_t, List_1_t3342231145 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
