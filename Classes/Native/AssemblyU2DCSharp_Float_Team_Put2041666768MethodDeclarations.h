﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_Team_Put
struct Float_Team_Put_t2041666768;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String
struct String_t;
// FloatTextUnit
struct FloatTextUnit_t2362298029;
// EventDelegate/Callback
struct Callback_t1094463061;
// System.String[]
struct StringU5BU5D_t4054002952;
// UILabel
struct UILabel_t291504320;
// UIWidget
struct UIWidget_t769069560;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_UITweener_Method1078127180.h"
#include "AssemblyU2DCSharp_Float_Team_Put2041666768.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"

// System.Void Float_Team_Put::.ctor()
extern "C"  void Float_Team_Put__ctor_m601348555 (Float_Team_Put_t2041666768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Team_Put::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_Team_Put_OnAwake_m679201927 (Float_Team_Put_t2041666768 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Team_Put::OnDestroy()
extern "C"  void Float_Team_Put_OnDestroy_m3980513604 (Float_Team_Put_t2041666768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_Team_Put::FloatID()
extern "C"  int32_t Float_Team_Put_FloatID_m1823253527 (Float_Team_Put_t2041666768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Team_Put::Init()
extern "C"  void Float_Team_Put_Init_m3850260265 (Float_Team_Put_t2041666768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Team_Put::SetFile(System.Object[])
extern "C"  void Float_Team_Put_SetFile_m1780495979 (Float_Team_Put_t2041666768 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Team_Put::SetFate(System.String,System.String)
extern "C"  void Float_Team_Put_SetFate_m895161351 (Float_Team_Put_t2041666768 * __this, String_t* ___name0, String_t* ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Team_Put::SetSuit(System.String,System.String,System.String,System.String)
extern "C"  void Float_Team_Put_SetSuit_m2037851006 (Float_Team_Put_t2041666768 * __this, String_t* ___tileA0, String_t* ___valueA1, String_t* ___tileB2, String_t* ___valueB3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Team_Put::SetMaster(System.String,System.String)
extern "C"  void Float_Team_Put_SetMaster_m2162290769 (Float_Team_Put_t2041666768 * __this, String_t* ___streng0, String_t* ___refine1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Team_Put::SetAtt(System.String)
extern "C"  void Float_Team_Put_SetAtt_m4001037002 (Float_Team_Put_t2041666768 * __this, String_t* ___att0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Team_Put::SetBG()
extern "C"  void Float_Team_Put_SetBG_m3430090064 (Float_Team_Put_t2041666768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Team_Put::ilo_MoveAlpha1(FloatTextUnit,System.Single,System.Single,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void Float_Team_Put_ilo_MoveAlpha1_m3047883972 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, float ___from1, float ___to2, float ___duration3, float ___delay4, Callback_t1094463061 * ___call5, GameObject_t3674682005 * ___go6, int32_t ___method7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Team_Put::ilo_SetSuit2(Float_Team_Put,System.String,System.String,System.String,System.String)
extern "C"  void Float_Team_Put_ilo_SetSuit2_m1985144815 (Il2CppObject * __this /* static, unused */, Float_Team_Put_t2041666768 * ____this0, String_t* ___tileA1, String_t* ___valueA2, String_t* ___tileB3, String_t* ___valueB4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Team_Put::ilo_SetMaster3(Float_Team_Put,System.String,System.String)
extern "C"  void Float_Team_Put_ilo_SetMaster3_m2132411395 (Il2CppObject * __this /* static, unused */, Float_Team_Put_t2041666768 * ____this0, String_t* ___streng1, String_t* ___refine2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Float_Team_Put::ilo_SplitStr_commn4(System.String)
extern "C"  StringU5BU5D_t4054002952* Float_Team_Put_ilo_SplitStr_commn4_m1760915539 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Team_Put::ilo_set_text5(UILabel,System.String)
extern "C"  void Float_Team_Put_ilo_set_text5_m303298637 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Float_Team_Put::ilo_get_height6(UIWidget)
extern "C"  int32_t Float_Team_Put_ilo_get_height6_m1781523446 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
