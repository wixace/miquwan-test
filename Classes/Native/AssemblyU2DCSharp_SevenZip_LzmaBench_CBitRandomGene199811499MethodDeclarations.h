﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.LzmaBench/CBitRandomGenerator
struct CBitRandomGenerator_t199811499;

#include "codegen/il2cpp-codegen.h"

// System.Void SevenZip.LzmaBench/CBitRandomGenerator::.ctor()
extern "C"  void CBitRandomGenerator__ctor_m2695091024 (CBitRandomGenerator_t199811499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.LzmaBench/CBitRandomGenerator::Init()
extern "C"  void CBitRandomGenerator_Init_m3225063684 (CBitRandomGenerator_t199811499 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.LzmaBench/CBitRandomGenerator::GetRnd(System.Int32)
extern "C"  uint32_t CBitRandomGenerator_GetRnd_m2703981330 (CBitRandomGenerator_t199811499 * __this, int32_t ___numBits0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
