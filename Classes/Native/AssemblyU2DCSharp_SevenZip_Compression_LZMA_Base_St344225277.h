﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SevenZip.Compression.LZMA.Base/State
struct  State_t344225277 
{
public:
	// System.UInt32 SevenZip.Compression.LZMA.Base/State::Index
	uint32_t ___Index_0;

public:
	inline static int32_t get_offset_of_Index_0() { return static_cast<int32_t>(offsetof(State_t344225277, ___Index_0)); }
	inline uint32_t get_Index_0() const { return ___Index_0; }
	inline uint32_t* get_address_of_Index_0() { return &___Index_0; }
	inline void set_Index_0(uint32_t value)
	{
		___Index_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: SevenZip.Compression.LZMA.Base/State
struct State_t344225277_marshaled_pinvoke
{
	uint32_t ___Index_0;
};
// Native definition for marshalling of: SevenZip.Compression.LZMA.Base/State
struct State_t344225277_marshaled_com
{
	uint32_t ___Index_0;
};
