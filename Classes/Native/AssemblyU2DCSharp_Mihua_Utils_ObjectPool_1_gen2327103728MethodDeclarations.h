﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mihua_Utils_ObjectPool_1_gen2200870896MethodDeclarations.h"

// System.Void Mihua.Utils.ObjectPool`1<Buff>::.cctor()
#define ObjectPool_1__cctor_m1408146560(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1__cctor_m2632395647_gshared)(__this /* static, unused */, method)
// System.Void Mihua.Utils.ObjectPool`1<Buff>::Recycle(T)
#define ObjectPool_1_Recycle_m2304931808(__this /* static, unused */, ___obj0, method) ((  void (*) (Il2CppObject * /* static, unused */, Buff_t2081907 *, const MethodInfo*))ObjectPool_1_Recycle_m1987265311_gshared)(__this /* static, unused */, ___obj0, method)
// System.Int32 Mihua.Utils.ObjectPool`1<Buff>::get_maxCount()
#define ObjectPool_1_get_maxCount_m2907596313(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1_get_maxCount_m713746044_gshared)(__this /* static, unused */, method)
// System.Void Mihua.Utils.ObjectPool`1<Buff>::set_maxCount(System.Int32)
#define ObjectPool_1_set_maxCount_m4278767312(__this /* static, unused */, ___value0, method) ((  void (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))ObjectPool_1_set_maxCount_m1492204559_gshared)(__this /* static, unused */, ___value0, method)
// System.Int32 Mihua.Utils.ObjectPool`1<Buff>::GetTotalCreated()
#define ObjectPool_1_GetTotalCreated_m2405771287(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1_GetTotalCreated_m1929750740_gshared)(__this /* static, unused */, method)
// System.Int32 Mihua.Utils.ObjectPool`1<Buff>::GetSize()
#define ObjectPool_1_GetSize_m3606325204(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1_GetSize_m2045490577_gshared)(__this /* static, unused */, method)
// T Mihua.Utils.ObjectPool`1<Buff>::GetObj()
#define ObjectPool_1_GetObj_m362474327(__this /* static, unused */, method) ((  Buff_t2081907 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1_GetObj_m2492700728_gshared)(__this /* static, unused */, method)
// System.Void Mihua.Utils.ObjectPool`1<Buff>::Cleanup()
#define ObjectPool_1_Cleanup_m1136744719(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ObjectPool_1_Cleanup_m433760752_gshared)(__this /* static, unused */, method)
