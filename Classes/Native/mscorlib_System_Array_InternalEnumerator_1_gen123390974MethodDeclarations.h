﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen123390974.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21341048298.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m865676958_gshared (InternalEnumerator_1_t123390974 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m865676958(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t123390974 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m865676958_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1115461186_gshared (InternalEnumerator_1_t123390974 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1115461186(__this, method) ((  void (*) (InternalEnumerator_1_t123390974 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1115461186_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4186828398_gshared (InternalEnumerator_1_t123390974 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4186828398(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t123390974 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m4186828398_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2911159797_gshared (InternalEnumerator_1_t123390974 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2911159797(__this, method) ((  void (*) (InternalEnumerator_1_t123390974 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2911159797_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1236662830_gshared (InternalEnumerator_1_t123390974 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1236662830(__this, method) ((  bool (*) (InternalEnumerator_1_t123390974 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1236662830_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<MScrollView/MoveWay,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t1341048298  InternalEnumerator_1_get_Current_m489056549_gshared (InternalEnumerator_1_t123390974 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m489056549(__this, method) ((  KeyValuePair_2_t1341048298  (*) (InternalEnumerator_1_t123390974 *, const MethodInfo*))InternalEnumerator_1_get_Current_m489056549_gshared)(__this, method)
