﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUIContentGenerated
struct UnityEngine_GUIContentGenerated_t3921667593;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_GUIContentGenerated::.ctor()
extern "C"  void UnityEngine_GUIContentGenerated__ctor_m624216962 (UnityEngine_GUIContentGenerated_t3921667593 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIContentGenerated::GUIContent_GUIContent1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIContentGenerated_GUIContent_GUIContent1_m1746436656 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIContentGenerated::GUIContent_GUIContent2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIContentGenerated_GUIContent_GUIContent2_m2991201137 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIContentGenerated::GUIContent_GUIContent3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIContentGenerated_GUIContent_GUIContent3_m4235965618 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIContentGenerated::GUIContent_GUIContent4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIContentGenerated_GUIContent_GUIContent4_m1185762803 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIContentGenerated::GUIContent_GUIContent5(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIContentGenerated_GUIContent_GUIContent5_m2430527284 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIContentGenerated::GUIContent_GUIContent6(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIContentGenerated_GUIContent_GUIContent6_m3675291765 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIContentGenerated::GUIContent_GUIContent7(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIContentGenerated_GUIContent_GUIContent7_m625088950 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIContentGenerated::GUIContent_GUIContent8(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUIContentGenerated_GUIContent_GUIContent8_m1869853431 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIContentGenerated::GUIContent_none(JSVCall)
extern "C"  void UnityEngine_GUIContentGenerated_GUIContent_none_m903342030 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIContentGenerated::GUIContent_text(JSVCall)
extern "C"  void UnityEngine_GUIContentGenerated_GUIContent_text_m2704684409 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIContentGenerated::GUIContent_image(JSVCall)
extern "C"  void UnityEngine_GUIContentGenerated_GUIContent_image_m1645895691 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIContentGenerated::GUIContent_tooltip(JSVCall)
extern "C"  void UnityEngine_GUIContentGenerated_GUIContent_tooltip_m2757416547 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIContentGenerated::__Register()
extern "C"  void UnityEngine_GUIContentGenerated___Register_m2794033797 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUIContentGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_GUIContentGenerated_ilo_getObject1_m2184738804 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUIContentGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_GUIContentGenerated_ilo_attachFinalizerObject2_m950297846 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_GUIContentGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_GUIContentGenerated_ilo_getObject3_m600226341 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_GUIContentGenerated::ilo_getStringS4(System.Int32)
extern "C"  String_t* UnityEngine_GUIContentGenerated_ilo_getStringS4_m651511017 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIContentGenerated::ilo_addJSCSRel5(System.Int32,System.Object)
extern "C"  void UnityEngine_GUIContentGenerated_ilo_addJSCSRel5_m2650004290 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUIContentGenerated::ilo_setObject6(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_GUIContentGenerated_ilo_setObject6_m1387004593 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
