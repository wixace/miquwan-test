﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.GraphNode>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m3961905389(__this, ___l0, method) ((  void (*) (Enumerator_t1411470692 *, List_1_t1391797922 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.GraphNode>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2488374085(__this, method) ((  void (*) (Enumerator_t1411470692 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.GraphNode>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m4141625777(__this, method) ((  Il2CppObject * (*) (Enumerator_t1411470692 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.GraphNode>::Dispose()
#define Enumerator_Dispose_m1704180242(__this, method) ((  void (*) (Enumerator_t1411470692 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.GraphNode>::VerifyState()
#define Enumerator_VerifyState_m190324811(__this, method) ((  void (*) (Enumerator_t1411470692 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.GraphNode>::MoveNext()
#define Enumerator_MoveNext_m3184996017(__this, method) ((  bool (*) (Enumerator_t1411470692 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.GraphNode>::get_Current()
#define Enumerator_get_Current_m3495840066(__this, method) ((  GraphNode_t23612370 * (*) (Enumerator_t1411470692 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
