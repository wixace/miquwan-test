﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Voxels.VoxelContour
struct  VoxelContour_t3597201016 
{
public:
	// System.Int32 Pathfinding.Voxels.VoxelContour::nverts
	int32_t ___nverts_0;
	// System.Int32[] Pathfinding.Voxels.VoxelContour::verts
	Int32U5BU5D_t3230847821* ___verts_1;
	// System.Int32[] Pathfinding.Voxels.VoxelContour::rverts
	Int32U5BU5D_t3230847821* ___rverts_2;
	// System.Int32 Pathfinding.Voxels.VoxelContour::reg
	int32_t ___reg_3;
	// System.Int32 Pathfinding.Voxels.VoxelContour::area
	int32_t ___area_4;

public:
	inline static int32_t get_offset_of_nverts_0() { return static_cast<int32_t>(offsetof(VoxelContour_t3597201016, ___nverts_0)); }
	inline int32_t get_nverts_0() const { return ___nverts_0; }
	inline int32_t* get_address_of_nverts_0() { return &___nverts_0; }
	inline void set_nverts_0(int32_t value)
	{
		___nverts_0 = value;
	}

	inline static int32_t get_offset_of_verts_1() { return static_cast<int32_t>(offsetof(VoxelContour_t3597201016, ___verts_1)); }
	inline Int32U5BU5D_t3230847821* get_verts_1() const { return ___verts_1; }
	inline Int32U5BU5D_t3230847821** get_address_of_verts_1() { return &___verts_1; }
	inline void set_verts_1(Int32U5BU5D_t3230847821* value)
	{
		___verts_1 = value;
		Il2CppCodeGenWriteBarrier(&___verts_1, value);
	}

	inline static int32_t get_offset_of_rverts_2() { return static_cast<int32_t>(offsetof(VoxelContour_t3597201016, ___rverts_2)); }
	inline Int32U5BU5D_t3230847821* get_rverts_2() const { return ___rverts_2; }
	inline Int32U5BU5D_t3230847821** get_address_of_rverts_2() { return &___rverts_2; }
	inline void set_rverts_2(Int32U5BU5D_t3230847821* value)
	{
		___rverts_2 = value;
		Il2CppCodeGenWriteBarrier(&___rverts_2, value);
	}

	inline static int32_t get_offset_of_reg_3() { return static_cast<int32_t>(offsetof(VoxelContour_t3597201016, ___reg_3)); }
	inline int32_t get_reg_3() const { return ___reg_3; }
	inline int32_t* get_address_of_reg_3() { return &___reg_3; }
	inline void set_reg_3(int32_t value)
	{
		___reg_3 = value;
	}

	inline static int32_t get_offset_of_area_4() { return static_cast<int32_t>(offsetof(VoxelContour_t3597201016, ___area_4)); }
	inline int32_t get_area_4() const { return ___area_4; }
	inline int32_t* get_address_of_area_4() { return &___area_4; }
	inline void set_area_4(int32_t value)
	{
		___area_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Pathfinding.Voxels.VoxelContour
struct VoxelContour_t3597201016_marshaled_pinvoke
{
	int32_t ___nverts_0;
	int32_t* ___verts_1;
	int32_t* ___rverts_2;
	int32_t ___reg_3;
	int32_t ___area_4;
};
// Native definition for marshalling of: Pathfinding.Voxels.VoxelContour
struct VoxelContour_t3597201016_marshaled_com
{
	int32_t ___nverts_0;
	int32_t* ___verts_1;
	int32_t* ___rverts_2;
	int32_t ___reg_3;
	int32_t ___area_4;
};
