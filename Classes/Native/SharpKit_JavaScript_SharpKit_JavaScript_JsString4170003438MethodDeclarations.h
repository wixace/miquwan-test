﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SharpKit.JavaScript.JsString
struct JsString_t4170003438;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "SharpKit_JavaScript_SharpKit_JavaScript_JsString4170003438.h"

// System.Void SharpKit.JavaScript.JsString::.ctor(System.Object)
extern "C"  void JsString__ctor_m1240413969 (JsString_t4170003438 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SharpKit.JavaScript.JsString SharpKit.JavaScript.JsString::op_Implicit(System.String)
extern "C"  JsString_t4170003438 * JsString_op_Implicit_m542190724 (Il2CppObject * __this /* static, unused */, String_t* ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SharpKit.JavaScript.JsString::op_Implicit(SharpKit.JavaScript.JsString)
extern "C"  String_t* JsString_op_Implicit_m3144283048 (Il2CppObject * __this /* static, unused */, JsString_t4170003438 * ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SharpKit.JavaScript.JsString::ToString()
extern "C"  String_t* JsString_ToString_m219910224 (JsString_t4170003438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
