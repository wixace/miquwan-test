﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._def01057f7990d4f1849b22c88776b52
struct _def01057f7990d4f1849b22c88776b52_t2370841426;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__def01057f7990d4f1849b22c2370841426.h"

// System.Void Little._def01057f7990d4f1849b22c88776b52::.ctor()
extern "C"  void _def01057f7990d4f1849b22c88776b52__ctor_m3187250971 (_def01057f7990d4f1849b22c88776b52_t2370841426 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._def01057f7990d4f1849b22c88776b52::_def01057f7990d4f1849b22c88776b52m2(System.Int32)
extern "C"  int32_t _def01057f7990d4f1849b22c88776b52__def01057f7990d4f1849b22c88776b52m2_m2525085785 (_def01057f7990d4f1849b22c88776b52_t2370841426 * __this, int32_t ____def01057f7990d4f1849b22c88776b52a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._def01057f7990d4f1849b22c88776b52::_def01057f7990d4f1849b22c88776b52m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _def01057f7990d4f1849b22c88776b52__def01057f7990d4f1849b22c88776b52m_m4085317245 (_def01057f7990d4f1849b22c88776b52_t2370841426 * __this, int32_t ____def01057f7990d4f1849b22c88776b52a0, int32_t ____def01057f7990d4f1849b22c88776b52201, int32_t ____def01057f7990d4f1849b22c88776b52c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._def01057f7990d4f1849b22c88776b52::ilo__def01057f7990d4f1849b22c88776b52m21(Little._def01057f7990d4f1849b22c88776b52,System.Int32)
extern "C"  int32_t _def01057f7990d4f1849b22c88776b52_ilo__def01057f7990d4f1849b22c88776b52m21_m4285083993 (Il2CppObject * __this /* static, unused */, _def01057f7990d4f1849b22c88776b52_t2370841426 * ____this0, int32_t ____def01057f7990d4f1849b22c88776b52a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
