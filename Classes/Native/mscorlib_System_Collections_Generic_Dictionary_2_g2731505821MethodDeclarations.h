﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>
struct Dictionary_2_t2731505821;
// System.Collections.Generic.IEqualityComparer`1<Pathfinding.Int3>
struct IEqualityComparer_1_t2765079998;
// System.Collections.Generic.IDictionary`2<Pathfinding.Int3,System.Int32>
struct IDictionary_2_t2309379166;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<Pathfinding.Int3>
struct ICollection_1_t2868635581;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t2048428487;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Int32>[]
struct KeyValuePair_2U5BU5D_t3232830822;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<Pathfinding.Int3,System.Int32>>
struct IEnumerator_1_t247184280;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int3,System.Int32>
struct KeyCollection_t63297976;
// System.Collections.Generic.Dictionary`2/ValueCollection<Pathfinding.Int3,System.Int32>
struct ValueCollection_t1432111534;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22630286527.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4048829213.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::.ctor()
extern "C"  void Dictionary_2__ctor_m3929272389_gshared (Dictionary_2_t2731505821 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3929272389(__this, method) ((  void (*) (Dictionary_2_t2731505821 *, const MethodInfo*))Dictionary_2__ctor_m3929272389_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m982637075_gshared (Dictionary_2_t2731505821 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m982637075(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t2731505821 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m982637075_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m412704124_gshared (Dictionary_2_t2731505821 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m412704124(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t2731505821 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m412704124_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m1028474093_gshared (Dictionary_2_t2731505821 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m1028474093(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t2731505821 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m1028474093_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m4181989377_gshared (Dictionary_2_t2731505821 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m4181989377(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t2731505821 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m4181989377_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m3351361757_gshared (Dictionary_2_t2731505821 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m3351361757(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2731505821 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m3351361757_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3705663346_gshared (Dictionary_2_t2731505821 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3705663346(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2731505821 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3705663346_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1023087026_gshared (Dictionary_2_t2731505821 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1023087026(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2731505821 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1023087026_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m2848850274_gshared (Dictionary_2_t2731505821 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m2848850274(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2731505821 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m2848850274_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m4218500560_gshared (Dictionary_2_t2731505821 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m4218500560(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2731505821 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m4218500560_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m4070322629_gshared (Dictionary_2_t2731505821 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m4070322629(__this, method) ((  bool (*) (Dictionary_2_t2731505821 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m4070322629_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1416593556_gshared (Dictionary_2_t2731505821 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1416593556(__this, method) ((  bool (*) (Dictionary_2_t2731505821 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1416593556_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m2539697196_gshared (Dictionary_2_t2731505821 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2539697196(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2731505821 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2539697196_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m3273483537_gshared (Dictionary_2_t2731505821 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3273483537(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2731505821 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m3273483537_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m93336224_gshared (Dictionary_2_t2731505821 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m93336224(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2731505821 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m93336224_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m1694689750_gshared (Dictionary_2_t2731505821 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1694689750(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2731505821 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1694689750_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2058756431_gshared (Dictionary_2_t2731505821 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m2058756431(__this, ___key0, method) ((  void (*) (Dictionary_2_t2731505821 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2058756431_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2976711998_gshared (Dictionary_2_t2731505821 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2976711998(__this, method) ((  bool (*) (Dictionary_2_t2731505821 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2976711998_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3650367978_gshared (Dictionary_2_t2731505821 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3650367978(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2731505821 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3650367978_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m4279784450_gshared (Dictionary_2_t2731505821 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m4279784450(__this, method) ((  bool (*) (Dictionary_2_t2731505821 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m4279784450_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1257871973_gshared (Dictionary_2_t2731505821 * __this, KeyValuePair_2_t2630286527  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1257871973(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t2731505821 *, KeyValuePair_2_t2630286527 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1257871973_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1481541757_gshared (Dictionary_2_t2731505821 * __this, KeyValuePair_2_t2630286527  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1481541757(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2731505821 *, KeyValuePair_2_t2630286527 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1481541757_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1323080457_gshared (Dictionary_2_t2731505821 * __this, KeyValuePair_2U5BU5D_t3232830822* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1323080457(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2731505821 *, KeyValuePair_2U5BU5D_t3232830822*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1323080457_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1643031266_gshared (Dictionary_2_t2731505821 * __this, KeyValuePair_2_t2630286527  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1643031266(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2731505821 *, KeyValuePair_2_t2630286527 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1643031266_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m3689167656_gshared (Dictionary_2_t2731505821 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m3689167656(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2731505821 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m3689167656_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3088471459_gshared (Dictionary_2_t2731505821 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3088471459(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2731505821 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3088471459_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3229849248_gshared (Dictionary_2_t2731505821 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3229849248(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2731505821 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3229849248_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3842584251_gshared (Dictionary_2_t2731505821 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3842584251(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2731505821 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3842584251_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3859606020_gshared (Dictionary_2_t2731505821 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m3859606020(__this, method) ((  int32_t (*) (Dictionary_2_t2731505821 *, const MethodInfo*))Dictionary_2_get_Count_m3859606020_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::get_Item(TKey)
extern "C"  int32_t Dictionary_2_get_Item_m1669660601_gshared (Dictionary_2_t2731505821 * __this, Int3_t1974045594  ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m1669660601(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t2731505821 *, Int3_t1974045594 , const MethodInfo*))Dictionary_2_get_Item_m1669660601_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m1427000924_gshared (Dictionary_2_t2731505821 * __this, Int3_t1974045594  ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m1427000924(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2731505821 *, Int3_t1974045594 , int32_t, const MethodInfo*))Dictionary_2_set_Item_m1427000924_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m3529445652_gshared (Dictionary_2_t2731505821 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m3529445652(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t2731505821 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3529445652_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m3461344547_gshared (Dictionary_2_t2731505821 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m3461344547(__this, ___size0, method) ((  void (*) (Dictionary_2_t2731505821 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3461344547_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m1307545567_gshared (Dictionary_2_t2731505821 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m1307545567(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2731505821 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m1307545567_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t2630286527  Dictionary_2_make_pair_m2556233259_gshared (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m2556233259(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2630286527  (*) (Il2CppObject * /* static, unused */, Int3_t1974045594 , int32_t, const MethodInfo*))Dictionary_2_make_pair_m2556233259_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::pick_key(TKey,TValue)
extern "C"  Int3_t1974045594  Dictionary_2_pick_key_m121173355_gshared (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m121173355(__this /* static, unused */, ___key0, ___value1, method) ((  Int3_t1974045594  (*) (Il2CppObject * /* static, unused */, Int3_t1974045594 , int32_t, const MethodInfo*))Dictionary_2_pick_key_m121173355_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::pick_value(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_value_m2872540395_gshared (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m2872540395(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Int3_t1974045594 , int32_t, const MethodInfo*))Dictionary_2_pick_value_m2872540395_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m2309764880_gshared (Dictionary_2_t2731505821 * __this, KeyValuePair_2U5BU5D_t3232830822* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m2309764880(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2731505821 *, KeyValuePair_2U5BU5D_t3232830822*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2309764880_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::Resize()
extern "C"  void Dictionary_2_Resize_m1498415644_gshared (Dictionary_2_t2731505821 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m1498415644(__this, method) ((  void (*) (Dictionary_2_t2731505821 *, const MethodInfo*))Dictionary_2_Resize_m1498415644_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m1948915137_gshared (Dictionary_2_t2731505821 * __this, Int3_t1974045594  ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m1948915137(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2731505821 *, Int3_t1974045594 , int32_t, const MethodInfo*))Dictionary_2_Add_m1948915137_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::Clear()
extern "C"  void Dictionary_2_Clear_m1335405680_gshared (Dictionary_2_t2731505821 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m1335405680(__this, method) ((  void (*) (Dictionary_2_t2731505821 *, const MethodInfo*))Dictionary_2_Clear_m1335405680_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m1570180512_gshared (Dictionary_2_t2731505821 * __this, Int3_t1974045594  ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1570180512(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2731505821 *, Int3_t1974045594 , const MethodInfo*))Dictionary_2_ContainsKey_m1570180512_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m554130413_gshared (Dictionary_2_t2731505821 * __this, int32_t ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m554130413(__this, ___value0, method) ((  bool (*) (Dictionary_2_t2731505821 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m554130413_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m635360058_gshared (Dictionary_2_t2731505821 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m635360058(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2731505821 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m635360058_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m711692138_gshared (Dictionary_2_t2731505821 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m711692138(__this, ___sender0, method) ((  void (*) (Dictionary_2_t2731505821 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m711692138_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2122850915_gshared (Dictionary_2_t2731505821 * __this, Int3_t1974045594  ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2122850915(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2731505821 *, Int3_t1974045594 , const MethodInfo*))Dictionary_2_Remove_m2122850915_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m1458075116_gshared (Dictionary_2_t2731505821 * __this, Int3_t1974045594  ___key0, int32_t* ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1458075116(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t2731505821 *, Int3_t1974045594 , int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m1458075116_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::get_Keys()
extern "C"  KeyCollection_t63297976 * Dictionary_2_get_Keys_m3508190457_gshared (Dictionary_2_t2731505821 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m3508190457(__this, method) ((  KeyCollection_t63297976 * (*) (Dictionary_2_t2731505821 *, const MethodInfo*))Dictionary_2_get_Keys_m3508190457_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::get_Values()
extern "C"  ValueCollection_t1432111534 * Dictionary_2_get_Values_m4185198265_gshared (Dictionary_2_t2731505821 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m4185198265(__this, method) ((  ValueCollection_t1432111534 * (*) (Dictionary_2_t2731505821 *, const MethodInfo*))Dictionary_2_get_Values_m4185198265_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::ToTKey(System.Object)
extern "C"  Int3_t1974045594  Dictionary_2_ToTKey_m3865999558_gshared (Dictionary_2_t2731505821 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m3865999558(__this, ___key0, method) ((  Int3_t1974045594  (*) (Dictionary_2_t2731505821 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3865999558_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::ToTValue(System.Object)
extern "C"  int32_t Dictionary_2_ToTValue_m1184854534_gshared (Dictionary_2_t2731505821 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1184854534(__this, ___value0, method) ((  int32_t (*) (Dictionary_2_t2731505821 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1184854534_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m4288985158_gshared (Dictionary_2_t2731505821 * __this, KeyValuePair_2_t2630286527  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m4288985158(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t2731505821 *, KeyValuePair_2_t2630286527 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m4288985158_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t4048829213  Dictionary_2_GetEnumerator_m2199387745_gshared (Dictionary_2_t2731505821 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m2199387745(__this, method) ((  Enumerator_t4048829213  (*) (Dictionary_2_t2731505821 *, const MethodInfo*))Dictionary_2_GetEnumerator_m2199387745_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m445847152_gshared (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___key0, int32_t ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m445847152(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, Int3_t1974045594 , int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m445847152_gshared)(__this /* static, unused */, ___key0, ___value1, method)
