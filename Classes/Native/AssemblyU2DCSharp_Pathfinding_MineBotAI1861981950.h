﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Animation
struct Animation_t1724966010;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "AssemblyU2DCSharp_AIPath1930792045.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.MineBotAI
struct  MineBotAI_t1861981950  : public AIPath_t1930792045
{
public:
	// UnityEngine.Animation Pathfinding.MineBotAI::anim
	Animation_t1724966010 * ___anim_33;
	// System.Single Pathfinding.MineBotAI::sleepVelocity
	float ___sleepVelocity_34;
	// System.Single Pathfinding.MineBotAI::animationSpeed
	float ___animationSpeed_35;
	// UnityEngine.GameObject Pathfinding.MineBotAI::endOfPathEffect
	GameObject_t3674682005 * ___endOfPathEffect_36;
	// UnityEngine.Vector3 Pathfinding.MineBotAI::lastTarget
	Vector3_t4282066566  ___lastTarget_37;

public:
	inline static int32_t get_offset_of_anim_33() { return static_cast<int32_t>(offsetof(MineBotAI_t1861981950, ___anim_33)); }
	inline Animation_t1724966010 * get_anim_33() const { return ___anim_33; }
	inline Animation_t1724966010 ** get_address_of_anim_33() { return &___anim_33; }
	inline void set_anim_33(Animation_t1724966010 * value)
	{
		___anim_33 = value;
		Il2CppCodeGenWriteBarrier(&___anim_33, value);
	}

	inline static int32_t get_offset_of_sleepVelocity_34() { return static_cast<int32_t>(offsetof(MineBotAI_t1861981950, ___sleepVelocity_34)); }
	inline float get_sleepVelocity_34() const { return ___sleepVelocity_34; }
	inline float* get_address_of_sleepVelocity_34() { return &___sleepVelocity_34; }
	inline void set_sleepVelocity_34(float value)
	{
		___sleepVelocity_34 = value;
	}

	inline static int32_t get_offset_of_animationSpeed_35() { return static_cast<int32_t>(offsetof(MineBotAI_t1861981950, ___animationSpeed_35)); }
	inline float get_animationSpeed_35() const { return ___animationSpeed_35; }
	inline float* get_address_of_animationSpeed_35() { return &___animationSpeed_35; }
	inline void set_animationSpeed_35(float value)
	{
		___animationSpeed_35 = value;
	}

	inline static int32_t get_offset_of_endOfPathEffect_36() { return static_cast<int32_t>(offsetof(MineBotAI_t1861981950, ___endOfPathEffect_36)); }
	inline GameObject_t3674682005 * get_endOfPathEffect_36() const { return ___endOfPathEffect_36; }
	inline GameObject_t3674682005 ** get_address_of_endOfPathEffect_36() { return &___endOfPathEffect_36; }
	inline void set_endOfPathEffect_36(GameObject_t3674682005 * value)
	{
		___endOfPathEffect_36 = value;
		Il2CppCodeGenWriteBarrier(&___endOfPathEffect_36, value);
	}

	inline static int32_t get_offset_of_lastTarget_37() { return static_cast<int32_t>(offsetof(MineBotAI_t1861981950, ___lastTarget_37)); }
	inline Vector3_t4282066566  get_lastTarget_37() const { return ___lastTarget_37; }
	inline Vector3_t4282066566 * get_address_of_lastTarget_37() { return &___lastTarget_37; }
	inline void set_lastTarget_37(Vector3_t4282066566  value)
	{
		___lastTarget_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
