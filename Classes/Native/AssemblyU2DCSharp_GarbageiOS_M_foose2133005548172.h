﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_foose213
struct  M_foose213_t3005548172  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_foose213::_kidrel
	uint32_t ____kidrel_0;
	// System.String GarbageiOS.M_foose213::_kistear
	String_t* ____kistear_1;
	// System.String GarbageiOS.M_foose213::_setralcou
	String_t* ____setralcou_2;
	// System.Boolean GarbageiOS.M_foose213::_rose
	bool ____rose_3;
	// System.Int32 GarbageiOS.M_foose213::_serreewaTisru
	int32_t ____serreewaTisru_4;
	// System.UInt32 GarbageiOS.M_foose213::_yearrororBereciher
	uint32_t ____yearrororBereciher_5;
	// System.UInt32 GarbageiOS.M_foose213::_calpasSirnar
	uint32_t ____calpasSirnar_6;
	// System.UInt32 GarbageiOS.M_foose213::_steewortou
	uint32_t ____steewortou_7;
	// System.Int32 GarbageiOS.M_foose213::_narrorRaveecea
	int32_t ____narrorRaveecea_8;
	// System.Int32 GarbageiOS.M_foose213::_riqai
	int32_t ____riqai_9;
	// System.Int32 GarbageiOS.M_foose213::_yirser
	int32_t ____yirser_10;
	// System.UInt32 GarbageiOS.M_foose213::_hutapear
	uint32_t ____hutapear_11;
	// System.Int32 GarbageiOS.M_foose213::_werepiryu
	int32_t ____werepiryu_12;
	// System.Int32 GarbageiOS.M_foose213::_fispallcho
	int32_t ____fispallcho_13;
	// System.String GarbageiOS.M_foose213::_cakearHiskal
	String_t* ____cakearHiskal_14;
	// System.Boolean GarbageiOS.M_foose213::_kasikouPourewor
	bool ____kasikouPourewor_15;
	// System.Single GarbageiOS.M_foose213::_leetaybowGemistree
	float ____leetaybowGemistree_16;
	// System.UInt32 GarbageiOS.M_foose213::_gastaibawCurqear
	uint32_t ____gastaibawCurqear_17;
	// System.UInt32 GarbageiOS.M_foose213::_yemyaikeePenor
	uint32_t ____yemyaikeePenor_18;
	// System.Single GarbageiOS.M_foose213::_fajearte
	float ____fajearte_19;
	// System.Boolean GarbageiOS.M_foose213::_wortallJootro
	bool ____wortallJootro_20;
	// System.Int32 GarbageiOS.M_foose213::_ceardriGasfisse
	int32_t ____ceardriGasfisse_21;
	// System.String GarbageiOS.M_foose213::_jastai
	String_t* ____jastai_22;
	// System.Single GarbageiOS.M_foose213::_nayrirsaKarday
	float ____nayrirsaKarday_23;
	// System.String GarbageiOS.M_foose213::_mamarlaViru
	String_t* ____mamarlaViru_24;
	// System.Int32 GarbageiOS.M_foose213::_chelchasda
	int32_t ____chelchasda_25;
	// System.UInt32 GarbageiOS.M_foose213::_degu
	uint32_t ____degu_26;
	// System.Int32 GarbageiOS.M_foose213::_pesiwhe
	int32_t ____pesiwhe_27;
	// System.Int32 GarbageiOS.M_foose213::_toodeqair
	int32_t ____toodeqair_28;
	// System.Boolean GarbageiOS.M_foose213::_durbair
	bool ____durbair_29;
	// System.UInt32 GarbageiOS.M_foose213::_seqaini
	uint32_t ____seqaini_30;
	// System.Boolean GarbageiOS.M_foose213::_fasa
	bool ____fasa_31;
	// System.String GarbageiOS.M_foose213::_bisgerjor
	String_t* ____bisgerjor_32;
	// System.Boolean GarbageiOS.M_foose213::_duneretai
	bool ____duneretai_33;
	// System.Single GarbageiOS.M_foose213::_serejurFenabear
	float ____serejurFenabear_34;
	// System.Single GarbageiOS.M_foose213::_qalljowrur
	float ____qalljowrur_35;
	// System.UInt32 GarbageiOS.M_foose213::_seacaNemwhirtur
	uint32_t ____seacaNemwhirtur_36;
	// System.Single GarbageiOS.M_foose213::_risral
	float ____risral_37;
	// System.Single GarbageiOS.M_foose213::_coletru
	float ____coletru_38;

public:
	inline static int32_t get_offset_of__kidrel_0() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____kidrel_0)); }
	inline uint32_t get__kidrel_0() const { return ____kidrel_0; }
	inline uint32_t* get_address_of__kidrel_0() { return &____kidrel_0; }
	inline void set__kidrel_0(uint32_t value)
	{
		____kidrel_0 = value;
	}

	inline static int32_t get_offset_of__kistear_1() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____kistear_1)); }
	inline String_t* get__kistear_1() const { return ____kistear_1; }
	inline String_t** get_address_of__kistear_1() { return &____kistear_1; }
	inline void set__kistear_1(String_t* value)
	{
		____kistear_1 = value;
		Il2CppCodeGenWriteBarrier(&____kistear_1, value);
	}

	inline static int32_t get_offset_of__setralcou_2() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____setralcou_2)); }
	inline String_t* get__setralcou_2() const { return ____setralcou_2; }
	inline String_t** get_address_of__setralcou_2() { return &____setralcou_2; }
	inline void set__setralcou_2(String_t* value)
	{
		____setralcou_2 = value;
		Il2CppCodeGenWriteBarrier(&____setralcou_2, value);
	}

	inline static int32_t get_offset_of__rose_3() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____rose_3)); }
	inline bool get__rose_3() const { return ____rose_3; }
	inline bool* get_address_of__rose_3() { return &____rose_3; }
	inline void set__rose_3(bool value)
	{
		____rose_3 = value;
	}

	inline static int32_t get_offset_of__serreewaTisru_4() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____serreewaTisru_4)); }
	inline int32_t get__serreewaTisru_4() const { return ____serreewaTisru_4; }
	inline int32_t* get_address_of__serreewaTisru_4() { return &____serreewaTisru_4; }
	inline void set__serreewaTisru_4(int32_t value)
	{
		____serreewaTisru_4 = value;
	}

	inline static int32_t get_offset_of__yearrororBereciher_5() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____yearrororBereciher_5)); }
	inline uint32_t get__yearrororBereciher_5() const { return ____yearrororBereciher_5; }
	inline uint32_t* get_address_of__yearrororBereciher_5() { return &____yearrororBereciher_5; }
	inline void set__yearrororBereciher_5(uint32_t value)
	{
		____yearrororBereciher_5 = value;
	}

	inline static int32_t get_offset_of__calpasSirnar_6() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____calpasSirnar_6)); }
	inline uint32_t get__calpasSirnar_6() const { return ____calpasSirnar_6; }
	inline uint32_t* get_address_of__calpasSirnar_6() { return &____calpasSirnar_6; }
	inline void set__calpasSirnar_6(uint32_t value)
	{
		____calpasSirnar_6 = value;
	}

	inline static int32_t get_offset_of__steewortou_7() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____steewortou_7)); }
	inline uint32_t get__steewortou_7() const { return ____steewortou_7; }
	inline uint32_t* get_address_of__steewortou_7() { return &____steewortou_7; }
	inline void set__steewortou_7(uint32_t value)
	{
		____steewortou_7 = value;
	}

	inline static int32_t get_offset_of__narrorRaveecea_8() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____narrorRaveecea_8)); }
	inline int32_t get__narrorRaveecea_8() const { return ____narrorRaveecea_8; }
	inline int32_t* get_address_of__narrorRaveecea_8() { return &____narrorRaveecea_8; }
	inline void set__narrorRaveecea_8(int32_t value)
	{
		____narrorRaveecea_8 = value;
	}

	inline static int32_t get_offset_of__riqai_9() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____riqai_9)); }
	inline int32_t get__riqai_9() const { return ____riqai_9; }
	inline int32_t* get_address_of__riqai_9() { return &____riqai_9; }
	inline void set__riqai_9(int32_t value)
	{
		____riqai_9 = value;
	}

	inline static int32_t get_offset_of__yirser_10() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____yirser_10)); }
	inline int32_t get__yirser_10() const { return ____yirser_10; }
	inline int32_t* get_address_of__yirser_10() { return &____yirser_10; }
	inline void set__yirser_10(int32_t value)
	{
		____yirser_10 = value;
	}

	inline static int32_t get_offset_of__hutapear_11() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____hutapear_11)); }
	inline uint32_t get__hutapear_11() const { return ____hutapear_11; }
	inline uint32_t* get_address_of__hutapear_11() { return &____hutapear_11; }
	inline void set__hutapear_11(uint32_t value)
	{
		____hutapear_11 = value;
	}

	inline static int32_t get_offset_of__werepiryu_12() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____werepiryu_12)); }
	inline int32_t get__werepiryu_12() const { return ____werepiryu_12; }
	inline int32_t* get_address_of__werepiryu_12() { return &____werepiryu_12; }
	inline void set__werepiryu_12(int32_t value)
	{
		____werepiryu_12 = value;
	}

	inline static int32_t get_offset_of__fispallcho_13() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____fispallcho_13)); }
	inline int32_t get__fispallcho_13() const { return ____fispallcho_13; }
	inline int32_t* get_address_of__fispallcho_13() { return &____fispallcho_13; }
	inline void set__fispallcho_13(int32_t value)
	{
		____fispallcho_13 = value;
	}

	inline static int32_t get_offset_of__cakearHiskal_14() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____cakearHiskal_14)); }
	inline String_t* get__cakearHiskal_14() const { return ____cakearHiskal_14; }
	inline String_t** get_address_of__cakearHiskal_14() { return &____cakearHiskal_14; }
	inline void set__cakearHiskal_14(String_t* value)
	{
		____cakearHiskal_14 = value;
		Il2CppCodeGenWriteBarrier(&____cakearHiskal_14, value);
	}

	inline static int32_t get_offset_of__kasikouPourewor_15() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____kasikouPourewor_15)); }
	inline bool get__kasikouPourewor_15() const { return ____kasikouPourewor_15; }
	inline bool* get_address_of__kasikouPourewor_15() { return &____kasikouPourewor_15; }
	inline void set__kasikouPourewor_15(bool value)
	{
		____kasikouPourewor_15 = value;
	}

	inline static int32_t get_offset_of__leetaybowGemistree_16() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____leetaybowGemistree_16)); }
	inline float get__leetaybowGemistree_16() const { return ____leetaybowGemistree_16; }
	inline float* get_address_of__leetaybowGemistree_16() { return &____leetaybowGemistree_16; }
	inline void set__leetaybowGemistree_16(float value)
	{
		____leetaybowGemistree_16 = value;
	}

	inline static int32_t get_offset_of__gastaibawCurqear_17() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____gastaibawCurqear_17)); }
	inline uint32_t get__gastaibawCurqear_17() const { return ____gastaibawCurqear_17; }
	inline uint32_t* get_address_of__gastaibawCurqear_17() { return &____gastaibawCurqear_17; }
	inline void set__gastaibawCurqear_17(uint32_t value)
	{
		____gastaibawCurqear_17 = value;
	}

	inline static int32_t get_offset_of__yemyaikeePenor_18() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____yemyaikeePenor_18)); }
	inline uint32_t get__yemyaikeePenor_18() const { return ____yemyaikeePenor_18; }
	inline uint32_t* get_address_of__yemyaikeePenor_18() { return &____yemyaikeePenor_18; }
	inline void set__yemyaikeePenor_18(uint32_t value)
	{
		____yemyaikeePenor_18 = value;
	}

	inline static int32_t get_offset_of__fajearte_19() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____fajearte_19)); }
	inline float get__fajearte_19() const { return ____fajearte_19; }
	inline float* get_address_of__fajearte_19() { return &____fajearte_19; }
	inline void set__fajearte_19(float value)
	{
		____fajearte_19 = value;
	}

	inline static int32_t get_offset_of__wortallJootro_20() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____wortallJootro_20)); }
	inline bool get__wortallJootro_20() const { return ____wortallJootro_20; }
	inline bool* get_address_of__wortallJootro_20() { return &____wortallJootro_20; }
	inline void set__wortallJootro_20(bool value)
	{
		____wortallJootro_20 = value;
	}

	inline static int32_t get_offset_of__ceardriGasfisse_21() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____ceardriGasfisse_21)); }
	inline int32_t get__ceardriGasfisse_21() const { return ____ceardriGasfisse_21; }
	inline int32_t* get_address_of__ceardriGasfisse_21() { return &____ceardriGasfisse_21; }
	inline void set__ceardriGasfisse_21(int32_t value)
	{
		____ceardriGasfisse_21 = value;
	}

	inline static int32_t get_offset_of__jastai_22() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____jastai_22)); }
	inline String_t* get__jastai_22() const { return ____jastai_22; }
	inline String_t** get_address_of__jastai_22() { return &____jastai_22; }
	inline void set__jastai_22(String_t* value)
	{
		____jastai_22 = value;
		Il2CppCodeGenWriteBarrier(&____jastai_22, value);
	}

	inline static int32_t get_offset_of__nayrirsaKarday_23() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____nayrirsaKarday_23)); }
	inline float get__nayrirsaKarday_23() const { return ____nayrirsaKarday_23; }
	inline float* get_address_of__nayrirsaKarday_23() { return &____nayrirsaKarday_23; }
	inline void set__nayrirsaKarday_23(float value)
	{
		____nayrirsaKarday_23 = value;
	}

	inline static int32_t get_offset_of__mamarlaViru_24() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____mamarlaViru_24)); }
	inline String_t* get__mamarlaViru_24() const { return ____mamarlaViru_24; }
	inline String_t** get_address_of__mamarlaViru_24() { return &____mamarlaViru_24; }
	inline void set__mamarlaViru_24(String_t* value)
	{
		____mamarlaViru_24 = value;
		Il2CppCodeGenWriteBarrier(&____mamarlaViru_24, value);
	}

	inline static int32_t get_offset_of__chelchasda_25() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____chelchasda_25)); }
	inline int32_t get__chelchasda_25() const { return ____chelchasda_25; }
	inline int32_t* get_address_of__chelchasda_25() { return &____chelchasda_25; }
	inline void set__chelchasda_25(int32_t value)
	{
		____chelchasda_25 = value;
	}

	inline static int32_t get_offset_of__degu_26() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____degu_26)); }
	inline uint32_t get__degu_26() const { return ____degu_26; }
	inline uint32_t* get_address_of__degu_26() { return &____degu_26; }
	inline void set__degu_26(uint32_t value)
	{
		____degu_26 = value;
	}

	inline static int32_t get_offset_of__pesiwhe_27() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____pesiwhe_27)); }
	inline int32_t get__pesiwhe_27() const { return ____pesiwhe_27; }
	inline int32_t* get_address_of__pesiwhe_27() { return &____pesiwhe_27; }
	inline void set__pesiwhe_27(int32_t value)
	{
		____pesiwhe_27 = value;
	}

	inline static int32_t get_offset_of__toodeqair_28() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____toodeqair_28)); }
	inline int32_t get__toodeqair_28() const { return ____toodeqair_28; }
	inline int32_t* get_address_of__toodeqair_28() { return &____toodeqair_28; }
	inline void set__toodeqair_28(int32_t value)
	{
		____toodeqair_28 = value;
	}

	inline static int32_t get_offset_of__durbair_29() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____durbair_29)); }
	inline bool get__durbair_29() const { return ____durbair_29; }
	inline bool* get_address_of__durbair_29() { return &____durbair_29; }
	inline void set__durbair_29(bool value)
	{
		____durbair_29 = value;
	}

	inline static int32_t get_offset_of__seqaini_30() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____seqaini_30)); }
	inline uint32_t get__seqaini_30() const { return ____seqaini_30; }
	inline uint32_t* get_address_of__seqaini_30() { return &____seqaini_30; }
	inline void set__seqaini_30(uint32_t value)
	{
		____seqaini_30 = value;
	}

	inline static int32_t get_offset_of__fasa_31() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____fasa_31)); }
	inline bool get__fasa_31() const { return ____fasa_31; }
	inline bool* get_address_of__fasa_31() { return &____fasa_31; }
	inline void set__fasa_31(bool value)
	{
		____fasa_31 = value;
	}

	inline static int32_t get_offset_of__bisgerjor_32() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____bisgerjor_32)); }
	inline String_t* get__bisgerjor_32() const { return ____bisgerjor_32; }
	inline String_t** get_address_of__bisgerjor_32() { return &____bisgerjor_32; }
	inline void set__bisgerjor_32(String_t* value)
	{
		____bisgerjor_32 = value;
		Il2CppCodeGenWriteBarrier(&____bisgerjor_32, value);
	}

	inline static int32_t get_offset_of__duneretai_33() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____duneretai_33)); }
	inline bool get__duneretai_33() const { return ____duneretai_33; }
	inline bool* get_address_of__duneretai_33() { return &____duneretai_33; }
	inline void set__duneretai_33(bool value)
	{
		____duneretai_33 = value;
	}

	inline static int32_t get_offset_of__serejurFenabear_34() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____serejurFenabear_34)); }
	inline float get__serejurFenabear_34() const { return ____serejurFenabear_34; }
	inline float* get_address_of__serejurFenabear_34() { return &____serejurFenabear_34; }
	inline void set__serejurFenabear_34(float value)
	{
		____serejurFenabear_34 = value;
	}

	inline static int32_t get_offset_of__qalljowrur_35() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____qalljowrur_35)); }
	inline float get__qalljowrur_35() const { return ____qalljowrur_35; }
	inline float* get_address_of__qalljowrur_35() { return &____qalljowrur_35; }
	inline void set__qalljowrur_35(float value)
	{
		____qalljowrur_35 = value;
	}

	inline static int32_t get_offset_of__seacaNemwhirtur_36() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____seacaNemwhirtur_36)); }
	inline uint32_t get__seacaNemwhirtur_36() const { return ____seacaNemwhirtur_36; }
	inline uint32_t* get_address_of__seacaNemwhirtur_36() { return &____seacaNemwhirtur_36; }
	inline void set__seacaNemwhirtur_36(uint32_t value)
	{
		____seacaNemwhirtur_36 = value;
	}

	inline static int32_t get_offset_of__risral_37() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____risral_37)); }
	inline float get__risral_37() const { return ____risral_37; }
	inline float* get_address_of__risral_37() { return &____risral_37; }
	inline void set__risral_37(float value)
	{
		____risral_37 = value;
	}

	inline static int32_t get_offset_of__coletru_38() { return static_cast<int32_t>(offsetof(M_foose213_t3005548172, ____coletru_38)); }
	inline float get__coletru_38() const { return ____coletru_38; }
	inline float* get_address_of__coletru_38() { return &____coletru_38; }
	inline void set__coletru_38(float value)
	{
		____coletru_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
