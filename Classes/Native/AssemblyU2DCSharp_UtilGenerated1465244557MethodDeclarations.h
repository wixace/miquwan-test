﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UtilGenerated
struct UtilGenerated_t1465244557;
// JSVCall
struct JSVCall_t3708497963;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Char[]
struct CharU5BU5D_t3324145743;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.UInt32[]
struct UInt32U5BU5D_t3230734560;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.String>>
struct List_1_t2743602661;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UtilGenerated::.ctor()
extern "C"  void UtilGenerated__ctor_m1597912190 (UtilGenerated_t1465244557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_Util1(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_Util1_m3648183284 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_AntiCheating(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_AntiCheating_m3005808590 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_Begin_CS_Sample__String(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_Begin_CS_Sample__String_m3661844025 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_ChangeShader__GameObject__Color__String__String(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_ChangeShader__GameObject__Color__String__String_m3584697924 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_EncodingUTF8__String(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_EncodingUTF8__String_m1958823058 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_End_CS_Sample(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_End_CS_Sample_m3173702938 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_FormatColorByStr__String__Char(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_FormatColorByStr__String__Char_m3756789842 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_FormatDateTime__Double(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_FormatDateTime__Double_m2354807328 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_FormatDateTime__UInt32(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_FormatDateTime__UInt32_m1651396456 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_FormatDateTime_world__Double(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_FormatDateTime_world__Double_m4125558515 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_FormatIntToString_KB__Single(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_FormatIntToString_KB__Single_m1226758679 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_FormatTimeMMDD__UInt32(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_FormatTimeMMDD__UInt32_m4125700794 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_FormatTimeSpan__UInt32(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_FormatTimeSpan__UInt32_m1959696004 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_FormatTimeYYMMDD__UInt32(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_FormatTimeYYMMDD__UInt32_m305588698 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_FormatUIString__Int32__Object_Array(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_FormatUIString__Int32__Object_Array_m317983632 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_FormatUIString__String__Object_Array(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_FormatUIString__String__Object_Array_m1860807075 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_FormatVector3ByStr__String__Char(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_FormatVector3ByStr__String__Char_m316926533 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_FromBase64String__String(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_FromBase64String__String_m1982436280 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_GetArticle_Float__String(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_GetArticle_Float__String_m2193983339 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_GetArticle_Int__String(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_GetArticle_Int__String_m2742763774 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_GetArticle_Uint__String(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_GetArticle_Uint__String_m462897903 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_GetAttributeName__String(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_GetAttributeName__String_m3748603391 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_GetBeizerList__Vector3__Vector3__Vector3__Int32(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_GetBeizerList__Vector3__Vector3__Vector3__Int32_m1837404696 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_getBytesNum__String(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_getBytesNum__String_m1193151495 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_GetCrc32__String(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_GetCrc32__String_m1505685227 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_GetCurProtectTime__Int32(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_GetCurProtectTime__Int32_m1876534617 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_GetHHMMSS__Double(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_GetHHMMSS__Double_m1622435116 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_GetList(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_GetList_m3961425369 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_GetListArr(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_GetListArr_m1102215882 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_GetOnlyId(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_GetOnlyId_m3104750562 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_GetPhysicsPos__Vector3(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_GetPhysicsPos__Vector3_m3855413322 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_GetRandomString__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_GetRandomString__Int32__Int32__Int32_m313113409 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_GetVector3__String(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_GetVector3__String_m2795943112 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_GetVector3_Arr__String(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_GetVector3_Arr__String_m751514570 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_GetYPos__Vector3(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_GetYPos__Vector3_m1485325948 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_GetYYMMDD__UInt32(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_GetYYMMDD__UInt32_m2397516468 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_MD5Encrypt__String(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_MD5Encrypt__String_m1273104341 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_MobileVebrate(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_MobileVebrate_m3411732982 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_PanelDown(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_PanelDown_m1952442123 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_PanelUp(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_PanelUp_m1318581828 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_RegexIsMatch__String__String(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_RegexIsMatch__String__String_m1133343219 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_Replace__String__String__String(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_Replace__String__String__String_m891300972 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_SplitStr__String__Char_Array(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_SplitStr__String__Char_Array_m1567532725 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_SplitStr_commn__String(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_SplitStr_commn__String_m1115785480 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_ToBase64String__Byte_Array(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_ToBase64String__Byte_Array_m2053571130 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_ToJsoin__ListT1_ListT1_String(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_ToJsoin__ListT1_ListT1_String_m3525304262 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_ZipCompress__String(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_ZipCompress__String_m3022004601 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::Util_ZipDecompress__Byte_Array(JSVCall,System.Int32)
extern "C"  bool UtilGenerated_Util_ZipDecompress__Byte_Array_m3169883499 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UtilGenerated::__Register()
extern "C"  void UtilGenerated___Register_m1881564937 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] UtilGenerated::<Util_FormatUIString__Int32__Object_Array>m__2F3()
extern "C"  ObjectU5BU5D_t1108656482* UtilGenerated_U3CUtil_FormatUIString__Int32__Object_ArrayU3Em__2F3_m2198355993 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] UtilGenerated::<Util_FormatUIString__String__Object_Array>m__2F4()
extern "C"  ObjectU5BU5D_t1108656482* UtilGenerated_U3CUtil_FormatUIString__String__Object_ArrayU3Em__2F4_m3283546041 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char[] UtilGenerated::<Util_SplitStr__String__Char_Array>m__2F5()
extern "C"  CharU5BU5D_t3324145743* UtilGenerated_U3CUtil_SplitStr__String__Char_ArrayU3Em__2F5_m3446974577 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UtilGenerated::<Util_ToBase64String__Byte_Array>m__2F6()
extern "C"  ByteU5BU5D_t4260760469* UtilGenerated_U3CUtil_ToBase64String__Byte_ArrayU3Em__2F6_m375494683 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UtilGenerated::<Util_ZipDecompress__Byte_Array>m__2F7()
extern "C"  ByteU5BU5D_t4260760469* UtilGenerated_U3CUtil_ZipDecompress__Byte_ArrayU3Em__2F7_m1103070123 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UtilGenerated_ilo_attachFinalizerObject1_m3871685689 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UtilGenerated::ilo_Begin_CS_Sample2(System.String)
extern "C"  void UtilGenerated_ilo_Begin_CS_Sample2_m2892994842 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UtilGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UtilGenerated_ilo_getObject3_m1541427817 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UtilGenerated::ilo_getStringS4(System.Int32)
extern "C"  String_t* UtilGenerated_ilo_getStringS4_m3708390309 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UtilGenerated::ilo_FormatColorByStr5(System.String,System.Char)
extern "C"  Color_t4194546905  UtilGenerated_ilo_FormatColorByStr5_m147493058 (Il2CppObject * __this /* static, unused */, String_t* ___str0, Il2CppChar ___c1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UtilGenerated::ilo_setObject6(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UtilGenerated_ilo_setObject6_m1710319541 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double UtilGenerated::ilo_getDouble7(System.Int32)
extern "C"  double UtilGenerated_ilo_getDouble7_m3944784951 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 UtilGenerated::ilo_getUInt328(System.Int32)
extern "C"  uint32_t UtilGenerated_ilo_getUInt328_m3088337272 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UtilGenerated::ilo_getSingle9(System.Int32)
extern "C"  float UtilGenerated_ilo_getSingle9_m130221401 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UtilGenerated::ilo_setStringS10(System.Int32,System.String)
extern "C"  void UtilGenerated_ilo_setStringS10_m1711616549 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UtilGenerated::ilo_FormatUIString11(System.Int32,System.Object[])
extern "C"  String_t* UtilGenerated_ilo_FormatUIString11_m3325764047 (Il2CppObject * __this /* static, unused */, int32_t ___key0, ObjectU5BU5D_t1108656482* ___datas1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UtilGenerated::ilo_setVector3S12(System.Int32,UnityEngine.Vector3)
extern "C"  void UtilGenerated_ilo_setVector3S12_m1071586291 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 UtilGenerated::ilo_getChar13(System.Int32)
extern "C"  int16_t UtilGenerated_ilo_getChar13_m294076912 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UtilGenerated::ilo_setArrayS14(System.Int32,System.Int32,System.Boolean)
extern "C"  void UtilGenerated_ilo_setArrayS14_m325858239 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UtilGenerated::ilo_setSingle15(System.Int32,System.Single)
extern "C"  void UtilGenerated_ilo_setSingle15_m526906317 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] UtilGenerated::ilo_GetArticle_Int16(System.String)
extern "C"  Int32U5BU5D_t3230847821* UtilGenerated_ilo_GetArticle_Int16_m1570036992 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32[] UtilGenerated::ilo_GetArticle_Uint17(System.String)
extern "C"  UInt32U5BU5D_t3230734560* UtilGenerated_ilo_GetArticle_Uint17_m1404445921 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UtilGenerated::ilo_getVector3S18(System.Int32)
extern "C"  Vector3_t4282066566  UtilGenerated_ilo_getVector3S18_m2880686804 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UtilGenerated::ilo_getInt3219(System.Int32)
extern "C"  int32_t UtilGenerated_ilo_getInt3219_m4136084060 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UtilGenerated::ilo_moveSaveID2Arr20(System.Int32)
extern "C"  void UtilGenerated_ilo_moveSaveID2Arr20_m1217229504 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UtilGenerated::ilo_getBytesNum21(System.String)
extern "C"  int32_t UtilGenerated_ilo_getBytesNum21_m1949576859 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UtilGenerated::ilo_GetCrc3222(System.String)
extern "C"  String_t* UtilGenerated_ilo_GetCrc3222_m1078544047 (Il2CppObject * __this /* static, unused */, String_t* ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> UtilGenerated::ilo_GetList23()
extern "C"  List_1_t1375417109 * UtilGenerated_ilo_GetList23_m821853647 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.String>> UtilGenerated::ilo_GetListArr24()
extern "C"  List_1_t2743602661 * UtilGenerated_ilo_GetListArr24_m838770473 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UtilGenerated::ilo_GetVector3_Arr25(System.String)
extern "C"  Vector3U5BU5D_t215400611* UtilGenerated_ilo_GetVector3_Arr25_m805340226 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UtilGenerated::ilo_PanelUp26()
extern "C"  void UtilGenerated_ilo_PanelUp26_m484361228 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UtilGenerated::ilo_RegexIsMatch27(System.String,System.String)
extern "C"  bool UtilGenerated_ilo_RegexIsMatch27_m820339776 (Il2CppObject * __this /* static, unused */, String_t* ___regex0, String_t* ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] UtilGenerated::ilo_SplitStr28(System.String,System.Char[])
extern "C"  StringU5BU5D_t4054002952* UtilGenerated_ilo_SplitStr28_m4258549826 (Il2CppObject * __this /* static, unused */, String_t* ___sFile0, CharU5BU5D_t3324145743* ___separator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UtilGenerated::ilo_ToBase64String29(System.Byte[])
extern "C"  String_t* UtilGenerated_ilo_ToBase64String29_m3734264113 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___datas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UtilGenerated::ilo_ZipCompress30(System.String)
extern "C"  ByteU5BU5D_t4260760469* UtilGenerated_ilo_ZipCompress30_m77810607 (Il2CppObject * __this /* static, unused */, String_t* ___jsonDatas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UtilGenerated::ilo_setByte31(System.Int32,System.Byte)
extern "C"  void UtilGenerated_ilo_setByte31_m3384960263 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint8_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UtilGenerated::ilo_ZipDecompress32(System.Byte[])
extern "C"  String_t* UtilGenerated_ilo_ZipDecompress32_m1756685690 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UtilGenerated::ilo_getObject33(System.Int32)
extern "C"  int32_t UtilGenerated_ilo_getObject33_m2235498525 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UtilGenerated::ilo_getWhatever34(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UtilGenerated_ilo_getWhatever34_m2763557150 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UtilGenerated::ilo_getArrayLength35(System.Int32)
extern "C"  int32_t UtilGenerated_ilo_getArrayLength35_m842094631 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UtilGenerated::ilo_getElement36(System.Int32,System.Int32)
extern "C"  int32_t UtilGenerated_ilo_getElement36_m2361271858 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte UtilGenerated::ilo_getByte37(System.Int32)
extern "C"  uint8_t UtilGenerated_ilo_getByte37_m921915240 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
