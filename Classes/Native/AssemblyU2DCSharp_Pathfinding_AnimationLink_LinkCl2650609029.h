﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AnimationClip
struct AnimationClip_t2007702890;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.AnimationLink/LinkClip
struct  LinkClip_t2650609029  : public Il2CppObject
{
public:
	// UnityEngine.AnimationClip Pathfinding.AnimationLink/LinkClip::clip
	AnimationClip_t2007702890 * ___clip_0;
	// UnityEngine.Vector3 Pathfinding.AnimationLink/LinkClip::velocity
	Vector3_t4282066566  ___velocity_1;
	// System.Int32 Pathfinding.AnimationLink/LinkClip::loopCount
	int32_t ___loopCount_2;

public:
	inline static int32_t get_offset_of_clip_0() { return static_cast<int32_t>(offsetof(LinkClip_t2650609029, ___clip_0)); }
	inline AnimationClip_t2007702890 * get_clip_0() const { return ___clip_0; }
	inline AnimationClip_t2007702890 ** get_address_of_clip_0() { return &___clip_0; }
	inline void set_clip_0(AnimationClip_t2007702890 * value)
	{
		___clip_0 = value;
		Il2CppCodeGenWriteBarrier(&___clip_0, value);
	}

	inline static int32_t get_offset_of_velocity_1() { return static_cast<int32_t>(offsetof(LinkClip_t2650609029, ___velocity_1)); }
	inline Vector3_t4282066566  get_velocity_1() const { return ___velocity_1; }
	inline Vector3_t4282066566 * get_address_of_velocity_1() { return &___velocity_1; }
	inline void set_velocity_1(Vector3_t4282066566  value)
	{
		___velocity_1 = value;
	}

	inline static int32_t get_offset_of_loopCount_2() { return static_cast<int32_t>(offsetof(LinkClip_t2650609029, ___loopCount_2)); }
	inline int32_t get_loopCount_2() const { return ___loopCount_2; }
	inline int32_t* get_address_of_loopCount_2() { return &___loopCount_2; }
	inline void set_loopCount_2(int32_t value)
	{
		___loopCount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
