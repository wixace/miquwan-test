﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen2974409999MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<Pathfinding.ABPath>::.ctor()
#define Stack_1__ctor_m1365198304(__this, method) ((  void (*) (Stack_1_t4286122072 *, const MethodInfo*))Stack_1__ctor_m2725689112_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Pathfinding.ABPath>::.ctor(System.Int32)
#define Stack_1__ctor_m1423125297(__this, ___count0, method) ((  void (*) (Stack_1_t4286122072 *, int32_t, const MethodInfo*))Stack_1__ctor_m3186788457_gshared)(__this, ___count0, method)
// System.Boolean System.Collections.Generic.Stack`1<Pathfinding.ABPath>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m3157883246(__this, method) ((  bool (*) (Stack_1_t4286122072 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m1582336274_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<Pathfinding.ABPath>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m2848444078(__this, method) ((  Il2CppObject * (*) (Stack_1_t4286122072 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m2938343088_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Pathfinding.ABPath>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m1702736740(__this, ___dest0, ___idx1, method) ((  void (*) (Stack_1_t4286122072 *, Il2CppArray *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m3277353260_gshared)(__this, ___dest0, ___idx1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<Pathfinding.ABPath>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4045759538(__this, method) ((  Il2CppObject* (*) (Stack_1_t4286122072 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m625377314_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<Pathfinding.ABPath>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m1858180979(__this, method) ((  Il2CppObject * (*) (Stack_1_t4286122072 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m4095051687_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Pathfinding.ABPath>::Clear()
#define Stack_1_Clear_m3066298891(__this, method) ((  void (*) (Stack_1_t4286122072 *, const MethodInfo*))Stack_1_Clear_m131822403_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<Pathfinding.ABPath>::Contains(T)
#define Stack_1_Contains_m4230011173(__this, ___t0, method) ((  bool (*) (Stack_1_t4286122072 *, ABPath_t1187561148 *, const MethodInfo*))Stack_1_Contains_m328948937_gshared)(__this, ___t0, method)
// T System.Collections.Generic.Stack`1<Pathfinding.ABPath>::Peek()
#define Stack_1_Peek_m3464831998(__this, method) ((  ABPath_t1187561148 * (*) (Stack_1_t4286122072 *, const MethodInfo*))Stack_1_Peek_m3418768488_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<Pathfinding.ABPath>::Pop()
#define Stack_1_Pop_m2467379824(__this, method) ((  ABPath_t1187561148 * (*) (Stack_1_t4286122072 *, const MethodInfo*))Stack_1_Pop_m4267009222_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<Pathfinding.ABPath>::Push(T)
#define Stack_1_Push_m1989675296(__this, ___t0, method) ((  void (*) (Stack_1_t4286122072 *, ABPath_t1187561148 *, const MethodInfo*))Stack_1_Push_m3350166104_gshared)(__this, ___t0, method)
// System.Int32 System.Collections.Generic.Stack`1<Pathfinding.ABPath>::get_Count()
#define Stack_1_get_Count_m2114525108(__this, method) ((  int32_t (*) (Stack_1_t4286122072 *, const MethodInfo*))Stack_1_get_Count_m3631765324_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<Pathfinding.ABPath>::GetEnumerator()
#define Stack_1_GetEnumerator_m3700181334(__this, method) ((  Enumerator_t3843908098  (*) (Stack_1_t4286122072 *, const MethodInfo*))Stack_1_GetEnumerator_m202302354_gshared)(__this, method)
