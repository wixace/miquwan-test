﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraShotMoveCfg
struct CameraShotMoveCfg_t2706860532;
// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraShotMoveCfg::.ctor()
extern "C"  void CameraShotMoveCfg__ctor_m2109879799 (CameraShotMoveCfg_t2706860532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension CameraShotMoveCfg::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * CameraShotMoveCfg_ProtoBuf_IExtensible_GetExtensionObject_m602484475 (CameraShotMoveCfg_t2706860532 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotMoveCfg::get_id()
extern "C"  int32_t CameraShotMoveCfg_get_id_m1763005475 (CameraShotMoveCfg_t2706860532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMoveCfg::set_id(System.Int32)
extern "C"  void CameraShotMoveCfg_set_id_m3448188726 (CameraShotMoveCfg_t2706860532 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotMoveCfg::get_heroOrNpcId()
extern "C"  int32_t CameraShotMoveCfg_get_heroOrNpcId_m1397862073 (CameraShotMoveCfg_t2706860532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMoveCfg::set_heroOrNpcId(System.Int32)
extern "C"  void CameraShotMoveCfg_set_heroOrNpcId_m3845865096 (CameraShotMoveCfg_t2706860532 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraShotMoveCfg::get_isHero()
extern "C"  int32_t CameraShotMoveCfg_get_isHero_m3419672972 (CameraShotMoveCfg_t2706860532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMoveCfg::set_isHero(System.Int32)
extern "C"  void CameraShotMoveCfg_set_isHero_m3431015967 (CameraShotMoveCfg_t2706860532 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotMoveCfg::get_posX()
extern "C"  float CameraShotMoveCfg_get_posX_m462305678 (CameraShotMoveCfg_t2706860532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMoveCfg::set_posX(System.Single)
extern "C"  void CameraShotMoveCfg_set_posX_m158994461 (CameraShotMoveCfg_t2706860532 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotMoveCfg::get_posY()
extern "C"  float CameraShotMoveCfg_get_posY_m462306639 (CameraShotMoveCfg_t2706860532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMoveCfg::set_posY(System.Single)
extern "C"  void CameraShotMoveCfg_set_posY_m3943427580 (CameraShotMoveCfg_t2706860532 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single CameraShotMoveCfg::get_posZ()
extern "C"  float CameraShotMoveCfg_get_posZ_m462307600 (CameraShotMoveCfg_t2706860532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraShotMoveCfg::set_posZ(System.Single)
extern "C"  void CameraShotMoveCfg_set_posZ_m3432893403 (CameraShotMoveCfg_t2706860532 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
