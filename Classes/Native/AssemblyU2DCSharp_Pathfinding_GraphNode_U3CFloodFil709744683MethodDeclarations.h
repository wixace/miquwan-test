﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.GraphNode/<FloodFill>c__AnonStorey10A
struct U3CFloodFillU3Ec__AnonStorey10A_t709744683;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.GraphNode/<FloodFill>c__AnonStorey10A::.ctor()
extern "C"  void U3CFloodFillU3Ec__AnonStorey10A__ctor_m836842912 (U3CFloodFillU3Ec__AnonStorey10A_t709744683 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.GraphNode/<FloodFill>c__AnonStorey10A::<>m__33C(Pathfinding.GraphNode)
extern "C"  void U3CFloodFillU3Ec__AnonStorey10A_U3CU3Em__33C_m2618467504 (U3CFloodFillU3Ec__AnonStorey10A_t709744683 * __this, GraphNode_t23612370 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
