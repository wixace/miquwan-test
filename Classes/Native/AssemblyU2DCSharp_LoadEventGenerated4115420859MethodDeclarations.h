﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadEventGenerated
struct LoadEventGenerated_t4115420859;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void LoadEventGenerated::.ctor()
extern "C"  void LoadEventGenerated__ctor_m905972800 (LoadEventGenerated_t4115420859 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadEventGenerated::LoadEvent_LoadEvent1(JSVCall,System.Int32)
extern "C"  bool LoadEventGenerated_LoadEvent_LoadEvent1_m3117036176 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadEventGenerated::LoadEvent_rate(JSVCall)
extern "C"  void LoadEventGenerated_LoadEvent_rate_m1990998872 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadEventGenerated::__Register()
extern "C"  void LoadEventGenerated___Register_m3607042183 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadEventGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void LoadEventGenerated_ilo_addJSCSRel1_m1265533692 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadEventGenerated::ilo_setInt322(System.Int32,System.Int32)
extern "C"  void LoadEventGenerated_ilo_setInt322_m2074285813 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
