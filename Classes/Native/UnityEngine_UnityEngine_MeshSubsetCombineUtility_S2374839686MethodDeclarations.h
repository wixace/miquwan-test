﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.MeshSubsetCombineUtility/SubMeshInstance
struct SubMeshInstance_t2374839686;
struct SubMeshInstance_t2374839686_marshaled_pinvoke;
struct SubMeshInstance_t2374839686_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct SubMeshInstance_t2374839686;
struct SubMeshInstance_t2374839686_marshaled_pinvoke;

extern "C" void SubMeshInstance_t2374839686_marshal_pinvoke(const SubMeshInstance_t2374839686& unmarshaled, SubMeshInstance_t2374839686_marshaled_pinvoke& marshaled);
extern "C" void SubMeshInstance_t2374839686_marshal_pinvoke_back(const SubMeshInstance_t2374839686_marshaled_pinvoke& marshaled, SubMeshInstance_t2374839686& unmarshaled);
extern "C" void SubMeshInstance_t2374839686_marshal_pinvoke_cleanup(SubMeshInstance_t2374839686_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct SubMeshInstance_t2374839686;
struct SubMeshInstance_t2374839686_marshaled_com;

extern "C" void SubMeshInstance_t2374839686_marshal_com(const SubMeshInstance_t2374839686& unmarshaled, SubMeshInstance_t2374839686_marshaled_com& marshaled);
extern "C" void SubMeshInstance_t2374839686_marshal_com_back(const SubMeshInstance_t2374839686_marshaled_com& marshaled, SubMeshInstance_t2374839686& unmarshaled);
extern "C" void SubMeshInstance_t2374839686_marshal_com_cleanup(SubMeshInstance_t2374839686_marshaled_com& marshaled);
