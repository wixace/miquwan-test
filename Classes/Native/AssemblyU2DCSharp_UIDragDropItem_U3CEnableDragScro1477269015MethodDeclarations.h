﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIDragDropItem/<EnableDragScrollView>c__Iterator26
struct U3CEnableDragScrollViewU3Ec__Iterator26_t1477269015;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void UIDragDropItem/<EnableDragScrollView>c__Iterator26::.ctor()
extern "C"  void U3CEnableDragScrollViewU3Ec__Iterator26__ctor_m497824868 (U3CEnableDragScrollViewU3Ec__Iterator26_t1477269015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIDragDropItem/<EnableDragScrollView>c__Iterator26::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CEnableDragScrollViewU3Ec__Iterator26_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1607674040 (U3CEnableDragScrollViewU3Ec__Iterator26_t1477269015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIDragDropItem/<EnableDragScrollView>c__Iterator26::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CEnableDragScrollViewU3Ec__Iterator26_System_Collections_IEnumerator_get_Current_m488479820 (U3CEnableDragScrollViewU3Ec__Iterator26_t1477269015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDragDropItem/<EnableDragScrollView>c__Iterator26::MoveNext()
extern "C"  bool U3CEnableDragScrollViewU3Ec__Iterator26_MoveNext_m2170806776 (U3CEnableDragScrollViewU3Ec__Iterator26_t1477269015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem/<EnableDragScrollView>c__Iterator26::Dispose()
extern "C"  void U3CEnableDragScrollViewU3Ec__Iterator26_Dispose_m1566611361 (U3CEnableDragScrollViewU3Ec__Iterator26_t1477269015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItem/<EnableDragScrollView>c__Iterator26::Reset()
extern "C"  void U3CEnableDragScrollViewU3Ec__Iterator26_Reset_m2439225105 (U3CEnableDragScrollViewU3Ec__Iterator26_t1477269015 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
