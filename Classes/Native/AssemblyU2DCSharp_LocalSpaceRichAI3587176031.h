﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LocalSpaceGraph
struct LocalSpaceGraph_t660012435;

#include "AssemblyU2DCSharp_Pathfinding_RichAI1710845178.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LocalSpaceRichAI
struct  LocalSpaceRichAI_t3587176031  : public RichAI_t1710845178
{
public:
	// LocalSpaceGraph LocalSpaceRichAI::graph
	LocalSpaceGraph_t660012435 * ___graph_42;

public:
	inline static int32_t get_offset_of_graph_42() { return static_cast<int32_t>(offsetof(LocalSpaceRichAI_t3587176031, ___graph_42)); }
	inline LocalSpaceGraph_t660012435 * get_graph_42() const { return ___graph_42; }
	inline LocalSpaceGraph_t660012435 ** get_address_of_graph_42() { return &___graph_42; }
	inline void set_graph_42(LocalSpaceGraph_t660012435 * value)
	{
		___graph_42 = value;
		Il2CppCodeGenWriteBarrier(&___graph_42, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
