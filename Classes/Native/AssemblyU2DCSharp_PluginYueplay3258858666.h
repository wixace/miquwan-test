﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginYueplay
struct  PluginYueplay_t3258858666  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginYueplay::uid
	String_t* ___uid_2;
	// System.String PluginYueplay::token
	String_t* ___token_3;
	// System.String PluginYueplay::userName
	String_t* ___userName_4;
	// System.String PluginYueplay::authroizeCode
	String_t* ___authroizeCode_5;
	// System.String PluginYueplay::CheckID
	String_t* ___CheckID_6;
	// System.String PluginYueplay::configId
	String_t* ___configId_7;
	// Mihua.SDK.PayInfo PluginYueplay::payInfo
	PayInfo_t1775308120 * ___payInfo_8;
	// System.Boolean PluginYueplay::isOut
	bool ___isOut_9;

public:
	inline static int32_t get_offset_of_uid_2() { return static_cast<int32_t>(offsetof(PluginYueplay_t3258858666, ___uid_2)); }
	inline String_t* get_uid_2() const { return ___uid_2; }
	inline String_t** get_address_of_uid_2() { return &___uid_2; }
	inline void set_uid_2(String_t* value)
	{
		___uid_2 = value;
		Il2CppCodeGenWriteBarrier(&___uid_2, value);
	}

	inline static int32_t get_offset_of_token_3() { return static_cast<int32_t>(offsetof(PluginYueplay_t3258858666, ___token_3)); }
	inline String_t* get_token_3() const { return ___token_3; }
	inline String_t** get_address_of_token_3() { return &___token_3; }
	inline void set_token_3(String_t* value)
	{
		___token_3 = value;
		Il2CppCodeGenWriteBarrier(&___token_3, value);
	}

	inline static int32_t get_offset_of_userName_4() { return static_cast<int32_t>(offsetof(PluginYueplay_t3258858666, ___userName_4)); }
	inline String_t* get_userName_4() const { return ___userName_4; }
	inline String_t** get_address_of_userName_4() { return &___userName_4; }
	inline void set_userName_4(String_t* value)
	{
		___userName_4 = value;
		Il2CppCodeGenWriteBarrier(&___userName_4, value);
	}

	inline static int32_t get_offset_of_authroizeCode_5() { return static_cast<int32_t>(offsetof(PluginYueplay_t3258858666, ___authroizeCode_5)); }
	inline String_t* get_authroizeCode_5() const { return ___authroizeCode_5; }
	inline String_t** get_address_of_authroizeCode_5() { return &___authroizeCode_5; }
	inline void set_authroizeCode_5(String_t* value)
	{
		___authroizeCode_5 = value;
		Il2CppCodeGenWriteBarrier(&___authroizeCode_5, value);
	}

	inline static int32_t get_offset_of_CheckID_6() { return static_cast<int32_t>(offsetof(PluginYueplay_t3258858666, ___CheckID_6)); }
	inline String_t* get_CheckID_6() const { return ___CheckID_6; }
	inline String_t** get_address_of_CheckID_6() { return &___CheckID_6; }
	inline void set_CheckID_6(String_t* value)
	{
		___CheckID_6 = value;
		Il2CppCodeGenWriteBarrier(&___CheckID_6, value);
	}

	inline static int32_t get_offset_of_configId_7() { return static_cast<int32_t>(offsetof(PluginYueplay_t3258858666, ___configId_7)); }
	inline String_t* get_configId_7() const { return ___configId_7; }
	inline String_t** get_address_of_configId_7() { return &___configId_7; }
	inline void set_configId_7(String_t* value)
	{
		___configId_7 = value;
		Il2CppCodeGenWriteBarrier(&___configId_7, value);
	}

	inline static int32_t get_offset_of_payInfo_8() { return static_cast<int32_t>(offsetof(PluginYueplay_t3258858666, ___payInfo_8)); }
	inline PayInfo_t1775308120 * get_payInfo_8() const { return ___payInfo_8; }
	inline PayInfo_t1775308120 ** get_address_of_payInfo_8() { return &___payInfo_8; }
	inline void set_payInfo_8(PayInfo_t1775308120 * value)
	{
		___payInfo_8 = value;
		Il2CppCodeGenWriteBarrier(&___payInfo_8, value);
	}

	inline static int32_t get_offset_of_isOut_9() { return static_cast<int32_t>(offsetof(PluginYueplay_t3258858666, ___isOut_9)); }
	inline bool get_isOut_9() const { return ___isOut_9; }
	inline bool* get_address_of_isOut_9() { return &___isOut_9; }
	inline void set_isOut_9(bool value)
	{
		___isOut_9 = value;
	}
};

struct PluginYueplay_t3258858666_StaticFields
{
public:
	// System.Action PluginYueplay::<>f__am$cache8
	Action_t3771233898 * ___U3CU3Ef__amU24cache8_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_10() { return static_cast<int32_t>(offsetof(PluginYueplay_t3258858666_StaticFields, ___U3CU3Ef__amU24cache8_10)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache8_10() const { return ___U3CU3Ef__amU24cache8_10; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache8_10() { return &___U3CU3Ef__amU24cache8_10; }
	inline void set_U3CU3Ef__amU24cache8_10(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache8_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
