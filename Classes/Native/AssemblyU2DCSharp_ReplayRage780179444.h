﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_ReplayBase779703160.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayRage
struct  ReplayRage_t780179444  : public ReplayBase_t779703160
{
public:
	// System.Single ReplayRage::delta
	float ___delta_8;

public:
	inline static int32_t get_offset_of_delta_8() { return static_cast<int32_t>(offsetof(ReplayRage_t780179444, ___delta_8)); }
	inline float get_delta_8() const { return ___delta_8; }
	inline float* get_address_of_delta_8() { return &___delta_8; }
	inline void set_delta_8(float value)
	{
		___delta_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
