﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TextStringCfg
struct TextStringCfg_t884535238;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void TextStringCfg::.ctor()
extern "C"  void TextStringCfg__ctor_m40620133 (TextStringCfg_t884535238 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TextStringCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void TextStringCfg_Init_m4263749888 (TextStringCfg_t884535238 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
