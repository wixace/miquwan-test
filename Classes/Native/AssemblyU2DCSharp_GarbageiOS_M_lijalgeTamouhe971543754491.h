﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_lijalgeTamouhe97
struct  M_lijalgeTamouhe97_t1543754491  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_lijalgeTamouhe97::_jaixozeeYeesel
	float ____jaixozeeYeesel_0;
	// System.UInt32 GarbageiOS.M_lijalgeTamouhe97::_douvou
	uint32_t ____douvou_1;
	// System.UInt32 GarbageiOS.M_lijalgeTamouhe97::_roururcear
	uint32_t ____roururcear_2;
	// System.Int32 GarbageiOS.M_lijalgeTamouhe97::_dresareHalketoo
	int32_t ____dresareHalketoo_3;
	// System.String GarbageiOS.M_lijalgeTamouhe97::_naysis
	String_t* ____naysis_4;
	// System.Int32 GarbageiOS.M_lijalgeTamouhe97::_dorsipay
	int32_t ____dorsipay_5;
	// System.Single GarbageiOS.M_lijalgeTamouhe97::_lasrapowNeneji
	float ____lasrapowNeneji_6;
	// System.Boolean GarbageiOS.M_lijalgeTamouhe97::_jemallSooyoustall
	bool ____jemallSooyoustall_7;
	// System.String GarbageiOS.M_lijalgeTamouhe97::_zemseGosassi
	String_t* ____zemseGosassi_8;
	// System.Boolean GarbageiOS.M_lijalgeTamouhe97::_corkisstu
	bool ____corkisstu_9;
	// System.String GarbageiOS.M_lijalgeTamouhe97::_teetawball
	String_t* ____teetawball_10;
	// System.Single GarbageiOS.M_lijalgeTamouhe97::_fakeToochow
	float ____fakeToochow_11;
	// System.Boolean GarbageiOS.M_lijalgeTamouhe97::_yasxihe
	bool ____yasxihe_12;
	// System.UInt32 GarbageiOS.M_lijalgeTamouhe97::_tedranaw
	uint32_t ____tedranaw_13;
	// System.Int32 GarbageiOS.M_lijalgeTamouhe97::_cairfege
	int32_t ____cairfege_14;
	// System.String GarbageiOS.M_lijalgeTamouhe97::_jadrerezo
	String_t* ____jadrerezo_15;
	// System.Single GarbageiOS.M_lijalgeTamouhe97::_nijarchu
	float ____nijarchu_16;
	// System.String GarbageiOS.M_lijalgeTamouhe97::_noutoNerememir
	String_t* ____noutoNerememir_17;
	// System.UInt32 GarbageiOS.M_lijalgeTamouhe97::_nairvaw
	uint32_t ____nairvaw_18;
	// System.String GarbageiOS.M_lijalgeTamouhe97::_wafe
	String_t* ____wafe_19;
	// System.UInt32 GarbageiOS.M_lijalgeTamouhe97::_potigairTihalldair
	uint32_t ____potigairTihalldair_20;
	// System.Boolean GarbageiOS.M_lijalgeTamouhe97::_saleepiTairlerexere
	bool ____saleepiTairlerexere_21;
	// System.String GarbageiOS.M_lijalgeTamouhe97::_sawborpo
	String_t* ____sawborpo_22;
	// System.String GarbageiOS.M_lijalgeTamouhe97::_seducuCeedife
	String_t* ____seducuCeedife_23;
	// System.Int32 GarbageiOS.M_lijalgeTamouhe97::_bivuTasxi
	int32_t ____bivuTasxi_24;
	// System.Int32 GarbageiOS.M_lijalgeTamouhe97::_lalcairSinawhur
	int32_t ____lalcairSinawhur_25;
	// System.Boolean GarbageiOS.M_lijalgeTamouhe97::_carchurhe
	bool ____carchurhe_26;
	// System.String GarbageiOS.M_lijalgeTamouhe97::_joukememDraircuna
	String_t* ____joukememDraircuna_27;
	// System.Boolean GarbageiOS.M_lijalgeTamouhe97::_stobaspel
	bool ____stobaspel_28;
	// System.String GarbageiOS.M_lijalgeTamouhe97::_hucesiQesea
	String_t* ____hucesiQesea_29;
	// System.String GarbageiOS.M_lijalgeTamouhe97::_ceecas
	String_t* ____ceecas_30;
	// System.Int32 GarbageiOS.M_lijalgeTamouhe97::_wirbiLati
	int32_t ____wirbiLati_31;
	// System.UInt32 GarbageiOS.M_lijalgeTamouhe97::_sugeHornarcur
	uint32_t ____sugeHornarcur_32;
	// System.Single GarbageiOS.M_lijalgeTamouhe97::_choqorFacer
	float ____choqorFacer_33;
	// System.Int32 GarbageiOS.M_lijalgeTamouhe97::_xorcaCola
	int32_t ____xorcaCola_34;
	// System.UInt32 GarbageiOS.M_lijalgeTamouhe97::_moheljorWallteeza
	uint32_t ____moheljorWallteeza_35;
	// System.String GarbageiOS.M_lijalgeTamouhe97::_waiwaswhairPalkearxe
	String_t* ____waiwaswhairPalkearxe_36;
	// System.Single GarbageiOS.M_lijalgeTamouhe97::_deaxasirHesatou
	float ____deaxasirHesatou_37;
	// System.UInt32 GarbageiOS.M_lijalgeTamouhe97::_saruchowFallcaysoo
	uint32_t ____saruchowFallcaysoo_38;
	// System.Boolean GarbageiOS.M_lijalgeTamouhe97::_muki
	bool ____muki_39;
	// System.String GarbageiOS.M_lijalgeTamouhe97::_vaiqesall
	String_t* ____vaiqesall_40;
	// System.Boolean GarbageiOS.M_lijalgeTamouhe97::_selor
	bool ____selor_41;
	// System.Int32 GarbageiOS.M_lijalgeTamouhe97::_xiqiMizeredra
	int32_t ____xiqiMizeredra_42;
	// System.UInt32 GarbageiOS.M_lijalgeTamouhe97::_tranene
	uint32_t ____tranene_43;
	// System.Int32 GarbageiOS.M_lijalgeTamouhe97::_stikayXarzi
	int32_t ____stikayXarzi_44;
	// System.String GarbageiOS.M_lijalgeTamouhe97::_whene
	String_t* ____whene_45;

public:
	inline static int32_t get_offset_of__jaixozeeYeesel_0() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____jaixozeeYeesel_0)); }
	inline float get__jaixozeeYeesel_0() const { return ____jaixozeeYeesel_0; }
	inline float* get_address_of__jaixozeeYeesel_0() { return &____jaixozeeYeesel_0; }
	inline void set__jaixozeeYeesel_0(float value)
	{
		____jaixozeeYeesel_0 = value;
	}

	inline static int32_t get_offset_of__douvou_1() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____douvou_1)); }
	inline uint32_t get__douvou_1() const { return ____douvou_1; }
	inline uint32_t* get_address_of__douvou_1() { return &____douvou_1; }
	inline void set__douvou_1(uint32_t value)
	{
		____douvou_1 = value;
	}

	inline static int32_t get_offset_of__roururcear_2() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____roururcear_2)); }
	inline uint32_t get__roururcear_2() const { return ____roururcear_2; }
	inline uint32_t* get_address_of__roururcear_2() { return &____roururcear_2; }
	inline void set__roururcear_2(uint32_t value)
	{
		____roururcear_2 = value;
	}

	inline static int32_t get_offset_of__dresareHalketoo_3() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____dresareHalketoo_3)); }
	inline int32_t get__dresareHalketoo_3() const { return ____dresareHalketoo_3; }
	inline int32_t* get_address_of__dresareHalketoo_3() { return &____dresareHalketoo_3; }
	inline void set__dresareHalketoo_3(int32_t value)
	{
		____dresareHalketoo_3 = value;
	}

	inline static int32_t get_offset_of__naysis_4() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____naysis_4)); }
	inline String_t* get__naysis_4() const { return ____naysis_4; }
	inline String_t** get_address_of__naysis_4() { return &____naysis_4; }
	inline void set__naysis_4(String_t* value)
	{
		____naysis_4 = value;
		Il2CppCodeGenWriteBarrier(&____naysis_4, value);
	}

	inline static int32_t get_offset_of__dorsipay_5() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____dorsipay_5)); }
	inline int32_t get__dorsipay_5() const { return ____dorsipay_5; }
	inline int32_t* get_address_of__dorsipay_5() { return &____dorsipay_5; }
	inline void set__dorsipay_5(int32_t value)
	{
		____dorsipay_5 = value;
	}

	inline static int32_t get_offset_of__lasrapowNeneji_6() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____lasrapowNeneji_6)); }
	inline float get__lasrapowNeneji_6() const { return ____lasrapowNeneji_6; }
	inline float* get_address_of__lasrapowNeneji_6() { return &____lasrapowNeneji_6; }
	inline void set__lasrapowNeneji_6(float value)
	{
		____lasrapowNeneji_6 = value;
	}

	inline static int32_t get_offset_of__jemallSooyoustall_7() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____jemallSooyoustall_7)); }
	inline bool get__jemallSooyoustall_7() const { return ____jemallSooyoustall_7; }
	inline bool* get_address_of__jemallSooyoustall_7() { return &____jemallSooyoustall_7; }
	inline void set__jemallSooyoustall_7(bool value)
	{
		____jemallSooyoustall_7 = value;
	}

	inline static int32_t get_offset_of__zemseGosassi_8() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____zemseGosassi_8)); }
	inline String_t* get__zemseGosassi_8() const { return ____zemseGosassi_8; }
	inline String_t** get_address_of__zemseGosassi_8() { return &____zemseGosassi_8; }
	inline void set__zemseGosassi_8(String_t* value)
	{
		____zemseGosassi_8 = value;
		Il2CppCodeGenWriteBarrier(&____zemseGosassi_8, value);
	}

	inline static int32_t get_offset_of__corkisstu_9() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____corkisstu_9)); }
	inline bool get__corkisstu_9() const { return ____corkisstu_9; }
	inline bool* get_address_of__corkisstu_9() { return &____corkisstu_9; }
	inline void set__corkisstu_9(bool value)
	{
		____corkisstu_9 = value;
	}

	inline static int32_t get_offset_of__teetawball_10() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____teetawball_10)); }
	inline String_t* get__teetawball_10() const { return ____teetawball_10; }
	inline String_t** get_address_of__teetawball_10() { return &____teetawball_10; }
	inline void set__teetawball_10(String_t* value)
	{
		____teetawball_10 = value;
		Il2CppCodeGenWriteBarrier(&____teetawball_10, value);
	}

	inline static int32_t get_offset_of__fakeToochow_11() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____fakeToochow_11)); }
	inline float get__fakeToochow_11() const { return ____fakeToochow_11; }
	inline float* get_address_of__fakeToochow_11() { return &____fakeToochow_11; }
	inline void set__fakeToochow_11(float value)
	{
		____fakeToochow_11 = value;
	}

	inline static int32_t get_offset_of__yasxihe_12() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____yasxihe_12)); }
	inline bool get__yasxihe_12() const { return ____yasxihe_12; }
	inline bool* get_address_of__yasxihe_12() { return &____yasxihe_12; }
	inline void set__yasxihe_12(bool value)
	{
		____yasxihe_12 = value;
	}

	inline static int32_t get_offset_of__tedranaw_13() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____tedranaw_13)); }
	inline uint32_t get__tedranaw_13() const { return ____tedranaw_13; }
	inline uint32_t* get_address_of__tedranaw_13() { return &____tedranaw_13; }
	inline void set__tedranaw_13(uint32_t value)
	{
		____tedranaw_13 = value;
	}

	inline static int32_t get_offset_of__cairfege_14() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____cairfege_14)); }
	inline int32_t get__cairfege_14() const { return ____cairfege_14; }
	inline int32_t* get_address_of__cairfege_14() { return &____cairfege_14; }
	inline void set__cairfege_14(int32_t value)
	{
		____cairfege_14 = value;
	}

	inline static int32_t get_offset_of__jadrerezo_15() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____jadrerezo_15)); }
	inline String_t* get__jadrerezo_15() const { return ____jadrerezo_15; }
	inline String_t** get_address_of__jadrerezo_15() { return &____jadrerezo_15; }
	inline void set__jadrerezo_15(String_t* value)
	{
		____jadrerezo_15 = value;
		Il2CppCodeGenWriteBarrier(&____jadrerezo_15, value);
	}

	inline static int32_t get_offset_of__nijarchu_16() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____nijarchu_16)); }
	inline float get__nijarchu_16() const { return ____nijarchu_16; }
	inline float* get_address_of__nijarchu_16() { return &____nijarchu_16; }
	inline void set__nijarchu_16(float value)
	{
		____nijarchu_16 = value;
	}

	inline static int32_t get_offset_of__noutoNerememir_17() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____noutoNerememir_17)); }
	inline String_t* get__noutoNerememir_17() const { return ____noutoNerememir_17; }
	inline String_t** get_address_of__noutoNerememir_17() { return &____noutoNerememir_17; }
	inline void set__noutoNerememir_17(String_t* value)
	{
		____noutoNerememir_17 = value;
		Il2CppCodeGenWriteBarrier(&____noutoNerememir_17, value);
	}

	inline static int32_t get_offset_of__nairvaw_18() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____nairvaw_18)); }
	inline uint32_t get__nairvaw_18() const { return ____nairvaw_18; }
	inline uint32_t* get_address_of__nairvaw_18() { return &____nairvaw_18; }
	inline void set__nairvaw_18(uint32_t value)
	{
		____nairvaw_18 = value;
	}

	inline static int32_t get_offset_of__wafe_19() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____wafe_19)); }
	inline String_t* get__wafe_19() const { return ____wafe_19; }
	inline String_t** get_address_of__wafe_19() { return &____wafe_19; }
	inline void set__wafe_19(String_t* value)
	{
		____wafe_19 = value;
		Il2CppCodeGenWriteBarrier(&____wafe_19, value);
	}

	inline static int32_t get_offset_of__potigairTihalldair_20() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____potigairTihalldair_20)); }
	inline uint32_t get__potigairTihalldair_20() const { return ____potigairTihalldair_20; }
	inline uint32_t* get_address_of__potigairTihalldair_20() { return &____potigairTihalldair_20; }
	inline void set__potigairTihalldair_20(uint32_t value)
	{
		____potigairTihalldair_20 = value;
	}

	inline static int32_t get_offset_of__saleepiTairlerexere_21() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____saleepiTairlerexere_21)); }
	inline bool get__saleepiTairlerexere_21() const { return ____saleepiTairlerexere_21; }
	inline bool* get_address_of__saleepiTairlerexere_21() { return &____saleepiTairlerexere_21; }
	inline void set__saleepiTairlerexere_21(bool value)
	{
		____saleepiTairlerexere_21 = value;
	}

	inline static int32_t get_offset_of__sawborpo_22() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____sawborpo_22)); }
	inline String_t* get__sawborpo_22() const { return ____sawborpo_22; }
	inline String_t** get_address_of__sawborpo_22() { return &____sawborpo_22; }
	inline void set__sawborpo_22(String_t* value)
	{
		____sawborpo_22 = value;
		Il2CppCodeGenWriteBarrier(&____sawborpo_22, value);
	}

	inline static int32_t get_offset_of__seducuCeedife_23() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____seducuCeedife_23)); }
	inline String_t* get__seducuCeedife_23() const { return ____seducuCeedife_23; }
	inline String_t** get_address_of__seducuCeedife_23() { return &____seducuCeedife_23; }
	inline void set__seducuCeedife_23(String_t* value)
	{
		____seducuCeedife_23 = value;
		Il2CppCodeGenWriteBarrier(&____seducuCeedife_23, value);
	}

	inline static int32_t get_offset_of__bivuTasxi_24() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____bivuTasxi_24)); }
	inline int32_t get__bivuTasxi_24() const { return ____bivuTasxi_24; }
	inline int32_t* get_address_of__bivuTasxi_24() { return &____bivuTasxi_24; }
	inline void set__bivuTasxi_24(int32_t value)
	{
		____bivuTasxi_24 = value;
	}

	inline static int32_t get_offset_of__lalcairSinawhur_25() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____lalcairSinawhur_25)); }
	inline int32_t get__lalcairSinawhur_25() const { return ____lalcairSinawhur_25; }
	inline int32_t* get_address_of__lalcairSinawhur_25() { return &____lalcairSinawhur_25; }
	inline void set__lalcairSinawhur_25(int32_t value)
	{
		____lalcairSinawhur_25 = value;
	}

	inline static int32_t get_offset_of__carchurhe_26() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____carchurhe_26)); }
	inline bool get__carchurhe_26() const { return ____carchurhe_26; }
	inline bool* get_address_of__carchurhe_26() { return &____carchurhe_26; }
	inline void set__carchurhe_26(bool value)
	{
		____carchurhe_26 = value;
	}

	inline static int32_t get_offset_of__joukememDraircuna_27() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____joukememDraircuna_27)); }
	inline String_t* get__joukememDraircuna_27() const { return ____joukememDraircuna_27; }
	inline String_t** get_address_of__joukememDraircuna_27() { return &____joukememDraircuna_27; }
	inline void set__joukememDraircuna_27(String_t* value)
	{
		____joukememDraircuna_27 = value;
		Il2CppCodeGenWriteBarrier(&____joukememDraircuna_27, value);
	}

	inline static int32_t get_offset_of__stobaspel_28() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____stobaspel_28)); }
	inline bool get__stobaspel_28() const { return ____stobaspel_28; }
	inline bool* get_address_of__stobaspel_28() { return &____stobaspel_28; }
	inline void set__stobaspel_28(bool value)
	{
		____stobaspel_28 = value;
	}

	inline static int32_t get_offset_of__hucesiQesea_29() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____hucesiQesea_29)); }
	inline String_t* get__hucesiQesea_29() const { return ____hucesiQesea_29; }
	inline String_t** get_address_of__hucesiQesea_29() { return &____hucesiQesea_29; }
	inline void set__hucesiQesea_29(String_t* value)
	{
		____hucesiQesea_29 = value;
		Il2CppCodeGenWriteBarrier(&____hucesiQesea_29, value);
	}

	inline static int32_t get_offset_of__ceecas_30() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____ceecas_30)); }
	inline String_t* get__ceecas_30() const { return ____ceecas_30; }
	inline String_t** get_address_of__ceecas_30() { return &____ceecas_30; }
	inline void set__ceecas_30(String_t* value)
	{
		____ceecas_30 = value;
		Il2CppCodeGenWriteBarrier(&____ceecas_30, value);
	}

	inline static int32_t get_offset_of__wirbiLati_31() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____wirbiLati_31)); }
	inline int32_t get__wirbiLati_31() const { return ____wirbiLati_31; }
	inline int32_t* get_address_of__wirbiLati_31() { return &____wirbiLati_31; }
	inline void set__wirbiLati_31(int32_t value)
	{
		____wirbiLati_31 = value;
	}

	inline static int32_t get_offset_of__sugeHornarcur_32() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____sugeHornarcur_32)); }
	inline uint32_t get__sugeHornarcur_32() const { return ____sugeHornarcur_32; }
	inline uint32_t* get_address_of__sugeHornarcur_32() { return &____sugeHornarcur_32; }
	inline void set__sugeHornarcur_32(uint32_t value)
	{
		____sugeHornarcur_32 = value;
	}

	inline static int32_t get_offset_of__choqorFacer_33() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____choqorFacer_33)); }
	inline float get__choqorFacer_33() const { return ____choqorFacer_33; }
	inline float* get_address_of__choqorFacer_33() { return &____choqorFacer_33; }
	inline void set__choqorFacer_33(float value)
	{
		____choqorFacer_33 = value;
	}

	inline static int32_t get_offset_of__xorcaCola_34() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____xorcaCola_34)); }
	inline int32_t get__xorcaCola_34() const { return ____xorcaCola_34; }
	inline int32_t* get_address_of__xorcaCola_34() { return &____xorcaCola_34; }
	inline void set__xorcaCola_34(int32_t value)
	{
		____xorcaCola_34 = value;
	}

	inline static int32_t get_offset_of__moheljorWallteeza_35() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____moheljorWallteeza_35)); }
	inline uint32_t get__moheljorWallteeza_35() const { return ____moheljorWallteeza_35; }
	inline uint32_t* get_address_of__moheljorWallteeza_35() { return &____moheljorWallteeza_35; }
	inline void set__moheljorWallteeza_35(uint32_t value)
	{
		____moheljorWallteeza_35 = value;
	}

	inline static int32_t get_offset_of__waiwaswhairPalkearxe_36() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____waiwaswhairPalkearxe_36)); }
	inline String_t* get__waiwaswhairPalkearxe_36() const { return ____waiwaswhairPalkearxe_36; }
	inline String_t** get_address_of__waiwaswhairPalkearxe_36() { return &____waiwaswhairPalkearxe_36; }
	inline void set__waiwaswhairPalkearxe_36(String_t* value)
	{
		____waiwaswhairPalkearxe_36 = value;
		Il2CppCodeGenWriteBarrier(&____waiwaswhairPalkearxe_36, value);
	}

	inline static int32_t get_offset_of__deaxasirHesatou_37() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____deaxasirHesatou_37)); }
	inline float get__deaxasirHesatou_37() const { return ____deaxasirHesatou_37; }
	inline float* get_address_of__deaxasirHesatou_37() { return &____deaxasirHesatou_37; }
	inline void set__deaxasirHesatou_37(float value)
	{
		____deaxasirHesatou_37 = value;
	}

	inline static int32_t get_offset_of__saruchowFallcaysoo_38() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____saruchowFallcaysoo_38)); }
	inline uint32_t get__saruchowFallcaysoo_38() const { return ____saruchowFallcaysoo_38; }
	inline uint32_t* get_address_of__saruchowFallcaysoo_38() { return &____saruchowFallcaysoo_38; }
	inline void set__saruchowFallcaysoo_38(uint32_t value)
	{
		____saruchowFallcaysoo_38 = value;
	}

	inline static int32_t get_offset_of__muki_39() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____muki_39)); }
	inline bool get__muki_39() const { return ____muki_39; }
	inline bool* get_address_of__muki_39() { return &____muki_39; }
	inline void set__muki_39(bool value)
	{
		____muki_39 = value;
	}

	inline static int32_t get_offset_of__vaiqesall_40() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____vaiqesall_40)); }
	inline String_t* get__vaiqesall_40() const { return ____vaiqesall_40; }
	inline String_t** get_address_of__vaiqesall_40() { return &____vaiqesall_40; }
	inline void set__vaiqesall_40(String_t* value)
	{
		____vaiqesall_40 = value;
		Il2CppCodeGenWriteBarrier(&____vaiqesall_40, value);
	}

	inline static int32_t get_offset_of__selor_41() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____selor_41)); }
	inline bool get__selor_41() const { return ____selor_41; }
	inline bool* get_address_of__selor_41() { return &____selor_41; }
	inline void set__selor_41(bool value)
	{
		____selor_41 = value;
	}

	inline static int32_t get_offset_of__xiqiMizeredra_42() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____xiqiMizeredra_42)); }
	inline int32_t get__xiqiMizeredra_42() const { return ____xiqiMizeredra_42; }
	inline int32_t* get_address_of__xiqiMizeredra_42() { return &____xiqiMizeredra_42; }
	inline void set__xiqiMizeredra_42(int32_t value)
	{
		____xiqiMizeredra_42 = value;
	}

	inline static int32_t get_offset_of__tranene_43() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____tranene_43)); }
	inline uint32_t get__tranene_43() const { return ____tranene_43; }
	inline uint32_t* get_address_of__tranene_43() { return &____tranene_43; }
	inline void set__tranene_43(uint32_t value)
	{
		____tranene_43 = value;
	}

	inline static int32_t get_offset_of__stikayXarzi_44() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____stikayXarzi_44)); }
	inline int32_t get__stikayXarzi_44() const { return ____stikayXarzi_44; }
	inline int32_t* get_address_of__stikayXarzi_44() { return &____stikayXarzi_44; }
	inline void set__stikayXarzi_44(int32_t value)
	{
		____stikayXarzi_44 = value;
	}

	inline static int32_t get_offset_of__whene_45() { return static_cast<int32_t>(offsetof(M_lijalgeTamouhe97_t1543754491, ____whene_45)); }
	inline String_t* get__whene_45() const { return ____whene_45; }
	inline String_t** get_address_of__whene_45() { return &____whene_45; }
	inline void set__whene_45(String_t* value)
	{
		____whene_45 = value;
		Il2CppCodeGenWriteBarrier(&____whene_45, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
