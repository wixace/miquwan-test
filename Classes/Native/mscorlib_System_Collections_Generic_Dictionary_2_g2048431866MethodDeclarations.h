﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>
struct Dictionary_2_t2048431866;
// System.Collections.Generic.IEqualityComparer`1<SoundTypeID>
struct IEqualityComparer_1_t122683848;
// System.Collections.Generic.IDictionary`2<SoundTypeID,System.Object>
struct IDictionary_2_t1626305211;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<SoundTypeID>
struct ICollection_1_t226239431;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<SoundTypeID,System.Object>[]
struct KeyValuePair_2U5BU5D_t71756021;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<SoundTypeID,System.Object>>
struct IEnumerator_1_t3859077621;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<SoundTypeID,System.Object>
struct KeyCollection_t3675191317;
// System.Collections.Generic.Dictionary`2/ValueCollection<SoundTypeID,System.Object>
struct ValueCollection_t749037579;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21947212572.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_SoundTypeID3626616740.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3365755258.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::.ctor()
extern "C"  void Dictionary_2__ctor_m1624792709_gshared (Dictionary_2_t2048431866 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1624792709(__this, method) ((  void (*) (Dictionary_2_t2048431866 *, const MethodInfo*))Dictionary_2__ctor_m1624792709_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3870731388_gshared (Dictionary_2_t2048431866 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m3870731388(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t2048431866 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3870731388_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m4044281907_gshared (Dictionary_2_t2048431866 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m4044281907(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t2048431866 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m4044281907_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m4191908950_gshared (Dictionary_2_t2048431866 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m4191908950(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t2048431866 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m4191908950_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m3287179690_gshared (Dictionary_2_t2048431866 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m3287179690(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t2048431866 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3287179690_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2430479942_gshared (Dictionary_2_t2048431866 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2430479942(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2048431866 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m2430479942_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m308762089_gshared (Dictionary_2_t2048431866 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m308762089(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2048431866 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m308762089_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3644354409_gshared (Dictionary_2_t2048431866 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3644354409(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2048431866 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3644354409_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m988065291_gshared (Dictionary_2_t2048431866 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m988065291(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2048431866 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m988065291_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m2710527033_gshared (Dictionary_2_t2048431866 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m2710527033(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2048431866 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m2710527033_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3991902204_gshared (Dictionary_2_t2048431866 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3991902204(__this, method) ((  bool (*) (Dictionary_2_t2048431866 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3991902204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1552611197_gshared (Dictionary_2_t2048431866 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1552611197(__this, method) ((  bool (*) (Dictionary_2_t2048431866 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1552611197_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m3338954403_gshared (Dictionary_2_t2048431866 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3338954403(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2048431866 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m3338954403_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m4033099976_gshared (Dictionary_2_t2048431866 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m4033099976(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2048431866 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m4033099976_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m2495850633_gshared (Dictionary_2_t2048431866 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m2495850633(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2048431866 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m2495850633_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m1864927949_gshared (Dictionary_2_t2048431866 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m1864927949(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2048431866 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1864927949_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m4008788742_gshared (Dictionary_2_t2048431866 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m4008788742(__this, ___key0, method) ((  void (*) (Dictionary_2_t2048431866 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m4008788742_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3216039847_gshared (Dictionary_2_t2048431866 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3216039847(__this, method) ((  bool (*) (Dictionary_2_t2048431866 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m3216039847_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4049767827_gshared (Dictionary_2_t2048431866 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4049767827(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2048431866 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4049767827_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3465062699_gshared (Dictionary_2_t2048431866 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3465062699(__this, method) ((  bool (*) (Dictionary_2_t2048431866 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3465062699_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1099117724_gshared (Dictionary_2_t2048431866 * __this, KeyValuePair_2_t1947212572  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1099117724(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t2048431866 *, KeyValuePair_2_t1947212572 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1099117724_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m619349926_gshared (Dictionary_2_t2048431866 * __this, KeyValuePair_2_t1947212572  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m619349926(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2048431866 *, KeyValuePair_2_t1947212572 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m619349926_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3280909696_gshared (Dictionary_2_t2048431866 * __this, KeyValuePair_2U5BU5D_t71756021* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3280909696(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2048431866 *, KeyValuePair_2U5BU5D_t71756021*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3280909696_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m211968075_gshared (Dictionary_2_t2048431866 * __this, KeyValuePair_2_t1947212572  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m211968075(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t2048431866 *, KeyValuePair_2_t1947212572 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m211968075_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m857703007_gshared (Dictionary_2_t2048431866 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m857703007(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2048431866 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m857703007_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1607485338_gshared (Dictionary_2_t2048431866 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1607485338(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2048431866 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1607485338_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3484136407_gshared (Dictionary_2_t2048431866 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3484136407(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t2048431866 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m3484136407_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2843661746_gshared (Dictionary_2_t2048431866 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2843661746(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t2048431866 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2843661746_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m3941885037_gshared (Dictionary_2_t2048431866 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m3941885037(__this, method) ((  int32_t (*) (Dictionary_2_t2048431866 *, const MethodInfo*))Dictionary_2_get_Count_m3941885037_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::get_Item(TKey)
extern "C"  Il2CppObject * Dictionary_2_get_Item_m3610347870_gshared (Dictionary_2_t2048431866 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m3610347870(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t2048431866 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m3610347870_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m651052933_gshared (Dictionary_2_t2048431866 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m651052933(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2048431866 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_set_Item_m651052933_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m3080097277_gshared (Dictionary_2_t2048431866 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m3080097277(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t2048431866 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3080097277_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2708731738_gshared (Dictionary_2_t2048431866 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2708731738(__this, ___size0, method) ((  void (*) (Dictionary_2_t2048431866 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2708731738_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m2992500182_gshared (Dictionary_2_t2048431866 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m2992500182(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2048431866 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2992500182_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1947212572  Dictionary_2_make_pair_m1378869282_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m1378869282(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1947212572  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_make_pair_m1378869282_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::pick_key(TKey,TValue)
extern "C"  int32_t Dictionary_2_pick_key_m3713715860_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m3713715860(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_key_m3713715860_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::pick_value(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_value_m3360032980_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m3360032980(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_pick_value_m3360032980_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m333619513_gshared (Dictionary_2_t2048431866 * __this, KeyValuePair_2U5BU5D_t71756021* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m333619513(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t2048431866 *, KeyValuePair_2U5BU5D_t71756021*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m333619513_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::Resize()
extern "C"  void Dictionary_2_Resize_m3181730387_gshared (Dictionary_2_t2048431866 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m3181730387(__this, method) ((  void (*) (Dictionary_2_t2048431866 *, const MethodInfo*))Dictionary_2_Resize_m3181730387_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m1608539088_gshared (Dictionary_2_t2048431866 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m1608539088(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t2048431866 *, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_Add_m1608539088_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::Clear()
extern "C"  void Dictionary_2_Clear_m3325893296_gshared (Dictionary_2_t2048431866 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m3325893296(__this, method) ((  void (*) (Dictionary_2_t2048431866 *, const MethodInfo*))Dictionary_2_Clear_m3325893296_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m1063078678_gshared (Dictionary_2_t2048431866 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1063078678(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2048431866 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m1063078678_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m1599153174_gshared (Dictionary_2_t2048431866 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m1599153174(__this, ___value0, method) ((  bool (*) (Dictionary_2_t2048431866 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsValue_m1599153174_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m700960675_gshared (Dictionary_2_t2048431866 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m700960675(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t2048431866 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m700960675_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m546927265_gshared (Dictionary_2_t2048431866 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m546927265(__this, ___sender0, method) ((  void (*) (Dictionary_2_t2048431866 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m546927265_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m3638155290_gshared (Dictionary_2_t2048431866 * __this, int32_t ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m3638155290(__this, ___key0, method) ((  bool (*) (Dictionary_2_t2048431866 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m3638155290_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m1817193839_gshared (Dictionary_2_t2048431866 * __this, int32_t ___key0, Il2CppObject ** ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m1817193839(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t2048431866 *, int32_t, Il2CppObject **, const MethodInfo*))Dictionary_2_TryGetValue_m1817193839_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::get_Keys()
extern "C"  KeyCollection_t3675191317 * Dictionary_2_get_Keys_m3980602736_gshared (Dictionary_2_t2048431866 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m3980602736(__this, method) ((  KeyCollection_t3675191317 * (*) (Dictionary_2_t2048431866 *, const MethodInfo*))Dictionary_2_get_Keys_m3980602736_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::get_Values()
extern "C"  ValueCollection_t749037579 * Dictionary_2_get_Values_m3108345456_gshared (Dictionary_2_t2048431866 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m3108345456(__this, method) ((  ValueCollection_t749037579 * (*) (Dictionary_2_t2048431866 *, const MethodInfo*))Dictionary_2_get_Values_m3108345456_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::ToTKey(System.Object)
extern "C"  int32_t Dictionary_2_ToTKey_m3163574767_gshared (Dictionary_2_t2048431866 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m3163574767(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t2048431866 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m3163574767_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::ToTValue(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTValue_m1672347119_gshared (Dictionary_2_t2048431866 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1672347119(__this, ___value0, method) ((  Il2CppObject * (*) (Dictionary_2_t2048431866 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1672347119_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m1168979453_gshared (Dictionary_2_t2048431866 * __this, KeyValuePair_2_t1947212572  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1168979453(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t2048431866 *, KeyValuePair_2_t1947212572 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1168979453_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::GetEnumerator()
extern "C"  Enumerator_t3365755258  Dictionary_2_GetEnumerator_m1357580490_gshared (Dictionary_2_t2048431866 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m1357580490(__this, method) ((  Enumerator_t3365755258  (*) (Dictionary_2_t2048431866 *, const MethodInfo*))Dictionary_2_GetEnumerator_m1357580490_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<SoundTypeID,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m3720128409_gshared (Il2CppObject * __this /* static, unused */, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m3720128409(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, Il2CppObject *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m3720128409_gshared)(__this /* static, unused */, ___key0, ___value1, method)
