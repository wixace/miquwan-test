﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_rube195
struct M_rube195_t4048455879;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_rube195::.ctor()
extern "C"  void M_rube195__ctor_m1012051580 (M_rube195_t4048455879 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rube195::M_weltuFemwe0(System.String[],System.Int32)
extern "C"  void M_rube195_M_weltuFemwe0_m1662713426 (M_rube195_t4048455879 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rube195::M_hayra1(System.String[],System.Int32)
extern "C"  void M_rube195_M_hayra1_m439355731 (M_rube195_t4048455879 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rube195::M_gasvi2(System.String[],System.Int32)
extern "C"  void M_rube195_M_gasvi2_m2231103351 (M_rube195_t4048455879 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rube195::M_rowca3(System.String[],System.Int32)
extern "C"  void M_rube195_M_rowca3_m2490416588 (M_rube195_t4048455879 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
