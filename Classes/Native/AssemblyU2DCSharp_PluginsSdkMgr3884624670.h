﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// PluginSjoy
struct PluginSjoy_t1606406260;
// PluginPush
struct PluginPush_t1606327565;
// PluginReYun
struct PluginReYun_t2552860172;
// PluginBugly
struct PluginBugly_t2538573678;
// PluginTlog
struct PluginTlog_t1606437955;
// PluginXunFei
struct PluginXunFei_t2016385516;
// PluginHongYou
struct PluginHongYou_t892448812;
// PluginYiJie
struct PluginYiJie_t2559429187;
// PluginsTaiqi
struct PluginsTaiqi_t2758543124;
// PluginAnqu
struct PluginAnqu_t1605873924;
// PluginXiaomi
struct PluginXiaomi_t2004955630;
// PluginXuegao
struct PluginXuegao_t2016148992;
// PluginYiwan
struct PluginYiwan_t2559472193;
// PluginWanmi
struct PluginWanmi_t2557378541;
// PluginMihua
struct PluginMihua_t2548376133;
// PluginHuawei
struct PluginHuawei_t1557978906;
// PluginVivo
struct PluginVivo_t1606494879;
// PluginOppo
struct PluginOppo_t1606292883;
// PluginYyb
struct PluginYyb_t190373103;
// PluginFlyme
struct PluginFlyme_t2542016952;
// PluginJinLi
struct PluginJinLi_t2545610073;
// PluginUC
struct PluginUC_t2499992865;
// Plugin360
struct Plugin360_t190334458;
// PluginQuick
struct PluginQuick_t2552428122;
// PluginZxhy
struct PluginZxhy_t1606628034;
// PluginOasis
struct PluginOasis_t2549995064;
// PluginAiWan
struct PluginAiWan_t2537276937;
// PluginJingang
struct PluginJingang_t2495688911;
// PluginZhiquyou
struct PluginZhiquyou_t1151299027;
// PluginYouMa
struct PluginYouMa_t2559648384;
// PluginIqiyi
struct PluginIqiyi_t2544921470;
// PluginDiYibo
struct PluginDiYibo_t1432128181;
// PluginYiwanSuper
struct PluginYiwanSuper_t2354859450;
// PluginHongyouNY
struct PluginHongyouNY_t2974371607;
// PluginInFox
struct PluginInFox_t2544798167;
// PluginR2
struct PluginR2_t2499992755;
// PluginYuewan
struct PluginYuewan_t2044793518;
// PluginSamsung
struct PluginSamsung_t1663707431;
// PluginFengqi
struct PluginFengqi_t1486316547;
// PluginZhangxun
struct PluginZhangxun_t919077400;
// PluginYouDong
struct PluginYouDong_t3100552560;
// PluginYouwo
struct PluginYouwo_t2559649700;
// PluginFuncheer
struct PluginFuncheer_t3732253601;
// PluginYueplay
struct PluginYueplay_t3258858666;
// PluginNewMH
struct PluginNewMH_t2549193640;
// PluginNewMiquwan
struct PluginNewMiquwan_t2470753303;
// PluginPaoJiao
struct PluginPaoJiao_t3296777368;
// PluginXima
struct PluginXima_t1606554168;
// PluginXiaoQi
struct PluginXiaoQi_t2004954762;
// PluginGuChuan
struct PluginGuChuan_t137064892;
// PluginZongYou
struct PluginZongYou_t3982613182;
// PluginHuoShu
struct PluginHuoShu_t1558361489;
// PluginTT
struct PluginTT_t2499992851;
// PluginGameFan
struct PluginGameFan_t3898102510;
// PluginGuoPan
struct PluginGuoPan_t1529729231;
// PluginCGame
struct PluginCGame_t2538121058;
// PluginAiLeXinYou
struct PluginAiLeXinYou_t3790076438;
// PluginJuFengJQ
struct PluginJuFengJQ_t970421149;
// PluginKuaiYou
struct PluginKuaiYou_t3714788570;
// PluginGuangdiantong
struct PluginGuangdiantong_t1625076095;
// PluginJinritoutiao
struct PluginJinritoutiao_t1969577066;
// PluginYouLong
struct PluginYouLong_t3100790888;
// PluginMiquwan
struct PluginMiquwan_t866441041;
// PluginGamecat
struct PluginGamecat_t3898130385;
// PluginJingqi
struct PluginJingqi_t1604527235;
// PluginYiyou
struct PluginYiyou_t2559474556;
// PluginChangku
struct PluginChangku_t537709740;
// PluginJuliang
struct PluginJuliang_t2837451263;
// PluginDianZhi
struct PluginDianZhi_t1453829974;
// PluginKuaiWan
struct PluginKuaiWan_t3714786207;
// PluginMoHe
struct PluginMoHe_t1606231090;
// PluginZhangYu
struct PluginZhangYu_t3770424661;
// PluginSanXiang
struct PluginSanXiang_t38647228;
// PluginShoumeng
struct PluginShoumeng_t2011739967;
// PluginCaoHua
struct PluginCaoHua_t1396735126;
// PluginXiaoNiu
struct PluginXiaoNiu_t2024052712;
// PluginYuLe
struct PluginYuLe_t1606594472;
// PluginXiongmaowan
struct PluginXiongmaowan_t3347898093;
// PluginQianYou
struct PluginQianYou_t106475207;
// PluginOMi
struct PluginOMi_t190362136;
// PluginAnfeng
struct PluginAnfeng_t1351242136;
// PluginQiZi
struct PluginQiZi_t1606345050;
// PluginShoumeng_A1
struct PluginShoumeng_A1_t4066769168;
// PluginTianYing
struct PluginTianYing_t4234189246;
// PluginFastPlay
struct PluginFastPlay_t3320186403;
// PluginYouxiang
struct PluginYouxiang_t1675681021;
// PluginYinHu
struct PluginYinHu_t2559462776;
// PluginQiangWan
struct PluginQiangWan_t3301124917;
// PluginKuaiWanSJ
struct PluginKuaiWanSJ_t791724598;
// PluginDolphin
struct PluginDolphin_t1635836683;
// PluginWanYu
struct PluginWanYu_t2557377933;
// PluginQuWan
struct PluginQuWan_t2552410765;
// PluginXiaoMai
struct PluginXiaoMai_t2024051491;
// PluginAiLe
struct PluginAiLe_t1605867956;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Action
struct Action_t3771233898;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t2362096582;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginsSdkMgr
struct  PluginsSdkMgr_t3884624670  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginsSdkMgr::HELP_APPKEY
	String_t* ___HELP_APPKEY_26;
	// Newtonsoft.Json.Linq.JObject PluginsSdkMgr::sdkObj
	JObject_t1798544199 * ___sdkObj_27;
	// PluginSjoy PluginsSdkMgr::sjoyPlugin
	PluginSjoy_t1606406260 * ___sjoyPlugin_28;
	// PluginPush PluginsSdkMgr::pushPlugin
	PluginPush_t1606327565 * ___pushPlugin_29;
	// PluginReYun PluginsSdkMgr::reyunPlugin
	PluginReYun_t2552860172 * ___reyunPlugin_30;
	// PluginBugly PluginsSdkMgr::buglyPlugin
	PluginBugly_t2538573678 * ___buglyPlugin_31;
	// PluginTlog PluginsSdkMgr::tlogPlugin
	PluginTlog_t1606437955 * ___tlogPlugin_32;
	// PluginXunFei PluginsSdkMgr::xunfeiPlugin
	PluginXunFei_t2016385516 * ___xunfeiPlugin_33;
	// PluginHongYou PluginsSdkMgr::hongyouPlugin
	PluginHongYou_t892448812 * ___hongyouPlugin_34;
	// PluginYiJie PluginsSdkMgr::yijiePlugin
	PluginYiJie_t2559429187 * ___yijiePlugin_35;
	// PluginsTaiqi PluginsSdkMgr::taiqiPlugin
	PluginsTaiqi_t2758543124 * ___taiqiPlugin_36;
	// PluginAnqu PluginsSdkMgr::anquPlugin
	PluginAnqu_t1605873924 * ___anquPlugin_37;
	// PluginXiaomi PluginsSdkMgr::xiaomiPlugin
	PluginXiaomi_t2004955630 * ___xiaomiPlugin_38;
	// PluginXuegao PluginsSdkMgr::xuegaoPlugin
	PluginXuegao_t2016148992 * ___xuegaoPlugin_39;
	// PluginYiwan PluginsSdkMgr::yiwanPlugin
	PluginYiwan_t2559472193 * ___yiwanPlugin_40;
	// PluginWanmi PluginsSdkMgr::wanmiPlugin
	PluginWanmi_t2557378541 * ___wanmiPlugin_41;
	// PluginMihua PluginsSdkMgr::mihuaPlugin
	PluginMihua_t2548376133 * ___mihuaPlugin_42;
	// PluginHuawei PluginsSdkMgr::huaweiPlugin
	PluginHuawei_t1557978906 * ___huaweiPlugin_43;
	// PluginVivo PluginsSdkMgr::vivoPlugin
	PluginVivo_t1606494879 * ___vivoPlugin_44;
	// PluginOppo PluginsSdkMgr::oppoPlugin
	PluginOppo_t1606292883 * ___oppoPlugin_45;
	// PluginYyb PluginsSdkMgr::yybPlugin
	PluginYyb_t190373103 * ___yybPlugin_46;
	// PluginFlyme PluginsSdkMgr::flymePlugin
	PluginFlyme_t2542016952 * ___flymePlugin_47;
	// PluginJinLi PluginsSdkMgr::jinliPlugin
	PluginJinLi_t2545610073 * ___jinliPlugin_48;
	// PluginUC PluginsSdkMgr::ucPlugin
	PluginUC_t2499992865 * ___ucPlugin_49;
	// Plugin360 PluginsSdkMgr::zq360Plugin
	Plugin360_t190334458 * ___zq360Plugin_50;
	// PluginQuick PluginsSdkMgr::quickPlugin
	PluginQuick_t2552428122 * ___quickPlugin_51;
	// PluginZxhy PluginsSdkMgr::zxhyPlugin
	PluginZxhy_t1606628034 * ___zxhyPlugin_52;
	// PluginOasis PluginsSdkMgr::oasisPlugin
	PluginOasis_t2549995064 * ___oasisPlugin_53;
	// PluginAiWan PluginsSdkMgr::aiwanPlugin
	PluginAiWan_t2537276937 * ___aiwanPlugin_54;
	// PluginJingang PluginsSdkMgr::jingangPlugin
	PluginJingang_t2495688911 * ___jingangPlugin_55;
	// PluginZhiquyou PluginsSdkMgr::zhiquyouPlugin
	PluginZhiquyou_t1151299027 * ___zhiquyouPlugin_56;
	// PluginYouMa PluginsSdkMgr::youmaPlugin
	PluginYouMa_t2559648384 * ___youmaPlugin_57;
	// PluginIqiyi PluginsSdkMgr::iqiyiPlugin
	PluginIqiyi_t2544921470 * ___iqiyiPlugin_58;
	// PluginDiYibo PluginsSdkMgr::diyiboPlugin
	PluginDiYibo_t1432128181 * ___diyiboPlugin_59;
	// PluginYiwanSuper PluginsSdkMgr::yiwansuperPlugin
	PluginYiwanSuper_t2354859450 * ___yiwansuperPlugin_60;
	// PluginHongyouNY PluginsSdkMgr::hongyouNYPlugin
	PluginHongyouNY_t2974371607 * ___hongyouNYPlugin_61;
	// PluginInFox PluginsSdkMgr::infoxPlugin
	PluginInFox_t2544798167 * ___infoxPlugin_62;
	// PluginR2 PluginsSdkMgr::r2Plugin
	PluginR2_t2499992755 * ___r2Plugin_63;
	// PluginYuewan PluginsSdkMgr::yuewanPlugin
	PluginYuewan_t2044793518 * ___yuewanPlugin_64;
	// PluginSamsung PluginsSdkMgr::samsungPlugin
	PluginSamsung_t1663707431 * ___samsungPlugin_65;
	// PluginFengqi PluginsSdkMgr::fengqiPlugin
	PluginFengqi_t1486316547 * ___fengqiPlugin_66;
	// PluginZhangxun PluginsSdkMgr::zhangxunPlugin
	PluginZhangxun_t919077400 * ___zhangxunPlugin_67;
	// PluginYouDong PluginsSdkMgr::youdongPlugin
	PluginYouDong_t3100552560 * ___youdongPlugin_68;
	// PluginYouwo PluginsSdkMgr::youwoPlugin
	PluginYouwo_t2559649700 * ___youwoPlugin_69;
	// PluginFuncheer PluginsSdkMgr::funcheerPlugin
	PluginFuncheer_t3732253601 * ___funcheerPlugin_70;
	// PluginYueplay PluginsSdkMgr::yueplayPlugin
	PluginYueplay_t3258858666 * ___yueplayPlugin_71;
	// PluginNewMH PluginsSdkMgr::newmhPlugin
	PluginNewMH_t2549193640 * ___newmhPlugin_72;
	// PluginNewMiquwan PluginsSdkMgr::newmiquwanPlugin
	PluginNewMiquwan_t2470753303 * ___newmiquwanPlugin_73;
	// PluginPaoJiao PluginsSdkMgr::paojiaoPlugin
	PluginPaoJiao_t3296777368 * ___paojiaoPlugin_74;
	// PluginXima PluginsSdkMgr::ximaPlugin
	PluginXima_t1606554168 * ___ximaPlugin_75;
	// PluginXiaoQi PluginsSdkMgr::xiaoqiPlugin
	PluginXiaoQi_t2004954762 * ___xiaoqiPlugin_76;
	// PluginGuChuan PluginsSdkMgr::guchuanPlugin
	PluginGuChuan_t137064892 * ___guchuanPlugin_77;
	// PluginZongYou PluginsSdkMgr::zongyouPlugin
	PluginZongYou_t3982613182 * ___zongyouPlugin_78;
	// PluginHuoShu PluginsSdkMgr::huoshuPlugin
	PluginHuoShu_t1558361489 * ___huoshuPlugin_79;
	// PluginTT PluginsSdkMgr::TTPlugin
	PluginTT_t2499992851 * ___TTPlugin_80;
	// PluginGameFan PluginsSdkMgr::gamefanPlugin
	PluginGameFan_t3898102510 * ___gamefanPlugin_81;
	// PluginGuoPan PluginsSdkMgr::guopanPlugin
	PluginGuoPan_t1529729231 * ___guopanPlugin_82;
	// PluginCGame PluginsSdkMgr::cgamePlugin
	PluginCGame_t2538121058 * ___cgamePlugin_83;
	// PluginAiLeXinYou PluginsSdkMgr::ailexinyouPlugin
	PluginAiLeXinYou_t3790076438 * ___ailexinyouPlugin_84;
	// PluginJuFengJQ PluginsSdkMgr::jufengJQPlugin
	PluginJuFengJQ_t970421149 * ___jufengJQPlugin_85;
	// PluginKuaiYou PluginsSdkMgr::kuaiyouPlugin
	PluginKuaiYou_t3714788570 * ___kuaiyouPlugin_86;
	// PluginGuangdiantong PluginsSdkMgr::guangdiantongPlugin
	PluginGuangdiantong_t1625076095 * ___guangdiantongPlugin_87;
	// PluginJinritoutiao PluginsSdkMgr::jinritoutiaoPlugin
	PluginJinritoutiao_t1969577066 * ___jinritoutiaoPlugin_88;
	// PluginYouLong PluginsSdkMgr::youlongPlugin
	PluginYouLong_t3100790888 * ___youlongPlugin_89;
	// PluginMiquwan PluginsSdkMgr::miquwanPlugin
	PluginMiquwan_t866441041 * ___miquwanPlugin_90;
	// PluginGamecat PluginsSdkMgr::gamecatPlugin
	PluginGamecat_t3898130385 * ___gamecatPlugin_91;
	// PluginJingqi PluginsSdkMgr::jingqiPlugin
	PluginJingqi_t1604527235 * ___jingqiPlugin_92;
	// PluginYiyou PluginsSdkMgr::yiyouPlugin
	PluginYiyou_t2559474556 * ___yiyouPlugin_93;
	// PluginChangku PluginsSdkMgr::changkuPlugin
	PluginChangku_t537709740 * ___changkuPlugin_94;
	// PluginJuliang PluginsSdkMgr::juliangPlugin
	PluginJuliang_t2837451263 * ___juliangPlugin_95;
	// PluginDianZhi PluginsSdkMgr::dianzhiPlugin
	PluginDianZhi_t1453829974 * ___dianzhiPlugin_96;
	// PluginKuaiWan PluginsSdkMgr::kuaiWanPlugin
	PluginKuaiWan_t3714786207 * ___kuaiWanPlugin_97;
	// PluginMoHe PluginsSdkMgr::moHePlugin
	PluginMoHe_t1606231090 * ___moHePlugin_98;
	// PluginZhangYu PluginsSdkMgr::zhangyuPlugin
	PluginZhangYu_t3770424661 * ___zhangyuPlugin_99;
	// PluginSanXiang PluginsSdkMgr::sanXiangPlugin
	PluginSanXiang_t38647228 * ___sanXiangPlugin_100;
	// PluginShoumeng PluginsSdkMgr::shoumengPlugin
	PluginShoumeng_t2011739967 * ___shoumengPlugin_101;
	// PluginCaoHua PluginsSdkMgr::caohuaPlugin
	PluginCaoHua_t1396735126 * ___caohuaPlugin_102;
	// PluginXiaoNiu PluginsSdkMgr::xiaoNiuPlugin
	PluginXiaoNiu_t2024052712 * ___xiaoNiuPlugin_103;
	// PluginYuLe PluginsSdkMgr::yulePlugin
	PluginYuLe_t1606594472 * ___yulePlugin_104;
	// PluginXiongmaowan PluginsSdkMgr::xiongmaowanPlugin
	PluginXiongmaowan_t3347898093 * ___xiongmaowanPlugin_105;
	// PluginQianYou PluginsSdkMgr::qianyouPlugin
	PluginQianYou_t106475207 * ___qianyouPlugin_106;
	// PluginOMi PluginsSdkMgr::oMiPlugin
	PluginOMi_t190362136 * ___oMiPlugin_107;
	// PluginAnfeng PluginsSdkMgr::anfengPlugin
	PluginAnfeng_t1351242136 * ___anfengPlugin_108;
	// PluginQiZi PluginsSdkMgr::qiziPlugin
	PluginQiZi_t1606345050 * ___qiziPlugin_109;
	// PluginShoumeng_A1 PluginsSdkMgr::shoumengA1Plugin
	PluginShoumeng_A1_t4066769168 * ___shoumengA1Plugin_110;
	// PluginTianYing PluginsSdkMgr::tianyingPlugin
	PluginTianYing_t4234189246 * ___tianyingPlugin_111;
	// PluginFastPlay PluginsSdkMgr::fastPlayPlugin
	PluginFastPlay_t3320186403 * ___fastPlayPlugin_112;
	// PluginYouxiang PluginsSdkMgr::youxiangPlugin
	PluginYouxiang_t1675681021 * ___youxiangPlugin_113;
	// PluginYinHu PluginsSdkMgr::yinHuPlugin
	PluginYinHu_t2559462776 * ___yinHuPlugin_114;
	// PluginQiangWan PluginsSdkMgr::qiangwanPlugin
	PluginQiangWan_t3301124917 * ___qiangwanPlugin_115;
	// PluginKuaiWanSJ PluginsSdkMgr::kuaiwansjPlugin
	PluginKuaiWanSJ_t791724598 * ___kuaiwansjPlugin_116;
	// PluginDolphin PluginsSdkMgr::dolphinPlugin
	PluginDolphin_t1635836683 * ___dolphinPlugin_117;
	// PluginWanYu PluginsSdkMgr::wanyuPlugin
	PluginWanYu_t2557377933 * ___wanyuPlugin_118;
	// PluginQuWan PluginsSdkMgr::quwanPlugin
	PluginQuWan_t2552410765 * ___quwanPlugin_119;
	// PluginXiaoMai PluginsSdkMgr::xiaomaiPlugin
	PluginXiaoMai_t2024051491 * ___xiaomaiPlugin_120;
	// PluginAiLe PluginsSdkMgr::ailePlugin
	PluginAiLe_t1605867956 * ___ailePlugin_121;
	// System.Boolean PluginsSdkMgr::IsOpenSjoy
	bool ___IsOpenSjoy_122;
	// System.Boolean PluginsSdkMgr::IsOpenPush
	bool ___IsOpenPush_123;
	// System.Boolean PluginsSdkMgr::IsOpenReYun
	bool ___IsOpenReYun_124;
	// System.Boolean PluginsSdkMgr::IsOpenBugly
	bool ___IsOpenBugly_125;
	// System.Boolean PluginsSdkMgr::IsOpenTool
	bool ___IsOpenTool_126;
	// System.Boolean PluginsSdkMgr::IsTlog
	bool ___IsTlog_127;
	// System.Boolean PluginsSdkMgr::IsOpenXunFei
	bool ___IsOpenXunFei_128;
	// System.Boolean PluginsSdkMgr::IsOpenHongYou
	bool ___IsOpenHongYou_129;
	// System.Boolean PluginsSdkMgr::IsOpenYiJie
	bool ___IsOpenYiJie_130;
	// System.Boolean PluginsSdkMgr::IsOpenTaiqi
	bool ___IsOpenTaiqi_131;
	// System.Boolean PluginsSdkMgr::IsOpenAnqu
	bool ___IsOpenAnqu_132;
	// System.Boolean PluginsSdkMgr::IsOpenXiaomi
	bool ___IsOpenXiaomi_133;
	// System.Boolean PluginsSdkMgr::IsOpenXuegao
	bool ___IsOpenXuegao_134;
	// System.Boolean PluginsSdkMgr::IsOpenYiwan
	bool ___IsOpenYiwan_135;
	// System.Boolean PluginsSdkMgr::IsOpenWanmi
	bool ___IsOpenWanmi_136;
	// System.Boolean PluginsSdkMgr::IsOpenHuawei
	bool ___IsOpenHuawei_137;
	// System.Boolean PluginsSdkMgr::IsOpenVivo
	bool ___IsOpenVivo_138;
	// System.Boolean PluginsSdkMgr::IsOpenOppo
	bool ___IsOpenOppo_139;
	// System.Boolean PluginsSdkMgr::IsOpenUC
	bool ___IsOpenUC_140;
	// System.Boolean PluginsSdkMgr::IsOpen360
	bool ___IsOpen360_141;
	// System.Boolean PluginsSdkMgr::IsOpenMihua
	bool ___IsOpenMihua_142;
	// System.Boolean PluginsSdkMgr::IsOpenYyb
	bool ___IsOpenYyb_143;
	// System.Boolean PluginsSdkMgr::IsOpenFlyme
	bool ___IsOpenFlyme_144;
	// System.Boolean PluginsSdkMgr::IsOpenJinLi
	bool ___IsOpenJinLi_145;
	// System.Boolean PluginsSdkMgr::IsOpenQuick
	bool ___IsOpenQuick_146;
	// System.Boolean PluginsSdkMgr::IsOpenZxhy
	bool ___IsOpenZxhy_147;
	// System.Boolean PluginsSdkMgr::IsOpenOasis
	bool ___IsOpenOasis_148;
	// System.Boolean PluginsSdkMgr::IsOpenOasisIos
	bool ___IsOpenOasisIos_149;
	// System.Boolean PluginsSdkMgr::IsOpenAiwan
	bool ___IsOpenAiwan_150;
	// System.Boolean PluginsSdkMgr::IsOpenJingang
	bool ___IsOpenJingang_151;
	// System.Boolean PluginsSdkMgr::IsOpenZhiquyou
	bool ___IsOpenZhiquyou_152;
	// System.Boolean PluginsSdkMgr::IsOpenYouMa
	bool ___IsOpenYouMa_153;
	// System.Boolean PluginsSdkMgr::IsOpenIqiyi
	bool ___IsOpenIqiyi_154;
	// System.Boolean PluginsSdkMgr::IsOpenDiyibo
	bool ___IsOpenDiyibo_155;
	// System.Boolean PluginsSdkMgr::IsOpenYiwansuper
	bool ___IsOpenYiwansuper_156;
	// System.Boolean PluginsSdkMgr::IsOpenHongyouNY
	bool ___IsOpenHongyouNY_157;
	// System.Boolean PluginsSdkMgr::IsOpenInFox
	bool ___IsOpenInFox_158;
	// System.Boolean PluginsSdkMgr::IsOpenR2
	bool ___IsOpenR2_159;
	// System.Boolean PluginsSdkMgr::IsOpenYuewan
	bool ___IsOpenYuewan_160;
	// System.Boolean PluginsSdkMgr::IsOpenSamsung
	bool ___IsOpenSamsung_161;
	// System.Boolean PluginsSdkMgr::IsOpenFengqi
	bool ___IsOpenFengqi_162;
	// System.Boolean PluginsSdkMgr::IsOpenZhangXun
	bool ___IsOpenZhangXun_163;
	// System.Boolean PluginsSdkMgr::IsOpenYouDong
	bool ___IsOpenYouDong_164;
	// System.Boolean PluginsSdkMgr::IsOpenYouWo
	bool ___IsOpenYouWo_165;
	// System.Boolean PluginsSdkMgr::IsOpenFuncheer
	bool ___IsOpenFuncheer_166;
	// System.Boolean PluginsSdkMgr::IsOpenYuePlay
	bool ___IsOpenYuePlay_167;
	// System.Boolean PluginsSdkMgr::IsOpenMiquwan
	bool ___IsOpenMiquwan_168;
	// System.Boolean PluginsSdkMgr::IsOpenGamecat
	bool ___IsOpenGamecat_169;
	// System.Boolean PluginsSdkMgr::IsOpenJingqi
	bool ___IsOpenJingqi_170;
	// System.Boolean PluginsSdkMgr::IsOpenYiyou
	bool ___IsOpenYiyou_171;
	// System.Boolean PluginsSdkMgr::IsOpenChangku
	bool ___IsOpenChangku_172;
	// System.Boolean PluginsSdkMgr::IsOpenJuliang
	bool ___IsOpenJuliang_173;
	// System.Boolean PluginsSdkMgr::IsOpenDianZhi
	bool ___IsOpenDianZhi_174;
	// System.Boolean PluginsSdkMgr::IsOpenKuaiWan
	bool ___IsOpenKuaiWan_175;
	// System.Boolean PluginsSdkMgr::IsOpenMoHe
	bool ___IsOpenMoHe_176;
	// System.Boolean PluginsSdkMgr::IsOpenZhangYu
	bool ___IsOpenZhangYu_177;
	// System.Boolean PluginsSdkMgr::IsOpenSanXiang
	bool ___IsOpenSanXiang_178;
	// System.Boolean PluginsSdkMgr::IsOpenShoumeng
	bool ___IsOpenShoumeng_179;
	// System.Boolean PluginsSdkMgr::IsOpenCaoHua
	bool ___IsOpenCaoHua_180;
	// System.Boolean PluginsSdkMgr::IsOpenYuLe
	bool ___IsOpenYuLe_181;
	// System.Boolean PluginsSdkMgr::IsOpenXiongmaowan
	bool ___IsOpenXiongmaowan_182;
	// System.Boolean PluginsSdkMgr::IsOpenQianyou
	bool ___IsOpenQianyou_183;
	// System.Boolean PluginsSdkMgr::IsOpenOMi
	bool ___IsOpenOMi_184;
	// System.Boolean PluginsSdkMgr::IsOpenAnfeng
	bool ___IsOpenAnfeng_185;
	// System.Boolean PluginsSdkMgr::IsOpenQizi
	bool ___IsOpenQizi_186;
	// System.Boolean PluginsSdkMgr::IsOpenShoumengA1
	bool ___IsOpenShoumengA1_187;
	// System.Boolean PluginsSdkMgr::IsOpenTianying
	bool ___IsOpenTianying_188;
	// System.Boolean PluginsSdkMgr::IsOpenFastPlay
	bool ___IsOpenFastPlay_189;
	// System.Boolean PluginsSdkMgr::IsOpenYouxiang
	bool ___IsOpenYouxiang_190;
	// System.Boolean PluginsSdkMgr::IsOpenXiaoNiu
	bool ___IsOpenXiaoNiu_191;
	// System.Boolean PluginsSdkMgr::IsOpenYinHu
	bool ___IsOpenYinHu_192;
	// System.Boolean PluginsSdkMgr::IsOpenQiangWan
	bool ___IsOpenQiangWan_193;
	// System.Boolean PluginsSdkMgr::IsOpenNewMH
	bool ___IsOpenNewMH_194;
	// System.Boolean PluginsSdkMgr::IsOpenKuaiWanSJ
	bool ___IsOpenKuaiWanSJ_195;
	// System.Boolean PluginsSdkMgr::IsOpenNewMiquwan
	bool ___IsOpenNewMiquwan_196;
	// System.Boolean PluginsSdkMgr::IsOpenPaoJiao
	bool ___IsOpenPaoJiao_197;
	// System.Boolean PluginsSdkMgr::IsOpenXiMa
	bool ___IsOpenXiMa_198;
	// System.Boolean PluginsSdkMgr::IsOpenXiaoQi
	bool ___IsOpenXiaoQi_199;
	// System.Boolean PluginsSdkMgr::IsOpenGuChuan
	bool ___IsOpenGuChuan_200;
	// System.Boolean PluginsSdkMgr::IsOpenDolphin
	bool ___IsOpenDolphin_201;
	// System.Boolean PluginsSdkMgr::IsOpenWanYu
	bool ___IsOpenWanYu_202;
	// System.Boolean PluginsSdkMgr::IsOpenQuWan
	bool ___IsOpenQuWan_203;
	// System.Boolean PluginsSdkMgr::IsOpenXiaomai
	bool ___IsOpenXiaomai_204;
	// System.Boolean PluginsSdkMgr::IsOpenZongYou
	bool ___IsOpenZongYou_205;
	// System.Boolean PluginsSdkMgr::IsOpenAiLe
	bool ___IsOpenAiLe_206;
	// System.Boolean PluginsSdkMgr::IsOpenHuoshu
	bool ___IsOpenHuoshu_207;
	// System.Boolean PluginsSdkMgr::IsOpenTT
	bool ___IsOpenTT_208;
	// System.Boolean PluginsSdkMgr::IsOpenGamefan
	bool ___IsOpenGamefan_209;
	// System.Boolean PluginsSdkMgr::IsOpenGuopan
	bool ___IsOpenGuopan_210;
	// System.Boolean PluginsSdkMgr::IsOpenCgame
	bool ___IsOpenCgame_211;
	// System.Boolean PluginsSdkMgr::IsOpenAiLeXinYou
	bool ___IsOpenAiLeXinYou_212;
	// System.Boolean PluginsSdkMgr::IsOpenJuFengJQ
	bool ___IsOpenJuFengJQ_213;
	// System.Boolean PluginsSdkMgr::IsOpenKuaiYou
	bool ___IsOpenKuaiYou_214;
	// System.Boolean PluginsSdkMgr::IsOpenGuangDianTong
	bool ___IsOpenGuangDianTong_215;
	// System.Boolean PluginsSdkMgr::IsOpenJinRiTouTiao
	bool ___IsOpenJinRiTouTiao_216;
	// System.Boolean PluginsSdkMgr::IsOpenYouLong
	bool ___IsOpenYouLong_217;
	// System.Boolean PluginsSdkMgr::isHideOldSever
	bool ___isHideOldSever_218;
	// System.Boolean PluginsSdkMgr::IsLoginClick
	bool ___IsLoginClick_219;
	// System.String PluginsSdkMgr::username
	String_t* ___username_221;
	// System.Action PluginsSdkMgr::OnOK
	Action_t3771233898 * ___OnOK_222;
	// System.Action PluginsSdkMgr::OnCancel
	Action_t3771233898 * ___OnCancel_223;
	// UnityEngine.AndroidJavaObject PluginsSdkMgr::<androidJavaObject>k__BackingField
	AndroidJavaObject_t2362096582 * ___U3CandroidJavaObjectU3Ek__BackingField_224;

public:
	inline static int32_t get_offset_of_HELP_APPKEY_26() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___HELP_APPKEY_26)); }
	inline String_t* get_HELP_APPKEY_26() const { return ___HELP_APPKEY_26; }
	inline String_t** get_address_of_HELP_APPKEY_26() { return &___HELP_APPKEY_26; }
	inline void set_HELP_APPKEY_26(String_t* value)
	{
		___HELP_APPKEY_26 = value;
		Il2CppCodeGenWriteBarrier(&___HELP_APPKEY_26, value);
	}

	inline static int32_t get_offset_of_sdkObj_27() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___sdkObj_27)); }
	inline JObject_t1798544199 * get_sdkObj_27() const { return ___sdkObj_27; }
	inline JObject_t1798544199 ** get_address_of_sdkObj_27() { return &___sdkObj_27; }
	inline void set_sdkObj_27(JObject_t1798544199 * value)
	{
		___sdkObj_27 = value;
		Il2CppCodeGenWriteBarrier(&___sdkObj_27, value);
	}

	inline static int32_t get_offset_of_sjoyPlugin_28() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___sjoyPlugin_28)); }
	inline PluginSjoy_t1606406260 * get_sjoyPlugin_28() const { return ___sjoyPlugin_28; }
	inline PluginSjoy_t1606406260 ** get_address_of_sjoyPlugin_28() { return &___sjoyPlugin_28; }
	inline void set_sjoyPlugin_28(PluginSjoy_t1606406260 * value)
	{
		___sjoyPlugin_28 = value;
		Il2CppCodeGenWriteBarrier(&___sjoyPlugin_28, value);
	}

	inline static int32_t get_offset_of_pushPlugin_29() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___pushPlugin_29)); }
	inline PluginPush_t1606327565 * get_pushPlugin_29() const { return ___pushPlugin_29; }
	inline PluginPush_t1606327565 ** get_address_of_pushPlugin_29() { return &___pushPlugin_29; }
	inline void set_pushPlugin_29(PluginPush_t1606327565 * value)
	{
		___pushPlugin_29 = value;
		Il2CppCodeGenWriteBarrier(&___pushPlugin_29, value);
	}

	inline static int32_t get_offset_of_reyunPlugin_30() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___reyunPlugin_30)); }
	inline PluginReYun_t2552860172 * get_reyunPlugin_30() const { return ___reyunPlugin_30; }
	inline PluginReYun_t2552860172 ** get_address_of_reyunPlugin_30() { return &___reyunPlugin_30; }
	inline void set_reyunPlugin_30(PluginReYun_t2552860172 * value)
	{
		___reyunPlugin_30 = value;
		Il2CppCodeGenWriteBarrier(&___reyunPlugin_30, value);
	}

	inline static int32_t get_offset_of_buglyPlugin_31() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___buglyPlugin_31)); }
	inline PluginBugly_t2538573678 * get_buglyPlugin_31() const { return ___buglyPlugin_31; }
	inline PluginBugly_t2538573678 ** get_address_of_buglyPlugin_31() { return &___buglyPlugin_31; }
	inline void set_buglyPlugin_31(PluginBugly_t2538573678 * value)
	{
		___buglyPlugin_31 = value;
		Il2CppCodeGenWriteBarrier(&___buglyPlugin_31, value);
	}

	inline static int32_t get_offset_of_tlogPlugin_32() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___tlogPlugin_32)); }
	inline PluginTlog_t1606437955 * get_tlogPlugin_32() const { return ___tlogPlugin_32; }
	inline PluginTlog_t1606437955 ** get_address_of_tlogPlugin_32() { return &___tlogPlugin_32; }
	inline void set_tlogPlugin_32(PluginTlog_t1606437955 * value)
	{
		___tlogPlugin_32 = value;
		Il2CppCodeGenWriteBarrier(&___tlogPlugin_32, value);
	}

	inline static int32_t get_offset_of_xunfeiPlugin_33() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___xunfeiPlugin_33)); }
	inline PluginXunFei_t2016385516 * get_xunfeiPlugin_33() const { return ___xunfeiPlugin_33; }
	inline PluginXunFei_t2016385516 ** get_address_of_xunfeiPlugin_33() { return &___xunfeiPlugin_33; }
	inline void set_xunfeiPlugin_33(PluginXunFei_t2016385516 * value)
	{
		___xunfeiPlugin_33 = value;
		Il2CppCodeGenWriteBarrier(&___xunfeiPlugin_33, value);
	}

	inline static int32_t get_offset_of_hongyouPlugin_34() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___hongyouPlugin_34)); }
	inline PluginHongYou_t892448812 * get_hongyouPlugin_34() const { return ___hongyouPlugin_34; }
	inline PluginHongYou_t892448812 ** get_address_of_hongyouPlugin_34() { return &___hongyouPlugin_34; }
	inline void set_hongyouPlugin_34(PluginHongYou_t892448812 * value)
	{
		___hongyouPlugin_34 = value;
		Il2CppCodeGenWriteBarrier(&___hongyouPlugin_34, value);
	}

	inline static int32_t get_offset_of_yijiePlugin_35() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___yijiePlugin_35)); }
	inline PluginYiJie_t2559429187 * get_yijiePlugin_35() const { return ___yijiePlugin_35; }
	inline PluginYiJie_t2559429187 ** get_address_of_yijiePlugin_35() { return &___yijiePlugin_35; }
	inline void set_yijiePlugin_35(PluginYiJie_t2559429187 * value)
	{
		___yijiePlugin_35 = value;
		Il2CppCodeGenWriteBarrier(&___yijiePlugin_35, value);
	}

	inline static int32_t get_offset_of_taiqiPlugin_36() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___taiqiPlugin_36)); }
	inline PluginsTaiqi_t2758543124 * get_taiqiPlugin_36() const { return ___taiqiPlugin_36; }
	inline PluginsTaiqi_t2758543124 ** get_address_of_taiqiPlugin_36() { return &___taiqiPlugin_36; }
	inline void set_taiqiPlugin_36(PluginsTaiqi_t2758543124 * value)
	{
		___taiqiPlugin_36 = value;
		Il2CppCodeGenWriteBarrier(&___taiqiPlugin_36, value);
	}

	inline static int32_t get_offset_of_anquPlugin_37() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___anquPlugin_37)); }
	inline PluginAnqu_t1605873924 * get_anquPlugin_37() const { return ___anquPlugin_37; }
	inline PluginAnqu_t1605873924 ** get_address_of_anquPlugin_37() { return &___anquPlugin_37; }
	inline void set_anquPlugin_37(PluginAnqu_t1605873924 * value)
	{
		___anquPlugin_37 = value;
		Il2CppCodeGenWriteBarrier(&___anquPlugin_37, value);
	}

	inline static int32_t get_offset_of_xiaomiPlugin_38() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___xiaomiPlugin_38)); }
	inline PluginXiaomi_t2004955630 * get_xiaomiPlugin_38() const { return ___xiaomiPlugin_38; }
	inline PluginXiaomi_t2004955630 ** get_address_of_xiaomiPlugin_38() { return &___xiaomiPlugin_38; }
	inline void set_xiaomiPlugin_38(PluginXiaomi_t2004955630 * value)
	{
		___xiaomiPlugin_38 = value;
		Il2CppCodeGenWriteBarrier(&___xiaomiPlugin_38, value);
	}

	inline static int32_t get_offset_of_xuegaoPlugin_39() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___xuegaoPlugin_39)); }
	inline PluginXuegao_t2016148992 * get_xuegaoPlugin_39() const { return ___xuegaoPlugin_39; }
	inline PluginXuegao_t2016148992 ** get_address_of_xuegaoPlugin_39() { return &___xuegaoPlugin_39; }
	inline void set_xuegaoPlugin_39(PluginXuegao_t2016148992 * value)
	{
		___xuegaoPlugin_39 = value;
		Il2CppCodeGenWriteBarrier(&___xuegaoPlugin_39, value);
	}

	inline static int32_t get_offset_of_yiwanPlugin_40() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___yiwanPlugin_40)); }
	inline PluginYiwan_t2559472193 * get_yiwanPlugin_40() const { return ___yiwanPlugin_40; }
	inline PluginYiwan_t2559472193 ** get_address_of_yiwanPlugin_40() { return &___yiwanPlugin_40; }
	inline void set_yiwanPlugin_40(PluginYiwan_t2559472193 * value)
	{
		___yiwanPlugin_40 = value;
		Il2CppCodeGenWriteBarrier(&___yiwanPlugin_40, value);
	}

	inline static int32_t get_offset_of_wanmiPlugin_41() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___wanmiPlugin_41)); }
	inline PluginWanmi_t2557378541 * get_wanmiPlugin_41() const { return ___wanmiPlugin_41; }
	inline PluginWanmi_t2557378541 ** get_address_of_wanmiPlugin_41() { return &___wanmiPlugin_41; }
	inline void set_wanmiPlugin_41(PluginWanmi_t2557378541 * value)
	{
		___wanmiPlugin_41 = value;
		Il2CppCodeGenWriteBarrier(&___wanmiPlugin_41, value);
	}

	inline static int32_t get_offset_of_mihuaPlugin_42() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___mihuaPlugin_42)); }
	inline PluginMihua_t2548376133 * get_mihuaPlugin_42() const { return ___mihuaPlugin_42; }
	inline PluginMihua_t2548376133 ** get_address_of_mihuaPlugin_42() { return &___mihuaPlugin_42; }
	inline void set_mihuaPlugin_42(PluginMihua_t2548376133 * value)
	{
		___mihuaPlugin_42 = value;
		Il2CppCodeGenWriteBarrier(&___mihuaPlugin_42, value);
	}

	inline static int32_t get_offset_of_huaweiPlugin_43() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___huaweiPlugin_43)); }
	inline PluginHuawei_t1557978906 * get_huaweiPlugin_43() const { return ___huaweiPlugin_43; }
	inline PluginHuawei_t1557978906 ** get_address_of_huaweiPlugin_43() { return &___huaweiPlugin_43; }
	inline void set_huaweiPlugin_43(PluginHuawei_t1557978906 * value)
	{
		___huaweiPlugin_43 = value;
		Il2CppCodeGenWriteBarrier(&___huaweiPlugin_43, value);
	}

	inline static int32_t get_offset_of_vivoPlugin_44() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___vivoPlugin_44)); }
	inline PluginVivo_t1606494879 * get_vivoPlugin_44() const { return ___vivoPlugin_44; }
	inline PluginVivo_t1606494879 ** get_address_of_vivoPlugin_44() { return &___vivoPlugin_44; }
	inline void set_vivoPlugin_44(PluginVivo_t1606494879 * value)
	{
		___vivoPlugin_44 = value;
		Il2CppCodeGenWriteBarrier(&___vivoPlugin_44, value);
	}

	inline static int32_t get_offset_of_oppoPlugin_45() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___oppoPlugin_45)); }
	inline PluginOppo_t1606292883 * get_oppoPlugin_45() const { return ___oppoPlugin_45; }
	inline PluginOppo_t1606292883 ** get_address_of_oppoPlugin_45() { return &___oppoPlugin_45; }
	inline void set_oppoPlugin_45(PluginOppo_t1606292883 * value)
	{
		___oppoPlugin_45 = value;
		Il2CppCodeGenWriteBarrier(&___oppoPlugin_45, value);
	}

	inline static int32_t get_offset_of_yybPlugin_46() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___yybPlugin_46)); }
	inline PluginYyb_t190373103 * get_yybPlugin_46() const { return ___yybPlugin_46; }
	inline PluginYyb_t190373103 ** get_address_of_yybPlugin_46() { return &___yybPlugin_46; }
	inline void set_yybPlugin_46(PluginYyb_t190373103 * value)
	{
		___yybPlugin_46 = value;
		Il2CppCodeGenWriteBarrier(&___yybPlugin_46, value);
	}

	inline static int32_t get_offset_of_flymePlugin_47() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___flymePlugin_47)); }
	inline PluginFlyme_t2542016952 * get_flymePlugin_47() const { return ___flymePlugin_47; }
	inline PluginFlyme_t2542016952 ** get_address_of_flymePlugin_47() { return &___flymePlugin_47; }
	inline void set_flymePlugin_47(PluginFlyme_t2542016952 * value)
	{
		___flymePlugin_47 = value;
		Il2CppCodeGenWriteBarrier(&___flymePlugin_47, value);
	}

	inline static int32_t get_offset_of_jinliPlugin_48() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___jinliPlugin_48)); }
	inline PluginJinLi_t2545610073 * get_jinliPlugin_48() const { return ___jinliPlugin_48; }
	inline PluginJinLi_t2545610073 ** get_address_of_jinliPlugin_48() { return &___jinliPlugin_48; }
	inline void set_jinliPlugin_48(PluginJinLi_t2545610073 * value)
	{
		___jinliPlugin_48 = value;
		Il2CppCodeGenWriteBarrier(&___jinliPlugin_48, value);
	}

	inline static int32_t get_offset_of_ucPlugin_49() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___ucPlugin_49)); }
	inline PluginUC_t2499992865 * get_ucPlugin_49() const { return ___ucPlugin_49; }
	inline PluginUC_t2499992865 ** get_address_of_ucPlugin_49() { return &___ucPlugin_49; }
	inline void set_ucPlugin_49(PluginUC_t2499992865 * value)
	{
		___ucPlugin_49 = value;
		Il2CppCodeGenWriteBarrier(&___ucPlugin_49, value);
	}

	inline static int32_t get_offset_of_zq360Plugin_50() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___zq360Plugin_50)); }
	inline Plugin360_t190334458 * get_zq360Plugin_50() const { return ___zq360Plugin_50; }
	inline Plugin360_t190334458 ** get_address_of_zq360Plugin_50() { return &___zq360Plugin_50; }
	inline void set_zq360Plugin_50(Plugin360_t190334458 * value)
	{
		___zq360Plugin_50 = value;
		Il2CppCodeGenWriteBarrier(&___zq360Plugin_50, value);
	}

	inline static int32_t get_offset_of_quickPlugin_51() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___quickPlugin_51)); }
	inline PluginQuick_t2552428122 * get_quickPlugin_51() const { return ___quickPlugin_51; }
	inline PluginQuick_t2552428122 ** get_address_of_quickPlugin_51() { return &___quickPlugin_51; }
	inline void set_quickPlugin_51(PluginQuick_t2552428122 * value)
	{
		___quickPlugin_51 = value;
		Il2CppCodeGenWriteBarrier(&___quickPlugin_51, value);
	}

	inline static int32_t get_offset_of_zxhyPlugin_52() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___zxhyPlugin_52)); }
	inline PluginZxhy_t1606628034 * get_zxhyPlugin_52() const { return ___zxhyPlugin_52; }
	inline PluginZxhy_t1606628034 ** get_address_of_zxhyPlugin_52() { return &___zxhyPlugin_52; }
	inline void set_zxhyPlugin_52(PluginZxhy_t1606628034 * value)
	{
		___zxhyPlugin_52 = value;
		Il2CppCodeGenWriteBarrier(&___zxhyPlugin_52, value);
	}

	inline static int32_t get_offset_of_oasisPlugin_53() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___oasisPlugin_53)); }
	inline PluginOasis_t2549995064 * get_oasisPlugin_53() const { return ___oasisPlugin_53; }
	inline PluginOasis_t2549995064 ** get_address_of_oasisPlugin_53() { return &___oasisPlugin_53; }
	inline void set_oasisPlugin_53(PluginOasis_t2549995064 * value)
	{
		___oasisPlugin_53 = value;
		Il2CppCodeGenWriteBarrier(&___oasisPlugin_53, value);
	}

	inline static int32_t get_offset_of_aiwanPlugin_54() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___aiwanPlugin_54)); }
	inline PluginAiWan_t2537276937 * get_aiwanPlugin_54() const { return ___aiwanPlugin_54; }
	inline PluginAiWan_t2537276937 ** get_address_of_aiwanPlugin_54() { return &___aiwanPlugin_54; }
	inline void set_aiwanPlugin_54(PluginAiWan_t2537276937 * value)
	{
		___aiwanPlugin_54 = value;
		Il2CppCodeGenWriteBarrier(&___aiwanPlugin_54, value);
	}

	inline static int32_t get_offset_of_jingangPlugin_55() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___jingangPlugin_55)); }
	inline PluginJingang_t2495688911 * get_jingangPlugin_55() const { return ___jingangPlugin_55; }
	inline PluginJingang_t2495688911 ** get_address_of_jingangPlugin_55() { return &___jingangPlugin_55; }
	inline void set_jingangPlugin_55(PluginJingang_t2495688911 * value)
	{
		___jingangPlugin_55 = value;
		Il2CppCodeGenWriteBarrier(&___jingangPlugin_55, value);
	}

	inline static int32_t get_offset_of_zhiquyouPlugin_56() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___zhiquyouPlugin_56)); }
	inline PluginZhiquyou_t1151299027 * get_zhiquyouPlugin_56() const { return ___zhiquyouPlugin_56; }
	inline PluginZhiquyou_t1151299027 ** get_address_of_zhiquyouPlugin_56() { return &___zhiquyouPlugin_56; }
	inline void set_zhiquyouPlugin_56(PluginZhiquyou_t1151299027 * value)
	{
		___zhiquyouPlugin_56 = value;
		Il2CppCodeGenWriteBarrier(&___zhiquyouPlugin_56, value);
	}

	inline static int32_t get_offset_of_youmaPlugin_57() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___youmaPlugin_57)); }
	inline PluginYouMa_t2559648384 * get_youmaPlugin_57() const { return ___youmaPlugin_57; }
	inline PluginYouMa_t2559648384 ** get_address_of_youmaPlugin_57() { return &___youmaPlugin_57; }
	inline void set_youmaPlugin_57(PluginYouMa_t2559648384 * value)
	{
		___youmaPlugin_57 = value;
		Il2CppCodeGenWriteBarrier(&___youmaPlugin_57, value);
	}

	inline static int32_t get_offset_of_iqiyiPlugin_58() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___iqiyiPlugin_58)); }
	inline PluginIqiyi_t2544921470 * get_iqiyiPlugin_58() const { return ___iqiyiPlugin_58; }
	inline PluginIqiyi_t2544921470 ** get_address_of_iqiyiPlugin_58() { return &___iqiyiPlugin_58; }
	inline void set_iqiyiPlugin_58(PluginIqiyi_t2544921470 * value)
	{
		___iqiyiPlugin_58 = value;
		Il2CppCodeGenWriteBarrier(&___iqiyiPlugin_58, value);
	}

	inline static int32_t get_offset_of_diyiboPlugin_59() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___diyiboPlugin_59)); }
	inline PluginDiYibo_t1432128181 * get_diyiboPlugin_59() const { return ___diyiboPlugin_59; }
	inline PluginDiYibo_t1432128181 ** get_address_of_diyiboPlugin_59() { return &___diyiboPlugin_59; }
	inline void set_diyiboPlugin_59(PluginDiYibo_t1432128181 * value)
	{
		___diyiboPlugin_59 = value;
		Il2CppCodeGenWriteBarrier(&___diyiboPlugin_59, value);
	}

	inline static int32_t get_offset_of_yiwansuperPlugin_60() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___yiwansuperPlugin_60)); }
	inline PluginYiwanSuper_t2354859450 * get_yiwansuperPlugin_60() const { return ___yiwansuperPlugin_60; }
	inline PluginYiwanSuper_t2354859450 ** get_address_of_yiwansuperPlugin_60() { return &___yiwansuperPlugin_60; }
	inline void set_yiwansuperPlugin_60(PluginYiwanSuper_t2354859450 * value)
	{
		___yiwansuperPlugin_60 = value;
		Il2CppCodeGenWriteBarrier(&___yiwansuperPlugin_60, value);
	}

	inline static int32_t get_offset_of_hongyouNYPlugin_61() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___hongyouNYPlugin_61)); }
	inline PluginHongyouNY_t2974371607 * get_hongyouNYPlugin_61() const { return ___hongyouNYPlugin_61; }
	inline PluginHongyouNY_t2974371607 ** get_address_of_hongyouNYPlugin_61() { return &___hongyouNYPlugin_61; }
	inline void set_hongyouNYPlugin_61(PluginHongyouNY_t2974371607 * value)
	{
		___hongyouNYPlugin_61 = value;
		Il2CppCodeGenWriteBarrier(&___hongyouNYPlugin_61, value);
	}

	inline static int32_t get_offset_of_infoxPlugin_62() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___infoxPlugin_62)); }
	inline PluginInFox_t2544798167 * get_infoxPlugin_62() const { return ___infoxPlugin_62; }
	inline PluginInFox_t2544798167 ** get_address_of_infoxPlugin_62() { return &___infoxPlugin_62; }
	inline void set_infoxPlugin_62(PluginInFox_t2544798167 * value)
	{
		___infoxPlugin_62 = value;
		Il2CppCodeGenWriteBarrier(&___infoxPlugin_62, value);
	}

	inline static int32_t get_offset_of_r2Plugin_63() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___r2Plugin_63)); }
	inline PluginR2_t2499992755 * get_r2Plugin_63() const { return ___r2Plugin_63; }
	inline PluginR2_t2499992755 ** get_address_of_r2Plugin_63() { return &___r2Plugin_63; }
	inline void set_r2Plugin_63(PluginR2_t2499992755 * value)
	{
		___r2Plugin_63 = value;
		Il2CppCodeGenWriteBarrier(&___r2Plugin_63, value);
	}

	inline static int32_t get_offset_of_yuewanPlugin_64() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___yuewanPlugin_64)); }
	inline PluginYuewan_t2044793518 * get_yuewanPlugin_64() const { return ___yuewanPlugin_64; }
	inline PluginYuewan_t2044793518 ** get_address_of_yuewanPlugin_64() { return &___yuewanPlugin_64; }
	inline void set_yuewanPlugin_64(PluginYuewan_t2044793518 * value)
	{
		___yuewanPlugin_64 = value;
		Il2CppCodeGenWriteBarrier(&___yuewanPlugin_64, value);
	}

	inline static int32_t get_offset_of_samsungPlugin_65() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___samsungPlugin_65)); }
	inline PluginSamsung_t1663707431 * get_samsungPlugin_65() const { return ___samsungPlugin_65; }
	inline PluginSamsung_t1663707431 ** get_address_of_samsungPlugin_65() { return &___samsungPlugin_65; }
	inline void set_samsungPlugin_65(PluginSamsung_t1663707431 * value)
	{
		___samsungPlugin_65 = value;
		Il2CppCodeGenWriteBarrier(&___samsungPlugin_65, value);
	}

	inline static int32_t get_offset_of_fengqiPlugin_66() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___fengqiPlugin_66)); }
	inline PluginFengqi_t1486316547 * get_fengqiPlugin_66() const { return ___fengqiPlugin_66; }
	inline PluginFengqi_t1486316547 ** get_address_of_fengqiPlugin_66() { return &___fengqiPlugin_66; }
	inline void set_fengqiPlugin_66(PluginFengqi_t1486316547 * value)
	{
		___fengqiPlugin_66 = value;
		Il2CppCodeGenWriteBarrier(&___fengqiPlugin_66, value);
	}

	inline static int32_t get_offset_of_zhangxunPlugin_67() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___zhangxunPlugin_67)); }
	inline PluginZhangxun_t919077400 * get_zhangxunPlugin_67() const { return ___zhangxunPlugin_67; }
	inline PluginZhangxun_t919077400 ** get_address_of_zhangxunPlugin_67() { return &___zhangxunPlugin_67; }
	inline void set_zhangxunPlugin_67(PluginZhangxun_t919077400 * value)
	{
		___zhangxunPlugin_67 = value;
		Il2CppCodeGenWriteBarrier(&___zhangxunPlugin_67, value);
	}

	inline static int32_t get_offset_of_youdongPlugin_68() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___youdongPlugin_68)); }
	inline PluginYouDong_t3100552560 * get_youdongPlugin_68() const { return ___youdongPlugin_68; }
	inline PluginYouDong_t3100552560 ** get_address_of_youdongPlugin_68() { return &___youdongPlugin_68; }
	inline void set_youdongPlugin_68(PluginYouDong_t3100552560 * value)
	{
		___youdongPlugin_68 = value;
		Il2CppCodeGenWriteBarrier(&___youdongPlugin_68, value);
	}

	inline static int32_t get_offset_of_youwoPlugin_69() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___youwoPlugin_69)); }
	inline PluginYouwo_t2559649700 * get_youwoPlugin_69() const { return ___youwoPlugin_69; }
	inline PluginYouwo_t2559649700 ** get_address_of_youwoPlugin_69() { return &___youwoPlugin_69; }
	inline void set_youwoPlugin_69(PluginYouwo_t2559649700 * value)
	{
		___youwoPlugin_69 = value;
		Il2CppCodeGenWriteBarrier(&___youwoPlugin_69, value);
	}

	inline static int32_t get_offset_of_funcheerPlugin_70() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___funcheerPlugin_70)); }
	inline PluginFuncheer_t3732253601 * get_funcheerPlugin_70() const { return ___funcheerPlugin_70; }
	inline PluginFuncheer_t3732253601 ** get_address_of_funcheerPlugin_70() { return &___funcheerPlugin_70; }
	inline void set_funcheerPlugin_70(PluginFuncheer_t3732253601 * value)
	{
		___funcheerPlugin_70 = value;
		Il2CppCodeGenWriteBarrier(&___funcheerPlugin_70, value);
	}

	inline static int32_t get_offset_of_yueplayPlugin_71() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___yueplayPlugin_71)); }
	inline PluginYueplay_t3258858666 * get_yueplayPlugin_71() const { return ___yueplayPlugin_71; }
	inline PluginYueplay_t3258858666 ** get_address_of_yueplayPlugin_71() { return &___yueplayPlugin_71; }
	inline void set_yueplayPlugin_71(PluginYueplay_t3258858666 * value)
	{
		___yueplayPlugin_71 = value;
		Il2CppCodeGenWriteBarrier(&___yueplayPlugin_71, value);
	}

	inline static int32_t get_offset_of_newmhPlugin_72() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___newmhPlugin_72)); }
	inline PluginNewMH_t2549193640 * get_newmhPlugin_72() const { return ___newmhPlugin_72; }
	inline PluginNewMH_t2549193640 ** get_address_of_newmhPlugin_72() { return &___newmhPlugin_72; }
	inline void set_newmhPlugin_72(PluginNewMH_t2549193640 * value)
	{
		___newmhPlugin_72 = value;
		Il2CppCodeGenWriteBarrier(&___newmhPlugin_72, value);
	}

	inline static int32_t get_offset_of_newmiquwanPlugin_73() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___newmiquwanPlugin_73)); }
	inline PluginNewMiquwan_t2470753303 * get_newmiquwanPlugin_73() const { return ___newmiquwanPlugin_73; }
	inline PluginNewMiquwan_t2470753303 ** get_address_of_newmiquwanPlugin_73() { return &___newmiquwanPlugin_73; }
	inline void set_newmiquwanPlugin_73(PluginNewMiquwan_t2470753303 * value)
	{
		___newmiquwanPlugin_73 = value;
		Il2CppCodeGenWriteBarrier(&___newmiquwanPlugin_73, value);
	}

	inline static int32_t get_offset_of_paojiaoPlugin_74() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___paojiaoPlugin_74)); }
	inline PluginPaoJiao_t3296777368 * get_paojiaoPlugin_74() const { return ___paojiaoPlugin_74; }
	inline PluginPaoJiao_t3296777368 ** get_address_of_paojiaoPlugin_74() { return &___paojiaoPlugin_74; }
	inline void set_paojiaoPlugin_74(PluginPaoJiao_t3296777368 * value)
	{
		___paojiaoPlugin_74 = value;
		Il2CppCodeGenWriteBarrier(&___paojiaoPlugin_74, value);
	}

	inline static int32_t get_offset_of_ximaPlugin_75() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___ximaPlugin_75)); }
	inline PluginXima_t1606554168 * get_ximaPlugin_75() const { return ___ximaPlugin_75; }
	inline PluginXima_t1606554168 ** get_address_of_ximaPlugin_75() { return &___ximaPlugin_75; }
	inline void set_ximaPlugin_75(PluginXima_t1606554168 * value)
	{
		___ximaPlugin_75 = value;
		Il2CppCodeGenWriteBarrier(&___ximaPlugin_75, value);
	}

	inline static int32_t get_offset_of_xiaoqiPlugin_76() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___xiaoqiPlugin_76)); }
	inline PluginXiaoQi_t2004954762 * get_xiaoqiPlugin_76() const { return ___xiaoqiPlugin_76; }
	inline PluginXiaoQi_t2004954762 ** get_address_of_xiaoqiPlugin_76() { return &___xiaoqiPlugin_76; }
	inline void set_xiaoqiPlugin_76(PluginXiaoQi_t2004954762 * value)
	{
		___xiaoqiPlugin_76 = value;
		Il2CppCodeGenWriteBarrier(&___xiaoqiPlugin_76, value);
	}

	inline static int32_t get_offset_of_guchuanPlugin_77() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___guchuanPlugin_77)); }
	inline PluginGuChuan_t137064892 * get_guchuanPlugin_77() const { return ___guchuanPlugin_77; }
	inline PluginGuChuan_t137064892 ** get_address_of_guchuanPlugin_77() { return &___guchuanPlugin_77; }
	inline void set_guchuanPlugin_77(PluginGuChuan_t137064892 * value)
	{
		___guchuanPlugin_77 = value;
		Il2CppCodeGenWriteBarrier(&___guchuanPlugin_77, value);
	}

	inline static int32_t get_offset_of_zongyouPlugin_78() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___zongyouPlugin_78)); }
	inline PluginZongYou_t3982613182 * get_zongyouPlugin_78() const { return ___zongyouPlugin_78; }
	inline PluginZongYou_t3982613182 ** get_address_of_zongyouPlugin_78() { return &___zongyouPlugin_78; }
	inline void set_zongyouPlugin_78(PluginZongYou_t3982613182 * value)
	{
		___zongyouPlugin_78 = value;
		Il2CppCodeGenWriteBarrier(&___zongyouPlugin_78, value);
	}

	inline static int32_t get_offset_of_huoshuPlugin_79() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___huoshuPlugin_79)); }
	inline PluginHuoShu_t1558361489 * get_huoshuPlugin_79() const { return ___huoshuPlugin_79; }
	inline PluginHuoShu_t1558361489 ** get_address_of_huoshuPlugin_79() { return &___huoshuPlugin_79; }
	inline void set_huoshuPlugin_79(PluginHuoShu_t1558361489 * value)
	{
		___huoshuPlugin_79 = value;
		Il2CppCodeGenWriteBarrier(&___huoshuPlugin_79, value);
	}

	inline static int32_t get_offset_of_TTPlugin_80() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___TTPlugin_80)); }
	inline PluginTT_t2499992851 * get_TTPlugin_80() const { return ___TTPlugin_80; }
	inline PluginTT_t2499992851 ** get_address_of_TTPlugin_80() { return &___TTPlugin_80; }
	inline void set_TTPlugin_80(PluginTT_t2499992851 * value)
	{
		___TTPlugin_80 = value;
		Il2CppCodeGenWriteBarrier(&___TTPlugin_80, value);
	}

	inline static int32_t get_offset_of_gamefanPlugin_81() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___gamefanPlugin_81)); }
	inline PluginGameFan_t3898102510 * get_gamefanPlugin_81() const { return ___gamefanPlugin_81; }
	inline PluginGameFan_t3898102510 ** get_address_of_gamefanPlugin_81() { return &___gamefanPlugin_81; }
	inline void set_gamefanPlugin_81(PluginGameFan_t3898102510 * value)
	{
		___gamefanPlugin_81 = value;
		Il2CppCodeGenWriteBarrier(&___gamefanPlugin_81, value);
	}

	inline static int32_t get_offset_of_guopanPlugin_82() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___guopanPlugin_82)); }
	inline PluginGuoPan_t1529729231 * get_guopanPlugin_82() const { return ___guopanPlugin_82; }
	inline PluginGuoPan_t1529729231 ** get_address_of_guopanPlugin_82() { return &___guopanPlugin_82; }
	inline void set_guopanPlugin_82(PluginGuoPan_t1529729231 * value)
	{
		___guopanPlugin_82 = value;
		Il2CppCodeGenWriteBarrier(&___guopanPlugin_82, value);
	}

	inline static int32_t get_offset_of_cgamePlugin_83() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___cgamePlugin_83)); }
	inline PluginCGame_t2538121058 * get_cgamePlugin_83() const { return ___cgamePlugin_83; }
	inline PluginCGame_t2538121058 ** get_address_of_cgamePlugin_83() { return &___cgamePlugin_83; }
	inline void set_cgamePlugin_83(PluginCGame_t2538121058 * value)
	{
		___cgamePlugin_83 = value;
		Il2CppCodeGenWriteBarrier(&___cgamePlugin_83, value);
	}

	inline static int32_t get_offset_of_ailexinyouPlugin_84() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___ailexinyouPlugin_84)); }
	inline PluginAiLeXinYou_t3790076438 * get_ailexinyouPlugin_84() const { return ___ailexinyouPlugin_84; }
	inline PluginAiLeXinYou_t3790076438 ** get_address_of_ailexinyouPlugin_84() { return &___ailexinyouPlugin_84; }
	inline void set_ailexinyouPlugin_84(PluginAiLeXinYou_t3790076438 * value)
	{
		___ailexinyouPlugin_84 = value;
		Il2CppCodeGenWriteBarrier(&___ailexinyouPlugin_84, value);
	}

	inline static int32_t get_offset_of_jufengJQPlugin_85() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___jufengJQPlugin_85)); }
	inline PluginJuFengJQ_t970421149 * get_jufengJQPlugin_85() const { return ___jufengJQPlugin_85; }
	inline PluginJuFengJQ_t970421149 ** get_address_of_jufengJQPlugin_85() { return &___jufengJQPlugin_85; }
	inline void set_jufengJQPlugin_85(PluginJuFengJQ_t970421149 * value)
	{
		___jufengJQPlugin_85 = value;
		Il2CppCodeGenWriteBarrier(&___jufengJQPlugin_85, value);
	}

	inline static int32_t get_offset_of_kuaiyouPlugin_86() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___kuaiyouPlugin_86)); }
	inline PluginKuaiYou_t3714788570 * get_kuaiyouPlugin_86() const { return ___kuaiyouPlugin_86; }
	inline PluginKuaiYou_t3714788570 ** get_address_of_kuaiyouPlugin_86() { return &___kuaiyouPlugin_86; }
	inline void set_kuaiyouPlugin_86(PluginKuaiYou_t3714788570 * value)
	{
		___kuaiyouPlugin_86 = value;
		Il2CppCodeGenWriteBarrier(&___kuaiyouPlugin_86, value);
	}

	inline static int32_t get_offset_of_guangdiantongPlugin_87() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___guangdiantongPlugin_87)); }
	inline PluginGuangdiantong_t1625076095 * get_guangdiantongPlugin_87() const { return ___guangdiantongPlugin_87; }
	inline PluginGuangdiantong_t1625076095 ** get_address_of_guangdiantongPlugin_87() { return &___guangdiantongPlugin_87; }
	inline void set_guangdiantongPlugin_87(PluginGuangdiantong_t1625076095 * value)
	{
		___guangdiantongPlugin_87 = value;
		Il2CppCodeGenWriteBarrier(&___guangdiantongPlugin_87, value);
	}

	inline static int32_t get_offset_of_jinritoutiaoPlugin_88() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___jinritoutiaoPlugin_88)); }
	inline PluginJinritoutiao_t1969577066 * get_jinritoutiaoPlugin_88() const { return ___jinritoutiaoPlugin_88; }
	inline PluginJinritoutiao_t1969577066 ** get_address_of_jinritoutiaoPlugin_88() { return &___jinritoutiaoPlugin_88; }
	inline void set_jinritoutiaoPlugin_88(PluginJinritoutiao_t1969577066 * value)
	{
		___jinritoutiaoPlugin_88 = value;
		Il2CppCodeGenWriteBarrier(&___jinritoutiaoPlugin_88, value);
	}

	inline static int32_t get_offset_of_youlongPlugin_89() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___youlongPlugin_89)); }
	inline PluginYouLong_t3100790888 * get_youlongPlugin_89() const { return ___youlongPlugin_89; }
	inline PluginYouLong_t3100790888 ** get_address_of_youlongPlugin_89() { return &___youlongPlugin_89; }
	inline void set_youlongPlugin_89(PluginYouLong_t3100790888 * value)
	{
		___youlongPlugin_89 = value;
		Il2CppCodeGenWriteBarrier(&___youlongPlugin_89, value);
	}

	inline static int32_t get_offset_of_miquwanPlugin_90() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___miquwanPlugin_90)); }
	inline PluginMiquwan_t866441041 * get_miquwanPlugin_90() const { return ___miquwanPlugin_90; }
	inline PluginMiquwan_t866441041 ** get_address_of_miquwanPlugin_90() { return &___miquwanPlugin_90; }
	inline void set_miquwanPlugin_90(PluginMiquwan_t866441041 * value)
	{
		___miquwanPlugin_90 = value;
		Il2CppCodeGenWriteBarrier(&___miquwanPlugin_90, value);
	}

	inline static int32_t get_offset_of_gamecatPlugin_91() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___gamecatPlugin_91)); }
	inline PluginGamecat_t3898130385 * get_gamecatPlugin_91() const { return ___gamecatPlugin_91; }
	inline PluginGamecat_t3898130385 ** get_address_of_gamecatPlugin_91() { return &___gamecatPlugin_91; }
	inline void set_gamecatPlugin_91(PluginGamecat_t3898130385 * value)
	{
		___gamecatPlugin_91 = value;
		Il2CppCodeGenWriteBarrier(&___gamecatPlugin_91, value);
	}

	inline static int32_t get_offset_of_jingqiPlugin_92() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___jingqiPlugin_92)); }
	inline PluginJingqi_t1604527235 * get_jingqiPlugin_92() const { return ___jingqiPlugin_92; }
	inline PluginJingqi_t1604527235 ** get_address_of_jingqiPlugin_92() { return &___jingqiPlugin_92; }
	inline void set_jingqiPlugin_92(PluginJingqi_t1604527235 * value)
	{
		___jingqiPlugin_92 = value;
		Il2CppCodeGenWriteBarrier(&___jingqiPlugin_92, value);
	}

	inline static int32_t get_offset_of_yiyouPlugin_93() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___yiyouPlugin_93)); }
	inline PluginYiyou_t2559474556 * get_yiyouPlugin_93() const { return ___yiyouPlugin_93; }
	inline PluginYiyou_t2559474556 ** get_address_of_yiyouPlugin_93() { return &___yiyouPlugin_93; }
	inline void set_yiyouPlugin_93(PluginYiyou_t2559474556 * value)
	{
		___yiyouPlugin_93 = value;
		Il2CppCodeGenWriteBarrier(&___yiyouPlugin_93, value);
	}

	inline static int32_t get_offset_of_changkuPlugin_94() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___changkuPlugin_94)); }
	inline PluginChangku_t537709740 * get_changkuPlugin_94() const { return ___changkuPlugin_94; }
	inline PluginChangku_t537709740 ** get_address_of_changkuPlugin_94() { return &___changkuPlugin_94; }
	inline void set_changkuPlugin_94(PluginChangku_t537709740 * value)
	{
		___changkuPlugin_94 = value;
		Il2CppCodeGenWriteBarrier(&___changkuPlugin_94, value);
	}

	inline static int32_t get_offset_of_juliangPlugin_95() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___juliangPlugin_95)); }
	inline PluginJuliang_t2837451263 * get_juliangPlugin_95() const { return ___juliangPlugin_95; }
	inline PluginJuliang_t2837451263 ** get_address_of_juliangPlugin_95() { return &___juliangPlugin_95; }
	inline void set_juliangPlugin_95(PluginJuliang_t2837451263 * value)
	{
		___juliangPlugin_95 = value;
		Il2CppCodeGenWriteBarrier(&___juliangPlugin_95, value);
	}

	inline static int32_t get_offset_of_dianzhiPlugin_96() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___dianzhiPlugin_96)); }
	inline PluginDianZhi_t1453829974 * get_dianzhiPlugin_96() const { return ___dianzhiPlugin_96; }
	inline PluginDianZhi_t1453829974 ** get_address_of_dianzhiPlugin_96() { return &___dianzhiPlugin_96; }
	inline void set_dianzhiPlugin_96(PluginDianZhi_t1453829974 * value)
	{
		___dianzhiPlugin_96 = value;
		Il2CppCodeGenWriteBarrier(&___dianzhiPlugin_96, value);
	}

	inline static int32_t get_offset_of_kuaiWanPlugin_97() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___kuaiWanPlugin_97)); }
	inline PluginKuaiWan_t3714786207 * get_kuaiWanPlugin_97() const { return ___kuaiWanPlugin_97; }
	inline PluginKuaiWan_t3714786207 ** get_address_of_kuaiWanPlugin_97() { return &___kuaiWanPlugin_97; }
	inline void set_kuaiWanPlugin_97(PluginKuaiWan_t3714786207 * value)
	{
		___kuaiWanPlugin_97 = value;
		Il2CppCodeGenWriteBarrier(&___kuaiWanPlugin_97, value);
	}

	inline static int32_t get_offset_of_moHePlugin_98() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___moHePlugin_98)); }
	inline PluginMoHe_t1606231090 * get_moHePlugin_98() const { return ___moHePlugin_98; }
	inline PluginMoHe_t1606231090 ** get_address_of_moHePlugin_98() { return &___moHePlugin_98; }
	inline void set_moHePlugin_98(PluginMoHe_t1606231090 * value)
	{
		___moHePlugin_98 = value;
		Il2CppCodeGenWriteBarrier(&___moHePlugin_98, value);
	}

	inline static int32_t get_offset_of_zhangyuPlugin_99() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___zhangyuPlugin_99)); }
	inline PluginZhangYu_t3770424661 * get_zhangyuPlugin_99() const { return ___zhangyuPlugin_99; }
	inline PluginZhangYu_t3770424661 ** get_address_of_zhangyuPlugin_99() { return &___zhangyuPlugin_99; }
	inline void set_zhangyuPlugin_99(PluginZhangYu_t3770424661 * value)
	{
		___zhangyuPlugin_99 = value;
		Il2CppCodeGenWriteBarrier(&___zhangyuPlugin_99, value);
	}

	inline static int32_t get_offset_of_sanXiangPlugin_100() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___sanXiangPlugin_100)); }
	inline PluginSanXiang_t38647228 * get_sanXiangPlugin_100() const { return ___sanXiangPlugin_100; }
	inline PluginSanXiang_t38647228 ** get_address_of_sanXiangPlugin_100() { return &___sanXiangPlugin_100; }
	inline void set_sanXiangPlugin_100(PluginSanXiang_t38647228 * value)
	{
		___sanXiangPlugin_100 = value;
		Il2CppCodeGenWriteBarrier(&___sanXiangPlugin_100, value);
	}

	inline static int32_t get_offset_of_shoumengPlugin_101() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___shoumengPlugin_101)); }
	inline PluginShoumeng_t2011739967 * get_shoumengPlugin_101() const { return ___shoumengPlugin_101; }
	inline PluginShoumeng_t2011739967 ** get_address_of_shoumengPlugin_101() { return &___shoumengPlugin_101; }
	inline void set_shoumengPlugin_101(PluginShoumeng_t2011739967 * value)
	{
		___shoumengPlugin_101 = value;
		Il2CppCodeGenWriteBarrier(&___shoumengPlugin_101, value);
	}

	inline static int32_t get_offset_of_caohuaPlugin_102() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___caohuaPlugin_102)); }
	inline PluginCaoHua_t1396735126 * get_caohuaPlugin_102() const { return ___caohuaPlugin_102; }
	inline PluginCaoHua_t1396735126 ** get_address_of_caohuaPlugin_102() { return &___caohuaPlugin_102; }
	inline void set_caohuaPlugin_102(PluginCaoHua_t1396735126 * value)
	{
		___caohuaPlugin_102 = value;
		Il2CppCodeGenWriteBarrier(&___caohuaPlugin_102, value);
	}

	inline static int32_t get_offset_of_xiaoNiuPlugin_103() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___xiaoNiuPlugin_103)); }
	inline PluginXiaoNiu_t2024052712 * get_xiaoNiuPlugin_103() const { return ___xiaoNiuPlugin_103; }
	inline PluginXiaoNiu_t2024052712 ** get_address_of_xiaoNiuPlugin_103() { return &___xiaoNiuPlugin_103; }
	inline void set_xiaoNiuPlugin_103(PluginXiaoNiu_t2024052712 * value)
	{
		___xiaoNiuPlugin_103 = value;
		Il2CppCodeGenWriteBarrier(&___xiaoNiuPlugin_103, value);
	}

	inline static int32_t get_offset_of_yulePlugin_104() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___yulePlugin_104)); }
	inline PluginYuLe_t1606594472 * get_yulePlugin_104() const { return ___yulePlugin_104; }
	inline PluginYuLe_t1606594472 ** get_address_of_yulePlugin_104() { return &___yulePlugin_104; }
	inline void set_yulePlugin_104(PluginYuLe_t1606594472 * value)
	{
		___yulePlugin_104 = value;
		Il2CppCodeGenWriteBarrier(&___yulePlugin_104, value);
	}

	inline static int32_t get_offset_of_xiongmaowanPlugin_105() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___xiongmaowanPlugin_105)); }
	inline PluginXiongmaowan_t3347898093 * get_xiongmaowanPlugin_105() const { return ___xiongmaowanPlugin_105; }
	inline PluginXiongmaowan_t3347898093 ** get_address_of_xiongmaowanPlugin_105() { return &___xiongmaowanPlugin_105; }
	inline void set_xiongmaowanPlugin_105(PluginXiongmaowan_t3347898093 * value)
	{
		___xiongmaowanPlugin_105 = value;
		Il2CppCodeGenWriteBarrier(&___xiongmaowanPlugin_105, value);
	}

	inline static int32_t get_offset_of_qianyouPlugin_106() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___qianyouPlugin_106)); }
	inline PluginQianYou_t106475207 * get_qianyouPlugin_106() const { return ___qianyouPlugin_106; }
	inline PluginQianYou_t106475207 ** get_address_of_qianyouPlugin_106() { return &___qianyouPlugin_106; }
	inline void set_qianyouPlugin_106(PluginQianYou_t106475207 * value)
	{
		___qianyouPlugin_106 = value;
		Il2CppCodeGenWriteBarrier(&___qianyouPlugin_106, value);
	}

	inline static int32_t get_offset_of_oMiPlugin_107() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___oMiPlugin_107)); }
	inline PluginOMi_t190362136 * get_oMiPlugin_107() const { return ___oMiPlugin_107; }
	inline PluginOMi_t190362136 ** get_address_of_oMiPlugin_107() { return &___oMiPlugin_107; }
	inline void set_oMiPlugin_107(PluginOMi_t190362136 * value)
	{
		___oMiPlugin_107 = value;
		Il2CppCodeGenWriteBarrier(&___oMiPlugin_107, value);
	}

	inline static int32_t get_offset_of_anfengPlugin_108() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___anfengPlugin_108)); }
	inline PluginAnfeng_t1351242136 * get_anfengPlugin_108() const { return ___anfengPlugin_108; }
	inline PluginAnfeng_t1351242136 ** get_address_of_anfengPlugin_108() { return &___anfengPlugin_108; }
	inline void set_anfengPlugin_108(PluginAnfeng_t1351242136 * value)
	{
		___anfengPlugin_108 = value;
		Il2CppCodeGenWriteBarrier(&___anfengPlugin_108, value);
	}

	inline static int32_t get_offset_of_qiziPlugin_109() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___qiziPlugin_109)); }
	inline PluginQiZi_t1606345050 * get_qiziPlugin_109() const { return ___qiziPlugin_109; }
	inline PluginQiZi_t1606345050 ** get_address_of_qiziPlugin_109() { return &___qiziPlugin_109; }
	inline void set_qiziPlugin_109(PluginQiZi_t1606345050 * value)
	{
		___qiziPlugin_109 = value;
		Il2CppCodeGenWriteBarrier(&___qiziPlugin_109, value);
	}

	inline static int32_t get_offset_of_shoumengA1Plugin_110() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___shoumengA1Plugin_110)); }
	inline PluginShoumeng_A1_t4066769168 * get_shoumengA1Plugin_110() const { return ___shoumengA1Plugin_110; }
	inline PluginShoumeng_A1_t4066769168 ** get_address_of_shoumengA1Plugin_110() { return &___shoumengA1Plugin_110; }
	inline void set_shoumengA1Plugin_110(PluginShoumeng_A1_t4066769168 * value)
	{
		___shoumengA1Plugin_110 = value;
		Il2CppCodeGenWriteBarrier(&___shoumengA1Plugin_110, value);
	}

	inline static int32_t get_offset_of_tianyingPlugin_111() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___tianyingPlugin_111)); }
	inline PluginTianYing_t4234189246 * get_tianyingPlugin_111() const { return ___tianyingPlugin_111; }
	inline PluginTianYing_t4234189246 ** get_address_of_tianyingPlugin_111() { return &___tianyingPlugin_111; }
	inline void set_tianyingPlugin_111(PluginTianYing_t4234189246 * value)
	{
		___tianyingPlugin_111 = value;
		Il2CppCodeGenWriteBarrier(&___tianyingPlugin_111, value);
	}

	inline static int32_t get_offset_of_fastPlayPlugin_112() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___fastPlayPlugin_112)); }
	inline PluginFastPlay_t3320186403 * get_fastPlayPlugin_112() const { return ___fastPlayPlugin_112; }
	inline PluginFastPlay_t3320186403 ** get_address_of_fastPlayPlugin_112() { return &___fastPlayPlugin_112; }
	inline void set_fastPlayPlugin_112(PluginFastPlay_t3320186403 * value)
	{
		___fastPlayPlugin_112 = value;
		Il2CppCodeGenWriteBarrier(&___fastPlayPlugin_112, value);
	}

	inline static int32_t get_offset_of_youxiangPlugin_113() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___youxiangPlugin_113)); }
	inline PluginYouxiang_t1675681021 * get_youxiangPlugin_113() const { return ___youxiangPlugin_113; }
	inline PluginYouxiang_t1675681021 ** get_address_of_youxiangPlugin_113() { return &___youxiangPlugin_113; }
	inline void set_youxiangPlugin_113(PluginYouxiang_t1675681021 * value)
	{
		___youxiangPlugin_113 = value;
		Il2CppCodeGenWriteBarrier(&___youxiangPlugin_113, value);
	}

	inline static int32_t get_offset_of_yinHuPlugin_114() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___yinHuPlugin_114)); }
	inline PluginYinHu_t2559462776 * get_yinHuPlugin_114() const { return ___yinHuPlugin_114; }
	inline PluginYinHu_t2559462776 ** get_address_of_yinHuPlugin_114() { return &___yinHuPlugin_114; }
	inline void set_yinHuPlugin_114(PluginYinHu_t2559462776 * value)
	{
		___yinHuPlugin_114 = value;
		Il2CppCodeGenWriteBarrier(&___yinHuPlugin_114, value);
	}

	inline static int32_t get_offset_of_qiangwanPlugin_115() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___qiangwanPlugin_115)); }
	inline PluginQiangWan_t3301124917 * get_qiangwanPlugin_115() const { return ___qiangwanPlugin_115; }
	inline PluginQiangWan_t3301124917 ** get_address_of_qiangwanPlugin_115() { return &___qiangwanPlugin_115; }
	inline void set_qiangwanPlugin_115(PluginQiangWan_t3301124917 * value)
	{
		___qiangwanPlugin_115 = value;
		Il2CppCodeGenWriteBarrier(&___qiangwanPlugin_115, value);
	}

	inline static int32_t get_offset_of_kuaiwansjPlugin_116() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___kuaiwansjPlugin_116)); }
	inline PluginKuaiWanSJ_t791724598 * get_kuaiwansjPlugin_116() const { return ___kuaiwansjPlugin_116; }
	inline PluginKuaiWanSJ_t791724598 ** get_address_of_kuaiwansjPlugin_116() { return &___kuaiwansjPlugin_116; }
	inline void set_kuaiwansjPlugin_116(PluginKuaiWanSJ_t791724598 * value)
	{
		___kuaiwansjPlugin_116 = value;
		Il2CppCodeGenWriteBarrier(&___kuaiwansjPlugin_116, value);
	}

	inline static int32_t get_offset_of_dolphinPlugin_117() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___dolphinPlugin_117)); }
	inline PluginDolphin_t1635836683 * get_dolphinPlugin_117() const { return ___dolphinPlugin_117; }
	inline PluginDolphin_t1635836683 ** get_address_of_dolphinPlugin_117() { return &___dolphinPlugin_117; }
	inline void set_dolphinPlugin_117(PluginDolphin_t1635836683 * value)
	{
		___dolphinPlugin_117 = value;
		Il2CppCodeGenWriteBarrier(&___dolphinPlugin_117, value);
	}

	inline static int32_t get_offset_of_wanyuPlugin_118() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___wanyuPlugin_118)); }
	inline PluginWanYu_t2557377933 * get_wanyuPlugin_118() const { return ___wanyuPlugin_118; }
	inline PluginWanYu_t2557377933 ** get_address_of_wanyuPlugin_118() { return &___wanyuPlugin_118; }
	inline void set_wanyuPlugin_118(PluginWanYu_t2557377933 * value)
	{
		___wanyuPlugin_118 = value;
		Il2CppCodeGenWriteBarrier(&___wanyuPlugin_118, value);
	}

	inline static int32_t get_offset_of_quwanPlugin_119() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___quwanPlugin_119)); }
	inline PluginQuWan_t2552410765 * get_quwanPlugin_119() const { return ___quwanPlugin_119; }
	inline PluginQuWan_t2552410765 ** get_address_of_quwanPlugin_119() { return &___quwanPlugin_119; }
	inline void set_quwanPlugin_119(PluginQuWan_t2552410765 * value)
	{
		___quwanPlugin_119 = value;
		Il2CppCodeGenWriteBarrier(&___quwanPlugin_119, value);
	}

	inline static int32_t get_offset_of_xiaomaiPlugin_120() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___xiaomaiPlugin_120)); }
	inline PluginXiaoMai_t2024051491 * get_xiaomaiPlugin_120() const { return ___xiaomaiPlugin_120; }
	inline PluginXiaoMai_t2024051491 ** get_address_of_xiaomaiPlugin_120() { return &___xiaomaiPlugin_120; }
	inline void set_xiaomaiPlugin_120(PluginXiaoMai_t2024051491 * value)
	{
		___xiaomaiPlugin_120 = value;
		Il2CppCodeGenWriteBarrier(&___xiaomaiPlugin_120, value);
	}

	inline static int32_t get_offset_of_ailePlugin_121() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___ailePlugin_121)); }
	inline PluginAiLe_t1605867956 * get_ailePlugin_121() const { return ___ailePlugin_121; }
	inline PluginAiLe_t1605867956 ** get_address_of_ailePlugin_121() { return &___ailePlugin_121; }
	inline void set_ailePlugin_121(PluginAiLe_t1605867956 * value)
	{
		___ailePlugin_121 = value;
		Il2CppCodeGenWriteBarrier(&___ailePlugin_121, value);
	}

	inline static int32_t get_offset_of_IsOpenSjoy_122() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenSjoy_122)); }
	inline bool get_IsOpenSjoy_122() const { return ___IsOpenSjoy_122; }
	inline bool* get_address_of_IsOpenSjoy_122() { return &___IsOpenSjoy_122; }
	inline void set_IsOpenSjoy_122(bool value)
	{
		___IsOpenSjoy_122 = value;
	}

	inline static int32_t get_offset_of_IsOpenPush_123() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenPush_123)); }
	inline bool get_IsOpenPush_123() const { return ___IsOpenPush_123; }
	inline bool* get_address_of_IsOpenPush_123() { return &___IsOpenPush_123; }
	inline void set_IsOpenPush_123(bool value)
	{
		___IsOpenPush_123 = value;
	}

	inline static int32_t get_offset_of_IsOpenReYun_124() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenReYun_124)); }
	inline bool get_IsOpenReYun_124() const { return ___IsOpenReYun_124; }
	inline bool* get_address_of_IsOpenReYun_124() { return &___IsOpenReYun_124; }
	inline void set_IsOpenReYun_124(bool value)
	{
		___IsOpenReYun_124 = value;
	}

	inline static int32_t get_offset_of_IsOpenBugly_125() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenBugly_125)); }
	inline bool get_IsOpenBugly_125() const { return ___IsOpenBugly_125; }
	inline bool* get_address_of_IsOpenBugly_125() { return &___IsOpenBugly_125; }
	inline void set_IsOpenBugly_125(bool value)
	{
		___IsOpenBugly_125 = value;
	}

	inline static int32_t get_offset_of_IsOpenTool_126() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenTool_126)); }
	inline bool get_IsOpenTool_126() const { return ___IsOpenTool_126; }
	inline bool* get_address_of_IsOpenTool_126() { return &___IsOpenTool_126; }
	inline void set_IsOpenTool_126(bool value)
	{
		___IsOpenTool_126 = value;
	}

	inline static int32_t get_offset_of_IsTlog_127() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsTlog_127)); }
	inline bool get_IsTlog_127() const { return ___IsTlog_127; }
	inline bool* get_address_of_IsTlog_127() { return &___IsTlog_127; }
	inline void set_IsTlog_127(bool value)
	{
		___IsTlog_127 = value;
	}

	inline static int32_t get_offset_of_IsOpenXunFei_128() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenXunFei_128)); }
	inline bool get_IsOpenXunFei_128() const { return ___IsOpenXunFei_128; }
	inline bool* get_address_of_IsOpenXunFei_128() { return &___IsOpenXunFei_128; }
	inline void set_IsOpenXunFei_128(bool value)
	{
		___IsOpenXunFei_128 = value;
	}

	inline static int32_t get_offset_of_IsOpenHongYou_129() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenHongYou_129)); }
	inline bool get_IsOpenHongYou_129() const { return ___IsOpenHongYou_129; }
	inline bool* get_address_of_IsOpenHongYou_129() { return &___IsOpenHongYou_129; }
	inline void set_IsOpenHongYou_129(bool value)
	{
		___IsOpenHongYou_129 = value;
	}

	inline static int32_t get_offset_of_IsOpenYiJie_130() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenYiJie_130)); }
	inline bool get_IsOpenYiJie_130() const { return ___IsOpenYiJie_130; }
	inline bool* get_address_of_IsOpenYiJie_130() { return &___IsOpenYiJie_130; }
	inline void set_IsOpenYiJie_130(bool value)
	{
		___IsOpenYiJie_130 = value;
	}

	inline static int32_t get_offset_of_IsOpenTaiqi_131() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenTaiqi_131)); }
	inline bool get_IsOpenTaiqi_131() const { return ___IsOpenTaiqi_131; }
	inline bool* get_address_of_IsOpenTaiqi_131() { return &___IsOpenTaiqi_131; }
	inline void set_IsOpenTaiqi_131(bool value)
	{
		___IsOpenTaiqi_131 = value;
	}

	inline static int32_t get_offset_of_IsOpenAnqu_132() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenAnqu_132)); }
	inline bool get_IsOpenAnqu_132() const { return ___IsOpenAnqu_132; }
	inline bool* get_address_of_IsOpenAnqu_132() { return &___IsOpenAnqu_132; }
	inline void set_IsOpenAnqu_132(bool value)
	{
		___IsOpenAnqu_132 = value;
	}

	inline static int32_t get_offset_of_IsOpenXiaomi_133() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenXiaomi_133)); }
	inline bool get_IsOpenXiaomi_133() const { return ___IsOpenXiaomi_133; }
	inline bool* get_address_of_IsOpenXiaomi_133() { return &___IsOpenXiaomi_133; }
	inline void set_IsOpenXiaomi_133(bool value)
	{
		___IsOpenXiaomi_133 = value;
	}

	inline static int32_t get_offset_of_IsOpenXuegao_134() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenXuegao_134)); }
	inline bool get_IsOpenXuegao_134() const { return ___IsOpenXuegao_134; }
	inline bool* get_address_of_IsOpenXuegao_134() { return &___IsOpenXuegao_134; }
	inline void set_IsOpenXuegao_134(bool value)
	{
		___IsOpenXuegao_134 = value;
	}

	inline static int32_t get_offset_of_IsOpenYiwan_135() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenYiwan_135)); }
	inline bool get_IsOpenYiwan_135() const { return ___IsOpenYiwan_135; }
	inline bool* get_address_of_IsOpenYiwan_135() { return &___IsOpenYiwan_135; }
	inline void set_IsOpenYiwan_135(bool value)
	{
		___IsOpenYiwan_135 = value;
	}

	inline static int32_t get_offset_of_IsOpenWanmi_136() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenWanmi_136)); }
	inline bool get_IsOpenWanmi_136() const { return ___IsOpenWanmi_136; }
	inline bool* get_address_of_IsOpenWanmi_136() { return &___IsOpenWanmi_136; }
	inline void set_IsOpenWanmi_136(bool value)
	{
		___IsOpenWanmi_136 = value;
	}

	inline static int32_t get_offset_of_IsOpenHuawei_137() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenHuawei_137)); }
	inline bool get_IsOpenHuawei_137() const { return ___IsOpenHuawei_137; }
	inline bool* get_address_of_IsOpenHuawei_137() { return &___IsOpenHuawei_137; }
	inline void set_IsOpenHuawei_137(bool value)
	{
		___IsOpenHuawei_137 = value;
	}

	inline static int32_t get_offset_of_IsOpenVivo_138() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenVivo_138)); }
	inline bool get_IsOpenVivo_138() const { return ___IsOpenVivo_138; }
	inline bool* get_address_of_IsOpenVivo_138() { return &___IsOpenVivo_138; }
	inline void set_IsOpenVivo_138(bool value)
	{
		___IsOpenVivo_138 = value;
	}

	inline static int32_t get_offset_of_IsOpenOppo_139() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenOppo_139)); }
	inline bool get_IsOpenOppo_139() const { return ___IsOpenOppo_139; }
	inline bool* get_address_of_IsOpenOppo_139() { return &___IsOpenOppo_139; }
	inline void set_IsOpenOppo_139(bool value)
	{
		___IsOpenOppo_139 = value;
	}

	inline static int32_t get_offset_of_IsOpenUC_140() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenUC_140)); }
	inline bool get_IsOpenUC_140() const { return ___IsOpenUC_140; }
	inline bool* get_address_of_IsOpenUC_140() { return &___IsOpenUC_140; }
	inline void set_IsOpenUC_140(bool value)
	{
		___IsOpenUC_140 = value;
	}

	inline static int32_t get_offset_of_IsOpen360_141() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpen360_141)); }
	inline bool get_IsOpen360_141() const { return ___IsOpen360_141; }
	inline bool* get_address_of_IsOpen360_141() { return &___IsOpen360_141; }
	inline void set_IsOpen360_141(bool value)
	{
		___IsOpen360_141 = value;
	}

	inline static int32_t get_offset_of_IsOpenMihua_142() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenMihua_142)); }
	inline bool get_IsOpenMihua_142() const { return ___IsOpenMihua_142; }
	inline bool* get_address_of_IsOpenMihua_142() { return &___IsOpenMihua_142; }
	inline void set_IsOpenMihua_142(bool value)
	{
		___IsOpenMihua_142 = value;
	}

	inline static int32_t get_offset_of_IsOpenYyb_143() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenYyb_143)); }
	inline bool get_IsOpenYyb_143() const { return ___IsOpenYyb_143; }
	inline bool* get_address_of_IsOpenYyb_143() { return &___IsOpenYyb_143; }
	inline void set_IsOpenYyb_143(bool value)
	{
		___IsOpenYyb_143 = value;
	}

	inline static int32_t get_offset_of_IsOpenFlyme_144() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenFlyme_144)); }
	inline bool get_IsOpenFlyme_144() const { return ___IsOpenFlyme_144; }
	inline bool* get_address_of_IsOpenFlyme_144() { return &___IsOpenFlyme_144; }
	inline void set_IsOpenFlyme_144(bool value)
	{
		___IsOpenFlyme_144 = value;
	}

	inline static int32_t get_offset_of_IsOpenJinLi_145() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenJinLi_145)); }
	inline bool get_IsOpenJinLi_145() const { return ___IsOpenJinLi_145; }
	inline bool* get_address_of_IsOpenJinLi_145() { return &___IsOpenJinLi_145; }
	inline void set_IsOpenJinLi_145(bool value)
	{
		___IsOpenJinLi_145 = value;
	}

	inline static int32_t get_offset_of_IsOpenQuick_146() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenQuick_146)); }
	inline bool get_IsOpenQuick_146() const { return ___IsOpenQuick_146; }
	inline bool* get_address_of_IsOpenQuick_146() { return &___IsOpenQuick_146; }
	inline void set_IsOpenQuick_146(bool value)
	{
		___IsOpenQuick_146 = value;
	}

	inline static int32_t get_offset_of_IsOpenZxhy_147() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenZxhy_147)); }
	inline bool get_IsOpenZxhy_147() const { return ___IsOpenZxhy_147; }
	inline bool* get_address_of_IsOpenZxhy_147() { return &___IsOpenZxhy_147; }
	inline void set_IsOpenZxhy_147(bool value)
	{
		___IsOpenZxhy_147 = value;
	}

	inline static int32_t get_offset_of_IsOpenOasis_148() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenOasis_148)); }
	inline bool get_IsOpenOasis_148() const { return ___IsOpenOasis_148; }
	inline bool* get_address_of_IsOpenOasis_148() { return &___IsOpenOasis_148; }
	inline void set_IsOpenOasis_148(bool value)
	{
		___IsOpenOasis_148 = value;
	}

	inline static int32_t get_offset_of_IsOpenOasisIos_149() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenOasisIos_149)); }
	inline bool get_IsOpenOasisIos_149() const { return ___IsOpenOasisIos_149; }
	inline bool* get_address_of_IsOpenOasisIos_149() { return &___IsOpenOasisIos_149; }
	inline void set_IsOpenOasisIos_149(bool value)
	{
		___IsOpenOasisIos_149 = value;
	}

	inline static int32_t get_offset_of_IsOpenAiwan_150() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenAiwan_150)); }
	inline bool get_IsOpenAiwan_150() const { return ___IsOpenAiwan_150; }
	inline bool* get_address_of_IsOpenAiwan_150() { return &___IsOpenAiwan_150; }
	inline void set_IsOpenAiwan_150(bool value)
	{
		___IsOpenAiwan_150 = value;
	}

	inline static int32_t get_offset_of_IsOpenJingang_151() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenJingang_151)); }
	inline bool get_IsOpenJingang_151() const { return ___IsOpenJingang_151; }
	inline bool* get_address_of_IsOpenJingang_151() { return &___IsOpenJingang_151; }
	inline void set_IsOpenJingang_151(bool value)
	{
		___IsOpenJingang_151 = value;
	}

	inline static int32_t get_offset_of_IsOpenZhiquyou_152() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenZhiquyou_152)); }
	inline bool get_IsOpenZhiquyou_152() const { return ___IsOpenZhiquyou_152; }
	inline bool* get_address_of_IsOpenZhiquyou_152() { return &___IsOpenZhiquyou_152; }
	inline void set_IsOpenZhiquyou_152(bool value)
	{
		___IsOpenZhiquyou_152 = value;
	}

	inline static int32_t get_offset_of_IsOpenYouMa_153() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenYouMa_153)); }
	inline bool get_IsOpenYouMa_153() const { return ___IsOpenYouMa_153; }
	inline bool* get_address_of_IsOpenYouMa_153() { return &___IsOpenYouMa_153; }
	inline void set_IsOpenYouMa_153(bool value)
	{
		___IsOpenYouMa_153 = value;
	}

	inline static int32_t get_offset_of_IsOpenIqiyi_154() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenIqiyi_154)); }
	inline bool get_IsOpenIqiyi_154() const { return ___IsOpenIqiyi_154; }
	inline bool* get_address_of_IsOpenIqiyi_154() { return &___IsOpenIqiyi_154; }
	inline void set_IsOpenIqiyi_154(bool value)
	{
		___IsOpenIqiyi_154 = value;
	}

	inline static int32_t get_offset_of_IsOpenDiyibo_155() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenDiyibo_155)); }
	inline bool get_IsOpenDiyibo_155() const { return ___IsOpenDiyibo_155; }
	inline bool* get_address_of_IsOpenDiyibo_155() { return &___IsOpenDiyibo_155; }
	inline void set_IsOpenDiyibo_155(bool value)
	{
		___IsOpenDiyibo_155 = value;
	}

	inline static int32_t get_offset_of_IsOpenYiwansuper_156() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenYiwansuper_156)); }
	inline bool get_IsOpenYiwansuper_156() const { return ___IsOpenYiwansuper_156; }
	inline bool* get_address_of_IsOpenYiwansuper_156() { return &___IsOpenYiwansuper_156; }
	inline void set_IsOpenYiwansuper_156(bool value)
	{
		___IsOpenYiwansuper_156 = value;
	}

	inline static int32_t get_offset_of_IsOpenHongyouNY_157() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenHongyouNY_157)); }
	inline bool get_IsOpenHongyouNY_157() const { return ___IsOpenHongyouNY_157; }
	inline bool* get_address_of_IsOpenHongyouNY_157() { return &___IsOpenHongyouNY_157; }
	inline void set_IsOpenHongyouNY_157(bool value)
	{
		___IsOpenHongyouNY_157 = value;
	}

	inline static int32_t get_offset_of_IsOpenInFox_158() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenInFox_158)); }
	inline bool get_IsOpenInFox_158() const { return ___IsOpenInFox_158; }
	inline bool* get_address_of_IsOpenInFox_158() { return &___IsOpenInFox_158; }
	inline void set_IsOpenInFox_158(bool value)
	{
		___IsOpenInFox_158 = value;
	}

	inline static int32_t get_offset_of_IsOpenR2_159() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenR2_159)); }
	inline bool get_IsOpenR2_159() const { return ___IsOpenR2_159; }
	inline bool* get_address_of_IsOpenR2_159() { return &___IsOpenR2_159; }
	inline void set_IsOpenR2_159(bool value)
	{
		___IsOpenR2_159 = value;
	}

	inline static int32_t get_offset_of_IsOpenYuewan_160() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenYuewan_160)); }
	inline bool get_IsOpenYuewan_160() const { return ___IsOpenYuewan_160; }
	inline bool* get_address_of_IsOpenYuewan_160() { return &___IsOpenYuewan_160; }
	inline void set_IsOpenYuewan_160(bool value)
	{
		___IsOpenYuewan_160 = value;
	}

	inline static int32_t get_offset_of_IsOpenSamsung_161() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenSamsung_161)); }
	inline bool get_IsOpenSamsung_161() const { return ___IsOpenSamsung_161; }
	inline bool* get_address_of_IsOpenSamsung_161() { return &___IsOpenSamsung_161; }
	inline void set_IsOpenSamsung_161(bool value)
	{
		___IsOpenSamsung_161 = value;
	}

	inline static int32_t get_offset_of_IsOpenFengqi_162() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenFengqi_162)); }
	inline bool get_IsOpenFengqi_162() const { return ___IsOpenFengqi_162; }
	inline bool* get_address_of_IsOpenFengqi_162() { return &___IsOpenFengqi_162; }
	inline void set_IsOpenFengqi_162(bool value)
	{
		___IsOpenFengqi_162 = value;
	}

	inline static int32_t get_offset_of_IsOpenZhangXun_163() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenZhangXun_163)); }
	inline bool get_IsOpenZhangXun_163() const { return ___IsOpenZhangXun_163; }
	inline bool* get_address_of_IsOpenZhangXun_163() { return &___IsOpenZhangXun_163; }
	inline void set_IsOpenZhangXun_163(bool value)
	{
		___IsOpenZhangXun_163 = value;
	}

	inline static int32_t get_offset_of_IsOpenYouDong_164() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenYouDong_164)); }
	inline bool get_IsOpenYouDong_164() const { return ___IsOpenYouDong_164; }
	inline bool* get_address_of_IsOpenYouDong_164() { return &___IsOpenYouDong_164; }
	inline void set_IsOpenYouDong_164(bool value)
	{
		___IsOpenYouDong_164 = value;
	}

	inline static int32_t get_offset_of_IsOpenYouWo_165() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenYouWo_165)); }
	inline bool get_IsOpenYouWo_165() const { return ___IsOpenYouWo_165; }
	inline bool* get_address_of_IsOpenYouWo_165() { return &___IsOpenYouWo_165; }
	inline void set_IsOpenYouWo_165(bool value)
	{
		___IsOpenYouWo_165 = value;
	}

	inline static int32_t get_offset_of_IsOpenFuncheer_166() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenFuncheer_166)); }
	inline bool get_IsOpenFuncheer_166() const { return ___IsOpenFuncheer_166; }
	inline bool* get_address_of_IsOpenFuncheer_166() { return &___IsOpenFuncheer_166; }
	inline void set_IsOpenFuncheer_166(bool value)
	{
		___IsOpenFuncheer_166 = value;
	}

	inline static int32_t get_offset_of_IsOpenYuePlay_167() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenYuePlay_167)); }
	inline bool get_IsOpenYuePlay_167() const { return ___IsOpenYuePlay_167; }
	inline bool* get_address_of_IsOpenYuePlay_167() { return &___IsOpenYuePlay_167; }
	inline void set_IsOpenYuePlay_167(bool value)
	{
		___IsOpenYuePlay_167 = value;
	}

	inline static int32_t get_offset_of_IsOpenMiquwan_168() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenMiquwan_168)); }
	inline bool get_IsOpenMiquwan_168() const { return ___IsOpenMiquwan_168; }
	inline bool* get_address_of_IsOpenMiquwan_168() { return &___IsOpenMiquwan_168; }
	inline void set_IsOpenMiquwan_168(bool value)
	{
		___IsOpenMiquwan_168 = value;
	}

	inline static int32_t get_offset_of_IsOpenGamecat_169() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenGamecat_169)); }
	inline bool get_IsOpenGamecat_169() const { return ___IsOpenGamecat_169; }
	inline bool* get_address_of_IsOpenGamecat_169() { return &___IsOpenGamecat_169; }
	inline void set_IsOpenGamecat_169(bool value)
	{
		___IsOpenGamecat_169 = value;
	}

	inline static int32_t get_offset_of_IsOpenJingqi_170() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenJingqi_170)); }
	inline bool get_IsOpenJingqi_170() const { return ___IsOpenJingqi_170; }
	inline bool* get_address_of_IsOpenJingqi_170() { return &___IsOpenJingqi_170; }
	inline void set_IsOpenJingqi_170(bool value)
	{
		___IsOpenJingqi_170 = value;
	}

	inline static int32_t get_offset_of_IsOpenYiyou_171() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenYiyou_171)); }
	inline bool get_IsOpenYiyou_171() const { return ___IsOpenYiyou_171; }
	inline bool* get_address_of_IsOpenYiyou_171() { return &___IsOpenYiyou_171; }
	inline void set_IsOpenYiyou_171(bool value)
	{
		___IsOpenYiyou_171 = value;
	}

	inline static int32_t get_offset_of_IsOpenChangku_172() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenChangku_172)); }
	inline bool get_IsOpenChangku_172() const { return ___IsOpenChangku_172; }
	inline bool* get_address_of_IsOpenChangku_172() { return &___IsOpenChangku_172; }
	inline void set_IsOpenChangku_172(bool value)
	{
		___IsOpenChangku_172 = value;
	}

	inline static int32_t get_offset_of_IsOpenJuliang_173() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenJuliang_173)); }
	inline bool get_IsOpenJuliang_173() const { return ___IsOpenJuliang_173; }
	inline bool* get_address_of_IsOpenJuliang_173() { return &___IsOpenJuliang_173; }
	inline void set_IsOpenJuliang_173(bool value)
	{
		___IsOpenJuliang_173 = value;
	}

	inline static int32_t get_offset_of_IsOpenDianZhi_174() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenDianZhi_174)); }
	inline bool get_IsOpenDianZhi_174() const { return ___IsOpenDianZhi_174; }
	inline bool* get_address_of_IsOpenDianZhi_174() { return &___IsOpenDianZhi_174; }
	inline void set_IsOpenDianZhi_174(bool value)
	{
		___IsOpenDianZhi_174 = value;
	}

	inline static int32_t get_offset_of_IsOpenKuaiWan_175() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenKuaiWan_175)); }
	inline bool get_IsOpenKuaiWan_175() const { return ___IsOpenKuaiWan_175; }
	inline bool* get_address_of_IsOpenKuaiWan_175() { return &___IsOpenKuaiWan_175; }
	inline void set_IsOpenKuaiWan_175(bool value)
	{
		___IsOpenKuaiWan_175 = value;
	}

	inline static int32_t get_offset_of_IsOpenMoHe_176() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenMoHe_176)); }
	inline bool get_IsOpenMoHe_176() const { return ___IsOpenMoHe_176; }
	inline bool* get_address_of_IsOpenMoHe_176() { return &___IsOpenMoHe_176; }
	inline void set_IsOpenMoHe_176(bool value)
	{
		___IsOpenMoHe_176 = value;
	}

	inline static int32_t get_offset_of_IsOpenZhangYu_177() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenZhangYu_177)); }
	inline bool get_IsOpenZhangYu_177() const { return ___IsOpenZhangYu_177; }
	inline bool* get_address_of_IsOpenZhangYu_177() { return &___IsOpenZhangYu_177; }
	inline void set_IsOpenZhangYu_177(bool value)
	{
		___IsOpenZhangYu_177 = value;
	}

	inline static int32_t get_offset_of_IsOpenSanXiang_178() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenSanXiang_178)); }
	inline bool get_IsOpenSanXiang_178() const { return ___IsOpenSanXiang_178; }
	inline bool* get_address_of_IsOpenSanXiang_178() { return &___IsOpenSanXiang_178; }
	inline void set_IsOpenSanXiang_178(bool value)
	{
		___IsOpenSanXiang_178 = value;
	}

	inline static int32_t get_offset_of_IsOpenShoumeng_179() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenShoumeng_179)); }
	inline bool get_IsOpenShoumeng_179() const { return ___IsOpenShoumeng_179; }
	inline bool* get_address_of_IsOpenShoumeng_179() { return &___IsOpenShoumeng_179; }
	inline void set_IsOpenShoumeng_179(bool value)
	{
		___IsOpenShoumeng_179 = value;
	}

	inline static int32_t get_offset_of_IsOpenCaoHua_180() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenCaoHua_180)); }
	inline bool get_IsOpenCaoHua_180() const { return ___IsOpenCaoHua_180; }
	inline bool* get_address_of_IsOpenCaoHua_180() { return &___IsOpenCaoHua_180; }
	inline void set_IsOpenCaoHua_180(bool value)
	{
		___IsOpenCaoHua_180 = value;
	}

	inline static int32_t get_offset_of_IsOpenYuLe_181() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenYuLe_181)); }
	inline bool get_IsOpenYuLe_181() const { return ___IsOpenYuLe_181; }
	inline bool* get_address_of_IsOpenYuLe_181() { return &___IsOpenYuLe_181; }
	inline void set_IsOpenYuLe_181(bool value)
	{
		___IsOpenYuLe_181 = value;
	}

	inline static int32_t get_offset_of_IsOpenXiongmaowan_182() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenXiongmaowan_182)); }
	inline bool get_IsOpenXiongmaowan_182() const { return ___IsOpenXiongmaowan_182; }
	inline bool* get_address_of_IsOpenXiongmaowan_182() { return &___IsOpenXiongmaowan_182; }
	inline void set_IsOpenXiongmaowan_182(bool value)
	{
		___IsOpenXiongmaowan_182 = value;
	}

	inline static int32_t get_offset_of_IsOpenQianyou_183() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenQianyou_183)); }
	inline bool get_IsOpenQianyou_183() const { return ___IsOpenQianyou_183; }
	inline bool* get_address_of_IsOpenQianyou_183() { return &___IsOpenQianyou_183; }
	inline void set_IsOpenQianyou_183(bool value)
	{
		___IsOpenQianyou_183 = value;
	}

	inline static int32_t get_offset_of_IsOpenOMi_184() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenOMi_184)); }
	inline bool get_IsOpenOMi_184() const { return ___IsOpenOMi_184; }
	inline bool* get_address_of_IsOpenOMi_184() { return &___IsOpenOMi_184; }
	inline void set_IsOpenOMi_184(bool value)
	{
		___IsOpenOMi_184 = value;
	}

	inline static int32_t get_offset_of_IsOpenAnfeng_185() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenAnfeng_185)); }
	inline bool get_IsOpenAnfeng_185() const { return ___IsOpenAnfeng_185; }
	inline bool* get_address_of_IsOpenAnfeng_185() { return &___IsOpenAnfeng_185; }
	inline void set_IsOpenAnfeng_185(bool value)
	{
		___IsOpenAnfeng_185 = value;
	}

	inline static int32_t get_offset_of_IsOpenQizi_186() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenQizi_186)); }
	inline bool get_IsOpenQizi_186() const { return ___IsOpenQizi_186; }
	inline bool* get_address_of_IsOpenQizi_186() { return &___IsOpenQizi_186; }
	inline void set_IsOpenQizi_186(bool value)
	{
		___IsOpenQizi_186 = value;
	}

	inline static int32_t get_offset_of_IsOpenShoumengA1_187() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenShoumengA1_187)); }
	inline bool get_IsOpenShoumengA1_187() const { return ___IsOpenShoumengA1_187; }
	inline bool* get_address_of_IsOpenShoumengA1_187() { return &___IsOpenShoumengA1_187; }
	inline void set_IsOpenShoumengA1_187(bool value)
	{
		___IsOpenShoumengA1_187 = value;
	}

	inline static int32_t get_offset_of_IsOpenTianying_188() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenTianying_188)); }
	inline bool get_IsOpenTianying_188() const { return ___IsOpenTianying_188; }
	inline bool* get_address_of_IsOpenTianying_188() { return &___IsOpenTianying_188; }
	inline void set_IsOpenTianying_188(bool value)
	{
		___IsOpenTianying_188 = value;
	}

	inline static int32_t get_offset_of_IsOpenFastPlay_189() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenFastPlay_189)); }
	inline bool get_IsOpenFastPlay_189() const { return ___IsOpenFastPlay_189; }
	inline bool* get_address_of_IsOpenFastPlay_189() { return &___IsOpenFastPlay_189; }
	inline void set_IsOpenFastPlay_189(bool value)
	{
		___IsOpenFastPlay_189 = value;
	}

	inline static int32_t get_offset_of_IsOpenYouxiang_190() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenYouxiang_190)); }
	inline bool get_IsOpenYouxiang_190() const { return ___IsOpenYouxiang_190; }
	inline bool* get_address_of_IsOpenYouxiang_190() { return &___IsOpenYouxiang_190; }
	inline void set_IsOpenYouxiang_190(bool value)
	{
		___IsOpenYouxiang_190 = value;
	}

	inline static int32_t get_offset_of_IsOpenXiaoNiu_191() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenXiaoNiu_191)); }
	inline bool get_IsOpenXiaoNiu_191() const { return ___IsOpenXiaoNiu_191; }
	inline bool* get_address_of_IsOpenXiaoNiu_191() { return &___IsOpenXiaoNiu_191; }
	inline void set_IsOpenXiaoNiu_191(bool value)
	{
		___IsOpenXiaoNiu_191 = value;
	}

	inline static int32_t get_offset_of_IsOpenYinHu_192() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenYinHu_192)); }
	inline bool get_IsOpenYinHu_192() const { return ___IsOpenYinHu_192; }
	inline bool* get_address_of_IsOpenYinHu_192() { return &___IsOpenYinHu_192; }
	inline void set_IsOpenYinHu_192(bool value)
	{
		___IsOpenYinHu_192 = value;
	}

	inline static int32_t get_offset_of_IsOpenQiangWan_193() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenQiangWan_193)); }
	inline bool get_IsOpenQiangWan_193() const { return ___IsOpenQiangWan_193; }
	inline bool* get_address_of_IsOpenQiangWan_193() { return &___IsOpenQiangWan_193; }
	inline void set_IsOpenQiangWan_193(bool value)
	{
		___IsOpenQiangWan_193 = value;
	}

	inline static int32_t get_offset_of_IsOpenNewMH_194() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenNewMH_194)); }
	inline bool get_IsOpenNewMH_194() const { return ___IsOpenNewMH_194; }
	inline bool* get_address_of_IsOpenNewMH_194() { return &___IsOpenNewMH_194; }
	inline void set_IsOpenNewMH_194(bool value)
	{
		___IsOpenNewMH_194 = value;
	}

	inline static int32_t get_offset_of_IsOpenKuaiWanSJ_195() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenKuaiWanSJ_195)); }
	inline bool get_IsOpenKuaiWanSJ_195() const { return ___IsOpenKuaiWanSJ_195; }
	inline bool* get_address_of_IsOpenKuaiWanSJ_195() { return &___IsOpenKuaiWanSJ_195; }
	inline void set_IsOpenKuaiWanSJ_195(bool value)
	{
		___IsOpenKuaiWanSJ_195 = value;
	}

	inline static int32_t get_offset_of_IsOpenNewMiquwan_196() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenNewMiquwan_196)); }
	inline bool get_IsOpenNewMiquwan_196() const { return ___IsOpenNewMiquwan_196; }
	inline bool* get_address_of_IsOpenNewMiquwan_196() { return &___IsOpenNewMiquwan_196; }
	inline void set_IsOpenNewMiquwan_196(bool value)
	{
		___IsOpenNewMiquwan_196 = value;
	}

	inline static int32_t get_offset_of_IsOpenPaoJiao_197() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenPaoJiao_197)); }
	inline bool get_IsOpenPaoJiao_197() const { return ___IsOpenPaoJiao_197; }
	inline bool* get_address_of_IsOpenPaoJiao_197() { return &___IsOpenPaoJiao_197; }
	inline void set_IsOpenPaoJiao_197(bool value)
	{
		___IsOpenPaoJiao_197 = value;
	}

	inline static int32_t get_offset_of_IsOpenXiMa_198() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenXiMa_198)); }
	inline bool get_IsOpenXiMa_198() const { return ___IsOpenXiMa_198; }
	inline bool* get_address_of_IsOpenXiMa_198() { return &___IsOpenXiMa_198; }
	inline void set_IsOpenXiMa_198(bool value)
	{
		___IsOpenXiMa_198 = value;
	}

	inline static int32_t get_offset_of_IsOpenXiaoQi_199() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenXiaoQi_199)); }
	inline bool get_IsOpenXiaoQi_199() const { return ___IsOpenXiaoQi_199; }
	inline bool* get_address_of_IsOpenXiaoQi_199() { return &___IsOpenXiaoQi_199; }
	inline void set_IsOpenXiaoQi_199(bool value)
	{
		___IsOpenXiaoQi_199 = value;
	}

	inline static int32_t get_offset_of_IsOpenGuChuan_200() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenGuChuan_200)); }
	inline bool get_IsOpenGuChuan_200() const { return ___IsOpenGuChuan_200; }
	inline bool* get_address_of_IsOpenGuChuan_200() { return &___IsOpenGuChuan_200; }
	inline void set_IsOpenGuChuan_200(bool value)
	{
		___IsOpenGuChuan_200 = value;
	}

	inline static int32_t get_offset_of_IsOpenDolphin_201() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenDolphin_201)); }
	inline bool get_IsOpenDolphin_201() const { return ___IsOpenDolphin_201; }
	inline bool* get_address_of_IsOpenDolphin_201() { return &___IsOpenDolphin_201; }
	inline void set_IsOpenDolphin_201(bool value)
	{
		___IsOpenDolphin_201 = value;
	}

	inline static int32_t get_offset_of_IsOpenWanYu_202() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenWanYu_202)); }
	inline bool get_IsOpenWanYu_202() const { return ___IsOpenWanYu_202; }
	inline bool* get_address_of_IsOpenWanYu_202() { return &___IsOpenWanYu_202; }
	inline void set_IsOpenWanYu_202(bool value)
	{
		___IsOpenWanYu_202 = value;
	}

	inline static int32_t get_offset_of_IsOpenQuWan_203() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenQuWan_203)); }
	inline bool get_IsOpenQuWan_203() const { return ___IsOpenQuWan_203; }
	inline bool* get_address_of_IsOpenQuWan_203() { return &___IsOpenQuWan_203; }
	inline void set_IsOpenQuWan_203(bool value)
	{
		___IsOpenQuWan_203 = value;
	}

	inline static int32_t get_offset_of_IsOpenXiaomai_204() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenXiaomai_204)); }
	inline bool get_IsOpenXiaomai_204() const { return ___IsOpenXiaomai_204; }
	inline bool* get_address_of_IsOpenXiaomai_204() { return &___IsOpenXiaomai_204; }
	inline void set_IsOpenXiaomai_204(bool value)
	{
		___IsOpenXiaomai_204 = value;
	}

	inline static int32_t get_offset_of_IsOpenZongYou_205() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenZongYou_205)); }
	inline bool get_IsOpenZongYou_205() const { return ___IsOpenZongYou_205; }
	inline bool* get_address_of_IsOpenZongYou_205() { return &___IsOpenZongYou_205; }
	inline void set_IsOpenZongYou_205(bool value)
	{
		___IsOpenZongYou_205 = value;
	}

	inline static int32_t get_offset_of_IsOpenAiLe_206() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenAiLe_206)); }
	inline bool get_IsOpenAiLe_206() const { return ___IsOpenAiLe_206; }
	inline bool* get_address_of_IsOpenAiLe_206() { return &___IsOpenAiLe_206; }
	inline void set_IsOpenAiLe_206(bool value)
	{
		___IsOpenAiLe_206 = value;
	}

	inline static int32_t get_offset_of_IsOpenHuoshu_207() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenHuoshu_207)); }
	inline bool get_IsOpenHuoshu_207() const { return ___IsOpenHuoshu_207; }
	inline bool* get_address_of_IsOpenHuoshu_207() { return &___IsOpenHuoshu_207; }
	inline void set_IsOpenHuoshu_207(bool value)
	{
		___IsOpenHuoshu_207 = value;
	}

	inline static int32_t get_offset_of_IsOpenTT_208() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenTT_208)); }
	inline bool get_IsOpenTT_208() const { return ___IsOpenTT_208; }
	inline bool* get_address_of_IsOpenTT_208() { return &___IsOpenTT_208; }
	inline void set_IsOpenTT_208(bool value)
	{
		___IsOpenTT_208 = value;
	}

	inline static int32_t get_offset_of_IsOpenGamefan_209() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenGamefan_209)); }
	inline bool get_IsOpenGamefan_209() const { return ___IsOpenGamefan_209; }
	inline bool* get_address_of_IsOpenGamefan_209() { return &___IsOpenGamefan_209; }
	inline void set_IsOpenGamefan_209(bool value)
	{
		___IsOpenGamefan_209 = value;
	}

	inline static int32_t get_offset_of_IsOpenGuopan_210() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenGuopan_210)); }
	inline bool get_IsOpenGuopan_210() const { return ___IsOpenGuopan_210; }
	inline bool* get_address_of_IsOpenGuopan_210() { return &___IsOpenGuopan_210; }
	inline void set_IsOpenGuopan_210(bool value)
	{
		___IsOpenGuopan_210 = value;
	}

	inline static int32_t get_offset_of_IsOpenCgame_211() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenCgame_211)); }
	inline bool get_IsOpenCgame_211() const { return ___IsOpenCgame_211; }
	inline bool* get_address_of_IsOpenCgame_211() { return &___IsOpenCgame_211; }
	inline void set_IsOpenCgame_211(bool value)
	{
		___IsOpenCgame_211 = value;
	}

	inline static int32_t get_offset_of_IsOpenAiLeXinYou_212() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenAiLeXinYou_212)); }
	inline bool get_IsOpenAiLeXinYou_212() const { return ___IsOpenAiLeXinYou_212; }
	inline bool* get_address_of_IsOpenAiLeXinYou_212() { return &___IsOpenAiLeXinYou_212; }
	inline void set_IsOpenAiLeXinYou_212(bool value)
	{
		___IsOpenAiLeXinYou_212 = value;
	}

	inline static int32_t get_offset_of_IsOpenJuFengJQ_213() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenJuFengJQ_213)); }
	inline bool get_IsOpenJuFengJQ_213() const { return ___IsOpenJuFengJQ_213; }
	inline bool* get_address_of_IsOpenJuFengJQ_213() { return &___IsOpenJuFengJQ_213; }
	inline void set_IsOpenJuFengJQ_213(bool value)
	{
		___IsOpenJuFengJQ_213 = value;
	}

	inline static int32_t get_offset_of_IsOpenKuaiYou_214() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenKuaiYou_214)); }
	inline bool get_IsOpenKuaiYou_214() const { return ___IsOpenKuaiYou_214; }
	inline bool* get_address_of_IsOpenKuaiYou_214() { return &___IsOpenKuaiYou_214; }
	inline void set_IsOpenKuaiYou_214(bool value)
	{
		___IsOpenKuaiYou_214 = value;
	}

	inline static int32_t get_offset_of_IsOpenGuangDianTong_215() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenGuangDianTong_215)); }
	inline bool get_IsOpenGuangDianTong_215() const { return ___IsOpenGuangDianTong_215; }
	inline bool* get_address_of_IsOpenGuangDianTong_215() { return &___IsOpenGuangDianTong_215; }
	inline void set_IsOpenGuangDianTong_215(bool value)
	{
		___IsOpenGuangDianTong_215 = value;
	}

	inline static int32_t get_offset_of_IsOpenJinRiTouTiao_216() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenJinRiTouTiao_216)); }
	inline bool get_IsOpenJinRiTouTiao_216() const { return ___IsOpenJinRiTouTiao_216; }
	inline bool* get_address_of_IsOpenJinRiTouTiao_216() { return &___IsOpenJinRiTouTiao_216; }
	inline void set_IsOpenJinRiTouTiao_216(bool value)
	{
		___IsOpenJinRiTouTiao_216 = value;
	}

	inline static int32_t get_offset_of_IsOpenYouLong_217() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsOpenYouLong_217)); }
	inline bool get_IsOpenYouLong_217() const { return ___IsOpenYouLong_217; }
	inline bool* get_address_of_IsOpenYouLong_217() { return &___IsOpenYouLong_217; }
	inline void set_IsOpenYouLong_217(bool value)
	{
		___IsOpenYouLong_217 = value;
	}

	inline static int32_t get_offset_of_isHideOldSever_218() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___isHideOldSever_218)); }
	inline bool get_isHideOldSever_218() const { return ___isHideOldSever_218; }
	inline bool* get_address_of_isHideOldSever_218() { return &___isHideOldSever_218; }
	inline void set_isHideOldSever_218(bool value)
	{
		___isHideOldSever_218 = value;
	}

	inline static int32_t get_offset_of_IsLoginClick_219() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___IsLoginClick_219)); }
	inline bool get_IsLoginClick_219() const { return ___IsLoginClick_219; }
	inline bool* get_address_of_IsLoginClick_219() { return &___IsLoginClick_219; }
	inline void set_IsLoginClick_219(bool value)
	{
		___IsLoginClick_219 = value;
	}

	inline static int32_t get_offset_of_username_221() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___username_221)); }
	inline String_t* get_username_221() const { return ___username_221; }
	inline String_t** get_address_of_username_221() { return &___username_221; }
	inline void set_username_221(String_t* value)
	{
		___username_221 = value;
		Il2CppCodeGenWriteBarrier(&___username_221, value);
	}

	inline static int32_t get_offset_of_OnOK_222() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___OnOK_222)); }
	inline Action_t3771233898 * get_OnOK_222() const { return ___OnOK_222; }
	inline Action_t3771233898 ** get_address_of_OnOK_222() { return &___OnOK_222; }
	inline void set_OnOK_222(Action_t3771233898 * value)
	{
		___OnOK_222 = value;
		Il2CppCodeGenWriteBarrier(&___OnOK_222, value);
	}

	inline static int32_t get_offset_of_OnCancel_223() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___OnCancel_223)); }
	inline Action_t3771233898 * get_OnCancel_223() const { return ___OnCancel_223; }
	inline Action_t3771233898 ** get_address_of_OnCancel_223() { return &___OnCancel_223; }
	inline void set_OnCancel_223(Action_t3771233898 * value)
	{
		___OnCancel_223 = value;
		Il2CppCodeGenWriteBarrier(&___OnCancel_223, value);
	}

	inline static int32_t get_offset_of_U3CandroidJavaObjectU3Ek__BackingField_224() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670, ___U3CandroidJavaObjectU3Ek__BackingField_224)); }
	inline AndroidJavaObject_t2362096582 * get_U3CandroidJavaObjectU3Ek__BackingField_224() const { return ___U3CandroidJavaObjectU3Ek__BackingField_224; }
	inline AndroidJavaObject_t2362096582 ** get_address_of_U3CandroidJavaObjectU3Ek__BackingField_224() { return &___U3CandroidJavaObjectU3Ek__BackingField_224; }
	inline void set_U3CandroidJavaObjectU3Ek__BackingField_224(AndroidJavaObject_t2362096582 * value)
	{
		___U3CandroidJavaObjectU3Ek__BackingField_224 = value;
		Il2CppCodeGenWriteBarrier(&___U3CandroidJavaObjectU3Ek__BackingField_224, value);
	}
};

struct PluginsSdkMgr_t3884624670_StaticFields
{
public:
	// PluginsSdkMgr PluginsSdkMgr::_inst
	PluginsSdkMgr_t3884624670 * ____inst_220;

public:
	inline static int32_t get_offset_of__inst_220() { return static_cast<int32_t>(offsetof(PluginsSdkMgr_t3884624670_StaticFields, ____inst_220)); }
	inline PluginsSdkMgr_t3884624670 * get__inst_220() const { return ____inst_220; }
	inline PluginsSdkMgr_t3884624670 ** get_address_of__inst_220() { return &____inst_220; }
	inline void set__inst_220(PluginsSdkMgr_t3884624670 * value)
	{
		____inst_220 = value;
		Il2CppCodeGenWriteBarrier(&____inst_220, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
