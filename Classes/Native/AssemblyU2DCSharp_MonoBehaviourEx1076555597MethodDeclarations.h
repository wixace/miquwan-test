﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MonoBehaviourEx
struct MonoBehaviourEx_t1076555597;

#include "codegen/il2cpp-codegen.h"

// System.Void MonoBehaviourEx::.ctor()
extern "C"  void MonoBehaviourEx__ctor_m737197758 (MonoBehaviourEx_t1076555597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MonoBehaviourEx::get_time()
extern "C"  float MonoBehaviourEx_get_time_m3230371952 (MonoBehaviourEx_t1076555597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single MonoBehaviourEx::get_deltaTime()
extern "C"  float MonoBehaviourEx_get_deltaTime_m3496459204 (MonoBehaviourEx_t1076555597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
