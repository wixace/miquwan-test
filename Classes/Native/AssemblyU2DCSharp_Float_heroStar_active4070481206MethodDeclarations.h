﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_heroStar_active
struct Float_heroStar_active_t4070481206;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// FloatTextUnit
struct FloatTextUnit_t2362298029;
// EventDelegate/Callback
struct Callback_t1094463061;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_UITweener_Method1078127180.h"

// System.Void Float_heroStar_active::.ctor()
extern "C"  void Float_heroStar_active__ctor_m4100380917 (Float_heroStar_active_t4070481206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_heroStar_active::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_heroStar_active_OnAwake_m2074766769 (Float_heroStar_active_t4070481206 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_heroStar_active::FloatID()
extern "C"  int32_t Float_heroStar_active_FloatID_m3516130291 (Float_heroStar_active_t4070481206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_heroStar_active::Init()
extern "C"  void Float_heroStar_active_Init_m2162016959 (Float_heroStar_active_t4070481206 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_heroStar_active::SetFile(System.Object[])
extern "C"  void Float_heroStar_active_SetFile_m2541629569 (Float_heroStar_active_t4070481206 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_heroStar_active::ilo_MoveScale1(FloatTextUnit,System.Single,System.Single,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void Float_heroStar_active_ilo_MoveScale1_m485294190 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, float ___from1, float ___to2, float ___duration3, float ___delay4, Callback_t1094463061 * ___call5, GameObject_t3674682005 * ___go6, int32_t ___method7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
