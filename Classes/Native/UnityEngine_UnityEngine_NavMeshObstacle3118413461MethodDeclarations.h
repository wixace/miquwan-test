﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.NavMeshObstacle
struct NavMeshObstacle_t3118413461;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_NavMeshObstacleShape1553866374.h"

// System.Void UnityEngine.NavMeshObstacle::.ctor()
extern "C"  void NavMeshObstacle__ctor_m1232951002 (NavMeshObstacle_t3118413461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.NavMeshObstacle::get_height()
extern "C"  float NavMeshObstacle_get_height_m701895918 (NavMeshObstacle_t3118413461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshObstacle::set_height(System.Single)
extern "C"  void NavMeshObstacle_set_height_m2130956669 (NavMeshObstacle_t3118413461 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.NavMeshObstacle::get_radius()
extern "C"  float NavMeshObstacle_get_radius_m1554170265 (NavMeshObstacle_t3118413461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshObstacle::set_radius(System.Single)
extern "C"  void NavMeshObstacle_set_radius_m441733810 (NavMeshObstacle_t3118413461 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.NavMeshObstacle::get_velocity()
extern "C"  Vector3_t4282066566  NavMeshObstacle_get_velocity_m558835062 (NavMeshObstacle_t3118413461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshObstacle::set_velocity(UnityEngine.Vector3)
extern "C"  void NavMeshObstacle_set_velocity_m2555065781 (NavMeshObstacle_t3118413461 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshObstacle::INTERNAL_get_velocity(UnityEngine.Vector3&)
extern "C"  void NavMeshObstacle_INTERNAL_get_velocity_m272438547 (NavMeshObstacle_t3118413461 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshObstacle::INTERNAL_set_velocity(UnityEngine.Vector3&)
extern "C"  void NavMeshObstacle_INTERNAL_set_velocity_m3988407943 (NavMeshObstacle_t3118413461 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshObstacle::get_carving()
extern "C"  bool NavMeshObstacle_get_carving_m2995115541 (NavMeshObstacle_t3118413461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshObstacle::set_carving(System.Boolean)
extern "C"  void NavMeshObstacle_set_carving_m1585381106 (NavMeshObstacle_t3118413461 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.NavMeshObstacle::get_carveOnlyStationary()
extern "C"  bool NavMeshObstacle_get_carveOnlyStationary_m2520711096 (NavMeshObstacle_t3118413461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshObstacle::set_carveOnlyStationary(System.Boolean)
extern "C"  void NavMeshObstacle_set_carveOnlyStationary_m2292844117 (NavMeshObstacle_t3118413461 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.NavMeshObstacle::get_carvingMoveThreshold()
extern "C"  float NavMeshObstacle_get_carvingMoveThreshold_m3450645121 (NavMeshObstacle_t3118413461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshObstacle::set_carvingMoveThreshold(System.Single)
extern "C"  void NavMeshObstacle_set_carvingMoveThreshold_m319988426 (NavMeshObstacle_t3118413461 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.NavMeshObstacle::get_carvingTimeToStationary()
extern "C"  float NavMeshObstacle_get_carvingTimeToStationary_m2021464279 (NavMeshObstacle_t3118413461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshObstacle::set_carvingTimeToStationary(System.Single)
extern "C"  void NavMeshObstacle_set_carvingTimeToStationary_m4172188724 (NavMeshObstacle_t3118413461 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.NavMeshObstacleShape UnityEngine.NavMeshObstacle::get_shape()
extern "C"  int32_t NavMeshObstacle_get_shape_m1333147814 (NavMeshObstacle_t3118413461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshObstacle::set_shape(UnityEngine.NavMeshObstacleShape)
extern "C"  void NavMeshObstacle_set_shape_m876776963 (NavMeshObstacle_t3118413461 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.NavMeshObstacle::get_center()
extern "C"  Vector3_t4282066566  NavMeshObstacle_get_center_m437357806 (NavMeshObstacle_t3118413461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshObstacle::set_center(UnityEngine.Vector3)
extern "C"  void NavMeshObstacle_set_center_m982353853 (NavMeshObstacle_t3118413461 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshObstacle::INTERNAL_get_center(UnityEngine.Vector3&)
extern "C"  void NavMeshObstacle_INTERNAL_get_center_m3877006091 (NavMeshObstacle_t3118413461 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshObstacle::INTERNAL_set_center(UnityEngine.Vector3&)
extern "C"  void NavMeshObstacle_INTERNAL_set_center_m1230596479 (NavMeshObstacle_t3118413461 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.NavMeshObstacle::get_size()
extern "C"  Vector3_t4282066566  NavMeshObstacle_get_size_m4064787354 (NavMeshObstacle_t3118413461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshObstacle::set_size(UnityEngine.Vector3)
extern "C"  void NavMeshObstacle_set_size_m188571153 (NavMeshObstacle_t3118413461 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshObstacle::INTERNAL_get_size(UnityEngine.Vector3&)
extern "C"  void NavMeshObstacle_INTERNAL_get_size_m1616938551 (NavMeshObstacle_t3118413461 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.NavMeshObstacle::INTERNAL_set_size(UnityEngine.Vector3&)
extern "C"  void NavMeshObstacle_INTERNAL_set_size_m1752732075 (NavMeshObstacle_t3118413461 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
