﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.FightWaitBvr
struct  FightWaitBvr_t948483225  : public IBehavior_t770859129
{
public:
	// System.Single Entity.Behavior.FightWaitBvr::curTime
	float ___curTime_2;
	// System.Single Entity.Behavior.FightWaitBvr::waitTime
	float ___waitTime_3;

public:
	inline static int32_t get_offset_of_curTime_2() { return static_cast<int32_t>(offsetof(FightWaitBvr_t948483225, ___curTime_2)); }
	inline float get_curTime_2() const { return ___curTime_2; }
	inline float* get_address_of_curTime_2() { return &___curTime_2; }
	inline void set_curTime_2(float value)
	{
		___curTime_2 = value;
	}

	inline static int32_t get_offset_of_waitTime_3() { return static_cast<int32_t>(offsetof(FightWaitBvr_t948483225, ___waitTime_3)); }
	inline float get_waitTime_3() const { return ___waitTime_3; }
	inline float* get_address_of_waitTime_3() { return &___waitTime_3; }
	inline void set_waitTime_3(float value)
	{
		___waitTime_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
