﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_sabidiTosair31
struct  M_sabidiTosair31_t2924213914  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_sabidiTosair31::_vainairrow
	int32_t ____vainairrow_0;
	// System.Int32 GarbageiOS.M_sabidiTosair31::_jehel
	int32_t ____jehel_1;
	// System.Boolean GarbageiOS.M_sabidiTosair31::_jaiforKistebere
	bool ____jaiforKistebere_2;
	// System.Int32 GarbageiOS.M_sabidiTosair31::_teakee
	int32_t ____teakee_3;
	// System.String GarbageiOS.M_sabidiTosair31::_varge
	String_t* ____varge_4;
	// System.UInt32 GarbageiOS.M_sabidiTosair31::_nira
	uint32_t ____nira_5;
	// System.Boolean GarbageiOS.M_sabidiTosair31::_jouniSuchokoo
	bool ____jouniSuchokoo_6;
	// System.Boolean GarbageiOS.M_sabidiTosair31::_nemou
	bool ____nemou_7;
	// System.Single GarbageiOS.M_sabidiTosair31::_sallsece
	float ____sallsece_8;
	// System.Boolean GarbageiOS.M_sabidiTosair31::_huqisChecisjem
	bool ____huqisChecisjem_9;
	// System.String GarbageiOS.M_sabidiTosair31::_taitereTedoubir
	String_t* ____taitereTedoubir_10;
	// System.UInt32 GarbageiOS.M_sabidiTosair31::_deartordeLuherenair
	uint32_t ____deartordeLuherenair_11;
	// System.Boolean GarbageiOS.M_sabidiTosair31::_lanerear
	bool ____lanerear_12;
	// System.UInt32 GarbageiOS.M_sabidiTosair31::_wayra
	uint32_t ____wayra_13;
	// System.Boolean GarbageiOS.M_sabidiTosair31::_sajairDresas
	bool ____sajairDresas_14;
	// System.UInt32 GarbageiOS.M_sabidiTosair31::_kajiwouRaba
	uint32_t ____kajiwouRaba_15;
	// System.String GarbageiOS.M_sabidiTosair31::_sawwisaiGemlallnair
	String_t* ____sawwisaiGemlallnair_16;
	// System.String GarbageiOS.M_sabidiTosair31::_saywewhawLesaw
	String_t* ____saywewhawLesaw_17;
	// System.Boolean GarbageiOS.M_sabidiTosair31::_vetee
	bool ____vetee_18;

public:
	inline static int32_t get_offset_of__vainairrow_0() { return static_cast<int32_t>(offsetof(M_sabidiTosair31_t2924213914, ____vainairrow_0)); }
	inline int32_t get__vainairrow_0() const { return ____vainairrow_0; }
	inline int32_t* get_address_of__vainairrow_0() { return &____vainairrow_0; }
	inline void set__vainairrow_0(int32_t value)
	{
		____vainairrow_0 = value;
	}

	inline static int32_t get_offset_of__jehel_1() { return static_cast<int32_t>(offsetof(M_sabidiTosair31_t2924213914, ____jehel_1)); }
	inline int32_t get__jehel_1() const { return ____jehel_1; }
	inline int32_t* get_address_of__jehel_1() { return &____jehel_1; }
	inline void set__jehel_1(int32_t value)
	{
		____jehel_1 = value;
	}

	inline static int32_t get_offset_of__jaiforKistebere_2() { return static_cast<int32_t>(offsetof(M_sabidiTosair31_t2924213914, ____jaiforKistebere_2)); }
	inline bool get__jaiforKistebere_2() const { return ____jaiforKistebere_2; }
	inline bool* get_address_of__jaiforKistebere_2() { return &____jaiforKistebere_2; }
	inline void set__jaiforKistebere_2(bool value)
	{
		____jaiforKistebere_2 = value;
	}

	inline static int32_t get_offset_of__teakee_3() { return static_cast<int32_t>(offsetof(M_sabidiTosair31_t2924213914, ____teakee_3)); }
	inline int32_t get__teakee_3() const { return ____teakee_3; }
	inline int32_t* get_address_of__teakee_3() { return &____teakee_3; }
	inline void set__teakee_3(int32_t value)
	{
		____teakee_3 = value;
	}

	inline static int32_t get_offset_of__varge_4() { return static_cast<int32_t>(offsetof(M_sabidiTosair31_t2924213914, ____varge_4)); }
	inline String_t* get__varge_4() const { return ____varge_4; }
	inline String_t** get_address_of__varge_4() { return &____varge_4; }
	inline void set__varge_4(String_t* value)
	{
		____varge_4 = value;
		Il2CppCodeGenWriteBarrier(&____varge_4, value);
	}

	inline static int32_t get_offset_of__nira_5() { return static_cast<int32_t>(offsetof(M_sabidiTosair31_t2924213914, ____nira_5)); }
	inline uint32_t get__nira_5() const { return ____nira_5; }
	inline uint32_t* get_address_of__nira_5() { return &____nira_5; }
	inline void set__nira_5(uint32_t value)
	{
		____nira_5 = value;
	}

	inline static int32_t get_offset_of__jouniSuchokoo_6() { return static_cast<int32_t>(offsetof(M_sabidiTosair31_t2924213914, ____jouniSuchokoo_6)); }
	inline bool get__jouniSuchokoo_6() const { return ____jouniSuchokoo_6; }
	inline bool* get_address_of__jouniSuchokoo_6() { return &____jouniSuchokoo_6; }
	inline void set__jouniSuchokoo_6(bool value)
	{
		____jouniSuchokoo_6 = value;
	}

	inline static int32_t get_offset_of__nemou_7() { return static_cast<int32_t>(offsetof(M_sabidiTosair31_t2924213914, ____nemou_7)); }
	inline bool get__nemou_7() const { return ____nemou_7; }
	inline bool* get_address_of__nemou_7() { return &____nemou_7; }
	inline void set__nemou_7(bool value)
	{
		____nemou_7 = value;
	}

	inline static int32_t get_offset_of__sallsece_8() { return static_cast<int32_t>(offsetof(M_sabidiTosair31_t2924213914, ____sallsece_8)); }
	inline float get__sallsece_8() const { return ____sallsece_8; }
	inline float* get_address_of__sallsece_8() { return &____sallsece_8; }
	inline void set__sallsece_8(float value)
	{
		____sallsece_8 = value;
	}

	inline static int32_t get_offset_of__huqisChecisjem_9() { return static_cast<int32_t>(offsetof(M_sabidiTosair31_t2924213914, ____huqisChecisjem_9)); }
	inline bool get__huqisChecisjem_9() const { return ____huqisChecisjem_9; }
	inline bool* get_address_of__huqisChecisjem_9() { return &____huqisChecisjem_9; }
	inline void set__huqisChecisjem_9(bool value)
	{
		____huqisChecisjem_9 = value;
	}

	inline static int32_t get_offset_of__taitereTedoubir_10() { return static_cast<int32_t>(offsetof(M_sabidiTosair31_t2924213914, ____taitereTedoubir_10)); }
	inline String_t* get__taitereTedoubir_10() const { return ____taitereTedoubir_10; }
	inline String_t** get_address_of__taitereTedoubir_10() { return &____taitereTedoubir_10; }
	inline void set__taitereTedoubir_10(String_t* value)
	{
		____taitereTedoubir_10 = value;
		Il2CppCodeGenWriteBarrier(&____taitereTedoubir_10, value);
	}

	inline static int32_t get_offset_of__deartordeLuherenair_11() { return static_cast<int32_t>(offsetof(M_sabidiTosair31_t2924213914, ____deartordeLuherenair_11)); }
	inline uint32_t get__deartordeLuherenair_11() const { return ____deartordeLuherenair_11; }
	inline uint32_t* get_address_of__deartordeLuherenair_11() { return &____deartordeLuherenair_11; }
	inline void set__deartordeLuherenair_11(uint32_t value)
	{
		____deartordeLuherenair_11 = value;
	}

	inline static int32_t get_offset_of__lanerear_12() { return static_cast<int32_t>(offsetof(M_sabidiTosair31_t2924213914, ____lanerear_12)); }
	inline bool get__lanerear_12() const { return ____lanerear_12; }
	inline bool* get_address_of__lanerear_12() { return &____lanerear_12; }
	inline void set__lanerear_12(bool value)
	{
		____lanerear_12 = value;
	}

	inline static int32_t get_offset_of__wayra_13() { return static_cast<int32_t>(offsetof(M_sabidiTosair31_t2924213914, ____wayra_13)); }
	inline uint32_t get__wayra_13() const { return ____wayra_13; }
	inline uint32_t* get_address_of__wayra_13() { return &____wayra_13; }
	inline void set__wayra_13(uint32_t value)
	{
		____wayra_13 = value;
	}

	inline static int32_t get_offset_of__sajairDresas_14() { return static_cast<int32_t>(offsetof(M_sabidiTosair31_t2924213914, ____sajairDresas_14)); }
	inline bool get__sajairDresas_14() const { return ____sajairDresas_14; }
	inline bool* get_address_of__sajairDresas_14() { return &____sajairDresas_14; }
	inline void set__sajairDresas_14(bool value)
	{
		____sajairDresas_14 = value;
	}

	inline static int32_t get_offset_of__kajiwouRaba_15() { return static_cast<int32_t>(offsetof(M_sabidiTosair31_t2924213914, ____kajiwouRaba_15)); }
	inline uint32_t get__kajiwouRaba_15() const { return ____kajiwouRaba_15; }
	inline uint32_t* get_address_of__kajiwouRaba_15() { return &____kajiwouRaba_15; }
	inline void set__kajiwouRaba_15(uint32_t value)
	{
		____kajiwouRaba_15 = value;
	}

	inline static int32_t get_offset_of__sawwisaiGemlallnair_16() { return static_cast<int32_t>(offsetof(M_sabidiTosair31_t2924213914, ____sawwisaiGemlallnair_16)); }
	inline String_t* get__sawwisaiGemlallnair_16() const { return ____sawwisaiGemlallnair_16; }
	inline String_t** get_address_of__sawwisaiGemlallnair_16() { return &____sawwisaiGemlallnair_16; }
	inline void set__sawwisaiGemlallnair_16(String_t* value)
	{
		____sawwisaiGemlallnair_16 = value;
		Il2CppCodeGenWriteBarrier(&____sawwisaiGemlallnair_16, value);
	}

	inline static int32_t get_offset_of__saywewhawLesaw_17() { return static_cast<int32_t>(offsetof(M_sabidiTosair31_t2924213914, ____saywewhawLesaw_17)); }
	inline String_t* get__saywewhawLesaw_17() const { return ____saywewhawLesaw_17; }
	inline String_t** get_address_of__saywewhawLesaw_17() { return &____saywewhawLesaw_17; }
	inline void set__saywewhawLesaw_17(String_t* value)
	{
		____saywewhawLesaw_17 = value;
		Il2CppCodeGenWriteBarrier(&____saywewhawLesaw_17, value);
	}

	inline static int32_t get_offset_of__vetee_18() { return static_cast<int32_t>(offsetof(M_sabidiTosair31_t2924213914, ____vetee_18)); }
	inline bool get__vetee_18() const { return ____vetee_18; }
	inline bool* get_address_of__vetee_18() { return &____vetee_18; }
	inline void set__vetee_18(bool value)
	{
		____vetee_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
