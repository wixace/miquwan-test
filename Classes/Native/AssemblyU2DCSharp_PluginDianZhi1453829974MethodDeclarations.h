﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginDianZhi
struct PluginDianZhi_t1453829974;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.String
struct String_t;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// Mihua.SDK.CreatRoleInfo
struct CreatRoleInfo_t2858846543;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_PluginDianZhi1453829974.h"

// System.Void PluginDianZhi::.ctor()
extern "C"  void PluginDianZhi__ctor_m1626575573 (PluginDianZhi_t1453829974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDianZhi::Init()
extern "C"  void PluginDianZhi_Init_m2497858783 (PluginDianZhi_t1453829974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginDianZhi::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginDianZhi_ReqSDKHttpLogin_m1572611718 (PluginDianZhi_t1453829974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginDianZhi::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginDianZhi_IsLoginSuccess_m1515536886 (PluginDianZhi_t1453829974 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDianZhi::OpenUserLogin()
extern "C"  void PluginDianZhi_OpenUserLogin_m1542131751 (PluginDianZhi_t1453829974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDianZhi::OnLoginSuccess(System.String)
extern "C"  void PluginDianZhi_OnLoginSuccess_m2315029530 (PluginDianZhi_t1453829974 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDianZhi::OnLogoutSuccess(System.String)
extern "C"  void PluginDianZhi_OnLogoutSuccess_m3049981365 (PluginDianZhi_t1453829974 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDianZhi::ResLogoutSuccess(System.String)
extern "C"  void PluginDianZhi_ResLogoutSuccess_m1014813978 (PluginDianZhi_t1453829974 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDianZhi::UserPay(CEvent.ZEvent)
extern "C"  void PluginDianZhi_UserPay_m825514347 (PluginDianZhi_t1453829974 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDianZhi::OnPaySuccess(System.String)
extern "C"  void PluginDianZhi_OnPaySuccess_m3739954393 (PluginDianZhi_t1453829974 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDianZhi::OnPayFail(System.String)
extern "C"  void PluginDianZhi_OnPayFail_m3500153608 (PluginDianZhi_t1453829974 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDianZhi::OnEnterGame(CEvent.ZEvent)
extern "C"  void PluginDianZhi_OnEnterGame_m384116157 (PluginDianZhi_t1453829974 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDianZhi::OnCreatRole(CEvent.ZEvent)
extern "C"  void PluginDianZhi_OnCreatRole_m1213532008 (PluginDianZhi_t1453829974 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDianZhi::OnLevelUp(CEvent.ZEvent)
extern "C"  void PluginDianZhi_OnLevelUp_m1491543560 (PluginDianZhi_t1453829974 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDianZhi::ClollectData(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginDianZhi_ClollectData_m752103865 (PluginDianZhi_t1453829974 * __this, String_t* ___type0, String_t* ___serverid1, String_t* ___servername2, String_t* ___roleid3, String_t* ___rolename4, String_t* ___rolelevel5, String_t* ___roleGold6, String_t* ___VIPLv7, String_t* ___CreatRoleTime8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDianZhi::switchAcc()
extern "C"  void PluginDianZhi_switchAcc_m4188249216 (PluginDianZhi_t1453829974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDianZhi::login()
extern "C"  void PluginDianZhi_login_m1148590396 (PluginDianZhi_t1453829974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDianZhi::goZF(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginDianZhi_goZF_m4111736059 (PluginDianZhi_t1453829974 * __this, String_t* ___amount0, String_t* ___orderId1, String_t* ___extra2, String_t* ___tempId3, String_t* ___serverId4, String_t* ___serverName5, String_t* ___roleId6, String_t* ___roleName7, String_t* ___setProductNumber8, String_t* ___setProductName9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDianZhi::coloectData(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginDianZhi_coloectData_m3255218182 (PluginDianZhi_t1453829974 * __this, String_t* ___serverId0, String_t* ___servername1, String_t* ___roleid2, String_t* ___rolename3, String_t* ___rolelevel4, String_t* ___ctime5, String_t* ___type6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDianZhi::<ResLogoutSuccess>m__416()
extern "C"  void PluginDianZhi_U3CResLogoutSuccessU3Em__416_m3850580484 (PluginDianZhi_t1453829974 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDianZhi::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginDianZhi_ilo_AddEventListener1_m2196658047 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginDianZhi::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginDianZhi_ilo_get_Instance2_m957320177 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginDianZhi::ilo_get_isSdkLogin3(VersionMgr)
extern "C"  bool PluginDianZhi_ilo_get_isSdkLogin3_m3276403525 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginDianZhi::ilo_get_DeviceID4(PluginsSdkMgr)
extern "C"  String_t* PluginDianZhi_ilo_get_DeviceID4_m1761028471 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDianZhi::ilo_OpenUserLogin5(PluginDianZhi)
extern "C"  void PluginDianZhi_ilo_OpenUserLogin5_m2418134841 (Il2CppObject * __this /* static, unused */, PluginDianZhi_t1453829974 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDianZhi::ilo_ClollectData6(PluginDianZhi,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginDianZhi_ilo_ClollectData6_m3784873564 (Il2CppObject * __this /* static, unused */, PluginDianZhi_t1453829974 * ____this0, String_t* ___type1, String_t* ___serverid2, String_t* ___servername3, String_t* ___roleid4, String_t* ___rolename5, String_t* ___rolelevel6, String_t* ___roleGold7, String_t* ___VIPLv8, String_t* ___CreatRoleTime9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginDianZhi::ilo_get_ProductsCfgMgr7()
extern "C"  ProductsCfgMgr_t2493714872 * PluginDianZhi_ilo_get_ProductsCfgMgr7_m3495514523 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginDianZhi::ilo_goZF8(PluginDianZhi,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginDianZhi_ilo_goZF8_m968105936 (Il2CppObject * __this /* static, unused */, PluginDianZhi_t1453829974 * ____this0, String_t* ___amount1, String_t* ___orderId2, String_t* ___extra3, String_t* ___tempId4, String_t* ___serverId5, String_t* ___serverName6, String_t* ___roleId7, String_t* ___roleName8, String_t* ___setProductNumber9, String_t* ___setProductName10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.CreatRoleInfo PluginDianZhi::ilo_Parse9(System.Object[])
extern "C"  CreatRoleInfo_t2858846543 * PluginDianZhi_ilo_Parse9_m536099578 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
