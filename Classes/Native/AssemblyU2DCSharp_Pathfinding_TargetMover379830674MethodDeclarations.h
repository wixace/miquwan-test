﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.TargetMover
struct TargetMover_t379830674;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_TargetMover379830674.h"

// System.Void Pathfinding.TargetMover::.ctor()
extern "C"  void TargetMover__ctor_m3696671205 (TargetMover_t379830674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TargetMover::Start()
extern "C"  void TargetMover_Start_m2643808997 (TargetMover_t379830674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TargetMover::OnGUI()
extern "C"  void TargetMover_OnGUI_m3192069855 (TargetMover_t379830674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TargetMover::Update()
extern "C"  void TargetMover_Update_m359552456 (TargetMover_t379830674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TargetMover::UpdateTargetPosition()
extern "C"  void TargetMover_UpdateTargetPosition_m1292333314 (TargetMover_t379830674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TargetMover::ilo_UpdateTargetPosition1(Pathfinding.TargetMover)
extern "C"  void TargetMover_ilo_UpdateTargetPosition1_m136813742 (Il2CppObject * __this /* static, unused */, TargetMover_t379830674 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
