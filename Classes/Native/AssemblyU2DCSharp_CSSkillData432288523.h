﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// skillCfg
struct skillCfg_t2142425171;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSSkillData
struct  CSSkillData_t432288523  : public Il2CppObject
{
public:
	// System.UInt32 CSSkillData::id
	uint32_t ___id_0;
	// System.UInt32 CSSkillData::level
	uint32_t ___level_1;
	// System.UInt32 CSSkillData::state
	uint32_t ___state_2;
	// System.Int32 CSSkillData::upLvID
	int32_t ___upLvID_3;
	// System.Boolean CSSkillData::noUpgrade
	bool ___noUpgrade_4;
	// skillCfg CSSkillData::cfg
	skillCfg_t2142425171 * ___cfg_5;
	// System.Boolean CSSkillData::isAwaken
	bool ___isAwaken_6;
	// System.UInt32 CSSkillData::awakenSkillID
	uint32_t ___awakenSkillID_7;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(CSSkillData_t432288523, ___id_0)); }
	inline uint32_t get_id_0() const { return ___id_0; }
	inline uint32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(uint32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_level_1() { return static_cast<int32_t>(offsetof(CSSkillData_t432288523, ___level_1)); }
	inline uint32_t get_level_1() const { return ___level_1; }
	inline uint32_t* get_address_of_level_1() { return &___level_1; }
	inline void set_level_1(uint32_t value)
	{
		___level_1 = value;
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(CSSkillData_t432288523, ___state_2)); }
	inline uint32_t get_state_2() const { return ___state_2; }
	inline uint32_t* get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(uint32_t value)
	{
		___state_2 = value;
	}

	inline static int32_t get_offset_of_upLvID_3() { return static_cast<int32_t>(offsetof(CSSkillData_t432288523, ___upLvID_3)); }
	inline int32_t get_upLvID_3() const { return ___upLvID_3; }
	inline int32_t* get_address_of_upLvID_3() { return &___upLvID_3; }
	inline void set_upLvID_3(int32_t value)
	{
		___upLvID_3 = value;
	}

	inline static int32_t get_offset_of_noUpgrade_4() { return static_cast<int32_t>(offsetof(CSSkillData_t432288523, ___noUpgrade_4)); }
	inline bool get_noUpgrade_4() const { return ___noUpgrade_4; }
	inline bool* get_address_of_noUpgrade_4() { return &___noUpgrade_4; }
	inline void set_noUpgrade_4(bool value)
	{
		___noUpgrade_4 = value;
	}

	inline static int32_t get_offset_of_cfg_5() { return static_cast<int32_t>(offsetof(CSSkillData_t432288523, ___cfg_5)); }
	inline skillCfg_t2142425171 * get_cfg_5() const { return ___cfg_5; }
	inline skillCfg_t2142425171 ** get_address_of_cfg_5() { return &___cfg_5; }
	inline void set_cfg_5(skillCfg_t2142425171 * value)
	{
		___cfg_5 = value;
		Il2CppCodeGenWriteBarrier(&___cfg_5, value);
	}

	inline static int32_t get_offset_of_isAwaken_6() { return static_cast<int32_t>(offsetof(CSSkillData_t432288523, ___isAwaken_6)); }
	inline bool get_isAwaken_6() const { return ___isAwaken_6; }
	inline bool* get_address_of_isAwaken_6() { return &___isAwaken_6; }
	inline void set_isAwaken_6(bool value)
	{
		___isAwaken_6 = value;
	}

	inline static int32_t get_offset_of_awakenSkillID_7() { return static_cast<int32_t>(offsetof(CSSkillData_t432288523, ___awakenSkillID_7)); }
	inline uint32_t get_awakenSkillID_7() const { return ___awakenSkillID_7; }
	inline uint32_t* get_address_of_awakenSkillID_7() { return &___awakenSkillID_7; }
	inline void set_awakenSkillID_7(uint32_t value)
	{
		___awakenSkillID_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
