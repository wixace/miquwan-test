﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Experimental.Networking.UnityWebRequest
struct UnityWebRequest_t327863158;
// System.Object
struct Il2CppObject;
// System.Action`1<System.Object>
struct Action_1_t271665211;
// System.Action
struct Action_t3771233898;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebUtil/<GetDataFromBmob>c__Iterator1`1<System.Object>
struct  U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690  : public Il2CppObject
{
public:
	// System.String WebUtil/<GetDataFromBmob>c__Iterator1`1::url
	String_t* ___url_0;
	// UnityEngine.Experimental.Networking.UnityWebRequest WebUtil/<GetDataFromBmob>c__Iterator1`1::<www>__0
	UnityWebRequest_t327863158 * ___U3CwwwU3E__0_1;
	// System.String WebUtil/<GetDataFromBmob>c__Iterator1`1::appId
	String_t* ___appId_2;
	// System.String WebUtil/<GetDataFromBmob>c__Iterator1`1::restApiKey
	String_t* ___restApiKey_3;
	// T WebUtil/<GetDataFromBmob>c__Iterator1`1::<data>__1
	Il2CppObject * ___U3CdataU3E__1_4;
	// System.Action`1<T> WebUtil/<GetDataFromBmob>c__Iterator1`1::success
	Action_1_t271665211 * ___success_5;
	// System.Action WebUtil/<GetDataFromBmob>c__Iterator1`1::failure
	Action_t3771233898 * ___failure_6;
	// System.Int32 WebUtil/<GetDataFromBmob>c__Iterator1`1::$PC
	int32_t ___U24PC_7;
	// System.Object WebUtil/<GetDataFromBmob>c__Iterator1`1::$current
	Il2CppObject * ___U24current_8;
	// System.String WebUtil/<GetDataFromBmob>c__Iterator1`1::<$>url
	String_t* ___U3CU24U3Eurl_9;
	// System.String WebUtil/<GetDataFromBmob>c__Iterator1`1::<$>appId
	String_t* ___U3CU24U3EappId_10;
	// System.String WebUtil/<GetDataFromBmob>c__Iterator1`1::<$>restApiKey
	String_t* ___U3CU24U3ErestApiKey_11;
	// System.Action`1<T> WebUtil/<GetDataFromBmob>c__Iterator1`1::<$>success
	Action_1_t271665211 * ___U3CU24U3Esuccess_12;
	// System.Action WebUtil/<GetDataFromBmob>c__Iterator1`1::<$>failure
	Action_t3771233898 * ___U3CU24U3Efailure_13;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690, ___U3CwwwU3E__0_1)); }
	inline UnityWebRequest_t327863158 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline UnityWebRequest_t327863158 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(UnityWebRequest_t327863158 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_1, value);
	}

	inline static int32_t get_offset_of_appId_2() { return static_cast<int32_t>(offsetof(U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690, ___appId_2)); }
	inline String_t* get_appId_2() const { return ___appId_2; }
	inline String_t** get_address_of_appId_2() { return &___appId_2; }
	inline void set_appId_2(String_t* value)
	{
		___appId_2 = value;
		Il2CppCodeGenWriteBarrier(&___appId_2, value);
	}

	inline static int32_t get_offset_of_restApiKey_3() { return static_cast<int32_t>(offsetof(U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690, ___restApiKey_3)); }
	inline String_t* get_restApiKey_3() const { return ___restApiKey_3; }
	inline String_t** get_address_of_restApiKey_3() { return &___restApiKey_3; }
	inline void set_restApiKey_3(String_t* value)
	{
		___restApiKey_3 = value;
		Il2CppCodeGenWriteBarrier(&___restApiKey_3, value);
	}

	inline static int32_t get_offset_of_U3CdataU3E__1_4() { return static_cast<int32_t>(offsetof(U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690, ___U3CdataU3E__1_4)); }
	inline Il2CppObject * get_U3CdataU3E__1_4() const { return ___U3CdataU3E__1_4; }
	inline Il2CppObject ** get_address_of_U3CdataU3E__1_4() { return &___U3CdataU3E__1_4; }
	inline void set_U3CdataU3E__1_4(Il2CppObject * value)
	{
		___U3CdataU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CdataU3E__1_4, value);
	}

	inline static int32_t get_offset_of_success_5() { return static_cast<int32_t>(offsetof(U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690, ___success_5)); }
	inline Action_1_t271665211 * get_success_5() const { return ___success_5; }
	inline Action_1_t271665211 ** get_address_of_success_5() { return &___success_5; }
	inline void set_success_5(Action_1_t271665211 * value)
	{
		___success_5 = value;
		Il2CppCodeGenWriteBarrier(&___success_5, value);
	}

	inline static int32_t get_offset_of_failure_6() { return static_cast<int32_t>(offsetof(U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690, ___failure_6)); }
	inline Action_t3771233898 * get_failure_6() const { return ___failure_6; }
	inline Action_t3771233898 ** get_address_of_failure_6() { return &___failure_6; }
	inline void set_failure_6(Action_t3771233898 * value)
	{
		___failure_6 = value;
		Il2CppCodeGenWriteBarrier(&___failure_6, value);
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}

	inline static int32_t get_offset_of_U24current_8() { return static_cast<int32_t>(offsetof(U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690, ___U24current_8)); }
	inline Il2CppObject * get_U24current_8() const { return ___U24current_8; }
	inline Il2CppObject ** get_address_of_U24current_8() { return &___U24current_8; }
	inline void set_U24current_8(Il2CppObject * value)
	{
		___U24current_8 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_8, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eurl_9() { return static_cast<int32_t>(offsetof(U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690, ___U3CU24U3Eurl_9)); }
	inline String_t* get_U3CU24U3Eurl_9() const { return ___U3CU24U3Eurl_9; }
	inline String_t** get_address_of_U3CU24U3Eurl_9() { return &___U3CU24U3Eurl_9; }
	inline void set_U3CU24U3Eurl_9(String_t* value)
	{
		___U3CU24U3Eurl_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eurl_9, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EappId_10() { return static_cast<int32_t>(offsetof(U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690, ___U3CU24U3EappId_10)); }
	inline String_t* get_U3CU24U3EappId_10() const { return ___U3CU24U3EappId_10; }
	inline String_t** get_address_of_U3CU24U3EappId_10() { return &___U3CU24U3EappId_10; }
	inline void set_U3CU24U3EappId_10(String_t* value)
	{
		___U3CU24U3EappId_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3EappId_10, value);
	}

	inline static int32_t get_offset_of_U3CU24U3ErestApiKey_11() { return static_cast<int32_t>(offsetof(U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690, ___U3CU24U3ErestApiKey_11)); }
	inline String_t* get_U3CU24U3ErestApiKey_11() const { return ___U3CU24U3ErestApiKey_11; }
	inline String_t** get_address_of_U3CU24U3ErestApiKey_11() { return &___U3CU24U3ErestApiKey_11; }
	inline void set_U3CU24U3ErestApiKey_11(String_t* value)
	{
		___U3CU24U3ErestApiKey_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3ErestApiKey_11, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Esuccess_12() { return static_cast<int32_t>(offsetof(U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690, ___U3CU24U3Esuccess_12)); }
	inline Action_1_t271665211 * get_U3CU24U3Esuccess_12() const { return ___U3CU24U3Esuccess_12; }
	inline Action_1_t271665211 ** get_address_of_U3CU24U3Esuccess_12() { return &___U3CU24U3Esuccess_12; }
	inline void set_U3CU24U3Esuccess_12(Action_1_t271665211 * value)
	{
		___U3CU24U3Esuccess_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Esuccess_12, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Efailure_13() { return static_cast<int32_t>(offsetof(U3CGetDataFromBmobU3Ec__Iterator1_1_t1159480690, ___U3CU24U3Efailure_13)); }
	inline Action_t3771233898 * get_U3CU24U3Efailure_13() const { return ___U3CU24U3Efailure_13; }
	inline Action_t3771233898 ** get_address_of_U3CU24U3Efailure_13() { return &___U3CU24U3Efailure_13; }
	inline void set_U3CU24U3Efailure_13(Action_t3771233898 * value)
	{
		___U3CU24U3Efailure_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Efailure_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
