﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_fearte56
struct M_fearte56_t2319210322;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_fearte56::.ctor()
extern "C"  void M_fearte56__ctor_m1194044193 (M_fearte56_t2319210322 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fearte56::M_malbijaCemze0(System.String[],System.Int32)
extern "C"  void M_fearte56_M_malbijaCemze0_m2480791444 (M_fearte56_t2319210322 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fearte56::M_rigawHehasall1(System.String[],System.Int32)
extern "C"  void M_fearte56_M_rigawHehasall1_m1973487123 (M_fearte56_t2319210322 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
