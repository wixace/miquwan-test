﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.DateTimeSerializer
struct DateTimeSerializer_t1929943847;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "mscorlib_System_DateTime4283661327.h"

// System.Void ProtoBuf.Serializers.DateTimeSerializer::.ctor(ProtoBuf.Meta.TypeModel)
extern "C"  void DateTimeSerializer__ctor_m1910614043 (DateTimeSerializer_t1929943847 * __this, TypeModel_t2730011105 * ___model0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.DateTimeSerializer::.cctor()
extern "C"  void DateTimeSerializer__cctor_m3878018527 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.DateTimeSerializer::ProtoBuf.Serializers.IProtoSerializer.get_RequiresOldValue()
extern "C"  bool DateTimeSerializer_ProtoBuf_Serializers_IProtoSerializer_get_RequiresOldValue_m2351852688 (DateTimeSerializer_t1929943847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.DateTimeSerializer::ProtoBuf.Serializers.IProtoSerializer.get_ReturnsValue()
extern "C"  bool DateTimeSerializer_ProtoBuf_Serializers_IProtoSerializer_get_ReturnsValue_m3367446310 (DateTimeSerializer_t1929943847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.DateTimeSerializer::get_ExpectedType()
extern "C"  Type_t * DateTimeSerializer_get_ExpectedType_m3935516343 (DateTimeSerializer_t1929943847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.DateTimeSerializer::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * DateTimeSerializer_Read_m1172407841 (DateTimeSerializer_t1929943847 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.DateTimeSerializer::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void DateTimeSerializer_Write_m3849274757 (DateTimeSerializer_t1929943847 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.DateTimeSerializer::ilo_WriteDateTime1(System.DateTime,ProtoBuf.ProtoWriter)
extern "C"  void DateTimeSerializer_ilo_WriteDateTime1_m4135782870 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
