﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_PuzzleManager_PropertyType901913455.h"
#include "AssemblyU2DCSharp_PuzzleManager_ResultType1561842103.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InputItem
struct  InputItem_t3710611933  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 InputItem::_basicPoint
	int32_t ____basicPoint_2;
	// System.Int32 InputItem::_propertyPoint
	int32_t ____propertyPoint_3;
	// System.Int32 InputItem::_totalPoint
	int32_t ____totalPoint_4;
	// System.Int32 InputItem::_dicePoint
	int32_t ____dicePoint_5;
	// System.Int32 InputItem::_thinkPoint
	int32_t ____thinkPoint_6;
	// UnityEngine.UI.Text InputItem::_basicPoinText
	Text_t9039225 * ____basicPoinText_7;
	// UnityEngine.UI.Text InputItem::_propertyPointText
	Text_t9039225 * ____propertyPointText_8;
	// UnityEngine.UI.Text InputItem::_propertyTypeText
	Text_t9039225 * ____propertyTypeText_9;
	// UnityEngine.UI.Text InputItem::_totalPointText
	Text_t9039225 * ____totalPointText_10;
	// UnityEngine.UI.Text InputItem::_dicePointText
	Text_t9039225 * ____dicePointText_11;
	// UnityEngine.UI.Text InputItem::_thinkPointText
	Text_t9039225 * ____thinkPointText_12;
	// PuzzleManager/PropertyType InputItem::propertyType
	int32_t ___propertyType_13;
	// UnityEngine.GameObject InputItem::_content
	GameObject_t3674682005 * ____content_14;
	// UnityEngine.GameObject InputItem::_mask
	GameObject_t3674682005 * ____mask_15;
	// System.Int32 InputItem::_id
	int32_t ____id_16;
	// System.Boolean InputItem::<IsOpened>k__BackingField
	bool ___U3CIsOpenedU3Ek__BackingField_17;
	// PuzzleManager/ResultType InputItem::<resultType>k__BackingField
	int32_t ___U3CresultTypeU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of__basicPoint_2() { return static_cast<int32_t>(offsetof(InputItem_t3710611933, ____basicPoint_2)); }
	inline int32_t get__basicPoint_2() const { return ____basicPoint_2; }
	inline int32_t* get_address_of__basicPoint_2() { return &____basicPoint_2; }
	inline void set__basicPoint_2(int32_t value)
	{
		____basicPoint_2 = value;
	}

	inline static int32_t get_offset_of__propertyPoint_3() { return static_cast<int32_t>(offsetof(InputItem_t3710611933, ____propertyPoint_3)); }
	inline int32_t get__propertyPoint_3() const { return ____propertyPoint_3; }
	inline int32_t* get_address_of__propertyPoint_3() { return &____propertyPoint_3; }
	inline void set__propertyPoint_3(int32_t value)
	{
		____propertyPoint_3 = value;
	}

	inline static int32_t get_offset_of__totalPoint_4() { return static_cast<int32_t>(offsetof(InputItem_t3710611933, ____totalPoint_4)); }
	inline int32_t get__totalPoint_4() const { return ____totalPoint_4; }
	inline int32_t* get_address_of__totalPoint_4() { return &____totalPoint_4; }
	inline void set__totalPoint_4(int32_t value)
	{
		____totalPoint_4 = value;
	}

	inline static int32_t get_offset_of__dicePoint_5() { return static_cast<int32_t>(offsetof(InputItem_t3710611933, ____dicePoint_5)); }
	inline int32_t get__dicePoint_5() const { return ____dicePoint_5; }
	inline int32_t* get_address_of__dicePoint_5() { return &____dicePoint_5; }
	inline void set__dicePoint_5(int32_t value)
	{
		____dicePoint_5 = value;
	}

	inline static int32_t get_offset_of__thinkPoint_6() { return static_cast<int32_t>(offsetof(InputItem_t3710611933, ____thinkPoint_6)); }
	inline int32_t get__thinkPoint_6() const { return ____thinkPoint_6; }
	inline int32_t* get_address_of__thinkPoint_6() { return &____thinkPoint_6; }
	inline void set__thinkPoint_6(int32_t value)
	{
		____thinkPoint_6 = value;
	}

	inline static int32_t get_offset_of__basicPoinText_7() { return static_cast<int32_t>(offsetof(InputItem_t3710611933, ____basicPoinText_7)); }
	inline Text_t9039225 * get__basicPoinText_7() const { return ____basicPoinText_7; }
	inline Text_t9039225 ** get_address_of__basicPoinText_7() { return &____basicPoinText_7; }
	inline void set__basicPoinText_7(Text_t9039225 * value)
	{
		____basicPoinText_7 = value;
		Il2CppCodeGenWriteBarrier(&____basicPoinText_7, value);
	}

	inline static int32_t get_offset_of__propertyPointText_8() { return static_cast<int32_t>(offsetof(InputItem_t3710611933, ____propertyPointText_8)); }
	inline Text_t9039225 * get__propertyPointText_8() const { return ____propertyPointText_8; }
	inline Text_t9039225 ** get_address_of__propertyPointText_8() { return &____propertyPointText_8; }
	inline void set__propertyPointText_8(Text_t9039225 * value)
	{
		____propertyPointText_8 = value;
		Il2CppCodeGenWriteBarrier(&____propertyPointText_8, value);
	}

	inline static int32_t get_offset_of__propertyTypeText_9() { return static_cast<int32_t>(offsetof(InputItem_t3710611933, ____propertyTypeText_9)); }
	inline Text_t9039225 * get__propertyTypeText_9() const { return ____propertyTypeText_9; }
	inline Text_t9039225 ** get_address_of__propertyTypeText_9() { return &____propertyTypeText_9; }
	inline void set__propertyTypeText_9(Text_t9039225 * value)
	{
		____propertyTypeText_9 = value;
		Il2CppCodeGenWriteBarrier(&____propertyTypeText_9, value);
	}

	inline static int32_t get_offset_of__totalPointText_10() { return static_cast<int32_t>(offsetof(InputItem_t3710611933, ____totalPointText_10)); }
	inline Text_t9039225 * get__totalPointText_10() const { return ____totalPointText_10; }
	inline Text_t9039225 ** get_address_of__totalPointText_10() { return &____totalPointText_10; }
	inline void set__totalPointText_10(Text_t9039225 * value)
	{
		____totalPointText_10 = value;
		Il2CppCodeGenWriteBarrier(&____totalPointText_10, value);
	}

	inline static int32_t get_offset_of__dicePointText_11() { return static_cast<int32_t>(offsetof(InputItem_t3710611933, ____dicePointText_11)); }
	inline Text_t9039225 * get__dicePointText_11() const { return ____dicePointText_11; }
	inline Text_t9039225 ** get_address_of__dicePointText_11() { return &____dicePointText_11; }
	inline void set__dicePointText_11(Text_t9039225 * value)
	{
		____dicePointText_11 = value;
		Il2CppCodeGenWriteBarrier(&____dicePointText_11, value);
	}

	inline static int32_t get_offset_of__thinkPointText_12() { return static_cast<int32_t>(offsetof(InputItem_t3710611933, ____thinkPointText_12)); }
	inline Text_t9039225 * get__thinkPointText_12() const { return ____thinkPointText_12; }
	inline Text_t9039225 ** get_address_of__thinkPointText_12() { return &____thinkPointText_12; }
	inline void set__thinkPointText_12(Text_t9039225 * value)
	{
		____thinkPointText_12 = value;
		Il2CppCodeGenWriteBarrier(&____thinkPointText_12, value);
	}

	inline static int32_t get_offset_of_propertyType_13() { return static_cast<int32_t>(offsetof(InputItem_t3710611933, ___propertyType_13)); }
	inline int32_t get_propertyType_13() const { return ___propertyType_13; }
	inline int32_t* get_address_of_propertyType_13() { return &___propertyType_13; }
	inline void set_propertyType_13(int32_t value)
	{
		___propertyType_13 = value;
	}

	inline static int32_t get_offset_of__content_14() { return static_cast<int32_t>(offsetof(InputItem_t3710611933, ____content_14)); }
	inline GameObject_t3674682005 * get__content_14() const { return ____content_14; }
	inline GameObject_t3674682005 ** get_address_of__content_14() { return &____content_14; }
	inline void set__content_14(GameObject_t3674682005 * value)
	{
		____content_14 = value;
		Il2CppCodeGenWriteBarrier(&____content_14, value);
	}

	inline static int32_t get_offset_of__mask_15() { return static_cast<int32_t>(offsetof(InputItem_t3710611933, ____mask_15)); }
	inline GameObject_t3674682005 * get__mask_15() const { return ____mask_15; }
	inline GameObject_t3674682005 ** get_address_of__mask_15() { return &____mask_15; }
	inline void set__mask_15(GameObject_t3674682005 * value)
	{
		____mask_15 = value;
		Il2CppCodeGenWriteBarrier(&____mask_15, value);
	}

	inline static int32_t get_offset_of__id_16() { return static_cast<int32_t>(offsetof(InputItem_t3710611933, ____id_16)); }
	inline int32_t get__id_16() const { return ____id_16; }
	inline int32_t* get_address_of__id_16() { return &____id_16; }
	inline void set__id_16(int32_t value)
	{
		____id_16 = value;
	}

	inline static int32_t get_offset_of_U3CIsOpenedU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(InputItem_t3710611933, ___U3CIsOpenedU3Ek__BackingField_17)); }
	inline bool get_U3CIsOpenedU3Ek__BackingField_17() const { return ___U3CIsOpenedU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CIsOpenedU3Ek__BackingField_17() { return &___U3CIsOpenedU3Ek__BackingField_17; }
	inline void set_U3CIsOpenedU3Ek__BackingField_17(bool value)
	{
		___U3CIsOpenedU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CresultTypeU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(InputItem_t3710611933, ___U3CresultTypeU3Ek__BackingField_18)); }
	inline int32_t get_U3CresultTypeU3Ek__BackingField_18() const { return ___U3CresultTypeU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CresultTypeU3Ek__BackingField_18() { return &___U3CresultTypeU3Ek__BackingField_18; }
	inline void set_U3CresultTypeU3Ek__BackingField_18(int32_t value)
	{
		___U3CresultTypeU3Ek__BackingField_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
