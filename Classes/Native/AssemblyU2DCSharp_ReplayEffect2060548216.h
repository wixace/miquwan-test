﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CombatEntity
struct CombatEntity_t684137495;

#include "AssemblyU2DCSharp_ReplayBase779703160.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReplayEffect
struct  ReplayEffect_t2060548216  : public ReplayBase_t779703160
{
public:
	// System.Int32 ReplayEffect::isPlayShoot
	int32_t ___isPlayShoot_8;
	// System.Int32 ReplayEffect::effectid
	int32_t ___effectid_9;
	// System.Int32 ReplayEffect::srcId
	int32_t ___srcId_10;
	// System.Int32 ReplayEffect::srcIsEnemy
	int32_t ___srcIsEnemy_11;
	// System.Single ReplayEffect::lifetime
	float ___lifetime_12;
	// System.Single ReplayEffect::posX
	float ___posX_13;
	// System.Single ReplayEffect::posY
	float ___posY_14;
	// System.Single ReplayEffect::posZ
	float ___posZ_15;
	// System.Int32 ReplayEffect::hostId
	int32_t ___hostId_16;
	// System.Int32 ReplayEffect::hostIsEnemy
	int32_t ___hostIsEnemy_17;
	// System.Single ReplayEffect::speed
	float ___speed_18;
	// System.Int32 ReplayEffect::isatk
	int32_t ___isatk_19;
	// System.Int32 ReplayEffect::tag
	int32_t ___tag_20;
	// System.Int32 ReplayEffect::skillID
	int32_t ___skillID_21;
	// CombatEntity ReplayEffect::_src
	CombatEntity_t684137495 * ____src_22;
	// CombatEntity ReplayEffect::_host
	CombatEntity_t684137495 * ____host_23;

public:
	inline static int32_t get_offset_of_isPlayShoot_8() { return static_cast<int32_t>(offsetof(ReplayEffect_t2060548216, ___isPlayShoot_8)); }
	inline int32_t get_isPlayShoot_8() const { return ___isPlayShoot_8; }
	inline int32_t* get_address_of_isPlayShoot_8() { return &___isPlayShoot_8; }
	inline void set_isPlayShoot_8(int32_t value)
	{
		___isPlayShoot_8 = value;
	}

	inline static int32_t get_offset_of_effectid_9() { return static_cast<int32_t>(offsetof(ReplayEffect_t2060548216, ___effectid_9)); }
	inline int32_t get_effectid_9() const { return ___effectid_9; }
	inline int32_t* get_address_of_effectid_9() { return &___effectid_9; }
	inline void set_effectid_9(int32_t value)
	{
		___effectid_9 = value;
	}

	inline static int32_t get_offset_of_srcId_10() { return static_cast<int32_t>(offsetof(ReplayEffect_t2060548216, ___srcId_10)); }
	inline int32_t get_srcId_10() const { return ___srcId_10; }
	inline int32_t* get_address_of_srcId_10() { return &___srcId_10; }
	inline void set_srcId_10(int32_t value)
	{
		___srcId_10 = value;
	}

	inline static int32_t get_offset_of_srcIsEnemy_11() { return static_cast<int32_t>(offsetof(ReplayEffect_t2060548216, ___srcIsEnemy_11)); }
	inline int32_t get_srcIsEnemy_11() const { return ___srcIsEnemy_11; }
	inline int32_t* get_address_of_srcIsEnemy_11() { return &___srcIsEnemy_11; }
	inline void set_srcIsEnemy_11(int32_t value)
	{
		___srcIsEnemy_11 = value;
	}

	inline static int32_t get_offset_of_lifetime_12() { return static_cast<int32_t>(offsetof(ReplayEffect_t2060548216, ___lifetime_12)); }
	inline float get_lifetime_12() const { return ___lifetime_12; }
	inline float* get_address_of_lifetime_12() { return &___lifetime_12; }
	inline void set_lifetime_12(float value)
	{
		___lifetime_12 = value;
	}

	inline static int32_t get_offset_of_posX_13() { return static_cast<int32_t>(offsetof(ReplayEffect_t2060548216, ___posX_13)); }
	inline float get_posX_13() const { return ___posX_13; }
	inline float* get_address_of_posX_13() { return &___posX_13; }
	inline void set_posX_13(float value)
	{
		___posX_13 = value;
	}

	inline static int32_t get_offset_of_posY_14() { return static_cast<int32_t>(offsetof(ReplayEffect_t2060548216, ___posY_14)); }
	inline float get_posY_14() const { return ___posY_14; }
	inline float* get_address_of_posY_14() { return &___posY_14; }
	inline void set_posY_14(float value)
	{
		___posY_14 = value;
	}

	inline static int32_t get_offset_of_posZ_15() { return static_cast<int32_t>(offsetof(ReplayEffect_t2060548216, ___posZ_15)); }
	inline float get_posZ_15() const { return ___posZ_15; }
	inline float* get_address_of_posZ_15() { return &___posZ_15; }
	inline void set_posZ_15(float value)
	{
		___posZ_15 = value;
	}

	inline static int32_t get_offset_of_hostId_16() { return static_cast<int32_t>(offsetof(ReplayEffect_t2060548216, ___hostId_16)); }
	inline int32_t get_hostId_16() const { return ___hostId_16; }
	inline int32_t* get_address_of_hostId_16() { return &___hostId_16; }
	inline void set_hostId_16(int32_t value)
	{
		___hostId_16 = value;
	}

	inline static int32_t get_offset_of_hostIsEnemy_17() { return static_cast<int32_t>(offsetof(ReplayEffect_t2060548216, ___hostIsEnemy_17)); }
	inline int32_t get_hostIsEnemy_17() const { return ___hostIsEnemy_17; }
	inline int32_t* get_address_of_hostIsEnemy_17() { return &___hostIsEnemy_17; }
	inline void set_hostIsEnemy_17(int32_t value)
	{
		___hostIsEnemy_17 = value;
	}

	inline static int32_t get_offset_of_speed_18() { return static_cast<int32_t>(offsetof(ReplayEffect_t2060548216, ___speed_18)); }
	inline float get_speed_18() const { return ___speed_18; }
	inline float* get_address_of_speed_18() { return &___speed_18; }
	inline void set_speed_18(float value)
	{
		___speed_18 = value;
	}

	inline static int32_t get_offset_of_isatk_19() { return static_cast<int32_t>(offsetof(ReplayEffect_t2060548216, ___isatk_19)); }
	inline int32_t get_isatk_19() const { return ___isatk_19; }
	inline int32_t* get_address_of_isatk_19() { return &___isatk_19; }
	inline void set_isatk_19(int32_t value)
	{
		___isatk_19 = value;
	}

	inline static int32_t get_offset_of_tag_20() { return static_cast<int32_t>(offsetof(ReplayEffect_t2060548216, ___tag_20)); }
	inline int32_t get_tag_20() const { return ___tag_20; }
	inline int32_t* get_address_of_tag_20() { return &___tag_20; }
	inline void set_tag_20(int32_t value)
	{
		___tag_20 = value;
	}

	inline static int32_t get_offset_of_skillID_21() { return static_cast<int32_t>(offsetof(ReplayEffect_t2060548216, ___skillID_21)); }
	inline int32_t get_skillID_21() const { return ___skillID_21; }
	inline int32_t* get_address_of_skillID_21() { return &___skillID_21; }
	inline void set_skillID_21(int32_t value)
	{
		___skillID_21 = value;
	}

	inline static int32_t get_offset_of__src_22() { return static_cast<int32_t>(offsetof(ReplayEffect_t2060548216, ____src_22)); }
	inline CombatEntity_t684137495 * get__src_22() const { return ____src_22; }
	inline CombatEntity_t684137495 ** get_address_of__src_22() { return &____src_22; }
	inline void set__src_22(CombatEntity_t684137495 * value)
	{
		____src_22 = value;
		Il2CppCodeGenWriteBarrier(&____src_22, value);
	}

	inline static int32_t get_offset_of__host_23() { return static_cast<int32_t>(offsetof(ReplayEffect_t2060548216, ____host_23)); }
	inline CombatEntity_t684137495 * get__host_23() const { return ____host_23; }
	inline CombatEntity_t684137495 ** get_address_of__host_23() { return &____host_23; }
	inline void set__host_23(CombatEntity_t684137495 * value)
	{
		____host_23 = value;
		Il2CppCodeGenWriteBarrier(&____host_23, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
