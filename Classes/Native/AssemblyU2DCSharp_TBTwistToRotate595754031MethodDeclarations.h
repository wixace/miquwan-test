﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TBTwistToRotate
struct TBTwistToRotate_t595754031;
// TwistGesture
struct TwistGesture_t4198361154;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_TwistGesture4198361154.h"
#include "AssemblyU2DCSharp_TBTwistToRotate595754031.h"

// System.Void TBTwistToRotate::.ctor()
extern "C"  void TBTwistToRotate__ctor_m3979120668 (TBTwistToRotate_t595754031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBTwistToRotate::Start()
extern "C"  void TBTwistToRotate_Start_m2926258460 (TBTwistToRotate_t595754031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TBTwistToRotate::GetRotationAxis()
extern "C"  Vector3_t4282066566  TBTwistToRotate_GetRotationAxis_m3955625577 (TBTwistToRotate_t595754031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBTwistToRotate::OnTwist(TwistGesture)
extern "C"  void TBTwistToRotate_OnTwist_m3725645824 (TBTwistToRotate_t595754031 * __this, TwistGesture_t4198361154 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TBTwistToRotate::ilo_get_DeltaRotation1(TwistGesture)
extern "C"  float TBTwistToRotate_ilo_get_DeltaRotation1_m2205315313 (Il2CppObject * __this /* static, unused */, TwistGesture_t4198361154 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TBTwistToRotate::ilo_GetRotationAxis2(TBTwistToRotate)
extern "C"  Vector3_t4282066566  TBTwistToRotate_ilo_GetRotationAxis2_m394743845 (Il2CppObject * __this /* static, unused */, TBTwistToRotate_t595754031 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
