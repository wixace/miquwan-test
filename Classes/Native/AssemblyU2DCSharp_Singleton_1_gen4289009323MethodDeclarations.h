﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Singleton_1_gen128664468MethodDeclarations.h"

// System.Void Singleton`1<ConfigAssetMgr>::.ctor()
#define Singleton_1__ctor_m3334170039(__this, method) ((  void (*) (Singleton_1_t4289009323 *, const MethodInfo*))Singleton_1__ctor_m3958676923_gshared)(__this, method)
// T Singleton`1<ConfigAssetMgr>::get_Instance()
#define Singleton_1_get_Instance_m2967915436(__this /* static, unused */, method) ((  ConfigAssetMgr_t4036193930 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_Instance_m1020946630_gshared)(__this /* static, unused */, method)
