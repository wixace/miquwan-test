﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_heroStar_step
struct Float_heroStar_step_t1144451324;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"

// System.Void Float_heroStar_step::.ctor()
extern "C"  void Float_heroStar_step__ctor_m218136047 (Float_heroStar_step_t1144451324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_heroStar_step::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_heroStar_step_OnAwake_m2713928875 (Float_heroStar_step_t1144451324 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_heroStar_step::FloatID()
extern "C"  int32_t Float_heroStar_step_FloatID_m2842560685 (Float_heroStar_step_t1144451324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_heroStar_step::Init()
extern "C"  void Float_heroStar_step_Init_m1482593925 (Float_heroStar_step_t1144451324 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_heroStar_step::SetFile(System.Object[])
extern "C"  void Float_heroStar_step_SetFile_m257416903 (Float_heroStar_step_t1144451324 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
