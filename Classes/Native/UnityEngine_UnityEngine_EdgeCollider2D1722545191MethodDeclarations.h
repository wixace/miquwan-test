﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.EdgeCollider2D
struct EdgeCollider2D_t1722545191;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.EdgeCollider2D::.ctor()
extern "C"  void EdgeCollider2D__ctor_m1213640298 (EdgeCollider2D_t1722545191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EdgeCollider2D::Reset()
extern "C"  void EdgeCollider2D_Reset_m3155040535 (EdgeCollider2D_t1722545191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.EdgeCollider2D::get_edgeCount()
extern "C"  int32_t EdgeCollider2D_get_edgeCount_m2720363921 (EdgeCollider2D_t1722545191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.EdgeCollider2D::get_pointCount()
extern "C"  int32_t EdgeCollider2D_get_pointCount_m1017457314 (EdgeCollider2D_t1722545191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] UnityEngine.EdgeCollider2D::get_points()
extern "C"  Vector2U5BU5D_t4024180168* EdgeCollider2D_get_points_m2958959967 (EdgeCollider2D_t1722545191 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EdgeCollider2D::set_points(UnityEngine.Vector2[])
extern "C"  void EdgeCollider2D_set_points_m1536201342 (EdgeCollider2D_t1722545191 * __this, Vector2U5BU5D_t4024180168* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
