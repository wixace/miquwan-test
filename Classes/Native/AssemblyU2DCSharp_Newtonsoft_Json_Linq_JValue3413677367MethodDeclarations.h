﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JValue
struct JValue_t3413677367;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// SerializableGuid
struct SerializableGuid_t3998617672;
// System.Uri
struct Uri_t1116831938;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// Newtonsoft.Json.JsonConverter[]
struct JsonConverterU5BU5D_t638349667;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t2159686854;
// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>
struct IList_1_t559366761;
// System.Type
struct Type_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType3916897561.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JValue3413677367.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Guid2862754429.h"
#include "AssemblyU2DCSharp_SerializableGuid3998617672.h"
#include "System_System_Uri1116831938.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "mscorlib_System_Nullable_1_gen4001024084.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "mscorlib_System_Type2863145774.h"

// System.Void Newtonsoft.Json.Linq.JValue::.ctor(System.Object,Newtonsoft.Json.Linq.JTokenType)
extern "C"  void JValue__ctor_m1246320787 (JValue_t3413677367 * __this, Il2CppObject * ___value0, int32_t ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JValue::.ctor(Newtonsoft.Json.Linq.JValue)
extern "C"  void JValue__ctor_m1552139175 (JValue_t3413677367 * __this, JValue_t3413677367 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JValue::.ctor(System.Int64)
extern "C"  void JValue__ctor_m2947450749 (JValue_t3413677367 * __this, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JValue::.ctor(System.UInt64)
extern "C"  void JValue__ctor_m2173799216 (JValue_t3413677367 * __this, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JValue::.ctor(System.Double)
extern "C"  void JValue__ctor_m1065028087 (JValue_t3413677367 * __this, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JValue::.ctor(System.DateTime)
extern "C"  void JValue__ctor_m1836641197 (JValue_t3413677367 * __this, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JValue::.ctor(System.Boolean)
extern "C"  void JValue__ctor_m3597696354 (JValue_t3413677367 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JValue::.ctor(System.String)
extern "C"  void JValue__ctor_m1633267127 (JValue_t3413677367 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JValue::.ctor(System.Guid)
extern "C"  void JValue__ctor_m1063262719 (JValue_t3413677367 * __this, Guid_t2862754429  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JValue::.ctor(SerializableGuid)
extern "C"  void JValue__ctor_m3924747235 (JValue_t3413677367 * __this, SerializableGuid_t3998617672 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JValue::.ctor(System.Uri)
extern "C"  void JValue__ctor_m2112922910 (JValue_t3413677367 * __this, Uri_t1116831938 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JValue::.ctor(System.TimeSpan)
extern "C"  void JValue__ctor_m2121436945 (JValue_t3413677367 * __this, TimeSpan_t413522987  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JValue::.ctor(System.Object)
extern "C"  void JValue__ctor_m1855377481 (JValue_t3413677367 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JValue::System.IComparable.CompareTo(System.Object)
extern "C"  int32_t JValue_System_IComparable_CompareTo_m1767108947 (JValue_t3413677367 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JValue::DeepEquals(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JValue_DeepEquals_m3930035354 (JValue_t3413677367 * __this, JToken_t3412245951 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JValue::get_HasValues()
extern "C"  bool JValue_get_HasValues_m4064322288 (JValue_t3413677367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JValue::Compare(Newtonsoft.Json.Linq.JTokenType,System.Object,System.Object)
extern "C"  int32_t JValue_Compare_m114514864 (Il2CppObject * __this /* static, unused */, int32_t ___valueType0, Il2CppObject * ___objA1, Il2CppObject * ___objB2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JValue::CompareFloat(System.Object,System.Object)
extern "C"  int32_t JValue_CompareFloat_m1448268672 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___objA0, Il2CppObject * ___objB1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JValue::CloneToken()
extern "C"  JToken_t3412245951 * JValue_CloneToken_m1449425130 (JValue_t3413677367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JValue Newtonsoft.Json.Linq.JValue::CreateComment(System.String)
extern "C"  JValue_t3413677367 * JValue_CreateComment_m1421088579 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JValue Newtonsoft.Json.Linq.JValue::CreateString(System.String)
extern "C"  JValue_t3413677367 * JValue_CreateString_m2955768687 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JValue::GetValueType(System.Nullable`1<Newtonsoft.Json.Linq.JTokenType>,System.Object)
extern "C"  int32_t JValue_GetValueType_m3860067780 (Il2CppObject * __this /* static, unused */, Nullable_1_t4001024084  ___current0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JValue::GetStringValueType(System.Nullable`1<Newtonsoft.Json.Linq.JTokenType>)
extern "C"  int32_t JValue_GetStringValueType_m2661569061 (Il2CppObject * __this /* static, unused */, Nullable_1_t4001024084  ___current0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JValue::get_Type()
extern "C"  int32_t JValue_get_Type_m92425515 (JValue_t3413677367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JValue::get_Value()
extern "C"  Il2CppObject * JValue_get_Value_m3578355014 (JValue_t3413677367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JValue::set_Value(System.Object)
extern "C"  void JValue_set_Value_m3011325463 (JValue_t3413677367 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JValue::WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
extern "C"  void JValue_WriteTo_m1810349370 (JValue_t3413677367 * __this, JsonWriter_t972330355 * ___writer0, JsonConverterU5BU5D_t638349667* ___converters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JValue::GetDeepHashCode()
extern "C"  int32_t JValue_GetDeepHashCode_m73880628 (JValue_t3413677367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JValue::ValuesEquals(Newtonsoft.Json.Linq.JValue,Newtonsoft.Json.Linq.JValue)
extern "C"  bool JValue_ValuesEquals_m139630744 (Il2CppObject * __this /* static, unused */, JValue_t3413677367 * ___v10, JValue_t3413677367 * ___v21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JValue::Equals(Newtonsoft.Json.Linq.JValue)
extern "C"  bool JValue_Equals_m1702790830 (JValue_t3413677367 * __this, JValue_t3413677367 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JValue::Equals(System.Object)
extern "C"  bool JValue_Equals_m3343276944 (JValue_t3413677367 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JValue::GetHashCode()
extern "C"  int32_t JValue_GetHashCode_m3044702248 (JValue_t3413677367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JValue::ToString()
extern "C"  String_t* JValue_ToString_m3436326050 (JValue_t3413677367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JValue::ToString(System.String)
extern "C"  String_t* JValue_ToString_m4224747936 (JValue_t3413677367 * __this, String_t* ___format0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JValue::ToString(System.IFormatProvider)
extern "C"  String_t* JValue_ToString_m1959234576 (JValue_t3413677367 * __this, Il2CppObject * ___formatProvider0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JValue::ToString(System.String,System.IFormatProvider)
extern "C"  String_t* JValue_ToString_m1582317396 (JValue_t3413677367 * __this, String_t* ___format0, Il2CppObject * ___formatProvider1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JValue::CompareTo(Newtonsoft.Json.Linq.JValue)
extern "C"  int32_t JValue_CompareTo_m2626646203 (JValue_t3413677367 * __this, JValue_t3413677367 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JValue::ilo_CompareFloat1(System.Object,System.Object)
extern "C"  int32_t JValue_ilo_CompareFloat1_m3172019908 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___objA0, Il2CppObject * ___objB1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JValue::ilo_GetString2(SerializableGuid)
extern "C"  String_t* JValue_ilo_GetString2_m2018987308 (Il2CppObject * __this /* static, unused */, SerializableGuid_t3998617672 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JValue::ilo_ApproxEquals3(System.Double,System.Double)
extern "C"  bool JValue_ilo_ApproxEquals3_m3009434874 (Il2CppObject * __this /* static, unused */, double ___d10, double ___d21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JValue::ilo_GetStringValueType4(System.Nullable`1<Newtonsoft.Json.Linq.JTokenType>)
extern "C"  int32_t JValue_ilo_GetStringValueType4_m4104229328 (Il2CppObject * __this /* static, unused */, Nullable_1_t4001024084  ___current0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JValue::ilo_GetValueType5(System.Nullable`1<Newtonsoft.Json.Linq.JTokenType>,System.Object)
extern "C"  int32_t JValue_ilo_GetValueType5_m3648538286 (Il2CppObject * __this /* static, unused */, Nullable_1_t4001024084  ___current0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Linq.JValue::ilo_GetMatchingConverter6(System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>,System.Type)
extern "C"  JsonConverter_t2159686854 * JValue_ilo_GetMatchingConverter6_m2730980808 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___converters0, Type_t * ___objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JValue::ilo_WriteValue7(Newtonsoft.Json.JsonWriter,System.Int64)
extern "C"  void JValue_ilo_WriteValue7_m731708973 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, int64_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JValue::ilo_WriteValue8(Newtonsoft.Json.JsonWriter,System.DateTime)
extern "C"  void JValue_ilo_WriteValue8_m325024126 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, DateTime_t4283661327  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JValue::ilo_WriteValue9(Newtonsoft.Json.JsonWriter,System.String)
extern "C"  void JValue_ilo_WriteValue9_m767355529 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JValue::ilo_Compare10(Newtonsoft.Json.Linq.JTokenType,System.Object,System.Object)
extern "C"  int32_t JValue_ilo_Compare10_m1237033892 (Il2CppObject * __this /* static, unused */, int32_t ___valueType0, Il2CppObject * ___objA1, Il2CppObject * ___objB2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JValue::ilo_Equals11(Newtonsoft.Json.Linq.JValue,Newtonsoft.Json.Linq.JValue)
extern "C"  bool JValue_ilo_Equals11_m2125800423 (Il2CppObject * __this /* static, unused */, JValue_t3413677367 * ____this0, JValue_t3413677367 * ___other1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JValue::ilo_ToString12(Newtonsoft.Json.Linq.JValue,System.String,System.IFormatProvider)
extern "C"  String_t* JValue_ilo_ToString12_m1458517052 (Il2CppObject * __this /* static, unused */, JValue_t3413677367 * ____this0, String_t* ___format1, Il2CppObject * ___formatProvider2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
