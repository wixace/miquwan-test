﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2<System.Object,System.Object>
struct SortedDictionary_2_t186228230;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary1266487790.h"

// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2723313193_gshared (Enumerator_t1266487790 * __this, SortedDictionary_2_t186228230 * ___dic0, const MethodInfo* method);
#define Enumerator__ctor_m2723313193(__this, ___dic0, method) ((  void (*) (Enumerator_t1266487790 *, SortedDictionary_2_t186228230 *, const MethodInfo*))Enumerator__ctor_m2723313193_gshared)(__this, ___dic0, method)
// System.Object System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m121718613_gshared (Enumerator_t1266487790 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m121718613(__this, method) ((  Il2CppObject * (*) (Enumerator_t1266487790 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m121718613_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2597270121_gshared (Enumerator_t1266487790 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2597270121(__this, method) ((  void (*) (Enumerator_t1266487790 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2597270121_gshared)(__this, method)
// TKey System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m655440371_gshared (Enumerator_t1266487790 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m655440371(__this, method) ((  Il2CppObject * (*) (Enumerator_t1266487790 *, const MethodInfo*))Enumerator_get_Current_m655440371_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2316713921_gshared (Enumerator_t1266487790 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2316713921(__this, method) ((  bool (*) (Enumerator_t1266487790 *, const MethodInfo*))Enumerator_MoveNext_m2316713921_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m259950702_gshared (Enumerator_t1266487790 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m259950702(__this, method) ((  void (*) (Enumerator_t1266487790 *, const MethodInfo*))Enumerator_Dispose_m259950702_gshared)(__this, method)
