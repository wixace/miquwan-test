﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Interpolate/<NewTimer>c__Iterator21
struct U3CNewTimerU3Ec__Iterator21_t78608517;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t1908816725;

#include "codegen/il2cpp-codegen.h"

// System.Void Interpolate/<NewTimer>c__Iterator21::.ctor()
extern "C"  void U3CNewTimerU3Ec__Iterator21__ctor_m2937753734 (U3CNewTimerU3Ec__Iterator21_t78608517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate/<NewTimer>c__Iterator21::System.Collections.Generic.IEnumerator<float>.get_Current()
extern "C"  float U3CNewTimerU3Ec__Iterator21_System_Collections_Generic_IEnumeratorU3CfloatU3E_get_Current_m3257496658 (U3CNewTimerU3Ec__Iterator21_t78608517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Interpolate/<NewTimer>c__Iterator21::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CNewTimerU3Ec__Iterator21_System_Collections_IEnumerator_get_Current_m2469894304 (U3CNewTimerU3Ec__Iterator21_t78608517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Interpolate/<NewTimer>c__Iterator21::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CNewTimerU3Ec__Iterator21_System_Collections_IEnumerable_GetEnumerator_m580318235 (U3CNewTimerU3Ec__Iterator21_t78608517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Single> Interpolate/<NewTimer>c__Iterator21::System.Collections.Generic.IEnumerable<float>.GetEnumerator()
extern "C"  Il2CppObject* U3CNewTimerU3Ec__Iterator21_System_Collections_Generic_IEnumerableU3CfloatU3E_GetEnumerator_m2485489563 (U3CNewTimerU3Ec__Iterator21_t78608517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Interpolate/<NewTimer>c__Iterator21::MoveNext()
extern "C"  bool U3CNewTimerU3Ec__Iterator21_MoveNext_m376385966 (U3CNewTimerU3Ec__Iterator21_t78608517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Interpolate/<NewTimer>c__Iterator21::Dispose()
extern "C"  void U3CNewTimerU3Ec__Iterator21_Dispose_m1286107971 (U3CNewTimerU3Ec__Iterator21_t78608517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Interpolate/<NewTimer>c__Iterator21::Reset()
extern "C"  void U3CNewTimerU3Ec__Iterator21_Reset_m584186675 (U3CNewTimerU3Ec__Iterator21_t78608517 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
