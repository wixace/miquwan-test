﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22144098788.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2441266561_gshared (KeyValuePair_2_t2144098788 * __this, Int2_t1974045593  ___key0, int32_t ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m2441266561(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2144098788 *, Int2_t1974045593 , int32_t, const MethodInfo*))KeyValuePair_2__ctor_m2441266561_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>::get_Key()
extern "C"  Int2_t1974045593  KeyValuePair_2_get_Key_m2231789767_gshared (KeyValuePair_2_t2144098788 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2231789767(__this, method) ((  Int2_t1974045593  (*) (KeyValuePair_2_t2144098788 *, const MethodInfo*))KeyValuePair_2_get_Key_m2231789767_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1704290312_gshared (KeyValuePair_2_t2144098788 * __this, Int2_t1974045593  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m1704290312(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2144098788 *, Int2_t1974045593 , const MethodInfo*))KeyValuePair_2_set_Key_m1704290312_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m4232945451_gshared (KeyValuePair_2_t2144098788 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m4232945451(__this, method) ((  int32_t (*) (KeyValuePair_2_t2144098788 *, const MethodInfo*))KeyValuePair_2_get_Value_m4232945451_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1097693192_gshared (KeyValuePair_2_t2144098788 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1097693192(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2144098788 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m1097693192_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Pathfinding.Int2,System.Int32>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m2959319872_gshared (KeyValuePair_2_t2144098788 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m2959319872(__this, method) ((  String_t* (*) (KeyValuePair_2_t2144098788 *, const MethodInfo*))KeyValuePair_2_ToString_m2959319872_gshared)(__this, method)
