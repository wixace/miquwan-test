﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.Object>
struct KeyCollection_t3365640714;
// System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>
struct Dictionary_2_t1738881263;
// System.Collections.Generic.IEnumerator`1<System.UInt16>
struct IEnumerator_1_t1936532972;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.UInt16[]
struct UInt16U5BU5D_t801649474;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2353817317.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m3352204200_gshared (KeyCollection_t3365640714 * __this, Dictionary_2_t1738881263 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m3352204200(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3365640714 *, Dictionary_2_t1738881263 *, const MethodInfo*))KeyCollection__ctor_m3352204200_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m912465070_gshared (KeyCollection_t3365640714 * __this, uint16_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m912465070(__this, ___item0, method) ((  void (*) (KeyCollection_t3365640714 *, uint16_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m912465070_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1149539749_gshared (KeyCollection_t3365640714 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1149539749(__this, method) ((  void (*) (KeyCollection_t3365640714 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1149539749_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2322272028_gshared (KeyCollection_t3365640714 * __this, uint16_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2322272028(__this, ___item0, method) ((  bool (*) (KeyCollection_t3365640714 *, uint16_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2322272028_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3542652801_gshared (KeyCollection_t3365640714 * __this, uint16_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3542652801(__this, ___item0, method) ((  bool (*) (KeyCollection_t3365640714 *, uint16_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3542652801_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m674247265_gshared (KeyCollection_t3365640714 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m674247265(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3365640714 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m674247265_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m1545742999_gshared (KeyCollection_t3365640714 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m1545742999(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3365640714 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1545742999_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4109246930_gshared (KeyCollection_t3365640714 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4109246930(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3365640714 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4109246930_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1201483645_gshared (KeyCollection_t3365640714 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1201483645(__this, method) ((  bool (*) (KeyCollection_t3365640714 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1201483645_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1305477231_gshared (KeyCollection_t3365640714 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1305477231(__this, method) ((  bool (*) (KeyCollection_t3365640714 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1305477231_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2500749915_gshared (KeyCollection_t3365640714 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2500749915(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3365640714 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2500749915_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m2041879133_gshared (KeyCollection_t3365640714 * __this, UInt16U5BU5D_t801649474* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m2041879133(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3365640714 *, UInt16U5BU5D_t801649474*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2041879133_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2353817317  KeyCollection_GetEnumerator_m2071243712_gshared (KeyCollection_t3365640714 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m2071243712(__this, method) ((  Enumerator_t2353817317  (*) (KeyCollection_t3365640714 *, const MethodInfo*))KeyCollection_GetEnumerator_m2071243712_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.UInt16,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m2917785909_gshared (KeyCollection_t3365640714 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m2917785909(__this, method) ((  int32_t (*) (KeyCollection_t3365640714 *, const MethodInfo*))KeyCollection_get_Count_m2917785909_gshared)(__this, method)
