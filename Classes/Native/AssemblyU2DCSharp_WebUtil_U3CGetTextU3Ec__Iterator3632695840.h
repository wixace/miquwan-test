﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.Experimental.Networking.UnityWebRequest
struct UnityWebRequest_t327863158;
// System.Action
struct Action_t3771233898;
// System.Action`1<System.String>
struct Action_1_t403047693;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// WebUtil/<GetText>c__Iterator3
struct  U3CGetTextU3Ec__Iterator3_t3632695840  : public Il2CppObject
{
public:
	// System.String WebUtil/<GetText>c__Iterator3::url
	String_t* ___url_0;
	// UnityEngine.Experimental.Networking.UnityWebRequest WebUtil/<GetText>c__Iterator3::<www>__0
	UnityWebRequest_t327863158 * ___U3CwwwU3E__0_1;
	// System.Action WebUtil/<GetText>c__Iterator3::failure
	Action_t3771233898 * ___failure_2;
	// System.Action`1<System.String> WebUtil/<GetText>c__Iterator3::success
	Action_1_t403047693 * ___success_3;
	// System.Int32 WebUtil/<GetText>c__Iterator3::$PC
	int32_t ___U24PC_4;
	// System.Object WebUtil/<GetText>c__Iterator3::$current
	Il2CppObject * ___U24current_5;
	// System.String WebUtil/<GetText>c__Iterator3::<$>url
	String_t* ___U3CU24U3Eurl_6;
	// System.Action WebUtil/<GetText>c__Iterator3::<$>failure
	Action_t3771233898 * ___U3CU24U3Efailure_7;
	// System.Action`1<System.String> WebUtil/<GetText>c__Iterator3::<$>success
	Action_1_t403047693 * ___U3CU24U3Esuccess_8;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CGetTextU3Ec__Iterator3_t3632695840, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier(&___url_0, value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CGetTextU3Ec__Iterator3_t3632695840, ___U3CwwwU3E__0_1)); }
	inline UnityWebRequest_t327863158 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline UnityWebRequest_t327863158 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(UnityWebRequest_t327863158 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3E__0_1, value);
	}

	inline static int32_t get_offset_of_failure_2() { return static_cast<int32_t>(offsetof(U3CGetTextU3Ec__Iterator3_t3632695840, ___failure_2)); }
	inline Action_t3771233898 * get_failure_2() const { return ___failure_2; }
	inline Action_t3771233898 ** get_address_of_failure_2() { return &___failure_2; }
	inline void set_failure_2(Action_t3771233898 * value)
	{
		___failure_2 = value;
		Il2CppCodeGenWriteBarrier(&___failure_2, value);
	}

	inline static int32_t get_offset_of_success_3() { return static_cast<int32_t>(offsetof(U3CGetTextU3Ec__Iterator3_t3632695840, ___success_3)); }
	inline Action_1_t403047693 * get_success_3() const { return ___success_3; }
	inline Action_1_t403047693 ** get_address_of_success_3() { return &___success_3; }
	inline void set_success_3(Action_1_t403047693 * value)
	{
		___success_3 = value;
		Il2CppCodeGenWriteBarrier(&___success_3, value);
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetTextU3Ec__Iterator3_t3632695840, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CGetTextU3Ec__Iterator3_t3632695840, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Eurl_6() { return static_cast<int32_t>(offsetof(U3CGetTextU3Ec__Iterator3_t3632695840, ___U3CU24U3Eurl_6)); }
	inline String_t* get_U3CU24U3Eurl_6() const { return ___U3CU24U3Eurl_6; }
	inline String_t** get_address_of_U3CU24U3Eurl_6() { return &___U3CU24U3Eurl_6; }
	inline void set_U3CU24U3Eurl_6(String_t* value)
	{
		___U3CU24U3Eurl_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Eurl_6, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Efailure_7() { return static_cast<int32_t>(offsetof(U3CGetTextU3Ec__Iterator3_t3632695840, ___U3CU24U3Efailure_7)); }
	inline Action_t3771233898 * get_U3CU24U3Efailure_7() const { return ___U3CU24U3Efailure_7; }
	inline Action_t3771233898 ** get_address_of_U3CU24U3Efailure_7() { return &___U3CU24U3Efailure_7; }
	inline void set_U3CU24U3Efailure_7(Action_t3771233898 * value)
	{
		___U3CU24U3Efailure_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Efailure_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Esuccess_8() { return static_cast<int32_t>(offsetof(U3CGetTextU3Ec__Iterator3_t3632695840, ___U3CU24U3Esuccess_8)); }
	inline Action_1_t403047693 * get_U3CU24U3Esuccess_8() const { return ___U3CU24U3Esuccess_8; }
	inline Action_1_t403047693 ** get_address_of_U3CU24U3Esuccess_8() { return &___U3CU24U3Esuccess_8; }
	inline void set_U3CU24U3Esuccess_8(Action_1_t403047693 * value)
	{
		___U3CU24U3Esuccess_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Esuccess_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
