﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>
struct Dictionary_2_t3630855731;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En653211827.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23529636437.h"
#include "AssemblyU2DCSharp_AIEnum_EAIEventtype3896289311.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1928248751_gshared (Enumerator_t653211827 * __this, Dictionary_2_t3630855731 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m1928248751(__this, ___dictionary0, method) ((  void (*) (Enumerator_t653211827 *, Dictionary_2_t3630855731 *, const MethodInfo*))Enumerator__ctor_m1928248751_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m601538706_gshared (Enumerator_t653211827 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m601538706(__this, method) ((  Il2CppObject * (*) (Enumerator_t653211827 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m601538706_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m677036966_gshared (Enumerator_t653211827 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m677036966(__this, method) ((  void (*) (Enumerator_t653211827 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m677036966_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1544419567_gshared (Enumerator_t653211827 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1544419567(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t653211827 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1544419567_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4255631342_gshared (Enumerator_t653211827 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4255631342(__this, method) ((  Il2CppObject * (*) (Enumerator_t653211827 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4255631342_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1901290240_gshared (Enumerator_t653211827 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1901290240(__this, method) ((  Il2CppObject * (*) (Enumerator_t653211827 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1901290240_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2962790162_gshared (Enumerator_t653211827 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2962790162(__this, method) ((  bool (*) (Enumerator_t653211827 *, const MethodInfo*))Enumerator_MoveNext_m2962790162_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Object>::get_Current()
extern "C"  KeyValuePair_2_t3529636437  Enumerator_get_Current_m2315292254_gshared (Enumerator_t653211827 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2315292254(__this, method) ((  KeyValuePair_2_t3529636437  (*) (Enumerator_t653211827 *, const MethodInfo*))Enumerator_get_Current_m2315292254_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Object>::get_CurrentKey()
extern "C"  uint8_t Enumerator_get_CurrentKey_m3377861599_gshared (Enumerator_t653211827 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m3377861599(__this, method) ((  uint8_t (*) (Enumerator_t653211827 *, const MethodInfo*))Enumerator_get_CurrentKey_m3377861599_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Object>::get_CurrentValue()
extern "C"  Il2CppObject * Enumerator_get_CurrentValue_m3676464963_gshared (Enumerator_t653211827 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m3676464963(__this, method) ((  Il2CppObject * (*) (Enumerator_t653211827 *, const MethodInfo*))Enumerator_get_CurrentValue_m3676464963_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Object>::Reset()
extern "C"  void Enumerator_Reset_m3044163969_gshared (Enumerator_t653211827 * __this, const MethodInfo* method);
#define Enumerator_Reset_m3044163969(__this, method) ((  void (*) (Enumerator_t653211827 *, const MethodInfo*))Enumerator_Reset_m3044163969_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Object>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2803150026_gshared (Enumerator_t653211827 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2803150026(__this, method) ((  void (*) (Enumerator_t653211827 *, const MethodInfo*))Enumerator_VerifyState_m2803150026_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Object>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m3627552178_gshared (Enumerator_t653211827 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m3627552178(__this, method) ((  void (*) (Enumerator_t653211827 *, const MethodInfo*))Enumerator_VerifyCurrent_m3627552178_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<AIEnum.EAIEventtype,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3092274705_gshared (Enumerator_t653211827 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3092274705(__this, method) ((  void (*) (Enumerator_t653211827 *, const MethodInfo*))Enumerator_Dispose_m3092274705_gshared)(__this, method)
