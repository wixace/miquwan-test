﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3821466570MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m614460794(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2471845744 *, Dictionary_2_t845086293 *, const MethodInfo*))KeyCollection__ctor_m2357291944_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt16>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1930520476(__this, ___item0, method) ((  void (*) (KeyCollection_t2471845744 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3544794286_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt16>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2156184595(__this, method) ((  void (*) (KeyCollection_t2471845744 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3033841061_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt16>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m3879495918(__this, ___item0, method) ((  bool (*) (KeyCollection_t2471845744 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1327359772_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt16>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1372208595(__this, ___item0, method) ((  bool (*) (KeyCollection_t2471845744 *, String_t*, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1454468993_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt16>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1703547599(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2471845744 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4191705697_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt16>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m2677679109(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2471845744 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1299600023_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt16>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2257926848(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2471845744 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1316768210_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt16>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m3045022927(__this, method) ((  bool (*) (KeyCollection_t2471845744 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2868512637_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt16>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m922307649(__this, method) ((  bool (*) (KeyCollection_t2471845744 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4275755119_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt16>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3133766573(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2471845744 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m1025196635_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt16>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m2152840239(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2471845744 *, StringU5BU5D_t4054002952*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m4291261021_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt16>::GetEnumerator()
#define KeyCollection_GetEnumerator_m2246604946(__this, method) ((  Enumerator_t1460022347  (*) (KeyCollection_t2471845744 *, const MethodInfo*))KeyCollection_GetEnumerator_m621511616_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.UInt16>::get_Count()
#define KeyCollection_get_Count_m4124632839(__this, method) ((  int32_t (*) (KeyCollection_t2471845744 *, const MethodInfo*))KeyCollection_get_Count_m2762783029_gshared)(__this, method)
