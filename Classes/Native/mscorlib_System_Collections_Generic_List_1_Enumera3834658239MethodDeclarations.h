﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Net.Mail.SmtpFailedRecipientException>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2154081699(__this, ___l0, method) ((  void (*) (Enumerator_t3834658239 *, List_1_t3814985469 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Net.Mail.SmtpFailedRecipientException>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3727109327(__this, method) ((  void (*) (Enumerator_t3834658239 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Net.Mail.SmtpFailedRecipientException>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3506294533(__this, method) ((  Il2CppObject * (*) (Enumerator_t3834658239 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Net.Mail.SmtpFailedRecipientException>::Dispose()
#define Enumerator_Dispose_m1137742920(__this, method) ((  void (*) (Enumerator_t3834658239 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Net.Mail.SmtpFailedRecipientException>::VerifyState()
#define Enumerator_VerifyState_m1854992257(__this, method) ((  void (*) (Enumerator_t3834658239 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Net.Mail.SmtpFailedRecipientException>::MoveNext()
#define Enumerator_MoveNext_m677317631(__this, method) ((  bool (*) (Enumerator_t3834658239 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Net.Mail.SmtpFailedRecipientException>::get_Current()
#define Enumerator_get_Current_m484366234(__this, method) ((  SmtpFailedRecipientException_t2446799917 * (*) (Enumerator_t3834658239 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
