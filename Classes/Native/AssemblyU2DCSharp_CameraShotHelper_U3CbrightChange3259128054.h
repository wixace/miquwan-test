﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CameraShotHelper
struct CameraShotHelper_t627387501;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraShotHelper/<brightChange>c__AnonStorey14F
struct  U3CbrightChangeU3Ec__AnonStorey14F_t3259128054  : public Il2CppObject
{
public:
	// System.Int32 CameraShotHelper/<brightChange>c__AnonStorey14F::brightId
	int32_t ___brightId_0;
	// System.Int32 CameraShotHelper/<brightChange>c__AnonStorey14F::curCameraShotId
	int32_t ___curCameraShotId_1;
	// System.Single CameraShotHelper/<brightChange>c__AnonStorey14F::time
	float ___time_2;
	// CameraShotHelper CameraShotHelper/<brightChange>c__AnonStorey14F::<>f__this
	CameraShotHelper_t627387501 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_brightId_0() { return static_cast<int32_t>(offsetof(U3CbrightChangeU3Ec__AnonStorey14F_t3259128054, ___brightId_0)); }
	inline int32_t get_brightId_0() const { return ___brightId_0; }
	inline int32_t* get_address_of_brightId_0() { return &___brightId_0; }
	inline void set_brightId_0(int32_t value)
	{
		___brightId_0 = value;
	}

	inline static int32_t get_offset_of_curCameraShotId_1() { return static_cast<int32_t>(offsetof(U3CbrightChangeU3Ec__AnonStorey14F_t3259128054, ___curCameraShotId_1)); }
	inline int32_t get_curCameraShotId_1() const { return ___curCameraShotId_1; }
	inline int32_t* get_address_of_curCameraShotId_1() { return &___curCameraShotId_1; }
	inline void set_curCameraShotId_1(int32_t value)
	{
		___curCameraShotId_1 = value;
	}

	inline static int32_t get_offset_of_time_2() { return static_cast<int32_t>(offsetof(U3CbrightChangeU3Ec__AnonStorey14F_t3259128054, ___time_2)); }
	inline float get_time_2() const { return ___time_2; }
	inline float* get_address_of_time_2() { return &___time_2; }
	inline void set_time_2(float value)
	{
		___time_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CbrightChangeU3Ec__AnonStorey14F_t3259128054, ___U3CU3Ef__this_3)); }
	inline CameraShotHelper_t627387501 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline CameraShotHelper_t627387501 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(CameraShotHelper_t627387501 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
