﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AstarSmoothFollow2
struct AstarSmoothFollow2_t2943886784;

#include "codegen/il2cpp-codegen.h"

// System.Void AstarSmoothFollow2::.ctor()
extern "C"  void AstarSmoothFollow2__ctor_m3827061467 (AstarSmoothFollow2_t2943886784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarSmoothFollow2::LateUpdate()
extern "C"  void AstarSmoothFollow2_LateUpdate_m693209176 (AstarSmoothFollow2_t2943886784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
