﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3339007067;
// System.Collections.Generic.IEnumerator`1<System.Type>
struct IEnumerator_1_t480043527;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.TypeExtensions/<AllInterfaces>c__Iterator20
struct  U3CAllInterfacesU3Ec__Iterator20_t3433998680  : public Il2CppObject
{
public:
	// System.Type Newtonsoft.Json.Utilities.TypeExtensions/<AllInterfaces>c__Iterator20::target
	Type_t * ___target_0;
	// System.Type[] Newtonsoft.Json.Utilities.TypeExtensions/<AllInterfaces>c__Iterator20::<$s_157>__0
	TypeU5BU5D_t3339007067* ___U3CU24s_157U3E__0_1;
	// System.Int32 Newtonsoft.Json.Utilities.TypeExtensions/<AllInterfaces>c__Iterator20::<$s_158>__1
	int32_t ___U3CU24s_158U3E__1_2;
	// System.Type Newtonsoft.Json.Utilities.TypeExtensions/<AllInterfaces>c__Iterator20::<IF>__2
	Type_t * ___U3CIFU3E__2_3;
	// System.Collections.Generic.IEnumerator`1<System.Type> Newtonsoft.Json.Utilities.TypeExtensions/<AllInterfaces>c__Iterator20::<$s_159>__3
	Il2CppObject* ___U3CU24s_159U3E__3_4;
	// System.Type Newtonsoft.Json.Utilities.TypeExtensions/<AllInterfaces>c__Iterator20::<childIF>__4
	Type_t * ___U3CchildIFU3E__4_5;
	// System.Int32 Newtonsoft.Json.Utilities.TypeExtensions/<AllInterfaces>c__Iterator20::$PC
	int32_t ___U24PC_6;
	// System.Type Newtonsoft.Json.Utilities.TypeExtensions/<AllInterfaces>c__Iterator20::$current
	Type_t * ___U24current_7;
	// System.Type Newtonsoft.Json.Utilities.TypeExtensions/<AllInterfaces>c__Iterator20::<$>target
	Type_t * ___U3CU24U3Etarget_8;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CAllInterfacesU3Ec__Iterator20_t3433998680, ___target_0)); }
	inline Type_t * get_target_0() const { return ___target_0; }
	inline Type_t ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Type_t * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier(&___target_0, value);
	}

	inline static int32_t get_offset_of_U3CU24s_157U3E__0_1() { return static_cast<int32_t>(offsetof(U3CAllInterfacesU3Ec__Iterator20_t3433998680, ___U3CU24s_157U3E__0_1)); }
	inline TypeU5BU5D_t3339007067* get_U3CU24s_157U3E__0_1() const { return ___U3CU24s_157U3E__0_1; }
	inline TypeU5BU5D_t3339007067** get_address_of_U3CU24s_157U3E__0_1() { return &___U3CU24s_157U3E__0_1; }
	inline void set_U3CU24s_157U3E__0_1(TypeU5BU5D_t3339007067* value)
	{
		___U3CU24s_157U3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_157U3E__0_1, value);
	}

	inline static int32_t get_offset_of_U3CU24s_158U3E__1_2() { return static_cast<int32_t>(offsetof(U3CAllInterfacesU3Ec__Iterator20_t3433998680, ___U3CU24s_158U3E__1_2)); }
	inline int32_t get_U3CU24s_158U3E__1_2() const { return ___U3CU24s_158U3E__1_2; }
	inline int32_t* get_address_of_U3CU24s_158U3E__1_2() { return &___U3CU24s_158U3E__1_2; }
	inline void set_U3CU24s_158U3E__1_2(int32_t value)
	{
		___U3CU24s_158U3E__1_2 = value;
	}

	inline static int32_t get_offset_of_U3CIFU3E__2_3() { return static_cast<int32_t>(offsetof(U3CAllInterfacesU3Ec__Iterator20_t3433998680, ___U3CIFU3E__2_3)); }
	inline Type_t * get_U3CIFU3E__2_3() const { return ___U3CIFU3E__2_3; }
	inline Type_t ** get_address_of_U3CIFU3E__2_3() { return &___U3CIFU3E__2_3; }
	inline void set_U3CIFU3E__2_3(Type_t * value)
	{
		___U3CIFU3E__2_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CIFU3E__2_3, value);
	}

	inline static int32_t get_offset_of_U3CU24s_159U3E__3_4() { return static_cast<int32_t>(offsetof(U3CAllInterfacesU3Ec__Iterator20_t3433998680, ___U3CU24s_159U3E__3_4)); }
	inline Il2CppObject* get_U3CU24s_159U3E__3_4() const { return ___U3CU24s_159U3E__3_4; }
	inline Il2CppObject** get_address_of_U3CU24s_159U3E__3_4() { return &___U3CU24s_159U3E__3_4; }
	inline void set_U3CU24s_159U3E__3_4(Il2CppObject* value)
	{
		___U3CU24s_159U3E__3_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24s_159U3E__3_4, value);
	}

	inline static int32_t get_offset_of_U3CchildIFU3E__4_5() { return static_cast<int32_t>(offsetof(U3CAllInterfacesU3Ec__Iterator20_t3433998680, ___U3CchildIFU3E__4_5)); }
	inline Type_t * get_U3CchildIFU3E__4_5() const { return ___U3CchildIFU3E__4_5; }
	inline Type_t ** get_address_of_U3CchildIFU3E__4_5() { return &___U3CchildIFU3E__4_5; }
	inline void set_U3CchildIFU3E__4_5(Type_t * value)
	{
		___U3CchildIFU3E__4_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CchildIFU3E__4_5, value);
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CAllInterfacesU3Ec__Iterator20_t3433998680, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CAllInterfacesU3Ec__Iterator20_t3433998680, ___U24current_7)); }
	inline Type_t * get_U24current_7() const { return ___U24current_7; }
	inline Type_t ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(Type_t * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_7, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Etarget_8() { return static_cast<int32_t>(offsetof(U3CAllInterfacesU3Ec__Iterator20_t3433998680, ___U3CU24U3Etarget_8)); }
	inline Type_t * get_U3CU24U3Etarget_8() const { return ___U3CU24U3Etarget_8; }
	inline Type_t ** get_address_of_U3CU24U3Etarget_8() { return &___U3CU24U3Etarget_8; }
	inline void set_U3CU24U3Etarget_8(Type_t * value)
	{
		___U3CU24U3Etarget_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Etarget_8, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
