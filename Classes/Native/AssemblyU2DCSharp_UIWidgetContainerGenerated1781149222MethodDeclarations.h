﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIWidgetContainerGenerated
struct UIWidgetContainerGenerated_t1781149222;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UIWidgetContainerGenerated::.ctor()
extern "C"  void UIWidgetContainerGenerated__ctor_m3253257525 (UIWidgetContainerGenerated_t1781149222 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetContainerGenerated::UIWidgetContainer_UIWidgetContainer1(JSVCall,System.Int32)
extern "C"  bool UIWidgetContainerGenerated_UIWidgetContainer_UIWidgetContainer1_m460965393 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetContainerGenerated::__Register()
extern "C"  void UIWidgetContainerGenerated___Register_m433893554 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetContainerGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UIWidgetContainerGenerated_ilo_addJSCSRel1_m1928897905 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
