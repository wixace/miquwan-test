﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JSDataExchange_Arr
struct  JSDataExchange_Arr_t122463832  : public Il2CppObject
{
public:
	// System.Type JSDataExchange_Arr::elementType
	Type_t * ___elementType_0;

public:
	inline static int32_t get_offset_of_elementType_0() { return static_cast<int32_t>(offsetof(JSDataExchange_Arr_t122463832, ___elementType_0)); }
	inline Type_t * get_elementType_0() const { return ___elementType_0; }
	inline Type_t ** get_address_of_elementType_0() { return &___elementType_0; }
	inline void set_elementType_0(Type_t * value)
	{
		___elementType_0 = value;
		Il2CppCodeGenWriteBarrier(&___elementType_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
