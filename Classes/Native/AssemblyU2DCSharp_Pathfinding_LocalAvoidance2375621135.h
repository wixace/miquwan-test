﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.CharacterController
struct CharacterController_t1618060635;
// Pathfinding.LocalAvoidance[]
struct LocalAvoidanceU5BU5D_t454871254;
// System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VO>
struct List_1_t3540405845;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_Resolu806529266.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.LocalAvoidance
struct  LocalAvoidance_t2375621135  : public MonoBehaviour_t667441552
{
public:
	// System.Single Pathfinding.LocalAvoidance::speed
	float ___speed_4;
	// System.Single Pathfinding.LocalAvoidance::delta
	float ___delta_5;
	// System.Single Pathfinding.LocalAvoidance::responability
	float ___responability_6;
	// Pathfinding.LocalAvoidance/ResolutionType Pathfinding.LocalAvoidance::resType
	int32_t ___resType_7;
	// UnityEngine.Vector3 Pathfinding.LocalAvoidance::velocity
	Vector3_t4282066566  ___velocity_8;
	// System.Single Pathfinding.LocalAvoidance::radius
	float ___radius_9;
	// System.Single Pathfinding.LocalAvoidance::maxSpeedScale
	float ___maxSpeedScale_10;
	// UnityEngine.Vector3[] Pathfinding.LocalAvoidance::samples
	Vector3U5BU5D_t215400611* ___samples_11;
	// System.Single Pathfinding.LocalAvoidance::sampleScale
	float ___sampleScale_12;
	// System.Single Pathfinding.LocalAvoidance::circleScale
	float ___circleScale_13;
	// System.Single Pathfinding.LocalAvoidance::circlePoint
	float ___circlePoint_14;
	// System.Boolean Pathfinding.LocalAvoidance::drawGizmos
	bool ___drawGizmos_15;
	// UnityEngine.CharacterController Pathfinding.LocalAvoidance::controller
	CharacterController_t1618060635 * ___controller_16;
	// Pathfinding.LocalAvoidance[] Pathfinding.LocalAvoidance::agents
	LocalAvoidanceU5BU5D_t454871254* ___agents_17;
	// UnityEngine.Vector3 Pathfinding.LocalAvoidance::preVelocity
	Vector3_t4282066566  ___preVelocity_18;
	// System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/VO> Pathfinding.LocalAvoidance::vos
	List_1_t3540405845 * ___vos_19;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(LocalAvoidance_t2375621135, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_delta_5() { return static_cast<int32_t>(offsetof(LocalAvoidance_t2375621135, ___delta_5)); }
	inline float get_delta_5() const { return ___delta_5; }
	inline float* get_address_of_delta_5() { return &___delta_5; }
	inline void set_delta_5(float value)
	{
		___delta_5 = value;
	}

	inline static int32_t get_offset_of_responability_6() { return static_cast<int32_t>(offsetof(LocalAvoidance_t2375621135, ___responability_6)); }
	inline float get_responability_6() const { return ___responability_6; }
	inline float* get_address_of_responability_6() { return &___responability_6; }
	inline void set_responability_6(float value)
	{
		___responability_6 = value;
	}

	inline static int32_t get_offset_of_resType_7() { return static_cast<int32_t>(offsetof(LocalAvoidance_t2375621135, ___resType_7)); }
	inline int32_t get_resType_7() const { return ___resType_7; }
	inline int32_t* get_address_of_resType_7() { return &___resType_7; }
	inline void set_resType_7(int32_t value)
	{
		___resType_7 = value;
	}

	inline static int32_t get_offset_of_velocity_8() { return static_cast<int32_t>(offsetof(LocalAvoidance_t2375621135, ___velocity_8)); }
	inline Vector3_t4282066566  get_velocity_8() const { return ___velocity_8; }
	inline Vector3_t4282066566 * get_address_of_velocity_8() { return &___velocity_8; }
	inline void set_velocity_8(Vector3_t4282066566  value)
	{
		___velocity_8 = value;
	}

	inline static int32_t get_offset_of_radius_9() { return static_cast<int32_t>(offsetof(LocalAvoidance_t2375621135, ___radius_9)); }
	inline float get_radius_9() const { return ___radius_9; }
	inline float* get_address_of_radius_9() { return &___radius_9; }
	inline void set_radius_9(float value)
	{
		___radius_9 = value;
	}

	inline static int32_t get_offset_of_maxSpeedScale_10() { return static_cast<int32_t>(offsetof(LocalAvoidance_t2375621135, ___maxSpeedScale_10)); }
	inline float get_maxSpeedScale_10() const { return ___maxSpeedScale_10; }
	inline float* get_address_of_maxSpeedScale_10() { return &___maxSpeedScale_10; }
	inline void set_maxSpeedScale_10(float value)
	{
		___maxSpeedScale_10 = value;
	}

	inline static int32_t get_offset_of_samples_11() { return static_cast<int32_t>(offsetof(LocalAvoidance_t2375621135, ___samples_11)); }
	inline Vector3U5BU5D_t215400611* get_samples_11() const { return ___samples_11; }
	inline Vector3U5BU5D_t215400611** get_address_of_samples_11() { return &___samples_11; }
	inline void set_samples_11(Vector3U5BU5D_t215400611* value)
	{
		___samples_11 = value;
		Il2CppCodeGenWriteBarrier(&___samples_11, value);
	}

	inline static int32_t get_offset_of_sampleScale_12() { return static_cast<int32_t>(offsetof(LocalAvoidance_t2375621135, ___sampleScale_12)); }
	inline float get_sampleScale_12() const { return ___sampleScale_12; }
	inline float* get_address_of_sampleScale_12() { return &___sampleScale_12; }
	inline void set_sampleScale_12(float value)
	{
		___sampleScale_12 = value;
	}

	inline static int32_t get_offset_of_circleScale_13() { return static_cast<int32_t>(offsetof(LocalAvoidance_t2375621135, ___circleScale_13)); }
	inline float get_circleScale_13() const { return ___circleScale_13; }
	inline float* get_address_of_circleScale_13() { return &___circleScale_13; }
	inline void set_circleScale_13(float value)
	{
		___circleScale_13 = value;
	}

	inline static int32_t get_offset_of_circlePoint_14() { return static_cast<int32_t>(offsetof(LocalAvoidance_t2375621135, ___circlePoint_14)); }
	inline float get_circlePoint_14() const { return ___circlePoint_14; }
	inline float* get_address_of_circlePoint_14() { return &___circlePoint_14; }
	inline void set_circlePoint_14(float value)
	{
		___circlePoint_14 = value;
	}

	inline static int32_t get_offset_of_drawGizmos_15() { return static_cast<int32_t>(offsetof(LocalAvoidance_t2375621135, ___drawGizmos_15)); }
	inline bool get_drawGizmos_15() const { return ___drawGizmos_15; }
	inline bool* get_address_of_drawGizmos_15() { return &___drawGizmos_15; }
	inline void set_drawGizmos_15(bool value)
	{
		___drawGizmos_15 = value;
	}

	inline static int32_t get_offset_of_controller_16() { return static_cast<int32_t>(offsetof(LocalAvoidance_t2375621135, ___controller_16)); }
	inline CharacterController_t1618060635 * get_controller_16() const { return ___controller_16; }
	inline CharacterController_t1618060635 ** get_address_of_controller_16() { return &___controller_16; }
	inline void set_controller_16(CharacterController_t1618060635 * value)
	{
		___controller_16 = value;
		Il2CppCodeGenWriteBarrier(&___controller_16, value);
	}

	inline static int32_t get_offset_of_agents_17() { return static_cast<int32_t>(offsetof(LocalAvoidance_t2375621135, ___agents_17)); }
	inline LocalAvoidanceU5BU5D_t454871254* get_agents_17() const { return ___agents_17; }
	inline LocalAvoidanceU5BU5D_t454871254** get_address_of_agents_17() { return &___agents_17; }
	inline void set_agents_17(LocalAvoidanceU5BU5D_t454871254* value)
	{
		___agents_17 = value;
		Il2CppCodeGenWriteBarrier(&___agents_17, value);
	}

	inline static int32_t get_offset_of_preVelocity_18() { return static_cast<int32_t>(offsetof(LocalAvoidance_t2375621135, ___preVelocity_18)); }
	inline Vector3_t4282066566  get_preVelocity_18() const { return ___preVelocity_18; }
	inline Vector3_t4282066566 * get_address_of_preVelocity_18() { return &___preVelocity_18; }
	inline void set_preVelocity_18(Vector3_t4282066566  value)
	{
		___preVelocity_18 = value;
	}

	inline static int32_t get_offset_of_vos_19() { return static_cast<int32_t>(offsetof(LocalAvoidance_t2375621135, ___vos_19)); }
	inline List_1_t3540405845 * get_vos_19() const { return ___vos_19; }
	inline List_1_t3540405845 ** get_address_of_vos_19() { return &___vos_19; }
	inline void set_vos_19(List_1_t3540405845 * value)
	{
		___vos_19 = value;
		Il2CppCodeGenWriteBarrier(&___vos_19, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
