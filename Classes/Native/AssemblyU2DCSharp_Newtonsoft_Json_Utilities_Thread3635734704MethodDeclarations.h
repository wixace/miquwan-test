﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Utilities_ThreadS648438005MethodDeclarations.h"

// System.Void Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Type>::.ctor(System.Func`2<TKey,TValue>)
#define ThreadSafeStore_2__ctor_m3660292291(__this, ___creator0, method) ((  void (*) (ThreadSafeStore_2_t3635734704 *, Func_2_t945728032 *, const MethodInfo*))ThreadSafeStore_2__ctor_m3447883656_gshared)(__this, ___creator0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Type>::Get(TKey)
#define ThreadSafeStore_2_Get_m3600102067(__this, ___key0, method) ((  Type_t * (*) (ThreadSafeStore_2_t3635734704 *, TypeNameKey_t2971844791 , const MethodInfo*))ThreadSafeStore_2_Get_m1825001592_gshared)(__this, ___key0, method)
// TValue Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Type>::AddValue(TKey)
#define ThreadSafeStore_2_AddValue_m1917274047(__this, ___key0, method) ((  Type_t * (*) (ThreadSafeStore_2_t3635734704 *, TypeNameKey_t2971844791 , const MethodInfo*))ThreadSafeStore_2_AddValue_m3087662362_gshared)(__this, ___key0, method)
