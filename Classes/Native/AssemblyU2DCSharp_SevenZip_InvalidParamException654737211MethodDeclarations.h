﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.InvalidParamException
struct InvalidParamException_t654737211;

#include "codegen/il2cpp-codegen.h"

// System.Void SevenZip.InvalidParamException::.ctor()
extern "C"  void InvalidParamException__ctor_m3216219644 (InvalidParamException_t654737211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
