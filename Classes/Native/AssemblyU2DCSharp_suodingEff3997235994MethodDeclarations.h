﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// suodingEff
struct suodingEff_t3997235994;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void suodingEff::.ctor()
extern "C"  void suodingEff__ctor_m2263513793 (suodingEff_t3997235994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void suodingEff::.cctor()
extern "C"  void suodingEff__cctor_m967354636 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void suodingEff::Awake()
extern "C"  void suodingEff_Awake_m2501119012 (suodingEff_t3997235994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void suodingEff::FixedUpdate()
extern "C"  void suodingEff_FixedUpdate_m2672001916 (suodingEff_t3997235994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 suodingEff::ilo_WorldToNgui1(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  suodingEff_ilo_WorldToNgui1_m3820546002 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ____pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
