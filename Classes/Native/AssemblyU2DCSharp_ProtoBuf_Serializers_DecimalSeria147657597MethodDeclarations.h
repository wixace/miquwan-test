﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.DecimalSerializer
struct DecimalSerializer_t147657597;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"

// System.Void ProtoBuf.Serializers.DecimalSerializer::.ctor(ProtoBuf.Meta.TypeModel)
extern "C"  void DecimalSerializer__ctor_m3015245121 (DecimalSerializer_t147657597 * __this, TypeModel_t2730011105 * ___model0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.DecimalSerializer::.cctor()
extern "C"  void DecimalSerializer__cctor_m4223243141 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.DecimalSerializer::ProtoBuf.Serializers.IProtoSerializer.get_RequiresOldValue()
extern "C"  bool DecimalSerializer_ProtoBuf_Serializers_IProtoSerializer_get_RequiresOldValue_m3975687710 (DecimalSerializer_t147657597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.DecimalSerializer::ProtoBuf.Serializers.IProtoSerializer.get_ReturnsValue()
extern "C"  bool DecimalSerializer_ProtoBuf_Serializers_IProtoSerializer_get_ReturnsValue_m718004148 (DecimalSerializer_t147657597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.DecimalSerializer::get_ExpectedType()
extern "C"  Type_t * DecimalSerializer_get_ExpectedType_m2510112465 (DecimalSerializer_t147657597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.DecimalSerializer::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * DecimalSerializer_Read_m624501393 (DecimalSerializer_t147657597 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.DecimalSerializer::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void DecimalSerializer_Write_m125787295 (DecimalSerializer_t147657597 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
