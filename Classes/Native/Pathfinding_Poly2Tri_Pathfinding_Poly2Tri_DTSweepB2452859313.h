﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Poly2Tri.AdvancingFrontNode
struct AdvancingFrontNode_t688967424;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Poly2Tri.DTSweepBasin
struct  DTSweepBasin_t2452859313  : public Il2CppObject
{
public:
	// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.DTSweepBasin::leftNode
	AdvancingFrontNode_t688967424 * ___leftNode_0;
	// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.DTSweepBasin::bottomNode
	AdvancingFrontNode_t688967424 * ___bottomNode_1;
	// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.DTSweepBasin::rightNode
	AdvancingFrontNode_t688967424 * ___rightNode_2;
	// System.Double Pathfinding.Poly2Tri.DTSweepBasin::width
	double ___width_3;
	// System.Boolean Pathfinding.Poly2Tri.DTSweepBasin::leftHighest
	bool ___leftHighest_4;

public:
	inline static int32_t get_offset_of_leftNode_0() { return static_cast<int32_t>(offsetof(DTSweepBasin_t2452859313, ___leftNode_0)); }
	inline AdvancingFrontNode_t688967424 * get_leftNode_0() const { return ___leftNode_0; }
	inline AdvancingFrontNode_t688967424 ** get_address_of_leftNode_0() { return &___leftNode_0; }
	inline void set_leftNode_0(AdvancingFrontNode_t688967424 * value)
	{
		___leftNode_0 = value;
		Il2CppCodeGenWriteBarrier(&___leftNode_0, value);
	}

	inline static int32_t get_offset_of_bottomNode_1() { return static_cast<int32_t>(offsetof(DTSweepBasin_t2452859313, ___bottomNode_1)); }
	inline AdvancingFrontNode_t688967424 * get_bottomNode_1() const { return ___bottomNode_1; }
	inline AdvancingFrontNode_t688967424 ** get_address_of_bottomNode_1() { return &___bottomNode_1; }
	inline void set_bottomNode_1(AdvancingFrontNode_t688967424 * value)
	{
		___bottomNode_1 = value;
		Il2CppCodeGenWriteBarrier(&___bottomNode_1, value);
	}

	inline static int32_t get_offset_of_rightNode_2() { return static_cast<int32_t>(offsetof(DTSweepBasin_t2452859313, ___rightNode_2)); }
	inline AdvancingFrontNode_t688967424 * get_rightNode_2() const { return ___rightNode_2; }
	inline AdvancingFrontNode_t688967424 ** get_address_of_rightNode_2() { return &___rightNode_2; }
	inline void set_rightNode_2(AdvancingFrontNode_t688967424 * value)
	{
		___rightNode_2 = value;
		Il2CppCodeGenWriteBarrier(&___rightNode_2, value);
	}

	inline static int32_t get_offset_of_width_3() { return static_cast<int32_t>(offsetof(DTSweepBasin_t2452859313, ___width_3)); }
	inline double get_width_3() const { return ___width_3; }
	inline double* get_address_of_width_3() { return &___width_3; }
	inline void set_width_3(double value)
	{
		___width_3 = value;
	}

	inline static int32_t get_offset_of_leftHighest_4() { return static_cast<int32_t>(offsetof(DTSweepBasin_t2452859313, ___leftHighest_4)); }
	inline bool get_leftHighest_4() const { return ___leftHighest_4; }
	inline bool* get_address_of_leftHighest_4() { return &___leftHighest_4; }
	inline void set_leftHighest_4(bool value)
	{
		___leftHighest_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
