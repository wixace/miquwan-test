﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_SparseTextureGenerated
struct UnityEngine_SparseTextureGenerated_t4133055580;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_SparseTextureGenerated::.ctor()
extern "C"  void UnityEngine_SparseTextureGenerated__ctor_m369593279 (UnityEngine_SparseTextureGenerated_t4133055580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SparseTextureGenerated::SparseTexture_SparseTexture1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SparseTextureGenerated_SparseTexture_SparseTexture1_m1564486307 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SparseTextureGenerated::SparseTexture_SparseTexture2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SparseTextureGenerated_SparseTexture_SparseTexture2_m2809250788 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SparseTextureGenerated::SparseTexture_tileWidth(JSVCall)
extern "C"  void UnityEngine_SparseTextureGenerated_SparseTexture_tileWidth_m455435350 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SparseTextureGenerated::SparseTexture_tileHeight(JSVCall)
extern "C"  void UnityEngine_SparseTextureGenerated_SparseTexture_tileHeight_m1889037609 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SparseTextureGenerated::SparseTexture_isCreated(JSVCall)
extern "C"  void UnityEngine_SparseTextureGenerated_SparseTexture_isCreated_m367140976 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SparseTextureGenerated::SparseTexture_UnloadTile__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SparseTextureGenerated_SparseTexture_UnloadTile__Int32__Int32__Int32_m2657702550 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SparseTextureGenerated::SparseTexture_UpdateTile__Int32__Int32__Int32__Color32_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SparseTextureGenerated_SparseTexture_UpdateTile__Int32__Int32__Int32__Color32_Array_m4202002674 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SparseTextureGenerated::SparseTexture_UpdateTileRaw__Int32__Int32__Int32__Byte_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SparseTextureGenerated_SparseTexture_UpdateTileRaw__Int32__Int32__Int32__Byte_Array_m432307020 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SparseTextureGenerated::__Register()
extern "C"  void UnityEngine_SparseTextureGenerated___Register_m1938190440 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32[] UnityEngine_SparseTextureGenerated::<SparseTexture_UpdateTile__Int32__Int32__Int32__Color32_Array>m__2D4()
extern "C"  Color32U5BU5D_t2960766953* UnityEngine_SparseTextureGenerated_U3CSparseTexture_UpdateTile__Int32__Int32__Int32__Color32_ArrayU3Em__2D4_m429655769 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine_SparseTextureGenerated::<SparseTexture_UpdateTileRaw__Int32__Int32__Int32__Byte_Array>m__2D5()
extern "C"  ByteU5BU5D_t4260760469* UnityEngine_SparseTextureGenerated_U3CSparseTexture_UpdateTileRaw__Int32__Int32__Int32__Byte_ArrayU3Em__2D5_m1924857092 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SparseTextureGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_SparseTextureGenerated_ilo_getObject1_m600615219 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SparseTextureGenerated::ilo_getInt322(System.Int32)
extern "C"  int32_t UnityEngine_SparseTextureGenerated_ilo_getInt322_m4144847943 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SparseTextureGenerated::ilo_attachFinalizerObject3(System.Int32)
extern "C"  bool UnityEngine_SparseTextureGenerated_ilo_attachFinalizerObject3_m1109110658 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SparseTextureGenerated::ilo_setInt324(System.Int32,System.Int32)
extern "C"  void UnityEngine_SparseTextureGenerated_ilo_setInt324_m4012626452 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SparseTextureGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void UnityEngine_SparseTextureGenerated_ilo_setBooleanS5_m700568940 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_SparseTextureGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_SparseTextureGenerated_ilo_getObject6_m4294556181 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte UnityEngine_SparseTextureGenerated::ilo_getByte7(System.Int32)
extern "C"  uint8_t UnityEngine_SparseTextureGenerated_ilo_getByte7_m2107009550 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
