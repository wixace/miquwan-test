﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<JSCLevelConfig>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m619755647(__this, ___l0, method) ((  void (*) (Enumerator_t2798957822 *, List_1_t2779285052 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<JSCLevelConfig>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3204368499(__this, method) ((  void (*) (Enumerator_t2798957822 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<JSCLevelConfig>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1612491369(__this, method) ((  Il2CppObject * (*) (Enumerator_t2798957822 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<JSCLevelConfig>::Dispose()
#define Enumerator_Dispose_m3879212580(__this, method) ((  void (*) (Enumerator_t2798957822 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<JSCLevelConfig>::VerifyState()
#define Enumerator_VerifyState_m745284445(__this, method) ((  void (*) (Enumerator_t2798957822 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<JSCLevelConfig>::MoveNext()
#define Enumerator_MoveNext_m3080910115(__this, method) ((  bool (*) (Enumerator_t2798957822 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<JSCLevelConfig>::get_Current()
#define Enumerator_get_Current_m3130542902(__this, method) ((  JSCLevelConfig_t1411099500 * (*) (Enumerator_t2798957822 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
