﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.Int2>
struct DefaultComparer_t482291370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.Int2>::.ctor()
extern "C"  void DefaultComparer__ctor_m1726752979_gshared (DefaultComparer_t482291370 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1726752979(__this, method) ((  void (*) (DefaultComparer_t482291370 *, const MethodInfo*))DefaultComparer__ctor_m1726752979_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.Int2>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m3596570500_gshared (DefaultComparer_t482291370 * __this, Int2_t1974045593  ___x0, Int2_t1974045593  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m3596570500(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t482291370 *, Int2_t1974045593 , Int2_t1974045593 , const MethodInfo*))DefaultComparer_Compare_m3596570500_gshared)(__this, ___x0, ___y1, method)
