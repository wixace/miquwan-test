﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._402f8d6f3c0995c0a79e19bb66784e6d
struct _402f8d6f3c0995c0a79e19bb66784e6d_t4265043569;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._402f8d6f3c0995c0a79e19bb66784e6d::.ctor()
extern "C"  void _402f8d6f3c0995c0a79e19bb66784e6d__ctor_m3806949084 (_402f8d6f3c0995c0a79e19bb66784e6d_t4265043569 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._402f8d6f3c0995c0a79e19bb66784e6d::_402f8d6f3c0995c0a79e19bb66784e6dm2(System.Int32)
extern "C"  int32_t _402f8d6f3c0995c0a79e19bb66784e6d__402f8d6f3c0995c0a79e19bb66784e6dm2_m2561438713 (_402f8d6f3c0995c0a79e19bb66784e6d_t4265043569 * __this, int32_t ____402f8d6f3c0995c0a79e19bb66784e6da0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._402f8d6f3c0995c0a79e19bb66784e6d::_402f8d6f3c0995c0a79e19bb66784e6dm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _402f8d6f3c0995c0a79e19bb66784e6d__402f8d6f3c0995c0a79e19bb66784e6dm_m4052328157 (_402f8d6f3c0995c0a79e19bb66784e6d_t4265043569 * __this, int32_t ____402f8d6f3c0995c0a79e19bb66784e6da0, int32_t ____402f8d6f3c0995c0a79e19bb66784e6d841, int32_t ____402f8d6f3c0995c0a79e19bb66784e6dc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
