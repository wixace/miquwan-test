﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_pemayTeresaw151
struct M_pemayTeresaw151_t2371013656;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_pemayTeresaw151::.ctor()
extern "C"  void M_pemayTeresaw151__ctor_m1696529995 (M_pemayTeresaw151_t2371013656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_pemayTeresaw151::M_forjorcemMaircee0(System.String[],System.Int32)
extern "C"  void M_pemayTeresaw151_M_forjorcemMaircee0_m3885729183 (M_pemayTeresaw151_t2371013656 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
