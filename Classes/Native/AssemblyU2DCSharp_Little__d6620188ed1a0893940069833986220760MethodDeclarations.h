﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._d6620188ed1a0893940069833ea5d12e
struct _d6620188ed1a0893940069833ea5d12e_t3986220760;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__d6620188ed1a0893940069833986220760.h"

// System.Void Little._d6620188ed1a0893940069833ea5d12e::.ctor()
extern "C"  void _d6620188ed1a0893940069833ea5d12e__ctor_m2999218773 (_d6620188ed1a0893940069833ea5d12e_t3986220760 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._d6620188ed1a0893940069833ea5d12e::_d6620188ed1a0893940069833ea5d12em2(System.Int32)
extern "C"  int32_t _d6620188ed1a0893940069833ea5d12e__d6620188ed1a0893940069833ea5d12em2_m4156220057 (_d6620188ed1a0893940069833ea5d12e_t3986220760 * __this, int32_t ____d6620188ed1a0893940069833ea5d12ea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._d6620188ed1a0893940069833ea5d12e::_d6620188ed1a0893940069833ea5d12em(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _d6620188ed1a0893940069833ea5d12e__d6620188ed1a0893940069833ea5d12em_m2277958717 (_d6620188ed1a0893940069833ea5d12e_t3986220760 * __this, int32_t ____d6620188ed1a0893940069833ea5d12ea0, int32_t ____d6620188ed1a0893940069833ea5d12e671, int32_t ____d6620188ed1a0893940069833ea5d12ec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._d6620188ed1a0893940069833ea5d12e::ilo__d6620188ed1a0893940069833ea5d12em21(Little._d6620188ed1a0893940069833ea5d12e,System.Int32)
extern "C"  int32_t _d6620188ed1a0893940069833ea5d12e_ilo__d6620188ed1a0893940069833ea5d12em21_m3946770335 (Il2CppObject * __this /* static, unused */, _d6620188ed1a0893940069833ea5d12e_t3986220760 * ____this0, int32_t ____d6620188ed1a0893940069833ea5d12ea1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
