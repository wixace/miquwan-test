﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Bounds>
struct List_1_t4079827401;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Bounds>
struct IEnumerable_1_t1717587510;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Bounds>
struct IEnumerator_1_t328539602;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UnityEngine.Bounds>
struct ICollection_1_t3606231836;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Bounds>
struct ReadOnlyCollection_1_t4268719385;
// UnityEngine.Bounds[]
struct BoundsU5BU5D_t3144995076;
// System.Predicate`1<UnityEngine.Bounds>
struct Predicate_1_t2322698732;
// System.Collections.Generic.IComparer`1<UnityEngine.Bounds>
struct IComparer_1_t991688595;
// System.Comparison`1<UnityEngine.Bounds>
struct Comparison_1_t1428003036;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4099500171.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::.ctor()
extern "C"  void List_1__ctor_m241578967_gshared (List_1_t4079827401 * __this, const MethodInfo* method);
#define List_1__ctor_m241578967(__this, method) ((  void (*) (List_1_t4079827401 *, const MethodInfo*))List_1__ctor_m241578967_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3510039263_gshared (List_1_t4079827401 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3510039263(__this, ___collection0, method) ((  void (*) (List_1_t4079827401 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3510039263_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1497103089_gshared (List_1_t4079827401 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1497103089(__this, ___capacity0, method) ((  void (*) (List_1_t4079827401 *, int32_t, const MethodInfo*))List_1__ctor_m1497103089_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::.cctor()
extern "C"  void List_1__cctor_m2865285645_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m2865285645(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m2865285645_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Bounds>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3568613034_gshared (List_1_t4079827401 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3568613034(__this, method) ((  Il2CppObject* (*) (List_1_t4079827401 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3568613034_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m792951716_gshared (List_1_t4079827401 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m792951716(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4079827401 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m792951716_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Bounds>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1484025395_gshared (List_1_t4079827401 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1484025395(__this, method) ((  Il2CppObject * (*) (List_1_t4079827401 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1484025395_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Bounds>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m3549486954_gshared (List_1_t4079827401 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m3549486954(__this, ___item0, method) ((  int32_t (*) (List_1_t4079827401 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3549486954_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Bounds>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1484846998_gshared (List_1_t4079827401 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1484846998(__this, ___item0, method) ((  bool (*) (List_1_t4079827401 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1484846998_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Bounds>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m2266976450_gshared (List_1_t4079827401 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m2266976450(__this, ___item0, method) ((  int32_t (*) (List_1_t4079827401 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m2266976450_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m4132819509_gshared (List_1_t4079827401 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m4132819509(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4079827401 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m4132819509_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m1800103123_gshared (List_1_t4079827401 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1800103123(__this, ___item0, method) ((  void (*) (List_1_t4079827401 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1800103123_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Bounds>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1050993687_gshared (List_1_t4079827401 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1050993687(__this, method) ((  bool (*) (List_1_t4079827401 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1050993687_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Bounds>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m1203712006_gshared (List_1_t4079827401 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1203712006(__this, method) ((  bool (*) (List_1_t4079827401 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m1203712006_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Bounds>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2821300792_gshared (List_1_t4079827401 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2821300792(__this, method) ((  Il2CppObject * (*) (List_1_t4079827401 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2821300792_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Bounds>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m2854719365_gshared (List_1_t4079827401 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m2854719365(__this, method) ((  bool (*) (List_1_t4079827401 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m2854719365_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Bounds>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1931569876_gshared (List_1_t4079827401 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1931569876(__this, method) ((  bool (*) (List_1_t4079827401 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1931569876_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Bounds>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m2211308991_gshared (List_1_t4079827401 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m2211308991(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t4079827401 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m2211308991_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3703719116_gshared (List_1_t4079827401 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3703719116(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4079827401 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3703719116_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::Add(T)
extern "C"  void List_1_Add_m4230907079_gshared (List_1_t4079827401 * __this, Bounds_t2711641849  ___item0, const MethodInfo* method);
#define List_1_Add_m4230907079(__this, ___item0, method) ((  void (*) (List_1_t4079827401 *, Bounds_t2711641849 , const MethodInfo*))List_1_Add_m4230907079_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m3398573978_gshared (List_1_t4079827401 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m3398573978(__this, ___newCount0, method) ((  void (*) (List_1_t4079827401 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m3398573978_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m981213197_gshared (List_1_t4079827401 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m981213197(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t4079827401 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m981213197_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m51220120_gshared (List_1_t4079827401 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m51220120(__this, ___collection0, method) ((  void (*) (List_1_t4079827401 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m51220120_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3607159640_gshared (List_1_t4079827401 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3607159640(__this, ___enumerable0, method) ((  void (*) (List_1_t4079827401 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3607159640_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m2061849663_gshared (List_1_t4079827401 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m2061849663(__this, ___collection0, method) ((  void (*) (List_1_t4079827401 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2061849663_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Bounds>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t4268719385 * List_1_AsReadOnly_m427544166_gshared (List_1_t4079827401 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m427544166(__this, method) ((  ReadOnlyCollection_1_t4268719385 * (*) (List_1_t4079827401 *, const MethodInfo*))List_1_AsReadOnly_m427544166_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Bounds>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m3539254957_gshared (List_1_t4079827401 * __this, Bounds_t2711641849  ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m3539254957(__this, ___item0, method) ((  int32_t (*) (List_1_t4079827401 *, Bounds_t2711641849 , const MethodInfo*))List_1_BinarySearch_m3539254957_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::Clear()
extern "C"  void List_1_Clear_m1942679554_gshared (List_1_t4079827401 * __this, const MethodInfo* method);
#define List_1_Clear_m1942679554(__this, method) ((  void (*) (List_1_t4079827401 *, const MethodInfo*))List_1_Clear_m1942679554_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Bounds>::Contains(T)
extern "C"  bool List_1_Contains_m1523779517_gshared (List_1_t4079827401 * __this, Bounds_t2711641849  ___item0, const MethodInfo* method);
#define List_1_Contains_m1523779517(__this, ___item0, method) ((  bool (*) (List_1_t4079827401 *, Bounds_t2711641849 , const MethodInfo*))List_1_Contains_m1523779517_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m461350159_gshared (List_1_t4079827401 * __this, BoundsU5BU5D_t3144995076* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m461350159(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t4079827401 *, BoundsU5BU5D_t3144995076*, int32_t, const MethodInfo*))List_1_CopyTo_m461350159_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UnityEngine.Bounds>::Find(System.Predicate`1<T>)
extern "C"  Bounds_t2711641849  List_1_Find_m4117674455_gshared (List_1_t4079827401 * __this, Predicate_1_t2322698732 * ___match0, const MethodInfo* method);
#define List_1_Find_m4117674455(__this, ___match0, method) ((  Bounds_t2711641849  (*) (List_1_t4079827401 *, Predicate_1_t2322698732 *, const MethodInfo*))List_1_Find_m4117674455_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m2010757812_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t2322698732 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m2010757812(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t2322698732 *, const MethodInfo*))List_1_CheckMatch_m2010757812_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Bounds>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m2547922833_gshared (List_1_t4079827401 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t2322698732 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m2547922833(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t4079827401 *, int32_t, int32_t, Predicate_1_t2322698732 *, const MethodInfo*))List_1_GetIndex_m2547922833_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Bounds>::GetEnumerator()
extern "C"  Enumerator_t4099500171  List_1_GetEnumerator_m3460506234_gshared (List_1_t4079827401 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m3460506234(__this, method) ((  Enumerator_t4099500171  (*) (List_1_t4079827401 *, const MethodInfo*))List_1_GetEnumerator_m3460506234_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Bounds>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2789623323_gshared (List_1_t4079827401 * __this, Bounds_t2711641849  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2789623323(__this, ___item0, method) ((  int32_t (*) (List_1_t4079827401 *, Bounds_t2711641849 , const MethodInfo*))List_1_IndexOf_m2789623323_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m1684339814_gshared (List_1_t4079827401 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m1684339814(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t4079827401 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m1684339814_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m207717343_gshared (List_1_t4079827401 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m207717343(__this, ___index0, method) ((  void (*) (List_1_t4079827401 *, int32_t, const MethodInfo*))List_1_CheckIndex_m207717343_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2677125958_gshared (List_1_t4079827401 * __this, int32_t ___index0, Bounds_t2711641849  ___item1, const MethodInfo* method);
#define List_1_Insert_m2677125958(__this, ___index0, ___item1, method) ((  void (*) (List_1_t4079827401 *, int32_t, Bounds_t2711641849 , const MethodInfo*))List_1_Insert_m2677125958_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m2504563515_gshared (List_1_t4079827401 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m2504563515(__this, ___collection0, method) ((  void (*) (List_1_t4079827401 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2504563515_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Bounds>::Remove(T)
extern "C"  bool List_1_Remove_m1672516344_gshared (List_1_t4079827401 * __this, Bounds_t2711641849  ___item0, const MethodInfo* method);
#define List_1_Remove_m1672516344(__this, ___item0, method) ((  bool (*) (List_1_t4079827401 *, Bounds_t2711641849 , const MethodInfo*))List_1_Remove_m1672516344_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Bounds>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m2919479198_gshared (List_1_t4079827401 * __this, Predicate_1_t2322698732 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m2919479198(__this, ___match0, method) ((  int32_t (*) (List_1_t4079827401 *, Predicate_1_t2322698732 *, const MethodInfo*))List_1_RemoveAll_m2919479198_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m550978828_gshared (List_1_t4079827401 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m550978828(__this, ___index0, method) ((  void (*) (List_1_t4079827401 *, int32_t, const MethodInfo*))List_1_RemoveAt_m550978828_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m88049967_gshared (List_1_t4079827401 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m88049967(__this, ___index0, ___count1, method) ((  void (*) (List_1_t4079827401 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m88049967_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::Reverse()
extern "C"  void List_1_Reverse_m727433696_gshared (List_1_t4079827401 * __this, const MethodInfo* method);
#define List_1_Reverse_m727433696(__this, method) ((  void (*) (List_1_t4079827401 *, const MethodInfo*))List_1_Reverse_m727433696_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::Sort()
extern "C"  void List_1_Sort_m3017918914_gshared (List_1_t4079827401 * __this, const MethodInfo* method);
#define List_1_Sort_m3017918914(__this, method) ((  void (*) (List_1_t4079827401 *, const MethodInfo*))List_1_Sort_m3017918914_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m4266384994_gshared (List_1_t4079827401 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m4266384994(__this, ___comparer0, method) ((  void (*) (List_1_t4079827401 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m4266384994_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m4187043989_gshared (List_1_t4079827401 * __this, Comparison_1_t1428003036 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m4187043989(__this, ___comparison0, method) ((  void (*) (List_1_t4079827401 *, Comparison_1_t1428003036 *, const MethodInfo*))List_1_Sort_m4187043989_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Bounds>::ToArray()
extern "C"  BoundsU5BU5D_t3144995076* List_1_ToArray_m720994361_gshared (List_1_t4079827401 * __this, const MethodInfo* method);
#define List_1_ToArray_m720994361(__this, method) ((  BoundsU5BU5D_t3144995076* (*) (List_1_t4079827401 *, const MethodInfo*))List_1_ToArray_m720994361_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::TrimExcess()
extern "C"  void List_1_TrimExcess_m1246695387_gshared (List_1_t4079827401 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m1246695387(__this, method) ((  void (*) (List_1_t4079827401 *, const MethodInfo*))List_1_TrimExcess_m1246695387_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Bounds>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m3478108363_gshared (List_1_t4079827401 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m3478108363(__this, method) ((  int32_t (*) (List_1_t4079827401 *, const MethodInfo*))List_1_get_Capacity_m3478108363_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m608762796_gshared (List_1_t4079827401 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m608762796(__this, ___value0, method) ((  void (*) (List_1_t4079827401 *, int32_t, const MethodInfo*))List_1_set_Capacity_m608762796_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Bounds>::get_Count()
extern "C"  int32_t List_1_get_Count_m686005993_gshared (List_1_t4079827401 * __this, const MethodInfo* method);
#define List_1_get_Count_m686005993(__this, method) ((  int32_t (*) (List_1_t4079827401 *, const MethodInfo*))List_1_get_Count_m686005993_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Bounds>::get_Item(System.Int32)
extern "C"  Bounds_t2711641849  List_1_get_Item_m3084665932_gshared (List_1_t4079827401 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3084665932(__this, ___index0, method) ((  Bounds_t2711641849  (*) (List_1_t4079827401 *, int32_t, const MethodInfo*))List_1_get_Item_m3084665932_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Bounds>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m1842231965_gshared (List_1_t4079827401 * __this, int32_t ___index0, Bounds_t2711641849  ___value1, const MethodInfo* method);
#define List_1_set_Item_m1842231965(__this, ___index0, ___value1, method) ((  void (*) (List_1_t4079827401 *, int32_t, Bounds_t2711641849 , const MethodInfo*))List_1_set_Item_m1842231965_gshared)(__this, ___index0, ___value1, method)
