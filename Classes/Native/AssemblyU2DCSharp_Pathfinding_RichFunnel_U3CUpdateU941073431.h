﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.MeshNode
struct MeshNode_t3005053445;
// Pathfinding.RichFunnel
struct RichFunnel_t2453525928;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RichFunnel/<Update>c__AnonStorey101
struct  U3CUpdateU3Ec__AnonStorey101_t941073431  : public Il2CppObject
{
public:
	// System.Int32 Pathfinding.RichFunnel/<Update>c__AnonStorey101::containingIndex
	int32_t ___containingIndex_0;
	// Pathfinding.Int3 Pathfinding.RichFunnel/<Update>c__AnonStorey101::i3Copy
	Int3_t1974045594  ___i3Copy_1;
	// Pathfinding.MeshNode Pathfinding.RichFunnel/<Update>c__AnonStorey101::containingPoint
	MeshNode_t3005053445 * ___containingPoint_2;
	// Pathfinding.RichFunnel Pathfinding.RichFunnel/<Update>c__AnonStorey101::<>f__this
	RichFunnel_t2453525928 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_containingIndex_0() { return static_cast<int32_t>(offsetof(U3CUpdateU3Ec__AnonStorey101_t941073431, ___containingIndex_0)); }
	inline int32_t get_containingIndex_0() const { return ___containingIndex_0; }
	inline int32_t* get_address_of_containingIndex_0() { return &___containingIndex_0; }
	inline void set_containingIndex_0(int32_t value)
	{
		___containingIndex_0 = value;
	}

	inline static int32_t get_offset_of_i3Copy_1() { return static_cast<int32_t>(offsetof(U3CUpdateU3Ec__AnonStorey101_t941073431, ___i3Copy_1)); }
	inline Int3_t1974045594  get_i3Copy_1() const { return ___i3Copy_1; }
	inline Int3_t1974045594 * get_address_of_i3Copy_1() { return &___i3Copy_1; }
	inline void set_i3Copy_1(Int3_t1974045594  value)
	{
		___i3Copy_1 = value;
	}

	inline static int32_t get_offset_of_containingPoint_2() { return static_cast<int32_t>(offsetof(U3CUpdateU3Ec__AnonStorey101_t941073431, ___containingPoint_2)); }
	inline MeshNode_t3005053445 * get_containingPoint_2() const { return ___containingPoint_2; }
	inline MeshNode_t3005053445 ** get_address_of_containingPoint_2() { return &___containingPoint_2; }
	inline void set_containingPoint_2(MeshNode_t3005053445 * value)
	{
		___containingPoint_2 = value;
		Il2CppCodeGenWriteBarrier(&___containingPoint_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CUpdateU3Ec__AnonStorey101_t941073431, ___U3CU3Ef__this_3)); }
	inline RichFunnel_t2453525928 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline RichFunnel_t2453525928 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(RichFunnel_t2453525928 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
