//
//  MQWPlatform.h
//  MiquwanSDK
//
//  Created by MiQuWang on 2019/10/30.
//  Copyright © 2019 LQX. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MQWOrder.h"
#import "MQWRole.h"

NS_ASSUME_NONNULL_BEGIN

@interface MQWPlatform : NSObject


/// 激活平台
/// @param appId     用户注册的appId
/// @param code      ISO的货币代码（人民币CNY 港元HKD 新台币TWD 欧元EUR 美元USD 英镑GBP 日元JPY）
+ (void)activePlatformWithAppId:(NSString *)appId currencyISO:(nullable NSString *)code;
/// 登录平台
/// @param needAutoLogin 是否需要自动登录
+ (void)loginPlatformNeedAutoLogin:(BOOL)needAutoLogin;
/// 退出平台
+ (void)logoutPlatform;

/// 打开用户管理界面
+ (void)openUserManagePage;

/// 购买游戏产品
/// @param order 产品信息
/// @param role  角色信息
+ (void)purchaseOrder:(nonnull MQWOrder *)order role:(nullable MQWRole *)role;

/// 上传游戏角色
/// @param role    角色模型
/// @param handler 结果回调
+ (void)uploadRole:(MQWRole *)role handler:(void (^)(BOOL, NSError * _Nullable))handler;
@end

NS_ASSUME_NONNULL_END
