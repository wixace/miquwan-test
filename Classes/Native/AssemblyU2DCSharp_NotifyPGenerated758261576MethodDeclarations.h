﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NotifyPGenerated
struct NotifyPGenerated_t758261576;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// NotifyP
struct NotifyP_t3794004487;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_NotifyP3794004487.h"

// System.Void NotifyPGenerated::.ctor()
extern "C"  void NotifyPGenerated__ctor_m1481027667 (NotifyPGenerated_t758261576 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NotifyPGenerated::NotifyP_NotifyP1(JSVCall,System.Int32)
extern "C"  bool NotifyPGenerated_NotifyP_NotifyP1_m1527400951 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotifyPGenerated::NotifyP_SCEND_DESTROY_MESSAGE(JSVCall)
extern "C"  void NotifyPGenerated_NotifyP_SCEND_DESTROY_MESSAGE_m3439511360 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotifyPGenerated::NotifyP_ID(JSVCall)
extern "C"  void NotifyPGenerated_NotifyP_ID_m3833383203 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotifyPGenerated::__Register()
extern "C"  void NotifyPGenerated___Register_m732629076 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotifyPGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void NotifyPGenerated_ilo_addJSCSRel1_m1000855951 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 NotifyPGenerated::ilo_getUInt322(System.Int32)
extern "C"  uint32_t NotifyPGenerated_ilo_getUInt322_m2593061651 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NotifyPGenerated::ilo_set_ID3(NotifyP,System.UInt32)
extern "C"  void NotifyPGenerated_ilo_set_ID3_m1664922822 (Il2CppObject * __this /* static, unused */, NotifyP_t3794004487 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
