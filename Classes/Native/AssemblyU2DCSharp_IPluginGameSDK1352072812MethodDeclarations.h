﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// IPluginGameSDK
struct IPluginGameSDK_t1352072812;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void IPluginGameSDK::.ctor()
extern "C"  void IPluginGameSDK__ctor_m1266173871 (IPluginGameSDK_t1352072812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPluginGameSDK::Init()
extern "C"  void IPluginGameSDK_Init_m1793496261 (IPluginGameSDK_t1352072812 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void IPluginGameSDK::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void IPluginGameSDK_ilo_AddEventListener1_m759197541 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
