﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GesturesMgr
struct GesturesMgr_t1320398158;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// TapGesture
struct TapGesture_t659145798;
// Pathfinding.Path
struct Path_t1974241691;
// PinchGesture
struct PinchGesture_t1502590799;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;
// UnityEngine.Camera
struct Camera_t2727095145;
// UICamera
struct UICamera_t189364953;
// HeroMgr
struct HeroMgr_t2475965342;
// TeamSkillMgr
struct TeamSkillMgr_t2030650276;
// CombatEntity
struct CombatEntity_t684137495;
// OnPathDelegate
struct OnPathDelegate_t598607977;
// Hero
struct Hero_t2245658;
// FightCtrl
struct FightCtrl_t648967803;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_TapGesture659145798.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_PinchGesture1502590799.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_UICamera189364953.h"
#include "AssemblyU2DCSharp_GesturesMgr1320398158.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitCamp642829979.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_HeroMgr2475965342.h"
#include "AssemblyU2DCSharp_OnPathDelegate598607977.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_FightCtrl648967803.h"

// System.Void GesturesMgr::.ctor()
extern "C"  void GesturesMgr__ctor_m3987400157 (GesturesMgr_t1320398158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GesturesMgr::get_DefaultFov()
extern "C"  float GesturesMgr_get_DefaultFov_m2921434384 (GesturesMgr_t1320398158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::set_DefaultFov(System.Single)
extern "C"  void GesturesMgr_set_DefaultFov_m2385610459 (GesturesMgr_t1320398158 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::Awake()
extern "C"  void GesturesMgr_Awake_m4225005376 (GesturesMgr_t1320398158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::OnLoadScene(CEvent.ZEvent)
extern "C"  void GesturesMgr_OnLoadScene_m4178541017 (GesturesMgr_t1320398158 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GesturesMgr::ShouldProcessTouch(System.Int32,UnityEngine.Vector2)
extern "C"  bool GesturesMgr_ShouldProcessTouch_m345183227 (GesturesMgr_t1320398158 * __this, int32_t ___fingerIndex0, Vector2_t4282066565  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::Start()
extern "C"  void GesturesMgr_Start_m2934537949 (GesturesMgr_t1320398158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::Update()
extern "C"  void GesturesMgr_Update_m782215376 (GesturesMgr_t1320398158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::OnTap(TapGesture)
extern "C"  void GesturesMgr_OnTap_m207807545 (GesturesMgr_t1320398158 * __this, TapGesture_t659145798 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::OnTap(UnityEngine.Vector3)
extern "C"  void GesturesMgr_OnTap_m705269306 (GesturesMgr_t1320398158 * __this, Vector3_t4282066566  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::OnPathComplete(Pathfinding.Path)
extern "C"  void GesturesMgr_OnPathComplete_m3455251605 (GesturesMgr_t1320398158 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::OnDoubleTap(TapGesture)
extern "C"  void GesturesMgr_OnDoubleTap_m1484063720 (GesturesMgr_t1320398158 * __this, TapGesture_t659145798 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::OnPinch(PinchGesture)
extern "C"  void GesturesMgr_OnPinch_m2599432935 (GesturesMgr_t1320398158 * __this, PinchGesture_t1502590799 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::SetPinch(System.Boolean)
extern "C"  void GesturesMgr_SetPinch_m2452492150 (GesturesMgr_t1320398158 * __this, bool ___isAllow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject GesturesMgr::GetSelectedGameObject(System.Int32)
extern "C"  GameObject_t3674682005 * GesturesMgr_GetSelectedGameObject_m1176155587 (GesturesMgr_t1320398158 * __this, int32_t ___layermask0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 GesturesMgr::GetHitPosOfScreenPoint()
extern "C"  Vector3_t4282066566  GesturesMgr_GetHitPosOfScreenPoint_m1927194179 (GesturesMgr_t1320398158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GesturesMgr::get_IdealZoomAmount()
extern "C"  float GesturesMgr_get_IdealZoomAmount_m1831957214 (GesturesMgr_t1320398158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::set_IdealZoomAmount(System.Single)
extern "C"  void GesturesMgr_set_IdealZoomAmount_m1106671437 (GesturesMgr_t1320398158 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GesturesMgr::get_ZoomAmount()
extern "C"  float GesturesMgr_get_ZoomAmount_m3344278671 (GesturesMgr_t1320398158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::set_ZoomAmount(System.Single)
extern "C"  void GesturesMgr_set_ZoomAmount_m60937660 (GesturesMgr_t1320398158 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GesturesMgr::get_CameraFov()
extern "C"  float GesturesMgr_get_CameraFov_m2013362630 (GesturesMgr_t1320398158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::set_CameraFov(System.Single)
extern "C"  void GesturesMgr_set_CameraFov_m2938485093 (GesturesMgr_t1320398158 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GesturesMgr::get_ZoomPercent()
extern "C"  float GesturesMgr_get_ZoomPercent_m332598672 (GesturesMgr_t1320398158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::SetZoomAmount(System.Single,System.Single)
extern "C"  void GesturesMgr_SetZoomAmount_m3812264584 (GesturesMgr_t1320398158 * __this, float ___min0, float ___max1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::OnDestroy()
extern "C"  void GesturesMgr_OnDestroy_m1068270678 (GesturesMgr_t1320398158 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject GesturesMgr::ilo_FindChild1(UnityEngine.GameObject,System.String)
extern "C"  GameObject_t3674682005 * GesturesMgr_ilo_FindChild1_m1592104391 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera GesturesMgr::ilo_get_cachedCamera2(UICamera)
extern "C"  Camera_t2727095145 * GesturesMgr_ilo_get_cachedCamera2_m3522889522 (Il2CppObject * __this /* static, unused */, UICamera_t189364953 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GesturesMgr::ilo_get_CameraFov3(GesturesMgr)
extern "C"  float GesturesMgr_ilo_get_CameraFov3_m622630818 (Il2CppObject * __this /* static, unused */, GesturesMgr_t1320398158 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GesturesMgr::ilo_get_ZoomAmount4(GesturesMgr)
extern "C"  float GesturesMgr_ilo_get_ZoomAmount4_m113808976 (Il2CppObject * __this /* static, unused */, GesturesMgr_t1320398158 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::ilo_set_ZoomAmount5(GesturesMgr,System.Single)
extern "C"  void GesturesMgr_ilo_set_ZoomAmount5_m1352132180 (Il2CppObject * __this /* static, unused */, GesturesMgr_t1320398158 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GesturesMgr::ilo_get_isOverUI6()
extern "C"  bool GesturesMgr_ilo_get_isOverUI6_m3662994967 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr GesturesMgr::ilo_get_instance7()
extern "C"  HeroMgr_t2475965342 * GesturesMgr_ilo_get_instance7_m3140421910 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TeamSkillMgr GesturesMgr::ilo_get_TeamSkillMgr8()
extern "C"  TeamSkillMgr_t2030650276 * GesturesMgr_ilo_get_TeamSkillMgr8_m466175436 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitCamp GesturesMgr::ilo_get_unitCamp9(CombatEntity)
extern "C"  int32_t GesturesMgr_ilo_get_unitCamp9_m3735471808 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::ilo_FocusAttack10(HeroMgr,CombatEntity)
extern "C"  void GesturesMgr_ilo_FocusAttack10_m1324561218 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, CombatEntity_t684137495 * ___unit1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::ilo_RequestAstarPath11(CombatEntity,UnityEngine.Vector3,OnPathDelegate)
extern "C"  void GesturesMgr_ilo_RequestAstarPath11_m1147864958 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, Vector3_t4282066566  ___pos1, OnPathDelegate_t598607977 * ___onPathComplete2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero GesturesMgr::ilo_GetCaptain12(HeroMgr)
extern "C"  Hero_t2245658 * GesturesMgr_ilo_GetCaptain12_m3387799310 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::ilo_RemovePathCallback13(CombatEntity,OnPathDelegate)
extern "C"  void GesturesMgr_ilo_RemovePathCallback13_m1534548298 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, OnPathDelegate_t598607977 * ___onPathComplete1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero GesturesMgr::ilo_get_captain14(HeroMgr)
extern "C"  Hero_t2245658 * GesturesMgr_ilo_get_captain14_m3186143595 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::ilo_UpdateTargetPos15(HeroMgr,UnityEngine.Vector3,System.Boolean,System.Boolean,Entity.Behavior.EBehaviorID)
extern "C"  void GesturesMgr_ilo_UpdateTargetPos15_m1799748204 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, Vector3_t4282066566  ___targetPos1, bool ___isDoubleClick2, bool ___isFixedSpeed3, uint8_t ___bev4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::ilo_BreakSkill16(FightCtrl,System.Boolean)
extern "C"  void GesturesMgr_ilo_BreakSkill16_m2669042871 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, bool ___isForce1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single GesturesMgr::ilo_Centimeters17(System.Single)
extern "C"  float GesturesMgr_ilo_Centimeters17_m2555396966 (Il2CppObject * __this /* static, unused */, float ___valueInPixels0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GesturesMgr::ilo_set_CameraFov18(GesturesMgr,System.Single)
extern "C"  void GesturesMgr_ilo_set_CameraFov18_m3883026223 (Il2CppObject * __this /* static, unused */, GesturesMgr_t1320398158 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
