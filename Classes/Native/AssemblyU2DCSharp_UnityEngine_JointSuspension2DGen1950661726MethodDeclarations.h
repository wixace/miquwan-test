﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_JointSuspension2DGenerated
struct UnityEngine_JointSuspension2DGenerated_t1950661726;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_JointSuspension2DGenerated::.ctor()
extern "C"  void UnityEngine_JointSuspension2DGenerated__ctor_m3361165821 (UnityEngine_JointSuspension2DGenerated_t1950661726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointSuspension2DGenerated::.cctor()
extern "C"  void UnityEngine_JointSuspension2DGenerated__cctor_m634829136 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_JointSuspension2DGenerated::JointSuspension2D_JointSuspension2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_JointSuspension2DGenerated_JointSuspension2D_JointSuspension2D1_m2861164585 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointSuspension2DGenerated::JointSuspension2D_dampingRatio(JSVCall)
extern "C"  void UnityEngine_JointSuspension2DGenerated_JointSuspension2D_dampingRatio_m46672497 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointSuspension2DGenerated::JointSuspension2D_frequency(JSVCall)
extern "C"  void UnityEngine_JointSuspension2DGenerated_JointSuspension2D_frequency_m3849527606 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointSuspension2DGenerated::JointSuspension2D_angle(JSVCall)
extern "C"  void UnityEngine_JointSuspension2DGenerated_JointSuspension2D_angle_m512739743 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointSuspension2DGenerated::__Register()
extern "C"  void UnityEngine_JointSuspension2DGenerated___Register_m602857706 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_JointSuspension2DGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_JointSuspension2DGenerated_ilo_getObject1_m3946061493 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointSuspension2DGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_JointSuspension2DGenerated_ilo_addJSCSRel2_m2793296570 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointSuspension2DGenerated::ilo_changeJSObj3(System.Int32,System.Object)
extern "C"  void UnityEngine_JointSuspension2DGenerated_ilo_changeJSObj3_m2800158350 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
