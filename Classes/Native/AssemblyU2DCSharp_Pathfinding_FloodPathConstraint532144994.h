﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.FloodPath
struct FloodPath_t3766979749;

#include "AssemblyU2DCSharp_Pathfinding_NNConstraint758567699.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.FloodPathConstraint
struct  FloodPathConstraint_t532144994  : public NNConstraint_t758567699
{
public:
	// Pathfinding.FloodPath Pathfinding.FloodPathConstraint::path
	FloodPath_t3766979749 * ___path_9;

public:
	inline static int32_t get_offset_of_path_9() { return static_cast<int32_t>(offsetof(FloodPathConstraint_t532144994, ___path_9)); }
	inline FloodPath_t3766979749 * get_path_9() const { return ___path_9; }
	inline FloodPath_t3766979749 ** get_address_of_path_9() { return &___path_9; }
	inline void set_path_9(FloodPath_t3766979749 * value)
	{
		___path_9 = value;
		Il2CppCodeGenWriteBarrier(&___path_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
