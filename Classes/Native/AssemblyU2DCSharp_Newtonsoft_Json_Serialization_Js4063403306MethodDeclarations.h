﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonFormatterConverter
struct JsonFormatterConverter_t4063403306;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.String
struct String_t;
// Newtonsoft.Json.Linq.JValue
struct JValue_t3413677367;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer251850770.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_TypeCode1814089915.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JValue3413677367.h"

// System.Void Newtonsoft.Json.Serialization.JsonFormatterConverter::.ctor(Newtonsoft.Json.JsonSerializer)
extern "C"  void JsonFormatterConverter__ctor_m311464059 (JsonFormatterConverter_t4063403306 * __this, JsonSerializer_t251850770 * ___serializer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonFormatterConverter::Convert(System.Object,System.Type)
extern "C"  Il2CppObject * JsonFormatterConverter_Convert_m1444438209 (JsonFormatterConverter_t4063403306 * __this, Il2CppObject * ___value0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonFormatterConverter::Convert(System.Object,System.TypeCode)
extern "C"  Il2CppObject * JsonFormatterConverter_Convert_m3744101460 (JsonFormatterConverter_t4063403306 * __this, Il2CppObject * ___value0, int32_t ___typeCode1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonFormatterConverter::ToBoolean(System.Object)
extern "C"  bool JsonFormatterConverter_ToBoolean_m1507473427 (JsonFormatterConverter_t4063403306 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte Newtonsoft.Json.Serialization.JsonFormatterConverter::ToByte(System.Object)
extern "C"  uint8_t JsonFormatterConverter_ToByte_m239339211 (JsonFormatterConverter_t4063403306 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char Newtonsoft.Json.Serialization.JsonFormatterConverter::ToChar(System.Object)
extern "C"  Il2CppChar JsonFormatterConverter_ToChar_m3651857419 (JsonFormatterConverter_t4063403306 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Newtonsoft.Json.Serialization.JsonFormatterConverter::ToDateTime(System.Object)
extern "C"  DateTime_t4283661327  JsonFormatterConverter_ToDateTime_m2397228651 (JsonFormatterConverter_t4063403306 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal Newtonsoft.Json.Serialization.JsonFormatterConverter::ToDecimal(System.Object)
extern "C"  Decimal_t1954350631  JsonFormatterConverter_ToDecimal_m3305177985 (JsonFormatterConverter_t4063403306 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Newtonsoft.Json.Serialization.JsonFormatterConverter::ToDouble(System.Object)
extern "C"  double JsonFormatterConverter_ToDouble_m862494443 (JsonFormatterConverter_t4063403306 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 Newtonsoft.Json.Serialization.JsonFormatterConverter::ToInt16(System.Object)
extern "C"  int16_t JsonFormatterConverter_ToInt16_m1919154363 (JsonFormatterConverter_t4063403306 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Serialization.JsonFormatterConverter::ToInt32(System.Object)
extern "C"  int32_t JsonFormatterConverter_ToInt32_m935115591 (JsonFormatterConverter_t4063403306 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.Serialization.JsonFormatterConverter::ToInt64(System.Object)
extern "C"  int64_t JsonFormatterConverter_ToInt64_m878402313 (JsonFormatterConverter_t4063403306 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte Newtonsoft.Json.Serialization.JsonFormatterConverter::ToSByte(System.Object)
extern "C"  int8_t JsonFormatterConverter_ToSByte_m2370394541 (JsonFormatterConverter_t4063403306 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Newtonsoft.Json.Serialization.JsonFormatterConverter::ToSingle(System.Object)
extern "C"  float JsonFormatterConverter_ToSingle_m542698955 (JsonFormatterConverter_t4063403306 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonFormatterConverter::ToString(System.Object)
extern "C"  String_t* JsonFormatterConverter_ToString_m2966759147 (JsonFormatterConverter_t4063403306 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 Newtonsoft.Json.Serialization.JsonFormatterConverter::ToUInt16(System.Object)
extern "C"  uint16_t JsonFormatterConverter_ToUInt16_m3470622891 (JsonFormatterConverter_t4063403306 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Newtonsoft.Json.Serialization.JsonFormatterConverter::ToUInt32(System.Object)
extern "C"  uint32_t JsonFormatterConverter_ToUInt32_m2301429739 (JsonFormatterConverter_t4063403306 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Newtonsoft.Json.Serialization.JsonFormatterConverter::ToUInt64(System.Object)
extern "C"  uint64_t JsonFormatterConverter_ToUInt64_m3866776523 (JsonFormatterConverter_t4063403306 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonFormatterConverter::ilo_get_Value1(Newtonsoft.Json.Linq.JValue)
extern "C"  Il2CppObject * JsonFormatterConverter_ilo_get_Value1_m1607340935 (Il2CppObject * __this /* static, unused */, JValue_t3413677367 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
