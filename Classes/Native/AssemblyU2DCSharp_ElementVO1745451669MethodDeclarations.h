﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ElementVO
struct ElementVO_t1745451669;
// elementCfg
struct elementCfg_t575911880;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_elementCfg575911880.h"

// System.Void ElementVO::.ctor(elementCfg)
extern "C"  void ElementVO__ctor_m2010134382 (ElementVO_t1745451669 * __this, elementCfg_t575911880 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
