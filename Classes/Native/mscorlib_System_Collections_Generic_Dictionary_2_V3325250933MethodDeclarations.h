﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va867596585MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<CombatEntity,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1847089736(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t3325250933 *, Dictionary_2_t329677924 *, const MethodInfo*))ValueCollection__ctor_m3711676323_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<CombatEntity,System.Single>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m483227882(__this, ___item0, method) ((  void (*) (ValueCollection_t3325250933 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m3314322479_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<CombatEntity,System.Single>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3555224243(__this, method) ((  void (*) (ValueCollection_t3325250933 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3470125176_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<CombatEntity,System.Single>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m897788224(__this, ___item0, method) ((  bool (*) (ValueCollection_t3325250933 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m489571671_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<CombatEntity,System.Single>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2106954213(__this, ___item0, method) ((  bool (*) (ValueCollection_t3325250933 *, float, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2830550972_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<CombatEntity,System.Single>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m749348467(__this, method) ((  Il2CppObject* (*) (ValueCollection_t3325250933 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m2329808262_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<CombatEntity,System.Single>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m738953207(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3325250933 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m1644057468_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<CombatEntity,System.Single>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1872892870(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3325250933 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3441146999_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<CombatEntity,System.Single>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3467931379(__this, method) ((  bool (*) (ValueCollection_t3325250933 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m3059714826_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<CombatEntity,System.Single>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3905636691(__this, method) ((  bool (*) (ValueCollection_t3325250933 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m501649386_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<CombatEntity,System.Single>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m4020915717(__this, method) ((  Il2CppObject * (*) (ValueCollection_t3325250933 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2031938262_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<CombatEntity,System.Single>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m3516714127(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t3325250933 *, SingleU5BU5D_t2316563989*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m3682165546_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<CombatEntity,System.Single>::GetEnumerator()
#define ValueCollection_GetEnumerator_m3024380728(__this, method) ((  Enumerator_t2556478628  (*) (ValueCollection_t3325250933 *, const MethodInfo*))ValueCollection_GetEnumerator_m2317742477_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<CombatEntity,System.Single>::get_Count()
#define ValueCollection_get_Count_m3293311885(__this, method) ((  int32_t (*) (ValueCollection_t3325250933 *, const MethodInfo*))ValueCollection_get_Count_m1032615472_gshared)(__this, method)
