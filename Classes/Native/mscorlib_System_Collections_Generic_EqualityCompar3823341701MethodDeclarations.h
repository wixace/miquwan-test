﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct DefaultComparer_t3823341701;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeB1080597738.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Rendering.ReflectionProbeBlendInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m2938174079_gshared (DefaultComparer_t3823341701 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2938174079(__this, method) ((  void (*) (DefaultComparer_t3823341701 *, const MethodInfo*))DefaultComparer__ctor_m2938174079_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Rendering.ReflectionProbeBlendInfo>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m22001812_gshared (DefaultComparer_t3823341701 * __this, ReflectionProbeBlendInfo_t1080597738  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m22001812(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t3823341701 *, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))DefaultComparer_GetHashCode_m22001812_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2974474132_gshared (DefaultComparer_t3823341701 * __this, ReflectionProbeBlendInfo_t1080597738  ___x0, ReflectionProbeBlendInfo_t1080597738  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2974474132(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t3823341701 *, ReflectionProbeBlendInfo_t1080597738 , ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))DefaultComparer_Equals_m2974474132_gshared)(__this, ___x0, ___y1, method)
