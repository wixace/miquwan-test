﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ABCheckUpdate
struct ABCheckUpdate_t527832336;
// System.String
struct String_t;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// VersionInfo
struct VersionInfo_t2356638086;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// Filelist`1<FileInfoRes>
struct Filelist_1_t2494814894;
// Filelist`1<FileInfoCrc>
struct Filelist_1_t2494800866;
// System.Text.Encoding
struct Encoding_t2012439129;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.Object
struct Il2CppObject;
// FlowControl.FlowContainerBase
struct FlowContainerBase_t4203254394;
// FlowControl.IFlowBase
struct IFlowBase_t1464761278;
// FlowControl.FlowContainerLine
struct FlowContainerLine_t4203559837;
// AssetDownMgr
struct AssetDownMgr_t4130275622;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// System.Action
struct Action_t3771233898;
// LoadingBoardMgr
struct LoadingBoardMgr_t303632014;
// AssetDownMgr/SetBytes
struct SetBytes_t2856136722;
// UnzipABAsset
struct UnzipABAsset_t414492807;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// HttpDownLoad
struct HttpDownLoad_t2310403440;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_VersionInfo2356638086.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Text_Encoding2012439129.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_ABCheckUpdate527832336.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_FlowControl_FlowContainerBase4203254394.h"
#include "AssemblyU2DCSharp_FlowControl_FlowContainerLine4203559837.h"
#include "AssemblyU2DCSharp_AssetDownMgr4130275622.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_AssetDownMgr_SetBytes2856136722.h"
#include "AssemblyU2DCSharp_HttpDownLoad2310403440.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_LoadingBoardMgr303632014.h"

// System.Void ABCheckUpdate::.ctor()
extern "C"  void ABCheckUpdate__ctor_m383975003 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ABCheckUpdate::get_GetUpdateTargetVS()
extern "C"  String_t* ABCheckUpdate_get_GetUpdateTargetVS_m1166599840 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ABCheckUpdate ABCheckUpdate::get_Instance()
extern "C"  ABCheckUpdate_t527832336 * ABCheckUpdate_get_Instance_m2603562116 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ABCheckUpdate::get_Version()
extern "C"  String_t* ABCheckUpdate_get_Version_m4082019915 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::DelOverFile()
extern "C"  void ABCheckUpdate_DelOverFile_m623081556 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::DelSubFile(System.Collections.Generic.List`1<System.String>)
extern "C"  void ABCheckUpdate_DelSubFile_m1189715662 (ABCheckUpdate_t527832336 * __this, List_1_t1375417109 * ___delList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::UpdateVersion(VersionInfo)
extern "C"  void ABCheckUpdate_UpdateVersion_m7983780 (ABCheckUpdate_t527832336 * __this, VersionInfo_t2356638086 * ___vsInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::Start()
extern "C"  void ABCheckUpdate_Start_m3626080091 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::LoadAllVSCfg()
extern "C"  void ABCheckUpdate_LoadAllVSCfg_m4084527861 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::GoToAppstoreDialog()
extern "C"  void ABCheckUpdate_GoToAppstoreDialog_m3695099892 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::GoToAppstore()
extern "C"  void ABCheckUpdate_GoToAppstore_m3042628812 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::LoadClientVer()
extern "C"  void ABCheckUpdate_LoadClientVer_m3847983243 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::LoadServerVer()
extern "C"  void ABCheckUpdate_LoadServerVer_m3263086611 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::CheckVer()
extern "C"  void ABCheckUpdate_CheckVer_m2928733764 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::LoadClientVS()
extern "C"  void ABCheckUpdate_LoadClientVS_m4003434199 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::LoadServerVS()
extern "C"  void ABCheckUpdate_LoadServerVS_m1075072591 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::Check()
extern "C"  void ABCheckUpdate_Check_m1970611841 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::StartDown()
extern "C"  void ABCheckUpdate_StartDown_m1750747805 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ABCheckUpdate::VerifyMD5(System.String,System.String)
extern "C"  bool ABCheckUpdate_VerifyMD5_m569196076 (ABCheckUpdate_t527832336 * __this, String_t* ___path0, String_t* ___md51, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::LoadedAll()
extern "C"  void ABCheckUpdate_LoadedAll_m4243199957 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ABCheckUpdate::SavePath()
extern "C"  String_t* ABCheckUpdate_SavePath_m1182671240 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::WriteByteFile(System.String,System.Byte[])
extern "C"  void ABCheckUpdate_WriteByteFile_m959226025 (ABCheckUpdate_t527832336 * __this, String_t* ___key0, ByteU5BU5D_t4260760469* ___abBytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::WriteClientVer()
extern "C"  void ABCheckUpdate_WriteClientVer_m620403202 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::WriteClientVS(Filelist`1<FileInfoRes>)
extern "C"  void ABCheckUpdate_WriteClientVS_m2797837657 (ABCheckUpdate_t527832336 * __this, Filelist_1_t2494814894 * ___fileList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::WriteCrc(Filelist`1<FileInfoCrc>)
extern "C"  void ABCheckUpdate_WriteCrc_m2040119951 (ABCheckUpdate_t527832336 * __this, Filelist_1_t2494800866 * ___fileList0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::WriteTextFile(System.String,System.String)
extern "C"  void ABCheckUpdate_WriteTextFile_m821048413 (ABCheckUpdate_t527832336 * __this, String_t* ___key0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ABCheckUpdate::BytesToString(System.Byte[],System.Text.Encoding)
extern "C"  String_t* ABCheckUpdate_BytesToString_m2494075579 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, Encoding_t2012439129 * ___encoding1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ABCheckUpdate::get_IsDownSuccess()
extern "C"  bool ABCheckUpdate_get_IsDownSuccess_m761253371 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ABCheckUpdate::get_WriteProgress()
extern "C"  float ABCheckUpdate_get_WriteProgress_m1327093384 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ABCheckUpdate::get_isPkged()
extern "C"  bool ABCheckUpdate_get_isPkged_m1725856965 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::set_isPkged(System.Boolean)
extern "C"  void ABCheckUpdate_set_isPkged_m3296162004 (ABCheckUpdate_t527832336 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ABCheckUpdate::get_PkgProgress()
extern "C"  float ABCheckUpdate_get_PkgProgress_m2548893429 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ABCheckUpdate::get_isVerifyed()
extern "C"  bool ABCheckUpdate_get_isVerifyed_m2407095328 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::set_isVerifyed(System.Boolean)
extern "C"  void ABCheckUpdate_set_isVerifyed_m3660316479 (ABCheckUpdate_t527832336 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ABCheckUpdate::get_VerifyProgress()
extern "C"  float ABCheckUpdate_get_VerifyProgress_m226737996 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::set_VerifyProgress(System.Single)
extern "C"  void ABCheckUpdate_set_VerifyProgress_m1288468767 (ABCheckUpdate_t527832336 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ABCheckUpdate::get_isStartDown()
extern "C"  bool ABCheckUpdate_get_isStartDown_m1248714430 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::set_isStartDown(System.Boolean)
extern "C"  void ABCheckUpdate_set_isStartDown_m345374477 (ABCheckUpdate_t527832336 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ABCheckUpdate::get_Progress()
extern "C"  float ABCheckUpdate_get_Progress_m184223155 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ABCheckUpdate::get_ProgressMsg()
extern "C"  String_t* ABCheckUpdate_get_ProgressMsg_m2172930951 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::IsOpenRepairAsset(CEvent.ZEvent)
extern "C"  void ABCheckUpdate_IsOpenRepairAsset_m1180533139 (ABCheckUpdate_t527832336 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::RepairAsset(CEvent.ZEvent)
extern "C"  void ABCheckUpdate_RepairAsset_m2605758943 (ABCheckUpdate_t527832336 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::RepairAssetShowDialog()
extern "C"  void ABCheckUpdate_RepairAssetShowDialog_m3128234977 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ABCheckUpdate::LocalVerifyMD5(System.String,System.String)
extern "C"  bool ABCheckUpdate_LocalVerifyMD5_m131784015 (ABCheckUpdate_t527832336 * __this, String_t* ___fullPath0, String_t* ___md51, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::<LoadAllVSCfg>m__3EB()
extern "C"  void ABCheckUpdate_U3CLoadAllVSCfgU3Em__3EB_m3076666626 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::<LoadClientVer>m__3EC(System.String,System.Byte[])
extern "C"  void ABCheckUpdate_U3CLoadClientVerU3Em__3EC_m3874434720 (ABCheckUpdate_t527832336 * __this, String_t* ___name0, ByteU5BU5D_t4260760469* ___abBytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::<LoadServerVer>m__3ED(System.String,System.Byte[])
extern "C"  void ABCheckUpdate_U3CLoadServerVerU3Em__3ED_m710485959 (ABCheckUpdate_t527832336 * __this, String_t* ___name0, ByteU5BU5D_t4260760469* ___abBytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::<LoadClientVS>m__3EE(System.String,System.Byte[])
extern "C"  void ABCheckUpdate_U3CLoadClientVSU3Em__3EE_m3335625858 (ABCheckUpdate_t527832336 * __this, String_t* ___name0, ByteU5BU5D_t4260760469* ___abBytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::<LoadServerVS>m__3EF(System.String,System.Byte[])
extern "C"  void ABCheckUpdate_U3CLoadServerVSU3Em__3EF_m2828872089 (ABCheckUpdate_t527832336 * __this, String_t* ___name0, ByteU5BU5D_t4260760469* ___abBytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::<StartDown>m__3F0(System.String,System.Byte[])
extern "C"  void ABCheckUpdate_U3CStartDownU3Em__3F0_m2940246438 (Il2CppObject * __this /* static, unused */, String_t* ___name0, ByteU5BU5D_t4260760469* ___abBytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::<StartDown>m__3F1()
extern "C"  void ABCheckUpdate_U3CStartDownU3Em__3F1_m1621028672 (ABCheckUpdate_t527832336 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::<LoadedAll>m__3F2()
extern "C"  void ABCheckUpdate_U3CLoadedAllU3Em__3F2_m1734366921 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::<RepairAsset>m__3F3(System.String,System.Byte[])
extern "C"  void ABCheckUpdate_U3CRepairAssetU3Em__3F3_m30713154 (ABCheckUpdate_t527832336 * __this, String_t* ___name0, ByteU5BU5D_t4260760469* ___abBytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::<RepairAssetShowDialog>m__3F4()
extern "C"  void ABCheckUpdate_U3CRepairAssetShowDialogU3Em__3F4_m1649228479 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::<LoadClientVS>m__3F5(System.String,System.Byte[])
extern "C"  void ABCheckUpdate_U3CLoadClientVSU3Em__3F5_m3505367763 (ABCheckUpdate_t527832336 * __this, String_t* ___name10, ByteU5BU5D_t4260760469* ___abBytes11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ABCheckUpdate::ilo_SavePath1(ABCheckUpdate)
extern "C"  String_t* ABCheckUpdate_ilo_SavePath1_m99384810 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_Log2(System.Object,System.Boolean)
extern "C"  void ABCheckUpdate_ilo_Log2_m4162004435 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_AddFlow3(FlowControl.FlowContainerBase,FlowControl.IFlowBase)
extern "C"  void ABCheckUpdate_ilo_AddFlow3_m1075399216 (Il2CppObject * __this /* static, unused */, FlowContainerBase_t4203254394 * ____this0, Il2CppObject * ___flow1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_Activate4(FlowControl.FlowContainerLine)
extern "C"  void ABCheckUpdate_ilo_Activate4_m1415629795 (Il2CppObject * __this /* static, unused */, FlowContainerLine_t4203559837 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_set_isStartDown5(ABCheckUpdate,System.Boolean)
extern "C"  void ABCheckUpdate_ilo_set_isStartDown5_m3017208207 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_set_isPkged6(ABCheckUpdate,System.Boolean)
extern "C"  void ABCheckUpdate_ilo_set_isPkged6_m3406434985 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_set_VerifyProgress7(ABCheckUpdate,System.Single)
extern "C"  void ABCheckUpdate_ilo_set_VerifyProgress7_m3208162381 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_Init8(AssetDownMgr)
extern "C"  void ABCheckUpdate_ilo_Init8_m1460842792 (Il2CppObject * __this /* static, unused */, AssetDownMgr_t4130275622 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_AddEventListener9(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void ABCheckUpdate_ilo_AddEventListener9_m198362161 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_LoadAllVSCfg10(VersionMgr,System.Int32,System.Action)
extern "C"  void ABCheckUpdate_ilo_LoadAllVSCfg10_m1458024999 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, int32_t ___type1, Action_t3771233898 * ___call2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// LoadingBoardMgr ABCheckUpdate::ilo_get_Instance11()
extern "C"  LoadingBoardMgr_t303632014 * ABCheckUpdate_ilo_get_Instance11_m3699003605 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr ABCheckUpdate::ilo_get_Instance12()
extern "C"  VersionMgr_t1322950208 * ABCheckUpdate_ilo_get_Instance12_m2091884958 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_LoadClientVS13(ABCheckUpdate)
extern "C"  void ABCheckUpdate_ilo_LoadClientVS13_m1187655414 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AssetDownMgr ABCheckUpdate::ilo_get_Instance14()
extern "C"  AssetDownMgr_t4130275622 * ABCheckUpdate_ilo_get_Instance14_m325752390 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_AddUpdateData15(AssetDownMgr,System.String,AssetDownMgr/SetBytes,System.Boolean)
extern "C"  void ABCheckUpdate_ilo_AddUpdateData15_m1172580847 (Il2CppObject * __this /* static, unused */, AssetDownMgr_t4130275622 * ____this0, String_t* ___path1, SetBytes_t2856136722 * ___downFinishCall2, bool ___isInsert3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo ABCheckUpdate::ilo_get_currentVS16(VersionMgr)
extern "C"  VersionInfo_t2356638086 * ABCheckUpdate_ilo_get_currentVS16_m494519685 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnzipABAsset ABCheckUpdate::ilo_get_Instance17()
extern "C"  UnzipABAsset_t414492807 * ABCheckUpdate_ilo_get_Instance17_m83881066 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_DelSubFile18(ABCheckUpdate,System.Collections.Generic.List`1<System.String>)
extern "C"  void ABCheckUpdate_ilo_DelSubFile18_m153722124 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, List_1_t1375417109 * ___delList1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_WriteCrc19(ABCheckUpdate,Filelist`1<FileInfoCrc>)
extern "C"  void ABCheckUpdate_ilo_WriteCrc19_m2847537840 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, Filelist_1_t2494800866 * ___fileList1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_set_isVerifyed20(ABCheckUpdate,System.Boolean)
extern "C"  void ABCheckUpdate_ilo_set_isVerifyed20_m2567396692 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_LogError21(System.Object,System.Boolean)
extern "C"  void ABCheckUpdate_ilo_LogError21_m3233797064 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_LoadedAll22(ABCheckUpdate)
extern "C"  void ABCheckUpdate_ilo_LoadedAll22_m1707213856 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ABCheckUpdate::ilo_GetMD523(System.Byte[])
extern "C"  String_t* ABCheckUpdate_ilo_GetMD523_m2254395367 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_WriteClientVS24(ABCheckUpdate,Filelist`1<FileInfoRes>)
extern "C"  void ABCheckUpdate_ilo_WriteClientVS24_m1948420070 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, Filelist_1_t2494814894 * ___fileList1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr ABCheckUpdate::ilo_get_Instance25()
extern "C"  PluginsSdkMgr_t3884624670 * ABCheckUpdate_ilo_get_Instance25_m2066347528 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_WriteTextFile26(ABCheckUpdate,System.String,System.String)
extern "C"  void ABCheckUpdate_ilo_WriteTextFile26_m1976681768 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, String_t* ___key1, String_t* ___text2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ABCheckUpdate::ilo_get_WriteProgress27(AssetDownMgr)
extern "C"  float ABCheckUpdate_ilo_get_WriteProgress27_m1673529748 (Il2CppObject * __this /* static, unused */, AssetDownMgr_t4130275622 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ABCheckUpdate::ilo_get_progress28(HttpDownLoad)
extern "C"  float ABCheckUpdate_ilo_get_progress28_m1090846748 (Il2CppObject * __this /* static, unused */, HttpDownLoad_t2310403440 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_ShowDialog29(PluginsSdkMgr,System.String,System.String,System.Boolean,System.String,System.String,System.Action,System.Action)
extern "C"  void ABCheckUpdate_ilo_ShowDialog29_m2386631935 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, String_t* ___title1, String_t* ___content2, bool ___cancelable3, String_t* ___okText4, String_t* ___cancelText5, Action_t3771233898 * ___OnOK6, Action_t3771233898 * ___OnCancel7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ABCheckUpdate::ilo_get_isReview30(VersionMgr)
extern "C"  bool ABCheckUpdate_ilo_get_isReview30_m1801584880 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_ReviewUI31(LoadingBoardMgr,System.Boolean)
extern "C"  void ABCheckUpdate_ilo_ReviewUI31_m3062723039 (Il2CppObject * __this /* static, unused */, LoadingBoardMgr_t303632014 * ____this0, bool ___isReview1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_GoToAppstoreDialog32(ABCheckUpdate)
extern "C"  void ABCheckUpdate_ilo_GoToAppstoreDialog32_m1457809564 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_LoadServerVer33(ABCheckUpdate)
extern "C"  void ABCheckUpdate_ilo_LoadServerVer33_m3620496002 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ABCheckUpdate::ilo_LocalVerifyMD534(ABCheckUpdate,System.String,System.String)
extern "C"  bool ABCheckUpdate_ilo_LocalVerifyMD534_m600859991 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, String_t* ___fullPath1, String_t* ___md52, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_WriteClientVer35(ABCheckUpdate)
extern "C"  void ABCheckUpdate_ilo_WriteClientVer35_m877168875 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_RepairAssetShowDialog36(ABCheckUpdate)
extern "C"  void ABCheckUpdate_ilo_RepairAssetShowDialog36_m3554881425 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ABCheckUpdate::ilo_LoadServerVS37(ABCheckUpdate)
extern "C"  void ABCheckUpdate_ilo_LoadServerVS37_m391824380 (Il2CppObject * __this /* static, unused */, ABCheckUpdate_t527832336 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
