﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AccelerationEventGenerated
struct UnityEngine_AccelerationEventGenerated_t3573902365;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_AccelerationEventGenerated::.ctor()
extern "C"  void UnityEngine_AccelerationEventGenerated__ctor_m3065440542 (UnityEngine_AccelerationEventGenerated_t3573902365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AccelerationEventGenerated::.cctor()
extern "C"  void UnityEngine_AccelerationEventGenerated__cctor_m57280079 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AccelerationEventGenerated::AccelerationEvent_AccelerationEvent1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AccelerationEventGenerated_AccelerationEvent_AccelerationEvent1_m3373629606 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AccelerationEventGenerated::AccelerationEvent_acceleration(JSVCall)
extern "C"  void UnityEngine_AccelerationEventGenerated_AccelerationEvent_acceleration_m1499966908 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AccelerationEventGenerated::AccelerationEvent_deltaTime(JSVCall)
extern "C"  void UnityEngine_AccelerationEventGenerated_AccelerationEvent_deltaTime_m3825358731 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AccelerationEventGenerated::__Register()
extern "C"  void UnityEngine_AccelerationEventGenerated___Register_m1023776361 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AccelerationEventGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_AccelerationEventGenerated_ilo_attachFinalizerObject1_m3499584385 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AccelerationEventGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_AccelerationEventGenerated_ilo_addJSCSRel2_m2545986907 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
