﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_MathGenerated
struct System_MathGenerated_t562349399;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_MathGenerated::.ctor()
extern "C"  void System_MathGenerated__ctor_m4079396644 (System_MathGenerated_t562349399 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_MathGenerated::Math_E(JSVCall)
extern "C"  void System_MathGenerated_Math_E_m2727934649 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_MathGenerated::Math_PI(JSVCall)
extern "C"  void System_MathGenerated_Math_PI_m3151886037 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Abs__Int64(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Abs__Int64_m3818942416 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Abs__SByte(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Abs__SByte_m2688630398 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Abs__Int16(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Abs__Int16_m2348537847 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Abs__Int32(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Abs__Int32_m1530433713 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Abs__Decimal(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Abs__Decimal_m151741620 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Abs__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Abs__Double_m100977872 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Abs__Single(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Abs__Single_m1381896711 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Acos__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Acos__Double_m1030504172 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Asin__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Asin__Double_m2168100477 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Atan__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Atan__Double_m3180352390 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Atan2__Double__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Atan2__Double__Double_m197956609 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_BigMul__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_BigMul__Int32__Int32_m962014137 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Ceiling__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Ceiling__Double_m3032511963 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Ceiling__Decimal(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Ceiling__Decimal_m834985225 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Cos__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Cos__Double_m3962012453 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Cosh__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Cosh__Double_m3632856039 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_DivRem__Int64__Int64__Int64(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_DivRem__Int64__Int64__Int64_m1362300081 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_DivRem__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_DivRem__Int32__Int32__Int32_m1030750386 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Exp__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Exp__Double_m1926160699 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Floor__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Floor__Double_m2290888682 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Floor__Decimal(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Floor__Decimal_m3614467290 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_IEEERemainder__Double__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_IEEERemainder__Double__Double_m1481647384 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Log__Double__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Log__Double__Double_m217842739 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Log__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Log__Double_m1574157346 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Log10__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Log10__Double_m1519321601 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Max__UInt16__UInt16(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Max__UInt16__UInt16_m2639735151 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Max__Single__Single(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Max__Single__Single_m2874855873 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Max__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Max__Int32__Int32_m3618126193 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Max__Double__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Max__Double__Double_m789147219 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Max__Byte__Byte(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Max__Byte__Byte_m4079607105 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Max__Decimal__Decimal(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Max__Decimal__Decimal_m3733964881 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Max__UInt32__UInt32(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Max__UInt32__UInt32_m693190883 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Max__UInt64__UInt64(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Max__UInt64__UInt64_m1947955233 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Max__Int16__Int16(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Max__Int16__Int16_m3779894961 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Max__Int64__Int64(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Max__Int64__Int64_m2686699665 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Max__SByte__SByte(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Max__SByte__SByte_m4182542545 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Min__UInt16__UInt16(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Min__UInt16__UInt16_m2084374493 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Min__Double__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Min__Double__Double_m233786561 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Min__Single__Single(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Min__Single__Single_m2319495215 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Min__UInt64__UInt64(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Min__UInt64__UInt64_m1392594575 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Min__Decimal__Decimal(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Min__Decimal__Decimal_m2608317247 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Min__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Min__Int32__Int32_m497998687 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Min__Int16__Int16(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Min__Int16__Int16_m659767455 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Min__UInt32__UInt32(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Min__UInt32__UInt32_m137830225 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Min__Int64__Int64(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Min__Int64__Int64_m3861539455 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Min__SByte__SByte(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Min__SByte__SByte_m1062415039 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Min__Byte__Byte(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Min__Byte__Byte_m2427200175 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Pow__Double__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Pow__Double__Double_m3482068039 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Round__Decimal__Int32__MidpointRounding(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Round__Decimal__Int32__MidpointRounding_m4180160628 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Round__Double__Int32__MidpointRounding(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Round__Double__Int32__MidpointRounding_m2540748832 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Round__Decimal__MidpointRounding(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Round__Decimal__MidpointRounding_m3703556692 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Round__Decimal__Int32(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Round__Decimal__Int32_m4204811576 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Round__Double__MidpointRounding(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Round__Double__MidpointRounding_m3966125096 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Round__Double__Int32(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Round__Double__Int32_m331593700 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Round__Decimal(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Round__Decimal_m4248086296 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Round__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Round__Double_m1480044012 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Sign__Int32(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Sign__Int32_m3857942078 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Sign__Int64(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Sign__Int64_m1851483485 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Sign__Single(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Sign__Single_m520211994 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Sign__Decimal(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Sign__Decimal_m3504286465 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Sign__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Sign__Double_m3534260451 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Sign__SByte(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Sign__SByte_m721171467 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Sign__Int16(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Sign__Int16_m381078916 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Sin__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Sin__Double_m804641462 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Sinh__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Sinh__Double_m243635830 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Sqrt__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Sqrt__Double_m418240774 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Tan__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Tan__Double_m1816893375 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Tanh__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Tanh__Double_m1558674061 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Truncate__Double(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Truncate__Double_m2998823116 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_MathGenerated::Math_Truncate__Decimal(JSVCall,System.Int32)
extern "C"  bool System_MathGenerated_Math_Truncate__Decimal_m4085598264 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_MathGenerated::__Register()
extern "C"  void System_MathGenerated___Register_m2969152547 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 System_MathGenerated::ilo_getInt641(System.Int32)
extern "C"  int64_t System_MathGenerated_ilo_getInt641_m1863657675 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_MathGenerated::ilo_setInt162(System.Int32,System.Int16)
extern "C"  void System_MathGenerated_ilo_setInt162_m2875759697 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int16_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_MathGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_MathGenerated_ilo_setObject3_m3999158120 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double System_MathGenerated::ilo_getDouble4(System.Int32)
extern "C"  double System_MathGenerated_ilo_getDouble4_m1004954740 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_MathGenerated::ilo_setDouble5(System.Int32,System.Double)
extern "C"  void System_MathGenerated_ilo_setDouble5_m684379010 (Il2CppObject * __this /* static, unused */, int32_t ___e0, double ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_MathGenerated::ilo_setInt646(System.Int32,System.Int64)
extern "C"  void System_MathGenerated_ilo_setInt646_m527195501 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int64_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_MathGenerated::ilo_setArgIndex7(System.Int32)
extern "C"  void System_MathGenerated_ilo_setArgIndex7_m1972136541 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_MathGenerated::ilo_setInt328(System.Int32,System.Int32)
extern "C"  void System_MathGenerated_ilo_setInt328_m2892534987 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System_MathGenerated::ilo_getByte9(System.Int32)
extern "C"  uint8_t System_MathGenerated_ilo_getByte9_m1752112139 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_MathGenerated::ilo_getObject10(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_MathGenerated_ilo_getObject10_m298493653 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_MathGenerated::ilo_setUInt3211(System.Int32,System.UInt32)
extern "C"  void System_MathGenerated_ilo_setUInt3211_m3699789775 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 System_MathGenerated::ilo_getUInt6412(System.Int32)
extern "C"  uint64_t System_MathGenerated_ilo_getUInt6412_m837139539 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 System_MathGenerated::ilo_getInt1613(System.Int32)
extern "C"  int16_t System_MathGenerated_ilo_getInt1613_m1619431820 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char System_MathGenerated::ilo_getSByte14(System.Int32)
extern "C"  Il2CppChar System_MathGenerated_ilo_getSByte14_m2088592778 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 System_MathGenerated::ilo_getUInt1615(System.Int32)
extern "C"  uint16_t System_MathGenerated_ilo_getUInt1615_m2715694692 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_MathGenerated::ilo_getInt3216(System.Int32)
extern "C"  int32_t System_MathGenerated_ilo_getInt3216_m396891663 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System_MathGenerated::ilo_getUInt3217(System.Int32)
extern "C"  uint32_t System_MathGenerated_ilo_getUInt3217_m2606599770 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_MathGenerated::ilo_setSByte18(System.Int32,System.SByte)
extern "C"  void System_MathGenerated_ilo_setSByte18_m354552166 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int8_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_MathGenerated::ilo_getEnum19(System.Int32)
extern "C"  int32_t System_MathGenerated_ilo_getEnum19_m52695409 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single System_MathGenerated::ilo_getSingle20(System.Int32)
extern "C"  float System_MathGenerated_ilo_getSingle20_m1692239696 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
