﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_WWWFormGenerated
struct UnityEngine_WWWFormGenerated_t1922299452;
// JSVCall
struct JSVCall_t3708497963;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_WWWFormGenerated::.ctor()
extern "C"  void UnityEngine_WWWFormGenerated__ctor_m2857897951 (UnityEngine_WWWFormGenerated_t1922299452 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWFormGenerated::WWWForm_WWWForm1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWFormGenerated_WWWForm_WWWForm1_m3760003139 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWFormGenerated::WWWForm_headers(JSVCall)
extern "C"  void UnityEngine_WWWFormGenerated_WWWForm_headers_m1953107208 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWFormGenerated::WWWForm_data(JSVCall)
extern "C"  void UnityEngine_WWWFormGenerated_WWWForm_data_m817113556 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWFormGenerated::WWWForm_AddBinaryData__String__Byte_Array__String__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWFormGenerated_WWWForm_AddBinaryData__String__Byte_Array__String__String_m2218995574 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWFormGenerated::WWWForm_AddBinaryData__String__Byte_Array__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWFormGenerated_WWWForm_AddBinaryData__String__Byte_Array__String_m697722405 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWFormGenerated::WWWForm_AddBinaryData__String__Byte_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWFormGenerated_WWWForm_AddBinaryData__String__Byte_Array_m1799797716 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWFormGenerated::WWWForm_AddField__String__String__Encoding(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWFormGenerated_WWWForm_AddField__String__String__Encoding_m3054701627 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWFormGenerated::WWWForm_AddField__String__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWFormGenerated_WWWForm_AddField__String__String_m3321872360 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWFormGenerated::WWWForm_AddField__String__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WWWFormGenerated_WWWForm_AddField__String__Int32_m1998558937 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWFormGenerated::__Register()
extern "C"  void UnityEngine_WWWFormGenerated___Register_m2273542216 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine_WWWFormGenerated::<WWWForm_AddBinaryData__String__Byte_Array__String__String>m__2EB()
extern "C"  ByteU5BU5D_t4260760469* UnityEngine_WWWFormGenerated_U3CWWWForm_AddBinaryData__String__Byte_Array__String__StringU3Em__2EB_m939685106 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine_WWWFormGenerated::<WWWForm_AddBinaryData__String__Byte_Array__String>m__2EC()
extern "C"  ByteU5BU5D_t4260760469* UnityEngine_WWWFormGenerated_U3CWWWForm_AddBinaryData__String__Byte_Array__StringU3Em__2EC_m1160382372 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine_WWWFormGenerated::<WWWForm_AddBinaryData__String__Byte_Array>m__2ED()
extern "C"  ByteU5BU5D_t4260760469* UnityEngine_WWWFormGenerated_U3CWWWForm_AddBinaryData__String__Byte_ArrayU3Em__2ED_m2506365782 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_WWWFormGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_WWWFormGenerated_ilo_getObject1_m2238192531 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WWWFormGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_WWWFormGenerated_ilo_attachFinalizerObject2_m2891734049 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWFormGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UnityEngine_WWWFormGenerated_ilo_addJSCSRel3_m4078775325 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WWWFormGenerated::ilo_moveSaveID2Arr4(System.Int32)
extern "C"  void UnityEngine_WWWFormGenerated_ilo_moveSaveID2Arr4_m3904279543 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_WWWFormGenerated::ilo_getStringS5(System.Int32)
extern "C"  String_t* UnityEngine_WWWFormGenerated_ilo_getStringS5_m1171596513 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_WWWFormGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_WWWFormGenerated_ilo_getObject6_m2283687349 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_WWWFormGenerated::ilo_getArrayLength7(System.Int32)
extern "C"  int32_t UnityEngine_WWWFormGenerated_ilo_getArrayLength7_m3081769019 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_WWWFormGenerated::ilo_getElement8(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_WWWFormGenerated_ilo_getElement8_m754919992 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
