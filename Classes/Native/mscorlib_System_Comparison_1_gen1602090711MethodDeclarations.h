﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen2887177558MethodDeclarations.h"

// System.Void System.Comparison`1<HeroMgr/LineupPos>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m739349469(__this, ___object0, ___method1, method) ((  void (*) (Comparison_1_t1602090711 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m487232819_gshared)(__this, ___object0, ___method1, method)
// System.Int32 System.Comparison`1<HeroMgr/LineupPos>::Invoke(T,T)
#define Comparison_1_Invoke_m4071751683(__this, ___x0, ___y1, method) ((  int32_t (*) (Comparison_1_t1602090711 *, LineupPos_t2885729524 *, LineupPos_t2885729524 *, const MethodInfo*))Comparison_1_Invoke_m1888033133_gshared)(__this, ___x0, ___y1, method)
// System.IAsyncResult System.Comparison`1<HeroMgr/LineupPos>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m2050387580(__this, ___x0, ___y1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Comparison_1_t1602090711 *, LineupPos_t2885729524 *, LineupPos_t2885729524 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Comparison_1_BeginInvoke_m3177996774_gshared)(__this, ___x0, ___y1, ___callback2, ___object3, method)
// System.Int32 System.Comparison`1<HeroMgr/LineupPos>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m52081545(__this, ___result0, method) ((  int32_t (*) (Comparison_1_t1602090711 *, Il2CppObject *, const MethodInfo*))Comparison_1_EndInvoke_m651541983_gshared)(__this, ___result0, method)
