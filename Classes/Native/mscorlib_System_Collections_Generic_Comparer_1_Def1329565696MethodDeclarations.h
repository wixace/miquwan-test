﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.LocalAvoidance/IntersectionPair>
struct DefaultComparer_t1329565696;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_LocalAvoidance_Inter2821319919.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.LocalAvoidance/IntersectionPair>::.ctor()
extern "C"  void DefaultComparer__ctor_m636367571_gshared (DefaultComparer_t1329565696 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m636367571(__this, method) ((  void (*) (DefaultComparer_t1329565696 *, const MethodInfo*))DefaultComparer__ctor_m636367571_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Pathfinding.LocalAvoidance/IntersectionPair>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1761995836_gshared (DefaultComparer_t1329565696 * __this, IntersectionPair_t2821319919  ___x0, IntersectionPair_t2821319919  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m1761995836(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t1329565696 *, IntersectionPair_t2821319919 , IntersectionPair_t2821319919 , const MethodInfo*))DefaultComparer_Compare_m1761995836_gshared)(__this, ___x0, ___y1, method)
