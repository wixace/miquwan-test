﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Int32[]>
struct List_1_t304066077;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh>
struct List_1_t1291247971;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// Pathfinding.Voxels.VoxelArea
struct VoxelArea_t3943332841;
// Pathfinding.Voxels.VoxelContourSet
struct VoxelContourSet_t2502455108;
// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_RecastGraph_Relevant3667774716.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Voxels.Voxelize
struct  Voxelize_t3998270866  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<Pathfinding.Voxels.ExtraMesh> Pathfinding.Voxels.Voxelize::inputExtraMeshes
	List_1_t1291247971 * ___inputExtraMeshes_13;
	// UnityEngine.Vector3[] Pathfinding.Voxels.Voxelize::inputVertices
	Vector3U5BU5D_t215400611* ___inputVertices_14;
	// System.Int32[] Pathfinding.Voxels.Voxelize::inputTriangles
	Int32U5BU5D_t3230847821* ___inputTriangles_15;
	// System.Int32 Pathfinding.Voxels.Voxelize::voxelWalkableClimb
	int32_t ___voxelWalkableClimb_16;
	// System.UInt32 Pathfinding.Voxels.Voxelize::voxelWalkableHeight
	uint32_t ___voxelWalkableHeight_17;
	// System.Single Pathfinding.Voxels.Voxelize::cellSize
	float ___cellSize_18;
	// System.Single Pathfinding.Voxels.Voxelize::cellHeight
	float ___cellHeight_19;
	// System.Int32 Pathfinding.Voxels.Voxelize::minRegionSize
	int32_t ___minRegionSize_20;
	// System.Int32 Pathfinding.Voxels.Voxelize::borderSize
	int32_t ___borderSize_21;
	// System.Single Pathfinding.Voxels.Voxelize::maxEdgeLength
	float ___maxEdgeLength_22;
	// System.Single Pathfinding.Voxels.Voxelize::maxSlope
	float ___maxSlope_23;
	// Pathfinding.RecastGraph/RelevantGraphSurfaceMode Pathfinding.Voxels.Voxelize::relevantGraphSurfaceMode
	int32_t ___relevantGraphSurfaceMode_24;
	// UnityEngine.Bounds Pathfinding.Voxels.Voxelize::forcedBounds
	Bounds_t2711641849  ___forcedBounds_25;
	// Pathfinding.Voxels.VoxelArea Pathfinding.Voxels.Voxelize::voxelArea
	VoxelArea_t3943332841 * ___voxelArea_26;
	// Pathfinding.Voxels.VoxelContourSet Pathfinding.Voxels.Voxelize::countourSet
	VoxelContourSet_t2502455108 * ___countourSet_27;
	// System.Int32 Pathfinding.Voxels.Voxelize::width
	int32_t ___width_28;
	// System.Int32 Pathfinding.Voxels.Voxelize::depth
	int32_t ___depth_29;
	// UnityEngine.Vector3 Pathfinding.Voxels.Voxelize::voxelOffset
	Vector3_t4282066566  ___voxelOffset_30;
	// System.String Pathfinding.Voxels.Voxelize::debugString
	String_t* ___debugString_31;
	// UnityEngine.Vector3 Pathfinding.Voxels.Voxelize::cellScale
	Vector3_t4282066566  ___cellScale_32;
	// UnityEngine.Vector3 Pathfinding.Voxels.Voxelize::cellScaleDivision
	Vector3_t4282066566  ___cellScaleDivision_33;

public:
	inline static int32_t get_offset_of_inputExtraMeshes_13() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866, ___inputExtraMeshes_13)); }
	inline List_1_t1291247971 * get_inputExtraMeshes_13() const { return ___inputExtraMeshes_13; }
	inline List_1_t1291247971 ** get_address_of_inputExtraMeshes_13() { return &___inputExtraMeshes_13; }
	inline void set_inputExtraMeshes_13(List_1_t1291247971 * value)
	{
		___inputExtraMeshes_13 = value;
		Il2CppCodeGenWriteBarrier(&___inputExtraMeshes_13, value);
	}

	inline static int32_t get_offset_of_inputVertices_14() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866, ___inputVertices_14)); }
	inline Vector3U5BU5D_t215400611* get_inputVertices_14() const { return ___inputVertices_14; }
	inline Vector3U5BU5D_t215400611** get_address_of_inputVertices_14() { return &___inputVertices_14; }
	inline void set_inputVertices_14(Vector3U5BU5D_t215400611* value)
	{
		___inputVertices_14 = value;
		Il2CppCodeGenWriteBarrier(&___inputVertices_14, value);
	}

	inline static int32_t get_offset_of_inputTriangles_15() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866, ___inputTriangles_15)); }
	inline Int32U5BU5D_t3230847821* get_inputTriangles_15() const { return ___inputTriangles_15; }
	inline Int32U5BU5D_t3230847821** get_address_of_inputTriangles_15() { return &___inputTriangles_15; }
	inline void set_inputTriangles_15(Int32U5BU5D_t3230847821* value)
	{
		___inputTriangles_15 = value;
		Il2CppCodeGenWriteBarrier(&___inputTriangles_15, value);
	}

	inline static int32_t get_offset_of_voxelWalkableClimb_16() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866, ___voxelWalkableClimb_16)); }
	inline int32_t get_voxelWalkableClimb_16() const { return ___voxelWalkableClimb_16; }
	inline int32_t* get_address_of_voxelWalkableClimb_16() { return &___voxelWalkableClimb_16; }
	inline void set_voxelWalkableClimb_16(int32_t value)
	{
		___voxelWalkableClimb_16 = value;
	}

	inline static int32_t get_offset_of_voxelWalkableHeight_17() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866, ___voxelWalkableHeight_17)); }
	inline uint32_t get_voxelWalkableHeight_17() const { return ___voxelWalkableHeight_17; }
	inline uint32_t* get_address_of_voxelWalkableHeight_17() { return &___voxelWalkableHeight_17; }
	inline void set_voxelWalkableHeight_17(uint32_t value)
	{
		___voxelWalkableHeight_17 = value;
	}

	inline static int32_t get_offset_of_cellSize_18() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866, ___cellSize_18)); }
	inline float get_cellSize_18() const { return ___cellSize_18; }
	inline float* get_address_of_cellSize_18() { return &___cellSize_18; }
	inline void set_cellSize_18(float value)
	{
		___cellSize_18 = value;
	}

	inline static int32_t get_offset_of_cellHeight_19() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866, ___cellHeight_19)); }
	inline float get_cellHeight_19() const { return ___cellHeight_19; }
	inline float* get_address_of_cellHeight_19() { return &___cellHeight_19; }
	inline void set_cellHeight_19(float value)
	{
		___cellHeight_19 = value;
	}

	inline static int32_t get_offset_of_minRegionSize_20() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866, ___minRegionSize_20)); }
	inline int32_t get_minRegionSize_20() const { return ___minRegionSize_20; }
	inline int32_t* get_address_of_minRegionSize_20() { return &___minRegionSize_20; }
	inline void set_minRegionSize_20(int32_t value)
	{
		___minRegionSize_20 = value;
	}

	inline static int32_t get_offset_of_borderSize_21() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866, ___borderSize_21)); }
	inline int32_t get_borderSize_21() const { return ___borderSize_21; }
	inline int32_t* get_address_of_borderSize_21() { return &___borderSize_21; }
	inline void set_borderSize_21(int32_t value)
	{
		___borderSize_21 = value;
	}

	inline static int32_t get_offset_of_maxEdgeLength_22() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866, ___maxEdgeLength_22)); }
	inline float get_maxEdgeLength_22() const { return ___maxEdgeLength_22; }
	inline float* get_address_of_maxEdgeLength_22() { return &___maxEdgeLength_22; }
	inline void set_maxEdgeLength_22(float value)
	{
		___maxEdgeLength_22 = value;
	}

	inline static int32_t get_offset_of_maxSlope_23() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866, ___maxSlope_23)); }
	inline float get_maxSlope_23() const { return ___maxSlope_23; }
	inline float* get_address_of_maxSlope_23() { return &___maxSlope_23; }
	inline void set_maxSlope_23(float value)
	{
		___maxSlope_23 = value;
	}

	inline static int32_t get_offset_of_relevantGraphSurfaceMode_24() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866, ___relevantGraphSurfaceMode_24)); }
	inline int32_t get_relevantGraphSurfaceMode_24() const { return ___relevantGraphSurfaceMode_24; }
	inline int32_t* get_address_of_relevantGraphSurfaceMode_24() { return &___relevantGraphSurfaceMode_24; }
	inline void set_relevantGraphSurfaceMode_24(int32_t value)
	{
		___relevantGraphSurfaceMode_24 = value;
	}

	inline static int32_t get_offset_of_forcedBounds_25() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866, ___forcedBounds_25)); }
	inline Bounds_t2711641849  get_forcedBounds_25() const { return ___forcedBounds_25; }
	inline Bounds_t2711641849 * get_address_of_forcedBounds_25() { return &___forcedBounds_25; }
	inline void set_forcedBounds_25(Bounds_t2711641849  value)
	{
		___forcedBounds_25 = value;
	}

	inline static int32_t get_offset_of_voxelArea_26() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866, ___voxelArea_26)); }
	inline VoxelArea_t3943332841 * get_voxelArea_26() const { return ___voxelArea_26; }
	inline VoxelArea_t3943332841 ** get_address_of_voxelArea_26() { return &___voxelArea_26; }
	inline void set_voxelArea_26(VoxelArea_t3943332841 * value)
	{
		___voxelArea_26 = value;
		Il2CppCodeGenWriteBarrier(&___voxelArea_26, value);
	}

	inline static int32_t get_offset_of_countourSet_27() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866, ___countourSet_27)); }
	inline VoxelContourSet_t2502455108 * get_countourSet_27() const { return ___countourSet_27; }
	inline VoxelContourSet_t2502455108 ** get_address_of_countourSet_27() { return &___countourSet_27; }
	inline void set_countourSet_27(VoxelContourSet_t2502455108 * value)
	{
		___countourSet_27 = value;
		Il2CppCodeGenWriteBarrier(&___countourSet_27, value);
	}

	inline static int32_t get_offset_of_width_28() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866, ___width_28)); }
	inline int32_t get_width_28() const { return ___width_28; }
	inline int32_t* get_address_of_width_28() { return &___width_28; }
	inline void set_width_28(int32_t value)
	{
		___width_28 = value;
	}

	inline static int32_t get_offset_of_depth_29() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866, ___depth_29)); }
	inline int32_t get_depth_29() const { return ___depth_29; }
	inline int32_t* get_address_of_depth_29() { return &___depth_29; }
	inline void set_depth_29(int32_t value)
	{
		___depth_29 = value;
	}

	inline static int32_t get_offset_of_voxelOffset_30() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866, ___voxelOffset_30)); }
	inline Vector3_t4282066566  get_voxelOffset_30() const { return ___voxelOffset_30; }
	inline Vector3_t4282066566 * get_address_of_voxelOffset_30() { return &___voxelOffset_30; }
	inline void set_voxelOffset_30(Vector3_t4282066566  value)
	{
		___voxelOffset_30 = value;
	}

	inline static int32_t get_offset_of_debugString_31() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866, ___debugString_31)); }
	inline String_t* get_debugString_31() const { return ___debugString_31; }
	inline String_t** get_address_of_debugString_31() { return &___debugString_31; }
	inline void set_debugString_31(String_t* value)
	{
		___debugString_31 = value;
		Il2CppCodeGenWriteBarrier(&___debugString_31, value);
	}

	inline static int32_t get_offset_of_cellScale_32() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866, ___cellScale_32)); }
	inline Vector3_t4282066566  get_cellScale_32() const { return ___cellScale_32; }
	inline Vector3_t4282066566 * get_address_of_cellScale_32() { return &___cellScale_32; }
	inline void set_cellScale_32(Vector3_t4282066566  value)
	{
		___cellScale_32 = value;
	}

	inline static int32_t get_offset_of_cellScaleDivision_33() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866, ___cellScaleDivision_33)); }
	inline Vector3_t4282066566  get_cellScaleDivision_33() const { return ___cellScaleDivision_33; }
	inline Vector3_t4282066566 * get_address_of_cellScaleDivision_33() { return &___cellScaleDivision_33; }
	inline void set_cellScaleDivision_33(Vector3_t4282066566  value)
	{
		___cellScaleDivision_33 = value;
	}
};

struct Voxelize_t3998270866_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Int32[]> Pathfinding.Voxels.Voxelize::intArrCache
	List_1_t304066077 * ___intArrCache_11;
	// System.Int32[] Pathfinding.Voxels.Voxelize::emptyArr
	Int32U5BU5D_t3230847821* ___emptyArr_12;

public:
	inline static int32_t get_offset_of_intArrCache_11() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866_StaticFields, ___intArrCache_11)); }
	inline List_1_t304066077 * get_intArrCache_11() const { return ___intArrCache_11; }
	inline List_1_t304066077 ** get_address_of_intArrCache_11() { return &___intArrCache_11; }
	inline void set_intArrCache_11(List_1_t304066077 * value)
	{
		___intArrCache_11 = value;
		Il2CppCodeGenWriteBarrier(&___intArrCache_11, value);
	}

	inline static int32_t get_offset_of_emptyArr_12() { return static_cast<int32_t>(offsetof(Voxelize_t3998270866_StaticFields, ___emptyArr_12)); }
	inline Int32U5BU5D_t3230847821* get_emptyArr_12() const { return ___emptyArr_12; }
	inline Int32U5BU5D_t3230847821** get_address_of_emptyArr_12() { return &___emptyArr_12; }
	inline void set_emptyArr_12(Int32U5BU5D_t3230847821* value)
	{
		___emptyArr_12 = value;
		Il2CppCodeGenWriteBarrier(&___emptyArr_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
