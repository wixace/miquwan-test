﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3353480155.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_ProtoBuf_Serializers_EnumSerializ276170183.h"

// System.Void System.Array/InternalEnumerator`1<ProtoBuf.Serializers.EnumSerializer/EnumPair>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2969223832_gshared (InternalEnumerator_1_t3353480155 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2969223832(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3353480155 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2969223832_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<ProtoBuf.Serializers.EnumSerializer/EnumPair>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27229192_gshared (InternalEnumerator_1_t3353480155 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27229192(__this, method) ((  void (*) (InternalEnumerator_1_t3353480155 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m27229192_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<ProtoBuf.Serializers.EnumSerializer/EnumPair>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3281802164_gshared (InternalEnumerator_1_t3353480155 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3281802164(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3353480155 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3281802164_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<ProtoBuf.Serializers.EnumSerializer/EnumPair>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2975214191_gshared (InternalEnumerator_1_t3353480155 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2975214191(__this, method) ((  void (*) (InternalEnumerator_1_t3353480155 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2975214191_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<ProtoBuf.Serializers.EnumSerializer/EnumPair>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m4281310452_gshared (InternalEnumerator_1_t3353480155 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m4281310452(__this, method) ((  bool (*) (InternalEnumerator_1_t3353480155 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m4281310452_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<ProtoBuf.Serializers.EnumSerializer/EnumPair>::get_Current()
extern "C"  EnumPair_t276170183  InternalEnumerator_1_get_Current_m1910422687_gshared (InternalEnumerator_1_t3353480155 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1910422687(__this, method) ((  EnumPair_t276170183  (*) (InternalEnumerator_1_t3353480155 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1910422687_gshared)(__this, method)
