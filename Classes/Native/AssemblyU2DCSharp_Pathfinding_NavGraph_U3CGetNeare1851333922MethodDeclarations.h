﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.NavGraph/<GetNearest>c__AnonStorey115
struct U3CGetNearestU3Ec__AnonStorey115_t1851333922;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.NavGraph/<GetNearest>c__AnonStorey115::.ctor()
extern "C"  void U3CGetNearestU3Ec__AnonStorey115__ctor_m2132711049 (U3CGetNearestU3Ec__AnonStorey115_t1851333922 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavGraph/<GetNearest>c__AnonStorey115::<>m__348(Pathfinding.GraphNode)
extern "C"  bool U3CGetNearestU3Ec__AnonStorey115_U3CU3Em__348_m2688339385 (U3CGetNearestU3Ec__AnonStorey115_t1851333922 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
