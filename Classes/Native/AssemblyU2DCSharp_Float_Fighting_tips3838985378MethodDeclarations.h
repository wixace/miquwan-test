﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_Fighting_tips
struct Float_Fighting_tips_t3838985378;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// FloatTextUnit
struct FloatTextUnit_t2362298029;
// EventDelegate/Callback
struct Callback_t1094463061;
// IZUpdate
struct IZUpdate_t3482043738;
// SoundMgr
struct SoundMgr_t1807284905;
// UITweener
struct UITweener_t105489188;
// UILabel
struct UILabel_t291504320;
// System.String
struct String_t;
// UISprite
struct UISprite_t661437049;
// UITexture
struct UITexture_t3903132647;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_UITweener_Method1078127180.h"
#include "AssemblyU2DCSharp_UITweener105489188.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Float_Fighting_tips3838985378.h"
#include "AssemblyU2DCSharp_UISprite661437049.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "AssemblyU2DCSharp_UITexture3903132647.h"

// System.Void Float_Fighting_tips::.ctor()
extern "C"  void Float_Fighting_tips__ctor_m571598089 (Float_Fighting_tips_t3838985378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_Fighting_tips::FloatID()
extern "C"  int32_t Float_Fighting_tips_FloatID_m3217166663 (Float_Fighting_tips_t3838985378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Fighting_tips::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_Fighting_tips_OnAwake_m2948038085 (Float_Fighting_tips_t3838985378 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Fighting_tips::OnDestroy()
extern "C"  void Float_Fighting_tips_OnDestroy_m3706195330 (Float_Fighting_tips_t3838985378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Fighting_tips::Init()
extern "C"  void Float_Fighting_tips_Init_m2186732587 (Float_Fighting_tips_t3838985378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Fighting_tips::Death()
extern "C"  void Float_Fighting_tips_Death_m2956660635 (Float_Fighting_tips_t3838985378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Fighting_tips::SetFile(System.Object[])
extern "C"  void Float_Fighting_tips_SetFile_m1194479341 (Float_Fighting_tips_t3838985378 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Fighting_tips::OnInit()
extern "C"  void Float_Fighting_tips_OnInit_m2158271594 (Float_Fighting_tips_t3838985378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Fighting_tips::SetNum(System.Int32)
extern "C"  void Float_Fighting_tips_SetNum_m3423512112 (Float_Fighting_tips_t3838985378 * __this, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Float_Fighting_tips::GetPlaceNum(System.Int32,System.Int32)
extern "C"  int32_t Float_Fighting_tips_GetPlaceNum_m1462963452 (Float_Fighting_tips_t3838985378 * __this, int32_t ___num0, int32_t ___n1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Fighting_tips::ZUpdate()
extern "C"  void Float_Fighting_tips_ZUpdate_m3413446090 (Float_Fighting_tips_t3838985378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Fighting_tips::SetUV(System.Int32)
extern "C"  void Float_Fighting_tips_SetUV_m3296662075 (Float_Fighting_tips_t3838985378 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Fighting_tips::SetEnd(System.Int32)
extern "C"  void Float_Fighting_tips_SetEnd_m2724334277 (Float_Fighting_tips_t3838985378 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Fighting_tips::ilo_MoveScale1(FloatTextUnit,System.Single,System.Single,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void Float_Fighting_tips_ilo_MoveScale1_m3845551322 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, float ___from1, float ___to2, float ___duration3, float ___delay4, Callback_t1094463061 * ___call5, GameObject_t3674682005 * ___go6, int32_t ___method7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Fighting_tips::ilo_MoveAlpha2(FloatTextUnit,System.Single,System.Single,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void Float_Fighting_tips_ilo_MoveAlpha2_m1579727239 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, float ___from1, float ___to2, float ___duration3, float ___delay4, Callback_t1094463061 * ___call5, GameObject_t3674682005 * ___go6, int32_t ___method7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Fighting_tips::ilo_RemoveUpdate3(IZUpdate)
extern "C"  void Float_Fighting_tips_ilo_RemoveUpdate3_m3385273696 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___update0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SoundMgr Float_Fighting_tips::ilo_get_SoundMgr4()
extern "C"  SoundMgr_t1807284905 * Float_Fighting_tips_ilo_get_SoundMgr4_m3354774122 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Fighting_tips::ilo_ResetToBeginning5(UITweener)
extern "C"  void Float_Fighting_tips_ilo_ResetToBeginning5_m3519848696 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Fighting_tips::ilo_PlayForward6(UITweener)
extern "C"  void Float_Fighting_tips_ilo_PlayForward6_m1949928379 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Fighting_tips::ilo_set_text7(UILabel,System.String)
extern "C"  void Float_Fighting_tips_ilo_set_text7_m1129216909 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Fighting_tips::ilo_OnInit8(Float_Fighting_tips)
extern "C"  void Float_Fighting_tips_ilo_OnInit8_m2134531667 (Il2CppObject * __this /* static, unused */, Float_Fighting_tips_t3838985378 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Float_Fighting_tips::ilo_GetPlaceNum9(Float_Fighting_tips,System.Int32,System.Int32)
extern "C"  int32_t Float_Fighting_tips_ilo_GetPlaceNum9_m3465414604 (Il2CppObject * __this /* static, unused */, Float_Fighting_tips_t3838985378 * ____this0, int32_t ___num1, int32_t ___n2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_Fighting_tips::ilo_set_spriteName10(UISprite,System.String)
extern "C"  void Float_Fighting_tips_ilo_set_spriteName10_m391865181 (Il2CppObject * __this /* static, unused */, UISprite_t661437049 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect Float_Fighting_tips::ilo_get_uvRect11(UITexture)
extern "C"  Rect_t4241904616  Float_Fighting_tips_ilo_get_uvRect11_m2114029809 (Il2CppObject * __this /* static, unused */, UITexture_t3903132647 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
