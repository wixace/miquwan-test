﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweenVolumeGenerated
struct TweenVolumeGenerated_t2043400650;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// TweenVolume
struct TweenVolume_t939656517;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_TweenVolume939656517.h"

// System.Void TweenVolumeGenerated::.ctor()
extern "C"  void TweenVolumeGenerated__ctor_m688581649 (TweenVolumeGenerated_t2043400650 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenVolumeGenerated::TweenVolume_TweenVolume1(JSVCall,System.Int32)
extern "C"  bool TweenVolumeGenerated_TweenVolume_TweenVolume1_m1083049341 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenVolumeGenerated::TweenVolume_from(JSVCall)
extern "C"  void TweenVolumeGenerated_TweenVolume_from_m3848031344 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenVolumeGenerated::TweenVolume_to(JSVCall)
extern "C"  void TweenVolumeGenerated_TweenVolume_to_m1558459839 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenVolumeGenerated::TweenVolume_audioSource(JSVCall)
extern "C"  void TweenVolumeGenerated_TweenVolume_audioSource_m129010497 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenVolumeGenerated::TweenVolume_value(JSVCall)
extern "C"  void TweenVolumeGenerated_TweenVolume_value_m4210253409 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenVolumeGenerated::TweenVolume_SetEndToCurrentValue(JSVCall,System.Int32)
extern "C"  bool TweenVolumeGenerated_TweenVolume_SetEndToCurrentValue_m4190565661 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenVolumeGenerated::TweenVolume_SetStartToCurrentValue(JSVCall,System.Int32)
extern "C"  bool TweenVolumeGenerated_TweenVolume_SetStartToCurrentValue_m1480221092 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenVolumeGenerated::TweenVolume_Begin__GameObject__Single__Single(JSVCall,System.Int32)
extern "C"  bool TweenVolumeGenerated_TweenVolume_Begin__GameObject__Single__Single_m2327616795 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenVolumeGenerated::__Register()
extern "C"  void TweenVolumeGenerated___Register_m663515990 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenVolumeGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void TweenVolumeGenerated_ilo_addJSCSRel1_m1052347981 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TweenVolumeGenerated::ilo_getSingle2(System.Int32)
extern "C"  float TweenVolumeGenerated_ilo_getSingle2_m3425265143 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TweenVolumeGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t TweenVolumeGenerated_ilo_setObject3_m403752091 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TweenVolumeGenerated::ilo_get_value4(TweenVolume)
extern "C"  float TweenVolumeGenerated_ilo_get_value4_m2638612399 (Il2CppObject * __this /* static, unused */, TweenVolume_t939656517 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenVolumeGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void TweenVolumeGenerated_ilo_setSingle5_m1355218119 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TweenVolumeGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * TweenVolumeGenerated_ilo_getObject6_m1699480131 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
