﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,PetsCfg>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m156713884(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t884060697 *, int32_t, PetsCfg_t988016752 *, const MethodInfo*))KeyValuePair_2__ctor_m11197230_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,PetsCfg>::get_Key()
#define KeyValuePair_2_get_Key_m12044492(__this, method) ((  int32_t (*) (KeyValuePair_2_t884060697 *, const MethodInfo*))KeyValuePair_2_get_Key_m494458106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,PetsCfg>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3530245389(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t884060697 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m4229413435_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,PetsCfg>::get_Value()
#define KeyValuePair_2_get_Value_m3383970444(__this, method) ((  PetsCfg_t988016752 * (*) (KeyValuePair_2_t884060697 *, const MethodInfo*))KeyValuePair_2_get_Value_m1563175098_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,PetsCfg>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1716734605(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t884060697 *, PetsCfg_t988016752 *, const MethodInfo*))KeyValuePair_2_set_Value_m1296398523_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,PetsCfg>::ToString()
#define KeyValuePair_2_ToString_m173553717(__this, method) ((  String_t* (*) (KeyValuePair_2_t884060697 *, const MethodInfo*))KeyValuePair_2_ToString_m491888647_gshared)(__this, method)
