﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_SkinnedMeshRendererGenerated
struct UnityEngine_SkinnedMeshRendererGenerated_t2121708023;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3792884695;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_SkinnedMeshRendererGenerated::.ctor()
extern "C"  void UnityEngine_SkinnedMeshRendererGenerated__ctor_m1589572228 (UnityEngine_SkinnedMeshRendererGenerated_t2121708023 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SkinnedMeshRendererGenerated::SkinnedMeshRenderer_SkinnedMeshRenderer1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SkinnedMeshRendererGenerated_SkinnedMeshRenderer_SkinnedMeshRenderer1_m554168692 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkinnedMeshRendererGenerated::SkinnedMeshRenderer_bones(JSVCall)
extern "C"  void UnityEngine_SkinnedMeshRendererGenerated_SkinnedMeshRenderer_bones_m2786055733 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkinnedMeshRendererGenerated::SkinnedMeshRenderer_rootBone(JSVCall)
extern "C"  void UnityEngine_SkinnedMeshRendererGenerated_SkinnedMeshRenderer_rootBone_m746590914 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkinnedMeshRendererGenerated::SkinnedMeshRenderer_quality(JSVCall)
extern "C"  void UnityEngine_SkinnedMeshRendererGenerated_SkinnedMeshRenderer_quality_m4113683941 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkinnedMeshRendererGenerated::SkinnedMeshRenderer_sharedMesh(JSVCall)
extern "C"  void UnityEngine_SkinnedMeshRendererGenerated_SkinnedMeshRenderer_sharedMesh_m3598798486 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkinnedMeshRendererGenerated::SkinnedMeshRenderer_updateWhenOffscreen(JSVCall)
extern "C"  void UnityEngine_SkinnedMeshRendererGenerated_SkinnedMeshRenderer_updateWhenOffscreen_m917897164 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkinnedMeshRendererGenerated::SkinnedMeshRenderer_localBounds(JSVCall)
extern "C"  void UnityEngine_SkinnedMeshRendererGenerated_SkinnedMeshRenderer_localBounds_m3569611524 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SkinnedMeshRendererGenerated::SkinnedMeshRenderer_BakeMesh__Mesh(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SkinnedMeshRendererGenerated_SkinnedMeshRenderer_BakeMesh__Mesh_m2484210422 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SkinnedMeshRendererGenerated::SkinnedMeshRenderer_GetBlendShapeWeight__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SkinnedMeshRendererGenerated_SkinnedMeshRenderer_GetBlendShapeWeight__Int32_m78320723 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SkinnedMeshRendererGenerated::SkinnedMeshRenderer_SetBlendShapeWeight__Int32__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SkinnedMeshRendererGenerated_SkinnedMeshRenderer_SetBlendShapeWeight__Int32__Single_m788120335 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkinnedMeshRendererGenerated::__Register()
extern "C"  void UnityEngine_SkinnedMeshRendererGenerated___Register_m1672051907 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform[] UnityEngine_SkinnedMeshRendererGenerated::<SkinnedMeshRenderer_bones>m__2D3()
extern "C"  TransformU5BU5D_t3792884695* UnityEngine_SkinnedMeshRendererGenerated_U3CSkinnedMeshRenderer_bonesU3Em__2D3_m1517064324 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SkinnedMeshRendererGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_SkinnedMeshRendererGenerated_ilo_getObject1_m1313037582 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkinnedMeshRendererGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_SkinnedMeshRendererGenerated_ilo_addJSCSRel2_m2534833089 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkinnedMeshRendererGenerated::ilo_setArrayS3(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_SkinnedMeshRendererGenerated_ilo_setArrayS3_m1843766401 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_SkinnedMeshRendererGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_SkinnedMeshRendererGenerated_ilo_getObject4_m3293498350 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkinnedMeshRendererGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void UnityEngine_SkinnedMeshRendererGenerated_ilo_setBooleanS5_m1098580081 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SkinnedMeshRendererGenerated::ilo_setObject6(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_SkinnedMeshRendererGenerated_ilo_setObject6_m969670603 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SkinnedMeshRendererGenerated::ilo_getInt327(System.Int32)
extern "C"  int32_t UnityEngine_SkinnedMeshRendererGenerated_ilo_getInt327_m1453085777 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SkinnedMeshRendererGenerated::ilo_setSingle8(System.Int32,System.Single)
extern "C"  void UnityEngine_SkinnedMeshRendererGenerated_ilo_setSingle8_m3459987191 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SkinnedMeshRendererGenerated::ilo_getElement9(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_SkinnedMeshRendererGenerated_ilo_getElement9_m2916204498 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
