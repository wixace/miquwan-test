﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Xml_XmlDocumentGenerated
struct System_Xml_XmlDocumentGenerated_t4282235573;
// JSVCall
struct JSVCall_t3708497963;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t4231404781;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Xml_XmlDocumentGenerated::.ctor()
extern "C"  void System_Xml_XmlDocumentGenerated__ctor_m3747444822 (System_Xml_XmlDocumentGenerated_t4282235573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_XmlDocument1(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_XmlDocument1_m2297378838 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_XmlDocument2(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_XmlDocument2_m3542143319 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlDocumentGenerated::XmlDocument_BaseURI(JSVCall)
extern "C"  void System_Xml_XmlDocumentGenerated_XmlDocument_BaseURI_m2054537957 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlDocumentGenerated::XmlDocument_DocumentElement(JSVCall)
extern "C"  void System_Xml_XmlDocumentGenerated_XmlDocument_DocumentElement_m1209074463 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlDocumentGenerated::XmlDocument_DocumentType(JSVCall)
extern "C"  void System_Xml_XmlDocumentGenerated_XmlDocument_DocumentType_m1105913495 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlDocumentGenerated::XmlDocument_Implementation(JSVCall)
extern "C"  void System_Xml_XmlDocumentGenerated_XmlDocument_Implementation_m4029058298 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlDocumentGenerated::XmlDocument_InnerXml(JSVCall)
extern "C"  void System_Xml_XmlDocumentGenerated_XmlDocument_InnerXml_m3938451499 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlDocumentGenerated::XmlDocument_IsReadOnly(JSVCall)
extern "C"  void System_Xml_XmlDocumentGenerated_XmlDocument_IsReadOnly_m3546864448 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlDocumentGenerated::XmlDocument_LocalName(JSVCall)
extern "C"  void System_Xml_XmlDocumentGenerated_XmlDocument_LocalName_m3971586826 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlDocumentGenerated::XmlDocument_Name(JSVCall)
extern "C"  void System_Xml_XmlDocumentGenerated_XmlDocument_Name_m3328616897 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlDocumentGenerated::XmlDocument_NameTable(JSVCall)
extern "C"  void System_Xml_XmlDocumentGenerated_XmlDocument_NameTable_m1030690365 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlDocumentGenerated::XmlDocument_NodeType(JSVCall)
extern "C"  void System_Xml_XmlDocumentGenerated_XmlDocument_NodeType_m4165508112 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlDocumentGenerated::XmlDocument_OwnerDocument(JSVCall)
extern "C"  void System_Xml_XmlDocumentGenerated_XmlDocument_OwnerDocument_m552541362 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlDocumentGenerated::XmlDocument_PreserveWhitespace(JSVCall)
extern "C"  void System_Xml_XmlDocumentGenerated_XmlDocument_PreserveWhitespace_m2755391907 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlDocumentGenerated::XmlDocument_XmlResolver(JSVCall)
extern "C"  void System_Xml_XmlDocumentGenerated_XmlDocument_XmlResolver_m3985944515 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlDocumentGenerated::XmlDocument_ParentNode(JSVCall)
extern "C"  void System_Xml_XmlDocumentGenerated_XmlDocument_ParentNode_m3227030304 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlDocumentGenerated::XmlDocument_Schemas(JSVCall)
extern "C"  void System_Xml_XmlDocumentGenerated_XmlDocument_Schemas_m4201591470 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlDocumentGenerated::XmlDocument_SchemaInfo(JSVCall)
extern "C"  void System_Xml_XmlDocumentGenerated_XmlDocument_SchemaInfo_m1928231517 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_CloneNode__Boolean(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_CloneNode__Boolean_m2589920768 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_CreateAttribute__String__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_CreateAttribute__String__String__String_m797140766 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_CreateAttribute__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_CreateAttribute__String__String_m2830943693 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_CreateAttribute__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_CreateAttribute__String_m2063287164 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_CreateCDataSection__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_CreateCDataSection__String_m589172636 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_CreateComment__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_CreateComment__String_m2588907391 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_CreateDocumentFragment(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_CreateDocumentFragment_m234280990 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_CreateDocumentType__String__String__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_CreateDocumentType__String__String__String__String_m2100761420 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_CreateElement__String__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_CreateElement__String__String__String_m1245707006 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_CreateElement__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_CreateElement__String__String_m666581421 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_CreateElement__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_CreateElement__String_m773560156 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_CreateEntityReference__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_CreateEntityReference__String_m3478917128 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_CreateNavigator(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_CreateNavigator_m4008120742 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_CreateNode__XmlNodeType__String__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_CreateNode__XmlNodeType__String__String__String_m3990776979 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_CreateNode__XmlNodeType__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_CreateNode__XmlNodeType__String__String_m3116044354 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_CreateNode__String__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_CreateNode__String__String__String_m2783179336 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_CreateProcessingInstruction__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_CreateProcessingInstruction__String__String_m457341548 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_CreateSignificantWhitespace__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_CreateSignificantWhitespace__String_m2413049584 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_CreateTextNode__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_CreateTextNode__String_m2349918131 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_CreateWhitespace__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_CreateWhitespace__String_m922642529 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_CreateXmlDeclaration__String__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_CreateXmlDeclaration__String__String__String_m2259271913 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_GetElementById__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_GetElementById__String_m3648227520 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_GetElementsByTagName__String__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_GetElementsByTagName__String__String_m265323802 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_GetElementsByTagName__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_GetElementsByTagName__String_m3952451529 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_ImportNode__XmlNode__Boolean(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_ImportNode__XmlNode__Boolean_m4098208205 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_Load__TextReader(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_Load__TextReader_m966004205 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_Load__XmlReader(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_Load__XmlReader_m3872062271 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_Load__Stream(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_Load__Stream_m1902824221 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_Load__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_Load__String_m2434181358 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_LoadXml__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_LoadXml__String_m353728013 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_ReadNode__XmlReader(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_ReadNode__XmlReader_m2448270509 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_Save__TextWriter(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_Save__TextWriter_m2931002484 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_Save__XmlWriter(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_Save__XmlWriter_m2966852920 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_Save__Stream(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_Save__Stream_m2086331764 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_Save__String(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_Save__String_m2617688901 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.ValidationEventHandler System_Xml_XmlDocumentGenerated::XmlDocument_Validate_GetDelegate_member35_arg0(CSRepresentedObject)
extern "C"  ValidationEventHandler_t4231404781 * System_Xml_XmlDocumentGenerated_XmlDocument_Validate_GetDelegate_member35_arg0_m1068145390 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_Validate__ValidationEventHandler__XmlNode(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_Validate__ValidationEventHandler__XmlNode_m264006469 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.ValidationEventHandler System_Xml_XmlDocumentGenerated::XmlDocument_Validate_GetDelegate_member36_arg0(CSRepresentedObject)
extern "C"  ValidationEventHandler_t4231404781 * System_Xml_XmlDocumentGenerated_XmlDocument_Validate_GetDelegate_member36_arg0_m2029759407 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_Validate__ValidationEventHandler(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_Validate__ValidationEventHandler_m1651820150 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_WriteContentTo__XmlWriter(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_WriteContentTo__XmlWriter_m3211685248 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::XmlDocument_WriteTo__XmlWriter(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_XmlDocument_WriteTo__XmlWriter_m940988711 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlDocumentGenerated::__Register()
extern "C"  void System_Xml_XmlDocumentGenerated___Register_m4038052401 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.ValidationEventHandler System_Xml_XmlDocumentGenerated::<XmlDocument_Validate__ValidationEventHandler__XmlNode>m__F6()
extern "C"  ValidationEventHandler_t4231404781 * System_Xml_XmlDocumentGenerated_U3CXmlDocument_Validate__ValidationEventHandler__XmlNodeU3Em__F6_m1740422177 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.ValidationEventHandler System_Xml_XmlDocumentGenerated::<XmlDocument_Validate__ValidationEventHandler>m__F8()
extern "C"  ValidationEventHandler_t4231404781 * System_Xml_XmlDocumentGenerated_U3CXmlDocument_Validate__ValidationEventHandlerU3Em__F8_m1768120474 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_ilo_attachFinalizerObject1_m4290000993 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlDocumentGenerated::ilo_setBooleanS2(System.Int32,System.Boolean)
extern "C"  void System_Xml_XmlDocumentGenerated_ilo_setBooleanS2_m3167917542 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlDocumentGenerated::ilo_setStringS3(System.Int32,System.String)
extern "C"  void System_Xml_XmlDocumentGenerated_ilo_setStringS3_m721068321 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::ilo_getBooleanS4(System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_ilo_getBooleanS4_m3497939601 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Xml_XmlDocumentGenerated::ilo_setObject5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_Xml_XmlDocumentGenerated_ilo_setObject5_m1138854940 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System_Xml_XmlDocumentGenerated::ilo_getStringS6(System.Int32)
extern "C"  String_t* System_Xml_XmlDocumentGenerated_ilo_getStringS6_m3401494335 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Xml_XmlDocumentGenerated::ilo_getEnum7(System.Int32)
extern "C"  int32_t System_Xml_XmlDocumentGenerated_ilo_getEnum7_m1864734244 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Xml_XmlDocumentGenerated::ilo_getObject8(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_Xml_XmlDocumentGenerated_ilo_getObject8_m2222371542 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlDocumentGenerated::ilo_isFunctionS9(System.Int32)
extern "C"  bool System_Xml_XmlDocumentGenerated_ilo_isFunctionS9_m1706653958 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.Schema.ValidationEventHandler System_Xml_XmlDocumentGenerated::ilo_XmlDocument_Validate_GetDelegate_member35_arg010(CSRepresentedObject)
extern "C"  ValidationEventHandler_t4231404781 * System_Xml_XmlDocumentGenerated_ilo_XmlDocument_Validate_GetDelegate_member35_arg010_m3058452252 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject System_Xml_XmlDocumentGenerated::ilo_getFunctionS11(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * System_Xml_XmlDocumentGenerated_ilo_getFunctionS11_m2335197658 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
