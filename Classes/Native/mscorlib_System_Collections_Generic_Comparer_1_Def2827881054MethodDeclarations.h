﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.UInt32>
struct DefaultComparer_t2827881054;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.UInt32>::.ctor()
extern "C"  void DefaultComparer__ctor_m889973404_gshared (DefaultComparer_t2827881054 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m889973404(__this, method) ((  void (*) (DefaultComparer_t2827881054 *, const MethodInfo*))DefaultComparer__ctor_m889973404_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.UInt32>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m887587603_gshared (DefaultComparer_t2827881054 * __this, uint32_t ___x0, uint32_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m887587603(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t2827881054 *, uint32_t, uint32_t, const MethodInfo*))DefaultComparer_Compare_m887587603_gshared)(__this, ___x0, ___y1, method)
