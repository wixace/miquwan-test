﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._4977984fa092d3ffa2fd97f1e85aa8c2
struct _4977984fa092d3ffa2fd97f1e85aa8c2_t3215607331;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__4977984fa092d3ffa2fd97f13215607331.h"

// System.Void Little._4977984fa092d3ffa2fd97f1e85aa8c2::.ctor()
extern "C"  void _4977984fa092d3ffa2fd97f1e85aa8c2__ctor_m1985091434 (_4977984fa092d3ffa2fd97f1e85aa8c2_t3215607331 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._4977984fa092d3ffa2fd97f1e85aa8c2::_4977984fa092d3ffa2fd97f1e85aa8c2m2(System.Int32)
extern "C"  int32_t _4977984fa092d3ffa2fd97f1e85aa8c2__4977984fa092d3ffa2fd97f1e85aa8c2m2_m822068921 (_4977984fa092d3ffa2fd97f1e85aa8c2_t3215607331 * __this, int32_t ____4977984fa092d3ffa2fd97f1e85aa8c2a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._4977984fa092d3ffa2fd97f1e85aa8c2::_4977984fa092d3ffa2fd97f1e85aa8c2m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _4977984fa092d3ffa2fd97f1e85aa8c2__4977984fa092d3ffa2fd97f1e85aa8c2m_m1278534685 (_4977984fa092d3ffa2fd97f1e85aa8c2_t3215607331 * __this, int32_t ____4977984fa092d3ffa2fd97f1e85aa8c2a0, int32_t ____4977984fa092d3ffa2fd97f1e85aa8c261, int32_t ____4977984fa092d3ffa2fd97f1e85aa8c2c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._4977984fa092d3ffa2fd97f1e85aa8c2::ilo__4977984fa092d3ffa2fd97f1e85aa8c2m21(Little._4977984fa092d3ffa2fd97f1e85aa8c2,System.Int32)
extern "C"  int32_t _4977984fa092d3ffa2fd97f1e85aa8c2_ilo__4977984fa092d3ffa2fd97f1e85aa8c2m21_m3061815626 (Il2CppObject * __this /* static, unused */, _4977984fa092d3ffa2fd97f1e85aa8c2_t3215607331 * ____this0, int32_t ____4977984fa092d3ffa2fd97f1e85aa8c2a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
