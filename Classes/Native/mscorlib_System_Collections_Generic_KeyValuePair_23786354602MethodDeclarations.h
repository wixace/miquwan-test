﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,friendNpcCfg>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m676096017(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3786354602 *, int32_t, friendNpcCfg_t3890310657 *, const MethodInfo*))KeyValuePair_2__ctor_m11197230_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,friendNpcCfg>::get_Key()
#define KeyValuePair_2_get_Key_m1441432631(__this, method) ((  int32_t (*) (KeyValuePair_2_t3786354602 *, const MethodInfo*))KeyValuePair_2_get_Key_m494458106_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,friendNpcCfg>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m3151260536(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3786354602 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m4229413435_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,friendNpcCfg>::get_Value()
#define KeyValuePair_2_get_Value_m1828861851(__this, method) ((  friendNpcCfg_t3890310657 * (*) (KeyValuePair_2_t3786354602 *, const MethodInfo*))KeyValuePair_2_get_Value_m1563175098_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,friendNpcCfg>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2426225528(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3786354602 *, friendNpcCfg_t3890310657 *, const MethodInfo*))KeyValuePair_2_set_Value_m1296398523_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,friendNpcCfg>::ToString()
#define KeyValuePair_2_ToString_m2934472528(__this, method) ((  String_t* (*) (KeyValuePair_2_t3786354602 *, const MethodInfo*))KeyValuePair_2_ToString_m491888647_gshared)(__this, method)
