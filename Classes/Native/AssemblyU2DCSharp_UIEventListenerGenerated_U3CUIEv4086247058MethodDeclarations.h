﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEventListenerGenerated/<UIEventListener_onDragEnd_GetDelegate_member12_arg0>c__AnonStoreyC0
struct U3CUIEventListener_onDragEnd_GetDelegate_member12_arg0U3Ec__AnonStoreyC0_t4086247058;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UIEventListenerGenerated/<UIEventListener_onDragEnd_GetDelegate_member12_arg0>c__AnonStoreyC0::.ctor()
extern "C"  void U3CUIEventListener_onDragEnd_GetDelegate_member12_arg0U3Ec__AnonStoreyC0__ctor_m1623680793 (U3CUIEventListener_onDragEnd_GetDelegate_member12_arg0U3Ec__AnonStoreyC0_t4086247058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated/<UIEventListener_onDragEnd_GetDelegate_member12_arg0>c__AnonStoreyC0::<>m__144(UnityEngine.GameObject)
extern "C"  void U3CUIEventListener_onDragEnd_GetDelegate_member12_arg0U3Ec__AnonStoreyC0_U3CU3Em__144_m2847783273 (U3CUIEventListener_onDragEnd_GetDelegate_member12_arg0U3Ec__AnonStoreyC0_t4086247058 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
