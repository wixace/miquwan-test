﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Common.JSONTokener
struct JSONTokener_t276688996;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// SFJSONObject
struct SFJSONObject_t2643764442;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Common_JSONTokener276688996.h"
#include "AssemblyU2DCSharp_SFJSONObject2643764442.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Common.JSONTokener::.ctor(System.String)
extern "C"  void JSONTokener__ctor_m550081282 (JSONTokener_t276688996 * __this, String_t* ___ins0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Common.JSONTokener::nextValue()
extern "C"  Il2CppObject * JSONTokener_nextValue_m1022893607 (JSONTokener_t276688996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Common.JSONTokener::nextCleanInternal()
extern "C"  int32_t JSONTokener_nextCleanInternal_m3223998243 (JSONTokener_t276688996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.JSONTokener::skipToEndOfLine()
extern "C"  void JSONTokener_skipToEndOfLine_m1604325866 (JSONTokener_t276688996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.JSONTokener::nextString(System.Char)
extern "C"  String_t* JSONTokener_nextString_m2817271058 (JSONTokener_t276688996 * __this, Il2CppChar ___quote0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char Common.JSONTokener::readEscapeCharacter()
extern "C"  Il2CppChar JSONTokener_readEscapeCharacter_m2217291890 (JSONTokener_t276688996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SFJSONObject Common.JSONTokener::readObject()
extern "C"  SFJSONObject_t2643764442 * JSONTokener_readObject_m498102972 (JSONTokener_t276688996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Common.JSONTokener::ilo_nextString1(Common.JSONTokener,System.Char)
extern "C"  String_t* JSONTokener_ilo_nextString1_m2973496049 (Il2CppObject * __this /* static, unused */, JSONTokener_t276688996 * ____this0, Il2CppChar ___quote1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.JSONTokener::ilo_skipToEndOfLine2(Common.JSONTokener)
extern "C"  void JSONTokener_ilo_skipToEndOfLine2_m444078658 (Il2CppObject * __this /* static, unused */, JSONTokener_t276688996 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Common.JSONTokener::ilo_nextValue3(Common.JSONTokener)
extern "C"  Il2CppObject * JSONTokener_ilo_nextValue3_m3697145894 (Il2CppObject * __this /* static, unused */, JSONTokener_t276688996 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Common.JSONTokener::ilo_put4(SFJSONObject,System.String,System.Object)
extern "C"  void JSONTokener_ilo_put4_m2622496332 (Il2CppObject * __this /* static, unused */, SFJSONObject_t2643764442 * ____this0, String_t* ___key1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Common.JSONTokener::ilo_nextCleanInternal5(Common.JSONTokener)
extern "C"  int32_t JSONTokener_ilo_nextCleanInternal5_m4238631212 (Il2CppObject * __this /* static, unused */, JSONTokener_t276688996 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
