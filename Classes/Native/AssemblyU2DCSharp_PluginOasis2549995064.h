﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginOasis
struct  PluginOasis_t2549995064  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginOasis::uid
	String_t* ___uid_2;
	// System.String PluginOasis::session
	String_t* ___session_3;
	// System.Int32 PluginOasis::showLocation
	int32_t ___showLocation_4;
	// System.Int32 PluginOasis::showLocationType
	int32_t ___showLocationType_5;
	// Mihua.SDK.PayInfo PluginOasis::info
	PayInfo_t1775308120 * ___info_6;
	// System.String[] PluginOasis::sdkInfo
	StringU5BU5D_t4054002952* ___sdkInfo_7;

public:
	inline static int32_t get_offset_of_uid_2() { return static_cast<int32_t>(offsetof(PluginOasis_t2549995064, ___uid_2)); }
	inline String_t* get_uid_2() const { return ___uid_2; }
	inline String_t** get_address_of_uid_2() { return &___uid_2; }
	inline void set_uid_2(String_t* value)
	{
		___uid_2 = value;
		Il2CppCodeGenWriteBarrier(&___uid_2, value);
	}

	inline static int32_t get_offset_of_session_3() { return static_cast<int32_t>(offsetof(PluginOasis_t2549995064, ___session_3)); }
	inline String_t* get_session_3() const { return ___session_3; }
	inline String_t** get_address_of_session_3() { return &___session_3; }
	inline void set_session_3(String_t* value)
	{
		___session_3 = value;
		Il2CppCodeGenWriteBarrier(&___session_3, value);
	}

	inline static int32_t get_offset_of_showLocation_4() { return static_cast<int32_t>(offsetof(PluginOasis_t2549995064, ___showLocation_4)); }
	inline int32_t get_showLocation_4() const { return ___showLocation_4; }
	inline int32_t* get_address_of_showLocation_4() { return &___showLocation_4; }
	inline void set_showLocation_4(int32_t value)
	{
		___showLocation_4 = value;
	}

	inline static int32_t get_offset_of_showLocationType_5() { return static_cast<int32_t>(offsetof(PluginOasis_t2549995064, ___showLocationType_5)); }
	inline int32_t get_showLocationType_5() const { return ___showLocationType_5; }
	inline int32_t* get_address_of_showLocationType_5() { return &___showLocationType_5; }
	inline void set_showLocationType_5(int32_t value)
	{
		___showLocationType_5 = value;
	}

	inline static int32_t get_offset_of_info_6() { return static_cast<int32_t>(offsetof(PluginOasis_t2549995064, ___info_6)); }
	inline PayInfo_t1775308120 * get_info_6() const { return ___info_6; }
	inline PayInfo_t1775308120 ** get_address_of_info_6() { return &___info_6; }
	inline void set_info_6(PayInfo_t1775308120 * value)
	{
		___info_6 = value;
		Il2CppCodeGenWriteBarrier(&___info_6, value);
	}

	inline static int32_t get_offset_of_sdkInfo_7() { return static_cast<int32_t>(offsetof(PluginOasis_t2549995064, ___sdkInfo_7)); }
	inline StringU5BU5D_t4054002952* get_sdkInfo_7() const { return ___sdkInfo_7; }
	inline StringU5BU5D_t4054002952** get_address_of_sdkInfo_7() { return &___sdkInfo_7; }
	inline void set_sdkInfo_7(StringU5BU5D_t4054002952* value)
	{
		___sdkInfo_7 = value;
		Il2CppCodeGenWriteBarrier(&___sdkInfo_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
