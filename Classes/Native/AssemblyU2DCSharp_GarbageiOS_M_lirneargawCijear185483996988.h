﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_lirneargawCijear185
struct  M_lirneargawCijear185_t483996988  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_lirneargawCijear185::_memsepairZepaw
	uint32_t ____memsepairZepaw_0;
	// System.String GarbageiOS.M_lirneargawCijear185::_bisu
	String_t* ____bisu_1;
	// System.Single GarbageiOS.M_lirneargawCijear185::_searstirWharte
	float ____searstirWharte_2;
	// System.UInt32 GarbageiOS.M_lirneargawCijear185::_tirdi
	uint32_t ____tirdi_3;
	// System.Int32 GarbageiOS.M_lirneargawCijear185::_bowsteaye
	int32_t ____bowsteaye_4;
	// System.Single GarbageiOS.M_lirneargawCijear185::_xoosogai
	float ____xoosogai_5;
	// System.String GarbageiOS.M_lirneargawCijear185::_jihor
	String_t* ____jihor_6;
	// System.Int32 GarbageiOS.M_lirneargawCijear185::_sastouDairher
	int32_t ____sastouDairher_7;
	// System.Single GarbageiOS.M_lirneargawCijear185::_baitakowLereawi
	float ____baitakowLereawi_8;
	// System.Single GarbageiOS.M_lirneargawCijear185::_jarreNalhe
	float ____jarreNalhe_9;
	// System.Int32 GarbageiOS.M_lirneargawCijear185::_deartou
	int32_t ____deartou_10;
	// System.Int32 GarbageiOS.M_lirneargawCijear185::_fezoje
	int32_t ____fezoje_11;
	// System.UInt32 GarbageiOS.M_lirneargawCijear185::_xawpiKasdaijair
	uint32_t ____xawpiKasdaijair_12;
	// System.String GarbageiOS.M_lirneargawCijear185::_jihee
	String_t* ____jihee_13;
	// System.Int32 GarbageiOS.M_lirneargawCijear185::_waicerrirCecall
	int32_t ____waicerrirCecall_14;
	// System.UInt32 GarbageiOS.M_lirneargawCijear185::_sallsawtaChahistar
	uint32_t ____sallsawtaChahistar_15;
	// System.String GarbageiOS.M_lirneargawCijear185::_rallcaihaHootraswu
	String_t* ____rallcaihaHootraswu_16;
	// System.Single GarbageiOS.M_lirneargawCijear185::_yihuxaNouno
	float ____yihuxaNouno_17;
	// System.UInt32 GarbageiOS.M_lirneargawCijear185::_hearse
	uint32_t ____hearse_18;
	// System.String GarbageiOS.M_lirneargawCijear185::_moogairGameerai
	String_t* ____moogairGameerai_19;
	// System.UInt32 GarbageiOS.M_lirneargawCijear185::_yarjemwelPuherebel
	uint32_t ____yarjemwelPuherebel_20;
	// System.UInt32 GarbageiOS.M_lirneargawCijear185::_cifoutirTelehaw
	uint32_t ____cifoutirTelehaw_21;
	// System.Single GarbageiOS.M_lirneargawCijear185::_whoupow
	float ____whoupow_22;
	// System.Int32 GarbageiOS.M_lirneargawCijear185::_steterebooJortoo
	int32_t ____steterebooJortoo_23;
	// System.String GarbageiOS.M_lirneargawCijear185::_reraisi
	String_t* ____reraisi_24;
	// System.UInt32 GarbageiOS.M_lirneargawCijear185::_kenurwhasDelsai
	uint32_t ____kenurwhasDelsai_25;
	// System.UInt32 GarbageiOS.M_lirneargawCijear185::_jese
	uint32_t ____jese_26;
	// System.UInt32 GarbageiOS.M_lirneargawCijear185::_lehomur
	uint32_t ____lehomur_27;
	// System.String GarbageiOS.M_lirneargawCijear185::_lailawal
	String_t* ____lailawal_28;
	// System.Single GarbageiOS.M_lirneargawCijear185::_toozeajasBoulear
	float ____toozeajasBoulear_29;
	// System.Int32 GarbageiOS.M_lirneargawCijear185::_yecowDeboukou
	int32_t ____yecowDeboukou_30;
	// System.Int32 GarbageiOS.M_lirneargawCijear185::_kirbertear
	int32_t ____kirbertear_31;
	// System.String GarbageiOS.M_lirneargawCijear185::_soyasas
	String_t* ____soyasas_32;
	// System.Boolean GarbageiOS.M_lirneargawCijear185::_survaSerme
	bool ____survaSerme_33;
	// System.Int32 GarbageiOS.M_lirneargawCijear185::_porceetouNalgel
	int32_t ____porceetouNalgel_34;
	// System.Single GarbageiOS.M_lirneargawCijear185::_janetoCokair
	float ____janetoCokair_35;
	// System.UInt32 GarbageiOS.M_lirneargawCijear185::_jimem
	uint32_t ____jimem_36;
	// System.Boolean GarbageiOS.M_lirneargawCijear185::_citaLobal
	bool ____citaLobal_37;
	// System.Boolean GarbageiOS.M_lirneargawCijear185::_sairdas
	bool ____sairdas_38;
	// System.Int32 GarbageiOS.M_lirneargawCijear185::_xedermis
	int32_t ____xedermis_39;
	// System.String GarbageiOS.M_lirneargawCijear185::_rounearloZedre
	String_t* ____rounearloZedre_40;
	// System.Boolean GarbageiOS.M_lirneargawCijear185::_rasso
	bool ____rasso_41;

public:
	inline static int32_t get_offset_of__memsepairZepaw_0() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____memsepairZepaw_0)); }
	inline uint32_t get__memsepairZepaw_0() const { return ____memsepairZepaw_0; }
	inline uint32_t* get_address_of__memsepairZepaw_0() { return &____memsepairZepaw_0; }
	inline void set__memsepairZepaw_0(uint32_t value)
	{
		____memsepairZepaw_0 = value;
	}

	inline static int32_t get_offset_of__bisu_1() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____bisu_1)); }
	inline String_t* get__bisu_1() const { return ____bisu_1; }
	inline String_t** get_address_of__bisu_1() { return &____bisu_1; }
	inline void set__bisu_1(String_t* value)
	{
		____bisu_1 = value;
		Il2CppCodeGenWriteBarrier(&____bisu_1, value);
	}

	inline static int32_t get_offset_of__searstirWharte_2() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____searstirWharte_2)); }
	inline float get__searstirWharte_2() const { return ____searstirWharte_2; }
	inline float* get_address_of__searstirWharte_2() { return &____searstirWharte_2; }
	inline void set__searstirWharte_2(float value)
	{
		____searstirWharte_2 = value;
	}

	inline static int32_t get_offset_of__tirdi_3() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____tirdi_3)); }
	inline uint32_t get__tirdi_3() const { return ____tirdi_3; }
	inline uint32_t* get_address_of__tirdi_3() { return &____tirdi_3; }
	inline void set__tirdi_3(uint32_t value)
	{
		____tirdi_3 = value;
	}

	inline static int32_t get_offset_of__bowsteaye_4() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____bowsteaye_4)); }
	inline int32_t get__bowsteaye_4() const { return ____bowsteaye_4; }
	inline int32_t* get_address_of__bowsteaye_4() { return &____bowsteaye_4; }
	inline void set__bowsteaye_4(int32_t value)
	{
		____bowsteaye_4 = value;
	}

	inline static int32_t get_offset_of__xoosogai_5() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____xoosogai_5)); }
	inline float get__xoosogai_5() const { return ____xoosogai_5; }
	inline float* get_address_of__xoosogai_5() { return &____xoosogai_5; }
	inline void set__xoosogai_5(float value)
	{
		____xoosogai_5 = value;
	}

	inline static int32_t get_offset_of__jihor_6() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____jihor_6)); }
	inline String_t* get__jihor_6() const { return ____jihor_6; }
	inline String_t** get_address_of__jihor_6() { return &____jihor_6; }
	inline void set__jihor_6(String_t* value)
	{
		____jihor_6 = value;
		Il2CppCodeGenWriteBarrier(&____jihor_6, value);
	}

	inline static int32_t get_offset_of__sastouDairher_7() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____sastouDairher_7)); }
	inline int32_t get__sastouDairher_7() const { return ____sastouDairher_7; }
	inline int32_t* get_address_of__sastouDairher_7() { return &____sastouDairher_7; }
	inline void set__sastouDairher_7(int32_t value)
	{
		____sastouDairher_7 = value;
	}

	inline static int32_t get_offset_of__baitakowLereawi_8() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____baitakowLereawi_8)); }
	inline float get__baitakowLereawi_8() const { return ____baitakowLereawi_8; }
	inline float* get_address_of__baitakowLereawi_8() { return &____baitakowLereawi_8; }
	inline void set__baitakowLereawi_8(float value)
	{
		____baitakowLereawi_8 = value;
	}

	inline static int32_t get_offset_of__jarreNalhe_9() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____jarreNalhe_9)); }
	inline float get__jarreNalhe_9() const { return ____jarreNalhe_9; }
	inline float* get_address_of__jarreNalhe_9() { return &____jarreNalhe_9; }
	inline void set__jarreNalhe_9(float value)
	{
		____jarreNalhe_9 = value;
	}

	inline static int32_t get_offset_of__deartou_10() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____deartou_10)); }
	inline int32_t get__deartou_10() const { return ____deartou_10; }
	inline int32_t* get_address_of__deartou_10() { return &____deartou_10; }
	inline void set__deartou_10(int32_t value)
	{
		____deartou_10 = value;
	}

	inline static int32_t get_offset_of__fezoje_11() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____fezoje_11)); }
	inline int32_t get__fezoje_11() const { return ____fezoje_11; }
	inline int32_t* get_address_of__fezoje_11() { return &____fezoje_11; }
	inline void set__fezoje_11(int32_t value)
	{
		____fezoje_11 = value;
	}

	inline static int32_t get_offset_of__xawpiKasdaijair_12() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____xawpiKasdaijair_12)); }
	inline uint32_t get__xawpiKasdaijair_12() const { return ____xawpiKasdaijair_12; }
	inline uint32_t* get_address_of__xawpiKasdaijair_12() { return &____xawpiKasdaijair_12; }
	inline void set__xawpiKasdaijair_12(uint32_t value)
	{
		____xawpiKasdaijair_12 = value;
	}

	inline static int32_t get_offset_of__jihee_13() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____jihee_13)); }
	inline String_t* get__jihee_13() const { return ____jihee_13; }
	inline String_t** get_address_of__jihee_13() { return &____jihee_13; }
	inline void set__jihee_13(String_t* value)
	{
		____jihee_13 = value;
		Il2CppCodeGenWriteBarrier(&____jihee_13, value);
	}

	inline static int32_t get_offset_of__waicerrirCecall_14() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____waicerrirCecall_14)); }
	inline int32_t get__waicerrirCecall_14() const { return ____waicerrirCecall_14; }
	inline int32_t* get_address_of__waicerrirCecall_14() { return &____waicerrirCecall_14; }
	inline void set__waicerrirCecall_14(int32_t value)
	{
		____waicerrirCecall_14 = value;
	}

	inline static int32_t get_offset_of__sallsawtaChahistar_15() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____sallsawtaChahistar_15)); }
	inline uint32_t get__sallsawtaChahistar_15() const { return ____sallsawtaChahistar_15; }
	inline uint32_t* get_address_of__sallsawtaChahistar_15() { return &____sallsawtaChahistar_15; }
	inline void set__sallsawtaChahistar_15(uint32_t value)
	{
		____sallsawtaChahistar_15 = value;
	}

	inline static int32_t get_offset_of__rallcaihaHootraswu_16() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____rallcaihaHootraswu_16)); }
	inline String_t* get__rallcaihaHootraswu_16() const { return ____rallcaihaHootraswu_16; }
	inline String_t** get_address_of__rallcaihaHootraswu_16() { return &____rallcaihaHootraswu_16; }
	inline void set__rallcaihaHootraswu_16(String_t* value)
	{
		____rallcaihaHootraswu_16 = value;
		Il2CppCodeGenWriteBarrier(&____rallcaihaHootraswu_16, value);
	}

	inline static int32_t get_offset_of__yihuxaNouno_17() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____yihuxaNouno_17)); }
	inline float get__yihuxaNouno_17() const { return ____yihuxaNouno_17; }
	inline float* get_address_of__yihuxaNouno_17() { return &____yihuxaNouno_17; }
	inline void set__yihuxaNouno_17(float value)
	{
		____yihuxaNouno_17 = value;
	}

	inline static int32_t get_offset_of__hearse_18() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____hearse_18)); }
	inline uint32_t get__hearse_18() const { return ____hearse_18; }
	inline uint32_t* get_address_of__hearse_18() { return &____hearse_18; }
	inline void set__hearse_18(uint32_t value)
	{
		____hearse_18 = value;
	}

	inline static int32_t get_offset_of__moogairGameerai_19() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____moogairGameerai_19)); }
	inline String_t* get__moogairGameerai_19() const { return ____moogairGameerai_19; }
	inline String_t** get_address_of__moogairGameerai_19() { return &____moogairGameerai_19; }
	inline void set__moogairGameerai_19(String_t* value)
	{
		____moogairGameerai_19 = value;
		Il2CppCodeGenWriteBarrier(&____moogairGameerai_19, value);
	}

	inline static int32_t get_offset_of__yarjemwelPuherebel_20() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____yarjemwelPuherebel_20)); }
	inline uint32_t get__yarjemwelPuherebel_20() const { return ____yarjemwelPuherebel_20; }
	inline uint32_t* get_address_of__yarjemwelPuherebel_20() { return &____yarjemwelPuherebel_20; }
	inline void set__yarjemwelPuherebel_20(uint32_t value)
	{
		____yarjemwelPuherebel_20 = value;
	}

	inline static int32_t get_offset_of__cifoutirTelehaw_21() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____cifoutirTelehaw_21)); }
	inline uint32_t get__cifoutirTelehaw_21() const { return ____cifoutirTelehaw_21; }
	inline uint32_t* get_address_of__cifoutirTelehaw_21() { return &____cifoutirTelehaw_21; }
	inline void set__cifoutirTelehaw_21(uint32_t value)
	{
		____cifoutirTelehaw_21 = value;
	}

	inline static int32_t get_offset_of__whoupow_22() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____whoupow_22)); }
	inline float get__whoupow_22() const { return ____whoupow_22; }
	inline float* get_address_of__whoupow_22() { return &____whoupow_22; }
	inline void set__whoupow_22(float value)
	{
		____whoupow_22 = value;
	}

	inline static int32_t get_offset_of__steterebooJortoo_23() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____steterebooJortoo_23)); }
	inline int32_t get__steterebooJortoo_23() const { return ____steterebooJortoo_23; }
	inline int32_t* get_address_of__steterebooJortoo_23() { return &____steterebooJortoo_23; }
	inline void set__steterebooJortoo_23(int32_t value)
	{
		____steterebooJortoo_23 = value;
	}

	inline static int32_t get_offset_of__reraisi_24() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____reraisi_24)); }
	inline String_t* get__reraisi_24() const { return ____reraisi_24; }
	inline String_t** get_address_of__reraisi_24() { return &____reraisi_24; }
	inline void set__reraisi_24(String_t* value)
	{
		____reraisi_24 = value;
		Il2CppCodeGenWriteBarrier(&____reraisi_24, value);
	}

	inline static int32_t get_offset_of__kenurwhasDelsai_25() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____kenurwhasDelsai_25)); }
	inline uint32_t get__kenurwhasDelsai_25() const { return ____kenurwhasDelsai_25; }
	inline uint32_t* get_address_of__kenurwhasDelsai_25() { return &____kenurwhasDelsai_25; }
	inline void set__kenurwhasDelsai_25(uint32_t value)
	{
		____kenurwhasDelsai_25 = value;
	}

	inline static int32_t get_offset_of__jese_26() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____jese_26)); }
	inline uint32_t get__jese_26() const { return ____jese_26; }
	inline uint32_t* get_address_of__jese_26() { return &____jese_26; }
	inline void set__jese_26(uint32_t value)
	{
		____jese_26 = value;
	}

	inline static int32_t get_offset_of__lehomur_27() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____lehomur_27)); }
	inline uint32_t get__lehomur_27() const { return ____lehomur_27; }
	inline uint32_t* get_address_of__lehomur_27() { return &____lehomur_27; }
	inline void set__lehomur_27(uint32_t value)
	{
		____lehomur_27 = value;
	}

	inline static int32_t get_offset_of__lailawal_28() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____lailawal_28)); }
	inline String_t* get__lailawal_28() const { return ____lailawal_28; }
	inline String_t** get_address_of__lailawal_28() { return &____lailawal_28; }
	inline void set__lailawal_28(String_t* value)
	{
		____lailawal_28 = value;
		Il2CppCodeGenWriteBarrier(&____lailawal_28, value);
	}

	inline static int32_t get_offset_of__toozeajasBoulear_29() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____toozeajasBoulear_29)); }
	inline float get__toozeajasBoulear_29() const { return ____toozeajasBoulear_29; }
	inline float* get_address_of__toozeajasBoulear_29() { return &____toozeajasBoulear_29; }
	inline void set__toozeajasBoulear_29(float value)
	{
		____toozeajasBoulear_29 = value;
	}

	inline static int32_t get_offset_of__yecowDeboukou_30() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____yecowDeboukou_30)); }
	inline int32_t get__yecowDeboukou_30() const { return ____yecowDeboukou_30; }
	inline int32_t* get_address_of__yecowDeboukou_30() { return &____yecowDeboukou_30; }
	inline void set__yecowDeboukou_30(int32_t value)
	{
		____yecowDeboukou_30 = value;
	}

	inline static int32_t get_offset_of__kirbertear_31() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____kirbertear_31)); }
	inline int32_t get__kirbertear_31() const { return ____kirbertear_31; }
	inline int32_t* get_address_of__kirbertear_31() { return &____kirbertear_31; }
	inline void set__kirbertear_31(int32_t value)
	{
		____kirbertear_31 = value;
	}

	inline static int32_t get_offset_of__soyasas_32() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____soyasas_32)); }
	inline String_t* get__soyasas_32() const { return ____soyasas_32; }
	inline String_t** get_address_of__soyasas_32() { return &____soyasas_32; }
	inline void set__soyasas_32(String_t* value)
	{
		____soyasas_32 = value;
		Il2CppCodeGenWriteBarrier(&____soyasas_32, value);
	}

	inline static int32_t get_offset_of__survaSerme_33() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____survaSerme_33)); }
	inline bool get__survaSerme_33() const { return ____survaSerme_33; }
	inline bool* get_address_of__survaSerme_33() { return &____survaSerme_33; }
	inline void set__survaSerme_33(bool value)
	{
		____survaSerme_33 = value;
	}

	inline static int32_t get_offset_of__porceetouNalgel_34() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____porceetouNalgel_34)); }
	inline int32_t get__porceetouNalgel_34() const { return ____porceetouNalgel_34; }
	inline int32_t* get_address_of__porceetouNalgel_34() { return &____porceetouNalgel_34; }
	inline void set__porceetouNalgel_34(int32_t value)
	{
		____porceetouNalgel_34 = value;
	}

	inline static int32_t get_offset_of__janetoCokair_35() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____janetoCokair_35)); }
	inline float get__janetoCokair_35() const { return ____janetoCokair_35; }
	inline float* get_address_of__janetoCokair_35() { return &____janetoCokair_35; }
	inline void set__janetoCokair_35(float value)
	{
		____janetoCokair_35 = value;
	}

	inline static int32_t get_offset_of__jimem_36() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____jimem_36)); }
	inline uint32_t get__jimem_36() const { return ____jimem_36; }
	inline uint32_t* get_address_of__jimem_36() { return &____jimem_36; }
	inline void set__jimem_36(uint32_t value)
	{
		____jimem_36 = value;
	}

	inline static int32_t get_offset_of__citaLobal_37() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____citaLobal_37)); }
	inline bool get__citaLobal_37() const { return ____citaLobal_37; }
	inline bool* get_address_of__citaLobal_37() { return &____citaLobal_37; }
	inline void set__citaLobal_37(bool value)
	{
		____citaLobal_37 = value;
	}

	inline static int32_t get_offset_of__sairdas_38() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____sairdas_38)); }
	inline bool get__sairdas_38() const { return ____sairdas_38; }
	inline bool* get_address_of__sairdas_38() { return &____sairdas_38; }
	inline void set__sairdas_38(bool value)
	{
		____sairdas_38 = value;
	}

	inline static int32_t get_offset_of__xedermis_39() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____xedermis_39)); }
	inline int32_t get__xedermis_39() const { return ____xedermis_39; }
	inline int32_t* get_address_of__xedermis_39() { return &____xedermis_39; }
	inline void set__xedermis_39(int32_t value)
	{
		____xedermis_39 = value;
	}

	inline static int32_t get_offset_of__rounearloZedre_40() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____rounearloZedre_40)); }
	inline String_t* get__rounearloZedre_40() const { return ____rounearloZedre_40; }
	inline String_t** get_address_of__rounearloZedre_40() { return &____rounearloZedre_40; }
	inline void set__rounearloZedre_40(String_t* value)
	{
		____rounearloZedre_40 = value;
		Il2CppCodeGenWriteBarrier(&____rounearloZedre_40, value);
	}

	inline static int32_t get_offset_of__rasso_41() { return static_cast<int32_t>(offsetof(M_lirneargawCijear185_t483996988, ____rasso_41)); }
	inline bool get__rasso_41() const { return ____rasso_41; }
	inline bool* get_address_of__rasso_41() { return &____rasso_41; }
	inline void set__rasso_41(bool value)
	{
		____rasso_41 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
