﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Object,UnityEngine.Vector3,System.Object>
struct U3CDelayCallExecU3Ec__Iterator41_3_t123600646;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Object,UnityEngine.Vector3,System.Object>::.ctor()
extern "C"  void U3CDelayCallExecU3Ec__Iterator41_3__ctor_m2899279191_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t123600646 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3__ctor_m2899279191(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator41_3_t123600646 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3__ctor_m2899279191_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Object,UnityEngine.Vector3,System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1717675291_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t123600646 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1717675291(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator41_3_t123600646 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1717675291_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Object,UnityEngine.Vector3,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_IEnumerator_get_Current_m1434402479_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t123600646 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_IEnumerator_get_Current_m1434402479(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator41_3_t123600646 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_IEnumerator_get_Current_m1434402479_gshared)(__this, method)
// System.Boolean BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Object,UnityEngine.Vector3,System.Object>::MoveNext()
extern "C"  bool U3CDelayCallExecU3Ec__Iterator41_3_MoveNext_m3530715197_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t123600646 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_MoveNext_m3530715197(__this, method) ((  bool (*) (U3CDelayCallExecU3Ec__Iterator41_3_t123600646 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_MoveNext_m3530715197_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Object,UnityEngine.Vector3,System.Object>::Dispose()
extern "C"  void U3CDelayCallExecU3Ec__Iterator41_3_Dispose_m2966777812_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t123600646 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_Dispose_m2966777812(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator41_3_t123600646 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_Dispose_m2966777812_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Object,UnityEngine.Vector3,System.Object>::Reset()
extern "C"  void U3CDelayCallExecU3Ec__Iterator41_3_Reset_m545712132_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t123600646 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_Reset_m545712132(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator41_3_t123600646 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_Reset_m545712132_gshared)(__this, method)
