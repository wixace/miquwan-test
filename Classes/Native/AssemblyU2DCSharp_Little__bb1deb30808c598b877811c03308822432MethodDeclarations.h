﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._bb1deb30808c598b877811c02f8055f6
struct _bb1deb30808c598b877811c02f8055f6_t3308822432;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._bb1deb30808c598b877811c02f8055f6::.ctor()
extern "C"  void _bb1deb30808c598b877811c02f8055f6__ctor_m2696157837 (_bb1deb30808c598b877811c02f8055f6_t3308822432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._bb1deb30808c598b877811c02f8055f6::_bb1deb30808c598b877811c02f8055f6m2(System.Int32)
extern "C"  int32_t _bb1deb30808c598b877811c02f8055f6__bb1deb30808c598b877811c02f8055f6m2_m4157468057 (_bb1deb30808c598b877811c02f8055f6_t3308822432 * __this, int32_t ____bb1deb30808c598b877811c02f8055f6a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._bb1deb30808c598b877811c02f8055f6::_bb1deb30808c598b877811c02f8055f6m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _bb1deb30808c598b877811c02f8055f6__bb1deb30808c598b877811c02f8055f6m_m161888573 (_bb1deb30808c598b877811c02f8055f6_t3308822432 * __this, int32_t ____bb1deb30808c598b877811c02f8055f6a0, int32_t ____bb1deb30808c598b877811c02f8055f6791, int32_t ____bb1deb30808c598b877811c02f8055f6c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
