﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UISpriteAnimationGenerated
struct UISpriteAnimationGenerated_t3721166020;
// JSVCall
struct JSVCall_t3708497963;
// UISpriteAnimation
struct UISpriteAnimation_t4279777547;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_UISpriteAnimation4279777547.h"
#include "mscorlib_System_String7231557.h"

// System.Void UISpriteAnimationGenerated::.ctor()
extern "C"  void UISpriteAnimationGenerated__ctor_m1783760983 (UISpriteAnimationGenerated_t3721166020 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteAnimationGenerated::UISpriteAnimation_UISpriteAnimation1(JSVCall,System.Int32)
extern "C"  bool UISpriteAnimationGenerated_UISpriteAnimation_UISpriteAnimation1_m496622699 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimationGenerated::UISpriteAnimation_frames(JSVCall)
extern "C"  void UISpriteAnimationGenerated_UISpriteAnimation_frames_m1268259840 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimationGenerated::UISpriteAnimation_framesPerSecond(JSVCall)
extern "C"  void UISpriteAnimationGenerated_UISpriteAnimation_framesPerSecond_m484912027 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimationGenerated::UISpriteAnimation_namePrefix(JSVCall)
extern "C"  void UISpriteAnimationGenerated_UISpriteAnimation_namePrefix_m3347651977 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimationGenerated::UISpriteAnimation_loop(JSVCall)
extern "C"  void UISpriteAnimationGenerated_UISpriteAnimation_loop_m4043952066 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimationGenerated::UISpriteAnimation_isPlaying(JSVCall)
extern "C"  void UISpriteAnimationGenerated_UISpriteAnimation_isPlaying_m1948678658 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteAnimationGenerated::UISpriteAnimation_Pause(JSVCall,System.Int32)
extern "C"  bool UISpriteAnimationGenerated_UISpriteAnimation_Pause_m3698798227 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteAnimationGenerated::UISpriteAnimation_Play(JSVCall,System.Int32)
extern "C"  bool UISpriteAnimationGenerated_UISpriteAnimation_Play_m2415486905 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteAnimationGenerated::UISpriteAnimation_RebuildSpriteList(JSVCall,System.Int32)
extern "C"  bool UISpriteAnimationGenerated_UISpriteAnimation_RebuildSpriteList_m1778881563 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteAnimationGenerated::UISpriteAnimation_ResetToBeginning(JSVCall,System.Int32)
extern "C"  bool UISpriteAnimationGenerated_UISpriteAnimation_ResetToBeginning_m3255251224 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimationGenerated::__Register()
extern "C"  void UISpriteAnimationGenerated___Register_m3653898960 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UISpriteAnimationGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UISpriteAnimationGenerated_ilo_getObject1_m2742053275 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteAnimationGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UISpriteAnimationGenerated_ilo_attachFinalizerObject2_m705146793 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UISpriteAnimationGenerated::ilo_get_framesPerSecond3(UISpriteAnimation)
extern "C"  int32_t UISpriteAnimationGenerated_ilo_get_framesPerSecond3_m1576438984 (Il2CppObject * __this /* static, unused */, UISpriteAnimation_t4279777547 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimationGenerated::ilo_setInt324(System.Int32,System.Int32)
extern "C"  void UISpriteAnimationGenerated_ilo_setInt324_m2046642300 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UISpriteAnimationGenerated::ilo_getInt325(System.Int32)
extern "C"  int32_t UISpriteAnimationGenerated_ilo_getInt325_m1255026082 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UISpriteAnimationGenerated::ilo_get_namePrefix6(UISpriteAnimation)
extern "C"  String_t* UISpriteAnimationGenerated_ilo_get_namePrefix6_m184251640 (Il2CppObject * __this /* static, unused */, UISpriteAnimation_t4279777547 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimationGenerated::ilo_setStringS7(System.Int32,System.String)
extern "C"  void UISpriteAnimationGenerated_ilo_setStringS7_m1808701094 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimationGenerated::ilo_set_namePrefix8(UISpriteAnimation,System.String)
extern "C"  void UISpriteAnimationGenerated_ilo_set_namePrefix8_m3619630531 (Il2CppObject * __this /* static, unused */, UISpriteAnimation_t4279777547 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimationGenerated::ilo_set_loop9(UISpriteAnimation,System.Boolean)
extern "C"  void UISpriteAnimationGenerated_ilo_set_loop9_m2822876816 (Il2CppObject * __this /* static, unused */, UISpriteAnimation_t4279777547 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UISpriteAnimationGenerated::ilo_get_isPlaying10(UISpriteAnimation)
extern "C"  bool UISpriteAnimationGenerated_ilo_get_isPlaying10_m1986734879 (Il2CppObject * __this /* static, unused */, UISpriteAnimation_t4279777547 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimationGenerated::ilo_setBooleanS11(System.Int32,System.Boolean)
extern "C"  void UISpriteAnimationGenerated_ilo_setBooleanS11_m3238474845 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimationGenerated::ilo_RebuildSpriteList12(UISpriteAnimation)
extern "C"  void UISpriteAnimationGenerated_ilo_RebuildSpriteList12_m2294711590 (Il2CppObject * __this /* static, unused */, UISpriteAnimation_t4279777547 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UISpriteAnimationGenerated::ilo_ResetToBeginning13(UISpriteAnimation)
extern "C"  void UISpriteAnimationGenerated_ilo_ResetToBeginning13_m3446091890 (Il2CppObject * __this /* static, unused */, UISpriteAnimation_t4279777547 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
