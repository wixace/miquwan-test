﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_sermemtereRoupu2
struct  M_sermemtereRoupu2_t3656819710  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_sermemtereRoupu2::_zitur
	int32_t ____zitur_0;
	// System.Boolean GarbageiOS.M_sermemtereRoupu2::_mallner
	bool ____mallner_1;
	// System.String GarbageiOS.M_sermemtereRoupu2::_soucechu
	String_t* ____soucechu_2;
	// System.Int32 GarbageiOS.M_sermemtereRoupu2::_samalou
	int32_t ____samalou_3;
	// System.Int32 GarbageiOS.M_sermemtereRoupu2::_corsecel
	int32_t ____corsecel_4;
	// System.Int32 GarbageiOS.M_sermemtereRoupu2::_feecosearXempo
	int32_t ____feecosearXempo_5;
	// System.Int32 GarbageiOS.M_sermemtereRoupu2::_dalhe
	int32_t ____dalhe_6;
	// System.String GarbageiOS.M_sermemtereRoupu2::_joojawchem
	String_t* ____joojawchem_7;
	// System.Boolean GarbageiOS.M_sermemtereRoupu2::_yezer
	bool ____yezer_8;
	// System.Single GarbageiOS.M_sermemtereRoupu2::_beqirway
	float ____beqirway_9;
	// System.Single GarbageiOS.M_sermemtereRoupu2::_tozenu
	float ____tozenu_10;
	// System.Int32 GarbageiOS.M_sermemtereRoupu2::_serawhu
	int32_t ____serawhu_11;
	// System.Single GarbageiOS.M_sermemtereRoupu2::_beltarelStoutearball
	float ____beltarelStoutearball_12;
	// System.String GarbageiOS.M_sermemtereRoupu2::_coutate
	String_t* ____coutate_13;
	// System.UInt32 GarbageiOS.M_sermemtereRoupu2::_jasbelTorpaixou
	uint32_t ____jasbelTorpaixou_14;
	// System.UInt32 GarbageiOS.M_sermemtereRoupu2::_rirpel
	uint32_t ____rirpel_15;
	// System.Single GarbageiOS.M_sermemtereRoupu2::_kouru
	float ____kouru_16;
	// System.String GarbageiOS.M_sermemtereRoupu2::_veadeWoujir
	String_t* ____veadeWoujir_17;
	// System.Single GarbageiOS.M_sermemtereRoupu2::_terede
	float ____terede_18;
	// System.Boolean GarbageiOS.M_sermemtereRoupu2::_wudoqurNismatru
	bool ____wudoqurNismatru_19;

public:
	inline static int32_t get_offset_of__zitur_0() { return static_cast<int32_t>(offsetof(M_sermemtereRoupu2_t3656819710, ____zitur_0)); }
	inline int32_t get__zitur_0() const { return ____zitur_0; }
	inline int32_t* get_address_of__zitur_0() { return &____zitur_0; }
	inline void set__zitur_0(int32_t value)
	{
		____zitur_0 = value;
	}

	inline static int32_t get_offset_of__mallner_1() { return static_cast<int32_t>(offsetof(M_sermemtereRoupu2_t3656819710, ____mallner_1)); }
	inline bool get__mallner_1() const { return ____mallner_1; }
	inline bool* get_address_of__mallner_1() { return &____mallner_1; }
	inline void set__mallner_1(bool value)
	{
		____mallner_1 = value;
	}

	inline static int32_t get_offset_of__soucechu_2() { return static_cast<int32_t>(offsetof(M_sermemtereRoupu2_t3656819710, ____soucechu_2)); }
	inline String_t* get__soucechu_2() const { return ____soucechu_2; }
	inline String_t** get_address_of__soucechu_2() { return &____soucechu_2; }
	inline void set__soucechu_2(String_t* value)
	{
		____soucechu_2 = value;
		Il2CppCodeGenWriteBarrier(&____soucechu_2, value);
	}

	inline static int32_t get_offset_of__samalou_3() { return static_cast<int32_t>(offsetof(M_sermemtereRoupu2_t3656819710, ____samalou_3)); }
	inline int32_t get__samalou_3() const { return ____samalou_3; }
	inline int32_t* get_address_of__samalou_3() { return &____samalou_3; }
	inline void set__samalou_3(int32_t value)
	{
		____samalou_3 = value;
	}

	inline static int32_t get_offset_of__corsecel_4() { return static_cast<int32_t>(offsetof(M_sermemtereRoupu2_t3656819710, ____corsecel_4)); }
	inline int32_t get__corsecel_4() const { return ____corsecel_4; }
	inline int32_t* get_address_of__corsecel_4() { return &____corsecel_4; }
	inline void set__corsecel_4(int32_t value)
	{
		____corsecel_4 = value;
	}

	inline static int32_t get_offset_of__feecosearXempo_5() { return static_cast<int32_t>(offsetof(M_sermemtereRoupu2_t3656819710, ____feecosearXempo_5)); }
	inline int32_t get__feecosearXempo_5() const { return ____feecosearXempo_5; }
	inline int32_t* get_address_of__feecosearXempo_5() { return &____feecosearXempo_5; }
	inline void set__feecosearXempo_5(int32_t value)
	{
		____feecosearXempo_5 = value;
	}

	inline static int32_t get_offset_of__dalhe_6() { return static_cast<int32_t>(offsetof(M_sermemtereRoupu2_t3656819710, ____dalhe_6)); }
	inline int32_t get__dalhe_6() const { return ____dalhe_6; }
	inline int32_t* get_address_of__dalhe_6() { return &____dalhe_6; }
	inline void set__dalhe_6(int32_t value)
	{
		____dalhe_6 = value;
	}

	inline static int32_t get_offset_of__joojawchem_7() { return static_cast<int32_t>(offsetof(M_sermemtereRoupu2_t3656819710, ____joojawchem_7)); }
	inline String_t* get__joojawchem_7() const { return ____joojawchem_7; }
	inline String_t** get_address_of__joojawchem_7() { return &____joojawchem_7; }
	inline void set__joojawchem_7(String_t* value)
	{
		____joojawchem_7 = value;
		Il2CppCodeGenWriteBarrier(&____joojawchem_7, value);
	}

	inline static int32_t get_offset_of__yezer_8() { return static_cast<int32_t>(offsetof(M_sermemtereRoupu2_t3656819710, ____yezer_8)); }
	inline bool get__yezer_8() const { return ____yezer_8; }
	inline bool* get_address_of__yezer_8() { return &____yezer_8; }
	inline void set__yezer_8(bool value)
	{
		____yezer_8 = value;
	}

	inline static int32_t get_offset_of__beqirway_9() { return static_cast<int32_t>(offsetof(M_sermemtereRoupu2_t3656819710, ____beqirway_9)); }
	inline float get__beqirway_9() const { return ____beqirway_9; }
	inline float* get_address_of__beqirway_9() { return &____beqirway_9; }
	inline void set__beqirway_9(float value)
	{
		____beqirway_9 = value;
	}

	inline static int32_t get_offset_of__tozenu_10() { return static_cast<int32_t>(offsetof(M_sermemtereRoupu2_t3656819710, ____tozenu_10)); }
	inline float get__tozenu_10() const { return ____tozenu_10; }
	inline float* get_address_of__tozenu_10() { return &____tozenu_10; }
	inline void set__tozenu_10(float value)
	{
		____tozenu_10 = value;
	}

	inline static int32_t get_offset_of__serawhu_11() { return static_cast<int32_t>(offsetof(M_sermemtereRoupu2_t3656819710, ____serawhu_11)); }
	inline int32_t get__serawhu_11() const { return ____serawhu_11; }
	inline int32_t* get_address_of__serawhu_11() { return &____serawhu_11; }
	inline void set__serawhu_11(int32_t value)
	{
		____serawhu_11 = value;
	}

	inline static int32_t get_offset_of__beltarelStoutearball_12() { return static_cast<int32_t>(offsetof(M_sermemtereRoupu2_t3656819710, ____beltarelStoutearball_12)); }
	inline float get__beltarelStoutearball_12() const { return ____beltarelStoutearball_12; }
	inline float* get_address_of__beltarelStoutearball_12() { return &____beltarelStoutearball_12; }
	inline void set__beltarelStoutearball_12(float value)
	{
		____beltarelStoutearball_12 = value;
	}

	inline static int32_t get_offset_of__coutate_13() { return static_cast<int32_t>(offsetof(M_sermemtereRoupu2_t3656819710, ____coutate_13)); }
	inline String_t* get__coutate_13() const { return ____coutate_13; }
	inline String_t** get_address_of__coutate_13() { return &____coutate_13; }
	inline void set__coutate_13(String_t* value)
	{
		____coutate_13 = value;
		Il2CppCodeGenWriteBarrier(&____coutate_13, value);
	}

	inline static int32_t get_offset_of__jasbelTorpaixou_14() { return static_cast<int32_t>(offsetof(M_sermemtereRoupu2_t3656819710, ____jasbelTorpaixou_14)); }
	inline uint32_t get__jasbelTorpaixou_14() const { return ____jasbelTorpaixou_14; }
	inline uint32_t* get_address_of__jasbelTorpaixou_14() { return &____jasbelTorpaixou_14; }
	inline void set__jasbelTorpaixou_14(uint32_t value)
	{
		____jasbelTorpaixou_14 = value;
	}

	inline static int32_t get_offset_of__rirpel_15() { return static_cast<int32_t>(offsetof(M_sermemtereRoupu2_t3656819710, ____rirpel_15)); }
	inline uint32_t get__rirpel_15() const { return ____rirpel_15; }
	inline uint32_t* get_address_of__rirpel_15() { return &____rirpel_15; }
	inline void set__rirpel_15(uint32_t value)
	{
		____rirpel_15 = value;
	}

	inline static int32_t get_offset_of__kouru_16() { return static_cast<int32_t>(offsetof(M_sermemtereRoupu2_t3656819710, ____kouru_16)); }
	inline float get__kouru_16() const { return ____kouru_16; }
	inline float* get_address_of__kouru_16() { return &____kouru_16; }
	inline void set__kouru_16(float value)
	{
		____kouru_16 = value;
	}

	inline static int32_t get_offset_of__veadeWoujir_17() { return static_cast<int32_t>(offsetof(M_sermemtereRoupu2_t3656819710, ____veadeWoujir_17)); }
	inline String_t* get__veadeWoujir_17() const { return ____veadeWoujir_17; }
	inline String_t** get_address_of__veadeWoujir_17() { return &____veadeWoujir_17; }
	inline void set__veadeWoujir_17(String_t* value)
	{
		____veadeWoujir_17 = value;
		Il2CppCodeGenWriteBarrier(&____veadeWoujir_17, value);
	}

	inline static int32_t get_offset_of__terede_18() { return static_cast<int32_t>(offsetof(M_sermemtereRoupu2_t3656819710, ____terede_18)); }
	inline float get__terede_18() const { return ____terede_18; }
	inline float* get_address_of__terede_18() { return &____terede_18; }
	inline void set__terede_18(float value)
	{
		____terede_18 = value;
	}

	inline static int32_t get_offset_of__wudoqurNismatru_19() { return static_cast<int32_t>(offsetof(M_sermemtereRoupu2_t3656819710, ____wudoqurNismatru_19)); }
	inline bool get__wudoqurNismatru_19() const { return ____wudoqurNismatru_19; }
	inline bool* get_address_of__wudoqurNismatru_19() { return &____wudoqurNismatru_19; }
	inline void set__wudoqurNismatru_19(bool value)
	{
		____wudoqurNismatru_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
