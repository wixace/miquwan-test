﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3512030511MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt16>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1103222803(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2162409685 *, Dictionary_2_t845086293 *, const MethodInfo*))Enumerator__ctor_m2846053953_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt16>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m615438062(__this, method) ((  Il2CppObject * (*) (Enumerator_t2162409685 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3872555648_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt16>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m918254274(__this, method) ((  void (*) (Enumerator_t2162409685 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2577642452_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m4271599051(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2162409685 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m854288221_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3679496522(__this, method) ((  Il2CppObject * (*) (Enumerator_t2162409685 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m739630940_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt16>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2286509404(__this, method) ((  Il2CppObject * (*) (Enumerator_t2162409685 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3164165870_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt16>::MoveNext()
#define Enumerator_MoveNext_m2799898286(__this, method) ((  bool (*) (Enumerator_t2162409685 *, const MethodInfo*))Enumerator_MoveNext_m3864346304_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt16>::get_Current()
#define Enumerator_get_Current_m1571085506(__this, method) ((  KeyValuePair_2_t743866999  (*) (Enumerator_t2162409685 *, const MethodInfo*))Enumerator_get_Current_m2798443376_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt16>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1774964731(__this, method) ((  String_t* (*) (Enumerator_t2162409685 *, const MethodInfo*))Enumerator_get_CurrentKey_m2936679053_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt16>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m548790751(__this, method) ((  uint16_t (*) (Enumerator_t2162409685 *, const MethodInfo*))Enumerator_get_CurrentValue_m264757233_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt16>::Reset()
#define Enumerator_Reset_m3673294309(__this, method) ((  void (*) (Enumerator_t2162409685 *, const MethodInfo*))Enumerator_Reset_m653401875_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt16>::VerifyState()
#define Enumerator_VerifyState_m1621136942(__this, method) ((  void (*) (Enumerator_t2162409685 *, const MethodInfo*))Enumerator_VerifyState_m2848494812_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt16>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1584344598(__this, method) ((  void (*) (Enumerator_t2162409685 *, const MethodInfo*))Enumerator_VerifyCurrent_m4254218564_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.UInt16>::Dispose()
#define Enumerator_Dispose_m2096142709(__this, method) ((  void (*) (Enumerator_t2162409685 *, const MethodInfo*))Enumerator_Dispose_m3377405731_gshared)(__this, method)
