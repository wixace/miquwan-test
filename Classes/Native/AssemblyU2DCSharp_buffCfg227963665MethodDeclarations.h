﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// buffCfg
struct buffCfg_t227963665;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void buffCfg::.ctor()
extern "C"  void buffCfg__ctor_m3288637306 (buffCfg_t227963665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void buffCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void buffCfg_Init_m966929867 (buffCfg_t227963665 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
