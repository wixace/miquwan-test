﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FingerClusterManager/<FindClusterById>c__AnonStorey128
struct U3CFindClusterByIdU3Ec__AnonStorey128_t1210815940;
// FingerClusterManager/Cluster
struct Cluster_t3870231303;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FingerClusterManager_Cluster3870231303.h"

// System.Void FingerClusterManager/<FindClusterById>c__AnonStorey128::.ctor()
extern "C"  void U3CFindClusterByIdU3Ec__AnonStorey128__ctor_m1489795927 (U3CFindClusterByIdU3Ec__AnonStorey128_t1210815940 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FingerClusterManager/<FindClusterById>c__AnonStorey128::<>m__362(FingerClusterManager/Cluster)
extern "C"  bool U3CFindClusterByIdU3Ec__AnonStorey128_U3CU3Em__362_m651818366 (U3CFindClusterByIdU3Ec__AnonStorey128_t1210815940 * __this, Cluster_t3870231303 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
