﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._a5b13b009eeb0667ba072545c2feaa6c
struct _a5b13b009eeb0667ba072545c2feaa6c_t3565678112;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._a5b13b009eeb0667ba072545c2feaa6c::.ctor()
extern "C"  void _a5b13b009eeb0667ba072545c2feaa6c__ctor_m3748994061 (_a5b13b009eeb0667ba072545c2feaa6c_t3565678112 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a5b13b009eeb0667ba072545c2feaa6c::_a5b13b009eeb0667ba072545c2feaa6cm2(System.Int32)
extern "C"  int32_t _a5b13b009eeb0667ba072545c2feaa6c__a5b13b009eeb0667ba072545c2feaa6cm2_m3210173849 (_a5b13b009eeb0667ba072545c2feaa6c_t3565678112 * __this, int32_t ____a5b13b009eeb0667ba072545c2feaa6ca0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a5b13b009eeb0667ba072545c2feaa6c::_a5b13b009eeb0667ba072545c2feaa6cm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _a5b13b009eeb0667ba072545c2feaa6c__a5b13b009eeb0667ba072545c2feaa6cm_m2996488509 (_a5b13b009eeb0667ba072545c2feaa6c_t3565678112 * __this, int32_t ____a5b13b009eeb0667ba072545c2feaa6ca0, int32_t ____a5b13b009eeb0667ba072545c2feaa6c431, int32_t ____a5b13b009eeb0667ba072545c2feaa6cc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
