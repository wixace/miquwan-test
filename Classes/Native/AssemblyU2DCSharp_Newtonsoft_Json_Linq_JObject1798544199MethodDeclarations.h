﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Object
struct Il2CppObject;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t950507765;
// System.String
struct String_t;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t901821544;
// System.Collections.Generic.ICollection`1<Newtonsoft.Json.Linq.JToken>
struct ICollection_1_t11868642;
// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>[]
struct KeyValuePair_2U5BU5D_t2467292914;
// System.ComponentModel.PropertyDescriptorCollection
struct PropertyDescriptorCollection_t3344846062;
// System.Attribute[]
struct AttributeU5BU5D_t4055800263;
// System.ComponentModel.AttributeCollection
struct AttributeCollection_t100867136;
// System.ComponentModel.TypeConverter
struct TypeConverter_t1753450284;
// System.ComponentModel.EventDescriptor
struct EventDescriptor_t1405012495;
// System.ComponentModel.PropertyDescriptor
struct PropertyDescriptor_t2073374448;
// System.Type
struct Type_t;
// System.ComponentModel.EventDescriptorCollection
struct EventDescriptorCollection_t3224620365;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken>
struct IList_1_t1811925858;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JProperty
struct JProperty_t2616415645;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JProperty>
struct IEnumerable_1_t1622361306;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// Newtonsoft.Json.JsonConverter[]
struct JsonConverterU5BU5D_t638349667;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>>
struct IEnumerator_1_t1748342780;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Linq.JToken>
struct IDictionary_2_t3810537666;
// Newtonsoft.Json.Linq.JObject/JPropertKeyedCollection
struct JPropertKeyedCollection_t2889692899;
// Newtonsoft.Json.Linq.JContainer
struct JContainer_t3364442311;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// Newtonsoft.Json.IJsonLineInfo
struct IJsonLineInfo_t1008519681;
// Newtonsoft.Json.Linq.JValue
struct JValue_t3413677367;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_System_ComponentModel_PropertyChangedEventHa950507765.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24131445027.h"
#include "mscorlib_System_Type2863145774.h"
#include "System_System_ComponentModel_PropertyDescriptor2073374448.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JProperty2616415645.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType3916897561.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JEnumerable_213262205.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer251850770.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject_JPr2889692899.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JContainer3364442311.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken4173078175.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JValue3413677367.h"

// System.Void Newtonsoft.Json.Linq.JObject::.ctor()
extern "C"  void JObject__ctor_m687346251 (JObject_t1798544199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::.ctor(Newtonsoft.Json.Linq.JObject)
extern "C"  void JObject__ctor_m1683974907 (JObject_t1798544199 * __this, JObject_t1798544199 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::.ctor(System.Object[])
extern "C"  void JObject__ctor_m2522030599 (JObject_t1798544199 * __this, ObjectU5BU5D_t1108656482* ___content0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::.ctor(System.Object)
extern "C"  void JObject__ctor_m3519938857 (JObject_t1798544199 * __this, Il2CppObject * ___content0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::.cctor()
extern "C"  void JObject__cctor_m3645768386 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::add_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern "C"  void JObject_add_PropertyChanged_m465613052 (JObject_t1798544199 * __this, PropertyChangedEventHandler_t950507765 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::remove_PropertyChanged(System.ComponentModel.PropertyChangedEventHandler)
extern "C"  void JObject_remove_PropertyChanged_m2949365321 (JObject_t1798544199 * __this, PropertyChangedEventHandler_t950507765 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JObject::System.Collections.Generic.IDictionary<string,Newtonsoft.Json.Linq.JToken>.ContainsKey(System.String)
extern "C"  bool JObject_System_Collections_Generic_IDictionaryU3CstringU2CNewtonsoft_Json_Linq_JTokenU3E_ContainsKey_m2989539326 (JObject_t1798544199 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<System.String> Newtonsoft.Json.Linq.JObject::System.Collections.Generic.IDictionary<string,Newtonsoft.Json.Linq.JToken>.get_Keys()
extern "C"  Il2CppObject* JObject_System_Collections_Generic_IDictionaryU3CstringU2CNewtonsoft_Json_Linq_JTokenU3E_get_Keys_m2240527165 (JObject_t1798544199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.ICollection`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject::System.Collections.Generic.IDictionary<string,Newtonsoft.Json.Linq.JToken>.get_Values()
extern "C"  Il2CppObject* JObject_System_Collections_Generic_IDictionaryU3CstringU2CNewtonsoft_Json_Linq_JTokenU3E_get_Values_m3766370259 (JObject_t1798544199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string,Newtonsoft.Json.Linq.JToken>>.Add(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>)
extern "C"  void JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Add_m45745293 (JObject_t1798544199 * __this, KeyValuePair_2_t4131445027  ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string,Newtonsoft.Json.Linq.JToken>>.Clear()
extern "C"  void JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Clear_m3676884154 (JObject_t1798544199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string,Newtonsoft.Json.Linq.JToken>>.Contains(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>)
extern "C"  bool JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Contains_m1879590503 (JObject_t1798544199 * __this, KeyValuePair_2_t4131445027  ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string,Newtonsoft.Json.Linq.JToken>>.CopyTo(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>[],System.Int32)
extern "C"  void JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_CopyTo_m3698654549 (JObject_t1798544199 * __this, KeyValuePair_2U5BU5D_t2467292914* ___array0, int32_t ___arrayIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string,Newtonsoft.Json.Linq.JToken>>.get_IsReadOnly()
extern "C"  bool JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_get_IsReadOnly_m1824767102 (JObject_t1798544199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JObject::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<string,Newtonsoft.Json.Linq.JToken>>.Remove(System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>)
extern "C"  bool JObject_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CstringU2CNewtonsoft_Json_Linq_JTokenU3EU3E_Remove_m3733559564 (JObject_t1798544199 * __this, KeyValuePair_2_t4131445027  ___item0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.PropertyDescriptorCollection Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetProperties()
extern "C"  PropertyDescriptorCollection_t3344846062 * JObject_System_ComponentModel_ICustomTypeDescriptor_GetProperties_m2683704492 (JObject_t1798544199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.PropertyDescriptorCollection Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetProperties(System.Attribute[])
extern "C"  PropertyDescriptorCollection_t3344846062 * JObject_System_ComponentModel_ICustomTypeDescriptor_GetProperties_m2912982029 (JObject_t1798544199 * __this, AttributeU5BU5D_t4055800263* ___attributes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.AttributeCollection Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetAttributes()
extern "C"  AttributeCollection_t100867136 * JObject_System_ComponentModel_ICustomTypeDescriptor_GetAttributes_m280751500 (JObject_t1798544199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetClassName()
extern "C"  String_t* JObject_System_ComponentModel_ICustomTypeDescriptor_GetClassName_m4070939255 (JObject_t1798544199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetComponentName()
extern "C"  String_t* JObject_System_ComponentModel_ICustomTypeDescriptor_GetComponentName_m2712294588 (JObject_t1798544199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.TypeConverter Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetConverter()
extern "C"  TypeConverter_t1753450284 * JObject_System_ComponentModel_ICustomTypeDescriptor_GetConverter_m2541962753 (JObject_t1798544199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.EventDescriptor Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetDefaultEvent()
extern "C"  EventDescriptor_t1405012495 * JObject_System_ComponentModel_ICustomTypeDescriptor_GetDefaultEvent_m2913021789 (JObject_t1798544199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.PropertyDescriptor Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetDefaultProperty()
extern "C"  PropertyDescriptor_t2073374448 * JObject_System_ComponentModel_ICustomTypeDescriptor_GetDefaultProperty_m2596435581 (JObject_t1798544199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetEditor(System.Type)
extern "C"  Il2CppObject * JObject_System_ComponentModel_ICustomTypeDescriptor_GetEditor_m1605489968 (JObject_t1798544199 * __this, Type_t * ___editorBaseType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.EventDescriptorCollection Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetEvents(System.Attribute[])
extern "C"  EventDescriptorCollection_t3224620365 * JObject_System_ComponentModel_ICustomTypeDescriptor_GetEvents_m312349180 (JObject_t1798544199 * __this, AttributeU5BU5D_t4055800263* ___attributes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.EventDescriptorCollection Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetEvents()
extern "C"  EventDescriptorCollection_t3224620365 * JObject_System_ComponentModel_ICustomTypeDescriptor_GetEvents_m3202318427 (JObject_t1798544199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JObject::System.ComponentModel.ICustomTypeDescriptor.GetPropertyOwner(System.ComponentModel.PropertyDescriptor)
extern "C"  Il2CppObject * JObject_System_ComponentModel_ICustomTypeDescriptor_GetPropertyOwner_m3687153949 (JObject_t1798544199 * __this, PropertyDescriptor_t2073374448 * ___pd0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject::get_ChildrenTokens()
extern "C"  Il2CppObject* JObject_get_ChildrenTokens_m2996943557 (JObject_t1798544199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JObject::DeepEquals(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JObject_DeepEquals_m2986927314 (JObject_t1798544199 * __this, JToken_t3412245951 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::InsertItem(System.Int32,Newtonsoft.Json.Linq.JToken)
extern "C"  void JObject_InsertItem_m1337264314 (JObject_t1798544199 * __this, int32_t ___index0, JToken_t3412245951 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::ValidateToken(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern "C"  void JObject_ValidateToken_m3538455138 (JObject_t1798544199 * __this, JToken_t3412245951 * ___o0, JToken_t3412245951 * ___existing1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::InternalPropertyChanged(Newtonsoft.Json.Linq.JProperty)
extern "C"  void JObject_InternalPropertyChanged_m1491818533 (JObject_t1798544199 * __this, JProperty_t2616415645 * ___childProperty0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::InternalPropertyChanging(Newtonsoft.Json.Linq.JProperty)
extern "C"  void JObject_InternalPropertyChanging_m3698502642 (JObject_t1798544199 * __this, JProperty_t2616415645 * ___childProperty0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JObject::CloneToken()
extern "C"  JToken_t3412245951 * JObject_CloneToken_m436076352 (JObject_t1798544199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JObject::get_Type()
extern "C"  int32_t JObject_get_Type_m430515853 (JObject_t1798544199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JProperty> Newtonsoft.Json.Linq.JObject::Properties()
extern "C"  Il2CppObject* JObject_Properties_m459862938 (JObject_t1798544199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JProperty Newtonsoft.Json.Linq.JObject::Property(System.String)
extern "C"  JProperty_t2616415645 * JObject_Property_m2115758301 (JObject_t1798544199 * __this, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject::PropertyValues()
extern "C"  JEnumerable_1_t213262205  JObject_PropertyValues_m1756234747 (JObject_t1798544199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JObject::get_Item(System.Object)
extern "C"  JToken_t3412245951 * JObject_get_Item_m3449151956 (JObject_t1798544199 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::set_Item(System.Object,Newtonsoft.Json.Linq.JToken)
extern "C"  void JObject_set_Item_m2517345583 (JObject_t1798544199 * __this, Il2CppObject * ___key0, JToken_t3412245951 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JObject::get_Item(System.String)
extern "C"  JToken_t3412245951 * JObject_get_Item_m3227041602 (JObject_t1798544199 * __this, String_t* ___propertyName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::set_Item(System.String,Newtonsoft.Json.Linq.JToken)
extern "C"  void JObject_set_Item_m1785016733 (JObject_t1798544199 * __this, String_t* ___propertyName0, JToken_t3412245951 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject::Load(Newtonsoft.Json.JsonReader)
extern "C"  JObject_t1798544199 * JObject_Load_m2664125836 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject::Parse(System.String)
extern "C"  JObject_t1798544199 * JObject_Parse_m1662485971 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject::FromObject(System.Object)
extern "C"  JObject_t1798544199 * JObject_FromObject_m901483877 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject::FromObject(System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  JObject_t1798544199 * JObject_FromObject_m3284766258 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, JsonSerializer_t251850770 * ___jsonSerializer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])
extern "C"  void JObject_WriteTo_m4286066778 (JObject_t1798544199 * __this, JsonWriter_t972330355 * ___writer0, JsonConverterU5BU5D_t638349667* ___converters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::Add(System.String,Newtonsoft.Json.Linq.JToken)
extern "C"  void JObject_Add_m2775484252 (JObject_t1798544199 * __this, String_t* ___propertyName0, JToken_t3412245951 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JObject::ContainsKey(System.String)
extern "C"  bool JObject_ContainsKey_m2712775469 (JObject_t1798544199 * __this, String_t* ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JObject::Remove(System.String)
extern "C"  bool JObject_Remove_m1691020241 (JObject_t1798544199 * __this, String_t* ___propertyName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JObject::TryGetValue(System.String,Newtonsoft.Json.Linq.JToken&)
extern "C"  bool JObject_TryGetValue_m616612913 (JObject_t1798544199 * __this, String_t* ___propertyName0, JToken_t3412245951 ** ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JObject::GetDeepHashCode()
extern "C"  int32_t JObject_GetDeepHashCode_m3644100088 (JObject_t1798544199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>> Newtonsoft.Json.Linq.JObject::GetEnumerator()
extern "C"  Il2CppObject* JObject_GetEnumerator_m768219894 (JObject_t1798544199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::OnPropertyChanged(System.String)
extern "C"  void JObject_OnPropertyChanged_m1844054777 (JObject_t1798544199 * __this, String_t* ___propertyName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Linq.JObject::GetTokenPropertyType(Newtonsoft.Json.Linq.JToken)
extern "C"  Type_t * JObject_GetTokenPropertyType_m3176537413 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JObject::<PropertyValues>m__375(Newtonsoft.Json.Linq.JProperty)
extern "C"  JToken_t3412245951 * JObject_U3CPropertyValuesU3Em__375_m698816429 (Il2CppObject * __this /* static, unused */, JProperty_t2616415645 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IDictionary`2<System.String,Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject::ilo_get_Dictionary1(Newtonsoft.Json.Linq.JObject/JPropertKeyedCollection)
extern "C"  Il2CppObject* JObject_ilo_get_Dictionary1_m2835816382 (Il2CppObject * __this /* static, unused */, JPropertKeyedCollection_t2889692899 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::ilo_RemoveAll2(Newtonsoft.Json.Linq.JContainer)
extern "C"  void JObject_ilo_RemoveAll2_m177835553 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JProperty Newtonsoft.Json.Linq.JObject::ilo_Property3(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JProperty_t2616415645 * JObject_ilo_Property3_m1658515017 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JObject::ilo_get_Value4(Newtonsoft.Json.Linq.JProperty)
extern "C"  JToken_t3412245951 * JObject_ilo_get_Value4_m342634845 (Il2CppObject * __this /* static, unused */, JProperty_t2616415645 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Linq.JObject::ilo_GetTokenPropertyType5(Newtonsoft.Json.Linq.JToken)
extern "C"  Type_t * JObject_ilo_GetTokenPropertyType5_m3053680043 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JObject::ilo_ContentsEqual6(Newtonsoft.Json.Linq.JContainer,Newtonsoft.Json.Linq.JContainer)
extern "C"  bool JObject_ilo_ContentsEqual6_m4089066690 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, JContainer_t3364442311 * ___container1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JObject::ilo_get_Type7(Newtonsoft.Json.Linq.JToken)
extern "C"  int32_t JObject_ilo_get_Type7_m2227059377 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JObject::ilo_get_Name8(Newtonsoft.Json.Linq.JProperty)
extern "C"  String_t* JObject_ilo_get_Name8_m593027473 (Il2CppObject * __this /* static, unused */, JProperty_t2616415645 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JObject::ilo_FormatWith9(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* JObject_ilo_FormatWith9_m3745546669 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::ilo_set_Item10(Newtonsoft.Json.Linq.JObject,System.String,Newtonsoft.Json.Linq.JToken)
extern "C"  void JObject_ilo_set_Item10_m1797497739 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, JToken_t3412245951 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::ilo_Add11(Newtonsoft.Json.Linq.JContainer,System.Object)
extern "C"  void JObject_ilo_Add11_m4152441913 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, Il2CppObject * ___content1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::ilo_ArgumentNotNull12(System.Object,System.String)
extern "C"  void JObject_ilo_ArgumentNotNull12_m2813177628 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, String_t* ___parameterName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonToken Newtonsoft.Json.Linq.JObject::ilo_get_TokenType13(Newtonsoft.Json.JsonReader)
extern "C"  int32_t JObject_ilo_get_TokenType13_m3532005515 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::ilo_SetLineInfo14(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.IJsonLineInfo)
extern "C"  void JObject_ilo_SetLineInfo14_m3664106267 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, Il2CppObject * ___lineInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JObject::ilo_WriteEndObject15(Newtonsoft.Json.JsonWriter)
extern "C"  void JObject_ilo_WriteEndObject15_m4152850491 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JObject::ilo_get_Value16(Newtonsoft.Json.Linq.JValue)
extern "C"  Il2CppObject * JObject_ilo_get_Value16_m2984083460 (Il2CppObject * __this /* static, unused */, JValue_t3413677367 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
