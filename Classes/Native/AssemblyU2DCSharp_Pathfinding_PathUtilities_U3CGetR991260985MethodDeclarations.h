﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.PathUtilities/<GetReachableNodes>c__AnonStorey125
struct U3CGetReachableNodesU3Ec__AnonStorey125_t991260985;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.PathUtilities/<GetReachableNodes>c__AnonStorey125::.ctor()
extern "C"  void U3CGetReachableNodesU3Ec__AnonStorey125__ctor_m1462882386 (U3CGetReachableNodesU3Ec__AnonStorey125_t991260985 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PathUtilities/<GetReachableNodes>c__AnonStorey125::<>m__35B(Pathfinding.GraphNode)
extern "C"  void U3CGetReachableNodesU3Ec__AnonStorey125_U3CU3Em__35B_m3584676997 (U3CGetReachableNodesU3Ec__AnonStorey125_t991260985 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.PathUtilities/<GetReachableNodes>c__AnonStorey125::<>m__35C(Pathfinding.GraphNode)
extern "C"  void U3CGetReachableNodesU3Ec__AnonStorey125_U3CU3Em__35C_m3517670244 (U3CGetReachableNodesU3Ec__AnonStorey125_t991260985 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
