﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=76
struct __StaticArrayInitTypeSizeU3D76_t894410371;
struct __StaticArrayInitTypeSizeU3D76_t894410371_marshaled_pinvoke;
struct __StaticArrayInitTypeSizeU3D76_t894410371_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct __StaticArrayInitTypeSizeU3D76_t894410371;
struct __StaticArrayInitTypeSizeU3D76_t894410371_marshaled_pinvoke;

extern "C" void __StaticArrayInitTypeSizeU3D76_t894410371_marshal_pinvoke(const __StaticArrayInitTypeSizeU3D76_t894410371& unmarshaled, __StaticArrayInitTypeSizeU3D76_t894410371_marshaled_pinvoke& marshaled);
extern "C" void __StaticArrayInitTypeSizeU3D76_t894410371_marshal_pinvoke_back(const __StaticArrayInitTypeSizeU3D76_t894410371_marshaled_pinvoke& marshaled, __StaticArrayInitTypeSizeU3D76_t894410371& unmarshaled);
extern "C" void __StaticArrayInitTypeSizeU3D76_t894410371_marshal_pinvoke_cleanup(__StaticArrayInitTypeSizeU3D76_t894410371_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct __StaticArrayInitTypeSizeU3D76_t894410371;
struct __StaticArrayInitTypeSizeU3D76_t894410371_marshaled_com;

extern "C" void __StaticArrayInitTypeSizeU3D76_t894410371_marshal_com(const __StaticArrayInitTypeSizeU3D76_t894410371& unmarshaled, __StaticArrayInitTypeSizeU3D76_t894410371_marshaled_com& marshaled);
extern "C" void __StaticArrayInitTypeSizeU3D76_t894410371_marshal_com_back(const __StaticArrayInitTypeSizeU3D76_t894410371_marshaled_com& marshaled, __StaticArrayInitTypeSizeU3D76_t894410371& unmarshaled);
extern "C" void __StaticArrayInitTypeSizeU3D76_t894410371_marshal_com_cleanup(__StaticArrayInitTypeSizeU3D76_t894410371_marshaled_com& marshaled);
