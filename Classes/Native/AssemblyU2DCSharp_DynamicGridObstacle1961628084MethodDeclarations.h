﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DynamicGridObstacle
struct DynamicGridObstacle_t1961628084;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"

// System.Void DynamicGridObstacle::.ctor()
extern "C"  void DynamicGridObstacle__ctor_m2597891127 (DynamicGridObstacle_t1961628084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicGridObstacle::Start()
extern "C"  void DynamicGridObstacle_Start_m1545028919 (DynamicGridObstacle_t1961628084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DynamicGridObstacle::UpdateGraphs()
extern "C"  Il2CppObject * DynamicGridObstacle_UpdateGraphs_m2368362099 (DynamicGridObstacle_t1961628084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicGridObstacle::OnDestroy()
extern "C"  void DynamicGridObstacle_OnDestroy_m2038140336 (DynamicGridObstacle_t1961628084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicGridObstacle::DoUpdateGraphs()
extern "C"  void DynamicGridObstacle_DoUpdateGraphs_m4009601062 (DynamicGridObstacle_t1961628084 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DynamicGridObstacle::BoundsVolume(UnityEngine.Bounds)
extern "C"  float DynamicGridObstacle_BoundsVolume_m4075364196 (Il2CppObject * __this /* static, unused */, Bounds_t2711641849  ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DynamicGridObstacle::ilo_BoundsVolume1(UnityEngine.Bounds)
extern "C"  float DynamicGridObstacle_ilo_BoundsVolume1_m1548425220 (Il2CppObject * __this /* static, unused */, Bounds_t2711641849  ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
