﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr_DGetV_1_gen4048467712MethodDeclarations.h"

// System.Void JSDataExchangeMgr/DGetV`1<UnityEngine.Color32[]>::.ctor(System.Object,System.IntPtr)
#define DGetV_1__ctor_m3820468076(__this, ___object0, ___method1, method) ((  void (*) (DGetV_1_t2838418294 *, Il2CppObject *, IntPtr_t, const MethodInfo*))DGetV_1__ctor_m3356914363_gshared)(__this, ___object0, ___method1, method)
// T JSDataExchangeMgr/DGetV`1<UnityEngine.Color32[]>::Invoke()
#define DGetV_1_Invoke_m183947653(__this, method) ((  Color32U5BU5D_t2960766953* (*) (DGetV_1_t2838418294 *, const MethodInfo*))DGetV_1_Invoke_m1509146772_gshared)(__this, method)
// System.IAsyncResult JSDataExchangeMgr/DGetV`1<UnityEngine.Color32[]>::BeginInvoke(System.AsyncCallback,System.Object)
#define DGetV_1_BeginInvoke_m2705753981(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (DGetV_1_t2838418294 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))DGetV_1_BeginInvoke_m2681184398_gshared)(__this, ___callback0, ___object1, method)
// T JSDataExchangeMgr/DGetV`1<UnityEngine.Color32[]>::EndInvoke(System.IAsyncResult)
#define DGetV_1_EndInvoke_m418157819(__this, ___result0, method) ((  Color32U5BU5D_t2960766953* (*) (DGetV_1_t2838418294 *, Il2CppObject *, const MethodInfo*))DGetV_1_EndInvoke_m1776565962_gshared)(__this, ___result0, method)
