﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.CollectionUtils/<TryGetSingleItem>c__AnonStorey136`1<System.Object>
struct U3CTryGetSingleItemU3Ec__AnonStorey136_1_t2444324479;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Utilities.CollectionUtils/<TryGetSingleItem>c__AnonStorey136`1<System.Object>::.ctor()
extern "C"  void U3CTryGetSingleItemU3Ec__AnonStorey136_1__ctor_m453258606_gshared (U3CTryGetSingleItemU3Ec__AnonStorey136_1_t2444324479 * __this, const MethodInfo* method);
#define U3CTryGetSingleItemU3Ec__AnonStorey136_1__ctor_m453258606(__this, method) ((  void (*) (U3CTryGetSingleItemU3Ec__AnonStorey136_1_t2444324479 *, const MethodInfo*))U3CTryGetSingleItemU3Ec__AnonStorey136_1__ctor_m453258606_gshared)(__this, method)
// T Newtonsoft.Json.Utilities.CollectionUtils/<TryGetSingleItem>c__AnonStorey136`1<System.Object>::<>m__392()
extern "C"  Il2CppObject * U3CTryGetSingleItemU3Ec__AnonStorey136_1_U3CU3Em__392_m3817006840_gshared (U3CTryGetSingleItemU3Ec__AnonStorey136_1_t2444324479 * __this, const MethodInfo* method);
#define U3CTryGetSingleItemU3Ec__AnonStorey136_1_U3CU3Em__392_m3817006840(__this, method) ((  Il2CppObject * (*) (U3CTryGetSingleItemU3Ec__AnonStorey136_1_t2444324479 *, const MethodInfo*))U3CTryGetSingleItemU3Ec__AnonStorey136_1_U3CU3Em__392_m3817006840_gshared)(__this, method)
