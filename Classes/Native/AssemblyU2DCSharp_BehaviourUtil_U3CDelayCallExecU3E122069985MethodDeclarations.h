﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Object,System.Int32,System.Int32>
struct U3CDelayCallExecU3Ec__Iterator41_3_t122069985;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Object,System.Int32,System.Int32>::.ctor()
extern "C"  void U3CDelayCallExecU3Ec__Iterator41_3__ctor_m3029900280_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t122069985 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3__ctor_m3029900280(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator41_3_t122069985 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3__ctor_m3029900280_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Object,System.Int32,System.Int32>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3476046874_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t122069985 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3476046874(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator41_3_t122069985 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3476046874_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Object,System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_IEnumerator_get_Current_m2173811630_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t122069985 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_IEnumerator_get_Current_m2173811630(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator41_3_t122069985 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_IEnumerator_get_Current_m2173811630_gshared)(__this, method)
// System.Boolean BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Object,System.Int32,System.Int32>::MoveNext()
extern "C"  bool U3CDelayCallExecU3Ec__Iterator41_3_MoveNext_m3031715324_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t122069985 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_MoveNext_m3031715324(__this, method) ((  bool (*) (U3CDelayCallExecU3Ec__Iterator41_3_t122069985 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_MoveNext_m3031715324_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Object,System.Int32,System.Int32>::Dispose()
extern "C"  void U3CDelayCallExecU3Ec__Iterator41_3_Dispose_m3939592757_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t122069985 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_Dispose_m3939592757(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator41_3_t122069985 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_Dispose_m3939592757_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Object,System.Int32,System.Int32>::Reset()
extern "C"  void U3CDelayCallExecU3Ec__Iterator41_3_Reset_m676333221_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t122069985 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_Reset_m676333221(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator41_3_t122069985 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_Reset_m676333221_gshared)(__this, method)
