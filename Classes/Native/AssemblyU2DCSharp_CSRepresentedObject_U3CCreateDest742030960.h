﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSRepresentedObject/<CreateDestructAction>c__AnonStoreyFB
struct  U3CCreateDestructActionU3Ec__AnonStoreyFB_t742030960  : public Il2CppObject
{
public:
	// System.Boolean CSRepresentedObject/<CreateDestructAction>c__AnonStoreyFB::bFunction
	bool ___bFunction_0;
	// System.Int32 CSRepresentedObject/<CreateDestructAction>c__AnonStoreyFB::jsObjID
	int32_t ___jsObjID_1;
	// System.Int32 CSRepresentedObject/<CreateDestructAction>c__AnonStoreyFB::round
	int32_t ___round_2;

public:
	inline static int32_t get_offset_of_bFunction_0() { return static_cast<int32_t>(offsetof(U3CCreateDestructActionU3Ec__AnonStoreyFB_t742030960, ___bFunction_0)); }
	inline bool get_bFunction_0() const { return ___bFunction_0; }
	inline bool* get_address_of_bFunction_0() { return &___bFunction_0; }
	inline void set_bFunction_0(bool value)
	{
		___bFunction_0 = value;
	}

	inline static int32_t get_offset_of_jsObjID_1() { return static_cast<int32_t>(offsetof(U3CCreateDestructActionU3Ec__AnonStoreyFB_t742030960, ___jsObjID_1)); }
	inline int32_t get_jsObjID_1() const { return ___jsObjID_1; }
	inline int32_t* get_address_of_jsObjID_1() { return &___jsObjID_1; }
	inline void set_jsObjID_1(int32_t value)
	{
		___jsObjID_1 = value;
	}

	inline static int32_t get_offset_of_round_2() { return static_cast<int32_t>(offsetof(U3CCreateDestructActionU3Ec__AnonStoreyFB_t742030960, ___round_2)); }
	inline int32_t get_round_2() const { return ___round_2; }
	inline int32_t* get_address_of_round_2() { return &___round_2; }
	inline void set_round_2(int32_t value)
	{
		___round_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
