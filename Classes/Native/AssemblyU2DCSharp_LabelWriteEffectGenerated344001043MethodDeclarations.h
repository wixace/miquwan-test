﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LabelWriteEffectGenerated
struct LabelWriteEffectGenerated_t344001043;
// JSVCall
struct JSVCall_t3708497963;
// System.Action
struct Action_t3771233898;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// LabelWriteEffect
struct LabelWriteEffect_t1910744796;
// System.Delegate
struct Delegate_t3310234105;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_LabelWriteEffect1910744796.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void LabelWriteEffectGenerated::.ctor()
extern "C"  void LabelWriteEffectGenerated__ctor_m2297553080 (LabelWriteEffectGenerated_t344001043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LabelWriteEffectGenerated::LabelWriteEffect_LabelWriteEffect1(JSVCall,System.Int32)
extern "C"  bool LabelWriteEffectGenerated_LabelWriteEffect_LabelWriteEffect1_m2024492602 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LabelWriteEffectGenerated::LabelWriteEffect_isPlaying(JSVCall)
extern "C"  void LabelWriteEffectGenerated_LabelWriteEffect_isPlaying_m1258283034 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LabelWriteEffectGenerated::LabelWriteEffect_TimeLen(JSVCall)
extern "C"  void LabelWriteEffectGenerated_LabelWriteEffect_TimeLen_m3853693526 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action LabelWriteEffectGenerated::LabelWriteEffect_AddFunctionList_GetDelegate_member0_arg0(CSRepresentedObject)
extern "C"  Action_t3771233898 * LabelWriteEffectGenerated_LabelWriteEffect_AddFunctionList_GetDelegate_member0_arg0_m3225510070 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LabelWriteEffectGenerated::LabelWriteEffect_AddFunctionList__Action(JSVCall,System.Int32)
extern "C"  bool LabelWriteEffectGenerated_LabelWriteEffect_AddFunctionList__Action_m1113589586 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LabelWriteEffectGenerated::LabelWriteEffect_ClearFunction(JSVCall,System.Int32)
extern "C"  bool LabelWriteEffectGenerated_LabelWriteEffect_ClearFunction_m2861123722 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LabelWriteEffectGenerated::LabelWriteEffect_Play__String__Single(JSVCall,System.Int32)
extern "C"  bool LabelWriteEffectGenerated_LabelWriteEffect_Play__String__Single_m4282334570 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LabelWriteEffectGenerated::LabelWriteEffect_Stop(JSVCall,System.Int32)
extern "C"  bool LabelWriteEffectGenerated_LabelWriteEffect_Stop_m1247551775 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LabelWriteEffectGenerated::__Register()
extern "C"  void LabelWriteEffectGenerated___Register_m1121406735 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action LabelWriteEffectGenerated::<LabelWriteEffect_AddFunctionList__Action>m__6A()
extern "C"  Action_t3771233898 * LabelWriteEffectGenerated_U3CLabelWriteEffect_AddFunctionList__ActionU3Em__6A_m1445103796 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LabelWriteEffectGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t LabelWriteEffectGenerated_ilo_getObject1_m2862403070 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LabelWriteEffectGenerated::ilo_setBooleanS2(System.Int32,System.Boolean)
extern "C"  void LabelWriteEffectGenerated_ilo_setBooleanS2_m3996624072 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single LabelWriteEffectGenerated::ilo_get_TimeLen3(LabelWriteEffect)
extern "C"  float LabelWriteEffectGenerated_ilo_get_TimeLen3_m959958091 (Il2CppObject * __this /* static, unused */, LabelWriteEffect_t1910744796 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LabelWriteEffectGenerated::ilo_addJSFunCSDelegateRel4(System.Int32,System.Delegate)
extern "C"  void LabelWriteEffectGenerated_ilo_addJSFunCSDelegateRel4_m4022979037 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LabelWriteEffectGenerated::ilo_AddFunctionList5(LabelWriteEffect,System.Action)
extern "C"  void LabelWriteEffectGenerated_ilo_AddFunctionList5_m1309702232 (Il2CppObject * __this /* static, unused */, LabelWriteEffect_t1910744796 * ____this0, Action_t3771233898 * ___at1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LabelWriteEffectGenerated::ilo_getStringS6(System.Int32)
extern "C"  String_t* LabelWriteEffectGenerated_ilo_getStringS6_m761489505 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single LabelWriteEffectGenerated::ilo_getSingle7(System.Int32)
extern "C"  float LabelWriteEffectGenerated_ilo_getSingle7_m1859673821 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LabelWriteEffectGenerated::ilo_getObject8(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * LabelWriteEffectGenerated_ilo_getObject8_m1233624180 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
