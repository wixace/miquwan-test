﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Util
struct Util_t2647074;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;
// System.Char[]
struct CharU5BU5D_t3324145743;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.UInt32[]
struct UInt32U5BU5D_t3230734560;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.String>>
struct List_1_t2743602661;
// System.Collections.Generic.List`1<System.String>
struct List_1_t1375417109;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// System.Action
struct Action_t3771233898;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Util::.ctor()
extern "C"  void Util__ctor_m3427314873 (Util_t2647074 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Util::.cctor()
extern "C"  void Util__cctor_m2685449748 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Util::SplitStr(System.String,System.Char[])
extern "C"  StringU5BU5D_t4054002952* Util_SplitStr_m3237272564 (Il2CppObject * __this /* static, unused */, String_t* ___sFile0, CharU5BU5D_t3324145743* ___separator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] Util::SplitStr_commn(System.String)
extern "C"  StringU5BU5D_t4054002952* Util_SplitStr_commn_m3359612 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] Util::GetArticle_Int(System.String)
extern "C"  Int32U5BU5D_t3230847821* Util_GetArticle_Int_m3584564539 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32[] Util::GetArticle_Uint(System.String)
extern "C"  UInt32U5BU5D_t3230734560* Util_GetArticle_Uint_m688002315 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] Util::GetVector3_Arr(System.String)
extern "C"  Vector3U5BU5D_t215400611* Util_GetVector3_Arr_m2205256707 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Util::GetVector3(System.String)
extern "C"  Vector3_t4282066566  Util_GetVector3_m735234019 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] Util::GetArticle_Float(System.String)
extern "C"  SingleU5BU5D_t2316563989* Util_GetArticle_Float_m646553712 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Util::getBytesNum(System.String)
extern "C"  int32_t Util_getBytesNum_m2507198536 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Util::FormatTimeYYMMDD(System.UInt32)
extern "C"  String_t* Util_FormatTimeYYMMDD_m2928786952 (Il2CppObject * __this /* static, unused */, uint32_t ___timer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Util::FormatTimeMMDD(System.UInt32)
extern "C"  String_t* Util_FormatTimeMMDD_m1635810536 (Il2CppObject * __this /* static, unused */, uint32_t ___timer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Util::FormatDateTime(System.UInt32)
extern "C"  DateTime_t4283661327  Util_FormatDateTime_m189748100 (Il2CppObject * __this /* static, unused */, uint32_t ___timer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan Util::FormatTimeSpan(System.UInt32)
extern "C"  TimeSpan_t413522987  Util_FormatTimeSpan_m2967648772 (Il2CppObject * __this /* static, unused */, uint32_t ___timpStamp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Util::GetCurProtectTime(System.Int32)
extern "C"  int32_t Util_GetCurProtectTime_m4005178284 (Il2CppObject * __this /* static, unused */, int32_t ___overTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Util::FormatDateTime(System.Double)
extern "C"  DateTime_t4283661327  Util_FormatDateTime_m3375947212 (Il2CppObject * __this /* static, unused */, double ___timer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Util::FormatDateTime_world(System.Double)
extern "C"  DateTime_t4283661327  Util_FormatDateTime_world_m3411585689 (Il2CppObject * __this /* static, unused */, double ___timer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Util::GetYYMMDD(System.UInt32)
extern "C"  String_t* Util_GetYYMMDD_m3396935568 (Il2CppObject * __this /* static, unused */, uint32_t ___timer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Util::GetHHMMSS(System.Double)
extern "C"  String_t* Util_GetHHMMSS_m1106977304 (Il2CppObject * __this /* static, unused */, double ___timer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Util::FormatIntToString_KB(System.Single)
extern "C"  String_t* Util_FormatIntToString_KB_m1982495915 (Il2CppObject * __this /* static, unused */, float ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Util::FormatUIString(System.Int32,System.Object[])
extern "C"  String_t* Util_FormatUIString_m4219328327 (Il2CppObject * __this /* static, unused */, int32_t ___key0, ObjectU5BU5D_t1108656482* ___datas1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Util::FormatUIString(System.String,System.Object[])
extern "C"  String_t* Util_FormatUIString_m3882177828 (Il2CppObject * __this /* static, unused */, String_t* ___str0, ObjectU5BU5D_t1108656482* ___datas1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Util::FormatVector3ByStr(System.String,System.Char)
extern "C"  Vector3_t4282066566  Util_FormatVector3ByStr_m235841075 (Il2CppObject * __this /* static, unused */, String_t* ___str0, Il2CppChar ___c1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color Util::FormatColorByStr(System.String,System.Char)
extern "C"  Color_t4194546905  Util_FormatColorByStr_m2576865369 (Il2CppObject * __this /* static, unused */, String_t* ___str0, Il2CppChar ___c1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Util::GetAttributeName(System.String)
extern "C"  String_t* Util_GetAttributeName_m3655860867 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Util::GetPhysicsPos(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Util_GetPhysicsPos_m453058425 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Util::GetYPos(UnityEngine.Vector3)
extern "C"  float Util_GetYPos_m248131837 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Util::ChangeShader(UnityEngine.GameObject,UnityEngine.Color,System.String,System.String)
extern "C"  void Util_ChangeShader_m3208696112 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___obj0, Color_t4194546905  ___color1, String_t* ___newShaderName2, String_t* ___oldshaderName3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Util::PanelUp()
extern "C"  void Util_PanelUp_m1869603926 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Util::PanelDown()
extern "C"  void Util_PanelDown_m907845149 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Util::ToBase64String(System.Byte[])
extern "C"  String_t* Util_ToBase64String_m3334615552 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___datas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Util::FromBase64String(System.String)
extern "C"  ByteU5BU5D_t4260760469* Util_FromBase64String_m3834403203 (Il2CppObject * __this /* static, unused */, String_t* ___datas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Util::ZipCompress(System.String)
extern "C"  ByteU5BU5D_t4260760469* Util_ZipCompress_m421088754 (Il2CppObject * __this /* static, unused */, String_t* ___jsonDatas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Util::ZipDecompress(System.Byte[])
extern "C"  String_t* Util_ZipDecompress_m3644719441 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Util::AntiCheating()
extern "C"  uint32_t Util_AntiCheating_m1607731063 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Util::MobileVebrate()
extern "C"  void Util_MobileVebrate_m1965171592 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Util::Begin_CS_Sample(System.String)
extern "C"  void Util_Begin_CS_Sample_m1224099656 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Util::End_CS_Sample()
extern "C"  void Util_End_CS_Sample_m4205318316 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Util::MD5Encrypt(System.String)
extern "C"  String_t* Util_MD5Encrypt_m1513177773 (Il2CppObject * __this /* static, unused */, String_t* ___strText0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Util::EncodingUTF8(System.String)
extern "C"  String_t* Util_EncodingUTF8_m2774546384 (Il2CppObject * __this /* static, unused */, String_t* ___strcode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] Util::GetBeizerList(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Int32)
extern "C"  Vector3U5BU5D_t215400611* Util_GetBeizerList_m3280250124 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___startPoint0, Vector3_t4282066566  ___controlPoint1, Vector3_t4282066566  ___endPoint2, int32_t ___segmentNum3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Util::CalculateCubicBezierPoint(System.Single,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Util_CalculateCubicBezierPoint_m3353000826 (Il2CppObject * __this /* static, unused */, float ___t0, Vector3_t4282066566  ___p01, Vector3_t4282066566  ___p12, Vector3_t4282066566  ___p23, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Util::GetOnlyId()
extern "C"  String_t* Util_GetOnlyId_m4144395441 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Util::RegexIsMatch(System.String,System.String)
extern "C"  bool Util_RegexIsMatch_m523754571 (Il2CppObject * __this /* static, unused */, String_t* ___regex0, String_t* ___info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Util::GetCrc32(System.String)
extern "C"  String_t* Util_GetCrc32_m2060144151 (Il2CppObject * __this /* static, unused */, String_t* ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Util::GetRandomString(System.Int32,System.Int32,System.Int32)
extern "C"  String_t* Util_GetRandomString_m2252167279 (Il2CppObject * __this /* static, unused */, int32_t ___startAscii0, int32_t ___endAscii1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Util::Replace(System.String,System.String,System.String)
extern "C"  String_t* Util_Replace_m786993906 (Il2CppObject * __this /* static, unused */, String_t* ___input0, String_t* ___pattern1, String_t* ___replacement2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Util::ToJsoin(System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.String>>)
extern "C"  String_t* Util_ToJsoin_m1343759610 (Il2CppObject * __this /* static, unused */, List_1_t2743602661 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.String> Util::GetList()
extern "C"  List_1_t1375417109 * Util_GetList_m248142426 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<System.String>> Util::GetListArr()
extern "C"  List_1_t2743602661 * Util_GetListArr_m1290991799 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture2D Util::ReadTexture2D(System.String)
extern "C"  Texture2D_t3884108195 * Util_ReadTexture2D_m3853631473 (Il2CppObject * __this /* static, unused */, String_t* ___imgPath0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Util::<PanelUp>m__3FB()
extern "C"  void Util_U3CPanelUpU3Em__3FB_m1105101748 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Util::<PanelDown>m__3FC()
extern "C"  void Util_U3CPanelDownU3Em__3FC_m1410381262 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] Util::ilo_GetArticle_Float1(System.String)
extern "C"  SingleU5BU5D_t2316563989* Util_ilo_GetArticle_Float1_m344240276 (Il2CppObject * __this /* static, unused */, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Util::ilo_FormatDateTime2(System.UInt32)
extern "C"  DateTime_t4283661327  Util_ilo_FormatDateTime2_m2472879119 (Il2CppObject * __this /* static, unused */, uint32_t ___timer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Util::ilo_FormatUIString3(System.String,System.Object[])
extern "C"  String_t* Util_ilo_FormatUIString3_m145012150 (Il2CppObject * __this /* static, unused */, String_t* ___str0, ObjectU5BU5D_t1108656482* ___datas1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Util::ilo_FormatUIString4(System.Int32,System.Object[])
extern "C"  String_t* Util_ilo_FormatUIString4_m1817175222 (Il2CppObject * __this /* static, unused */, int32_t ___key0, ObjectU5BU5D_t1108656482* ___datas1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Util::ilo_GetYPos5(UnityEngine.Vector3)
extern "C"  float Util_ilo_GetYPos5_m1615239851 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Util::ilo_DelayCall6(System.Single,System.Action)
extern "C"  uint32_t Util_ilo_DelayCall6_m2096420692 (Il2CppObject * __this /* static, unused */, float ___delaytime0, Action_t3771233898 * ___act1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Util::ilo_Add7(Newtonsoft.Json.Linq.JObject,System.String,Newtonsoft.Json.Linq.JToken)
extern "C"  void Util_ilo_Add7_m3288669924 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, JToken_t3412245951 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Util::ilo_ToString8(Newtonsoft.Json.Linq.JToken)
extern "C"  String_t* Util_ilo_ToString8_m726287421 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Util::ilo_Log9(System.Object,System.Boolean)
extern "C"  void Util_ilo_Log9_m898409852 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Util::ilo_LogError10(System.Object,System.Boolean)
extern "C"  void Util_ilo_LogError10_m1009475594 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
