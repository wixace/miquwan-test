﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_chemeWhehibea102
struct  M_chemeWhehibea102_t4136819874  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_chemeWhehibea102::_sorsi
	float ____sorsi_0;
	// System.Single GarbageiOS.M_chemeWhehibea102::_jermir
	float ____jermir_1;
	// System.Single GarbageiOS.M_chemeWhehibea102::_mejowwai
	float ____mejowwai_2;
	// System.Single GarbageiOS.M_chemeWhehibea102::_daitu
	float ____daitu_3;
	// System.Int32 GarbageiOS.M_chemeWhehibea102::_hasere
	int32_t ____hasere_4;
	// System.Int32 GarbageiOS.M_chemeWhehibea102::_jurteLawhelay
	int32_t ____jurteLawhelay_5;
	// System.String GarbageiOS.M_chemeWhehibea102::_paynastaDarporkear
	String_t* ____paynastaDarporkear_6;
	// System.String GarbageiOS.M_chemeWhehibea102::_cheresearhas
	String_t* ____cheresearhas_7;
	// System.String GarbageiOS.M_chemeWhehibea102::_nesayweMoureaste
	String_t* ____nesayweMoureaste_8;
	// System.Single GarbageiOS.M_chemeWhehibea102::_pawpairPairgis
	float ____pawpairPairgis_9;
	// System.Boolean GarbageiOS.M_chemeWhehibea102::_supeatar
	bool ____supeatar_10;
	// System.UInt32 GarbageiOS.M_chemeWhehibea102::_jairkair
	uint32_t ____jairkair_11;
	// System.Single GarbageiOS.M_chemeWhehibea102::_cafeema
	float ____cafeema_12;
	// System.UInt32 GarbageiOS.M_chemeWhehibea102::_pemdrear
	uint32_t ____pemdrear_13;
	// System.Boolean GarbageiOS.M_chemeWhehibea102::_chispu
	bool ____chispu_14;
	// System.Int32 GarbageiOS.M_chemeWhehibea102::_hodrowneCisdor
	int32_t ____hodrowneCisdor_15;
	// System.String GarbageiOS.M_chemeWhehibea102::_berepevaWalda
	String_t* ____berepevaWalda_16;
	// System.Int32 GarbageiOS.M_chemeWhehibea102::_boudrearawJeawairjis
	int32_t ____boudrearawJeawairjis_17;
	// System.UInt32 GarbageiOS.M_chemeWhehibea102::_najecher
	uint32_t ____najecher_18;
	// System.Boolean GarbageiOS.M_chemeWhehibea102::_drelti
	bool ____drelti_19;
	// System.UInt32 GarbageiOS.M_chemeWhehibea102::_tamatall
	uint32_t ____tamatall_20;
	// System.Int32 GarbageiOS.M_chemeWhehibea102::_suse
	int32_t ____suse_21;
	// System.String GarbageiOS.M_chemeWhehibea102::_seetroqa
	String_t* ____seetroqa_22;
	// System.UInt32 GarbageiOS.M_chemeWhehibea102::_mortrur
	uint32_t ____mortrur_23;
	// System.Single GarbageiOS.M_chemeWhehibea102::_tomis
	float ____tomis_24;
	// System.Single GarbageiOS.M_chemeWhehibea102::_wharairCayhede
	float ____wharairCayhede_25;
	// System.Single GarbageiOS.M_chemeWhehibea102::_gaswher
	float ____gaswher_26;
	// System.Boolean GarbageiOS.M_chemeWhehibea102::_deyaitiHiswhaiso
	bool ____deyaitiHiswhaiso_27;
	// System.Boolean GarbageiOS.M_chemeWhehibea102::_chorcaljo
	bool ____chorcaljo_28;
	// System.UInt32 GarbageiOS.M_chemeWhehibea102::_safe
	uint32_t ____safe_29;
	// System.String GarbageiOS.M_chemeWhehibea102::_tujay
	String_t* ____tujay_30;
	// System.Single GarbageiOS.M_chemeWhehibea102::_mallcasWemjadai
	float ____mallcasWemjadai_31;
	// System.Boolean GarbageiOS.M_chemeWhehibea102::_taipujaZerxouri
	bool ____taipujaZerxouri_32;
	// System.UInt32 GarbageiOS.M_chemeWhehibea102::_rarere
	uint32_t ____rarere_33;
	// System.UInt32 GarbageiOS.M_chemeWhehibea102::_malouLucema
	uint32_t ____malouLucema_34;
	// System.Int32 GarbageiOS.M_chemeWhehibea102::_hairpirTaijousa
	int32_t ____hairpirTaijousa_35;
	// System.String GarbageiOS.M_chemeWhehibea102::_toku
	String_t* ____toku_36;
	// System.Boolean GarbageiOS.M_chemeWhehibea102::_soubaTaryalli
	bool ____soubaTaryalli_37;
	// System.UInt32 GarbageiOS.M_chemeWhehibea102::_hatea
	uint32_t ____hatea_38;
	// System.Single GarbageiOS.M_chemeWhehibea102::_lisrorTenoti
	float ____lisrorTenoti_39;
	// System.UInt32 GarbageiOS.M_chemeWhehibea102::_dotehorDalidall
	uint32_t ____dotehorDalidall_40;
	// System.Boolean GarbageiOS.M_chemeWhehibea102::_stokelTipawve
	bool ____stokelTipawve_41;
	// System.Boolean GarbageiOS.M_chemeWhehibea102::_jachelsear
	bool ____jachelsear_42;
	// System.Int32 GarbageiOS.M_chemeWhehibea102::_paiboStowjerestou
	int32_t ____paiboStowjerestou_43;

public:
	inline static int32_t get_offset_of__sorsi_0() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____sorsi_0)); }
	inline float get__sorsi_0() const { return ____sorsi_0; }
	inline float* get_address_of__sorsi_0() { return &____sorsi_0; }
	inline void set__sorsi_0(float value)
	{
		____sorsi_0 = value;
	}

	inline static int32_t get_offset_of__jermir_1() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____jermir_1)); }
	inline float get__jermir_1() const { return ____jermir_1; }
	inline float* get_address_of__jermir_1() { return &____jermir_1; }
	inline void set__jermir_1(float value)
	{
		____jermir_1 = value;
	}

	inline static int32_t get_offset_of__mejowwai_2() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____mejowwai_2)); }
	inline float get__mejowwai_2() const { return ____mejowwai_2; }
	inline float* get_address_of__mejowwai_2() { return &____mejowwai_2; }
	inline void set__mejowwai_2(float value)
	{
		____mejowwai_2 = value;
	}

	inline static int32_t get_offset_of__daitu_3() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____daitu_3)); }
	inline float get__daitu_3() const { return ____daitu_3; }
	inline float* get_address_of__daitu_3() { return &____daitu_3; }
	inline void set__daitu_3(float value)
	{
		____daitu_3 = value;
	}

	inline static int32_t get_offset_of__hasere_4() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____hasere_4)); }
	inline int32_t get__hasere_4() const { return ____hasere_4; }
	inline int32_t* get_address_of__hasere_4() { return &____hasere_4; }
	inline void set__hasere_4(int32_t value)
	{
		____hasere_4 = value;
	}

	inline static int32_t get_offset_of__jurteLawhelay_5() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____jurteLawhelay_5)); }
	inline int32_t get__jurteLawhelay_5() const { return ____jurteLawhelay_5; }
	inline int32_t* get_address_of__jurteLawhelay_5() { return &____jurteLawhelay_5; }
	inline void set__jurteLawhelay_5(int32_t value)
	{
		____jurteLawhelay_5 = value;
	}

	inline static int32_t get_offset_of__paynastaDarporkear_6() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____paynastaDarporkear_6)); }
	inline String_t* get__paynastaDarporkear_6() const { return ____paynastaDarporkear_6; }
	inline String_t** get_address_of__paynastaDarporkear_6() { return &____paynastaDarporkear_6; }
	inline void set__paynastaDarporkear_6(String_t* value)
	{
		____paynastaDarporkear_6 = value;
		Il2CppCodeGenWriteBarrier(&____paynastaDarporkear_6, value);
	}

	inline static int32_t get_offset_of__cheresearhas_7() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____cheresearhas_7)); }
	inline String_t* get__cheresearhas_7() const { return ____cheresearhas_7; }
	inline String_t** get_address_of__cheresearhas_7() { return &____cheresearhas_7; }
	inline void set__cheresearhas_7(String_t* value)
	{
		____cheresearhas_7 = value;
		Il2CppCodeGenWriteBarrier(&____cheresearhas_7, value);
	}

	inline static int32_t get_offset_of__nesayweMoureaste_8() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____nesayweMoureaste_8)); }
	inline String_t* get__nesayweMoureaste_8() const { return ____nesayweMoureaste_8; }
	inline String_t** get_address_of__nesayweMoureaste_8() { return &____nesayweMoureaste_8; }
	inline void set__nesayweMoureaste_8(String_t* value)
	{
		____nesayweMoureaste_8 = value;
		Il2CppCodeGenWriteBarrier(&____nesayweMoureaste_8, value);
	}

	inline static int32_t get_offset_of__pawpairPairgis_9() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____pawpairPairgis_9)); }
	inline float get__pawpairPairgis_9() const { return ____pawpairPairgis_9; }
	inline float* get_address_of__pawpairPairgis_9() { return &____pawpairPairgis_9; }
	inline void set__pawpairPairgis_9(float value)
	{
		____pawpairPairgis_9 = value;
	}

	inline static int32_t get_offset_of__supeatar_10() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____supeatar_10)); }
	inline bool get__supeatar_10() const { return ____supeatar_10; }
	inline bool* get_address_of__supeatar_10() { return &____supeatar_10; }
	inline void set__supeatar_10(bool value)
	{
		____supeatar_10 = value;
	}

	inline static int32_t get_offset_of__jairkair_11() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____jairkair_11)); }
	inline uint32_t get__jairkair_11() const { return ____jairkair_11; }
	inline uint32_t* get_address_of__jairkair_11() { return &____jairkair_11; }
	inline void set__jairkair_11(uint32_t value)
	{
		____jairkair_11 = value;
	}

	inline static int32_t get_offset_of__cafeema_12() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____cafeema_12)); }
	inline float get__cafeema_12() const { return ____cafeema_12; }
	inline float* get_address_of__cafeema_12() { return &____cafeema_12; }
	inline void set__cafeema_12(float value)
	{
		____cafeema_12 = value;
	}

	inline static int32_t get_offset_of__pemdrear_13() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____pemdrear_13)); }
	inline uint32_t get__pemdrear_13() const { return ____pemdrear_13; }
	inline uint32_t* get_address_of__pemdrear_13() { return &____pemdrear_13; }
	inline void set__pemdrear_13(uint32_t value)
	{
		____pemdrear_13 = value;
	}

	inline static int32_t get_offset_of__chispu_14() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____chispu_14)); }
	inline bool get__chispu_14() const { return ____chispu_14; }
	inline bool* get_address_of__chispu_14() { return &____chispu_14; }
	inline void set__chispu_14(bool value)
	{
		____chispu_14 = value;
	}

	inline static int32_t get_offset_of__hodrowneCisdor_15() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____hodrowneCisdor_15)); }
	inline int32_t get__hodrowneCisdor_15() const { return ____hodrowneCisdor_15; }
	inline int32_t* get_address_of__hodrowneCisdor_15() { return &____hodrowneCisdor_15; }
	inline void set__hodrowneCisdor_15(int32_t value)
	{
		____hodrowneCisdor_15 = value;
	}

	inline static int32_t get_offset_of__berepevaWalda_16() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____berepevaWalda_16)); }
	inline String_t* get__berepevaWalda_16() const { return ____berepevaWalda_16; }
	inline String_t** get_address_of__berepevaWalda_16() { return &____berepevaWalda_16; }
	inline void set__berepevaWalda_16(String_t* value)
	{
		____berepevaWalda_16 = value;
		Il2CppCodeGenWriteBarrier(&____berepevaWalda_16, value);
	}

	inline static int32_t get_offset_of__boudrearawJeawairjis_17() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____boudrearawJeawairjis_17)); }
	inline int32_t get__boudrearawJeawairjis_17() const { return ____boudrearawJeawairjis_17; }
	inline int32_t* get_address_of__boudrearawJeawairjis_17() { return &____boudrearawJeawairjis_17; }
	inline void set__boudrearawJeawairjis_17(int32_t value)
	{
		____boudrearawJeawairjis_17 = value;
	}

	inline static int32_t get_offset_of__najecher_18() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____najecher_18)); }
	inline uint32_t get__najecher_18() const { return ____najecher_18; }
	inline uint32_t* get_address_of__najecher_18() { return &____najecher_18; }
	inline void set__najecher_18(uint32_t value)
	{
		____najecher_18 = value;
	}

	inline static int32_t get_offset_of__drelti_19() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____drelti_19)); }
	inline bool get__drelti_19() const { return ____drelti_19; }
	inline bool* get_address_of__drelti_19() { return &____drelti_19; }
	inline void set__drelti_19(bool value)
	{
		____drelti_19 = value;
	}

	inline static int32_t get_offset_of__tamatall_20() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____tamatall_20)); }
	inline uint32_t get__tamatall_20() const { return ____tamatall_20; }
	inline uint32_t* get_address_of__tamatall_20() { return &____tamatall_20; }
	inline void set__tamatall_20(uint32_t value)
	{
		____tamatall_20 = value;
	}

	inline static int32_t get_offset_of__suse_21() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____suse_21)); }
	inline int32_t get__suse_21() const { return ____suse_21; }
	inline int32_t* get_address_of__suse_21() { return &____suse_21; }
	inline void set__suse_21(int32_t value)
	{
		____suse_21 = value;
	}

	inline static int32_t get_offset_of__seetroqa_22() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____seetroqa_22)); }
	inline String_t* get__seetroqa_22() const { return ____seetroqa_22; }
	inline String_t** get_address_of__seetroqa_22() { return &____seetroqa_22; }
	inline void set__seetroqa_22(String_t* value)
	{
		____seetroqa_22 = value;
		Il2CppCodeGenWriteBarrier(&____seetroqa_22, value);
	}

	inline static int32_t get_offset_of__mortrur_23() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____mortrur_23)); }
	inline uint32_t get__mortrur_23() const { return ____mortrur_23; }
	inline uint32_t* get_address_of__mortrur_23() { return &____mortrur_23; }
	inline void set__mortrur_23(uint32_t value)
	{
		____mortrur_23 = value;
	}

	inline static int32_t get_offset_of__tomis_24() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____tomis_24)); }
	inline float get__tomis_24() const { return ____tomis_24; }
	inline float* get_address_of__tomis_24() { return &____tomis_24; }
	inline void set__tomis_24(float value)
	{
		____tomis_24 = value;
	}

	inline static int32_t get_offset_of__wharairCayhede_25() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____wharairCayhede_25)); }
	inline float get__wharairCayhede_25() const { return ____wharairCayhede_25; }
	inline float* get_address_of__wharairCayhede_25() { return &____wharairCayhede_25; }
	inline void set__wharairCayhede_25(float value)
	{
		____wharairCayhede_25 = value;
	}

	inline static int32_t get_offset_of__gaswher_26() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____gaswher_26)); }
	inline float get__gaswher_26() const { return ____gaswher_26; }
	inline float* get_address_of__gaswher_26() { return &____gaswher_26; }
	inline void set__gaswher_26(float value)
	{
		____gaswher_26 = value;
	}

	inline static int32_t get_offset_of__deyaitiHiswhaiso_27() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____deyaitiHiswhaiso_27)); }
	inline bool get__deyaitiHiswhaiso_27() const { return ____deyaitiHiswhaiso_27; }
	inline bool* get_address_of__deyaitiHiswhaiso_27() { return &____deyaitiHiswhaiso_27; }
	inline void set__deyaitiHiswhaiso_27(bool value)
	{
		____deyaitiHiswhaiso_27 = value;
	}

	inline static int32_t get_offset_of__chorcaljo_28() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____chorcaljo_28)); }
	inline bool get__chorcaljo_28() const { return ____chorcaljo_28; }
	inline bool* get_address_of__chorcaljo_28() { return &____chorcaljo_28; }
	inline void set__chorcaljo_28(bool value)
	{
		____chorcaljo_28 = value;
	}

	inline static int32_t get_offset_of__safe_29() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____safe_29)); }
	inline uint32_t get__safe_29() const { return ____safe_29; }
	inline uint32_t* get_address_of__safe_29() { return &____safe_29; }
	inline void set__safe_29(uint32_t value)
	{
		____safe_29 = value;
	}

	inline static int32_t get_offset_of__tujay_30() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____tujay_30)); }
	inline String_t* get__tujay_30() const { return ____tujay_30; }
	inline String_t** get_address_of__tujay_30() { return &____tujay_30; }
	inline void set__tujay_30(String_t* value)
	{
		____tujay_30 = value;
		Il2CppCodeGenWriteBarrier(&____tujay_30, value);
	}

	inline static int32_t get_offset_of__mallcasWemjadai_31() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____mallcasWemjadai_31)); }
	inline float get__mallcasWemjadai_31() const { return ____mallcasWemjadai_31; }
	inline float* get_address_of__mallcasWemjadai_31() { return &____mallcasWemjadai_31; }
	inline void set__mallcasWemjadai_31(float value)
	{
		____mallcasWemjadai_31 = value;
	}

	inline static int32_t get_offset_of__taipujaZerxouri_32() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____taipujaZerxouri_32)); }
	inline bool get__taipujaZerxouri_32() const { return ____taipujaZerxouri_32; }
	inline bool* get_address_of__taipujaZerxouri_32() { return &____taipujaZerxouri_32; }
	inline void set__taipujaZerxouri_32(bool value)
	{
		____taipujaZerxouri_32 = value;
	}

	inline static int32_t get_offset_of__rarere_33() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____rarere_33)); }
	inline uint32_t get__rarere_33() const { return ____rarere_33; }
	inline uint32_t* get_address_of__rarere_33() { return &____rarere_33; }
	inline void set__rarere_33(uint32_t value)
	{
		____rarere_33 = value;
	}

	inline static int32_t get_offset_of__malouLucema_34() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____malouLucema_34)); }
	inline uint32_t get__malouLucema_34() const { return ____malouLucema_34; }
	inline uint32_t* get_address_of__malouLucema_34() { return &____malouLucema_34; }
	inline void set__malouLucema_34(uint32_t value)
	{
		____malouLucema_34 = value;
	}

	inline static int32_t get_offset_of__hairpirTaijousa_35() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____hairpirTaijousa_35)); }
	inline int32_t get__hairpirTaijousa_35() const { return ____hairpirTaijousa_35; }
	inline int32_t* get_address_of__hairpirTaijousa_35() { return &____hairpirTaijousa_35; }
	inline void set__hairpirTaijousa_35(int32_t value)
	{
		____hairpirTaijousa_35 = value;
	}

	inline static int32_t get_offset_of__toku_36() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____toku_36)); }
	inline String_t* get__toku_36() const { return ____toku_36; }
	inline String_t** get_address_of__toku_36() { return &____toku_36; }
	inline void set__toku_36(String_t* value)
	{
		____toku_36 = value;
		Il2CppCodeGenWriteBarrier(&____toku_36, value);
	}

	inline static int32_t get_offset_of__soubaTaryalli_37() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____soubaTaryalli_37)); }
	inline bool get__soubaTaryalli_37() const { return ____soubaTaryalli_37; }
	inline bool* get_address_of__soubaTaryalli_37() { return &____soubaTaryalli_37; }
	inline void set__soubaTaryalli_37(bool value)
	{
		____soubaTaryalli_37 = value;
	}

	inline static int32_t get_offset_of__hatea_38() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____hatea_38)); }
	inline uint32_t get__hatea_38() const { return ____hatea_38; }
	inline uint32_t* get_address_of__hatea_38() { return &____hatea_38; }
	inline void set__hatea_38(uint32_t value)
	{
		____hatea_38 = value;
	}

	inline static int32_t get_offset_of__lisrorTenoti_39() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____lisrorTenoti_39)); }
	inline float get__lisrorTenoti_39() const { return ____lisrorTenoti_39; }
	inline float* get_address_of__lisrorTenoti_39() { return &____lisrorTenoti_39; }
	inline void set__lisrorTenoti_39(float value)
	{
		____lisrorTenoti_39 = value;
	}

	inline static int32_t get_offset_of__dotehorDalidall_40() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____dotehorDalidall_40)); }
	inline uint32_t get__dotehorDalidall_40() const { return ____dotehorDalidall_40; }
	inline uint32_t* get_address_of__dotehorDalidall_40() { return &____dotehorDalidall_40; }
	inline void set__dotehorDalidall_40(uint32_t value)
	{
		____dotehorDalidall_40 = value;
	}

	inline static int32_t get_offset_of__stokelTipawve_41() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____stokelTipawve_41)); }
	inline bool get__stokelTipawve_41() const { return ____stokelTipawve_41; }
	inline bool* get_address_of__stokelTipawve_41() { return &____stokelTipawve_41; }
	inline void set__stokelTipawve_41(bool value)
	{
		____stokelTipawve_41 = value;
	}

	inline static int32_t get_offset_of__jachelsear_42() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____jachelsear_42)); }
	inline bool get__jachelsear_42() const { return ____jachelsear_42; }
	inline bool* get_address_of__jachelsear_42() { return &____jachelsear_42; }
	inline void set__jachelsear_42(bool value)
	{
		____jachelsear_42 = value;
	}

	inline static int32_t get_offset_of__paiboStowjerestou_43() { return static_cast<int32_t>(offsetof(M_chemeWhehibea102_t4136819874, ____paiboStowjerestou_43)); }
	inline int32_t get__paiboStowjerestou_43() const { return ____paiboStowjerestou_43; }
	inline int32_t* get_address_of__paiboStowjerestou_43() { return &____paiboStowjerestou_43; }
	inline void set__paiboStowjerestou_43(int32_t value)
	{
		____paiboStowjerestou_43 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
