﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._889197c7a454ba5aed152f90a7e50af2
struct _889197c7a454ba5aed152f90a7e50af2_t1674211281;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._889197c7a454ba5aed152f90a7e50af2::.ctor()
extern "C"  void _889197c7a454ba5aed152f90a7e50af2__ctor_m1127936380 (_889197c7a454ba5aed152f90a7e50af2_t1674211281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._889197c7a454ba5aed152f90a7e50af2::_889197c7a454ba5aed152f90a7e50af2m2(System.Int32)
extern "C"  int32_t _889197c7a454ba5aed152f90a7e50af2__889197c7a454ba5aed152f90a7e50af2m2_m2666854393 (_889197c7a454ba5aed152f90a7e50af2_t1674211281 * __this, int32_t ____889197c7a454ba5aed152f90a7e50af2a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._889197c7a454ba5aed152f90a7e50af2::_889197c7a454ba5aed152f90a7e50af2m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _889197c7a454ba5aed152f90a7e50af2__889197c7a454ba5aed152f90a7e50af2m_m454925021 (_889197c7a454ba5aed152f90a7e50af2_t1674211281 * __this, int32_t ____889197c7a454ba5aed152f90a7e50af2a0, int32_t ____889197c7a454ba5aed152f90a7e50af2171, int32_t ____889197c7a454ba5aed152f90a7e50af2c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
