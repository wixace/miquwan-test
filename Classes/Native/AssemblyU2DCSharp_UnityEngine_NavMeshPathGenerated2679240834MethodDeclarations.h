﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_NavMeshPathGenerated
struct UnityEngine_NavMeshPathGenerated_t2679240834;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_NavMeshPathGenerated::.ctor()
extern "C"  void UnityEngine_NavMeshPathGenerated__ctor_m3851841625 (UnityEngine_NavMeshPathGenerated_t2679240834 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshPathGenerated::NavMeshPath_NavMeshPath1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshPathGenerated_NavMeshPath_NavMeshPath1_m429608213 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshPathGenerated::NavMeshPath_corners(JSVCall)
extern "C"  void UnityEngine_NavMeshPathGenerated_NavMeshPath_corners_m4154851548 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshPathGenerated::NavMeshPath_status(JSVCall)
extern "C"  void UnityEngine_NavMeshPathGenerated_NavMeshPath_status_m658005248 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshPathGenerated::NavMeshPath_ClearCorners(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshPathGenerated_NavMeshPath_ClearCorners_m1188131882 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshPathGenerated::NavMeshPath_GetCornersNonAlloc__Vector3_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshPathGenerated_NavMeshPath_GetCornersNonAlloc__Vector3_Array_m120703971 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshPathGenerated::__Register()
extern "C"  void UnityEngine_NavMeshPathGenerated___Register_m1148214286 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine_NavMeshPathGenerated::<NavMeshPath_GetCornersNonAlloc__Vector3_Array>m__27E()
extern "C"  Vector3U5BU5D_t215400611* UnityEngine_NavMeshPathGenerated_U3CNavMeshPath_GetCornersNonAlloc__Vector3_ArrayU3Em__27E_m3269209724 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshPathGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_NavMeshPathGenerated_ilo_attachFinalizerObject1_m134680230 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshPathGenerated::ilo_moveSaveID2Arr2(System.Int32)
extern "C"  void UnityEngine_NavMeshPathGenerated_ilo_moveSaveID2Arr2_m1973951343 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
