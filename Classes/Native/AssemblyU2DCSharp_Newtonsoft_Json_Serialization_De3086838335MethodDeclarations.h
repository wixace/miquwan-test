﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.DefaultContractResolver
struct DefaultContractResolver_t3086838335;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_t507353639;
// Newtonsoft.Json.Serialization.JsonContract
struct JsonContract_t1328848902;
// System.Type
struct Type_t;
// System.Collections.Generic.List`1<System.Reflection.MemberInfo>
struct List_1_t1068734154;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// Newtonsoft.Json.Serialization.JObjectContract
struct JObjectContract_t2867848225;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t4136801618;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.JsonProperty>
struct IList_1_t3597302380;
// Newtonsoft.Json.Serialization.JsonPropertyCollection
struct JsonPropertyCollection_t717767559;
// Newtonsoft.Json.Serialization.JsonProperty
struct JsonProperty_t902655177;
// System.Reflection.ParameterInfo
struct ParameterInfo_t2235474049;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t2159686854;
// System.Func`1<System.Object>
struct Func_1_t1001010649;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// Newtonsoft.Json.Serialization.JsonDictionaryContract
struct JsonDictionaryContract_t989352188;
// Newtonsoft.Json.Serialization.JsonArrayContract
struct JsonArrayContract_t145179369;
// Newtonsoft.Json.Serialization.JsonPrimitiveContract
struct JsonPrimitiveContract_t554063095;
// Newtonsoft.Json.Serialization.JsonLinqContract
struct JsonLinqContract_t1458265766;
// Newtonsoft.Json.Serialization.JsonISerializableContract
struct JsonISerializableContract_t624170136;
// Newtonsoft.Json.Serialization.JsonStringContract
struct JsonStringContract_t4087007991;
// System.Reflection.ParameterInfo[]
struct ParameterInfoU5BU5D_t2015293532;
// System.String
struct String_t;
// Newtonsoft.Json.Serialization.IValueProvider
struct IValueProvider_t175677893;
// System.Reflection.ICustomAttributeProvider
struct ICustomAttributeProvider_t1425685797;
// System.Predicate`1<System.Object>
struct Predicate_1_t3781873254;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Utilities.ReflectionDelegateFactory
struct ReflectionDelegateFactory_t1590616920;
// System.Runtime.Serialization.DataContractAttribute
struct DataContractAttribute_t2462274566;
// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>
struct IList_1_t559366761;
// Newtonsoft.Json.JObjectAttribute
struct JObjectAttribute_t3411920523;
// Newtonsoft.Json.JsonArrayAttribute
struct JsonArrayAttribute_t2333618243;
// System.ComponentModel.TypeConverter
struct TypeConverter_t1753450284;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Runtime.Serialization.DataMemberAttribute
struct DataMemberAttribute_t2601848894;
// Newtonsoft.Json.JsonPropertyAttribute
struct JsonPropertyAttribute_t3435603053;
// System.Action`2<System.Object,System.Object>
struct Action_2_t4293064463;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_BindingFlags1523912596.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "mscorlib_System_Reflection_ConstructorInfo4136801618.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso717767559.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso902655177.h"
#include "mscorlib_System_Reflection_ParameterInfo2235474049.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Js1328848902.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_MemberSerializat1550301796.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_JO2867848225.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonConverter2159686854.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Required3921306327.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"
#include "mscorlib_System_Nullable_1_gen2838778904.h"
#include "mscorlib_System_Nullable_1_gen1653574568.h"
#include "mscorlib_System_Nullable_1_gen2845787645.h"
#include "mscorlib_System_Nullable_1_gen140208118.h"
#include "mscorlib_System_Nullable_1_gen2443451997.h"
#include "AssemblyU2DCSharp_System_Runtime_Serialization_Dat2462274566.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_De3086838335.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonPropertyAttr3435603053.h"
#include "mscorlib_System_Nullable_1_gen1237965023.h"

// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::.ctor()
extern "C"  void DefaultContractResolver__ctor_m2226229541 (DefaultContractResolver_t3086838335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::.ctor(System.Boolean)
extern "C"  void DefaultContractResolver__ctor_m1094509020 (DefaultContractResolver_t3086838335 * __this, bool ___shareCache0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::.cctor()
extern "C"  void DefaultContractResolver__cctor_m4106510120 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.Serialization.DefaultContractResolver::get_Instance()
extern "C"  Il2CppObject * DefaultContractResolver_get_Instance_m3971822488 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::get_DynamicCodeGeneration()
extern "C"  bool DefaultContractResolver_get_DynamicCodeGeneration_m2334145810 (DefaultContractResolver_t3086838335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.BindingFlags Newtonsoft.Json.Serialization.DefaultContractResolver::get_DefaultMembersSearchFlags()
extern "C"  int32_t DefaultContractResolver_get_DefaultMembersSearchFlags_m3419364184 (DefaultContractResolver_t3086838335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::set_DefaultMembersSearchFlags(System.Reflection.BindingFlags)
extern "C"  void DefaultContractResolver_set_DefaultMembersSearchFlags_m3054309287 (DefaultContractResolver_t3086838335 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::get_SerializeCompilerGeneratedMembers()
extern "C"  bool DefaultContractResolver_get_SerializeCompilerGeneratedMembers_m4255795607 (DefaultContractResolver_t3086838335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::set_SerializeCompilerGeneratedMembers(System.Boolean)
extern "C"  void DefaultContractResolver_set_SerializeCompilerGeneratedMembers_m3856346150 (DefaultContractResolver_t3086838335 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.DefaultContractResolver::ResolveContract(System.Type)
extern "C"  JsonContract_t1328848902 * DefaultContractResolver_ResolveContract_m264068904 (DefaultContractResolver_t3086838335 * __this, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Reflection.MemberInfo> Newtonsoft.Json.Serialization.DefaultContractResolver::GetSerializableMembers(System.Type)
extern "C"  List_1_t1068734154 * DefaultContractResolver_GetSerializableMembers_m442594293 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::ShouldSerializeEntityMember(System.Reflection.MemberInfo)
extern "C"  bool DefaultContractResolver_ShouldSerializeEntityMember_m694562357 (DefaultContractResolver_t3086838335 * __this, MemberInfo_t * ___memberInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JObjectContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateObjectContract(System.Type)
extern "C"  JObjectContract_t2867848225 * DefaultContractResolver_CreateObjectContract_m3175354904 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.DefaultContractResolver::GetAttributeConstructor(System.Type)
extern "C"  ConstructorInfo_t4136801618 * DefaultContractResolver_GetAttributeConstructor_m2752338419 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.DefaultContractResolver::GetParametrizedConstructor(System.Type)
extern "C"  ConstructorInfo_t4136801618 * DefaultContractResolver_GetParametrizedConstructor_m2325060875 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.JsonProperty> Newtonsoft.Json.Serialization.DefaultContractResolver::CreateConstructorParameters(System.Reflection.ConstructorInfo,Newtonsoft.Json.Serialization.JsonPropertyCollection)
extern "C"  Il2CppObject* DefaultContractResolver_CreateConstructorParameters_m3808836759 (DefaultContractResolver_t3086838335 * __this, ConstructorInfo_t4136801618 * ___constructor0, JsonPropertyCollection_t717767559 * ___memberProperties1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.DefaultContractResolver::CreatePropertyFromConstructorParameter(Newtonsoft.Json.Serialization.JsonProperty,System.Reflection.ParameterInfo)
extern "C"  JsonProperty_t902655177 * DefaultContractResolver_CreatePropertyFromConstructorParameter_m4004255909 (DefaultContractResolver_t3086838335 * __this, JsonProperty_t902655177 * ___matchingMemberProperty0, ParameterInfo_t2235474049 * ___parameterInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.DefaultContractResolver::ResolveContractConverter(System.Type)
extern "C"  JsonConverter_t2159686854 * DefaultContractResolver_ResolveContractConverter_m1182381512 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Func`1<System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver::GetDefaultCreator(System.Type)
extern "C"  Func_1_t1001010649 * DefaultContractResolver_GetDefaultCreator_m2773754592 (DefaultContractResolver_t3086838335 * __this, Type_t * ___createdType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::InitializeContract(Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void DefaultContractResolver_InitializeContract_m3088838164 (DefaultContractResolver_t3086838335 * __this, JsonContract_t1328848902 * ___contract0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ResolveCallbackMethods(Newtonsoft.Json.Serialization.JsonContract,System.Type)
extern "C"  void DefaultContractResolver_ResolveCallbackMethods_m606941862 (DefaultContractResolver_t3086838335 * __this, JsonContract_t1328848902 * ___contract0, Type_t * ___t1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::GetCallbackMethodsForType(System.Type,System.Reflection.MethodInfo&,System.Reflection.MethodInfo&,System.Reflection.MethodInfo&,System.Reflection.MethodInfo&,System.Reflection.MethodInfo&)
extern "C"  void DefaultContractResolver_GetCallbackMethodsForType_m2447904967 (DefaultContractResolver_t3086838335 * __this, Type_t * ___type0, MethodInfo_t ** ___onSerializing1, MethodInfo_t ** ___onSerialized2, MethodInfo_t ** ___onDeserializing3, MethodInfo_t ** ___onDeserialized4, MethodInfo_t ** ___onError5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonDictionaryContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateDictionaryContract(System.Type)
extern "C"  JsonDictionaryContract_t989352188 * DefaultContractResolver_CreateDictionaryContract_m2514124560 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonArrayContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateArrayContract(System.Type)
extern "C"  JsonArrayContract_t145179369 * DefaultContractResolver_CreateArrayContract_m3305075332 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonPrimitiveContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreatePrimitiveContract(System.Type)
extern "C"  JsonPrimitiveContract_t554063095 * DefaultContractResolver_CreatePrimitiveContract_m227119044 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonLinqContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateLinqContract(System.Type)
extern "C"  JsonLinqContract_t1458265766 * DefaultContractResolver_CreateLinqContract_m3088859324 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonISerializableContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateISerializableContract(System.Type)
extern "C"  JsonISerializableContract_t624170136 * DefaultContractResolver_CreateISerializableContract_m2168635876 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonStringContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateStringContract(System.Type)
extern "C"  JsonStringContract_t4087007991 * DefaultContractResolver_CreateStringContract_m2973852506 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.DefaultContractResolver::CreateContract(System.Type)
extern "C"  JsonContract_t1328848902 * DefaultContractResolver_CreateContract_m1655376124 (DefaultContractResolver_t3086838335 * __this, Type_t * ___objectType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::CanConvertToString(System.Type)
extern "C"  bool DefaultContractResolver_CanConvertToString_m982344671 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::IsValidCallback(System.Reflection.MethodInfo,System.Reflection.ParameterInfo[],System.Type,System.Reflection.MethodInfo,System.Type&)
extern "C"  bool DefaultContractResolver_IsValidCallback_m1424809419 (Il2CppObject * __this /* static, unused */, MethodInfo_t * ___method0, ParameterInfoU5BU5D_t2015293532* ___parameters1, Type_t * ___attributeType2, MethodInfo_t * ___currentCallback3, Type_t ** ___prevAttributeType4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.DefaultContractResolver::GetClrTypeFullName(System.Type)
extern "C"  String_t* DefaultContractResolver_GetClrTypeFullName_m1162570678 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.JsonProperty> Newtonsoft.Json.Serialization.DefaultContractResolver::CreateProperties(System.Type,Newtonsoft.Json.MemberSerialization)
extern "C"  Il2CppObject* DefaultContractResolver_CreateProperties_m1810277208 (DefaultContractResolver_t3086838335 * __this, Type_t * ___type0, int32_t ___memberSerialization1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IValueProvider Newtonsoft.Json.Serialization.DefaultContractResolver::CreateMemberValueProvider(System.Reflection.MemberInfo)
extern "C"  Il2CppObject * DefaultContractResolver_CreateMemberValueProvider_m1888863220 (DefaultContractResolver_t3086838335 * __this, MemberInfo_t * ___member0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.DefaultContractResolver::CreateProperty(System.Reflection.MemberInfo,Newtonsoft.Json.MemberSerialization)
extern "C"  JsonProperty_t902655177 * DefaultContractResolver_CreateProperty_m1388020866 (DefaultContractResolver_t3086838335 * __this, MemberInfo_t * ___member0, int32_t ___memberSerialization1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::SetPropertySettingsFromAttributes(Newtonsoft.Json.Serialization.JsonProperty,System.Reflection.ICustomAttributeProvider,System.String,System.Type,Newtonsoft.Json.MemberSerialization,System.Boolean&,System.Boolean&)
extern "C"  void DefaultContractResolver_SetPropertySettingsFromAttributes_m875975449 (DefaultContractResolver_t3086838335 * __this, JsonProperty_t902655177 * ___property0, Il2CppObject * ___attributeProvider1, String_t* ___name2, Type_t * ___declaringType3, int32_t ___memberSerialization4, bool* ___allowNonPublicAccess5, bool* ___hasExplicitAttribute6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver::CreateShouldSerializeTest(System.Reflection.MemberInfo)
extern "C"  Predicate_1_t3781873254 * DefaultContractResolver_CreateShouldSerializeTest_m464574170 (DefaultContractResolver_t3086838335 * __this, MemberInfo_t * ___member0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::SetIsSpecifiedActions(Newtonsoft.Json.Serialization.JsonProperty,System.Reflection.MemberInfo,System.Boolean)
extern "C"  void DefaultContractResolver_SetIsSpecifiedActions_m2070632079 (DefaultContractResolver_t3086838335 * __this, JsonProperty_t902655177 * ___property0, MemberInfo_t * ___member1, bool ___allowNonPublicAccess2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.DefaultContractResolver::ResolvePropertyName(System.String)
extern "C"  String_t* DefaultContractResolver_ResolvePropertyName_m1898433744 (DefaultContractResolver_t3086838335 * __this, String_t* ___propertyName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::<GetSerializableMembers>m__381(System.Reflection.MemberInfo)
extern "C"  bool DefaultContractResolver_U3CGetSerializableMembersU3Em__381_m2241735036 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::<GetSerializableMembers>m__382(System.Reflection.MemberInfo)
extern "C"  bool DefaultContractResolver_U3CGetSerializableMembersU3Em__382_m2592534973 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::<CreateObjectContract>m__383(System.Reflection.ConstructorInfo)
extern "C"  bool DefaultContractResolver_U3CCreateObjectContractU3Em__383_m3287869229 (Il2CppObject * __this /* static, unused */, ConstructorInfo_t4136801618 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::<GetAttributeConstructor>m__384(System.Reflection.ConstructorInfo)
extern "C"  bool DefaultContractResolver_U3CGetAttributeConstructorU3Em__384_m2967039823 (Il2CppObject * __this /* static, unused */, ConstructorInfo_t4136801618 * ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Serialization.DefaultContractResolver::<CreateProperties>m__386(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  int32_t DefaultContractResolver_U3CCreatePropertiesU3Em__386_m2474197496 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Reflection.MemberInfo> Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_GetFieldsAndProperties1(System.Type,System.Reflection.BindingFlags)
extern "C"  List_1_t1068734154 * DefaultContractResolver_ilo_GetFieldsAndProperties1_m2518537196 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, int32_t ___bindingAttr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_AssignableToTypeName2(System.Type,System.String,System.Type&)
extern "C"  bool DefaultContractResolver_ilo_AssignableToTypeName2_m3657894321 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, String_t* ___fullTypeName1, Type_t ** ___match2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_get_Properties3(Newtonsoft.Json.Serialization.JObjectContract)
extern "C"  JsonPropertyCollection_t717767559 * DefaultContractResolver_ilo_get_Properties3_m1341540404 (Il2CppObject * __this /* static, unused */, JObjectContract_t2867848225 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_get_ConstructorParameters4(Newtonsoft.Json.Serialization.JObjectContract)
extern "C"  JsonPropertyCollection_t717767559 * DefaultContractResolver_ilo_get_ConstructorParameters4_m3343500590 (Il2CppObject * __this /* static, unused */, JObjectContract_t2867848225 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_get_PropertyType5(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Type_t * DefaultContractResolver_ilo_get_PropertyType5_m3849122999 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_set_Readable6(Newtonsoft.Json.Serialization.JsonProperty,System.Boolean)
extern "C"  void DefaultContractResolver_ilo_set_Readable6_m4140186324 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_get_PropertyName7(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  String_t* DefaultContractResolver_ilo_get_PropertyName7_m1990134289 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_get_Converter8(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  JsonConverter_t2159686854 * DefaultContractResolver_ilo_get_Converter8_m1407111923 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_set_Converter9(Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonConverter)
extern "C"  void DefaultContractResolver_ilo_set_Converter9_m3740346145 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, JsonConverter_t2159686854 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_get_MemberConverter10(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  JsonConverter_t2159686854 * DefaultContractResolver_ilo_get_MemberConverter10_m2690790540 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_get_DefaultValue11(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Il2CppObject * DefaultContractResolver_ilo_get_DefaultValue11_m1086782758 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Required Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_get_Required12(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  int32_t DefaultContractResolver_ilo_get_Required12_m3349571586 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_set_Required13(Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Required)
extern "C"  void DefaultContractResolver_ilo_set_Required13_m2784058994 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_get_IsReference14(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Nullable_1_t560925241  DefaultContractResolver_ilo_get_IsReference14_m1185383844 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_get_NullValueHandling15(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Nullable_1_t2838778904  DefaultContractResolver_ilo_get_NullValueHandling15_m1652022168 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling> Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_get_DefaultValueHandling16(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Nullable_1_t1653574568  DefaultContractResolver_ilo_get_DefaultValueHandling16_m1719383131 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_set_DefaultValueHandling17(Newtonsoft.Json.Serialization.JsonProperty,System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>)
extern "C"  void DefaultContractResolver_ilo_set_DefaultValueHandling17_m630372093 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, Nullable_1_t1653574568  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_get_ReferenceLoopHandling18(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Nullable_1_t2845787645  DefaultContractResolver_ilo_get_ReferenceLoopHandling18_m2512497189 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling> Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_get_ObjectCreationHandling19(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Nullable_1_t140208118  DefaultContractResolver_ilo_get_ObjectCreationHandling19_m931807966 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_set_ObjectCreationHandling20(Newtonsoft.Json.Serialization.JsonProperty,System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>)
extern "C"  void DefaultContractResolver_ilo_set_ObjectCreationHandling20_m4047468529 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, Nullable_1_t140208118  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_set_TypeNameHandling21(Newtonsoft.Json.Serialization.JsonProperty,System.Nullable`1<Newtonsoft.Json.TypeNameHandling>)
extern "C"  void DefaultContractResolver_ilo_set_TypeNameHandling21_m4014406272 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, Nullable_1_t2443451997  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Utilities.ReflectionDelegateFactory Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_get_ReflectionDelegateFactory22()
extern "C"  ReflectionDelegateFactory_t1590616920 * DefaultContractResolver_ilo_get_ReflectionDelegateFactory22_m1093512819 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_set_IsReference23(Newtonsoft.Json.Serialization.JsonContract,System.Nullable`1<System.Boolean>)
extern "C"  void DefaultContractResolver_ilo_set_IsReference23_m3223294538 (Il2CppObject * __this /* static, unused */, JsonContract_t1328848902 * ____this0, Nullable_1_t560925241  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_get_UnderlyingType24(Newtonsoft.Json.Serialization.JsonContract)
extern "C"  Type_t * DefaultContractResolver_ilo_get_UnderlyingType24_m930652205 (Il2CppObject * __this /* static, unused */, JsonContract_t1328848902 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_get_IsReference25(System.Runtime.Serialization.DataContractAttribute)
extern "C"  bool DefaultContractResolver_ilo_get_IsReference25_m2813382994 (Il2CppObject * __this /* static, unused */, DataContractAttribute_t2462274566 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_ResolveContractConverter26(Newtonsoft.Json.Serialization.DefaultContractResolver,System.Type)
extern "C"  JsonConverter_t2159686854 * DefaultContractResolver_ilo_ResolveContractConverter26_m2217313687 (Il2CppObject * __this /* static, unused */, DefaultContractResolver_t3086838335 * ____this0, Type_t * ___objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_set_Converter27(Newtonsoft.Json.Serialization.JsonContract,Newtonsoft.Json.JsonConverter)
extern "C"  void DefaultContractResolver_ilo_set_Converter27_m3795867252 (Il2CppObject * __this /* static, unused */, JsonContract_t1328848902 * ____this0, JsonConverter_t2159686854 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_GetMatchingConverter28(System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>,System.Type)
extern "C"  JsonConverter_t2159686854 * DefaultContractResolver_ilo_GetMatchingConverter28_m1168283810 (Il2CppObject * __this /* static, unused */, Il2CppObject* ___converters0, Type_t * ___objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_get_CreatedType29(Newtonsoft.Json.Serialization.JsonContract)
extern "C"  Type_t * DefaultContractResolver_ilo_get_CreatedType29_m166683965 (Il2CppObject * __this /* static, unused */, JsonContract_t1328848902 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_GetCallbackMethodsForType30(Newtonsoft.Json.Serialization.DefaultContractResolver,System.Type,System.Reflection.MethodInfo&,System.Reflection.MethodInfo&,System.Reflection.MethodInfo&,System.Reflection.MethodInfo&,System.Reflection.MethodInfo&)
extern "C"  void DefaultContractResolver_ilo_GetCallbackMethodsForType30_m1328117027 (Il2CppObject * __this /* static, unused */, DefaultContractResolver_t3086838335 * ____this0, Type_t * ___type1, MethodInfo_t ** ___onSerializing2, MethodInfo_t ** ___onSerialized3, MethodInfo_t ** ___onDeserializing4, MethodInfo_t ** ___onDeserialized5, MethodInfo_t ** ___onError6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_set_OnSerializing31(Newtonsoft.Json.Serialization.JsonContract,System.Reflection.MethodInfo)
extern "C"  void DefaultContractResolver_ilo_set_OnSerializing31_m804328433 (Il2CppObject * __this /* static, unused */, JsonContract_t1328848902 * ____this0, MethodInfo_t * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_IsValidCallback32(System.Reflection.MethodInfo,System.Reflection.ParameterInfo[],System.Type,System.Reflection.MethodInfo,System.Type&)
extern "C"  bool DefaultContractResolver_ilo_IsValidCallback32_m4133070615 (Il2CppObject * __this /* static, unused */, MethodInfo_t * ___method0, ParameterInfoU5BU5D_t2015293532* ___parameters1, Type_t * ___attributeType2, MethodInfo_t * ___currentCallback3, Type_t ** ___prevAttributeType4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_InitializeContract33(Newtonsoft.Json.Serialization.DefaultContractResolver,Newtonsoft.Json.Serialization.JsonContract)
extern "C"  void DefaultContractResolver_ilo_InitializeContract33_m2877614017 (Il2CppObject * __this /* static, unused */, DefaultContractResolver_t3086838335 * ____this0, JsonContract_t1328848902 * ___contract1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JObjectAttribute Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_GetJObjectAttribute34(System.Type)
extern "C"  JObjectAttribute_t3411920523 * DefaultContractResolver_ilo_GetJObjectAttribute34_m29484394 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JObjectContract Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_CreateObjectContract35(Newtonsoft.Json.Serialization.DefaultContractResolver,System.Type)
extern "C"  JObjectContract_t2867848225 * DefaultContractResolver_ilo_CreateObjectContract35_m471055881 (Il2CppObject * __this /* static, unused */, DefaultContractResolver_t3086838335 * ____this0, Type_t * ___objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonArrayAttribute Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_GetJsonArrayAttribute36(System.Type)
extern "C"  JsonArrayAttribute_t2333618243 * DefaultContractResolver_ilo_GetJsonArrayAttribute36_m1337918504 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonArrayContract Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_CreateArrayContract37(Newtonsoft.Json.Serialization.DefaultContractResolver,System.Type)
extern "C"  JsonArrayContract_t145179369 * DefaultContractResolver_ilo_CreateArrayContract37_m2867568697 (Il2CppObject * __this /* static, unused */, DefaultContractResolver_t3086838335 * ____this0, Type_t * ___objectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_IsDictionaryType38(System.Type)
extern "C"  bool DefaultContractResolver_ilo_IsDictionaryType38_m539997436 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_CanConvertToString39(System.Type)
extern "C"  bool DefaultContractResolver_ilo_CanConvertToString39_m2067941030 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.TypeConverter Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_GetConverter40(System.Type)
extern "C"  TypeConverter_t1753450284 * DefaultContractResolver_ilo_GetConverter40_m2479274577 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_GetClrTypeFullName41(System.Type)
extern "C"  String_t* DefaultContractResolver_ilo_GetClrTypeFullName41_m337379974 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_FormatWith42(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* DefaultContractResolver_ilo_FormatWith42_m563499944 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_CreateProperty43(Newtonsoft.Json.Serialization.DefaultContractResolver,System.Reflection.MemberInfo,Newtonsoft.Json.MemberSerialization)
extern "C"  JsonProperty_t902655177 * DefaultContractResolver_ilo_CreateProperty43_m2356658062 (Il2CppObject * __this /* static, unused */, DefaultContractResolver_t3086838335 * ____this0, MemberInfo_t * ___member1, int32_t ___memberSerialization2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_GetMemberUnderlyingType44(System.Reflection.MemberInfo)
extern "C"  Type_t * DefaultContractResolver_ilo_GetMemberUnderlyingType44_m1271544453 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_set_ValueProvider45(Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.IValueProvider)
extern "C"  void DefaultContractResolver_ilo_set_ValueProvider45_m3574331124 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_CanSetMemberValue46(System.Reflection.MemberInfo,System.Boolean,System.Boolean)
extern "C"  bool DefaultContractResolver_ilo_CanSetMemberValue46_m184347583 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, bool ___nonPublic1, bool ___canSetReadOnly2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_set_Writable47(Newtonsoft.Json.Serialization.JsonProperty,System.Boolean)
extern "C"  void DefaultContractResolver_ilo_set_Writable47_m2343119035 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Runtime.Serialization.DataMemberAttribute Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_GetDataMemberAttribute48(System.Reflection.MemberInfo)
extern "C"  DataMemberAttribute_t2601848894 * DefaultContractResolver_ilo_GetDataMemberAttribute48_m2054731924 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___memberInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_get_PropertyName49(Newtonsoft.Json.JsonPropertyAttribute)
extern "C"  String_t* DefaultContractResolver_ilo_get_PropertyName49_m3730826507 (Il2CppObject * __this /* static, unused */, JsonPropertyAttribute_t3435603053 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_ResolvePropertyName50(Newtonsoft.Json.Serialization.DefaultContractResolver,System.String)
extern "C"  String_t* DefaultContractResolver_ilo_ResolvePropertyName50_m3702038126 (Il2CppObject * __this /* static, unused */, DefaultContractResolver_t3086838335 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_set_Order51(Newtonsoft.Json.Serialization.JsonProperty,System.Nullable`1<System.Int32>)
extern "C"  void DefaultContractResolver_ilo_set_Order51_m3128560949 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, Nullable_1_t1237965023  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_GetJsonConverter52(System.Reflection.ICustomAttributeProvider,System.Type)
extern "C"  JsonConverter_t2159686854 * DefaultContractResolver_ilo_GetJsonConverter52_m1856316957 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___attributeProvider0, Type_t * ___targetConvertedType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_set_MemberConverter53(Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.JsonConverter)
extern "C"  void DefaultContractResolver_ilo_set_MemberConverter53_m3983491696 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, JsonConverter_t2159686854 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.BindingFlags Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_get_DefaultMembersSearchFlags54(Newtonsoft.Json.Serialization.DefaultContractResolver)
extern "C"  int32_t DefaultContractResolver_ilo_get_DefaultMembersSearchFlags54_m1740897480 (Il2CppObject * __this /* static, unused */, DefaultContractResolver_t3086838335 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_set_GetIsSpecified55(Newtonsoft.Json.Serialization.JsonProperty,System.Predicate`1<System.Object>)
extern "C"  void DefaultContractResolver_ilo_set_GetIsSpecified55_m1522051016 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, Predicate_1_t3781873254 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_set_SetIsSpecified56(Newtonsoft.Json.Serialization.JsonProperty,System.Action`2<System.Object,System.Object>)
extern "C"  void DefaultContractResolver_ilo_set_SetIsSpecified56_m1703289759 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, Action_2_t4293064463 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_IsIndexedProperty57(System.Reflection.MemberInfo)
extern "C"  bool DefaultContractResolver_ilo_IsIndexedProperty57_m662404086 (Il2CppObject * __this /* static, unused */, MemberInfo_t * ___member0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.Serialization.DefaultContractResolver::ilo_get_Order58(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  Nullable_1_t1237965023  DefaultContractResolver_ilo_get_Order58_m3658100183 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
