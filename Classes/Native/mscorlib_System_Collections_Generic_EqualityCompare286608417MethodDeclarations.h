﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<PointCloudRegognizer/Point>
struct DefaultComparer_t286608417;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_PointCloudRegognizer_Point1838831750.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<PointCloudRegognizer/Point>::.ctor()
extern "C"  void DefaultComparer__ctor_m117480498_gshared (DefaultComparer_t286608417 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m117480498(__this, method) ((  void (*) (DefaultComparer_t286608417 *, const MethodInfo*))DefaultComparer__ctor_m117480498_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<PointCloudRegognizer/Point>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m3640848769_gshared (DefaultComparer_t286608417 * __this, Point_t1838831750  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m3640848769(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t286608417 *, Point_t1838831750 , const MethodInfo*))DefaultComparer_GetHashCode_m3640848769_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<PointCloudRegognizer/Point>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m780158151_gshared (DefaultComparer_t286608417 * __this, Point_t1838831750  ___x0, Point_t1838831750  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m780158151(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t286608417 *, Point_t1838831750 , Point_t1838831750 , const MethodInfo*))DefaultComparer_Equals_m780158151_gshared)(__this, ___x0, ___y1, method)
