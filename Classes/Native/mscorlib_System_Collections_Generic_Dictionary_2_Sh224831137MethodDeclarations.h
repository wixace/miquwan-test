﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>
struct ShimEnumerator_t224831137;
// System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>
struct Dictionary_2_t509053110;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m36487486_gshared (ShimEnumerator_t224831137 * __this, Dictionary_2_t509053110 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m36487486(__this, ___host0, method) ((  void (*) (ShimEnumerator_t224831137 *, Dictionary_2_t509053110 *, const MethodInfo*))ShimEnumerator__ctor_m36487486_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m2244559975_gshared (ShimEnumerator_t224831137 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m2244559975(__this, method) ((  bool (*) (ShimEnumerator_t224831137 *, const MethodInfo*))ShimEnumerator_MoveNext_m2244559975_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m1389139811_gshared (ShimEnumerator_t224831137 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1389139811(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t224831137 *, const MethodInfo*))ShimEnumerator_get_Entry_m1389139811_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m2118860002_gshared (ShimEnumerator_t224831137 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m2118860002(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t224831137 *, const MethodInfo*))ShimEnumerator_get_Key_m2118860002_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1458399988_gshared (ShimEnumerator_t224831137 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1458399988(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t224831137 *, const MethodInfo*))ShimEnumerator_get_Value_m1458399988_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2787740796_gshared (ShimEnumerator_t224831137 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2787740796(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t224831137 *, const MethodInfo*))ShimEnumerator_get_Current_m2787740796_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::Reset()
extern "C"  void ShimEnumerator_Reset_m1831100048_gshared (ShimEnumerator_t224831137 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1831100048(__this, method) ((  void (*) (ShimEnumerator_t224831137 *, const MethodInfo*))ShimEnumerator_Reset_m1831100048_gshared)(__this, method)
