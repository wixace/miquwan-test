﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginReYunGenerated
struct PluginReYunGenerated_t1051998435;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// PluginReYun
struct PluginReYun_t2552860172;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "AssemblyU2DCSharp_PluginReYun2552860172.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Gender2129321697.h"

// System.Void PluginReYunGenerated::.ctor()
extern "C"  void PluginReYunGenerated__ctor_m3503452440 (PluginReYunGenerated_t1051998435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginReYunGenerated::PluginReYun_PluginReYun1(JSVCall,System.Int32)
extern "C"  bool PluginReYunGenerated_PluginReYun_PluginReYun1_m763512776 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginReYunGenerated::PluginReYun_CreatRole__ZEvent(JSVCall,System.Int32)
extern "C"  bool PluginReYunGenerated_PluginReYun_CreatRole__ZEvent_m438003198 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginReYunGenerated::PluginReYun_Game_getDeviceId(JSVCall,System.Int32)
extern "C"  bool PluginReYunGenerated_PluginReYun_Game_getDeviceId_m987795517 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginReYunGenerated::PluginReYun_Game_Init__String__String(JSVCall,System.Int32)
extern "C"  bool PluginReYunGenerated_PluginReYun_Game_Init__String__String_m3125877406 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginReYunGenerated::PluginReYun_Game_Login__String__Gender__String__String__Int32__String(JSVCall,System.Int32)
extern "C"  bool PluginReYunGenerated_PluginReYun_Game_Login__String__Gender__String__String__Int32__String_m3054545166 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginReYunGenerated::PluginReYun_Game_Quest__String__QuestStatus__String(JSVCall,System.Int32)
extern "C"  bool PluginReYunGenerated_PluginReYun_Game_Quest__String__QuestStatus__String_m2457762206 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginReYunGenerated::PluginReYun_Game_Register__String__Gender__String__String__String__String(JSVCall,System.Int32)
extern "C"  bool PluginReYunGenerated_PluginReYun_Game_Register__String__Gender__String__String__String__String_m442209893 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginReYunGenerated::PluginReYun_Game_SetEconomy__String__Int32__Single(JSVCall,System.Int32)
extern "C"  bool PluginReYunGenerated_PluginReYun_Game_SetEconomy__String__Int32__Single_m2599696127 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginReYunGenerated::PluginReYun_Game_SetEvent__String__DictionaryT2_String_String(JSVCall,System.Int32)
extern "C"  bool PluginReYunGenerated_PluginReYun_Game_SetEvent__String__DictionaryT2_String_String_m2375303049 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginReYunGenerated::PluginReYun_Game_setPrintLog__Boolean(JSVCall,System.Int32)
extern "C"  bool PluginReYunGenerated_PluginReYun_Game_setPrintLog__Boolean_m3811795707 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginReYunGenerated::PluginReYun_Init(JSVCall,System.Int32)
extern "C"  bool PluginReYunGenerated_PluginReYun_Init_m2197050643 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginReYunGenerated::PluginReYun_OnDestory(JSVCall,System.Int32)
extern "C"  bool PluginReYunGenerated_PluginReYun_OnDestory_m1642851796 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginReYunGenerated::PluginReYun_Track_getDeviceId(JSVCall,System.Int32)
extern "C"  bool PluginReYunGenerated_PluginReYun_Track_getDeviceId_m890588594 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginReYunGenerated::PluginReYun_Track_Init__String__String(JSVCall,System.Int32)
extern "C"  bool PluginReYunGenerated_PluginReYun_Track_Init__String__String_m3635523785 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginReYunGenerated::PluginReYun_Track_Login__String(JSVCall,System.Int32)
extern "C"  bool PluginReYunGenerated_PluginReYun_Track_Login__String_m2239552965 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginReYunGenerated::PluginReYun_Track_Register__String(JSVCall,System.Int32)
extern "C"  bool PluginReYunGenerated_PluginReYun_Track_Register__String_m3414162891 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginReYunGenerated::PluginReYun_Track_SetEvent__String(JSVCall,System.Int32)
extern "C"  bool PluginReYunGenerated_PluginReYun_Track_SetEvent__String_m3905722304 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginReYunGenerated::PluginReYun_Track_setPrintLog__Boolean(JSVCall,System.Int32)
extern "C"  bool PluginReYunGenerated_PluginReYun_Track_setPrintLog__Boolean_m26474790 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYunGenerated::__Register()
extern "C"  void PluginReYunGenerated___Register_m1151466159 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginReYunGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool PluginReYunGenerated_ilo_attachFinalizerObject1_m3693498311 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYunGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void PluginReYunGenerated_ilo_addJSCSRel2_m2999533653 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PluginReYunGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * PluginReYunGenerated_ilo_getObject3_m2450734937 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYunGenerated::ilo_CreatRole4(PluginReYun,CEvent.ZEvent)
extern "C"  void PluginReYunGenerated_ilo_CreatRole4_m3325232583 (Il2CppObject * __this /* static, unused */, PluginReYun_t2552860172 * ____this0, ZEvent_t3638018500 * ___ev1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginReYunGenerated::ilo_Game_getDeviceId5(PluginReYun)
extern "C"  String_t* PluginReYunGenerated_ilo_Game_getDeviceId5_m533134507 (Il2CppObject * __this /* static, unused */, PluginReYun_t2552860172 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYunGenerated::ilo_setStringS6(System.Int32,System.String)
extern "C"  void PluginReYunGenerated_ilo_setStringS6_m1752272486 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginReYunGenerated::ilo_getStringS7(System.Int32)
extern "C"  String_t* PluginReYunGenerated_ilo_getStringS7_m1143015132 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PluginReYunGenerated::ilo_getInt328(System.Int32)
extern "C"  int32_t PluginReYunGenerated_ilo_getInt328_m2466969638 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PluginReYunGenerated::ilo_getEnum9(System.Int32)
extern "C"  int32_t PluginReYunGenerated_ilo_getEnum9_m2615261056 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYunGenerated::ilo_Game_Register10(PluginReYun,System.String,Gender,System.String,System.String,System.String,System.String)
extern "C"  void PluginReYunGenerated_ilo_Game_Register10_m10740485 (Il2CppObject * __this /* static, unused */, PluginReYun_t2552860172 * ____this0, String_t* ___account1, int32_t ___gender2, String_t* ___age3, String_t* ___serverId4, String_t* ___accountType5, String_t* ___rolename6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PluginReYunGenerated::ilo_getSingle11(System.Int32)
extern "C"  float PluginReYunGenerated_ilo_getSingle11_m19672742 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYunGenerated::ilo_Game_SetEconomy12(PluginReYun,System.String,System.Int32,System.Single)
extern "C"  void PluginReYunGenerated_ilo_Game_SetEconomy12_m3233062695 (Il2CppObject * __this /* static, unused */, PluginReYun_t2552860172 * ____this0, String_t* ___itemName1, int32_t ___itemAmount2, float ___itemTotalPrice3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYunGenerated::ilo_Game_SetEvent13(PluginReYun,System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void PluginReYunGenerated_ilo_Game_SetEvent13_m1165788991 (Il2CppObject * __this /* static, unused */, PluginReYun_t2552860172 * ____this0, String_t* ___eventName1, Dictionary_2_t827649927 * ___dict2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginReYunGenerated::ilo_Track_getDeviceId14(PluginReYun)
extern "C"  String_t* PluginReYunGenerated_ilo_Track_getDeviceId14_m265927952 (Il2CppObject * __this /* static, unused */, PluginReYun_t2552860172 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYunGenerated::ilo_Track_Init15(PluginReYun,System.String,System.String)
extern "C"  void PluginReYunGenerated_ilo_Track_Init15_m3190663063 (Il2CppObject * __this /* static, unused */, PluginReYun_t2552860172 * ____this0, String_t* ___appKey1, String_t* ___channelId2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginReYunGenerated::ilo_Track_Register16(PluginReYun,System.String)
extern "C"  void PluginReYunGenerated_ilo_Track_Register16_m675274311 (Il2CppObject * __this /* static, unused */, PluginReYun_t2552860172 * ____this0, String_t* ___account1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginReYunGenerated::ilo_getBooleanS17(System.Int32)
extern "C"  bool PluginReYunGenerated_ilo_getBooleanS17_m4088758055 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
