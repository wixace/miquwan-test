﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICameraGenerated
struct UICameraGenerated_t831349430;
// JSVCall
struct JSVCall_t3708497963;
// UICamera/GetKeyStateFunc
struct GetKeyStateFunc_t2745734774;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// UICamera/GetAxisFunc
struct GetAxisFunc_t783615269;
// UICamera/OnScreenResize
struct OnScreenResize_t3828578773;
// UICamera/OnCustomInput
struct OnCustomInput_t736305828;
// UICamera/VoidDelegate
struct VoidDelegate_t3135042255;
// UICamera/BoolDelegate
struct BoolDelegate_t2094540325;
// UICamera/FloatDelegate
struct FloatDelegate_t1146521387;
// UICamera/VectorDelegate
struct VectorDelegate_t2747913726;
// UICamera/ObjectDelegate
struct ObjectDelegate_t3703364602;
// UICamera/KeyCodeDelegate
struct KeyCodeDelegate_t4120455995;
// UICamera/MoveDelegate
struct MoveDelegate_t1906135052;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;
// System.Delegate
struct Delegate_t3310234105;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UICamera
struct UICamera_t189364953;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_UICamera189364953.h"

// System.Void UICameraGenerated::.ctor()
extern "C"  void UICameraGenerated__ctor_m2657810293 (UICameraGenerated_t831349430 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated::UICamera_UICamera1(JSVCall,System.Int32)
extern "C"  bool UICameraGenerated_UICamera_UICamera1_m3593841021 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_list(JSVCall)
extern "C"  void UICameraGenerated_UICamera_list_m3531264464 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/GetKeyStateFunc UICameraGenerated::UICamera_GetKeyDown_GetDelegate_member1_arg0(CSRepresentedObject)
extern "C"  GetKeyStateFunc_t2745734774 * UICameraGenerated_UICamera_GetKeyDown_GetDelegate_member1_arg0_m1235298112 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_GetKeyDown(JSVCall)
extern "C"  void UICameraGenerated_UICamera_GetKeyDown_m1224341379 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/GetKeyStateFunc UICameraGenerated::UICamera_GetKeyUp_GetDelegate_member2_arg0(CSRepresentedObject)
extern "C"  GetKeyStateFunc_t2745734774 * UICameraGenerated_UICamera_GetKeyUp_GetDelegate_member2_arg0_m104405050 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_GetKeyUp(JSVCall)
extern "C"  void UICameraGenerated_UICamera_GetKeyUp_m2178335082 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/GetKeyStateFunc UICameraGenerated::UICamera_GetKey_GetDelegate_member3_arg0(CSRepresentedObject)
extern "C"  GetKeyStateFunc_t2745734774 * UICameraGenerated_UICamera_GetKey_GetDelegate_member3_arg0_m1174284160 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_GetKey(JSVCall)
extern "C"  void UICameraGenerated_UICamera_GetKey_m355855749 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/GetAxisFunc UICameraGenerated::UICamera_GetAxis_GetDelegate_member4_arg0(CSRepresentedObject)
extern "C"  GetAxisFunc_t783615269 * UICameraGenerated_UICamera_GetAxis_GetDelegate_member4_arg0_m3580781824 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_GetAxis(JSVCall)
extern "C"  void UICameraGenerated_UICamera_GetAxis_m1037965127 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/OnScreenResize UICameraGenerated::UICamera_onScreenResize_GetDelegate_member5_arg0(CSRepresentedObject)
extern "C"  OnScreenResize_t3828578773 * UICameraGenerated_UICamera_onScreenResize_GetDelegate_member5_arg0_m113407951 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_onScreenResize(JSVCall)
extern "C"  void UICameraGenerated_UICamera_onScreenResize_m1652871631 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_eventType(JSVCall)
extern "C"  void UICameraGenerated_UICamera_eventType_m1684774122 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_eventsGoToColliders(JSVCall)
extern "C"  void UICameraGenerated_UICamera_eventsGoToColliders_m2716844187 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_eventReceiverMask(JSVCall)
extern "C"  void UICameraGenerated_UICamera_eventReceiverMask_m1160679881 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_debug(JSVCall)
extern "C"  void UICameraGenerated_UICamera_debug_m203220587 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_useMouse(JSVCall)
extern "C"  void UICameraGenerated_UICamera_useMouse_m4231258896 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_useTouch(JSVCall)
extern "C"  void UICameraGenerated_UICamera_useTouch_m3084225974 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_allowMultiTouch(JSVCall)
extern "C"  void UICameraGenerated_UICamera_allowMultiTouch_m3053217935 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_useKeyboard(JSVCall)
extern "C"  void UICameraGenerated_UICamera_useKeyboard_m3148719760 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_useController(JSVCall)
extern "C"  void UICameraGenerated_UICamera_useController_m1403359803 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_stickyTooltip(JSVCall)
extern "C"  void UICameraGenerated_UICamera_stickyTooltip_m1959444164 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_tooltipDelay(JSVCall)
extern "C"  void UICameraGenerated_UICamera_tooltipDelay_m1232022894 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_mouseDragThreshold(JSVCall)
extern "C"  void UICameraGenerated_UICamera_mouseDragThreshold_m1338581500 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_mouseClickThreshold(JSVCall)
extern "C"  void UICameraGenerated_UICamera_mouseClickThreshold_m3613801110 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_touchDragThreshold(JSVCall)
extern "C"  void UICameraGenerated_UICamera_touchDragThreshold_m4286346518 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_touchClickThreshold(JSVCall)
extern "C"  void UICameraGenerated_UICamera_touchClickThreshold_m505236156 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_rangeDistance(JSVCall)
extern "C"  void UICameraGenerated_UICamera_rangeDistance_m1418843980 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_scrollAxisName(JSVCall)
extern "C"  void UICameraGenerated_UICamera_scrollAxisName_m1379119893 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_verticalAxisName(JSVCall)
extern "C"  void UICameraGenerated_UICamera_verticalAxisName_m1850986380 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_horizontalAxisName(JSVCall)
extern "C"  void UICameraGenerated_UICamera_horizontalAxisName_m1619636894 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_submitKey0(JSVCall)
extern "C"  void UICameraGenerated_UICamera_submitKey0_m1804350181 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_submitKey1(JSVCall)
extern "C"  void UICameraGenerated_UICamera_submitKey1_m1607836676 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_cancelKey0(JSVCall)
extern "C"  void UICameraGenerated_UICamera_cancelKey0_m3077059523 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_cancelKey1(JSVCall)
extern "C"  void UICameraGenerated_UICamera_cancelKey1_m2880546018 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/OnCustomInput UICameraGenerated::UICamera_onCustomInput_GetDelegate_member29_arg0(CSRepresentedObject)
extern "C"  OnCustomInput_t736305828 * UICameraGenerated_UICamera_onCustomInput_GetDelegate_member29_arg0_m1377380093 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_onCustomInput(JSVCall)
extern "C"  void UICameraGenerated_UICamera_onCustomInput_m3459128612 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_showTooltips(JSVCall)
extern "C"  void UICameraGenerated_UICamera_showTooltips_m1626333281 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_lastTouchPosition(JSVCall)
extern "C"  void UICameraGenerated_UICamera_lastTouchPosition_m1850024204 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_lastWorldPosition(JSVCall)
extern "C"  void UICameraGenerated_UICamera_lastWorldPosition_m1940305177 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_lastHit(JSVCall)
extern "C"  void UICameraGenerated_UICamera_lastHit_m2512154849 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_current(JSVCall)
extern "C"  void UICameraGenerated_UICamera_current_m3044234245 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_currentCamera(JSVCall)
extern "C"  void UICameraGenerated_UICamera_currentCamera_m87549728 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_currentScheme(JSVCall)
extern "C"  void UICameraGenerated_UICamera_currentScheme_m2798208160 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_currentTouchID(JSVCall)
extern "C"  void UICameraGenerated_UICamera_currentTouchID_m1443907309 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_currentKey(JSVCall)
extern "C"  void UICameraGenerated_UICamera_currentKey_m3267457864 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_currentTouch(JSVCall)
extern "C"  void UICameraGenerated_UICamera_currentTouch_m2683429416 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_inputHasFocus(JSVCall)
extern "C"  void UICameraGenerated_UICamera_inputHasFocus_m4221785110 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_fallThrough(JSVCall)
extern "C"  void UICameraGenerated_UICamera_fallThrough_m2850543060 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/VoidDelegate UICameraGenerated::UICamera_onClick_GetDelegate_member42_arg0(CSRepresentedObject)
extern "C"  VoidDelegate_t3135042255 * UICameraGenerated_UICamera_onClick_GetDelegate_member42_arg0_m1115727920 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_onClick(JSVCall)
extern "C"  void UICameraGenerated_UICamera_onClick_m3873281173 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/VoidDelegate UICameraGenerated::UICamera_onDoubleClick_GetDelegate_member43_arg0(CSRepresentedObject)
extern "C"  VoidDelegate_t3135042255 * UICameraGenerated_UICamera_onDoubleClick_GetDelegate_member43_arg0_m1679063586 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_onDoubleClick(JSVCall)
extern "C"  void UICameraGenerated_UICamera_onDoubleClick_m3769961862 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/BoolDelegate UICameraGenerated::UICamera_onHover_GetDelegate_member44_arg0(CSRepresentedObject)
extern "C"  BoolDelegate_t2094540325 * UICameraGenerated_UICamera_onHover_GetDelegate_member44_arg0_m1945491560 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_onHover(JSVCall)
extern "C"  void UICameraGenerated_UICamera_onHover_m1468824417 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/BoolDelegate UICameraGenerated::UICamera_onPress_GetDelegate_member45_arg0(CSRepresentedObject)
extern "C"  BoolDelegate_t2094540325 * UICameraGenerated_UICamera_onPress_GetDelegate_member45_arg0_m230300482 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_onPress(JSVCall)
extern "C"  void UICameraGenerated_UICamera_onPress_m3355646010 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/BoolDelegate UICameraGenerated::UICamera_onSelect_GetDelegate_member46_arg0(CSRepresentedObject)
extern "C"  BoolDelegate_t2094540325 * UICameraGenerated_UICamera_onSelect_GetDelegate_member46_arg0_m135883904 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_onSelect(JSVCall)
extern "C"  void UICameraGenerated_UICamera_onSelect_m1193005907 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/FloatDelegate UICameraGenerated::UICamera_onScroll_GetDelegate_member47_arg0(CSRepresentedObject)
extern "C"  FloatDelegate_t1146521387 * UICameraGenerated_UICamera_onScroll_GetDelegate_member47_arg0_m3183535852 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_onScroll(JSVCall)
extern "C"  void UICameraGenerated_UICamera_onScroll_m18980002 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/VectorDelegate UICameraGenerated::UICamera_onDrag_GetDelegate_member48_arg0(CSRepresentedObject)
extern "C"  VectorDelegate_t2747913726 * UICameraGenerated_UICamera_onDrag_GetDelegate_member48_arg0_m3000535011 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_onDrag(JSVCall)
extern "C"  void UICameraGenerated_UICamera_onDrag_m51173467 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/VoidDelegate UICameraGenerated::UICamera_onDragStart_GetDelegate_member49_arg0(CSRepresentedObject)
extern "C"  VoidDelegate_t3135042255 * UICameraGenerated_UICamera_onDragStart_GetDelegate_member49_arg0_m2161447217 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_onDragStart(JSVCall)
extern "C"  void UICameraGenerated_UICamera_onDragStart_m370398671 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/ObjectDelegate UICameraGenerated::UICamera_onDragOver_GetDelegate_member50_arg0(CSRepresentedObject)
extern "C"  ObjectDelegate_t3703364602 * UICameraGenerated_UICamera_onDragOver_GetDelegate_member50_arg0_m1204646594 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_onDragOver(JSVCall)
extern "C"  void UICameraGenerated_UICamera_onDragOver_m396461927 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/ObjectDelegate UICameraGenerated::UICamera_onDragOut_GetDelegate_member51_arg0(CSRepresentedObject)
extern "C"  ObjectDelegate_t3703364602 * UICameraGenerated_UICamera_onDragOut_GetDelegate_member51_arg0_m3904202449 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_onDragOut(JSVCall)
extern "C"  void UICameraGenerated_UICamera_onDragOut_m3327333155 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/VoidDelegate UICameraGenerated::UICamera_onDragEnd_GetDelegate_member52_arg0(CSRepresentedObject)
extern "C"  VoidDelegate_t3135042255 * UICameraGenerated_UICamera_onDragEnd_GetDelegate_member52_arg0_m164403568 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_onDragEnd(JSVCall)
extern "C"  void UICameraGenerated_UICamera_onDragEnd_m579512374 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/ObjectDelegate UICameraGenerated::UICamera_onDrop_GetDelegate_member53_arg0(CSRepresentedObject)
extern "C"  ObjectDelegate_t3703364602 * UICameraGenerated_UICamera_onDrop_GetDelegate_member53_arg0_m3122165598 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_onDrop(JSVCall)
extern "C"  void UICameraGenerated_UICamera_onDrop_m3190003968 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/KeyCodeDelegate UICameraGenerated::UICamera_onKey_GetDelegate_member54_arg0(CSRepresentedObject)
extern "C"  KeyCodeDelegate_t4120455995 * UICameraGenerated_UICamera_onKey_GetDelegate_member54_arg0_m1150116888 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_onKey(JSVCall)
extern "C"  void UICameraGenerated_UICamera_onKey_m1361622878 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/BoolDelegate UICameraGenerated::UICamera_onTooltip_GetDelegate_member55_arg0(CSRepresentedObject)
extern "C"  BoolDelegate_t2094540325 * UICameraGenerated_UICamera_onTooltip_GetDelegate_member55_arg0_m1319356833 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_onTooltip(JSVCall)
extern "C"  void UICameraGenerated_UICamera_onTooltip_m626891450 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/MoveDelegate UICameraGenerated::UICamera_onMouseMove_GetDelegate_member56_arg0(CSRepresentedObject)
extern "C"  MoveDelegate_t1906135052 * UICameraGenerated_UICamera_onMouseMove_GetDelegate_member56_arg0_m4150251752 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_onMouseMove(JSVCall)
extern "C"  void UICameraGenerated_UICamera_onMouseMove_m3716408391 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_controller(JSVCall)
extern "C"  void UICameraGenerated_UICamera_controller_m4120944274 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_isDragging(JSVCall)
extern "C"  void UICameraGenerated_UICamera_isDragging_m3517073973 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_hoveredObject(JSVCall)
extern "C"  void UICameraGenerated_UICamera_hoveredObject_m1209768612 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_currentRay(JSVCall)
extern "C"  void UICameraGenerated_UICamera_currentRay_m2768907741 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_cachedCamera(JSVCall)
extern "C"  void UICameraGenerated_UICamera_cachedCamera_m4118007207 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_isOverUI(JSVCall)
extern "C"  void UICameraGenerated_UICamera_isOverUI_m350167228 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_selectedObject(JSVCall)
extern "C"  void UICameraGenerated_UICamera_selectedObject_m1037913652 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_touchCount(JSVCall)
extern "C"  void UICameraGenerated_UICamera_touchCount_m3097146270 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_dragCount(JSVCall)
extern "C"  void UICameraGenerated_UICamera_dragCount_m1443275139 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_mainCamera(JSVCall)
extern "C"  void UICameraGenerated_UICamera_mainCamera_m2542539312 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::UICamera_eventHandler(JSVCall)
extern "C"  void UICameraGenerated_UICamera_eventHandler_m3492382558 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated::UICamera_ProcessMouse(JSVCall,System.Int32)
extern "C"  bool UICameraGenerated_UICamera_ProcessMouse_m1480213939 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated::UICamera_ProcessOthers(JSVCall,System.Int32)
extern "C"  bool UICameraGenerated_UICamera_ProcessOthers_m1296076311 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated::UICamera_ProcessTouch__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UICameraGenerated_UICamera_ProcessTouch__Boolean__Boolean_m2920091341 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated::UICamera_ProcessTouches(JSVCall,System.Int32)
extern "C"  bool UICameraGenerated_UICamera_ProcessTouches_m2370949115 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated::UICamera_ShowTooltip__Boolean(JSVCall,System.Int32)
extern "C"  bool UICameraGenerated_UICamera_ShowTooltip__Boolean_m1138391519 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated::UICamera_FindCameraForLayer__Int32(JSVCall,System.Int32)
extern "C"  bool UICameraGenerated_UICamera_FindCameraForLayer__Int32_m2885864013 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated::UICamera_GetMouse__Int32(JSVCall,System.Int32)
extern "C"  bool UICameraGenerated_UICamera_GetMouse__Int32_m1256596324 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated::UICamera_GetTouch__Int32(JSVCall,System.Int32)
extern "C"  bool UICameraGenerated_UICamera_GetTouch__Int32_m2856189706 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated::UICamera_IsHighlighted__GameObject(JSVCall,System.Int32)
extern "C"  bool UICameraGenerated_UICamera_IsHighlighted__GameObject_m2257302655 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated::UICamera_IsPressed__GameObject(JSVCall,System.Int32)
extern "C"  bool UICameraGenerated_UICamera_IsPressed__GameObject_m4009584014 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated::UICamera_Notify__GameObject__String__Object(JSVCall,System.Int32)
extern "C"  bool UICameraGenerated_UICamera_Notify__GameObject__String__Object_m3561275431 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated::UICamera_Raycast__MouseOrTouch(JSVCall,System.Int32)
extern "C"  bool UICameraGenerated_UICamera_Raycast__MouseOrTouch_m2753238597 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated::UICamera_Raycast__Vector3(JSVCall,System.Int32)
extern "C"  bool UICameraGenerated_UICamera_Raycast__Vector3_m3312259204 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated::UICamera_RemoveTouch__Int32(JSVCall,System.Int32)
extern "C"  bool UICameraGenerated_UICamera_RemoveTouch__Int32_m3811931792 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::__Register()
extern "C"  void UICameraGenerated___Register_m2116019826 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/GetKeyStateFunc UICameraGenerated::<UICamera_GetKeyDown>m__FF()
extern "C"  GetKeyStateFunc_t2745734774 * UICameraGenerated_U3CUICamera_GetKeyDownU3Em__FF_m586453484 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/GetKeyStateFunc UICameraGenerated::<UICamera_GetKeyUp>m__101()
extern "C"  GetKeyStateFunc_t2745734774 * UICameraGenerated_U3CUICamera_GetKeyUpU3Em__101_m614987599 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/GetKeyStateFunc UICameraGenerated::<UICamera_GetKey>m__103()
extern "C"  GetKeyStateFunc_t2745734774 * UICameraGenerated_U3CUICamera_GetKeyU3Em__103_m543090860 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/GetAxisFunc UICameraGenerated::<UICamera_GetAxis>m__105()
extern "C"  GetAxisFunc_t783615269 * UICameraGenerated_U3CUICamera_GetAxisU3Em__105_m90437105 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/OnScreenResize UICameraGenerated::<UICamera_onScreenResize>m__107()
extern "C"  OnScreenResize_t3828578773 * UICameraGenerated_U3CUICamera_onScreenResizeU3Em__107_m4118112337 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/OnCustomInput UICameraGenerated::<UICamera_onCustomInput>m__109()
extern "C"  OnCustomInput_t736305828 * UICameraGenerated_U3CUICamera_onCustomInputU3Em__109_m1498578449 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/VoidDelegate UICameraGenerated::<UICamera_onClick>m__10B()
extern "C"  VoidDelegate_t3135042255 * UICameraGenerated_U3CUICamera_onClickU3Em__10B_m167885664 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/VoidDelegate UICameraGenerated::<UICamera_onDoubleClick>m__10D()
extern "C"  VoidDelegate_t3135042255 * UICameraGenerated_U3CUICamera_onDoubleClickU3Em__10D_m3398352595 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/BoolDelegate UICameraGenerated::<UICamera_onHover>m__10F()
extern "C"  BoolDelegate_t2094540325 * UICameraGenerated_U3CUICamera_onHoverU3Em__10F_m3390275078 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/BoolDelegate UICameraGenerated::<UICamera_onPress>m__111()
extern "C"  BoolDelegate_t2094540325 * UICameraGenerated_U3CUICamera_onPressU3Em__111_m982138985 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/BoolDelegate UICameraGenerated::<UICamera_onSelect>m__113()
extern "C"  BoolDelegate_t2094540325 * UICameraGenerated_U3CUICamera_onSelectU3Em__113_m3277638176 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/FloatDelegate UICameraGenerated::<UICamera_onScroll>m__115()
extern "C"  FloatDelegate_t1146521387 * UICameraGenerated_U3CUICamera_onScrollU3Em__115_m2039825877 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/VectorDelegate UICameraGenerated::<UICamera_onDrag>m__117()
extern "C"  VectorDelegate_t2747913726 * UICameraGenerated_U3CUICamera_onDragU3Em__117_m1840731059 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/VoidDelegate UICameraGenerated::<UICamera_onDragStart>m__119()
extern "C"  VoidDelegate_t3135042255 * UICameraGenerated_U3CUICamera_onDragStartU3Em__119_m4152201520 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/ObjectDelegate UICameraGenerated::<UICamera_onDragOver>m__11B()
extern "C"  ObjectDelegate_t3703364602 * UICameraGenerated_U3CUICamera_onDragOverU3Em__11B_m1035545358 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/ObjectDelegate UICameraGenerated::<UICamera_onDragOut>m__11D()
extern "C"  ObjectDelegate_t3703364602 * UICameraGenerated_U3CUICamera_onDragOutU3Em__11D_m2708328634 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/VoidDelegate UICameraGenerated::<UICamera_onDragEnd>m__11F()
extern "C"  VoidDelegate_t3135042255 * UICameraGenerated_U3CUICamera_onDragEndU3Em__11F_m3395818340 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/ObjectDelegate UICameraGenerated::<UICamera_onDrop>m__121()
extern "C"  ObjectDelegate_t3703364602 * UICameraGenerated_U3CUICamera_onDropU3Em__121_m1177482677 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/KeyCodeDelegate UICameraGenerated::<UICamera_onKey>m__123()
extern "C"  KeyCodeDelegate_t4120455995 * UICameraGenerated_U3CUICamera_onKeyU3Em__123_m1409394906 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/BoolDelegate UICameraGenerated::<UICamera_onTooltip>m__125()
extern "C"  BoolDelegate_t2094540325 * UICameraGenerated_U3CUICamera_onTooltipU3Em__125_m3474860492 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/MoveDelegate UICameraGenerated::<UICamera_onMouseMove>m__127()
extern "C"  MoveDelegate_t1906135052 * UICameraGenerated_U3CUICamera_onMouseMoveU3Em__127_m3886235778 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICameraGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UICameraGenerated_ilo_getObject1_m1898090849 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UICameraGenerated_ilo_attachFinalizerObject2_m3661240419 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UICameraGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UICameraGenerated_ilo_getObject3_m254085650 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICameraGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UICameraGenerated_ilo_setObject4_m3570981788 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::ilo_setEnum5(System.Int32,System.Int32)
extern "C"  void UICameraGenerated_ilo_setEnum5_m2920418962 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated::ilo_getBooleanS6(System.Int32)
extern "C"  bool UICameraGenerated_ilo_getBooleanS6_m4106398228 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::ilo_setBooleanS7(System.Int32,System.Boolean)
extern "C"  void UICameraGenerated_ilo_setBooleanS7_m2672173024 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::ilo_setSingle8(System.Int32,System.Single)
extern "C"  void UICameraGenerated_ilo_setSingle8_m2071250022 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UICameraGenerated::ilo_getSingle9(System.Int32)
extern "C"  float UICameraGenerated_ilo_getSingle9_m3645741250 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::ilo_setStringS10(System.Int32,System.String)
extern "C"  void UICameraGenerated_ilo_setStringS10_m4089367950 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UICameraGenerated::ilo_getStringS11(System.Int32)
extern "C"  String_t* UICameraGenerated_ilo_getStringS11_m2934717916 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICameraGenerated::ilo_getEnum12(System.Int32)
extern "C"  int32_t UICameraGenerated_ilo_getEnum12_m3747065175 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::ilo_setVector2S13(System.Int32,UnityEngine.Vector2)
extern "C"  void UICameraGenerated_ilo_setVector2S13_m1997712237 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UICameraGenerated::ilo_getVector2S14(System.Int32)
extern "C"  Vector2_t4282066565  UICameraGenerated_ilo_getVector2S14_m3094357577 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::ilo_setVector3S15(System.Int32,UnityEngine.Vector3)
extern "C"  void UICameraGenerated_ilo_setVector3S15_m2786510765 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::ilo_addJSFunCSDelegateRel16(System.Int32,System.Delegate)
extern "C"  void UICameraGenerated_ilo_addJSFunCSDelegateRel16_m1920820301 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Ray UICameraGenerated::ilo_get_currentRay17()
extern "C"  Ray_t3134616544  UICameraGenerated_ilo_get_currentRay17_m4169000386 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UICameraGenerated::ilo_get_selectedObject18()
extern "C"  GameObject_t3674682005 * UICameraGenerated_ilo_get_selectedObject18_m2476542487 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::ilo_set_selectedObject19(UnityEngine.GameObject)
extern "C"  void UICameraGenerated_ilo_set_selectedObject19_m2333238937 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICameraGenerated::ilo_get_touchCount20()
extern "C"  int32_t UICameraGenerated_ilo_get_touchCount20_m2491557899 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::ilo_ProcessOthers21(UICamera)
extern "C"  void UICameraGenerated_ilo_ProcessOthers21_m2855900088 (Il2CppObject * __this /* static, unused */, UICamera_t189364953 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated::ilo_ShowTooltip22(UICamera,System.Boolean)
extern "C"  void UICameraGenerated_ilo_ShowTooltip22_m1560038416 (Il2CppObject * __this /* static, unused */, UICamera_t189364953 * ____this0, bool ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UICameraGenerated::ilo_getInt3223(System.Int32)
extern "C"  int32_t UICameraGenerated_ilo_getInt3223_m3236210462 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera UICameraGenerated::ilo_FindCameraForLayer24(System.Int32)
extern "C"  UICamera_t189364953 * UICameraGenerated_ilo_FindCameraForLayer24_m3934212375 (Il2CppObject * __this /* static, unused */, int32_t ___layer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UICameraGenerated::ilo_getWhatever25(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UICameraGenerated_ilo_getWhatever25_m2401844407 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated::ilo_Raycast26(UnityEngine.Vector3)
extern "C"  bool UICameraGenerated_ilo_Raycast26_m1393500888 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___inPos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject UICameraGenerated::ilo_getFunctionS27(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * UICameraGenerated_ilo_getFunctionS27_m1751539776 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICameraGenerated::ilo_isFunctionS28(System.Int32)
extern "C"  bool UICameraGenerated_ilo_isFunctionS28_m299178332 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/GetKeyStateFunc UICameraGenerated::ilo_UICamera_GetKeyUp_GetDelegate_member2_arg029(CSRepresentedObject)
extern "C"  GetKeyStateFunc_t2745734774 * UICameraGenerated_ilo_UICamera_GetKeyUp_GetDelegate_member2_arg029_m1200319552 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/VoidDelegate UICameraGenerated::ilo_UICamera_onClick_GetDelegate_member42_arg030(CSRepresentedObject)
extern "C"  VoidDelegate_t3135042255 * UICameraGenerated_ilo_UICamera_onClick_GetDelegate_member42_arg030_m1317464544 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/ObjectDelegate UICameraGenerated::ilo_UICamera_onDrop_GetDelegate_member53_arg031(CSRepresentedObject)
extern "C"  ObjectDelegate_t3703364602 * UICameraGenerated_ilo_UICamera_onDrop_GetDelegate_member53_arg031_m2875477683 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera/KeyCodeDelegate UICameraGenerated::ilo_UICamera_onKey_GetDelegate_member54_arg032(CSRepresentedObject)
extern "C"  KeyCodeDelegate_t4120455995 * UICameraGenerated_ilo_UICamera_onKey_GetDelegate_member54_arg032_m1385806150 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
