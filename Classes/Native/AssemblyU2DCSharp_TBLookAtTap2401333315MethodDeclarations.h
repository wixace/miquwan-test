﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TBLookAtTap
struct TBLookAtTap_t2401333315;
// TapGesture
struct TapGesture_t659145798;
// Gesture
struct Gesture_t1589572905;
// TBDragView
struct TBDragView_t862864007;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_TapGesture659145798.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_Gesture1589572905.h"
#include "AssemblyU2DCSharp_TBDragView862864007.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void TBLookAtTap::.ctor()
extern "C"  void TBLookAtTap__ctor_m1239459464 (TBLookAtTap_t2401333315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBLookAtTap::Awake()
extern "C"  void TBLookAtTap_Awake_m1477064683 (TBLookAtTap_t2401333315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBLookAtTap::Start()
extern "C"  void TBLookAtTap_Start_m186597256 (TBLookAtTap_t2401333315 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBLookAtTap::OnTap(TapGesture)
extern "C"  void TBLookAtTap_OnTap_m1697385252 (TBLookAtTap_t2401333315 * __this, TapGesture_t659145798 * ___gesture0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 TBLookAtTap::ilo_get_Position1(Gesture)
extern "C"  Vector2_t4282066565  TBLookAtTap_ilo_get_Position1_m4090165788 (Il2CppObject * __this /* static, unused */, Gesture_t1589572905 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TBLookAtTap::ilo_LookAt2(TBDragView,UnityEngine.Vector3)
extern "C"  void TBLookAtTap_ilo_LookAt2_m729940927 (Il2CppObject * __this /* static, unused */, TBDragView_t862864007 * ____this0, Vector3_t4282066566  ___pos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
