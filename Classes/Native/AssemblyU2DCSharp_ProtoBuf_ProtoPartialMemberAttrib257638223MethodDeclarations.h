﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.ProtoPartialMemberAttribute
struct ProtoPartialMemberAttribute_t257638223;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void ProtoBuf.ProtoPartialMemberAttribute::.ctor(System.Int32,System.String)
extern "C"  void ProtoPartialMemberAttribute__ctor_m4053890 (ProtoPartialMemberAttribute_t257638223 * __this, int32_t ___tag0, String_t* ___memberName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.ProtoPartialMemberAttribute::get_MemberName()
extern "C"  String_t* ProtoPartialMemberAttribute_get_MemberName_m4263266400 (ProtoPartialMemberAttribute_t257638223 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
