﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.IPathModifier>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m252896482(__this, ___l0, method) ((  void (*) (Enumerator_t2476581657 *, List_1_t2456908887 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.IPathModifier>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1328884272(__this, method) ((  void (*) (Enumerator_t2476581657 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.IPathModifier>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2233638620(__this, method) ((  Il2CppObject * (*) (Enumerator_t2476581657 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.IPathModifier>::Dispose()
#define Enumerator_Dispose_m2772781895(__this, method) ((  void (*) (Enumerator_t2476581657 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.IPathModifier>::VerifyState()
#define Enumerator_VerifyState_m147066624(__this, method) ((  void (*) (Enumerator_t2476581657 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.IPathModifier>::MoveNext()
#define Enumerator_MoveNext_m2879862044(__this, method) ((  bool (*) (Enumerator_t2476581657 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.IPathModifier>::get_Current()
#define Enumerator_get_Current_m1970249847(__this, method) ((  Il2CppObject * (*) (Enumerator_t2476581657 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
