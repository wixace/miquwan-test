﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraHelper/<MoveCamera>c__AnonStorey14D
struct U3CMoveCameraU3Ec__AnonStorey14D_t2872051618;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraHelper/<MoveCamera>c__AnonStorey14D::.ctor()
extern "C"  void U3CMoveCameraU3Ec__AnonStorey14D__ctor_m1187963913 (U3CMoveCameraU3Ec__AnonStorey14D_t2872051618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraHelper/<MoveCamera>c__AnonStorey14D::<>m__3BE()
extern "C"  void U3CMoveCameraU3Ec__AnonStorey14D_U3CU3Em__3BE_m2934586822 (U3CMoveCameraU3Ec__AnonStorey14D_t2872051618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
