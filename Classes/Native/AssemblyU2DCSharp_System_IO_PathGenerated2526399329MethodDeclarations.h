﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_IO_PathGenerated
struct System_IO_PathGenerated_t2526399329;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"

// System.Void System_IO_PathGenerated::.ctor()
extern "C"  void System_IO_PathGenerated__ctor_m2208354602 (System_IO_PathGenerated_t2526399329 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_PathGenerated::Path_AltDirectorySeparatorChar(JSVCall)
extern "C"  void System_IO_PathGenerated_Path_AltDirectorySeparatorChar_m3262591920 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_PathGenerated::Path_DirectorySeparatorChar(JSVCall)
extern "C"  void System_IO_PathGenerated_Path_DirectorySeparatorChar_m2799743671 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_PathGenerated::Path_PathSeparator(JSVCall)
extern "C"  void System_IO_PathGenerated_Path_PathSeparator_m3816690311 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_PathGenerated::Path_VolumeSeparatorChar(JSVCall)
extern "C"  void System_IO_PathGenerated_Path_VolumeSeparatorChar_m2067015750 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_PathGenerated::Path_ChangeExtension__String__String(JSVCall,System.Int32)
extern "C"  bool System_IO_PathGenerated_Path_ChangeExtension__String__String_m1867261261 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_PathGenerated::Path_Combine__String__String(JSVCall,System.Int32)
extern "C"  bool System_IO_PathGenerated_Path_Combine__String__String_m1408750365 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_PathGenerated::Path_GetDirectoryName__String(JSVCall,System.Int32)
extern "C"  bool System_IO_PathGenerated_Path_GetDirectoryName__String_m388975801 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_PathGenerated::Path_GetExtension__String(JSVCall,System.Int32)
extern "C"  bool System_IO_PathGenerated_Path_GetExtension__String_m1518924704 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_PathGenerated::Path_GetFileName__String(JSVCall,System.Int32)
extern "C"  bool System_IO_PathGenerated_Path_GetFileName__String_m1760821546 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_PathGenerated::Path_GetFileNameWithoutExtension__String(JSVCall,System.Int32)
extern "C"  bool System_IO_PathGenerated_Path_GetFileNameWithoutExtension__String_m479225697 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_PathGenerated::Path_GetFullPath__String(JSVCall,System.Int32)
extern "C"  bool System_IO_PathGenerated_Path_GetFullPath__String_m2534066583 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_PathGenerated::Path_GetInvalidFileNameChars(JSVCall,System.Int32)
extern "C"  bool System_IO_PathGenerated_Path_GetInvalidFileNameChars_m3815703409 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_PathGenerated::Path_GetInvalidPathChars(JSVCall,System.Int32)
extern "C"  bool System_IO_PathGenerated_Path_GetInvalidPathChars_m3654720435 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_PathGenerated::Path_GetPathRoot__String(JSVCall,System.Int32)
extern "C"  bool System_IO_PathGenerated_Path_GetPathRoot__String_m2411927978 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_PathGenerated::Path_GetRandomFileName(JSVCall,System.Int32)
extern "C"  bool System_IO_PathGenerated_Path_GetRandomFileName_m1769797820 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_PathGenerated::Path_GetTempFileName(JSVCall,System.Int32)
extern "C"  bool System_IO_PathGenerated_Path_GetTempFileName_m2898180173 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_PathGenerated::Path_GetTempPath(JSVCall,System.Int32)
extern "C"  bool System_IO_PathGenerated_Path_GetTempPath_m3448706635 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_PathGenerated::Path_HasExtension__String(JSVCall,System.Int32)
extern "C"  bool System_IO_PathGenerated_Path_HasExtension__String_m3629091804 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_PathGenerated::Path_IsPathRooted__String(JSVCall,System.Int32)
extern "C"  bool System_IO_PathGenerated_Path_IsPathRooted__String_m2076559911 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_PathGenerated::__Register()
extern "C"  void System_IO_PathGenerated___Register_m310826461 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_PathGenerated::ilo_setChar1(System.Int32,System.Char)
extern "C"  void System_IO_PathGenerated_ilo_setChar1_m2457344686 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Il2CppChar ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System_IO_PathGenerated::ilo_getStringS2(System.Int32)
extern "C"  String_t* System_IO_PathGenerated_ilo_getStringS2_m3755465487 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_PathGenerated::ilo_setStringS3(System.Int32,System.String)
extern "C"  void System_IO_PathGenerated_ilo_setStringS3_m560799733 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
