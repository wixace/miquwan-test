﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._ff8ba0cebdfe44bdb5336cb8b08ba789
struct _ff8ba0cebdfe44bdb5336cb8b08ba789_t2917261756;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__ff8ba0cebdfe44bdb5336cb82917261756.h"

// System.Void Little._ff8ba0cebdfe44bdb5336cb8b08ba789::.ctor()
extern "C"  void _ff8ba0cebdfe44bdb5336cb8b08ba789__ctor_m224229361 (_ff8ba0cebdfe44bdb5336cb8b08ba789_t2917261756 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._ff8ba0cebdfe44bdb5336cb8b08ba789::_ff8ba0cebdfe44bdb5336cb8b08ba789m2(System.Int32)
extern "C"  int32_t _ff8ba0cebdfe44bdb5336cb8b08ba789__ff8ba0cebdfe44bdb5336cb8b08ba789m2_m3900430361 (_ff8ba0cebdfe44bdb5336cb8b08ba789_t2917261756 * __this, int32_t ____ff8ba0cebdfe44bdb5336cb8b08ba789a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._ff8ba0cebdfe44bdb5336cb8b08ba789::_ff8ba0cebdfe44bdb5336cb8b08ba789m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _ff8ba0cebdfe44bdb5336cb8b08ba789__ff8ba0cebdfe44bdb5336cb8b08ba789m_m3627884221 (_ff8ba0cebdfe44bdb5336cb8b08ba789_t2917261756 * __this, int32_t ____ff8ba0cebdfe44bdb5336cb8b08ba789a0, int32_t ____ff8ba0cebdfe44bdb5336cb8b08ba789411, int32_t ____ff8ba0cebdfe44bdb5336cb8b08ba789c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._ff8ba0cebdfe44bdb5336cb8b08ba789::ilo__ff8ba0cebdfe44bdb5336cb8b08ba789m21(Little._ff8ba0cebdfe44bdb5336cb8b08ba789,System.Int32)
extern "C"  int32_t _ff8ba0cebdfe44bdb5336cb8b08ba789_ilo__ff8ba0cebdfe44bdb5336cb8b08ba789m21_m3691639555 (Il2CppObject * __this /* static, unused */, _ff8ba0cebdfe44bdb5336cb8b08ba789_t2917261756 * ____this0, int32_t ____ff8ba0cebdfe44bdb5336cb8b08ba789a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
