﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_caicemnir124
struct  M_caicemnir124_t4200694444  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_caicemnir124::_keanawlearNivis
	String_t* ____keanawlearNivis_0;
	// System.Single GarbageiOS.M_caicemnir124::_gawelxere
	float ____gawelxere_1;
	// System.Single GarbageiOS.M_caicemnir124::_roliDeacezay
	float ____roliDeacezay_2;
	// System.UInt32 GarbageiOS.M_caicemnir124::_nisjirmowLearbaju
	uint32_t ____nisjirmowLearbaju_3;
	// System.UInt32 GarbageiOS.M_caicemnir124::_whopar
	uint32_t ____whopar_4;
	// System.UInt32 GarbageiOS.M_caicemnir124::_stewhor
	uint32_t ____stewhor_5;
	// System.String GarbageiOS.M_caicemnir124::_merejaso
	String_t* ____merejaso_6;
	// System.Int32 GarbageiOS.M_caicemnir124::_jelarur
	int32_t ____jelarur_7;
	// System.String GarbageiOS.M_caicemnir124::_wasrobirSidrir
	String_t* ____wasrobirSidrir_8;
	// System.UInt32 GarbageiOS.M_caicemnir124::_cairreaGegelxel
	uint32_t ____cairreaGegelxel_9;
	// System.Boolean GarbageiOS.M_caicemnir124::_deetar
	bool ____deetar_10;
	// System.Boolean GarbageiOS.M_caicemnir124::_xowsairsemDalwair
	bool ____xowsairsemDalwair_11;
	// System.String GarbageiOS.M_caicemnir124::_tercouwooSejairne
	String_t* ____tercouwooSejairne_12;
	// System.String GarbageiOS.M_caicemnir124::_kirpas
	String_t* ____kirpas_13;
	// System.Boolean GarbageiOS.M_caicemnir124::_keceacer
	bool ____keceacer_14;
	// System.UInt32 GarbageiOS.M_caicemnir124::_keregelnouNufall
	uint32_t ____keregelnouNufall_15;
	// System.Boolean GarbageiOS.M_caicemnir124::_weadirpurRumawhere
	bool ____weadirpurRumawhere_16;
	// System.UInt32 GarbageiOS.M_caicemnir124::_bawtobe
	uint32_t ____bawtobe_17;
	// System.Boolean GarbageiOS.M_caicemnir124::_cembi
	bool ____cembi_18;
	// System.Boolean GarbageiOS.M_caicemnir124::_merkem
	bool ____merkem_19;
	// System.Boolean GarbageiOS.M_caicemnir124::_capar
	bool ____capar_20;
	// System.Int32 GarbageiOS.M_caicemnir124::_xorru
	int32_t ____xorru_21;
	// System.Boolean GarbageiOS.M_caicemnir124::_salwhisa
	bool ____salwhisa_22;
	// System.Boolean GarbageiOS.M_caicemnir124::_sanou
	bool ____sanou_23;
	// System.String GarbageiOS.M_caicemnir124::_merdedeMemnu
	String_t* ____merdedeMemnu_24;
	// System.Boolean GarbageiOS.M_caicemnir124::_paygear
	bool ____paygear_25;
	// System.UInt32 GarbageiOS.M_caicemnir124::_hedardee
	uint32_t ____hedardee_26;
	// System.Int32 GarbageiOS.M_caicemnir124::_lawcarai
	int32_t ____lawcarai_27;
	// System.String GarbageiOS.M_caicemnir124::_jevelTairkallsa
	String_t* ____jevelTairkallsa_28;
	// System.Boolean GarbageiOS.M_caicemnir124::_sawhoJeawheebu
	bool ____sawhoJeawheebu_29;
	// System.String GarbageiOS.M_caicemnir124::_winiLecoowhe
	String_t* ____winiLecoowhe_30;
	// System.Int32 GarbageiOS.M_caicemnir124::_sehelri
	int32_t ____sehelri_31;
	// System.UInt32 GarbageiOS.M_caicemnir124::_gimaJaltu
	uint32_t ____gimaJaltu_32;
	// System.String GarbageiOS.M_caicemnir124::_cemxemMuqo
	String_t* ____cemxemMuqo_33;
	// System.Single GarbageiOS.M_caicemnir124::_lorti
	float ____lorti_34;
	// System.Int32 GarbageiOS.M_caicemnir124::_doreTadra
	int32_t ____doreTadra_35;
	// System.String GarbageiOS.M_caicemnir124::_testooyas
	String_t* ____testooyas_36;
	// System.Single GarbageiOS.M_caicemnir124::_joustasurSterenem
	float ____joustasurSterenem_37;
	// System.UInt32 GarbageiOS.M_caicemnir124::_waiba
	uint32_t ____waiba_38;
	// System.Single GarbageiOS.M_caicemnir124::_taizel
	float ____taizel_39;

public:
	inline static int32_t get_offset_of__keanawlearNivis_0() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____keanawlearNivis_0)); }
	inline String_t* get__keanawlearNivis_0() const { return ____keanawlearNivis_0; }
	inline String_t** get_address_of__keanawlearNivis_0() { return &____keanawlearNivis_0; }
	inline void set__keanawlearNivis_0(String_t* value)
	{
		____keanawlearNivis_0 = value;
		Il2CppCodeGenWriteBarrier(&____keanawlearNivis_0, value);
	}

	inline static int32_t get_offset_of__gawelxere_1() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____gawelxere_1)); }
	inline float get__gawelxere_1() const { return ____gawelxere_1; }
	inline float* get_address_of__gawelxere_1() { return &____gawelxere_1; }
	inline void set__gawelxere_1(float value)
	{
		____gawelxere_1 = value;
	}

	inline static int32_t get_offset_of__roliDeacezay_2() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____roliDeacezay_2)); }
	inline float get__roliDeacezay_2() const { return ____roliDeacezay_2; }
	inline float* get_address_of__roliDeacezay_2() { return &____roliDeacezay_2; }
	inline void set__roliDeacezay_2(float value)
	{
		____roliDeacezay_2 = value;
	}

	inline static int32_t get_offset_of__nisjirmowLearbaju_3() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____nisjirmowLearbaju_3)); }
	inline uint32_t get__nisjirmowLearbaju_3() const { return ____nisjirmowLearbaju_3; }
	inline uint32_t* get_address_of__nisjirmowLearbaju_3() { return &____nisjirmowLearbaju_3; }
	inline void set__nisjirmowLearbaju_3(uint32_t value)
	{
		____nisjirmowLearbaju_3 = value;
	}

	inline static int32_t get_offset_of__whopar_4() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____whopar_4)); }
	inline uint32_t get__whopar_4() const { return ____whopar_4; }
	inline uint32_t* get_address_of__whopar_4() { return &____whopar_4; }
	inline void set__whopar_4(uint32_t value)
	{
		____whopar_4 = value;
	}

	inline static int32_t get_offset_of__stewhor_5() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____stewhor_5)); }
	inline uint32_t get__stewhor_5() const { return ____stewhor_5; }
	inline uint32_t* get_address_of__stewhor_5() { return &____stewhor_5; }
	inline void set__stewhor_5(uint32_t value)
	{
		____stewhor_5 = value;
	}

	inline static int32_t get_offset_of__merejaso_6() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____merejaso_6)); }
	inline String_t* get__merejaso_6() const { return ____merejaso_6; }
	inline String_t** get_address_of__merejaso_6() { return &____merejaso_6; }
	inline void set__merejaso_6(String_t* value)
	{
		____merejaso_6 = value;
		Il2CppCodeGenWriteBarrier(&____merejaso_6, value);
	}

	inline static int32_t get_offset_of__jelarur_7() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____jelarur_7)); }
	inline int32_t get__jelarur_7() const { return ____jelarur_7; }
	inline int32_t* get_address_of__jelarur_7() { return &____jelarur_7; }
	inline void set__jelarur_7(int32_t value)
	{
		____jelarur_7 = value;
	}

	inline static int32_t get_offset_of__wasrobirSidrir_8() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____wasrobirSidrir_8)); }
	inline String_t* get__wasrobirSidrir_8() const { return ____wasrobirSidrir_8; }
	inline String_t** get_address_of__wasrobirSidrir_8() { return &____wasrobirSidrir_8; }
	inline void set__wasrobirSidrir_8(String_t* value)
	{
		____wasrobirSidrir_8 = value;
		Il2CppCodeGenWriteBarrier(&____wasrobirSidrir_8, value);
	}

	inline static int32_t get_offset_of__cairreaGegelxel_9() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____cairreaGegelxel_9)); }
	inline uint32_t get__cairreaGegelxel_9() const { return ____cairreaGegelxel_9; }
	inline uint32_t* get_address_of__cairreaGegelxel_9() { return &____cairreaGegelxel_9; }
	inline void set__cairreaGegelxel_9(uint32_t value)
	{
		____cairreaGegelxel_9 = value;
	}

	inline static int32_t get_offset_of__deetar_10() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____deetar_10)); }
	inline bool get__deetar_10() const { return ____deetar_10; }
	inline bool* get_address_of__deetar_10() { return &____deetar_10; }
	inline void set__deetar_10(bool value)
	{
		____deetar_10 = value;
	}

	inline static int32_t get_offset_of__xowsairsemDalwair_11() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____xowsairsemDalwair_11)); }
	inline bool get__xowsairsemDalwair_11() const { return ____xowsairsemDalwair_11; }
	inline bool* get_address_of__xowsairsemDalwair_11() { return &____xowsairsemDalwair_11; }
	inline void set__xowsairsemDalwair_11(bool value)
	{
		____xowsairsemDalwair_11 = value;
	}

	inline static int32_t get_offset_of__tercouwooSejairne_12() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____tercouwooSejairne_12)); }
	inline String_t* get__tercouwooSejairne_12() const { return ____tercouwooSejairne_12; }
	inline String_t** get_address_of__tercouwooSejairne_12() { return &____tercouwooSejairne_12; }
	inline void set__tercouwooSejairne_12(String_t* value)
	{
		____tercouwooSejairne_12 = value;
		Il2CppCodeGenWriteBarrier(&____tercouwooSejairne_12, value);
	}

	inline static int32_t get_offset_of__kirpas_13() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____kirpas_13)); }
	inline String_t* get__kirpas_13() const { return ____kirpas_13; }
	inline String_t** get_address_of__kirpas_13() { return &____kirpas_13; }
	inline void set__kirpas_13(String_t* value)
	{
		____kirpas_13 = value;
		Il2CppCodeGenWriteBarrier(&____kirpas_13, value);
	}

	inline static int32_t get_offset_of__keceacer_14() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____keceacer_14)); }
	inline bool get__keceacer_14() const { return ____keceacer_14; }
	inline bool* get_address_of__keceacer_14() { return &____keceacer_14; }
	inline void set__keceacer_14(bool value)
	{
		____keceacer_14 = value;
	}

	inline static int32_t get_offset_of__keregelnouNufall_15() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____keregelnouNufall_15)); }
	inline uint32_t get__keregelnouNufall_15() const { return ____keregelnouNufall_15; }
	inline uint32_t* get_address_of__keregelnouNufall_15() { return &____keregelnouNufall_15; }
	inline void set__keregelnouNufall_15(uint32_t value)
	{
		____keregelnouNufall_15 = value;
	}

	inline static int32_t get_offset_of__weadirpurRumawhere_16() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____weadirpurRumawhere_16)); }
	inline bool get__weadirpurRumawhere_16() const { return ____weadirpurRumawhere_16; }
	inline bool* get_address_of__weadirpurRumawhere_16() { return &____weadirpurRumawhere_16; }
	inline void set__weadirpurRumawhere_16(bool value)
	{
		____weadirpurRumawhere_16 = value;
	}

	inline static int32_t get_offset_of__bawtobe_17() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____bawtobe_17)); }
	inline uint32_t get__bawtobe_17() const { return ____bawtobe_17; }
	inline uint32_t* get_address_of__bawtobe_17() { return &____bawtobe_17; }
	inline void set__bawtobe_17(uint32_t value)
	{
		____bawtobe_17 = value;
	}

	inline static int32_t get_offset_of__cembi_18() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____cembi_18)); }
	inline bool get__cembi_18() const { return ____cembi_18; }
	inline bool* get_address_of__cembi_18() { return &____cembi_18; }
	inline void set__cembi_18(bool value)
	{
		____cembi_18 = value;
	}

	inline static int32_t get_offset_of__merkem_19() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____merkem_19)); }
	inline bool get__merkem_19() const { return ____merkem_19; }
	inline bool* get_address_of__merkem_19() { return &____merkem_19; }
	inline void set__merkem_19(bool value)
	{
		____merkem_19 = value;
	}

	inline static int32_t get_offset_of__capar_20() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____capar_20)); }
	inline bool get__capar_20() const { return ____capar_20; }
	inline bool* get_address_of__capar_20() { return &____capar_20; }
	inline void set__capar_20(bool value)
	{
		____capar_20 = value;
	}

	inline static int32_t get_offset_of__xorru_21() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____xorru_21)); }
	inline int32_t get__xorru_21() const { return ____xorru_21; }
	inline int32_t* get_address_of__xorru_21() { return &____xorru_21; }
	inline void set__xorru_21(int32_t value)
	{
		____xorru_21 = value;
	}

	inline static int32_t get_offset_of__salwhisa_22() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____salwhisa_22)); }
	inline bool get__salwhisa_22() const { return ____salwhisa_22; }
	inline bool* get_address_of__salwhisa_22() { return &____salwhisa_22; }
	inline void set__salwhisa_22(bool value)
	{
		____salwhisa_22 = value;
	}

	inline static int32_t get_offset_of__sanou_23() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____sanou_23)); }
	inline bool get__sanou_23() const { return ____sanou_23; }
	inline bool* get_address_of__sanou_23() { return &____sanou_23; }
	inline void set__sanou_23(bool value)
	{
		____sanou_23 = value;
	}

	inline static int32_t get_offset_of__merdedeMemnu_24() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____merdedeMemnu_24)); }
	inline String_t* get__merdedeMemnu_24() const { return ____merdedeMemnu_24; }
	inline String_t** get_address_of__merdedeMemnu_24() { return &____merdedeMemnu_24; }
	inline void set__merdedeMemnu_24(String_t* value)
	{
		____merdedeMemnu_24 = value;
		Il2CppCodeGenWriteBarrier(&____merdedeMemnu_24, value);
	}

	inline static int32_t get_offset_of__paygear_25() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____paygear_25)); }
	inline bool get__paygear_25() const { return ____paygear_25; }
	inline bool* get_address_of__paygear_25() { return &____paygear_25; }
	inline void set__paygear_25(bool value)
	{
		____paygear_25 = value;
	}

	inline static int32_t get_offset_of__hedardee_26() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____hedardee_26)); }
	inline uint32_t get__hedardee_26() const { return ____hedardee_26; }
	inline uint32_t* get_address_of__hedardee_26() { return &____hedardee_26; }
	inline void set__hedardee_26(uint32_t value)
	{
		____hedardee_26 = value;
	}

	inline static int32_t get_offset_of__lawcarai_27() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____lawcarai_27)); }
	inline int32_t get__lawcarai_27() const { return ____lawcarai_27; }
	inline int32_t* get_address_of__lawcarai_27() { return &____lawcarai_27; }
	inline void set__lawcarai_27(int32_t value)
	{
		____lawcarai_27 = value;
	}

	inline static int32_t get_offset_of__jevelTairkallsa_28() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____jevelTairkallsa_28)); }
	inline String_t* get__jevelTairkallsa_28() const { return ____jevelTairkallsa_28; }
	inline String_t** get_address_of__jevelTairkallsa_28() { return &____jevelTairkallsa_28; }
	inline void set__jevelTairkallsa_28(String_t* value)
	{
		____jevelTairkallsa_28 = value;
		Il2CppCodeGenWriteBarrier(&____jevelTairkallsa_28, value);
	}

	inline static int32_t get_offset_of__sawhoJeawheebu_29() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____sawhoJeawheebu_29)); }
	inline bool get__sawhoJeawheebu_29() const { return ____sawhoJeawheebu_29; }
	inline bool* get_address_of__sawhoJeawheebu_29() { return &____sawhoJeawheebu_29; }
	inline void set__sawhoJeawheebu_29(bool value)
	{
		____sawhoJeawheebu_29 = value;
	}

	inline static int32_t get_offset_of__winiLecoowhe_30() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____winiLecoowhe_30)); }
	inline String_t* get__winiLecoowhe_30() const { return ____winiLecoowhe_30; }
	inline String_t** get_address_of__winiLecoowhe_30() { return &____winiLecoowhe_30; }
	inline void set__winiLecoowhe_30(String_t* value)
	{
		____winiLecoowhe_30 = value;
		Il2CppCodeGenWriteBarrier(&____winiLecoowhe_30, value);
	}

	inline static int32_t get_offset_of__sehelri_31() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____sehelri_31)); }
	inline int32_t get__sehelri_31() const { return ____sehelri_31; }
	inline int32_t* get_address_of__sehelri_31() { return &____sehelri_31; }
	inline void set__sehelri_31(int32_t value)
	{
		____sehelri_31 = value;
	}

	inline static int32_t get_offset_of__gimaJaltu_32() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____gimaJaltu_32)); }
	inline uint32_t get__gimaJaltu_32() const { return ____gimaJaltu_32; }
	inline uint32_t* get_address_of__gimaJaltu_32() { return &____gimaJaltu_32; }
	inline void set__gimaJaltu_32(uint32_t value)
	{
		____gimaJaltu_32 = value;
	}

	inline static int32_t get_offset_of__cemxemMuqo_33() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____cemxemMuqo_33)); }
	inline String_t* get__cemxemMuqo_33() const { return ____cemxemMuqo_33; }
	inline String_t** get_address_of__cemxemMuqo_33() { return &____cemxemMuqo_33; }
	inline void set__cemxemMuqo_33(String_t* value)
	{
		____cemxemMuqo_33 = value;
		Il2CppCodeGenWriteBarrier(&____cemxemMuqo_33, value);
	}

	inline static int32_t get_offset_of__lorti_34() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____lorti_34)); }
	inline float get__lorti_34() const { return ____lorti_34; }
	inline float* get_address_of__lorti_34() { return &____lorti_34; }
	inline void set__lorti_34(float value)
	{
		____lorti_34 = value;
	}

	inline static int32_t get_offset_of__doreTadra_35() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____doreTadra_35)); }
	inline int32_t get__doreTadra_35() const { return ____doreTadra_35; }
	inline int32_t* get_address_of__doreTadra_35() { return &____doreTadra_35; }
	inline void set__doreTadra_35(int32_t value)
	{
		____doreTadra_35 = value;
	}

	inline static int32_t get_offset_of__testooyas_36() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____testooyas_36)); }
	inline String_t* get__testooyas_36() const { return ____testooyas_36; }
	inline String_t** get_address_of__testooyas_36() { return &____testooyas_36; }
	inline void set__testooyas_36(String_t* value)
	{
		____testooyas_36 = value;
		Il2CppCodeGenWriteBarrier(&____testooyas_36, value);
	}

	inline static int32_t get_offset_of__joustasurSterenem_37() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____joustasurSterenem_37)); }
	inline float get__joustasurSterenem_37() const { return ____joustasurSterenem_37; }
	inline float* get_address_of__joustasurSterenem_37() { return &____joustasurSterenem_37; }
	inline void set__joustasurSterenem_37(float value)
	{
		____joustasurSterenem_37 = value;
	}

	inline static int32_t get_offset_of__waiba_38() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____waiba_38)); }
	inline uint32_t get__waiba_38() const { return ____waiba_38; }
	inline uint32_t* get_address_of__waiba_38() { return &____waiba_38; }
	inline void set__waiba_38(uint32_t value)
	{
		____waiba_38 = value;
	}

	inline static int32_t get_offset_of__taizel_39() { return static_cast<int32_t>(offsetof(M_caicemnir124_t4200694444, ____taizel_39)); }
	inline float get__taizel_39() const { return ____taizel_39; }
	inline float* get_address_of__taizel_39() { return &____taizel_39; }
	inline void set__taizel_39(float value)
	{
		____taizel_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
