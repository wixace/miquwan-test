﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<ProductsCfgMgr/ProductInfo>
struct DefaultComparer_t4048735201;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr_ProductInfo1305991238.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<ProductsCfgMgr/ProductInfo>::.ctor()
extern "C"  void DefaultComparer__ctor_m2670816242_gshared (DefaultComparer_t4048735201 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2670816242(__this, method) ((  void (*) (DefaultComparer_t4048735201 *, const MethodInfo*))DefaultComparer__ctor_m2670816242_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<ProductsCfgMgr/ProductInfo>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m2801592769_gshared (DefaultComparer_t4048735201 * __this, ProductInfo_t1305991238  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m2801592769(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t4048735201 *, ProductInfo_t1305991238 , const MethodInfo*))DefaultComparer_GetHashCode_m2801592769_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<ProductsCfgMgr/ProductInfo>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m2655204487_gshared (DefaultComparer_t4048735201 * __this, ProductInfo_t1305991238  ___x0, ProductInfo_t1305991238  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m2655204487(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t4048735201 *, ProductInfo_t1305991238 , ProductInfo_t1305991238 , const MethodInfo*))DefaultComparer_Equals_m2655204487_gshared)(__this, ___x0, ___y1, method)
