﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadEvent
struct  LoadEvent_t3369598260  : public ZEvent_t3638018500
{
public:
	// System.Int32 LoadEvent::rate
	int32_t ___rate_6;

public:
	inline static int32_t get_offset_of_rate_6() { return static_cast<int32_t>(offsetof(LoadEvent_t3369598260, ___rate_6)); }
	inline int32_t get_rate_6() const { return ___rate_6; }
	inline int32_t* get_address_of_rate_6() { return &___rate_6; }
	inline void set_rate_6(int32_t value)
	{
		___rate_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
