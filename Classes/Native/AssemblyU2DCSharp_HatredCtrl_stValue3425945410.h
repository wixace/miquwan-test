﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CombatEntity
struct CombatEntity_t684137495;

#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HatredCtrl/stValue
struct  stValue_t3425945410 
{
public:
	// CombatEntity HatredCtrl/stValue::target
	CombatEntity_t684137495 * ___target_0;
	// System.Single HatredCtrl/stValue::hatred
	float ___hatred_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(stValue_t3425945410, ___target_0)); }
	inline CombatEntity_t684137495 * get_target_0() const { return ___target_0; }
	inline CombatEntity_t684137495 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(CombatEntity_t684137495 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier(&___target_0, value);
	}

	inline static int32_t get_offset_of_hatred_1() { return static_cast<int32_t>(offsetof(stValue_t3425945410, ___hatred_1)); }
	inline float get_hatred_1() const { return ___hatred_1; }
	inline float* get_address_of_hatred_1() { return &___hatred_1; }
	inline void set_hatred_1(float value)
	{
		___hatred_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: HatredCtrl/stValue
struct stValue_t3425945410_marshaled_pinvoke
{
	CombatEntity_t684137495 * ___target_0;
	float ___hatred_1;
};
// Native definition for marshalling of: HatredCtrl/stValue
struct stValue_t3425945410_marshaled_com
{
	CombatEntity_t684137495 * ___target_0;
	float ___hatred_1;
};
