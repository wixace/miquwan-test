﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GizmosGenerated
struct UnityEngine_GizmosGenerated_t3665165038;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_GizmosGenerated::.ctor()
extern "C"  void UnityEngine_GizmosGenerated__ctor_m1860377661 (UnityEngine_GizmosGenerated_t3665165038 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_Gizmos1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_Gizmos1_m1392399925 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GizmosGenerated::Gizmos_color(JSVCall)
extern "C"  void UnityEngine_GizmosGenerated_Gizmos_color_m3784794403 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GizmosGenerated::Gizmos_matrix(JSVCall)
extern "C"  void UnityEngine_GizmosGenerated_Gizmos_matrix_m1206415173 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawCube__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawCube__Vector3__Vector3_m422098462 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawFrustum__Vector3__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawFrustum__Vector3__Single__Single__Single__Single_m585324983 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawGUITexture__Rect__Texture__Int32__Int32__Int32__Int32__Material(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawGUITexture__Rect__Texture__Int32__Int32__Int32__Int32__Material_m838958711 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawGUITexture__Rect__Texture__Int32__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawGUITexture__Rect__Texture__Int32__Int32__Int32__Int32_m1014330192 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawGUITexture__Rect__Texture__Material(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawGUITexture__Rect__Texture__Material_m3058813559 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawGUITexture__Rect__Texture(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawGUITexture__Rect__Texture_m242532176 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawIcon__Vector3__String__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawIcon__Vector3__String__Boolean_m2419640841 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawIcon__Vector3__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawIcon__Vector3__String_m1223740481 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawLine__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawLine__Vector3__Vector3_m2368059101 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawMesh__Mesh__Int32__Vector3__Quaternion__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawMesh__Mesh__Int32__Vector3__Quaternion__Vector3_m374992015 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawMesh__Mesh__Vector3__Quaternion__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawMesh__Mesh__Vector3__Quaternion__Vector3_m2698222373 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawMesh__Mesh__Int32__Vector3__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawMesh__Mesh__Int32__Vector3__Quaternion_m4090959139 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawMesh__Mesh__Int32__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawMesh__Mesh__Int32__Vector3_m1030050437 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawMesh__Mesh__Vector3__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawMesh__Mesh__Vector3__Quaternion_m1505789389 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawMesh__Mesh__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawMesh__Mesh__Int32_m568394349 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawMesh__Mesh__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawMesh__Mesh__Vector3_m46330415 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawMesh__Mesh(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawMesh__Mesh_m591608707 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawRay__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawRay__Vector3__Vector3_m1461454435 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawRay__Ray(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawRay__Ray_m818820393 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawSphere__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawSphere__Vector3__Single_m1921233252 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawWireCube__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawWireCube__Vector3__Vector3_m2042665795 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawWireMesh__Mesh__Int32__Vector3__Quaternion__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawWireMesh__Mesh__Int32__Vector3__Quaternion__Vector3_m1768336138 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawWireMesh__Mesh__Vector3__Quaternion__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawWireMesh__Mesh__Vector3__Quaternion__Vector3_m3434897674 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawWireMesh__Mesh__Int32__Vector3__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawWireMesh__Mesh__Int32__Vector3__Quaternion_m3801223240 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawWireMesh__Mesh__Vector3__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawWireMesh__Mesh__Vector3__Quaternion_m3017251400 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawWireMesh__Mesh__Int32__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawWireMesh__Mesh__Int32__Vector3_m395058474 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawWireMesh__Mesh__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawWireMesh__Mesh__Vector3_m3444614954 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawWireMesh__Mesh__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawWireMesh__Mesh__Int32_m817740328 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawWireMesh__Mesh(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawWireMesh__Mesh_m4090884136 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::Gizmos_DrawWireSphere__Vector3__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_Gizmos_DrawWireSphere__Vector3__Single_m619213023 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GizmosGenerated::__Register()
extern "C"  void UnityEngine_GizmosGenerated___Register_m4234641066 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GizmosGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_GizmosGenerated_ilo_getObject1_m2174198553 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_GizmosGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_GizmosGenerated_ilo_getObject2_m107196041 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GizmosGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_GizmosGenerated_ilo_setObject3_m576644883 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_GizmosGenerated::ilo_getVector3S4(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_GizmosGenerated_ilo_getVector3S4_m1774293356 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_GizmosGenerated::ilo_getSingle5(System.Int32)
extern "C"  float UnityEngine_GizmosGenerated_ilo_getSingle5_m2655990 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GizmosGenerated::ilo_getInt326(System.Int32)
extern "C"  int32_t UnityEngine_GizmosGenerated_ilo_getInt326_m3021354661 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_GizmosGenerated::ilo_getStringS7(System.Int32)
extern "C"  String_t* UnityEngine_GizmosGenerated_ilo_getStringS7_m958246823 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GizmosGenerated::ilo_getBooleanS8(System.Int32)
extern "C"  bool UnityEngine_GizmosGenerated_ilo_getBooleanS8_m2308188622 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
