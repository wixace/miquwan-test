﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V4272688975MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,Pathfinding.Ionic.Zip.ZipEntry>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m1949482047(__this, ___host0, method) ((  void (*) (Enumerator_t1539126751 *, Dictionary_2_t3607293343 *, const MethodInfo*))Enumerator__ctor_m76754913_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,Pathfinding.Ionic.Zip.ZipEntry>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3303253580(__this, method) ((  Il2CppObject * (*) (Enumerator_t1539126751 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3118196448_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,Pathfinding.Ionic.Zip.ZipEntry>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3501378326(__this, method) ((  void (*) (Enumerator_t1539126751 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3702199860_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,Pathfinding.Ionic.Zip.ZipEntry>::Dispose()
#define Enumerator_Dispose_m1787306145(__this, method) ((  void (*) (Enumerator_t1539126751 *, const MethodInfo*))Enumerator_Dispose_m1628348611_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,Pathfinding.Ionic.Zip.ZipEntry>::MoveNext()
#define Enumerator_MoveNext_m2429373916(__this, method) ((  bool (*) (Enumerator_t1539126751 *, const MethodInfo*))Enumerator_MoveNext_m3556422944_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,Pathfinding.Ionic.Zip.ZipEntry>::get_Current()
#define Enumerator_get_Current_m1542202753(__this, method) ((  ZipEntry_t2786874973 * (*) (Enumerator_t1539126751 *, const MethodInfo*))Enumerator_get_Current_m841474402_gshared)(__this, method)
