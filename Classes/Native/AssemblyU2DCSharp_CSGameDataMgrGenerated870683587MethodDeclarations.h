﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSGameDataMgrGenerated
struct CSGameDataMgrGenerated_t870683587;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void CSGameDataMgrGenerated::.ctor()
extern "C"  void CSGameDataMgrGenerated__ctor_m1016933432 (CSGameDataMgrGenerated_t870683587 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSGameDataMgrGenerated::CSGameDataMgr_CSGameDataMgr1(JSVCall,System.Int32)
extern "C"  bool CSGameDataMgrGenerated_CSGameDataMgr_CSGameDataMgr1_m2347149608 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGameDataMgrGenerated::CSGameDataMgr_levelData(JSVCall)
extern "C"  void CSGameDataMgrGenerated_CSGameDataMgr_levelData_m3105323286 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGameDataMgrGenerated::CSGameDataMgr_playerData(JSVCall)
extern "C"  void CSGameDataMgrGenerated_CSGameDataMgr_playerData_m623352413 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGameDataMgrGenerated::CSGameDataMgr_heroData(JSVCall)
extern "C"  void CSGameDataMgrGenerated_CSGameDataMgr_heroData_m3094640228 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGameDataMgrGenerated::CSGameDataMgr_robData(JSVCall)
extern "C"  void CSGameDataMgrGenerated_CSGameDataMgr_robData_m709229557 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGameDataMgrGenerated::CSGameDataMgr_storyData(JSVCall)
extern "C"  void CSGameDataMgrGenerated_CSGameDataMgr_storyData_m2960341829 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGameDataMgrGenerated::CSGameDataMgr_kruunuData(JSVCall)
extern "C"  void CSGameDataMgrGenerated_CSGameDataMgr_kruunuData_m1368193040 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGameDataMgrGenerated::CSGameDataMgr_guideData(JSVCall)
extern "C"  void CSGameDataMgrGenerated_CSGameDataMgr_guideData_m4052204702 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGameDataMgrGenerated::CSGameDataMgr_bossData(JSVCall)
extern "C"  void CSGameDataMgrGenerated_CSGameDataMgr_bossData_m2239659473 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGameDataMgrGenerated::CSGameDataMgr_shiliBossData(JSVCall)
extern "C"  void CSGameDataMgrGenerated_CSGameDataMgr_shiliBossData_m3481974684 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGameDataMgrGenerated::CSGameDataMgr_fightforData(JSVCall)
extern "C"  void CSGameDataMgrGenerated_CSGameDataMgr_fightforData_m688220421 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGameDataMgrGenerated::CSGameDataMgr_dailyData(JSVCall)
extern "C"  void CSGameDataMgrGenerated_CSGameDataMgr_dailyData_m3401306273 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGameDataMgrGenerated::CSGameDataMgr_campfightData(JSVCall)
extern "C"  void CSGameDataMgrGenerated_CSGameDataMgr_campfightData_m3644697931 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGameDataMgrGenerated::CSGameDataMgr_provingGroundData(JSVCall)
extern "C"  void CSGameDataMgrGenerated_CSGameDataMgr_provingGroundData_m3205605722 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGameDataMgrGenerated::CSGameDataMgr_arenaInterServiceData(JSVCall)
extern "C"  void CSGameDataMgrGenerated_CSGameDataMgr_arenaInterServiceData_m1248222778 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGameDataMgrGenerated::__Register()
extern "C"  void CSGameDataMgrGenerated___Register_m4159648655 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSGameDataMgrGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t CSGameDataMgrGenerated_ilo_getObject1_m152068954 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGameDataMgrGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void CSGameDataMgrGenerated_ilo_addJSCSRel2_m2383211381 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CSGameDataMgrGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * CSGameDataMgrGenerated_ilo_getObject3_m3543041785 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSGameDataMgrGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t CSGameDataMgrGenerated_ilo_setObject4_m1608085 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
