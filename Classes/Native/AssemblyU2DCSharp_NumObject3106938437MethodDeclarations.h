﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NumObject
struct NumObject_t3106938437;

#include "codegen/il2cpp-codegen.h"

// System.Void NumObject::.ctor()
extern "C"  void NumObject__ctor_m2365076678 (NumObject_t3106938437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NumObject::Reset()
extern "C"  void NumObject_Reset_m11509619 (NumObject_t3106938437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NumObject::Cleanup()
extern "C"  void NumObject_Cleanup_m3469748936 (NumObject_t3106938437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
