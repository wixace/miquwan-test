﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.SDK.Sdkwww/<RequestExe>c__Iterator38
struct U3CRequestExeU3Ec__Iterator38_t3508389199;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void Mihua.SDK.Sdkwww/<RequestExe>c__Iterator38::.ctor()
extern "C"  void U3CRequestExeU3Ec__Iterator38__ctor_m637467692 (U3CRequestExeU3Ec__Iterator38_t3508389199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mihua.SDK.Sdkwww/<RequestExe>c__Iterator38::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CRequestExeU3Ec__Iterator38_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4237439728 (U3CRequestExeU3Ec__Iterator38_t3508389199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Mihua.SDK.Sdkwww/<RequestExe>c__Iterator38::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CRequestExeU3Ec__Iterator38_System_Collections_IEnumerator_get_Current_m4108850820 (U3CRequestExeU3Ec__Iterator38_t3508389199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Mihua.SDK.Sdkwww/<RequestExe>c__Iterator38::MoveNext()
extern "C"  bool U3CRequestExeU3Ec__Iterator38_MoveNext_m1683200304 (U3CRequestExeU3Ec__Iterator38_t3508389199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.SDK.Sdkwww/<RequestExe>c__Iterator38::Dispose()
extern "C"  void U3CRequestExeU3Ec__Iterator38_Dispose_m2619379049 (U3CRequestExeU3Ec__Iterator38_t3508389199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mihua.SDK.Sdkwww/<RequestExe>c__Iterator38::Reset()
extern "C"  void U3CRequestExeU3Ec__Iterator38_Reset_m2578867929 (U3CRequestExeU3Ec__Iterator38_t3508389199 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
