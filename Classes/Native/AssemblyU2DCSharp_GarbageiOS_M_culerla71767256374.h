﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_culerla71
struct  M_culerla71_t767256374  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_culerla71::_xudearxo
	bool ____xudearxo_0;
	// System.String GarbageiOS.M_culerla71::_yuyalNearcallme
	String_t* ____yuyalNearcallme_1;
	// System.Boolean GarbageiOS.M_culerla71::_cousa
	bool ____cousa_2;
	// System.UInt32 GarbageiOS.M_culerla71::_heseseeBerekirnai
	uint32_t ____heseseeBerekirnai_3;
	// System.Single GarbageiOS.M_culerla71::_beasaNibe
	float ____beasaNibe_4;
	// System.Single GarbageiOS.M_culerla71::_rawserMutallhe
	float ____rawserMutallhe_5;
	// System.UInt32 GarbageiOS.M_culerla71::_teltirNissa
	uint32_t ____teltirNissa_6;
	// System.Int32 GarbageiOS.M_culerla71::_joocee
	int32_t ____joocee_7;
	// System.Int32 GarbageiOS.M_culerla71::_rersal
	int32_t ____rersal_8;
	// System.Boolean GarbageiOS.M_culerla71::_coutrasjur
	bool ____coutrasjur_9;
	// System.Boolean GarbageiOS.M_culerla71::_qaldapou
	bool ____qaldapou_10;
	// System.Boolean GarbageiOS.M_culerla71::_qaslallNispai
	bool ____qaslallNispai_11;
	// System.String GarbageiOS.M_culerla71::_wezubem
	String_t* ____wezubem_12;
	// System.Boolean GarbageiOS.M_culerla71::_jairjoo
	bool ____jairjoo_13;

public:
	inline static int32_t get_offset_of__xudearxo_0() { return static_cast<int32_t>(offsetof(M_culerla71_t767256374, ____xudearxo_0)); }
	inline bool get__xudearxo_0() const { return ____xudearxo_0; }
	inline bool* get_address_of__xudearxo_0() { return &____xudearxo_0; }
	inline void set__xudearxo_0(bool value)
	{
		____xudearxo_0 = value;
	}

	inline static int32_t get_offset_of__yuyalNearcallme_1() { return static_cast<int32_t>(offsetof(M_culerla71_t767256374, ____yuyalNearcallme_1)); }
	inline String_t* get__yuyalNearcallme_1() const { return ____yuyalNearcallme_1; }
	inline String_t** get_address_of__yuyalNearcallme_1() { return &____yuyalNearcallme_1; }
	inline void set__yuyalNearcallme_1(String_t* value)
	{
		____yuyalNearcallme_1 = value;
		Il2CppCodeGenWriteBarrier(&____yuyalNearcallme_1, value);
	}

	inline static int32_t get_offset_of__cousa_2() { return static_cast<int32_t>(offsetof(M_culerla71_t767256374, ____cousa_2)); }
	inline bool get__cousa_2() const { return ____cousa_2; }
	inline bool* get_address_of__cousa_2() { return &____cousa_2; }
	inline void set__cousa_2(bool value)
	{
		____cousa_2 = value;
	}

	inline static int32_t get_offset_of__heseseeBerekirnai_3() { return static_cast<int32_t>(offsetof(M_culerla71_t767256374, ____heseseeBerekirnai_3)); }
	inline uint32_t get__heseseeBerekirnai_3() const { return ____heseseeBerekirnai_3; }
	inline uint32_t* get_address_of__heseseeBerekirnai_3() { return &____heseseeBerekirnai_3; }
	inline void set__heseseeBerekirnai_3(uint32_t value)
	{
		____heseseeBerekirnai_3 = value;
	}

	inline static int32_t get_offset_of__beasaNibe_4() { return static_cast<int32_t>(offsetof(M_culerla71_t767256374, ____beasaNibe_4)); }
	inline float get__beasaNibe_4() const { return ____beasaNibe_4; }
	inline float* get_address_of__beasaNibe_4() { return &____beasaNibe_4; }
	inline void set__beasaNibe_4(float value)
	{
		____beasaNibe_4 = value;
	}

	inline static int32_t get_offset_of__rawserMutallhe_5() { return static_cast<int32_t>(offsetof(M_culerla71_t767256374, ____rawserMutallhe_5)); }
	inline float get__rawserMutallhe_5() const { return ____rawserMutallhe_5; }
	inline float* get_address_of__rawserMutallhe_5() { return &____rawserMutallhe_5; }
	inline void set__rawserMutallhe_5(float value)
	{
		____rawserMutallhe_5 = value;
	}

	inline static int32_t get_offset_of__teltirNissa_6() { return static_cast<int32_t>(offsetof(M_culerla71_t767256374, ____teltirNissa_6)); }
	inline uint32_t get__teltirNissa_6() const { return ____teltirNissa_6; }
	inline uint32_t* get_address_of__teltirNissa_6() { return &____teltirNissa_6; }
	inline void set__teltirNissa_6(uint32_t value)
	{
		____teltirNissa_6 = value;
	}

	inline static int32_t get_offset_of__joocee_7() { return static_cast<int32_t>(offsetof(M_culerla71_t767256374, ____joocee_7)); }
	inline int32_t get__joocee_7() const { return ____joocee_7; }
	inline int32_t* get_address_of__joocee_7() { return &____joocee_7; }
	inline void set__joocee_7(int32_t value)
	{
		____joocee_7 = value;
	}

	inline static int32_t get_offset_of__rersal_8() { return static_cast<int32_t>(offsetof(M_culerla71_t767256374, ____rersal_8)); }
	inline int32_t get__rersal_8() const { return ____rersal_8; }
	inline int32_t* get_address_of__rersal_8() { return &____rersal_8; }
	inline void set__rersal_8(int32_t value)
	{
		____rersal_8 = value;
	}

	inline static int32_t get_offset_of__coutrasjur_9() { return static_cast<int32_t>(offsetof(M_culerla71_t767256374, ____coutrasjur_9)); }
	inline bool get__coutrasjur_9() const { return ____coutrasjur_9; }
	inline bool* get_address_of__coutrasjur_9() { return &____coutrasjur_9; }
	inline void set__coutrasjur_9(bool value)
	{
		____coutrasjur_9 = value;
	}

	inline static int32_t get_offset_of__qaldapou_10() { return static_cast<int32_t>(offsetof(M_culerla71_t767256374, ____qaldapou_10)); }
	inline bool get__qaldapou_10() const { return ____qaldapou_10; }
	inline bool* get_address_of__qaldapou_10() { return &____qaldapou_10; }
	inline void set__qaldapou_10(bool value)
	{
		____qaldapou_10 = value;
	}

	inline static int32_t get_offset_of__qaslallNispai_11() { return static_cast<int32_t>(offsetof(M_culerla71_t767256374, ____qaslallNispai_11)); }
	inline bool get__qaslallNispai_11() const { return ____qaslallNispai_11; }
	inline bool* get_address_of__qaslallNispai_11() { return &____qaslallNispai_11; }
	inline void set__qaslallNispai_11(bool value)
	{
		____qaslallNispai_11 = value;
	}

	inline static int32_t get_offset_of__wezubem_12() { return static_cast<int32_t>(offsetof(M_culerla71_t767256374, ____wezubem_12)); }
	inline String_t* get__wezubem_12() const { return ____wezubem_12; }
	inline String_t** get_address_of__wezubem_12() { return &____wezubem_12; }
	inline void set__wezubem_12(String_t* value)
	{
		____wezubem_12 = value;
		Il2CppCodeGenWriteBarrier(&____wezubem_12, value);
	}

	inline static int32_t get_offset_of__jairjoo_13() { return static_cast<int32_t>(offsetof(M_culerla71_t767256374, ____jairjoo_13)); }
	inline bool get__jairjoo_13() const { return ____jairjoo_13; }
	inline bool* get_address_of__jairjoo_13() { return &____jairjoo_13; }
	inline void set__jairjoo_13(bool value)
	{
		____jairjoo_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
