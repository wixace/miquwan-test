﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_tremperGerege187
struct  M_tremperGerege187_t3246597050  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_tremperGerege187::_kojor
	int32_t ____kojor_0;
	// System.UInt32 GarbageiOS.M_tremperGerege187::_retray
	uint32_t ____retray_1;
	// System.Boolean GarbageiOS.M_tremperGerege187::_rairhemTairche
	bool ____rairhemTairche_2;
	// System.String GarbageiOS.M_tremperGerege187::_jairse
	String_t* ____jairse_3;
	// System.UInt32 GarbageiOS.M_tremperGerege187::_telwoucel
	uint32_t ____telwoucel_4;
	// System.Single GarbageiOS.M_tremperGerege187::_deenelLenestu
	float ____deenelLenestu_5;
	// System.Int32 GarbageiOS.M_tremperGerege187::_deefaMeroone
	int32_t ____deefaMeroone_6;
	// System.Int32 GarbageiOS.M_tremperGerege187::_drasfasjalReltisle
	int32_t ____drasfasjalReltisle_7;
	// System.String GarbageiOS.M_tremperGerege187::_jasfaslereChacerewhou
	String_t* ____jasfaslereChacerewhou_8;
	// System.UInt32 GarbageiOS.M_tremperGerege187::_whube
	uint32_t ____whube_9;
	// System.String GarbageiOS.M_tremperGerege187::_lostairSisduwe
	String_t* ____lostairSisduwe_10;
	// System.String GarbageiOS.M_tremperGerege187::_selhair
	String_t* ____selhair_11;
	// System.Int32 GarbageiOS.M_tremperGerege187::_stouqalsouKoumairyu
	int32_t ____stouqalsouKoumairyu_12;
	// System.Boolean GarbageiOS.M_tremperGerege187::_juheTufu
	bool ____juheTufu_13;
	// System.Int32 GarbageiOS.M_tremperGerege187::_corgor
	int32_t ____corgor_14;
	// System.Single GarbageiOS.M_tremperGerege187::_wirnelva
	float ____wirnelva_15;
	// System.Single GarbageiOS.M_tremperGerege187::_toofearheaSoujo
	float ____toofearheaSoujo_16;
	// System.Int32 GarbageiOS.M_tremperGerege187::_cowwowwhall
	int32_t ____cowwowwhall_17;

public:
	inline static int32_t get_offset_of__kojor_0() { return static_cast<int32_t>(offsetof(M_tremperGerege187_t3246597050, ____kojor_0)); }
	inline int32_t get__kojor_0() const { return ____kojor_0; }
	inline int32_t* get_address_of__kojor_0() { return &____kojor_0; }
	inline void set__kojor_0(int32_t value)
	{
		____kojor_0 = value;
	}

	inline static int32_t get_offset_of__retray_1() { return static_cast<int32_t>(offsetof(M_tremperGerege187_t3246597050, ____retray_1)); }
	inline uint32_t get__retray_1() const { return ____retray_1; }
	inline uint32_t* get_address_of__retray_1() { return &____retray_1; }
	inline void set__retray_1(uint32_t value)
	{
		____retray_1 = value;
	}

	inline static int32_t get_offset_of__rairhemTairche_2() { return static_cast<int32_t>(offsetof(M_tremperGerege187_t3246597050, ____rairhemTairche_2)); }
	inline bool get__rairhemTairche_2() const { return ____rairhemTairche_2; }
	inline bool* get_address_of__rairhemTairche_2() { return &____rairhemTairche_2; }
	inline void set__rairhemTairche_2(bool value)
	{
		____rairhemTairche_2 = value;
	}

	inline static int32_t get_offset_of__jairse_3() { return static_cast<int32_t>(offsetof(M_tremperGerege187_t3246597050, ____jairse_3)); }
	inline String_t* get__jairse_3() const { return ____jairse_3; }
	inline String_t** get_address_of__jairse_3() { return &____jairse_3; }
	inline void set__jairse_3(String_t* value)
	{
		____jairse_3 = value;
		Il2CppCodeGenWriteBarrier(&____jairse_3, value);
	}

	inline static int32_t get_offset_of__telwoucel_4() { return static_cast<int32_t>(offsetof(M_tremperGerege187_t3246597050, ____telwoucel_4)); }
	inline uint32_t get__telwoucel_4() const { return ____telwoucel_4; }
	inline uint32_t* get_address_of__telwoucel_4() { return &____telwoucel_4; }
	inline void set__telwoucel_4(uint32_t value)
	{
		____telwoucel_4 = value;
	}

	inline static int32_t get_offset_of__deenelLenestu_5() { return static_cast<int32_t>(offsetof(M_tremperGerege187_t3246597050, ____deenelLenestu_5)); }
	inline float get__deenelLenestu_5() const { return ____deenelLenestu_5; }
	inline float* get_address_of__deenelLenestu_5() { return &____deenelLenestu_5; }
	inline void set__deenelLenestu_5(float value)
	{
		____deenelLenestu_5 = value;
	}

	inline static int32_t get_offset_of__deefaMeroone_6() { return static_cast<int32_t>(offsetof(M_tremperGerege187_t3246597050, ____deefaMeroone_6)); }
	inline int32_t get__deefaMeroone_6() const { return ____deefaMeroone_6; }
	inline int32_t* get_address_of__deefaMeroone_6() { return &____deefaMeroone_6; }
	inline void set__deefaMeroone_6(int32_t value)
	{
		____deefaMeroone_6 = value;
	}

	inline static int32_t get_offset_of__drasfasjalReltisle_7() { return static_cast<int32_t>(offsetof(M_tremperGerege187_t3246597050, ____drasfasjalReltisle_7)); }
	inline int32_t get__drasfasjalReltisle_7() const { return ____drasfasjalReltisle_7; }
	inline int32_t* get_address_of__drasfasjalReltisle_7() { return &____drasfasjalReltisle_7; }
	inline void set__drasfasjalReltisle_7(int32_t value)
	{
		____drasfasjalReltisle_7 = value;
	}

	inline static int32_t get_offset_of__jasfaslereChacerewhou_8() { return static_cast<int32_t>(offsetof(M_tremperGerege187_t3246597050, ____jasfaslereChacerewhou_8)); }
	inline String_t* get__jasfaslereChacerewhou_8() const { return ____jasfaslereChacerewhou_8; }
	inline String_t** get_address_of__jasfaslereChacerewhou_8() { return &____jasfaslereChacerewhou_8; }
	inline void set__jasfaslereChacerewhou_8(String_t* value)
	{
		____jasfaslereChacerewhou_8 = value;
		Il2CppCodeGenWriteBarrier(&____jasfaslereChacerewhou_8, value);
	}

	inline static int32_t get_offset_of__whube_9() { return static_cast<int32_t>(offsetof(M_tremperGerege187_t3246597050, ____whube_9)); }
	inline uint32_t get__whube_9() const { return ____whube_9; }
	inline uint32_t* get_address_of__whube_9() { return &____whube_9; }
	inline void set__whube_9(uint32_t value)
	{
		____whube_9 = value;
	}

	inline static int32_t get_offset_of__lostairSisduwe_10() { return static_cast<int32_t>(offsetof(M_tremperGerege187_t3246597050, ____lostairSisduwe_10)); }
	inline String_t* get__lostairSisduwe_10() const { return ____lostairSisduwe_10; }
	inline String_t** get_address_of__lostairSisduwe_10() { return &____lostairSisduwe_10; }
	inline void set__lostairSisduwe_10(String_t* value)
	{
		____lostairSisduwe_10 = value;
		Il2CppCodeGenWriteBarrier(&____lostairSisduwe_10, value);
	}

	inline static int32_t get_offset_of__selhair_11() { return static_cast<int32_t>(offsetof(M_tremperGerege187_t3246597050, ____selhair_11)); }
	inline String_t* get__selhair_11() const { return ____selhair_11; }
	inline String_t** get_address_of__selhair_11() { return &____selhair_11; }
	inline void set__selhair_11(String_t* value)
	{
		____selhair_11 = value;
		Il2CppCodeGenWriteBarrier(&____selhair_11, value);
	}

	inline static int32_t get_offset_of__stouqalsouKoumairyu_12() { return static_cast<int32_t>(offsetof(M_tremperGerege187_t3246597050, ____stouqalsouKoumairyu_12)); }
	inline int32_t get__stouqalsouKoumairyu_12() const { return ____stouqalsouKoumairyu_12; }
	inline int32_t* get_address_of__stouqalsouKoumairyu_12() { return &____stouqalsouKoumairyu_12; }
	inline void set__stouqalsouKoumairyu_12(int32_t value)
	{
		____stouqalsouKoumairyu_12 = value;
	}

	inline static int32_t get_offset_of__juheTufu_13() { return static_cast<int32_t>(offsetof(M_tremperGerege187_t3246597050, ____juheTufu_13)); }
	inline bool get__juheTufu_13() const { return ____juheTufu_13; }
	inline bool* get_address_of__juheTufu_13() { return &____juheTufu_13; }
	inline void set__juheTufu_13(bool value)
	{
		____juheTufu_13 = value;
	}

	inline static int32_t get_offset_of__corgor_14() { return static_cast<int32_t>(offsetof(M_tremperGerege187_t3246597050, ____corgor_14)); }
	inline int32_t get__corgor_14() const { return ____corgor_14; }
	inline int32_t* get_address_of__corgor_14() { return &____corgor_14; }
	inline void set__corgor_14(int32_t value)
	{
		____corgor_14 = value;
	}

	inline static int32_t get_offset_of__wirnelva_15() { return static_cast<int32_t>(offsetof(M_tremperGerege187_t3246597050, ____wirnelva_15)); }
	inline float get__wirnelva_15() const { return ____wirnelva_15; }
	inline float* get_address_of__wirnelva_15() { return &____wirnelva_15; }
	inline void set__wirnelva_15(float value)
	{
		____wirnelva_15 = value;
	}

	inline static int32_t get_offset_of__toofearheaSoujo_16() { return static_cast<int32_t>(offsetof(M_tremperGerege187_t3246597050, ____toofearheaSoujo_16)); }
	inline float get__toofearheaSoujo_16() const { return ____toofearheaSoujo_16; }
	inline float* get_address_of__toofearheaSoujo_16() { return &____toofearheaSoujo_16; }
	inline void set__toofearheaSoujo_16(float value)
	{
		____toofearheaSoujo_16 = value;
	}

	inline static int32_t get_offset_of__cowwowwhall_17() { return static_cast<int32_t>(offsetof(M_tremperGerege187_t3246597050, ____cowwowwhall_17)); }
	inline int32_t get__cowwowwhall_17() const { return ____cowwowwhall_17; }
	inline int32_t* get_address_of__cowwowwhall_17() { return &____cowwowwhall_17; }
	inline void set__cowwowwhall_17(int32_t value)
	{
		____cowwowwhall_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
