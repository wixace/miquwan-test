﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Interpolate/<NewCounter>c__Iterator22
struct  U3CNewCounterU3Ec__Iterator22_t1689671549  : public Il2CppObject
{
public:
	// System.Int32 Interpolate/<NewCounter>c__Iterator22::start
	int32_t ___start_0;
	// System.Int32 Interpolate/<NewCounter>c__Iterator22::<i>__0
	int32_t ___U3CiU3E__0_1;
	// System.Int32 Interpolate/<NewCounter>c__Iterator22::end
	int32_t ___end_2;
	// System.Int32 Interpolate/<NewCounter>c__Iterator22::step
	int32_t ___step_3;
	// System.Int32 Interpolate/<NewCounter>c__Iterator22::$PC
	int32_t ___U24PC_4;
	// System.Single Interpolate/<NewCounter>c__Iterator22::$current
	float ___U24current_5;
	// System.Int32 Interpolate/<NewCounter>c__Iterator22::<$>start
	int32_t ___U3CU24U3Estart_6;
	// System.Int32 Interpolate/<NewCounter>c__Iterator22::<$>end
	int32_t ___U3CU24U3Eend_7;
	// System.Int32 Interpolate/<NewCounter>c__Iterator22::<$>step
	int32_t ___U3CU24U3Estep_8;

public:
	inline static int32_t get_offset_of_start_0() { return static_cast<int32_t>(offsetof(U3CNewCounterU3Ec__Iterator22_t1689671549, ___start_0)); }
	inline int32_t get_start_0() const { return ___start_0; }
	inline int32_t* get_address_of_start_0() { return &___start_0; }
	inline void set_start_0(int32_t value)
	{
		___start_0 = value;
	}

	inline static int32_t get_offset_of_U3CiU3E__0_1() { return static_cast<int32_t>(offsetof(U3CNewCounterU3Ec__Iterator22_t1689671549, ___U3CiU3E__0_1)); }
	inline int32_t get_U3CiU3E__0_1() const { return ___U3CiU3E__0_1; }
	inline int32_t* get_address_of_U3CiU3E__0_1() { return &___U3CiU3E__0_1; }
	inline void set_U3CiU3E__0_1(int32_t value)
	{
		___U3CiU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_end_2() { return static_cast<int32_t>(offsetof(U3CNewCounterU3Ec__Iterator22_t1689671549, ___end_2)); }
	inline int32_t get_end_2() const { return ___end_2; }
	inline int32_t* get_address_of_end_2() { return &___end_2; }
	inline void set_end_2(int32_t value)
	{
		___end_2 = value;
	}

	inline static int32_t get_offset_of_step_3() { return static_cast<int32_t>(offsetof(U3CNewCounterU3Ec__Iterator22_t1689671549, ___step_3)); }
	inline int32_t get_step_3() const { return ___step_3; }
	inline int32_t* get_address_of_step_3() { return &___step_3; }
	inline void set_step_3(int32_t value)
	{
		___step_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CNewCounterU3Ec__Iterator22_t1689671549, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CNewCounterU3Ec__Iterator22_t1689671549, ___U24current_5)); }
	inline float get_U24current_5() const { return ___U24current_5; }
	inline float* get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(float value)
	{
		___U24current_5 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Estart_6() { return static_cast<int32_t>(offsetof(U3CNewCounterU3Ec__Iterator22_t1689671549, ___U3CU24U3Estart_6)); }
	inline int32_t get_U3CU24U3Estart_6() const { return ___U3CU24U3Estart_6; }
	inline int32_t* get_address_of_U3CU24U3Estart_6() { return &___U3CU24U3Estart_6; }
	inline void set_U3CU24U3Estart_6(int32_t value)
	{
		___U3CU24U3Estart_6 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Eend_7() { return static_cast<int32_t>(offsetof(U3CNewCounterU3Ec__Iterator22_t1689671549, ___U3CU24U3Eend_7)); }
	inline int32_t get_U3CU24U3Eend_7() const { return ___U3CU24U3Eend_7; }
	inline int32_t* get_address_of_U3CU24U3Eend_7() { return &___U3CU24U3Eend_7; }
	inline void set_U3CU24U3Eend_7(int32_t value)
	{
		___U3CU24U3Eend_7 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3Estep_8() { return static_cast<int32_t>(offsetof(U3CNewCounterU3Ec__Iterator22_t1689671549, ___U3CU24U3Estep_8)); }
	inline int32_t get_U3CU24U3Estep_8() const { return ___U3CU24U3Estep_8; }
	inline int32_t* get_address_of_U3CU24U3Estep_8() { return &___U3CU24U3Estep_8; }
	inline void set_U3CU24U3Estep_8(int32_t value)
	{
		___U3CU24U3Estep_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
