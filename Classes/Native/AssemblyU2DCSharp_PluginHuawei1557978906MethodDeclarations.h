﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginHuawei
struct PluginHuawei_t1557978906;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// System.Object
struct Il2CppObject;
// VersionMgr
struct VersionMgr_t1322950208;
// VersionInfo
struct VersionInfo_t2356638086;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginHuawei::.ctor()
extern "C"  void PluginHuawei__ctor_m2875945153 (PluginHuawei_t1557978906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuawei::Init()
extern "C"  void PluginHuawei_Init_m1983971699 (PluginHuawei_t1557978906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginHuawei::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginHuawei_ReqSDKHttpLogin_m2217405398 (PluginHuawei_t1557978906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginHuawei::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginHuawei_IsLoginSuccess_m3391735154 (PluginHuawei_t1557978906 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuawei::OpenUserLogin()
extern "C"  void PluginHuawei_OpenUserLogin_m38552083 (PluginHuawei_t1557978906 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuawei::UserPay(CEvent.ZEvent)
extern "C"  void PluginHuawei_UserPay_m2833212927 (PluginHuawei_t1557978906 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuawei::RolelvUpinfo(CEvent.ZEvent)
extern "C"  void PluginHuawei_RolelvUpinfo_m2214292847 (PluginHuawei_t1557978906 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuawei::OnInitSuccess(System.String)
extern "C"  void PluginHuawei_OnInitSuccess_m372885903 (PluginHuawei_t1557978906 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuawei::OnInitFail(System.String)
extern "C"  void PluginHuawei_OnInitFail_m2801248786 (PluginHuawei_t1557978906 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuawei::OnLoginSuccess(System.String)
extern "C"  void PluginHuawei_OnLoginSuccess_m3359240454 (PluginHuawei_t1557978906 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuawei::OnLoginFail(System.String)
extern "C"  void PluginHuawei_OnLoginFail_m3864169979 (PluginHuawei_t1557978906 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuawei::OnUserSwitchSuccess(System.String)
extern "C"  void PluginHuawei_OnUserSwitchSuccess_m2702327134 (PluginHuawei_t1557978906 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuawei::OnPaySuccess(System.String)
extern "C"  void PluginHuawei_OnPaySuccess_m4040481989 (PluginHuawei_t1557978906 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuawei::OnPayFail(System.String)
extern "C"  void PluginHuawei_OnPayFail_m163205788 (PluginHuawei_t1557978906 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuawei::OnLogoutSuccess(System.String)
extern "C"  void PluginHuawei_OnLogoutSuccess_m1060781641 (PluginHuawei_t1557978906 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuawei::OnLogoutFail(System.String)
extern "C"  void PluginHuawei_OnLogoutFail_m3638034328 (PluginHuawei_t1557978906 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuawei::<OnUserSwitchSuccess>m__42A()
extern "C"  void PluginHuawei_U3COnUserSwitchSuccessU3Em__42A_m245351818 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuawei::<OnLogoutSuccess>m__42B()
extern "C"  void PluginHuawei_U3COnLogoutSuccessU3Em__42B_m520213110 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuawei::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginHuawei_ilo_AddEventListener1_m123628563 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuawei::ilo_Log2(System.Object,System.Boolean)
extern "C"  void PluginHuawei_ilo_Log2_m2453005997 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginHuawei::ilo_get_Instance3()
extern "C"  VersionMgr_t1322950208 * PluginHuawei_ilo_get_Instance3_m3611509764 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginHuawei::ilo_get_currentVS4(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginHuawei_ilo_get_currentVS4_m781225162 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginHuawei::ilo_get_PluginsSdkMgr5()
extern "C"  PluginsSdkMgr_t3884624670 * PluginHuawei_ilo_get_PluginsSdkMgr5_m536068363 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuawei::ilo_ReqSDKHttpLogin6(PluginsSdkMgr)
extern "C"  void PluginHuawei_ilo_ReqSDKHttpLogin6_m1602641325 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTextMgr PluginHuawei::ilo_get_FloatTextMgr7()
extern "C"  FloatTextMgr_t630384591 * PluginHuawei_ilo_get_FloatTextMgr7_m3695769277 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginHuawei::ilo_Logout8(System.Action)
extern "C"  void PluginHuawei_ilo_Logout8_m3245605059 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
