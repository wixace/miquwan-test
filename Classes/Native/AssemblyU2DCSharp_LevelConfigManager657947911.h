﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LevelConfigManager
struct LevelConfigManager_t657947911;
// System.Collections.Generic.Dictionary`2<System.Int32,JSCLevelConfig>
struct Dictionary_2_t1408362739;
// System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig>
struct Dictionary_2_t3423381968;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LevelConfigManager
struct  LevelConfigManager_t657947911  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,JSCLevelConfig> LevelConfigManager::levelconfigdict
	Dictionary_2_t1408362739 * ___levelconfigdict_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,JSCMapPathConfig> LevelConfigManager::mapPathInfoDic
	Dictionary_2_t3423381968 * ___mapPathInfoDic_2;
	// System.Int32 LevelConfigManager::curlevel
	int32_t ___curlevel_3;

public:
	inline static int32_t get_offset_of_levelconfigdict_1() { return static_cast<int32_t>(offsetof(LevelConfigManager_t657947911, ___levelconfigdict_1)); }
	inline Dictionary_2_t1408362739 * get_levelconfigdict_1() const { return ___levelconfigdict_1; }
	inline Dictionary_2_t1408362739 ** get_address_of_levelconfigdict_1() { return &___levelconfigdict_1; }
	inline void set_levelconfigdict_1(Dictionary_2_t1408362739 * value)
	{
		___levelconfigdict_1 = value;
		Il2CppCodeGenWriteBarrier(&___levelconfigdict_1, value);
	}

	inline static int32_t get_offset_of_mapPathInfoDic_2() { return static_cast<int32_t>(offsetof(LevelConfigManager_t657947911, ___mapPathInfoDic_2)); }
	inline Dictionary_2_t3423381968 * get_mapPathInfoDic_2() const { return ___mapPathInfoDic_2; }
	inline Dictionary_2_t3423381968 ** get_address_of_mapPathInfoDic_2() { return &___mapPathInfoDic_2; }
	inline void set_mapPathInfoDic_2(Dictionary_2_t3423381968 * value)
	{
		___mapPathInfoDic_2 = value;
		Il2CppCodeGenWriteBarrier(&___mapPathInfoDic_2, value);
	}

	inline static int32_t get_offset_of_curlevel_3() { return static_cast<int32_t>(offsetof(LevelConfigManager_t657947911, ___curlevel_3)); }
	inline int32_t get_curlevel_3() const { return ___curlevel_3; }
	inline int32_t* get_address_of_curlevel_3() { return &___curlevel_3; }
	inline void set_curlevel_3(int32_t value)
	{
		___curlevel_3 = value;
	}
};

struct LevelConfigManager_t657947911_StaticFields
{
public:
	// LevelConfigManager LevelConfigManager::_inst
	LevelConfigManager_t657947911 * ____inst_0;
	// System.Boolean LevelConfigManager::<inited>k__BackingField
	bool ___U3CinitedU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of__inst_0() { return static_cast<int32_t>(offsetof(LevelConfigManager_t657947911_StaticFields, ____inst_0)); }
	inline LevelConfigManager_t657947911 * get__inst_0() const { return ____inst_0; }
	inline LevelConfigManager_t657947911 ** get_address_of__inst_0() { return &____inst_0; }
	inline void set__inst_0(LevelConfigManager_t657947911 * value)
	{
		____inst_0 = value;
		Il2CppCodeGenWriteBarrier(&____inst_0, value);
	}

	inline static int32_t get_offset_of_U3CinitedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(LevelConfigManager_t657947911_StaticFields, ___U3CinitedU3Ek__BackingField_4)); }
	inline bool get_U3CinitedU3Ek__BackingField_4() const { return ___U3CinitedU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CinitedU3Ek__BackingField_4() { return &___U3CinitedU3Ek__BackingField_4; }
	inline void set_U3CinitedU3Ek__BackingField_4(bool value)
	{
		___U3CinitedU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
