﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConstructorID
struct ConstructorID_t3348888181;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine_NavMeshHitGenerated
struct  UnityEngine_NavMeshHitGenerated_t2933202596  : public Il2CppObject
{
public:

public:
};

struct UnityEngine_NavMeshHitGenerated_t2933202596_StaticFields
{
public:
	// ConstructorID UnityEngine_NavMeshHitGenerated::constructorID0
	ConstructorID_t3348888181 * ___constructorID0_0;

public:
	inline static int32_t get_offset_of_constructorID0_0() { return static_cast<int32_t>(offsetof(UnityEngine_NavMeshHitGenerated_t2933202596_StaticFields, ___constructorID0_0)); }
	inline ConstructorID_t3348888181 * get_constructorID0_0() const { return ___constructorID0_0; }
	inline ConstructorID_t3348888181 ** get_address_of_constructorID0_0() { return &___constructorID0_0; }
	inline void set_constructorID0_0(ConstructorID_t3348888181 * value)
	{
		___constructorID0_0 = value;
		Il2CppCodeGenWriteBarrier(&___constructorID0_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
