﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_jisjeeFigoka120
struct M_jisjeeFigoka120_t893448696;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_jisjeeFigoka120::.ctor()
extern "C"  void M_jisjeeFigoka120__ctor_m646804587 (M_jisjeeFigoka120_t893448696 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jisjeeFigoka120::M_tenuneLerwe0(System.String[],System.Int32)
extern "C"  void M_jisjeeFigoka120_M_tenuneLerwe0_m1781528890 (M_jisjeeFigoka120_t893448696 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jisjeeFigoka120::M_yarralPudel1(System.String[],System.Int32)
extern "C"  void M_jisjeeFigoka120_M_yarralPudel1_m3529407520 (M_jisjeeFigoka120_t893448696 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_jisjeeFigoka120::M_rasxu2(System.String[],System.Int32)
extern "C"  void M_jisjeeFigoka120_M_rasxu2_m1985875507 (M_jisjeeFigoka120_t893448696 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
