﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen981688446.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"

// System.Void System.Array/InternalEnumerator`1<FLOAT_TEXT_ID>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m482902639_gshared (InternalEnumerator_1_t981688446 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m482902639(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t981688446 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m482902639_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<FLOAT_TEXT_ID>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2827904721_gshared (InternalEnumerator_1_t981688446 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2827904721(__this, method) ((  void (*) (InternalEnumerator_1_t981688446 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2827904721_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<FLOAT_TEXT_ID>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2206495175_gshared (InternalEnumerator_1_t981688446 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2206495175(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t981688446 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m2206495175_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<FLOAT_TEXT_ID>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1902279430_gshared (InternalEnumerator_1_t981688446 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1902279430(__this, method) ((  void (*) (InternalEnumerator_1_t981688446 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1902279430_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<FLOAT_TEXT_ID>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2876186497_gshared (InternalEnumerator_1_t981688446 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2876186497(__this, method) ((  bool (*) (InternalEnumerator_1_t981688446 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2876186497_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<FLOAT_TEXT_ID>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2326624408_gshared (InternalEnumerator_1_t981688446 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2326624408(__this, method) ((  int32_t (*) (InternalEnumerator_1_t981688446 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2326624408_gshared)(__this, method)
