﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_CachingGenerated
struct UnityEngine_CachingGenerated_t1578616312;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_CachingGenerated::.ctor()
extern "C"  void UnityEngine_CachingGenerated__ctor_m3399234467 (UnityEngine_CachingGenerated_t1578616312 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CachingGenerated::Caching_Caching1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CachingGenerated_Caching_Caching1_m3052416375 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CachingGenerated::Caching_spaceFree(JSVCall)
extern "C"  void UnityEngine_CachingGenerated_Caching_spaceFree_m1859576308 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CachingGenerated::Caching_maximumAvailableDiskSpace(JSVCall)
extern "C"  void UnityEngine_CachingGenerated_Caching_maximumAvailableDiskSpace_m2710266726 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CachingGenerated::Caching_spaceOccupied(JSVCall)
extern "C"  void UnityEngine_CachingGenerated_Caching_spaceOccupied_m488108290 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CachingGenerated::Caching_expirationDelay(JSVCall)
extern "C"  void UnityEngine_CachingGenerated_Caching_expirationDelay_m665901234 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CachingGenerated::Caching_enabled(JSVCall)
extern "C"  void UnityEngine_CachingGenerated_Caching_enabled_m3283330085 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CachingGenerated::Caching_compressionEnabled(JSVCall)
extern "C"  void UnityEngine_CachingGenerated_Caching_compressionEnabled_m733998123 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CachingGenerated::Caching_ready(JSVCall)
extern "C"  void UnityEngine_CachingGenerated_Caching_ready_m239440899 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CachingGenerated::Caching_Authorize__String__String__Int64__Int32__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CachingGenerated_Caching_Authorize__String__String__Int64__Int32__String_m2468373018 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CachingGenerated::Caching_Authorize__String__String__Int64__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CachingGenerated_Caching_Authorize__String__String__Int64__String_m2758383608 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CachingGenerated::Caching_CleanCache(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CachingGenerated_Caching_CleanCache_m1246317470 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CachingGenerated::Caching_IsVersionCached__String__Hash128(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CachingGenerated_Caching_IsVersionCached__String__Hash128_m2096462093 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CachingGenerated::Caching_IsVersionCached__String__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CachingGenerated_Caching_IsVersionCached__String__Int32_m3011165234 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CachingGenerated::Caching_MarkAsUsed__String__Hash128(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CachingGenerated_Caching_MarkAsUsed__String__Hash128_m545410745 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CachingGenerated::Caching_MarkAsUsed__String__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_CachingGenerated_Caching_MarkAsUsed__String__Int32_m1431899358 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CachingGenerated::__Register()
extern "C"  void UnityEngine_CachingGenerated___Register_m4191360772 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CachingGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_CachingGenerated_ilo_getObject1_m1049279055 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_CachingGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_CachingGenerated_ilo_attachFinalizerObject2_m3367682269 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 UnityEngine_CachingGenerated::ilo_getInt643(System.Int32)
extern "C"  int64_t UnityEngine_CachingGenerated_ilo_getInt643_m2087613260 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CachingGenerated::ilo_setBooleanS4(System.Int32,System.Boolean)
extern "C"  void UnityEngine_CachingGenerated_ilo_setBooleanS4_m1959839409 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_CachingGenerated::ilo_getInt325(System.Int32)
extern "C"  int32_t UnityEngine_CachingGenerated_ilo_getInt325_m1200420462 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_CachingGenerated::ilo_getStringS6(System.Int32)
extern "C"  String_t* UnityEngine_CachingGenerated_ilo_getStringS6_m182568934 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
