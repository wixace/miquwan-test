﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ActiveAnimation
struct ActiveAnimation_t557316862;
// System.String
struct String_t;
// UnityEngine.Animation
struct Animation_t1724966010;
// UnityEngine.Animator
struct Animator_t2776330603;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t1077642479;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UIPanel
struct UIPanel_t295209936;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_AnimationOrTween_Direction1859285089.h"
#include "UnityEngine_UnityEngine_Animation1724966010.h"
#include "AssemblyU2DCSharp_AnimationOrTween_EnableCondition1579250490.h"
#include "AssemblyU2DCSharp_AnimationOrTween_DisableConditio1452383689.h"
#include "UnityEngine_UnityEngine_Animator2776330603.h"
#include "AssemblyU2DCSharp_ActiveAnimation557316862.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"

// System.Void ActiveAnimation::.ctor()
extern "C"  void ActiveAnimation__ctor_m942098989 (ActiveAnimation_t557316862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ActiveAnimation::get_playbackTime()
extern "C"  float ActiveAnimation_get_playbackTime_m2980075932 (ActiveAnimation_t557316862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActiveAnimation::get_isPlaying()
extern "C"  bool ActiveAnimation_get_isPlaying_m1988265818 (ActiveAnimation_t557316862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveAnimation::Finish()
extern "C"  void ActiveAnimation_Finish_m3165870026 (ActiveAnimation_t557316862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveAnimation::Reset()
extern "C"  void ActiveAnimation_Reset_m2883499226 (ActiveAnimation_t557316862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveAnimation::Start()
extern "C"  void ActiveAnimation_Start_m4184204077 (ActiveAnimation_t557316862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveAnimation::Update()
extern "C"  void ActiveAnimation_Update_m867159680 (ActiveAnimation_t557316862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveAnimation::Play(System.String,AnimationOrTween.Direction)
extern "C"  void ActiveAnimation_Play_m1105978650 (ActiveAnimation_t557316862 * __this, String_t* ___clipName0, int32_t ___playDirection1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ActiveAnimation ActiveAnimation::Play(UnityEngine.Animation,System.String,AnimationOrTween.Direction,AnimationOrTween.EnableCondition,AnimationOrTween.DisableCondition)
extern "C"  ActiveAnimation_t557316862 * ActiveAnimation_Play_m2459698113 (Il2CppObject * __this /* static, unused */, Animation_t1724966010 * ___anim0, String_t* ___clipName1, int32_t ___playDirection2, int32_t ___enableBeforePlay3, int32_t ___disableCondition4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ActiveAnimation ActiveAnimation::Play(UnityEngine.Animation,System.String,AnimationOrTween.Direction)
extern "C"  ActiveAnimation_t557316862 * ActiveAnimation_Play_m479659302 (Il2CppObject * __this /* static, unused */, Animation_t1724966010 * ___anim0, String_t* ___clipName1, int32_t ___playDirection2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ActiveAnimation ActiveAnimation::Play(UnityEngine.Animation,AnimationOrTween.Direction)
extern "C"  ActiveAnimation_t557316862 * ActiveAnimation_Play_m3105139874 (Il2CppObject * __this /* static, unused */, Animation_t1724966010 * ___anim0, int32_t ___playDirection1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ActiveAnimation ActiveAnimation::Play(UnityEngine.Animator,System.String,AnimationOrTween.Direction,AnimationOrTween.EnableCondition,AnimationOrTween.DisableCondition)
extern "C"  ActiveAnimation_t557316862 * ActiveAnimation_Play_m2827471594 (Il2CppObject * __this /* static, unused */, Animator_t2776330603 * ___anim0, String_t* ___clipName1, int32_t ___playDirection2, int32_t ___enableBeforePlay3, int32_t ___disableCondition4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveAnimation::OnDestory()
extern "C"  void ActiveAnimation_OnDestory_m1905612608 (ActiveAnimation_t557316862 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ActiveAnimation::ilo_get_playbackTime1(ActiveAnimation)
extern "C"  float ActiveAnimation_ilo_get_playbackTime1_m534485008 (Il2CppObject * __this /* static, unused */, ActiveAnimation_t557316862 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActiveAnimation::ilo_IsValid2(System.Collections.Generic.List`1<EventDelegate>)
extern "C"  bool ActiveAnimation_ilo_IsValid2_m2658550809 (Il2CppObject * __this /* static, unused */, List_1_t1077642479 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActiveAnimation::ilo_get_isPlaying3(ActiveAnimation)
extern "C"  bool ActiveAnimation_ilo_get_isPlaying3_m2265427462 (Il2CppObject * __this /* static, unused */, ActiveAnimation_t557316862 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActiveAnimation::ilo_GetActive4(UnityEngine.GameObject)
extern "C"  bool ActiveAnimation_ilo_GetActive4_m614893606 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveAnimation::ilo_SetActive5(UnityEngine.GameObject,System.Boolean)
extern "C"  void ActiveAnimation_ilo_SetActive5_m4204751822 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ActiveAnimation ActiveAnimation::ilo_Play6(UnityEngine.Animation,System.String,AnimationOrTween.Direction,AnimationOrTween.EnableCondition,AnimationOrTween.DisableCondition)
extern "C"  ActiveAnimation_t557316862 * ActiveAnimation_ilo_Play6_m2382320392 (Il2CppObject * __this /* static, unused */, Animation_t1724966010 * ___anim0, String_t* ___clipName1, int32_t ___playDirection2, int32_t ___enableBeforePlay3, int32_t ___disableCondition4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveAnimation::ilo_Refresh7(UIPanel)
extern "C"  void ActiveAnimation_ilo_Refresh7_m3667513404 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
