﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweenWidthGenerated
struct TweenWidthGenerated_t521563540;
// JSVCall
struct JSVCall_t3708497963;
// TweenWidth
struct TweenWidth_t2940542523;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_TweenWidth2940542523.h"

// System.Void TweenWidthGenerated::.ctor()
extern "C"  void TweenWidthGenerated__ctor_m2607262807 (TweenWidthGenerated_t521563540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenWidthGenerated::TweenWidth_TweenWidth1(JSVCall,System.Int32)
extern "C"  bool TweenWidthGenerated_TweenWidth_TweenWidth1_m3102651547 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenWidthGenerated::TweenWidth_from(JSVCall)
extern "C"  void TweenWidthGenerated_TweenWidth_from_m2939013124 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenWidthGenerated::TweenWidth_to(JSVCall)
extern "C"  void TweenWidthGenerated_TweenWidth_to_m4064773715 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenWidthGenerated::TweenWidth_updateTable(JSVCall)
extern "C"  void TweenWidthGenerated_TweenWidth_updateTable_m2484372153 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenWidthGenerated::TweenWidth_cachedWidget(JSVCall)
extern "C"  void TweenWidthGenerated_TweenWidth_cachedWidget_m4042537256 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenWidthGenerated::TweenWidth_value(JSVCall)
extern "C"  void TweenWidthGenerated_TweenWidth_value_m1800492365 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenWidthGenerated::TweenWidth_SetEndToCurrentValue(JSVCall,System.Int32)
extern "C"  bool TweenWidthGenerated_TweenWidth_SetEndToCurrentValue_m3615960841 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenWidthGenerated::TweenWidth_SetStartToCurrentValue(JSVCall,System.Int32)
extern "C"  bool TweenWidthGenerated_TweenWidth_SetStartToCurrentValue_m3335770256 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenWidthGenerated::TweenWidth_Begin__UIWidget__Single__Int32(JSVCall,System.Int32)
extern "C"  bool TweenWidthGenerated_TweenWidth_Begin__UIWidget__Single__Int32_m1821573154 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenWidthGenerated::__Register()
extern "C"  void TweenWidthGenerated___Register_m3902467792 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TweenWidthGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t TweenWidthGenerated_ilo_getObject1_m745527615 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenWidthGenerated::ilo_setInt322(System.Int32,System.Int32)
extern "C"  void TweenWidthGenerated_ilo_setInt322_m4153021886 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 TweenWidthGenerated::ilo_getInt323(System.Int32)
extern "C"  int32_t TweenWidthGenerated_ilo_getInt323_m4133053692 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenWidthGenerated::ilo_SetStartToCurrentValue4(TweenWidth)
extern "C"  void TweenWidthGenerated_ilo_SetStartToCurrentValue4_m1151649736 (Il2CppObject * __this /* static, unused */, TweenWidth_t2940542523 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
