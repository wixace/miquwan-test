﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSSerializer
struct JSSerializer_t3534558139;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// JSComponent
struct JSComponent_t1642894772;
// JSSerializer/SerializeStruct
struct SerializeStruct_t2767317089;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSSerializer_UnitType4025954002.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_JSComponent1642894772.h"
#include "AssemblyU2DCSharp_JSSerializer_SerializeStruct2767317089.h"
#include "AssemblyU2DCSharp_JSSerializer3534558139.h"

// System.Void JSSerializer::.ctor()
extern "C"  void JSSerializer__ctor_m1288036672 (JSSerializer_t3534558139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSSerializer::toID(JSSerializer/UnitType,System.String)
extern "C"  int32_t JSSerializer_toID_m103777556 (JSSerializer_t3534558139 * __this, int32_t ___eType0, String_t* ___strValue1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSSerializer::GetGameObjectMonoBehaviourJSObj(UnityEngine.GameObject,System.String,JSComponent&)
extern "C"  int32_t JSSerializer_GetGameObjectMonoBehaviourJSObj_m3730728629 (JSSerializer_t3534558139 * __this, GameObject_t3674682005 * ___go0, String_t* ___scriptName1, JSComponent_t1642894772 ** ___component2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSSerializer::TraverseSerialize(System.Int32,JSSerializer/SerializeStruct)
extern "C"  void JSSerializer_TraverseSerialize_m1971888040 (JSSerializer_t3534558139 * __this, int32_t ___jsObjID0, SerializeStruct_t2767317089 * ___st1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSSerializer::NextString()
extern "C"  String_t* JSSerializer_NextString_m1558677003 (JSSerializer_t3534558139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSSerializer::get_DataSerialized()
extern "C"  bool JSSerializer_get_DataSerialized_m1497552783 (JSSerializer_t3534558139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSSerializer::initSerializedData(System.Int32)
extern "C"  void JSSerializer_initSerializedData_m1065297395 (JSSerializer_t3534558139 * __this, int32_t ___jsObjID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSSerializer::SetObjectFieldOrProperty(System.Int32,System.String,System.Int32)
extern "C"  void JSSerializer_SetObjectFieldOrProperty_m598231893 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, String_t* ___name1, int32_t ___valueID2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSSerializer::ilo_setBooleanS1(System.Int32,System.Boolean)
extern "C"  void JSSerializer_ilo_setBooleanS1_m4209136561 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSSerializer::ilo_setInt322(System.Int32,System.Int32)
extern "C"  void JSSerializer_ilo_setInt322_m2141970933 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSSerializer::ilo_getSaveID3()
extern "C"  int32_t JSSerializer_ilo_getSaveID3_m3020515786 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSSerializer::ilo_setUInt324(System.Int32,System.UInt32)
extern "C"  void JSSerializer_ilo_setUInt324_m1865629397 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSSerializer::ilo_setDouble5(System.Int32,System.Double)
extern "C"  void JSSerializer_ilo_setDouble5_m1014725606 (Il2CppObject * __this /* static, unused */, int32_t ___e0, double ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSSerializer::ilo_GetJSObjID6(JSComponent,System.Boolean)
extern "C"  int32_t JSSerializer_ilo_GetJSObjID6_m1320476511 (Il2CppObject * __this /* static, unused */, JSComponent_t1642894772 * ____this0, bool ___callSerialize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSSerializer::ilo_NextString7(JSSerializer)
extern "C"  String_t* JSSerializer_ilo_NextString7_m2083372032 (Il2CppObject * __this /* static, unused */, JSSerializer_t3534558139 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSSerializer::ilo_AddChild8(JSSerializer/SerializeStruct,JSSerializer/SerializeStruct)
extern "C"  void JSSerializer_ilo_AddChild8_m665347134 (Il2CppObject * __this /* static, unused */, SerializeStruct_t2767317089 * ____this0, SerializeStruct_t2767317089 * ___ss1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSSerializer::ilo_removeID9(JSSerializer/SerializeStruct)
extern "C"  void JSSerializer_ilo_removeID9_m2186253348 (Il2CppObject * __this /* static, unused */, SerializeStruct_t2767317089 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSSerializer::ilo_setProperty10(System.Int32,System.String,System.Int32)
extern "C"  int32_t JSSerializer_ilo_setProperty10_m3715543927 (Il2CppObject * __this /* static, unused */, int32_t ___id0, String_t* ___name1, int32_t ___valueID2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
