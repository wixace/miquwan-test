﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "mscorlib_System_ValueType1744280289.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.NNInfo
struct  NNInfo_t1570625892 
{
public:
	// Pathfinding.GraphNode Pathfinding.NNInfo::node
	GraphNode_t23612370 * ___node_0;
	// Pathfinding.GraphNode Pathfinding.NNInfo::constrainedNode
	GraphNode_t23612370 * ___constrainedNode_1;
	// UnityEngine.Vector3 Pathfinding.NNInfo::clampedPosition
	Vector3_t4282066566  ___clampedPosition_2;
	// UnityEngine.Vector3 Pathfinding.NNInfo::constClampedPosition
	Vector3_t4282066566  ___constClampedPosition_3;
	// System.Int32 Pathfinding.NNInfo::row
	int32_t ___row_4;
	// System.Int32 Pathfinding.NNInfo::col
	int32_t ___col_5;

public:
	inline static int32_t get_offset_of_node_0() { return static_cast<int32_t>(offsetof(NNInfo_t1570625892, ___node_0)); }
	inline GraphNode_t23612370 * get_node_0() const { return ___node_0; }
	inline GraphNode_t23612370 ** get_address_of_node_0() { return &___node_0; }
	inline void set_node_0(GraphNode_t23612370 * value)
	{
		___node_0 = value;
		Il2CppCodeGenWriteBarrier(&___node_0, value);
	}

	inline static int32_t get_offset_of_constrainedNode_1() { return static_cast<int32_t>(offsetof(NNInfo_t1570625892, ___constrainedNode_1)); }
	inline GraphNode_t23612370 * get_constrainedNode_1() const { return ___constrainedNode_1; }
	inline GraphNode_t23612370 ** get_address_of_constrainedNode_1() { return &___constrainedNode_1; }
	inline void set_constrainedNode_1(GraphNode_t23612370 * value)
	{
		___constrainedNode_1 = value;
		Il2CppCodeGenWriteBarrier(&___constrainedNode_1, value);
	}

	inline static int32_t get_offset_of_clampedPosition_2() { return static_cast<int32_t>(offsetof(NNInfo_t1570625892, ___clampedPosition_2)); }
	inline Vector3_t4282066566  get_clampedPosition_2() const { return ___clampedPosition_2; }
	inline Vector3_t4282066566 * get_address_of_clampedPosition_2() { return &___clampedPosition_2; }
	inline void set_clampedPosition_2(Vector3_t4282066566  value)
	{
		___clampedPosition_2 = value;
	}

	inline static int32_t get_offset_of_constClampedPosition_3() { return static_cast<int32_t>(offsetof(NNInfo_t1570625892, ___constClampedPosition_3)); }
	inline Vector3_t4282066566  get_constClampedPosition_3() const { return ___constClampedPosition_3; }
	inline Vector3_t4282066566 * get_address_of_constClampedPosition_3() { return &___constClampedPosition_3; }
	inline void set_constClampedPosition_3(Vector3_t4282066566  value)
	{
		___constClampedPosition_3 = value;
	}

	inline static int32_t get_offset_of_row_4() { return static_cast<int32_t>(offsetof(NNInfo_t1570625892, ___row_4)); }
	inline int32_t get_row_4() const { return ___row_4; }
	inline int32_t* get_address_of_row_4() { return &___row_4; }
	inline void set_row_4(int32_t value)
	{
		___row_4 = value;
	}

	inline static int32_t get_offset_of_col_5() { return static_cast<int32_t>(offsetof(NNInfo_t1570625892, ___col_5)); }
	inline int32_t get_col_5() const { return ___col_5; }
	inline int32_t* get_address_of_col_5() { return &___col_5; }
	inline void set_col_5(int32_t value)
	{
		___col_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Pathfinding.NNInfo
struct NNInfo_t1570625892_marshaled_pinvoke
{
	GraphNode_t23612370 * ___node_0;
	GraphNode_t23612370 * ___constrainedNode_1;
	Vector3_t4282066566_marshaled_pinvoke ___clampedPosition_2;
	Vector3_t4282066566_marshaled_pinvoke ___constClampedPosition_3;
	int32_t ___row_4;
	int32_t ___col_5;
};
// Native definition for marshalling of: Pathfinding.NNInfo
struct NNInfo_t1570625892_marshaled_com
{
	GraphNode_t23612370 * ___node_0;
	GraphNode_t23612370 * ___constrainedNode_1;
	Vector3_t4282066566_marshaled_com ___clampedPosition_2;
	Vector3_t4282066566_marshaled_com ___constClampedPosition_3;
	int32_t ___row_4;
	int32_t ___col_5;
};
