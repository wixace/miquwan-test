﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_TBPinchZoom_ZoomMethod1064823268.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TBPinchZoom
struct  TBPinchZoom_t813254879  : public MonoBehaviour_t667441552
{
public:
	// TBPinchZoom/ZoomMethod TBPinchZoom::zoomMethod
	int32_t ___zoomMethod_2;
	// System.Single TBPinchZoom::zoomSpeed
	float ___zoomSpeed_3;
	// System.Single TBPinchZoom::minZoomAmount
	float ___minZoomAmount_4;
	// System.Single TBPinchZoom::maxZoomAmount
	float ___maxZoomAmount_5;
	// System.Single TBPinchZoom::SmoothSpeed
	float ___SmoothSpeed_6;
	// UnityEngine.Vector3 TBPinchZoom::defaultPos
	Vector3_t4282066566  ___defaultPos_7;
	// System.Single TBPinchZoom::defaultFov
	float ___defaultFov_8;
	// System.Single TBPinchZoom::defaultOrthoSize
	float ___defaultOrthoSize_9;
	// System.Single TBPinchZoom::idealZoomAmount
	float ___idealZoomAmount_10;
	// System.Single TBPinchZoom::zoomAmount
	float ___zoomAmount_11;

public:
	inline static int32_t get_offset_of_zoomMethod_2() { return static_cast<int32_t>(offsetof(TBPinchZoom_t813254879, ___zoomMethod_2)); }
	inline int32_t get_zoomMethod_2() const { return ___zoomMethod_2; }
	inline int32_t* get_address_of_zoomMethod_2() { return &___zoomMethod_2; }
	inline void set_zoomMethod_2(int32_t value)
	{
		___zoomMethod_2 = value;
	}

	inline static int32_t get_offset_of_zoomSpeed_3() { return static_cast<int32_t>(offsetof(TBPinchZoom_t813254879, ___zoomSpeed_3)); }
	inline float get_zoomSpeed_3() const { return ___zoomSpeed_3; }
	inline float* get_address_of_zoomSpeed_3() { return &___zoomSpeed_3; }
	inline void set_zoomSpeed_3(float value)
	{
		___zoomSpeed_3 = value;
	}

	inline static int32_t get_offset_of_minZoomAmount_4() { return static_cast<int32_t>(offsetof(TBPinchZoom_t813254879, ___minZoomAmount_4)); }
	inline float get_minZoomAmount_4() const { return ___minZoomAmount_4; }
	inline float* get_address_of_minZoomAmount_4() { return &___minZoomAmount_4; }
	inline void set_minZoomAmount_4(float value)
	{
		___minZoomAmount_4 = value;
	}

	inline static int32_t get_offset_of_maxZoomAmount_5() { return static_cast<int32_t>(offsetof(TBPinchZoom_t813254879, ___maxZoomAmount_5)); }
	inline float get_maxZoomAmount_5() const { return ___maxZoomAmount_5; }
	inline float* get_address_of_maxZoomAmount_5() { return &___maxZoomAmount_5; }
	inline void set_maxZoomAmount_5(float value)
	{
		___maxZoomAmount_5 = value;
	}

	inline static int32_t get_offset_of_SmoothSpeed_6() { return static_cast<int32_t>(offsetof(TBPinchZoom_t813254879, ___SmoothSpeed_6)); }
	inline float get_SmoothSpeed_6() const { return ___SmoothSpeed_6; }
	inline float* get_address_of_SmoothSpeed_6() { return &___SmoothSpeed_6; }
	inline void set_SmoothSpeed_6(float value)
	{
		___SmoothSpeed_6 = value;
	}

	inline static int32_t get_offset_of_defaultPos_7() { return static_cast<int32_t>(offsetof(TBPinchZoom_t813254879, ___defaultPos_7)); }
	inline Vector3_t4282066566  get_defaultPos_7() const { return ___defaultPos_7; }
	inline Vector3_t4282066566 * get_address_of_defaultPos_7() { return &___defaultPos_7; }
	inline void set_defaultPos_7(Vector3_t4282066566  value)
	{
		___defaultPos_7 = value;
	}

	inline static int32_t get_offset_of_defaultFov_8() { return static_cast<int32_t>(offsetof(TBPinchZoom_t813254879, ___defaultFov_8)); }
	inline float get_defaultFov_8() const { return ___defaultFov_8; }
	inline float* get_address_of_defaultFov_8() { return &___defaultFov_8; }
	inline void set_defaultFov_8(float value)
	{
		___defaultFov_8 = value;
	}

	inline static int32_t get_offset_of_defaultOrthoSize_9() { return static_cast<int32_t>(offsetof(TBPinchZoom_t813254879, ___defaultOrthoSize_9)); }
	inline float get_defaultOrthoSize_9() const { return ___defaultOrthoSize_9; }
	inline float* get_address_of_defaultOrthoSize_9() { return &___defaultOrthoSize_9; }
	inline void set_defaultOrthoSize_9(float value)
	{
		___defaultOrthoSize_9 = value;
	}

	inline static int32_t get_offset_of_idealZoomAmount_10() { return static_cast<int32_t>(offsetof(TBPinchZoom_t813254879, ___idealZoomAmount_10)); }
	inline float get_idealZoomAmount_10() const { return ___idealZoomAmount_10; }
	inline float* get_address_of_idealZoomAmount_10() { return &___idealZoomAmount_10; }
	inline void set_idealZoomAmount_10(float value)
	{
		___idealZoomAmount_10 = value;
	}

	inline static int32_t get_offset_of_zoomAmount_11() { return static_cast<int32_t>(offsetof(TBPinchZoom_t813254879, ___zoomAmount_11)); }
	inline float get_zoomAmount_11() const { return ___zoomAmount_11; }
	inline float* get_address_of_zoomAmount_11() { return &___zoomAmount_11; }
	inline void set_zoomAmount_11(float value)
	{
		___zoomAmount_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
