﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocalizationGenerated
struct LocalizationGenerated_t1868013270;
// Localization/LoadFunction
struct LoadFunction_t2234103444;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// JSVCall
struct JSVCall_t3708497963;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Collections.Generic.Dictionary`2<System.String,System.String[]>
struct Dictionary_2_t579454026;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;
// UnityEngine.TextAsset
struct TextAsset_t3836129977;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_TextAsset3836129977.h"
#include "mscorlib_System_String7231557.h"

// System.Void LocalizationGenerated::.ctor()
extern "C"  void LocalizationGenerated__ctor_m2938481493 (LocalizationGenerated_t1868013270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Localization/LoadFunction LocalizationGenerated::Localization_loadFunction_GetDelegate_member0_arg0(CSRepresentedObject)
extern "C"  LoadFunction_t2234103444 * LocalizationGenerated_Localization_loadFunction_GetDelegate_member0_arg0_m178481162 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalizationGenerated::Localization_loadFunction(JSVCall)
extern "C"  void LocalizationGenerated_Localization_loadFunction_m1012673680 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalizationGenerated::Localization_localizationHasBeenSet(JSVCall)
extern "C"  void LocalizationGenerated_Localization_localizationHasBeenSet_m2624779065 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalizationGenerated::Localization_dictionary(JSVCall)
extern "C"  void LocalizationGenerated_Localization_dictionary_m3355536344 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalizationGenerated::Localization_knownLanguages(JSVCall)
extern "C"  void LocalizationGenerated_Localization_knownLanguages_m3109776022 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalizationGenerated::Localization_language(JSVCall)
extern "C"  void LocalizationGenerated_Localization_language_m282438998 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalizationGenerated::Localization_Exists__String(JSVCall,System.Int32)
extern "C"  bool LocalizationGenerated_Localization_Exists__String_m3344342890 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalizationGenerated::Localization_Format__String__Object_Array(JSVCall,System.Int32)
extern "C"  bool LocalizationGenerated_Localization_Format__String__Object_Array_m1883251518 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalizationGenerated::Localization_Get__String(JSVCall,System.Int32)
extern "C"  bool LocalizationGenerated_Localization_Get__String_m260246796 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalizationGenerated::Localization_Load__TextAsset(JSVCall,System.Int32)
extern "C"  bool LocalizationGenerated_Localization_Load__TextAsset_m285578818 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalizationGenerated::Localization_LoadCSV__TextAsset__Boolean(JSVCall,System.Int32)
extern "C"  bool LocalizationGenerated_Localization_LoadCSV__TextAsset__Boolean_m2189651498 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalizationGenerated::Localization_LoadCSV__Byte_Array__Boolean(JSVCall,System.Int32)
extern "C"  bool LocalizationGenerated_Localization_LoadCSV__Byte_Array__Boolean_m734935075 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalizationGenerated::Localization_Set__String__Byte_Array(JSVCall,System.Int32)
extern "C"  bool LocalizationGenerated_Localization_Set__String__Byte_Array_m763002202 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalizationGenerated::Localization_Set__String__DictionaryT2_String_String(JSVCall,System.Int32)
extern "C"  bool LocalizationGenerated_Localization_Set__String__DictionaryT2_String_String_m730863116 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalizationGenerated::__Register()
extern "C"  void LocalizationGenerated___Register_m3277495954 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Localization/LoadFunction LocalizationGenerated::<Localization_loadFunction>m__6F()
extern "C"  LoadFunction_t2234103444 * LocalizationGenerated_U3CLocalization_loadFunctionU3Em__6F_m1099902999 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] LocalizationGenerated::<Localization_Format__String__Object_Array>m__70()
extern "C"  ObjectU5BU5D_t1108656482* LocalizationGenerated_U3CLocalization_Format__String__Object_ArrayU3Em__70_m2483374799 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] LocalizationGenerated::<Localization_LoadCSV__Byte_Array__Boolean>m__71()
extern "C"  ByteU5BU5D_t4260760469* LocalizationGenerated_U3CLocalization_LoadCSV__Byte_Array__BooleanU3Em__71_m169529726 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] LocalizationGenerated::<Localization_LoadCSV__Byte_Array__Boolean>m__72()
extern "C"  ByteU5BU5D_t4260760469* LocalizationGenerated_U3CLocalization_LoadCSV__Byte_Array__BooleanU3Em__72_m169530687 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] LocalizationGenerated::<Localization_Set__String__Byte_Array>m__73()
extern "C"  ByteU5BU5D_t4260760469* LocalizationGenerated_U3CLocalization_Set__String__Byte_ArrayU3Em__73_m2760421171 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalizationGenerated::ilo_getBooleanS1(System.Int32)
extern "C"  bool LocalizationGenerated_ilo_getBooleanS1_m26591471 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String[]> LocalizationGenerated::ilo_get_dictionary2()
extern "C"  Dictionary_2_t579454026 * LocalizationGenerated_ilo_get_dictionary2_m1450680643 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LocalizationGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t LocalizationGenerated_ilo_setObject3_m2913289339 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LocalizationGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * LocalizationGenerated_ilo_getObject4_m2126022835 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalizationGenerated::ilo_set_dictionary5(System.Collections.Generic.Dictionary`2<System.String,System.String[]>)
extern "C"  void LocalizationGenerated_ilo_set_dictionary5_m3162344405 (Il2CppObject * __this /* static, unused */, Dictionary_2_t579454026 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] LocalizationGenerated::ilo_get_knownLanguages6()
extern "C"  StringU5BU5D_t4054002952* LocalizationGenerated_ilo_get_knownLanguages6_m83919094 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalizationGenerated::ilo_setArrayS7(System.Int32,System.Int32,System.Boolean)
extern "C"  void LocalizationGenerated_ilo_setArrayS7_m506218196 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalizationGenerated::ilo_get_language8()
extern "C"  String_t* LocalizationGenerated_ilo_get_language8_m3726122458 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LocalizationGenerated::ilo_getStringS9(System.Int32)
extern "C"  String_t* LocalizationGenerated_ilo_getStringS9_m16185217 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalizationGenerated::ilo_setBooleanS10(System.Int32,System.Boolean)
extern "C"  void LocalizationGenerated_ilo_setBooleanS10_m3863585792 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalizationGenerated::ilo_Load11(UnityEngine.TextAsset)
extern "C"  void LocalizationGenerated_ilo_Load11_m2657432766 (Il2CppObject * __this /* static, unused */, TextAsset_t3836129977 * ___asset0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalizationGenerated::ilo_Set12(System.String,System.Byte[])
extern "C"  void LocalizationGenerated_ilo_Set12_m1479137154 (Il2CppObject * __this /* static, unused */, String_t* ___languageName0, ByteU5BU5D_t4260760469* ___bytes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LocalizationGenerated::ilo_isFunctionS13(System.Int32)
extern "C"  bool LocalizationGenerated_ilo_isFunctionS13_m1684226584 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Localization/LoadFunction LocalizationGenerated::ilo_Localization_loadFunction_GetDelegate_member0_arg014(CSRepresentedObject)
extern "C"  LoadFunction_t2234103444 * LocalizationGenerated_ilo_Localization_loadFunction_GetDelegate_member0_arg014_m1221345460 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LocalizationGenerated::ilo_getObject15(System.Int32)
extern "C"  int32_t LocalizationGenerated_ilo_getObject15_m2112738936 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LocalizationGenerated::ilo_getArrayLength16(System.Int32)
extern "C"  int32_t LocalizationGenerated_ilo_getArrayLength16_m524758899 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte LocalizationGenerated::ilo_getByte17(System.Int32)
extern "C"  uint8_t LocalizationGenerated_ilo_getByte17_m1891331329 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LocalizationGenerated::ilo_getElement18(System.Int32,System.Int32)
extern "C"  int32_t LocalizationGenerated_ilo_getElement18_m1450775877 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
