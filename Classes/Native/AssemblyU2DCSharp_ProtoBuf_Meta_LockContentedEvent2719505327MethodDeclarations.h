﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.LockContentedEventHandler
struct LockContentedEventHandler_t2719505327;
// System.Object
struct Il2CppObject;
// ProtoBuf.Meta.LockContentedEventArgs
struct LockContentedEventArgs_t2181263112;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_LockContentedEvent2181263112.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ProtoBuf.Meta.LockContentedEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void LockContentedEventHandler__ctor_m2389377102 (LockContentedEventHandler_t2719505327 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.LockContentedEventHandler::Invoke(System.Object,ProtoBuf.Meta.LockContentedEventArgs)
extern "C"  void LockContentedEventHandler_Invoke_m2896675274 (LockContentedEventHandler_t2719505327 * __this, Il2CppObject * ___sender0, LockContentedEventArgs_t2181263112 * ___args1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ProtoBuf.Meta.LockContentedEventHandler::BeginInvoke(System.Object,ProtoBuf.Meta.LockContentedEventArgs,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * LockContentedEventHandler_BeginInvoke_m1893458873 (LockContentedEventHandler_t2719505327 * __this, Il2CppObject * ___sender0, LockContentedEventArgs_t2181263112 * ___args1, AsyncCallback_t1369114871 * ___callback2, Il2CppObject * ___object3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Meta.LockContentedEventHandler::EndInvoke(System.IAsyncResult)
extern "C"  void LockContentedEventHandler_EndInvoke_m838256990 (LockContentedEventHandler_t2719505327 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
