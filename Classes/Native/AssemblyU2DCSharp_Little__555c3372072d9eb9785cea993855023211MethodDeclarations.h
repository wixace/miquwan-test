﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._555c3372072d9eb9785cea99dc6854bd
struct _555c3372072d9eb9785cea99dc6854bd_t3855023211;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._555c3372072d9eb9785cea99dc6854bd::.ctor()
extern "C"  void _555c3372072d9eb9785cea99dc6854bd__ctor_m1523184162 (_555c3372072d9eb9785cea99dc6854bd_t3855023211 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._555c3372072d9eb9785cea99dc6854bd::_555c3372072d9eb9785cea99dc6854bdm2(System.Int32)
extern "C"  int32_t _555c3372072d9eb9785cea99dc6854bd__555c3372072d9eb9785cea99dc6854bdm2_m4056277433 (_555c3372072d9eb9785cea99dc6854bd_t3855023211 * __this, int32_t ____555c3372072d9eb9785cea99dc6854bda0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._555c3372072d9eb9785cea99dc6854bd::_555c3372072d9eb9785cea99dc6854bdm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _555c3372072d9eb9785cea99dc6854bd__555c3372072d9eb9785cea99dc6854bdm_m3253938461 (_555c3372072d9eb9785cea99dc6854bd_t3855023211 * __this, int32_t ____555c3372072d9eb9785cea99dc6854bda0, int32_t ____555c3372072d9eb9785cea99dc6854bd401, int32_t ____555c3372072d9eb9785cea99dc6854bdc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
