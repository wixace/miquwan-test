﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateMethodCall>c__AnonStorey141`1<System.Object>
struct U3CCreateMethodCallU3Ec__AnonStorey141_1_t1149710483;
// System.Object
struct Il2CppObject;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateMethodCall>c__AnonStorey141`1<System.Object>::.ctor()
extern "C"  void U3CCreateMethodCallU3Ec__AnonStorey141_1__ctor_m1978002130_gshared (U3CCreateMethodCallU3Ec__AnonStorey141_1_t1149710483 * __this, const MethodInfo* method);
#define U3CCreateMethodCallU3Ec__AnonStorey141_1__ctor_m1978002130(__this, method) ((  void (*) (U3CCreateMethodCallU3Ec__AnonStorey141_1_t1149710483 *, const MethodInfo*))U3CCreateMethodCallU3Ec__AnonStorey141_1__ctor_m1978002130_gshared)(__this, method)
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateMethodCall>c__AnonStorey141`1<System.Object>::<>m__3A6(T,System.Object[])
extern "C"  Il2CppObject * U3CCreateMethodCallU3Ec__AnonStorey141_1_U3CU3Em__3A6_m3580767078_gshared (U3CCreateMethodCallU3Ec__AnonStorey141_1_t1149710483 * __this, Il2CppObject * ___o0, ObjectU5BU5D_t1108656482* ___a1, const MethodInfo* method);
#define U3CCreateMethodCallU3Ec__AnonStorey141_1_U3CU3Em__3A6_m3580767078(__this, ___o0, ___a1, method) ((  Il2CppObject * (*) (U3CCreateMethodCallU3Ec__AnonStorey141_1_t1149710483 *, Il2CppObject *, ObjectU5BU5D_t1108656482*, const MethodInfo*))U3CCreateMethodCallU3Ec__AnonStorey141_1_U3CU3Em__3A6_m3580767078_gshared)(__this, ___o0, ___a1, method)
// System.Object Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateMethodCall>c__AnonStorey141`1<System.Object>::<>m__3A7(T,System.Object[])
extern "C"  Il2CppObject * U3CCreateMethodCallU3Ec__AnonStorey141_1_U3CU3Em__3A7_m3171942853_gshared (U3CCreateMethodCallU3Ec__AnonStorey141_1_t1149710483 * __this, Il2CppObject * ___o0, ObjectU5BU5D_t1108656482* ___a1, const MethodInfo* method);
#define U3CCreateMethodCallU3Ec__AnonStorey141_1_U3CU3Em__3A7_m3171942853(__this, ___o0, ___a1, method) ((  Il2CppObject * (*) (U3CCreateMethodCallU3Ec__AnonStorey141_1_t1149710483 *, Il2CppObject *, ObjectU5BU5D_t1108656482*, const MethodInfo*))U3CCreateMethodCallU3Ec__AnonStorey141_1_U3CU3Em__3A7_m3171942853_gshared)(__this, ___o0, ___a1, method)
