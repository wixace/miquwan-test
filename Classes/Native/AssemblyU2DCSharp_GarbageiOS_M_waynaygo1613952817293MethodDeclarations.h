﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_waynaygo161
struct M_waynaygo161_t3952817293;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_waynaygo1613952817293.h"

// System.Void GarbageiOS.M_waynaygo161::.ctor()
extern "C"  void M_waynaygo161__ctor_m2164069750 (M_waynaygo161_t3952817293 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_waynaygo161::M_kalcair0(System.String[],System.Int32)
extern "C"  void M_waynaygo161_M_kalcair0_m3942713674 (M_waynaygo161_t3952817293 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_waynaygo161::M_learjelelKalla1(System.String[],System.Int32)
extern "C"  void M_waynaygo161_M_learjelelKalla1_m841754413 (M_waynaygo161_t3952817293 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_waynaygo161::M_hisstuDoukai2(System.String[],System.Int32)
extern "C"  void M_waynaygo161_M_hisstuDoukai2_m4112665152 (M_waynaygo161_t3952817293 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_waynaygo161::M_fasjeSile3(System.String[],System.Int32)
extern "C"  void M_waynaygo161_M_fasjeSile3_m1659581256 (M_waynaygo161_t3952817293 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_waynaygo161::M_yerederebea4(System.String[],System.Int32)
extern "C"  void M_waynaygo161_M_yerederebea4_m1232270208 (M_waynaygo161_t3952817293 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_waynaygo161::ilo_M_fasjeSile31(GarbageiOS.M_waynaygo161,System.String[],System.Int32)
extern "C"  void M_waynaygo161_ilo_M_fasjeSile31_m304771795 (Il2CppObject * __this /* static, unused */, M_waynaygo161_t3952817293 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
