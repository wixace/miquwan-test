﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.InternalEmitParticleArguments
struct  InternalEmitParticleArguments_t1613430166 
{
public:
	// UnityEngine.Vector3 UnityEngine.InternalEmitParticleArguments::pos
	Vector3_t4282066566  ___pos_0;
	// UnityEngine.Vector3 UnityEngine.InternalEmitParticleArguments::velocity
	Vector3_t4282066566  ___velocity_1;
	// System.Single UnityEngine.InternalEmitParticleArguments::size
	float ___size_2;
	// System.Single UnityEngine.InternalEmitParticleArguments::energy
	float ___energy_3;
	// UnityEngine.Color UnityEngine.InternalEmitParticleArguments::color
	Color_t4194546905  ___color_4;
	// System.Single UnityEngine.InternalEmitParticleArguments::rotation
	float ___rotation_5;
	// System.Single UnityEngine.InternalEmitParticleArguments::angularVelocity
	float ___angularVelocity_6;

public:
	inline static int32_t get_offset_of_pos_0() { return static_cast<int32_t>(offsetof(InternalEmitParticleArguments_t1613430166, ___pos_0)); }
	inline Vector3_t4282066566  get_pos_0() const { return ___pos_0; }
	inline Vector3_t4282066566 * get_address_of_pos_0() { return &___pos_0; }
	inline void set_pos_0(Vector3_t4282066566  value)
	{
		___pos_0 = value;
	}

	inline static int32_t get_offset_of_velocity_1() { return static_cast<int32_t>(offsetof(InternalEmitParticleArguments_t1613430166, ___velocity_1)); }
	inline Vector3_t4282066566  get_velocity_1() const { return ___velocity_1; }
	inline Vector3_t4282066566 * get_address_of_velocity_1() { return &___velocity_1; }
	inline void set_velocity_1(Vector3_t4282066566  value)
	{
		___velocity_1 = value;
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(InternalEmitParticleArguments_t1613430166, ___size_2)); }
	inline float get_size_2() const { return ___size_2; }
	inline float* get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(float value)
	{
		___size_2 = value;
	}

	inline static int32_t get_offset_of_energy_3() { return static_cast<int32_t>(offsetof(InternalEmitParticleArguments_t1613430166, ___energy_3)); }
	inline float get_energy_3() const { return ___energy_3; }
	inline float* get_address_of_energy_3() { return &___energy_3; }
	inline void set_energy_3(float value)
	{
		___energy_3 = value;
	}

	inline static int32_t get_offset_of_color_4() { return static_cast<int32_t>(offsetof(InternalEmitParticleArguments_t1613430166, ___color_4)); }
	inline Color_t4194546905  get_color_4() const { return ___color_4; }
	inline Color_t4194546905 * get_address_of_color_4() { return &___color_4; }
	inline void set_color_4(Color_t4194546905  value)
	{
		___color_4 = value;
	}

	inline static int32_t get_offset_of_rotation_5() { return static_cast<int32_t>(offsetof(InternalEmitParticleArguments_t1613430166, ___rotation_5)); }
	inline float get_rotation_5() const { return ___rotation_5; }
	inline float* get_address_of_rotation_5() { return &___rotation_5; }
	inline void set_rotation_5(float value)
	{
		___rotation_5 = value;
	}

	inline static int32_t get_offset_of_angularVelocity_6() { return static_cast<int32_t>(offsetof(InternalEmitParticleArguments_t1613430166, ___angularVelocity_6)); }
	inline float get_angularVelocity_6() const { return ___angularVelocity_6; }
	inline float* get_address_of_angularVelocity_6() { return &___angularVelocity_6; }
	inline void set_angularVelocity_6(float value)
	{
		___angularVelocity_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: UnityEngine.InternalEmitParticleArguments
struct InternalEmitParticleArguments_t1613430166_marshaled_pinvoke
{
	Vector3_t4282066566_marshaled_pinvoke ___pos_0;
	Vector3_t4282066566_marshaled_pinvoke ___velocity_1;
	float ___size_2;
	float ___energy_3;
	Color_t4194546905_marshaled_pinvoke ___color_4;
	float ___rotation_5;
	float ___angularVelocity_6;
};
// Native definition for marshalling of: UnityEngine.InternalEmitParticleArguments
struct InternalEmitParticleArguments_t1613430166_marshaled_com
{
	Vector3_t4282066566_marshaled_com ___pos_0;
	Vector3_t4282066566_marshaled_com ___velocity_1;
	float ___size_2;
	float ___energy_3;
	Color_t4194546905_marshaled_com ___color_4;
	float ___rotation_5;
	float ___angularVelocity_6;
};
