﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MemberID
struct MemberID_t3710171477;

#include "codegen/il2cpp-codegen.h"

// System.Void MemberID::.ctor()
extern "C"  void MemberID__ctor_m3808343270 (MemberID_t3710171477 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
