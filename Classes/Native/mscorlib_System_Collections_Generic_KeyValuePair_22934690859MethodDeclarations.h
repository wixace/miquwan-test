﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Mesh,UnityEngine.Vector3[]>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2086884892(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2934690859 *, Mesh_t4241756145 *, Vector3U5BU5D_t215400611*, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Mesh,UnityEngine.Vector3[]>::get_Key()
#define KeyValuePair_2_get_Key_m1964856908(__this, method) ((  Mesh_t4241756145 * (*) (KeyValuePair_2_t2934690859 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Mesh,UnityEngine.Vector3[]>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1874698893(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2934690859 *, Mesh_t4241756145 *, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Mesh,UnityEngine.Vector3[]>::get_Value()
#define KeyValuePair_2_get_Value_m4025190540(__this, method) ((  Vector3U5BU5D_t215400611* (*) (KeyValuePair_2_t2934690859 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Mesh,UnityEngine.Vector3[]>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m809166861(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2934690859 *, Vector3U5BU5D_t215400611*, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Mesh,UnityEngine.Vector3[]>::ToString()
#define KeyValuePair_2_ToString_m3906999669(__this, method) ((  String_t* (*) (KeyValuePair_2_t2934690859 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
