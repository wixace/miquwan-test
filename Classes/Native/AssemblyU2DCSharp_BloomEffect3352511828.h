﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t3191267369;
// UnityEngine.Material
struct Material_t3870600107;

#include "AssemblyU2DCSharp_PostEffectBase3382443234.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BloomEffect
struct  BloomEffect_t3352511828  : public PostEffectBase_t3382443234
{
public:
	// System.Int32 BloomEffect::downSample
	int32_t ___downSample_2;
	// System.Int32 BloomEffect::samplerScale
	int32_t ___samplerScale_3;
	// UnityEngine.Color BloomEffect::colorThreshold
	Color_t4194546905  ___colorThreshold_4;
	// UnityEngine.Color BloomEffect::bloomColor
	Color_t4194546905  ___bloomColor_5;
	// System.Single BloomEffect::bloomFactor
	float ___bloomFactor_6;
	// UnityEngine.Vector2 BloomEffect::blurCenter
	Vector2_t4282066565  ___blurCenter_7;
	// System.Single BloomEffect::lerp
	float ___lerp_8;
	// UnityEngine.Shader BloomEffect::curShader
	Shader_t3191267369 * ___curShader_9;
	// UnityEngine.Material BloomEffect::_Material
	Material_t3870600107 * ____Material_10;

public:
	inline static int32_t get_offset_of_downSample_2() { return static_cast<int32_t>(offsetof(BloomEffect_t3352511828, ___downSample_2)); }
	inline int32_t get_downSample_2() const { return ___downSample_2; }
	inline int32_t* get_address_of_downSample_2() { return &___downSample_2; }
	inline void set_downSample_2(int32_t value)
	{
		___downSample_2 = value;
	}

	inline static int32_t get_offset_of_samplerScale_3() { return static_cast<int32_t>(offsetof(BloomEffect_t3352511828, ___samplerScale_3)); }
	inline int32_t get_samplerScale_3() const { return ___samplerScale_3; }
	inline int32_t* get_address_of_samplerScale_3() { return &___samplerScale_3; }
	inline void set_samplerScale_3(int32_t value)
	{
		___samplerScale_3 = value;
	}

	inline static int32_t get_offset_of_colorThreshold_4() { return static_cast<int32_t>(offsetof(BloomEffect_t3352511828, ___colorThreshold_4)); }
	inline Color_t4194546905  get_colorThreshold_4() const { return ___colorThreshold_4; }
	inline Color_t4194546905 * get_address_of_colorThreshold_4() { return &___colorThreshold_4; }
	inline void set_colorThreshold_4(Color_t4194546905  value)
	{
		___colorThreshold_4 = value;
	}

	inline static int32_t get_offset_of_bloomColor_5() { return static_cast<int32_t>(offsetof(BloomEffect_t3352511828, ___bloomColor_5)); }
	inline Color_t4194546905  get_bloomColor_5() const { return ___bloomColor_5; }
	inline Color_t4194546905 * get_address_of_bloomColor_5() { return &___bloomColor_5; }
	inline void set_bloomColor_5(Color_t4194546905  value)
	{
		___bloomColor_5 = value;
	}

	inline static int32_t get_offset_of_bloomFactor_6() { return static_cast<int32_t>(offsetof(BloomEffect_t3352511828, ___bloomFactor_6)); }
	inline float get_bloomFactor_6() const { return ___bloomFactor_6; }
	inline float* get_address_of_bloomFactor_6() { return &___bloomFactor_6; }
	inline void set_bloomFactor_6(float value)
	{
		___bloomFactor_6 = value;
	}

	inline static int32_t get_offset_of_blurCenter_7() { return static_cast<int32_t>(offsetof(BloomEffect_t3352511828, ___blurCenter_7)); }
	inline Vector2_t4282066565  get_blurCenter_7() const { return ___blurCenter_7; }
	inline Vector2_t4282066565 * get_address_of_blurCenter_7() { return &___blurCenter_7; }
	inline void set_blurCenter_7(Vector2_t4282066565  value)
	{
		___blurCenter_7 = value;
	}

	inline static int32_t get_offset_of_lerp_8() { return static_cast<int32_t>(offsetof(BloomEffect_t3352511828, ___lerp_8)); }
	inline float get_lerp_8() const { return ___lerp_8; }
	inline float* get_address_of_lerp_8() { return &___lerp_8; }
	inline void set_lerp_8(float value)
	{
		___lerp_8 = value;
	}

	inline static int32_t get_offset_of_curShader_9() { return static_cast<int32_t>(offsetof(BloomEffect_t3352511828, ___curShader_9)); }
	inline Shader_t3191267369 * get_curShader_9() const { return ___curShader_9; }
	inline Shader_t3191267369 ** get_address_of_curShader_9() { return &___curShader_9; }
	inline void set_curShader_9(Shader_t3191267369 * value)
	{
		___curShader_9 = value;
		Il2CppCodeGenWriteBarrier(&___curShader_9, value);
	}

	inline static int32_t get_offset_of__Material_10() { return static_cast<int32_t>(offsetof(BloomEffect_t3352511828, ____Material_10)); }
	inline Material_t3870600107 * get__Material_10() const { return ____Material_10; }
	inline Material_t3870600107 ** get_address_of__Material_10() { return &____Material_10; }
	inline void set__Material_10(Material_t3870600107 * value)
	{
		____Material_10 = value;
		Il2CppCodeGenWriteBarrier(&____Material_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
