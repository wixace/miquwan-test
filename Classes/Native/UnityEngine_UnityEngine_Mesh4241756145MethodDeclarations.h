﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Mesh
struct Mesh_t4241756145;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// System.Object
struct Il2CppObject;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t701588350;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t1355284823;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t1355284821;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;
// System.Collections.Generic.List`1<UnityEngine.Color>
struct List_1_t1267765161;
// UnityEngine.Color32[]
struct Color32U5BU5D_t2960766953;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t1967039240;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// UnityEngine.CombineInstance[]
struct CombineInstanceU5BU5D_t1850423023;
// UnityEngine.BoneWeight[]
struct BoneWeightU5BU5D_t1503835233;
// UnityEngine.Matrix4x4[]
struct Matrix4x4U5BU5D_t1421664456;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_MeshTopology2348044928.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine.Mesh::.ctor()
extern "C"  void Mesh__ctor_m2684203808 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
extern "C"  void Mesh_Internal_Create_m3749730360 (Il2CppObject * __this /* static, unused */, Mesh_t4241756145 * ___mono0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::Clear(System.Boolean)
extern "C"  void Mesh_Clear_m1789068674 (Mesh_t4241756145 * __this, bool ___keepVertexLayout0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::Clear()
extern "C"  void Mesh_Clear_m90337099 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mesh::get_isReadable()
extern "C"  bool Mesh_get_isReadable_m466758317 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mesh::get_canAccess()
extern "C"  bool Mesh_get_canAccess_m3042962979 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
extern "C"  Vector3U5BU5D_t215400611* Mesh_get_vertices_m3685486174 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
extern "C"  void Mesh_set_vertices_m2628866109 (Mesh_t4241756145 * __this, Vector3U5BU5D_t215400611* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetVertices(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void Mesh_SetVertices_m701834806 (Mesh_t4241756145 * __this, List_1_t1355284822 * ___inVertices0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetVerticesInternal(System.Object)
extern "C"  void Mesh_SetVerticesInternal_m1274639230 (Mesh_t4241756145 * __this, Il2CppObject * ___vertices0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine.Mesh::get_normals()
extern "C"  Vector3U5BU5D_t215400611* Mesh_get_normals_m3396909641 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_normals(UnityEngine.Vector3[])
extern "C"  void Mesh_set_normals_m3763698282 (Mesh_t4241756145 * __this, Vector3U5BU5D_t215400611* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetNormals(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void Mesh_SetNormals_m2039144779 (Mesh_t4241756145 * __this, List_1_t1355284822 * ___inNormals0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetNormalsInternal(System.Object)
extern "C"  void Mesh_SetNormalsInternal_m1710845385 (Mesh_t4241756145 * __this, Il2CppObject * ___normals0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4[] UnityEngine.Mesh::get_tangents()
extern "C"  Vector4U5BU5D_t701588350* Mesh_get_tangents_m3235865682 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_tangents(UnityEngine.Vector4[])
extern "C"  void Mesh_set_tangents_m2117559847 (Mesh_t4241756145 * __this, Vector4U5BU5D_t701588350* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetTangents(System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern "C"  void Mesh_SetTangents_m2005345740 (Mesh_t4241756145 * __this, List_1_t1355284823 * ___inTangents0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetTangentsInternal(System.Object)
extern "C"  void Mesh_SetTangentsInternal_m763303177 (Mesh_t4241756145 * __this, Il2CppObject * ___tangents0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv()
extern "C"  Vector2U5BU5D_t4024180168* Mesh_get_uv_m558008935 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])
extern "C"  void Mesh_set_uv_m498907190 (Mesh_t4241756145 * __this, Vector2U5BU5D_t4024180168* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv2()
extern "C"  Vector2U5BU5D_t4024180168* Mesh_get_uv2_m118417421 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_uv2(UnityEngine.Vector2[])
extern "C"  void Mesh_set_uv2_m1515914022 (Mesh_t4241756145 * __this, Vector2U5BU5D_t4024180168* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv3()
extern "C"  Vector2U5BU5D_t4024180168* Mesh_get_uv3_m118418382 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_uv3(UnityEngine.Vector2[])
extern "C"  void Mesh_set_uv3_m1448907269 (Mesh_t4241756145 * __this, Vector2U5BU5D_t4024180168* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] UnityEngine.Mesh::get_uv4()
extern "C"  Vector2U5BU5D_t4024180168* Mesh_get_uv4_m118419343 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_uv4(UnityEngine.Vector2[])
extern "C"  void Mesh_set_uv4_m1381900516 (Mesh_t4241756145 * __this, Vector2U5BU5D_t4024180168* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Array UnityEngine.Mesh::ExtractListData(System.Object)
extern "C"  Il2CppArray * Mesh_ExtractListData_m2453560898 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetUVsInternal(System.Array,System.Int32,System.Int32,System.Int32)
extern "C"  void Mesh_SetUVsInternal_m4014622176 (Mesh_t4241756145 * __this, Il2CppArray * ___uvs0, int32_t ___channel1, int32_t ___dim2, int32_t ___arraySize3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::GetUVsInternal(System.Array,System.Int32,System.Int32)
extern "C"  void Mesh_GetUVsInternal_m156495075 (Mesh_t4241756145 * __this, Il2CppArray * ___uvs0, int32_t ___channel1, int32_t ___dim2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mesh::CheckCanAccessUVChannel(System.Int32)
extern "C"  bool Mesh_CheckCanAccessUVChannel_m1325919903 (Mesh_t4241756145 * __this, int32_t ___channel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::ResizeList(System.Object,System.Int32)
extern "C"  void Mesh_ResizeList_m1846456089 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___list0, int32_t ___size1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern "C"  void Mesh_SetUVs_m116216925 (Mesh_t4241756145 * __this, int32_t ___channel0, List_1_t1355284821 * ___uvs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void Mesh_SetUVs_m116217886 (Mesh_t4241756145 * __this, int32_t ___channel0, List_1_t1355284822 * ___uvs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern "C"  void Mesh_SetUVs_m116218847 (Mesh_t4241756145 * __this, int32_t ___channel0, List_1_t1355284823 * ___uvs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::GetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern "C"  void Mesh_GetUVs_m2596492113 (Mesh_t4241756145 * __this, int32_t ___channel0, List_1_t1355284821 * ___uvs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::GetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void Mesh_GetUVs_m2596493074 (Mesh_t4241756145 * __this, int32_t ___channel0, List_1_t1355284822 * ___uvs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::GetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern "C"  void Mesh_GetUVs_m2596494035 (Mesh_t4241756145 * __this, int32_t ___channel0, List_1_t1355284823 * ___uvs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UnityEngine.Mesh::get_bounds()
extern "C"  Bounds_t2711641849  Mesh_get_bounds_m1625335237 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_bounds(UnityEngine.Bounds)
extern "C"  void Mesh_set_bounds_m1860910602 (Mesh_t4241756145 * __this, Bounds_t2711641849  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::INTERNAL_get_bounds(UnityEngine.Bounds&)
extern "C"  void Mesh_INTERNAL_get_bounds_m739771994 (Mesh_t4241756145 * __this, Bounds_t2711641849 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::INTERNAL_set_bounds(UnityEngine.Bounds&)
extern "C"  void Mesh_INTERNAL_set_bounds_m654403942 (Mesh_t4241756145 * __this, Bounds_t2711641849 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine.Mesh::get_colors()
extern "C"  ColorU5BU5D_t2441545636* Mesh_get_colors_m641154978 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_colors(UnityEngine.Color[])
extern "C"  void Mesh_set_colors_m1701609395 (Mesh_t4241756145 * __this, ColorU5BU5D_t2441545636* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetColors(System.Collections.Generic.List`1<UnityEngine.Color>)
extern "C"  void Mesh_SetColors_m1076073024 (Mesh_t4241756145 * __this, List_1_t1267765161 * ___inColors0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetColorsInternal(System.Object)
extern "C"  void Mesh_SetColorsInternal_m1062792295 (Mesh_t4241756145 * __this, Il2CppObject * ___colors0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color32[] UnityEngine.Mesh::get_colors32()
extern "C"  Color32U5BU5D_t2960766953* Mesh_get_colors32_m192356802 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_colors32(UnityEngine.Color32[])
extern "C"  void Mesh_set_colors32_m119130293 (Mesh_t4241756145 * __this, Color32U5BU5D_t2960766953* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetColors(System.Collections.Generic.List`1<UnityEngine.Color32>)
extern "C"  void Mesh_SetColors_m3313707935 (Mesh_t4241756145 * __this, List_1_t1967039240 * ___inColors0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetColors32Internal(System.Object)
extern "C"  void Mesh_SetColors32Internal_m1830241000 (Mesh_t4241756145 * __this, Il2CppObject * ___colors0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::RecalculateBounds()
extern "C"  void Mesh_RecalculateBounds_m3754336742 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::RecalculateNormals()
extern "C"  void Mesh_RecalculateNormals_m1806625021 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::Optimize()
extern "C"  void Mesh_Optimize_m386784321 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] UnityEngine.Mesh::get_triangles()
extern "C"  Int32U5BU5D_t3230847821* Mesh_get_triangles_m2145908418 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_triangles(System.Int32[])
extern "C"  void Mesh_set_triangles_m2341339867 (Mesh_t4241756145 * __this, Int32U5BU5D_t3230847821* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] UnityEngine.Mesh::GetTriangles(System.Int32)
extern "C"  Int32U5BU5D_t3230847821* Mesh_GetTriangles_m3189498632 (Mesh_t4241756145 * __this, int32_t ___submesh0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetTriangles(System.Int32[],System.Int32)
extern "C"  void Mesh_SetTriangles_m636210907 (Mesh_t4241756145 * __this, Int32U5BU5D_t3230847821* ___triangles0, int32_t ___submesh1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetTriangles(System.Collections.Generic.List`1<System.Int32>,System.Int32)
extern "C"  void Mesh_SetTriangles_m456382467 (Mesh_t4241756145 * __this, List_1_t2522024052 * ___inTriangles0, int32_t ___submesh1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetTrianglesInternal(System.Object,System.Int32)
extern "C"  void Mesh_SetTrianglesInternal_m2955775213 (Mesh_t4241756145 * __this, Il2CppObject * ___triangles0, int32_t ___submesh1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] UnityEngine.Mesh::GetIndices(System.Int32)
extern "C"  Int32U5BU5D_t3230847821* Mesh_GetIndices_m637494532 (Mesh_t4241756145 * __this, int32_t ___submesh0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::SetIndices(System.Int32[],UnityEngine.MeshTopology,System.Int32)
extern "C"  void Mesh_SetIndices_m83842044 (Mesh_t4241756145 * __this, Int32U5BU5D_t3230847821* ___indices0, int32_t ___topology1, int32_t ___submesh2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.MeshTopology UnityEngine.Mesh::GetTopology(System.Int32)
extern "C"  int32_t Mesh_GetTopology_m242197656 (Mesh_t4241756145 * __this, int32_t ___submesh0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mesh::get_vertexCount()
extern "C"  int32_t Mesh_get_vertexCount_m538235264 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mesh::get_subMeshCount()
extern "C"  int32_t Mesh_get_subMeshCount_m4159827535 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_subMeshCount(System.Int32)
extern "C"  void Mesh_set_subMeshCount_m1647706132 (Mesh_t4241756145 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::CombineMeshes(UnityEngine.CombineInstance[],System.Boolean,System.Boolean)
extern "C"  void Mesh_CombineMeshes_m3216282747 (Mesh_t4241756145 * __this, CombineInstanceU5BU5D_t1850423023* ___combine0, bool ___mergeSubMeshes1, bool ___useMatrices2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::CombineMeshes(UnityEngine.CombineInstance[],System.Boolean)
extern "C"  void Mesh_CombineMeshes_m2497553090 (Mesh_t4241756145 * __this, CombineInstanceU5BU5D_t1850423023* ___combine0, bool ___mergeSubMeshes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::CombineMeshes(UnityEngine.CombineInstance[])
extern "C"  void Mesh_CombineMeshes_m2934484827 (Mesh_t4241756145 * __this, CombineInstanceU5BU5D_t1850423023* ___combine0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.BoneWeight[] UnityEngine.Mesh::get_boneWeights()
extern "C"  BoneWeightU5BU5D_t1503835233* Mesh_get_boneWeights_m2083262386 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_boneWeights(UnityEngine.BoneWeight[])
extern "C"  void Mesh_set_boneWeights_m3598851843 (Mesh_t4241756145 * __this, BoneWeightU5BU5D_t1503835233* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4[] UnityEngine.Mesh::get_bindposes()
extern "C"  Matrix4x4U5BU5D_t1421664456* Mesh_get_bindposes_m2757156481 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::set_bindposes(UnityEngine.Matrix4x4[])
extern "C"  void Mesh_set_bindposes_m1849173170 (Mesh_t4241756145 * __this, Matrix4x4U5BU5D_t1421664456* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::MarkDynamic()
extern "C"  void Mesh_MarkDynamic_m2243343024 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::UploadMeshData(System.Boolean)
extern "C"  void Mesh_UploadMeshData_m3515607347 (Mesh_t4241756145 * __this, bool ___markNoLogerReadable0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mesh::GetBlendShapeIndex(System.String)
extern "C"  int32_t Mesh_GetBlendShapeIndex_m3879257074 (Mesh_t4241756145 * __this, String_t* ___blendShapeName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mesh::get_blendShapeCount()
extern "C"  int32_t Mesh_get_blendShapeCount_m1875056084 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Mesh::GetBlendShapeName(System.Int32)
extern "C"  String_t* Mesh_GetBlendShapeName_m1681794749 (Mesh_t4241756145 * __this, int32_t ___shapeIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mesh::GetBlendShapeFrameCount(System.Int32)
extern "C"  int32_t Mesh_GetBlendShapeFrameCount_m832163543 (Mesh_t4241756145 * __this, int32_t ___shapeIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mesh::GetBlendShapeFrameWeight(System.Int32,System.Int32)
extern "C"  float Mesh_GetBlendShapeFrameWeight_m1276640631 (Mesh_t4241756145 * __this, int32_t ___shapeIndex0, int32_t ___frameIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::GetBlendShapeFrameVertices(System.Int32,System.Int32,UnityEngine.Vector3[],UnityEngine.Vector3[],UnityEngine.Vector3[])
extern "C"  void Mesh_GetBlendShapeFrameVertices_m2153543797 (Mesh_t4241756145 * __this, int32_t ___shapeIndex0, int32_t ___frameIndex1, Vector3U5BU5D_t215400611* ___deltaVertices2, Vector3U5BU5D_t215400611* ___deltaNormals3, Vector3U5BU5D_t215400611* ___deltaTangents4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::ClearBlendShapes()
extern "C"  void Mesh_ClearBlendShapes_m634956858 (Mesh_t4241756145 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::AddBlendShapeFrame(System.String,System.Single,UnityEngine.Vector3[],UnityEngine.Vector3[],UnityEngine.Vector3[])
extern "C"  void Mesh_AddBlendShapeFrame_m2084910010 (Mesh_t4241756145 * __this, String_t* ___shapeName0, float ___frameWeight1, Vector3U5BU5D_t215400611* ___deltaVertices2, Vector3U5BU5D_t215400611* ___deltaNormals3, Vector3U5BU5D_t215400611* ___deltaTangents4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
