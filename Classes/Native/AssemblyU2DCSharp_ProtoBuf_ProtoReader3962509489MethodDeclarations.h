﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// System.IO.Stream
struct Stream_t1561764144;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// ProtoBuf.SerializationContext
struct SerializationContext_t3997850667;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Exception
struct Exception_t3991598821;
// System.Object
struct Il2CppObject;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ProtoBuf.IExtensible
struct IExtensible_t1056931882;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// ProtoBuf.NetObjectCache
struct NetObjectCache_t514806898;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "AssemblyU2DCSharp_ProtoBuf_SerializationContext3997850667.h"
#include "AssemblyU2DCSharp_ProtoBuf_WireType2355646059.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_SubItemToken3365128146.h"
#include "AssemblyU2DCSharp_ProtoBuf_PrefixStyle1974492709.h"
#include "mscorlib_System_Exception3991598821.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "AssemblyU2DCSharp_ProtoBuf_DataFormat274207885.h"
#include "AssemblyU2DCSharp_ProtoBuf_NetObjectCache514806898.h"

// System.Void ProtoBuf.ProtoReader::.ctor(System.IO.Stream,ProtoBuf.Meta.TypeModel,ProtoBuf.SerializationContext)
extern "C"  void ProtoReader__ctor_m3693820343 (ProtoReader_t3962509489 * __this, Stream_t1561764144 * ___source0, TypeModel_t2730011105 * ___model1, SerializationContext_t3997850667 * ___context2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::.ctor(System.IO.Stream,ProtoBuf.Meta.TypeModel,ProtoBuf.SerializationContext,System.Int32)
extern "C"  void ProtoReader__ctor_m1212654720 (ProtoReader_t3962509489 * __this, Stream_t1561764144 * ___source0, TypeModel_t2730011105 * ___model1, SerializationContext_t3997850667 * ___context2, int32_t ___length3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::.cctor()
extern "C"  void ProtoReader__cctor_m2486028602 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoReader::get_FieldNumber()
extern "C"  int32_t ProtoReader_get_FieldNumber_m2585639709 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.WireType ProtoBuf.ProtoReader::get_WireType()
extern "C"  int32_t ProtoReader_get_WireType_m2317237962 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoReader::get_InternStrings()
extern "C"  bool ProtoReader_get_InternStrings_m2939778532 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::set_InternStrings(System.Boolean)
extern "C"  void ProtoReader_set_InternStrings_m2458535323 (ProtoReader_t3962509489 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::Init(ProtoBuf.ProtoReader,System.IO.Stream,ProtoBuf.Meta.TypeModel,ProtoBuf.SerializationContext,System.Int32)
extern "C"  void ProtoReader_Init_m497993926 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ___reader0, Stream_t1561764144 * ___source1, TypeModel_t2730011105 * ___model2, SerializationContext_t3997850667 * ___context3, int32_t ___length4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.SerializationContext ProtoBuf.ProtoReader::get_Context()
extern "C"  SerializationContext_t3997850667 * ProtoReader_get_Context_m1327595462 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::Dispose()
extern "C"  void ProtoReader_Dispose_m1715839056 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoReader::TryReadUInt32VariantWithoutMoving(System.Boolean,System.UInt32&)
extern "C"  int32_t ProtoReader_TryReadUInt32VariantWithoutMoving_m948594531 (ProtoReader_t3962509489 * __this, bool ___trimNegative0, uint32_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ProtoBuf.ProtoReader::ReadUInt32Variant(System.Boolean)
extern "C"  uint32_t ProtoReader_ReadUInt32Variant_m3086544995 (ProtoReader_t3962509489 * __this, bool ___trimNegative0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoReader::TryReadUInt32Variant(System.UInt32&)
extern "C"  bool ProtoReader_TryReadUInt32Variant_m4181358290 (ProtoReader_t3962509489 * __this, uint32_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ProtoBuf.ProtoReader::ReadUInt32()
extern "C"  uint32_t ProtoReader_ReadUInt32_m1102198779 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoReader::get_Position()
extern "C"  int32_t ProtoReader_get_Position_m2367177873 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::Ensure(System.Int32,System.Boolean)
extern "C"  void ProtoReader_Ensure_m1231666653 (ProtoReader_t3962509489 * __this, int32_t ___count0, bool ___strict1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 ProtoBuf.ProtoReader::ReadInt16()
extern "C"  int16_t ProtoReader_ReadInt16_m1082782407 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 ProtoBuf.ProtoReader::ReadUInt16()
extern "C"  uint16_t ProtoReader_ReadUInt16_m3948845243 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte ProtoBuf.ProtoReader::ReadByte()
extern "C"  uint8_t ProtoReader_ReadByte_m35573083 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.SByte ProtoBuf.ProtoReader::ReadSByte()
extern "C"  int8_t ProtoReader_ReadSByte_m3612178133 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoReader::ReadInt32()
extern "C"  int32_t ProtoReader_ReadInt32_m2376482363 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoReader::Zag(System.UInt32)
extern "C"  int32_t ProtoReader_Zag_m363758743 (Il2CppObject * __this /* static, unused */, uint32_t ___ziggedValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ProtoBuf.ProtoReader::Zag(System.UInt64)
extern "C"  int64_t ProtoReader_Zag_m3560785561 (Il2CppObject * __this /* static, unused */, uint64_t ___ziggedValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 ProtoBuf.ProtoReader::ReadInt64()
extern "C"  int64_t ProtoReader_ReadInt64_m3088501625 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoReader::TryReadUInt64VariantWithoutMoving(System.UInt64&)
extern "C"  int32_t ProtoReader_TryReadUInt64VariantWithoutMoving_m4213109294 (ProtoReader_t3962509489 * __this, uint64_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 ProtoBuf.ProtoReader::ReadUInt64Variant()
extern "C"  uint64_t ProtoReader_ReadUInt64Variant_m1808636940 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.ProtoReader::Intern(System.String)
extern "C"  String_t* ProtoReader_Intern_m2690583868 (ProtoReader_t3962509489 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.ProtoReader::ReadString()
extern "C"  String_t* ProtoReader_ReadString_m1911715579 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::ThrowEnumException(System.Type,System.Int32)
extern "C"  void ProtoReader_ThrowEnumException_m2523665943 (ProtoReader_t3962509489 * __this, Type_t * ___type0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception ProtoBuf.ProtoReader::CreateWireTypeException()
extern "C"  Exception_t3991598821 * ProtoReader_CreateWireTypeException_m564330232 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception ProtoBuf.ProtoReader::CreateException(System.String)
extern "C"  Exception_t3991598821 * ProtoReader_CreateException_m2669558667 (ProtoReader_t3962509489 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double ProtoBuf.ProtoReader::ReadDouble()
extern "C"  double ProtoReader_ReadDouble_m150400763 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.ProtoReader::ReadObject(System.Object,System.Int32,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * ProtoReader_ReadObject_m2964570176 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, int32_t ___key1, ProtoReader_t3962509489 * ___reader2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.ProtoReader::ReadTypedObject(System.Object,System.Int32,ProtoBuf.ProtoReader,System.Type)
extern "C"  Il2CppObject * ProtoReader_ReadTypedObject_m1832257535 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, int32_t ___key1, ProtoReader_t3962509489 * ___reader2, Type_t * ___type3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::EndSubItem(ProtoBuf.SubItemToken,ProtoBuf.ProtoReader)
extern "C"  void ProtoReader_EndSubItem_m3760869086 (Il2CppObject * __this /* static, unused */, SubItemToken_t3365128146  ___token0, ProtoReader_t3962509489 * ___reader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.SubItemToken ProtoBuf.ProtoReader::StartSubItem(ProtoBuf.ProtoReader)
extern "C"  SubItemToken_t3365128146  ProtoReader_StartSubItem_m1355391812 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoReader::ReadFieldHeader()
extern "C"  int32_t ProtoReader_ReadFieldHeader_m3895580820 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoReader::TryReadFieldHeader(System.Int32)
extern "C"  bool ProtoReader_TryReadFieldHeader_m287839372 (ProtoReader_t3962509489 * __this, int32_t ___field0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.TypeModel ProtoBuf.ProtoReader::get_Model()
extern "C"  TypeModel_t2730011105 * ProtoReader_get_Model_m2485789735 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::Hint(ProtoBuf.WireType)
extern "C"  void ProtoReader_Hint_m3250801944 (ProtoReader_t3962509489 * __this, int32_t ___wireType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::Assert(ProtoBuf.WireType)
extern "C"  void ProtoReader_Assert_m3495865849 (ProtoReader_t3962509489 * __this, int32_t ___wireType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::SkipField()
extern "C"  void ProtoReader_SkipField_m953919980 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 ProtoBuf.ProtoReader::ReadUInt64()
extern "C"  uint64_t ProtoReader_ReadUInt64_m1697220571 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ProtoBuf.ProtoReader::ReadSingle()
extern "C"  float ProtoReader_ReadSingle_m1885135323 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoReader::ReadBoolean()
extern "C"  bool ProtoReader_ReadBoolean_m2050798575 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ProtoBuf.ProtoReader::AppendBytes(System.Byte[],ProtoBuf.ProtoReader)
extern "C"  ByteU5BU5D_t4260760469* ProtoReader_AppendBytes_m2935986189 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___value0, ProtoReader_t3962509489 * ___reader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoReader::ReadByteOrThrow(System.IO.Stream)
extern "C"  int32_t ProtoReader_ReadByteOrThrow_m3437351583 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoReader::ReadLengthPrefix(System.IO.Stream,System.Boolean,ProtoBuf.PrefixStyle,System.Int32&)
extern "C"  int32_t ProtoReader_ReadLengthPrefix_m596132614 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, bool ___expectHeader1, int32_t ___style2, int32_t* ___fieldNumber3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoReader::DirectReadLittleEndianInt32(System.IO.Stream)
extern "C"  int32_t ProtoReader_DirectReadLittleEndianInt32_m2530564536 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoReader::DirectReadBigEndianInt32(System.IO.Stream)
extern "C"  int32_t ProtoReader_DirectReadBigEndianInt32_m4293318088 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoReader::DirectReadVarintInt32(System.IO.Stream)
extern "C"  int32_t ProtoReader_DirectReadVarintInt32_m1107630305 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::DirectReadBytes(System.IO.Stream,System.Byte[],System.Int32,System.Int32)
extern "C"  void ProtoReader_DirectReadBytes_m1529436855 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, ByteU5BU5D_t4260760469* ___buffer1, int32_t ___offset2, int32_t ___count3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ProtoBuf.ProtoReader::DirectReadBytes(System.IO.Stream,System.Int32)
extern "C"  ByteU5BU5D_t4260760469* ProtoReader_DirectReadBytes_m1926313261 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ProtoBuf.ProtoReader::DirectReadString(System.IO.Stream,System.Int32)
extern "C"  String_t* ProtoReader_DirectReadString_m3686894300 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, int32_t ___length1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoReader::ReadLengthPrefix(System.IO.Stream,System.Boolean,ProtoBuf.PrefixStyle,System.Int32&,System.Int32&)
extern "C"  int32_t ProtoReader_ReadLengthPrefix_m3535535707 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, bool ___expectHeader1, int32_t ___style2, int32_t* ___fieldNumber3, int32_t* ___bytesRead4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoReader::TryReadUInt32Variant(System.IO.Stream,System.UInt32&)
extern "C"  int32_t ProtoReader_TryReadUInt32Variant_m2844709959 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, uint32_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::Seek(System.IO.Stream,System.Int32,System.Byte[])
extern "C"  void ProtoReader_Seek_m3857292922 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, int32_t ___count1, ByteU5BU5D_t4260760469* ___buffer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception ProtoBuf.ProtoReader::AddErrorData(System.Exception,ProtoBuf.ProtoReader)
extern "C"  Exception_t3991598821 * ProtoReader_AddErrorData_m2470571263 (Il2CppObject * __this /* static, unused */, Exception_t3991598821 * ___exception0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception ProtoBuf.ProtoReader::EoF(ProtoBuf.ProtoReader)
extern "C"  Exception_t3991598821 * ProtoReader_EoF_m396342776 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::AppendExtensionData(ProtoBuf.IExtensible)
extern "C"  void ProtoReader_AppendExtensionData_m1988577823 (ProtoReader_t3962509489 * __this, Il2CppObject * ___instance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::AppendExtensionField(ProtoBuf.ProtoWriter)
extern "C"  void ProtoReader_AppendExtensionField_m1602801294 (ProtoReader_t3962509489 * __this, ProtoWriter_t4117914721 * ___writer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoReader::HasSubValue(ProtoBuf.WireType,ProtoBuf.ProtoReader)
extern "C"  bool ProtoReader_HasSubValue_m259164438 (Il2CppObject * __this /* static, unused */, int32_t ___wireType0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoReader::GetTypeKey(System.Type&)
extern "C"  int32_t ProtoReader_GetTypeKey_m1642913761 (ProtoReader_t3962509489 * __this, Type_t ** ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.NetObjectCache ProtoBuf.ProtoReader::get_NetCache()
extern "C"  NetObjectCache_t514806898 * ProtoReader_get_NetCache_m2051816937 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.ProtoReader::DeserializeType(System.String)
extern "C"  Type_t * ProtoReader_DeserializeType_m2262053458 (ProtoReader_t3962509489 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::SetRootObject(System.Object)
extern "C"  void ProtoReader_SetRootObject_m1426799648 (ProtoReader_t3962509489 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::NoteObject(System.Object,ProtoBuf.ProtoReader)
extern "C"  void ProtoReader_NoteObject_m2255877196 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___reader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.ProtoReader::ReadType()
extern "C"  Type_t * ProtoReader_ReadType_m3879606043 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::TrapNextObject(System.Int32)
extern "C"  void ProtoReader_TrapNextObject_m602483393 (ProtoReader_t3962509489 * __this, int32_t ___newObjectKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::CheckFullyConsumed()
extern "C"  void ProtoReader_CheckFullyConsumed_m1155805499 (ProtoReader_t3962509489 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.ProtoReader::Merge(ProtoBuf.ProtoReader,System.Object,System.Object)
extern "C"  Il2CppObject * ProtoReader_Merge_m2940597608 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ___parent0, Il2CppObject * ___from1, Il2CppObject * ___to2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.ProtoReader ProtoBuf.ProtoReader::Create(System.IO.Stream,ProtoBuf.Meta.TypeModel,ProtoBuf.SerializationContext,System.Int32)
extern "C"  ProtoReader_t3962509489 * ProtoReader_Create_m3327146033 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, TypeModel_t2730011105 * ___model1, SerializationContext_t3997850667 * ___context2, int32_t ___len3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.ProtoReader ProtoBuf.ProtoReader::GetRecycled()
extern "C"  ProtoReader_t3962509489 * ProtoReader_GetRecycled_m1963571779 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::Recycle(ProtoBuf.ProtoReader)
extern "C"  void ProtoReader_Recycle_m3668229756 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception ProtoBuf.ProtoReader::ilo_EoF1(ProtoBuf.ProtoReader)
extern "C"  Exception_t3991598821 * ProtoReader_ilo_EoF1_m3168893886 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception ProtoBuf.ProtoReader::ilo_AddErrorData2(System.Exception,ProtoBuf.ProtoReader)
extern "C"  Exception_t3991598821 * ProtoReader_ilo_AddErrorData2_m4104059740 (Il2CppObject * __this /* static, unused */, Exception_t3991598821 * ___exception0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoReader::ilo_TryReadUInt32VariantWithoutMoving3(ProtoBuf.ProtoReader,System.Boolean,System.UInt32&)
extern "C"  int32_t ProtoReader_ilo_TryReadUInt32VariantWithoutMoving3_m3135633117 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, bool ___trimNegative1, uint32_t* ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 ProtoBuf.ProtoReader::ilo_ReadUInt644(ProtoBuf.ProtoReader)
extern "C"  uint64_t ProtoReader_ilo_ReadUInt644_m1841842176 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::ilo_ResizeAndFlushLeft5(System.Byte[]&,System.Int32,System.Int32,System.Int32)
extern "C"  void ProtoReader_ilo_ResizeAndFlushLeft5_m238928425 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469** ___buffer0, int32_t ___toFitAtLeastBytes1, int32_t ___copyFromIndex2, int32_t ___copyBytes3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ProtoBuf.ProtoReader::ilo_ReadUInt32Variant6(ProtoBuf.ProtoReader,System.Boolean)
extern "C"  uint32_t ProtoReader_ilo_ReadUInt32Variant6_m3780041734 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, bool ___trimNegative1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::ilo_Ensure7(ProtoBuf.ProtoReader,System.Int32,System.Boolean)
extern "C"  void ProtoReader_ilo_Ensure7_m810750741 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, int32_t ___count1, bool ___strict2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoReader::ilo_Zag8(System.UInt32)
extern "C"  int32_t ProtoReader_ilo_Zag8_m2579952912 (Il2CppObject * __this /* static, unused */, uint32_t ___ziggedValue0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception ProtoBuf.ProtoReader::ilo_CreateWireTypeException9(ProtoBuf.ProtoReader)
extern "C"  Exception_t3991598821 * ProtoReader_ilo_CreateWireTypeException9_m3746414478 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoReader::ilo_ReadInt3210(ProtoBuf.ProtoReader)
extern "C"  int32_t ProtoReader_ilo_ReadInt3210_m749235455 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single ProtoBuf.ProtoReader::ilo_ReadSingle11(ProtoBuf.ProtoReader)
extern "C"  float ProtoReader_ilo_ReadSingle11_m2472280614 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.SubItemToken ProtoBuf.ProtoReader::ilo_StartSubItem12(ProtoBuf.ProtoReader)
extern "C"  SubItemToken_t3365128146  ProtoReader_ilo_StartSubItem12_m2584364952 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoReader::ilo_TryDeserializeAuxiliaryType13(ProtoBuf.Meta.TypeModel,ProtoBuf.ProtoReader,ProtoBuf.DataFormat,System.Int32,System.Type,System.Object&,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern "C"  bool ProtoReader_ilo_TryDeserializeAuxiliaryType13_m4149616185 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, ProtoReader_t3962509489 * ___reader1, int32_t ___format2, int32_t ___tag3, Type_t * ___type4, Il2CppObject ** ___value5, bool ___skipOtherFields6, bool ___asListItem7, bool ___autoCreate8, bool ___insideList9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::ilo_ThrowUnexpectedType14(System.Type)
extern "C"  void ProtoReader_ilo_ThrowUnexpectedType14_m571035975 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception ProtoBuf.ProtoReader::ilo_CreateException15(ProtoBuf.ProtoReader,System.String)
extern "C"  Exception_t3991598821 * ProtoReader_ilo_CreateException15_m1885149916 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoReader::ilo_TryReadUInt32Variant16(ProtoBuf.ProtoReader,System.UInt32&)
extern "C"  bool ProtoReader_ilo_TryReadUInt32Variant16_m1064881224 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, uint32_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::ilo_Seek17(System.IO.Stream,System.Int32,System.Byte[])
extern "C"  void ProtoReader_ilo_Seek17_m3873340161 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, int32_t ___count1, ByteU5BU5D_t4260760469* ___buffer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::ilo_SkipField18(ProtoBuf.ProtoReader)
extern "C"  void ProtoReader_ilo_SkipField18_m3024470264 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 ProtoBuf.ProtoReader::ilo_ReadUInt64Variant19(ProtoBuf.ProtoReader)
extern "C"  uint64_t ProtoReader_ilo_ReadUInt64Variant19_m2358338905 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 ProtoBuf.ProtoReader::ilo_ReadUInt3220(ProtoBuf.ProtoReader)
extern "C"  uint32_t ProtoReader_ilo_ReadUInt3220_m1606301284 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.ProtoReader::ilo_IsInfinity21(System.Double)
extern "C"  bool ProtoReader_ilo_IsInfinity21_m3200697657 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::ilo_BlockCopy22(System.Byte[],System.Int32,System.Byte[],System.Int32,System.Int32)
extern "C"  void ProtoReader_ilo_BlockCopy22_m1540470737 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___from0, int32_t ___fromIndex1, ByteU5BU5D_t4260760469* ___to2, int32_t ___toIndex3, int32_t ___count4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoReader::ilo_ReadByteOrThrow23(System.IO.Stream)
extern "C"  int32_t ProtoReader_ilo_ReadByteOrThrow23_m2136600781 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoReader::ilo_TryReadUInt32Variant24(System.IO.Stream,System.UInt32&)
extern "C"  int32_t ProtoReader_ilo_TryReadUInt32Variant24_m4098800626 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, uint32_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::ilo_DirectReadBytes25(System.IO.Stream,System.Byte[],System.Int32,System.Int32)
extern "C"  void ProtoReader_ilo_DirectReadBytes25_m4256640039 (Il2CppObject * __this /* static, unused */, Stream_t1561764144 * ___source0, ByteU5BU5D_t4260760469* ___buffer1, int32_t ___offset2, int32_t ___count3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::ilo_WriteFieldHeader26(System.Int32,ProtoBuf.WireType,ProtoBuf.ProtoWriter)
extern "C"  void ProtoReader_ilo_WriteFieldHeader26_m2736404579 (Il2CppObject * __this /* static, unused */, int32_t ___fieldNumber0, int32_t ___wireType1, ProtoWriter_t4117914721 * ___writer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::ilo_WriteInt3227(System.Int32,ProtoBuf.ProtoWriter)
extern "C"  void ProtoReader_ilo_WriteInt3227_m863939685 (Il2CppObject * __this /* static, unused */, int32_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::ilo_WriteInt6428(System.Int64,ProtoBuf.ProtoWriter)
extern "C"  void ProtoReader_ilo_WriteInt6428_m1305338596 (Il2CppObject * __this /* static, unused */, int64_t ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] ProtoBuf.ProtoReader::ilo_AppendBytes29(System.Byte[],ProtoBuf.ProtoReader)
extern "C"  ByteU5BU5D_t4260760469* ProtoReader_ilo_AppendBytes29_m1829989441 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___value0, ProtoReader_t3962509489 * ___reader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.ProtoReader::ilo_ReadFieldHeader30(ProtoBuf.ProtoReader)
extern "C"  int32_t ProtoReader_ilo_ReadFieldHeader30_m1439700630 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::ilo_EndSubItem31(ProtoBuf.SubItemToken,ProtoBuf.ProtoReader)
extern "C"  void ProtoReader_ilo_EndSubItem31_m1177175919 (Il2CppObject * __this /* static, unused */, SubItemToken_t3365128146  ___token0, ProtoReader_t3962509489 * ___reader1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.ProtoReader::ilo_DeserializeType32(ProtoBuf.Meta.TypeModel,System.String)
extern "C"  Type_t * ProtoReader_ilo_DeserializeType32_m757501929 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::ilo_RegisterTrappedObject33(ProtoBuf.NetObjectCache,System.Object)
extern "C"  void ProtoReader_ilo_RegisterTrappedObject33_m301406299 (Il2CppObject * __this /* static, unused */, NetObjectCache_t514806898 * ____this0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Meta.TypeModel ProtoBuf.ProtoReader::ilo_get_Model34(ProtoBuf.ProtoReader)
extern "C"  TypeModel_t2730011105 * ProtoReader_ilo_get_Model34_m836270765 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.SerializationContext ProtoBuf.ProtoReader::ilo_get_Context35(ProtoBuf.ProtoReader)
extern "C"  SerializationContext_t3997850667 * ProtoReader_ilo_get_Context35_m1483182733 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.ProtoReader::ilo_Serialize36(ProtoBuf.Meta.TypeModel,System.IO.Stream,System.Object,ProtoBuf.SerializationContext)
extern "C"  void ProtoReader_ilo_Serialize36_m758452909 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ____this0, Stream_t1561764144 * ___dest1, Il2CppObject * ___value2, SerializationContext_t3997850667 * ___context3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
