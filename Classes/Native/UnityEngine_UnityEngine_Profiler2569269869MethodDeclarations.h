﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Profiler
struct Profiler_t2569269869;
// System.String
struct String_t;
// UnityEngine.Object
struct Object_t3071478659;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"

// System.Void UnityEngine.Profiler::.ctor()
extern "C"  void Profiler__ctor_m1272452004 (Profiler_t2569269869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Profiler::get_supported()
extern "C"  bool Profiler_get_supported_m4164251265 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Profiler::get_logFile()
extern "C"  String_t* Profiler_get_logFile_m3531301398 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Profiler::set_logFile(System.String)
extern "C"  void Profiler_set_logFile_m1625726909 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Profiler::get_enableBinaryLog()
extern "C"  bool Profiler_get_enableBinaryLog_m15419667 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Profiler::set_enableBinaryLog(System.Boolean)
extern "C"  void Profiler_set_enableBinaryLog_m3031043708 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Profiler::get_enabled()
extern "C"  bool Profiler_get_enabled_m755611860 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Profiler::set_enabled(System.Boolean)
extern "C"  void Profiler_set_enabled_m4246121085 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Profiler::get_maxNumberOfSamplesPerFrame()
extern "C"  int32_t Profiler_get_maxNumberOfSamplesPerFrame_m3760008222 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Profiler::set_maxNumberOfSamplesPerFrame(System.Int32)
extern "C"  void Profiler_set_maxNumberOfSamplesPerFrame_m431490147 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 UnityEngine.Profiler::get_usedHeapSize()
extern "C"  uint32_t Profiler_get_usedHeapSize_m1345079886 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Profiler::GetRuntimeMemorySize(UnityEngine.Object)
extern "C"  int32_t Profiler_GetRuntimeMemorySize_m2061200206 (Il2CppObject * __this /* static, unused */, Object_t3071478659 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 UnityEngine.Profiler::GetMonoHeapSize()
extern "C"  uint32_t Profiler_GetMonoHeapSize_m566354381 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 UnityEngine.Profiler::GetMonoUsedSize()
extern "C"  uint32_t Profiler_GetMonoUsedSize_m3388251870 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 UnityEngine.Profiler::GetTotalAllocatedMemory()
extern "C"  uint32_t Profiler_GetTotalAllocatedMemory_m2031857025 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 UnityEngine.Profiler::GetTotalUnusedReservedMemory()
extern "C"  uint32_t Profiler_GetTotalUnusedReservedMemory_m1531473960 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 UnityEngine.Profiler::GetTotalReservedMemory()
extern "C"  uint32_t Profiler_GetTotalReservedMemory_m1843892274 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
