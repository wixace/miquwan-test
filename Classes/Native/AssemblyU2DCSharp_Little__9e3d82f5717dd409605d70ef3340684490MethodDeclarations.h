﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._9e3d82f5717dd409605d70ef53061203
struct _9e3d82f5717dd409605d70ef53061203_t3340684490;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._9e3d82f5717dd409605d70ef53061203::.ctor()
extern "C"  void _9e3d82f5717dd409605d70ef53061203__ctor_m1585257635 (_9e3d82f5717dd409605d70ef53061203_t3340684490 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._9e3d82f5717dd409605d70ef53061203::_9e3d82f5717dd409605d70ef53061203m2(System.Int32)
extern "C"  int32_t _9e3d82f5717dd409605d70ef53061203__9e3d82f5717dd409605d70ef53061203m2_m782142809 (_9e3d82f5717dd409605d70ef53061203_t3340684490 * __this, int32_t ____9e3d82f5717dd409605d70ef53061203a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._9e3d82f5717dd409605d70ef53061203::_9e3d82f5717dd409605d70ef53061203m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _9e3d82f5717dd409605d70ef53061203__9e3d82f5717dd409605d70ef53061203m_m2054476157 (_9e3d82f5717dd409605d70ef53061203_t3340684490 * __this, int32_t ____9e3d82f5717dd409605d70ef53061203a0, int32_t ____9e3d82f5717dd409605d70ef53061203791, int32_t ____9e3d82f5717dd409605d70ef53061203c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
