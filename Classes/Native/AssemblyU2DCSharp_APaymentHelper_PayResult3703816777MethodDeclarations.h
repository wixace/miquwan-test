﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// APaymentHelper/PayResult
struct PayResult_t3703816777;

#include "codegen/il2cpp-codegen.h"

// System.Void APaymentHelper/PayResult::.ctor()
extern "C"  void PayResult__ctor_m2693353074 (PayResult_t3703816777 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
