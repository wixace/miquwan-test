﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ResolutionGenerated
struct UnityEngine_ResolutionGenerated_t596750299;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine_ResolutionGenerated::.ctor()
extern "C"  void UnityEngine_ResolutionGenerated__ctor_m1955851760 (UnityEngine_ResolutionGenerated_t596750299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ResolutionGenerated::.cctor()
extern "C"  void UnityEngine_ResolutionGenerated__cctor_m19766205 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ResolutionGenerated::Resolution_Resolution1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ResolutionGenerated_Resolution_Resolution1_m2159650114 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ResolutionGenerated::Resolution_width(JSVCall)
extern "C"  void UnityEngine_ResolutionGenerated_Resolution_width_m1577184064 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ResolutionGenerated::Resolution_height(JSVCall)
extern "C"  void UnityEngine_ResolutionGenerated_Resolution_height_m2303509375 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ResolutionGenerated::Resolution_refreshRate(JSVCall)
extern "C"  void UnityEngine_ResolutionGenerated_Resolution_refreshRate_m3390829131 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ResolutionGenerated::Resolution_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ResolutionGenerated_Resolution_ToString_m3747375377 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ResolutionGenerated::__Register()
extern "C"  void UnityEngine_ResolutionGenerated___Register_m3674522839 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ResolutionGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_ResolutionGenerated_ilo_attachFinalizerObject1_m3002883591 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ResolutionGenerated::ilo_changeJSObj2(System.Int32,System.Object)
extern "C"  void UnityEngine_ResolutionGenerated_ilo_changeJSObj2_m1830425082 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ResolutionGenerated::ilo_setStringS3(System.Int32,System.String)
extern "C"  void UnityEngine_ResolutionGenerated_ilo_setStringS3_m1674564027 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
