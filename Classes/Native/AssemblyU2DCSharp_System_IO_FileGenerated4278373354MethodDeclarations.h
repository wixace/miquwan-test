﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_IO_FileGenerated
struct System_IO_FileGenerated_t4278373354;
// JSVCall
struct JSVCall_t3708497963;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"

// System.Void System_IO_FileGenerated::.ctor()
extern "C"  void System_IO_FileGenerated__ctor_m18963137 (System_IO_FileGenerated_t4278373354 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_AppendAllText__String__String__Encoding(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_AppendAllText__String__String__Encoding_m1576819397 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_AppendAllText__String__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_AppendAllText__String__String_m792056562 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_AppendText__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_AppendText__String_m1470615326 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_Copy__String__String__Boolean(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_Copy__String__String__Boolean_m1053832461 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_Copy__String__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_Copy__String__String_m3090689213 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_Create__String__Int32__FileOptions__FileSecurity(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_Create__String__Int32__FileOptions__FileSecurity_m2533037955 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_Create__String__Int32__FileOptions(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_Create__String__Int32__FileOptions_m3717848615 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_Create__String__Int32(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_Create__String__Int32_m177105789 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_Create__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_Create__String_m3594708083 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_CreateText__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_CreateText__String_m990750848 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_Decrypt__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_Decrypt__String_m3351572410 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_Delete__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_Delete__String_m3032329442 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_Encrypt__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_Encrypt__String_m4054171282 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_Exists__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_Exists__String_m440078291 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_GetAccessControl__String__AccessControlSections(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_GetAccessControl__String__AccessControlSections_m167575247 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_GetAccessControl__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_GetAccessControl__String_m3471014554 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_GetAttributes__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_GetAttributes__String_m4018475834 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_GetCreationTime__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_GetCreationTime__String_m1577472303 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_GetCreationTimeUtc__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_GetCreationTimeUtc__String_m1037949689 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_GetLastAccessTime__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_GetLastAccessTime__String_m1366426346 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_GetLastAccessTimeUtc__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_GetLastAccessTimeUtc__String_m1599966046 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_GetLastWriteTime__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_GetLastWriteTime__String_m7970775 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_GetLastWriteTimeUtc__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_GetLastWriteTimeUtc__String_m3326880593 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_Move__String__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_Move__String__String_m1489802265 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_Open__String__FileMode__FileAccess__FileShare(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_Open__String__FileMode__FileAccess__FileShare_m3531661381 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_Open__String__FileMode__FileAccess(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_Open__String__FileMode__FileAccess_m2235963904 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_Open__String__FileMode(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_Open__String__FileMode_m2177576576 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_OpenRead__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_OpenRead__String_m2063561271 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_OpenText__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_OpenText__String_m2149661582 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_OpenWrite__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_OpenWrite__String_m1382746402 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_ReadAllBytes__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_ReadAllBytes__String_m840448951 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_ReadAllLines__String__Encoding(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_ReadAllLines__String__Encoding_m2800925662 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_ReadAllLines__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_ReadAllLines__String_m1904545611 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_ReadAllText__String__Encoding(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_ReadAllText__String__Encoding_m1139622776 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_ReadAllText__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_ReadAllText__String_m2990370661 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_Replace__String__String__String__Boolean(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_Replace__String__String__String__Boolean_m3247912007 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_Replace__String__String__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_Replace__String__String__String_m730612803 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_SetAccessControl__String__FileSecurity(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_SetAccessControl__String__FileSecurity_m1862399786 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_SetAttributes__String__FileAttributes(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_SetAttributes__String__FileAttributes_m4112175609 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_SetCreationTime__String__DateTime(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_SetCreationTime__String__DateTime_m1010535446 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_SetCreationTimeUtc__String__DateTime(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_SetCreationTimeUtc__String__DateTime_m110753736 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_SetLastAccessTime__String__DateTime(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_SetLastAccessTime__String__DateTime_m3683187985 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_SetLastAccessTimeUtc__String__DateTime(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_SetLastAccessTimeUtc__String__DateTime_m998809837 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_SetLastWriteTime__String__DateTime(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_SetLastWriteTime__String__DateTime_m772967462 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_SetLastWriteTimeUtc__String__DateTime(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_SetLastWriteTimeUtc__String__DateTime_m829046200 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_WriteAllBytes__String__Byte_Array(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_WriteAllBytes__String__Byte_Array_m2394838136 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_WriteAllLines__String__String_Array__Encoding(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_WriteAllLines__String__String_Array__Encoding_m2592862056 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_WriteAllLines__String__String_Array(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_WriteAllLines__String__String_Array_m3239397205 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_WriteAllText__String__String__Encoding(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_WriteAllText__String__String__Encoding_m1299395562 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::File_WriteAllText__String__String(JSVCall,System.Int32)
extern "C"  bool System_IO_FileGenerated_File_WriteAllText__String__String_m2100160087 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_FileGenerated::__Register()
extern "C"  void System_IO_FileGenerated___Register_m3466572454 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System_IO_FileGenerated::<File_WriteAllBytes__String__Byte_Array>m__C3()
extern "C"  ByteU5BU5D_t4260760469* System_IO_FileGenerated_U3CFile_WriteAllBytes__String__Byte_ArrayU3Em__C3_m1425605789 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System_IO_FileGenerated::<File_WriteAllLines__String__String_Array__Encoding>m__C4()
extern "C"  StringU5BU5D_t4054002952* System_IO_FileGenerated_U3CFile_WriteAllLines__String__String_Array__EncodingU3Em__C4_m667991589 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System_IO_FileGenerated::<File_WriteAllLines__String__String_Array>m__C5()
extern "C"  StringU5BU5D_t4054002952* System_IO_FileGenerated_U3CFile_WriteAllLines__String__String_ArrayU3Em__C5_m2114248915 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System_IO_FileGenerated::ilo_getStringS1(System.Int32)
extern "C"  String_t* System_IO_FileGenerated_ilo_getStringS1_m834772773 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_IO_FileGenerated::ilo_getInt322(System.Int32)
extern "C"  int32_t System_IO_FileGenerated_ilo_getInt322_m2771949861 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_IO_FileGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_IO_FileGenerated_ilo_setObject3_m4254948367 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_IO_FileGenerated::ilo_getEnum4(System.Int32)
extern "C"  int32_t System_IO_FileGenerated_ilo_getEnum4_m2029396246 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_FileGenerated::ilo_setByte5(System.Int32,System.Byte)
extern "C"  void System_IO_FileGenerated_ilo_setByte5_m1035566295 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint8_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_FileGenerated::ilo_moveSaveID2Arr6(System.Int32)
extern "C"  void System_IO_FileGenerated_ilo_moveSaveID2Arr6_m2724134107 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_FileGenerated::ilo_setStringS7(System.Int32,System.String)
extern "C"  void System_IO_FileGenerated_ilo_setStringS7_m3965352976 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_IO_FileGenerated::ilo_setArrayS8(System.Int32,System.Int32,System.Boolean)
extern "C"  void System_IO_FileGenerated_ilo_setArrayS8_m2666789801 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_IO_FileGenerated::ilo_getBooleanS9(System.Int32)
extern "C"  bool System_IO_FileGenerated_ilo_getBooleanS9_m3319367435 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_IO_FileGenerated::ilo_getObject10(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_IO_FileGenerated_ilo_getObject10_m3610703036 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_IO_FileGenerated::ilo_getObject11(System.Int32)
extern "C"  int32_t System_IO_FileGenerated_ilo_getObject11_m4216436832 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_IO_FileGenerated::ilo_getElement12(System.Int32,System.Int32)
extern "C"  int32_t System_IO_FileGenerated_ilo_getElement12_m1915807479 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_IO_FileGenerated::ilo_getArrayLength13(System.Int32)
extern "C"  int32_t System_IO_FileGenerated_ilo_getArrayLength13_m2533448004 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
