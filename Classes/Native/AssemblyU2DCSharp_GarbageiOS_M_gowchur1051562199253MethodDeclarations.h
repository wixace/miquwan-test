﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_gowchur105
struct M_gowchur105_t1562199253;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_gowchur1051562199253.h"

// System.Void GarbageiOS.M_gowchur105::.ctor()
extern "C"  void M_gowchur105__ctor_m574727934 (M_gowchur105_t1562199253 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gowchur105::M_lirsuKegasyu0(System.String[],System.Int32)
extern "C"  void M_gowchur105_M_lirsuKegasyu0_m2100363149 (M_gowchur105_t1562199253 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gowchur105::M_jousar1(System.String[],System.Int32)
extern "C"  void M_gowchur105_M_jousar1_m1419903326 (M_gowchur105_t1562199253 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gowchur105::M_gahoCelree2(System.String[],System.Int32)
extern "C"  void M_gowchur105_M_gahoCelree2_m1478421962 (M_gowchur105_t1562199253 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gowchur105::M_fasstas3(System.String[],System.Int32)
extern "C"  void M_gowchur105_M_fasstas3_m258246455 (M_gowchur105_t1562199253 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gowchur105::M_hisrere4(System.String[],System.Int32)
extern "C"  void M_gowchur105_M_hisrere4_m1898702091 (M_gowchur105_t1562199253 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gowchur105::ilo_M_gahoCelree21(GarbageiOS.M_gowchur105,System.String[],System.Int32)
extern "C"  void M_gowchur105_ilo_M_gahoCelree21_m1597778219 (Il2CppObject * __this /* static, unused */, M_gowchur105_t1562199253 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gowchur105::ilo_M_fasstas32(GarbageiOS.M_gowchur105,System.String[],System.Int32)
extern "C"  void M_gowchur105_ilo_M_fasstas32_m3949959705 (Il2CppObject * __this /* static, unused */, M_gowchur105_t1562199253 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
