﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ClothSphereColliderPair
struct ClothSphereColliderPair_t2674965067;
struct ClothSphereColliderPair_t2674965067_marshaled_pinvoke;
struct ClothSphereColliderPair_t2674965067_marshaled_com;

#include "codegen/il2cpp-codegen.h"


// Methods for marshaling
struct ClothSphereColliderPair_t2674965067;
struct ClothSphereColliderPair_t2674965067_marshaled_pinvoke;

extern "C" void ClothSphereColliderPair_t2674965067_marshal_pinvoke(const ClothSphereColliderPair_t2674965067& unmarshaled, ClothSphereColliderPair_t2674965067_marshaled_pinvoke& marshaled);
extern "C" void ClothSphereColliderPair_t2674965067_marshal_pinvoke_back(const ClothSphereColliderPair_t2674965067_marshaled_pinvoke& marshaled, ClothSphereColliderPair_t2674965067& unmarshaled);
extern "C" void ClothSphereColliderPair_t2674965067_marshal_pinvoke_cleanup(ClothSphereColliderPair_t2674965067_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ClothSphereColliderPair_t2674965067;
struct ClothSphereColliderPair_t2674965067_marshaled_com;

extern "C" void ClothSphereColliderPair_t2674965067_marshal_com(const ClothSphereColliderPair_t2674965067& unmarshaled, ClothSphereColliderPair_t2674965067_marshaled_com& marshaled);
extern "C" void ClothSphereColliderPair_t2674965067_marshal_com_back(const ClothSphereColliderPair_t2674965067_marshaled_com& marshaled, ClothSphereColliderPair_t2674965067& unmarshaled);
extern "C" void ClothSphereColliderPair_t2674965067_marshal_com_cleanup(ClothSphereColliderPair_t2674965067_marshaled_com& marshaled);
