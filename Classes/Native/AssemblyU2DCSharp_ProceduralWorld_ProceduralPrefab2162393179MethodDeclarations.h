﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProceduralWorld/ProceduralPrefab
struct ProceduralPrefab_t2162393179;

#include "codegen/il2cpp-codegen.h"

// System.Void ProceduralWorld/ProceduralPrefab::.ctor()
extern "C"  void ProceduralPrefab__ctor_m2557324448 (ProceduralPrefab_t2162393179 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
