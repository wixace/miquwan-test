﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_hootallhowNuhouxow214
struct  M_hootallhowNuhouxow214_t3661699681  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_hootallhowNuhouxow214::_kokagaRurbana
	String_t* ____kokagaRurbana_0;
	// System.UInt32 GarbageiOS.M_hootallhowNuhouxow214::_subealelMurlar
	uint32_t ____subealelMurlar_1;
	// System.Int32 GarbageiOS.M_hootallhowNuhouxow214::_mirhairsouMabimas
	int32_t ____mirhairsouMabimas_2;
	// System.Boolean GarbageiOS.M_hootallhowNuhouxow214::_taqorRoopu
	bool ____taqorRoopu_3;
	// System.Single GarbageiOS.M_hootallhowNuhouxow214::_werahasFeli
	float ____werahasFeli_4;
	// System.String GarbageiOS.M_hootallhowNuhouxow214::_xoulecur
	String_t* ____xoulecur_5;
	// System.Boolean GarbageiOS.M_hootallhowNuhouxow214::_cexoofe
	bool ____cexoofe_6;
	// System.Int32 GarbageiOS.M_hootallhowNuhouxow214::_nazoolea
	int32_t ____nazoolea_7;
	// System.UInt32 GarbageiOS.M_hootallhowNuhouxow214::_laipairmaMeajemjel
	uint32_t ____laipairmaMeajemjel_8;
	// System.UInt32 GarbageiOS.M_hootallhowNuhouxow214::_mubunu
	uint32_t ____mubunu_9;
	// System.Boolean GarbageiOS.M_hootallhowNuhouxow214::_whowjalaiKiswhea
	bool ____whowjalaiKiswhea_10;
	// System.Single GarbageiOS.M_hootallhowNuhouxow214::_lawmirnel
	float ____lawmirnel_11;
	// System.String GarbageiOS.M_hootallhowNuhouxow214::_purcaiMuferal
	String_t* ____purcaiMuferal_12;
	// System.UInt32 GarbageiOS.M_hootallhowNuhouxow214::_kare
	uint32_t ____kare_13;
	// System.Boolean GarbageiOS.M_hootallhowNuhouxow214::_rorsairmurFirsi
	bool ____rorsairmurFirsi_14;
	// System.Boolean GarbageiOS.M_hootallhowNuhouxow214::_daltoro
	bool ____daltoro_15;
	// System.String GarbageiOS.M_hootallhowNuhouxow214::_jeasawsa
	String_t* ____jeasawsa_16;
	// System.Boolean GarbageiOS.M_hootallhowNuhouxow214::_wayfaJoumemu
	bool ____wayfaJoumemu_17;
	// System.UInt32 GarbageiOS.M_hootallhowNuhouxow214::_jernairMazurcou
	uint32_t ____jernairMazurcou_18;
	// System.String GarbageiOS.M_hootallhowNuhouxow214::_suvair
	String_t* ____suvair_19;
	// System.Boolean GarbageiOS.M_hootallhowNuhouxow214::_seartreNayza
	bool ____seartreNayza_20;
	// System.Boolean GarbageiOS.M_hootallhowNuhouxow214::_dreregelreaRayball
	bool ____dreregelreaRayball_21;
	// System.Single GarbageiOS.M_hootallhowNuhouxow214::_stoulereGownarree
	float ____stoulereGownarree_22;
	// System.Boolean GarbageiOS.M_hootallhowNuhouxow214::_nallmearwemBipi
	bool ____nallmearwemBipi_23;
	// System.String GarbageiOS.M_hootallhowNuhouxow214::_jiska
	String_t* ____jiska_24;
	// System.Single GarbageiOS.M_hootallhowNuhouxow214::_tekaja
	float ____tekaja_25;
	// System.UInt32 GarbageiOS.M_hootallhowNuhouxow214::_celji
	uint32_t ____celji_26;
	// System.Boolean GarbageiOS.M_hootallhowNuhouxow214::_kasaipurCoonere
	bool ____kasaipurCoonere_27;
	// System.UInt32 GarbageiOS.M_hootallhowNuhouxow214::_sepalno
	uint32_t ____sepalno_28;
	// System.UInt32 GarbageiOS.M_hootallhowNuhouxow214::_raqoLallhoni
	uint32_t ____raqoLallhoni_29;
	// System.Single GarbageiOS.M_hootallhowNuhouxow214::_saycadis
	float ____saycadis_30;
	// System.Int32 GarbageiOS.M_hootallhowNuhouxow214::_yecelKasa
	int32_t ____yecelKasa_31;
	// System.UInt32 GarbageiOS.M_hootallhowNuhouxow214::_baivaSerfe
	uint32_t ____baivaSerfe_32;
	// System.Int32 GarbageiOS.M_hootallhowNuhouxow214::_lowbis
	int32_t ____lowbis_33;
	// System.Boolean GarbageiOS.M_hootallhowNuhouxow214::_stabairtoBeame
	bool ____stabairtoBeame_34;
	// System.UInt32 GarbageiOS.M_hootallhowNuhouxow214::_mesetaPawusay
	uint32_t ____mesetaPawusay_35;
	// System.UInt32 GarbageiOS.M_hootallhowNuhouxow214::_rutefear
	uint32_t ____rutefear_36;
	// System.Boolean GarbageiOS.M_hootallhowNuhouxow214::_poupearreDeresallmal
	bool ____poupearreDeresallmal_37;
	// System.UInt32 GarbageiOS.M_hootallhowNuhouxow214::_ceeteMairre
	uint32_t ____ceeteMairre_38;
	// System.Single GarbageiOS.M_hootallhowNuhouxow214::_saterpi
	float ____saterpi_39;
	// System.String GarbageiOS.M_hootallhowNuhouxow214::_qarjuHorstevay
	String_t* ____qarjuHorstevay_40;
	// System.UInt32 GarbageiOS.M_hootallhowNuhouxow214::_relsooni
	uint32_t ____relsooni_41;
	// System.Single GarbageiOS.M_hootallhowNuhouxow214::_stellaw
	float ____stellaw_42;
	// System.Single GarbageiOS.M_hootallhowNuhouxow214::_jeporweLanuke
	float ____jeporweLanuke_43;
	// System.Boolean GarbageiOS.M_hootallhowNuhouxow214::_tetay
	bool ____tetay_44;

public:
	inline static int32_t get_offset_of__kokagaRurbana_0() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____kokagaRurbana_0)); }
	inline String_t* get__kokagaRurbana_0() const { return ____kokagaRurbana_0; }
	inline String_t** get_address_of__kokagaRurbana_0() { return &____kokagaRurbana_0; }
	inline void set__kokagaRurbana_0(String_t* value)
	{
		____kokagaRurbana_0 = value;
		Il2CppCodeGenWriteBarrier(&____kokagaRurbana_0, value);
	}

	inline static int32_t get_offset_of__subealelMurlar_1() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____subealelMurlar_1)); }
	inline uint32_t get__subealelMurlar_1() const { return ____subealelMurlar_1; }
	inline uint32_t* get_address_of__subealelMurlar_1() { return &____subealelMurlar_1; }
	inline void set__subealelMurlar_1(uint32_t value)
	{
		____subealelMurlar_1 = value;
	}

	inline static int32_t get_offset_of__mirhairsouMabimas_2() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____mirhairsouMabimas_2)); }
	inline int32_t get__mirhairsouMabimas_2() const { return ____mirhairsouMabimas_2; }
	inline int32_t* get_address_of__mirhairsouMabimas_2() { return &____mirhairsouMabimas_2; }
	inline void set__mirhairsouMabimas_2(int32_t value)
	{
		____mirhairsouMabimas_2 = value;
	}

	inline static int32_t get_offset_of__taqorRoopu_3() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____taqorRoopu_3)); }
	inline bool get__taqorRoopu_3() const { return ____taqorRoopu_3; }
	inline bool* get_address_of__taqorRoopu_3() { return &____taqorRoopu_3; }
	inline void set__taqorRoopu_3(bool value)
	{
		____taqorRoopu_3 = value;
	}

	inline static int32_t get_offset_of__werahasFeli_4() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____werahasFeli_4)); }
	inline float get__werahasFeli_4() const { return ____werahasFeli_4; }
	inline float* get_address_of__werahasFeli_4() { return &____werahasFeli_4; }
	inline void set__werahasFeli_4(float value)
	{
		____werahasFeli_4 = value;
	}

	inline static int32_t get_offset_of__xoulecur_5() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____xoulecur_5)); }
	inline String_t* get__xoulecur_5() const { return ____xoulecur_5; }
	inline String_t** get_address_of__xoulecur_5() { return &____xoulecur_5; }
	inline void set__xoulecur_5(String_t* value)
	{
		____xoulecur_5 = value;
		Il2CppCodeGenWriteBarrier(&____xoulecur_5, value);
	}

	inline static int32_t get_offset_of__cexoofe_6() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____cexoofe_6)); }
	inline bool get__cexoofe_6() const { return ____cexoofe_6; }
	inline bool* get_address_of__cexoofe_6() { return &____cexoofe_6; }
	inline void set__cexoofe_6(bool value)
	{
		____cexoofe_6 = value;
	}

	inline static int32_t get_offset_of__nazoolea_7() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____nazoolea_7)); }
	inline int32_t get__nazoolea_7() const { return ____nazoolea_7; }
	inline int32_t* get_address_of__nazoolea_7() { return &____nazoolea_7; }
	inline void set__nazoolea_7(int32_t value)
	{
		____nazoolea_7 = value;
	}

	inline static int32_t get_offset_of__laipairmaMeajemjel_8() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____laipairmaMeajemjel_8)); }
	inline uint32_t get__laipairmaMeajemjel_8() const { return ____laipairmaMeajemjel_8; }
	inline uint32_t* get_address_of__laipairmaMeajemjel_8() { return &____laipairmaMeajemjel_8; }
	inline void set__laipairmaMeajemjel_8(uint32_t value)
	{
		____laipairmaMeajemjel_8 = value;
	}

	inline static int32_t get_offset_of__mubunu_9() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____mubunu_9)); }
	inline uint32_t get__mubunu_9() const { return ____mubunu_9; }
	inline uint32_t* get_address_of__mubunu_9() { return &____mubunu_9; }
	inline void set__mubunu_9(uint32_t value)
	{
		____mubunu_9 = value;
	}

	inline static int32_t get_offset_of__whowjalaiKiswhea_10() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____whowjalaiKiswhea_10)); }
	inline bool get__whowjalaiKiswhea_10() const { return ____whowjalaiKiswhea_10; }
	inline bool* get_address_of__whowjalaiKiswhea_10() { return &____whowjalaiKiswhea_10; }
	inline void set__whowjalaiKiswhea_10(bool value)
	{
		____whowjalaiKiswhea_10 = value;
	}

	inline static int32_t get_offset_of__lawmirnel_11() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____lawmirnel_11)); }
	inline float get__lawmirnel_11() const { return ____lawmirnel_11; }
	inline float* get_address_of__lawmirnel_11() { return &____lawmirnel_11; }
	inline void set__lawmirnel_11(float value)
	{
		____lawmirnel_11 = value;
	}

	inline static int32_t get_offset_of__purcaiMuferal_12() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____purcaiMuferal_12)); }
	inline String_t* get__purcaiMuferal_12() const { return ____purcaiMuferal_12; }
	inline String_t** get_address_of__purcaiMuferal_12() { return &____purcaiMuferal_12; }
	inline void set__purcaiMuferal_12(String_t* value)
	{
		____purcaiMuferal_12 = value;
		Il2CppCodeGenWriteBarrier(&____purcaiMuferal_12, value);
	}

	inline static int32_t get_offset_of__kare_13() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____kare_13)); }
	inline uint32_t get__kare_13() const { return ____kare_13; }
	inline uint32_t* get_address_of__kare_13() { return &____kare_13; }
	inline void set__kare_13(uint32_t value)
	{
		____kare_13 = value;
	}

	inline static int32_t get_offset_of__rorsairmurFirsi_14() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____rorsairmurFirsi_14)); }
	inline bool get__rorsairmurFirsi_14() const { return ____rorsairmurFirsi_14; }
	inline bool* get_address_of__rorsairmurFirsi_14() { return &____rorsairmurFirsi_14; }
	inline void set__rorsairmurFirsi_14(bool value)
	{
		____rorsairmurFirsi_14 = value;
	}

	inline static int32_t get_offset_of__daltoro_15() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____daltoro_15)); }
	inline bool get__daltoro_15() const { return ____daltoro_15; }
	inline bool* get_address_of__daltoro_15() { return &____daltoro_15; }
	inline void set__daltoro_15(bool value)
	{
		____daltoro_15 = value;
	}

	inline static int32_t get_offset_of__jeasawsa_16() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____jeasawsa_16)); }
	inline String_t* get__jeasawsa_16() const { return ____jeasawsa_16; }
	inline String_t** get_address_of__jeasawsa_16() { return &____jeasawsa_16; }
	inline void set__jeasawsa_16(String_t* value)
	{
		____jeasawsa_16 = value;
		Il2CppCodeGenWriteBarrier(&____jeasawsa_16, value);
	}

	inline static int32_t get_offset_of__wayfaJoumemu_17() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____wayfaJoumemu_17)); }
	inline bool get__wayfaJoumemu_17() const { return ____wayfaJoumemu_17; }
	inline bool* get_address_of__wayfaJoumemu_17() { return &____wayfaJoumemu_17; }
	inline void set__wayfaJoumemu_17(bool value)
	{
		____wayfaJoumemu_17 = value;
	}

	inline static int32_t get_offset_of__jernairMazurcou_18() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____jernairMazurcou_18)); }
	inline uint32_t get__jernairMazurcou_18() const { return ____jernairMazurcou_18; }
	inline uint32_t* get_address_of__jernairMazurcou_18() { return &____jernairMazurcou_18; }
	inline void set__jernairMazurcou_18(uint32_t value)
	{
		____jernairMazurcou_18 = value;
	}

	inline static int32_t get_offset_of__suvair_19() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____suvair_19)); }
	inline String_t* get__suvair_19() const { return ____suvair_19; }
	inline String_t** get_address_of__suvair_19() { return &____suvair_19; }
	inline void set__suvair_19(String_t* value)
	{
		____suvair_19 = value;
		Il2CppCodeGenWriteBarrier(&____suvair_19, value);
	}

	inline static int32_t get_offset_of__seartreNayza_20() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____seartreNayza_20)); }
	inline bool get__seartreNayza_20() const { return ____seartreNayza_20; }
	inline bool* get_address_of__seartreNayza_20() { return &____seartreNayza_20; }
	inline void set__seartreNayza_20(bool value)
	{
		____seartreNayza_20 = value;
	}

	inline static int32_t get_offset_of__dreregelreaRayball_21() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____dreregelreaRayball_21)); }
	inline bool get__dreregelreaRayball_21() const { return ____dreregelreaRayball_21; }
	inline bool* get_address_of__dreregelreaRayball_21() { return &____dreregelreaRayball_21; }
	inline void set__dreregelreaRayball_21(bool value)
	{
		____dreregelreaRayball_21 = value;
	}

	inline static int32_t get_offset_of__stoulereGownarree_22() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____stoulereGownarree_22)); }
	inline float get__stoulereGownarree_22() const { return ____stoulereGownarree_22; }
	inline float* get_address_of__stoulereGownarree_22() { return &____stoulereGownarree_22; }
	inline void set__stoulereGownarree_22(float value)
	{
		____stoulereGownarree_22 = value;
	}

	inline static int32_t get_offset_of__nallmearwemBipi_23() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____nallmearwemBipi_23)); }
	inline bool get__nallmearwemBipi_23() const { return ____nallmearwemBipi_23; }
	inline bool* get_address_of__nallmearwemBipi_23() { return &____nallmearwemBipi_23; }
	inline void set__nallmearwemBipi_23(bool value)
	{
		____nallmearwemBipi_23 = value;
	}

	inline static int32_t get_offset_of__jiska_24() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____jiska_24)); }
	inline String_t* get__jiska_24() const { return ____jiska_24; }
	inline String_t** get_address_of__jiska_24() { return &____jiska_24; }
	inline void set__jiska_24(String_t* value)
	{
		____jiska_24 = value;
		Il2CppCodeGenWriteBarrier(&____jiska_24, value);
	}

	inline static int32_t get_offset_of__tekaja_25() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____tekaja_25)); }
	inline float get__tekaja_25() const { return ____tekaja_25; }
	inline float* get_address_of__tekaja_25() { return &____tekaja_25; }
	inline void set__tekaja_25(float value)
	{
		____tekaja_25 = value;
	}

	inline static int32_t get_offset_of__celji_26() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____celji_26)); }
	inline uint32_t get__celji_26() const { return ____celji_26; }
	inline uint32_t* get_address_of__celji_26() { return &____celji_26; }
	inline void set__celji_26(uint32_t value)
	{
		____celji_26 = value;
	}

	inline static int32_t get_offset_of__kasaipurCoonere_27() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____kasaipurCoonere_27)); }
	inline bool get__kasaipurCoonere_27() const { return ____kasaipurCoonere_27; }
	inline bool* get_address_of__kasaipurCoonere_27() { return &____kasaipurCoonere_27; }
	inline void set__kasaipurCoonere_27(bool value)
	{
		____kasaipurCoonere_27 = value;
	}

	inline static int32_t get_offset_of__sepalno_28() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____sepalno_28)); }
	inline uint32_t get__sepalno_28() const { return ____sepalno_28; }
	inline uint32_t* get_address_of__sepalno_28() { return &____sepalno_28; }
	inline void set__sepalno_28(uint32_t value)
	{
		____sepalno_28 = value;
	}

	inline static int32_t get_offset_of__raqoLallhoni_29() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____raqoLallhoni_29)); }
	inline uint32_t get__raqoLallhoni_29() const { return ____raqoLallhoni_29; }
	inline uint32_t* get_address_of__raqoLallhoni_29() { return &____raqoLallhoni_29; }
	inline void set__raqoLallhoni_29(uint32_t value)
	{
		____raqoLallhoni_29 = value;
	}

	inline static int32_t get_offset_of__saycadis_30() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____saycadis_30)); }
	inline float get__saycadis_30() const { return ____saycadis_30; }
	inline float* get_address_of__saycadis_30() { return &____saycadis_30; }
	inline void set__saycadis_30(float value)
	{
		____saycadis_30 = value;
	}

	inline static int32_t get_offset_of__yecelKasa_31() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____yecelKasa_31)); }
	inline int32_t get__yecelKasa_31() const { return ____yecelKasa_31; }
	inline int32_t* get_address_of__yecelKasa_31() { return &____yecelKasa_31; }
	inline void set__yecelKasa_31(int32_t value)
	{
		____yecelKasa_31 = value;
	}

	inline static int32_t get_offset_of__baivaSerfe_32() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____baivaSerfe_32)); }
	inline uint32_t get__baivaSerfe_32() const { return ____baivaSerfe_32; }
	inline uint32_t* get_address_of__baivaSerfe_32() { return &____baivaSerfe_32; }
	inline void set__baivaSerfe_32(uint32_t value)
	{
		____baivaSerfe_32 = value;
	}

	inline static int32_t get_offset_of__lowbis_33() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____lowbis_33)); }
	inline int32_t get__lowbis_33() const { return ____lowbis_33; }
	inline int32_t* get_address_of__lowbis_33() { return &____lowbis_33; }
	inline void set__lowbis_33(int32_t value)
	{
		____lowbis_33 = value;
	}

	inline static int32_t get_offset_of__stabairtoBeame_34() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____stabairtoBeame_34)); }
	inline bool get__stabairtoBeame_34() const { return ____stabairtoBeame_34; }
	inline bool* get_address_of__stabairtoBeame_34() { return &____stabairtoBeame_34; }
	inline void set__stabairtoBeame_34(bool value)
	{
		____stabairtoBeame_34 = value;
	}

	inline static int32_t get_offset_of__mesetaPawusay_35() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____mesetaPawusay_35)); }
	inline uint32_t get__mesetaPawusay_35() const { return ____mesetaPawusay_35; }
	inline uint32_t* get_address_of__mesetaPawusay_35() { return &____mesetaPawusay_35; }
	inline void set__mesetaPawusay_35(uint32_t value)
	{
		____mesetaPawusay_35 = value;
	}

	inline static int32_t get_offset_of__rutefear_36() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____rutefear_36)); }
	inline uint32_t get__rutefear_36() const { return ____rutefear_36; }
	inline uint32_t* get_address_of__rutefear_36() { return &____rutefear_36; }
	inline void set__rutefear_36(uint32_t value)
	{
		____rutefear_36 = value;
	}

	inline static int32_t get_offset_of__poupearreDeresallmal_37() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____poupearreDeresallmal_37)); }
	inline bool get__poupearreDeresallmal_37() const { return ____poupearreDeresallmal_37; }
	inline bool* get_address_of__poupearreDeresallmal_37() { return &____poupearreDeresallmal_37; }
	inline void set__poupearreDeresallmal_37(bool value)
	{
		____poupearreDeresallmal_37 = value;
	}

	inline static int32_t get_offset_of__ceeteMairre_38() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____ceeteMairre_38)); }
	inline uint32_t get__ceeteMairre_38() const { return ____ceeteMairre_38; }
	inline uint32_t* get_address_of__ceeteMairre_38() { return &____ceeteMairre_38; }
	inline void set__ceeteMairre_38(uint32_t value)
	{
		____ceeteMairre_38 = value;
	}

	inline static int32_t get_offset_of__saterpi_39() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____saterpi_39)); }
	inline float get__saterpi_39() const { return ____saterpi_39; }
	inline float* get_address_of__saterpi_39() { return &____saterpi_39; }
	inline void set__saterpi_39(float value)
	{
		____saterpi_39 = value;
	}

	inline static int32_t get_offset_of__qarjuHorstevay_40() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____qarjuHorstevay_40)); }
	inline String_t* get__qarjuHorstevay_40() const { return ____qarjuHorstevay_40; }
	inline String_t** get_address_of__qarjuHorstevay_40() { return &____qarjuHorstevay_40; }
	inline void set__qarjuHorstevay_40(String_t* value)
	{
		____qarjuHorstevay_40 = value;
		Il2CppCodeGenWriteBarrier(&____qarjuHorstevay_40, value);
	}

	inline static int32_t get_offset_of__relsooni_41() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____relsooni_41)); }
	inline uint32_t get__relsooni_41() const { return ____relsooni_41; }
	inline uint32_t* get_address_of__relsooni_41() { return &____relsooni_41; }
	inline void set__relsooni_41(uint32_t value)
	{
		____relsooni_41 = value;
	}

	inline static int32_t get_offset_of__stellaw_42() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____stellaw_42)); }
	inline float get__stellaw_42() const { return ____stellaw_42; }
	inline float* get_address_of__stellaw_42() { return &____stellaw_42; }
	inline void set__stellaw_42(float value)
	{
		____stellaw_42 = value;
	}

	inline static int32_t get_offset_of__jeporweLanuke_43() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____jeporweLanuke_43)); }
	inline float get__jeporweLanuke_43() const { return ____jeporweLanuke_43; }
	inline float* get_address_of__jeporweLanuke_43() { return &____jeporweLanuke_43; }
	inline void set__jeporweLanuke_43(float value)
	{
		____jeporweLanuke_43 = value;
	}

	inline static int32_t get_offset_of__tetay_44() { return static_cast<int32_t>(offsetof(M_hootallhowNuhouxow214_t3661699681, ____tetay_44)); }
	inline bool get__tetay_44() const { return ____tetay_44; }
	inline bool* get_address_of__tetay_44() { return &____tetay_44; }
	inline void set__tetay_44(bool value)
	{
		____tetay_44 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
