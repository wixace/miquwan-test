﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimationSpriteSheet1
struct AnimationSpriteSheet1_t2485186395;

#include "codegen/il2cpp-codegen.h"

// System.Void AnimationSpriteSheet1::.ctor()
extern "C"  void AnimationSpriteSheet1__ctor_m4032250907 (AnimationSpriteSheet1_t2485186395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationSpriteSheet1::Update()
extern "C"  void AnimationSpriteSheet1_Update_m2172588626 (AnimationSpriteSheet1_t2485186395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimationSpriteSheet1::Main()
extern "C"  void AnimationSpriteSheet1_Main_m2539418946 (AnimationSpriteSheet1_t2485186395 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
