﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3596925312.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_519615340.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PushType,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3213272366_gshared (InternalEnumerator_1_t3596925312 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3213272366(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3596925312 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3213272366_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PushType,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m826799538_gshared (InternalEnumerator_1_t3596925312 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m826799538(__this, method) ((  void (*) (InternalEnumerator_1_t3596925312 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m826799538_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PushType,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3443364712_gshared (InternalEnumerator_1_t3596925312 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3443364712(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3596925312 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3443364712_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PushType,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2836017285_gshared (InternalEnumerator_1_t3596925312 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2836017285(__this, method) ((  void (*) (InternalEnumerator_1_t3596925312 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2836017285_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PushType,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2548268258_gshared (InternalEnumerator_1_t3596925312 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2548268258(__this, method) ((  bool (*) (InternalEnumerator_1_t3596925312 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2548268258_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<PushType,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t519615340  InternalEnumerator_1_get_Current_m4225177623_gshared (InternalEnumerator_1_t3596925312 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m4225177623(__this, method) ((  KeyValuePair_2_t519615340  (*) (InternalEnumerator_1_t3596925312 *, const MethodInfo*))InternalEnumerator_1_get_Current_m4225177623_gshared)(__this, method)
