﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.ProtoDecoratorBase
struct ProtoDecoratorBase_t2329902142;
// ProtoBuf.Serializers.IProtoSerializer
struct IProtoSerializer_t3033312651;

#include "codegen/il2cpp-codegen.h"

// System.Void ProtoBuf.Serializers.ProtoDecoratorBase::.ctor(ProtoBuf.Serializers.IProtoSerializer)
extern "C"  void ProtoDecoratorBase__ctor_m892877306 (ProtoDecoratorBase_t2329902142 * __this, Il2CppObject * ___tail0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
