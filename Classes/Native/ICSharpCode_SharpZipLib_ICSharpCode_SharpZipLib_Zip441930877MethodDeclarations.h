﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer
struct InflaterInputBuffer_t441930877;
// System.IO.Stream
struct Stream_t1561764144;
// ICSharpCode.SharpZipLib.Zip.Compression.Inflater
struct Inflater_t1975778921;
// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1975778921.h"

// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::.ctor(System.IO.Stream,System.Int32)
extern "C"  void InflaterInputBuffer__ctor_m3230220962 (InflaterInputBuffer_t441930877 * __this, Stream_t1561764144 * ___stream0, int32_t ___bufferSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::get_RawLength()
extern "C"  int32_t InflaterInputBuffer_get_RawLength_m1307271085 (InflaterInputBuffer_t441930877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::get_Available()
extern "C"  int32_t InflaterInputBuffer_get_Available_m3610562184 (InflaterInputBuffer_t441930877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::set_Available(System.Int32)
extern "C"  void InflaterInputBuffer_set_Available_m1900611929 (InflaterInputBuffer_t441930877 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::SetInflaterInput(ICSharpCode.SharpZipLib.Zip.Compression.Inflater)
extern "C"  void InflaterInputBuffer_SetInflaterInput_m3831681298 (InflaterInputBuffer_t441930877 * __this, Inflater_t1975778921 * ___inflater0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::Fill()
extern "C"  void InflaterInputBuffer_Fill_m1169415305 (InflaterInputBuffer_t441930877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadClearTextBuffer(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t InflaterInputBuffer_ReadClearTextBuffer_m1437123005 (InflaterInputBuffer_t441930877 * __this, ByteU5BU5D_t4260760469* ___outBuffer0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeByte()
extern "C"  int32_t InflaterInputBuffer_ReadLeByte_m1881028497 (InflaterInputBuffer_t441930877 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
