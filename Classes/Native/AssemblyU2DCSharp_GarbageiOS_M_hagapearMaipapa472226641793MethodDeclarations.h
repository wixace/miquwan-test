﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_hagapearMaipapa47
struct M_hagapearMaipapa47_t2226641793;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_hagapearMaipapa472226641793.h"

// System.Void GarbageiOS.M_hagapearMaipapa47::.ctor()
extern "C"  void M_hagapearMaipapa47__ctor_m3128168194 (M_hagapearMaipapa47_t2226641793 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hagapearMaipapa47::M_kisje0(System.String[],System.Int32)
extern "C"  void M_hagapearMaipapa47_M_kisje0_m3137568171 (M_hagapearMaipapa47_t2226641793 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hagapearMaipapa47::M_mismelsurSorwoje1(System.String[],System.Int32)
extern "C"  void M_hagapearMaipapa47_M_mismelsurSorwoje1_m1601836768 (M_hagapearMaipapa47_t2226641793 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hagapearMaipapa47::M_delsas2(System.String[],System.Int32)
extern "C"  void M_hagapearMaipapa47_M_delsas2_m1067875069 (M_hagapearMaipapa47_t2226641793 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hagapearMaipapa47::M_gairzerecu3(System.String[],System.Int32)
extern "C"  void M_hagapearMaipapa47_M_gairzerecu3_m3432560837 (M_hagapearMaipapa47_t2226641793 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hagapearMaipapa47::M_bisvaimisTrirdaiwe4(System.String[],System.Int32)
extern "C"  void M_hagapearMaipapa47_M_bisvaimisTrirdaiwe4_m815905515 (M_hagapearMaipapa47_t2226641793 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_hagapearMaipapa47::ilo_M_gairzerecu31(GarbageiOS.M_hagapearMaipapa47,System.String[],System.Int32)
extern "C"  void M_hagapearMaipapa47_ilo_M_gairzerecu31_m1887583550 (Il2CppObject * __this /* static, unused */, M_hagapearMaipapa47_t2226641793 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
