﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_buloule196
struct  M_buloule196_t722519942  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_buloule196::_medrairStemawpee
	String_t* ____medrairStemawpee_0;
	// System.Boolean GarbageiOS.M_buloule196::_jalqergurRookall
	bool ____jalqergurRookall_1;
	// System.Single GarbageiOS.M_buloule196::_talnoukoNeacarca
	float ____talnoukoNeacarca_2;
	// System.UInt32 GarbageiOS.M_buloule196::_sasfee
	uint32_t ____sasfee_3;
	// System.Boolean GarbageiOS.M_buloule196::_treestas
	bool ____treestas_4;
	// System.Single GarbageiOS.M_buloule196::_loomeltrorDerislar
	float ____loomeltrorDerislar_5;
	// System.UInt32 GarbageiOS.M_buloule196::_draywhou
	uint32_t ____draywhou_6;
	// System.Int32 GarbageiOS.M_buloule196::_comuRawjehe
	int32_t ____comuRawjehe_7;
	// System.Boolean GarbageiOS.M_buloule196::_stive
	bool ____stive_8;
	// System.String GarbageiOS.M_buloule196::_mowcoBemjerebu
	String_t* ____mowcoBemjerebu_9;
	// System.UInt32 GarbageiOS.M_buloule196::_remrere
	uint32_t ____remrere_10;
	// System.UInt32 GarbageiOS.M_buloule196::_wemzistou
	uint32_t ____wemzistou_11;
	// System.Boolean GarbageiOS.M_buloule196::_zallstereMarxoure
	bool ____zallstereMarxoure_12;
	// System.Boolean GarbageiOS.M_buloule196::_meko
	bool ____meko_13;
	// System.Int32 GarbageiOS.M_buloule196::_helnow
	int32_t ____helnow_14;
	// System.Single GarbageiOS.M_buloule196::_temai
	float ____temai_15;
	// System.String GarbageiOS.M_buloule196::_bewem
	String_t* ____bewem_16;
	// System.Boolean GarbageiOS.M_buloule196::_ditire
	bool ____ditire_17;
	// System.UInt32 GarbageiOS.M_buloule196::_sujorRaircipe
	uint32_t ____sujorRaircipe_18;
	// System.String GarbageiOS.M_buloule196::_cesir
	String_t* ____cesir_19;
	// System.Single GarbageiOS.M_buloule196::_sidaYarlowlow
	float ____sidaYarlowlow_20;
	// System.String GarbageiOS.M_buloule196::_chunerke
	String_t* ____chunerke_21;
	// System.Int32 GarbageiOS.M_buloule196::_dearmourereSenaygall
	int32_t ____dearmourereSenaygall_22;
	// System.UInt32 GarbageiOS.M_buloule196::_tomallmiHejerhu
	uint32_t ____tomallmiHejerhu_23;
	// System.UInt32 GarbageiOS.M_buloule196::_mastall
	uint32_t ____mastall_24;
	// System.Single GarbageiOS.M_buloule196::_paneCeryerevoo
	float ____paneCeryerevoo_25;
	// System.Boolean GarbageiOS.M_buloule196::_jayqoomaTearjea
	bool ____jayqoomaTearjea_26;
	// System.Boolean GarbageiOS.M_buloule196::_delel
	bool ____delel_27;
	// System.Int32 GarbageiOS.M_buloule196::_tearxecheJirzarpee
	int32_t ____tearxecheJirzarpee_28;
	// System.Int32 GarbageiOS.M_buloule196::_nideDenahair
	int32_t ____nideDenahair_29;
	// System.Boolean GarbageiOS.M_buloule196::_souterTresa
	bool ____souterTresa_30;
	// System.Single GarbageiOS.M_buloule196::_viwawSairzemre
	float ____viwawSairzemre_31;
	// System.Boolean GarbageiOS.M_buloule196::_saymahuMairchafo
	bool ____saymahuMairchafo_32;
	// System.Int32 GarbageiOS.M_buloule196::_temawJevayse
	int32_t ____temawJevayse_33;
	// System.Single GarbageiOS.M_buloule196::_zezunouQesearsee
	float ____zezunouQesearsee_34;
	// System.String GarbageiOS.M_buloule196::_mukelRoqeeje
	String_t* ____mukelRoqeeje_35;

public:
	inline static int32_t get_offset_of__medrairStemawpee_0() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____medrairStemawpee_0)); }
	inline String_t* get__medrairStemawpee_0() const { return ____medrairStemawpee_0; }
	inline String_t** get_address_of__medrairStemawpee_0() { return &____medrairStemawpee_0; }
	inline void set__medrairStemawpee_0(String_t* value)
	{
		____medrairStemawpee_0 = value;
		Il2CppCodeGenWriteBarrier(&____medrairStemawpee_0, value);
	}

	inline static int32_t get_offset_of__jalqergurRookall_1() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____jalqergurRookall_1)); }
	inline bool get__jalqergurRookall_1() const { return ____jalqergurRookall_1; }
	inline bool* get_address_of__jalqergurRookall_1() { return &____jalqergurRookall_1; }
	inline void set__jalqergurRookall_1(bool value)
	{
		____jalqergurRookall_1 = value;
	}

	inline static int32_t get_offset_of__talnoukoNeacarca_2() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____talnoukoNeacarca_2)); }
	inline float get__talnoukoNeacarca_2() const { return ____talnoukoNeacarca_2; }
	inline float* get_address_of__talnoukoNeacarca_2() { return &____talnoukoNeacarca_2; }
	inline void set__talnoukoNeacarca_2(float value)
	{
		____talnoukoNeacarca_2 = value;
	}

	inline static int32_t get_offset_of__sasfee_3() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____sasfee_3)); }
	inline uint32_t get__sasfee_3() const { return ____sasfee_3; }
	inline uint32_t* get_address_of__sasfee_3() { return &____sasfee_3; }
	inline void set__sasfee_3(uint32_t value)
	{
		____sasfee_3 = value;
	}

	inline static int32_t get_offset_of__treestas_4() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____treestas_4)); }
	inline bool get__treestas_4() const { return ____treestas_4; }
	inline bool* get_address_of__treestas_4() { return &____treestas_4; }
	inline void set__treestas_4(bool value)
	{
		____treestas_4 = value;
	}

	inline static int32_t get_offset_of__loomeltrorDerislar_5() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____loomeltrorDerislar_5)); }
	inline float get__loomeltrorDerislar_5() const { return ____loomeltrorDerislar_5; }
	inline float* get_address_of__loomeltrorDerislar_5() { return &____loomeltrorDerislar_5; }
	inline void set__loomeltrorDerislar_5(float value)
	{
		____loomeltrorDerislar_5 = value;
	}

	inline static int32_t get_offset_of__draywhou_6() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____draywhou_6)); }
	inline uint32_t get__draywhou_6() const { return ____draywhou_6; }
	inline uint32_t* get_address_of__draywhou_6() { return &____draywhou_6; }
	inline void set__draywhou_6(uint32_t value)
	{
		____draywhou_6 = value;
	}

	inline static int32_t get_offset_of__comuRawjehe_7() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____comuRawjehe_7)); }
	inline int32_t get__comuRawjehe_7() const { return ____comuRawjehe_7; }
	inline int32_t* get_address_of__comuRawjehe_7() { return &____comuRawjehe_7; }
	inline void set__comuRawjehe_7(int32_t value)
	{
		____comuRawjehe_7 = value;
	}

	inline static int32_t get_offset_of__stive_8() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____stive_8)); }
	inline bool get__stive_8() const { return ____stive_8; }
	inline bool* get_address_of__stive_8() { return &____stive_8; }
	inline void set__stive_8(bool value)
	{
		____stive_8 = value;
	}

	inline static int32_t get_offset_of__mowcoBemjerebu_9() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____mowcoBemjerebu_9)); }
	inline String_t* get__mowcoBemjerebu_9() const { return ____mowcoBemjerebu_9; }
	inline String_t** get_address_of__mowcoBemjerebu_9() { return &____mowcoBemjerebu_9; }
	inline void set__mowcoBemjerebu_9(String_t* value)
	{
		____mowcoBemjerebu_9 = value;
		Il2CppCodeGenWriteBarrier(&____mowcoBemjerebu_9, value);
	}

	inline static int32_t get_offset_of__remrere_10() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____remrere_10)); }
	inline uint32_t get__remrere_10() const { return ____remrere_10; }
	inline uint32_t* get_address_of__remrere_10() { return &____remrere_10; }
	inline void set__remrere_10(uint32_t value)
	{
		____remrere_10 = value;
	}

	inline static int32_t get_offset_of__wemzistou_11() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____wemzistou_11)); }
	inline uint32_t get__wemzistou_11() const { return ____wemzistou_11; }
	inline uint32_t* get_address_of__wemzistou_11() { return &____wemzistou_11; }
	inline void set__wemzistou_11(uint32_t value)
	{
		____wemzistou_11 = value;
	}

	inline static int32_t get_offset_of__zallstereMarxoure_12() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____zallstereMarxoure_12)); }
	inline bool get__zallstereMarxoure_12() const { return ____zallstereMarxoure_12; }
	inline bool* get_address_of__zallstereMarxoure_12() { return &____zallstereMarxoure_12; }
	inline void set__zallstereMarxoure_12(bool value)
	{
		____zallstereMarxoure_12 = value;
	}

	inline static int32_t get_offset_of__meko_13() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____meko_13)); }
	inline bool get__meko_13() const { return ____meko_13; }
	inline bool* get_address_of__meko_13() { return &____meko_13; }
	inline void set__meko_13(bool value)
	{
		____meko_13 = value;
	}

	inline static int32_t get_offset_of__helnow_14() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____helnow_14)); }
	inline int32_t get__helnow_14() const { return ____helnow_14; }
	inline int32_t* get_address_of__helnow_14() { return &____helnow_14; }
	inline void set__helnow_14(int32_t value)
	{
		____helnow_14 = value;
	}

	inline static int32_t get_offset_of__temai_15() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____temai_15)); }
	inline float get__temai_15() const { return ____temai_15; }
	inline float* get_address_of__temai_15() { return &____temai_15; }
	inline void set__temai_15(float value)
	{
		____temai_15 = value;
	}

	inline static int32_t get_offset_of__bewem_16() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____bewem_16)); }
	inline String_t* get__bewem_16() const { return ____bewem_16; }
	inline String_t** get_address_of__bewem_16() { return &____bewem_16; }
	inline void set__bewem_16(String_t* value)
	{
		____bewem_16 = value;
		Il2CppCodeGenWriteBarrier(&____bewem_16, value);
	}

	inline static int32_t get_offset_of__ditire_17() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____ditire_17)); }
	inline bool get__ditire_17() const { return ____ditire_17; }
	inline bool* get_address_of__ditire_17() { return &____ditire_17; }
	inline void set__ditire_17(bool value)
	{
		____ditire_17 = value;
	}

	inline static int32_t get_offset_of__sujorRaircipe_18() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____sujorRaircipe_18)); }
	inline uint32_t get__sujorRaircipe_18() const { return ____sujorRaircipe_18; }
	inline uint32_t* get_address_of__sujorRaircipe_18() { return &____sujorRaircipe_18; }
	inline void set__sujorRaircipe_18(uint32_t value)
	{
		____sujorRaircipe_18 = value;
	}

	inline static int32_t get_offset_of__cesir_19() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____cesir_19)); }
	inline String_t* get__cesir_19() const { return ____cesir_19; }
	inline String_t** get_address_of__cesir_19() { return &____cesir_19; }
	inline void set__cesir_19(String_t* value)
	{
		____cesir_19 = value;
		Il2CppCodeGenWriteBarrier(&____cesir_19, value);
	}

	inline static int32_t get_offset_of__sidaYarlowlow_20() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____sidaYarlowlow_20)); }
	inline float get__sidaYarlowlow_20() const { return ____sidaYarlowlow_20; }
	inline float* get_address_of__sidaYarlowlow_20() { return &____sidaYarlowlow_20; }
	inline void set__sidaYarlowlow_20(float value)
	{
		____sidaYarlowlow_20 = value;
	}

	inline static int32_t get_offset_of__chunerke_21() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____chunerke_21)); }
	inline String_t* get__chunerke_21() const { return ____chunerke_21; }
	inline String_t** get_address_of__chunerke_21() { return &____chunerke_21; }
	inline void set__chunerke_21(String_t* value)
	{
		____chunerke_21 = value;
		Il2CppCodeGenWriteBarrier(&____chunerke_21, value);
	}

	inline static int32_t get_offset_of__dearmourereSenaygall_22() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____dearmourereSenaygall_22)); }
	inline int32_t get__dearmourereSenaygall_22() const { return ____dearmourereSenaygall_22; }
	inline int32_t* get_address_of__dearmourereSenaygall_22() { return &____dearmourereSenaygall_22; }
	inline void set__dearmourereSenaygall_22(int32_t value)
	{
		____dearmourereSenaygall_22 = value;
	}

	inline static int32_t get_offset_of__tomallmiHejerhu_23() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____tomallmiHejerhu_23)); }
	inline uint32_t get__tomallmiHejerhu_23() const { return ____tomallmiHejerhu_23; }
	inline uint32_t* get_address_of__tomallmiHejerhu_23() { return &____tomallmiHejerhu_23; }
	inline void set__tomallmiHejerhu_23(uint32_t value)
	{
		____tomallmiHejerhu_23 = value;
	}

	inline static int32_t get_offset_of__mastall_24() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____mastall_24)); }
	inline uint32_t get__mastall_24() const { return ____mastall_24; }
	inline uint32_t* get_address_of__mastall_24() { return &____mastall_24; }
	inline void set__mastall_24(uint32_t value)
	{
		____mastall_24 = value;
	}

	inline static int32_t get_offset_of__paneCeryerevoo_25() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____paneCeryerevoo_25)); }
	inline float get__paneCeryerevoo_25() const { return ____paneCeryerevoo_25; }
	inline float* get_address_of__paneCeryerevoo_25() { return &____paneCeryerevoo_25; }
	inline void set__paneCeryerevoo_25(float value)
	{
		____paneCeryerevoo_25 = value;
	}

	inline static int32_t get_offset_of__jayqoomaTearjea_26() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____jayqoomaTearjea_26)); }
	inline bool get__jayqoomaTearjea_26() const { return ____jayqoomaTearjea_26; }
	inline bool* get_address_of__jayqoomaTearjea_26() { return &____jayqoomaTearjea_26; }
	inline void set__jayqoomaTearjea_26(bool value)
	{
		____jayqoomaTearjea_26 = value;
	}

	inline static int32_t get_offset_of__delel_27() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____delel_27)); }
	inline bool get__delel_27() const { return ____delel_27; }
	inline bool* get_address_of__delel_27() { return &____delel_27; }
	inline void set__delel_27(bool value)
	{
		____delel_27 = value;
	}

	inline static int32_t get_offset_of__tearxecheJirzarpee_28() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____tearxecheJirzarpee_28)); }
	inline int32_t get__tearxecheJirzarpee_28() const { return ____tearxecheJirzarpee_28; }
	inline int32_t* get_address_of__tearxecheJirzarpee_28() { return &____tearxecheJirzarpee_28; }
	inline void set__tearxecheJirzarpee_28(int32_t value)
	{
		____tearxecheJirzarpee_28 = value;
	}

	inline static int32_t get_offset_of__nideDenahair_29() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____nideDenahair_29)); }
	inline int32_t get__nideDenahair_29() const { return ____nideDenahair_29; }
	inline int32_t* get_address_of__nideDenahair_29() { return &____nideDenahair_29; }
	inline void set__nideDenahair_29(int32_t value)
	{
		____nideDenahair_29 = value;
	}

	inline static int32_t get_offset_of__souterTresa_30() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____souterTresa_30)); }
	inline bool get__souterTresa_30() const { return ____souterTresa_30; }
	inline bool* get_address_of__souterTresa_30() { return &____souterTresa_30; }
	inline void set__souterTresa_30(bool value)
	{
		____souterTresa_30 = value;
	}

	inline static int32_t get_offset_of__viwawSairzemre_31() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____viwawSairzemre_31)); }
	inline float get__viwawSairzemre_31() const { return ____viwawSairzemre_31; }
	inline float* get_address_of__viwawSairzemre_31() { return &____viwawSairzemre_31; }
	inline void set__viwawSairzemre_31(float value)
	{
		____viwawSairzemre_31 = value;
	}

	inline static int32_t get_offset_of__saymahuMairchafo_32() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____saymahuMairchafo_32)); }
	inline bool get__saymahuMairchafo_32() const { return ____saymahuMairchafo_32; }
	inline bool* get_address_of__saymahuMairchafo_32() { return &____saymahuMairchafo_32; }
	inline void set__saymahuMairchafo_32(bool value)
	{
		____saymahuMairchafo_32 = value;
	}

	inline static int32_t get_offset_of__temawJevayse_33() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____temawJevayse_33)); }
	inline int32_t get__temawJevayse_33() const { return ____temawJevayse_33; }
	inline int32_t* get_address_of__temawJevayse_33() { return &____temawJevayse_33; }
	inline void set__temawJevayse_33(int32_t value)
	{
		____temawJevayse_33 = value;
	}

	inline static int32_t get_offset_of__zezunouQesearsee_34() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____zezunouQesearsee_34)); }
	inline float get__zezunouQesearsee_34() const { return ____zezunouQesearsee_34; }
	inline float* get_address_of__zezunouQesearsee_34() { return &____zezunouQesearsee_34; }
	inline void set__zezunouQesearsee_34(float value)
	{
		____zezunouQesearsee_34 = value;
	}

	inline static int32_t get_offset_of__mukelRoqeeje_35() { return static_cast<int32_t>(offsetof(M_buloule196_t722519942, ____mukelRoqeeje_35)); }
	inline String_t* get__mukelRoqeeje_35() const { return ____mukelRoqeeje_35; }
	inline String_t** get_address_of__mukelRoqeeje_35() { return &____mukelRoqeeje_35; }
	inline void set__mukelRoqeeje_35(String_t* value)
	{
		____mukelRoqeeje_35 = value;
		Il2CppCodeGenWriteBarrier(&____mukelRoqeeje_35, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
