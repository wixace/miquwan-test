﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;
// System.IO.TextWriter
struct TextWriter_t2304124208;
// System.Enum
struct Enum_t2862688501;
// SerializableGuid
struct SerializableGuid_t3998617672;
// System.Uri
struct Uri_t1116831938;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// Newtonsoft.Json.JsonConverter[]
struct JsonConverterU5BU5D_t638349667;
// Newtonsoft.Json.JsonSerializerSettings
struct JsonSerializerSettings_t2589405525;
// System.Xml.XmlNode
struct XmlNode_t856910923;
// System.Xml.XmlDocument
struct XmlDocument_t730752740;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>
struct IList_1_t559366761;
// Newtonsoft.Json.Converters.XmlNodeConverter
struct XmlNodeConverter_t567010885;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_DateTimeOffset3884714306.h"
#include "mscorlib_System_TimeSpan413522987.h"
#include "mscorlib_System_IO_TextWriter2304124208.h"
#include "mscorlib_System_DateTimeKind1472618179.h"
#include "mscorlib_System_Enum2862688501.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_Guid2862754429.h"
#include "AssemblyU2DCSharp_SerializableGuid3998617672.h"
#include "System_System_Uri1116831938.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_TypeCode1814089915.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Formatting732683613.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializerSe2589405525.h"
#include "System_Xml_System_Xml_XmlNode856910923.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer251850770.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_XmlNod567010885.h"

// System.Void Newtonsoft.Json.JsonConvert::.cctor()
extern "C"  void JsonConvert__cctor_m3054884120 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.DateTime)
extern "C"  String_t* JsonConvert_ToString_m3527749856 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.DateTimeOffset)
extern "C"  String_t* JsonConvert_ToString_m1294160205 (Il2CppObject * __this /* static, unused */, DateTimeOffset_t3884714306  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan Newtonsoft.Json.JsonConvert::GetUtcOffset(System.DateTime)
extern "C"  TimeSpan_t413522987  JsonConvert_GetUtcOffset_m2311143013 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___dateTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonConvert::WriteDateTimeString(System.IO.TextWriter,System.DateTime)
extern "C"  void JsonConvert_WriteDateTimeString_m1251812455 (Il2CppObject * __this /* static, unused */, TextWriter_t2304124208 * ___writer0, DateTime_t4283661327  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonConvert::WriteDateTimeString(System.IO.TextWriter,System.DateTime,System.TimeSpan,System.DateTimeKind)
extern "C"  void JsonConvert_WriteDateTimeString_m2217067899 (Il2CppObject * __this /* static, unused */, TextWriter_t2304124208 * ___writer0, DateTime_t4283661327  ___value1, TimeSpan_t413522987  ___offset2, int32_t ___kind3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.JsonConvert::ToUniversalTicks(System.DateTime)
extern "C"  int64_t JsonConvert_ToUniversalTicks_m3167476146 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___dateTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.JsonConvert::ToUniversalTicks(System.DateTime,System.TimeSpan)
extern "C"  int64_t JsonConvert_ToUniversalTicks_m3182670344 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___dateTime0, TimeSpan_t413522987  ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.JsonConvert::ConvertDateTimeToJavaScriptTicks(System.DateTime,System.TimeSpan)
extern "C"  int64_t JsonConvert_ConvertDateTimeToJavaScriptTicks_m1053952366 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___dateTime0, TimeSpan_t413522987  ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.JsonConvert::ConvertDateTimeToJavaScriptTicks(System.DateTime)
extern "C"  int64_t JsonConvert_ConvertDateTimeToJavaScriptTicks_m1147256088 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___dateTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.JsonConvert::ConvertDateTimeToJavaScriptTicks(System.DateTime,System.Boolean)
extern "C"  int64_t JsonConvert_ConvertDateTimeToJavaScriptTicks_m792314725 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___dateTime0, bool ___convertToUtc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.JsonConvert::UniversialTicksToJavaScriptTicks(System.Int64)
extern "C"  int64_t JsonConvert_UniversialTicksToJavaScriptTicks_m3995657022 (Il2CppObject * __this /* static, unused */, int64_t ___universialTicks0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Newtonsoft.Json.JsonConvert::ConvertJavaScriptTicksToDateTime(System.Int64)
extern "C"  DateTime_t4283661327  JsonConvert_ConvertJavaScriptTicksToDateTime_m3057513940 (Il2CppObject * __this /* static, unused */, int64_t ___javaScriptTicks0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Boolean)
extern "C"  String_t* JsonConvert_ToString_m188564943 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Char)
extern "C"  String_t* JsonConvert_ToString_m1483640197 (Il2CppObject * __this /* static, unused */, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Enum)
extern "C"  String_t* JsonConvert_ToString_m1485685050 (Il2CppObject * __this /* static, unused */, Enum_t2862688501 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Int32)
extern "C"  String_t* JsonConvert_ToString_m3220994985 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Int16)
extern "C"  String_t* JsonConvert_ToString_m3220993187 (Il2CppObject * __this /* static, unused */, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.UInt16)
extern "C"  String_t* JsonConvert_ToString_m2063822492 (Il2CppObject * __this /* static, unused */, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.UInt32)
extern "C"  String_t* JsonConvert_ToString_m2063824290 (Il2CppObject * __this /* static, unused */, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Int64)
extern "C"  String_t* JsonConvert_ToString_m3220997930 (Il2CppObject * __this /* static, unused */, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.UInt64)
extern "C"  String_t* JsonConvert_ToString_m2063827235 (Il2CppObject * __this /* static, unused */, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Single)
extern "C"  String_t* JsonConvert_ToString_m1204618835 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Double)
extern "C"  String_t* JsonConvert_ToString_m955056106 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::EnsureDecimalPlace(System.Double,System.String)
extern "C"  String_t* JsonConvert_EnsureDecimalPlace_m719734846 (Il2CppObject * __this /* static, unused */, double ___value0, String_t* ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::EnsureDecimalPlace(System.String)
extern "C"  String_t* JsonConvert_EnsureDecimalPlace_m2406944578 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Byte)
extern "C"  String_t* JsonConvert_ToString_m1483240979 (Il2CppObject * __this /* static, unused */, uint8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.SByte)
extern "C"  String_t* JsonConvert_ToString_m3466864572 (Il2CppObject * __this /* static, unused */, int8_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Decimal)
extern "C"  String_t* JsonConvert_ToString_m3043001286 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Guid)
extern "C"  String_t* JsonConvert_ToString_m1487728818 (Il2CppObject * __this /* static, unused */, Guid_t2862754429  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(SerializableGuid)
extern "C"  String_t* JsonConvert_ToString_m514540816 (Il2CppObject * __this /* static, unused */, SerializableGuid_t3998617672 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.TimeSpan)
extern "C"  String_t* JsonConvert_ToString_m3812545604 (Il2CppObject * __this /* static, unused */, TimeSpan_t413522987  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Uri)
extern "C"  String_t* JsonConvert_ToString_m186952715 (Il2CppObject * __this /* static, unused */, Uri_t1116831938 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.String)
extern "C"  String_t* JsonConvert_ToString_m1523295146 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.String,System.Char)
extern "C"  String_t* JsonConvert_ToString_m3169420097 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Il2CppChar ___delimter1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ToString(System.Object)
extern "C"  String_t* JsonConvert_ToString_m1745405500 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonConvert::IsJsonPrimitiveTypeCode(System.TypeCode)
extern "C"  bool JsonConvert_IsJsonPrimitiveTypeCode_m570975977 (Il2CppObject * __this /* static, unused */, int32_t ___typeCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonConvert::IsJsonPrimitiveType(System.Type)
extern "C"  bool JsonConvert_IsJsonPrimitiveType_m2233907267 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonConvert::IsJsonPrimitive(System.Object)
extern "C"  bool JsonConvert_IsJsonPrimitive_m4189109496 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::SerializeObject(System.Object)
extern "C"  String_t* JsonConvert_SerializeObject_m695676031 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::SerializeObject(System.Object,Newtonsoft.Json.Formatting)
extern "C"  String_t* JsonConvert_SerializeObject_m1579456237 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, int32_t ___formatting1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::SerializeObject(System.Object,Newtonsoft.Json.JsonConverter[])
extern "C"  String_t* JsonConvert_SerializeObject_m3179709574 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, JsonConverterU5BU5D_t638349667* ___converters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::SerializeObject(System.Object,Newtonsoft.Json.Formatting,Newtonsoft.Json.JsonConverter[])
extern "C"  String_t* JsonConvert_SerializeObject_m1690220276 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, int32_t ___formatting1, JsonConverterU5BU5D_t638349667* ___converters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::SerializeObject(System.Object,Newtonsoft.Json.Formatting,Newtonsoft.Json.JsonSerializerSettings)
extern "C"  String_t* JsonConvert_SerializeObject_m3789630151 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, int32_t ___formatting1, JsonSerializerSettings_t2589405525 * ___settings2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonConvert::DeserializeObject(System.String)
extern "C"  Il2CppObject * JsonConvert_DeserializeObject_m3773224604 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonConvert::DeserializeObject(System.String,Newtonsoft.Json.JsonSerializerSettings)
extern "C"  Il2CppObject * JsonConvert_DeserializeObject_m2493363256 (Il2CppObject * __this /* static, unused */, String_t* ___value0, JsonSerializerSettings_t2589405525 * ___settings1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonConvert::DeserializeObject(System.String,System.Type)
extern "C"  Il2CppObject * JsonConvert_DeserializeObject_m43867087 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Type_t * ___type1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonConvert::DeserializeObject(System.String,System.Type,Newtonsoft.Json.JsonConverter[])
extern "C"  Il2CppObject * JsonConvert_DeserializeObject_m1838904278 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Type_t * ___type1, JsonConverterU5BU5D_t638349667* ___converters2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonConvert::DeserializeObject(System.String,System.Type,Newtonsoft.Json.JsonSerializerSettings)
extern "C"  Il2CppObject * JsonConvert_DeserializeObject_m2067958693 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Type_t * ___type1, JsonSerializerSettings_t2589405525 * ___settings2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonConvert::PopulateObject(System.String,System.Object)
extern "C"  void JsonConvert_PopulateObject_m1210203352 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Il2CppObject * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonConvert::PopulateObject(System.String,System.Object,Newtonsoft.Json.JsonSerializerSettings)
extern "C"  void JsonConvert_PopulateObject_m704643708 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Il2CppObject * ___target1, JsonSerializerSettings_t2589405525 * ___settings2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::SerializeXmlNode(System.Xml.XmlNode)
extern "C"  String_t* JsonConvert_SerializeXmlNode_m1224997954 (Il2CppObject * __this /* static, unused */, XmlNode_t856910923 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::SerializeXmlNode(System.Xml.XmlNode,Newtonsoft.Json.Formatting)
extern "C"  String_t* JsonConvert_SerializeXmlNode_m1531381066 (Il2CppObject * __this /* static, unused */, XmlNode_t856910923 * ___node0, int32_t ___formatting1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::SerializeXmlNode(System.Xml.XmlNode,Newtonsoft.Json.Formatting,System.Boolean)
extern "C"  String_t* JsonConvert_SerializeXmlNode_m3868774643 (Il2CppObject * __this /* static, unused */, XmlNode_t856910923 * ___node0, int32_t ___formatting1, bool ___omitRootObject2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlDocument Newtonsoft.Json.JsonConvert::DeserializeXmlNode(System.String)
extern "C"  XmlDocument_t730752740 * JsonConvert_DeserializeXmlNode_m1661705114 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlDocument Newtonsoft.Json.JsonConvert::DeserializeXmlNode(System.String,System.String)
extern "C"  XmlDocument_t730752740 * JsonConvert_DeserializeXmlNode_m2878828118 (Il2CppObject * __this /* static, unused */, String_t* ___value0, String_t* ___deserializeRootElementName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlDocument Newtonsoft.Json.JsonConvert::DeserializeXmlNode(System.String,System.String,System.Boolean)
extern "C"  XmlDocument_t730752740 * JsonConvert_DeserializeXmlNode_m1394585191 (Il2CppObject * __this /* static, unused */, String_t* ___value0, String_t* ___deserializeRootElementName1, bool ___writeArrayAttribute2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.TimeSpan Newtonsoft.Json.JsonConvert::ilo_GetUtcOffset1(System.DateTime)
extern "C"  TimeSpan_t413522987  JsonConvert_ilo_GetUtcOffset1_m3689952235 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___dateTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonConvert::ilo_WriteDateTimeString2(System.IO.TextWriter,System.DateTime,System.TimeSpan,System.DateTimeKind)
extern "C"  void JsonConvert_ilo_WriteDateTimeString2_m1318563846 (Il2CppObject * __this /* static, unused */, TextWriter_t2304124208 * ___writer0, DateTime_t4283661327  ___value1, TimeSpan_t413522987  ___offset2, int32_t ___kind3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.JsonConvert::ilo_ToUniversalTicks3(System.DateTime,System.TimeSpan)
extern "C"  int64_t JsonConvert_ilo_ToUniversalTicks3_m3120853586 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___dateTime0, TimeSpan_t413522987  ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.JsonConvert::ilo_ToUniversalTicks4(System.DateTime)
extern "C"  int64_t JsonConvert_ilo_ToUniversalTicks4_m1950109723 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___dateTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.JsonConvert::ilo_UniversialTicksToJavaScriptTicks5(System.Int64)
extern "C"  int64_t JsonConvert_ilo_UniversialTicksToJavaScriptTicks5_m1391430442 (Il2CppObject * __this /* static, unused */, int64_t ___universialTicks0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ilo_ToString6(System.String)
extern "C"  String_t* JsonConvert_ilo_ToString6_m2783488373 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ilo_ToString7(System.Char)
extern "C"  String_t* JsonConvert_ilo_ToString7_m1207325615 (Il2CppObject * __this /* static, unused */, Il2CppChar ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ilo_ToString8(System.Boolean)
extern "C"  String_t* JsonConvert_ilo_ToString8_m3306468710 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ilo_ToString9(System.UInt16)
extern "C"  String_t* JsonConvert_ilo_ToString9_m1792413188 (Il2CppObject * __this /* static, unused */, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ilo_ToString10(System.UInt32)
extern "C"  String_t* JsonConvert_ilo_ToString10_m1675308560 (Il2CppObject * __this /* static, unused */, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ilo_ToString11(System.Guid)
extern "C"  String_t* JsonConvert_ilo_ToString11_m825341503 (Il2CppObject * __this /* static, unused */, Guid_t2862754429  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.JsonConvert::ilo_FormatWith12(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* JsonConvert_ilo_FormatWith12_m2659647291 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonConvert::ilo_IsNullableType13(System.Type)
extern "C"  bool JsonConvert_ilo_IsNullableType13_m20722436 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.JsonConvert::ilo_IsJsonPrimitiveTypeCode14(System.TypeCode)
extern "C"  bool JsonConvert_ilo_IsJsonPrimitiveTypeCode14_m3313407257 (Il2CppObject * __this /* static, unused */, int32_t ___typeCode0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonConvert::ilo_Serialize15(Newtonsoft.Json.JsonSerializer,Newtonsoft.Json.JsonWriter,System.Object)
extern "C"  void JsonConvert_ilo_Serialize15_m1500896687 (Il2CppObject * __this /* static, unused */, JsonSerializer_t251850770 * ____this0, JsonWriter_t972330355 * ___jsonWriter1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.JsonConvert::ilo_DeserializeObject16(System.String,System.Type,Newtonsoft.Json.JsonSerializerSettings)
extern "C"  Il2CppObject * JsonConvert_ilo_DeserializeObject16_m897178487 (Il2CppObject * __this /* static, unused */, String_t* ___value0, Type_t * ___type1, JsonSerializerSettings_t2589405525 * ___settings2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonConvert::ilo_set_Converters17(Newtonsoft.Json.JsonSerializerSettings,System.Collections.Generic.IList`1<Newtonsoft.Json.JsonConverter>)
extern "C"  void JsonConvert_ilo_set_Converters17_m3208118148 (Il2CppObject * __this /* static, unused */, JsonSerializerSettings_t2589405525 * ____this0, Il2CppObject* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonConvert::ilo_set_DeserializeRootElementName18(Newtonsoft.Json.Converters.XmlNodeConverter,System.String)
extern "C"  void JsonConvert_ilo_set_DeserializeRootElementName18_m1281909973 (Il2CppObject * __this /* static, unused */, XmlNodeConverter_t567010885 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.JsonConvert::ilo_set_WriteArrayAttribute19(Newtonsoft.Json.Converters.XmlNodeConverter,System.Boolean)
extern "C"  void JsonConvert_ilo_set_WriteArrayAttribute19_m712358309 (Il2CppObject * __this /* static, unused */, XmlNodeConverter_t567010885 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
