﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameInitialize
struct GameInitialize_t3015964994;
// FlowControl.FlowBase
struct FlowBase_t3680091731;
// System.String
struct String_t;
// FlowControl.FlowContainerBase
struct FlowContainerBase_t4203254394;
// FlowControl.IFlowBase
struct IFlowBase_t1464761278;
// FlowControl.FlowContainerAnd
struct FlowContainerAnd_t1365439512;
// UIModelDisplayMgr
struct UIModelDisplayMgr_t1446490315;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FlowControl_FlowBase3680091731.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_FlowControl_FlowContainerBase4203254394.h"
#include "AssemblyU2DCSharp_FlowControl_FlowContainerAnd1365439512.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void GameInitialize::.ctor()
extern "C"  void GameInitialize__ctor_m2812878233 (GameInitialize_t3015964994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameInitialize::Awake()
extern "C"  void GameInitialize_Awake_m3050483452 (GameInitialize_t3015964994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameInitialize::nom()
extern "C"  void GameInitialize_nom_m2203160611 (GameInitialize_t3015964994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameInitialize::ilo_set_name1(FlowControl.FlowBase,System.String)
extern "C"  void GameInitialize_ilo_set_name1_m4036048651 (Il2CppObject * __this /* static, unused */, FlowBase_t3680091731 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameInitialize::ilo_AddFlow2(FlowControl.FlowContainerBase,FlowControl.IFlowBase)
extern "C"  void GameInitialize_ilo_AddFlow2_m588693839 (Il2CppObject * __this /* static, unused */, FlowContainerBase_t4203254394 * ____this0, Il2CppObject * ___flow1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameInitialize::ilo_Activate3(FlowControl.FlowContainerAnd)
extern "C"  void GameInitialize_ilo_Activate3_m2901669441 (Il2CppObject * __this /* static, unused */, FlowContainerAnd_t1365439512 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIModelDisplayMgr GameInitialize::ilo_get_Instance4()
extern "C"  UIModelDisplayMgr_t1446490315 * GameInitialize_ilo_get_Instance4_m209248594 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameInitialize::ilo_Log5(System.Object,System.Boolean)
extern "C"  void GameInitialize_ilo_Log5_m3769825176 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
