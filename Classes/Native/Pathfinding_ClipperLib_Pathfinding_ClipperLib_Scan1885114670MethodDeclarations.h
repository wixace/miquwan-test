﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.ClipperLib.Scanbeam
struct Scanbeam_t1885114670;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.ClipperLib.Scanbeam::.ctor()
extern "C"  void Scanbeam__ctor_m2642114495 (Scanbeam_t1885114670 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
