//  iOSipv6.h 
//  Unity-iPhone 
//  Created by luzufei on 2017/3/2. 
 
#ifndef iOSipv6_h 
#define iOSipv6_h 
@interface BundleId : NSObject 
+(const char*)getIPv6:(const char*)mHost withPort:(const char* )mPort; 
@end 
#endif /* iOSipv6_h */ 
