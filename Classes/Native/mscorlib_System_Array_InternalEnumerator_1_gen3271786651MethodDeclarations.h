﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3271786651.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_HumanBone194476679.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.HumanBone>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1737148479_gshared (InternalEnumerator_1_t3271786651 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1737148479(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3271786651 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1737148479_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.HumanBone>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1691496193_gshared (InternalEnumerator_1_t3271786651 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1691496193(__this, method) ((  void (*) (InternalEnumerator_1_t3271786651 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1691496193_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.HumanBone>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3560231159_gshared (InternalEnumerator_1_t3271786651 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3560231159(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3271786651 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3560231159_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.HumanBone>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m559596758_gshared (InternalEnumerator_1_t3271786651 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m559596758(__this, method) ((  void (*) (InternalEnumerator_1_t3271786651 *, const MethodInfo*))InternalEnumerator_1_Dispose_m559596758_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.HumanBone>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2716396977_gshared (InternalEnumerator_1_t3271786651 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2716396977(__this, method) ((  bool (*) (InternalEnumerator_1_t3271786651 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2716396977_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.HumanBone>::get_Current()
extern "C"  HumanBone_t194476679  InternalEnumerator_1_get_Current_m2547607912_gshared (InternalEnumerator_1_t3271786651 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2547607912(__this, method) ((  HumanBone_t194476679  (*) (InternalEnumerator_1_t3271786651 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2547607912_gshared)(__this, method)
