﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEffectMgr/<Clear>c__AnonStorey15C
struct U3CClearU3Ec__AnonStorey15C_t2197468457;

#include "codegen/il2cpp-codegen.h"

// System.Void UIEffectMgr/<Clear>c__AnonStorey15C::.ctor()
extern "C"  void U3CClearU3Ec__AnonStorey15C__ctor_m3401534562 (U3CClearU3Ec__AnonStorey15C_t2197468457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEffectMgr/<Clear>c__AnonStorey15C::<>m__3EA()
extern "C"  void U3CClearU3Ec__AnonStorey15C_U3CU3Em__3EA_m2490013926 (U3CClearU3Ec__AnonStorey15C_t2197468457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
