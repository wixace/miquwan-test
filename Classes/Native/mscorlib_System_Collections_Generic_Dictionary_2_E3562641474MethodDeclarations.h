﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Int32>
struct Dictionary_2_t2245318082;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3562641474.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22144098788.h"
#include "AssemblyU2DCSharp_Pathfinding_Int21974045593.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3874573919_gshared (Enumerator_t3562641474 * __this, Dictionary_2_t2245318082 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m3874573919(__this, ___dictionary0, method) ((  void (*) (Enumerator_t3562641474 *, Dictionary_2_t2245318082 *, const MethodInfo*))Enumerator__ctor_m3874573919_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m835081058_gshared (Enumerator_t3562641474 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m835081058(__this, method) ((  Il2CppObject * (*) (Enumerator_t3562641474 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m835081058_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m479798518_gshared (Enumerator_t3562641474 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m479798518(__this, method) ((  void (*) (Enumerator_t3562641474 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m479798518_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2407170751_gshared (Enumerator_t3562641474 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2407170751(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t3562641474 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2407170751_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4163763902_gshared (Enumerator_t3562641474 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4163763902(__this, method) ((  Il2CppObject * (*) (Enumerator_t3562641474 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4163763902_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3810993616_gshared (Enumerator_t3562641474 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3810993616(__this, method) ((  Il2CppObject * (*) (Enumerator_t3562641474 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3810993616_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1659461474_gshared (Enumerator_t3562641474 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1659461474(__this, method) ((  bool (*) (Enumerator_t3562641474 *, const MethodInfo*))Enumerator_MoveNext_m1659461474_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Int32>::get_Current()
extern "C"  KeyValuePair_2_t2144098788  Enumerator_get_Current_m3149463822_gshared (Enumerator_t3562641474 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3149463822(__this, method) ((  KeyValuePair_2_t2144098788  (*) (Enumerator_t3562641474 *, const MethodInfo*))Enumerator_get_Current_m3149463822_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Int32>::get_CurrentKey()
extern "C"  Int2_t1974045593  Enumerator_get_CurrentKey_m3214553391_gshared (Enumerator_t3562641474 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m3214553391(__this, method) ((  Int2_t1974045593  (*) (Enumerator_t3562641474 *, const MethodInfo*))Enumerator_get_CurrentKey_m3214553391_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Int32>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m3044510099_gshared (Enumerator_t3562641474 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m3044510099(__this, method) ((  int32_t (*) (Enumerator_t3562641474 *, const MethodInfo*))Enumerator_get_CurrentValue_m3044510099_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Int32>::Reset()
extern "C"  void Enumerator_Reset_m2563689009_gshared (Enumerator_t3562641474 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2563689009(__this, method) ((  void (*) (Enumerator_t3562641474 *, const MethodInfo*))Enumerator_Reset_m2563689009_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Int32>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1127477626_gshared (Enumerator_t3562641474 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1127477626(__this, method) ((  void (*) (Enumerator_t3562641474 *, const MethodInfo*))Enumerator_VerifyState_m1127477626_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Int32>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m3919111778_gshared (Enumerator_t3562641474 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m3919111778(__this, method) ((  void (*) (Enumerator_t3562641474 *, const MethodInfo*))Enumerator_VerifyCurrent_m3919111778_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int2,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m917338817_gshared (Enumerator_t3562641474 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m917338817(__this, method) ((  void (*) (Enumerator_t3562641474 *, const MethodInfo*))Enumerator_Dispose_m917338817_gshared)(__this, method)
