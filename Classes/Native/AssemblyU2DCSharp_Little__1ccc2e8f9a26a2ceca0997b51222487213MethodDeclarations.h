﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._1ccc2e8f9a26a2ceca0997b5916a6b94
struct _1ccc2e8f9a26a2ceca0997b5916a6b94_t1222487213;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__1ccc2e8f9a26a2ceca0997b51222487213.h"

// System.Void Little._1ccc2e8f9a26a2ceca0997b5916a6b94::.ctor()
extern "C"  void _1ccc2e8f9a26a2ceca0997b5916a6b94__ctor_m2301127712 (_1ccc2e8f9a26a2ceca0997b5916a6b94_t1222487213 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1ccc2e8f9a26a2ceca0997b5916a6b94::_1ccc2e8f9a26a2ceca0997b5916a6b94m2(System.Int32)
extern "C"  int32_t _1ccc2e8f9a26a2ceca0997b5916a6b94__1ccc2e8f9a26a2ceca0997b5916a6b94m2_m3127569017 (_1ccc2e8f9a26a2ceca0997b5916a6b94_t1222487213 * __this, int32_t ____1ccc2e8f9a26a2ceca0997b5916a6b94a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1ccc2e8f9a26a2ceca0997b5916a6b94::_1ccc2e8f9a26a2ceca0997b5916a6b94m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _1ccc2e8f9a26a2ceca0997b5916a6b94__1ccc2e8f9a26a2ceca0997b5916a6b94m_m3129309277 (_1ccc2e8f9a26a2ceca0997b5916a6b94_t1222487213 * __this, int32_t ____1ccc2e8f9a26a2ceca0997b5916a6b94a0, int32_t ____1ccc2e8f9a26a2ceca0997b5916a6b94351, int32_t ____1ccc2e8f9a26a2ceca0997b5916a6b94c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1ccc2e8f9a26a2ceca0997b5916a6b94::ilo__1ccc2e8f9a26a2ceca0997b5916a6b94m21(Little._1ccc2e8f9a26a2ceca0997b5916a6b94,System.Int32)
extern "C"  int32_t _1ccc2e8f9a26a2ceca0997b5916a6b94_ilo__1ccc2e8f9a26a2ceca0997b5916a6b94m21_m85487380 (Il2CppObject * __this /* static, unused */, _1ccc2e8f9a26a2ceca0997b5916a6b94_t1222487213 * ____this0, int32_t ____1ccc2e8f9a26a2ceca0997b5916a6b94a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
