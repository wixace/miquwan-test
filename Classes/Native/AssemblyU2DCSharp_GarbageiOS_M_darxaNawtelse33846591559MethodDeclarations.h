﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_darxaNawtelse33
struct M_darxaNawtelse33_t846591559;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_darxaNawtelse33::.ctor()
extern "C"  void M_darxaNawtelse33__ctor_m3177634044 (M_darxaNawtelse33_t846591559 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_darxaNawtelse33::M_ceneJape0(System.String[],System.Int32)
extern "C"  void M_darxaNawtelse33_M_ceneJape0_m1799268010 (M_darxaNawtelse33_t846591559 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
