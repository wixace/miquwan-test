﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_NavMeshObstacleGenerated
struct UnityEngine_NavMeshObstacleGenerated_t1447408984;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_NavMeshObstacleGenerated::.ctor()
extern "C"  void UnityEngine_NavMeshObstacleGenerated__ctor_m2999318595 (UnityEngine_NavMeshObstacleGenerated_t1447408984 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshObstacleGenerated::NavMeshObstacle_NavMeshObstacle1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_NavMeshObstacleGenerated_NavMeshObstacle_NavMeshObstacle1_m443168663 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshObstacleGenerated::NavMeshObstacle_height(JSVCall)
extern "C"  void UnityEngine_NavMeshObstacleGenerated_NavMeshObstacle_height_m736573631 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshObstacleGenerated::NavMeshObstacle_radius(JSVCall)
extern "C"  void UnityEngine_NavMeshObstacleGenerated_NavMeshObstacle_radius_m690619828 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshObstacleGenerated::NavMeshObstacle_velocity(JSVCall)
extern "C"  void UnityEngine_NavMeshObstacleGenerated_NavMeshObstacle_velocity_m3628218825 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshObstacleGenerated::NavMeshObstacle_carving(JSVCall)
extern "C"  void UnityEngine_NavMeshObstacleGenerated_NavMeshObstacle_carving_m1017576326 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshObstacleGenerated::NavMeshObstacle_carveOnlyStationary(JSVCall)
extern "C"  void UnityEngine_NavMeshObstacleGenerated_NavMeshObstacle_carveOnlyStationary_m514426499 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshObstacleGenerated::NavMeshObstacle_carvingMoveThreshold(JSVCall)
extern "C"  void UnityEngine_NavMeshObstacleGenerated_NavMeshObstacle_carvingMoveThreshold_m1942074700 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshObstacleGenerated::NavMeshObstacle_carvingTimeToStationary(JSVCall)
extern "C"  void UnityEngine_NavMeshObstacleGenerated_NavMeshObstacle_carvingTimeToStationary_m3978727914 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshObstacleGenerated::NavMeshObstacle_shape(JSVCall)
extern "C"  void UnityEngine_NavMeshObstacleGenerated_NavMeshObstacle_shape_m841323141 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshObstacleGenerated::NavMeshObstacle_center(JSVCall)
extern "C"  void UnityEngine_NavMeshObstacleGenerated_NavMeshObstacle_center_m3488723281 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshObstacleGenerated::NavMeshObstacle_size(JSVCall)
extern "C"  void UnityEngine_NavMeshObstacleGenerated_NavMeshObstacle_size_m3064078117 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshObstacleGenerated::__Register()
extern "C"  void UnityEngine_NavMeshObstacleGenerated___Register_m1244345956 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshObstacleGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_NavMeshObstacleGenerated_ilo_attachFinalizerObject1_m2760880636 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshObstacleGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_NavMeshObstacleGenerated_ilo_addJSCSRel2_m2767572480 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_NavMeshObstacleGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_NavMeshObstacleGenerated_ilo_getSingle3_m1235394374 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_NavMeshObstacleGenerated::ilo_getBooleanS4(System.Int32)
extern "C"  bool UnityEngine_NavMeshObstacleGenerated_ilo_getBooleanS4_m2942108140 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshObstacleGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void UnityEngine_NavMeshObstacleGenerated_ilo_setBooleanS5_m1422835440 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshObstacleGenerated::ilo_setEnum6(System.Int32,System.Int32)
extern "C"  void UnityEngine_NavMeshObstacleGenerated_ilo_setEnum6_m3403112127 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_NavMeshObstacleGenerated::ilo_getVector3S7(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_NavMeshObstacleGenerated_ilo_getVector3S7_m3530608541 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_NavMeshObstacleGenerated::ilo_setVector3S8(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_NavMeshObstacleGenerated_ilo_setVector3S8_m2818560107 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
