﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t291504320;

#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Float_rewards
struct  Float_rewards_t2465166369  : public FloatTextUnit_t2362298029
{
public:
	// UILabel Float_rewards::labelNum
	UILabel_t291504320 * ___labelNum_3;

public:
	inline static int32_t get_offset_of_labelNum_3() { return static_cast<int32_t>(offsetof(Float_rewards_t2465166369, ___labelNum_3)); }
	inline UILabel_t291504320 * get_labelNum_3() const { return ___labelNum_3; }
	inline UILabel_t291504320 ** get_address_of_labelNum_3() { return &___labelNum_3; }
	inline void set_labelNum_3(UILabel_t291504320 * value)
	{
		___labelNum_3 = value;
		Il2CppCodeGenWriteBarrier(&___labelNum_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
