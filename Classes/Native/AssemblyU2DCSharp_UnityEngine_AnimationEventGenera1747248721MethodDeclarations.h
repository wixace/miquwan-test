﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AnimationEventGenerated
struct UnityEngine_AnimationEventGenerated_t1747248721;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_AnimationEventGenerated::.ctor()
extern "C"  void UnityEngine_AnimationEventGenerated__ctor_m1324738618 (UnityEngine_AnimationEventGenerated_t1747248721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AnimationEventGenerated::AnimationEvent_AnimationEvent1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AnimationEventGenerated_AnimationEvent_AnimationEvent1_m3543616760 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationEventGenerated::AnimationEvent_stringParameter(JSVCall)
extern "C"  void UnityEngine_AnimationEventGenerated_AnimationEvent_stringParameter_m189815022 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationEventGenerated::AnimationEvent_floatParameter(JSVCall)
extern "C"  void UnityEngine_AnimationEventGenerated_AnimationEvent_floatParameter_m112062585 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationEventGenerated::AnimationEvent_intParameter(JSVCall)
extern "C"  void UnityEngine_AnimationEventGenerated_AnimationEvent_intParameter_m1531479884 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationEventGenerated::AnimationEvent_objectReferenceParameter(JSVCall)
extern "C"  void UnityEngine_AnimationEventGenerated_AnimationEvent_objectReferenceParameter_m376194281 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationEventGenerated::AnimationEvent_functionName(JSVCall)
extern "C"  void UnityEngine_AnimationEventGenerated_AnimationEvent_functionName_m1770931043 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationEventGenerated::AnimationEvent_time(JSVCall)
extern "C"  void UnityEngine_AnimationEventGenerated_AnimationEvent_time_m503437913 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationEventGenerated::AnimationEvent_messageOptions(JSVCall)
extern "C"  void UnityEngine_AnimationEventGenerated_AnimationEvent_messageOptions_m3794106319 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationEventGenerated::AnimationEvent_isFiredByLegacy(JSVCall)
extern "C"  void UnityEngine_AnimationEventGenerated_AnimationEvent_isFiredByLegacy_m4030200834 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationEventGenerated::AnimationEvent_isFiredByAnimator(JSVCall)
extern "C"  void UnityEngine_AnimationEventGenerated_AnimationEvent_isFiredByAnimator_m1775137124 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationEventGenerated::AnimationEvent_animationState(JSVCall)
extern "C"  void UnityEngine_AnimationEventGenerated_AnimationEvent_animationState_m3405862329 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationEventGenerated::AnimationEvent_animatorStateInfo(JSVCall)
extern "C"  void UnityEngine_AnimationEventGenerated_AnimationEvent_animatorStateInfo_m1324899790 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationEventGenerated::AnimationEvent_animatorClipInfo(JSVCall)
extern "C"  void UnityEngine_AnimationEventGenerated_AnimationEvent_animatorClipInfo_m277324801 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationEventGenerated::__Register()
extern "C"  void UnityEngine_AnimationEventGenerated___Register_m1863690445 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationEventGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_AnimationEventGenerated_ilo_addJSCSRel1_m2444716534 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_AnimationEventGenerated::ilo_getStringS2(System.Int32)
extern "C"  String_t* UnityEngine_AnimationEventGenerated_ilo_getStringS2_m2695924127 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationEventGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_AnimationEventGenerated_ilo_setSingle3_m429571836 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_AnimationEventGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_AnimationEventGenerated_ilo_getObject4_m295677166 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_AnimationEventGenerated::ilo_getSingle5(System.Int32)
extern "C"  float UnityEngine_AnimationEventGenerated_ilo_getSingle5_m3100251929 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationEventGenerated::ilo_setEnum6(System.Int32,System.Int32)
extern "C"  void UnityEngine_AnimationEventGenerated_ilo_setEnum6_m3199316662 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimationEventGenerated::ilo_getEnum7(System.Int32)
extern "C"  int32_t UnityEngine_AnimationEventGenerated_ilo_getEnum7_m12438208 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AnimationEventGenerated::ilo_setBooleanS8(System.Int32,System.Boolean)
extern "C"  void UnityEngine_AnimationEventGenerated_ilo_setBooleanS8_m780664964 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AnimationEventGenerated::ilo_setObject9(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_AnimationEventGenerated_ilo_setObject9_m4167709372 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
