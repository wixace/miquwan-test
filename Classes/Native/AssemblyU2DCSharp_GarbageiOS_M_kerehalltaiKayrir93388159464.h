﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_kerehalltaiKayrir93
struct  M_kerehalltaiKayrir93_t388159464  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_kerehalltaiKayrir93::_mallpairdaw
	int32_t ____mallpairdaw_0;
	// System.Boolean GarbageiOS.M_kerehalltaiKayrir93::_sebugooTirha
	bool ____sebugooTirha_1;
	// System.UInt32 GarbageiOS.M_kerehalltaiKayrir93::_ruca
	uint32_t ____ruca_2;
	// System.Single GarbageiOS.M_kerehalltaiKayrir93::_moto
	float ____moto_3;
	// System.Single GarbageiOS.M_kerehalltaiKayrir93::_kerrouChatrou
	float ____kerrouChatrou_4;

public:
	inline static int32_t get_offset_of__mallpairdaw_0() { return static_cast<int32_t>(offsetof(M_kerehalltaiKayrir93_t388159464, ____mallpairdaw_0)); }
	inline int32_t get__mallpairdaw_0() const { return ____mallpairdaw_0; }
	inline int32_t* get_address_of__mallpairdaw_0() { return &____mallpairdaw_0; }
	inline void set__mallpairdaw_0(int32_t value)
	{
		____mallpairdaw_0 = value;
	}

	inline static int32_t get_offset_of__sebugooTirha_1() { return static_cast<int32_t>(offsetof(M_kerehalltaiKayrir93_t388159464, ____sebugooTirha_1)); }
	inline bool get__sebugooTirha_1() const { return ____sebugooTirha_1; }
	inline bool* get_address_of__sebugooTirha_1() { return &____sebugooTirha_1; }
	inline void set__sebugooTirha_1(bool value)
	{
		____sebugooTirha_1 = value;
	}

	inline static int32_t get_offset_of__ruca_2() { return static_cast<int32_t>(offsetof(M_kerehalltaiKayrir93_t388159464, ____ruca_2)); }
	inline uint32_t get__ruca_2() const { return ____ruca_2; }
	inline uint32_t* get_address_of__ruca_2() { return &____ruca_2; }
	inline void set__ruca_2(uint32_t value)
	{
		____ruca_2 = value;
	}

	inline static int32_t get_offset_of__moto_3() { return static_cast<int32_t>(offsetof(M_kerehalltaiKayrir93_t388159464, ____moto_3)); }
	inline float get__moto_3() const { return ____moto_3; }
	inline float* get_address_of__moto_3() { return &____moto_3; }
	inline void set__moto_3(float value)
	{
		____moto_3 = value;
	}

	inline static int32_t get_offset_of__kerrouChatrou_4() { return static_cast<int32_t>(offsetof(M_kerehalltaiKayrir93_t388159464, ____kerrouChatrou_4)); }
	inline float get__kerrouChatrou_4() const { return ____kerrouChatrou_4; }
	inline float* get_address_of__kerrouChatrou_4() { return &____kerrouChatrou_4; }
	inline void set__kerrouChatrou_4(float value)
	{
		____kerrouChatrou_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
