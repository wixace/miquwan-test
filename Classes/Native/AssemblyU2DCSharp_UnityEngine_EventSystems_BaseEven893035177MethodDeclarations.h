﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_BaseEventDataGenerated
struct UnityEngine_EventSystems_BaseEventDataGenerated_t893035177;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_EventSystems_BaseEventDataGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_BaseEventDataGenerated__ctor_m3007935202 (UnityEngine_EventSystems_BaseEventDataGenerated_t893035177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_BaseEventDataGenerated::BaseEventData_BaseEventData1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_BaseEventDataGenerated_BaseEventData_BaseEventData1_m3663198024 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_BaseEventDataGenerated::BaseEventData_currentInputModule(JSVCall)
extern "C"  void UnityEngine_EventSystems_BaseEventDataGenerated_BaseEventData_currentInputModule_m2551120156 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_BaseEventDataGenerated::BaseEventData_selectedObject(JSVCall)
extern "C"  void UnityEngine_EventSystems_BaseEventDataGenerated_BaseEventData_selectedObject_m3274952991 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_BaseEventDataGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_BaseEventDataGenerated___Register_m1645643557 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_EventSystems_BaseEventDataGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_EventSystems_BaseEventDataGenerated_ilo_setObject1_m2044384268 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_EventSystems_BaseEventDataGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_EventSystems_BaseEventDataGenerated_ilo_getObject2_m1805259972 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
