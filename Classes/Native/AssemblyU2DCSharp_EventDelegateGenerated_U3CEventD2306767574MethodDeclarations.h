﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EventDelegateGenerated/<EventDelegate_Add_GetDelegate_member5_arg1>c__AnonStorey5D
struct U3CEventDelegate_Add_GetDelegate_member5_arg1U3Ec__AnonStorey5D_t2306767574;

#include "codegen/il2cpp-codegen.h"

// System.Void EventDelegateGenerated/<EventDelegate_Add_GetDelegate_member5_arg1>c__AnonStorey5D::.ctor()
extern "C"  void U3CEventDelegate_Add_GetDelegate_member5_arg1U3Ec__AnonStorey5D__ctor_m65574533 (U3CEventDelegate_Add_GetDelegate_member5_arg1U3Ec__AnonStorey5D_t2306767574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void EventDelegateGenerated/<EventDelegate_Add_GetDelegate_member5_arg1>c__AnonStorey5D::<>m__46()
extern "C"  void U3CEventDelegate_Add_GetDelegate_member5_arg1U3Ec__AnonStorey5D_U3CU3Em__46_m900751888 (U3CEventDelegate_Add_GetDelegate_member5_arg1U3Ec__AnonStorey5D_t2306767574 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
