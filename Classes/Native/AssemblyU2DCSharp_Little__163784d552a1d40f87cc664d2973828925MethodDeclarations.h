﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._163784d552a1d40f87cc664dfde1d86d
struct _163784d552a1d40f87cc664dfde1d86d_t2973828925;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._163784d552a1d40f87cc664dfde1d86d::.ctor()
extern "C"  void _163784d552a1d40f87cc664dfde1d86d__ctor_m456782736 (_163784d552a1d40f87cc664dfde1d86d_t2973828925 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._163784d552a1d40f87cc664dfde1d86d::_163784d552a1d40f87cc664dfde1d86dm2(System.Int32)
extern "C"  int32_t _163784d552a1d40f87cc664dfde1d86d__163784d552a1d40f87cc664dfde1d86dm2_m1329619065 (_163784d552a1d40f87cc664dfde1d86d_t2973828925 * __this, int32_t ____163784d552a1d40f87cc664dfde1d86da0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._163784d552a1d40f87cc664dfde1d86d::_163784d552a1d40f87cc664dfde1d86dm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _163784d552a1d40f87cc664dfde1d86d__163784d552a1d40f87cc664dfde1d86dm_m4221616733 (_163784d552a1d40f87cc664dfde1d86d_t2973828925 * __this, int32_t ____163784d552a1d40f87cc664dfde1d86da0, int32_t ____163784d552a1d40f87cc664dfde1d86d491, int32_t ____163784d552a1d40f87cc664dfde1d86dc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
