﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_HingeJointGenerated
struct UnityEngine_HingeJointGenerated_t2757046216;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_HingeJointGenerated::.ctor()
extern "C"  void UnityEngine_HingeJointGenerated__ctor_m3058501795 (UnityEngine_HingeJointGenerated_t2757046216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_HingeJointGenerated::HingeJoint_HingeJoint1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_HingeJointGenerated_HingeJoint_HingeJoint1_m1337081935 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJointGenerated::HingeJoint_motor(JSVCall)
extern "C"  void UnityEngine_HingeJointGenerated_HingeJoint_motor_m3565257617 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJointGenerated::HingeJoint_limits(JSVCall)
extern "C"  void UnityEngine_HingeJointGenerated_HingeJoint_limits_m1178312078 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJointGenerated::HingeJoint_spring(JSVCall)
extern "C"  void UnityEngine_HingeJointGenerated_HingeJoint_spring_m2685250361 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJointGenerated::HingeJoint_useMotor(JSVCall)
extern "C"  void UnityEngine_HingeJointGenerated_HingeJoint_useMotor_m296631096 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJointGenerated::HingeJoint_useLimits(JSVCall)
extern "C"  void UnityEngine_HingeJointGenerated_HingeJoint_useLimits_m2930105031 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJointGenerated::HingeJoint_useSpring(JSVCall)
extern "C"  void UnityEngine_HingeJointGenerated_HingeJoint_useSpring_m142076018 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJointGenerated::HingeJoint_velocity(JSVCall)
extern "C"  void UnityEngine_HingeJointGenerated_HingeJoint_velocity_m2491798121 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJointGenerated::HingeJoint_angle(JSVCall)
extern "C"  void UnityEngine_HingeJointGenerated_HingeJoint_angle_m3307916531 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJointGenerated::__Register()
extern "C"  void UnityEngine_HingeJointGenerated___Register_m1415537156 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJointGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_HingeJointGenerated_ilo_addJSCSRel1_m1674856927 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_HingeJointGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_HingeJointGenerated_ilo_setObject2_m1432526636 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HingeJointGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void UnityEngine_HingeJointGenerated_ilo_setBooleanS3_m3918344210 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
