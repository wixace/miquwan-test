﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSCLevelConfig
struct JSCLevelConfig_t1411099500;
// ProtoBuf.IExtension
struct IExtension_t1606339106;
// System.String
struct String_t;
// System.Collections.Generic.List`1<JSCWaveNpcConfig>
struct List_1_t1294722912;
// JSCLevelHeroConfig
struct JSCLevelHeroConfig_t1953226502;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSCLevelHeroConfig1953226502.h"

// System.Void JSCLevelConfig::.ctor()
extern "C"  void JSCLevelConfig__ctor_m2649111215 (JSCLevelConfig_t1411099500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension JSCLevelConfig::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * JSCLevelConfig_ProtoBuf_IExtensible_GetExtensionObject_m1212073983 (JSCLevelConfig_t1411099500 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSCLevelConfig::get_id()
extern "C"  int32_t JSCLevelConfig_get_id_m2496477511 (JSCLevelConfig_t1411099500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCLevelConfig::set_id(System.Int32)
extern "C"  void JSCLevelConfig_set_id_m3597833598 (JSCLevelConfig_t1411099500 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSCLevelConfig::get_resName()
extern "C"  String_t* JSCLevelConfig_get_resName_m2929633932 (JSCLevelConfig_t1411099500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCLevelConfig::set_resName(System.String)
extern "C"  void JSCLevelConfig_set_resName_m1105329927 (JSCLevelConfig_t1411099500 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSCLevelConfig::get_type()
extern "C"  int32_t JSCLevelConfig_get_type_m2859655398 (JSCLevelConfig_t1411099500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCLevelConfig::set_type(System.Int32)
extern "C"  void JSCLevelConfig_set_type_m2131100957 (JSCLevelConfig_t1411099500 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 JSCLevelConfig::get_mapPathId()
extern "C"  int32_t JSCLevelConfig_get_mapPathId_m2706534994 (JSCLevelConfig_t1411099500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCLevelConfig::set_mapPathId(System.Int32)
extern "C"  void JSCLevelConfig_set_mapPathId_m3498091645 (JSCLevelConfig_t1411099500 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<JSCWaveNpcConfig> JSCLevelConfig::get_monsters()
extern "C"  List_1_t1294722912 * JSCLevelConfig_get_monsters_m4241265370 (JSCLevelConfig_t1411099500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<JSCWaveNpcConfig> JSCLevelConfig::get_others()
extern "C"  List_1_t1294722912 * JSCLevelConfig_get_others_m4015429604 (JSCLevelConfig_t1411099500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<JSCWaveNpcConfig> JSCLevelConfig::get_guide()
extern "C"  List_1_t1294722912 * JSCLevelConfig_get_guide_m679111869 (JSCLevelConfig_t1411099500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSCLevelHeroConfig JSCLevelConfig::get_hero()
extern "C"  JSCLevelHeroConfig_t1953226502 * JSCLevelConfig_get_hero_m154782351 (JSCLevelConfig_t1411099500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCLevelConfig::set_hero(JSCLevelHeroConfig)
extern "C"  void JSCLevelConfig_set_hero_m2047934854 (JSCLevelConfig_t1411099500 * __this, JSCLevelHeroConfig_t1953226502 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// JSCLevelHeroConfig JSCLevelConfig::get_guideHero()
extern "C"  JSCLevelHeroConfig_t1953226502 * JSCLevelConfig_get_guideHero_m1367218435 (JSCLevelConfig_t1411099500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSCLevelConfig::set_guideHero(JSCLevelHeroConfig)
extern "C"  void JSCLevelConfig_set_guideHero_m2453259168 (JSCLevelConfig_t1411099500 * __this, JSCLevelHeroConfig_t1953226502 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<JSCWaveNpcConfig> JSCLevelConfig::get_plot()
extern "C"  List_1_t1294722912 * JSCLevelConfig_get_plot_m3319491042 (JSCLevelConfig_t1411099500 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
