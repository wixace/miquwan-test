﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_suhemChowkawjal157
struct  M_suhemChowkawjal157_t3742158740  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_suhemChowkawjal157::_sellaluGerepee
	float ____sellaluGerepee_0;
	// System.String GarbageiOS.M_suhemChowkawjal157::_nasjor
	String_t* ____nasjor_1;
	// System.Boolean GarbageiOS.M_suhemChowkawjal157::_geafo
	bool ____geafo_2;
	// System.Single GarbageiOS.M_suhemChowkawjal157::_jenall
	float ____jenall_3;
	// System.Single GarbageiOS.M_suhemChowkawjal157::_secousea
	float ____secousea_4;
	// System.Int32 GarbageiOS.M_suhemChowkawjal157::_yeretawBemcisa
	int32_t ____yeretawBemcisa_5;
	// System.Int32 GarbageiOS.M_suhemChowkawjal157::_xelnoucere
	int32_t ____xelnoucere_6;
	// System.Boolean GarbageiOS.M_suhemChowkawjal157::_lepouliBelfafou
	bool ____lepouliBelfafou_7;
	// System.String GarbageiOS.M_suhemChowkawjal157::_chairdrem
	String_t* ____chairdrem_8;
	// System.Int32 GarbageiOS.M_suhemChowkawjal157::_sallhirGounernea
	int32_t ____sallhirGounernea_9;
	// System.String GarbageiOS.M_suhemChowkawjal157::_harkefo
	String_t* ____harkefo_10;
	// System.String GarbageiOS.M_suhemChowkawjal157::_selbe
	String_t* ____selbe_11;
	// System.Int32 GarbageiOS.M_suhemChowkawjal157::_karster
	int32_t ____karster_12;
	// System.String GarbageiOS.M_suhemChowkawjal157::_hirmallbor
	String_t* ____hirmallbor_13;
	// System.Int32 GarbageiOS.M_suhemChowkawjal157::_whijoKibalmaw
	int32_t ____whijoKibalmaw_14;
	// System.Boolean GarbageiOS.M_suhemChowkawjal157::_lerelarboo
	bool ____lerelarboo_15;
	// System.Boolean GarbageiOS.M_suhemChowkawjal157::_traifo
	bool ____traifo_16;
	// System.Single GarbageiOS.M_suhemChowkawjal157::_cehiheaKaldremna
	float ____cehiheaKaldremna_17;
	// System.String GarbageiOS.M_suhemChowkawjal157::_roucasSaimatraw
	String_t* ____roucasSaimatraw_18;
	// System.Single GarbageiOS.M_suhemChowkawjal157::_pallbeeciMowfurmall
	float ____pallbeeciMowfurmall_19;
	// System.UInt32 GarbageiOS.M_suhemChowkawjal157::_sawmallco
	uint32_t ____sawmallco_20;
	// System.UInt32 GarbageiOS.M_suhemChowkawjal157::_jisatouBuchousem
	uint32_t ____jisatouBuchousem_21;
	// System.Int32 GarbageiOS.M_suhemChowkawjal157::_kecouPereda
	int32_t ____kecouPereda_22;
	// System.String GarbageiOS.M_suhemChowkawjal157::_whoutepemPacufis
	String_t* ____whoutepemPacufis_23;
	// System.Boolean GarbageiOS.M_suhemChowkawjal157::_yulair
	bool ____yulair_24;
	// System.String GarbageiOS.M_suhemChowkawjal157::_rebesemFaydrur
	String_t* ____rebesemFaydrur_25;
	// System.String GarbageiOS.M_suhemChowkawjal157::_learroSemkaker
	String_t* ____learroSemkaker_26;
	// System.UInt32 GarbageiOS.M_suhemChowkawjal157::_korjofe
	uint32_t ____korjofe_27;
	// System.UInt32 GarbageiOS.M_suhemChowkawjal157::_memdeaseSure
	uint32_t ____memdeaseSure_28;
	// System.UInt32 GarbageiOS.M_suhemChowkawjal157::_doudeSemdra
	uint32_t ____doudeSemdra_29;
	// System.Int32 GarbageiOS.M_suhemChowkawjal157::_satasteSinirdre
	int32_t ____satasteSinirdre_30;
	// System.Boolean GarbageiOS.M_suhemChowkawjal157::_runoo
	bool ____runoo_31;
	// System.Boolean GarbageiOS.M_suhemChowkawjal157::_kamaHalberehor
	bool ____kamaHalberehor_32;
	// System.String GarbageiOS.M_suhemChowkawjal157::_torqur
	String_t* ____torqur_33;
	// System.Boolean GarbageiOS.M_suhemChowkawjal157::_hitrorsouSarsownere
	bool ____hitrorsouSarsownere_34;
	// System.String GarbageiOS.M_suhemChowkawjal157::_sabemi
	String_t* ____sabemi_35;
	// System.Int32 GarbageiOS.M_suhemChowkawjal157::_jukee
	int32_t ____jukee_36;
	// System.UInt32 GarbageiOS.M_suhemChowkawjal157::_pace
	uint32_t ____pace_37;
	// System.Single GarbageiOS.M_suhemChowkawjal157::_jalseDeper
	float ____jalseDeper_38;
	// System.Int32 GarbageiOS.M_suhemChowkawjal157::_kalernou
	int32_t ____kalernou_39;
	// System.Boolean GarbageiOS.M_suhemChowkawjal157::_sezallgall
	bool ____sezallgall_40;
	// System.UInt32 GarbageiOS.M_suhemChowkawjal157::_pelcoDajayraw
	uint32_t ____pelcoDajayraw_41;
	// System.Single GarbageiOS.M_suhemChowkawjal157::_wepi
	float ____wepi_42;
	// System.Single GarbageiOS.M_suhemChowkawjal157::_maigopeTorji
	float ____maigopeTorji_43;
	// System.String GarbageiOS.M_suhemChowkawjal157::_faturdu
	String_t* ____faturdu_44;
	// System.Single GarbageiOS.M_suhemChowkawjal157::_dirku
	float ____dirku_45;
	// System.Int32 GarbageiOS.M_suhemChowkawjal157::_sorpay
	int32_t ____sorpay_46;
	// System.Single GarbageiOS.M_suhemChowkawjal157::_jalgoo
	float ____jalgoo_47;
	// System.Int32 GarbageiOS.M_suhemChowkawjal157::_rereher
	int32_t ____rereher_48;
	// System.Int32 GarbageiOS.M_suhemChowkawjal157::_drebayCeejorjair
	int32_t ____drebayCeejorjair_49;
	// System.Single GarbageiOS.M_suhemChowkawjal157::_sajerrisRocorsair
	float ____sajerrisRocorsair_50;
	// System.Single GarbageiOS.M_suhemChowkawjal157::_powturdroRoorarwho
	float ____powturdroRoorarwho_51;
	// System.UInt32 GarbageiOS.M_suhemChowkawjal157::_jerdrow
	uint32_t ____jerdrow_52;
	// System.Single GarbageiOS.M_suhemChowkawjal157::_xelstemPisstevou
	float ____xelstemPisstevou_53;

public:
	inline static int32_t get_offset_of__sellaluGerepee_0() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____sellaluGerepee_0)); }
	inline float get__sellaluGerepee_0() const { return ____sellaluGerepee_0; }
	inline float* get_address_of__sellaluGerepee_0() { return &____sellaluGerepee_0; }
	inline void set__sellaluGerepee_0(float value)
	{
		____sellaluGerepee_0 = value;
	}

	inline static int32_t get_offset_of__nasjor_1() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____nasjor_1)); }
	inline String_t* get__nasjor_1() const { return ____nasjor_1; }
	inline String_t** get_address_of__nasjor_1() { return &____nasjor_1; }
	inline void set__nasjor_1(String_t* value)
	{
		____nasjor_1 = value;
		Il2CppCodeGenWriteBarrier(&____nasjor_1, value);
	}

	inline static int32_t get_offset_of__geafo_2() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____geafo_2)); }
	inline bool get__geafo_2() const { return ____geafo_2; }
	inline bool* get_address_of__geafo_2() { return &____geafo_2; }
	inline void set__geafo_2(bool value)
	{
		____geafo_2 = value;
	}

	inline static int32_t get_offset_of__jenall_3() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____jenall_3)); }
	inline float get__jenall_3() const { return ____jenall_3; }
	inline float* get_address_of__jenall_3() { return &____jenall_3; }
	inline void set__jenall_3(float value)
	{
		____jenall_3 = value;
	}

	inline static int32_t get_offset_of__secousea_4() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____secousea_4)); }
	inline float get__secousea_4() const { return ____secousea_4; }
	inline float* get_address_of__secousea_4() { return &____secousea_4; }
	inline void set__secousea_4(float value)
	{
		____secousea_4 = value;
	}

	inline static int32_t get_offset_of__yeretawBemcisa_5() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____yeretawBemcisa_5)); }
	inline int32_t get__yeretawBemcisa_5() const { return ____yeretawBemcisa_5; }
	inline int32_t* get_address_of__yeretawBemcisa_5() { return &____yeretawBemcisa_5; }
	inline void set__yeretawBemcisa_5(int32_t value)
	{
		____yeretawBemcisa_5 = value;
	}

	inline static int32_t get_offset_of__xelnoucere_6() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____xelnoucere_6)); }
	inline int32_t get__xelnoucere_6() const { return ____xelnoucere_6; }
	inline int32_t* get_address_of__xelnoucere_6() { return &____xelnoucere_6; }
	inline void set__xelnoucere_6(int32_t value)
	{
		____xelnoucere_6 = value;
	}

	inline static int32_t get_offset_of__lepouliBelfafou_7() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____lepouliBelfafou_7)); }
	inline bool get__lepouliBelfafou_7() const { return ____lepouliBelfafou_7; }
	inline bool* get_address_of__lepouliBelfafou_7() { return &____lepouliBelfafou_7; }
	inline void set__lepouliBelfafou_7(bool value)
	{
		____lepouliBelfafou_7 = value;
	}

	inline static int32_t get_offset_of__chairdrem_8() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____chairdrem_8)); }
	inline String_t* get__chairdrem_8() const { return ____chairdrem_8; }
	inline String_t** get_address_of__chairdrem_8() { return &____chairdrem_8; }
	inline void set__chairdrem_8(String_t* value)
	{
		____chairdrem_8 = value;
		Il2CppCodeGenWriteBarrier(&____chairdrem_8, value);
	}

	inline static int32_t get_offset_of__sallhirGounernea_9() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____sallhirGounernea_9)); }
	inline int32_t get__sallhirGounernea_9() const { return ____sallhirGounernea_9; }
	inline int32_t* get_address_of__sallhirGounernea_9() { return &____sallhirGounernea_9; }
	inline void set__sallhirGounernea_9(int32_t value)
	{
		____sallhirGounernea_9 = value;
	}

	inline static int32_t get_offset_of__harkefo_10() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____harkefo_10)); }
	inline String_t* get__harkefo_10() const { return ____harkefo_10; }
	inline String_t** get_address_of__harkefo_10() { return &____harkefo_10; }
	inline void set__harkefo_10(String_t* value)
	{
		____harkefo_10 = value;
		Il2CppCodeGenWriteBarrier(&____harkefo_10, value);
	}

	inline static int32_t get_offset_of__selbe_11() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____selbe_11)); }
	inline String_t* get__selbe_11() const { return ____selbe_11; }
	inline String_t** get_address_of__selbe_11() { return &____selbe_11; }
	inline void set__selbe_11(String_t* value)
	{
		____selbe_11 = value;
		Il2CppCodeGenWriteBarrier(&____selbe_11, value);
	}

	inline static int32_t get_offset_of__karster_12() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____karster_12)); }
	inline int32_t get__karster_12() const { return ____karster_12; }
	inline int32_t* get_address_of__karster_12() { return &____karster_12; }
	inline void set__karster_12(int32_t value)
	{
		____karster_12 = value;
	}

	inline static int32_t get_offset_of__hirmallbor_13() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____hirmallbor_13)); }
	inline String_t* get__hirmallbor_13() const { return ____hirmallbor_13; }
	inline String_t** get_address_of__hirmallbor_13() { return &____hirmallbor_13; }
	inline void set__hirmallbor_13(String_t* value)
	{
		____hirmallbor_13 = value;
		Il2CppCodeGenWriteBarrier(&____hirmallbor_13, value);
	}

	inline static int32_t get_offset_of__whijoKibalmaw_14() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____whijoKibalmaw_14)); }
	inline int32_t get__whijoKibalmaw_14() const { return ____whijoKibalmaw_14; }
	inline int32_t* get_address_of__whijoKibalmaw_14() { return &____whijoKibalmaw_14; }
	inline void set__whijoKibalmaw_14(int32_t value)
	{
		____whijoKibalmaw_14 = value;
	}

	inline static int32_t get_offset_of__lerelarboo_15() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____lerelarboo_15)); }
	inline bool get__lerelarboo_15() const { return ____lerelarboo_15; }
	inline bool* get_address_of__lerelarboo_15() { return &____lerelarboo_15; }
	inline void set__lerelarboo_15(bool value)
	{
		____lerelarboo_15 = value;
	}

	inline static int32_t get_offset_of__traifo_16() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____traifo_16)); }
	inline bool get__traifo_16() const { return ____traifo_16; }
	inline bool* get_address_of__traifo_16() { return &____traifo_16; }
	inline void set__traifo_16(bool value)
	{
		____traifo_16 = value;
	}

	inline static int32_t get_offset_of__cehiheaKaldremna_17() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____cehiheaKaldremna_17)); }
	inline float get__cehiheaKaldremna_17() const { return ____cehiheaKaldremna_17; }
	inline float* get_address_of__cehiheaKaldremna_17() { return &____cehiheaKaldremna_17; }
	inline void set__cehiheaKaldremna_17(float value)
	{
		____cehiheaKaldremna_17 = value;
	}

	inline static int32_t get_offset_of__roucasSaimatraw_18() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____roucasSaimatraw_18)); }
	inline String_t* get__roucasSaimatraw_18() const { return ____roucasSaimatraw_18; }
	inline String_t** get_address_of__roucasSaimatraw_18() { return &____roucasSaimatraw_18; }
	inline void set__roucasSaimatraw_18(String_t* value)
	{
		____roucasSaimatraw_18 = value;
		Il2CppCodeGenWriteBarrier(&____roucasSaimatraw_18, value);
	}

	inline static int32_t get_offset_of__pallbeeciMowfurmall_19() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____pallbeeciMowfurmall_19)); }
	inline float get__pallbeeciMowfurmall_19() const { return ____pallbeeciMowfurmall_19; }
	inline float* get_address_of__pallbeeciMowfurmall_19() { return &____pallbeeciMowfurmall_19; }
	inline void set__pallbeeciMowfurmall_19(float value)
	{
		____pallbeeciMowfurmall_19 = value;
	}

	inline static int32_t get_offset_of__sawmallco_20() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____sawmallco_20)); }
	inline uint32_t get__sawmallco_20() const { return ____sawmallco_20; }
	inline uint32_t* get_address_of__sawmallco_20() { return &____sawmallco_20; }
	inline void set__sawmallco_20(uint32_t value)
	{
		____sawmallco_20 = value;
	}

	inline static int32_t get_offset_of__jisatouBuchousem_21() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____jisatouBuchousem_21)); }
	inline uint32_t get__jisatouBuchousem_21() const { return ____jisatouBuchousem_21; }
	inline uint32_t* get_address_of__jisatouBuchousem_21() { return &____jisatouBuchousem_21; }
	inline void set__jisatouBuchousem_21(uint32_t value)
	{
		____jisatouBuchousem_21 = value;
	}

	inline static int32_t get_offset_of__kecouPereda_22() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____kecouPereda_22)); }
	inline int32_t get__kecouPereda_22() const { return ____kecouPereda_22; }
	inline int32_t* get_address_of__kecouPereda_22() { return &____kecouPereda_22; }
	inline void set__kecouPereda_22(int32_t value)
	{
		____kecouPereda_22 = value;
	}

	inline static int32_t get_offset_of__whoutepemPacufis_23() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____whoutepemPacufis_23)); }
	inline String_t* get__whoutepemPacufis_23() const { return ____whoutepemPacufis_23; }
	inline String_t** get_address_of__whoutepemPacufis_23() { return &____whoutepemPacufis_23; }
	inline void set__whoutepemPacufis_23(String_t* value)
	{
		____whoutepemPacufis_23 = value;
		Il2CppCodeGenWriteBarrier(&____whoutepemPacufis_23, value);
	}

	inline static int32_t get_offset_of__yulair_24() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____yulair_24)); }
	inline bool get__yulair_24() const { return ____yulair_24; }
	inline bool* get_address_of__yulair_24() { return &____yulair_24; }
	inline void set__yulair_24(bool value)
	{
		____yulair_24 = value;
	}

	inline static int32_t get_offset_of__rebesemFaydrur_25() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____rebesemFaydrur_25)); }
	inline String_t* get__rebesemFaydrur_25() const { return ____rebesemFaydrur_25; }
	inline String_t** get_address_of__rebesemFaydrur_25() { return &____rebesemFaydrur_25; }
	inline void set__rebesemFaydrur_25(String_t* value)
	{
		____rebesemFaydrur_25 = value;
		Il2CppCodeGenWriteBarrier(&____rebesemFaydrur_25, value);
	}

	inline static int32_t get_offset_of__learroSemkaker_26() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____learroSemkaker_26)); }
	inline String_t* get__learroSemkaker_26() const { return ____learroSemkaker_26; }
	inline String_t** get_address_of__learroSemkaker_26() { return &____learroSemkaker_26; }
	inline void set__learroSemkaker_26(String_t* value)
	{
		____learroSemkaker_26 = value;
		Il2CppCodeGenWriteBarrier(&____learroSemkaker_26, value);
	}

	inline static int32_t get_offset_of__korjofe_27() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____korjofe_27)); }
	inline uint32_t get__korjofe_27() const { return ____korjofe_27; }
	inline uint32_t* get_address_of__korjofe_27() { return &____korjofe_27; }
	inline void set__korjofe_27(uint32_t value)
	{
		____korjofe_27 = value;
	}

	inline static int32_t get_offset_of__memdeaseSure_28() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____memdeaseSure_28)); }
	inline uint32_t get__memdeaseSure_28() const { return ____memdeaseSure_28; }
	inline uint32_t* get_address_of__memdeaseSure_28() { return &____memdeaseSure_28; }
	inline void set__memdeaseSure_28(uint32_t value)
	{
		____memdeaseSure_28 = value;
	}

	inline static int32_t get_offset_of__doudeSemdra_29() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____doudeSemdra_29)); }
	inline uint32_t get__doudeSemdra_29() const { return ____doudeSemdra_29; }
	inline uint32_t* get_address_of__doudeSemdra_29() { return &____doudeSemdra_29; }
	inline void set__doudeSemdra_29(uint32_t value)
	{
		____doudeSemdra_29 = value;
	}

	inline static int32_t get_offset_of__satasteSinirdre_30() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____satasteSinirdre_30)); }
	inline int32_t get__satasteSinirdre_30() const { return ____satasteSinirdre_30; }
	inline int32_t* get_address_of__satasteSinirdre_30() { return &____satasteSinirdre_30; }
	inline void set__satasteSinirdre_30(int32_t value)
	{
		____satasteSinirdre_30 = value;
	}

	inline static int32_t get_offset_of__runoo_31() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____runoo_31)); }
	inline bool get__runoo_31() const { return ____runoo_31; }
	inline bool* get_address_of__runoo_31() { return &____runoo_31; }
	inline void set__runoo_31(bool value)
	{
		____runoo_31 = value;
	}

	inline static int32_t get_offset_of__kamaHalberehor_32() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____kamaHalberehor_32)); }
	inline bool get__kamaHalberehor_32() const { return ____kamaHalberehor_32; }
	inline bool* get_address_of__kamaHalberehor_32() { return &____kamaHalberehor_32; }
	inline void set__kamaHalberehor_32(bool value)
	{
		____kamaHalberehor_32 = value;
	}

	inline static int32_t get_offset_of__torqur_33() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____torqur_33)); }
	inline String_t* get__torqur_33() const { return ____torqur_33; }
	inline String_t** get_address_of__torqur_33() { return &____torqur_33; }
	inline void set__torqur_33(String_t* value)
	{
		____torqur_33 = value;
		Il2CppCodeGenWriteBarrier(&____torqur_33, value);
	}

	inline static int32_t get_offset_of__hitrorsouSarsownere_34() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____hitrorsouSarsownere_34)); }
	inline bool get__hitrorsouSarsownere_34() const { return ____hitrorsouSarsownere_34; }
	inline bool* get_address_of__hitrorsouSarsownere_34() { return &____hitrorsouSarsownere_34; }
	inline void set__hitrorsouSarsownere_34(bool value)
	{
		____hitrorsouSarsownere_34 = value;
	}

	inline static int32_t get_offset_of__sabemi_35() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____sabemi_35)); }
	inline String_t* get__sabemi_35() const { return ____sabemi_35; }
	inline String_t** get_address_of__sabemi_35() { return &____sabemi_35; }
	inline void set__sabemi_35(String_t* value)
	{
		____sabemi_35 = value;
		Il2CppCodeGenWriteBarrier(&____sabemi_35, value);
	}

	inline static int32_t get_offset_of__jukee_36() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____jukee_36)); }
	inline int32_t get__jukee_36() const { return ____jukee_36; }
	inline int32_t* get_address_of__jukee_36() { return &____jukee_36; }
	inline void set__jukee_36(int32_t value)
	{
		____jukee_36 = value;
	}

	inline static int32_t get_offset_of__pace_37() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____pace_37)); }
	inline uint32_t get__pace_37() const { return ____pace_37; }
	inline uint32_t* get_address_of__pace_37() { return &____pace_37; }
	inline void set__pace_37(uint32_t value)
	{
		____pace_37 = value;
	}

	inline static int32_t get_offset_of__jalseDeper_38() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____jalseDeper_38)); }
	inline float get__jalseDeper_38() const { return ____jalseDeper_38; }
	inline float* get_address_of__jalseDeper_38() { return &____jalseDeper_38; }
	inline void set__jalseDeper_38(float value)
	{
		____jalseDeper_38 = value;
	}

	inline static int32_t get_offset_of__kalernou_39() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____kalernou_39)); }
	inline int32_t get__kalernou_39() const { return ____kalernou_39; }
	inline int32_t* get_address_of__kalernou_39() { return &____kalernou_39; }
	inline void set__kalernou_39(int32_t value)
	{
		____kalernou_39 = value;
	}

	inline static int32_t get_offset_of__sezallgall_40() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____sezallgall_40)); }
	inline bool get__sezallgall_40() const { return ____sezallgall_40; }
	inline bool* get_address_of__sezallgall_40() { return &____sezallgall_40; }
	inline void set__sezallgall_40(bool value)
	{
		____sezallgall_40 = value;
	}

	inline static int32_t get_offset_of__pelcoDajayraw_41() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____pelcoDajayraw_41)); }
	inline uint32_t get__pelcoDajayraw_41() const { return ____pelcoDajayraw_41; }
	inline uint32_t* get_address_of__pelcoDajayraw_41() { return &____pelcoDajayraw_41; }
	inline void set__pelcoDajayraw_41(uint32_t value)
	{
		____pelcoDajayraw_41 = value;
	}

	inline static int32_t get_offset_of__wepi_42() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____wepi_42)); }
	inline float get__wepi_42() const { return ____wepi_42; }
	inline float* get_address_of__wepi_42() { return &____wepi_42; }
	inline void set__wepi_42(float value)
	{
		____wepi_42 = value;
	}

	inline static int32_t get_offset_of__maigopeTorji_43() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____maigopeTorji_43)); }
	inline float get__maigopeTorji_43() const { return ____maigopeTorji_43; }
	inline float* get_address_of__maigopeTorji_43() { return &____maigopeTorji_43; }
	inline void set__maigopeTorji_43(float value)
	{
		____maigopeTorji_43 = value;
	}

	inline static int32_t get_offset_of__faturdu_44() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____faturdu_44)); }
	inline String_t* get__faturdu_44() const { return ____faturdu_44; }
	inline String_t** get_address_of__faturdu_44() { return &____faturdu_44; }
	inline void set__faturdu_44(String_t* value)
	{
		____faturdu_44 = value;
		Il2CppCodeGenWriteBarrier(&____faturdu_44, value);
	}

	inline static int32_t get_offset_of__dirku_45() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____dirku_45)); }
	inline float get__dirku_45() const { return ____dirku_45; }
	inline float* get_address_of__dirku_45() { return &____dirku_45; }
	inline void set__dirku_45(float value)
	{
		____dirku_45 = value;
	}

	inline static int32_t get_offset_of__sorpay_46() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____sorpay_46)); }
	inline int32_t get__sorpay_46() const { return ____sorpay_46; }
	inline int32_t* get_address_of__sorpay_46() { return &____sorpay_46; }
	inline void set__sorpay_46(int32_t value)
	{
		____sorpay_46 = value;
	}

	inline static int32_t get_offset_of__jalgoo_47() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____jalgoo_47)); }
	inline float get__jalgoo_47() const { return ____jalgoo_47; }
	inline float* get_address_of__jalgoo_47() { return &____jalgoo_47; }
	inline void set__jalgoo_47(float value)
	{
		____jalgoo_47 = value;
	}

	inline static int32_t get_offset_of__rereher_48() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____rereher_48)); }
	inline int32_t get__rereher_48() const { return ____rereher_48; }
	inline int32_t* get_address_of__rereher_48() { return &____rereher_48; }
	inline void set__rereher_48(int32_t value)
	{
		____rereher_48 = value;
	}

	inline static int32_t get_offset_of__drebayCeejorjair_49() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____drebayCeejorjair_49)); }
	inline int32_t get__drebayCeejorjair_49() const { return ____drebayCeejorjair_49; }
	inline int32_t* get_address_of__drebayCeejorjair_49() { return &____drebayCeejorjair_49; }
	inline void set__drebayCeejorjair_49(int32_t value)
	{
		____drebayCeejorjair_49 = value;
	}

	inline static int32_t get_offset_of__sajerrisRocorsair_50() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____sajerrisRocorsair_50)); }
	inline float get__sajerrisRocorsair_50() const { return ____sajerrisRocorsair_50; }
	inline float* get_address_of__sajerrisRocorsair_50() { return &____sajerrisRocorsair_50; }
	inline void set__sajerrisRocorsair_50(float value)
	{
		____sajerrisRocorsair_50 = value;
	}

	inline static int32_t get_offset_of__powturdroRoorarwho_51() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____powturdroRoorarwho_51)); }
	inline float get__powturdroRoorarwho_51() const { return ____powturdroRoorarwho_51; }
	inline float* get_address_of__powturdroRoorarwho_51() { return &____powturdroRoorarwho_51; }
	inline void set__powturdroRoorarwho_51(float value)
	{
		____powturdroRoorarwho_51 = value;
	}

	inline static int32_t get_offset_of__jerdrow_52() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____jerdrow_52)); }
	inline uint32_t get__jerdrow_52() const { return ____jerdrow_52; }
	inline uint32_t* get_address_of__jerdrow_52() { return &____jerdrow_52; }
	inline void set__jerdrow_52(uint32_t value)
	{
		____jerdrow_52 = value;
	}

	inline static int32_t get_offset_of__xelstemPisstevou_53() { return static_cast<int32_t>(offsetof(M_suhemChowkawjal157_t3742158740, ____xelstemPisstevou_53)); }
	inline float get__xelstemPisstevou_53() const { return ____xelstemPisstevou_53; }
	inline float* get_address_of__xelstemPisstevou_53() { return &____xelstemPisstevou_53; }
	inline void set__xelstemPisstevou_53(float value)
	{
		____xelstemPisstevou_53 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
