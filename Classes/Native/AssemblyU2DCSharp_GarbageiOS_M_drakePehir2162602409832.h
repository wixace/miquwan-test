﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_drakePehir216
struct  M_drakePehir216_t2602409832  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_drakePehir216::_mascaCabur
	String_t* ____mascaCabur_0;
	// System.Int32 GarbageiOS.M_drakePehir216::_rarmordea
	int32_t ____rarmordea_1;
	// System.Boolean GarbageiOS.M_drakePehir216::_balayse
	bool ____balayse_2;
	// System.UInt32 GarbageiOS.M_drakePehir216::_diderje
	uint32_t ____diderje_3;
	// System.Int32 GarbageiOS.M_drakePehir216::_yougair
	int32_t ____yougair_4;
	// System.String GarbageiOS.M_drakePehir216::_kooqallral
	String_t* ____kooqallral_5;
	// System.String GarbageiOS.M_drakePehir216::_leseasiVoohu
	String_t* ____leseasiVoohu_6;
	// System.UInt32 GarbageiOS.M_drakePehir216::_whikee
	uint32_t ____whikee_7;
	// System.Int32 GarbageiOS.M_drakePehir216::_naidra
	int32_t ____naidra_8;
	// System.Single GarbageiOS.M_drakePehir216::_gereserekall
	float ____gereserekall_9;
	// System.String GarbageiOS.M_drakePehir216::_mimascasTiza
	String_t* ____mimascasTiza_10;
	// System.Single GarbageiOS.M_drakePehir216::_samibalSira
	float ____samibalSira_11;
	// System.Single GarbageiOS.M_drakePehir216::_poopimur
	float ____poopimur_12;
	// System.String GarbageiOS.M_drakePehir216::_kerawBemeeso
	String_t* ____kerawBemeeso_13;
	// System.UInt32 GarbageiOS.M_drakePehir216::_coumayFoowhoutor
	uint32_t ____coumayFoowhoutor_14;
	// System.Boolean GarbageiOS.M_drakePehir216::_vemxouki
	bool ____vemxouki_15;
	// System.Boolean GarbageiOS.M_drakePehir216::_dallherasGerenoulas
	bool ____dallherasGerenoulas_16;
	// System.String GarbageiOS.M_drakePehir216::_stepo
	String_t* ____stepo_17;
	// System.UInt32 GarbageiOS.M_drakePehir216::_tanaiheVeyiqa
	uint32_t ____tanaiheVeyiqa_18;
	// System.UInt32 GarbageiOS.M_drakePehir216::_coomuho
	uint32_t ____coomuho_19;
	// System.Boolean GarbageiOS.M_drakePehir216::_stalcee
	bool ____stalcee_20;
	// System.String GarbageiOS.M_drakePehir216::_tairsteli
	String_t* ____tairsteli_21;
	// System.String GarbageiOS.M_drakePehir216::_curwhereDersi
	String_t* ____curwhereDersi_22;
	// System.Int32 GarbageiOS.M_drakePehir216::_fearcaitair
	int32_t ____fearcaitair_23;
	// System.UInt32 GarbageiOS.M_drakePehir216::_nemwawe
	uint32_t ____nemwawe_24;

public:
	inline static int32_t get_offset_of__mascaCabur_0() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____mascaCabur_0)); }
	inline String_t* get__mascaCabur_0() const { return ____mascaCabur_0; }
	inline String_t** get_address_of__mascaCabur_0() { return &____mascaCabur_0; }
	inline void set__mascaCabur_0(String_t* value)
	{
		____mascaCabur_0 = value;
		Il2CppCodeGenWriteBarrier(&____mascaCabur_0, value);
	}

	inline static int32_t get_offset_of__rarmordea_1() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____rarmordea_1)); }
	inline int32_t get__rarmordea_1() const { return ____rarmordea_1; }
	inline int32_t* get_address_of__rarmordea_1() { return &____rarmordea_1; }
	inline void set__rarmordea_1(int32_t value)
	{
		____rarmordea_1 = value;
	}

	inline static int32_t get_offset_of__balayse_2() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____balayse_2)); }
	inline bool get__balayse_2() const { return ____balayse_2; }
	inline bool* get_address_of__balayse_2() { return &____balayse_2; }
	inline void set__balayse_2(bool value)
	{
		____balayse_2 = value;
	}

	inline static int32_t get_offset_of__diderje_3() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____diderje_3)); }
	inline uint32_t get__diderje_3() const { return ____diderje_3; }
	inline uint32_t* get_address_of__diderje_3() { return &____diderje_3; }
	inline void set__diderje_3(uint32_t value)
	{
		____diderje_3 = value;
	}

	inline static int32_t get_offset_of__yougair_4() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____yougair_4)); }
	inline int32_t get__yougair_4() const { return ____yougair_4; }
	inline int32_t* get_address_of__yougair_4() { return &____yougair_4; }
	inline void set__yougair_4(int32_t value)
	{
		____yougair_4 = value;
	}

	inline static int32_t get_offset_of__kooqallral_5() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____kooqallral_5)); }
	inline String_t* get__kooqallral_5() const { return ____kooqallral_5; }
	inline String_t** get_address_of__kooqallral_5() { return &____kooqallral_5; }
	inline void set__kooqallral_5(String_t* value)
	{
		____kooqallral_5 = value;
		Il2CppCodeGenWriteBarrier(&____kooqallral_5, value);
	}

	inline static int32_t get_offset_of__leseasiVoohu_6() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____leseasiVoohu_6)); }
	inline String_t* get__leseasiVoohu_6() const { return ____leseasiVoohu_6; }
	inline String_t** get_address_of__leseasiVoohu_6() { return &____leseasiVoohu_6; }
	inline void set__leseasiVoohu_6(String_t* value)
	{
		____leseasiVoohu_6 = value;
		Il2CppCodeGenWriteBarrier(&____leseasiVoohu_6, value);
	}

	inline static int32_t get_offset_of__whikee_7() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____whikee_7)); }
	inline uint32_t get__whikee_7() const { return ____whikee_7; }
	inline uint32_t* get_address_of__whikee_7() { return &____whikee_7; }
	inline void set__whikee_7(uint32_t value)
	{
		____whikee_7 = value;
	}

	inline static int32_t get_offset_of__naidra_8() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____naidra_8)); }
	inline int32_t get__naidra_8() const { return ____naidra_8; }
	inline int32_t* get_address_of__naidra_8() { return &____naidra_8; }
	inline void set__naidra_8(int32_t value)
	{
		____naidra_8 = value;
	}

	inline static int32_t get_offset_of__gereserekall_9() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____gereserekall_9)); }
	inline float get__gereserekall_9() const { return ____gereserekall_9; }
	inline float* get_address_of__gereserekall_9() { return &____gereserekall_9; }
	inline void set__gereserekall_9(float value)
	{
		____gereserekall_9 = value;
	}

	inline static int32_t get_offset_of__mimascasTiza_10() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____mimascasTiza_10)); }
	inline String_t* get__mimascasTiza_10() const { return ____mimascasTiza_10; }
	inline String_t** get_address_of__mimascasTiza_10() { return &____mimascasTiza_10; }
	inline void set__mimascasTiza_10(String_t* value)
	{
		____mimascasTiza_10 = value;
		Il2CppCodeGenWriteBarrier(&____mimascasTiza_10, value);
	}

	inline static int32_t get_offset_of__samibalSira_11() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____samibalSira_11)); }
	inline float get__samibalSira_11() const { return ____samibalSira_11; }
	inline float* get_address_of__samibalSira_11() { return &____samibalSira_11; }
	inline void set__samibalSira_11(float value)
	{
		____samibalSira_11 = value;
	}

	inline static int32_t get_offset_of__poopimur_12() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____poopimur_12)); }
	inline float get__poopimur_12() const { return ____poopimur_12; }
	inline float* get_address_of__poopimur_12() { return &____poopimur_12; }
	inline void set__poopimur_12(float value)
	{
		____poopimur_12 = value;
	}

	inline static int32_t get_offset_of__kerawBemeeso_13() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____kerawBemeeso_13)); }
	inline String_t* get__kerawBemeeso_13() const { return ____kerawBemeeso_13; }
	inline String_t** get_address_of__kerawBemeeso_13() { return &____kerawBemeeso_13; }
	inline void set__kerawBemeeso_13(String_t* value)
	{
		____kerawBemeeso_13 = value;
		Il2CppCodeGenWriteBarrier(&____kerawBemeeso_13, value);
	}

	inline static int32_t get_offset_of__coumayFoowhoutor_14() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____coumayFoowhoutor_14)); }
	inline uint32_t get__coumayFoowhoutor_14() const { return ____coumayFoowhoutor_14; }
	inline uint32_t* get_address_of__coumayFoowhoutor_14() { return &____coumayFoowhoutor_14; }
	inline void set__coumayFoowhoutor_14(uint32_t value)
	{
		____coumayFoowhoutor_14 = value;
	}

	inline static int32_t get_offset_of__vemxouki_15() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____vemxouki_15)); }
	inline bool get__vemxouki_15() const { return ____vemxouki_15; }
	inline bool* get_address_of__vemxouki_15() { return &____vemxouki_15; }
	inline void set__vemxouki_15(bool value)
	{
		____vemxouki_15 = value;
	}

	inline static int32_t get_offset_of__dallherasGerenoulas_16() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____dallherasGerenoulas_16)); }
	inline bool get__dallherasGerenoulas_16() const { return ____dallherasGerenoulas_16; }
	inline bool* get_address_of__dallherasGerenoulas_16() { return &____dallherasGerenoulas_16; }
	inline void set__dallherasGerenoulas_16(bool value)
	{
		____dallherasGerenoulas_16 = value;
	}

	inline static int32_t get_offset_of__stepo_17() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____stepo_17)); }
	inline String_t* get__stepo_17() const { return ____stepo_17; }
	inline String_t** get_address_of__stepo_17() { return &____stepo_17; }
	inline void set__stepo_17(String_t* value)
	{
		____stepo_17 = value;
		Il2CppCodeGenWriteBarrier(&____stepo_17, value);
	}

	inline static int32_t get_offset_of__tanaiheVeyiqa_18() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____tanaiheVeyiqa_18)); }
	inline uint32_t get__tanaiheVeyiqa_18() const { return ____tanaiheVeyiqa_18; }
	inline uint32_t* get_address_of__tanaiheVeyiqa_18() { return &____tanaiheVeyiqa_18; }
	inline void set__tanaiheVeyiqa_18(uint32_t value)
	{
		____tanaiheVeyiqa_18 = value;
	}

	inline static int32_t get_offset_of__coomuho_19() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____coomuho_19)); }
	inline uint32_t get__coomuho_19() const { return ____coomuho_19; }
	inline uint32_t* get_address_of__coomuho_19() { return &____coomuho_19; }
	inline void set__coomuho_19(uint32_t value)
	{
		____coomuho_19 = value;
	}

	inline static int32_t get_offset_of__stalcee_20() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____stalcee_20)); }
	inline bool get__stalcee_20() const { return ____stalcee_20; }
	inline bool* get_address_of__stalcee_20() { return &____stalcee_20; }
	inline void set__stalcee_20(bool value)
	{
		____stalcee_20 = value;
	}

	inline static int32_t get_offset_of__tairsteli_21() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____tairsteli_21)); }
	inline String_t* get__tairsteli_21() const { return ____tairsteli_21; }
	inline String_t** get_address_of__tairsteli_21() { return &____tairsteli_21; }
	inline void set__tairsteli_21(String_t* value)
	{
		____tairsteli_21 = value;
		Il2CppCodeGenWriteBarrier(&____tairsteli_21, value);
	}

	inline static int32_t get_offset_of__curwhereDersi_22() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____curwhereDersi_22)); }
	inline String_t* get__curwhereDersi_22() const { return ____curwhereDersi_22; }
	inline String_t** get_address_of__curwhereDersi_22() { return &____curwhereDersi_22; }
	inline void set__curwhereDersi_22(String_t* value)
	{
		____curwhereDersi_22 = value;
		Il2CppCodeGenWriteBarrier(&____curwhereDersi_22, value);
	}

	inline static int32_t get_offset_of__fearcaitair_23() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____fearcaitair_23)); }
	inline int32_t get__fearcaitair_23() const { return ____fearcaitair_23; }
	inline int32_t* get_address_of__fearcaitair_23() { return &____fearcaitair_23; }
	inline void set__fearcaitair_23(int32_t value)
	{
		____fearcaitair_23 = value;
	}

	inline static int32_t get_offset_of__nemwawe_24() { return static_cast<int32_t>(offsetof(M_drakePehir216_t2602409832, ____nemwawe_24)); }
	inline uint32_t get__nemwawe_24() const { return ____nemwawe_24; }
	inline uint32_t* get_address_of__nemwawe_24() { return &____nemwawe_24; }
	inline void set__nemwawe_24(uint32_t value)
	{
		____nemwawe_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
