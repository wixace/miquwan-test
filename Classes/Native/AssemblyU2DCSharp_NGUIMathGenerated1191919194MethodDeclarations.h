﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NGUIMathGenerated
struct NGUIMathGenerated_t1191919194;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;
// UIWidget
struct UIWidget_t769069560;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// UnityEngine.Transform
struct Transform_t1659122786;
// System.String
struct String_t;
// UIRect
struct UIRect_t2503437976;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_UIRect2503437976.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"

// System.Void NGUIMathGenerated::.ctor()
extern "C"  void NGUIMathGenerated__ctor_m3093296209 (NGUIMathGenerated_t1191919194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_AdjustByDPI__Single(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_AdjustByDPI__Single_m2676630436 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_AdjustWidget__UIWidget__Single__Single__Single__Single__Int32__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_AdjustWidget__UIWidget__Single__Single__Single__Single__Int32__Int32__Int32__Int32_m3402529000 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_AdjustWidget__UIWidget__Single__Single__Single__Single__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_AdjustWidget__UIWidget__Single__Single__Single__Single__Int32__Int32_m2856323304 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_AdjustWidget__UIWidget__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_AdjustWidget__UIWidget__Single__Single__Single__Single_m1621989608 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_CalculateAbsoluteWidgetBounds__Transform(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_CalculateAbsoluteWidgetBounds__Transform_m3388361139 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_CalculateRelativeWidgetBounds__Transform__Transform__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_CalculateRelativeWidgetBounds__Transform__Transform__Boolean__Boolean_m3887255344 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_CalculateRelativeWidgetBounds__Transform__Boolean(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_CalculateRelativeWidgetBounds__Transform__Boolean_m2753808492 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_CalculateRelativeWidgetBounds__Transform__Transform(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_CalculateRelativeWidgetBounds__Transform__Transform_m3442129904 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_CalculateRelativeWidgetBounds__Transform(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_CalculateRelativeWidgetBounds__Transform_m3591537278 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_ClampIndex__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_ClampIndex__Int32__Int32_m1268128724 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_ColorToInt__Color(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_ColorToInt__Color_m996266071 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_ConstrainRect__Vector2__Vector2__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_ConstrainRect__Vector2__Vector2__Vector2__Vector2_m4198367072 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_ConvertToPixels__Rect__Int32__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_ConvertToPixels__Rect__Int32__Int32__Boolean_m61671174 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_ConvertToTexCoords__Rect__Int32__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_ConvertToTexCoords__Rect__Int32__Int32__Boolean_m234101842 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_DecimalToHex24__Int32(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_DecimalToHex24__Int32_m3846052034 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_DecimalToHex32__Int32(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_DecimalToHex32__Int32_m4174219717 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_DecimalToHex8__Int32(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_DecimalToHex8__Int32_m1285510306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_DecimalToHexChar__Int32(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_DecimalToHexChar__Int32_m3605626574 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_DistanceToRectangle__Vector3_Array__Vector2__Camera(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_DistanceToRectangle__Vector3_Array__Vector2__Camera_m1702335950 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_DistanceToRectangle__Vector2_Array__Vector2(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_DistanceToRectangle__Vector2_Array__Vector2_m389060554 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_GetPivot__Vector2(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_GetPivot__Vector2_m2370809704 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_GetPivotOffset__Pivot(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_GetPivotOffset__Pivot_m1055717448 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_HexToColor__UInt32(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_HexToColor__UInt32_m942887651 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_HexToDecimal__Char(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_HexToDecimal__Char_m4160649998 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_IntToBinary__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_IntToBinary__Int32__Int32_m3206550416 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_IntToColor__Int32(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_IntToColor__Int32_m871775578 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_Lerp__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_Lerp__Single__Single__Single_m1860214860 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_MakePixelPerfect__Rect__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_MakePixelPerfect__Rect__Int32__Int32_m1574586838 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_MakePixelPerfect__Rect(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_MakePixelPerfect__Rect_m3817861718 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_MoveRect__UIRect__Single__Single(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_MoveRect__UIRect__Single__Single_m482936794 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_MoveWidget__UIRect__Single__Single(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_MoveWidget__UIRect__Single__Single_m731323642 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_OverlayPosition__Transform__Vector3__Camera__Camera(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_OverlayPosition__Transform__Vector3__Camera__Camera_m3140924652 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_OverlayPosition__Transform__Vector3__Camera(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_OverlayPosition__Transform__Vector3__Camera_m3299496327 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_OverlayPosition__Transform__Transform(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_OverlayPosition__Transform__Transform_m3953226750 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_RepeatIndex__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_RepeatIndex__Int32__Int32_m2361095708 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_ResizeWidget__UIWidget__Pivot__Single__Single__Int32__Int32__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_ResizeWidget__UIWidget__Pivot__Single__Single__Int32__Int32__Int32__Int32_m46121031 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_ResizeWidget__UIWidget__Pivot__Single__Single__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_ResizeWidget__UIWidget__Pivot__Single__Single__Int32__Int32_m883823239 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_RotateTowards__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_RotateTowards__Single__Single__Single_m2365434494 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_ScreenToParentPixels__Vector2__Transform(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_ScreenToParentPixels__Vector2__Transform_m1406278488 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_ScreenToPixels__Vector2__Transform(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_ScreenToPixels__Vector2__Transform_m3096237262 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_SpringDampen__Vector3__Single__Single(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_SpringDampen__Vector3__Single__Single_m2937882767 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_SpringDampen__Vector2__Single__Single(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_SpringDampen__Vector2__Single__Single_m3860566350 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_SpringLerp__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_SpringLerp__Single__Single__Single__Single_m4117649025 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_SpringLerp__Quaternion__Quaternion__Single__Single(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_SpringLerp__Quaternion__Quaternion__Single__Single_m98778669 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_SpringLerp__Vector3__Vector3__Single__Single(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_SpringLerp__Vector3__Vector3__Single__Single_m2262812657 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_SpringLerp__Vector2__Vector2__Single__Single(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_SpringLerp__Vector2__Vector2__Single__Single_m2215915217 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_SpringLerp__Single__Single(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_SpringLerp__Single__Single_m1104708081 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_WorldToLocalPoint__Vector3__Camera__Camera__Transform(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_WorldToLocalPoint__Vector3__Camera__Camera__Transform_m3442187369 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_Wrap01__Single(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_Wrap01__Single_m4091029008 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::NGUIMath_WrapAngle__Single(JSVCall,System.Int32)
extern "C"  bool NGUIMathGenerated_NGUIMath_WrapAngle__Single_m60614806 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIMathGenerated::__Register()
extern "C"  void NGUIMathGenerated___Register_m4182938390 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] NGUIMathGenerated::<NGUIMath_DistanceToRectangle__Vector3_Array__Vector2__Camera>m__86()
extern "C"  Vector3U5BU5D_t215400611* NGUIMathGenerated_U3CNGUIMath_DistanceToRectangle__Vector3_Array__Vector2__CameraU3Em__86_m1192361903 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] NGUIMathGenerated::<NGUIMath_DistanceToRectangle__Vector2_Array__Vector2>m__87()
extern "C"  Vector2U5BU5D_t4024180168* NGUIMathGenerated_U3CNGUIMath_DistanceToRectangle__Vector2_Array__Vector2U3Em__87_m802157581 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIMathGenerated::ilo_setInt321(System.Int32,System.Int32)
extern "C"  void NGUIMathGenerated_ilo_setInt321_m3911921061 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NGUIMathGenerated::ilo_getSingle2(System.Int32)
extern "C"  float NGUIMathGenerated_ilo_getSingle2_m159028639 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NGUIMathGenerated::ilo_getInt323(System.Int32)
extern "C"  int32_t NGUIMathGenerated_ilo_getInt323_m2874152822 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIMathGenerated::ilo_AdjustWidget4(UIWidget,System.Single,System.Single,System.Single,System.Single,System.Int32,System.Int32)
extern "C"  void NGUIMathGenerated_ilo_AdjustWidget4_m1541571257 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ___w0, float ___left1, float ___bottom2, float ___right3, float ___top4, int32_t ___minWidth5, int32_t ___minHeight6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object NGUIMathGenerated::ilo_getObject5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * NGUIMathGenerated_ilo_getObject5_m3174499768 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds NGUIMathGenerated::ilo_CalculateRelativeWidgetBounds6(UnityEngine.Transform,UnityEngine.Transform,System.Boolean,System.Boolean)
extern "C"  Bounds_t2711641849  NGUIMathGenerated_ilo_CalculateRelativeWidgetBounds6_m4000624884 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___relativeTo0, Transform_t1659122786 * ___content1, bool ___considerInactive2, bool ___considerParents3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUIMathGenerated::ilo_getBooleanS7(System.Int32)
extern "C"  bool NGUIMathGenerated_ilo_getBooleanS7_m1545887737 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds NGUIMathGenerated::ilo_CalculateRelativeWidgetBounds8(UnityEngine.Transform,System.Boolean)
extern "C"  Bounds_t2711641849  NGUIMathGenerated_ilo_CalculateRelativeWidgetBounds8_m1762697602 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___trans0, bool ___considerInactive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NGUIMathGenerated::ilo_setObject9(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t NGUIMathGenerated_ilo_setObject9_m3607949189 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 NGUIMathGenerated::ilo_getVector2S10(System.Int32)
extern "C"  Vector2_t4282066565  NGUIMathGenerated_ilo_getVector2S10_m4098205217 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIMathGenerated::ilo_setVector2S11(System.Int32,UnityEngine.Vector2)
extern "C"  void NGUIMathGenerated_ilo_setVector2S11_m3337834183 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect NGUIMathGenerated::ilo_ConvertToTexCoords12(UnityEngine.Rect,System.Int32,System.Int32,System.Boolean)
extern "C"  Rect_t4241904616  NGUIMathGenerated_ilo_ConvertToTexCoords12_m58127452 (Il2CppObject * __this /* static, unused */, Rect_t4241904616  ___rect0, int32_t ___width1, int32_t ___height2, bool ___isConvert3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String NGUIMathGenerated::ilo_DecimalToHex3213(System.Int32)
extern "C"  String_t* NGUIMathGenerated_ilo_DecimalToHex3213_m3775705956 (Il2CppObject * __this /* static, unused */, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Char NGUIMathGenerated::ilo_DecimalToHexChar14(System.Int32)
extern "C"  Il2CppChar NGUIMathGenerated_ilo_DecimalToHexChar14_m2431527361 (Il2CppObject * __this /* static, unused */, int32_t ___num0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIMathGenerated::ilo_setChar15(System.Int32,System.Char)
extern "C"  void NGUIMathGenerated_ilo_setChar15_m1100559840 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Il2CppChar ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIMathGenerated::ilo_setSingle16(System.Int32,System.Single)
extern "C"  void NGUIMathGenerated_ilo_setSingle16_m3480840865 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color NGUIMathGenerated::ilo_IntToColor17(System.Int32)
extern "C"  Color_t4194546905  NGUIMathGenerated_ilo_IntToColor17_m4042334223 (Il2CppObject * __this /* static, unused */, int32_t ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUIMathGenerated::ilo_MoveRect18(UIRect,System.Single,System.Single)
extern "C"  void NGUIMathGenerated_ilo_MoveRect18_m2639368436 (Il2CppObject * __this /* static, unused */, UIRect_t2503437976 * ___rect0, float ___x1, float ___y2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 NGUIMathGenerated::ilo_getVector3S19(System.Int32)
extern "C"  Vector3_t4282066566  NGUIMathGenerated_ilo_getVector3S19_m258399976 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NGUIMathGenerated::ilo_getEnum20(System.Int32)
extern "C"  int32_t NGUIMathGenerated_ilo_getEnum20_m172273040 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NGUIMathGenerated::ilo_RotateTowards21(System.Single,System.Single,System.Single)
extern "C"  float NGUIMathGenerated_ilo_RotateTowards21_m2151181645 (Il2CppObject * __this /* static, unused */, float ___from0, float ___to1, float ___maxAngle2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NGUIMathGenerated::ilo_getArgIndex22()
extern "C"  int32_t NGUIMathGenerated_ilo_getArgIndex22_m733083740 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion NGUIMathGenerated::ilo_SpringLerp23(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single,System.Single)
extern "C"  Quaternion_t1553702882  NGUIMathGenerated_ilo_SpringLerp23_m514645805 (Il2CppObject * __this /* static, unused */, Quaternion_t1553702882  ___from0, Quaternion_t1553702882  ___to1, float ___strength2, float ___deltaTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 NGUIMathGenerated::ilo_SpringLerp24(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern "C"  Vector3_t4282066566  NGUIMathGenerated_ilo_SpringLerp24_m636001348 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___from0, Vector3_t4282066566  ___to1, float ___strength2, float ___deltaTime3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NGUIMathGenerated::ilo_WrapAngle25(System.Single)
extern "C"  float NGUIMathGenerated_ilo_WrapAngle25_m1025866519 (Il2CppObject * __this /* static, unused */, float ___angle0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NGUIMathGenerated::ilo_getElement26(System.Int32,System.Int32)
extern "C"  int32_t NGUIMathGenerated_ilo_getElement26_m1660345604 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
