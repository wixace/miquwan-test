﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_raspalkiHurcaytoo103
struct  M_raspalkiHurcaytoo103_t2071944155  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_raspalkiHurcaytoo103::_troukormas
	uint32_t ____troukormas_0;
	// System.Boolean GarbageiOS.M_raspalkiHurcaytoo103::_pealaSeajaibe
	bool ____pealaSeajaibe_1;
	// System.String GarbageiOS.M_raspalkiHurcaytoo103::_caikejeHelmaidel
	String_t* ____caikejeHelmaidel_2;
	// System.UInt32 GarbageiOS.M_raspalkiHurcaytoo103::_vonuBerere
	uint32_t ____vonuBerere_3;
	// System.Single GarbageiOS.M_raspalkiHurcaytoo103::_vonarfeXexurca
	float ____vonarfeXexurca_4;
	// System.String GarbageiOS.M_raspalkiHurcaytoo103::_ligi
	String_t* ____ligi_5;
	// System.Int32 GarbageiOS.M_raspalkiHurcaytoo103::_hutraWayju
	int32_t ____hutraWayju_6;
	// System.UInt32 GarbageiOS.M_raspalkiHurcaytoo103::_reete
	uint32_t ____reete_7;
	// System.Boolean GarbageiOS.M_raspalkiHurcaytoo103::_towgearir
	bool ____towgearir_8;
	// System.String GarbageiOS.M_raspalkiHurcaytoo103::_haihe
	String_t* ____haihe_9;
	// System.Int32 GarbageiOS.M_raspalkiHurcaytoo103::_xonearPeldouka
	int32_t ____xonearPeldouka_10;
	// System.Boolean GarbageiOS.M_raspalkiHurcaytoo103::_toulehou
	bool ____toulehou_11;
	// System.Int32 GarbageiOS.M_raspalkiHurcaytoo103::_torxow
	int32_t ____torxow_12;
	// System.Single GarbageiOS.M_raspalkiHurcaytoo103::_telsarraw
	float ____telsarraw_13;
	// System.UInt32 GarbageiOS.M_raspalkiHurcaytoo103::_cernairchearFeru
	uint32_t ____cernairchearFeru_14;
	// System.String GarbageiOS.M_raspalkiHurcaytoo103::_stawjurdor
	String_t* ____stawjurdor_15;
	// System.UInt32 GarbageiOS.M_raspalkiHurcaytoo103::_sohair
	uint32_t ____sohair_16;
	// System.Int32 GarbageiOS.M_raspalkiHurcaytoo103::_jayjaw
	int32_t ____jayjaw_17;
	// System.Single GarbageiOS.M_raspalkiHurcaytoo103::_wugecou
	float ____wugecou_18;
	// System.Int32 GarbageiOS.M_raspalkiHurcaytoo103::_jawurveDewallfal
	int32_t ____jawurveDewallfal_19;
	// System.String GarbageiOS.M_raspalkiHurcaytoo103::_mawpursiGarowgee
	String_t* ____mawpursiGarowgee_20;
	// System.Single GarbageiOS.M_raspalkiHurcaytoo103::_welmesis
	float ____welmesis_21;

public:
	inline static int32_t get_offset_of__troukormas_0() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____troukormas_0)); }
	inline uint32_t get__troukormas_0() const { return ____troukormas_0; }
	inline uint32_t* get_address_of__troukormas_0() { return &____troukormas_0; }
	inline void set__troukormas_0(uint32_t value)
	{
		____troukormas_0 = value;
	}

	inline static int32_t get_offset_of__pealaSeajaibe_1() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____pealaSeajaibe_1)); }
	inline bool get__pealaSeajaibe_1() const { return ____pealaSeajaibe_1; }
	inline bool* get_address_of__pealaSeajaibe_1() { return &____pealaSeajaibe_1; }
	inline void set__pealaSeajaibe_1(bool value)
	{
		____pealaSeajaibe_1 = value;
	}

	inline static int32_t get_offset_of__caikejeHelmaidel_2() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____caikejeHelmaidel_2)); }
	inline String_t* get__caikejeHelmaidel_2() const { return ____caikejeHelmaidel_2; }
	inline String_t** get_address_of__caikejeHelmaidel_2() { return &____caikejeHelmaidel_2; }
	inline void set__caikejeHelmaidel_2(String_t* value)
	{
		____caikejeHelmaidel_2 = value;
		Il2CppCodeGenWriteBarrier(&____caikejeHelmaidel_2, value);
	}

	inline static int32_t get_offset_of__vonuBerere_3() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____vonuBerere_3)); }
	inline uint32_t get__vonuBerere_3() const { return ____vonuBerere_3; }
	inline uint32_t* get_address_of__vonuBerere_3() { return &____vonuBerere_3; }
	inline void set__vonuBerere_3(uint32_t value)
	{
		____vonuBerere_3 = value;
	}

	inline static int32_t get_offset_of__vonarfeXexurca_4() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____vonarfeXexurca_4)); }
	inline float get__vonarfeXexurca_4() const { return ____vonarfeXexurca_4; }
	inline float* get_address_of__vonarfeXexurca_4() { return &____vonarfeXexurca_4; }
	inline void set__vonarfeXexurca_4(float value)
	{
		____vonarfeXexurca_4 = value;
	}

	inline static int32_t get_offset_of__ligi_5() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____ligi_5)); }
	inline String_t* get__ligi_5() const { return ____ligi_5; }
	inline String_t** get_address_of__ligi_5() { return &____ligi_5; }
	inline void set__ligi_5(String_t* value)
	{
		____ligi_5 = value;
		Il2CppCodeGenWriteBarrier(&____ligi_5, value);
	}

	inline static int32_t get_offset_of__hutraWayju_6() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____hutraWayju_6)); }
	inline int32_t get__hutraWayju_6() const { return ____hutraWayju_6; }
	inline int32_t* get_address_of__hutraWayju_6() { return &____hutraWayju_6; }
	inline void set__hutraWayju_6(int32_t value)
	{
		____hutraWayju_6 = value;
	}

	inline static int32_t get_offset_of__reete_7() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____reete_7)); }
	inline uint32_t get__reete_7() const { return ____reete_7; }
	inline uint32_t* get_address_of__reete_7() { return &____reete_7; }
	inline void set__reete_7(uint32_t value)
	{
		____reete_7 = value;
	}

	inline static int32_t get_offset_of__towgearir_8() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____towgearir_8)); }
	inline bool get__towgearir_8() const { return ____towgearir_8; }
	inline bool* get_address_of__towgearir_8() { return &____towgearir_8; }
	inline void set__towgearir_8(bool value)
	{
		____towgearir_8 = value;
	}

	inline static int32_t get_offset_of__haihe_9() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____haihe_9)); }
	inline String_t* get__haihe_9() const { return ____haihe_9; }
	inline String_t** get_address_of__haihe_9() { return &____haihe_9; }
	inline void set__haihe_9(String_t* value)
	{
		____haihe_9 = value;
		Il2CppCodeGenWriteBarrier(&____haihe_9, value);
	}

	inline static int32_t get_offset_of__xonearPeldouka_10() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____xonearPeldouka_10)); }
	inline int32_t get__xonearPeldouka_10() const { return ____xonearPeldouka_10; }
	inline int32_t* get_address_of__xonearPeldouka_10() { return &____xonearPeldouka_10; }
	inline void set__xonearPeldouka_10(int32_t value)
	{
		____xonearPeldouka_10 = value;
	}

	inline static int32_t get_offset_of__toulehou_11() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____toulehou_11)); }
	inline bool get__toulehou_11() const { return ____toulehou_11; }
	inline bool* get_address_of__toulehou_11() { return &____toulehou_11; }
	inline void set__toulehou_11(bool value)
	{
		____toulehou_11 = value;
	}

	inline static int32_t get_offset_of__torxow_12() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____torxow_12)); }
	inline int32_t get__torxow_12() const { return ____torxow_12; }
	inline int32_t* get_address_of__torxow_12() { return &____torxow_12; }
	inline void set__torxow_12(int32_t value)
	{
		____torxow_12 = value;
	}

	inline static int32_t get_offset_of__telsarraw_13() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____telsarraw_13)); }
	inline float get__telsarraw_13() const { return ____telsarraw_13; }
	inline float* get_address_of__telsarraw_13() { return &____telsarraw_13; }
	inline void set__telsarraw_13(float value)
	{
		____telsarraw_13 = value;
	}

	inline static int32_t get_offset_of__cernairchearFeru_14() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____cernairchearFeru_14)); }
	inline uint32_t get__cernairchearFeru_14() const { return ____cernairchearFeru_14; }
	inline uint32_t* get_address_of__cernairchearFeru_14() { return &____cernairchearFeru_14; }
	inline void set__cernairchearFeru_14(uint32_t value)
	{
		____cernairchearFeru_14 = value;
	}

	inline static int32_t get_offset_of__stawjurdor_15() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____stawjurdor_15)); }
	inline String_t* get__stawjurdor_15() const { return ____stawjurdor_15; }
	inline String_t** get_address_of__stawjurdor_15() { return &____stawjurdor_15; }
	inline void set__stawjurdor_15(String_t* value)
	{
		____stawjurdor_15 = value;
		Il2CppCodeGenWriteBarrier(&____stawjurdor_15, value);
	}

	inline static int32_t get_offset_of__sohair_16() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____sohair_16)); }
	inline uint32_t get__sohair_16() const { return ____sohair_16; }
	inline uint32_t* get_address_of__sohair_16() { return &____sohair_16; }
	inline void set__sohair_16(uint32_t value)
	{
		____sohair_16 = value;
	}

	inline static int32_t get_offset_of__jayjaw_17() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____jayjaw_17)); }
	inline int32_t get__jayjaw_17() const { return ____jayjaw_17; }
	inline int32_t* get_address_of__jayjaw_17() { return &____jayjaw_17; }
	inline void set__jayjaw_17(int32_t value)
	{
		____jayjaw_17 = value;
	}

	inline static int32_t get_offset_of__wugecou_18() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____wugecou_18)); }
	inline float get__wugecou_18() const { return ____wugecou_18; }
	inline float* get_address_of__wugecou_18() { return &____wugecou_18; }
	inline void set__wugecou_18(float value)
	{
		____wugecou_18 = value;
	}

	inline static int32_t get_offset_of__jawurveDewallfal_19() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____jawurveDewallfal_19)); }
	inline int32_t get__jawurveDewallfal_19() const { return ____jawurveDewallfal_19; }
	inline int32_t* get_address_of__jawurveDewallfal_19() { return &____jawurveDewallfal_19; }
	inline void set__jawurveDewallfal_19(int32_t value)
	{
		____jawurveDewallfal_19 = value;
	}

	inline static int32_t get_offset_of__mawpursiGarowgee_20() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____mawpursiGarowgee_20)); }
	inline String_t* get__mawpursiGarowgee_20() const { return ____mawpursiGarowgee_20; }
	inline String_t** get_address_of__mawpursiGarowgee_20() { return &____mawpursiGarowgee_20; }
	inline void set__mawpursiGarowgee_20(String_t* value)
	{
		____mawpursiGarowgee_20 = value;
		Il2CppCodeGenWriteBarrier(&____mawpursiGarowgee_20, value);
	}

	inline static int32_t get_offset_of__welmesis_21() { return static_cast<int32_t>(offsetof(M_raspalkiHurcaytoo103_t2071944155, ____welmesis_21)); }
	inline float get__welmesis_21() const { return ____welmesis_21; }
	inline float* get_address_of__welmesis_21() { return &____welmesis_21; }
	inline void set__welmesis_21(float value)
	{
		____welmesis_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
