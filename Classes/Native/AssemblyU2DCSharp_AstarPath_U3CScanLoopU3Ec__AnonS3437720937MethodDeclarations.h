﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AstarPath/<ScanLoop>c__AnonStorey107
struct U3CScanLoopU3Ec__AnonStorey107_t3437720937;

#include "codegen/il2cpp-codegen.h"

// System.Void AstarPath/<ScanLoop>c__AnonStorey107::.ctor()
extern "C"  void U3CScanLoopU3Ec__AnonStorey107__ctor_m1862056786 (U3CScanLoopU3Ec__AnonStorey107_t3437720937 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
