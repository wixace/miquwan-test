﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Object
struct Object_t3071478659;

#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mihua.Asset.ABLoadOperation.AssetOperationSimulation
struct  AssetOperationSimulation_t2083017220  : public AssetOperation_t778728221
{
public:
	// UnityEngine.Object Mihua.Asset.ABLoadOperation.AssetOperationSimulation::m_SimulatedObject
	Object_t3071478659 * ___m_SimulatedObject_2;

public:
	inline static int32_t get_offset_of_m_SimulatedObject_2() { return static_cast<int32_t>(offsetof(AssetOperationSimulation_t2083017220, ___m_SimulatedObject_2)); }
	inline Object_t3071478659 * get_m_SimulatedObject_2() const { return ___m_SimulatedObject_2; }
	inline Object_t3071478659 ** get_address_of_m_SimulatedObject_2() { return &___m_SimulatedObject_2; }
	inline void set_m_SimulatedObject_2(Object_t3071478659 * value)
	{
		___m_SimulatedObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_SimulatedObject_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
