﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginQiangWan
struct PluginQiangWan_t3301124917;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.String
struct String_t;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// System.Object
struct Il2CppObject;
// ProductsCfgMgr
struct ProductsCfgMgr_t2493714872;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Mihua.SDK.CreatRoleInfo
struct CreatRoleInfo_t2858846543;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_PluginQiangWan3301124917.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr2493714872.h"
#include "AssemblyU2DCSharp_FloatTextMgr630384591.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"

// System.Void PluginQiangWan::.ctor()
extern "C"  void PluginQiangWan__ctor_m904359174 (PluginQiangWan_t3301124917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::Init()
extern "C"  void PluginQiangWan_Init_m534898830 (PluginQiangWan_t3301124917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginQiangWan::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginQiangWan_ReqSDKHttpLogin_m1077168475 (PluginQiangWan_t3301124917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginQiangWan::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginQiangWan_IsLoginSuccess_m1303670413 (PluginQiangWan_t3301124917 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::OpenUserLogin()
extern "C"  void PluginQiangWan_OpenUserLogin_m897460568 (PluginQiangWan_t3301124917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::OnLoginSuccess(System.String)
extern "C"  void PluginQiangWan_OnLoginSuccess_m1185889163 (PluginQiangWan_t3301124917 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::OnLogoutSuccess(System.String)
extern "C"  void PluginQiangWan_OnLogoutSuccess_m2406368356 (PluginQiangWan_t3301124917 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::ResLogoutSuccess(System.String)
extern "C"  void PluginQiangWan_ResLogoutSuccess_m2537647179 (PluginQiangWan_t3301124917 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::UserPay(CEvent.ZEvent)
extern "C"  void PluginQiangWan_UserPay_m3686577946 (PluginQiangWan_t3301124917 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginQiangWan_SignCallBack_m140991667 (PluginQiangWan_t3301124917 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginQiangWan::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginQiangWan_BuildOrderParam2WWWForm_m1349279195 (PluginQiangWan_t3301124917 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::OnPaySuccess(System.String)
extern "C"  void PluginQiangWan_OnPaySuccess_m1723139210 (PluginQiangWan_t3301124917 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::OnPayFail(System.String)
extern "C"  void PluginQiangWan_OnPayFail_m4203202807 (PluginQiangWan_t3301124917 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::OnEnterGame(CEvent.ZEvent)
extern "C"  void PluginQiangWan_OnEnterGame_m1704530924 (PluginQiangWan_t3301124917 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::OnCreatRole(CEvent.ZEvent)
extern "C"  void PluginQiangWan_OnCreatRole_m2533946775 (PluginQiangWan_t3301124917 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::OnLevelUp(CEvent.ZEvent)
extern "C"  void PluginQiangWan_OnLevelUp_m2194592759 (PluginQiangWan_t3301124917 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::ClollectData(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginQiangWan_ClollectData_m2607905642 (PluginQiangWan_t3301124917 * __this, String_t* ___type0, String_t* ___serverid1, String_t* ___servername2, String_t* ___roleid3, String_t* ___rolename4, String_t* ___rolelevel5, String_t* ___roleGold6, String_t* ___VIPLv7, String_t* ___CreatRoleTime8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::initSdk(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginQiangWan_initSdk_m1364901404 (PluginQiangWan_t3301124917 * __this, String_t* ___sdkid0, String_t* ___sdappname1, String_t* ___sdkappid2, String_t* ___sdkpromoteid3, String_t* ___sdkpromotename4, String_t* ___sdktesttype5, String_t* ___sdktrackkey6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::switchAcc()
extern "C"  void PluginQiangWan_switchAcc_m533526065 (PluginQiangWan_t3301124917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::login()
extern "C"  void PluginQiangWan_login_m426373997 (PluginQiangWan_t3301124917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::goZF(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginQiangWan_goZF_m4264425768 (PluginQiangWan_t3301124917 * __this, String_t* ___amount0, String_t* ___orderId1, String_t* ___extra2, String_t* ___tempId3, String_t* ___serverId4, String_t* ___serverName5, String_t* ___roleId6, String_t* ___roleName7, String_t* ___setProductNumber8, String_t* ___setProductName9, String_t* ___rolelv10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::coloectData(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginQiangWan_coloectData_m862959797 (PluginQiangWan_t3301124917 * __this, String_t* ___serverId0, String_t* ___servername1, String_t* ___roleid2, String_t* ___rolename3, String_t* ___rolelevel4, String_t* ___ctime5, String_t* ___type6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::<ResLogoutSuccess>m__44D()
extern "C"  void PluginQiangWan_U3CResLogoutSuccessU3Em__44D_m876130782 (PluginQiangWan_t3301124917 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginQiangWan_ilo_AddEventListener1_m2843278126 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginQiangWan::ilo_get_isSdkLogin2(VersionMgr)
extern "C"  bool PluginQiangWan_ilo_get_isSdkLogin2_m1296913165 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginQiangWan::ilo_get_DeviceID3(PluginsSdkMgr)
extern "C"  String_t* PluginQiangWan_ilo_get_DeviceID3_m1155676973 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginQiangWan::ilo_get_Instance4()
extern "C"  VersionMgr_t1322950208 * PluginQiangWan_ilo_get_Instance4_m2857039818 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::ilo_login5(PluginQiangWan)
extern "C"  void PluginQiangWan_ilo_login5_m4229347368 (Il2CppObject * __this /* static, unused */, PluginQiangWan_t3301124917 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::ilo_ReqSDKHttpLogin6(PluginsSdkMgr)
extern "C"  void PluginQiangWan_ilo_ReqSDKHttpLogin6_m2063134834 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::ilo_ClollectData7(PluginQiangWan,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginQiangWan_ilo_ClollectData7_m2119317973 (Il2CppObject * __this /* static, unused */, PluginQiangWan_t3301124917 * ____this0, String_t* ___type1, String_t* ___serverid2, String_t* ___servername3, String_t* ___roleid4, String_t* ___rolename5, String_t* ___rolelevel6, String_t* ___roleGold7, String_t* ___VIPLv8, String_t* ___CreatRoleTime9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::ilo_switchAcc8(PluginQiangWan)
extern "C"  void PluginQiangWan_ilo_switchAcc8_m2667576679 (Il2CppObject * __this /* static, unused */, PluginQiangWan_t3301124917 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginQiangWan::ilo_ContainsKey9(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  bool PluginQiangWan_ilo_ContainsKey9_m2411530954 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken PluginQiangWan::ilo_get_Item10(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * PluginQiangWan_ilo_get_Item10_m2528832547 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginQiangWan::ilo_ToString11(Newtonsoft.Json.Linq.JToken)
extern "C"  String_t* PluginQiangWan_ilo_ToString11_m1837385866 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::ilo_Log12(System.Object,System.Boolean)
extern "C"  void PluginQiangWan_ilo_Log12_m3804207263 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProductsCfgMgr PluginQiangWan::ilo_get_ProductsCfgMgr13()
extern "C"  ProductsCfgMgr_t2493714872 * PluginQiangWan_ilo_get_ProductsCfgMgr13_m1370274745 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginQiangWan::ilo_GetProductID14(ProductsCfgMgr,System.String)
extern "C"  String_t* PluginQiangWan_ilo_GetProductID14_m3452504399 (Il2CppObject * __this /* static, unused */, ProductsCfgMgr_t2493714872 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginQiangWan::ilo_FloatText15(FloatTextMgr,FLOAT_TEXT_ID,System.Int32,System.Object[])
extern "C"  void PluginQiangWan_ilo_FloatText15_m4249410418 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, int32_t ___textId1, int32_t ___offsetY2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.CreatRoleInfo PluginQiangWan::ilo_Parse16(System.Object[])
extern "C"  CreatRoleInfo_t2858846543 * PluginQiangWan_ilo_Parse16_m2475303577 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
