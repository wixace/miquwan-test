﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIToggledComponentsGenerated
struct UIToggledComponentsGenerated_t3958731549;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UIToggledComponentsGenerated::.ctor()
extern "C"  void UIToggledComponentsGenerated__ctor_m1970176030 (UIToggledComponentsGenerated_t3958731549 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggledComponentsGenerated::UIToggledComponents_UIToggledComponents1(JSVCall,System.Int32)
extern "C"  bool UIToggledComponentsGenerated_UIToggledComponents_UIToggledComponents1_m1814637942 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggledComponentsGenerated::UIToggledComponents_activate(JSVCall)
extern "C"  void UIToggledComponentsGenerated_UIToggledComponents_activate_m532997281 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggledComponentsGenerated::UIToggledComponents_deactivate(JSVCall)
extern "C"  void UIToggledComponentsGenerated_UIToggledComponents_deactivate_m1923176128 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggledComponentsGenerated::UIToggledComponents_Toggle(JSVCall,System.Int32)
extern "C"  bool UIToggledComponentsGenerated_UIToggledComponents_Toggle_m15626699 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggledComponentsGenerated::__Register()
extern "C"  void UIToggledComponentsGenerated___Register_m3295960937 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIToggledComponentsGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UIToggledComponentsGenerated_ilo_getObject1_m1084266676 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIToggledComponentsGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIToggledComponentsGenerated_ilo_getObject2_m2728998034 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
