﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ActiveAnimationGenerated
struct ActiveAnimationGenerated_t4002167857;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;
// ActiveAnimation
struct ActiveAnimation_t557316862;
// UnityEngine.Animation
struct Animation_t1724966010;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Animation1724966010.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_AnimationOrTween_Direction1859285089.h"

// System.Void ActiveAnimationGenerated::.ctor()
extern "C"  void ActiveAnimationGenerated__ctor_m684161418 (ActiveAnimationGenerated_t4002167857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActiveAnimationGenerated::ActiveAnimation_ActiveAnimation1(JSVCall,System.Int32)
extern "C"  bool ActiveAnimationGenerated_ActiveAnimation_ActiveAnimation1_m356373554 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveAnimationGenerated::ActiveAnimation_current(JSVCall)
extern "C"  void ActiveAnimationGenerated_ActiveAnimation_current_m2787575655 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveAnimationGenerated::ActiveAnimation_onFinished(JSVCall)
extern "C"  void ActiveAnimationGenerated_ActiveAnimation_onFinished_m2707298235 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveAnimationGenerated::ActiveAnimation_eventReceiver(JSVCall)
extern "C"  void ActiveAnimationGenerated_ActiveAnimation_eventReceiver_m1850917047 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveAnimationGenerated::ActiveAnimation_callWhenFinished(JSVCall)
extern "C"  void ActiveAnimationGenerated_ActiveAnimation_callWhenFinished_m2235145090 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveAnimationGenerated::ActiveAnimation_isPlaying(JSVCall)
extern "C"  void ActiveAnimationGenerated_ActiveAnimation_isPlaying_m3687122716 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActiveAnimationGenerated::ActiveAnimation_Finish(JSVCall,System.Int32)
extern "C"  bool ActiveAnimationGenerated_ActiveAnimation_Finish_m2750949746 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActiveAnimationGenerated::ActiveAnimation_Reset(JSVCall,System.Int32)
extern "C"  bool ActiveAnimationGenerated_ActiveAnimation_Reset_m1944290130 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActiveAnimationGenerated::ActiveAnimation_Play__Animator__String__Direction__EnableCondition__DisableCondition(JSVCall,System.Int32)
extern "C"  bool ActiveAnimationGenerated_ActiveAnimation_Play__Animator__String__Direction__EnableCondition__DisableCondition_m3208536023 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActiveAnimationGenerated::ActiveAnimation_Play__Animation__String__Direction__EnableCondition__DisableCondition(JSVCall,System.Int32)
extern "C"  bool ActiveAnimationGenerated_ActiveAnimation_Play__Animation__String__Direction__EnableCondition__DisableCondition_m1904944560 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActiveAnimationGenerated::ActiveAnimation_Play__Animation__String__Direction(JSVCall,System.Int32)
extern "C"  bool ActiveAnimationGenerated_ActiveAnimation_Play__Animation__String__Direction_m885703325 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActiveAnimationGenerated::ActiveAnimation_Play__Animation__Direction(JSVCall,System.Int32)
extern "C"  bool ActiveAnimationGenerated_ActiveAnimation_Play__Animation__Direction_m2012924558 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveAnimationGenerated::__Register()
extern "C"  void ActiveAnimationGenerated___Register_m119171453 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ActiveAnimationGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool ActiveAnimationGenerated_ilo_attachFinalizerObject1_m2748490645 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ActiveAnimationGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void ActiveAnimationGenerated_ilo_addJSCSRel2_m4086724551 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ActiveAnimationGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t ActiveAnimationGenerated_ilo_setObject3_m4161616322 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ActiveAnimationGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * ActiveAnimationGenerated_ilo_getObject4_m2364456 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ActiveAnimationGenerated::ilo_getStringS5(System.Int32)
extern "C"  String_t* ActiveAnimationGenerated_ilo_getStringS5_m3923555020 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ActiveAnimationGenerated::ilo_getEnum6(System.Int32)
extern "C"  int32_t ActiveAnimationGenerated_ilo_getEnum6_m3226284043 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ActiveAnimation ActiveAnimationGenerated::ilo_Play7(UnityEngine.Animation,System.String,AnimationOrTween.Direction)
extern "C"  ActiveAnimation_t557316862 * ActiveAnimationGenerated_ilo_Play7_m416319955 (Il2CppObject * __this /* static, unused */, Animation_t1724966010 * ___anim0, String_t* ___clipName1, int32_t ___playDirection2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
