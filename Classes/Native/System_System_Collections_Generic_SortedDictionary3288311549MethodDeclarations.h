﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Int32>
struct KeyCollection_t3288311549;
// System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>
struct SortedDictionary_2_t3586408994;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t3065703549;
// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "System_System_Collections_Generic_SortedDictionary_371701258.h"

// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Int32>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m3550402856_gshared (KeyCollection_t3288311549 * __this, SortedDictionary_2_t3586408994 * ___dic0, const MethodInfo* method);
#define KeyCollection__ctor_m3550402856(__this, ___dic0, method) ((  void (*) (KeyCollection_t3288311549 *, SortedDictionary_2_t3586408994 *, const MethodInfo*))KeyCollection__ctor_m3550402856_gshared)(__this, ___dic0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1545902443_gshared (KeyCollection_t3288311549 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1545902443(__this, ___item0, method) ((  void (*) (KeyCollection_t3288311549 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1545902443_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2097683874_gshared (KeyCollection_t3288311549 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2097683874(__this, method) ((  void (*) (KeyCollection_t3288311549 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2097683874_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m24278891_gshared (KeyCollection_t3288311549 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m24278891(__this, ___item0, method) ((  bool (*) (KeyCollection_t3288311549 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m24278891_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1068794482_gshared (KeyCollection_t3288311549 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1068794482(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3288311549 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1068794482_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m443269132_gshared (KeyCollection_t3288311549 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m443269132(__this, method) ((  bool (*) (KeyCollection_t3288311549 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m443269132_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3258697616_gshared (KeyCollection_t3288311549 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3258697616(__this, ___item0, method) ((  bool (*) (KeyCollection_t3288311549 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3258697616_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m3088097044_gshared (KeyCollection_t3288311549 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m3088097044(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3288311549 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3088097044_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2530224062_gshared (KeyCollection_t3288311549 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2530224062(__this, method) ((  bool (*) (KeyCollection_t3288311549 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m2530224062_gshared)(__this, method)
// System.Object System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m3958322430_gshared (KeyCollection_t3288311549 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3958322430(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3288311549 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3958322430_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3890997027_gshared (KeyCollection_t3288311549 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3890997027(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3288311549 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3890997027_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Int32>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m1707792320_gshared (KeyCollection_t3288311549 * __this, Int32U5BU5D_t3230847821* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define KeyCollection_CopyTo_m1707792320(__this, ___array0, ___arrayIndex1, method) ((  void (*) (KeyCollection_t3288311549 *, Int32U5BU5D_t3230847821*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1707792320_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Int32 System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Int32>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m1688616452_gshared (KeyCollection_t3288311549 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m1688616452(__this, method) ((  int32_t (*) (KeyCollection_t3288311549 *, const MethodInfo*))KeyCollection_get_Count_m1688616452_gshared)(__this, method)
// System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.SortedDictionary`2/KeyCollection<System.Int32,System.Int32>::GetEnumerator()
extern "C"  Enumerator_t371701258  KeyCollection_GetEnumerator_m787326592_gshared (KeyCollection_t3288311549 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m787326592(__this, method) ((  Enumerator_t371701258  (*) (KeyCollection_t3288311549 *, const MethodInfo*))KeyCollection_GetEnumerator_m787326592_gshared)(__this, method)
