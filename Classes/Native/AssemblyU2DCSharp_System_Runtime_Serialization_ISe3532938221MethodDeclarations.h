﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Runtime_Serialization_ISerializableGenerated
struct System_Runtime_Serialization_ISerializableGenerated_t3532938221;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void System_Runtime_Serialization_ISerializableGenerated::.ctor()
extern "C"  void System_Runtime_Serialization_ISerializableGenerated__ctor_m2216178206 (System_Runtime_Serialization_ISerializableGenerated_t3532938221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Runtime_Serialization_ISerializableGenerated::ISerializable_GetObjectData__SerializationInfo__StreamingContext(JSVCall,System.Int32)
extern "C"  bool System_Runtime_Serialization_ISerializableGenerated_ISerializable_GetObjectData__SerializationInfo__StreamingContext_m3031146437 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Runtime_Serialization_ISerializableGenerated::__Register()
extern "C"  void System_Runtime_Serialization_ISerializableGenerated___Register_m906620265 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Runtime_Serialization_ISerializableGenerated::ilo_getObject1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_Runtime_Serialization_ISerializableGenerated_ilo_getObject1_m3644776583 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
