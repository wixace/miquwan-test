﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CombatEntity/<EnterPartBloom>c__AnonStorey151
struct U3CEnterPartBloomU3Ec__AnonStorey151_t1955055656;
// Mihua.Asset.ABLoadOperation.AssetOperation
struct AssetOperation_t778728221;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"

// System.Void CombatEntity/<EnterPartBloom>c__AnonStorey151::.ctor()
extern "C"  void U3CEnterPartBloomU3Ec__AnonStorey151__ctor_m1007658563 (U3CEnterPartBloomU3Ec__AnonStorey151_t1955055656 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CombatEntity/<EnterPartBloom>c__AnonStorey151::<>m__3C6(Mihua.Asset.ABLoadOperation.AssetOperation)
extern "C"  void U3CEnterPartBloomU3Ec__AnonStorey151_U3CU3Em__3C6_m3397916611 (U3CEnterPartBloomU3Ec__AnonStorey151_t1955055656 * __this, AssetOperation_t778728221 * ___ao0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
