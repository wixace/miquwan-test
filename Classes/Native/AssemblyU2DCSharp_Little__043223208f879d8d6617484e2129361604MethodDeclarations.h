﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._043223208f879d8d6617484e8ae125a3
struct _043223208f879d8d6617484e8ae125a3_t2129361604;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._043223208f879d8d6617484e8ae125a3::.ctor()
extern "C"  void _043223208f879d8d6617484e8ae125a3__ctor_m3713763305 (_043223208f879d8d6617484e8ae125a3_t2129361604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._043223208f879d8d6617484e8ae125a3::_043223208f879d8d6617484e8ae125a3m2(System.Int32)
extern "C"  int32_t _043223208f879d8d6617484e8ae125a3__043223208f879d8d6617484e8ae125a3m2_m1910569753 (_043223208f879d8d6617484e8ae125a3_t2129361604 * __this, int32_t ____043223208f879d8d6617484e8ae125a3a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._043223208f879d8d6617484e8ae125a3::_043223208f879d8d6617484e8ae125a3m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _043223208f879d8d6617484e8ae125a3__043223208f879d8d6617484e8ae125a3m_m4040514493 (_043223208f879d8d6617484e8ae125a3_t2129361604 * __this, int32_t ____043223208f879d8d6617484e8ae125a3a0, int32_t ____043223208f879d8d6617484e8ae125a3841, int32_t ____043223208f879d8d6617484e8ae125a3c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
