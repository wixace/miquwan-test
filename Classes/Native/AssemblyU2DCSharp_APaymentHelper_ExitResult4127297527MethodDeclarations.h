﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// APaymentHelper/ExitResult
struct ExitResult_t4127297527;

#include "codegen/il2cpp-codegen.h"

// System.Void APaymentHelper/ExitResult::.ctor()
extern "C"  void ExitResult__ctor_m278903636 (ExitResult_t4127297527 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
