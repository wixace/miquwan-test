﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraAniCfgMgr
struct CameraAniCfgMgr_t11764299;
// CameraAniMap
struct CameraAniMap_t3004656005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CameraAniMap3004656005.h"

// System.Void CameraAniCfgMgr::.ctor()
extern "C"  void CameraAniCfgMgr__ctor_m3351947648 (CameraAniCfgMgr_t11764299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraAniCfgMgr::.cctor()
extern "C"  void CameraAniCfgMgr__cctor_m349065773 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CameraAniCfgMgr::get_inited()
extern "C"  bool CameraAniCfgMgr_get_inited_m2729107432 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraAniCfgMgr::set_inited(System.Boolean)
extern "C"  void CameraAniCfgMgr_set_inited_m306715655 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CameraAniCfgMgr CameraAniCfgMgr::get_instance()
extern "C"  CameraAniCfgMgr_t11764299 * CameraAniCfgMgr_get_instance_m212109914 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraAniCfgMgr::Init()
extern "C"  void CameraAniCfgMgr_Init_m3938989268 (CameraAniCfgMgr_t11764299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CameraAniMap CameraAniCfgMgr::GetCameraAniMapByMapid(System.Int32)
extern "C"  CameraAniMap_t3004656005 * CameraAniCfgMgr_GetCameraAniMapByMapid_m2508311106 (CameraAniCfgMgr_t11764299 * __this, int32_t ___mapid0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CameraAniCfgMgr::ilo_get_id1(CameraAniMap)
extern "C"  int32_t CameraAniCfgMgr_ilo_get_id1_m295339041 (Il2CppObject * __this /* static, unused */, CameraAniMap_t3004656005 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraAniCfgMgr::ilo_set_inited2(System.Boolean)
extern "C"  void CameraAniCfgMgr_ilo_set_inited2_m424306408 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
