﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_cheasel3
struct M_cheasel3_t4147046314;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_cheasel34147046314.h"

// System.Void GarbageiOS.M_cheasel3::.ctor()
extern "C"  void M_cheasel3__ctor_m2472148425 (M_cheasel3_t4147046314 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheasel3::M_tanouqaLoowalldro0(System.String[],System.Int32)
extern "C"  void M_cheasel3_M_tanouqaLoowalldro0_m2664226002 (M_cheasel3_t4147046314 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheasel3::M_jisjursirNite1(System.String[],System.Int32)
extern "C"  void M_cheasel3_M_jisjursirNite1_m2464127072 (M_cheasel3_t4147046314 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheasel3::M_joowiRornatrou2(System.String[],System.Int32)
extern "C"  void M_cheasel3_M_joowiRornatrou2_m1042009230 (M_cheasel3_t4147046314 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheasel3::M_durhis3(System.String[],System.Int32)
extern "C"  void M_cheasel3_M_durhis3_m3092827438 (M_cheasel3_t4147046314 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheasel3::M_wugeaje4(System.String[],System.Int32)
extern "C"  void M_cheasel3_M_wugeaje4_m3081148728 (M_cheasel3_t4147046314 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheasel3::M_javeBornor5(System.String[],System.Int32)
extern "C"  void M_cheasel3_M_javeBornor5_m3926761679 (M_cheasel3_t4147046314 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheasel3::ilo_M_tanouqaLoowalldro01(GarbageiOS.M_cheasel3,System.String[],System.Int32)
extern "C"  void M_cheasel3_ilo_M_tanouqaLoowalldro01_m3528505586 (Il2CppObject * __this /* static, unused */, M_cheasel3_t4147046314 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheasel3::ilo_M_joowiRornatrou22(GarbageiOS.M_cheasel3,System.String[],System.Int32)
extern "C"  void M_cheasel3_ilo_M_joowiRornatrou22_m4180188669 (Il2CppObject * __this /* static, unused */, M_cheasel3_t4147046314 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheasel3::ilo_M_durhis33(GarbageiOS.M_cheasel3,System.String[],System.Int32)
extern "C"  void M_cheasel3_ilo_M_durhis33_m2409840094 (Il2CppObject * __this /* static, unused */, M_cheasel3_t4147046314 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_cheasel3::ilo_M_wugeaje44(GarbageiOS.M_cheasel3,System.String[],System.Int32)
extern "C"  void M_cheasel3_ilo_M_wugeaje44_m1260610191 (Il2CppObject * __this /* static, unused */, M_cheasel3_t4147046314 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
