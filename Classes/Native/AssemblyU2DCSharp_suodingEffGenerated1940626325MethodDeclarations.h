﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// suodingEffGenerated
struct suodingEffGenerated_t1940626325;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void suodingEffGenerated::.ctor()
extern "C"  void suodingEffGenerated__ctor_m3306139510 (suodingEffGenerated_t1940626325 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean suodingEffGenerated::suodingEff_suodingEff1(JSVCall,System.Int32)
extern "C"  bool suodingEffGenerated_suodingEff_suodingEff1_m1203340860 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void suodingEffGenerated::suodingEff_targetTf(JSVCall)
extern "C"  void suodingEffGenerated_suodingEff_targetTf_m597293387 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void suodingEffGenerated::__Register()
extern "C"  void suodingEffGenerated___Register_m3680700177 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object suodingEffGenerated::ilo_getObject1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * suodingEffGenerated_ilo_getObject1_m1998519343 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
