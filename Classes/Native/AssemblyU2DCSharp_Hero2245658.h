﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// CEvent.EventDispatch`1<CEvent.ZEvent>
struct EventDispatch_1_t535157068;
// CSHeroUnit
struct CSHeroUnit_t3764358446;
// AICtrl
struct AICtrl_t1930422963;

#include "AssemblyU2DCSharp_CombatEntity684137495.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Hero
struct  Hero_t2245658  : public CombatEntity_t684137495
{
public:
	// CEvent.EventDispatch`1<CEvent.ZEvent> Hero::eventDispatch
	EventDispatch_1_t535157068 * ___eventDispatch_323;
	// System.Int32 Hero::place
	int32_t ___place_324;
	// System.Single Hero::MycritP
	float ___MycritP_325;
	// System.Boolean Hero::forceMove
	bool ___forceMove_326;
	// System.Int32 Hero::marshalIndex
	int32_t ___marshalIndex_327;
	// System.Boolean Hero::IsAIMoveEndReached
	bool ___IsAIMoveEndReached_328;
	// CSHeroUnit Hero::tpl
	CSHeroUnit_t3764358446 * ___tpl_329;
	// AICtrl Hero::<aiCtrl>k__BackingField
	AICtrl_t1930422963 * ___U3CaiCtrlU3Ek__BackingField_330;
	// System.Boolean Hero::<IsBattling>k__BackingField
	bool ___U3CIsBattlingU3Ek__BackingField_331;

public:
	inline static int32_t get_offset_of_eventDispatch_323() { return static_cast<int32_t>(offsetof(Hero_t2245658, ___eventDispatch_323)); }
	inline EventDispatch_1_t535157068 * get_eventDispatch_323() const { return ___eventDispatch_323; }
	inline EventDispatch_1_t535157068 ** get_address_of_eventDispatch_323() { return &___eventDispatch_323; }
	inline void set_eventDispatch_323(EventDispatch_1_t535157068 * value)
	{
		___eventDispatch_323 = value;
		Il2CppCodeGenWriteBarrier(&___eventDispatch_323, value);
	}

	inline static int32_t get_offset_of_place_324() { return static_cast<int32_t>(offsetof(Hero_t2245658, ___place_324)); }
	inline int32_t get_place_324() const { return ___place_324; }
	inline int32_t* get_address_of_place_324() { return &___place_324; }
	inline void set_place_324(int32_t value)
	{
		___place_324 = value;
	}

	inline static int32_t get_offset_of_MycritP_325() { return static_cast<int32_t>(offsetof(Hero_t2245658, ___MycritP_325)); }
	inline float get_MycritP_325() const { return ___MycritP_325; }
	inline float* get_address_of_MycritP_325() { return &___MycritP_325; }
	inline void set_MycritP_325(float value)
	{
		___MycritP_325 = value;
	}

	inline static int32_t get_offset_of_forceMove_326() { return static_cast<int32_t>(offsetof(Hero_t2245658, ___forceMove_326)); }
	inline bool get_forceMove_326() const { return ___forceMove_326; }
	inline bool* get_address_of_forceMove_326() { return &___forceMove_326; }
	inline void set_forceMove_326(bool value)
	{
		___forceMove_326 = value;
	}

	inline static int32_t get_offset_of_marshalIndex_327() { return static_cast<int32_t>(offsetof(Hero_t2245658, ___marshalIndex_327)); }
	inline int32_t get_marshalIndex_327() const { return ___marshalIndex_327; }
	inline int32_t* get_address_of_marshalIndex_327() { return &___marshalIndex_327; }
	inline void set_marshalIndex_327(int32_t value)
	{
		___marshalIndex_327 = value;
	}

	inline static int32_t get_offset_of_IsAIMoveEndReached_328() { return static_cast<int32_t>(offsetof(Hero_t2245658, ___IsAIMoveEndReached_328)); }
	inline bool get_IsAIMoveEndReached_328() const { return ___IsAIMoveEndReached_328; }
	inline bool* get_address_of_IsAIMoveEndReached_328() { return &___IsAIMoveEndReached_328; }
	inline void set_IsAIMoveEndReached_328(bool value)
	{
		___IsAIMoveEndReached_328 = value;
	}

	inline static int32_t get_offset_of_tpl_329() { return static_cast<int32_t>(offsetof(Hero_t2245658, ___tpl_329)); }
	inline CSHeroUnit_t3764358446 * get_tpl_329() const { return ___tpl_329; }
	inline CSHeroUnit_t3764358446 ** get_address_of_tpl_329() { return &___tpl_329; }
	inline void set_tpl_329(CSHeroUnit_t3764358446 * value)
	{
		___tpl_329 = value;
		Il2CppCodeGenWriteBarrier(&___tpl_329, value);
	}

	inline static int32_t get_offset_of_U3CaiCtrlU3Ek__BackingField_330() { return static_cast<int32_t>(offsetof(Hero_t2245658, ___U3CaiCtrlU3Ek__BackingField_330)); }
	inline AICtrl_t1930422963 * get_U3CaiCtrlU3Ek__BackingField_330() const { return ___U3CaiCtrlU3Ek__BackingField_330; }
	inline AICtrl_t1930422963 ** get_address_of_U3CaiCtrlU3Ek__BackingField_330() { return &___U3CaiCtrlU3Ek__BackingField_330; }
	inline void set_U3CaiCtrlU3Ek__BackingField_330(AICtrl_t1930422963 * value)
	{
		___U3CaiCtrlU3Ek__BackingField_330 = value;
		Il2CppCodeGenWriteBarrier(&___U3CaiCtrlU3Ek__BackingField_330, value);
	}

	inline static int32_t get_offset_of_U3CIsBattlingU3Ek__BackingField_331() { return static_cast<int32_t>(offsetof(Hero_t2245658, ___U3CIsBattlingU3Ek__BackingField_331)); }
	inline bool get_U3CIsBattlingU3Ek__BackingField_331() const { return ___U3CIsBattlingU3Ek__BackingField_331; }
	inline bool* get_address_of_U3CIsBattlingU3Ek__BackingField_331() { return &___U3CIsBattlingU3Ek__BackingField_331; }
	inline void set_U3CIsBattlingU3Ek__BackingField_331(bool value)
	{
		___U3CIsBattlingU3Ek__BackingField_331 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
