﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3793750323MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1407409370(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1956437375 *, Dictionary_2_t329677924 *, const MethodInfo*))KeyCollection__ctor_m2966486865_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,System.Single>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m176020540(__this, ___item0, method) ((  void (*) (KeyCollection_t1956437375 *, CombatEntity_t684137495 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3744746341_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,System.Single>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3821396147(__this, method) ((  void (*) (KeyCollection_t1956437375 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3127903772_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,System.Single>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m4106424082(__this, ___item0, method) ((  bool (*) (KeyCollection_t1956437375 *, CombatEntity_t684137495 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1936554693_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,System.Single>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4165737719(__this, ___item0, method) ((  bool (*) (KeyCollection_t1956437375 *, CombatEntity_t684137495 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1106499946_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,System.Single>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2701799173(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1956437375 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1601911768_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,System.Single>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m2415218853(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1956437375 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2068684942_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,System.Single>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1325936244(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1956437375 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3665360777_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,System.Single>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m2079656435(__this, method) ((  bool (*) (KeyCollection_t1956437375 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4189279462_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,System.Single>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3750385253(__this, method) ((  bool (*) (KeyCollection_t1956437375 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m4278789400_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,System.Single>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2690326871(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1956437375 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3733356996_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,System.Single>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m1982507919(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1956437375 *, CombatEntityU5BU5D_t3566310830*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m3360156166_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,System.Single>::GetEnumerator()
#define KeyCollection_GetEnumerator_m172197980(__this, method) ((  Enumerator_t944613978  (*) (KeyCollection_t1956437375 *, const MethodInfo*))KeyCollection_GetEnumerator_m4094390505_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<CombatEntity,System.Single>::get_Count()
#define KeyCollection_get_Count_m202209567(__this, method) ((  int32_t (*) (KeyCollection_t1956437375 *, const MethodInfo*))KeyCollection_get_Count_m3992691422_gshared)(__this, method)
