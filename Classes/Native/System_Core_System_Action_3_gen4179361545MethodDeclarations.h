﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Action_3_gen1721707197MethodDeclarations.h"

// System.Void System.Action`3<System.Boolean,CombatEntity,System.Int32>::.ctor(System.Object,System.IntPtr)
#define Action_3__ctor_m299623776(__this, ___object0, ___method1, method) ((  void (*) (Action_3_t4179361545 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Action_3__ctor_m3521016969_gshared)(__this, ___object0, ___method1, method)
// System.Void System.Action`3<System.Boolean,CombatEntity,System.Int32>::Invoke(T1,T2,T3)
#define Action_3_Invoke_m2691379468(__this, ___arg10, ___arg21, ___arg32, method) ((  void (*) (Action_3_t4179361545 *, bool, CombatEntity_t684137495 *, int32_t, const MethodInfo*))Action_3_Invoke_m2964307845_gshared)(__this, ___arg10, ___arg21, ___arg32, method)
// System.IAsyncResult System.Action`3<System.Boolean,CombatEntity,System.Int32>::BeginInvoke(T1,T2,T3,System.AsyncCallback,System.Object)
#define Action_3_BeginInvoke_m543157691(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method) ((  Il2CppObject * (*) (Action_3_t4179361545 *, bool, CombatEntity_t684137495 *, int32_t, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Action_3_BeginInvoke_m2072042172_gshared)(__this, ___arg10, ___arg21, ___arg32, ___callback3, ___object4, method)
// System.Void System.Action`3<System.Boolean,CombatEntity,System.Int32>::EndInvoke(System.IAsyncResult)
#define Action_3_EndInvoke_m1573014624(__this, ___result0, method) ((  void (*) (Action_3_t4179361545 *, Il2CppObject *, const MethodInfo*))Action_3_EndInvoke_m3568014105_gshared)(__this, ___result0, method)
