﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Converters.XmlNodeWrapper
struct XmlNodeWrapper_t2503940216;
// System.Xml.XmlNode
struct XmlNode_t856910923;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Converters.IXmlNode>
struct IList_1_t749432081;
// Newtonsoft.Json.Converters.IXmlNode
struct IXmlNode_t2349752174;
// System.Xml.XmlAttribute
struct XmlAttribute_t6647939;

#include "codegen/il2cpp-codegen.h"
#include "System_Xml_System_Xml_XmlNode856910923.h"
#include "System_Xml_System_Xml_XmlNodeType992114213.h"
#include "mscorlib_System_String7231557.h"
#include "System_Xml_System_Xml_XmlAttribute6647939.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Converters_XmlNo2503940216.h"

// System.Void Newtonsoft.Json.Converters.XmlNodeWrapper::.ctor(System.Xml.XmlNode)
extern "C"  void XmlNodeWrapper__ctor_m3381972616 (XmlNodeWrapper_t2503940216 * __this, XmlNode_t856910923 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Converters.XmlNodeWrapper::get_WrappedNode()
extern "C"  Il2CppObject * XmlNodeWrapper_get_WrappedNode_m573484220 (XmlNodeWrapper_t2503940216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Xml.XmlNodeType Newtonsoft.Json.Converters.XmlNodeWrapper::get_NodeType()
extern "C"  int32_t XmlNodeWrapper_get_NodeType_m2384883038 (XmlNodeWrapper_t2503940216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeWrapper::get_Name()
extern "C"  String_t* XmlNodeWrapper_get_Name_m4274491722 (XmlNodeWrapper_t2503940216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeWrapper::get_LocalName()
extern "C"  String_t* XmlNodeWrapper_get_LocalName_m745565785 (XmlNodeWrapper_t2503940216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeWrapper::get_ChildNodes()
extern "C"  Il2CppObject* XmlNodeWrapper_get_ChildNodes_m950697247 (XmlNodeWrapper_t2503940216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::WrapNode(System.Xml.XmlNode)
extern "C"  Il2CppObject * XmlNodeWrapper_WrapNode_m4060457797 (XmlNodeWrapper_t2503940216 * __this, XmlNode_t856910923 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeWrapper::get_Attributes()
extern "C"  Il2CppObject* XmlNodeWrapper_get_Attributes_m1363351297 (XmlNodeWrapper_t2503940216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::get_ParentNode()
extern "C"  Il2CppObject * XmlNodeWrapper_get_ParentNode_m2285943185 (XmlNodeWrapper_t2503940216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeWrapper::get_Value()
extern "C"  String_t* XmlNodeWrapper_get_Value_m2169931124 (XmlNodeWrapper_t2503940216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Converters.XmlNodeWrapper::set_Value(System.String)
extern "C"  void XmlNodeWrapper_set_Value_m2628501893 (XmlNodeWrapper_t2503940216 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::AppendChild(Newtonsoft.Json.Converters.IXmlNode)
extern "C"  Il2CppObject * XmlNodeWrapper_AppendChild_m3899530868 (XmlNodeWrapper_t2503940216 * __this, Il2CppObject * ___newChild0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeWrapper::get_Prefix()
extern "C"  String_t* XmlNodeWrapper_get_Prefix_m2965027857 (XmlNodeWrapper_t2503940216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Converters.XmlNodeWrapper::get_NamespaceURI()
extern "C"  String_t* XmlNodeWrapper_get_NamespaceURI_m427172176 (XmlNodeWrapper_t2503940216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::<get_ChildNodes>m__368(System.Xml.XmlNode)
extern "C"  Il2CppObject * XmlNodeWrapper_U3Cget_ChildNodesU3Em__368_m319660581 (XmlNodeWrapper_t2503940216 * __this, XmlNode_t856910923 * ___n0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::<get_Attributes>m__369(System.Xml.XmlAttribute)
extern "C"  Il2CppObject * XmlNodeWrapper_U3Cget_AttributesU3Em__369_m823997840 (XmlNodeWrapper_t2503940216 * __this, XmlAttribute_t6647939 * ___a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Converters.IXmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::ilo_WrapNode1(Newtonsoft.Json.Converters.XmlNodeWrapper,System.Xml.XmlNode)
extern "C"  Il2CppObject * XmlNodeWrapper_ilo_WrapNode1_m2730941193 (Il2CppObject * __this /* static, unused */, XmlNodeWrapper_t2503940216 * ____this0, XmlNode_t856910923 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
