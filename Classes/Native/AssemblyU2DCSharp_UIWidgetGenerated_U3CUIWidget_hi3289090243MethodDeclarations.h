﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIWidgetGenerated/<UIWidget_hitCheck_GetDelegate_member7_arg0>c__AnonStoreyD7
struct U3CUIWidget_hitCheck_GetDelegate_member7_arg0U3Ec__AnonStoreyD7_t3289090243;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UIWidgetGenerated/<UIWidget_hitCheck_GetDelegate_member7_arg0>c__AnonStoreyD7::.ctor()
extern "C"  void U3CUIWidget_hitCheck_GetDelegate_member7_arg0U3Ec__AnonStoreyD7__ctor_m2715665416 (U3CUIWidget_hitCheck_GetDelegate_member7_arg0U3Ec__AnonStoreyD7_t3289090243 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated/<UIWidget_hitCheck_GetDelegate_member7_arg0>c__AnonStoreyD7::<>m__173(UnityEngine.Vector3)
extern "C"  bool U3CUIWidget_hitCheck_GetDelegate_member7_arg0U3Ec__AnonStoreyD7_U3CU3Em__173_m562373903 (U3CUIWidget_hitCheck_GetDelegate_member7_arg0U3Ec__AnonStoreyD7_t3289090243 * __this, Vector3_t4282066566  ___worldPos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
