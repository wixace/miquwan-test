﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Va375146588MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,UIModelDisplay>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m3735325244(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2229818102 *, Dictionary_2_t3529212389 *, const MethodInfo*))ValueCollection__ctor_m1424081231_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,UIModelDisplay>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1791064694(__this, ___item0, method) ((  void (*) (ValueCollection_t2229818102 *, UIModelDisplay_t1730520589 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1162319107_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,UIModelDisplay>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3990032959(__this, method) ((  void (*) (ValueCollection_t2229818102 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3447618380_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,UIModelDisplay>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3976201456(__this, ___item0, method) ((  bool (*) (ValueCollection_t2229818102 *, UIModelDisplay_t1730520589 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3595086663_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,UIModelDisplay>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2364905877(__this, ___item0, method) ((  bool (*) (ValueCollection_t2229818102 *, UIModelDisplay_t1730520589 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m3423725996_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,UIModelDisplay>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m3363642125(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2229818102 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m781163340_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,UIModelDisplay>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m119746691(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2229818102 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m3925634384_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,UIModelDisplay>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1061524670(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2229818102 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m3307136479_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,UIModelDisplay>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2251377315(__this, method) ((  bool (*) (ValueCollection_t2229818102 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1870262522_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,UIModelDisplay>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1299197187(__this, method) ((  bool (*) (ValueCollection_t2229818102 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2926992858_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,UIModelDisplay>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m792549615(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2229818102 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2796159372_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,UIModelDisplay>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m101031811(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2229818102 *, UIModelDisplayU5BU5D_t2767931168*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m512190934_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,UIModelDisplay>::GetEnumerator()
#define ValueCollection_GetEnumerator_m1884506086(__this, method) ((  Enumerator_t1461045797  (*) (ValueCollection_t2229818102 *, const MethodInfo*))ValueCollection_GetEnumerator_m1457321599_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<UIModelDisplayType,UIModelDisplay>::get_Count()
#define ValueCollection_get_Count_m822225865(__this, method) ((  int32_t (*) (ValueCollection_t2229818102 *, const MethodInfo*))ValueCollection_get_Count_m1377673108_gshared)(__this, method)
