﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AnimatedWidget
struct AnimatedWidget_t1642214887;
// UIWidget
struct UIWidget_t769069560;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"

// System.Void AnimatedWidget::.ctor()
extern "C"  void AnimatedWidget__ctor_m2770619028 (AnimatedWidget_t1642214887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatedWidget::OnEnable()
extern "C"  void AnimatedWidget_OnEnable_m901547122 (AnimatedWidget_t1642214887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatedWidget::LateUpdate()
extern "C"  void AnimatedWidget_LateUpdate_m2908519487 (AnimatedWidget_t1642214887 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AnimatedWidget::ilo_set_height1(UIWidget,System.Int32)
extern "C"  void AnimatedWidget_ilo_set_height1_m4177963587 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
