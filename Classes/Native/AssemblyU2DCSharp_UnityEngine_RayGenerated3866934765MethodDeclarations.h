﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_RayGenerated
struct UnityEngine_RayGenerated_t3866934765;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine_RayGenerated::.ctor()
extern "C"  void UnityEngine_RayGenerated__ctor_m2261199694 (UnityEngine_RayGenerated_t3866934765 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RayGenerated::.cctor()
extern "C"  void UnityEngine_RayGenerated__cctor_m895617567 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RayGenerated::Ray_Ray1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RayGenerated_Ray_Ray1_m3159754582 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RayGenerated::Ray_Ray2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RayGenerated_Ray_Ray2_m109551767 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RayGenerated::Ray_origin(JSVCall)
extern "C"  void UnityEngine_RayGenerated_Ray_origin_m226321910 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RayGenerated::Ray_direction(JSVCall)
extern "C"  void UnityEngine_RayGenerated_Ray_direction_m1967370321 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RayGenerated::Ray_GetPoint__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RayGenerated_Ray_GetPoint__Single_m3637050609 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RayGenerated::Ray_ToString__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RayGenerated_Ray_ToString__String_m3272800012 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RayGenerated::Ray_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RayGenerated_Ray_ToString_m1968841659 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RayGenerated::__Register()
extern "C"  void UnityEngine_RayGenerated___Register_m3904320057 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RayGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_RayGenerated_ilo_getObject1_m2203075204 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RayGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_RayGenerated_ilo_attachFinalizerObject2_m1585095826 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_RayGenerated::ilo_getVector3S3(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_RayGenerated_ilo_getVector3S3_m786374190 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RayGenerated::ilo_addJSCSRel4(System.Int32,System.Object)
extern "C"  void UnityEngine_RayGenerated_ilo_addJSCSRel4_m3866058893 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RayGenerated::ilo_setVector3S5(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_RayGenerated_ilo_setVector3S5_m33228093 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RayGenerated::ilo_setStringS6(System.Int32,System.String)
extern "C"  void UnityEngine_RayGenerated_ilo_setStringS6_m1232594332 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
