﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginYuLe
struct PluginYuLe_t1606594472;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.String
struct String_t;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// VersionInfo
struct VersionInfo_t2356638086;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Mihua.SDK.EnterGameInfo
struct EnterGameInfo_t1897532954;
// System.Object
struct Il2CppObject;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginYuLe1606594472.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginYuLe::.ctor()
extern "C"  void PluginYuLe__ctor_m1523582451 (PluginYuLe_t1606594472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuLe::Init()
extern "C"  void PluginYuLe_Init_m3325820417 (PluginYuLe_t1606594472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginYuLe::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginYuLe_ReqSDKHttpLogin_m2333677320 (PluginYuLe_t1606594472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYuLe::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginYuLe_IsLoginSuccess_m3692565760 (PluginYuLe_t1606594472 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuLe::OpenUserLogin()
extern "C"  void PluginYuLe_OpenUserLogin_m3618211141 (PluginYuLe_t1606594472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuLe::UserPay(System.String)
extern "C"  void PluginYuLe_UserPay_m631542036 (PluginYuLe_t1606594472 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuLe::UserPay(CEvent.ZEvent)
extern "C"  void PluginYuLe_UserPay_m3113977485 (PluginYuLe_t1606594472 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuLe::EnterGame(CEvent.ZEvent)
extern "C"  void PluginYuLe_EnterGame_m1156704480 (PluginYuLe_t1606594472 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuLe::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginYuLe_RoleUpgrade_m3826905988 (PluginYuLe_t1606594472 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuLe::OnLoginSuccess(System.String)
extern "C"  void PluginYuLe_OnLoginSuccess_m1887483064 (PluginYuLe_t1606594472 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuLe::OnLogoutSuccess(System.String)
extern "C"  void PluginYuLe_OnLogoutSuccess_m2680942807 (PluginYuLe_t1606594472 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuLe::OnLogout(System.String)
extern "C"  void PluginYuLe_OnLogout_m3782910536 (PluginYuLe_t1606594472 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuLe::initSdk()
extern "C"  void PluginYuLe_initSdk_m1248063931 (PluginYuLe_t1606594472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuLe::login()
extern "C"  void PluginYuLe_login_m1045597274 (PluginYuLe_t1606594472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuLe::logout()
extern "C"  void PluginYuLe_logout_m2354567131 (PluginYuLe_t1606594472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuLe::userInfo(System.String,System.String,System.String,System.String)
extern "C"  void PluginYuLe_userInfo_m621954380 (PluginYuLe_t1606594472 * __this, String_t* ___serverid0, String_t* ___servername1, String_t* ___roleid2, String_t* ___rolename3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuLe::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYuLe_pay_m1215419825 (PluginYuLe_t1606594472 * __this, String_t* ___YULESDKAppleIAPWithUserId0, String_t* ___withServerID1, String_t* ___withRoleId2, String_t* ___withRoleName3, String_t* ___withAmount4, String_t* ___withProduct5, String_t* ___withInfo6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuLe::<OnLogoutSuccess>m__47C()
extern "C"  void PluginYuLe_U3COnLogoutSuccessU3Em__47C_m2475985092 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuLe::<OnLogout>m__47D()
extern "C"  void PluginYuLe_U3COnLogoutU3Em__47D_m3092295934 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginYuLe::ilo_get_currentVS1(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginYuLe_ilo_get_currentVS1_m2171181845 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuLe::ilo_initSdk2(PluginYuLe)
extern "C"  void PluginYuLe_ilo_initSdk2_m603585348 (Il2CppObject * __this /* static, unused */, PluginYuLe_t1606594472 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginYuLe::ilo_get_DeviceID3(PluginsSdkMgr)
extern "C"  String_t* PluginYuLe_ilo_get_DeviceID3_m4003340384 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginYuLe::ilo_get_Instance4()
extern "C"  VersionMgr_t1322950208 * PluginYuLe_ilo_get_Instance4_m1217322679 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.PayInfo PluginYuLe::ilo_Parse5(System.Object[])
extern "C"  PayInfo_t1775308120 * PluginYuLe_ilo_Parse5_m1332990213 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___payOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.EnterGameInfo PluginYuLe::ilo_Parse6(System.Object[])
extern "C"  EnterGameInfo_t1897532954 * PluginYuLe_ilo_Parse6_m2786873894 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuLe::ilo_Log7(System.Object,System.Boolean)
extern "C"  void PluginYuLe_ilo_Log7_m225907840 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuLe::ilo_logout8(PluginYuLe)
extern "C"  void PluginYuLe_ilo_logout8_m621496420 (Il2CppObject * __this /* static, unused */, PluginYuLe_t1606594472 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuLe::ilo_Logout9(System.Action)
extern "C"  void PluginYuLe_ilo_Logout9_m3229812784 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYuLe::ilo_ReqSDKHttpLogin10(PluginsSdkMgr)
extern "C"  void PluginYuLe_ilo_ReqSDKHttpLogin10_m1271067474 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
