﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// NavMeshRenderer
struct  NavMeshRenderer_t3558381171  : public MonoBehaviour_t667441552
{
public:
	// System.String NavMeshRenderer::lastLevel
	String_t* ___lastLevel_2;

public:
	inline static int32_t get_offset_of_lastLevel_2() { return static_cast<int32_t>(offsetof(NavMeshRenderer_t3558381171, ___lastLevel_2)); }
	inline String_t* get_lastLevel_2() const { return ___lastLevel_2; }
	inline String_t** get_address_of_lastLevel_2() { return &___lastLevel_2; }
	inline void set_lastLevel_2(String_t* value)
	{
		___lastLevel_2 = value;
		Il2CppCodeGenWriteBarrier(&___lastLevel_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
