﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UILoadScene
struct UILoadScene_t3799334674;

#include "codegen/il2cpp-codegen.h"

// System.Void UILoadScene::.ctor()
extern "C"  void UILoadScene__ctor_m2757169305 (UILoadScene_t3799334674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILoadScene::Start()
extern "C"  void UILoadScene_Start_m1704307097 (UILoadScene_t3799334674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UILoadScene::<Start>m__48C()
extern "C"  void UILoadScene_U3CStartU3Em__48C_m3530029281 (UILoadScene_t3799334674 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
