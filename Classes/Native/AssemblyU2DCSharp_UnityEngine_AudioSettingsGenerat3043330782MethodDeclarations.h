﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AudioSettingsGenerated
struct UnityEngine_AudioSettingsGenerated_t3043330782;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_AudioSettingsGenerated::.ctor()
extern "C"  void UnityEngine_AudioSettingsGenerated__ctor_m755539837 (UnityEngine_AudioSettingsGenerated_t3043330782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSettingsGenerated::AudioSettings_AudioSettings1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSettingsGenerated_AudioSettings_AudioSettings1_m2771920553 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSettingsGenerated::AudioSettings_driverCapabilities(JSVCall)
extern "C"  void UnityEngine_AudioSettingsGenerated_AudioSettings_driverCapabilities_m2222290748 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSettingsGenerated::AudioSettings_speakerMode(JSVCall)
extern "C"  void UnityEngine_AudioSettingsGenerated_AudioSettings_speakerMode_m1623834640 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSettingsGenerated::AudioSettings_dspTime(JSVCall)
extern "C"  void UnityEngine_AudioSettingsGenerated_AudioSettings_dspTime_m707412388 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSettingsGenerated::AudioSettings_outputSampleRate(JSVCall)
extern "C"  void UnityEngine_AudioSettingsGenerated_AudioSettings_outputSampleRate_m1620678991 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSettingsGenerated::AudioSettings_GetConfiguration(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSettingsGenerated_AudioSettings_GetConfiguration_m2603863217 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSettingsGenerated::AudioSettings_GetDSPBufferSize__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSettingsGenerated_AudioSettings_GetDSPBufferSize__Int32__Int32_m1627883261 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioSettingsGenerated::AudioSettings_Reset__AudioConfiguration(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioSettingsGenerated_AudioSettings_Reset__AudioConfiguration_m3218854400 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSettingsGenerated::__Register()
extern "C"  void UnityEngine_AudioSettingsGenerated___Register_m1165099882 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSettingsGenerated::ilo_setDouble1(System.Int32,System.Double)
extern "C"  void UnityEngine_AudioSettingsGenerated_ilo_setDouble1_m3735083653 (Il2CppObject * __this /* static, unused */, int32_t ___e0, double ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioSettingsGenerated::ilo_setInt322(System.Int32,System.Int32)
extern "C"  void UnityEngine_AudioSettingsGenerated_ilo_setInt322_m1405787992 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AudioSettingsGenerated::ilo_incArgIndex3()
extern "C"  int32_t UnityEngine_AudioSettingsGenerated_ilo_incArgIndex3_m927598753 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_AudioSettingsGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_AudioSettingsGenerated_ilo_getObject4_m3745682581 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
