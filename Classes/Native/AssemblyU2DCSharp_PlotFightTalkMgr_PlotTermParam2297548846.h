﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// CombatEntity
struct CombatEntity_t684137495;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PlotFightTalkMgr_TestingType3614572920.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlotFightTalkMgr/PlotTermParam
struct  PlotTermParam_t2297548846  : public Il2CppObject
{
public:
	// PlotFightTalkMgr/TestingType PlotFightTalkMgr/PlotTermParam::type
	int32_t ___type_0;
	// System.Int32 PlotFightTalkMgr/PlotTermParam::sceneId
	int32_t ___sceneId_1;
	// System.Collections.Generic.List`1<System.Int32> PlotFightTalkMgr/PlotTermParam::activeNpcs
	List_1_t2522024052 * ___activeNpcs_2;
	// System.Collections.Generic.List`1<System.Int32> PlotFightTalkMgr/PlotTermParam::teamHeros
	List_1_t2522024052 * ___teamHeros_3;
	// System.Int32 PlotFightTalkMgr/PlotTermParam::dieNpcId
	int32_t ___dieNpcId_4;
	// System.Int32 PlotFightTalkMgr/PlotTermParam::storyId
	int32_t ___storyId_5;
	// System.Int32 PlotFightTalkMgr/PlotTermParam::skillId
	int32_t ___skillId_6;
	// System.Int32 PlotFightTalkMgr/PlotTermParam::userId
	int32_t ___userId_7;
	// System.Int32 PlotFightTalkMgr/PlotTermParam::aiId
	int32_t ___aiId_8;
	// CombatEntity PlotFightTalkMgr/PlotTermParam::npc
	CombatEntity_t684137495 * ___npc_9;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(PlotTermParam_t2297548846, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_sceneId_1() { return static_cast<int32_t>(offsetof(PlotTermParam_t2297548846, ___sceneId_1)); }
	inline int32_t get_sceneId_1() const { return ___sceneId_1; }
	inline int32_t* get_address_of_sceneId_1() { return &___sceneId_1; }
	inline void set_sceneId_1(int32_t value)
	{
		___sceneId_1 = value;
	}

	inline static int32_t get_offset_of_activeNpcs_2() { return static_cast<int32_t>(offsetof(PlotTermParam_t2297548846, ___activeNpcs_2)); }
	inline List_1_t2522024052 * get_activeNpcs_2() const { return ___activeNpcs_2; }
	inline List_1_t2522024052 ** get_address_of_activeNpcs_2() { return &___activeNpcs_2; }
	inline void set_activeNpcs_2(List_1_t2522024052 * value)
	{
		___activeNpcs_2 = value;
		Il2CppCodeGenWriteBarrier(&___activeNpcs_2, value);
	}

	inline static int32_t get_offset_of_teamHeros_3() { return static_cast<int32_t>(offsetof(PlotTermParam_t2297548846, ___teamHeros_3)); }
	inline List_1_t2522024052 * get_teamHeros_3() const { return ___teamHeros_3; }
	inline List_1_t2522024052 ** get_address_of_teamHeros_3() { return &___teamHeros_3; }
	inline void set_teamHeros_3(List_1_t2522024052 * value)
	{
		___teamHeros_3 = value;
		Il2CppCodeGenWriteBarrier(&___teamHeros_3, value);
	}

	inline static int32_t get_offset_of_dieNpcId_4() { return static_cast<int32_t>(offsetof(PlotTermParam_t2297548846, ___dieNpcId_4)); }
	inline int32_t get_dieNpcId_4() const { return ___dieNpcId_4; }
	inline int32_t* get_address_of_dieNpcId_4() { return &___dieNpcId_4; }
	inline void set_dieNpcId_4(int32_t value)
	{
		___dieNpcId_4 = value;
	}

	inline static int32_t get_offset_of_storyId_5() { return static_cast<int32_t>(offsetof(PlotTermParam_t2297548846, ___storyId_5)); }
	inline int32_t get_storyId_5() const { return ___storyId_5; }
	inline int32_t* get_address_of_storyId_5() { return &___storyId_5; }
	inline void set_storyId_5(int32_t value)
	{
		___storyId_5 = value;
	}

	inline static int32_t get_offset_of_skillId_6() { return static_cast<int32_t>(offsetof(PlotTermParam_t2297548846, ___skillId_6)); }
	inline int32_t get_skillId_6() const { return ___skillId_6; }
	inline int32_t* get_address_of_skillId_6() { return &___skillId_6; }
	inline void set_skillId_6(int32_t value)
	{
		___skillId_6 = value;
	}

	inline static int32_t get_offset_of_userId_7() { return static_cast<int32_t>(offsetof(PlotTermParam_t2297548846, ___userId_7)); }
	inline int32_t get_userId_7() const { return ___userId_7; }
	inline int32_t* get_address_of_userId_7() { return &___userId_7; }
	inline void set_userId_7(int32_t value)
	{
		___userId_7 = value;
	}

	inline static int32_t get_offset_of_aiId_8() { return static_cast<int32_t>(offsetof(PlotTermParam_t2297548846, ___aiId_8)); }
	inline int32_t get_aiId_8() const { return ___aiId_8; }
	inline int32_t* get_address_of_aiId_8() { return &___aiId_8; }
	inline void set_aiId_8(int32_t value)
	{
		___aiId_8 = value;
	}

	inline static int32_t get_offset_of_npc_9() { return static_cast<int32_t>(offsetof(PlotTermParam_t2297548846, ___npc_9)); }
	inline CombatEntity_t684137495 * get_npc_9() const { return ___npc_9; }
	inline CombatEntity_t684137495 ** get_address_of_npc_9() { return &___npc_9; }
	inline void set_npc_9(CombatEntity_t684137495 * value)
	{
		___npc_9 = value;
		Il2CppCodeGenWriteBarrier(&___npc_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
