﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ConstructorID
struct ConstructorID_t3348888181;
// PropertyID
struct PropertyID_t1067426256;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Singleton77Generated
struct  Singleton77Generated_t261572036  : public Il2CppObject
{
public:

public:
};

struct Singleton77Generated_t261572036_StaticFields
{
public:
	// ConstructorID Singleton77Generated::constructorID0
	ConstructorID_t3348888181 * ___constructorID0_0;
	// PropertyID Singleton77Generated::propertyID0
	PropertyID_t1067426256 * ___propertyID0_1;

public:
	inline static int32_t get_offset_of_constructorID0_0() { return static_cast<int32_t>(offsetof(Singleton77Generated_t261572036_StaticFields, ___constructorID0_0)); }
	inline ConstructorID_t3348888181 * get_constructorID0_0() const { return ___constructorID0_0; }
	inline ConstructorID_t3348888181 ** get_address_of_constructorID0_0() { return &___constructorID0_0; }
	inline void set_constructorID0_0(ConstructorID_t3348888181 * value)
	{
		___constructorID0_0 = value;
		Il2CppCodeGenWriteBarrier(&___constructorID0_0, value);
	}

	inline static int32_t get_offset_of_propertyID0_1() { return static_cast<int32_t>(offsetof(Singleton77Generated_t261572036_StaticFields, ___propertyID0_1)); }
	inline PropertyID_t1067426256 * get_propertyID0_1() const { return ___propertyID0_1; }
	inline PropertyID_t1067426256 ** get_address_of_propertyID0_1() { return &___propertyID0_1; }
	inline void set_propertyID0_1(PropertyID_t1067426256 * value)
	{
		___propertyID0_1 = value;
		Il2CppCodeGenWriteBarrier(&___propertyID0_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
