﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._1905588120be6b1119db69d0c5eaaa0c
struct _1905588120be6b1119db69d0c5eaaa0c_t1065843205;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__1905588120be6b1119db69d01065843205.h"

// System.Void Little._1905588120be6b1119db69d0c5eaaa0c::.ctor()
extern "C"  void _1905588120be6b1119db69d0c5eaaa0c__ctor_m3379831240 (_1905588120be6b1119db69d0c5eaaa0c_t1065843205 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1905588120be6b1119db69d0c5eaaa0c::_1905588120be6b1119db69d0c5eaaa0cm2(System.Int32)
extern "C"  int32_t _1905588120be6b1119db69d0c5eaaa0c__1905588120be6b1119db69d0c5eaaa0cm2_m1762208633 (_1905588120be6b1119db69d0c5eaaa0c_t1065843205 * __this, int32_t ____1905588120be6b1119db69d0c5eaaa0ca0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1905588120be6b1119db69d0c5eaaa0c::_1905588120be6b1119db69d0c5eaaa0cm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _1905588120be6b1119db69d0c5eaaa0c__1905588120be6b1119db69d0c5eaaa0cm_m1203918685 (_1905588120be6b1119db69d0c5eaaa0c_t1065843205 * __this, int32_t ____1905588120be6b1119db69d0c5eaaa0ca0, int32_t ____1905588120be6b1119db69d0c5eaaa0c701, int32_t ____1905588120be6b1119db69d0c5eaaa0cc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1905588120be6b1119db69d0c5eaaa0c::ilo__1905588120be6b1119db69d0c5eaaa0cm21(Little._1905588120be6b1119db69d0c5eaaa0c,System.Int32)
extern "C"  int32_t _1905588120be6b1119db69d0c5eaaa0c_ilo__1905588120be6b1119db69d0c5eaaa0cm21_m2400186220 (Il2CppObject * __this /* static, unused */, _1905588120be6b1119db69d0c5eaaa0c_t1065843205 * ____this0, int32_t ____1905588120be6b1119db69d0c5eaaa0ca1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
