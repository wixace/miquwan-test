﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.LineRenderer
struct LineRenderer_t1892709339;

#include "AssemblyU2DCSharp_IEffComponent3802264961.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ActorLine
struct  ActorLine_t2375805801  : public IEffComponent_t3802264961
{
public:
	// UnityEngine.Transform ActorLine::host
	Transform_t1659122786 * ___host_1;
	// UnityEngine.Transform ActorLine::targetTr
	Transform_t1659122786 * ___targetTr_2;
	// System.Single ActorLine::lifetime
	float ___lifetime_3;
	// System.Single ActorLine::startTime
	float ___startTime_4;
	// UnityEngine.LineRenderer ActorLine::lineRenderer
	LineRenderer_t1892709339 * ___lineRenderer_5;

public:
	inline static int32_t get_offset_of_host_1() { return static_cast<int32_t>(offsetof(ActorLine_t2375805801, ___host_1)); }
	inline Transform_t1659122786 * get_host_1() const { return ___host_1; }
	inline Transform_t1659122786 ** get_address_of_host_1() { return &___host_1; }
	inline void set_host_1(Transform_t1659122786 * value)
	{
		___host_1 = value;
		Il2CppCodeGenWriteBarrier(&___host_1, value);
	}

	inline static int32_t get_offset_of_targetTr_2() { return static_cast<int32_t>(offsetof(ActorLine_t2375805801, ___targetTr_2)); }
	inline Transform_t1659122786 * get_targetTr_2() const { return ___targetTr_2; }
	inline Transform_t1659122786 ** get_address_of_targetTr_2() { return &___targetTr_2; }
	inline void set_targetTr_2(Transform_t1659122786 * value)
	{
		___targetTr_2 = value;
		Il2CppCodeGenWriteBarrier(&___targetTr_2, value);
	}

	inline static int32_t get_offset_of_lifetime_3() { return static_cast<int32_t>(offsetof(ActorLine_t2375805801, ___lifetime_3)); }
	inline float get_lifetime_3() const { return ___lifetime_3; }
	inline float* get_address_of_lifetime_3() { return &___lifetime_3; }
	inline void set_lifetime_3(float value)
	{
		___lifetime_3 = value;
	}

	inline static int32_t get_offset_of_startTime_4() { return static_cast<int32_t>(offsetof(ActorLine_t2375805801, ___startTime_4)); }
	inline float get_startTime_4() const { return ___startTime_4; }
	inline float* get_address_of_startTime_4() { return &___startTime_4; }
	inline void set_startTime_4(float value)
	{
		___startTime_4 = value;
	}

	inline static int32_t get_offset_of_lineRenderer_5() { return static_cast<int32_t>(offsetof(ActorLine_t2375805801, ___lineRenderer_5)); }
	inline LineRenderer_t1892709339 * get_lineRenderer_5() const { return ___lineRenderer_5; }
	inline LineRenderer_t1892709339 ** get_address_of_lineRenderer_5() { return &___lineRenderer_5; }
	inline void set_lineRenderer_5(LineRenderer_t1892709339 * value)
	{
		___lineRenderer_5 = value;
		Il2CppCodeGenWriteBarrier(&___lineRenderer_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
