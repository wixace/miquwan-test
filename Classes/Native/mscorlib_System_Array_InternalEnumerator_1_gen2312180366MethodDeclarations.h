﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2312180366.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_RenderBuffer3529837690.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.RenderBuffer>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2789032830_gshared (InternalEnumerator_1_t2312180366 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2789032830(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2312180366 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2789032830_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RenderBuffer>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m934516066_gshared (InternalEnumerator_1_t2312180366 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m934516066(__this, method) ((  void (*) (InternalEnumerator_1_t2312180366 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m934516066_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RenderBuffer>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3664495886_gshared (InternalEnumerator_1_t2312180366 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3664495886(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2312180366 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3664495886_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RenderBuffer>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3245568725_gshared (InternalEnumerator_1_t2312180366 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3245568725(__this, method) ((  void (*) (InternalEnumerator_1_t2312180366 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3245568725_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RenderBuffer>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2834181198_gshared (InternalEnumerator_1_t2312180366 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2834181198(__this, method) ((  bool (*) (InternalEnumerator_1_t2312180366 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2834181198_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.RenderBuffer>::get_Current()
extern "C"  RenderBuffer_t3529837690  InternalEnumerator_1_get_Current_m718952325_gshared (InternalEnumerator_1_t2312180366 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m718952325(__this, method) ((  RenderBuffer_t3529837690  (*) (InternalEnumerator_1_t2312180366 *, const MethodInfo*))InternalEnumerator_1_get_Current_m718952325_gshared)(__this, method)
