﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2594088108MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m2179140860(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2009986174 *, Dictionary_2_t383226723 *, const MethodInfo*))KeyCollection__ctor_m2069746375_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2555715290(__this, ___item0, method) ((  void (*) (KeyCollection_t2009986174 *, Int2_t1974045593 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3124930095_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1026110161(__this, method) ((  void (*) (KeyCollection_t2009986174 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m302680934_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2080112(__this, ___item0, method) ((  bool (*) (KeyCollection_t2009986174 *, Int2_t1974045593 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m163836927_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4112304853(__this, ___item0, method) ((  bool (*) (KeyCollection_t2009986174 *, Int2_t1974045593 , const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2284542244_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2133131021(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2009986174 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2963816760_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m3628623299(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2009986174 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1076397784_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2843257086(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2009986174 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m4286215783_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m585078993(__this, method) ((  bool (*) (KeyCollection_t2009986174 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1414555552_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m980566723(__this, method) ((  bool (*) (KeyCollection_t2009986174 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m635533394_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3633726255(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2009986174 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m3949889412_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m3194184625(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2009986174 *, Int2U5BU5D_t30096868*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m1959794044_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::GetEnumerator()
#define KeyCollection_GetEnumerator_m3804870548(__this, method) ((  Enumerator_t998162777  (*) (KeyCollection_t2009986174 *, const MethodInfo*))KeyCollection_GetEnumerator_m4101563209_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<Pathfinding.Int2,ProceduralWorld/ProceduralTile>::get_Count()
#define KeyCollection_get_Count_m2419964041(__this, method) ((  int32_t (*) (KeyCollection_t2009986174 *, const MethodInfo*))KeyCollection_get_Count_m2937548556_gshared)(__this, method)
