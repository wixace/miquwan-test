﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E3480326721MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m2595883968(__this, ___dictionary0, method) ((  void (*) (Enumerator_t2130705895 *, Dictionary_2_t813382503 *, const MethodInfo*))Enumerator__ctor_m2292393966_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1048863201(__this, method) ((  Il2CppObject * (*) (Enumerator_t2130705895 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m2350395251_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m2268048629(__this, method) ((  void (*) (Enumerator_t2130705895 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1288278023_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2866246590(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t2130705895 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m290819920_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3554734589(__this, method) ((  Il2CppObject * (*) (Enumerator_t2130705895 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4025797135_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2649376079(__this, method) ((  Il2CppObject * (*) (Enumerator_t2130705895 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m73949409_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::MoveNext()
#define Enumerator_MoveNext_m514208097(__this, method) ((  bool (*) (Enumerator_t2130705895 *, const MethodInfo*))Enumerator_MoveNext_m3325100915_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_Current()
#define Enumerator_get_Current_m2781856559(__this, method) ((  KeyValuePair_2_t712163209  (*) (Enumerator_t2130705895 *, const MethodInfo*))Enumerator_get_Current_m3112427485_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m1386443374(__this, method) ((  String_t* (*) (Enumerator_t2130705895 *, const MethodInfo*))Enumerator_get_CurrentKey_m1064890112_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1896699282(__this, method) ((  KeyValuePair_2_t4287931429  (*) (Enumerator_t2130705895 *, const MethodInfo*))Enumerator_get_CurrentValue_m2121659812_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::Reset()
#define Enumerator_Reset_m3591964178(__this, method) ((  void (*) (Enumerator_t2130705895 *, const MethodInfo*))Enumerator_Reset_m1469300032_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::VerifyState()
#define Enumerator_VerifyState_m1862271131(__this, method) ((  void (*) (Enumerator_t2130705895 *, const MethodInfo*))Enumerator_VerifyState_m2192842057_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m1386066243(__this, method) ((  void (*) (Enumerator_t2130705895 *, const MethodInfo*))Enumerator_VerifyCurrent_m1237146225_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.UInt64,System.Int32>>::Dispose()
#define Enumerator_Dispose_m1247298146(__this, method) ((  void (*) (Enumerator_t2130705895 *, const MethodInfo*))Enumerator_Dispose_m1476519440_gshared)(__this, method)
