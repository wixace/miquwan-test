﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/JSDelayFun1
struct JSDelayFun1_t1074711452;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void BehaviourUtil/JSDelayFun1::.ctor(System.Object,System.IntPtr)
extern "C"  void JSDelayFun1__ctor_m600963443 (JSDelayFun1_t1074711452 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/JSDelayFun1::Invoke(System.Object)
extern "C"  void JSDelayFun1_Invoke_m1980571431 (JSDelayFun1_t1074711452 * __this, Il2CppObject * ___arg10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult BehaviourUtil/JSDelayFun1::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * JSDelayFun1_BeginInvoke_m1084999632 (JSDelayFun1_t1074711452 * __this, Il2CppObject * ___arg10, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/JSDelayFun1::EndInvoke(System.IAsyncResult)
extern "C"  void JSDelayFun1_EndInvoke_m2116689155 (JSDelayFun1_t1074711452 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
