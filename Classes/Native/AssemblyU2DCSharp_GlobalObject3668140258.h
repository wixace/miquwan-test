﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalObject
struct  GlobalObject_t3668140258  : public Il2CppObject
{
public:

public:
};

struct GlobalObject_t3668140258_StaticFields
{
public:
	// UnityEngine.GameObject GlobalObject::globalObject
	GameObject_t3674682005 * ___globalObject_0;

public:
	inline static int32_t get_offset_of_globalObject_0() { return static_cast<int32_t>(offsetof(GlobalObject_t3668140258_StaticFields, ___globalObject_0)); }
	inline GameObject_t3674682005 * get_globalObject_0() const { return ___globalObject_0; }
	inline GameObject_t3674682005 ** get_address_of_globalObject_0() { return &___globalObject_0; }
	inline void set_globalObject_0(GameObject_t3674682005 * value)
	{
		___globalObject_0 = value;
		Il2CppCodeGenWriteBarrier(&___globalObject_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
