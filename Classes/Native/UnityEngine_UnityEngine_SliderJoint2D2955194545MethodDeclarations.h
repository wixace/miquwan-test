﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.SliderJoint2D
struct SliderJoint2D_t2955194545;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_JointMotor2D682576033.h"
#include "UnityEngine_UnityEngine_JointTranslationLimits2D2599234005.h"
#include "UnityEngine_UnityEngine_JointLimitState2D3271314440.h"
#include "UnityEngine_UnityEngine_SliderJoint2D2955194545.h"

// System.Void UnityEngine.SliderJoint2D::.ctor()
extern "C"  void SliderJoint2D__ctor_m177657854 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SliderJoint2D::get_autoConfigureAngle()
extern "C"  bool SliderJoint2D_get_autoConfigureAngle_m4042937573 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SliderJoint2D::set_autoConfigureAngle(System.Boolean)
extern "C"  void SliderJoint2D_set_autoConfigureAngle_m16111414 (SliderJoint2D_t2955194545 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.SliderJoint2D::get_angle()
extern "C"  float SliderJoint2D_get_angle_m2311873010 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SliderJoint2D::set_angle(System.Single)
extern "C"  void SliderJoint2D_set_angle_m1872959993 (SliderJoint2D_t2955194545 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SliderJoint2D::get_useMotor()
extern "C"  bool SliderJoint2D_get_useMotor_m4239753335 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SliderJoint2D::set_useMotor(System.Boolean)
extern "C"  void SliderJoint2D_set_useMotor_m3725243336 (SliderJoint2D_t2955194545 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.SliderJoint2D::get_useLimits()
extern "C"  bool SliderJoint2D_get_useLimits_m3899623160 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SliderJoint2D::set_useLimits(System.Boolean)
extern "C"  void SliderJoint2D_set_useLimits_m1328927637 (SliderJoint2D_t2955194545 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointMotor2D UnityEngine.SliderJoint2D::get_motor()
extern "C"  JointMotor2D_t682576033  SliderJoint2D_get_motor_m543999139 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SliderJoint2D::set_motor(UnityEngine.JointMotor2D)
extern "C"  void SliderJoint2D_set_motor_m3207411008 (SliderJoint2D_t2955194545 * __this, JointMotor2D_t682576033  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SliderJoint2D::INTERNAL_get_motor(UnityEngine.JointMotor2D&)
extern "C"  void SliderJoint2D_INTERNAL_get_motor_m2924520624 (SliderJoint2D_t2955194545 * __this, JointMotor2D_t682576033 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SliderJoint2D::INTERNAL_set_motor(UnityEngine.JointMotor2D&)
extern "C"  void SliderJoint2D_INTERNAL_set_motor_m558319908 (SliderJoint2D_t2955194545 * __this, JointMotor2D_t682576033 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointTranslationLimits2D UnityEngine.SliderJoint2D::get_limits()
extern "C"  JointTranslationLimits2D_t2599234005  SliderJoint2D_get_limits_m1555470976 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SliderJoint2D::set_limits(UnityEngine.JointTranslationLimits2D)
extern "C"  void SliderJoint2D_set_limits_m2283242867 (SliderJoint2D_t2955194545 * __this, JointTranslationLimits2D_t2599234005  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SliderJoint2D::INTERNAL_get_limits(UnityEngine.JointTranslationLimits2D&)
extern "C"  void SliderJoint2D_INTERNAL_get_limits_m3170078997 (SliderJoint2D_t2955194545 * __this, JointTranslationLimits2D_t2599234005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SliderJoint2D::INTERNAL_set_limits(UnityEngine.JointTranslationLimits2D&)
extern "C"  void SliderJoint2D_INTERNAL_set_limits_m3076966689 (SliderJoint2D_t2955194545 * __this, JointTranslationLimits2D_t2599234005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointLimitState2D UnityEngine.SliderJoint2D::get_limitState()
extern "C"  int32_t SliderJoint2D_get_limitState_m1291794381 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.SliderJoint2D::get_referenceAngle()
extern "C"  float SliderJoint2D_get_referenceAngle_m539557931 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.SliderJoint2D::get_jointTranslation()
extern "C"  float SliderJoint2D_get_jointTranslation_m4164567914 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.SliderJoint2D::get_jointSpeed()
extern "C"  float SliderJoint2D_get_jointSpeed_m1002478976 (SliderJoint2D_t2955194545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.SliderJoint2D::GetMotorForce(System.Single)
extern "C"  float SliderJoint2D_GetMotorForce_m3044414359 (SliderJoint2D_t2955194545 * __this, float ___timeStep0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.SliderJoint2D::INTERNAL_CALL_GetMotorForce(UnityEngine.SliderJoint2D,System.Single)
extern "C"  float SliderJoint2D_INTERNAL_CALL_GetMotorForce_m2702916190 (Il2CppObject * __this /* static, unused */, SliderJoint2D_t2955194545 * ___self0, float ___timeStep1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
