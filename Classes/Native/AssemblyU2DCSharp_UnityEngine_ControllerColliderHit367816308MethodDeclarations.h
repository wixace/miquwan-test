﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ControllerColliderHitGenerated
struct UnityEngine_ControllerColliderHitGenerated_t367816308;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_ControllerColliderHitGenerated::.ctor()
extern "C"  void UnityEngine_ControllerColliderHitGenerated__ctor_m859557031 (UnityEngine_ControllerColliderHitGenerated_t367816308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ControllerColliderHitGenerated::ControllerColliderHit_ControllerColliderHit1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ControllerColliderHitGenerated_ControllerColliderHit_ControllerColliderHit1_m1450143467 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ControllerColliderHitGenerated::ControllerColliderHit_controller(JSVCall)
extern "C"  void UnityEngine_ControllerColliderHitGenerated_ControllerColliderHit_controller_m2233359538 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ControllerColliderHitGenerated::ControllerColliderHit_collider(JSVCall)
extern "C"  void UnityEngine_ControllerColliderHitGenerated_ControllerColliderHit_collider_m2085076314 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ControllerColliderHitGenerated::ControllerColliderHit_rigidbody(JSVCall)
extern "C"  void UnityEngine_ControllerColliderHitGenerated_ControllerColliderHit_rigidbody_m2368878993 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ControllerColliderHitGenerated::ControllerColliderHit_gameObject(JSVCall)
extern "C"  void UnityEngine_ControllerColliderHitGenerated_ControllerColliderHit_gameObject_m1581043229 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ControllerColliderHitGenerated::ControllerColliderHit_transform(JSVCall)
extern "C"  void UnityEngine_ControllerColliderHitGenerated_ControllerColliderHit_transform_m189271890 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ControllerColliderHitGenerated::ControllerColliderHit_point(JSVCall)
extern "C"  void UnityEngine_ControllerColliderHitGenerated_ControllerColliderHit_point_m4246858158 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ControllerColliderHitGenerated::ControllerColliderHit_normal(JSVCall)
extern "C"  void UnityEngine_ControllerColliderHitGenerated_ControllerColliderHit_normal_m1289956295 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ControllerColliderHitGenerated::ControllerColliderHit_moveDirection(JSVCall)
extern "C"  void UnityEngine_ControllerColliderHitGenerated_ControllerColliderHit_moveDirection_m3029385808 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ControllerColliderHitGenerated::ControllerColliderHit_moveLength(JSVCall)
extern "C"  void UnityEngine_ControllerColliderHitGenerated_ControllerColliderHit_moveLength_m2539870359 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ControllerColliderHitGenerated::__Register()
extern "C"  void UnityEngine_ControllerColliderHitGenerated___Register_m954105984 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ControllerColliderHitGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_ControllerColliderHitGenerated_ilo_getObject1_m2094897995 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ControllerColliderHitGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_ControllerColliderHitGenerated_ilo_addJSCSRel2_m2006990948 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ControllerColliderHitGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_ControllerColliderHitGenerated_ilo_setObject3_m1541904197 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ControllerColliderHitGenerated::ilo_setVector3S4(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_ControllerColliderHitGenerated_ilo_setVector3S4_m2695079555 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ControllerColliderHitGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void UnityEngine_ControllerColliderHitGenerated_ilo_setSingle5_m116728945 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
