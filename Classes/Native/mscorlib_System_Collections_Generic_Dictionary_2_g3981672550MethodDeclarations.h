﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ge127038044MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::.ctor()
#define Dictionary_2__ctor_m739076419(__this, method) ((  void (*) (Dictionary_2_t3981672550 *, const MethodInfo*))Dictionary_2__ctor_m1970740427_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m796036931(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3981672550 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3055481794_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m3218034252(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t3981672550 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m246380973_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::.ctor(System.Int32)
#define Dictionary_2__ctor_m225372701(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3981672550 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2283862428_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m3862628657(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t3981672550 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2785720432_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m4178673165(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3981672550 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m3086956428_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m3541254540(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3981672550 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m84494883_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m690283496(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3981672550 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3802058915_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m1934556682(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3981672550 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m2651933137_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.IDictionary.get_Values()
#define Dictionary_2_System_Collections_IDictionary_get_Values_m1755687032(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3981672550 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m3959692927_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.IDictionary.get_IsFixedSize()
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m464765137(__this, method) ((  bool (*) (Dictionary_2_t3981672550 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m690399222_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.IDictionary.get_IsReadOnly()
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3655589896(__this, method) ((  bool (*) (Dictionary_2_t3981672550 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3801415747_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3988536370(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3981672550 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1033400925_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m83723745(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3981672550 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m544260162_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m2209533904(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3981672550 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1552604367_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m918421474(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3981672550 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m1265514311_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m3792822815(__this, ___key0, method) ((  void (*) (Dictionary_2_t3981672550 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m887094912_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2650573490(__this, method) ((  bool (*) (Dictionary_2_t3981672550 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2891781485_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m2634836772(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3981672550 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3190917273_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m1353212022(__this, method) ((  bool (*) (Dictionary_2_t3981672550 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3694459761_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3668095797(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3981672550 *, KeyValuePair_2_t3880453256 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3891948310_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2272287985(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3981672550 *, KeyValuePair_2_t3880453256 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2643129836_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3118407641(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3981672550 *, KeyValuePair_2U5BU5D_t2732257369*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m1310130554_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1822624854(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3981672550 *, KeyValuePair_2_t3880453256 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1291167761_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m571819000(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3981672550 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m1681839833_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3394569479(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3981672550 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m39598356_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1567761406(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3981672550 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2619058961_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1484316363(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3981672550 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1614585004_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::get_Count()
#define Dictionary_2_get_Count_m1272819436(__this, method) ((  int32_t (*) (Dictionary_2_t3981672550 *, const MethodInfo*))Dictionary_2_get_Count_m2017330867_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::get_Item(TKey)
#define Dictionary_2_get_Item_m3075168129(__this, ___key0, method) ((  List_1_t3730483581 * (*) (Dictionary_2_t3981672550 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m225447128_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m2740676492(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3981672550 *, int32_t, List_1_t3730483581 *, const MethodInfo*))Dictionary_2_set_Item_m969380683_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m3546906180(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3981672550 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m966794307_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m3428189683(__this, ___size0, method) ((  void (*) (Dictionary_2_t3981672550 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2722942420_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m3719429295(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3981672550 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m2526602960_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m1057130819(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t3880453256  (*) (Il2CppObject * /* static, unused */, int32_t, List_1_t3730483581 *, const MethodInfo*))Dictionary_2_make_pair_m2209175068_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m4197144347(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, List_1_t3730483581 *, const MethodInfo*))Dictionary_2_pick_key_m2250582618_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m1790942903(__this /* static, unused */, ___key0, ___value1, method) ((  List_1_t3730483581 * (*) (Il2CppObject * /* static, unused */, int32_t, List_1_t3730483581 *, const MethodInfo*))Dictionary_2_pick_value_m3081424538_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m228327488(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3981672550 *, KeyValuePair_2U5BU5D_t2732257369*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m1459479295_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::Resize()
#define Dictionary_2_Resize_m3082926828(__this, method) ((  void (*) (Dictionary_2_t3981672550 *, const MethodInfo*))Dictionary_2_Resize_m1021207757_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::Add(TKey,TValue)
#define Dictionary_2_Add_m1857098755(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3981672550 *, int32_t, List_1_t3730483581 *, const MethodInfo*))Dictionary_2_Add_m3943451722_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::Clear()
#define Dictionary_2_Clear_m2440177006(__this, method) ((  void (*) (Dictionary_2_t3981672550 *, const MethodInfo*))Dictionary_2_Clear_m3671841014_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m3533104262(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3981672550 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2716258908_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m2670339169(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3981672550 *, List_1_t3730483581 *, const MethodInfo*))Dictionary_2_ContainsValue_m3348731996_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m242116714(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3981672550 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m2458268393_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m2759055418(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3981672550 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m2799251227_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::Remove(TKey)
#define Dictionary_2_Remove_m774915951(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3981672550 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m723410196_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m466227258(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3981672550 *, int32_t, List_1_t3730483581 **, const MethodInfo*))Dictionary_2_TryGetValue_m3608743605_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::get_Keys()
#define Dictionary_2_get_Keys_m1277661089(__this, method) ((  KeyCollection_t1313464705 * (*) (Dictionary_2_t3981672550 *, const MethodInfo*))Dictionary_2_get_Keys_m2511387754_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::get_Values()
#define Dictionary_2_get_Values_m4139070333(__this, method) ((  ValueCollection_t2682278263 * (*) (Dictionary_2_t3981672550 *, const MethodInfo*))Dictionary_2_get_Values_m638292586_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m3647003254(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t3981672550 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1700441525_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m103257042(__this, ___value0, method) ((  List_1_t3730483581 * (*) (Dictionary_2_t3981672550 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1393738677_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m3231612242(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3981672550 *, KeyValuePair_2_t3880453256 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m516745975_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m1529884093(__this, method) ((  Enumerator_t1004028646  (*) (Dictionary_2_t3981672550 *, const MethodInfo*))Dictionary_2_GetEnumerator_m632969680_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<FLOAT_TEXT_ID,System.Collections.Generic.List`1<FloatTextUnit>>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m1568888078(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, List_1_t3730483581 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m1056878111_gshared)(__this /* static, unused */, ___key0, ___value1, method)
