﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FingerEventDetector_1_gen3103176768MethodDeclarations.h"

// System.Void FingerEventDetector`1<FingerHoverEvent>::.ctor()
#define FingerEventDetector_1__ctor_m1607644680(__this, method) ((  void (*) (FingerEventDetector_1_t699428852 *, const MethodInfo*))FingerEventDetector_1__ctor_m3763465287_gshared)(__this, method)
// T FingerEventDetector`1<FingerHoverEvent>::CreateFingerEvent()
#define FingerEventDetector_1_CreateFingerEvent_m1891348346(__this, method) ((  FingerHoverEvent_t1767068455 * (*) (FingerEventDetector_1_t699428852 *, const MethodInfo*))FingerEventDetector_1_CreateFingerEvent_m1420222875_gshared)(__this, method)
// System.Type FingerEventDetector`1<FingerHoverEvent>::GetEventType()
#define FingerEventDetector_1_GetEventType_m2983604640(__this, method) ((  Type_t * (*) (FingerEventDetector_1_t699428852 *, const MethodInfo*))FingerEventDetector_1_GetEventType_m3851890933_gshared)(__this, method)
// System.Void FingerEventDetector`1<FingerHoverEvent>::Start()
#define FingerEventDetector_1_Start_m554782472(__this, method) ((  void (*) (FingerEventDetector_1_t699428852 *, const MethodInfo*))FingerEventDetector_1_Start_m2710603079_gshared)(__this, method)
// System.Void FingerEventDetector`1<FingerHoverEvent>::OnDestroy()
#define FingerEventDetector_1_OnDestroy_m3150595841(__this, method) ((  void (*) (FingerEventDetector_1_t699428852 *, const MethodInfo*))FingerEventDetector_1_OnDestroy_m3778430400_gshared)(__this, method)
// System.Void FingerEventDetector`1<FingerHoverEvent>::FingerGestures_OnInputProviderChanged()
#define FingerEventDetector_1_FingerGestures_OnInputProviderChanged_m3822875658(__this, method) ((  void (*) (FingerEventDetector_1_t699428852 *, const MethodInfo*))FingerEventDetector_1_FingerGestures_OnInputProviderChanged_m572190281_gshared)(__this, method)
// System.Void FingerEventDetector`1<FingerHoverEvent>::Init()
#define FingerEventDetector_1_Init_m1388869452(__this, method) ((  void (*) (FingerEventDetector_1_t699428852 *, const MethodInfo*))FingerEventDetector_1_Init_m2289696045_gshared)(__this, method)
// System.Void FingerEventDetector`1<FingerHoverEvent>::Init(System.Int32)
#define FingerEventDetector_1_Init_m876714653(__this, ___fingersCount0, method) ((  void (*) (FingerEventDetector_1_t699428852 *, int32_t, const MethodInfo*))FingerEventDetector_1_Init_m469522174_gshared)(__this, ___fingersCount0, method)
// T FingerEventDetector`1<FingerHoverEvent>::GetEvent(FingerGestures/Finger)
#define FingerEventDetector_1_GetEvent_m2221782796(__this, ___finger0, method) ((  FingerHoverEvent_t1767068455 * (*) (FingerEventDetector_1_t699428852 *, Finger_t182428197 *, const MethodInfo*))FingerEventDetector_1_GetEvent_m906084269_gshared)(__this, ___finger0, method)
// T FingerEventDetector`1<FingerHoverEvent>::GetEvent(System.Int32)
#define FingerEventDetector_1_GetEvent_m2593953426(__this, ___fingerIndex0, method) ((  FingerHoverEvent_t1767068455 * (*) (FingerEventDetector_1_t699428852 *, int32_t, const MethodInfo*))FingerEventDetector_1_GetEvent_m3248170193_gshared)(__this, ___fingerIndex0, method)
// FingerGestures FingerEventDetector`1<FingerHoverEvent>::ilo_get_Instance1()
#define FingerEventDetector_1_ilo_get_Instance1_m670957136(__this /* static, unused */, method) ((  FingerGestures_t2907604723 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))FingerEventDetector_1_ilo_get_Instance1_m1581142907_gshared)(__this /* static, unused */, method)
// FingerGestures/Finger FingerEventDetector`1<FingerHoverEvent>::ilo_GetFinger2(System.Int32)
#define FingerEventDetector_1_ilo_GetFinger2_m693379557(__this /* static, unused */, ___index0, method) ((  Finger_t182428197 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))FingerEventDetector_1_ilo_GetFinger2_m3652595042_gshared)(__this /* static, unused */, ___index0, method)
