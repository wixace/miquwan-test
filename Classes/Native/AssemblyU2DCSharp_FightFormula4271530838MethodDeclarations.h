﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CombatEntity
struct CombatEntity_t684137495;
// Skill
struct Skill_t79944241;
// Buff
struct Buff_t2081907;
// EffectMgr
struct EffectMgr_t535289511;
// FightCtrl
struct FightCtrl_t648967803;
// skillCfg
struct skillCfg_t2142425171;
// TeamSkillMgr
struct TeamSkillMgr_t2030650276;
// BuffCtrl
struct BuffCtrl_t2836564350;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Skill79944241.h"
#include "AssemblyU2DCSharp_DamageResult3811312780.h"
#include "AssemblyU2DCSharp_FightEnum_EDamageType2535357340.h"
#include "AssemblyU2DCSharp_Buff2081907.h"
#include "AssemblyU2DCSharp_BuffEnum_EBuffFunType4189275319.h"
#include "AssemblyU2DCSharp_EffectMgr535289511.h"
#include "AssemblyU2DCSharp_FightCtrl648967803.h"
#include "AssemblyU2DCSharp_BuffCtrl2836564350.h"
#include "AssemblyU2DCSharp_BuffEnum_EBuffState2159802479.h"

// System.Void FightFormula::.cctor()
extern "C"  void FightFormula__cctor_m3498630216 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightFormula::Cure(CombatEntity,CombatEntity,Skill)
extern "C"  int32_t FightFormula_Cure_m4213552303 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ___self0, CombatEntity_t684137495 * ___target1, Skill_t79944241 * ___skill2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DamageResult FightFormula::Damage(CombatEntity,CombatEntity,System.Int32)
extern "C"  DamageResult_t3811312780  FightFormula_Damage_m3334973040 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ___self0, CombatEntity_t684137495 * ___target1, int32_t ___skillID2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::GetNormalDamage(CombatEntity,CombatEntity,System.Int32)
extern "C"  float FightFormula_GetNormalDamage_m3207981364 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ___self0, CombatEntity_t684137495 * ___target1, int32_t ___skillID2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::GetSkillAddDamage(CombatEntity,CombatEntity,Skill)
extern "C"  float FightFormula_GetSkillAddDamage_m2571086677 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ___self0, CombatEntity_t684137495 * ___target1, Skill_t79944241 * ___skill2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::GetSkillAddValueDamage(CombatEntity,CombatEntity,Skill)
extern "C"  float FightFormula_GetSkillAddValueDamage_m128487964 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ___self0, CombatEntity_t684137495 * ___target1, Skill_t79944241 * ___skill2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::GetElementRestraint(CombatEntity,CombatEntity)
extern "C"  float FightFormula_GetElementRestraint_m3022532873 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ___self0, CombatEntity_t684137495 * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::GetLevelStifle(CombatEntity,CombatEntity)
extern "C"  float FightFormula_GetLevelStifle_m4060475862 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ___self0, CombatEntity_t684137495 * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::GetCountryRestraint(CombatEntity,CombatEntity)
extern "C"  float FightFormula_GetCountryRestraint_m3482690723 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ___self0, CombatEntity_t684137495 * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::GetStateRestraint(CombatEntity,CombatEntity)
extern "C"  float FightFormula_GetStateRestraint_m1275178270 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ___self0, CombatEntity_t684137495 * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::GetCountryResistance(CombatEntity,CombatEntity)
extern "C"  float FightFormula_GetCountryResistance_m2487981506 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ___self0, CombatEntity_t684137495 * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightEnum.EDamageType FightFormula::GetDamageType(CombatEntity,CombatEntity,System.Int32)
extern "C"  int32_t FightFormula_GetDamageType_m2440572529 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ___self0, CombatEntity_t684137495 * ___target1, int32_t ___skillID2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightFormula::IsAddFun(System.Int32,System.Int32,CombatEntity,System.Single)
extern "C"  bool FightFormula_IsAddFun_m1903896393 (Il2CppObject * __this /* static, unused */, int32_t ___skillBuffHit0, int32_t ___highType1, CombatEntity_t684137495 * ___target2, float ___randomNum3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightFormula::IsAddFun(System.Int32,System.Int32,System.Single)
extern "C"  bool FightFormula_IsAddFun_m1060955632 (Il2CppObject * __this /* static, unused */, int32_t ___skillBuffHit0, int32_t ___resist1, float ___randomNum2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::BuffHurt(Buff,CombatEntity)
extern "C"  float FightFormula_BuffHurt_m3323221301 (Il2CppObject * __this /* static, unused */, Buff_t2081907 * ___buff0, CombatEntity_t684137495 * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::BuffFunHurt(CombatEntity,System.Single,BuffEnum.EBuffFunType,System.Int32)
extern "C"  float FightFormula_BuffFunHurt_m3899134182 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ___target0, float ___lasttime1, int32_t ___funType2, int32_t ___para3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::RangeValue(System.Single,System.Single,System.Single)
extern "C"  float FightFormula_RangeValue_m1124628982 (Il2CppObject * __this /* static, unused */, float ___value0, float ___min1, float ___max2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightFormula::ilo_get_antiHurt1(CombatEntity)
extern "C"  bool FightFormula_ilo_get_antiHurt1_m2228005788 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightFormula::ilo_get_curAntiNum2(CombatEntity)
extern "C"  int32_t FightFormula_ilo_get_curAntiNum2_m2673624720 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightFormula::ilo_get_inUseAntiNum3(CombatEntity)
extern "C"  int32_t FightFormula_ilo_get_inUseAntiNum3_m969526579 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EffectMgr FightFormula::ilo_get_EffectMgr4()
extern "C"  EffectMgr_t535289511 * FightFormula_ilo_get_EffectMgr4_m1991020340 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightFormula::ilo_get_AntiEndEffectID5(CombatEntity)
extern "C"  int32_t FightFormula_ilo_get_AntiEndEffectID5_m1809955618 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightFormula::ilo_PlayEffect6(EffectMgr,System.Int32,CombatEntity)
extern "C"  void FightFormula_ilo_PlayEffect6_m3758519138 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, int32_t ___id1, CombatEntity_t684137495 * ___srcTf2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::ilo_GetNormalDamage7(CombatEntity,CombatEntity,System.Int32)
extern "C"  float FightFormula_ilo_GetNormalDamage7_m2012862938 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ___self0, CombatEntity_t684137495 * ___target1, int32_t ___skillID2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::ilo_get_finalHurt8(CombatEntity)
extern "C"  float FightFormula_ilo_get_finalHurt8_m3104756483 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightFormula::ilo_get_curShieldNum9(CombatEntity)
extern "C"  int32_t FightFormula_ilo_get_curShieldNum9_m3019292830 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FightFormula::ilo_set_curShieldNum10(CombatEntity,System.Int32)
extern "C"  void FightFormula_ilo_set_curShieldNum10_m2366180035 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightCtrl FightFormula::ilo_get_fightCtrl11(CombatEntity)
extern "C"  FightCtrl_t648967803 * FightFormula_ilo_get_fightCtrl11_m1103017235 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Skill FightFormula::ilo_GetSkill12(FightCtrl,System.Int32)
extern "C"  Skill_t79944241 * FightFormula_ilo_GetSkill12_m4209220912 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightFormula::ilo_get_damageType13(CombatEntity)
extern "C"  int32_t FightFormula_ilo_get_damageType13_m992696765 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::ilo_get_MD14(CombatEntity)
extern "C"  float FightFormula_ilo_get_MD14_m582459146 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::ilo_get_hurtP15(CombatEntity)
extern "C"  float FightFormula_ilo_get_hurtP15_m3229812361 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::ilo_get_hurtL16(CombatEntity)
extern "C"  float FightFormula_ilo_get_hurtL16_m3619092678 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::ilo_GetLevelStifle17(CombatEntity,CombatEntity)
extern "C"  float FightFormula_ilo_GetLevelStifle17_m791419037 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ___self0, CombatEntity_t684137495 * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::ilo_GetElementRestraint18(CombatEntity,CombatEntity)
extern "C"  float FightFormula_ilo_GetElementRestraint18_m2794029237 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ___self0, CombatEntity_t684137495 * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::ilo_get_atkPowerP19(CombatEntity)
extern "C"  float FightFormula_ilo_get_atkPowerP19_m1213551887 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::ilo_get_atkPowerL20(CombatEntity)
extern "C"  float FightFormula_ilo_get_atkPowerL20_m3196649121 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::ilo_get_defenseP21(CombatEntity)
extern "C"  float FightFormula_ilo_get_defenseP21_m1427419167 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::ilo_GetCountryResistance22(CombatEntity,CombatEntity)
extern "C"  float FightFormula_ilo_GetCountryResistance22_m2670791535 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ___self0, CombatEntity_t684137495 * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::ilo_GetStateRestraint23(CombatEntity,CombatEntity)
extern "C"  float FightFormula_ilo_GetStateRestraint23_m1524710384 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ___self0, CombatEntity_t684137495 * ___target1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// skillCfg FightFormula::ilo_get_mpJson24(Skill)
extern "C"  skillCfg_t2142425171 * FightFormula_ilo_get_mpJson24_m1945938095 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TeamSkillMgr FightFormula::ilo_get_TeamSkillMgr25()
extern "C"  TeamSkillMgr_t2030650276 * FightFormula_ilo_get_TeamSkillMgr25_m3536934139 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::ilo_get_thumpAddValue26(Skill)
extern "C"  float FightFormula_ilo_get_thumpAddValue26_m3076478574 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightFormula::ilo_get_damage_add_value27(Skill)
extern "C"  int32_t FightFormula_ilo_get_damage_add_value27_m47458608 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightFormula::ilo_get_level28(CombatEntity)
extern "C"  int32_t FightFormula_ilo_get_level28_m116729868 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightFormula::ilo_get_stateRestraint29(CombatEntity)
extern "C"  bool FightFormula_ilo_get_stateRestraint29_m2141942266 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BuffCtrl FightFormula::ilo_get_buffCtrl30(CombatEntity)
extern "C"  BuffCtrl_t2836564350 * FightFormula_ilo_get_buffCtrl30_m447875070 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightFormula::ilo_HasBuffState31(BuffCtrl,BuffEnum.EBuffState)
extern "C"  bool FightFormula_ilo_HasBuffState31_m2923283299 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, uint64_t ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightFormula::ilo_get_resistanceCountryType32(CombatEntity)
extern "C"  int32_t FightFormula_ilo_get_resistanceCountryType32_m2889447640 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::ilo_get_hits33(CombatEntity)
extern "C"  float FightFormula_ilo_get_hits33_m1058650800 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::ilo_RangeValue34(System.Single,System.Single,System.Single)
extern "C"  float FightFormula_ilo_RangeValue34_m2807347682 (Il2CppObject * __this /* static, unused */, float ___value0, float ___min1, float ___max2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 FightFormula::ilo_get_stable35(CombatEntity)
extern "C"  int32_t FightFormula_ilo_get_stable35_m981358063 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FightFormula::ilo_IsAddFun36(System.Int32,System.Int32,System.Single)
extern "C"  bool FightFormula_ilo_IsAddFun36_m3678080346 (Il2CppObject * __this /* static, unused */, int32_t ___skillBuffHit0, int32_t ___resist1, float ___randomNum2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::ilo_BuffFunHurt37(CombatEntity,System.Single,BuffEnum.EBuffFunType,System.Int32)
extern "C"  float FightFormula_ilo_BuffFunHurt37_m3312302837 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ___target0, float ___lasttime1, int32_t ___funType2, int32_t ___para3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single FightFormula::ilo_get_maxHp38(CombatEntity)
extern "C"  float FightFormula_ilo_get_maxHp38_m3585620597 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
