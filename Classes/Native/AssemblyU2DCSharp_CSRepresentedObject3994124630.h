﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSRepresentedObject
struct  CSRepresentedObject_t3994124630  : public Il2CppObject
{
public:
	// System.Int32 CSRepresentedObject::jsObjID
	int32_t ___jsObjID_2;
	// System.Boolean CSRepresentedObject::bFunction
	bool ___bFunction_3;
	// System.Int32 CSRepresentedObject::jsEngineRound
	int32_t ___jsEngineRound_4;

public:
	inline static int32_t get_offset_of_jsObjID_2() { return static_cast<int32_t>(offsetof(CSRepresentedObject_t3994124630, ___jsObjID_2)); }
	inline int32_t get_jsObjID_2() const { return ___jsObjID_2; }
	inline int32_t* get_address_of_jsObjID_2() { return &___jsObjID_2; }
	inline void set_jsObjID_2(int32_t value)
	{
		___jsObjID_2 = value;
	}

	inline static int32_t get_offset_of_bFunction_3() { return static_cast<int32_t>(offsetof(CSRepresentedObject_t3994124630, ___bFunction_3)); }
	inline bool get_bFunction_3() const { return ___bFunction_3; }
	inline bool* get_address_of_bFunction_3() { return &___bFunction_3; }
	inline void set_bFunction_3(bool value)
	{
		___bFunction_3 = value;
	}

	inline static int32_t get_offset_of_jsEngineRound_4() { return static_cast<int32_t>(offsetof(CSRepresentedObject_t3994124630, ___jsEngineRound_4)); }
	inline int32_t get_jsEngineRound_4() const { return ___jsEngineRound_4; }
	inline int32_t* get_address_of_jsEngineRound_4() { return &___jsEngineRound_4; }
	inline void set_jsEngineRound_4(int32_t value)
	{
		___jsEngineRound_4 = value;
	}
};

struct CSRepresentedObject_t3994124630_StaticFields
{
public:
	// System.Int32 CSRepresentedObject::s_objCount
	int32_t ___s_objCount_0;
	// System.Int32 CSRepresentedObject::s_funCount
	int32_t ___s_funCount_1;

public:
	inline static int32_t get_offset_of_s_objCount_0() { return static_cast<int32_t>(offsetof(CSRepresentedObject_t3994124630_StaticFields, ___s_objCount_0)); }
	inline int32_t get_s_objCount_0() const { return ___s_objCount_0; }
	inline int32_t* get_address_of_s_objCount_0() { return &___s_objCount_0; }
	inline void set_s_objCount_0(int32_t value)
	{
		___s_objCount_0 = value;
	}

	inline static int32_t get_offset_of_s_funCount_1() { return static_cast<int32_t>(offsetof(CSRepresentedObject_t3994124630_StaticFields, ___s_funCount_1)); }
	inline int32_t get_s_funCount_1() const { return ___s_funCount_1; }
	inline int32_t* get_address_of_s_funCount_1() { return &___s_funCount_1; }
	inline void set_s_funCount_1(int32_t value)
	{
		___s_funCount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
