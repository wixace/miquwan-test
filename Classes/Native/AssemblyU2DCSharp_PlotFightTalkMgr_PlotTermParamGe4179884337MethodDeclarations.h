﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlotFightTalkMgr_PlotTermParamGenerated
struct PlotFightTalkMgr_PlotTermParamGenerated_t4179884337;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void PlotFightTalkMgr_PlotTermParamGenerated::.ctor()
extern "C"  void PlotFightTalkMgr_PlotTermParamGenerated__ctor_m4175846746 (PlotFightTalkMgr_PlotTermParamGenerated_t4179884337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlotFightTalkMgr_PlotTermParamGenerated::PlotTermParam_PlotTermParam1(JSVCall,System.Int32)
extern "C"  bool PlotFightTalkMgr_PlotTermParamGenerated_PlotTermParam_PlotTermParam1_m1717485366 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr_PlotTermParamGenerated::PlotTermParam_type(JSVCall)
extern "C"  void PlotFightTalkMgr_PlotTermParamGenerated_PlotTermParam_type_m2206317860 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr_PlotTermParamGenerated::PlotTermParam_sceneId(JSVCall)
extern "C"  void PlotFightTalkMgr_PlotTermParamGenerated_PlotTermParam_sceneId_m1248070151 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr_PlotTermParamGenerated::PlotTermParam_activeNpcs(JSVCall)
extern "C"  void PlotFightTalkMgr_PlotTermParamGenerated_PlotTermParam_activeNpcs_m1550457414 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr_PlotTermParamGenerated::PlotTermParam_teamHeros(JSVCall)
extern "C"  void PlotFightTalkMgr_PlotTermParamGenerated_PlotTermParam_teamHeros_m2444551954 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr_PlotTermParamGenerated::PlotTermParam_dieNpcId(JSVCall)
extern "C"  void PlotFightTalkMgr_PlotTermParamGenerated_PlotTermParam_dieNpcId_m313214818 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr_PlotTermParamGenerated::PlotTermParam_storyId(JSVCall)
extern "C"  void PlotFightTalkMgr_PlotTermParamGenerated_PlotTermParam_storyId_m1754070526 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr_PlotTermParamGenerated::PlotTermParam_skillId(JSVCall)
extern "C"  void PlotFightTalkMgr_PlotTermParamGenerated_PlotTermParam_skillId_m4186700578 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr_PlotTermParamGenerated::PlotTermParam_userId(JSVCall)
extern "C"  void PlotFightTalkMgr_PlotTermParamGenerated_PlotTermParam_userId_m1364215192 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr_PlotTermParamGenerated::PlotTermParam_aiId(JSVCall)
extern "C"  void PlotFightTalkMgr_PlotTermParamGenerated_PlotTermParam_aiId_m2978823963 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr_PlotTermParamGenerated::PlotTermParam_npc(JSVCall)
extern "C"  void PlotFightTalkMgr_PlotTermParamGenerated_PlotTermParam_npc_m654868269 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr_PlotTermParamGenerated::__Register()
extern "C"  void PlotFightTalkMgr_PlotTermParamGenerated___Register_m1489069997 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlotFightTalkMgr_PlotTermParamGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t PlotFightTalkMgr_PlotTermParamGenerated_ilo_getObject1_m747514652 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr_PlotTermParamGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void PlotFightTalkMgr_PlotTermParamGenerated_ilo_addJSCSRel2_m1436267415 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlotFightTalkMgr_PlotTermParamGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t PlotFightTalkMgr_PlotTermParamGenerated_ilo_setObject3_m2327618326 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 PlotFightTalkMgr_PlotTermParamGenerated::ilo_getInt324(System.Int32)
extern "C"  int32_t PlotFightTalkMgr_PlotTermParamGenerated_ilo_getInt324_m2454080960 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlotFightTalkMgr_PlotTermParamGenerated::ilo_setInt325(System.Int32,System.Int32)
extern "C"  void PlotFightTalkMgr_PlotTermParamGenerated_ilo_setInt325_m1110959928 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
