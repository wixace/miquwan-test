﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2213438269.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_CompactVoxelC3431095593.h"

// System.Void System.Array/InternalEnumerator`1<Pathfinding.Voxels.CompactVoxelCell>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3425231745_gshared (InternalEnumerator_1_t2213438269 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3425231745(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2213438269 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3425231745_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.Voxels.CompactVoxelCell>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2563340415_gshared (InternalEnumerator_1_t2213438269 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2563340415(__this, method) ((  void (*) (InternalEnumerator_1_t2213438269 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2563340415_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Pathfinding.Voxels.CompactVoxelCell>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3443401525_gshared (InternalEnumerator_1_t2213438269 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3443401525(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2213438269 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3443401525_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.Voxels.CompactVoxelCell>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3200494232_gshared (InternalEnumerator_1_t2213438269 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3200494232(__this, method) ((  void (*) (InternalEnumerator_1_t2213438269 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3200494232_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.Voxels.CompactVoxelCell>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3229553839_gshared (InternalEnumerator_1_t2213438269 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3229553839(__this, method) ((  bool (*) (InternalEnumerator_1_t2213438269 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3229553839_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Pathfinding.Voxels.CompactVoxelCell>::get_Current()
extern "C"  CompactVoxelCell_t3431095593  InternalEnumerator_1_get_Current_m1529929834_gshared (InternalEnumerator_1_t2213438269 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1529929834(__this, method) ((  CompactVoxelCell_t3431095593  (*) (InternalEnumerator_1_t2213438269 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1529929834_gshared)(__this, method)
