﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// Pathfinding.AstarData
struct AstarData_t3283402719;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.AstarData/<FindGraphsOfType>c__Iterator8
struct  U3CFindGraphsOfTypeU3Ec__Iterator8_t1950563766  : public Il2CppObject
{
public:
	// System.Int32 Pathfinding.AstarData/<FindGraphsOfType>c__Iterator8::<i>__0
	int32_t ___U3CiU3E__0_0;
	// System.Type Pathfinding.AstarData/<FindGraphsOfType>c__Iterator8::type
	Type_t * ___type_1;
	// System.Int32 Pathfinding.AstarData/<FindGraphsOfType>c__Iterator8::$PC
	int32_t ___U24PC_2;
	// System.Object Pathfinding.AstarData/<FindGraphsOfType>c__Iterator8::$current
	Il2CppObject * ___U24current_3;
	// System.Type Pathfinding.AstarData/<FindGraphsOfType>c__Iterator8::<$>type
	Type_t * ___U3CU24U3Etype_4;
	// Pathfinding.AstarData Pathfinding.AstarData/<FindGraphsOfType>c__Iterator8::<>f__this
	AstarData_t3283402719 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_U3CiU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFindGraphsOfTypeU3Ec__Iterator8_t1950563766, ___U3CiU3E__0_0)); }
	inline int32_t get_U3CiU3E__0_0() const { return ___U3CiU3E__0_0; }
	inline int32_t* get_address_of_U3CiU3E__0_0() { return &___U3CiU3E__0_0; }
	inline void set_U3CiU3E__0_0(int32_t value)
	{
		___U3CiU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(U3CFindGraphsOfTypeU3Ec__Iterator8_t1950563766, ___type_1)); }
	inline Type_t * get_type_1() const { return ___type_1; }
	inline Type_t ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(Type_t * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier(&___type_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CFindGraphsOfTypeU3Ec__Iterator8_t1950563766, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CFindGraphsOfTypeU3Ec__Iterator8_t1950563766, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Etype_4() { return static_cast<int32_t>(offsetof(U3CFindGraphsOfTypeU3Ec__Iterator8_t1950563766, ___U3CU24U3Etype_4)); }
	inline Type_t * get_U3CU24U3Etype_4() const { return ___U3CU24U3Etype_4; }
	inline Type_t ** get_address_of_U3CU24U3Etype_4() { return &___U3CU24U3Etype_4; }
	inline void set_U3CU24U3Etype_4(Type_t * value)
	{
		___U3CU24U3Etype_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Etype_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CFindGraphsOfTypeU3Ec__Iterator8_t1950563766, ___U3CU3Ef__this_5)); }
	inline AstarData_t3283402719 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline AstarData_t3283402719 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(AstarData_t3283402719 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
