﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ButtonClickStateGenerated/<ButtonClickState_onClick_GetDelegate_member6_arg0>c__AnonStorey51
struct U3CButtonClickState_onClick_GetDelegate_member6_arg0U3Ec__AnonStorey51_t482994463;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void ButtonClickStateGenerated/<ButtonClickState_onClick_GetDelegate_member6_arg0>c__AnonStorey51::.ctor()
extern "C"  void U3CButtonClickState_onClick_GetDelegate_member6_arg0U3Ec__AnonStorey51__ctor_m2968841308 (U3CButtonClickState_onClick_GetDelegate_member6_arg0U3Ec__AnonStorey51_t482994463 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ButtonClickStateGenerated/<ButtonClickState_onClick_GetDelegate_member6_arg0>c__AnonStorey51::<>m__16(UnityEngine.GameObject,System.Boolean)
extern "C"  void U3CButtonClickState_onClick_GetDelegate_member6_arg0U3Ec__AnonStorey51_U3CU3Em__16_m2486141499 (U3CButtonClickState_onClick_GetDelegate_member6_arg0U3Ec__AnonStorey51_t482994463 * __this, GameObject_t3674682005 * ___go0, bool ___isPress1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
