﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.PathHandler
struct PathHandler_t918952263;
// Pathfinding.RecastGraph
struct RecastGraph_t2197443166;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RecastGraph/<OnDrawGizmos>c__AnonStorey11A
struct  U3COnDrawGizmosU3Ec__AnonStorey11A_t1239375581  : public Il2CppObject
{
public:
	// Pathfinding.PathHandler Pathfinding.RecastGraph/<OnDrawGizmos>c__AnonStorey11A::debugData
	PathHandler_t918952263 * ___debugData_0;
	// Pathfinding.RecastGraph Pathfinding.RecastGraph/<OnDrawGizmos>c__AnonStorey11A::<>f__this
	RecastGraph_t2197443166 * ___U3CU3Ef__this_1;

public:
	inline static int32_t get_offset_of_debugData_0() { return static_cast<int32_t>(offsetof(U3COnDrawGizmosU3Ec__AnonStorey11A_t1239375581, ___debugData_0)); }
	inline PathHandler_t918952263 * get_debugData_0() const { return ___debugData_0; }
	inline PathHandler_t918952263 ** get_address_of_debugData_0() { return &___debugData_0; }
	inline void set_debugData_0(PathHandler_t918952263 * value)
	{
		___debugData_0 = value;
		Il2CppCodeGenWriteBarrier(&___debugData_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_1() { return static_cast<int32_t>(offsetof(U3COnDrawGizmosU3Ec__AnonStorey11A_t1239375581, ___U3CU3Ef__this_1)); }
	inline RecastGraph_t2197443166 * get_U3CU3Ef__this_1() const { return ___U3CU3Ef__this_1; }
	inline RecastGraph_t2197443166 ** get_address_of_U3CU3Ef__this_1() { return &___U3CU3Ef__this_1; }
	inline void set_U3CU3Ef__this_1(RecastGraph_t2197443166 * value)
	{
		___U3CU3Ef__this_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
