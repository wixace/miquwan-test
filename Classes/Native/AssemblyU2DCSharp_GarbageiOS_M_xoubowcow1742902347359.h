﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_xoubowcow174
struct  M_xoubowcow174_t2902347359  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_xoubowcow174::_dawraisaw
	float ____dawraisaw_0;
	// System.Single GarbageiOS.M_xoubowcow174::_tarju
	float ____tarju_1;
	// System.Single GarbageiOS.M_xoubowcow174::_fiskouyay
	float ____fiskouyay_2;
	// System.UInt32 GarbageiOS.M_xoubowcow174::_nitate
	uint32_t ____nitate_3;
	// System.Int32 GarbageiOS.M_xoubowcow174::_moutri
	int32_t ____moutri_4;
	// System.Single GarbageiOS.M_xoubowcow174::_hotowlasSooza
	float ____hotowlasSooza_5;
	// System.Boolean GarbageiOS.M_xoubowcow174::_rucehear
	bool ____rucehear_6;
	// System.Single GarbageiOS.M_xoubowcow174::_cowyeeStunisrem
	float ____cowyeeStunisrem_7;
	// System.Single GarbageiOS.M_xoubowcow174::_netidorGaimaw
	float ____netidorGaimaw_8;
	// System.UInt32 GarbageiOS.M_xoubowcow174::_foulaCoowida
	uint32_t ____foulaCoowida_9;
	// System.Single GarbageiOS.M_xoubowcow174::_lermeTeasaw
	float ____lermeTeasaw_10;
	// System.String GarbageiOS.M_xoubowcow174::_sewalcooRemercar
	String_t* ____sewalcooRemercar_11;
	// System.Int32 GarbageiOS.M_xoubowcow174::_chearpir
	int32_t ____chearpir_12;
	// System.UInt32 GarbageiOS.M_xoubowcow174::_boolaBulou
	uint32_t ____boolaBulou_13;
	// System.UInt32 GarbageiOS.M_xoubowcow174::_sawgitaReargee
	uint32_t ____sawgitaReargee_14;
	// System.Int32 GarbageiOS.M_xoubowcow174::_gisbor
	int32_t ____gisbor_15;
	// System.String GarbageiOS.M_xoubowcow174::_yallmaLezor
	String_t* ____yallmaLezor_16;
	// System.String GarbageiOS.M_xoubowcow174::_cirgalllouQisno
	String_t* ____cirgalllouQisno_17;
	// System.Single GarbageiOS.M_xoubowcow174::_biwhem
	float ____biwhem_18;
	// System.String GarbageiOS.M_xoubowcow174::_zeacarwe
	String_t* ____zeacarwe_19;
	// System.String GarbageiOS.M_xoubowcow174::_sarjafawRelkall
	String_t* ____sarjafawRelkall_20;
	// System.Single GarbageiOS.M_xoubowcow174::_kalharpiFawmuna
	float ____kalharpiFawmuna_21;
	// System.UInt32 GarbageiOS.M_xoubowcow174::_searxubar
	uint32_t ____searxubar_22;
	// System.UInt32 GarbageiOS.M_xoubowcow174::_saylawjo
	uint32_t ____saylawjo_23;
	// System.String GarbageiOS.M_xoubowcow174::_bumiqaSticou
	String_t* ____bumiqaSticou_24;
	// System.Boolean GarbageiOS.M_xoubowcow174::_telhi
	bool ____telhi_25;
	// System.UInt32 GarbageiOS.M_xoubowcow174::_xoomearser
	uint32_t ____xoomearser_26;
	// System.Boolean GarbageiOS.M_xoubowcow174::_dede
	bool ____dede_27;
	// System.String GarbageiOS.M_xoubowcow174::_jokouLepair
	String_t* ____jokouLepair_28;
	// System.Int32 GarbageiOS.M_xoubowcow174::_sergataQeseqel
	int32_t ____sergataQeseqel_29;
	// System.Single GarbageiOS.M_xoubowcow174::_dairsteresemCallnizal
	float ____dairsteresemCallnizal_30;
	// System.Boolean GarbageiOS.M_xoubowcow174::_kagutra
	bool ____kagutra_31;
	// System.String GarbageiOS.M_xoubowcow174::_lekurrarDateloo
	String_t* ____lekurrarDateloo_32;
	// System.Boolean GarbageiOS.M_xoubowcow174::_boudoRayci
	bool ____boudoRayci_33;
	// System.Boolean GarbageiOS.M_xoubowcow174::_trakouster
	bool ____trakouster_34;
	// System.Single GarbageiOS.M_xoubowcow174::_coocerar
	float ____coocerar_35;
	// System.String GarbageiOS.M_xoubowcow174::_gepeePairgowar
	String_t* ____gepeePairgowar_36;
	// System.Single GarbageiOS.M_xoubowcow174::_jarbelqou
	float ____jarbelqou_37;
	// System.Int32 GarbageiOS.M_xoubowcow174::_borsereke
	int32_t ____borsereke_38;
	// System.Single GarbageiOS.M_xoubowcow174::_merjo
	float ____merjo_39;

public:
	inline static int32_t get_offset_of__dawraisaw_0() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____dawraisaw_0)); }
	inline float get__dawraisaw_0() const { return ____dawraisaw_0; }
	inline float* get_address_of__dawraisaw_0() { return &____dawraisaw_0; }
	inline void set__dawraisaw_0(float value)
	{
		____dawraisaw_0 = value;
	}

	inline static int32_t get_offset_of__tarju_1() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____tarju_1)); }
	inline float get__tarju_1() const { return ____tarju_1; }
	inline float* get_address_of__tarju_1() { return &____tarju_1; }
	inline void set__tarju_1(float value)
	{
		____tarju_1 = value;
	}

	inline static int32_t get_offset_of__fiskouyay_2() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____fiskouyay_2)); }
	inline float get__fiskouyay_2() const { return ____fiskouyay_2; }
	inline float* get_address_of__fiskouyay_2() { return &____fiskouyay_2; }
	inline void set__fiskouyay_2(float value)
	{
		____fiskouyay_2 = value;
	}

	inline static int32_t get_offset_of__nitate_3() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____nitate_3)); }
	inline uint32_t get__nitate_3() const { return ____nitate_3; }
	inline uint32_t* get_address_of__nitate_3() { return &____nitate_3; }
	inline void set__nitate_3(uint32_t value)
	{
		____nitate_3 = value;
	}

	inline static int32_t get_offset_of__moutri_4() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____moutri_4)); }
	inline int32_t get__moutri_4() const { return ____moutri_4; }
	inline int32_t* get_address_of__moutri_4() { return &____moutri_4; }
	inline void set__moutri_4(int32_t value)
	{
		____moutri_4 = value;
	}

	inline static int32_t get_offset_of__hotowlasSooza_5() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____hotowlasSooza_5)); }
	inline float get__hotowlasSooza_5() const { return ____hotowlasSooza_5; }
	inline float* get_address_of__hotowlasSooza_5() { return &____hotowlasSooza_5; }
	inline void set__hotowlasSooza_5(float value)
	{
		____hotowlasSooza_5 = value;
	}

	inline static int32_t get_offset_of__rucehear_6() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____rucehear_6)); }
	inline bool get__rucehear_6() const { return ____rucehear_6; }
	inline bool* get_address_of__rucehear_6() { return &____rucehear_6; }
	inline void set__rucehear_6(bool value)
	{
		____rucehear_6 = value;
	}

	inline static int32_t get_offset_of__cowyeeStunisrem_7() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____cowyeeStunisrem_7)); }
	inline float get__cowyeeStunisrem_7() const { return ____cowyeeStunisrem_7; }
	inline float* get_address_of__cowyeeStunisrem_7() { return &____cowyeeStunisrem_7; }
	inline void set__cowyeeStunisrem_7(float value)
	{
		____cowyeeStunisrem_7 = value;
	}

	inline static int32_t get_offset_of__netidorGaimaw_8() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____netidorGaimaw_8)); }
	inline float get__netidorGaimaw_8() const { return ____netidorGaimaw_8; }
	inline float* get_address_of__netidorGaimaw_8() { return &____netidorGaimaw_8; }
	inline void set__netidorGaimaw_8(float value)
	{
		____netidorGaimaw_8 = value;
	}

	inline static int32_t get_offset_of__foulaCoowida_9() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____foulaCoowida_9)); }
	inline uint32_t get__foulaCoowida_9() const { return ____foulaCoowida_9; }
	inline uint32_t* get_address_of__foulaCoowida_9() { return &____foulaCoowida_9; }
	inline void set__foulaCoowida_9(uint32_t value)
	{
		____foulaCoowida_9 = value;
	}

	inline static int32_t get_offset_of__lermeTeasaw_10() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____lermeTeasaw_10)); }
	inline float get__lermeTeasaw_10() const { return ____lermeTeasaw_10; }
	inline float* get_address_of__lermeTeasaw_10() { return &____lermeTeasaw_10; }
	inline void set__lermeTeasaw_10(float value)
	{
		____lermeTeasaw_10 = value;
	}

	inline static int32_t get_offset_of__sewalcooRemercar_11() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____sewalcooRemercar_11)); }
	inline String_t* get__sewalcooRemercar_11() const { return ____sewalcooRemercar_11; }
	inline String_t** get_address_of__sewalcooRemercar_11() { return &____sewalcooRemercar_11; }
	inline void set__sewalcooRemercar_11(String_t* value)
	{
		____sewalcooRemercar_11 = value;
		Il2CppCodeGenWriteBarrier(&____sewalcooRemercar_11, value);
	}

	inline static int32_t get_offset_of__chearpir_12() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____chearpir_12)); }
	inline int32_t get__chearpir_12() const { return ____chearpir_12; }
	inline int32_t* get_address_of__chearpir_12() { return &____chearpir_12; }
	inline void set__chearpir_12(int32_t value)
	{
		____chearpir_12 = value;
	}

	inline static int32_t get_offset_of__boolaBulou_13() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____boolaBulou_13)); }
	inline uint32_t get__boolaBulou_13() const { return ____boolaBulou_13; }
	inline uint32_t* get_address_of__boolaBulou_13() { return &____boolaBulou_13; }
	inline void set__boolaBulou_13(uint32_t value)
	{
		____boolaBulou_13 = value;
	}

	inline static int32_t get_offset_of__sawgitaReargee_14() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____sawgitaReargee_14)); }
	inline uint32_t get__sawgitaReargee_14() const { return ____sawgitaReargee_14; }
	inline uint32_t* get_address_of__sawgitaReargee_14() { return &____sawgitaReargee_14; }
	inline void set__sawgitaReargee_14(uint32_t value)
	{
		____sawgitaReargee_14 = value;
	}

	inline static int32_t get_offset_of__gisbor_15() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____gisbor_15)); }
	inline int32_t get__gisbor_15() const { return ____gisbor_15; }
	inline int32_t* get_address_of__gisbor_15() { return &____gisbor_15; }
	inline void set__gisbor_15(int32_t value)
	{
		____gisbor_15 = value;
	}

	inline static int32_t get_offset_of__yallmaLezor_16() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____yallmaLezor_16)); }
	inline String_t* get__yallmaLezor_16() const { return ____yallmaLezor_16; }
	inline String_t** get_address_of__yallmaLezor_16() { return &____yallmaLezor_16; }
	inline void set__yallmaLezor_16(String_t* value)
	{
		____yallmaLezor_16 = value;
		Il2CppCodeGenWriteBarrier(&____yallmaLezor_16, value);
	}

	inline static int32_t get_offset_of__cirgalllouQisno_17() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____cirgalllouQisno_17)); }
	inline String_t* get__cirgalllouQisno_17() const { return ____cirgalllouQisno_17; }
	inline String_t** get_address_of__cirgalllouQisno_17() { return &____cirgalllouQisno_17; }
	inline void set__cirgalllouQisno_17(String_t* value)
	{
		____cirgalllouQisno_17 = value;
		Il2CppCodeGenWriteBarrier(&____cirgalllouQisno_17, value);
	}

	inline static int32_t get_offset_of__biwhem_18() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____biwhem_18)); }
	inline float get__biwhem_18() const { return ____biwhem_18; }
	inline float* get_address_of__biwhem_18() { return &____biwhem_18; }
	inline void set__biwhem_18(float value)
	{
		____biwhem_18 = value;
	}

	inline static int32_t get_offset_of__zeacarwe_19() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____zeacarwe_19)); }
	inline String_t* get__zeacarwe_19() const { return ____zeacarwe_19; }
	inline String_t** get_address_of__zeacarwe_19() { return &____zeacarwe_19; }
	inline void set__zeacarwe_19(String_t* value)
	{
		____zeacarwe_19 = value;
		Il2CppCodeGenWriteBarrier(&____zeacarwe_19, value);
	}

	inline static int32_t get_offset_of__sarjafawRelkall_20() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____sarjafawRelkall_20)); }
	inline String_t* get__sarjafawRelkall_20() const { return ____sarjafawRelkall_20; }
	inline String_t** get_address_of__sarjafawRelkall_20() { return &____sarjafawRelkall_20; }
	inline void set__sarjafawRelkall_20(String_t* value)
	{
		____sarjafawRelkall_20 = value;
		Il2CppCodeGenWriteBarrier(&____sarjafawRelkall_20, value);
	}

	inline static int32_t get_offset_of__kalharpiFawmuna_21() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____kalharpiFawmuna_21)); }
	inline float get__kalharpiFawmuna_21() const { return ____kalharpiFawmuna_21; }
	inline float* get_address_of__kalharpiFawmuna_21() { return &____kalharpiFawmuna_21; }
	inline void set__kalharpiFawmuna_21(float value)
	{
		____kalharpiFawmuna_21 = value;
	}

	inline static int32_t get_offset_of__searxubar_22() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____searxubar_22)); }
	inline uint32_t get__searxubar_22() const { return ____searxubar_22; }
	inline uint32_t* get_address_of__searxubar_22() { return &____searxubar_22; }
	inline void set__searxubar_22(uint32_t value)
	{
		____searxubar_22 = value;
	}

	inline static int32_t get_offset_of__saylawjo_23() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____saylawjo_23)); }
	inline uint32_t get__saylawjo_23() const { return ____saylawjo_23; }
	inline uint32_t* get_address_of__saylawjo_23() { return &____saylawjo_23; }
	inline void set__saylawjo_23(uint32_t value)
	{
		____saylawjo_23 = value;
	}

	inline static int32_t get_offset_of__bumiqaSticou_24() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____bumiqaSticou_24)); }
	inline String_t* get__bumiqaSticou_24() const { return ____bumiqaSticou_24; }
	inline String_t** get_address_of__bumiqaSticou_24() { return &____bumiqaSticou_24; }
	inline void set__bumiqaSticou_24(String_t* value)
	{
		____bumiqaSticou_24 = value;
		Il2CppCodeGenWriteBarrier(&____bumiqaSticou_24, value);
	}

	inline static int32_t get_offset_of__telhi_25() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____telhi_25)); }
	inline bool get__telhi_25() const { return ____telhi_25; }
	inline bool* get_address_of__telhi_25() { return &____telhi_25; }
	inline void set__telhi_25(bool value)
	{
		____telhi_25 = value;
	}

	inline static int32_t get_offset_of__xoomearser_26() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____xoomearser_26)); }
	inline uint32_t get__xoomearser_26() const { return ____xoomearser_26; }
	inline uint32_t* get_address_of__xoomearser_26() { return &____xoomearser_26; }
	inline void set__xoomearser_26(uint32_t value)
	{
		____xoomearser_26 = value;
	}

	inline static int32_t get_offset_of__dede_27() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____dede_27)); }
	inline bool get__dede_27() const { return ____dede_27; }
	inline bool* get_address_of__dede_27() { return &____dede_27; }
	inline void set__dede_27(bool value)
	{
		____dede_27 = value;
	}

	inline static int32_t get_offset_of__jokouLepair_28() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____jokouLepair_28)); }
	inline String_t* get__jokouLepair_28() const { return ____jokouLepair_28; }
	inline String_t** get_address_of__jokouLepair_28() { return &____jokouLepair_28; }
	inline void set__jokouLepair_28(String_t* value)
	{
		____jokouLepair_28 = value;
		Il2CppCodeGenWriteBarrier(&____jokouLepair_28, value);
	}

	inline static int32_t get_offset_of__sergataQeseqel_29() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____sergataQeseqel_29)); }
	inline int32_t get__sergataQeseqel_29() const { return ____sergataQeseqel_29; }
	inline int32_t* get_address_of__sergataQeseqel_29() { return &____sergataQeseqel_29; }
	inline void set__sergataQeseqel_29(int32_t value)
	{
		____sergataQeseqel_29 = value;
	}

	inline static int32_t get_offset_of__dairsteresemCallnizal_30() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____dairsteresemCallnizal_30)); }
	inline float get__dairsteresemCallnizal_30() const { return ____dairsteresemCallnizal_30; }
	inline float* get_address_of__dairsteresemCallnizal_30() { return &____dairsteresemCallnizal_30; }
	inline void set__dairsteresemCallnizal_30(float value)
	{
		____dairsteresemCallnizal_30 = value;
	}

	inline static int32_t get_offset_of__kagutra_31() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____kagutra_31)); }
	inline bool get__kagutra_31() const { return ____kagutra_31; }
	inline bool* get_address_of__kagutra_31() { return &____kagutra_31; }
	inline void set__kagutra_31(bool value)
	{
		____kagutra_31 = value;
	}

	inline static int32_t get_offset_of__lekurrarDateloo_32() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____lekurrarDateloo_32)); }
	inline String_t* get__lekurrarDateloo_32() const { return ____lekurrarDateloo_32; }
	inline String_t** get_address_of__lekurrarDateloo_32() { return &____lekurrarDateloo_32; }
	inline void set__lekurrarDateloo_32(String_t* value)
	{
		____lekurrarDateloo_32 = value;
		Il2CppCodeGenWriteBarrier(&____lekurrarDateloo_32, value);
	}

	inline static int32_t get_offset_of__boudoRayci_33() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____boudoRayci_33)); }
	inline bool get__boudoRayci_33() const { return ____boudoRayci_33; }
	inline bool* get_address_of__boudoRayci_33() { return &____boudoRayci_33; }
	inline void set__boudoRayci_33(bool value)
	{
		____boudoRayci_33 = value;
	}

	inline static int32_t get_offset_of__trakouster_34() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____trakouster_34)); }
	inline bool get__trakouster_34() const { return ____trakouster_34; }
	inline bool* get_address_of__trakouster_34() { return &____trakouster_34; }
	inline void set__trakouster_34(bool value)
	{
		____trakouster_34 = value;
	}

	inline static int32_t get_offset_of__coocerar_35() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____coocerar_35)); }
	inline float get__coocerar_35() const { return ____coocerar_35; }
	inline float* get_address_of__coocerar_35() { return &____coocerar_35; }
	inline void set__coocerar_35(float value)
	{
		____coocerar_35 = value;
	}

	inline static int32_t get_offset_of__gepeePairgowar_36() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____gepeePairgowar_36)); }
	inline String_t* get__gepeePairgowar_36() const { return ____gepeePairgowar_36; }
	inline String_t** get_address_of__gepeePairgowar_36() { return &____gepeePairgowar_36; }
	inline void set__gepeePairgowar_36(String_t* value)
	{
		____gepeePairgowar_36 = value;
		Il2CppCodeGenWriteBarrier(&____gepeePairgowar_36, value);
	}

	inline static int32_t get_offset_of__jarbelqou_37() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____jarbelqou_37)); }
	inline float get__jarbelqou_37() const { return ____jarbelqou_37; }
	inline float* get_address_of__jarbelqou_37() { return &____jarbelqou_37; }
	inline void set__jarbelqou_37(float value)
	{
		____jarbelqou_37 = value;
	}

	inline static int32_t get_offset_of__borsereke_38() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____borsereke_38)); }
	inline int32_t get__borsereke_38() const { return ____borsereke_38; }
	inline int32_t* get_address_of__borsereke_38() { return &____borsereke_38; }
	inline void set__borsereke_38(int32_t value)
	{
		____borsereke_38 = value;
	}

	inline static int32_t get_offset_of__merjo_39() { return static_cast<int32_t>(offsetof(M_xoubowcow174_t2902347359, ____merjo_39)); }
	inline float get__merjo_39() const { return ____merjo_39; }
	inline float* get_address_of__merjo_39() { return &____merjo_39; }
	inline void set__merjo_39(float value)
	{
		____merjo_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
