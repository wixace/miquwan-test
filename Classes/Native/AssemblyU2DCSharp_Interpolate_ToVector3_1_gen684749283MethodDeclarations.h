﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Interpolate/ToVector3`1<UnityEngine.Vector3>
struct ToVector3_1_t684749283;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void Interpolate/ToVector3`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void ToVector3_1__ctor_m3233597424_gshared (ToVector3_1_t684749283 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define ToVector3_1__ctor_m3233597424(__this, ___object0, ___method1, method) ((  void (*) (ToVector3_1_t684749283 *, Il2CppObject *, IntPtr_t, const MethodInfo*))ToVector3_1__ctor_m3233597424_gshared)(__this, ___object0, ___method1, method)
// UnityEngine.Vector3 Interpolate/ToVector3`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  Vector3_t4282066566  ToVector3_1_Invoke_m2293659546_gshared (ToVector3_1_t684749283 * __this, Vector3_t4282066566  ___v0, const MethodInfo* method);
#define ToVector3_1_Invoke_m2293659546(__this, ___v0, method) ((  Vector3_t4282066566  (*) (ToVector3_1_t684749283 *, Vector3_t4282066566 , const MethodInfo*))ToVector3_1_Invoke_m2293659546_gshared)(__this, ___v0, method)
// System.IAsyncResult Interpolate/ToVector3`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * ToVector3_1_BeginInvoke_m2324920609_gshared (ToVector3_1_t684749283 * __this, Vector3_t4282066566  ___v0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method);
#define ToVector3_1_BeginInvoke_m2324920609(__this, ___v0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (ToVector3_1_t684749283 *, Vector3_t4282066566 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))ToVector3_1_BeginInvoke_m2324920609_gshared)(__this, ___v0, ___callback1, ___object2, method)
// UnityEngine.Vector3 Interpolate/ToVector3`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  Vector3_t4282066566  ToVector3_1_EndInvoke_m2047897722_gshared (ToVector3_1_t684749283 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define ToVector3_1_EndInvoke_m2047897722(__this, ___result0, method) ((  Vector3_t4282066566  (*) (ToVector3_1_t684749283 *, Il2CppObject *, const MethodInfo*))ToVector3_1_EndInvoke_m2047897722_gshared)(__this, ___result0, method)
