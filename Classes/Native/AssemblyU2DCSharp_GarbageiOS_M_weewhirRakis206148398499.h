﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_weewhirRakis206
struct  M_weewhirRakis206_t148398499  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_weewhirRakis206::_reeketiFawleho
	uint32_t ____reeketiFawleho_0;
	// System.UInt32 GarbageiOS.M_weewhirRakis206::_jersereCetoca
	uint32_t ____jersereCetoca_1;
	// System.UInt32 GarbageiOS.M_weewhirRakis206::_caze
	uint32_t ____caze_2;
	// System.UInt32 GarbageiOS.M_weewhirRakis206::_houbalhe
	uint32_t ____houbalhe_3;
	// System.String GarbageiOS.M_weewhirRakis206::_boomereBina
	String_t* ____boomereBina_4;
	// System.Single GarbageiOS.M_weewhirRakis206::_chorvinor
	float ____chorvinor_5;
	// System.Single GarbageiOS.M_weewhirRakis206::_jeewomirJamar
	float ____jeewomirJamar_6;
	// System.Boolean GarbageiOS.M_weewhirRakis206::_tarzow
	bool ____tarzow_7;
	// System.String GarbageiOS.M_weewhirRakis206::_peatuBeeseemer
	String_t* ____peatuBeeseemer_8;
	// System.String GarbageiOS.M_weewhirRakis206::_semjerfow
	String_t* ____semjerfow_9;
	// System.Boolean GarbageiOS.M_weewhirRakis206::_yallvayLaxow
	bool ____yallvayLaxow_10;

public:
	inline static int32_t get_offset_of__reeketiFawleho_0() { return static_cast<int32_t>(offsetof(M_weewhirRakis206_t148398499, ____reeketiFawleho_0)); }
	inline uint32_t get__reeketiFawleho_0() const { return ____reeketiFawleho_0; }
	inline uint32_t* get_address_of__reeketiFawleho_0() { return &____reeketiFawleho_0; }
	inline void set__reeketiFawleho_0(uint32_t value)
	{
		____reeketiFawleho_0 = value;
	}

	inline static int32_t get_offset_of__jersereCetoca_1() { return static_cast<int32_t>(offsetof(M_weewhirRakis206_t148398499, ____jersereCetoca_1)); }
	inline uint32_t get__jersereCetoca_1() const { return ____jersereCetoca_1; }
	inline uint32_t* get_address_of__jersereCetoca_1() { return &____jersereCetoca_1; }
	inline void set__jersereCetoca_1(uint32_t value)
	{
		____jersereCetoca_1 = value;
	}

	inline static int32_t get_offset_of__caze_2() { return static_cast<int32_t>(offsetof(M_weewhirRakis206_t148398499, ____caze_2)); }
	inline uint32_t get__caze_2() const { return ____caze_2; }
	inline uint32_t* get_address_of__caze_2() { return &____caze_2; }
	inline void set__caze_2(uint32_t value)
	{
		____caze_2 = value;
	}

	inline static int32_t get_offset_of__houbalhe_3() { return static_cast<int32_t>(offsetof(M_weewhirRakis206_t148398499, ____houbalhe_3)); }
	inline uint32_t get__houbalhe_3() const { return ____houbalhe_3; }
	inline uint32_t* get_address_of__houbalhe_3() { return &____houbalhe_3; }
	inline void set__houbalhe_3(uint32_t value)
	{
		____houbalhe_3 = value;
	}

	inline static int32_t get_offset_of__boomereBina_4() { return static_cast<int32_t>(offsetof(M_weewhirRakis206_t148398499, ____boomereBina_4)); }
	inline String_t* get__boomereBina_4() const { return ____boomereBina_4; }
	inline String_t** get_address_of__boomereBina_4() { return &____boomereBina_4; }
	inline void set__boomereBina_4(String_t* value)
	{
		____boomereBina_4 = value;
		Il2CppCodeGenWriteBarrier(&____boomereBina_4, value);
	}

	inline static int32_t get_offset_of__chorvinor_5() { return static_cast<int32_t>(offsetof(M_weewhirRakis206_t148398499, ____chorvinor_5)); }
	inline float get__chorvinor_5() const { return ____chorvinor_5; }
	inline float* get_address_of__chorvinor_5() { return &____chorvinor_5; }
	inline void set__chorvinor_5(float value)
	{
		____chorvinor_5 = value;
	}

	inline static int32_t get_offset_of__jeewomirJamar_6() { return static_cast<int32_t>(offsetof(M_weewhirRakis206_t148398499, ____jeewomirJamar_6)); }
	inline float get__jeewomirJamar_6() const { return ____jeewomirJamar_6; }
	inline float* get_address_of__jeewomirJamar_6() { return &____jeewomirJamar_6; }
	inline void set__jeewomirJamar_6(float value)
	{
		____jeewomirJamar_6 = value;
	}

	inline static int32_t get_offset_of__tarzow_7() { return static_cast<int32_t>(offsetof(M_weewhirRakis206_t148398499, ____tarzow_7)); }
	inline bool get__tarzow_7() const { return ____tarzow_7; }
	inline bool* get_address_of__tarzow_7() { return &____tarzow_7; }
	inline void set__tarzow_7(bool value)
	{
		____tarzow_7 = value;
	}

	inline static int32_t get_offset_of__peatuBeeseemer_8() { return static_cast<int32_t>(offsetof(M_weewhirRakis206_t148398499, ____peatuBeeseemer_8)); }
	inline String_t* get__peatuBeeseemer_8() const { return ____peatuBeeseemer_8; }
	inline String_t** get_address_of__peatuBeeseemer_8() { return &____peatuBeeseemer_8; }
	inline void set__peatuBeeseemer_8(String_t* value)
	{
		____peatuBeeseemer_8 = value;
		Il2CppCodeGenWriteBarrier(&____peatuBeeseemer_8, value);
	}

	inline static int32_t get_offset_of__semjerfow_9() { return static_cast<int32_t>(offsetof(M_weewhirRakis206_t148398499, ____semjerfow_9)); }
	inline String_t* get__semjerfow_9() const { return ____semjerfow_9; }
	inline String_t** get_address_of__semjerfow_9() { return &____semjerfow_9; }
	inline void set__semjerfow_9(String_t* value)
	{
		____semjerfow_9 = value;
		Il2CppCodeGenWriteBarrier(&____semjerfow_9, value);
	}

	inline static int32_t get_offset_of__yallvayLaxow_10() { return static_cast<int32_t>(offsetof(M_weewhirRakis206_t148398499, ____yallvayLaxow_10)); }
	inline bool get__yallvayLaxow_10() const { return ____yallvayLaxow_10; }
	inline bool* get_address_of__yallvayLaxow_10() { return &____yallvayLaxow_10; }
	inline void set__yallvayLaxow_10(bool value)
	{
		____yallvayLaxow_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
