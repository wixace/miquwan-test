﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIButtonGenerated
struct UIButtonGenerated_t521157961;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// UIButton
struct UIButton_t179429094;
// UnityEngine.Sprite
struct Sprite_t3199167241;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_UIButton179429094.h"
#include "UnityEngine_UnityEngine_Sprite3199167241.h"

// System.Void UIButtonGenerated::.ctor()
extern "C"  void UIButtonGenerated__ctor_m2477347394 (UIButtonGenerated_t521157961 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonGenerated::UIButton_UIButton1(JSVCall,System.Int32)
extern "C"  bool UIButtonGenerated_UIButton_UIButton1_m3127044528 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonGenerated::UIButton_current(JSVCall)
extern "C"  void UIButtonGenerated_UIButton_current_m1683229989 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonGenerated::UIButton_dragHighlight(JSVCall)
extern "C"  void UIButtonGenerated_UIButton_dragHighlight_m1026824862 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonGenerated::UIButton_hoverSprite(JSVCall)
extern "C"  void UIButtonGenerated_UIButton_hoverSprite_m259821693 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonGenerated::UIButton_pressedSprite(JSVCall)
extern "C"  void UIButtonGenerated_UIButton_pressedSprite_m805398231 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonGenerated::UIButton_disabledSprite(JSVCall)
extern "C"  void UIButtonGenerated_UIButton_disabledSprite_m947692109 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonGenerated::UIButton_hoverSprite2D(JSVCall)
extern "C"  void UIButtonGenerated_UIButton_hoverSprite2D_m2904335531 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonGenerated::UIButton_pressedSprite2D(JSVCall)
extern "C"  void UIButtonGenerated_UIButton_pressedSprite2D_m3217378437 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonGenerated::UIButton_disabledSprite2D(JSVCall)
extern "C"  void UIButtonGenerated_UIButton_disabledSprite2D_m2522841723 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonGenerated::UIButton_pixelSnap(JSVCall)
extern "C"  void UIButtonGenerated_UIButton_pixelSnap_m1081192206 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonGenerated::UIButton_onClick(JSVCall)
extern "C"  void UIButtonGenerated_UIButton_onClick_m2512276917 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonGenerated::UIButton_isEnabled(JSVCall)
extern "C"  void UIButtonGenerated_UIButton_isEnabled_m1436721063 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonGenerated::UIButton_normalSprite(JSVCall)
extern "C"  void UIButtonGenerated_UIButton_normalSprite_m4175232642 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonGenerated::UIButton_normalSprite2D(JSVCall)
extern "C"  void UIButtonGenerated_UIButton_normalSprite2D_m3222906224 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonGenerated::UIButton_SetState__State__Boolean(JSVCall,System.Int32)
extern "C"  bool UIButtonGenerated_UIButton_SetState__State__Boolean_m2589736803 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonGenerated::__Register()
extern "C"  void UIButtonGenerated___Register_m295388101 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIButtonGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UIButtonGenerated_ilo_getObject1_m2337728692 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIButtonGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIButtonGenerated_ilo_setObject2_m1374607981 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIButtonGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIButtonGenerated_ilo_getObject3_m433017509 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonGenerated::ilo_setStringS4(System.Int32,System.String)
extern "C"  void UIButtonGenerated_ilo_setStringS4_m1611979150 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIButtonGenerated::ilo_getStringS5(System.Int32)
extern "C"  String_t* UIButtonGenerated_ilo_getStringS5_m2874023722 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonGenerated::ilo_get_isEnabled6(UIButton)
extern "C"  bool UIButtonGenerated_ilo_get_isEnabled6_m1547220131 (Il2CppObject * __this /* static, unused */, UIButton_t179429094 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonGenerated::ilo_getBooleanS7(System.Int32)
extern "C"  bool UIButtonGenerated_ilo_getBooleanS7_m2904154856 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonGenerated::ilo_set_normalSprite8(UIButton,System.String)
extern "C"  void UIButtonGenerated_ilo_set_normalSprite8_m1348034386 (Il2CppObject * __this /* static, unused */, UIButton_t179429094 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonGenerated::ilo_set_normalSprite2D9(UIButton,UnityEngine.Sprite)
extern "C"  void UIButtonGenerated_ilo_set_normalSprite2D9_m372850149 (Il2CppObject * __this /* static, unused */, UIButton_t179429094 * ____this0, Sprite_t3199167241 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
