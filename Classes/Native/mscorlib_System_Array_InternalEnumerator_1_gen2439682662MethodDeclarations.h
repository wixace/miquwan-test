﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2439682662.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_AstarPath_GUOSingle3657339986.h"

// System.Void System.Array/InternalEnumerator`1<AstarPath/GUOSingle>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3870581463_gshared (InternalEnumerator_1_t2439682662 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3870581463(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2439682662 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3870581463_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<AstarPath/GUOSingle>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3189087593_gshared (InternalEnumerator_1_t2439682662 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3189087593(__this, method) ((  void (*) (InternalEnumerator_1_t2439682662 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3189087593_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<AstarPath/GUOSingle>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3638426271_gshared (InternalEnumerator_1_t2439682662 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3638426271(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2439682662 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3638426271_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<AstarPath/GUOSingle>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2335194478_gshared (InternalEnumerator_1_t2439682662 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2335194478(__this, method) ((  void (*) (InternalEnumerator_1_t2439682662 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2335194478_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<AstarPath/GUOSingle>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2303598745_gshared (InternalEnumerator_1_t2439682662 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2303598745(__this, method) ((  bool (*) (InternalEnumerator_1_t2439682662 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2303598745_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<AstarPath/GUOSingle>::get_Current()
extern "C"  GUOSingle_t3657339986  InternalEnumerator_1_get_Current_m3565124160_gshared (InternalEnumerator_1_t2439682662 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3565124160(__this, method) ((  GUOSingle_t3657339986  (*) (InternalEnumerator_1_t2439682662 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3565124160_gshared)(__this, method)
