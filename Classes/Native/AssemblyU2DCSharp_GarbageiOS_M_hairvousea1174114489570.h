﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_hairvousea117
struct  M_hairvousea117_t4114489570  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_hairvousea117::_pecha
	String_t* ____pecha_0;
	// System.String GarbageiOS.M_hairvousea117::_hircer
	String_t* ____hircer_1;
	// System.Single GarbageiOS.M_hairvousea117::_jouchi
	float ____jouchi_2;
	// System.Single GarbageiOS.M_hairvousea117::_chastadoRomaje
	float ____chastadoRomaje_3;
	// System.UInt32 GarbageiOS.M_hairvousea117::_kijurji
	uint32_t ____kijurji_4;
	// System.Single GarbageiOS.M_hairvousea117::_remgele
	float ____remgele_5;
	// System.String GarbageiOS.M_hairvousea117::_ricer
	String_t* ____ricer_6;
	// System.String GarbageiOS.M_hairvousea117::_peanorVipoozee
	String_t* ____peanorVipoozee_7;
	// System.String GarbageiOS.M_hairvousea117::_fatruKipe
	String_t* ____fatruKipe_8;
	// System.UInt32 GarbageiOS.M_hairvousea117::_carra
	uint32_t ____carra_9;
	// System.Int32 GarbageiOS.M_hairvousea117::_nobou
	int32_t ____nobou_10;
	// System.Single GarbageiOS.M_hairvousea117::_sine
	float ____sine_11;
	// System.Int32 GarbageiOS.M_hairvousea117::_telfeaniDreepisja
	int32_t ____telfeaniDreepisja_12;
	// System.UInt32 GarbageiOS.M_hairvousea117::_nefa
	uint32_t ____nefa_13;
	// System.Single GarbageiOS.M_hairvousea117::_wojowwor
	float ____wojowwor_14;
	// System.String GarbageiOS.M_hairvousea117::_sisterWhistar
	String_t* ____sisterWhistar_15;
	// System.UInt32 GarbageiOS.M_hairvousea117::_semtacerQowi
	uint32_t ____semtacerQowi_16;
	// System.String GarbageiOS.M_hairvousea117::_talllear
	String_t* ____talllear_17;
	// System.Single GarbageiOS.M_hairvousea117::_gordarorHemrouyi
	float ____gordarorHemrouyi_18;
	// System.Boolean GarbageiOS.M_hairvousea117::_parmirCowjeecou
	bool ____parmirCowjeecou_19;
	// System.String GarbageiOS.M_hairvousea117::_nearpereDarno
	String_t* ____nearpereDarno_20;
	// System.UInt32 GarbageiOS.M_hairvousea117::_trairroujaiLerepuse
	uint32_t ____trairroujaiLerepuse_21;
	// System.Boolean GarbageiOS.M_hairvousea117::_bemjalmiReje
	bool ____bemjalmiReje_22;
	// System.Boolean GarbageiOS.M_hairvousea117::_fawtor
	bool ____fawtor_23;
	// System.String GarbageiOS.M_hairvousea117::_ceme
	String_t* ____ceme_24;
	// System.Int32 GarbageiOS.M_hairvousea117::_selcemkerPotanas
	int32_t ____selcemkerPotanas_25;
	// System.String GarbageiOS.M_hairvousea117::_tuhairoDearor
	String_t* ____tuhairoDearor_26;
	// System.String GarbageiOS.M_hairvousea117::_sisailou
	String_t* ____sisailou_27;
	// System.UInt32 GarbageiOS.M_hairvousea117::_nonouPiveka
	uint32_t ____nonouPiveka_28;
	// System.String GarbageiOS.M_hairvousea117::_jedreavairCaloolir
	String_t* ____jedreavairCaloolir_29;
	// System.UInt32 GarbageiOS.M_hairvousea117::_lallwouRoborer
	uint32_t ____lallwouRoborer_30;
	// System.UInt32 GarbageiOS.M_hairvousea117::_roturSteka
	uint32_t ____roturSteka_31;
	// System.String GarbageiOS.M_hairvousea117::_yevalqaSaspis
	String_t* ____yevalqaSaspis_32;
	// System.String GarbageiOS.M_hairvousea117::_noujida
	String_t* ____noujida_33;
	// System.Int32 GarbageiOS.M_hairvousea117::_kashoTice
	int32_t ____kashoTice_34;
	// System.String GarbageiOS.M_hairvousea117::_jita
	String_t* ____jita_35;
	// System.Single GarbageiOS.M_hairvousea117::_sairuSairtrewe
	float ____sairuSairtrewe_36;
	// System.Boolean GarbageiOS.M_hairvousea117::_drumay
	bool ____drumay_37;
	// System.Single GarbageiOS.M_hairvousea117::_demkawball
	float ____demkawball_38;
	// System.Boolean GarbageiOS.M_hairvousea117::_toca
	bool ____toca_39;
	// System.UInt32 GarbageiOS.M_hairvousea117::_dalcaju
	uint32_t ____dalcaju_40;
	// System.Int32 GarbageiOS.M_hairvousea117::_merniree
	int32_t ____merniree_41;
	// System.Boolean GarbageiOS.M_hairvousea117::_mertrelvur
	bool ____mertrelvur_42;
	// System.Int32 GarbageiOS.M_hairvousea117::_wiculo
	int32_t ____wiculo_43;

public:
	inline static int32_t get_offset_of__pecha_0() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____pecha_0)); }
	inline String_t* get__pecha_0() const { return ____pecha_0; }
	inline String_t** get_address_of__pecha_0() { return &____pecha_0; }
	inline void set__pecha_0(String_t* value)
	{
		____pecha_0 = value;
		Il2CppCodeGenWriteBarrier(&____pecha_0, value);
	}

	inline static int32_t get_offset_of__hircer_1() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____hircer_1)); }
	inline String_t* get__hircer_1() const { return ____hircer_1; }
	inline String_t** get_address_of__hircer_1() { return &____hircer_1; }
	inline void set__hircer_1(String_t* value)
	{
		____hircer_1 = value;
		Il2CppCodeGenWriteBarrier(&____hircer_1, value);
	}

	inline static int32_t get_offset_of__jouchi_2() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____jouchi_2)); }
	inline float get__jouchi_2() const { return ____jouchi_2; }
	inline float* get_address_of__jouchi_2() { return &____jouchi_2; }
	inline void set__jouchi_2(float value)
	{
		____jouchi_2 = value;
	}

	inline static int32_t get_offset_of__chastadoRomaje_3() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____chastadoRomaje_3)); }
	inline float get__chastadoRomaje_3() const { return ____chastadoRomaje_3; }
	inline float* get_address_of__chastadoRomaje_3() { return &____chastadoRomaje_3; }
	inline void set__chastadoRomaje_3(float value)
	{
		____chastadoRomaje_3 = value;
	}

	inline static int32_t get_offset_of__kijurji_4() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____kijurji_4)); }
	inline uint32_t get__kijurji_4() const { return ____kijurji_4; }
	inline uint32_t* get_address_of__kijurji_4() { return &____kijurji_4; }
	inline void set__kijurji_4(uint32_t value)
	{
		____kijurji_4 = value;
	}

	inline static int32_t get_offset_of__remgele_5() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____remgele_5)); }
	inline float get__remgele_5() const { return ____remgele_5; }
	inline float* get_address_of__remgele_5() { return &____remgele_5; }
	inline void set__remgele_5(float value)
	{
		____remgele_5 = value;
	}

	inline static int32_t get_offset_of__ricer_6() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____ricer_6)); }
	inline String_t* get__ricer_6() const { return ____ricer_6; }
	inline String_t** get_address_of__ricer_6() { return &____ricer_6; }
	inline void set__ricer_6(String_t* value)
	{
		____ricer_6 = value;
		Il2CppCodeGenWriteBarrier(&____ricer_6, value);
	}

	inline static int32_t get_offset_of__peanorVipoozee_7() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____peanorVipoozee_7)); }
	inline String_t* get__peanorVipoozee_7() const { return ____peanorVipoozee_7; }
	inline String_t** get_address_of__peanorVipoozee_7() { return &____peanorVipoozee_7; }
	inline void set__peanorVipoozee_7(String_t* value)
	{
		____peanorVipoozee_7 = value;
		Il2CppCodeGenWriteBarrier(&____peanorVipoozee_7, value);
	}

	inline static int32_t get_offset_of__fatruKipe_8() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____fatruKipe_8)); }
	inline String_t* get__fatruKipe_8() const { return ____fatruKipe_8; }
	inline String_t** get_address_of__fatruKipe_8() { return &____fatruKipe_8; }
	inline void set__fatruKipe_8(String_t* value)
	{
		____fatruKipe_8 = value;
		Il2CppCodeGenWriteBarrier(&____fatruKipe_8, value);
	}

	inline static int32_t get_offset_of__carra_9() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____carra_9)); }
	inline uint32_t get__carra_9() const { return ____carra_9; }
	inline uint32_t* get_address_of__carra_9() { return &____carra_9; }
	inline void set__carra_9(uint32_t value)
	{
		____carra_9 = value;
	}

	inline static int32_t get_offset_of__nobou_10() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____nobou_10)); }
	inline int32_t get__nobou_10() const { return ____nobou_10; }
	inline int32_t* get_address_of__nobou_10() { return &____nobou_10; }
	inline void set__nobou_10(int32_t value)
	{
		____nobou_10 = value;
	}

	inline static int32_t get_offset_of__sine_11() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____sine_11)); }
	inline float get__sine_11() const { return ____sine_11; }
	inline float* get_address_of__sine_11() { return &____sine_11; }
	inline void set__sine_11(float value)
	{
		____sine_11 = value;
	}

	inline static int32_t get_offset_of__telfeaniDreepisja_12() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____telfeaniDreepisja_12)); }
	inline int32_t get__telfeaniDreepisja_12() const { return ____telfeaniDreepisja_12; }
	inline int32_t* get_address_of__telfeaniDreepisja_12() { return &____telfeaniDreepisja_12; }
	inline void set__telfeaniDreepisja_12(int32_t value)
	{
		____telfeaniDreepisja_12 = value;
	}

	inline static int32_t get_offset_of__nefa_13() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____nefa_13)); }
	inline uint32_t get__nefa_13() const { return ____nefa_13; }
	inline uint32_t* get_address_of__nefa_13() { return &____nefa_13; }
	inline void set__nefa_13(uint32_t value)
	{
		____nefa_13 = value;
	}

	inline static int32_t get_offset_of__wojowwor_14() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____wojowwor_14)); }
	inline float get__wojowwor_14() const { return ____wojowwor_14; }
	inline float* get_address_of__wojowwor_14() { return &____wojowwor_14; }
	inline void set__wojowwor_14(float value)
	{
		____wojowwor_14 = value;
	}

	inline static int32_t get_offset_of__sisterWhistar_15() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____sisterWhistar_15)); }
	inline String_t* get__sisterWhistar_15() const { return ____sisterWhistar_15; }
	inline String_t** get_address_of__sisterWhistar_15() { return &____sisterWhistar_15; }
	inline void set__sisterWhistar_15(String_t* value)
	{
		____sisterWhistar_15 = value;
		Il2CppCodeGenWriteBarrier(&____sisterWhistar_15, value);
	}

	inline static int32_t get_offset_of__semtacerQowi_16() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____semtacerQowi_16)); }
	inline uint32_t get__semtacerQowi_16() const { return ____semtacerQowi_16; }
	inline uint32_t* get_address_of__semtacerQowi_16() { return &____semtacerQowi_16; }
	inline void set__semtacerQowi_16(uint32_t value)
	{
		____semtacerQowi_16 = value;
	}

	inline static int32_t get_offset_of__talllear_17() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____talllear_17)); }
	inline String_t* get__talllear_17() const { return ____talllear_17; }
	inline String_t** get_address_of__talllear_17() { return &____talllear_17; }
	inline void set__talllear_17(String_t* value)
	{
		____talllear_17 = value;
		Il2CppCodeGenWriteBarrier(&____talllear_17, value);
	}

	inline static int32_t get_offset_of__gordarorHemrouyi_18() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____gordarorHemrouyi_18)); }
	inline float get__gordarorHemrouyi_18() const { return ____gordarorHemrouyi_18; }
	inline float* get_address_of__gordarorHemrouyi_18() { return &____gordarorHemrouyi_18; }
	inline void set__gordarorHemrouyi_18(float value)
	{
		____gordarorHemrouyi_18 = value;
	}

	inline static int32_t get_offset_of__parmirCowjeecou_19() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____parmirCowjeecou_19)); }
	inline bool get__parmirCowjeecou_19() const { return ____parmirCowjeecou_19; }
	inline bool* get_address_of__parmirCowjeecou_19() { return &____parmirCowjeecou_19; }
	inline void set__parmirCowjeecou_19(bool value)
	{
		____parmirCowjeecou_19 = value;
	}

	inline static int32_t get_offset_of__nearpereDarno_20() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____nearpereDarno_20)); }
	inline String_t* get__nearpereDarno_20() const { return ____nearpereDarno_20; }
	inline String_t** get_address_of__nearpereDarno_20() { return &____nearpereDarno_20; }
	inline void set__nearpereDarno_20(String_t* value)
	{
		____nearpereDarno_20 = value;
		Il2CppCodeGenWriteBarrier(&____nearpereDarno_20, value);
	}

	inline static int32_t get_offset_of__trairroujaiLerepuse_21() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____trairroujaiLerepuse_21)); }
	inline uint32_t get__trairroujaiLerepuse_21() const { return ____trairroujaiLerepuse_21; }
	inline uint32_t* get_address_of__trairroujaiLerepuse_21() { return &____trairroujaiLerepuse_21; }
	inline void set__trairroujaiLerepuse_21(uint32_t value)
	{
		____trairroujaiLerepuse_21 = value;
	}

	inline static int32_t get_offset_of__bemjalmiReje_22() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____bemjalmiReje_22)); }
	inline bool get__bemjalmiReje_22() const { return ____bemjalmiReje_22; }
	inline bool* get_address_of__bemjalmiReje_22() { return &____bemjalmiReje_22; }
	inline void set__bemjalmiReje_22(bool value)
	{
		____bemjalmiReje_22 = value;
	}

	inline static int32_t get_offset_of__fawtor_23() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____fawtor_23)); }
	inline bool get__fawtor_23() const { return ____fawtor_23; }
	inline bool* get_address_of__fawtor_23() { return &____fawtor_23; }
	inline void set__fawtor_23(bool value)
	{
		____fawtor_23 = value;
	}

	inline static int32_t get_offset_of__ceme_24() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____ceme_24)); }
	inline String_t* get__ceme_24() const { return ____ceme_24; }
	inline String_t** get_address_of__ceme_24() { return &____ceme_24; }
	inline void set__ceme_24(String_t* value)
	{
		____ceme_24 = value;
		Il2CppCodeGenWriteBarrier(&____ceme_24, value);
	}

	inline static int32_t get_offset_of__selcemkerPotanas_25() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____selcemkerPotanas_25)); }
	inline int32_t get__selcemkerPotanas_25() const { return ____selcemkerPotanas_25; }
	inline int32_t* get_address_of__selcemkerPotanas_25() { return &____selcemkerPotanas_25; }
	inline void set__selcemkerPotanas_25(int32_t value)
	{
		____selcemkerPotanas_25 = value;
	}

	inline static int32_t get_offset_of__tuhairoDearor_26() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____tuhairoDearor_26)); }
	inline String_t* get__tuhairoDearor_26() const { return ____tuhairoDearor_26; }
	inline String_t** get_address_of__tuhairoDearor_26() { return &____tuhairoDearor_26; }
	inline void set__tuhairoDearor_26(String_t* value)
	{
		____tuhairoDearor_26 = value;
		Il2CppCodeGenWriteBarrier(&____tuhairoDearor_26, value);
	}

	inline static int32_t get_offset_of__sisailou_27() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____sisailou_27)); }
	inline String_t* get__sisailou_27() const { return ____sisailou_27; }
	inline String_t** get_address_of__sisailou_27() { return &____sisailou_27; }
	inline void set__sisailou_27(String_t* value)
	{
		____sisailou_27 = value;
		Il2CppCodeGenWriteBarrier(&____sisailou_27, value);
	}

	inline static int32_t get_offset_of__nonouPiveka_28() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____nonouPiveka_28)); }
	inline uint32_t get__nonouPiveka_28() const { return ____nonouPiveka_28; }
	inline uint32_t* get_address_of__nonouPiveka_28() { return &____nonouPiveka_28; }
	inline void set__nonouPiveka_28(uint32_t value)
	{
		____nonouPiveka_28 = value;
	}

	inline static int32_t get_offset_of__jedreavairCaloolir_29() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____jedreavairCaloolir_29)); }
	inline String_t* get__jedreavairCaloolir_29() const { return ____jedreavairCaloolir_29; }
	inline String_t** get_address_of__jedreavairCaloolir_29() { return &____jedreavairCaloolir_29; }
	inline void set__jedreavairCaloolir_29(String_t* value)
	{
		____jedreavairCaloolir_29 = value;
		Il2CppCodeGenWriteBarrier(&____jedreavairCaloolir_29, value);
	}

	inline static int32_t get_offset_of__lallwouRoborer_30() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____lallwouRoborer_30)); }
	inline uint32_t get__lallwouRoborer_30() const { return ____lallwouRoborer_30; }
	inline uint32_t* get_address_of__lallwouRoborer_30() { return &____lallwouRoborer_30; }
	inline void set__lallwouRoborer_30(uint32_t value)
	{
		____lallwouRoborer_30 = value;
	}

	inline static int32_t get_offset_of__roturSteka_31() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____roturSteka_31)); }
	inline uint32_t get__roturSteka_31() const { return ____roturSteka_31; }
	inline uint32_t* get_address_of__roturSteka_31() { return &____roturSteka_31; }
	inline void set__roturSteka_31(uint32_t value)
	{
		____roturSteka_31 = value;
	}

	inline static int32_t get_offset_of__yevalqaSaspis_32() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____yevalqaSaspis_32)); }
	inline String_t* get__yevalqaSaspis_32() const { return ____yevalqaSaspis_32; }
	inline String_t** get_address_of__yevalqaSaspis_32() { return &____yevalqaSaspis_32; }
	inline void set__yevalqaSaspis_32(String_t* value)
	{
		____yevalqaSaspis_32 = value;
		Il2CppCodeGenWriteBarrier(&____yevalqaSaspis_32, value);
	}

	inline static int32_t get_offset_of__noujida_33() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____noujida_33)); }
	inline String_t* get__noujida_33() const { return ____noujida_33; }
	inline String_t** get_address_of__noujida_33() { return &____noujida_33; }
	inline void set__noujida_33(String_t* value)
	{
		____noujida_33 = value;
		Il2CppCodeGenWriteBarrier(&____noujida_33, value);
	}

	inline static int32_t get_offset_of__kashoTice_34() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____kashoTice_34)); }
	inline int32_t get__kashoTice_34() const { return ____kashoTice_34; }
	inline int32_t* get_address_of__kashoTice_34() { return &____kashoTice_34; }
	inline void set__kashoTice_34(int32_t value)
	{
		____kashoTice_34 = value;
	}

	inline static int32_t get_offset_of__jita_35() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____jita_35)); }
	inline String_t* get__jita_35() const { return ____jita_35; }
	inline String_t** get_address_of__jita_35() { return &____jita_35; }
	inline void set__jita_35(String_t* value)
	{
		____jita_35 = value;
		Il2CppCodeGenWriteBarrier(&____jita_35, value);
	}

	inline static int32_t get_offset_of__sairuSairtrewe_36() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____sairuSairtrewe_36)); }
	inline float get__sairuSairtrewe_36() const { return ____sairuSairtrewe_36; }
	inline float* get_address_of__sairuSairtrewe_36() { return &____sairuSairtrewe_36; }
	inline void set__sairuSairtrewe_36(float value)
	{
		____sairuSairtrewe_36 = value;
	}

	inline static int32_t get_offset_of__drumay_37() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____drumay_37)); }
	inline bool get__drumay_37() const { return ____drumay_37; }
	inline bool* get_address_of__drumay_37() { return &____drumay_37; }
	inline void set__drumay_37(bool value)
	{
		____drumay_37 = value;
	}

	inline static int32_t get_offset_of__demkawball_38() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____demkawball_38)); }
	inline float get__demkawball_38() const { return ____demkawball_38; }
	inline float* get_address_of__demkawball_38() { return &____demkawball_38; }
	inline void set__demkawball_38(float value)
	{
		____demkawball_38 = value;
	}

	inline static int32_t get_offset_of__toca_39() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____toca_39)); }
	inline bool get__toca_39() const { return ____toca_39; }
	inline bool* get_address_of__toca_39() { return &____toca_39; }
	inline void set__toca_39(bool value)
	{
		____toca_39 = value;
	}

	inline static int32_t get_offset_of__dalcaju_40() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____dalcaju_40)); }
	inline uint32_t get__dalcaju_40() const { return ____dalcaju_40; }
	inline uint32_t* get_address_of__dalcaju_40() { return &____dalcaju_40; }
	inline void set__dalcaju_40(uint32_t value)
	{
		____dalcaju_40 = value;
	}

	inline static int32_t get_offset_of__merniree_41() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____merniree_41)); }
	inline int32_t get__merniree_41() const { return ____merniree_41; }
	inline int32_t* get_address_of__merniree_41() { return &____merniree_41; }
	inline void set__merniree_41(int32_t value)
	{
		____merniree_41 = value;
	}

	inline static int32_t get_offset_of__mertrelvur_42() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____mertrelvur_42)); }
	inline bool get__mertrelvur_42() const { return ____mertrelvur_42; }
	inline bool* get_address_of__mertrelvur_42() { return &____mertrelvur_42; }
	inline void set__mertrelvur_42(bool value)
	{
		____mertrelvur_42 = value;
	}

	inline static int32_t get_offset_of__wiculo_43() { return static_cast<int32_t>(offsetof(M_hairvousea117_t4114489570, ____wiculo_43)); }
	inline int32_t get__wiculo_43() const { return ____wiculo_43; }
	inline int32_t* get_address_of__wiculo_43() { return &____wiculo_43; }
	inline void set__wiculo_43(int32_t value)
	{
		____wiculo_43 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
