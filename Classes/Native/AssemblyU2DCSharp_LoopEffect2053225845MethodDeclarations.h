﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoopEffect
struct LoopEffect_t2053225845;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// EffectCtrl
struct EffectCtrl_t3708787644;
// IEffComponent
struct IEffComponent_t3802264961;
// EffectMgr
struct EffectMgr_t535289511;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_IEffComponent3802264961.h"
#include "AssemblyU2DCSharp_EffectMgr535289511.h"
#include "AssemblyU2DCSharp_EffectCtrl3708787644.h"

// System.Void LoopEffect::.ctor()
extern "C"  void LoopEffect__ctor_m480194246 (LoopEffect_t2053225845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoopEffect::OnRemove(CEvent.ZEvent)
extern "C"  void LoopEffect_OnRemove_m3931259994 (LoopEffect_t2053225845 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoopEffect::Init(UnityEngine.GameObject)
extern "C"  void LoopEffect_Init_m1228156870 (LoopEffect_t2053225845 * __this, GameObject_t3674682005 * ___effGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoopEffect::Update()
extern "C"  void LoopEffect_Update_m3727981831 (LoopEffect_t2053225845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoopEffect::Play()
extern "C"  void LoopEffect_Play_m303897586 (LoopEffect_t2053225845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoopEffect::Stop()
extern "C"  void LoopEffect_Stop_m397581632 (LoopEffect_t2053225845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoopEffect::Reset()
extern "C"  void LoopEffect_Reset_m2421594483 (LoopEffect_t2053225845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoopEffect::Clear()
extern "C"  void LoopEffect_Clear_m2181294833 (LoopEffect_t2053225845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EffectCtrl LoopEffect::ilo_get_effCtrl1(IEffComponent)
extern "C"  EffectCtrl_t3708787644 * LoopEffect_ilo_get_effCtrl1_m1698889909 (Il2CppObject * __this /* static, unused */, IEffComponent_t3802264961 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoopEffect::ilo_ResetEffectGameObject2(EffectMgr,EffectCtrl)
extern "C"  void LoopEffect_ilo_ResetEffectGameObject2_m2637105489 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, EffectCtrl_t3708787644 * ___effectObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
