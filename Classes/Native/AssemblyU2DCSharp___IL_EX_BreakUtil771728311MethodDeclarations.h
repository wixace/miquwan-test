﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Boolean __IL_EX.BreakUtil::IsBreak1(System.Int32)
extern "C"  bool BreakUtil_IsBreak1_m742320052 (Il2CppObject * __this /* static, unused */, int32_t ___value10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak2(System.Int32)
extern "C"  bool BreakUtil_IsBreak2_m2249871861 (Il2CppObject * __this /* static, unused */, int32_t ___value20, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak3(System.Int32)
extern "C"  bool BreakUtil_IsBreak3_m3757423670 (Il2CppObject * __this /* static, unused */, int32_t ___value30, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak4(System.Int32)
extern "C"  bool BreakUtil_IsBreak4_m970008183 (Il2CppObject * __this /* static, unused */, int32_t ___value40, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak5(System.Int32)
extern "C"  bool BreakUtil_IsBreak5_m2477559992 (Il2CppObject * __this /* static, unused */, int32_t ___value50, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak6(System.Int32)
extern "C"  bool BreakUtil_IsBreak6_m3985111801 (Il2CppObject * __this /* static, unused */, int32_t ___value60, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak7(System.Int32)
extern "C"  bool BreakUtil_IsBreak7_m1197696314 (Il2CppObject * __this /* static, unused */, int32_t ___value70, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak8(System.Int32)
extern "C"  bool BreakUtil_IsBreak8_m2705248123 (Il2CppObject * __this /* static, unused */, int32_t ___value80, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak9(System.Int32)
extern "C"  bool BreakUtil_IsBreak9_m4212799932 (Il2CppObject * __this /* static, unused */, int32_t ___value90, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak10(System.Int32)
extern "C"  bool BreakUtil_IsBreak10_m3606864480 (Il2CppObject * __this /* static, unused */, int32_t ___value100, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak11(System.Int32)
extern "C"  bool BreakUtil_IsBreak11_m819448993 (Il2CppObject * __this /* static, unused */, int32_t ___value110, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak12(System.Int32)
extern "C"  bool BreakUtil_IsBreak12_m2327000802 (Il2CppObject * __this /* static, unused */, int32_t ___value120, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak13(System.Int32)
extern "C"  bool BreakUtil_IsBreak13_m3834552611 (Il2CppObject * __this /* static, unused */, int32_t ___value130, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak14(System.Int32)
extern "C"  bool BreakUtil_IsBreak14_m1047137124 (Il2CppObject * __this /* static, unused */, int32_t ___value140, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak15(System.Int32)
extern "C"  bool BreakUtil_IsBreak15_m2554688933 (Il2CppObject * __this /* static, unused */, int32_t ___value150, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak16(System.Int32)
extern "C"  bool BreakUtil_IsBreak16_m4062240742 (Il2CppObject * __this /* static, unused */, int32_t ___value160, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak17(System.Int32)
extern "C"  bool BreakUtil_IsBreak17_m1274825255 (Il2CppObject * __this /* static, unused */, int32_t ___value170, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak18(System.Int32)
extern "C"  bool BreakUtil_IsBreak18_m2782377064 (Il2CppObject * __this /* static, unused */, int32_t ___value180, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak19(System.Int32)
extern "C"  bool BreakUtil_IsBreak19_m4289928873 (Il2CppObject * __this /* static, unused */, int32_t ___value190, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak20(System.Int32)
extern "C"  bool BreakUtil_IsBreak20_m3096330303 (Il2CppObject * __this /* static, unused */, int32_t ___value200, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak21(System.Int32)
extern "C"  bool BreakUtil_IsBreak21_m308914816 (Il2CppObject * __this /* static, unused */, int32_t ___value210, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak22(System.Int32)
extern "C"  bool BreakUtil_IsBreak22_m1816466625 (Il2CppObject * __this /* static, unused */, int32_t ___value220, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak23(System.Int32)
extern "C"  bool BreakUtil_IsBreak23_m3324018434 (Il2CppObject * __this /* static, unused */, int32_t ___value230, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak24(System.Int32)
extern "C"  bool BreakUtil_IsBreak24_m536602947 (Il2CppObject * __this /* static, unused */, int32_t ___value240, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak25(System.Int32)
extern "C"  bool BreakUtil_IsBreak25_m2044154756 (Il2CppObject * __this /* static, unused */, int32_t ___value250, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak26(System.Int32)
extern "C"  bool BreakUtil_IsBreak26_m3551706565 (Il2CppObject * __this /* static, unused */, int32_t ___value260, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak27(System.Int32)
extern "C"  bool BreakUtil_IsBreak27_m764291078 (Il2CppObject * __this /* static, unused */, int32_t ___value270, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak28(System.Int32)
extern "C"  bool BreakUtil_IsBreak28_m2271842887 (Il2CppObject * __this /* static, unused */, int32_t ___value280, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak29(System.Int32)
extern "C"  bool BreakUtil_IsBreak29_m3779394696 (Il2CppObject * __this /* static, unused */, int32_t ___value290, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak30(System.Int32)
extern "C"  bool BreakUtil_IsBreak30_m2585796126 (Il2CppObject * __this /* static, unused */, int32_t ___value300, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak31(System.Int32)
extern "C"  bool BreakUtil_IsBreak31_m4093347935 (Il2CppObject * __this /* static, unused */, int32_t ___value310, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak32(System.Int32)
extern "C"  bool BreakUtil_IsBreak32_m1305932448 (Il2CppObject * __this /* static, unused */, int32_t ___value320, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak33(System.Int32)
extern "C"  bool BreakUtil_IsBreak33_m2813484257 (Il2CppObject * __this /* static, unused */, int32_t ___value330, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak34(System.Int32)
extern "C"  bool BreakUtil_IsBreak34_m26068770 (Il2CppObject * __this /* static, unused */, int32_t ___value340, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak35(System.Int32)
extern "C"  bool BreakUtil_IsBreak35_m1533620579 (Il2CppObject * __this /* static, unused */, int32_t ___value350, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak36(System.Int32)
extern "C"  bool BreakUtil_IsBreak36_m3041172388 (Il2CppObject * __this /* static, unused */, int32_t ___value360, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak37(System.Int32)
extern "C"  bool BreakUtil_IsBreak37_m253756901 (Il2CppObject * __this /* static, unused */, int32_t ___value370, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak38(System.Int32)
extern "C"  bool BreakUtil_IsBreak38_m1761308710 (Il2CppObject * __this /* static, unused */, int32_t ___value380, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak39(System.Int32)
extern "C"  bool BreakUtil_IsBreak39_m3268860519 (Il2CppObject * __this /* static, unused */, int32_t ___value390, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak1(System.Single)
extern "C"  bool BreakUtil_IsBreak1_m1675107240 (Il2CppObject * __this /* static, unused */, float ___value10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak2(System.Single)
extern "C"  bool BreakUtil_IsBreak2_m1164573063 (Il2CppObject * __this /* static, unused */, float ___value20, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak3(System.Single)
extern "C"  bool BreakUtil_IsBreak3_m654038886 (Il2CppObject * __this /* static, unused */, float ___value30, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak4(System.Single)
extern "C"  bool BreakUtil_IsBreak4_m143504709 (Il2CppObject * __this /* static, unused */, float ___value40, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak5(System.Single)
extern "C"  bool BreakUtil_IsBreak5_m3927937828 (Il2CppObject * __this /* static, unused */, float ___value50, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak6(System.Single)
extern "C"  bool BreakUtil_IsBreak6_m3417403651 (Il2CppObject * __this /* static, unused */, float ___value60, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak7(System.Single)
extern "C"  bool BreakUtil_IsBreak7_m2906869474 (Il2CppObject * __this /* static, unused */, float ___value70, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak8(System.Single)
extern "C"  bool BreakUtil_IsBreak8_m2396335297 (Il2CppObject * __this /* static, unused */, float ___value80, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak9(System.Single)
extern "C"  bool BreakUtil_IsBreak9_m1885801120 (Il2CppObject * __this /* static, unused */, float ___value90, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak10(System.Single)
extern "C"  bool BreakUtil_IsBreak10_m281671292 (Il2CppObject * __this /* static, unused */, float ___value100, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak11(System.Single)
extern "C"  bool BreakUtil_IsBreak11_m4066104411 (Il2CppObject * __this /* static, unused */, float ___value110, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak12(System.Single)
extern "C"  bool BreakUtil_IsBreak12_m3555570234 (Il2CppObject * __this /* static, unused */, float ___value120, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak13(System.Single)
extern "C"  bool BreakUtil_IsBreak13_m3045036057 (Il2CppObject * __this /* static, unused */, float ___value130, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak14(System.Single)
extern "C"  bool BreakUtil_IsBreak14_m2534501880 (Il2CppObject * __this /* static, unused */, float ___value140, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak15(System.Single)
extern "C"  bool BreakUtil_IsBreak15_m2023967703 (Il2CppObject * __this /* static, unused */, float ___value150, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak16(System.Single)
extern "C"  bool BreakUtil_IsBreak16_m1513433526 (Il2CppObject * __this /* static, unused */, float ___value160, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak17(System.Single)
extern "C"  bool BreakUtil_IsBreak17_m1002899349 (Il2CppObject * __this /* static, unused */, float ___value170, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak18(System.Single)
extern "C"  bool BreakUtil_IsBreak18_m492365172 (Il2CppObject * __this /* static, unused */, float ___value180, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak19(System.Single)
extern "C"  bool BreakUtil_IsBreak19_m4276798291 (Il2CppObject * __this /* static, unused */, float ___value190, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak20(System.Single)
extern "C"  bool BreakUtil_IsBreak20_m1634980989 (Il2CppObject * __this /* static, unused */, float ___value200, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak21(System.Single)
extern "C"  bool BreakUtil_IsBreak21_m1124446812 (Il2CppObject * __this /* static, unused */, float ___value210, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak22(System.Single)
extern "C"  bool BreakUtil_IsBreak22_m613912635 (Il2CppObject * __this /* static, unused */, float ___value220, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak23(System.Single)
extern "C"  bool BreakUtil_IsBreak23_m103378458 (Il2CppObject * __this /* static, unused */, float ___value230, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak24(System.Single)
extern "C"  bool BreakUtil_IsBreak24_m3887811577 (Il2CppObject * __this /* static, unused */, float ___value240, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak25(System.Single)
extern "C"  bool BreakUtil_IsBreak25_m3377277400 (Il2CppObject * __this /* static, unused */, float ___value250, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak26(System.Single)
extern "C"  bool BreakUtil_IsBreak26_m2866743223 (Il2CppObject * __this /* static, unused */, float ___value260, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak27(System.Single)
extern "C"  bool BreakUtil_IsBreak27_m2356209046 (Il2CppObject * __this /* static, unused */, float ___value270, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak28(System.Single)
extern "C"  bool BreakUtil_IsBreak28_m1845674869 (Il2CppObject * __this /* static, unused */, float ___value280, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak29(System.Single)
extern "C"  bool BreakUtil_IsBreak29_m1335140692 (Il2CppObject * __this /* static, unused */, float ___value290, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak30(System.Single)
extern "C"  bool BreakUtil_IsBreak30_m2988290686 (Il2CppObject * __this /* static, unused */, float ___value300, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak31(System.Single)
extern "C"  bool BreakUtil_IsBreak31_m2477756509 (Il2CppObject * __this /* static, unused */, float ___value310, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak32(System.Single)
extern "C"  bool BreakUtil_IsBreak32_m1967222332 (Il2CppObject * __this /* static, unused */, float ___value320, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak33(System.Single)
extern "C"  bool BreakUtil_IsBreak33_m1456688155 (Il2CppObject * __this /* static, unused */, float ___value330, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak34(System.Single)
extern "C"  bool BreakUtil_IsBreak34_m946153978 (Il2CppObject * __this /* static, unused */, float ___value340, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak35(System.Single)
extern "C"  bool BreakUtil_IsBreak35_m435619801 (Il2CppObject * __this /* static, unused */, float ___value350, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak36(System.Single)
extern "C"  bool BreakUtil_IsBreak36_m4220052920 (Il2CppObject * __this /* static, unused */, float ___value360, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak37(System.Single)
extern "C"  bool BreakUtil_IsBreak37_m3709518743 (Il2CppObject * __this /* static, unused */, float ___value370, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak38(System.Single)
extern "C"  bool BreakUtil_IsBreak38_m3198984566 (Il2CppObject * __this /* static, unused */, float ___value380, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak39(System.Single)
extern "C"  bool BreakUtil_IsBreak39_m2688450389 (Il2CppObject * __this /* static, unused */, float ___value390, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak1(System.String)
extern "C"  bool BreakUtil_IsBreak1_m1993783551 (Il2CppObject * __this /* static, unused */, String_t* ___value10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak2(System.String)
extern "C"  bool BreakUtil_IsBreak2_m1483249374 (Il2CppObject * __this /* static, unused */, String_t* ___value20, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak3(System.String)
extern "C"  bool BreakUtil_IsBreak3_m972715197 (Il2CppObject * __this /* static, unused */, String_t* ___value30, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak4(System.String)
extern "C"  bool BreakUtil_IsBreak4_m462181020 (Il2CppObject * __this /* static, unused */, String_t* ___value40, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak5(System.String)
extern "C"  bool BreakUtil_IsBreak5_m4246614139 (Il2CppObject * __this /* static, unused */, String_t* ___value50, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak6(System.String)
extern "C"  bool BreakUtil_IsBreak6_m3736079962 (Il2CppObject * __this /* static, unused */, String_t* ___value60, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak7(System.String)
extern "C"  bool BreakUtil_IsBreak7_m3225545785 (Il2CppObject * __this /* static, unused */, String_t* ___value70, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean __IL_EX.BreakUtil::IsBreak8(System.String)
extern "C"  bool BreakUtil_IsBreak8_m2715011608 (Il2CppObject * __this /* static, unused */, String_t* ___value80, const MethodInfo* method) IL2CPP_METHOD_ATTR;
