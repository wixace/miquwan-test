﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginYueplay
struct PluginYueplay_t3258858666;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionInfo
struct VersionInfo_t2356638086;
// VersionMgr
struct VersionMgr_t1322950208;
// FloatTextMgr
struct FloatTextMgr_t630384591;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginYueplay3258858666.h"
#include "AssemblyU2DCSharp_FloatTextMgr630384591.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"

// System.Void PluginYueplay::.ctor()
extern "C"  void PluginYueplay__ctor_m965002241 (PluginYueplay_t3258858666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::Init()
extern "C"  void PluginYueplay_Init_m1645233715 (PluginYueplay_t3258858666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginYueplay::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginYueplay_ReqSDKHttpLogin_m3532837042 (PluginYueplay_t3258858666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYueplay::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginYueplay_IsLoginSuccess_m188841802 (PluginYueplay_t3258858666 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::OpenUserLogin()
extern "C"  void PluginYueplay_OpenUserLogin_m2826851667 (PluginYueplay_t3258858666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::UserPay(CEvent.ZEvent)
extern "C"  void PluginYueplay_UserPay_m3037539007 (PluginYueplay_t3258858666 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::EnterGame(CEvent.ZEvent)
extern "C"  void PluginYueplay_EnterGame_m713771154 (PluginYueplay_t3258858666 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::CreatRole(CEvent.ZEvent)
extern "C"  void PluginYueplay_CreatRole_m1543187005 (PluginYueplay_t3258858666 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::RoleUpdate(CEvent.ZEvent)
extern "C"  void PluginYueplay_RoleUpdate_m998564601 (PluginYueplay_t3258858666 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::SignCallBack(System.Boolean,System.String)
extern "C"  void PluginYueplay_SignCallBack_m4002843608 (PluginYueplay_t3258858666 * __this, bool ___success0, String_t* ___arg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginYueplay::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginYueplay_BuildOrderParam2WWWForm_m1449737794 (PluginYueplay_t3258858666 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::OnLoginSuccess(System.String)
extern "C"  void PluginYueplay_OnLoginSuccess_m1856590918 (PluginYueplay_t3258858666 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::OnLoginFail(System.String)
extern "C"  void PluginYueplay_OnLoginFail_m3901747899 (PluginYueplay_t3258858666 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::GameLoginSuccess(CEvent.ZEvent)
extern "C"  void PluginYueplay_GameLoginSuccess_m1792862284 (PluginYueplay_t3258858666 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::JsonParse(System.String)
extern "C"  void PluginYueplay_JsonParse_m3620914488 (PluginYueplay_t3258858666 * __this, String_t* ___arg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::OnUserSwitchSuccess(System.String)
extern "C"  void PluginYueplay_OnUserSwitchSuccess_m83812894 (PluginYueplay_t3258858666 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::OnPaySuccess(System.String)
extern "C"  void PluginYueplay_OnPaySuccess_m910430213 (PluginYueplay_t3258858666 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::OnPayFail(System.String)
extern "C"  void PluginYueplay_OnPayFail_m3247040348 (PluginYueplay_t3258858666 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::OnLogoutSuccess(System.String)
extern "C"  void PluginYueplay_OnLogoutSuccess_m1723286281 (PluginYueplay_t3258858666 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::SceneLoad(CEvent.ZEvent)
extern "C"  void PluginYueplay_SceneLoad_m2845332202 (PluginYueplay_t3258858666 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::OnLogout(System.String)
extern "C"  void PluginYueplay_OnLogout_m1413317718 (PluginYueplay_t3258858666 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::initSdk(System.String,System.String)
extern "C"  void PluginYueplay_initSdk_m1657442773 (PluginYueplay_t3258858666 * __this, String_t* ___appkey0, String_t* ___appsecret1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::login()
extern "C"  void PluginYueplay_login_m487017064 (PluginYueplay_t3258858666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::logout()
extern "C"  void PluginYueplay_logout_m2218449805 (PluginYueplay_t3258858666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::confirmlogin(System.String,System.String,System.String)
extern "C"  void PluginYueplay_confirmlogin_m1603409806 (PluginYueplay_t3258858666 * __this, String_t* ___accesstoken0, String_t* ___userid1, String_t* ___username2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::enterGame(System.Int32,System.String,System.Int32,System.Int32,System.String,System.String,System.String,System.String)
extern "C"  void PluginYueplay_enterGame_m204997638 (PluginYueplay_t3258858666 * __this, int32_t ___roleId0, String_t* ___roleName1, int32_t ___roleLv2, int32_t ___serverId3, String_t* ___serverName4, String_t* ___Balance5, String_t* ___VipLevel6, String_t* ___unionName7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::creatRole(System.Int32,System.String,System.Int32,System.Int32,System.String,System.String,System.String,System.String)
extern "C"  void PluginYueplay_creatRole_m1816429819 (PluginYueplay_t3258858666 * __this, int32_t ___roleId0, String_t* ___roleName1, int32_t ___roleLv2, int32_t ___serverId3, String_t* ___serverName4, String_t* ___Balance5, String_t* ___VipLevel6, String_t* ___unionName7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::roleUpdate(System.Int32,System.String,System.Int32,System.Int32,System.String,System.String,System.String,System.String)
extern "C"  void PluginYueplay_roleUpdate_m2136230015 (PluginYueplay_t3258858666 * __this, int32_t ___roleId0, String_t* ___roleName1, int32_t ___roleLv2, int32_t ___serverId3, String_t* ___serverName4, String_t* ___Balance5, String_t* ___VipLevel6, String_t* ___unionName7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::pay(System.String,System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.Int32,System.String,System.String)
extern "C"  void PluginYueplay_pay_m1040544299 (PluginYueplay_t3258858666 * __this, String_t* ___OrderId0, int32_t ___ZfAmount1, String_t* ___producttype2, String_t* ___ProductName3, String_t* ___extra4, String_t* ___NotifyUrl5, String_t* ___RoleId6, String_t* ___ServerName7, int32_t ___ServerId8, String_t* ___RoleName9, String_t* ___ProductId10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::activeInitializeIosUI()
extern "C"  void PluginYueplay_activeInitializeIosUI_m3954797642 (PluginYueplay_t3258858666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::<OnLogout>m__47E()
extern "C"  void PluginYueplay_U3COnLogoutU3Em__47E_m2777694705 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginYueplay_ilo_AddEventListener1_m648523987 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginYueplay::ilo_get_currentVS2(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginYueplay_ilo_get_currentVS2_m3970693006 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::ilo_initSdk3(PluginYueplay,System.String,System.String)
extern "C"  void PluginYueplay_ilo_initSdk3_m2913005825 (Il2CppObject * __this /* static, unused */, PluginYueplay_t3258858666 * ____this0, String_t* ___appkey1, String_t* ___appsecret2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYueplay::ilo_get_isSdkLogin4(VersionMgr)
extern "C"  bool PluginYueplay_ilo_get_isSdkLogin4_m510158578 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginYueplay::ilo_get_Instance5()
extern "C"  VersionMgr_t1322950208 * PluginYueplay_ilo_get_Instance5_m3538183776 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::ilo_OpenUserLogin6(PluginYueplay)
extern "C"  void PluginYueplay_ilo_OpenUserLogin6_m744725616 (Il2CppObject * __this /* static, unused */, PluginYueplay_t3258858666 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::ilo_login7(PluginYueplay)
extern "C"  void PluginYueplay_ilo_login7_m695100324 (Il2CppObject * __this /* static, unused */, PluginYueplay_t3258858666 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::ilo_FloatText8(FloatTextMgr,FLOAT_TEXT_ID,System.Int32,System.Object[])
extern "C"  void PluginYueplay_ilo_FloatText8_m1167207595 (Il2CppObject * __this /* static, unused */, FloatTextMgr_t630384591 * ____this0, int32_t ___textId1, int32_t ___offsetY2, ObjectU5BU5D_t1108656482* ___args3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::ilo_creatRole9(PluginYueplay,System.Int32,System.String,System.Int32,System.Int32,System.String,System.String,System.String,System.String)
extern "C"  void PluginYueplay_ilo_creatRole9_m456839755 (Il2CppObject * __this /* static, unused */, PluginYueplay_t3258858666 * ____this0, int32_t ___roleId1, String_t* ___roleName2, int32_t ___roleLv3, int32_t ___serverId4, String_t* ___serverName5, String_t* ___Balance6, String_t* ___VipLevel7, String_t* ___unionName8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken PluginYueplay::ilo_get_Item10(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * PluginYueplay_ilo_get_Item10_m1806242770 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginYueplay::ilo_get_PluginsSdkMgr11()
extern "C"  PluginsSdkMgr_t3884624670 * PluginYueplay_ilo_get_PluginsSdkMgr11_m3101194102 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::ilo_Log12(System.Object,System.Boolean)
extern "C"  void PluginYueplay_ilo_Log12_m235553690 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken PluginYueplay::ilo_get_Item13(Newtonsoft.Json.Linq.JToken,System.Object)
extern "C"  JToken_t3412245951 * PluginYueplay_ilo_get_Item13_m2018375279 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, Il2CppObject * ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::ilo_logout14(PluginYueplay)
extern "C"  void PluginYueplay_ilo_logout14_m1517308869 (Il2CppObject * __this /* static, unused */, PluginYueplay_t3258858666 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYueplay::ilo_activeInitializeIosUI15(PluginYueplay)
extern "C"  void PluginYueplay_ilo_activeInitializeIosUI15_m771665101 (Il2CppObject * __this /* static, unused */, PluginYueplay_t3258858666 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
