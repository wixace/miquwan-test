﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIWidgetGenerated/<UIWidget_mOnRender_GetDelegate_member2_arg0>c__AnonStoreyD6
struct U3CUIWidget_mOnRender_GetDelegate_member2_arg0U3Ec__AnonStoreyD6_t1115858176;
// UnityEngine.Material
struct Material_t3870600107;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"

// System.Void UIWidgetGenerated/<UIWidget_mOnRender_GetDelegate_member2_arg0>c__AnonStoreyD6::.ctor()
extern "C"  void U3CUIWidget_mOnRender_GetDelegate_member2_arg0U3Ec__AnonStoreyD6__ctor_m2039482267 (U3CUIWidget_mOnRender_GetDelegate_member2_arg0U3Ec__AnonStoreyD6_t1115858176 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated/<UIWidget_mOnRender_GetDelegate_member2_arg0>c__AnonStoreyD6::<>m__171(UnityEngine.Material)
extern "C"  void U3CUIWidget_mOnRender_GetDelegate_member2_arg0U3Ec__AnonStoreyD6_U3CU3Em__171_m3211473707 (U3CUIWidget_mOnRender_GetDelegate_member2_arg0U3Ec__AnonStoreyD6_t1115858176 * __this, Material_t3870600107 * ___mat0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
