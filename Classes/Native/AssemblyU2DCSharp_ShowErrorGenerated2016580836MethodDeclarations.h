﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShowErrorGenerated
struct ShowErrorGenerated_t2016580836;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"

// System.Void ShowErrorGenerated::.ctor()
extern "C"  void ShowErrorGenerated__ctor_m2130881591 (ShowErrorGenerated_t2016580836 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ShowErrorGenerated::ShowError_ShowError1(JSVCall,System.Int32)
extern "C"  bool ShowErrorGenerated_ShowError_ShowError1_m1041820875 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowErrorGenerated::ShowError_isShow(JSVCall)
extern "C"  void ShowErrorGenerated_ShowError_isShow_m103092799 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ShowErrorGenerated::ShowError_AddShow__String(JSVCall,System.Int32)
extern "C"  bool ShowErrorGenerated_ShowError_AddShow__String_m491544044 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowErrorGenerated::__Register()
extern "C"  void ShowErrorGenerated___Register_m3611613936 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ShowErrorGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t ShowErrorGenerated_ilo_getObject1_m1934024635 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ShowErrorGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool ShowErrorGenerated_ilo_attachFinalizerObject2_m3610049481 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ShowErrorGenerated::ilo_getBooleanS3(System.Int32)
extern "C"  bool ShowErrorGenerated_ilo_getBooleanS3_m991901239 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ShowErrorGenerated::ilo_AddShow4(System.String)
extern "C"  void ShowErrorGenerated_ilo_AddShow4_m3488336620 (Il2CppObject * __this /* static, unused */, String_t* ___log0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
