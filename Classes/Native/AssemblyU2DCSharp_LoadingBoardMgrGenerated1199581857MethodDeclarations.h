﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadingBoardMgrGenerated
struct LoadingBoardMgrGenerated_t1199581857;
// JSVCall
struct JSVCall_t3708497963;
// System.Action
struct Action_t3771233898;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.String
struct String_t;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// LoadingBoardMgr
struct LoadingBoardMgr_t303632014;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_LoadingBoardMgr303632014.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void LoadingBoardMgrGenerated::.ctor()
extern "C"  void LoadingBoardMgrGenerated__ctor_m1484835098 (LoadingBoardMgrGenerated_t1199581857 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadingBoardMgrGenerated::LoadingBoardMgr_LoadingBoardMgr1(JSVCall,System.Int32)
extern "C"  bool LoadingBoardMgrGenerated_LoadingBoardMgr_LoadingBoardMgr1_m2290395010 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_checkUpdate(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_checkUpdate_m3638598607 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_update(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_update_m1269762947 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_unzipAss(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_unzipAss_m2210919763 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_moveAss(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_moveAss_m3038421680 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_loadJs(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_loadJs_m2066038333 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_initAss(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_initAss_m1449256303 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_loadCfg(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_loadCfg_m1781079490 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_initCfg(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_initCfg_m1656147692 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_AssetMgrInit(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_AssetMgrInit_m3306814036 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_versionMsg(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_versionMsg_m4078425539 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_readCfg(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_readCfg_m822225522 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_downError(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_downError_m1973027674 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_downing(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_downing_m3813402016 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_ProgressBar_sdr(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_ProgressBar_sdr_m603051416 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_ForeGround_sp(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_ForeGround_sp_m3902058407 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_Thumb_sp(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_Thumb_sp_m2574248422 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_Percent_txt(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_Percent_txt_m3426807306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_ClientVer_txt(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_ClientVer_txt_m859295447 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_mTransform(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_mTransform_m3443308685 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_anitTransform(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_anitTransform_m1954830444 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_BGTweenAlpha(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_BGTweenAlpha_m2260490452 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::LoadingBoardMgr_Instance(JSVCall)
extern "C"  void LoadingBoardMgrGenerated_LoadingBoardMgr_Instance_m2623202199 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadingBoardMgrGenerated::LoadingBoardMgr_Clear(JSVCall,System.Int32)
extern "C"  bool LoadingBoardMgrGenerated_LoadingBoardMgr_Clear_m3056798960 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadingBoardMgrGenerated::LoadingBoardMgr_Close(JSVCall,System.Int32)
extern "C"  bool LoadingBoardMgrGenerated_LoadingBoardMgr_Close_m3551351387 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadingBoardMgrGenerated::LoadingBoardMgr_OnAwake__GameObject(JSVCall,System.Int32)
extern "C"  bool LoadingBoardMgrGenerated_LoadingBoardMgr_OnAwake__GameObject_m2473568090 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadingBoardMgrGenerated::LoadingBoardMgr_OnAwakeAAS__GameObject(JSVCall,System.Int32)
extern "C"  bool LoadingBoardMgrGenerated_LoadingBoardMgr_OnAwakeAAS__GameObject_m3326567773 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadingBoardMgrGenerated::LoadingBoardMgr_OnDestroy(JSVCall,System.Int32)
extern "C"  bool LoadingBoardMgrGenerated_LoadingBoardMgr_OnDestroy_m2234372158 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action LoadingBoardMgrGenerated::LoadingBoardMgr_OpenDialog_GetDelegate_member5_arg0(CSRepresentedObject)
extern "C"  Action_t3771233898 * LoadingBoardMgrGenerated_LoadingBoardMgr_OpenDialog_GetDelegate_member5_arg0_m3073201316 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadingBoardMgrGenerated::LoadingBoardMgr_OpenDialog__Action(JSVCall,System.Int32)
extern "C"  bool LoadingBoardMgrGenerated_LoadingBoardMgr_OpenDialog__Action_m2319046503 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadingBoardMgrGenerated::LoadingBoardMgr_Reset(JSVCall,System.Int32)
extern "C"  bool LoadingBoardMgrGenerated_LoadingBoardMgr_Reset_m3289343602 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadingBoardMgrGenerated::LoadingBoardMgr_SetAASAlpha__Single(JSVCall,System.Int32)
extern "C"  bool LoadingBoardMgrGenerated_LoadingBoardMgr_SetAASAlpha__Single_m1384372152 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadingBoardMgrGenerated::LoadingBoardMgr_SetDepth__Int32(JSVCall,System.Int32)
extern "C"  bool LoadingBoardMgrGenerated_LoadingBoardMgr_SetDepth__Int32_m4013219856 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadingBoardMgrGenerated::LoadingBoardMgr_SetDescri__String(JSVCall,System.Int32)
extern "C"  bool LoadingBoardMgrGenerated_LoadingBoardMgr_SetDescri__String_m2723358398 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadingBoardMgrGenerated::LoadingBoardMgr_SetPercent__Single__String(JSVCall,System.Int32)
extern "C"  bool LoadingBoardMgrGenerated_LoadingBoardMgr_SetPercent__Single__String_m3038155227 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadingBoardMgrGenerated::LoadingBoardMgr_SetVer__String(JSVCall,System.Int32)
extern "C"  bool LoadingBoardMgrGenerated_LoadingBoardMgr_SetVer__String_m3782987121 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadingBoardMgrGenerated::LoadingBoardMgr_Show(JSVCall,System.Int32)
extern "C"  bool LoadingBoardMgrGenerated_LoadingBoardMgr_Show_m1971867836 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadingBoardMgrGenerated::LoadingBoardMgr_ShowAAS(JSVCall,System.Int32)
extern "C"  bool LoadingBoardMgrGenerated_LoadingBoardMgr_ShowAAS_m4092772729 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadingBoardMgrGenerated::LoadingBoardMgr_ShowAASEnd(JSVCall,System.Int32)
extern "C"  bool LoadingBoardMgrGenerated_LoadingBoardMgr_ShowAASEnd_m1658501668 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::__Register()
extern "C"  void LoadingBoardMgrGenerated___Register_m2274644973 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action LoadingBoardMgrGenerated::<LoadingBoardMgr_OpenDialog__Action>m__6D()
extern "C"  Action_t3771233898 * LoadingBoardMgrGenerated_U3CLoadingBoardMgr_OpenDialog__ActionU3Em__6D_m1368952556 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LoadingBoardMgrGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t LoadingBoardMgrGenerated_ilo_getObject1_m1698397240 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::ilo_setStringS2(System.Int32,System.String)
extern "C"  void LoadingBoardMgrGenerated_ilo_setStringS2_m1675885412 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 LoadingBoardMgrGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t LoadingBoardMgrGenerated_ilo_setObject3_m3581838386 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object LoadingBoardMgrGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * LoadingBoardMgrGenerated_ilo_getObject4_m1081565848 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::ilo_Clear5(LoadingBoardMgr)
extern "C"  void LoadingBoardMgrGenerated_ilo_Clear5_m1381011743 (Il2CppObject * __this /* static, unused */, LoadingBoardMgr_t303632014 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::ilo_OnAwake6(LoadingBoardMgr,UnityEngine.GameObject)
extern "C"  void LoadingBoardMgrGenerated_ilo_OnAwake6_m772157191 (Il2CppObject * __this /* static, unused */, LoadingBoardMgr_t303632014 * ____this0, GameObject_t3674682005 * ___viewGO1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::ilo_SetAASAlpha7(LoadingBoardMgr,System.Single)
extern "C"  void LoadingBoardMgrGenerated_ilo_SetAASAlpha7_m346570626 (Il2CppObject * __this /* static, unused */, LoadingBoardMgr_t303632014 * ____this0, float ___alpha1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single LoadingBoardMgrGenerated::ilo_getSingle8(System.Int32)
extern "C"  float LoadingBoardMgrGenerated_ilo_getSingle8_m1253175316 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LoadingBoardMgrGenerated::ilo_getStringS9(System.Int32)
extern "C"  String_t* LoadingBoardMgrGenerated_ilo_getStringS9_m175415136 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::ilo_SetPercent10(LoadingBoardMgr,System.Single,System.String)
extern "C"  void LoadingBoardMgrGenerated_ilo_SetPercent10_m1793191654 (Il2CppObject * __this /* static, unused */, LoadingBoardMgr_t303632014 * ____this0, float ___value1, String_t* ___msg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::ilo_ShowAAS11(LoadingBoardMgr)
extern "C"  void LoadingBoardMgrGenerated_ilo_ShowAAS11_m2967938793 (Il2CppObject * __this /* static, unused */, LoadingBoardMgr_t303632014 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadingBoardMgrGenerated::ilo_ShowAASEnd12(LoadingBoardMgr)
extern "C"  void LoadingBoardMgrGenerated_ilo_ShowAASEnd12_m2468671393 (Il2CppObject * __this /* static, unused */, LoadingBoardMgr_t303632014 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
