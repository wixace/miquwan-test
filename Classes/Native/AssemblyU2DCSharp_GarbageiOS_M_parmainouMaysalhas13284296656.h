﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_parmainouMaysalhas144
struct  M_parmainouMaysalhas144_t3284296656  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_parmainouMaysalhas144::_dixarfalCehe
	String_t* ____dixarfalCehe_0;
	// System.Single GarbageiOS.M_parmainouMaysalhas144::_jajofasFarkevear
	float ____jajofasFarkevear_1;
	// System.Int32 GarbageiOS.M_parmainouMaysalhas144::_kaballGidefe
	int32_t ____kaballGidefe_2;
	// System.UInt32 GarbageiOS.M_parmainouMaysalhas144::_tralalGourirhear
	uint32_t ____tralalGourirhear_3;
	// System.String GarbageiOS.M_parmainouMaysalhas144::_kurkisgawYapow
	String_t* ____kurkisgawYapow_4;
	// System.Int32 GarbageiOS.M_parmainouMaysalhas144::_searle
	int32_t ____searle_5;
	// System.String GarbageiOS.M_parmainouMaysalhas144::_bairqaljisSearlou
	String_t* ____bairqaljisSearlou_6;
	// System.Int32 GarbageiOS.M_parmainouMaysalhas144::_lavisyir
	int32_t ____lavisyir_7;
	// System.String GarbageiOS.M_parmainouMaysalhas144::_rawhouwe
	String_t* ____rawhouwe_8;
	// System.Single GarbageiOS.M_parmainouMaysalhas144::_pesurtu
	float ____pesurtu_9;
	// System.Single GarbageiOS.M_parmainouMaysalhas144::_cirarne
	float ____cirarne_10;
	// System.Int32 GarbageiOS.M_parmainouMaysalhas144::_radooxa
	int32_t ____radooxa_11;
	// System.String GarbageiOS.M_parmainouMaysalhas144::_kelvelerCheacasme
	String_t* ____kelvelerCheacasme_12;
	// System.Boolean GarbageiOS.M_parmainouMaysalhas144::_jurwere
	bool ____jurwere_13;
	// System.UInt32 GarbageiOS.M_parmainouMaysalhas144::_paidaNasri
	uint32_t ____paidaNasri_14;
	// System.UInt32 GarbageiOS.M_parmainouMaysalhas144::_keenelCelweno
	uint32_t ____keenelCelweno_15;
	// System.String GarbageiOS.M_parmainouMaysalhas144::_tobaldoo
	String_t* ____tobaldoo_16;
	// System.Boolean GarbageiOS.M_parmainouMaysalhas144::_nayse
	bool ____nayse_17;
	// System.Int32 GarbageiOS.M_parmainouMaysalhas144::_lasnir
	int32_t ____lasnir_18;
	// System.Int32 GarbageiOS.M_parmainouMaysalhas144::_tomerseaJeltoo
	int32_t ____tomerseaJeltoo_19;
	// System.UInt32 GarbageiOS.M_parmainouMaysalhas144::_macerexel
	uint32_t ____macerexel_20;
	// System.Boolean GarbageiOS.M_parmainouMaysalhas144::_wearserfoYuteepi
	bool ____wearserfoYuteepi_21;
	// System.Int32 GarbageiOS.M_parmainouMaysalhas144::_pallrariNearwafem
	int32_t ____pallrariNearwafem_22;
	// System.Single GarbageiOS.M_parmainouMaysalhas144::_stewhutallYeawa
	float ____stewhutallYeawa_23;
	// System.Int32 GarbageiOS.M_parmainouMaysalhas144::_rowheldrawNoujer
	int32_t ____rowheldrawNoujer_24;
	// System.Single GarbageiOS.M_parmainouMaysalhas144::_dereTaifo
	float ____dereTaifo_25;
	// System.UInt32 GarbageiOS.M_parmainouMaysalhas144::_seter
	uint32_t ____seter_26;
	// System.UInt32 GarbageiOS.M_parmainouMaysalhas144::_lasaJallyouma
	uint32_t ____lasaJallyouma_27;
	// System.Boolean GarbageiOS.M_parmainouMaysalhas144::_tearceeKupaw
	bool ____tearceeKupaw_28;
	// System.Boolean GarbageiOS.M_parmainouMaysalhas144::_lareSishas
	bool ____lareSishas_29;
	// System.UInt32 GarbageiOS.M_parmainouMaysalhas144::_banurparCiscasbou
	uint32_t ____banurparCiscasbou_30;
	// System.String GarbageiOS.M_parmainouMaysalhas144::_qisjuBerriroo
	String_t* ____qisjuBerriroo_31;
	// System.UInt32 GarbageiOS.M_parmainouMaysalhas144::_sinur
	uint32_t ____sinur_32;
	// System.Int32 GarbageiOS.M_parmainouMaysalhas144::_mardawhaTinall
	int32_t ____mardawhaTinall_33;
	// System.Int32 GarbageiOS.M_parmainouMaysalhas144::_sale
	int32_t ____sale_34;

public:
	inline static int32_t get_offset_of__dixarfalCehe_0() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____dixarfalCehe_0)); }
	inline String_t* get__dixarfalCehe_0() const { return ____dixarfalCehe_0; }
	inline String_t** get_address_of__dixarfalCehe_0() { return &____dixarfalCehe_0; }
	inline void set__dixarfalCehe_0(String_t* value)
	{
		____dixarfalCehe_0 = value;
		Il2CppCodeGenWriteBarrier(&____dixarfalCehe_0, value);
	}

	inline static int32_t get_offset_of__jajofasFarkevear_1() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____jajofasFarkevear_1)); }
	inline float get__jajofasFarkevear_1() const { return ____jajofasFarkevear_1; }
	inline float* get_address_of__jajofasFarkevear_1() { return &____jajofasFarkevear_1; }
	inline void set__jajofasFarkevear_1(float value)
	{
		____jajofasFarkevear_1 = value;
	}

	inline static int32_t get_offset_of__kaballGidefe_2() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____kaballGidefe_2)); }
	inline int32_t get__kaballGidefe_2() const { return ____kaballGidefe_2; }
	inline int32_t* get_address_of__kaballGidefe_2() { return &____kaballGidefe_2; }
	inline void set__kaballGidefe_2(int32_t value)
	{
		____kaballGidefe_2 = value;
	}

	inline static int32_t get_offset_of__tralalGourirhear_3() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____tralalGourirhear_3)); }
	inline uint32_t get__tralalGourirhear_3() const { return ____tralalGourirhear_3; }
	inline uint32_t* get_address_of__tralalGourirhear_3() { return &____tralalGourirhear_3; }
	inline void set__tralalGourirhear_3(uint32_t value)
	{
		____tralalGourirhear_3 = value;
	}

	inline static int32_t get_offset_of__kurkisgawYapow_4() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____kurkisgawYapow_4)); }
	inline String_t* get__kurkisgawYapow_4() const { return ____kurkisgawYapow_4; }
	inline String_t** get_address_of__kurkisgawYapow_4() { return &____kurkisgawYapow_4; }
	inline void set__kurkisgawYapow_4(String_t* value)
	{
		____kurkisgawYapow_4 = value;
		Il2CppCodeGenWriteBarrier(&____kurkisgawYapow_4, value);
	}

	inline static int32_t get_offset_of__searle_5() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____searle_5)); }
	inline int32_t get__searle_5() const { return ____searle_5; }
	inline int32_t* get_address_of__searle_5() { return &____searle_5; }
	inline void set__searle_5(int32_t value)
	{
		____searle_5 = value;
	}

	inline static int32_t get_offset_of__bairqaljisSearlou_6() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____bairqaljisSearlou_6)); }
	inline String_t* get__bairqaljisSearlou_6() const { return ____bairqaljisSearlou_6; }
	inline String_t** get_address_of__bairqaljisSearlou_6() { return &____bairqaljisSearlou_6; }
	inline void set__bairqaljisSearlou_6(String_t* value)
	{
		____bairqaljisSearlou_6 = value;
		Il2CppCodeGenWriteBarrier(&____bairqaljisSearlou_6, value);
	}

	inline static int32_t get_offset_of__lavisyir_7() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____lavisyir_7)); }
	inline int32_t get__lavisyir_7() const { return ____lavisyir_7; }
	inline int32_t* get_address_of__lavisyir_7() { return &____lavisyir_7; }
	inline void set__lavisyir_7(int32_t value)
	{
		____lavisyir_7 = value;
	}

	inline static int32_t get_offset_of__rawhouwe_8() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____rawhouwe_8)); }
	inline String_t* get__rawhouwe_8() const { return ____rawhouwe_8; }
	inline String_t** get_address_of__rawhouwe_8() { return &____rawhouwe_8; }
	inline void set__rawhouwe_8(String_t* value)
	{
		____rawhouwe_8 = value;
		Il2CppCodeGenWriteBarrier(&____rawhouwe_8, value);
	}

	inline static int32_t get_offset_of__pesurtu_9() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____pesurtu_9)); }
	inline float get__pesurtu_9() const { return ____pesurtu_9; }
	inline float* get_address_of__pesurtu_9() { return &____pesurtu_9; }
	inline void set__pesurtu_9(float value)
	{
		____pesurtu_9 = value;
	}

	inline static int32_t get_offset_of__cirarne_10() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____cirarne_10)); }
	inline float get__cirarne_10() const { return ____cirarne_10; }
	inline float* get_address_of__cirarne_10() { return &____cirarne_10; }
	inline void set__cirarne_10(float value)
	{
		____cirarne_10 = value;
	}

	inline static int32_t get_offset_of__radooxa_11() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____radooxa_11)); }
	inline int32_t get__radooxa_11() const { return ____radooxa_11; }
	inline int32_t* get_address_of__radooxa_11() { return &____radooxa_11; }
	inline void set__radooxa_11(int32_t value)
	{
		____radooxa_11 = value;
	}

	inline static int32_t get_offset_of__kelvelerCheacasme_12() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____kelvelerCheacasme_12)); }
	inline String_t* get__kelvelerCheacasme_12() const { return ____kelvelerCheacasme_12; }
	inline String_t** get_address_of__kelvelerCheacasme_12() { return &____kelvelerCheacasme_12; }
	inline void set__kelvelerCheacasme_12(String_t* value)
	{
		____kelvelerCheacasme_12 = value;
		Il2CppCodeGenWriteBarrier(&____kelvelerCheacasme_12, value);
	}

	inline static int32_t get_offset_of__jurwere_13() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____jurwere_13)); }
	inline bool get__jurwere_13() const { return ____jurwere_13; }
	inline bool* get_address_of__jurwere_13() { return &____jurwere_13; }
	inline void set__jurwere_13(bool value)
	{
		____jurwere_13 = value;
	}

	inline static int32_t get_offset_of__paidaNasri_14() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____paidaNasri_14)); }
	inline uint32_t get__paidaNasri_14() const { return ____paidaNasri_14; }
	inline uint32_t* get_address_of__paidaNasri_14() { return &____paidaNasri_14; }
	inline void set__paidaNasri_14(uint32_t value)
	{
		____paidaNasri_14 = value;
	}

	inline static int32_t get_offset_of__keenelCelweno_15() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____keenelCelweno_15)); }
	inline uint32_t get__keenelCelweno_15() const { return ____keenelCelweno_15; }
	inline uint32_t* get_address_of__keenelCelweno_15() { return &____keenelCelweno_15; }
	inline void set__keenelCelweno_15(uint32_t value)
	{
		____keenelCelweno_15 = value;
	}

	inline static int32_t get_offset_of__tobaldoo_16() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____tobaldoo_16)); }
	inline String_t* get__tobaldoo_16() const { return ____tobaldoo_16; }
	inline String_t** get_address_of__tobaldoo_16() { return &____tobaldoo_16; }
	inline void set__tobaldoo_16(String_t* value)
	{
		____tobaldoo_16 = value;
		Il2CppCodeGenWriteBarrier(&____tobaldoo_16, value);
	}

	inline static int32_t get_offset_of__nayse_17() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____nayse_17)); }
	inline bool get__nayse_17() const { return ____nayse_17; }
	inline bool* get_address_of__nayse_17() { return &____nayse_17; }
	inline void set__nayse_17(bool value)
	{
		____nayse_17 = value;
	}

	inline static int32_t get_offset_of__lasnir_18() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____lasnir_18)); }
	inline int32_t get__lasnir_18() const { return ____lasnir_18; }
	inline int32_t* get_address_of__lasnir_18() { return &____lasnir_18; }
	inline void set__lasnir_18(int32_t value)
	{
		____lasnir_18 = value;
	}

	inline static int32_t get_offset_of__tomerseaJeltoo_19() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____tomerseaJeltoo_19)); }
	inline int32_t get__tomerseaJeltoo_19() const { return ____tomerseaJeltoo_19; }
	inline int32_t* get_address_of__tomerseaJeltoo_19() { return &____tomerseaJeltoo_19; }
	inline void set__tomerseaJeltoo_19(int32_t value)
	{
		____tomerseaJeltoo_19 = value;
	}

	inline static int32_t get_offset_of__macerexel_20() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____macerexel_20)); }
	inline uint32_t get__macerexel_20() const { return ____macerexel_20; }
	inline uint32_t* get_address_of__macerexel_20() { return &____macerexel_20; }
	inline void set__macerexel_20(uint32_t value)
	{
		____macerexel_20 = value;
	}

	inline static int32_t get_offset_of__wearserfoYuteepi_21() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____wearserfoYuteepi_21)); }
	inline bool get__wearserfoYuteepi_21() const { return ____wearserfoYuteepi_21; }
	inline bool* get_address_of__wearserfoYuteepi_21() { return &____wearserfoYuteepi_21; }
	inline void set__wearserfoYuteepi_21(bool value)
	{
		____wearserfoYuteepi_21 = value;
	}

	inline static int32_t get_offset_of__pallrariNearwafem_22() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____pallrariNearwafem_22)); }
	inline int32_t get__pallrariNearwafem_22() const { return ____pallrariNearwafem_22; }
	inline int32_t* get_address_of__pallrariNearwafem_22() { return &____pallrariNearwafem_22; }
	inline void set__pallrariNearwafem_22(int32_t value)
	{
		____pallrariNearwafem_22 = value;
	}

	inline static int32_t get_offset_of__stewhutallYeawa_23() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____stewhutallYeawa_23)); }
	inline float get__stewhutallYeawa_23() const { return ____stewhutallYeawa_23; }
	inline float* get_address_of__stewhutallYeawa_23() { return &____stewhutallYeawa_23; }
	inline void set__stewhutallYeawa_23(float value)
	{
		____stewhutallYeawa_23 = value;
	}

	inline static int32_t get_offset_of__rowheldrawNoujer_24() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____rowheldrawNoujer_24)); }
	inline int32_t get__rowheldrawNoujer_24() const { return ____rowheldrawNoujer_24; }
	inline int32_t* get_address_of__rowheldrawNoujer_24() { return &____rowheldrawNoujer_24; }
	inline void set__rowheldrawNoujer_24(int32_t value)
	{
		____rowheldrawNoujer_24 = value;
	}

	inline static int32_t get_offset_of__dereTaifo_25() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____dereTaifo_25)); }
	inline float get__dereTaifo_25() const { return ____dereTaifo_25; }
	inline float* get_address_of__dereTaifo_25() { return &____dereTaifo_25; }
	inline void set__dereTaifo_25(float value)
	{
		____dereTaifo_25 = value;
	}

	inline static int32_t get_offset_of__seter_26() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____seter_26)); }
	inline uint32_t get__seter_26() const { return ____seter_26; }
	inline uint32_t* get_address_of__seter_26() { return &____seter_26; }
	inline void set__seter_26(uint32_t value)
	{
		____seter_26 = value;
	}

	inline static int32_t get_offset_of__lasaJallyouma_27() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____lasaJallyouma_27)); }
	inline uint32_t get__lasaJallyouma_27() const { return ____lasaJallyouma_27; }
	inline uint32_t* get_address_of__lasaJallyouma_27() { return &____lasaJallyouma_27; }
	inline void set__lasaJallyouma_27(uint32_t value)
	{
		____lasaJallyouma_27 = value;
	}

	inline static int32_t get_offset_of__tearceeKupaw_28() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____tearceeKupaw_28)); }
	inline bool get__tearceeKupaw_28() const { return ____tearceeKupaw_28; }
	inline bool* get_address_of__tearceeKupaw_28() { return &____tearceeKupaw_28; }
	inline void set__tearceeKupaw_28(bool value)
	{
		____tearceeKupaw_28 = value;
	}

	inline static int32_t get_offset_of__lareSishas_29() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____lareSishas_29)); }
	inline bool get__lareSishas_29() const { return ____lareSishas_29; }
	inline bool* get_address_of__lareSishas_29() { return &____lareSishas_29; }
	inline void set__lareSishas_29(bool value)
	{
		____lareSishas_29 = value;
	}

	inline static int32_t get_offset_of__banurparCiscasbou_30() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____banurparCiscasbou_30)); }
	inline uint32_t get__banurparCiscasbou_30() const { return ____banurparCiscasbou_30; }
	inline uint32_t* get_address_of__banurparCiscasbou_30() { return &____banurparCiscasbou_30; }
	inline void set__banurparCiscasbou_30(uint32_t value)
	{
		____banurparCiscasbou_30 = value;
	}

	inline static int32_t get_offset_of__qisjuBerriroo_31() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____qisjuBerriroo_31)); }
	inline String_t* get__qisjuBerriroo_31() const { return ____qisjuBerriroo_31; }
	inline String_t** get_address_of__qisjuBerriroo_31() { return &____qisjuBerriroo_31; }
	inline void set__qisjuBerriroo_31(String_t* value)
	{
		____qisjuBerriroo_31 = value;
		Il2CppCodeGenWriteBarrier(&____qisjuBerriroo_31, value);
	}

	inline static int32_t get_offset_of__sinur_32() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____sinur_32)); }
	inline uint32_t get__sinur_32() const { return ____sinur_32; }
	inline uint32_t* get_address_of__sinur_32() { return &____sinur_32; }
	inline void set__sinur_32(uint32_t value)
	{
		____sinur_32 = value;
	}

	inline static int32_t get_offset_of__mardawhaTinall_33() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____mardawhaTinall_33)); }
	inline int32_t get__mardawhaTinall_33() const { return ____mardawhaTinall_33; }
	inline int32_t* get_address_of__mardawhaTinall_33() { return &____mardawhaTinall_33; }
	inline void set__mardawhaTinall_33(int32_t value)
	{
		____mardawhaTinall_33 = value;
	}

	inline static int32_t get_offset_of__sale_34() { return static_cast<int32_t>(offsetof(M_parmainouMaysalhas144_t3284296656, ____sale_34)); }
	inline int32_t get__sale_34() const { return ____sale_34; }
	inline int32_t* get_address_of__sale_34() { return &____sale_34; }
	inline void set__sale_34(int32_t value)
	{
		____sale_34 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
