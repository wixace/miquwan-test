﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FloatTweener
struct FloatTweener_t1127747196;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// FloatTweener/setValue
struct setValue_t3220385890;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FloatTweener_setValue3220385890.h"

// System.Void FloatTweener::.ctor()
extern "C"  void FloatTweener__ctor_m3142741919 (FloatTweener_t1127747196 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject FloatTweener::get_placeholder()
extern "C"  GameObject_t3674682005 * FloatTweener_get_placeholder_m1263939602 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FloatTweener FloatTweener::Execute(System.Single,System.Single,System.Single,FloatTweener/setValue,System.Boolean)
extern "C"  FloatTweener_t1127747196 * FloatTweener_Execute_m3841553711 (Il2CppObject * __this /* static, unused */, float ___from0, float ___to1, float ___duration2, setValue_t3220385890 * ___fn3, bool ___pingpong4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FloatTweener::OnUpdate(System.Single,System.Boolean)
extern "C"  void FloatTweener_OnUpdate_m318454335 (FloatTweener_t1127747196 * __this, float ___factor0, bool ___isFinished1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
