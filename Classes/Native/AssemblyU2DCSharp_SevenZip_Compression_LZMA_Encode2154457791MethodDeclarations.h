﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Compression.LZMA.Encoder/LenPriceTableEncoder
struct LenPriceTableEncoder_t2154457791;
// SevenZip.Compression.RangeCoder.Encoder
struct Encoder_t2248006694;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_2248006694.h"

// System.Void SevenZip.Compression.LZMA.Encoder/LenPriceTableEncoder::.ctor()
extern "C"  void LenPriceTableEncoder__ctor_m3690860732 (LenPriceTableEncoder_t2154457791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder/LenPriceTableEncoder::SetTableSize(System.UInt32)
extern "C"  void LenPriceTableEncoder_SetTableSize_m2680710565 (LenPriceTableEncoder_t2154457791 * __this, uint32_t ___tableSize0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Encoder/LenPriceTableEncoder::GetPrice(System.UInt32,System.UInt32)
extern "C"  uint32_t LenPriceTableEncoder_GetPrice_m3018930744 (LenPriceTableEncoder_t2154457791 * __this, uint32_t ___symbol0, uint32_t ___posState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder/LenPriceTableEncoder::UpdateTable(System.UInt32)
extern "C"  void LenPriceTableEncoder_UpdateTable_m3052648699 (LenPriceTableEncoder_t2154457791 * __this, uint32_t ___posState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder/LenPriceTableEncoder::UpdateTables(System.UInt32)
extern "C"  void LenPriceTableEncoder_UpdateTables_m183372196 (LenPriceTableEncoder_t2154457791 * __this, uint32_t ___numPosStates0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder/LenPriceTableEncoder::Encode(SevenZip.Compression.RangeCoder.Encoder,System.UInt32,System.UInt32)
extern "C"  void LenPriceTableEncoder_Encode_m126578756 (LenPriceTableEncoder_t2154457791 * __this, Encoder_t2248006694 * ___rangeEncoder0, uint32_t ___symbol1, uint32_t ___posState2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
