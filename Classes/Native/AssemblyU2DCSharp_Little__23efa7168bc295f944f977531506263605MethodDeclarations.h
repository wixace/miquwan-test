﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._23efa7168bc295f944f9775382d3a0b0
struct _23efa7168bc295f944f9775382d3a0b0_t1506263605;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__23efa7168bc295f944f977531506263605.h"

// System.Void Little._23efa7168bc295f944f9775382d3a0b0::.ctor()
extern "C"  void _23efa7168bc295f944f9775382d3a0b0__ctor_m2898923416 (_23efa7168bc295f944f9775382d3a0b0_t1506263605 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._23efa7168bc295f944f9775382d3a0b0::_23efa7168bc295f944f9775382d3a0b0m2(System.Int32)
extern "C"  int32_t _23efa7168bc295f944f9775382d3a0b0__23efa7168bc295f944f9775382d3a0b0m2_m2874850681 (_23efa7168bc295f944f9775382d3a0b0_t1506263605 * __this, int32_t ____23efa7168bc295f944f9775382d3a0b0a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._23efa7168bc295f944f9775382d3a0b0::_23efa7168bc295f944f9775382d3a0b0m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _23efa7168bc295f944f9775382d3a0b0__23efa7168bc295f944f9775382d3a0b0m_m2214135133 (_23efa7168bc295f944f9775382d3a0b0_t1506263605 * __this, int32_t ____23efa7168bc295f944f9775382d3a0b0a0, int32_t ____23efa7168bc295f944f9775382d3a0b0481, int32_t ____23efa7168bc295f944f9775382d3a0b0c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._23efa7168bc295f944f9775382d3a0b0::ilo__23efa7168bc295f944f9775382d3a0b0m21(Little._23efa7168bc295f944f9775382d3a0b0,System.Int32)
extern "C"  int32_t _23efa7168bc295f944f9775382d3a0b0_ilo__23efa7168bc295f944f9775382d3a0b0m21_m709479836 (Il2CppObject * __this /* static, unused */, _23efa7168bc295f944f9775382d3a0b0_t1506263605 * ____this0, int32_t ____23efa7168bc295f944f9775382d3a0b0a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
