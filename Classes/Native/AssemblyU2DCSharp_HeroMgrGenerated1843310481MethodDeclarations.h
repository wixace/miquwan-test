﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HeroMgrGenerated
struct HeroMgrGenerated_t1843310481;
// JSVCall
struct JSVCall_t3708497963;
// System.Action
struct Action_t3771233898;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.String
struct String_t;
// System.Delegate
struct Delegate_t3310234105;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// HeroMgr
struct HeroMgr_t2475965342;
// CombatEntity
struct CombatEntity_t684137495;
// Hero
struct Hero_t2245658;
// CSHeroUnit
struct CSHeroUnit_t3764358446;
// System.Collections.Generic.List`1<CSHeroUnit>
struct List_1_t837576702;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_HeroMgr2475965342.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_CSHeroUnit3764358446.h"
#include "AssemblyU2DCSharp_Hero2245658.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"

// System.Void HeroMgrGenerated::.ctor()
extern "C"  void HeroMgrGenerated__ctor_m3324275242 (HeroMgrGenerated_t1843310481 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_HeroMgr1(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_HeroMgr1_m1025910098 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_ALL_TARGET_REACHED(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_ALL_TARGET_REACHED_m3711477130 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_HERO_DEAD(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_HERO_DEAD_m3433647767 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_FOCUS_ATTACK(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_FOCUS_ATTACK_m2768620541 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_CREAT_NPC_HERO(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_CREAT_NPC_HERO_m828426046 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_GADSKILL(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_GADSKILL_m1605051365 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_GADHEROID(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_GADHEROID_m154130209 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action HeroMgrGenerated::HeroMgr_LeaveCallBack_GetDelegate_member6_arg0(CSRepresentedObject)
extern "C"  Action_t3771233898 * HeroMgrGenerated_HeroMgr_LeaveCallBack_GetDelegate_member6_arg0_m353757023 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_LeaveCallBack(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_LeaveCallBack_m2369337444 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_killMonsterCount(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_killMonsterCount_m1618444633 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_friendsNpcMaxHp(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_friendsNpcMaxHp_m881153696 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_herosMaxHp(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_herosMaxHp_m993464409 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_biaocheMaxHp(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_biaocheMaxHp_m107521387 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_intRot(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_intRot_m179842276 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_heroContainer(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_heroContainer_m3896844089 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_gadHero(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_gadHero_m4223356700 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_guide(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_guide_m2814321540 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_deadHeroNum(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_deadHeroNum_m1154555992 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_curRound(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_curRound_m3517413982 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_power(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_power_m1775210395 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_isPowerBuff(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_isPowerBuff_m3041879634 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_heroUnits(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_heroUnits_m3366332939 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_instance(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_instance_m828747991 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_isWaitTargetReached(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_isWaitTargetReached_m3076083902 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_isUsingActiveSkill(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_isUsingActiveSkill_m2335478875 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_captain(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_captain_m3141802460 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::HeroMgr_CameraSmoothFollow(JSVCall)
extern "C"  void HeroMgrGenerated_HeroMgr_CameraSmoothFollow_m599451624 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_AddBuffOnTeamSkill(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_AddBuffOnTeamSkill_m1182361280 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_AddHero__CombatEntity(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_AddHero__CombatEntity_m203558645 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_AllHerosIsAIMoveEndReached(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_AllHerosIsAIMoveEndReached_m3312166705 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_ChangeHeroToDead__Int32(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_ChangeHeroToDead__Int32_m1256616392 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_Clear(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_Clear_m1247889936 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_CreateGuide__GameObject(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_CreateGuide__GameObject_m1423547764 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_CreateHero__CSHeroUnit__Boolean(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_CreateHero__CSHeroUnit__Boolean_m3221783431 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_CreateHeros__ListT1_CSHeroUnit__Int32(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_CreateHeros__ListT1_CSHeroUnit__Int32_m802054780 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_CreatGuideHeroNpc__Int32__JSCLevelHeroPointConfig(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_CreatGuideHeroNpc__Int32__JSCLevelHeroPointConfig_m2798800791 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_CreatGuideNpcs__Int32(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_CreatGuideNpcs__Int32_m945565036 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_Dead__CombatEntity(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_Dead__CombatEntity_m3930640122 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_EnterAutoMode__Boolean(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_EnterAutoMode__Boolean_m2680580829 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_EnterAutoNext(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_EnterAutoNext_m2061804093 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_EnterBattle(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_EnterBattle_m3091872691 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_EnterHeroBattle__Hero(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_EnterHeroBattle__Hero_m2728233799 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_FindTarget_AutoFight(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_FindTarget_AutoFight_m790420875 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_FocusAttack__CombatEntity(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_FocusAttack__CombatEntity_m2437140602 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetAllHero(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetAllHero_m3670175012 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetAllHeros(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetAllHeros_m2719948689 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetCameraTarger(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetCameraTarger_m1076000013 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetCaptain(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetCaptain_m3557072461 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetCureHero(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetCureHero_m428377464 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetDefenseHero__CombatEntity(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetDefenseHero__CombatEntity_m3346514490 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetFlyHeroPos__Vector3(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetFlyHeroPos__Vector3_m827074706 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetHero__Int32(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetHero__Int32_m3212051581 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetHeroByID__Int32(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetHeroByID__Int32_m3762716395 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetHeroBySex__Int32(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetHeroBySex__Int32_m1974652530 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetHeroIntelligenceNum__Int32(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetHeroIntelligenceNum__Int32_m2477820730 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetHeroUnit__UInt32(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetHeroUnit__UInt32_m193688944 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetHeroUnits(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetHeroUnits_m1109288446 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetHPMinHero(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetHPMinHero_m3080696973 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetHurtMaxHero(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetHurtMaxHero_m1926906200 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetMainTarget(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetMainTarget_m1317756451 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetMarshalPointNext(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetMarshalPointNext_m3964607454 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetMinMarshalPoint__Int32(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetMinMarshalPoint__Int32_m1049534339 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetNearestHero__CombatEntity(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetNearestHero__CombatEntity_m1831890520 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetNearestHero__Vector3(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetNearestHero__Vector3_m3041458641 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetRemoteHero(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetRemoteHero_m2040235609 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_GetTotalDamageHero(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_GetTotalDamageHero_m479588566 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_Init(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_Init_m2987678063 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_IsAllElement(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_IsAllElement_m1092803460 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_IsAtkRangeMonster__Hero__Single(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_IsAtkRangeMonster__Hero__Single_m3258464336 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_isCanLeaveBattle(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_isCanLeaveBattle_m3916418728 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_IsThreeCountry__HERO_COUNTRY(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_IsThreeCountry__HERO_COUNTRY_m267517202 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_IsThreeElement__HERO_ELEMENT(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_IsThreeElement__HERO_ELEMENT_m3153338462 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_IsTrueHerosEndPoint(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_IsTrueHerosEndPoint_m3728415961 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_IsTrueMarshalIndex(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_IsTrueMarshalIndex_m1591471275 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_LeaveAutoMode(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_LeaveAutoMode_m3893732492 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_LeaveBattle(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_LeaveBattle_m3298823954 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action HeroMgrGenerated::HeroMgr_LeaveBattleReq_GetDelegate_member49_arg0(CSRepresentedObject)
extern "C"  Action_t3771233898 * HeroMgrGenerated_HeroMgr_LeaveBattleReq_GetDelegate_member49_arg0_m1978546909 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_LeaveBattleReq__Action(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_LeaveBattleReq__Action_m114997796 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action HeroMgrGenerated::HeroMgr_LeaveBattleReqOnWin_GetDelegate_member50_arg0(CSRepresentedObject)
extern "C"  Action_t3771233898 * HeroMgrGenerated_HeroMgr_LeaveBattleReqOnWin_GetDelegate_member50_arg0_m966283748 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_LeaveBattleReqOnWin__Action(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_LeaveBattleReqOnWin__Action_m1414062631 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_Lose(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_Lose_m3136829876 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_MathfMarshalPos__Hero__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_MathfMarshalPos__Hero__Vector3__Vector3_m199822033 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_RemoveBuffOnTeamEnd(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_RemoveBuffOnTeamEnd_m3498344235 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_ResetPlace(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_ResetPlace_m1289103575 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_SetCameraTargetObjPoint__CombatEntity__Boolean(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_SetCameraTargetObjPoint__CombatEntity__Boolean_m184428607 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_SetHeroModelView__Boolean(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_SetHeroModelView__Boolean_m979155225 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_SetHeroPos(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_SetHeroPos_m3310021911 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_UpdateTargetPos__Vector3__Boolean__Boolean__EBehaviorID(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_UpdateTargetPos__Vector3__Boolean__Boolean__EBehaviorID_m971168543 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_Win(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_Win_m3145608927 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::HeroMgr_ZUpdate(JSVCall,System.Int32)
extern "C"  bool HeroMgrGenerated_HeroMgr_ZUpdate_m2134355110 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::__Register()
extern "C"  void HeroMgrGenerated___Register_m4284144861 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action HeroMgrGenerated::<HeroMgr_LeaveCallBack>m__63()
extern "C"  Action_t3771233898 * HeroMgrGenerated_U3CHeroMgr_LeaveCallBackU3Em__63_m455277283 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action HeroMgrGenerated::<HeroMgr_LeaveBattleReq__Action>m__65()
extern "C"  Action_t3771233898 * HeroMgrGenerated_U3CHeroMgr_LeaveBattleReq__ActionU3Em__65_m3635734074 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action HeroMgrGenerated::<HeroMgr_LeaveBattleReqOnWin__Action>m__67()
extern "C"  Action_t3771233898 * HeroMgrGenerated_U3CHeroMgr_LeaveBattleReqOnWin__ActionU3Em__67_m3736799055 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::ilo_setStringS1(System.Int32,System.String)
extern "C"  void HeroMgrGenerated_ilo_setStringS1_m2570612723 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::ilo_setInt322(System.Int32,System.Int32)
extern "C"  void HeroMgrGenerated_ilo_setInt322_m3913784907 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::ilo_addJSFunCSDelegateRel3(System.Int32,System.Delegate)
extern "C"  void HeroMgrGenerated_ilo_addJSFunCSDelegateRel3_m392270442 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object HeroMgrGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * HeroMgrGenerated_ilo_getObject4_m2539730056 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroMgrGenerated::ilo_setObject5(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t HeroMgrGenerated_ilo_setObject5_m1218783140 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::ilo_setBooleanS6(System.Int32,System.Boolean)
extern "C"  void HeroMgrGenerated_ilo_setBooleanS6_m3210491702 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::ilo_getBooleanS7(System.Int32)
extern "C"  bool HeroMgrGenerated_ilo_getBooleanS7_m2968780008 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr HeroMgrGenerated::ilo_get_instance8()
extern "C"  HeroMgr_t2475965342 * HeroMgrGenerated_ilo_get_instance8_m193360986 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::ilo_get_isWaitTargetReached9(HeroMgr)
extern "C"  bool HeroMgrGenerated_ilo_get_isWaitTargetReached9_m4243787987 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::ilo_get_isUsingActiveSkill10(HeroMgr)
extern "C"  bool HeroMgrGenerated_ilo_get_isUsingActiveSkill10_m3631681402 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::ilo_AddBuffOnTeamSkill11(HeroMgr)
extern "C"  void HeroMgrGenerated_ilo_AddBuffOnTeamSkill11_m4024974406 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::ilo_AddHero12(HeroMgr,CombatEntity)
extern "C"  void HeroMgrGenerated_ilo_AddHero12_m387909132 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, CombatEntity_t684137495 * ___entity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroMgrGenerated::ilo_getInt3213(System.Int32)
extern "C"  int32_t HeroMgrGenerated_ilo_getInt3213_m2134231814 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::ilo_ChangeHeroToDead14(HeroMgr,System.Int32)
extern "C"  void HeroMgrGenerated_ilo_ChangeHeroToDead14_m1633077596 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::ilo_Clear15(HeroMgr)
extern "C"  void HeroMgrGenerated_ilo_Clear15_m1105822638 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero HeroMgrGenerated::ilo_CreateHero16(HeroMgr,CSHeroUnit,System.Boolean)
extern "C"  Hero_t2245658 * HeroMgrGenerated_ilo_CreateHero16_m1515272558 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, CSHeroUnit_t3764358446 * ___unit1, bool ___isCatain2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::ilo_CreateHeros17(HeroMgr,System.Collections.Generic.List`1<CSHeroUnit>,System.Int32)
extern "C"  void HeroMgrGenerated_ilo_CreateHeros17_m2835732257 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, List_1_t837576702 * ___heroUnitList1, int32_t ___count2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::ilo_EnterAutoMode18(HeroMgr,System.Boolean)
extern "C"  void HeroMgrGenerated_ilo_EnterAutoMode18_m490106735 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, bool ___isMarshalMove1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::ilo_EnterHeroBattle19(HeroMgr,Hero)
extern "C"  void HeroMgrGenerated_ilo_EnterHeroBattle19_m1239443519 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, Hero_t2245658 * ___hero1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity HeroMgrGenerated::ilo_FindTarget_AutoFight20(HeroMgr)
extern "C"  CombatEntity_t684137495 * HeroMgrGenerated_ilo_FindTarget_AutoFight20_m1241062967 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform HeroMgrGenerated::ilo_GetCameraTarger21(HeroMgr)
extern "C"  Transform_t1659122786 * HeroMgrGenerated_ilo_GetCameraTarger21_m984547828 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 HeroMgrGenerated::ilo_getVector3S22(System.Int32)
extern "C"  Vector3_t4282066566  HeroMgrGenerated_ilo_getVector3S22_m3234103973 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero HeroMgrGenerated::ilo_GetHeroByID23(HeroMgr,System.Int32)
extern "C"  Hero_t2245658 * HeroMgrGenerated_ilo_GetHeroByID23_m104001336 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HeroMgrGenerated::ilo_GetMarshalPointNext24(HeroMgr)
extern "C"  GameObject_t3674682005 * HeroMgrGenerated_ilo_GetMarshalPointNext24_m1111518711 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroMgrGenerated::ilo_getArgIndex25()
extern "C"  int32_t HeroMgrGenerated_ilo_getArgIndex25_m520553244 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject HeroMgrGenerated::ilo_GetMinMarshalPoint26(HeroMgr,System.Int32&)
extern "C"  GameObject_t3674682005 * HeroMgrGenerated_ilo_GetMinMarshalPoint26_m3989416789 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, int32_t* ___marshalIndex1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero HeroMgrGenerated::ilo_GetNearestHero27(HeroMgr,CombatEntity)
extern "C"  Hero_t2245658 * HeroMgrGenerated_ilo_GetNearestHero27_m3377340722 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, CombatEntity_t684137495 * ___entity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Hero HeroMgrGenerated::ilo_GetNearestHero28(HeroMgr,UnityEngine.Vector3)
extern "C"  Hero_t2245658 * HeroMgrGenerated_ilo_GetNearestHero28_m461598319 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, Vector3_t4282066566  ___pos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single HeroMgrGenerated::ilo_getSingle29(System.Int32)
extern "C"  float HeroMgrGenerated_ilo_getSingle29_m602363935 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::ilo_IsTrueMarshalIndex30(HeroMgr)
extern "C"  bool HeroMgrGenerated_ilo_IsTrueMarshalIndex30_m2038303850 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::ilo_LeaveBattle31(HeroMgr)
extern "C"  void HeroMgrGenerated_ilo_LeaveBattle31_m2785449586 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::ilo_LeaveBattleReqOnWin32(HeroMgr,System.Action)
extern "C"  void HeroMgrGenerated_ilo_LeaveBattleReqOnWin32_m1334224073 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, Action_t3771233898 * ___callBack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::ilo_RemoveBuffOnTeamEnd33(HeroMgr)
extern "C"  void HeroMgrGenerated_ilo_RemoveBuffOnTeamEnd33_m3603906615 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::ilo_SetCameraTargetObjPoint34(HeroMgr,CombatEntity,System.Boolean)
extern "C"  void HeroMgrGenerated_ilo_SetCameraTargetObjPoint34_m3995782555 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, CombatEntity_t684137495 * ___catainHero1, bool ___isFollowHero2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::ilo_SetHeroPos35(HeroMgr)
extern "C"  void HeroMgrGenerated_ilo_SetHeroPos35_m4091269709 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::ilo_UpdateTargetPos36(HeroMgr,UnityEngine.Vector3,System.Boolean,System.Boolean,Entity.Behavior.EBehaviorID)
extern "C"  void HeroMgrGenerated_ilo_UpdateTargetPos36_m3859454752 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, Vector3_t4282066566  ___targetPos1, bool ___isDoubleClick2, bool ___isFixedSpeed3, uint8_t ___bev4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 HeroMgrGenerated::ilo_getEnum37(System.Int32)
extern "C"  int32_t HeroMgrGenerated_ilo_getEnum37_m1344710067 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HeroMgrGenerated::ilo_Win38(HeroMgr)
extern "C"  void HeroMgrGenerated_ilo_Win38_m2651409054 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean HeroMgrGenerated::ilo_isFunctionS39(System.Int32)
extern "C"  bool HeroMgrGenerated_ilo_isFunctionS39_m900303689 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject HeroMgrGenerated::ilo_getFunctionS40(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * HeroMgrGenerated_ilo_getFunctionS40_m526463228 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
