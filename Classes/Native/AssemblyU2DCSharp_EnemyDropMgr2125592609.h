﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// EnemyDropMgr
struct EnemyDropMgr_t2125592609;
// System.Collections.Generic.List`1<EnemyDropItem>
struct List_1_t2836940026;
// System.Collections.Generic.Dictionary`2<CombatEntity,System.Single>
struct Dictionary_2_t329677924;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyDropMgr
struct  EnemyDropMgr_t2125592609  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<EnemyDropItem> EnemyDropMgr::enemyDropItemList
	List_1_t2836940026 * ___enemyDropItemList_1;
	// System.Collections.Generic.List`1<EnemyDropItem> EnemyDropMgr::_dropList
	List_1_t2836940026 * ____dropList_2;
	// System.Collections.Generic.Dictionary`2<CombatEntity,System.Single> EnemyDropMgr::_hurtDic
	Dictionary_2_t329677924 * ____hurtDic_3;

public:
	inline static int32_t get_offset_of_enemyDropItemList_1() { return static_cast<int32_t>(offsetof(EnemyDropMgr_t2125592609, ___enemyDropItemList_1)); }
	inline List_1_t2836940026 * get_enemyDropItemList_1() const { return ___enemyDropItemList_1; }
	inline List_1_t2836940026 ** get_address_of_enemyDropItemList_1() { return &___enemyDropItemList_1; }
	inline void set_enemyDropItemList_1(List_1_t2836940026 * value)
	{
		___enemyDropItemList_1 = value;
		Il2CppCodeGenWriteBarrier(&___enemyDropItemList_1, value);
	}

	inline static int32_t get_offset_of__dropList_2() { return static_cast<int32_t>(offsetof(EnemyDropMgr_t2125592609, ____dropList_2)); }
	inline List_1_t2836940026 * get__dropList_2() const { return ____dropList_2; }
	inline List_1_t2836940026 ** get_address_of__dropList_2() { return &____dropList_2; }
	inline void set__dropList_2(List_1_t2836940026 * value)
	{
		____dropList_2 = value;
		Il2CppCodeGenWriteBarrier(&____dropList_2, value);
	}

	inline static int32_t get_offset_of__hurtDic_3() { return static_cast<int32_t>(offsetof(EnemyDropMgr_t2125592609, ____hurtDic_3)); }
	inline Dictionary_2_t329677924 * get__hurtDic_3() const { return ____hurtDic_3; }
	inline Dictionary_2_t329677924 ** get_address_of__hurtDic_3() { return &____hurtDic_3; }
	inline void set__hurtDic_3(Dictionary_2_t329677924 * value)
	{
		____hurtDic_3 = value;
		Il2CppCodeGenWriteBarrier(&____hurtDic_3, value);
	}
};

struct EnemyDropMgr_t2125592609_StaticFields
{
public:
	// EnemyDropMgr EnemyDropMgr::_instance
	EnemyDropMgr_t2125592609 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(EnemyDropMgr_t2125592609_StaticFields, ____instance_0)); }
	inline EnemyDropMgr_t2125592609 * get__instance_0() const { return ____instance_0; }
	inline EnemyDropMgr_t2125592609 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(EnemyDropMgr_t2125592609 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
