﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.ClipperLib.LocalMinima
struct LocalMinima_t2863342752;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.TEdge>>
struct List_1_t2392209947;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.ClipperLib.ClipperBase
struct  ClipperBase_t512275816  : public Il2CppObject
{
public:
	// Pathfinding.ClipperLib.LocalMinima Pathfinding.ClipperLib.ClipperBase::m_MinimaList
	LocalMinima_t2863342752 * ___m_MinimaList_6;
	// Pathfinding.ClipperLib.LocalMinima Pathfinding.ClipperLib.ClipperBase::m_CurrentLM
	LocalMinima_t2863342752 * ___m_CurrentLM_7;
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.TEdge>> Pathfinding.ClipperLib.ClipperBase::m_edges
	List_1_t2392209947 * ___m_edges_8;
	// System.Boolean Pathfinding.ClipperLib.ClipperBase::m_UseFullRange
	bool ___m_UseFullRange_9;
	// System.Boolean Pathfinding.ClipperLib.ClipperBase::m_HasOpenPaths
	bool ___m_HasOpenPaths_10;
	// System.Boolean Pathfinding.ClipperLib.ClipperBase::<PreserveCollinear>k__BackingField
	bool ___U3CPreserveCollinearU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_m_MinimaList_6() { return static_cast<int32_t>(offsetof(ClipperBase_t512275816, ___m_MinimaList_6)); }
	inline LocalMinima_t2863342752 * get_m_MinimaList_6() const { return ___m_MinimaList_6; }
	inline LocalMinima_t2863342752 ** get_address_of_m_MinimaList_6() { return &___m_MinimaList_6; }
	inline void set_m_MinimaList_6(LocalMinima_t2863342752 * value)
	{
		___m_MinimaList_6 = value;
		Il2CppCodeGenWriteBarrier(&___m_MinimaList_6, value);
	}

	inline static int32_t get_offset_of_m_CurrentLM_7() { return static_cast<int32_t>(offsetof(ClipperBase_t512275816, ___m_CurrentLM_7)); }
	inline LocalMinima_t2863342752 * get_m_CurrentLM_7() const { return ___m_CurrentLM_7; }
	inline LocalMinima_t2863342752 ** get_address_of_m_CurrentLM_7() { return &___m_CurrentLM_7; }
	inline void set_m_CurrentLM_7(LocalMinima_t2863342752 * value)
	{
		___m_CurrentLM_7 = value;
		Il2CppCodeGenWriteBarrier(&___m_CurrentLM_7, value);
	}

	inline static int32_t get_offset_of_m_edges_8() { return static_cast<int32_t>(offsetof(ClipperBase_t512275816, ___m_edges_8)); }
	inline List_1_t2392209947 * get_m_edges_8() const { return ___m_edges_8; }
	inline List_1_t2392209947 ** get_address_of_m_edges_8() { return &___m_edges_8; }
	inline void set_m_edges_8(List_1_t2392209947 * value)
	{
		___m_edges_8 = value;
		Il2CppCodeGenWriteBarrier(&___m_edges_8, value);
	}

	inline static int32_t get_offset_of_m_UseFullRange_9() { return static_cast<int32_t>(offsetof(ClipperBase_t512275816, ___m_UseFullRange_9)); }
	inline bool get_m_UseFullRange_9() const { return ___m_UseFullRange_9; }
	inline bool* get_address_of_m_UseFullRange_9() { return &___m_UseFullRange_9; }
	inline void set_m_UseFullRange_9(bool value)
	{
		___m_UseFullRange_9 = value;
	}

	inline static int32_t get_offset_of_m_HasOpenPaths_10() { return static_cast<int32_t>(offsetof(ClipperBase_t512275816, ___m_HasOpenPaths_10)); }
	inline bool get_m_HasOpenPaths_10() const { return ___m_HasOpenPaths_10; }
	inline bool* get_address_of_m_HasOpenPaths_10() { return &___m_HasOpenPaths_10; }
	inline void set_m_HasOpenPaths_10(bool value)
	{
		___m_HasOpenPaths_10 = value;
	}

	inline static int32_t get_offset_of_U3CPreserveCollinearU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ClipperBase_t512275816, ___U3CPreserveCollinearU3Ek__BackingField_11)); }
	inline bool get_U3CPreserveCollinearU3Ek__BackingField_11() const { return ___U3CPreserveCollinearU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CPreserveCollinearU3Ek__BackingField_11() { return &___U3CPreserveCollinearU3Ek__BackingField_11; }
	inline void set_U3CPreserveCollinearU3Ek__BackingField_11(bool value)
	{
		___U3CPreserveCollinearU3Ek__BackingField_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
