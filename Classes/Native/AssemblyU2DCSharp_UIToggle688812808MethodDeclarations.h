﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIToggle
struct UIToggle_t688812808;
// UIWidget
struct UIWidget_t769069560;
// System.Collections.Generic.List`1<EventDelegate>
struct List_1_t1077642479;
// UnityEngine.Behaviour
struct Behaviour_t200106419;
// TweenAlpha
struct TweenAlpha_t2920325587;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UITweener
struct UITweener_t105489188;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIToggle688812808.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "UnityEngine_UnityEngine_Behaviour200106419.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_UITweener105489188.h"

// System.Void UIToggle::.ctor()
extern "C"  void UIToggle__ctor_m3478074515 (UIToggle_t688812808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggle::.cctor()
extern "C"  void UIToggle__cctor_m4258998650 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggle::get_value()
extern "C"  bool UIToggle_get_value_m2236783845 (UIToggle_t688812808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggle::set_value(System.Boolean)
extern "C"  void UIToggle_set_value_m116851932 (UIToggle_t688812808 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggle::get_isColliderEnabled()
extern "C"  bool UIToggle_get_isColliderEnabled_m1749226647 (UIToggle_t688812808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggle::get_isChecked()
extern "C"  bool UIToggle_get_isChecked_m803657233 (UIToggle_t688812808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggle::set_isChecked(System.Boolean)
extern "C"  void UIToggle_set_isChecked_m63185160 (UIToggle_t688812808 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIToggle UIToggle::GetActiveToggle(System.Int32)
extern "C"  UIToggle_t688812808 * UIToggle_GetActiveToggle_m348454301 (Il2CppObject * __this /* static, unused */, int32_t ___group0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggle::OnEnable()
extern "C"  void UIToggle_OnEnable_m1303438867 (UIToggle_t688812808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggle::OnDisable()
extern "C"  void UIToggle_OnDisable_m2192836474 (UIToggle_t688812808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggle::Start()
extern "C"  void UIToggle_Start_m2425212307 (UIToggle_t688812808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggle::OnClick()
extern "C"  void UIToggle_OnClick_m2789257818 (UIToggle_t688812808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggle::Set(System.Boolean)
extern "C"  void UIToggle_Set_m2078222794 (UIToggle_t688812808 * __this, bool ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggle::OnDestory()
extern "C"  void UIToggle_OnDestory_m72720038 (UIToggle_t688812808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggle::ilo_set_value1(UIToggle,System.Boolean)
extern "C"  void UIToggle_ilo_set_value1_m2929141860 (Il2CppObject * __this /* static, unused */, UIToggle_t688812808 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggle::ilo_set_alpha2(UIWidget,System.Single)
extern "C"  void UIToggle_ilo_set_alpha2_m2922357218 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggle::ilo_IsValid3(System.Collections.Generic.List`1<EventDelegate>)
extern "C"  bool UIToggle_ilo_IsValid3_m2586420380 (Il2CppObject * __this /* static, unused */, List_1_t1077642479 * ___list0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggle::ilo_Set4(UIToggle,System.Boolean)
extern "C"  void UIToggle_ilo_Set4_m1927146223 (Il2CppObject * __this /* static, unused */, UIToggle_t688812808 * ____this0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIToggle::ilo_GetActive5(UnityEngine.Behaviour)
extern "C"  bool UIToggle_ilo_GetActive5_m3834276731 (Il2CppObject * __this /* static, unused */, Behaviour_t200106419 * ___mb0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TweenAlpha UIToggle::ilo_Begin6(UnityEngine.GameObject,System.Single,System.Single)
extern "C"  TweenAlpha_t2920325587 * UIToggle_ilo_Begin6_m1293433661 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, float ___duration1, float ___alpha2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggle::ilo_Play7(UITweener,System.Boolean)
extern "C"  void UIToggle_ilo_Play7_m2242746896 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, bool ___forward1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIToggle::ilo_set_tweenFactor8(UITweener,System.Single)
extern "C"  void UIToggle_ilo_set_tweenFactor8_m2829587348 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
