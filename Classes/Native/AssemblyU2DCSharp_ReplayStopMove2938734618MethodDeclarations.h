﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayStopMove
struct ReplayStopMove_t2938734618;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.String
struct String_t;
// ReplayBase
struct ReplayBase_t779703160;
// CombatEntity
struct CombatEntity_t684137495;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ReplayBase779703160.h"

// System.Void ReplayStopMove::.ctor()
extern "C"  void ReplayStopMove__ctor_m2291843521 (ReplayStopMove_t2938734618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayStopMove::.ctor(System.Object[])
extern "C"  void ReplayStopMove__ctor_m1851316177 (ReplayStopMove_t2938734618 * __this, ObjectU5BU5D_t1108656482* ___datas0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] ReplayStopMove::ParserJsonStr(System.String)
extern "C"  StringU5BU5D_t4054002952* ReplayStopMove_ParserJsonStr_m1448572666 (ReplayStopMove_t2938734618 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayStopMove::GetJsonStr()
extern "C"  String_t* ReplayStopMove_GetJsonStr_m4111773881 (ReplayStopMove_t2938734618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayStopMove::Execute()
extern "C"  void ReplayStopMove_Execute_m1994790292 (ReplayStopMove_t2938734618 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayStopMove::ilo_GetJsonStr1(ReplayBase)
extern "C"  String_t* ReplayStopMove_ilo_GetJsonStr1_m2921407855 (Il2CppObject * __this /* static, unused */, ReplayBase_t779703160 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayStopMove::ilo_Execute2(ReplayBase)
extern "C"  void ReplayStopMove_ilo_Execute2_m425210619 (Il2CppObject * __this /* static, unused */, ReplayBase_t779703160 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity ReplayStopMove::ilo_get_Entity3(ReplayBase)
extern "C"  CombatEntity_t684137495 * ReplayStopMove_ilo_get_Entity3_m78309205 (Il2CppObject * __this /* static, unused */, ReplayBase_t779703160 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
