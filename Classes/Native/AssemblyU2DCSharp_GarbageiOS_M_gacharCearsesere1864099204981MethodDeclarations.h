﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_gacharCearsesere186
struct M_gacharCearsesere186_t4099204981;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_gacharCearsesere186::.ctor()
extern "C"  void M_gacharCearsesere186__ctor_m1704890254 (M_gacharCearsesere186_t4099204981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_gacharCearsesere186::M_jelfaWhacay0(System.String[],System.Int32)
extern "C"  void M_gacharCearsesere186_M_jelfaWhacay0_m2362881016 (M_gacharCearsesere186_t4099204981 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
