﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginJinLi
struct PluginJinLi_t2545610073;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// VersionInfo
struct VersionInfo_t2356638086;
// System.Object
struct Il2CppObject;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginJinLi2545610073.h"
#include "mscorlib_System_Object4170816371.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginJinLi::.ctor()
extern "C"  void PluginJinLi__ctor_m3086189106 (PluginJinLi_t2545610073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinLi::Init()
extern "C"  void PluginJinLi_Init_m1990753762 (PluginJinLi_t2545610073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinLi::OnDestory()
extern "C"  void PluginJinLi_OnDestory_m1087112389 (PluginJinLi_t2545610073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinLi::Clear()
extern "C"  void PluginJinLi_Clear_m492322397 (PluginJinLi_t2545610073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinLi::RoleEnterGame(CEvent.ZEvent)
extern "C"  void PluginJinLi_RoleEnterGame_m1405387927 (PluginJinLi_t2545610073 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinLi::UserPay(CEvent.ZEvent)
extern "C"  void PluginJinLi_UserPay_m1014375534 (PluginJinLi_t2545610073 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinLi::onJinLiLoginSuccess(System.String)
extern "C"  void PluginJinLi_onJinLiLoginSuccess_m360688043 (PluginJinLi_t2545610073 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinLi::onJinLiLoginError(System.String)
extern "C"  void PluginJinLi_onJinLiLoginError_m3949290566 (PluginJinLi_t2545610073 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinLi::OnPayJinLiSuccess(System.String)
extern "C"  void PluginJinLi_OnPayJinLiSuccess_m1029130706 (PluginJinLi_t2545610073 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinLi::OnPayJinLiError(System.String)
extern "C"  void PluginJinLi_OnPayJinLiError_m2233786925 (PluginJinLi_t2545610073 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinLi::OnExitGameSuccess(System.String)
extern "C"  void PluginJinLi_OnExitGameSuccess_m2758921694 (PluginJinLi_t2545610073 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinLi::OnLogoutFlyme(System.String)
extern "C"  void PluginJinLi_OnLogoutFlyme_m2175472 (PluginJinLi_t2545610073 * __this, String_t* ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginJinLi::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginJinLi_ReqSDKHttpLogin_m1119545635 (PluginJinLi_t2545610073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginJinLi::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginJinLi_IsLoginSuccess_m262167801 (PluginJinLi_t2545610073 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinLi::LoginJinLiMgr()
extern "C"  void PluginJinLi_LoginJinLiMgr_m3690539813 (PluginJinLi_t2545610073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinLi::PlayJinLiMgr(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.Int32,System.String,System.String,System.String,System.String)
extern "C"  void PluginJinLi_PlayJinLiMgr_m737343251 (PluginJinLi_t2545610073 * __this, String_t* ___price0, String_t* ___roleId1, String_t* ___roleName2, String_t* ___roleLevel3, String_t* ___serverId4, String_t* ___serverName5, String_t* ___productId6, String_t* ___productName7, int32_t ___buyNum8, String_t* ___exts9, String_t* ___productDes10, String_t* ___notifyUrl11, String_t* ___orderUrl12, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinLi::<OnExitGameSuccess>m__430()
extern "C"  void PluginJinLi_U3COnExitGameSuccessU3Em__430_m290998426 (PluginJinLi_t2545610073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinLi::<OnLogoutFlyme>m__431()
extern "C"  void PluginJinLi_U3COnLogoutFlymeU3Em__431_m353121005 (PluginJinLi_t2545610073 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinLi::ilo_RemoveEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginJinLi_ilo_RemoveEventListener1_m851017527 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginJinLi::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginJinLi_ilo_get_Instance2_m328905486 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginJinLi::ilo_get_currentVS3(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginJinLi_ilo_get_currentVS3_m1637110142 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinLi::ilo_PlayJinLiMgr4(PluginJinLi,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.Int32,System.String,System.String,System.String,System.String)
extern "C"  void PluginJinLi_ilo_PlayJinLiMgr4_m1105716575 (Il2CppObject * __this /* static, unused */, PluginJinLi_t2545610073 * ____this0, String_t* ___price1, String_t* ___roleId2, String_t* ___roleName3, String_t* ___roleLevel4, String_t* ___serverId5, String_t* ___serverName6, String_t* ___productId7, String_t* ___productName8, int32_t ___buyNum9, String_t* ___exts10, String_t* ___productDes11, String_t* ___notifyUrl12, String_t* ___orderUrl13, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinLi::ilo_Log5(System.Object,System.Boolean)
extern "C"  void PluginJinLi_ilo_Log5_m2230468575 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginJinLi::ilo_get_PluginsSdkMgr6()
extern "C"  PluginsSdkMgr_t3884624670 * PluginJinLi_ilo_get_PluginsSdkMgr6_m2291303793 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinLi::ilo_DispatchEvent7(CEvent.ZEvent)
extern "C"  void PluginJinLi_ilo_DispatchEvent7_m2995629695 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinLi::ilo_Logout8(System.Action)
extern "C"  void PluginJinLi_ilo_Logout8_m362278834 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginJinLi::ilo_get_isSdkLogin9(VersionMgr)
extern "C"  bool PluginJinLi_ilo_get_isSdkLogin9_m1316249512 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinLi::ilo_LoginJinLiMgr10(PluginJinLi)
extern "C"  void PluginJinLi_ilo_LoginJinLiMgr10_m3360480744 (Il2CppObject * __this /* static, unused */, PluginJinLi_t2545610073 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
