﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_HumanBoneGenerated
struct UnityEngine_HumanBoneGenerated_t2851322470;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine_HumanBoneGenerated::.ctor()
extern "C"  void UnityEngine_HumanBoneGenerated__ctor_m3889902837 (UnityEngine_HumanBoneGenerated_t2851322470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanBoneGenerated::.cctor()
extern "C"  void UnityEngine_HumanBoneGenerated__cctor_m4140774744 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_HumanBoneGenerated::HumanBone_HumanBone1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_HumanBoneGenerated_HumanBone_HumanBone1_m2164135489 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanBoneGenerated::HumanBone_limit(JSVCall)
extern "C"  void UnityEngine_HumanBoneGenerated_HumanBone_limit_m3076791751 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanBoneGenerated::HumanBone_boneName(JSVCall)
extern "C"  void UnityEngine_HumanBoneGenerated_HumanBone_boneName_m1592722395 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanBoneGenerated::HumanBone_humanName(JSVCall)
extern "C"  void UnityEngine_HumanBoneGenerated_HumanBone_humanName_m3853929162 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanBoneGenerated::__Register()
extern "C"  void UnityEngine_HumanBoneGenerated___Register_m2361006322 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanBoneGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_HumanBoneGenerated_ilo_addJSCSRel1_m2323526961 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_HumanBoneGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_HumanBoneGenerated_ilo_setObject2_m3222717046 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_HumanBoneGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_HumanBoneGenerated_ilo_getObject3_m3006828188 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanBoneGenerated::ilo_setStringS4(System.Int32,System.String)
extern "C"  void UnityEngine_HumanBoneGenerated_ilo_setStringS4_m383164097 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_HumanBoneGenerated::ilo_changeJSObj5(System.Int32,System.Object)
extern "C"  void UnityEngine_HumanBoneGenerated_ilo_changeJSObj5_m2519721880 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
