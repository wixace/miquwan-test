﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._ce1d463b22e54f9e87683a2ff5702fb3
struct _ce1d463b22e54f9e87683a2ff5702fb3_t77555453;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._ce1d463b22e54f9e87683a2ff5702fb3::.ctor()
extern "C"  void _ce1d463b22e54f9e87683a2ff5702fb3__ctor_m699091920 (_ce1d463b22e54f9e87683a2ff5702fb3_t77555453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._ce1d463b22e54f9e87683a2ff5702fb3::_ce1d463b22e54f9e87683a2ff5702fb3m2(System.Int32)
extern "C"  int32_t _ce1d463b22e54f9e87683a2ff5702fb3__ce1d463b22e54f9e87683a2ff5702fb3m2_m4010166393 (_ce1d463b22e54f9e87683a2ff5702fb3_t77555453 * __this, int32_t ____ce1d463b22e54f9e87683a2ff5702fb3a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._ce1d463b22e54f9e87683a2ff5702fb3::_ce1d463b22e54f9e87683a2ff5702fb3m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _ce1d463b22e54f9e87683a2ff5702fb3__ce1d463b22e54f9e87683a2ff5702fb3m_m651156061 (_ce1d463b22e54f9e87683a2ff5702fb3_t77555453 * __this, int32_t ____ce1d463b22e54f9e87683a2ff5702fb3a0, int32_t ____ce1d463b22e54f9e87683a2ff5702fb3601, int32_t ____ce1d463b22e54f9e87683a2ff5702fb3c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
