﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T4195681701MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.String,ProductsCfgMgr/ProductInfo,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m4279507735(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t2435197095 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m1454147881_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,ProductsCfgMgr/ProductInfo,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m3413701765(__this, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t2435197095 *, String_t*, ProductInfo_t1305991238 , const MethodInfo*))Transform_1_Invoke_m357065907_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.String,ProductsCfgMgr/ProductInfo,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m1179703652(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t2435197095 *, String_t*, ProductInfo_t1305991238 , AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m2897740690_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,ProductsCfgMgr/ProductInfo,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m891033317(__this, ___result0, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t2435197095 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m506617335_gshared)(__this, ___result0, method)
