﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._cf1127bb4f13e3fc97532db610de04c3
struct _cf1127bb4f13e3fc97532db610de04c3_t1223594806;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._cf1127bb4f13e3fc97532db610de04c3::.ctor()
extern "C"  void _cf1127bb4f13e3fc97532db610de04c3__ctor_m1376243639 (_cf1127bb4f13e3fc97532db610de04c3_t1223594806 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._cf1127bb4f13e3fc97532db610de04c3::_cf1127bb4f13e3fc97532db610de04c3m2(System.Int32)
extern "C"  int32_t _cf1127bb4f13e3fc97532db610de04c3__cf1127bb4f13e3fc97532db610de04c3m2_m744723929 (_cf1127bb4f13e3fc97532db610de04c3_t1223594806 * __this, int32_t ____cf1127bb4f13e3fc97532db610de04c3a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._cf1127bb4f13e3fc97532db610de04c3::_cf1127bb4f13e3fc97532db610de04c3m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _cf1127bb4f13e3fc97532db610de04c3__cf1127bb4f13e3fc97532db610de04c3m_m3415365885 (_cf1127bb4f13e3fc97532db610de04c3_t1223594806 * __this, int32_t ____cf1127bb4f13e3fc97532db610de04c3a0, int32_t ____cf1127bb4f13e3fc97532db610de04c3141, int32_t ____cf1127bb4f13e3fc97532db610de04c3c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
