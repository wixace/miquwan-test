﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginJinritoutiao
struct PluginJinritoutiao_t1969577066;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void PluginJinritoutiao::.ctor()
extern "C"  void PluginJinritoutiao__ctor_m795933553 (PluginJinritoutiao_t1969577066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinritoutiao::Init()
extern "C"  void PluginJinritoutiao_Init_m2332516547 (PluginJinritoutiao_t1969577066 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinritoutiao::CreatRole(CEvent.ZEvent)
extern "C"  void PluginJinritoutiao_CreatRole_m3539386573 (PluginJinritoutiao_t1969577066 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinritoutiao::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginJinritoutiao_RoleUpgrade_m1867145542 (PluginJinritoutiao_t1969577066 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinritoutiao::UserPay(CEvent.ZEvent)
extern "C"  void PluginJinritoutiao_UserPay_m2954700111 (PluginJinritoutiao_t1969577066 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinritoutiao::initJrtt(System.String,System.String)
extern "C"  void PluginJinritoutiao_initJrtt_m464430419 (Il2CppObject * __this /* static, unused */, String_t* ___initId0, String_t* ___initName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinritoutiao::creatRoleJrtt(System.String,System.String,System.String,System.String)
extern "C"  void PluginJinritoutiao_creatRoleJrtt_m1631807872 (Il2CppObject * __this /* static, unused */, String_t* ___serverName0, String_t* ___roleName1, String_t* ___serverId2, String_t* ___roleId3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinritoutiao::updateRoleJrtt(System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginJinritoutiao_updateRoleJrtt_m2618020820 (Il2CppObject * __this /* static, unused */, String_t* ___serverName0, String_t* ___roleName1, String_t* ___serverId2, String_t* ___roleId3, String_t* ___vipLv4, String_t* ___roleLv5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinritoutiao::jrttP(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginJinritoutiao_jrttP_m4029212171 (Il2CppObject * __this /* static, unused */, String_t* ___roleId0, String_t* ___roleNum1, String_t* ___serverId2, String_t* ___money3, String_t* ___orderNo4, String_t* ___productName5, String_t* ___productType6, String_t* ___productId7, String_t* ___payNum8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinritoutiao::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginJinritoutiao_ilo_AddEventListener1_m1166166883 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginJinritoutiao::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginJinritoutiao_ilo_get_Instance2_m1606528179 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinritoutiao::ilo_Log3(System.Object,System.Boolean)
extern "C"  void PluginJinritoutiao_ilo_Log3_m2249444926 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginJinritoutiao::ilo_jrttP4(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginJinritoutiao_ilo_jrttP4_m2053570096 (Il2CppObject * __this /* static, unused */, String_t* ___roleId0, String_t* ___roleNum1, String_t* ___serverId2, String_t* ___money3, String_t* ___orderNo4, String_t* ___productName5, String_t* ___productType6, String_t* ___productId7, String_t* ___payNum8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
