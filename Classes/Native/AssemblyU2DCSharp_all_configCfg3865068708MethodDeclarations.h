﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// all_configCfg
struct all_configCfg_t3865068708;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void all_configCfg::.ctor()
extern "C"  void all_configCfg__ctor_m762516807 (all_configCfg_t3865068708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void all_configCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void all_configCfg_Init_m702460126 (all_configCfg_t3865068708 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
