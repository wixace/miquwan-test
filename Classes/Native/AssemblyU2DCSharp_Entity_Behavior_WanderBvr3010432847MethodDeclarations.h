﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Entity.Behavior.WanderBvr
struct WanderBvr_t3010432847;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// Entity.Behavior.IBehavior
struct IBehavior_t770859129;
// CombatEntity
struct CombatEntity_t684137495;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_Entity_Behavior_WanderBvr3010432847.h"
#include "mscorlib_System_String7231557.h"

// System.Void Entity.Behavior.WanderBvr::.ctor()
extern "C"  void WanderBvr__ctor_m3427331467 (WanderBvr_t3010432847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.EBehaviorID Entity.Behavior.WanderBvr::get_id()
extern "C"  uint8_t WanderBvr_get_id_m3760033675 (WanderBvr_t3010432847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.WanderBvr::Reason()
extern "C"  void WanderBvr_Reason_m2719448477 (WanderBvr_t3010432847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.WanderBvr::Action()
extern "C"  void WanderBvr_Action_m1916155151 (WanderBvr_t3010432847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.WanderBvr::SetParams(System.Object[])
extern "C"  void WanderBvr_SetParams_m1501740641 (WanderBvr_t3010432847 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.WanderBvr::DoEntering()
extern "C"  void WanderBvr_DoEntering_m500997838 (WanderBvr_t3010432847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.WanderBvr::MoveEnd(CEvent.ZEvent)
extern "C"  void WanderBvr_MoveEnd_m1709076968 (WanderBvr_t3010432847 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Entity.Behavior.WanderBvr::GetmovePosition()
extern "C"  Vector3_t4282066566  WanderBvr_GetmovePosition_m1670103443 (WanderBvr_t3010432847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.WanderBvr::DoLeaving()
extern "C"  void WanderBvr_DoLeaving_m2262186770 (WanderBvr_t3010432847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.WanderBvr::Pause()
extern "C"  void WanderBvr_Pause_m3481457439 (WanderBvr_t3010432847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.WanderBvr::Play()
extern "C"  void WanderBvr_Play_m814608525 (WanderBvr_t3010432847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Entity.Behavior.WanderBvr::CanTran2OtherBehavior(Entity.Behavior.IBehavior)
extern "C"  bool WanderBvr_CanTran2OtherBehavior_m1508311424 (WanderBvr_t3010432847 * __this, IBehavior_t770859129 * ___behavior0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.WanderBvr::ilo_get_entity1(Entity.Behavior.IBehavior)
extern "C"  CombatEntity_t684137495 * WanderBvr_ilo_get_entity1_m390119047 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CombatEntity Entity.Behavior.WanderBvr::ilo_get_atkTarget2(CombatEntity)
extern "C"  CombatEntity_t684137495 * WanderBvr_ilo_get_atkTarget2_m643731969 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl Entity.Behavior.WanderBvr::ilo_get_bvrCtrl3(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * WanderBvr_ilo_get_bvrCtrl3_m3820976394 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.WanderBvr::ilo_TranBehavior4(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void WanderBvr_ilo_TranBehavior4_m3795257575 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___id1, ObjectU5BU5D_t1108656482* ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Entity.Behavior.WanderBvr::ilo_GetmovePosition5(Entity.Behavior.WanderBvr)
extern "C"  Vector3_t4282066566  WanderBvr_ilo_GetmovePosition5_m809068059 (Il2CppObject * __this /* static, unused */, WanderBvr_t3010432847 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.WanderBvr::ilo_AddEventListener6(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void WanderBvr_ilo_AddEventListener6_m2912861348 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Entity.Behavior.WanderBvr::ilo_get_viewDis7(CombatEntity)
extern "C"  float WanderBvr_ilo_get_viewDis7_m3742957152 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.WanderBvr::ilo_Pause8(Entity.Behavior.IBehavior)
extern "C"  void WanderBvr_ilo_Pause8_m3318934554 (Il2CppObject * __this /* static, unused */, IBehavior_t770859129 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Entity.Behavior.WanderBvr::ilo_StopMove9(CombatEntity,System.Boolean)
extern "C"  void WanderBvr_ilo_StopMove9_m3112440280 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___isPlayAnim1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
