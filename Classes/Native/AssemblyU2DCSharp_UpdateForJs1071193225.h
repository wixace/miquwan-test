﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ICsObj
struct ICsObj_t2155308958;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UpdateForJs
struct  UpdateForJs_t1071193225  : public Il2CppObject
{
public:
	// ICsObj UpdateForJs::csObj
	Il2CppObject * ___csObj_0;
	// System.Int32 UpdateForJs::jsObjID
	int32_t ___jsObjID_1;

public:
	inline static int32_t get_offset_of_csObj_0() { return static_cast<int32_t>(offsetof(UpdateForJs_t1071193225, ___csObj_0)); }
	inline Il2CppObject * get_csObj_0() const { return ___csObj_0; }
	inline Il2CppObject ** get_address_of_csObj_0() { return &___csObj_0; }
	inline void set_csObj_0(Il2CppObject * value)
	{
		___csObj_0 = value;
		Il2CppCodeGenWriteBarrier(&___csObj_0, value);
	}

	inline static int32_t get_offset_of_jsObjID_1() { return static_cast<int32_t>(offsetof(UpdateForJs_t1071193225, ___jsObjID_1)); }
	inline int32_t get_jsObjID_1() const { return ___jsObjID_1; }
	inline int32_t* get_address_of_jsObjID_1() { return &___jsObjID_1; }
	inline void set_jsObjID_1(int32_t value)
	{
		___jsObjID_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
