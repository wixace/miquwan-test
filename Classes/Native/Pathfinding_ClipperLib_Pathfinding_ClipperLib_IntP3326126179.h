﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.ClipperLib.IntPoint
struct  IntPoint_t3326126179 
{
public:
	// System.Int64 Pathfinding.ClipperLib.IntPoint::X
	int64_t ___X_0;
	// System.Int64 Pathfinding.ClipperLib.IntPoint::Y
	int64_t ___Y_1;

public:
	inline static int32_t get_offset_of_X_0() { return static_cast<int32_t>(offsetof(IntPoint_t3326126179, ___X_0)); }
	inline int64_t get_X_0() const { return ___X_0; }
	inline int64_t* get_address_of_X_0() { return &___X_0; }
	inline void set_X_0(int64_t value)
	{
		___X_0 = value;
	}

	inline static int32_t get_offset_of_Y_1() { return static_cast<int32_t>(offsetof(IntPoint_t3326126179, ___Y_1)); }
	inline int64_t get_Y_1() const { return ___Y_1; }
	inline int64_t* get_address_of_Y_1() { return &___Y_1; }
	inline void set_Y_1(int64_t value)
	{
		___Y_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Pathfinding.ClipperLib.IntPoint
struct IntPoint_t3326126179_marshaled_pinvoke
{
	int64_t ___X_0;
	int64_t ___Y_1;
};
// Native definition for marshalling of: Pathfinding.ClipperLib.IntPoint
struct IntPoint_t3326126179_marshaled_com
{
	int64_t ___X_0;
	int64_t ___Y_1;
};
