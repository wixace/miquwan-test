﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>>
struct Dictionary_2_t3565730160;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AntiCheatMgr
struct  AntiCheatMgr_t3474304487  : public Il2CppObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>> AntiCheatMgr::heroAntiCheatCSData
	Dictionary_2_t3565730160 * ___heroAntiCheatCSData_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.Dictionary`2<System.String,ACData>> AntiCheatMgr::heroAntiCheatData
	Dictionary_2_t3565730160 * ___heroAntiCheatData_1;

public:
	inline static int32_t get_offset_of_heroAntiCheatCSData_0() { return static_cast<int32_t>(offsetof(AntiCheatMgr_t3474304487, ___heroAntiCheatCSData_0)); }
	inline Dictionary_2_t3565730160 * get_heroAntiCheatCSData_0() const { return ___heroAntiCheatCSData_0; }
	inline Dictionary_2_t3565730160 ** get_address_of_heroAntiCheatCSData_0() { return &___heroAntiCheatCSData_0; }
	inline void set_heroAntiCheatCSData_0(Dictionary_2_t3565730160 * value)
	{
		___heroAntiCheatCSData_0 = value;
		Il2CppCodeGenWriteBarrier(&___heroAntiCheatCSData_0, value);
	}

	inline static int32_t get_offset_of_heroAntiCheatData_1() { return static_cast<int32_t>(offsetof(AntiCheatMgr_t3474304487, ___heroAntiCheatData_1)); }
	inline Dictionary_2_t3565730160 * get_heroAntiCheatData_1() const { return ___heroAntiCheatData_1; }
	inline Dictionary_2_t3565730160 ** get_address_of_heroAntiCheatData_1() { return &___heroAntiCheatData_1; }
	inline void set_heroAntiCheatData_1(Dictionary_2_t3565730160 * value)
	{
		___heroAntiCheatData_1 = value;
		Il2CppCodeGenWriteBarrier(&___heroAntiCheatData_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
