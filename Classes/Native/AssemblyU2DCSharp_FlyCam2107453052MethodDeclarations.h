﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlyCam
struct FlyCam_t2107453052;

#include "codegen/il2cpp-codegen.h"

// System.Void FlyCam::.ctor()
extern "C"  void FlyCam__ctor_m1429258655 (FlyCam_t2107453052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlyCam::Update()
extern "C"  void FlyCam_Update_m3084207438 (FlyCam_t2107453052 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
