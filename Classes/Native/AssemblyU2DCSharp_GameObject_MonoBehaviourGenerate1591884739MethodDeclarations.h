﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameObject_MonoBehaviourGenerated
struct GameObject_MonoBehaviourGenerated_t1591884739;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void GameObject_MonoBehaviourGenerated::.ctor()
extern "C"  void GameObject_MonoBehaviourGenerated__ctor_m3814599944 (GameObject_MonoBehaviourGenerated_t1591884739 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameObject_MonoBehaviourGenerated::.cctor()
extern "C"  void GameObject_MonoBehaviourGenerated__cctor_m1806385061 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameObject_MonoBehaviourGenerated::GameObject_MonoBehaviour_GetOrCreateComponentT1__GameObject(JSVCall,System.Int32)
extern "C"  bool GameObject_MonoBehaviourGenerated_GameObject_MonoBehaviour_GetOrCreateComponentT1__GameObject_m4253513491 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameObject_MonoBehaviourGenerated::GameObject_MonoBehaviour_SafeGetComponentT1__GameObject(JSVCall,System.Int32)
extern "C"  bool GameObject_MonoBehaviourGenerated_GameObject_MonoBehaviour_SafeGetComponentT1__GameObject_m1028646719 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameObject_MonoBehaviourGenerated::__Register()
extern "C"  void GameObject_MonoBehaviourGenerated___Register_m3300799679 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameObject_MonoBehaviourGenerated::ilo_getObject1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * GameObject_MonoBehaviourGenerated_ilo_getObject1_m2654918429 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameObject_MonoBehaviourGenerated::ilo_setWhatever2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  void GameObject_MonoBehaviourGenerated_ilo_setWhatever2_m2092128050 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
