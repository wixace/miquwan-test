﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._f1c0c250f177a2fd0c562538bec03b16
struct _f1c0c250f177a2fd0c562538bec03b16_t2380164245;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._f1c0c250f177a2fd0c562538bec03b16::.ctor()
extern "C"  void _f1c0c250f177a2fd0c562538bec03b16__ctor_m303894840 (_f1c0c250f177a2fd0c562538bec03b16_t2380164245 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f1c0c250f177a2fd0c562538bec03b16::_f1c0c250f177a2fd0c562538bec03b16m2(System.Int32)
extern "C"  int32_t _f1c0c250f177a2fd0c562538bec03b16__f1c0c250f177a2fd0c562538bec03b16m2_m2557976953 (_f1c0c250f177a2fd0c562538bec03b16_t2380164245 * __this, int32_t ____f1c0c250f177a2fd0c562538bec03b16a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f1c0c250f177a2fd0c562538bec03b16::_f1c0c250f177a2fd0c562538bec03b16m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _f1c0c250f177a2fd0c562538bec03b16__f1c0c250f177a2fd0c562538bec03b16m_m3697582429 (_f1c0c250f177a2fd0c562538bec03b16_t2380164245 * __this, int32_t ____f1c0c250f177a2fd0c562538bec03b16a0, int32_t ____f1c0c250f177a2fd0c562538bec03b16951, int32_t ____f1c0c250f177a2fd0c562538bec03b16c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
