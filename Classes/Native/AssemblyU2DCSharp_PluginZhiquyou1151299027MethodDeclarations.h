﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginZhiquyou
struct PluginZhiquyou_t1151299027;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// VersionInfo
struct VersionInfo_t2356638086;
// VersionMgr
struct VersionMgr_t1322950208;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Char[]
struct CharU5BU5D_t3324145743;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginZhiquyou1151299027.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void PluginZhiquyou::.ctor()
extern "C"  void PluginZhiquyou__ctor_m2181643304 (PluginZhiquyou_t1151299027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhiquyou::Init()
extern "C"  void PluginZhiquyou_Init_m714648876 (PluginZhiquyou_t1151299027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginZhiquyou::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginZhiquyou_ReqSDKHttpLogin_m1241454077 (PluginZhiquyou_t1151299027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginZhiquyou::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginZhiquyou_IsLoginSuccess_m2804857131 (PluginZhiquyou_t1151299027 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhiquyou::OpenUserLogin()
extern "C"  void PluginZhiquyou_OpenUserLogin_m3614946938 (PluginZhiquyou_t1151299027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhiquyou::RolelvUpinfo(CEvent.ZEvent)
extern "C"  void PluginZhiquyou_RolelvUpinfo_m1179680598 (PluginZhiquyou_t1151299027 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhiquyou::UserPay(CEvent.ZEvent)
extern "C"  void PluginZhiquyou_UserPay_m2437054392 (PluginZhiquyou_t1151299027 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhiquyou::OnInitSuccess(System.String)
extern "C"  void PluginZhiquyou_OnInitSuccess_m2659644552 (PluginZhiquyou_t1151299027 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhiquyou::OnInitFail(System.String)
extern "C"  void PluginZhiquyou_OnInitFail_m3412462009 (PluginZhiquyou_t1151299027 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhiquyou::OnLoginSuccess(System.String)
extern "C"  void PluginZhiquyou_OnLoginSuccess_m1234314541 (PluginZhiquyou_t1151299027 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhiquyou::OnLoginFail(System.String)
extern "C"  void PluginZhiquyou_OnLoginFail_m1336943412 (PluginZhiquyou_t1151299027 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhiquyou::OnPaySuccess(System.String)
extern "C"  void PluginZhiquyou_OnPaySuccess_m3005869740 (PluginZhiquyou_t1151299027 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhiquyou::OnPayFail(System.String)
extern "C"  void PluginZhiquyou_OnPayFail_m1706942997 (PluginZhiquyou_t1151299027 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhiquyou::OnExitGameSuccess(System.String)
extern "C"  void PluginZhiquyou_OnExitGameSuccess_m3219395752 (PluginZhiquyou_t1151299027 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhiquyou::OnLogoutSuccess(System.String)
extern "C"  void PluginZhiquyou_OnLogoutSuccess_m3907555074 (PluginZhiquyou_t1151299027 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhiquyou::OnLogoutFail(System.String)
extern "C"  void PluginZhiquyou_OnLogoutFail_m2603422079 (PluginZhiquyou_t1151299027 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhiquyou::Initsdk(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginZhiquyou_Initsdk_m2627455026 (PluginZhiquyou_t1151299027 * __this, String_t* ___appidParam0, String_t* ___appkeyParam1, String_t* ___adId2, String_t* ___appAdKeyParam3, String_t* ___gameId4, String_t* ___channelIdParam5, String_t* ___subchannelParam6, String_t* ___platformParam7, String_t* ___contactParam8, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhiquyou::Login()
extern "C"  void PluginZhiquyou_Login_m3368311407 (PluginZhiquyou_t1151299027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhiquyou::Pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginZhiquyou_Pay_m3320306336 (PluginZhiquyou_t1151299027 * __this, String_t* ___orderId0, String_t* ___orderRmb1, String_t* ___productId2, String_t* ___productName3, String_t* ___productDescription4, String_t* ___createTime5, String_t* ___notifyUrl6, String_t* ___callbackInfo7, String_t* ___review8, String_t* ___serverId9, String_t* ___iapH5PayWay10, String_t* ___txh5payWay11, String_t* ___alh5payWay12, String_t* ___appSchemes13, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhiquyou::<OnLogoutSuccess>m__483()
extern "C"  void PluginZhiquyou_U3COnLogoutSuccessU3Em__483_m1131590664 (PluginZhiquyou_t1151299027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginZhiquyou::ilo_get_currentVS1(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginZhiquyou_ilo_get_currentVS1_m2168728256 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] PluginZhiquyou::ilo_SplitStr2(System.String,System.Char[])
extern "C"  StringU5BU5D_t4054002952* PluginZhiquyou_ilo_SplitStr2_m2758096874 (Il2CppObject * __this /* static, unused */, String_t* ___sFile0, CharU5BU5D_t3324145743* ___separator1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhiquyou::ilo_AddEventListener3(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginZhiquyou_ilo_AddEventListener3_m2072976394 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginZhiquyou::ilo_get_Instance4()
extern "C"  VersionMgr_t1322950208 * PluginZhiquyou_ilo_get_Instance4_m1821713388 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhiquyou::ilo_OpenUserLogin5(PluginZhiquyou)
extern "C"  void PluginZhiquyou_ilo_OpenUserLogin5_m1666926749 (Il2CppObject * __this /* static, unused */, PluginZhiquyou_t1151299027 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhiquyou::ilo_Log6(System.Object,System.Boolean)
extern "C"  void PluginZhiquyou_ilo_Log6_m2309721962 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginZhiquyou::ilo_DispatchEvent7(CEvent.ZEvent)
extern "C"  void PluginZhiquyou_ilo_DispatchEvent7_m90456309 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
