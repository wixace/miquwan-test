﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._a4701da7d683d85f933526f3364b817c
struct _a4701da7d683d85f933526f3364b817c_t501932483;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__a4701da7d683d85f933526f33501932483.h"

// System.Void Little._a4701da7d683d85f933526f3364b817c::.ctor()
extern "C"  void _a4701da7d683d85f933526f3364b817c__ctor_m1165397962 (_a4701da7d683d85f933526f3364b817c_t501932483 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a4701da7d683d85f933526f3364b817c::_a4701da7d683d85f933526f3364b817cm2(System.Int32)
extern "C"  int32_t _a4701da7d683d85f933526f3364b817c__a4701da7d683d85f933526f3364b817cm2_m2574066361 (_a4701da7d683d85f933526f3364b817c_t501932483 * __this, int32_t ____a4701da7d683d85f933526f3364b817ca0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a4701da7d683d85f933526f3364b817c::_a4701da7d683d85f933526f3364b817cm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _a4701da7d683d85f933526f3364b817c__a4701da7d683d85f933526f3364b817cm_m1648822301 (_a4701da7d683d85f933526f3364b817c_t501932483 * __this, int32_t ____a4701da7d683d85f933526f3364b817ca0, int32_t ____a4701da7d683d85f933526f3364b817c711, int32_t ____a4701da7d683d85f933526f3364b817cc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a4701da7d683d85f933526f3364b817c::ilo__a4701da7d683d85f933526f3364b817cm21(Little._a4701da7d683d85f933526f3364b817c,System.Int32)
extern "C"  int32_t _a4701da7d683d85f933526f3364b817c_ilo__a4701da7d683d85f933526f3364b817cm21_m1148618986 (Il2CppObject * __this /* static, unused */, _a4701da7d683d85f933526f3364b817c_t501932483 * ____this0, int32_t ____a4701da7d683d85f933526f3364b817ca1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
