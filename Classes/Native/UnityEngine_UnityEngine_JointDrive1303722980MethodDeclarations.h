﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.JointDrive
struct JointDrive_t1303722980;
struct JointDrive_t1303722980_marshaled_pinvoke;
struct JointDrive_t1303722980_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_JointDrive1303722980.h"

// System.Single UnityEngine.JointDrive::get_positionSpring()
extern "C"  float JointDrive_get_positionSpring_m44700450 (JointDrive_t1303722980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointDrive::set_positionSpring(System.Single)
extern "C"  void JointDrive_set_positionSpring_m1953684321 (JointDrive_t1303722980 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.JointDrive::get_positionDamper()
extern "C"  float JointDrive_get_positionDamper_m3402724514 (JointDrive_t1303722980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointDrive::set_positionDamper(System.Single)
extern "C"  void JointDrive_set_positionDamper_m1542842849 (JointDrive_t1303722980 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.JointDrive::get_maximumForce()
extern "C"  float JointDrive_get_maximumForce_m1332878519 (JointDrive_t1303722980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.JointDrive::set_maximumForce(System.Single)
extern "C"  void JointDrive_set_maximumForce_m140268012 (JointDrive_t1303722980 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct JointDrive_t1303722980;
struct JointDrive_t1303722980_marshaled_pinvoke;

extern "C" void JointDrive_t1303722980_marshal_pinvoke(const JointDrive_t1303722980& unmarshaled, JointDrive_t1303722980_marshaled_pinvoke& marshaled);
extern "C" void JointDrive_t1303722980_marshal_pinvoke_back(const JointDrive_t1303722980_marshaled_pinvoke& marshaled, JointDrive_t1303722980& unmarshaled);
extern "C" void JointDrive_t1303722980_marshal_pinvoke_cleanup(JointDrive_t1303722980_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct JointDrive_t1303722980;
struct JointDrive_t1303722980_marshaled_com;

extern "C" void JointDrive_t1303722980_marshal_com(const JointDrive_t1303722980& unmarshaled, JointDrive_t1303722980_marshaled_com& marshaled);
extern "C" void JointDrive_t1303722980_marshal_com_back(const JointDrive_t1303722980_marshaled_com& marshaled, JointDrive_t1303722980& unmarshaled);
extern "C" void JointDrive_t1303722980_marshal_com_cleanup(JointDrive_t1303722980_marshaled_com& marshaled);
