﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AudioReverbZone
struct AudioReverbZone_t3879424842;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AudioReverbPreset2425455165.h"

// System.Void UnityEngine.AudioReverbZone::.ctor()
extern "C"  void AudioReverbZone__ctor_m602781125 (AudioReverbZone_t3879424842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbZone::get_minDistance()
extern "C"  float AudioReverbZone_get_minDistance_m155605005 (AudioReverbZone_t3879424842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbZone::set_minDistance(System.Single)
extern "C"  void AudioReverbZone_set_minDistance_m921777342 (AudioReverbZone_t3879424842 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbZone::get_maxDistance()
extern "C"  float AudioReverbZone_get_maxDistance_m2628266143 (AudioReverbZone_t3879424842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbZone::set_maxDistance(System.Single)
extern "C"  void AudioReverbZone_set_maxDistance_m3984482668 (AudioReverbZone_t3879424842 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioReverbPreset UnityEngine.AudioReverbZone::get_reverbPreset()
extern "C"  int32_t AudioReverbZone_get_reverbPreset_m2703197014 (AudioReverbZone_t3879424842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbZone::set_reverbPreset(UnityEngine.AudioReverbPreset)
extern "C"  void AudioReverbZone_set_reverbPreset_m2655407253 (AudioReverbZone_t3879424842 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AudioReverbZone::get_room()
extern "C"  int32_t AudioReverbZone_get_room_m943769187 (AudioReverbZone_t3879424842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbZone::set_room(System.Int32)
extern "C"  void AudioReverbZone_set_room_m4291116840 (AudioReverbZone_t3879424842 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AudioReverbZone::get_roomHF()
extern "C"  int32_t AudioReverbZone_get_roomHF_m725071713 (AudioReverbZone_t3879424842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbZone::set_roomHF(System.Int32)
extern "C"  void AudioReverbZone_set_roomHF_m1841591846 (AudioReverbZone_t3879424842 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AudioReverbZone::get_roomLF()
extern "C"  int32_t AudioReverbZone_get_roomLF_m725190877 (AudioReverbZone_t3879424842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbZone::set_roomLF(System.Int32)
extern "C"  void AudioReverbZone_set_roomLF_m4094422434 (AudioReverbZone_t3879424842 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbZone::get_decayTime()
extern "C"  float AudioReverbZone_get_decayTime_m1105242765 (AudioReverbZone_t3879424842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbZone::set_decayTime(System.Single)
extern "C"  void AudioReverbZone_set_decayTime_m3125572158 (AudioReverbZone_t3879424842 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbZone::get_decayHFRatio()
extern "C"  float AudioReverbZone_get_decayHFRatio_m2178608431 (AudioReverbZone_t3879424842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbZone::set_decayHFRatio(System.Single)
extern "C"  void AudioReverbZone_set_decayHFRatio_m166767324 (AudioReverbZone_t3879424842 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AudioReverbZone::get_reflections()
extern "C"  int32_t AudioReverbZone_get_reflections_m1718016898 (AudioReverbZone_t3879424842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbZone::set_reflections(System.Int32)
extern "C"  void AudioReverbZone_set_reflections_m2213741407 (AudioReverbZone_t3879424842 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbZone::get_reflectionsDelay()
extern "C"  float AudioReverbZone_get_reflectionsDelay_m331679159 (AudioReverbZone_t3879424842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbZone::set_reflectionsDelay(System.Single)
extern "C"  void AudioReverbZone_set_reflectionsDelay_m3086462804 (AudioReverbZone_t3879424842 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.AudioReverbZone::get_reverb()
extern "C"  int32_t AudioReverbZone_get_reverb_m634263514 (AudioReverbZone_t3879424842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbZone::set_reverb(System.Int32)
extern "C"  void AudioReverbZone_set_reverb_m364439839 (AudioReverbZone_t3879424842 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbZone::get_reverbDelay()
extern "C"  float AudioReverbZone_get_reverbDelay_m598438903 (AudioReverbZone_t3879424842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbZone::set_reverbDelay(System.Single)
extern "C"  void AudioReverbZone_set_reverbDelay_m3249161492 (AudioReverbZone_t3879424842 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbZone::get_HFReference()
extern "C"  float AudioReverbZone_get_HFReference_m3916037651 (AudioReverbZone_t3879424842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbZone::set_HFReference(System.Single)
extern "C"  void AudioReverbZone_set_HFReference_m60688504 (AudioReverbZone_t3879424842 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbZone::get_LFReference()
extern "C"  float AudioReverbZone_get_LFReference_m2742425623 (AudioReverbZone_t3879424842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbZone::set_LFReference(System.Single)
extern "C"  void AudioReverbZone_set_LFReference_m184767732 (AudioReverbZone_t3879424842 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbZone::get_roomRolloffFactor()
extern "C"  float AudioReverbZone_get_roomRolloffFactor_m474180300 (AudioReverbZone_t3879424842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbZone::set_roomRolloffFactor(System.Single)
extern "C"  void AudioReverbZone_set_roomRolloffFactor_m1078480991 (AudioReverbZone_t3879424842 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbZone::get_diffusion()
extern "C"  float AudioReverbZone_get_diffusion_m948434219 (AudioReverbZone_t3879424842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbZone::set_diffusion(System.Single)
extern "C"  void AudioReverbZone_set_diffusion_m2385500256 (AudioReverbZone_t3879424842 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioReverbZone::get_density()
extern "C"  float AudioReverbZone_get_density_m1013819086 (AudioReverbZone_t3879424842 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioReverbZone::set_density(System.Single)
extern "C"  void AudioReverbZone_set_density_m1076144029 (AudioReverbZone_t3879424842 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
