﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._36949f319cd37557ab6761367603d7d8
struct _36949f319cd37557ab6761367603d7d8_t97615140;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._36949f319cd37557ab6761367603d7d8::.ctor()
extern "C"  void _36949f319cd37557ab6761367603d7d8__ctor_m3886166409 (_36949f319cd37557ab6761367603d7d8_t97615140 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._36949f319cd37557ab6761367603d7d8::_36949f319cd37557ab6761367603d7d8m2(System.Int32)
extern "C"  int32_t _36949f319cd37557ab6761367603d7d8__36949f319cd37557ab6761367603d7d8m2_m1308548889 (_36949f319cd37557ab6761367603d7d8_t97615140 * __this, int32_t ____36949f319cd37557ab6761367603d7d8a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._36949f319cd37557ab6761367603d7d8::_36949f319cd37557ab6761367603d7d8m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _36949f319cd37557ab6761367603d7d8__36949f319cd37557ab6761367603d7d8m_m2371876797 (_36949f319cd37557ab6761367603d7d8_t97615140 * __this, int32_t ____36949f319cd37557ab6761367603d7d8a0, int32_t ____36949f319cd37557ab6761367603d7d8981, int32_t ____36949f319cd37557ab6761367603d7d8c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
