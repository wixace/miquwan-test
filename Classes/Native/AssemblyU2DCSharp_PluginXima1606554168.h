﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// XiMaPay
struct XiMaPay_t3871054307;
// XiMaNewPay
struct XiMaNewPay_t2653418445;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "AssemblyU2DCSharp_PluginXima_BigPayType3764069177.h"
#include "AssemblyU2DCSharp_PluginXima_PayType53038859.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginXima
struct  PluginXima_t1606554168  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginXima::password
	String_t* ___password_4;
	// System.String PluginXima::username
	String_t* ___username_5;
	// PluginXima/BigPayType PluginXima::bigPayType
	int32_t ___bigPayType_6;
	// PluginXima/PayType PluginXima::allPayType
	int32_t ___allPayType_7;
	// XiMaPay PluginXima::ximaSDK
	XiMaPay_t3871054307 * ___ximaSDK_8;
	// XiMaNewPay PluginXima::ximaNewSDK
	XiMaNewPay_t2653418445 * ___ximaNewSDK_9;
	// Mihua.SDK.PayInfo PluginXima::payInfo
	PayInfo_t1775308120 * ___payInfo_10;
	// System.Int32 PluginXima::creditNum
	int32_t ___creditNum_11;
	// System.Int32 PluginXima::rechargeCount
	int32_t ___rechargeCount_12;

public:
	inline static int32_t get_offset_of_password_4() { return static_cast<int32_t>(offsetof(PluginXima_t1606554168, ___password_4)); }
	inline String_t* get_password_4() const { return ___password_4; }
	inline String_t** get_address_of_password_4() { return &___password_4; }
	inline void set_password_4(String_t* value)
	{
		___password_4 = value;
		Il2CppCodeGenWriteBarrier(&___password_4, value);
	}

	inline static int32_t get_offset_of_username_5() { return static_cast<int32_t>(offsetof(PluginXima_t1606554168, ___username_5)); }
	inline String_t* get_username_5() const { return ___username_5; }
	inline String_t** get_address_of_username_5() { return &___username_5; }
	inline void set_username_5(String_t* value)
	{
		___username_5 = value;
		Il2CppCodeGenWriteBarrier(&___username_5, value);
	}

	inline static int32_t get_offset_of_bigPayType_6() { return static_cast<int32_t>(offsetof(PluginXima_t1606554168, ___bigPayType_6)); }
	inline int32_t get_bigPayType_6() const { return ___bigPayType_6; }
	inline int32_t* get_address_of_bigPayType_6() { return &___bigPayType_6; }
	inline void set_bigPayType_6(int32_t value)
	{
		___bigPayType_6 = value;
	}

	inline static int32_t get_offset_of_allPayType_7() { return static_cast<int32_t>(offsetof(PluginXima_t1606554168, ___allPayType_7)); }
	inline int32_t get_allPayType_7() const { return ___allPayType_7; }
	inline int32_t* get_address_of_allPayType_7() { return &___allPayType_7; }
	inline void set_allPayType_7(int32_t value)
	{
		___allPayType_7 = value;
	}

	inline static int32_t get_offset_of_ximaSDK_8() { return static_cast<int32_t>(offsetof(PluginXima_t1606554168, ___ximaSDK_8)); }
	inline XiMaPay_t3871054307 * get_ximaSDK_8() const { return ___ximaSDK_8; }
	inline XiMaPay_t3871054307 ** get_address_of_ximaSDK_8() { return &___ximaSDK_8; }
	inline void set_ximaSDK_8(XiMaPay_t3871054307 * value)
	{
		___ximaSDK_8 = value;
		Il2CppCodeGenWriteBarrier(&___ximaSDK_8, value);
	}

	inline static int32_t get_offset_of_ximaNewSDK_9() { return static_cast<int32_t>(offsetof(PluginXima_t1606554168, ___ximaNewSDK_9)); }
	inline XiMaNewPay_t2653418445 * get_ximaNewSDK_9() const { return ___ximaNewSDK_9; }
	inline XiMaNewPay_t2653418445 ** get_address_of_ximaNewSDK_9() { return &___ximaNewSDK_9; }
	inline void set_ximaNewSDK_9(XiMaNewPay_t2653418445 * value)
	{
		___ximaNewSDK_9 = value;
		Il2CppCodeGenWriteBarrier(&___ximaNewSDK_9, value);
	}

	inline static int32_t get_offset_of_payInfo_10() { return static_cast<int32_t>(offsetof(PluginXima_t1606554168, ___payInfo_10)); }
	inline PayInfo_t1775308120 * get_payInfo_10() const { return ___payInfo_10; }
	inline PayInfo_t1775308120 ** get_address_of_payInfo_10() { return &___payInfo_10; }
	inline void set_payInfo_10(PayInfo_t1775308120 * value)
	{
		___payInfo_10 = value;
		Il2CppCodeGenWriteBarrier(&___payInfo_10, value);
	}

	inline static int32_t get_offset_of_creditNum_11() { return static_cast<int32_t>(offsetof(PluginXima_t1606554168, ___creditNum_11)); }
	inline int32_t get_creditNum_11() const { return ___creditNum_11; }
	inline int32_t* get_address_of_creditNum_11() { return &___creditNum_11; }
	inline void set_creditNum_11(int32_t value)
	{
		___creditNum_11 = value;
	}

	inline static int32_t get_offset_of_rechargeCount_12() { return static_cast<int32_t>(offsetof(PluginXima_t1606554168, ___rechargeCount_12)); }
	inline int32_t get_rechargeCount_12() const { return ___rechargeCount_12; }
	inline int32_t* get_address_of_rechargeCount_12() { return &___rechargeCount_12; }
	inline void set_rechargeCount_12(int32_t value)
	{
		___rechargeCount_12 = value;
	}
};

struct PluginXima_t1606554168_StaticFields
{
public:
	// System.String PluginXima::appKey
	String_t* ___appKey_3;

public:
	inline static int32_t get_offset_of_appKey_3() { return static_cast<int32_t>(offsetof(PluginXima_t1606554168_StaticFields, ___appKey_3)); }
	inline String_t* get_appKey_3() const { return ___appKey_3; }
	inline String_t** get_address_of_appKey_3() { return &___appKey_3; }
	inline void set_appKey_3(String_t* value)
	{
		___appKey_3 = value;
		Il2CppCodeGenWriteBarrier(&___appKey_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
