﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DepthOfFieldScatter
struct DepthOfFieldScatter_t1867756894;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void DepthOfFieldScatter::.ctor()
extern "C"  void DepthOfFieldScatter__ctor_m2805829316 (DepthOfFieldScatter_t1867756894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DepthOfFieldScatter::CheckResources()
extern "C"  bool DepthOfFieldScatter_CheckResources_m1510947159 (DepthOfFieldScatter_t1867756894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfFieldScatter::OnEnable()
extern "C"  void DepthOfFieldScatter_OnEnable_m1879216706 (DepthOfFieldScatter_t1867756894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfFieldScatter::OnDisable()
extern "C"  void DepthOfFieldScatter_OnDisable_m2862080299 (DepthOfFieldScatter_t1867756894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfFieldScatter::ReleaseComputeResources()
extern "C"  void DepthOfFieldScatter_ReleaseComputeResources_m732061239 (DepthOfFieldScatter_t1867756894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfFieldScatter::CreateComputeResources()
extern "C"  void DepthOfFieldScatter_CreateComputeResources_m412588682 (DepthOfFieldScatter_t1867756894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single DepthOfFieldScatter::FocalDistance01(System.Single)
extern "C"  float DepthOfFieldScatter_FocalDistance01_m2342287714 (DepthOfFieldScatter_t1867756894 * __this, float ___worldDist0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfFieldScatter::WriteCoc(UnityEngine.RenderTexture,System.Boolean)
extern "C"  void DepthOfFieldScatter_WriteCoc_m1274203505 (DepthOfFieldScatter_t1867756894 * __this, RenderTexture_t1963041563 * ___fromTo0, bool ___fgDilate1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfFieldScatter::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void DepthOfFieldScatter_OnRenderImage_m2404279066 (DepthOfFieldScatter_t1867756894 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DepthOfFieldScatter::Main()
extern "C"  void DepthOfFieldScatter_Main_m144552313 (DepthOfFieldScatter_t1867756894 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
