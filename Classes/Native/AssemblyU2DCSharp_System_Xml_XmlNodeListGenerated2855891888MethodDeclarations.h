﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Xml_XmlNodeListGenerated
struct System_Xml_XmlNodeListGenerated_t2855891888;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Xml_XmlNodeListGenerated::.ctor()
extern "C"  void System_Xml_XmlNodeListGenerated__ctor_m2886904251 (System_Xml_XmlNodeListGenerated_t2855891888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeListGenerated::XmlNodeList_Count(JSVCall)
extern "C"  void System_Xml_XmlNodeListGenerated_XmlNodeList_Count_m3248304871 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeListGenerated::XmlNodeList_ItemOf_Int32(JSVCall)
extern "C"  void System_Xml_XmlNodeListGenerated_XmlNodeList_ItemOf_Int32_m2365859805 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeListGenerated::XmlNodeList_GetEnumerator(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeListGenerated_XmlNodeList_GetEnumerator_m705899631 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XmlNodeListGenerated::XmlNodeList_Item__Int32(JSVCall,System.Int32)
extern "C"  bool System_Xml_XmlNodeListGenerated_XmlNodeList_Item__Int32_m413495056 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeListGenerated::__Register()
extern "C"  void System_Xml_XmlNodeListGenerated___Register_m384479212 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlNodeListGenerated::ilo_setInt321(System.Int32,System.Int32)
extern "C"  void System_Xml_XmlNodeListGenerated_ilo_setInt321_m2022329723 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Xml_XmlNodeListGenerated::ilo_setObject2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_Xml_XmlNodeListGenerated_ilo_setObject2_m2918415636 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
