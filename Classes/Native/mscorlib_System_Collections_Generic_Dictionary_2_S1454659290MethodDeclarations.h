﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt16,System.Object>
struct ShimEnumerator_t1454659290;
// System.Collections.Generic.Dictionary`2<System.UInt16,System.Object>
struct Dictionary_2_t1738881263;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt16,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m1055072154_gshared (ShimEnumerator_t1454659290 * __this, Dictionary_2_t1738881263 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m1055072154(__this, ___host0, method) ((  void (*) (ShimEnumerator_t1454659290 *, Dictionary_2_t1738881263 *, const MethodInfo*))ShimEnumerator__ctor_m1055072154_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt16,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1558061383_gshared (ShimEnumerator_t1454659290 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1558061383(__this, method) ((  bool (*) (ShimEnumerator_t1454659290 *, const MethodInfo*))ShimEnumerator_MoveNext_m1558061383_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt16,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m1381073773_gshared (ShimEnumerator_t1454659290 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1381073773(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t1454659290 *, const MethodInfo*))ShimEnumerator_get_Entry_m1381073773_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt16,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m3241823880_gshared (ShimEnumerator_t1454659290 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m3241823880(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1454659290 *, const MethodInfo*))ShimEnumerator_get_Key_m3241823880_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt16,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m2589895450_gshared (ShimEnumerator_t1454659290 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m2589895450(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1454659290 *, const MethodInfo*))ShimEnumerator_get_Value_m2589895450_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt16,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m3528153890_gshared (ShimEnumerator_t1454659290 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m3528153890(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t1454659290 *, const MethodInfo*))ShimEnumerator_get_Current_m3528153890_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.UInt16,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m2049241836_gshared (ShimEnumerator_t1454659290 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m2049241836(__this, method) ((  void (*) (ShimEnumerator_t1454659290 *, const MethodInfo*))ShimEnumerator_Reset_m2049241836_gshared)(__this, method)
