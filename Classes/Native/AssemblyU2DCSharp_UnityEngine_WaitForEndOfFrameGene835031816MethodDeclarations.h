﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_WaitForEndOfFrameGenerated
struct UnityEngine_WaitForEndOfFrameGenerated_t835031816;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_WaitForEndOfFrameGenerated::.ctor()
extern "C"  void UnityEngine_WaitForEndOfFrameGenerated__ctor_m3626785427 (UnityEngine_WaitForEndOfFrameGenerated_t835031816 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WaitForEndOfFrameGenerated::WaitForEndOfFrame_WaitForEndOfFrame1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WaitForEndOfFrameGenerated_WaitForEndOfFrame_WaitForEndOfFrame1_m675604391 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WaitForEndOfFrameGenerated::__Register()
extern "C"  void UnityEngine_WaitForEndOfFrameGenerated___Register_m1475724820 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WaitForEndOfFrameGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_WaitForEndOfFrameGenerated_ilo_addJSCSRel1_m3379913679 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
