﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIPanelGenerated
struct UIPanelGenerated_t504133535;
// JSVCall
struct JSVCall_t3708497963;
// UIPanel/OnGeometryUpdated
struct OnGeometryUpdated_t4285773675;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// UIPanel/OnClippingMoved
struct OnClippingMoved_t1586118419;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.Delegate
struct Delegate_t3310234105;
// UIPanel
struct UIPanel_t295209936;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// UnityEngine.Transform
struct Transform_t1659122786;
// UIDrawCall
struct UIDrawCall_t913273974;
// UIWidget
struct UIWidget_t769069560;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_UIDrawCall_Clipping3937989211.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"

// System.Void UIPanelGenerated::.ctor()
extern "C"  void UIPanelGenerated__ctor_m3036402140 (UIPanelGenerated_t504133535 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_UIPanel1(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_UIPanel1_m1878788220 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_list(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_list_m301939410 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIPanel/OnGeometryUpdated UIPanelGenerated::UIPanel_onGeometryUpdated_GetDelegate_member1_arg0(CSRepresentedObject)
extern "C"  OnGeometryUpdated_t4285773675 * UIPanelGenerated_UIPanel_onGeometryUpdated_GetDelegate_member1_arg0_m2525347970 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_onGeometryUpdated(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_onGeometryUpdated_m384369874 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_showInPanelTool(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_showInPanelTool_m3308431746 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_generateNormals(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_generateNormals_m2779089861 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_widgetsAreStatic(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_widgetsAreStatic_m965312541 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_cullWhileDragging(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_cullWhileDragging_m55938574 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_alwaysOnScreen(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_alwaysOnScreen_m944956438 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_anchorOffset(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_anchorOffset_m3913348360 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_softBorderPadding(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_softBorderPadding_m2414100929 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_renderQueue(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_renderQueue_m2993305441 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_startingRenderQueue(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_startingRenderQueue_m685946305 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_widgets(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_widgets_m2677538637 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_drawCalls(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_drawCalls_m2290101195 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_worldToLocal(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_worldToLocal_m1534065586 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_drawCallClipRange(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_drawCallClipRange_m2469952977 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIPanel/OnClippingMoved UIPanelGenerated::UIPanel_onClipMove_GetDelegate_member15_arg0(CSRepresentedObject)
extern "C"  OnClippingMoved_t1586118419 * UIPanelGenerated_UIPanel_onClipMove_GetDelegate_member15_arg0_m142915885 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_onClipMove(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_onClipMove_m174427120 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_nextUnusedDepth(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_nextUnusedDepth_m3811520194 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_canBeAnchored(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_canBeAnchored_m1463871765 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_alpha(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_alpha_m3213016830 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_depth(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_depth_m2285987641 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_sortingOrder(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_sortingOrder_m2164345542 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_width(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_width_m2842670998 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_height(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_height_m2878898665 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_halfPixelOffset(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_halfPixelOffset_m3099629910 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_usedForUI(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_usedForUI_m741186620 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_drawCallOffset(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_drawCallOffset_m1915510747 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_clipping(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_clipping_m2777952814 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_parentPanel(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_parentPanel_m3525409762 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_clipCount(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_clipCount_m3858636509 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_hasClipping(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_hasClipping_m2299351072 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_hasCumulativeClipping(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_hasCumulativeClipping_m2307799149 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_clipOffset(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_clipOffset_m1082191245 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_clipTexture(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_clipTexture_m1553145553 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_baseClipRegion(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_baseClipRegion_m1934878907 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_finalClipRegion(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_finalClipRegion_m626495458 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_clipSoftness(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_clipSoftness_m1474543071 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_localCorners(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_localCorners_m356145725 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::UIPanel_worldCorners(JSVCall)
extern "C"  void UIPanelGenerated_UIPanel_worldCorners_m2579392612 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_AddWidget__UIWidget(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_AddWidget__UIWidget_m751034628 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_Affects__UIWidget(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_Affects__UIWidget_m1703199805 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_CalculateConstrainOffset__Vector2__Vector2(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_CalculateConstrainOffset__Vector2__Vector2_m911341567 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_CalculateFinalAlpha__Int32(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_CalculateFinalAlpha__Int32_m254869435 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_ConstrainTargetToBounds__Transform__Bounds__Boolean(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_ConstrainTargetToBounds__Transform__Bounds__Boolean_m2235880422 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_ConstrainTargetToBounds__Transform__Boolean(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_ConstrainTargetToBounds__Transform__Boolean_m3956151099 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_FillDrawCall__UIDrawCall(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_FillDrawCall__UIDrawCall_m3995749366 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_FindDrawCall__UIWidget(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_FindDrawCall__UIWidget_m1803484654 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_GetSides__Transform(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_GetSides__Transform_m3903673805 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_GetViewSize(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_GetViewSize_m589126051 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_Invalidate__Boolean(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_Invalidate__Boolean_m2597362804 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_IsVisible__Vector3__Vector3__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_IsVisible__Vector3__Vector3__Vector3__Vector3_m1012212047 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_IsVisible__Vector3(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_IsVisible__Vector3_m1833098979 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_IsVisible__UIWidget(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_IsVisible__UIWidget_m3658533543 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_ParentHasChanged(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_ParentHasChanged_m2802413311 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_RebuildAllDrawCalls(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_RebuildAllDrawCalls_m11889330 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_Refresh(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_Refresh_m3555271810 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_RemoveWidget__UIWidget(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_RemoveWidget__UIWidget_m2475289403 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_SetDirty(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_SetDirty_m3184176523 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_SetRect__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_SetRect__Single__Single__Single__Single_m3958761965 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_SortWidgets(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_SortWidgets_m453989720 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_CompareFunc__UIPanel__UIPanel(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_CompareFunc__UIPanel__UIPanel_m1727372720 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_Find__Transform__Boolean__Int32(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_Find__Transform__Boolean__Int32_m1406319776 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_Find__Transform__Boolean(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_Find__Transform__Boolean_m4271522992 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::UIPanel_Find__Transform(JSVCall,System.Int32)
extern "C"  bool UIPanelGenerated_UIPanel_Find__Transform_m726034874 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::__Register()
extern "C"  void UIPanelGenerated___Register_m2982835307 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIPanel/OnGeometryUpdated UIPanelGenerated::<UIPanel_onGeometryUpdated>m__156()
extern "C"  OnGeometryUpdated_t4285773675 * UIPanelGenerated_U3CUIPanel_onGeometryUpdatedU3Em__156_m827293686 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIPanel/OnClippingMoved UIPanelGenerated::<UIPanel_onClipMove>m__158()
extern "C"  OnClippingMoved_t1586118419 * UIPanelGenerated_U3CUIPanel_onClipMoveU3Em__158_m2118055650 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIPanelGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIPanelGenerated_ilo_setObject1_m3545600558 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::ilo_addJSFunCSDelegateRel2(System.Int32,System.Delegate)
extern "C"  void UIPanelGenerated_ilo_addJSFunCSDelegateRel2_m2723005623 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void UIPanelGenerated_ilo_setBooleanS3_m4178855563 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::ilo_getBooleanS4(System.Int32)
extern "C"  bool UIPanelGenerated_ilo_getBooleanS4_m3426471475 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIPanelGenerated::ilo_getObject5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIPanelGenerated_ilo_getObject5_m1362961559 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::ilo_setInt326(System.Int32,System.Int32)
extern "C"  void UIPanelGenerated_ilo_setInt326_m2090046037 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::ilo_set_alpha7(UIPanel,System.Single)
extern "C"  void UIPanelGenerated_ilo_set_alpha7_m1999353430 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIPanelGenerated::ilo_get_sortingOrder8(UIPanel)
extern "C"  int32_t UIPanelGenerated_ilo_get_sortingOrder8_m337229892 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::ilo_get_usedForUI9(UIPanel)
extern "C"  bool UIPanelGenerated_ilo_get_usedForUI9_m126329137 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIPanelGenerated::ilo_get_drawCallOffset10(UIPanel)
extern "C"  Vector3_t4282066566  UIPanelGenerated_ilo_get_drawCallOffset10_m3037182800 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::ilo_setVector3S11(System.Int32,UnityEngine.Vector3)
extern "C"  void UIPanelGenerated_ilo_setVector3S11_m2777932944 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::ilo_set_clipping12(UIPanel,UIDrawCall/Clipping)
extern "C"  void UIPanelGenerated_ilo_set_clipping12_m656096344 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIPanelGenerated::ilo_get_clipCount13(UIPanel)
extern "C"  int32_t UIPanelGenerated_ilo_get_clipCount13_m416282929 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::ilo_get_hasClipping14(UIPanel)
extern "C"  bool UIPanelGenerated_ilo_get_hasClipping14_m1710173369 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::ilo_get_hasCumulativeClipping15(UIPanel)
extern "C"  bool UIPanelGenerated_ilo_get_hasCumulativeClipping15_m677233061 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UIPanelGenerated::ilo_get_clipOffset16(UIPanel)
extern "C"  Vector2_t4282066565  UIPanelGenerated_ilo_get_clipOffset16_m388699579 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::ilo_setVector2S17(System.Int32,UnityEngine.Vector2)
extern "C"  void UIPanelGenerated_ilo_setVector2S17_m2908079704 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::ilo_set_clipOffset18(UIPanel,UnityEngine.Vector2)
extern "C"  void UIPanelGenerated_ilo_set_clipOffset18_m100607028 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, Vector2_t4282066565  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::ilo_set_clipTexture19(UIPanel,UnityEngine.Texture2D)
extern "C"  void UIPanelGenerated_ilo_set_clipTexture19_m2791466779 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, Texture2D_t3884108195 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UIPanelGenerated::ilo_get_finalClipRegion20(UIPanel)
extern "C"  Vector4_t4282066567  UIPanelGenerated_ilo_get_finalClipRegion20_m685762597 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UIPanelGenerated::ilo_getVector2S21(System.Int32)
extern "C"  Vector2_t4282066565  UIPanelGenerated_ilo_getVector2S21_m1362364982 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UIPanelGenerated::ilo_get_localCorners22(UIPanel)
extern "C"  Vector3U5BU5D_t215400611* UIPanelGenerated_ilo_get_localCorners22_m8539091 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::ilo_moveSaveID2Arr23(System.Int32)
extern "C"  void UIPanelGenerated_ilo_moveSaveID2Arr23_m4035488101 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIPanelGenerated::ilo_CalculateConstrainOffset24(UIPanel,UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector3_t4282066566  UIPanelGenerated_ilo_CalculateConstrainOffset24_m1651944431 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, Vector2_t4282066565  ___min1, Vector2_t4282066565  ___max2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIPanelGenerated::ilo_CalculateFinalAlpha25(UIPanel,System.Int32)
extern "C"  float UIPanelGenerated_ilo_CalculateFinalAlpha25_m2670922529 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, int32_t ___frameID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::ilo_ConstrainTargetToBounds26(UIPanel,UnityEngine.Transform,System.Boolean)
extern "C"  bool UIPanelGenerated_ilo_ConstrainTargetToBounds26_m2258805331 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, Transform_t1659122786 * ___target1, bool ___immediate2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIDrawCall UIPanelGenerated::ilo_FindDrawCall27(UIPanel,UIWidget)
extern "C"  UIDrawCall_t913273974 * UIPanelGenerated_ilo_FindDrawCall27_m3517949614 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, UIWidget_t769069560 * ___w1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::ilo_Invalidate28(UIPanel,System.Boolean)
extern "C"  void UIPanelGenerated_ilo_Invalidate28_m1264205431 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, bool ___includeChildren1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIPanelGenerated::ilo_getVector3S29(System.Int32)
extern "C"  Vector3_t4282066566  UIPanelGenerated_ilo_getVector3S29_m673995806 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::ilo_IsVisible30(UIPanel,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool UIPanelGenerated_ilo_IsVisible30_m813885622 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, Vector3_t4282066566  ___a1, Vector3_t4282066566  ___b2, Vector3_t4282066566  ___c3, Vector3_t4282066566  ___d4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIPanelGenerated::ilo_Refresh31(UIPanel)
extern "C"  void UIPanelGenerated_ilo_Refresh31_m3649770626 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject UIPanelGenerated::ilo_getFunctionS32(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * UIPanelGenerated_ilo_getFunctionS32_m1276931117 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIPanel/OnGeometryUpdated UIPanelGenerated::ilo_UIPanel_onGeometryUpdated_GetDelegate_member1_arg033(CSRepresentedObject)
extern "C"  OnGeometryUpdated_t4285773675 * UIPanelGenerated_ilo_UIPanel_onGeometryUpdated_GetDelegate_member1_arg033_m942093071 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIPanelGenerated::ilo_isFunctionS34(System.Int32)
extern "C"  bool UIPanelGenerated_ilo_isFunctionS34_m1724410294 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
