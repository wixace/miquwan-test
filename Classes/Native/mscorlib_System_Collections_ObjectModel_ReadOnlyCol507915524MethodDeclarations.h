﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCo1432926611MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m54929634(__this, ___list0, method) ((  void (*) (ReadOnlyCollection_1_t507915524 *, Il2CppObject*, const MethodInfo*))ReadOnlyCollection_1__ctor_m1366664402_gshared)(__this, ___list0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m642193868(__this, ___item0, method) ((  void (*) (ReadOnlyCollection_1_t507915524 *, OutRec_t3245805284 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2541166012_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m439864446(__this, method) ((  void (*) (ReadOnlyCollection_1_t507915524 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m3473426062_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3602304435(__this, ___index0, ___item1, method) ((  void (*) (ReadOnlyCollection_1_t507915524 *, int32_t, OutRec_t3245805284 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m3496388003_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m345372839(__this, ___item0, method) ((  bool (*) (ReadOnlyCollection_1_t507915524 *, OutRec_t3245805284 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m348744375_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1476157305(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t507915524 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m1370240873_gshared)(__this, ___index0, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m4168558013(__this, ___index0, method) ((  OutRec_t3245805284 * (*) (ReadOnlyCollection_1_t507915524 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m3534609325_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m1880518090(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t507915524 *, int32_t, OutRec_t3245805284 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m3174042042_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3726576392(__this, method) ((  bool (*) (ReadOnlyCollection_1_t507915524 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2459576056_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m3320624785(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t507915524 *, Il2CppArray *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m1945557633_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m1387763788(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t507915524 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m3330065468_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m4203316645(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t507915524 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m1628967861_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m579881375(__this, method) ((  void (*) (ReadOnlyCollection_1_t507915524 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m514207119_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m3680561095(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t507915524 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m736178103_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m2559861117(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t507915524 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m3658311565_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m1812248360(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t507915524 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m2823806264_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m346845440(__this, ___value0, method) ((  void (*) (ReadOnlyCollection_1_t507915524 *, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m2498539760_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m3747629752(__this, ___index0, method) ((  void (*) (ReadOnlyCollection_1_t507915524 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m1730676936_gshared)(__this, ___index0, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1370457653(__this, method) ((  bool (*) (ReadOnlyCollection_1_t507915524 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m1373829189_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m3244244129(__this, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t507915524 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m918746289_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m2399647862(__this, method) ((  bool (*) (ReadOnlyCollection_1_t507915524 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m932754534_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2886721475(__this, method) ((  bool (*) (ReadOnlyCollection_1_t507915524 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m2423760339_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m637962728(__this, ___index0, method) ((  Il2CppObject * (*) (ReadOnlyCollection_1_t507915524 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m3512499704_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m2722871551(__this, ___index0, ___value1, method) ((  void (*) (ReadOnlyCollection_1_t507915524 *, int32_t, Il2CppObject *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m4167408399_gshared)(__this, ___index0, ___value1, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::Contains(T)
#define ReadOnlyCollection_1_Contains_m828405996(__this, ___value0, method) ((  bool (*) (ReadOnlyCollection_1_t507915524 *, OutRec_t3245805284 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m687553276_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m401084668(__this, ___array0, ___index1, method) ((  void (*) (ReadOnlyCollection_1_t507915524 *, OutRecU5BU5D_t1391330573*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m475587820_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m4289640399(__this, method) ((  Il2CppObject* (*) (ReadOnlyCollection_1_t507915524 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m809369055_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m1427980416(__this, ___value0, method) ((  int32_t (*) (ReadOnlyCollection_1_t507915524 *, OutRec_t3245805284 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m817393776_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::get_Count()
#define ReadOnlyCollection_1_get_Count_m1135027451(__this, method) ((  int32_t (*) (ReadOnlyCollection_1_t507915524 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m3681678091_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<Pathfinding.ClipperLib.OutRec>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m3779355133(__this, ___index0, method) ((  OutRec_t3245805284 * (*) (ReadOnlyCollection_1_t507915524 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m2421641197_gshared)(__this, ___index0, method)
