﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_Joint2DGenerated
struct UnityEngine_Joint2DGenerated_t1582777851;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine_Joint2DGenerated::.ctor()
extern "C"  void UnityEngine_Joint2DGenerated__ctor_m917047040 (UnityEngine_Joint2DGenerated_t1582777851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Joint2DGenerated::Joint2D_Joint2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Joint2DGenerated_Joint2D_Joint2D1_m1366702336 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Joint2DGenerated::Joint2D_connectedBody(JSVCall)
extern "C"  void UnityEngine_Joint2DGenerated_Joint2D_connectedBody_m2312612129 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Joint2DGenerated::Joint2D_enableCollision(JSVCall)
extern "C"  void UnityEngine_Joint2DGenerated_Joint2D_enableCollision_m1122076541 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Joint2DGenerated::Joint2D_breakForce(JSVCall)
extern "C"  void UnityEngine_Joint2DGenerated_Joint2D_breakForce_m3822848020 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Joint2DGenerated::Joint2D_breakTorque(JSVCall)
extern "C"  void UnityEngine_Joint2DGenerated_Joint2D_breakTorque_m2570276035 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Joint2DGenerated::Joint2D_reactionForce(JSVCall)
extern "C"  void UnityEngine_Joint2DGenerated_Joint2D_reactionForce_m1594667274 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Joint2DGenerated::Joint2D_reactionTorque(JSVCall)
extern "C"  void UnityEngine_Joint2DGenerated_Joint2D_reactionTorque_m2216149645 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Joint2DGenerated::Joint2D_GetReactionForce__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Joint2DGenerated_Joint2D_GetReactionForce__Single_m164727615 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_Joint2DGenerated::Joint2D_GetReactionTorque__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_Joint2DGenerated_Joint2D_GetReactionTorque__Single_m4281654536 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Joint2DGenerated::__Register()
extern "C"  void UnityEngine_Joint2DGenerated___Register_m3800356295 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_Joint2DGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_Joint2DGenerated_ilo_getObject1_m694457106 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_Joint2DGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_Joint2DGenerated_ilo_getObject2_m3904017264 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Joint2DGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_Joint2DGenerated_ilo_setSingle3_m2391308918 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_Joint2DGenerated::ilo_setVector2S4(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_Joint2DGenerated_ilo_setVector2S4_m3682532074 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_Joint2DGenerated::ilo_getSingle5(System.Int32)
extern "C"  float UnityEngine_Joint2DGenerated_ilo_getSingle5_m1758624043 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
