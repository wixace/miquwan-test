﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Collider
struct Collider_t2939674232;
// UnityEngine.WheelHit
struct WheelHit_t1311777980;
struct WheelHit_t1311777980_marshaled_pinvoke;
struct WheelHit_t1311777980_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_WheelHit1311777980.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// UnityEngine.Collider UnityEngine.WheelHit::get_collider()
extern "C"  Collider_t2939674232 * WheelHit_get_collider_m761289136 (WheelHit_t1311777980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelHit::set_collider(UnityEngine.Collider)
extern "C"  void WheelHit_set_collider_m2933791477 (WheelHit_t1311777980 * __this, Collider_t2939674232 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.WheelHit::get_point()
extern "C"  Vector3_t4282066566  WheelHit_get_point_m1962698976 (WheelHit_t1311777980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelHit::set_point(UnityEngine.Vector3)
extern "C"  void WheelHit_set_point_m928645363 (WheelHit_t1311777980 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.WheelHit::get_normal()
extern "C"  Vector3_t4282066566  WheelHit_get_normal_m1779710905 (WheelHit_t1311777980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelHit::set_normal(UnityEngine.Vector3)
extern "C"  void WheelHit_set_normal_m2833717446 (WheelHit_t1311777980 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.WheelHit::get_forwardDir()
extern "C"  Vector3_t4282066566  WheelHit_get_forwardDir_m3158503834 (WheelHit_t1311777980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelHit::set_forwardDir(UnityEngine.Vector3)
extern "C"  void WheelHit_set_forwardDir_m3723336261 (WheelHit_t1311777980 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.WheelHit::get_sidewaysDir()
extern "C"  Vector3_t4282066566  WheelHit_get_sidewaysDir_m3856975074 (WheelHit_t1311777980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelHit::set_sidewaysDir(UnityEngine.Vector3)
extern "C"  void WheelHit_set_sidewaysDir_m163966513 (WheelHit_t1311777980 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelHit::get_force()
extern "C"  float WheelHit_get_force_m1730015529 (WheelHit_t1311777980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelHit::set_force(System.Single)
extern "C"  void WheelHit_set_force_m2175612810 (WheelHit_t1311777980 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelHit::get_forwardSlip()
extern "C"  float WheelHit_get_forwardSlip_m2761683875 (WheelHit_t1311777980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelHit::set_forwardSlip(System.Single)
extern "C"  void WheelHit_set_forwardSlip_m828349776 (WheelHit_t1311777980 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelHit::get_sidewaysSlip()
extern "C"  float WheelHit_get_sidewaysSlip_m164325567 (WheelHit_t1311777980 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelHit::set_sidewaysSlip(System.Single)
extern "C"  void WheelHit_set_sidewaysSlip_m3045426660 (WheelHit_t1311777980 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct WheelHit_t1311777980;
struct WheelHit_t1311777980_marshaled_pinvoke;

extern "C" void WheelHit_t1311777980_marshal_pinvoke(const WheelHit_t1311777980& unmarshaled, WheelHit_t1311777980_marshaled_pinvoke& marshaled);
extern "C" void WheelHit_t1311777980_marshal_pinvoke_back(const WheelHit_t1311777980_marshaled_pinvoke& marshaled, WheelHit_t1311777980& unmarshaled);
extern "C" void WheelHit_t1311777980_marshal_pinvoke_cleanup(WheelHit_t1311777980_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct WheelHit_t1311777980;
struct WheelHit_t1311777980_marshaled_com;

extern "C" void WheelHit_t1311777980_marshal_com(const WheelHit_t1311777980& unmarshaled, WheelHit_t1311777980_marshaled_com& marshaled);
extern "C" void WheelHit_t1311777980_marshal_com_back(const WheelHit_t1311777980_marshaled_com& marshaled, WheelHit_t1311777980& unmarshaled);
extern "C" void WheelHit_t1311777980_marshal_com_cleanup(WheelHit_t1311777980_marshaled_com& marshaled);
