﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_RandomGenerated
struct UnityEngine_RandomGenerated_t2061072804;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_RandomGenerated::.ctor()
extern "C"  void UnityEngine_RandomGenerated__ctor_m1606666823 (UnityEngine_RandomGenerated_t2061072804 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RandomGenerated::Random_Random1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RandomGenerated_Random_Random1_m2304349099 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RandomGenerated::Random_seed(JSVCall)
extern "C"  void UnityEngine_RandomGenerated_Random_seed_m2676788181 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RandomGenerated::Random_value(JSVCall)
extern "C"  void UnityEngine_RandomGenerated_Random_value_m3022643605 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RandomGenerated::Random_insideUnitSphere(JSVCall)
extern "C"  void UnityEngine_RandomGenerated_Random_insideUnitSphere_m3804892729 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RandomGenerated::Random_insideUnitCircle(JSVCall)
extern "C"  void UnityEngine_RandomGenerated_Random_insideUnitCircle_m3697388694 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RandomGenerated::Random_onUnitSphere(JSVCall)
extern "C"  void UnityEngine_RandomGenerated_Random_onUnitSphere_m2732103542 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RandomGenerated::Random_rotation(JSVCall)
extern "C"  void UnityEngine_RandomGenerated_Random_rotation_m1875204456 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RandomGenerated::Random_rotationUniform(JSVCall)
extern "C"  void UnityEngine_RandomGenerated_Random_rotationUniform_m948720720 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RandomGenerated::Random_ColorHSV__Single__Single__Single__Single__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RandomGenerated_Random_ColorHSV__Single__Single__Single__Single__Single__Single__Single__Single_m2361854957 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RandomGenerated::Random_ColorHSV__Single__Single__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RandomGenerated_Random_ColorHSV__Single__Single__Single__Single__Single__Single_m591499101 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RandomGenerated::Random_ColorHSV__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RandomGenerated_Random_ColorHSV__Single__Single__Single__Single_m1016132301 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RandomGenerated::Random_ColorHSV__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RandomGenerated_Random_ColorHSV__Single__Single_m3732223549 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RandomGenerated::Random_ColorHSV(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RandomGenerated_Random_ColorHSV_m2393790893 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RandomGenerated::Random_Range__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RandomGenerated_Random_Range__Single__Single_m3676410218 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RandomGenerated::Random_Range__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RandomGenerated_Random_Range__Int32__Int32_m1513934682 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RandomGenerated::__Register()
extern "C"  void UnityEngine_RandomGenerated___Register_m2480146144 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RandomGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_RandomGenerated_ilo_getObject1_m46726991 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RandomGenerated::ilo_getInt322(System.Int32)
extern "C"  int32_t UnityEngine_RandomGenerated_ilo_getInt322_m2187317931 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RandomGenerated::ilo_setVector3S3(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_RandomGenerated_ilo_setVector3S3_m2529731874 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RandomGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_RandomGenerated_ilo_setObject4_m672928138 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_RandomGenerated::ilo_getSingle5(System.Int32)
extern "C"  float UnityEngine_RandomGenerated_ilo_getSingle5_m2170151724 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RandomGenerated::ilo_setSingle6(System.Int32,System.Single)
extern "C"  void UnityEngine_RandomGenerated_ilo_setSingle6_m1620518738 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
