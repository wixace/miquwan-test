﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWW
struct WWW_t3134621005;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mihua.Net.UrlLoader
struct  UrlLoader_t2490729496  : public Il2CppObject
{
public:
	// System.Single Mihua.Net.UrlLoader::lastTime
	float ___lastTime_0;
	// System.Single Mihua.Net.UrlLoader::_progress
	float ____progress_1;
	// UnityEngine.WWW Mihua.Net.UrlLoader::<www>k__BackingField
	WWW_t3134621005 * ___U3CwwwU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_lastTime_0() { return static_cast<int32_t>(offsetof(UrlLoader_t2490729496, ___lastTime_0)); }
	inline float get_lastTime_0() const { return ___lastTime_0; }
	inline float* get_address_of_lastTime_0() { return &___lastTime_0; }
	inline void set_lastTime_0(float value)
	{
		___lastTime_0 = value;
	}

	inline static int32_t get_offset_of__progress_1() { return static_cast<int32_t>(offsetof(UrlLoader_t2490729496, ____progress_1)); }
	inline float get__progress_1() const { return ____progress_1; }
	inline float* get_address_of__progress_1() { return &____progress_1; }
	inline void set__progress_1(float value)
	{
		____progress_1 = value;
	}

	inline static int32_t get_offset_of_U3CwwwU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(UrlLoader_t2490729496, ___U3CwwwU3Ek__BackingField_2)); }
	inline WWW_t3134621005 * get_U3CwwwU3Ek__BackingField_2() const { return ___U3CwwwU3Ek__BackingField_2; }
	inline WWW_t3134621005 ** get_address_of_U3CwwwU3Ek__BackingField_2() { return &___U3CwwwU3Ek__BackingField_2; }
	inline void set_U3CwwwU3Ek__BackingField_2(WWW_t3134621005 * value)
	{
		___U3CwwwU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CwwwU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
