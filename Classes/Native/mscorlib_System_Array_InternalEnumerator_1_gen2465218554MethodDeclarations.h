﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2465218554.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_TypeFlag3682875878.h"

// System.Void System.Array/InternalEnumerator`1<TypeFlag>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3545948279_gshared (InternalEnumerator_1_t2465218554 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3545948279(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2465218554 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3545948279_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<TypeFlag>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m511835081_gshared (InternalEnumerator_1_t2465218554 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m511835081(__this, method) ((  void (*) (InternalEnumerator_1_t2465218554 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m511835081_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<TypeFlag>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3101505589_gshared (InternalEnumerator_1_t2465218554 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3101505589(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2465218554 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3101505589_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<TypeFlag>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3030991630_gshared (InternalEnumerator_1_t2465218554 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3030991630(__this, method) ((  void (*) (InternalEnumerator_1_t2465218554 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3030991630_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<TypeFlag>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3253444405_gshared (InternalEnumerator_1_t2465218554 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3253444405(__this, method) ((  bool (*) (InternalEnumerator_1_t2465218554 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3253444405_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<TypeFlag>::get_Current()
extern "C"  int32_t InternalEnumerator_1_get_Current_m2455847230_gshared (InternalEnumerator_1_t2465218554 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m2455847230(__this, method) ((  int32_t (*) (InternalEnumerator_1_t2465218554 *, const MethodInfo*))InternalEnumerator_1_get_Current_m2455847230_gshared)(__this, method)
