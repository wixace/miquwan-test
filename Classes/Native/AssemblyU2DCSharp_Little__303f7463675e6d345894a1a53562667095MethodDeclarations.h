﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._303f7463675e6d345894a1a59eea2628
struct _303f7463675e6d345894a1a59eea2628_t3562667095;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._303f7463675e6d345894a1a59eea2628::.ctor()
extern "C"  void _303f7463675e6d345894a1a59eea2628__ctor_m198843318 (_303f7463675e6d345894a1a59eea2628_t3562667095 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._303f7463675e6d345894a1a59eea2628::_303f7463675e6d345894a1a59eea2628m2(System.Int32)
extern "C"  int32_t _303f7463675e6d345894a1a59eea2628__303f7463675e6d345894a1a59eea2628m2_m1857321529 (_303f7463675e6d345894a1a59eea2628_t3562667095 * __this, int32_t ____303f7463675e6d345894a1a59eea2628a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._303f7463675e6d345894a1a59eea2628::_303f7463675e6d345894a1a59eea2628m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _303f7463675e6d345894a1a59eea2628__303f7463675e6d345894a1a59eea2628m_m46735517 (_303f7463675e6d345894a1a59eea2628_t3562667095 * __this, int32_t ____303f7463675e6d345894a1a59eea2628a0, int32_t ____303f7463675e6d345894a1a59eea2628431, int32_t ____303f7463675e6d345894a1a59eea2628c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
