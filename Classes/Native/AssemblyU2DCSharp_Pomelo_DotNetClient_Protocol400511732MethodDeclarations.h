﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pomelo.DotNetClient.Protocol
struct Protocol_t400511732;
// Pomelo.DotNetClient.PomeloClient
struct PomeloClient_t4215271137;
// System.Net.Sockets.Socket
struct Socket_t2157335841;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.Action`1<Newtonsoft.Json.Linq.JObject>
struct Action_1_t2194360335;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// Pomelo.DotNetClient.HandShakeService
struct HandShakeService_t4068105274;
// Pomelo.DotNetClient.MessageProtocol
struct MessageProtocol_t562499845;
// Pomelo.DotNetClient.Transporter
struct Transporter_t1342168412;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_PomeloClient4215271137.h"
#include "System_System_Net_Sockets_Socket2157335841.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_PackageType3964380262.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_HandShakeSer4068105274.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_Protocol400511732.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_MessageProtoc562499845.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_Transporter1342168412.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Pomelo.DotNetClient.Protocol::.ctor(Pomelo.DotNetClient.PomeloClient,System.Net.Sockets.Socket,System.UInt32)
extern "C"  void Protocol__ctor_m2288096023 (Protocol_t400511732 * __this, PomeloClient_t4215271137 * ___pc0, Socket_t2157335841 * ___socket1, uint32_t ___socketID2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pomelo.DotNetClient.PomeloClient Pomelo.DotNetClient.Protocol::getPomeloClient()
extern "C"  PomeloClient_t4215271137 * Protocol_getPomeloClient_m233619268 (Protocol_t400511732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Protocol::start(Newtonsoft.Json.Linq.JObject,System.Action`1<Newtonsoft.Json.Linq.JObject>)
extern "C"  void Protocol_start_m1737208876 (Protocol_t400511732 * __this, JObject_t1798544199 * ___user0, Action_1_t2194360335 * ___callback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Protocol::send(System.String,Newtonsoft.Json.Linq.JObject)
extern "C"  void Protocol_send_m602326936 (Protocol_t400511732 * __this, String_t* ___route0, JObject_t1798544199 * ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Protocol::send(System.String,System.UInt32,Newtonsoft.Json.Linq.JObject)
extern "C"  void Protocol_send_m175093348 (Protocol_t400511732 * __this, String_t* ___route0, uint32_t ___id1, JObject_t1798544199 * ___msg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Protocol::send(Pomelo.DotNetClient.PackageType)
extern "C"  void Protocol_send_m2048107145 (Protocol_t400511732 * __this, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Protocol::send(Pomelo.DotNetClient.PackageType,Newtonsoft.Json.Linq.JObject)
extern "C"  void Protocol_send_m3935271117 (Protocol_t400511732 * __this, int32_t ___type0, JObject_t1798544199 * ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Protocol::send(Pomelo.DotNetClient.PackageType,System.Byte[])
extern "C"  void Protocol_send_m1649248204 (Protocol_t400511732 * __this, int32_t ___type0, ByteU5BU5D_t4260760469* ___body1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Protocol::processMessage(System.Byte[])
extern "C"  void Protocol_processMessage_m3150576981 (Protocol_t400511732 * __this, ByteU5BU5D_t4260760469* ___bytes0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Protocol::processHandshakeData(Newtonsoft.Json.Linq.JObject)
extern "C"  void Protocol_processHandshakeData_m20290014 (Protocol_t400511732 * __this, JObject_t1798544199 * ___msg0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Protocol::onDisconnect()
extern "C"  void Protocol_onDisconnect_m1512033847 (Protocol_t400511732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Protocol::close()
extern "C"  void Protocol_close_m674251070 (Protocol_t400511732 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Protocol::ilo_request1(Pomelo.DotNetClient.HandShakeService,Newtonsoft.Json.Linq.JObject,System.Action`1<Newtonsoft.Json.Linq.JObject>)
extern "C"  void Protocol_ilo_request1_m3980090002 (Il2CppObject * __this /* static, unused */, HandShakeService_t4068105274 * ____this0, JObject_t1798544199 * ___user1, Action_1_t2194360335 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Protocol::ilo_send2(Pomelo.DotNetClient.Protocol,System.String,System.UInt32,Newtonsoft.Json.Linq.JObject)
extern "C"  void Protocol_ilo_send2_m2139344316 (Il2CppObject * __this /* static, unused */, Protocol_t400511732 * ____this0, String_t* ___route1, uint32_t ___id2, JObject_t1798544199 * ___msg3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.DotNetClient.Protocol::ilo_encode3(Pomelo.DotNetClient.MessageProtocol,System.String,System.UInt32,Newtonsoft.Json.Linq.JObject)
extern "C"  ByteU5BU5D_t4260760469* Protocol_ilo_encode3_m4172065726 (Il2CppObject * __this /* static, unused */, MessageProtocol_t562499845 * ____this0, String_t* ___route1, uint32_t ___id2, JObject_t1798544199 * ___msg3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Protocol::ilo_send4(Pomelo.DotNetClient.Protocol,Pomelo.DotNetClient.PackageType,System.Byte[])
extern "C"  void Protocol_ilo_send4_m796628086 (Il2CppObject * __this /* static, unused */, Protocol_t400511732 * ____this0, int32_t ___type1, ByteU5BU5D_t4260760469* ___body2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Pomelo.DotNetClient.Protocol::ilo_encode5(Pomelo.DotNetClient.PackageType)
extern "C"  ByteU5BU5D_t4260760469* Protocol_ilo_encode5_m3874199301 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Protocol::ilo_send6(Pomelo.DotNetClient.Transporter,System.Byte[])
extern "C"  void Protocol_ilo_send6_m2571081081 (Il2CppObject * __this /* static, unused */, Transporter_t1342168412 * ____this0, ByteU5BU5D_t4260760469* ___buffer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Protocol::ilo_Log7(System.Object,System.Boolean)
extern "C"  void Protocol_ilo_Log7_m2254922699 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Pomelo.DotNetClient.Protocol::ilo_DeserializeObject8(System.String)
extern "C"  Il2CppObject * Protocol_ilo_DeserializeObject8_m4054293700 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Protocol::ilo_close9(Pomelo.DotNetClient.Protocol)
extern "C"  void Protocol_ilo_close9_m2435045885 (Il2CppObject * __this /* static, unused */, Protocol_t400511732 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pomelo.DotNetClient.PomeloClient Pomelo.DotNetClient.Protocol::ilo_getPomeloClient10(Pomelo.DotNetClient.Protocol)
extern "C"  PomeloClient_t4215271137 * Protocol_ilo_getPomeloClient10_m517014973 (Il2CppObject * __this /* static, unused */, Protocol_t400511732 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pomelo.DotNetClient.Protocol::ilo_ContainsKey11(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  bool Protocol_ilo_ContainsKey11_m790180203 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Pomelo.DotNetClient.Protocol::ilo_get_Item12(Newtonsoft.Json.Linq.JObject,System.String)
extern "C"  JToken_t3412245951 * Protocol_ilo_get_Item12_m332533667 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Protocol::ilo_ack13(Pomelo.DotNetClient.HandShakeService)
extern "C"  void Protocol_ilo_ack13_m2889290309 (Il2CppObject * __this /* static, unused */, HandShakeService_t4068105274 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Protocol::ilo_invokeCallback14(Pomelo.DotNetClient.HandShakeService,Newtonsoft.Json.Linq.JObject)
extern "C"  void Protocol_ilo_invokeCallback14_m2317201984 (Il2CppObject * __this /* static, unused */, HandShakeService_t4068105274 * ____this0, JObject_t1798544199 * ___data1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pomelo.DotNetClient.Protocol::ilo_close15(Pomelo.DotNetClient.Transporter)
extern "C"  void Protocol_ilo_close15_m2499733704 (Il2CppObject * __this /* static, unused */, Transporter_t1342168412 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
