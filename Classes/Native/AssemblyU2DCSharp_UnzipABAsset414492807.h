﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnzipABAsset
struct UnzipABAsset_t414492807;
// System.Collections.Generic.List`1<FileInfoRes>
struct List_1_t2585245350;
// Mihua.Net.UrlLoader
struct UrlLoader_t2490729496;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnzipABAsset
struct  UnzipABAsset_t414492807  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<FileInfoRes> UnzipABAsset::updateList
	List_1_t2585245350 * ___updateList_1;
	// System.Int32 UnzipABAsset::total
	int32_t ___total_2;
	// System.Int32 UnzipABAsset::index
	int32_t ___index_3;
	// System.Boolean UnzipABAsset::CanUnzip
	bool ___CanUnzip_4;
	// Mihua.Net.UrlLoader UnzipABAsset::loader
	UrlLoader_t2490729496 * ___loader_5;
	// System.Boolean UnzipABAsset::isPause
	bool ___isPause_6;
	// System.Boolean UnzipABAsset::isChecked
	bool ___isChecked_7;
	// System.Boolean UnzipABAsset::IsUnZipFinish
	bool ___IsUnZipFinish_8;

public:
	inline static int32_t get_offset_of_updateList_1() { return static_cast<int32_t>(offsetof(UnzipABAsset_t414492807, ___updateList_1)); }
	inline List_1_t2585245350 * get_updateList_1() const { return ___updateList_1; }
	inline List_1_t2585245350 ** get_address_of_updateList_1() { return &___updateList_1; }
	inline void set_updateList_1(List_1_t2585245350 * value)
	{
		___updateList_1 = value;
		Il2CppCodeGenWriteBarrier(&___updateList_1, value);
	}

	inline static int32_t get_offset_of_total_2() { return static_cast<int32_t>(offsetof(UnzipABAsset_t414492807, ___total_2)); }
	inline int32_t get_total_2() const { return ___total_2; }
	inline int32_t* get_address_of_total_2() { return &___total_2; }
	inline void set_total_2(int32_t value)
	{
		___total_2 = value;
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(UnzipABAsset_t414492807, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_CanUnzip_4() { return static_cast<int32_t>(offsetof(UnzipABAsset_t414492807, ___CanUnzip_4)); }
	inline bool get_CanUnzip_4() const { return ___CanUnzip_4; }
	inline bool* get_address_of_CanUnzip_4() { return &___CanUnzip_4; }
	inline void set_CanUnzip_4(bool value)
	{
		___CanUnzip_4 = value;
	}

	inline static int32_t get_offset_of_loader_5() { return static_cast<int32_t>(offsetof(UnzipABAsset_t414492807, ___loader_5)); }
	inline UrlLoader_t2490729496 * get_loader_5() const { return ___loader_5; }
	inline UrlLoader_t2490729496 ** get_address_of_loader_5() { return &___loader_5; }
	inline void set_loader_5(UrlLoader_t2490729496 * value)
	{
		___loader_5 = value;
		Il2CppCodeGenWriteBarrier(&___loader_5, value);
	}

	inline static int32_t get_offset_of_isPause_6() { return static_cast<int32_t>(offsetof(UnzipABAsset_t414492807, ___isPause_6)); }
	inline bool get_isPause_6() const { return ___isPause_6; }
	inline bool* get_address_of_isPause_6() { return &___isPause_6; }
	inline void set_isPause_6(bool value)
	{
		___isPause_6 = value;
	}

	inline static int32_t get_offset_of_isChecked_7() { return static_cast<int32_t>(offsetof(UnzipABAsset_t414492807, ___isChecked_7)); }
	inline bool get_isChecked_7() const { return ___isChecked_7; }
	inline bool* get_address_of_isChecked_7() { return &___isChecked_7; }
	inline void set_isChecked_7(bool value)
	{
		___isChecked_7 = value;
	}

	inline static int32_t get_offset_of_IsUnZipFinish_8() { return static_cast<int32_t>(offsetof(UnzipABAsset_t414492807, ___IsUnZipFinish_8)); }
	inline bool get_IsUnZipFinish_8() const { return ___IsUnZipFinish_8; }
	inline bool* get_address_of_IsUnZipFinish_8() { return &___IsUnZipFinish_8; }
	inline void set_IsUnZipFinish_8(bool value)
	{
		___IsUnZipFinish_8 = value;
	}
};

struct UnzipABAsset_t414492807_StaticFields
{
public:
	// UnzipABAsset UnzipABAsset::_instance
	UnzipABAsset_t414492807 * ____instance_0;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(UnzipABAsset_t414492807_StaticFields, ____instance_0)); }
	inline UnzipABAsset_t414492807 * get__instance_0() const { return ____instance_0; }
	inline UnzipABAsset_t414492807 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(UnzipABAsset_t414492807 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
