﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;

#include "System_Xml_System_Xml_XmlLinkedNode901819716.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDeclaration
struct  XmlDeclaration_t1240444833  : public XmlLinkedNode_t901819716
{
public:
	// System.String System.Xml.XmlDeclaration::encoding
	String_t* ___encoding_8;
	// System.String System.Xml.XmlDeclaration::standalone
	String_t* ___standalone_9;
	// System.String System.Xml.XmlDeclaration::version
	String_t* ___version_10;

public:
	inline static int32_t get_offset_of_encoding_8() { return static_cast<int32_t>(offsetof(XmlDeclaration_t1240444833, ___encoding_8)); }
	inline String_t* get_encoding_8() const { return ___encoding_8; }
	inline String_t** get_address_of_encoding_8() { return &___encoding_8; }
	inline void set_encoding_8(String_t* value)
	{
		___encoding_8 = value;
		Il2CppCodeGenWriteBarrier(&___encoding_8, value);
	}

	inline static int32_t get_offset_of_standalone_9() { return static_cast<int32_t>(offsetof(XmlDeclaration_t1240444833, ___standalone_9)); }
	inline String_t* get_standalone_9() const { return ___standalone_9; }
	inline String_t** get_address_of_standalone_9() { return &___standalone_9; }
	inline void set_standalone_9(String_t* value)
	{
		___standalone_9 = value;
		Il2CppCodeGenWriteBarrier(&___standalone_9, value);
	}

	inline static int32_t get_offset_of_version_10() { return static_cast<int32_t>(offsetof(XmlDeclaration_t1240444833, ___version_10)); }
	inline String_t* get_version_10() const { return ___version_10; }
	inline String_t** get_address_of_version_10() { return &___version_10; }
	inline void set_version_10(String_t* value)
	{
		___version_10 = value;
		Il2CppCodeGenWriteBarrier(&___version_10, value);
	}
};

struct XmlDeclaration_t1240444833_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlDeclaration::<>f__switch$map4A
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map4A_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map4A_11() { return static_cast<int32_t>(offsetof(XmlDeclaration_t1240444833_StaticFields, ___U3CU3Ef__switchU24map4A_11)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map4A_11() const { return ___U3CU3Ef__switchU24map4A_11; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map4A_11() { return &___U3CU3Ef__switchU24map4A_11; }
	inline void set_U3CU3Ef__switchU24map4A_11(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map4A_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map4A_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
