﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.AdvancedSmooth/TurnConstructor
struct TurnConstructor_t3543216936;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// Pathfinding.AdvancedSmooth/Turn
struct Turn_t465195378;
struct Turn_t465195378_marshaled_pinvoke;
struct Turn_t465195378_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_AdvancedSmooth_Turn465195378.h"
#include "AssemblyU2DCSharp_Pathfinding_AdvancedSmooth_TurnC3543216936.h"

// System.Void Pathfinding.AdvancedSmooth/Turn::.ctor(System.Single,Pathfinding.AdvancedSmooth/TurnConstructor,System.Int32)
extern "C"  void Turn__ctor_m4148310411 (Turn_t465195378 * __this, float ___length0, TurnConstructor_t3543216936 * ___constructor1, int32_t ___id2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.AdvancedSmooth/Turn::get_score()
extern "C"  float Turn_get_score_m3209315692 (Turn_t465195378 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AdvancedSmooth/Turn::GetPath(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern "C"  void Turn_GetPath_m57001711 (Turn_t465195378 * __this, List_1_t1355284822 * ___output0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.AdvancedSmooth/Turn::CompareTo(Pathfinding.AdvancedSmooth/Turn)
extern "C"  int32_t Turn_CompareTo_m3350767099 (Turn_t465195378 * __this, Turn_t465195378  ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.AdvancedSmooth/Turn::op_LessThan(Pathfinding.AdvancedSmooth/Turn,Pathfinding.AdvancedSmooth/Turn)
extern "C"  bool Turn_op_LessThan_m325657239 (Il2CppObject * __this /* static, unused */, Turn_t465195378  ___lhs0, Turn_t465195378  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.AdvancedSmooth/Turn::op_GreaterThan(Pathfinding.AdvancedSmooth/Turn,Pathfinding.AdvancedSmooth/Turn)
extern "C"  bool Turn_op_GreaterThan_m2235757990 (Il2CppObject * __this /* static, unused */, Turn_t465195378  ___lhs0, Turn_t465195378  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Turn_t465195378;
struct Turn_t465195378_marshaled_pinvoke;

extern "C" void Turn_t465195378_marshal_pinvoke(const Turn_t465195378& unmarshaled, Turn_t465195378_marshaled_pinvoke& marshaled);
extern "C" void Turn_t465195378_marshal_pinvoke_back(const Turn_t465195378_marshaled_pinvoke& marshaled, Turn_t465195378& unmarshaled);
extern "C" void Turn_t465195378_marshal_pinvoke_cleanup(Turn_t465195378_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Turn_t465195378;
struct Turn_t465195378_marshaled_com;

extern "C" void Turn_t465195378_marshal_com(const Turn_t465195378& unmarshaled, Turn_t465195378_marshaled_com& marshaled);
extern "C" void Turn_t465195378_marshal_com_back(const Turn_t465195378_marshaled_com& marshaled, Turn_t465195378& unmarshaled);
extern "C" void Turn_t465195378_marshal_com_cleanup(Turn_t465195378_marshaled_com& marshaled);
