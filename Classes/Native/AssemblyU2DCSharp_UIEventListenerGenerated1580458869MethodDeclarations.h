﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEventListenerGenerated
struct UIEventListenerGenerated_t1580458869;
// JSVCall
struct JSVCall_t3708497963;
// UIEventListener/VoidDelegate
struct VoidDelegate_t986119182;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// UIEventListener/BoolDelegate
struct BoolDelegate_t4240584548;
// UIEventListener/FloatDelegate
struct FloatDelegate_t3249382860;
// UIEventListener/VectorDelegate
struct VectorDelegate_t3512109949;
// UIEventListener/ObjectDelegate
struct ObjectDelegate_t172593529;
// UIEventListener/KeyCodeDelegate
struct KeyCodeDelegate_t2040735132;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// System.Delegate
struct Delegate_t3310234105;
// UIEventListener
struct UIEventListener_t1278105402;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UIEventListenerGenerated::.ctor()
extern "C"  void UIEventListenerGenerated__ctor_m827178694 (UIEventListenerGenerated_t1580458869 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIEventListenerGenerated::UIEventListener_UIEventListener1(JSVCall,System.Int32)
extern "C"  bool UIEventListenerGenerated_UIEventListener_UIEventListener1_m2868189182 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated::UIEventListener_parameter(JSVCall)
extern "C"  void UIEventListenerGenerated_UIEventListener_parameter_m3634485311 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/VoidDelegate UIEventListenerGenerated::UIEventListener_onSubmit_GetDelegate_member1_arg0(CSRepresentedObject)
extern "C"  VoidDelegate_t986119182 * UIEventListenerGenerated_UIEventListener_onSubmit_GetDelegate_member1_arg0_m145958238 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated::UIEventListener_onSubmit(JSVCall)
extern "C"  void UIEventListenerGenerated_UIEventListener_onSubmit_m311851021 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/VoidDelegate UIEventListenerGenerated::UIEventListener_onClick_GetDelegate_member2_arg0(CSRepresentedObject)
extern "C"  VoidDelegate_t986119182 * UIEventListenerGenerated_UIEventListener_onClick_GetDelegate_member2_arg0_m280477231 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated::UIEventListener_onClick(JSVCall)
extern "C"  void UIEventListenerGenerated_UIEventListener_onClick_m861946239 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/VoidDelegate UIEventListenerGenerated::UIEventListener_onDoubleClick_GetDelegate_member3_arg0(CSRepresentedObject)
extern "C"  VoidDelegate_t986119182 * UIEventListenerGenerated_UIEventListener_onDoubleClick_GetDelegate_member3_arg0_m972834655 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated::UIEventListener_onDoubleClick(JSVCall)
extern "C"  void UIEventListenerGenerated_UIEventListener_onDoubleClick_m1308156656 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/BoolDelegate UIEventListenerGenerated::UIEventListener_onHover_GetDelegate_member4_arg0(CSRepresentedObject)
extern "C"  BoolDelegate_t4240584548 * UIEventListenerGenerated_UIEventListener_onHover_GetDelegate_member4_arg0_m2463773627 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated::UIEventListener_onHover(JSVCall)
extern "C"  void UIEventListenerGenerated_UIEventListener_onHover_m2752456779 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/BoolDelegate UIEventListenerGenerated::UIEventListener_onPress_GetDelegate_member5_arg0(CSRepresentedObject)
extern "C"  BoolDelegate_t4240584548 * UIEventListenerGenerated_UIEventListener_onPress_GetDelegate_member5_arg0_m1676471107 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated::UIEventListener_onPress(JSVCall)
extern "C"  void UIEventListenerGenerated_UIEventListener_onPress_m344311076 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/BoolDelegate UIEventListenerGenerated::UIEventListener_onSelect_GetDelegate_member6_arg0(CSRepresentedObject)
extern "C"  BoolDelegate_t4240584548 * UIEventListenerGenerated_UIEventListener_onSelect_GetDelegate_member6_arg0_m1604140657 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated::UIEventListener_onSelect(JSVCall)
extern "C"  void UIEventListenerGenerated_UIEventListener_onSelect_m2330903465 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/FloatDelegate UIEventListenerGenerated::UIEventListener_onScroll_GetDelegate_member7_arg0(CSRepresentedObject)
extern "C"  FloatDelegate_t3249382860 * UIEventListenerGenerated_UIEventListener_onScroll_GetDelegate_member7_arg0_m3915689073 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated::UIEventListener_onScroll(JSVCall)
extern "C"  void UIEventListenerGenerated_UIEventListener_onScroll_m1156877560 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/VoidDelegate UIEventListenerGenerated::UIEventListener_onDragStart_GetDelegate_member8_arg0(CSRepresentedObject)
extern "C"  VoidDelegate_t986119182 * UIEventListenerGenerated_UIEventListener_onDragStart_GetDelegate_member8_arg0_m3054363195 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated::UIEventListener_onDragStart(JSVCall)
extern "C"  void UIEventListenerGenerated_UIEventListener_onDragStart_m3594649017 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/VectorDelegate UIEventListenerGenerated::UIEventListener_onDrag_GetDelegate_member9_arg0(CSRepresentedObject)
extern "C"  VectorDelegate_t3512109949 * UIEventListenerGenerated_UIEventListener_onDrag_GetDelegate_member9_arg0_m723120371 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated::UIEventListener_onDrag(JSVCall)
extern "C"  void UIEventListenerGenerated_UIEventListener_onDrag_m2724980273 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/VoidDelegate UIEventListenerGenerated::UIEventListener_onDragOver_GetDelegate_member10_arg0(CSRepresentedObject)
extern "C"  VoidDelegate_t986119182 * UIEventListenerGenerated_UIEventListener_onDragOver_GetDelegate_member10_arg0_m4074764294 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated::UIEventListener_onDragOver(JSVCall)
extern "C"  void UIEventListenerGenerated_UIEventListener_onDragOver_m2994321981 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/VoidDelegate UIEventListenerGenerated::UIEventListener_onDragOut_GetDelegate_member11_arg0(CSRepresentedObject)
extern "C"  VoidDelegate_t986119182 * UIEventListenerGenerated_UIEventListener_onDragOut_GetDelegate_member11_arg0_m2489011861 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated::UIEventListener_onDragOut(JSVCall)
extern "C"  void UIEventListenerGenerated_UIEventListener_onDragOut_m4242419085 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/VoidDelegate UIEventListenerGenerated::UIEventListener_onDragEnd_GetDelegate_member12_arg0(CSRepresentedObject)
extern "C"  VoidDelegate_t986119182 * UIEventListenerGenerated_UIEventListener_onDragEnd_GetDelegate_member12_arg0_m3281948201 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated::UIEventListener_onDragEnd(JSVCall)
extern "C"  void UIEventListenerGenerated_UIEventListener_onDragEnd_m1494598304 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/ObjectDelegate UIEventListenerGenerated::UIEventListener_onDrop_GetDelegate_member13_arg0(CSRepresentedObject)
extern "C"  ObjectDelegate_t172593529 * UIEventListenerGenerated_UIEventListener_onDrop_GetDelegate_member13_arg0_m1156514637 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated::UIEventListener_onDrop(JSVCall)
extern "C"  void UIEventListenerGenerated_UIEventListener_onDrop_m1568843478 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/KeyCodeDelegate UIEventListenerGenerated::UIEventListener_onKey_GetDelegate_member14_arg0(CSRepresentedObject)
extern "C"  KeyCodeDelegate_t2040735132 * UIEventListenerGenerated_UIEventListener_onKey_GetDelegate_member14_arg0_m3048548187 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated::UIEventListener_onKey(JSVCall)
extern "C"  void UIEventListenerGenerated_UIEventListener_onKey_m3110442696 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/BoolDelegate UIEventListenerGenerated::UIEventListener_onTooltip_GetDelegate_member15_arg0(CSRepresentedObject)
extern "C"  BoolDelegate_t4240584548 * UIEventListenerGenerated_UIEventListener_onTooltip_GetDelegate_member15_arg0_m2564766938 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated::UIEventListener_onTooltip(JSVCall)
extern "C"  void UIEventListenerGenerated_UIEventListener_onTooltip_m1541977380 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated::UIEventListener_soundID(JSVCall)
extern "C"  void UIEventListenerGenerated_UIEventListener_soundID_m2432168734 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIEventListenerGenerated::UIEventListener_Get__GameObject__Single(JSVCall,System.Int32)
extern "C"  bool UIEventListenerGenerated_UIEventListener_Get__GameObject__Single_m2934727274 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIEventListenerGenerated::UIEventListener_Get__GameObject__Int32(JSVCall,System.Int32)
extern "C"  bool UIEventListenerGenerated_UIEventListener_Get__GameObject__Int32_m2134714350 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIEventListenerGenerated::UIEventListener_Get__GameObject(JSVCall,System.Int32)
extern "C"  bool UIEventListenerGenerated_UIEventListener_Get__GameObject_m3298022690 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated::__Register()
extern "C"  void UIEventListenerGenerated___Register_m2266630593 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/VoidDelegate UIEventListenerGenerated::<UIEventListener_onSubmit>m__12F()
extern "C"  VoidDelegate_t986119182 * UIEventListenerGenerated_U3CUIEventListener_onSubmitU3Em__12F_m2535698539 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/VoidDelegate UIEventListenerGenerated::<UIEventListener_onClick>m__131()
extern "C"  VoidDelegate_t986119182 * UIEventListenerGenerated_U3CUIEventListener_onClickU3Em__131_m3342144685 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/VoidDelegate UIEventListenerGenerated::<UIEventListener_onDoubleClick>m__133()
extern "C"  VoidDelegate_t986119182 * UIEventListenerGenerated_U3CUIEventListener_onDoubleClickU3Em__133_m2232111968 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/BoolDelegate UIEventListenerGenerated::<UIEventListener_onHover>m__135()
extern "C"  BoolDelegate_t4240584548 * UIEventListenerGenerated_U3CUIEventListener_onHoverU3Em__135_m3923552467 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/BoolDelegate UIEventListenerGenerated::<UIEventListener_onPress>m__137()
extern "C"  BoolDelegate_t4240584548 * UIEventListenerGenerated_U3CUIEventListener_onPressU3Em__137_m1515408686 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/BoolDelegate UIEventListenerGenerated::<UIEventListener_onSelect>m__139()
extern "C"  BoolDelegate_t4240584548 * UIEventListenerGenerated_U3CUIEventListener_onSelectU3Em__139_m2627169283 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/FloatDelegate UIEventListenerGenerated::<UIEventListener_onScroll>m__13B()
extern "C"  FloatDelegate_t3249382860 * UIEventListenerGenerated_U3CUIEventListener_onScrollU3Em__13B_m2796652361 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/VoidDelegate UIEventListenerGenerated::<UIEventListener_onDragStart>m__13D()
extern "C"  VoidDelegate_t986119182 * UIEventListenerGenerated_U3CUIEventListener_onDragStartU3Em__13D_m423687866 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/VectorDelegate UIEventListenerGenerated::<UIEventListener_onDrag>m__13F()
extern "C"  VectorDelegate_t3512109949 * UIEventListenerGenerated_U3CUIEventListener_onDragU3Em__13F_m396522527 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/VoidDelegate UIEventListenerGenerated::<UIEventListener_onDragOver>m__141()
extern "C"  VoidDelegate_t986119182 * UIEventListenerGenerated_U3CUIEventListener_onDragOverU3Em__141_m35724612 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/VoidDelegate UIEventListenerGenerated::<UIEventListener_onDragOut>m__143()
extern "C"  VoidDelegate_t986119182 * UIEventListenerGenerated_U3CUIEventListener_onDragOutU3Em__143_m2814694364 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/VoidDelegate UIEventListenerGenerated::<UIEventListener_onDragEnd>m__145()
extern "C"  VoidDelegate_t986119182 * UIEventListenerGenerated_U3CUIEventListener_onDragEndU3Em__145_m66875505 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/ObjectDelegate UIEventListenerGenerated::<UIEventListener_onDrop>m__147()
extern "C"  ObjectDelegate_t172593529 * UIEventListenerGenerated_U3CUIEventListener_onDropU3Em__147_m2761653592 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/KeyCodeDelegate UIEventListenerGenerated::<UIEventListener_onKey>m__149()
extern "C"  KeyCodeDelegate_t2040735132 * UIEventListenerGenerated_U3CUIEventListener_onKeyU3Em__149_m2789408277 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/BoolDelegate UIEventListenerGenerated::<UIEventListener_onTooltip>m__14B()
extern "C"  BoolDelegate_t4240584548 * UIEventListenerGenerated_U3CUIEventListener_onTooltipU3Em__14B_m488240280 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIEventListenerGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UIEventListenerGenerated_ilo_attachFinalizerObject1_m660272601 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated::ilo_setWhatever2(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  void UIEventListenerGenerated_ilo_setWhatever2_m2569414580 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIEventListenerGenerated::ilo_getWhatever3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIEventListenerGenerated_ilo_getWhatever3_m4158826856 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIEventListenerGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIEventListenerGenerated_ilo_setObject4_m877914183 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated::ilo_addJSFunCSDelegateRel5(System.Int32,System.Delegate)
extern "C"  void UIEventListenerGenerated_ilo_addJSFunCSDelegateRel5_m3205335504 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIEventListenerGenerated::ilo_getInt326(System.Int32)
extern "C"  int32_t UIEventListenerGenerated_ilo_getInt326_m2761467218 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIEventListenerGenerated::ilo_getSingle7(System.Int32)
extern "C"  float UIEventListenerGenerated_ilo_getSingle7_m754848679 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIEventListenerGenerated::ilo_getObject8(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIEventListenerGenerated_ilo_getObject8_m2167247728 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener UIEventListenerGenerated::ilo_Get9(UnityEngine.GameObject)
extern "C"  UIEventListener_t1278105402 * UIEventListenerGenerated_ilo_Get9_m1936678757 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject UIEventListenerGenerated::ilo_getFunctionS10(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * UIEventListenerGenerated_ilo_getFunctionS10_m2610781507 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIEventListenerGenerated::ilo_isFunctionS11(System.Int32)
extern "C"  bool UIEventListenerGenerated_ilo_isFunctionS11_m3389010463 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/VoidDelegate UIEventListenerGenerated::ilo_UIEventListener_onClick_GetDelegate_member2_arg012(CSRepresentedObject)
extern "C"  VoidDelegate_t986119182 * UIEventListenerGenerated_ilo_UIEventListener_onClick_GetDelegate_member2_arg012_m3497036315 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/BoolDelegate UIEventListenerGenerated::ilo_UIEventListener_onHover_GetDelegate_member4_arg013(CSRepresentedObject)
extern "C"  BoolDelegate_t4240584548 * UIEventListenerGenerated_ilo_UIEventListener_onHover_GetDelegate_member4_arg013_m1706732614 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/BoolDelegate UIEventListenerGenerated::ilo_UIEventListener_onSelect_GetDelegate_member6_arg014(CSRepresentedObject)
extern "C"  BoolDelegate_t4240584548 * UIEventListenerGenerated_ilo_UIEventListener_onSelect_GetDelegate_member6_arg014_m2549571297 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/VoidDelegate UIEventListenerGenerated::ilo_UIEventListener_onDragOut_GetDelegate_member11_arg015(CSRepresentedObject)
extern "C"  VoidDelegate_t986119182 * UIEventListenerGenerated_ilo_UIEventListener_onDragOut_GetDelegate_member11_arg015_m2025960676 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/VoidDelegate UIEventListenerGenerated::ilo_UIEventListener_onDragEnd_GetDelegate_member12_arg016(CSRepresentedObject)
extern "C"  VoidDelegate_t986119182 * UIEventListenerGenerated_ilo_UIEventListener_onDragEnd_GetDelegate_member12_arg016_m1790515735 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIEventListener/KeyCodeDelegate UIEventListenerGenerated::ilo_UIEventListener_onKey_GetDelegate_member14_arg017(CSRepresentedObject)
extern "C"  KeyCodeDelegate_t2040735132 * UIEventListenerGenerated_ilo_UIEventListener_onKey_GetDelegate_member14_arg017_m2601178856 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
