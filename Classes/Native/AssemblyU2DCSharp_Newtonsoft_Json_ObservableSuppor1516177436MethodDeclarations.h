﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.ObservableSupport.AddingNewEventArgs
struct AddingNewEventArgs_t1516177436;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.ObservableSupport.AddingNewEventArgs::.ctor()
extern "C"  void AddingNewEventArgs__ctor_m2460763404 (AddingNewEventArgs_t1516177436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.ObservableSupport.AddingNewEventArgs::.ctor(System.Object)
extern "C"  void AddingNewEventArgs__ctor_m1714425864 (AddingNewEventArgs_t1516177436 * __this, Il2CppObject * ___newObject0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.ObservableSupport.AddingNewEventArgs::get_NewObject()
extern "C"  Il2CppObject * AddingNewEventArgs_get_NewObject_m1200828267 (AddingNewEventArgs_t1516177436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.ObservableSupport.AddingNewEventArgs::set_NewObject(System.Object)
extern "C"  void AddingNewEventArgs_set_NewObject_m2217393928 (AddingNewEventArgs_t1516177436 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
