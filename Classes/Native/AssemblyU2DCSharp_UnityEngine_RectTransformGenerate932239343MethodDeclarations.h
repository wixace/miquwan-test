﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_RectTransformGenerated
struct UnityEngine_RectTransformGenerated_t932239343;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_RectTransformGenerated::.ctor()
extern "C"  void UnityEngine_RectTransformGenerated__ctor_m992245132 (UnityEngine_RectTransformGenerated_t932239343 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectTransformGenerated::RectTransform_RectTransform1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectTransformGenerated_RectTransform_RectTransform1_m2069058076 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectTransformGenerated::RectTransform_rect(JSVCall)
extern "C"  void UnityEngine_RectTransformGenerated_RectTransform_rect_m1760921908 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectTransformGenerated::RectTransform_anchorMin(JSVCall)
extern "C"  void UnityEngine_RectTransformGenerated_RectTransform_anchorMin_m3254565111 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectTransformGenerated::RectTransform_anchorMax(JSVCall)
extern "C"  void UnityEngine_RectTransformGenerated_RectTransform_anchorMax_m2780139045 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectTransformGenerated::RectTransform_anchoredPosition3D(JSVCall)
extern "C"  void UnityEngine_RectTransformGenerated_RectTransform_anchoredPosition3D_m1260877898 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectTransformGenerated::RectTransform_anchoredPosition(JSVCall)
extern "C"  void UnityEngine_RectTransformGenerated_RectTransform_anchoredPosition_m2597408987 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectTransformGenerated::RectTransform_sizeDelta(JSVCall)
extern "C"  void UnityEngine_RectTransformGenerated_RectTransform_sizeDelta_m388611421 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectTransformGenerated::RectTransform_pivot(JSVCall)
extern "C"  void UnityEngine_RectTransformGenerated_RectTransform_pivot_m60676274 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectTransformGenerated::RectTransform_offsetMin(JSVCall)
extern "C"  void UnityEngine_RectTransformGenerated_RectTransform_offsetMin_m2284289493 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectTransformGenerated::RectTransform_offsetMax(JSVCall)
extern "C"  void UnityEngine_RectTransformGenerated_RectTransform_offsetMax_m1809863427 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectTransformGenerated::RectTransform_GetLocalCorners__Vector3_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectTransformGenerated_RectTransform_GetLocalCorners__Vector3_Array_m3223564532 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectTransformGenerated::RectTransform_GetWorldCorners__Vector3_Array(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectTransformGenerated_RectTransform_GetWorldCorners__Vector3_Array_m3226635931 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectTransformGenerated::RectTransform_SetInsetAndSizeFromParentEdge__Edge__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectTransformGenerated_RectTransform_SetInsetAndSizeFromParentEdge__Edge__Single__Single_m2383343018 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectTransformGenerated::RectTransform_SetSizeWithCurrentAnchors__Axis__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_RectTransformGenerated_RectTransform_SetSizeWithCurrentAnchors__Axis__Single_m2038639590 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectTransformGenerated::__Register()
extern "C"  void UnityEngine_RectTransformGenerated___Register_m384081595 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine_RectTransformGenerated::<RectTransform_GetLocalCorners__Vector3_Array>m__2CD()
extern "C"  Vector3U5BU5D_t215400611* UnityEngine_RectTransformGenerated_U3CRectTransform_GetLocalCorners__Vector3_ArrayU3Em__2CD_m601829184 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3[] UnityEngine_RectTransformGenerated::<RectTransform_GetWorldCorners__Vector3_Array>m__2CE()
extern "C"  Vector3U5BU5D_t215400611* UnityEngine_RectTransformGenerated_U3CRectTransform_GetWorldCorners__Vector3_ArrayU3Em__2CE_m3728093690 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_RectTransformGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_RectTransformGenerated_ilo_attachFinalizerObject1_m1488527059 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectTransformGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_RectTransformGenerated_ilo_addJSCSRel2_m1668618953 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RectTransformGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_RectTransformGenerated_ilo_setObject3_m1233920384 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectTransformGenerated::ilo_setVector2S4(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_RectTransformGenerated_ilo_setVector2S4_m649358558 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_RectTransformGenerated::ilo_getVector2S5(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_RectTransformGenerated_ilo_getVector2S5_m1919457234 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_RectTransformGenerated::ilo_setVector3S6(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_RectTransformGenerated_ilo_setVector3S6_m135118848 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_RectTransformGenerated::ilo_getVector3S7(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_RectTransformGenerated_ilo_getVector3S7_m322389684 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_RectTransformGenerated::ilo_getSingle8(System.Int32)
extern "C"  float UnityEngine_RectTransformGenerated_ilo_getSingle8_m2840301538 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RectTransformGenerated::ilo_getElement9(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_RectTransformGenerated_ilo_getElement9_m1177485898 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_RectTransformGenerated::ilo_getObject10(System.Int32)
extern "C"  int32_t UnityEngine_RectTransformGenerated_ilo_getObject10_m2272703182 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
