﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_SleepTimeoutGenerated
struct UnityEngine_SleepTimeoutGenerated_t963851709;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_SleepTimeoutGenerated::.ctor()
extern "C"  void UnityEngine_SleepTimeoutGenerated__ctor_m3918384206 (UnityEngine_SleepTimeoutGenerated_t963851709 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SleepTimeoutGenerated::SleepTimeout_SleepTimeout1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SleepTimeoutGenerated_SleepTimeout_SleepTimeout1_m396123684 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SleepTimeoutGenerated::SleepTimeout_NeverSleep(JSVCall)
extern "C"  void UnityEngine_SleepTimeoutGenerated_SleepTimeout_NeverSleep_m3472053979 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SleepTimeoutGenerated::SleepTimeout_SystemSetting(JSVCall)
extern "C"  void UnityEngine_SleepTimeoutGenerated_SleepTimeout_SystemSetting_m1200064741 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SleepTimeoutGenerated::__Register()
extern "C"  void UnityEngine_SleepTimeoutGenerated___Register_m233648441 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SleepTimeoutGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_SleepTimeoutGenerated_ilo_getObject1_m1952762408 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SleepTimeoutGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_SleepTimeoutGenerated_ilo_attachFinalizerObject2_m510388906 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SleepTimeoutGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void UnityEngine_SleepTimeoutGenerated_ilo_setInt323_m1230706694 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
