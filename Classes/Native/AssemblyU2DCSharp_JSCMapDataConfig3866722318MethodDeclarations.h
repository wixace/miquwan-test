﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSCMapDataConfig
struct JSCMapDataConfig_t3866722318;
// ProtoBuf.IExtension
struct IExtension_t1606339106;
// System.Collections.Generic.List`1<JSCLevelConfig>
struct List_1_t2779285052;
// System.Collections.Generic.List`1<JSCMapPathConfig>
struct List_1_t499336985;

#include "codegen/il2cpp-codegen.h"

// System.Void JSCMapDataConfig::.ctor()
extern "C"  void JSCMapDataConfig__ctor_m1318049357 (JSCMapDataConfig_t3866722318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.IExtension JSCMapDataConfig::ProtoBuf.IExtensible.GetExtensionObject(System.Boolean)
extern "C"  Il2CppObject * JSCMapDataConfig_ProtoBuf_IExtensible_GetExtensionObject_m2665854493 (JSCMapDataConfig_t3866722318 * __this, bool ___createIfMissing0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<JSCLevelConfig> JSCMapDataConfig::get_jarr()
extern "C"  List_1_t2779285052 * JSCMapDataConfig_get_jarr_m1363684406 (JSCMapDataConfig_t3866722318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<JSCMapPathConfig> JSCMapDataConfig::get_pathjarr()
extern "C"  List_1_t499336985 * JSCMapDataConfig_get_pathjarr_m589031672 (JSCMapDataConfig_t3866722318 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
