﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EListGenerated/<EList_SortT1__ListT1_T__ComparisonT1_T>c__AnonStorey5B
struct U3CEList_SortT1__ListT1_T__ComparisonT1_TU3Ec__AnonStorey5B_t1476703505;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void EListGenerated/<EList_SortT1__ListT1_T__ComparisonT1_T>c__AnonStorey5B::.ctor()
extern "C"  void U3CEList_SortT1__ListT1_T__ComparisonT1_TU3Ec__AnonStorey5B__ctor_m3551662762 (U3CEList_SortT1__ListT1_T__ComparisonT1_TU3Ec__AnonStorey5B_t1476703505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object EListGenerated/<EList_SortT1__ListT1_T__ComparisonT1_T>c__AnonStorey5B::<>m__41()
extern "C"  Il2CppObject * U3CEList_SortT1__ListT1_T__ComparisonT1_TU3Ec__AnonStorey5B_U3CU3Em__41_m3403416091 (U3CEList_SortT1__ListT1_T__ComparisonT1_TU3Ec__AnonStorey5B_t1476703505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
