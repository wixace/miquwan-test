﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Mail.AlternateViewCollection
struct AlternateViewCollection_t943595443;
// System.Net.Mail.AlternateView
struct AlternateView_t1036763893;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Mail_AlternateView1036763893.h"

// System.Void System.Net.Mail.AlternateViewCollection::.ctor()
extern "C"  void AlternateViewCollection__ctor_m1942971252 (AlternateViewCollection_t943595443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.AlternateViewCollection::Dispose()
extern "C"  void AlternateViewCollection_Dispose_m3077849777 (AlternateViewCollection_t943595443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.AlternateViewCollection::ClearItems()
extern "C"  void AlternateViewCollection_ClearItems_m340094147 (AlternateViewCollection_t943595443 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.AlternateViewCollection::InsertItem(System.Int32,System.Net.Mail.AlternateView)
extern "C"  void AlternateViewCollection_InsertItem_m418336801 (AlternateViewCollection_t943595443 * __this, int32_t ___index0, AlternateView_t1036763893 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.AlternateViewCollection::RemoveItem(System.Int32)
extern "C"  void AlternateViewCollection_RemoveItem_m739257048 (AlternateViewCollection_t943595443 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.AlternateViewCollection::SetItem(System.Int32,System.Net.Mail.AlternateView)
extern "C"  void AlternateViewCollection_SetItem_m3720926604 (AlternateViewCollection_t943595443 * __this, int32_t ___index0, AlternateView_t1036763893 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
