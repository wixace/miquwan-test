﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_RuntimeAnimatorController274649809.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorOverrideController
struct  AnimatorOverrideController_t57503379  : public RuntimeAnimatorController_t274649809
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
