﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<EffectCtrl>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1659512783(__this, ___l0, method) ((  void (*) (Enumerator_t801678670 *, List_1_t782005900 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<EffectCtrl>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1964050211(__this, method) ((  void (*) (Enumerator_t801678670 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<EffectCtrl>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2256897945(__this, method) ((  Il2CppObject * (*) (Enumerator_t801678670 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<EffectCtrl>::Dispose()
#define Enumerator_Dispose_m2865125748(__this, method) ((  void (*) (Enumerator_t801678670 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<EffectCtrl>::VerifyState()
#define Enumerator_VerifyState_m763903661(__this, method) ((  void (*) (Enumerator_t801678670 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<EffectCtrl>::MoveNext()
#define Enumerator_MoveNext_m1173351635(__this, method) ((  bool (*) (Enumerator_t801678670 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<EffectCtrl>::get_Current()
#define Enumerator_get_Current_m2718793478(__this, method) ((  EffectCtrl_t3708787644 * (*) (Enumerator_t801678670 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
