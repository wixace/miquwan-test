﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIDragScrollView
struct UIDragScrollView_t123487002;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UnityEngine.Behaviour
struct Behaviour_t200106419;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Behaviour200106419.h"

// System.Void UIDragScrollView::.ctor()
extern "C"  void UIDragScrollView__ctor_m1888075969 (UIDragScrollView_t123487002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragScrollView::OnEnable()
extern "C"  void UIDragScrollView_OnEnable_m2851062565 (UIDragScrollView_t123487002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragScrollView::Start()
extern "C"  void UIDragScrollView_Start_m835213761 (UIDragScrollView_t123487002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragScrollView::FindScrollView()
extern "C"  void UIDragScrollView_FindScrollView_m561079502 (UIDragScrollView_t123487002 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragScrollView::OnPress(System.Boolean)
extern "C"  void UIDragScrollView_OnPress_m3659971386 (UIDragScrollView_t123487002 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragScrollView::OnDrag(UnityEngine.Vector2)
extern "C"  void UIDragScrollView_OnDrag_m3078757668 (UIDragScrollView_t123487002 * __this, Vector2_t4282066565  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragScrollView::OnScroll(System.Single)
extern "C"  void UIDragScrollView_OnScroll_m378897436 (UIDragScrollView_t123487002 * __this, float ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDragScrollView::ilo_GetActive1(UnityEngine.GameObject)
extern "C"  bool UIDragScrollView_ilo_GetActive1_m3783459831 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDragScrollView::ilo_GetActive2(UnityEngine.Behaviour)
extern "C"  bool UIDragScrollView_ilo_GetActive2_m4141726348 (Il2CppObject * __this /* static, unused */, Behaviour_t200106419 * ___mb0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
