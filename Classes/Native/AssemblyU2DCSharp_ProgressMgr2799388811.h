﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "AssemblyU2DCSharp_Singleton_1_gen3052204204.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProgressMgr
struct  ProgressMgr_t2799388811  : public Singleton_1_t3052204204
{
public:
	// UnityEngine.GameObject ProgressMgr::parentGO
	GameObject_t3674682005 * ___parentGO_3;

public:
	inline static int32_t get_offset_of_parentGO_3() { return static_cast<int32_t>(offsetof(ProgressMgr_t2799388811, ___parentGO_3)); }
	inline GameObject_t3674682005 * get_parentGO_3() const { return ___parentGO_3; }
	inline GameObject_t3674682005 ** get_address_of_parentGO_3() { return &___parentGO_3; }
	inline void set_parentGO_3(GameObject_t3674682005 * value)
	{
		___parentGO_3 = value;
		Il2CppCodeGenWriteBarrier(&___parentGO_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
