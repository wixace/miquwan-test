﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_Mihua_Asset_ABLoadOperation_Asset778728221.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Mihua.Asset.ABLoadOperation.AssetOperationLoaded
struct  AssetOperationLoaded_t667103362  : public AssetOperation_t778728221
{
public:
	// System.String Mihua.Asset.ABLoadOperation.AssetOperationLoaded::m_AssetName
	String_t* ___m_AssetName_2;

public:
	inline static int32_t get_offset_of_m_AssetName_2() { return static_cast<int32_t>(offsetof(AssetOperationLoaded_t667103362, ___m_AssetName_2)); }
	inline String_t* get_m_AssetName_2() const { return ___m_AssetName_2; }
	inline String_t** get_address_of_m_AssetName_2() { return &___m_AssetName_2; }
	inline void set_m_AssetName_2(String_t* value)
	{
		___m_AssetName_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_AssetName_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
