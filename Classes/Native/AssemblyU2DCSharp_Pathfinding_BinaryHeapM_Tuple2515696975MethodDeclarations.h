﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.PathNode
struct PathNode_t417131581;
// Pathfinding.BinaryHeapM/Tuple
struct Tuple_t2515696975;
struct Tuple_t2515696975_marshaled_pinvoke;
struct Tuple_t2515696975_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_BinaryHeapM_Tuple2515696975.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"

// System.Void Pathfinding.BinaryHeapM/Tuple::.ctor(System.UInt32,Pathfinding.PathNode)
extern "C"  void Tuple__ctor_m734461047 (Tuple_t2515696975 * __this, uint32_t ___F0, PathNode_t417131581 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Tuple_t2515696975;
struct Tuple_t2515696975_marshaled_pinvoke;

extern "C" void Tuple_t2515696975_marshal_pinvoke(const Tuple_t2515696975& unmarshaled, Tuple_t2515696975_marshaled_pinvoke& marshaled);
extern "C" void Tuple_t2515696975_marshal_pinvoke_back(const Tuple_t2515696975_marshaled_pinvoke& marshaled, Tuple_t2515696975& unmarshaled);
extern "C" void Tuple_t2515696975_marshal_pinvoke_cleanup(Tuple_t2515696975_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Tuple_t2515696975;
struct Tuple_t2515696975_marshaled_com;

extern "C" void Tuple_t2515696975_marshal_com(const Tuple_t2515696975& unmarshaled, Tuple_t2515696975_marshaled_com& marshaled);
extern "C" void Tuple_t2515696975_marshal_com_back(const Tuple_t2515696975_marshaled_com& marshaled, Tuple_t2515696975& unmarshaled);
extern "C" void Tuple_t2515696975_marshal_com_cleanup(Tuple_t2515696975_marshaled_com& marshaled);
