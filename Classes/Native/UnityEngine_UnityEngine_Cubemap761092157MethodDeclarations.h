﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Cubemap
struct Cubemap_t761092157;
// UnityEngine.Color[]
struct ColorU5BU5D_t2441545636;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_TextureFormat4189619560.h"
#include "UnityEngine_UnityEngine_CubemapFace2005084858.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Cubemap761092157.h"

// System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern "C"  void Cubemap__ctor_m1716353673 (Cubemap_t761092157 * __this, int32_t ___size0, int32_t ___format1, bool ___mipmap2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cubemap::SetPixel(UnityEngine.CubemapFace,System.Int32,System.Int32,UnityEngine.Color)
extern "C"  void Cubemap_SetPixel_m4256925327 (Cubemap_t761092157 * __this, int32_t ___face0, int32_t ___x1, int32_t ___y2, Color_t4194546905  ___color3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cubemap::INTERNAL_CALL_SetPixel(UnityEngine.Cubemap,UnityEngine.CubemapFace,System.Int32,System.Int32,UnityEngine.Color&)
extern "C"  void Cubemap_INTERNAL_CALL_SetPixel_m3087142432 (Il2CppObject * __this /* static, unused */, Cubemap_t761092157 * ___self0, int32_t ___face1, int32_t ___x2, int32_t ___y3, Color_t4194546905 * ___color4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.Cubemap::GetPixel(UnityEngine.CubemapFace,System.Int32,System.Int32)
extern "C"  Color_t4194546905  Cubemap_GetPixel_m987981290 (Cubemap_t761092157 * __this, int32_t ___face0, int32_t ___x1, int32_t ___y2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cubemap::INTERNAL_CALL_GetPixel(UnityEngine.Cubemap,UnityEngine.CubemapFace,System.Int32,System.Int32,UnityEngine.Color&)
extern "C"  void Cubemap_INTERNAL_CALL_GetPixel_m1640660140 (Il2CppObject * __this /* static, unused */, Cubemap_t761092157 * ___self0, int32_t ___face1, int32_t ___x2, int32_t ___y3, Color_t4194546905 * ___value4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine.Cubemap::GetPixels(UnityEngine.CubemapFace,System.Int32)
extern "C"  ColorU5BU5D_t2441545636* Cubemap_GetPixels_m2475988330 (Cubemap_t761092157 * __this, int32_t ___face0, int32_t ___miplevel1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine.Cubemap::GetPixels(UnityEngine.CubemapFace)
extern "C"  ColorU5BU5D_t2441545636* Cubemap_GetPixels_m1272386701 (Cubemap_t761092157 * __this, int32_t ___face0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cubemap::SetPixels(UnityEngine.Color[],UnityEngine.CubemapFace,System.Int32)
extern "C"  void Cubemap_SetPixels_m3935588179 (Cubemap_t761092157 * __this, ColorU5BU5D_t2441545636* ___colors0, int32_t ___face1, int32_t ___miplevel2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cubemap::SetPixels(UnityEngine.Color[],UnityEngine.CubemapFace)
extern "C"  void Cubemap_SetPixels_m3802297348 (Cubemap_t761092157 * __this, ColorU5BU5D_t2441545636* ___colors0, int32_t ___face1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Cubemap::get_mipmapCount()
extern "C"  int32_t Cubemap_get_mipmapCount_m3147390126 (Cubemap_t761092157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cubemap::Apply(System.Boolean,System.Boolean)
extern "C"  void Cubemap_Apply_m1087048616 (Cubemap_t761092157 * __this, bool ___updateMipmaps0, bool ___makeNoLongerReadable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cubemap::Apply(System.Boolean)
extern "C"  void Cubemap_Apply_m1278170933 (Cubemap_t761092157 * __this, bool ___updateMipmaps0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cubemap::Apply()
extern "C"  void Cubemap_Apply_m3230623294 (Cubemap_t761092157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TextureFormat UnityEngine.Cubemap::get_format()
extern "C"  int32_t Cubemap_get_format_m837850778 (Cubemap_t761092157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cubemap::Internal_Create(UnityEngine.Cubemap,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern "C"  void Cubemap_Internal_Create_m3180260873 (Il2CppObject * __this /* static, unused */, Cubemap_t761092157 * ___mono0, int32_t ___size1, int32_t ___format2, bool ___mipmap3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cubemap::SmoothEdges(System.Int32)
extern "C"  void Cubemap_SmoothEdges_m59019177 (Cubemap_t761092157 * __this, int32_t ___smoothRegionWidthInPixels0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Cubemap::SmoothEdges()
extern "C"  void Cubemap_SmoothEdges_m3882458712 (Cubemap_t761092157 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
