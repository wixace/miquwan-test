﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen890961634.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Pathfinding_RVO_RVOQuadtree_Node2108618958.h"

// System.Void System.Array/InternalEnumerator`1<Pathfinding.RVO.RVOQuadtree/Node>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1785786207_gshared (InternalEnumerator_1_t890961634 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1785786207(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t890961634 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1785786207_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.RVO.RVOQuadtree/Node>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1477407713_gshared (InternalEnumerator_1_t890961634 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1477407713(__this, method) ((  void (*) (InternalEnumerator_1_t890961634 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1477407713_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Pathfinding.RVO.RVOQuadtree/Node>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3908971853_gshared (InternalEnumerator_1_t890961634 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3908971853(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t890961634 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3908971853_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.RVO.RVOQuadtree/Node>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1129403382_gshared (InternalEnumerator_1_t890961634 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1129403382(__this, method) ((  void (*) (InternalEnumerator_1_t890961634 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1129403382_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.RVO.RVOQuadtree/Node>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m2546782541_gshared (InternalEnumerator_1_t890961634 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m2546782541(__this, method) ((  bool (*) (InternalEnumerator_1_t890961634 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m2546782541_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Pathfinding.RVO.RVOQuadtree/Node>::get_Current()
extern "C"  Node_t2108618958  InternalEnumerator_1_get_Current_m3428646_gshared (InternalEnumerator_1_t890961634 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3428646(__this, method) ((  Node_t2108618958  (*) (InternalEnumerator_1_t890961634 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3428646_gshared)(__this, method)
