﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Joint
struct Joint_t4201008640;
// UnityEngine.Rigidbody
struct Rigidbody_t3346577219;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine.Joint::.ctor()
extern "C"  void Joint__ctor_m1651806863 (Joint_t4201008640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody UnityEngine.Joint::get_connectedBody()
extern "C"  Rigidbody_t3346577219 * Joint_get_connectedBody_m3483565964 (Joint_t4201008640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint::set_connectedBody(UnityEngine.Rigidbody)
extern "C"  void Joint_set_connectedBody_m2794572257 (Joint_t4201008640 * __this, Rigidbody_t3346577219 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Joint::get_axis()
extern "C"  Vector3_t4282066566  Joint_get_axis_m2536834789 (Joint_t4201008640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint::set_axis(UnityEngine.Vector3)
extern "C"  void Joint_set_axis_m3406737510 (Joint_t4201008640 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint::INTERNAL_get_axis(UnityEngine.Vector3&)
extern "C"  void Joint_INTERNAL_get_axis_m2136751500 (Joint_t4201008640 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint::INTERNAL_set_axis(UnityEngine.Vector3&)
extern "C"  void Joint_INTERNAL_set_axis_m2272545024 (Joint_t4201008640 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Joint::get_anchor()
extern "C"  Vector3_t4282066566  Joint_get_anchor_m2186926137 (Joint_t4201008640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint::set_anchor(UnityEngine.Vector3)
extern "C"  void Joint_set_anchor_m1010170386 (Joint_t4201008640 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint::INTERNAL_get_anchor(UnityEngine.Vector3&)
extern "C"  void Joint_INTERNAL_get_anchor_m1634428896 (Joint_t4201008640 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint::INTERNAL_set_anchor(UnityEngine.Vector3&)
extern "C"  void Joint_INTERNAL_set_anchor_m3282986580 (Joint_t4201008640 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Joint::get_connectedAnchor()
extern "C"  Vector3_t4282066566  Joint_get_connectedAnchor_m2103479868 (Joint_t4201008640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint::set_connectedAnchor(UnityEngine.Vector3)
extern "C"  void Joint_set_connectedAnchor_m1591545163 (Joint_t4201008640 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint::INTERNAL_get_connectedAnchor(UnityEngine.Vector3&)
extern "C"  void Joint_INTERNAL_get_connectedAnchor_m1657863867 (Joint_t4201008640 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint::INTERNAL_set_connectedAnchor(UnityEngine.Vector3&)
extern "C"  void Joint_INTERNAL_set_connectedAnchor_m4199744199 (Joint_t4201008640 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Joint::get_autoConfigureConnectedAnchor()
extern "C"  bool Joint_get_autoConfigureConnectedAnchor_m842844159 (Joint_t4201008640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint::set_autoConfigureConnectedAnchor(System.Boolean)
extern "C"  void Joint_set_autoConfigureConnectedAnchor_m1114251856 (Joint_t4201008640 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Joint::get_breakForce()
extern "C"  float Joint_get_breakForce_m2199604638 (Joint_t4201008640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint::set_breakForce(System.Single)
extern "C"  void Joint_set_breakForce_m1632884685 (Joint_t4201008640 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Joint::get_breakTorque()
extern "C"  float Joint_get_breakTorque_m2406239833 (Joint_t4201008640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint::set_breakTorque(System.Single)
extern "C"  void Joint_set_breakTorque_m4042152178 (Joint_t4201008640 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Joint::get_enableCollision()
extern "C"  bool Joint_get_enableCollision_m1718450233 (Joint_t4201008640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint::set_enableCollision(System.Boolean)
extern "C"  void Joint_set_enableCollision_m2305836182 (Joint_t4201008640 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Joint::get_enablePreprocessing()
extern "C"  bool Joint_get_enablePreprocessing_m1136004861 (Joint_t4201008640 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint::set_enablePreprocessing(System.Boolean)
extern "C"  void Joint_set_enablePreprocessing_m3937661274 (Joint_t4201008640 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
