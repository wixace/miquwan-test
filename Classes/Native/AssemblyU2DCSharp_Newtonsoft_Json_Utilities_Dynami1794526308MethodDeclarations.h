﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.Emit.ModuleBuilder
struct ModuleBuilder_t595214213;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Utilities.DynamicWrapper::.cctor()
extern "C"  void DynamicWrapper__cctor_m2645179807 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.Emit.ModuleBuilder Newtonsoft.Json.Utilities.DynamicWrapper::get_ModuleBuilder()
extern "C"  ModuleBuilder_t595214213 * DynamicWrapper_get_ModuleBuilder_m338907405 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.DynamicWrapper::Init()
extern "C"  void DynamicWrapper_Init_m1009538246 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Newtonsoft.Json.Utilities.DynamicWrapper::GetStrongKey()
extern "C"  ByteU5BU5D_t4260760469* DynamicWrapper_GetStrongKey_m1823714482 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Utilities.DynamicWrapper::GetWrapper(System.Type,System.Type)
extern "C"  Type_t * DynamicWrapper_GetWrapper_m2141837791 (Il2CppObject * __this /* static, unused */, Type_t * ___interfaceType0, Type_t * ___realObjectType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Utilities.DynamicWrapper::GetUnderlyingObject(System.Object)
extern "C"  Il2CppObject * DynamicWrapper_GetUnderlyingObject_m1739995627 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___wrapper0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Utilities.DynamicWrapper::GenerateWrapperType(System.Type,System.Type)
extern "C"  Type_t * DynamicWrapper_GenerateWrapperType_m171545314 (Il2CppObject * __this /* static, unused */, Type_t * ___interfaceType0, Type_t * ___underlyingType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Newtonsoft.Json.Utilities.DynamicWrapper::ilo_GetStrongKey1()
extern "C"  ByteU5BU5D_t4260760469* DynamicWrapper_ilo_GetStrongKey1_m3772287054 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
