﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Single[,]
struct SingleU5BU2CU5D_t2316563990;
// ProceduralWorld/ProceduralPrefab
struct ProceduralPrefab_t2162393179;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object
struct Il2CppObject;
// ProceduralWorld/ProceduralTile
struct ProceduralTile_t3586714437;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13
struct  U3CInternalGenerateU3Ec__Iterator13_t2030525401  : public Il2CppObject
{
public:
	// System.Int32 ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::<counter>__0
	int32_t ___U3CcounterU3E__0_0;
	// System.Single[,] ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::<ditherMap>__1
	SingleU5BU2CU5D_t2316563990* ___U3CditherMapU3E__1_1;
	// System.Int32 ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::<i>__2
	int32_t ___U3CiU3E__2_2;
	// ProceduralWorld/ProceduralPrefab ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::<pref>__3
	ProceduralPrefab_t2162393179 * ___U3CprefU3E__3_3;
	// UnityEngine.Vector3 ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::<p>__4
	Vector3_t4282066566  ___U3CpU3E__4_4;
	// UnityEngine.GameObject ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::<ob>__5
	GameObject_t3674682005 * ___U3CobU3E__5_5;
	// System.Single ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::<subSize>__6
	float ___U3CsubSizeU3E__6_6;
	// System.Int32 ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::<sx>__7
	int32_t ___U3CsxU3E__7_7;
	// System.Int32 ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::<sz>__8
	int32_t ___U3CszU3E__8_8;
	// System.Int32 ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::<sx>__9
	int32_t ___U3CsxU3E__9_9;
	// System.Int32 ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::<sz>__10
	int32_t ___U3CszU3E__10_10;
	// System.Single ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::<px>__11
	float ___U3CpxU3E__11_11;
	// System.Single ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::<pz>__12
	float ___U3CpzU3E__12_12;
	// System.Single ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::<perl>__13
	float ___U3CperlU3E__13_13;
	// System.Single ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::<density>__14
	float ___U3CdensityU3E__14_14;
	// System.Single ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::<fcount>__15
	float ___U3CfcountU3E__15_15;
	// System.Int32 ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::<count>__16
	int32_t ___U3CcountU3E__16_16;
	// System.Int32 ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::<j>__17
	int32_t ___U3CjU3E__17_17;
	// UnityEngine.Vector3 ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::<p>__18
	Vector3_t4282066566  ___U3CpU3E__18_18;
	// UnityEngine.GameObject ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::<ob>__19
	GameObject_t3674682005 * ___U3CobU3E__19_19;
	// System.Int32 ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::$PC
	int32_t ___U24PC_20;
	// System.Object ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::$current
	Il2CppObject * ___U24current_21;
	// ProceduralWorld/ProceduralTile ProceduralWorld/ProceduralTile/<InternalGenerate>c__Iterator13::<>f__this
	ProceduralTile_t3586714437 * ___U3CU3Ef__this_22;

public:
	inline static int32_t get_offset_of_U3CcounterU3E__0_0() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U3CcounterU3E__0_0)); }
	inline int32_t get_U3CcounterU3E__0_0() const { return ___U3CcounterU3E__0_0; }
	inline int32_t* get_address_of_U3CcounterU3E__0_0() { return &___U3CcounterU3E__0_0; }
	inline void set_U3CcounterU3E__0_0(int32_t value)
	{
		___U3CcounterU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CditherMapU3E__1_1() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U3CditherMapU3E__1_1)); }
	inline SingleU5BU2CU5D_t2316563990* get_U3CditherMapU3E__1_1() const { return ___U3CditherMapU3E__1_1; }
	inline SingleU5BU2CU5D_t2316563990** get_address_of_U3CditherMapU3E__1_1() { return &___U3CditherMapU3E__1_1; }
	inline void set_U3CditherMapU3E__1_1(SingleU5BU2CU5D_t2316563990* value)
	{
		___U3CditherMapU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CditherMapU3E__1_1, value);
	}

	inline static int32_t get_offset_of_U3CiU3E__2_2() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U3CiU3E__2_2)); }
	inline int32_t get_U3CiU3E__2_2() const { return ___U3CiU3E__2_2; }
	inline int32_t* get_address_of_U3CiU3E__2_2() { return &___U3CiU3E__2_2; }
	inline void set_U3CiU3E__2_2(int32_t value)
	{
		___U3CiU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CprefU3E__3_3() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U3CprefU3E__3_3)); }
	inline ProceduralPrefab_t2162393179 * get_U3CprefU3E__3_3() const { return ___U3CprefU3E__3_3; }
	inline ProceduralPrefab_t2162393179 ** get_address_of_U3CprefU3E__3_3() { return &___U3CprefU3E__3_3; }
	inline void set_U3CprefU3E__3_3(ProceduralPrefab_t2162393179 * value)
	{
		___U3CprefU3E__3_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CprefU3E__3_3, value);
	}

	inline static int32_t get_offset_of_U3CpU3E__4_4() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U3CpU3E__4_4)); }
	inline Vector3_t4282066566  get_U3CpU3E__4_4() const { return ___U3CpU3E__4_4; }
	inline Vector3_t4282066566 * get_address_of_U3CpU3E__4_4() { return &___U3CpU3E__4_4; }
	inline void set_U3CpU3E__4_4(Vector3_t4282066566  value)
	{
		___U3CpU3E__4_4 = value;
	}

	inline static int32_t get_offset_of_U3CobU3E__5_5() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U3CobU3E__5_5)); }
	inline GameObject_t3674682005 * get_U3CobU3E__5_5() const { return ___U3CobU3E__5_5; }
	inline GameObject_t3674682005 ** get_address_of_U3CobU3E__5_5() { return &___U3CobU3E__5_5; }
	inline void set_U3CobU3E__5_5(GameObject_t3674682005 * value)
	{
		___U3CobU3E__5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobU3E__5_5, value);
	}

	inline static int32_t get_offset_of_U3CsubSizeU3E__6_6() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U3CsubSizeU3E__6_6)); }
	inline float get_U3CsubSizeU3E__6_6() const { return ___U3CsubSizeU3E__6_6; }
	inline float* get_address_of_U3CsubSizeU3E__6_6() { return &___U3CsubSizeU3E__6_6; }
	inline void set_U3CsubSizeU3E__6_6(float value)
	{
		___U3CsubSizeU3E__6_6 = value;
	}

	inline static int32_t get_offset_of_U3CsxU3E__7_7() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U3CsxU3E__7_7)); }
	inline int32_t get_U3CsxU3E__7_7() const { return ___U3CsxU3E__7_7; }
	inline int32_t* get_address_of_U3CsxU3E__7_7() { return &___U3CsxU3E__7_7; }
	inline void set_U3CsxU3E__7_7(int32_t value)
	{
		___U3CsxU3E__7_7 = value;
	}

	inline static int32_t get_offset_of_U3CszU3E__8_8() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U3CszU3E__8_8)); }
	inline int32_t get_U3CszU3E__8_8() const { return ___U3CszU3E__8_8; }
	inline int32_t* get_address_of_U3CszU3E__8_8() { return &___U3CszU3E__8_8; }
	inline void set_U3CszU3E__8_8(int32_t value)
	{
		___U3CszU3E__8_8 = value;
	}

	inline static int32_t get_offset_of_U3CsxU3E__9_9() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U3CsxU3E__9_9)); }
	inline int32_t get_U3CsxU3E__9_9() const { return ___U3CsxU3E__9_9; }
	inline int32_t* get_address_of_U3CsxU3E__9_9() { return &___U3CsxU3E__9_9; }
	inline void set_U3CsxU3E__9_9(int32_t value)
	{
		___U3CsxU3E__9_9 = value;
	}

	inline static int32_t get_offset_of_U3CszU3E__10_10() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U3CszU3E__10_10)); }
	inline int32_t get_U3CszU3E__10_10() const { return ___U3CszU3E__10_10; }
	inline int32_t* get_address_of_U3CszU3E__10_10() { return &___U3CszU3E__10_10; }
	inline void set_U3CszU3E__10_10(int32_t value)
	{
		___U3CszU3E__10_10 = value;
	}

	inline static int32_t get_offset_of_U3CpxU3E__11_11() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U3CpxU3E__11_11)); }
	inline float get_U3CpxU3E__11_11() const { return ___U3CpxU3E__11_11; }
	inline float* get_address_of_U3CpxU3E__11_11() { return &___U3CpxU3E__11_11; }
	inline void set_U3CpxU3E__11_11(float value)
	{
		___U3CpxU3E__11_11 = value;
	}

	inline static int32_t get_offset_of_U3CpzU3E__12_12() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U3CpzU3E__12_12)); }
	inline float get_U3CpzU3E__12_12() const { return ___U3CpzU3E__12_12; }
	inline float* get_address_of_U3CpzU3E__12_12() { return &___U3CpzU3E__12_12; }
	inline void set_U3CpzU3E__12_12(float value)
	{
		___U3CpzU3E__12_12 = value;
	}

	inline static int32_t get_offset_of_U3CperlU3E__13_13() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U3CperlU3E__13_13)); }
	inline float get_U3CperlU3E__13_13() const { return ___U3CperlU3E__13_13; }
	inline float* get_address_of_U3CperlU3E__13_13() { return &___U3CperlU3E__13_13; }
	inline void set_U3CperlU3E__13_13(float value)
	{
		___U3CperlU3E__13_13 = value;
	}

	inline static int32_t get_offset_of_U3CdensityU3E__14_14() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U3CdensityU3E__14_14)); }
	inline float get_U3CdensityU3E__14_14() const { return ___U3CdensityU3E__14_14; }
	inline float* get_address_of_U3CdensityU3E__14_14() { return &___U3CdensityU3E__14_14; }
	inline void set_U3CdensityU3E__14_14(float value)
	{
		___U3CdensityU3E__14_14 = value;
	}

	inline static int32_t get_offset_of_U3CfcountU3E__15_15() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U3CfcountU3E__15_15)); }
	inline float get_U3CfcountU3E__15_15() const { return ___U3CfcountU3E__15_15; }
	inline float* get_address_of_U3CfcountU3E__15_15() { return &___U3CfcountU3E__15_15; }
	inline void set_U3CfcountU3E__15_15(float value)
	{
		___U3CfcountU3E__15_15 = value;
	}

	inline static int32_t get_offset_of_U3CcountU3E__16_16() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U3CcountU3E__16_16)); }
	inline int32_t get_U3CcountU3E__16_16() const { return ___U3CcountU3E__16_16; }
	inline int32_t* get_address_of_U3CcountU3E__16_16() { return &___U3CcountU3E__16_16; }
	inline void set_U3CcountU3E__16_16(int32_t value)
	{
		___U3CcountU3E__16_16 = value;
	}

	inline static int32_t get_offset_of_U3CjU3E__17_17() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U3CjU3E__17_17)); }
	inline int32_t get_U3CjU3E__17_17() const { return ___U3CjU3E__17_17; }
	inline int32_t* get_address_of_U3CjU3E__17_17() { return &___U3CjU3E__17_17; }
	inline void set_U3CjU3E__17_17(int32_t value)
	{
		___U3CjU3E__17_17 = value;
	}

	inline static int32_t get_offset_of_U3CpU3E__18_18() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U3CpU3E__18_18)); }
	inline Vector3_t4282066566  get_U3CpU3E__18_18() const { return ___U3CpU3E__18_18; }
	inline Vector3_t4282066566 * get_address_of_U3CpU3E__18_18() { return &___U3CpU3E__18_18; }
	inline void set_U3CpU3E__18_18(Vector3_t4282066566  value)
	{
		___U3CpU3E__18_18 = value;
	}

	inline static int32_t get_offset_of_U3CobU3E__19_19() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U3CobU3E__19_19)); }
	inline GameObject_t3674682005 * get_U3CobU3E__19_19() const { return ___U3CobU3E__19_19; }
	inline GameObject_t3674682005 ** get_address_of_U3CobU3E__19_19() { return &___U3CobU3E__19_19; }
	inline void set_U3CobU3E__19_19(GameObject_t3674682005 * value)
	{
		___U3CobU3E__19_19 = value;
		Il2CppCodeGenWriteBarrier(&___U3CobU3E__19_19, value);
	}

	inline static int32_t get_offset_of_U24PC_20() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U24PC_20)); }
	inline int32_t get_U24PC_20() const { return ___U24PC_20; }
	inline int32_t* get_address_of_U24PC_20() { return &___U24PC_20; }
	inline void set_U24PC_20(int32_t value)
	{
		___U24PC_20 = value;
	}

	inline static int32_t get_offset_of_U24current_21() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U24current_21)); }
	inline Il2CppObject * get_U24current_21() const { return ___U24current_21; }
	inline Il2CppObject ** get_address_of_U24current_21() { return &___U24current_21; }
	inline void set_U24current_21(Il2CppObject * value)
	{
		___U24current_21 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_21, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_22() { return static_cast<int32_t>(offsetof(U3CInternalGenerateU3Ec__Iterator13_t2030525401, ___U3CU3Ef__this_22)); }
	inline ProceduralTile_t3586714437 * get_U3CU3Ef__this_22() const { return ___U3CU3Ef__this_22; }
	inline ProceduralTile_t3586714437 ** get_address_of_U3CU3Ef__this_22() { return &___U3CU3Ef__this_22; }
	inline void set_U3CU3Ef__this_22(ProceduralTile_t3586714437 * value)
	{
		___U3CU3Ef__this_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_22, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
