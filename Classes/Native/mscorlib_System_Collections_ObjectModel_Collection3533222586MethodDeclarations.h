﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Core.RpsChoice>
struct Collection_1_t3533222586;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Core.RpsChoice[]
struct RpsChoiceU5BU5D_t3806589061;
// System.Collections.Generic.IEnumerator`1<Core.RpsChoice>
struct IEnumerator_1_t1964662181;
// System.Collections.Generic.IList`1<Core.RpsChoice>
struct IList_1_t2747444335;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Core_RpsChoice52797132.h"

// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::.ctor()
extern "C"  void Collection_1__ctor_m189875246_gshared (Collection_1_t3533222586 * __this, const MethodInfo* method);
#define Collection_1__ctor_m189875246(__this, method) ((  void (*) (Collection_1_t3533222586 *, const MethodInfo*))Collection_1__ctor_m189875246_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m68472905_gshared (Collection_1_t3533222586 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m68472905(__this, method) ((  bool (*) (Collection_1_t3533222586 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m68472905_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m3185213654_gshared (Collection_1_t3533222586 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m3185213654(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3533222586 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m3185213654_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2207167589_gshared (Collection_1_t3533222586 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2207167589(__this, method) ((  Il2CppObject * (*) (Collection_1_t3533222586 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2207167589_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m3790305784_gshared (Collection_1_t3533222586 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m3790305784(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3533222586 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m3790305784_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m414128712_gshared (Collection_1_t3533222586 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m414128712(__this, ___value0, method) ((  bool (*) (Collection_1_t3533222586 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m414128712_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1517155408_gshared (Collection_1_t3533222586 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1517155408(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3533222586 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1517155408_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m377017795_gshared (Collection_1_t3533222586 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m377017795(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3533222586 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m377017795_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m2013508229_gshared (Collection_1_t3533222586 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m2013508229(__this, ___value0, method) ((  void (*) (Collection_1_t3533222586 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2013508229_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m2157361172_gshared (Collection_1_t3533222586 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2157361172(__this, method) ((  bool (*) (Collection_1_t3533222586 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m2157361172_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m425413254_gshared (Collection_1_t3533222586 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m425413254(__this, method) ((  Il2CppObject * (*) (Collection_1_t3533222586 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m425413254_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m629809079_gshared (Collection_1_t3533222586 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m629809079(__this, method) ((  bool (*) (Collection_1_t3533222586 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m629809079_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m3522366562_gshared (Collection_1_t3533222586 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m3522366562(__this, method) ((  bool (*) (Collection_1_t3533222586 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m3522366562_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m1865855629_gshared (Collection_1_t3533222586 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1865855629(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3533222586 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1865855629_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2150800602_gshared (Collection_1_t3533222586 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2150800602(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3533222586 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2150800602_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::Add(T)
extern "C"  void Collection_1_Add_m966098577_gshared (Collection_1_t3533222586 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Add_m966098577(__this, ___item0, method) ((  void (*) (Collection_1_t3533222586 *, int32_t, const MethodInfo*))Collection_1_Add_m966098577_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::Clear()
extern "C"  void Collection_1_Clear_m1890975833_gshared (Collection_1_t3533222586 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1890975833(__this, method) ((  void (*) (Collection_1_t3533222586 *, const MethodInfo*))Collection_1_Clear_m1890975833_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::ClearItems()
extern "C"  void Collection_1_ClearItems_m2153050185_gshared (Collection_1_t3533222586 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m2153050185(__this, method) ((  void (*) (Collection_1_t3533222586 *, const MethodInfo*))Collection_1_ClearItems_m2153050185_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::Contains(T)
extern "C"  bool Collection_1_Contains_m3526243787_gshared (Collection_1_t3533222586 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Contains_m3526243787(__this, ___item0, method) ((  bool (*) (Collection_1_t3533222586 *, int32_t, const MethodInfo*))Collection_1_Contains_m3526243787_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m85541953_gshared (Collection_1_t3533222586 * __this, RpsChoiceU5BU5D_t3806589061* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m85541953(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3533222586 *, RpsChoiceU5BU5D_t3806589061*, int32_t, const MethodInfo*))Collection_1_CopyTo_m85541953_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m721460386_gshared (Collection_1_t3533222586 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m721460386(__this, method) ((  Il2CppObject* (*) (Collection_1_t3533222586 *, const MethodInfo*))Collection_1_GetEnumerator_m721460386_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m22178253_gshared (Collection_1_t3533222586 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m22178253(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3533222586 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m22178253_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m4178409208_gshared (Collection_1_t3533222586 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_Insert_m4178409208(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3533222586 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m4178409208_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2562746667_gshared (Collection_1_t3533222586 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2562746667(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3533222586 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m2562746667_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m245190809_gshared (Collection_1_t3533222586 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m245190809(__this, method) ((  Il2CppObject* (*) (Collection_1_t3533222586 *, const MethodInfo*))Collection_1_get_Items_m245190809_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::Remove(T)
extern "C"  bool Collection_1_Remove_m3766217862_gshared (Collection_1_t3533222586 * __this, int32_t ___item0, const MethodInfo* method);
#define Collection_1_Remove_m3766217862(__this, ___item0, method) ((  bool (*) (Collection_1_t3533222586 *, int32_t, const MethodInfo*))Collection_1_Remove_m3766217862_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m2052262078_gshared (Collection_1_t3533222586 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m2052262078(__this, ___index0, method) ((  void (*) (Collection_1_t3533222586 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m2052262078_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m3733493086_gshared (Collection_1_t3533222586 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m3733493086(__this, ___index0, method) ((  void (*) (Collection_1_t3533222586 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m3733493086_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m504833358_gshared (Collection_1_t3533222586 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m504833358(__this, method) ((  int32_t (*) (Collection_1_t3533222586 *, const MethodInfo*))Collection_1_get_Count_m504833358_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::get_Item(System.Int32)
extern "C"  int32_t Collection_1_get_Item_m2442362212_gshared (Collection_1_t3533222586 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m2442362212(__this, ___index0, method) ((  int32_t (*) (Collection_1_t3533222586 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2442362212_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m1466423759_gshared (Collection_1_t3533222586 * __this, int32_t ___index0, int32_t ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m1466423759(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3533222586 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m1466423759_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1998256330_gshared (Collection_1_t3533222586 * __this, int32_t ___index0, int32_t ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1998256330(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3533222586 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m1998256330_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2431421697_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2431421697(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2431421697_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::ConvertItem(System.Object)
extern "C"  int32_t Collection_1_ConvertItem_m3393706563_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m3393706563(__this /* static, unused */, ___item0, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m3393706563_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m657478273_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m657478273(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m657478273_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m3566935331_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m3566935331(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m3566935331_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Core.RpsChoice>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m2951607388_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m2951607388(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m2951607388_gshared)(__this /* static, unused */, ___list0, method)
