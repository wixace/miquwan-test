﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// CSBossUnit
struct CSBossUnit_t2214856481;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSBossData
struct  CSBossData_t2214337863  : public Il2CppObject
{
public:
	// CSBossUnit CSBossData::CurBossUnit
	CSBossUnit_t2214856481 * ___CurBossUnit_0;

public:
	inline static int32_t get_offset_of_CurBossUnit_0() { return static_cast<int32_t>(offsetof(CSBossData_t2214337863, ___CurBossUnit_0)); }
	inline CSBossUnit_t2214856481 * get_CurBossUnit_0() const { return ___CurBossUnit_0; }
	inline CSBossUnit_t2214856481 ** get_address_of_CurBossUnit_0() { return &___CurBossUnit_0; }
	inline void set_CurBossUnit_0(CSBossUnit_t2214856481 * value)
	{
		___CurBossUnit_0 = value;
		Il2CppCodeGenWriteBarrier(&___CurBossUnit_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
