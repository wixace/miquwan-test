﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSGuideVO
struct CSGuideVO_t4116883493;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"

// System.Void CSGuideVO::.ctor(System.Int32)
extern "C"  void CSGuideVO__ctor_m52384567 (CSGuideVO_t4116883493 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSGuideVO::.ctor(Newtonsoft.Json.Linq.JToken)
extern "C"  void CSGuideVO__ctor_m912463748 (CSGuideVO_t4116883493 * __this, JToken_t3412245951 * ___jToken0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
