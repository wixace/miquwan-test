﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.TypeModel/DeserializeItemsIterator`1<System.Object>
struct DeserializeItemsIterator_1_t2272739822;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.IO.Stream
struct Stream_t1561764144;
// ProtoBuf.SerializationContext
struct SerializationContext_t3997850667;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_ProtoBuf_PrefixStyle1974492709.h"
#include "AssemblyU2DCSharp_ProtoBuf_SerializationContext3997850667.h"

// System.Void ProtoBuf.Meta.TypeModel/DeserializeItemsIterator`1<System.Object>::.ctor(ProtoBuf.Meta.TypeModel,System.IO.Stream,ProtoBuf.PrefixStyle,System.Int32,ProtoBuf.SerializationContext)
extern "C"  void DeserializeItemsIterator_1__ctor_m4149891648_gshared (DeserializeItemsIterator_1_t2272739822 * __this, TypeModel_t2730011105 * ___model0, Stream_t1561764144 * ___source1, int32_t ___style2, int32_t ___expectedField3, SerializationContext_t3997850667 * ___context4, const MethodInfo* method);
#define DeserializeItemsIterator_1__ctor_m4149891648(__this, ___model0, ___source1, ___style2, ___expectedField3, ___context4, method) ((  void (*) (DeserializeItemsIterator_1_t2272739822 *, TypeModel_t2730011105 *, Stream_t1561764144 *, int32_t, int32_t, SerializationContext_t3997850667 *, const MethodInfo*))DeserializeItemsIterator_1__ctor_m4149891648_gshared)(__this, ___model0, ___source1, ___style2, ___expectedField3, ___context4, method)
// System.Collections.Generic.IEnumerator`1<T> ProtoBuf.Meta.TypeModel/DeserializeItemsIterator`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* DeserializeItemsIterator_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2056884287_gshared (DeserializeItemsIterator_1_t2272739822 * __this, const MethodInfo* method);
#define DeserializeItemsIterator_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2056884287(__this, method) ((  Il2CppObject* (*) (DeserializeItemsIterator_1_t2272739822 *, const MethodInfo*))DeserializeItemsIterator_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2056884287_gshared)(__this, method)
// System.Void ProtoBuf.Meta.TypeModel/DeserializeItemsIterator`1<System.Object>::System.IDisposable.Dispose()
extern "C"  void DeserializeItemsIterator_1_System_IDisposable_Dispose_m171632202_gshared (DeserializeItemsIterator_1_t2272739822 * __this, const MethodInfo* method);
#define DeserializeItemsIterator_1_System_IDisposable_Dispose_m171632202(__this, method) ((  void (*) (DeserializeItemsIterator_1_t2272739822 *, const MethodInfo*))DeserializeItemsIterator_1_System_IDisposable_Dispose_m171632202_gshared)(__this, method)
// T ProtoBuf.Meta.TypeModel/DeserializeItemsIterator`1<System.Object>::get_Current()
extern "C"  Il2CppObject * DeserializeItemsIterator_1_get_Current_m3483484962_gshared (DeserializeItemsIterator_1_t2272739822 * __this, const MethodInfo* method);
#define DeserializeItemsIterator_1_get_Current_m3483484962(__this, method) ((  Il2CppObject * (*) (DeserializeItemsIterator_1_t2272739822 *, const MethodInfo*))DeserializeItemsIterator_1_get_Current_m3483484962_gshared)(__this, method)
