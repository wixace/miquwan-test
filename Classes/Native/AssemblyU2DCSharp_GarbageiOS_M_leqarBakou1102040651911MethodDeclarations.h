﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_leqarBakou110
struct M_leqarBakou110_t2040651911;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_leqarBakou1102040651911.h"

// System.Void GarbageiOS.M_leqarBakou110::.ctor()
extern "C"  void M_leqarBakou110__ctor_m1582701756 (M_leqarBakou110_t2040651911 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_leqarBakou110::M_caleaDoustel0(System.String[],System.Int32)
extern "C"  void M_leqarBakou110_M_caleaDoustel0_m3969944167 (M_leqarBakou110_t2040651911 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_leqarBakou110::M_nawsairlar1(System.String[],System.Int32)
extern "C"  void M_leqarBakou110_M_nawsairlar1_m2152664910 (M_leqarBakou110_t2040651911 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_leqarBakou110::M_wibearduRerekall2(System.String[],System.Int32)
extern "C"  void M_leqarBakou110_M_wibearduRerekall2_m757638430 (M_leqarBakou110_t2040651911 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_leqarBakou110::ilo_M_nawsairlar11(GarbageiOS.M_leqarBakou110,System.String[],System.Int32)
extern "C"  void M_leqarBakou110_ilo_M_nawsairlar11_m2428442189 (Il2CppObject * __this /* static, unused */, M_leqarBakou110_t2040651911 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
