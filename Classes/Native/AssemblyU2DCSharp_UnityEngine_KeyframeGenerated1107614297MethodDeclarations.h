﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_KeyframeGenerated
struct UnityEngine_KeyframeGenerated_t1107614297;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_KeyframeGenerated::.ctor()
extern "C"  void UnityEngine_KeyframeGenerated__ctor_m273959730 (UnityEngine_KeyframeGenerated_t1107614297 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_KeyframeGenerated::.cctor()
extern "C"  void UnityEngine_KeyframeGenerated__cctor_m3715688123 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_KeyframeGenerated::Keyframe_Keyframe1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_KeyframeGenerated_Keyframe_Keyframe1_m3223568320 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_KeyframeGenerated::Keyframe_Keyframe2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_KeyframeGenerated_Keyframe_Keyframe2_m173365505 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_KeyframeGenerated::Keyframe_Keyframe3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_KeyframeGenerated_Keyframe_Keyframe3_m1418129986 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_KeyframeGenerated::Keyframe_time(JSVCall)
extern "C"  void UnityEngine_KeyframeGenerated_Keyframe_time_m1061111321 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_KeyframeGenerated::Keyframe_value(JSVCall)
extern "C"  void UnityEngine_KeyframeGenerated_Keyframe_value_m2624761461 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_KeyframeGenerated::Keyframe_inTangent(JSVCall)
extern "C"  void UnityEngine_KeyframeGenerated_Keyframe_inTangent_m552031078 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_KeyframeGenerated::Keyframe_outTangent(JSVCall)
extern "C"  void UnityEngine_KeyframeGenerated_Keyframe_outTangent_m872621327 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_KeyframeGenerated::Keyframe_tangentMode(JSVCall)
extern "C"  void UnityEngine_KeyframeGenerated_Keyframe_tangentMode_m3684701246 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_KeyframeGenerated::__Register()
extern "C"  void UnityEngine_KeyframeGenerated___Register_m3130417365 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_KeyframeGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_KeyframeGenerated_ilo_addJSCSRel1_m3315708142 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_KeyframeGenerated::ilo_getObject2(System.Int32)
extern "C"  int32_t UnityEngine_KeyframeGenerated_ilo_getObject2_m3222574341 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_KeyframeGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_KeyframeGenerated_ilo_getSingle3_m1286832543 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_KeyframeGenerated::ilo_attachFinalizerObject4(System.Int32)
extern "C"  bool UnityEngine_KeyframeGenerated_ilo_attachFinalizerObject4_m1789063880 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_KeyframeGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void UnityEngine_KeyframeGenerated_ilo_setSingle5_m2398061062 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_KeyframeGenerated::ilo_changeJSObj6(System.Int32,System.Object)
extern "C"  void UnityEngine_KeyframeGenerated_ilo_changeJSObj6_m3905669116 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
