﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<CombatEntity,TargetMgr/FightPoint>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m3184855442(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t152597490 *, CombatEntity_t684137495 *, FightPoint_t4216057832 *, const MethodInfo*))KeyValuePair_2__ctor_m4168265535_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<CombatEntity,TargetMgr/FightPoint>::get_Key()
#define KeyValuePair_2_get_Key_m1301757334(__this, method) ((  CombatEntity_t684137495 * (*) (KeyValuePair_2_t152597490 *, const MethodInfo*))KeyValuePair_2_get_Key_m3256475977_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<CombatEntity,TargetMgr/FightPoint>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m2421890391(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t152597490 *, CombatEntity_t684137495 *, const MethodInfo*))KeyValuePair_2_set_Key_m1278074762_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<CombatEntity,TargetMgr/FightPoint>::get_Value()
#define KeyValuePair_2_get_Value_m2349294394(__this, method) ((  FightPoint_t4216057832 * (*) (KeyValuePair_2_t152597490 *, const MethodInfo*))KeyValuePair_2_get_Value_m3899079597_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<CombatEntity,TargetMgr/FightPoint>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m2091511255(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t152597490 *, FightPoint_t4216057832 *, const MethodInfo*))KeyValuePair_2_set_Value_m2954518154_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<CombatEntity,TargetMgr/FightPoint>::ToString()
#define KeyValuePair_2_ToString_m2951257809(__this, method) ((  String_t* (*) (KeyValuePair_2_t152597490 *, const MethodInfo*))KeyValuePair_2_ToString_m1313859518_gshared)(__this, method)
