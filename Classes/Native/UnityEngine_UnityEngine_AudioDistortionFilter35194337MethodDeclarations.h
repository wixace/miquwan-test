﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AudioDistortionFilter
struct AudioDistortionFilter_t35194337;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.AudioDistortionFilter::.ctor()
extern "C"  void AudioDistortionFilter__ctor_m2109835726 (AudioDistortionFilter_t35194337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AudioDistortionFilter::get_distortionLevel()
extern "C"  float AudioDistortionFilter_get_distortionLevel_m2386806102 (AudioDistortionFilter_t35194337 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioDistortionFilter::set_distortionLevel(System.Single)
extern "C"  void AudioDistortionFilter_set_distortionLevel_m1211616533 (AudioDistortionFilter_t35194337 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
