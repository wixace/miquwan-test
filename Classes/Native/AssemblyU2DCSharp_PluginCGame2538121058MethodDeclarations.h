﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginCGame
struct PluginCGame_t2538121058;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// UnityEngine.WWWForm
struct WWWForm_t461342257;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// VersionInfo
struct VersionInfo_t2356638086;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_Mihua_SDK_PayInfo1775308120.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginCGame2538121058.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "System_Core_System_Action3771233898.h"

// System.Void PluginCGame::.ctor()
extern "C"  void PluginCGame__ctor_m3654025801 (PluginCGame_t2538121058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::Init()
extern "C"  void PluginCGame_Init_m2286165739 (PluginCGame_t2538121058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginCGame::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginCGame_ReqSDKHttpLogin_m2961143930 (PluginCGame_t2538121058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginCGame::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginCGame_IsLoginSuccess_m3327898370 (PluginCGame_t2538121058 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::OpenUserLogin()
extern "C"  void PluginCGame_OpenUserLogin_m1474743195 (PluginCGame_t2538121058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::UserPay(CEvent.ZEvent)
extern "C"  void PluginCGame_UserPay_m1655214967 (PluginCGame_t2538121058 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.WWWForm PluginCGame::BuildOrderParam2WWWForm(Mihua.SDK.PayInfo)
extern "C"  WWWForm_t461342257 * PluginCGame_BuildOrderParam2WWWForm_m802444090 (PluginCGame_t2538121058 * __this, PayInfo_t1775308120 * ___info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::CreateRole(CEvent.ZEvent)
extern "C"  void PluginCGame_CreateRole_m2124693550 (PluginCGame_t2538121058 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::EnterGame(CEvent.ZEvent)
extern "C"  void PluginCGame_EnterGame_m3740230474 (PluginCGame_t2538121058 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginCGame_RoleUpgrade_m4104289134 (PluginCGame_t2538121058 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::OnLoginSuccess(System.String)
extern "C"  void PluginCGame_OnLoginSuccess_m1891708046 (PluginCGame_t2538121058 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::OnLogoutSuccess(System.String)
extern "C"  void PluginCGame_OnLogoutSuccess_m2811917249 (PluginCGame_t2538121058 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::OnLogout(System.String)
extern "C"  void PluginCGame_OnLogout_m1510945438 (PluginCGame_t2538121058 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::SwitchAccount(System.String)
extern "C"  void PluginCGame_SwitchAccount_m1977169794 (PluginCGame_t2538121058 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::initSdk(System.String,System.String)
extern "C"  void PluginCGame_initSdk_m1577343117 (PluginCGame_t2538121058 * __this, String_t* ___appId0, String_t* ___appkey1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::login()
extern "C"  void PluginCGame_login_m3176040624 (PluginCGame_t2538121058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::logout()
extern "C"  void PluginCGame_logout_m3973801541 (PluginCGame_t2538121058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::updateRoleInfo(System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginCGame_updateRoleInfo_m3109914826 (PluginCGame_t2538121058 * __this, String_t* ___roleID0, String_t* ___roleName1, String_t* ___roleLv2, String_t* ___serverID3, String_t* ___serverName4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginCGame_pay_m1224675219 (PluginCGame_t2538121058 * __this, String_t* ___orderID0, String_t* ___price1, String_t* ___roleID2, String_t* ___roleName3, String_t* ___roleLv4, String_t* ___serverId5, String_t* ___serverName6, String_t* ___extra17, String_t* ___extra28, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::<OnLogoutSuccess>m__40E()
extern "C"  void PluginCGame_U3COnLogoutSuccessU3Em__40E_m960705667 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::<OnLogout>m__40F()
extern "C"  void PluginCGame_U3COnLogoutU3Em__40F_m317299665 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::<SwitchAccount>m__410()
extern "C"  void PluginCGame_U3CSwitchAccountU3Em__410_m2587649166 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginCGame_ilo_AddEventListener1_m2746861963 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginCGame::ilo_get_Instance2()
extern "C"  VersionMgr_t1322950208 * PluginCGame_ilo_get_Instance2_m578341029 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginCGame::ilo_get_PluginsSdkMgr3()
extern "C"  PluginsSdkMgr_t3884624670 * PluginCGame_ilo_get_PluginsSdkMgr3_m3957672695 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginCGame::ilo_get_isSdkLogin4(VersionMgr)
extern "C"  bool PluginCGame_ilo_get_isSdkLogin4_m3331631674 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionInfo PluginCGame::ilo_get_currentVS5(VersionMgr)
extern "C"  VersionInfo_t2356638086 * PluginCGame_ilo_get_currentVS5_m4116034697 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::ilo_updateRoleInfo6(PluginCGame,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginCGame_ilo_updateRoleInfo6_m622815487 (Il2CppObject * __this /* static, unused */, PluginCGame_t2538121058 * ____this0, String_t* ___roleID1, String_t* ___roleName2, String_t* ___roleLv3, String_t* ___serverID4, String_t* ___serverName5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::ilo_ReqSDKHttpLogin7(PluginsSdkMgr)
extern "C"  void PluginCGame_ilo_ReqSDKHttpLogin7_m1381413140 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::ilo_Logout8(System.Action)
extern "C"  void PluginCGame_ilo_Logout8_m4017730107 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginCGame::ilo_logout9(PluginCGame)
extern "C"  void PluginCGame_ilo_logout9_m3492799117 (Il2CppObject * __this /* static, unused */, PluginCGame_t2538121058 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
