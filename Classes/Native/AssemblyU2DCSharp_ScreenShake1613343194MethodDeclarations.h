﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScreenShake
struct ScreenShake_t1613343194;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "AssemblyU2DCSharp_ScreenShake1613343194.h"

// System.Void ScreenShake::.ctor()
extern "C"  void ScreenShake__ctor_m2472167121 (ScreenShake_t1613343194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenShake::StartShake(System.Single,System.Single,UnityEngine.Camera)
extern "C"  void ScreenShake_StartShake_m2207068593 (ScreenShake_t1613343194 * __this, float ____fps0, float ____shakeTime1, Camera_t2727095145 * ____cam2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenShake::StartShake(System.Single,System.Single,UnityEngine.Camera,System.Single)
extern "C"  void ScreenShake_StartShake_m3864163030 (ScreenShake_t1613343194 * __this, float ____fps0, float ____shakeTime1, Camera_t2727095145 * ____cam2, float ____shakeDelta3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenShake::Update()
extern "C"  void ScreenShake_Update_m1054631516 (ScreenShake_t1613343194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScreenShake::ilo_StartShake1(ScreenShake,System.Single,System.Single,UnityEngine.Camera,System.Single)
extern "C"  void ScreenShake_ilo_StartShake1_m319335094 (Il2CppObject * __this /* static, unused */, ScreenShake_t1613343194 * ____this0, float ____fps1, float ____shakeTime2, Camera_t2727095145 * ____cam3, float ____shakeDelta4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
