﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TiltShiftHdr
struct TiltShiftHdr_t3131000081;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void TiltShiftHdr::.ctor()
extern "C"  void TiltShiftHdr__ctor_m3150881171 (TiltShiftHdr_t3131000081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TiltShiftHdr::CheckResources()
extern "C"  bool TiltShiftHdr_CheckResources_m4228207092 (TiltShiftHdr_t3131000081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TiltShiftHdr::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void TiltShiftHdr_OnRenderImage_m1299940075 (TiltShiftHdr_t3131000081 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TiltShiftHdr::Main()
extern "C"  void TiltShiftHdr_Main_m155683018 (TiltShiftHdr_t3131000081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
