﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._e3d68d5a9ce77f230dcc97aaf33931a6
struct _e3d68d5a9ce77f230dcc97aaf33931a6_t3540753077;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__e3d68d5a9ce77f230dcc97aa3540753077.h"

// System.Void Little._e3d68d5a9ce77f230dcc97aaf33931a6::.ctor()
extern "C"  void _e3d68d5a9ce77f230dcc97aaf33931a6__ctor_m185714456 (_e3d68d5a9ce77f230dcc97aaf33931a6_t3540753077 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e3d68d5a9ce77f230dcc97aaf33931a6::_e3d68d5a9ce77f230dcc97aaf33931a6m2(System.Int32)
extern "C"  int32_t _e3d68d5a9ce77f230dcc97aaf33931a6__e3d68d5a9ce77f230dcc97aaf33931a6m2_m3813141881 (_e3d68d5a9ce77f230dcc97aaf33931a6_t3540753077 * __this, int32_t ____e3d68d5a9ce77f230dcc97aaf33931a6a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e3d68d5a9ce77f230dcc97aaf33931a6::_e3d68d5a9ce77f230dcc97aaf33931a6m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _e3d68d5a9ce77f230dcc97aaf33931a6__e3d68d5a9ce77f230dcc97aaf33931a6m_m3762410845 (_e3d68d5a9ce77f230dcc97aaf33931a6_t3540753077 * __this, int32_t ____e3d68d5a9ce77f230dcc97aaf33931a6a0, int32_t ____e3d68d5a9ce77f230dcc97aaf33931a6181, int32_t ____e3d68d5a9ce77f230dcc97aaf33931a6c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e3d68d5a9ce77f230dcc97aaf33931a6::ilo__e3d68d5a9ce77f230dcc97aaf33931a6m21(Little._e3d68d5a9ce77f230dcc97aaf33931a6,System.Int32)
extern "C"  int32_t _e3d68d5a9ce77f230dcc97aaf33931a6_ilo__e3d68d5a9ce77f230dcc97aaf33931a6m21_m1674720796 (Il2CppObject * __this /* static, unused */, _e3d68d5a9ce77f230dcc97aaf33931a6_t3540753077 * ____this0, int32_t ____e3d68d5a9ce77f230dcc97aaf33931a6a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
