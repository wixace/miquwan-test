﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UISprite
struct UISprite_t661437049;

#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Float_fight_fuwen
struct  Float_fight_fuwen_t2985897407  : public FloatTextUnit_t2362298029
{
public:
	// UISprite Float_fight_fuwen::Sprite_Text
	UISprite_t661437049 * ___Sprite_Text_3;
	// UISprite Float_fight_fuwen::Sprite_Text02
	UISprite_t661437049 * ___Sprite_Text02_4;

public:
	inline static int32_t get_offset_of_Sprite_Text_3() { return static_cast<int32_t>(offsetof(Float_fight_fuwen_t2985897407, ___Sprite_Text_3)); }
	inline UISprite_t661437049 * get_Sprite_Text_3() const { return ___Sprite_Text_3; }
	inline UISprite_t661437049 ** get_address_of_Sprite_Text_3() { return &___Sprite_Text_3; }
	inline void set_Sprite_Text_3(UISprite_t661437049 * value)
	{
		___Sprite_Text_3 = value;
		Il2CppCodeGenWriteBarrier(&___Sprite_Text_3, value);
	}

	inline static int32_t get_offset_of_Sprite_Text02_4() { return static_cast<int32_t>(offsetof(Float_fight_fuwen_t2985897407, ___Sprite_Text02_4)); }
	inline UISprite_t661437049 * get_Sprite_Text02_4() const { return ___Sprite_Text02_4; }
	inline UISprite_t661437049 ** get_address_of_Sprite_Text02_4() { return &___Sprite_Text02_4; }
	inline void set_Sprite_Text02_4(UISprite_t661437049 * value)
	{
		___Sprite_Text02_4 = value;
		Il2CppCodeGenWriteBarrier(&___Sprite_Text02_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
