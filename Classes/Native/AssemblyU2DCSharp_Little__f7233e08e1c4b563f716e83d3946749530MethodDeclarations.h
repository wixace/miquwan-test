﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._f7233e08e1c4b563f716e83d50b79414
struct _f7233e08e1c4b563f716e83d50b79414_t3946749530;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._f7233e08e1c4b563f716e83d50b79414::.ctor()
extern "C"  void _f7233e08e1c4b563f716e83d50b79414__ctor_m3420982547 (_f7233e08e1c4b563f716e83d50b79414_t3946749530 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f7233e08e1c4b563f716e83d50b79414::_f7233e08e1c4b563f716e83d50b79414m2(System.Int32)
extern "C"  int32_t _f7233e08e1c4b563f716e83d50b79414__f7233e08e1c4b563f716e83d50b79414m2_m4025000793 (_f7233e08e1c4b563f716e83d50b79414_t3946749530 * __this, int32_t ____f7233e08e1c4b563f716e83d50b79414a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f7233e08e1c4b563f716e83d50b79414::_f7233e08e1c4b563f716e83d50b79414m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _f7233e08e1c4b563f716e83d50b79414__f7233e08e1c4b563f716e83d50b79414m_m2374990717 (_f7233e08e1c4b563f716e83d50b79414_t3946749530 * __this, int32_t ____f7233e08e1c4b563f716e83d50b79414a0, int32_t ____f7233e08e1c4b563f716e83d50b7941471, int32_t ____f7233e08e1c4b563f716e83d50b79414c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
