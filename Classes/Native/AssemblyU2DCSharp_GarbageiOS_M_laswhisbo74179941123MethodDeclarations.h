﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_laswhisbo74
struct M_laswhisbo74_t179941123;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_laswhisbo74::.ctor()
extern "C"  void M_laswhisbo74__ctor_m492961728 (M_laswhisbo74_t179941123 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_laswhisbo74::M_najerelas0(System.String[],System.Int32)
extern "C"  void M_laswhisbo74_M_najerelas0_m1074523840 (M_laswhisbo74_t179941123 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
