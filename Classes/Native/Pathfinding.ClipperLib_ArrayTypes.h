﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// Pathfinding.ClipperLib.PolyNode
struct PolyNode_t3336253808;
// Pathfinding.ClipperLib.TEdge
struct TEdge_t3950806139;
// Pathfinding.ClipperLib.OutRec
struct OutRec_t3245805284;
// Pathfinding.ClipperLib.Join
struct Join_t3970117804;

#include "mscorlib_System_Array1146569071.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Poly3336253808.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_TEdg3950806139.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_OutR3245805284.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Join3970117804.h"

#pragma once
// Pathfinding.ClipperLib.PolyNode[]
struct PolyNodeU5BU5D_t1512843985  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) PolyNode_t3336253808 * m_Items[1];

public:
	inline PolyNode_t3336253808 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline PolyNode_t3336253808 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, PolyNode_t3336253808 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.ClipperLib.IntPoint[]
struct IntPointU5BU5D_t3364663986  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) IntPoint_t3326126179  m_Items[1];

public:
	inline IntPoint_t3326126179  GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline IntPoint_t3326126179 * GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, IntPoint_t3326126179  value)
	{
		m_Items[index] = value;
	}
};
// Pathfinding.ClipperLib.TEdge[]
struct TEdgeU5BU5D_t326708026  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) TEdge_t3950806139 * m_Items[1];

public:
	inline TEdge_t3950806139 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline TEdge_t3950806139 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, TEdge_t3950806139 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.ClipperLib.OutRec[]
struct OutRecU5BU5D_t1391330573  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) OutRec_t3245805284 * m_Items[1];

public:
	inline OutRec_t3245805284 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline OutRec_t3245805284 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, OutRec_t3245805284 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// Pathfinding.ClipperLib.Join[]
struct JoinU5BU5D_t207584037  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Join_t3970117804 * m_Items[1];

public:
	inline Join_t3970117804 * GetAt(il2cpp_array_size_t index) const { return m_Items[index]; }
	inline Join_t3970117804 ** GetAddressAt(il2cpp_array_size_t index) { return m_Items + index; }
	inline void SetAt(il2cpp_array_size_t index, Join_t3970117804 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
