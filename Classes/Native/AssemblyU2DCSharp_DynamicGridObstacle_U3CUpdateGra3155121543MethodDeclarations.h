﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DynamicGridObstacle/<UpdateGraphs>c__Iterator16
struct U3CUpdateGraphsU3Ec__Iterator16_t3155121543;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void DynamicGridObstacle/<UpdateGraphs>c__Iterator16::.ctor()
extern "C"  void U3CUpdateGraphsU3Ec__Iterator16__ctor_m2202868164 (U3CUpdateGraphsU3Ec__Iterator16_t3155121543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DynamicGridObstacle/<UpdateGraphs>c__Iterator16::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CUpdateGraphsU3Ec__Iterator16_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m829109774 (U3CUpdateGraphsU3Ec__Iterator16_t3155121543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DynamicGridObstacle/<UpdateGraphs>c__Iterator16::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CUpdateGraphsU3Ec__Iterator16_System_Collections_IEnumerator_get_Current_m3719763362 (U3CUpdateGraphsU3Ec__Iterator16_t3155121543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DynamicGridObstacle/<UpdateGraphs>c__Iterator16::MoveNext()
extern "C"  bool U3CUpdateGraphsU3Ec__Iterator16_MoveNext_m2846254512 (U3CUpdateGraphsU3Ec__Iterator16_t3155121543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicGridObstacle/<UpdateGraphs>c__Iterator16::Dispose()
extern "C"  void U3CUpdateGraphsU3Ec__Iterator16_Dispose_m3730679041 (U3CUpdateGraphsU3Ec__Iterator16_t3155121543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicGridObstacle/<UpdateGraphs>c__Iterator16::Reset()
extern "C"  void U3CUpdateGraphsU3Ec__Iterator16_Reset_m4144268401 (U3CUpdateGraphsU3Ec__Iterator16_t3155121543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
