﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Type
struct Type_t;
// System.String
struct String_t;
// System.Type[]
struct TypeU5BU5D_t3339007067;
// System.Collections.Generic.IEnumerable`1<System.Type>
struct IEnumerable_1_t1869091435;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>
struct IEnumerable_1_t3619649022;
// System.Reflection.ParameterInfo
struct ParameterInfo_t2235474049;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "mscorlib_System_Reflection_ParameterInfo2235474049.h"

// System.Reflection.MethodInfo Newtonsoft.Json.Utilities.TypeExtensions::GetGenericMethod(System.Type,System.String,System.Type[])
extern "C"  MethodInfo_t * TypeExtensions_GetGenericMethod_m2367586794 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, String_t* ___name1, TypeU5BU5D_t3339007067* ___parameterTypes2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.TypeExtensions::HasParameters(System.Reflection.MethodInfo,System.Type[])
extern "C"  bool TypeExtensions_HasParameters_m3020331584 (Il2CppObject * __this /* static, unused */, MethodInfo_t * ___method0, TypeU5BU5D_t3339007067* ___parameterTypes1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Type> Newtonsoft.Json.Utilities.TypeExtensions::AllInterfaces(System.Type)
extern "C"  Il2CppObject* TypeExtensions_AllInterfaces_m4015172817 (Il2CppObject * __this /* static, unused */, Type_t * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo> Newtonsoft.Json.Utilities.TypeExtensions::AllMethods(System.Type)
extern "C"  Il2CppObject* TypeExtensions_AllMethods_m271055485 (Il2CppObject * __this /* static, unused */, Type_t * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Utilities.TypeExtensions::<HasParameters>m__39F(System.Reflection.ParameterInfo)
extern "C"  Type_t * TypeExtensions_U3CHasParametersU3Em__39F_m689768702 (Il2CppObject * __this /* static, unused */, ParameterInfo_t2235474049 * ___parameter0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo> Newtonsoft.Json.Utilities.TypeExtensions::<AllMethods>m__3A0(System.Type)
extern "C"  Il2CppObject* TypeExtensions_U3CAllMethodsU3Em__3A0_m152482120 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo Newtonsoft.Json.Utilities.TypeExtensions::<AllMethods>m__3A1(System.Type,System.Reflection.MethodInfo)
extern "C"  MethodInfo_t * TypeExtensions_U3CAllMethodsU3Em__3A1_m3867103547 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MethodInfo_t * ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Type> Newtonsoft.Json.Utilities.TypeExtensions::ilo_AllInterfaces1(System.Type)
extern "C"  Il2CppObject* TypeExtensions_ilo_AllInterfaces1_m92319067 (Il2CppObject * __this /* static, unused */, Type_t * ___target0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
