﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// asset_sharedCfg
struct asset_sharedCfg_t1901313840;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void asset_sharedCfg::.ctor()
extern "C"  void asset_sharedCfg__ctor_m3344965179 (asset_sharedCfg_t1901313840 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void asset_sharedCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void asset_sharedCfg_Init_m495530858 (asset_sharedCfg_t1901313840 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
