﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_BoneWeightGenerated
struct UnityEngine_BoneWeightGenerated_t1865500715;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_BoneWeightGenerated::.ctor()
extern "C"  void UnityEngine_BoneWeightGenerated__ctor_m3186798496 (UnityEngine_BoneWeightGenerated_t1865500715 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoneWeightGenerated::.cctor()
extern "C"  void UnityEngine_BoneWeightGenerated__cctor_m3819376653 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoneWeightGenerated::BoneWeight_BoneWeight1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoneWeightGenerated_BoneWeight_BoneWeight1_m1116168082 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoneWeightGenerated::BoneWeight_weight0(JSVCall)
extern "C"  void UnityEngine_BoneWeightGenerated_BoneWeight_weight0_m3199655566 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoneWeightGenerated::BoneWeight_weight1(JSVCall)
extern "C"  void UnityEngine_BoneWeightGenerated_BoneWeight_weight1_m3003142061 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoneWeightGenerated::BoneWeight_weight2(JSVCall)
extern "C"  void UnityEngine_BoneWeightGenerated_BoneWeight_weight2_m2806628556 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoneWeightGenerated::BoneWeight_weight3(JSVCall)
extern "C"  void UnityEngine_BoneWeightGenerated_BoneWeight_weight3_m2610115051 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoneWeightGenerated::BoneWeight_boneIndex0(JSVCall)
extern "C"  void UnityEngine_BoneWeightGenerated_BoneWeight_boneIndex0_m3731481476 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoneWeightGenerated::BoneWeight_boneIndex1(JSVCall)
extern "C"  void UnityEngine_BoneWeightGenerated_BoneWeight_boneIndex1_m3534967971 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoneWeightGenerated::BoneWeight_boneIndex2(JSVCall)
extern "C"  void UnityEngine_BoneWeightGenerated_BoneWeight_boneIndex2_m3338454466 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoneWeightGenerated::BoneWeight_boneIndex3(JSVCall)
extern "C"  void UnityEngine_BoneWeightGenerated_BoneWeight_boneIndex3_m3141940961 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoneWeightGenerated::BoneWeight_Equals__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoneWeightGenerated_BoneWeight_Equals__Object_m4099822627 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoneWeightGenerated::BoneWeight_GetHashCode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoneWeightGenerated_BoneWeight_GetHashCode_m196718286 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoneWeightGenerated::BoneWeight_op_Equality__BoneWeight__BoneWeight(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoneWeightGenerated_BoneWeight_op_Equality__BoneWeight__BoneWeight_m2653308045 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_BoneWeightGenerated::BoneWeight_op_Inequality__BoneWeight__BoneWeight(JSVCall,System.Int32)
extern "C"  bool UnityEngine_BoneWeightGenerated_BoneWeight_op_Inequality__BoneWeight__BoneWeight_m892025202 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoneWeightGenerated::__Register()
extern "C"  void UnityEngine_BoneWeightGenerated___Register_m1074499879 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_BoneWeightGenerated::ilo_getSingle1(System.Int32)
extern "C"  float UnityEngine_BoneWeightGenerated_ilo_getSingle1_m1995234415 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoneWeightGenerated::ilo_setSingle2(System.Int32,System.Single)
extern "C"  void UnityEngine_BoneWeightGenerated_ilo_setSingle2_m3050383701 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoneWeightGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void UnityEngine_BoneWeightGenerated_ilo_setInt323_m1351313140 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_BoneWeightGenerated::ilo_getInt324(System.Int32)
extern "C"  int32_t UnityEngine_BoneWeightGenerated_ilo_getInt324_m3338907398 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_BoneWeightGenerated::ilo_getWhatever5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_BoneWeightGenerated_ilo_getWhatever5_m4157014854 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_BoneWeightGenerated::ilo_changeJSObj6(System.Int32,System.Object)
extern "C"  void UnityEngine_BoneWeightGenerated_ilo_changeJSObj6_m479932494 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_BoneWeightGenerated::ilo_getObject7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_BoneWeightGenerated_ilo_getObject7_m936476747 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
