﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSRobData
struct CSRobData_t815326335;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void CSRobData::.ctor()
extern "C"  void CSRobData__ctor_m280088524 (CSRobData_t815326335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSRobData::LoadData(System.String)
extern "C"  void CSRobData_LoadData_m1148020186 (CSRobData_t815326335 * __this, String_t* ___jsonStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSRobData::UnLoadData()
extern "C"  void CSRobData_UnLoadData_m1732261857 (CSRobData_t815326335 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
