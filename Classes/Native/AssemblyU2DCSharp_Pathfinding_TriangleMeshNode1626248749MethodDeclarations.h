﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.TriangleMeshNode
struct TriangleMeshNode_t1626248749;
// AstarPath
struct AstarPath_t4090270936;
// Pathfinding.INavmeshHolder
struct INavmeshHolder_t1019551337;
// Pathfinding.Path
struct Path_t1974241691;
// Pathfinding.PathNode
struct PathNode_t417131581;
// Pathfinding.PathHandler
struct PathHandler_t918952263;
// Pathfinding.GraphNode
struct GraphNode_t23612370;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// Pathfinding.Serialization.GraphSerializationContext
struct GraphSerializationContext_t3256954663;
// Pathfinding.NodeLink3Node
struct NodeLink3Node_t1065608603;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"
#include "AssemblyU2DCSharp_Pathfinding_PathHandler918952263.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_Serialization_GraphS3256954663.h"
#include "AssemblyU2DCSharp_Pathfinding_TriangleMeshNode1626248749.h"
#include "AssemblyU2DCSharp_Pathfinding_NodeLink3Node1065608603.h"

// System.Void Pathfinding.TriangleMeshNode::.ctor(AstarPath)
extern "C"  void TriangleMeshNode__ctor_m1224193728 (TriangleMeshNode_t1626248749 * __this, AstarPath_t4090270936 * ___astar0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TriangleMeshNode::.cctor()
extern "C"  void TriangleMeshNode__cctor_m3871891475 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.INavmeshHolder Pathfinding.TriangleMeshNode::GetNavmeshHolder(System.UInt32)
extern "C"  Il2CppObject * TriangleMeshNode_GetNavmeshHolder_m2714066730 (Il2CppObject * __this /* static, unused */, uint32_t ___graphIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TriangleMeshNode::SetNavmeshHolder(System.Int32,Pathfinding.INavmeshHolder)
extern "C"  void TriangleMeshNode_SetNavmeshHolder_m194817812 (Il2CppObject * __this /* static, unused */, int32_t ___graphIndex0, Il2CppObject * ___graph1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TriangleMeshNode::UpdatePositionFromVertices()
extern "C"  void TriangleMeshNode_UpdatePositionFromVertices_m2583905567 (TriangleMeshNode_t1626248749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.TriangleMeshNode::GetVertexIndex(System.Int32)
extern "C"  int32_t TriangleMeshNode_GetVertexIndex_m1543174433 (TriangleMeshNode_t1626248749 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.TriangleMeshNode::GetVertexArrayIndex(System.Int32)
extern "C"  int32_t TriangleMeshNode_GetVertexArrayIndex_m3654786606 (TriangleMeshNode_t1626248749 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.TriangleMeshNode::GetVertex(System.Int32)
extern "C"  Int3_t1974045594  TriangleMeshNode_GetVertex_m1703326996 (TriangleMeshNode_t1626248749 * __this, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.TriangleMeshNode::GetVertexCount()
extern "C"  int32_t TriangleMeshNode_GetVertexCount_m3491465421 (TriangleMeshNode_t1626248749 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.TriangleMeshNode::ClosestPointOnNode(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  TriangleMeshNode_ClosestPointOnNode_m1674333149 (TriangleMeshNode_t1626248749 * __this, Vector3_t4282066566  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.TriangleMeshNode::ClosestPointOnNodeXZ(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  TriangleMeshNode_ClosestPointOnNodeXZ_m1420591323 (TriangleMeshNode_t1626248749 * __this, Vector3_t4282066566  ____p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.TriangleMeshNode::ContainsPoint(Pathfinding.Int3)
extern "C"  bool TriangleMeshNode_ContainsPoint_m1166966951 (TriangleMeshNode_t1626248749 * __this, Int3_t1974045594  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TriangleMeshNode::UpdateRecursiveG(Pathfinding.Path,Pathfinding.PathNode,Pathfinding.PathHandler)
extern "C"  void TriangleMeshNode_UpdateRecursiveG_m925150637 (TriangleMeshNode_t1626248749 * __this, Path_t1974241691 * ___path0, PathNode_t417131581 * ___pathNode1, PathHandler_t918952263 * ___handler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TriangleMeshNode::Open(Pathfinding.Path,Pathfinding.PathNode,Pathfinding.PathHandler)
extern "C"  void TriangleMeshNode_Open_m3967449185 (TriangleMeshNode_t1626248749 * __this, Path_t1974241691 * ___path0, PathNode_t417131581 * ___pathNode1, PathHandler_t918952263 * ___handler2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.TriangleMeshNode::SharedEdge(Pathfinding.GraphNode)
extern "C"  int32_t TriangleMeshNode_SharedEdge_m2694339314 (TriangleMeshNode_t1626248749 * __this, GraphNode_t23612370 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.TriangleMeshNode::GetPortal(Pathfinding.GraphNode,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Boolean)
extern "C"  bool TriangleMeshNode_GetPortal_m1193990423 (TriangleMeshNode_t1626248749 * __this, GraphNode_t23612370 * ____other0, List_1_t1355284822 * ___left1, List_1_t1355284822 * ___right2, bool ___backwards3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.TriangleMeshNode::GetPortal(Pathfinding.GraphNode,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Boolean,System.Int32&,System.Int32&)
extern "C"  bool TriangleMeshNode_GetPortal_m1408926593 (TriangleMeshNode_t1626248749 * __this, GraphNode_t23612370 * ____other0, List_1_t1355284822 * ___left1, List_1_t1355284822 * ___right2, bool ___backwards3, int32_t* ___aIndex4, int32_t* ___bIndex5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TriangleMeshNode::SerializeNode(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void TriangleMeshNode_SerializeNode_m521791187 (TriangleMeshNode_t1626248749 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TriangleMeshNode::DeserializeNode(Pathfinding.Serialization.GraphSerializationContext)
extern "C"  void TriangleMeshNode_DeserializeNode_m3225176276 (TriangleMeshNode_t1626248749 * __this, GraphSerializationContext_t3256954663 * ___ctx0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.INavmeshHolder Pathfinding.TriangleMeshNode::ilo_GetNavmeshHolder1(System.UInt32)
extern "C"  Il2CppObject * TriangleMeshNode_ilo_GetNavmeshHolder1_m1143094154 (Il2CppObject * __this /* static, unused */, uint32_t ___graphIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.TriangleMeshNode::ilo_op_Addition2(Pathfinding.Int3,Pathfinding.Int3)
extern "C"  Int3_t1974045594  TriangleMeshNode_ilo_op_Addition2_m1426335202 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lhs0, Int3_t1974045594  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.TriangleMeshNode::ilo_GetVertex3(Pathfinding.INavmeshHolder,System.Int32)
extern "C"  Int3_t1974045594  TriangleMeshNode_ilo_GetVertex3_m1551469359 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.TriangleMeshNode::ilo_get_GraphIndex4(Pathfinding.GraphNode)
extern "C"  uint32_t TriangleMeshNode_ilo_get_GraphIndex4_m4028458555 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Pathfinding.TriangleMeshNode::ilo_NearestPointFactor5(Pathfinding.Int3,Pathfinding.Int3,Pathfinding.Int3)
extern "C"  float TriangleMeshNode_ilo_NearestPointFactor5_m1337800671 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___lineStart0, Int3_t1974045594  ___lineEnd1, Int3_t1974045594  ___point2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.TriangleMeshNode::ilo_get_flag26(Pathfinding.PathNode)
extern "C"  bool TriangleMeshNode_ilo_get_flag26_m887931737 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.TriangleMeshNode::ilo_GetConnectionSpecialCost7(Pathfinding.Path,Pathfinding.GraphNode,Pathfinding.GraphNode,System.UInt32)
extern "C"  uint32_t TriangleMeshNode_ilo_GetConnectionSpecialCost7_m503267776 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, GraphNode_t23612370 * ___a1, GraphNode_t23612370 * ___b2, uint32_t ___currentCost3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 Pathfinding.TriangleMeshNode::ilo_get_PathID8(Pathfinding.PathHandler)
extern "C"  uint16_t TriangleMeshNode_ilo_get_PathID8_m3926501432 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.TriangleMeshNode::ilo_CalculateHScore9(Pathfinding.Path,Pathfinding.GraphNode)
extern "C"  uint32_t TriangleMeshNode_ilo_CalculateHScore9_m3232557400 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TriangleMeshNode::ilo_PushNode10(Pathfinding.PathHandler,Pathfinding.PathNode)
extern "C"  void TriangleMeshNode_ilo_PushNode10_m1014150134 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, PathNode_t417131581 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TriangleMeshNode::ilo_UpdateRecursiveG11(Pathfinding.TriangleMeshNode,Pathfinding.Path,Pathfinding.PathNode,Pathfinding.PathHandler)
extern "C"  void TriangleMeshNode_ilo_UpdateRecursiveG11_m561073033 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ____this0, Path_t1974241691 * ___path1, PathNode_t417131581 * ___pathNode2, PathHandler_t918952263 * ___handler3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.TriangleMeshNode::ilo_GetPortal12(Pathfinding.TriangleMeshNode,Pathfinding.GraphNode,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Boolean,System.Int32&,System.Int32&)
extern "C"  bool TriangleMeshNode_ilo_GetPortal12_m2138990798 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ____this0, GraphNode_t23612370 * ____other1, List_1_t1355284822 * ___left2, List_1_t1355284822 * ___right3, bool ___backwards4, int32_t* ___aIndex5, int32_t* ___bIndex6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Pathfinding.TriangleMeshNode::ilo_get_GraphIndex13(Pathfinding.GraphNode)
extern "C"  uint32_t TriangleMeshNode_ilo_get_GraphIndex13_m1570875461 (Il2CppObject * __this /* static, unused */, GraphNode_t23612370 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.TriangleMeshNode::ilo_GetVertexIndex14(Pathfinding.TriangleMeshNode,System.Int32)
extern "C"  int32_t TriangleMeshNode_ilo_GetVertexIndex14_m3290184146 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ____this0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.TriangleMeshNode::ilo_GetVertexIndex15(Pathfinding.TriangleMeshNode,System.Int32)
extern "C"  int32_t TriangleMeshNode_ilo_GetVertexIndex15_m2265490225 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ____this0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.TriangleMeshNode::ilo_GetPortal16(Pathfinding.NodeLink3Node,Pathfinding.GraphNode,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Boolean)
extern "C"  bool TriangleMeshNode_ilo_GetPortal16_m3353912780 (Il2CppObject * __this /* static, unused */, NodeLink3Node_t1065608603 * ____this0, GraphNode_t23612370 * ___other1, List_1_t1355284822 * ___left2, List_1_t1355284822 * ___right3, bool ___backwards4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Int3 Pathfinding.TriangleMeshNode::ilo_GetVertex17(Pathfinding.TriangleMeshNode,System.Int32)
extern "C"  Int3_t1974045594  TriangleMeshNode_ilo_GetVertex17_m609656866 (Il2CppObject * __this /* static, unused */, TriangleMeshNode_t1626248749 * ____this0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.TriangleMeshNode::ilo_get_Item18(Pathfinding.Int3&,System.Int32)
extern "C"  int32_t TriangleMeshNode_ilo_get_Item18_m2308251115 (Il2CppObject * __this /* static, unused */, Int3_t1974045594 * ____this0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.TriangleMeshNode::ilo_set_Item19(Pathfinding.Int3&,System.Int32,System.Int32)
extern "C"  void TriangleMeshNode_ilo_set_Item19_m1105569733 (Il2CppObject * __this /* static, unused */, Int3_t1974045594 * ____this0, int32_t ___i1, int32_t ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.TriangleMeshNode::ilo_op_Explicit20(Pathfinding.Int3)
extern "C"  Vector3_t4282066566  TriangleMeshNode_ilo_op_Explicit20_m1841937645 (Il2CppObject * __this /* static, unused */, Int3_t1974045594  ___ob0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.GraphNode Pathfinding.TriangleMeshNode::ilo_GetOther21(Pathfinding.NodeLink3Node,Pathfinding.GraphNode)
extern "C"  GraphNode_t23612370 * TriangleMeshNode_ilo_GetOther21_m1799825286 (Il2CppObject * __this /* static, unused */, NodeLink3Node_t1065608603 * ____this0, GraphNode_t23612370 * ___a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
