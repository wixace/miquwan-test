﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._ff5ce3edb3281388ad32295d634258da
struct _ff5ce3edb3281388ad32295d634258da_t3437101216;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__ff5ce3edb3281388ad32295d3437101216.h"

// System.Void Little._ff5ce3edb3281388ad32295d634258da::.ctor()
extern "C"  void _ff5ce3edb3281388ad32295d634258da__ctor_m915134861 (_ff5ce3edb3281388ad32295d634258da_t3437101216 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._ff5ce3edb3281388ad32295d634258da::_ff5ce3edb3281388ad32295d634258dam2(System.Int32)
extern "C"  int32_t _ff5ce3edb3281388ad32295d634258da__ff5ce3edb3281388ad32295d634258dam2_m2638892441 (_ff5ce3edb3281388ad32295d634258da_t3437101216 * __this, int32_t ____ff5ce3edb3281388ad32295d634258daa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._ff5ce3edb3281388ad32295d634258da::_ff5ce3edb3281388ad32295d634258dam(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _ff5ce3edb3281388ad32295d634258da__ff5ce3edb3281388ad32295d634258dam_m3335903549 (_ff5ce3edb3281388ad32295d634258da_t3437101216 * __this, int32_t ____ff5ce3edb3281388ad32295d634258daa0, int32_t ____ff5ce3edb3281388ad32295d634258da881, int32_t ____ff5ce3edb3281388ad32295d634258dac2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._ff5ce3edb3281388ad32295d634258da::ilo__ff5ce3edb3281388ad32295d634258dam21(Little._ff5ce3edb3281388ad32295d634258da,System.Int32)
extern "C"  int32_t _ff5ce3edb3281388ad32295d634258da_ilo__ff5ce3edb3281388ad32295d634258dam21_m2908265063 (Il2CppObject * __this /* static, unused */, _ff5ce3edb3281388ad32295d634258da_t3437101216 * ____this0, int32_t ____ff5ce3edb3281388ad32295d634258daa1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
