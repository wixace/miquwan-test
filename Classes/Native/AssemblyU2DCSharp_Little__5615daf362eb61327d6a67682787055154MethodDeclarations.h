﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._5615daf362eb61327d6a676803f77b63
struct _5615daf362eb61327d6a676803f77b63_t2787055154;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._5615daf362eb61327d6a676803f77b63::.ctor()
extern "C"  void _5615daf362eb61327d6a676803f77b63__ctor_m2392123451 (_5615daf362eb61327d6a676803f77b63_t2787055154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._5615daf362eb61327d6a676803f77b63::_5615daf362eb61327d6a676803f77b63m2(System.Int32)
extern "C"  int32_t _5615daf362eb61327d6a676803f77b63__5615daf362eb61327d6a676803f77b63m2_m1743967321 (_5615daf362eb61327d6a676803f77b63_t2787055154 * __this, int32_t ____5615daf362eb61327d6a676803f77b63a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._5615daf362eb61327d6a676803f77b63::_5615daf362eb61327d6a676803f77b63m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _5615daf362eb61327d6a676803f77b63__5615daf362eb61327d6a676803f77b63m_m3987630717 (_5615daf362eb61327d6a676803f77b63_t2787055154 * __this, int32_t ____5615daf362eb61327d6a676803f77b63a0, int32_t ____5615daf362eb61327d6a676803f77b63661, int32_t ____5615daf362eb61327d6a676803f77b63c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
