﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Interpolate/<NewCatmullRom>c__Iterator25`1<UnityEngine.Vector3>
struct U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t1898964319;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void Interpolate/<NewCatmullRom>c__Iterator25`1<UnityEngine.Vector3>::.ctor()
extern "C"  void U3CNewCatmullRomU3Ec__Iterator25_1__ctor_m2684839573_gshared (U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686 * __this, const MethodInfo* method);
#define U3CNewCatmullRomU3Ec__Iterator25_1__ctor_m2684839573(__this, method) ((  void (*) (U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686 *, const MethodInfo*))U3CNewCatmullRomU3Ec__Iterator25_1__ctor_m2684839573_gshared)(__this, method)
// UnityEngine.Vector3 Interpolate/<NewCatmullRom>c__Iterator25`1<UnityEngine.Vector3>::System.Collections.Generic.IEnumerator<UnityEngine.Vector3>.get_Current()
extern "C"  Vector3_t4282066566  U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_Generic_IEnumeratorU3CUnityEngine_Vector3U3E_get_Current_m3961429906_gshared (U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686 * __this, const MethodInfo* method);
#define U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_Generic_IEnumeratorU3CUnityEngine_Vector3U3E_get_Current_m3961429906(__this, method) ((  Vector3_t4282066566  (*) (U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686 *, const MethodInfo*))U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_Generic_IEnumeratorU3CUnityEngine_Vector3U3E_get_Current_m3961429906_gshared)(__this, method)
// System.Object Interpolate/<NewCatmullRom>c__Iterator25`1<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_IEnumerator_get_Current_m981987697_gshared (U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686 * __this, const MethodInfo* method);
#define U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_IEnumerator_get_Current_m981987697(__this, method) ((  Il2CppObject * (*) (U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686 *, const MethodInfo*))U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_IEnumerator_get_Current_m981987697_gshared)(__this, method)
// System.Collections.IEnumerator Interpolate/<NewCatmullRom>c__Iterator25`1<UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_IEnumerable_GetEnumerator_m3959994156_gshared (U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686 * __this, const MethodInfo* method);
#define U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_IEnumerable_GetEnumerator_m3959994156(__this, method) ((  Il2CppObject * (*) (U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686 *, const MethodInfo*))U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_IEnumerable_GetEnumerator_m3959994156_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3> Interpolate/<NewCatmullRom>c__Iterator25`1<UnityEngine.Vector3>::System.Collections.Generic.IEnumerable<UnityEngine.Vector3>.GetEnumerator()
extern "C"  Il2CppObject* U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_Generic_IEnumerableU3CUnityEngine_Vector3U3E_GetEnumerator_m2874550719_gshared (U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686 * __this, const MethodInfo* method);
#define U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_Generic_IEnumerableU3CUnityEngine_Vector3U3E_GetEnumerator_m2874550719(__this, method) ((  Il2CppObject* (*) (U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686 *, const MethodInfo*))U3CNewCatmullRomU3Ec__Iterator25_1_System_Collections_Generic_IEnumerableU3CUnityEngine_Vector3U3E_GetEnumerator_m2874550719_gshared)(__this, method)
// System.Boolean Interpolate/<NewCatmullRom>c__Iterator25`1<UnityEngine.Vector3>::MoveNext()
extern "C"  bool U3CNewCatmullRomU3Ec__Iterator25_1_MoveNext_m3260318527_gshared (U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686 * __this, const MethodInfo* method);
#define U3CNewCatmullRomU3Ec__Iterator25_1_MoveNext_m3260318527(__this, method) ((  bool (*) (U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686 *, const MethodInfo*))U3CNewCatmullRomU3Ec__Iterator25_1_MoveNext_m3260318527_gshared)(__this, method)
// System.Void Interpolate/<NewCatmullRom>c__Iterator25`1<UnityEngine.Vector3>::Dispose()
extern "C"  void U3CNewCatmullRomU3Ec__Iterator25_1_Dispose_m3048735122_gshared (U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686 * __this, const MethodInfo* method);
#define U3CNewCatmullRomU3Ec__Iterator25_1_Dispose_m3048735122(__this, method) ((  void (*) (U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686 *, const MethodInfo*))U3CNewCatmullRomU3Ec__Iterator25_1_Dispose_m3048735122_gshared)(__this, method)
// System.Void Interpolate/<NewCatmullRom>c__Iterator25`1<UnityEngine.Vector3>::Reset()
extern "C"  void U3CNewCatmullRomU3Ec__Iterator25_1_Reset_m331272514_gshared (U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686 * __this, const MethodInfo* method);
#define U3CNewCatmullRomU3Ec__Iterator25_1_Reset_m331272514(__this, method) ((  void (*) (U3CNewCatmullRomU3Ec__Iterator25_1_t2530530686 *, const MethodInfo*))U3CNewCatmullRomU3Ec__Iterator25_1_Reset_m331272514_gshared)(__this, method)
