﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_TrueForAll_GetDelegate_member45_arg0>c__AnonStorey97`1<System.Object>
struct U3CListA1_TrueForAll_GetDelegate_member45_arg0U3Ec__AnonStorey97_1_t3358868182;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_TrueForAll_GetDelegate_member45_arg0>c__AnonStorey97`1<System.Object>::.ctor()
extern "C"  void U3CListA1_TrueForAll_GetDelegate_member45_arg0U3Ec__AnonStorey97_1__ctor_m1181171917_gshared (U3CListA1_TrueForAll_GetDelegate_member45_arg0U3Ec__AnonStorey97_1_t3358868182 * __this, const MethodInfo* method);
#define U3CListA1_TrueForAll_GetDelegate_member45_arg0U3Ec__AnonStorey97_1__ctor_m1181171917(__this, method) ((  void (*) (U3CListA1_TrueForAll_GetDelegate_member45_arg0U3Ec__AnonStorey97_1_t3358868182 *, const MethodInfo*))U3CListA1_TrueForAll_GetDelegate_member45_arg0U3Ec__AnonStorey97_1__ctor_m1181171917_gshared)(__this, method)
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_TrueForAll_GetDelegate_member45_arg0>c__AnonStorey97`1<System.Object>::<>m__BC(T)
extern "C"  bool U3CListA1_TrueForAll_GetDelegate_member45_arg0U3Ec__AnonStorey97_1_U3CU3Em__BC_m2034572443_gshared (U3CListA1_TrueForAll_GetDelegate_member45_arg0U3Ec__AnonStorey97_1_t3358868182 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CListA1_TrueForAll_GetDelegate_member45_arg0U3Ec__AnonStorey97_1_U3CU3Em__BC_m2034572443(__this, ___obj0, method) ((  bool (*) (U3CListA1_TrueForAll_GetDelegate_member45_arg0U3Ec__AnonStorey97_1_t3358868182 *, Il2CppObject *, const MethodInfo*))U3CListA1_TrueForAll_GetDelegate_member45_arg0U3Ec__AnonStorey97_1_U3CU3Em__BC_m2034572443_gshared)(__this, ___obj0, method)
