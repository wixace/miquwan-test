﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<AIEnum.EAIEventtype,System.Object>
struct Dictionary_2_t3630855731;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1562689139.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<AIEnum.EAIEventtype,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m3495942895_gshared (Enumerator_t1562689139 * __this, Dictionary_2_t3630855731 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m3495942895(__this, ___host0, method) ((  void (*) (Enumerator_t1562689139 *, Dictionary_2_t3630855731 *, const MethodInfo*))Enumerator__ctor_m3495942895_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<AIEnum.EAIEventtype,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1981322578_gshared (Enumerator_t1562689139 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1981322578(__this, method) ((  Il2CppObject * (*) (Enumerator_t1562689139 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1981322578_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<AIEnum.EAIEventtype,System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3884572262_gshared (Enumerator_t1562689139 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3884572262(__this, method) ((  void (*) (Enumerator_t1562689139 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3884572262_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<AIEnum.EAIEventtype,System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3421457233_gshared (Enumerator_t1562689139 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3421457233(__this, method) ((  void (*) (Enumerator_t1562689139 *, const MethodInfo*))Enumerator_Dispose_m3421457233_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<AIEnum.EAIEventtype,System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m4238939602_gshared (Enumerator_t1562689139 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m4238939602(__this, method) ((  bool (*) (Enumerator_t1562689139 *, const MethodInfo*))Enumerator_MoveNext_m4238939602_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<AIEnum.EAIEventtype,System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m130820720_gshared (Enumerator_t1562689139 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m130820720(__this, method) ((  Il2CppObject * (*) (Enumerator_t1562689139 *, const MethodInfo*))Enumerator_get_Current_m130820720_gshared)(__this, method)
