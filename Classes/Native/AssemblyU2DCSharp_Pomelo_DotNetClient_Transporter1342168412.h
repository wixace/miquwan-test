﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Net.Sockets.Socket
struct Socket_t2157335841;
// System.Action`1<System.Byte[]>
struct Action_1_t361609309;
// Pomelo.DotNetClient.StateObject
struct StateObject_t4115849974;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.Action
struct Action_t3771233898;
// System.Object
struct Il2CppObject;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_TransportStat796197636.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pomelo.DotNetClient.Transporter
struct  Transporter_t1342168412  : public Il2CppObject
{
public:
	// System.UInt32 Pomelo.DotNetClient.Transporter::id
	uint32_t ___id_2;
	// System.Net.Sockets.Socket Pomelo.DotNetClient.Transporter::socket
	Socket_t2157335841 * ___socket_3;
	// System.Action`1<System.Byte[]> Pomelo.DotNetClient.Transporter::messageProcesser
	Action_1_t361609309 * ___messageProcesser_4;
	// Pomelo.DotNetClient.StateObject Pomelo.DotNetClient.Transporter::stateObject
	StateObject_t4115849974 * ___stateObject_5;
	// Pomelo.DotNetClient.TransportState Pomelo.DotNetClient.Transporter::transportState
	int32_t ___transportState_6;
	// System.IAsyncResult Pomelo.DotNetClient.Transporter::asyncReceive
	Il2CppObject * ___asyncReceive_7;
	// System.IAsyncResult Pomelo.DotNetClient.Transporter::asyncSend
	Il2CppObject * ___asyncSend_8;
	// System.Boolean Pomelo.DotNetClient.Transporter::onSending
	bool ___onSending_9;
	// System.Boolean Pomelo.DotNetClient.Transporter::onReceiving
	bool ___onReceiving_10;
	// System.Byte[] Pomelo.DotNetClient.Transporter::headBuffer
	ByteU5BU5D_t4260760469* ___headBuffer_11;
	// System.Byte[] Pomelo.DotNetClient.Transporter::buffer
	ByteU5BU5D_t4260760469* ___buffer_12;
	// System.Int32 Pomelo.DotNetClient.Transporter::bufferOffset
	int32_t ___bufferOffset_13;
	// System.Int32 Pomelo.DotNetClient.Transporter::pkgLength
	int32_t ___pkgLength_14;
	// System.Action Pomelo.DotNetClient.Transporter::onDisconnect
	Action_t3771233898 * ___onDisconnect_15;
	// System.Object Pomelo.DotNetClient.Transporter::_lock
	Il2CppObject * ____lock_16;

public:
	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(Transporter_t1342168412, ___id_2)); }
	inline uint32_t get_id_2() const { return ___id_2; }
	inline uint32_t* get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(uint32_t value)
	{
		___id_2 = value;
	}

	inline static int32_t get_offset_of_socket_3() { return static_cast<int32_t>(offsetof(Transporter_t1342168412, ___socket_3)); }
	inline Socket_t2157335841 * get_socket_3() const { return ___socket_3; }
	inline Socket_t2157335841 ** get_address_of_socket_3() { return &___socket_3; }
	inline void set_socket_3(Socket_t2157335841 * value)
	{
		___socket_3 = value;
		Il2CppCodeGenWriteBarrier(&___socket_3, value);
	}

	inline static int32_t get_offset_of_messageProcesser_4() { return static_cast<int32_t>(offsetof(Transporter_t1342168412, ___messageProcesser_4)); }
	inline Action_1_t361609309 * get_messageProcesser_4() const { return ___messageProcesser_4; }
	inline Action_1_t361609309 ** get_address_of_messageProcesser_4() { return &___messageProcesser_4; }
	inline void set_messageProcesser_4(Action_1_t361609309 * value)
	{
		___messageProcesser_4 = value;
		Il2CppCodeGenWriteBarrier(&___messageProcesser_4, value);
	}

	inline static int32_t get_offset_of_stateObject_5() { return static_cast<int32_t>(offsetof(Transporter_t1342168412, ___stateObject_5)); }
	inline StateObject_t4115849974 * get_stateObject_5() const { return ___stateObject_5; }
	inline StateObject_t4115849974 ** get_address_of_stateObject_5() { return &___stateObject_5; }
	inline void set_stateObject_5(StateObject_t4115849974 * value)
	{
		___stateObject_5 = value;
		Il2CppCodeGenWriteBarrier(&___stateObject_5, value);
	}

	inline static int32_t get_offset_of_transportState_6() { return static_cast<int32_t>(offsetof(Transporter_t1342168412, ___transportState_6)); }
	inline int32_t get_transportState_6() const { return ___transportState_6; }
	inline int32_t* get_address_of_transportState_6() { return &___transportState_6; }
	inline void set_transportState_6(int32_t value)
	{
		___transportState_6 = value;
	}

	inline static int32_t get_offset_of_asyncReceive_7() { return static_cast<int32_t>(offsetof(Transporter_t1342168412, ___asyncReceive_7)); }
	inline Il2CppObject * get_asyncReceive_7() const { return ___asyncReceive_7; }
	inline Il2CppObject ** get_address_of_asyncReceive_7() { return &___asyncReceive_7; }
	inline void set_asyncReceive_7(Il2CppObject * value)
	{
		___asyncReceive_7 = value;
		Il2CppCodeGenWriteBarrier(&___asyncReceive_7, value);
	}

	inline static int32_t get_offset_of_asyncSend_8() { return static_cast<int32_t>(offsetof(Transporter_t1342168412, ___asyncSend_8)); }
	inline Il2CppObject * get_asyncSend_8() const { return ___asyncSend_8; }
	inline Il2CppObject ** get_address_of_asyncSend_8() { return &___asyncSend_8; }
	inline void set_asyncSend_8(Il2CppObject * value)
	{
		___asyncSend_8 = value;
		Il2CppCodeGenWriteBarrier(&___asyncSend_8, value);
	}

	inline static int32_t get_offset_of_onSending_9() { return static_cast<int32_t>(offsetof(Transporter_t1342168412, ___onSending_9)); }
	inline bool get_onSending_9() const { return ___onSending_9; }
	inline bool* get_address_of_onSending_9() { return &___onSending_9; }
	inline void set_onSending_9(bool value)
	{
		___onSending_9 = value;
	}

	inline static int32_t get_offset_of_onReceiving_10() { return static_cast<int32_t>(offsetof(Transporter_t1342168412, ___onReceiving_10)); }
	inline bool get_onReceiving_10() const { return ___onReceiving_10; }
	inline bool* get_address_of_onReceiving_10() { return &___onReceiving_10; }
	inline void set_onReceiving_10(bool value)
	{
		___onReceiving_10 = value;
	}

	inline static int32_t get_offset_of_headBuffer_11() { return static_cast<int32_t>(offsetof(Transporter_t1342168412, ___headBuffer_11)); }
	inline ByteU5BU5D_t4260760469* get_headBuffer_11() const { return ___headBuffer_11; }
	inline ByteU5BU5D_t4260760469** get_address_of_headBuffer_11() { return &___headBuffer_11; }
	inline void set_headBuffer_11(ByteU5BU5D_t4260760469* value)
	{
		___headBuffer_11 = value;
		Il2CppCodeGenWriteBarrier(&___headBuffer_11, value);
	}

	inline static int32_t get_offset_of_buffer_12() { return static_cast<int32_t>(offsetof(Transporter_t1342168412, ___buffer_12)); }
	inline ByteU5BU5D_t4260760469* get_buffer_12() const { return ___buffer_12; }
	inline ByteU5BU5D_t4260760469** get_address_of_buffer_12() { return &___buffer_12; }
	inline void set_buffer_12(ByteU5BU5D_t4260760469* value)
	{
		___buffer_12 = value;
		Il2CppCodeGenWriteBarrier(&___buffer_12, value);
	}

	inline static int32_t get_offset_of_bufferOffset_13() { return static_cast<int32_t>(offsetof(Transporter_t1342168412, ___bufferOffset_13)); }
	inline int32_t get_bufferOffset_13() const { return ___bufferOffset_13; }
	inline int32_t* get_address_of_bufferOffset_13() { return &___bufferOffset_13; }
	inline void set_bufferOffset_13(int32_t value)
	{
		___bufferOffset_13 = value;
	}

	inline static int32_t get_offset_of_pkgLength_14() { return static_cast<int32_t>(offsetof(Transporter_t1342168412, ___pkgLength_14)); }
	inline int32_t get_pkgLength_14() const { return ___pkgLength_14; }
	inline int32_t* get_address_of_pkgLength_14() { return &___pkgLength_14; }
	inline void set_pkgLength_14(int32_t value)
	{
		___pkgLength_14 = value;
	}

	inline static int32_t get_offset_of_onDisconnect_15() { return static_cast<int32_t>(offsetof(Transporter_t1342168412, ___onDisconnect_15)); }
	inline Action_t3771233898 * get_onDisconnect_15() const { return ___onDisconnect_15; }
	inline Action_t3771233898 ** get_address_of_onDisconnect_15() { return &___onDisconnect_15; }
	inline void set_onDisconnect_15(Action_t3771233898 * value)
	{
		___onDisconnect_15 = value;
		Il2CppCodeGenWriteBarrier(&___onDisconnect_15, value);
	}

	inline static int32_t get_offset_of__lock_16() { return static_cast<int32_t>(offsetof(Transporter_t1342168412, ____lock_16)); }
	inline Il2CppObject * get__lock_16() const { return ____lock_16; }
	inline Il2CppObject ** get_address_of__lock_16() { return &____lock_16; }
	inline void set__lock_16(Il2CppObject * value)
	{
		____lock_16 = value;
		Il2CppCodeGenWriteBarrier(&____lock_16, value);
	}
};

struct Transporter_t1342168412_StaticFields
{
public:
	// System.UInt32 Pomelo.DotNetClient.Transporter::currentSocketID
	uint32_t ___currentSocketID_1;

public:
	inline static int32_t get_offset_of_currentSocketID_1() { return static_cast<int32_t>(offsetof(Transporter_t1342168412_StaticFields, ___currentSocketID_1)); }
	inline uint32_t get_currentSocketID_1() const { return ___currentSocketID_1; }
	inline uint32_t* get_address_of_currentSocketID_1() { return &___currentSocketID_1; }
	inline void set_currentSocketID_1(uint32_t value)
	{
		___currentSocketID_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
