﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_rearneremeNairdermi234
struct M_rearneremeNairdermi234_t620816888;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_rearneremeNairdermi2620816888.h"

// System.Void GarbageiOS.M_rearneremeNairdermi234::.ctor()
extern "C"  void M_rearneremeNairdermi234__ctor_m392101179 (M_rearneremeNairdermi234_t620816888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rearneremeNairdermi234::M_jeastoujis0(System.String[],System.Int32)
extern "C"  void M_rearneremeNairdermi234_M_jeastoujis0_m499894183 (M_rearneremeNairdermi234_t620816888 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rearneremeNairdermi234::M_firopow1(System.String[],System.Int32)
extern "C"  void M_rearneremeNairdermi234_M_firopow1_m2687809483 (M_rearneremeNairdermi234_t620816888 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rearneremeNairdermi234::M_cawzicaViri2(System.String[],System.Int32)
extern "C"  void M_rearneremeNairdermi234_M_cawzicaViri2_m3204020148 (M_rearneremeNairdermi234_t620816888 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rearneremeNairdermi234::M_tenouNiscaw3(System.String[],System.Int32)
extern "C"  void M_rearneremeNairdermi234_M_tenouNiscaw3_m3040296577 (M_rearneremeNairdermi234_t620816888 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rearneremeNairdermi234::M_bemeCharkooqall4(System.String[],System.Int32)
extern "C"  void M_rearneremeNairdermi234_M_bemeCharkooqall4_m2757976828 (M_rearneremeNairdermi234_t620816888 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rearneremeNairdermi234::ilo_M_jeastoujis01(GarbageiOS.M_rearneremeNairdermi234,System.String[],System.Int32)
extern "C"  void M_rearneremeNairdermi234_ilo_M_jeastoujis01_m3536736977 (Il2CppObject * __this /* static, unused */, M_rearneremeNairdermi234_t620816888 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rearneremeNairdermi234::ilo_M_firopow12(GarbageiOS.M_rearneremeNairdermi234,System.String[],System.Int32)
extern "C"  void M_rearneremeNairdermi234_ilo_M_firopow12_m3970797928 (Il2CppObject * __this /* static, unused */, M_rearneremeNairdermi234_t620816888 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_rearneremeNairdermi234::ilo_M_cawzicaViri23(GarbageiOS.M_rearneremeNairdermi234,System.String[],System.Int32)
extern "C"  void M_rearneremeNairdermi234_ilo_M_cawzicaViri23_m3832028256 (Il2CppObject * __this /* static, unused */, M_rearneremeNairdermi234_t620816888 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
