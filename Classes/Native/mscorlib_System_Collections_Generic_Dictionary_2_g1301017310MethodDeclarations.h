﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>
struct Dictionary_2_t1301017310;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t666883479;
// System.Collections.Generic.IDictionary`2<System.Object,HatredCtrl/stValue>
struct IDictionary_2_t878890655;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.Generic.ICollection`1<HatredCtrl/stValue>
struct ICollection_1_t25568101;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>[]
struct KeyValuePair_2U5BU5D_t2495925505;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,HatredCtrl/stValue>>
struct IEnumerator_1_t3111663065;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,HatredCtrl/stValue>
struct KeyCollection_t2927776761;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,HatredCtrl/stValue>
struct ValueCollection_t1623023;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21199798016.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2618340702.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::.ctor()
extern "C"  void Dictionary_2__ctor_m1760616043_gshared (Dictionary_2_t1301017310 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m1760616043(__this, method) ((  void (*) (Dictionary_2_t1301017310 *, const MethodInfo*))Dictionary_2__ctor_m1760616043_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2476287330_gshared (Dictionary_2_t1301017310 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m2476287330(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t1301017310 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2476287330_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m3766189069_gshared (Dictionary_2_t1301017310 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m3766189069(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t1301017310 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3766189069_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m3890563900_gshared (Dictionary_2_t1301017310 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m3890563900(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t1301017310 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3890563900_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m1561031184_gshared (Dictionary_2_t1301017310 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m1561031184(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t1301017310 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1561031184_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m2681162028_gshared (Dictionary_2_t1301017310 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m2681162028(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1301017310 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m2681162028_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2305136077_gshared (Dictionary_2_t1301017310 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2305136077(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1301017310 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2305136077_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3506438249_gshared (Dictionary_2_t1301017310 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3506438249(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1301017310 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m3506438249_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m590581481_gshared (Dictionary_2_t1301017310 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m590581481(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1301017310 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m590581481_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m2980674967_gshared (Dictionary_2_t1301017310 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m2980674967(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1301017310 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m2980674967_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2918672530_gshared (Dictionary_2_t1301017310 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2918672530(__this, method) ((  bool (*) (Dictionary_2_t1301017310 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2918672530_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3734748199_gshared (Dictionary_2_t1301017310 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3734748199(__this, method) ((  bool (*) (Dictionary_2_t1301017310 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m3734748199_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m2943380467_gshared (Dictionary_2_t1301017310 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m2943380467(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1301017310 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2943380467_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m2294571682_gshared (Dictionary_2_t1301017310 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m2294571682(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1301017310 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m2294571682_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m2824027759_gshared (Dictionary_2_t1301017310 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m2824027759(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1301017310 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m2824027759_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m3707441891_gshared (Dictionary_2_t1301017310 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m3707441891(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1301017310 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m3707441891_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m511992032_gshared (Dictionary_2_t1301017310 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m511992032(__this, ___key0, method) ((  void (*) (Dictionary_2_t1301017310 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m511992032_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2367373137_gshared (Dictionary_2_t1301017310 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2367373137(__this, method) ((  bool (*) (Dictionary_2_t1301017310 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m2367373137_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m711740995_gshared (Dictionary_2_t1301017310 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m711740995(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1301017310 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m711740995_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3005759573_gshared (Dictionary_2_t1301017310 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3005759573(__this, method) ((  bool (*) (Dictionary_2_t1301017310 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3005759573_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1723997558_gshared (Dictionary_2_t1301017310 * __this, KeyValuePair_2_t1199798016  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1723997558(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t1301017310 *, KeyValuePair_2_t1199798016 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1723997558_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3321062096_gshared (Dictionary_2_t1301017310 * __this, KeyValuePair_2_t1199798016  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3321062096(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1301017310 *, KeyValuePair_2_t1199798016 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3321062096_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3774536666_gshared (Dictionary_2_t1301017310 * __this, KeyValuePair_2U5BU5D_t2495925505* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3774536666(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1301017310 *, KeyValuePair_2U5BU5D_t2495925505*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m3774536666_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m94109173_gshared (Dictionary_2_t1301017310 * __this, KeyValuePair_2_t1199798016  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m94109173(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t1301017310 *, KeyValuePair_2_t1199798016 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m94109173_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m2441259321_gshared (Dictionary_2_t1301017310 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2441259321(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1301017310 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m2441259321_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3446803976_gshared (Dictionary_2_t1301017310 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3446803976(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1301017310 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3446803976_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m919449087_gshared (Dictionary_2_t1301017310 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m919449087(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t1301017310 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m919449087_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m706665036_gshared (Dictionary_2_t1301017310 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m706665036(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t1301017310 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m706665036_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m938276875_gshared (Dictionary_2_t1301017310 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m938276875(__this, method) ((  int32_t (*) (Dictionary_2_t1301017310 *, const MethodInfo*))Dictionary_2_get_Count_m938276875_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::get_Item(TKey)
extern "C"  stValue_t3425945410  Dictionary_2_get_Item_m1607219996_gshared (Dictionary_2_t1301017310 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m1607219996(__this, ___key0, method) ((  stValue_t3425945410  (*) (Dictionary_2_t1301017310 *, Il2CppObject *, const MethodInfo*))Dictionary_2_get_Item_m1607219996_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m3116236011_gshared (Dictionary_2_t1301017310 * __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m3116236011(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1301017310 *, Il2CppObject *, stValue_t3425945410 , const MethodInfo*))Dictionary_2_set_Item_m3116236011_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m3594313187_gshared (Dictionary_2_t1301017310 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m3594313187(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t1301017310 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3594313187_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m3332014132_gshared (Dictionary_2_t1301017310 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m3332014132(__this, ___size0, method) ((  void (*) (Dictionary_2_t1301017310 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3332014132_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m4065259824_gshared (Dictionary_2_t1301017310 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m4065259824(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1301017310 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m4065259824_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t1199798016  Dictionary_2_make_pair_m2719450820_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m2719450820(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t1199798016  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, stValue_t3425945410 , const MethodInfo*))Dictionary_2_make_pair_m2719450820_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::pick_key(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_key_m2004343930_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m2004343930(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, stValue_t3425945410 , const MethodInfo*))Dictionary_2_pick_key_m2004343930_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::pick_value(TKey,TValue)
extern "C"  stValue_t3425945410  Dictionary_2_pick_value_m1654942934_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m1654942934(__this /* static, unused */, ___key0, ___value1, method) ((  stValue_t3425945410  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, stValue_t3425945410 , const MethodInfo*))Dictionary_2_pick_value_m1654942934_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m2972380319_gshared (Dictionary_2_t1301017310 * __this, KeyValuePair_2U5BU5D_t2495925505* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m2972380319(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t1301017310 *, KeyValuePair_2U5BU5D_t2495925505*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2972380319_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::Resize()
extern "C"  void Dictionary_2_Resize_m3097286445_gshared (Dictionary_2_t1301017310 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m3097286445(__this, method) ((  void (*) (Dictionary_2_t1301017310 *, const MethodInfo*))Dictionary_2_Resize_m3097286445_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m3689045674_gshared (Dictionary_2_t1301017310 * __this, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m3689045674(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t1301017310 *, Il2CppObject *, stValue_t3425945410 , const MethodInfo*))Dictionary_2_Add_m3689045674_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::Clear()
extern "C"  void Dictionary_2_Clear_m3461716630_gshared (Dictionary_2_t1301017310 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m3461716630(__this, method) ((  void (*) (Dictionary_2_t1301017310 *, const MethodInfo*))Dictionary_2_Clear_m3461716630_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m2601694528_gshared (Dictionary_2_t1301017310 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m2601694528(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1301017310 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsKey_m2601694528_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m2962319680_gshared (Dictionary_2_t1301017310 * __this, stValue_t3425945410  ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m2962319680(__this, ___value0, method) ((  bool (*) (Dictionary_2_t1301017310 *, stValue_t3425945410 , const MethodInfo*))Dictionary_2_ContainsValue_m2962319680_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m3766133897_gshared (Dictionary_2_t1301017310 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m3766133897(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t1301017310 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m3766133897_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m1560183163_gshared (Dictionary_2_t1301017310 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m1560183163(__this, ___sender0, method) ((  void (*) (Dictionary_2_t1301017310 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m1560183163_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m356596656_gshared (Dictionary_2_t1301017310 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m356596656(__this, ___key0, method) ((  bool (*) (Dictionary_2_t1301017310 *, Il2CppObject *, const MethodInfo*))Dictionary_2_Remove_m356596656_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m3962949017_gshared (Dictionary_2_t1301017310 * __this, Il2CppObject * ___key0, stValue_t3425945410 * ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m3962949017(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t1301017310 *, Il2CppObject *, stValue_t3425945410 *, const MethodInfo*))Dictionary_2_TryGetValue_m3962949017_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::get_Keys()
extern "C"  KeyCollection_t2927776761 * Dictionary_2_get_Keys_m2590504098_gshared (Dictionary_2_t1301017310 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m2590504098(__this, method) ((  KeyCollection_t2927776761 * (*) (Dictionary_2_t1301017310 *, const MethodInfo*))Dictionary_2_get_Keys_m2590504098_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::get_Values()
extern "C"  ValueCollection_t1623023 * Dictionary_2_get_Values_m2331410622_gshared (Dictionary_2_t1301017310 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m2331410622(__this, method) ((  ValueCollection_t1623023 * (*) (Dictionary_2_t1301017310 *, const MethodInfo*))Dictionary_2_get_Values_m2331410622_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::ToTKey(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTKey_m1454202837_gshared (Dictionary_2_t1301017310 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m1454202837(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t1301017310 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m1454202837_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::ToTValue(System.Object)
extern "C"  stValue_t3425945410  Dictionary_2_ToTValue_m4262224369_gshared (Dictionary_2_t1301017310 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m4262224369(__this, ___value0, method) ((  stValue_t3425945410  (*) (Dictionary_2_t1301017310 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m4262224369_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m1672276371_gshared (Dictionary_2_t1301017310 * __this, KeyValuePair_2_t1199798016  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m1672276371(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t1301017310 *, KeyValuePair_2_t1199798016 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m1672276371_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::GetEnumerator()
extern "C"  Enumerator_t2618340702  Dictionary_2_GetEnumerator_m2036937270_gshared (Dictionary_2_t1301017310 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m2036937270(__this, method) ((  Enumerator_t2618340702  (*) (Dictionary_2_t1301017310 *, const MethodInfo*))Dictionary_2_GetEnumerator_m2036937270_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,HatredCtrl/stValue>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m4152592685_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, stValue_t3425945410  ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m4152592685(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, stValue_t3425945410 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m4152592685_gshared)(__this /* static, unused */, ___key0, ___value1, method)
