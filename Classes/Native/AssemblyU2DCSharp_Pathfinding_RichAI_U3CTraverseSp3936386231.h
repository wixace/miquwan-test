﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.RichSpecial
struct RichSpecial_t2303562271;
// Pathfinding.AnimationLink
struct AnimationLink_t1204462080;
// System.Object
struct Il2CppObject;
// Pathfinding.RichAI
struct RichAI_t1710845178;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RichAI/<TraverseSpecial>c__Iterator6
struct  U3CTraverseSpecialU3Ec__Iterator6_t3936386231  : public Il2CppObject
{
public:
	// Pathfinding.RichSpecial Pathfinding.RichAI/<TraverseSpecial>c__Iterator6::rs
	RichSpecial_t2303562271 * ___rs_0;
	// Pathfinding.AnimationLink Pathfinding.RichAI/<TraverseSpecial>c__Iterator6::<al>__0
	AnimationLink_t1204462080 * ___U3CalU3E__0_1;
	// System.Int32 Pathfinding.RichAI/<TraverseSpecial>c__Iterator6::$PC
	int32_t ___U24PC_2;
	// System.Object Pathfinding.RichAI/<TraverseSpecial>c__Iterator6::$current
	Il2CppObject * ___U24current_3;
	// Pathfinding.RichSpecial Pathfinding.RichAI/<TraverseSpecial>c__Iterator6::<$>rs
	RichSpecial_t2303562271 * ___U3CU24U3Ers_4;
	// Pathfinding.RichAI Pathfinding.RichAI/<TraverseSpecial>c__Iterator6::<>f__this
	RichAI_t1710845178 * ___U3CU3Ef__this_5;

public:
	inline static int32_t get_offset_of_rs_0() { return static_cast<int32_t>(offsetof(U3CTraverseSpecialU3Ec__Iterator6_t3936386231, ___rs_0)); }
	inline RichSpecial_t2303562271 * get_rs_0() const { return ___rs_0; }
	inline RichSpecial_t2303562271 ** get_address_of_rs_0() { return &___rs_0; }
	inline void set_rs_0(RichSpecial_t2303562271 * value)
	{
		___rs_0 = value;
		Il2CppCodeGenWriteBarrier(&___rs_0, value);
	}

	inline static int32_t get_offset_of_U3CalU3E__0_1() { return static_cast<int32_t>(offsetof(U3CTraverseSpecialU3Ec__Iterator6_t3936386231, ___U3CalU3E__0_1)); }
	inline AnimationLink_t1204462080 * get_U3CalU3E__0_1() const { return ___U3CalU3E__0_1; }
	inline AnimationLink_t1204462080 ** get_address_of_U3CalU3E__0_1() { return &___U3CalU3E__0_1; }
	inline void set_U3CalU3E__0_1(AnimationLink_t1204462080 * value)
	{
		___U3CalU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CalU3E__0_1, value);
	}

	inline static int32_t get_offset_of_U24PC_2() { return static_cast<int32_t>(offsetof(U3CTraverseSpecialU3Ec__Iterator6_t3936386231, ___U24PC_2)); }
	inline int32_t get_U24PC_2() const { return ___U24PC_2; }
	inline int32_t* get_address_of_U24PC_2() { return &___U24PC_2; }
	inline void set_U24PC_2(int32_t value)
	{
		___U24PC_2 = value;
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CTraverseSpecialU3Ec__Iterator6_t3936386231, ___U24current_3)); }
	inline Il2CppObject * get_U24current_3() const { return ___U24current_3; }
	inline Il2CppObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(Il2CppObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_3, value);
	}

	inline static int32_t get_offset_of_U3CU24U3Ers_4() { return static_cast<int32_t>(offsetof(U3CTraverseSpecialU3Ec__Iterator6_t3936386231, ___U3CU24U3Ers_4)); }
	inline RichSpecial_t2303562271 * get_U3CU24U3Ers_4() const { return ___U3CU24U3Ers_4; }
	inline RichSpecial_t2303562271 ** get_address_of_U3CU24U3Ers_4() { return &___U3CU24U3Ers_4; }
	inline void set_U3CU24U3Ers_4(RichSpecial_t2303562271 * value)
	{
		___U3CU24U3Ers_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU24U3Ers_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_5() { return static_cast<int32_t>(offsetof(U3CTraverseSpecialU3Ec__Iterator6_t3936386231, ___U3CU3Ef__this_5)); }
	inline RichAI_t1710845178 * get_U3CU3Ef__this_5() const { return ___U3CU3Ef__this_5; }
	inline RichAI_t1710845178 ** get_address_of_U3CU3Ef__this_5() { return &___U3CU3Ef__this_5; }
	inline void set_U3CU3Ef__this_5(RichAI_t1710845178 * value)
	{
		___U3CU3Ef__this_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
