﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_fawouStearcurrar52
struct  M_fawouStearcurrar52_t1461196931  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_fawouStearcurrar52::_wowcay
	bool ____wowcay_0;
	// System.Int32 GarbageiOS.M_fawouStearcurrar52::_tereseTakair
	int32_t ____tereseTakair_1;
	// System.String GarbageiOS.M_fawouStearcurrar52::_fallcerelur
	String_t* ____fallcerelur_2;
	// System.Int32 GarbageiOS.M_fawouStearcurrar52::_beetaltarZerwi
	int32_t ____beetaltarZerwi_3;
	// System.Single GarbageiOS.M_fawouStearcurrar52::_dairstealoNallvur
	float ____dairstealoNallvur_4;
	// System.String GarbageiOS.M_fawouStearcurrar52::_poroo
	String_t* ____poroo_5;
	// System.UInt32 GarbageiOS.M_fawouStearcurrar52::_nurcibe
	uint32_t ____nurcibe_6;
	// System.String GarbageiOS.M_fawouStearcurrar52::_jemiPorcera
	String_t* ____jemiPorcera_7;
	// System.Single GarbageiOS.M_fawouStearcurrar52::_saigeetri
	float ____saigeetri_8;
	// System.Single GarbageiOS.M_fawouStearcurrar52::_gastousisLalljere
	float ____gastousisLalljere_9;
	// System.Single GarbageiOS.M_fawouStearcurrar52::_teafeala
	float ____teafeala_10;
	// System.Int32 GarbageiOS.M_fawouStearcurrar52::_stayyarar
	int32_t ____stayyarar_11;
	// System.Boolean GarbageiOS.M_fawouStearcurrar52::_pallmeDinafee
	bool ____pallmeDinafee_12;
	// System.Single GarbageiOS.M_fawouStearcurrar52::_jaydrall
	float ____jaydrall_13;
	// System.Boolean GarbageiOS.M_fawouStearcurrar52::_wecirkeSemfem
	bool ____wecirkeSemfem_14;
	// System.UInt32 GarbageiOS.M_fawouStearcurrar52::_nalsisCapemow
	uint32_t ____nalsisCapemow_15;
	// System.Single GarbageiOS.M_fawouStearcurrar52::_rarjea
	float ____rarjea_16;
	// System.Int32 GarbageiOS.M_fawouStearcurrar52::_tufecas
	int32_t ____tufecas_17;
	// System.Int32 GarbageiOS.M_fawouStearcurrar52::_kaiweTeewirke
	int32_t ____kaiweTeewirke_18;
	// System.UInt32 GarbageiOS.M_fawouStearcurrar52::_drutoNempalcere
	uint32_t ____drutoNempalcere_19;
	// System.Single GarbageiOS.M_fawouStearcurrar52::_drasisLereyi
	float ____drasisLereyi_20;
	// System.Boolean GarbageiOS.M_fawouStearcurrar52::_chere
	bool ____chere_21;
	// System.Single GarbageiOS.M_fawouStearcurrar52::_bileNearchisqe
	float ____bileNearchisqe_22;
	// System.String GarbageiOS.M_fawouStearcurrar52::_jirbasCawmibe
	String_t* ____jirbasCawmibe_23;
	// System.String GarbageiOS.M_fawouStearcurrar52::_drowpoo
	String_t* ____drowpoo_24;

public:
	inline static int32_t get_offset_of__wowcay_0() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____wowcay_0)); }
	inline bool get__wowcay_0() const { return ____wowcay_0; }
	inline bool* get_address_of__wowcay_0() { return &____wowcay_0; }
	inline void set__wowcay_0(bool value)
	{
		____wowcay_0 = value;
	}

	inline static int32_t get_offset_of__tereseTakair_1() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____tereseTakair_1)); }
	inline int32_t get__tereseTakair_1() const { return ____tereseTakair_1; }
	inline int32_t* get_address_of__tereseTakair_1() { return &____tereseTakair_1; }
	inline void set__tereseTakair_1(int32_t value)
	{
		____tereseTakair_1 = value;
	}

	inline static int32_t get_offset_of__fallcerelur_2() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____fallcerelur_2)); }
	inline String_t* get__fallcerelur_2() const { return ____fallcerelur_2; }
	inline String_t** get_address_of__fallcerelur_2() { return &____fallcerelur_2; }
	inline void set__fallcerelur_2(String_t* value)
	{
		____fallcerelur_2 = value;
		Il2CppCodeGenWriteBarrier(&____fallcerelur_2, value);
	}

	inline static int32_t get_offset_of__beetaltarZerwi_3() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____beetaltarZerwi_3)); }
	inline int32_t get__beetaltarZerwi_3() const { return ____beetaltarZerwi_3; }
	inline int32_t* get_address_of__beetaltarZerwi_3() { return &____beetaltarZerwi_3; }
	inline void set__beetaltarZerwi_3(int32_t value)
	{
		____beetaltarZerwi_3 = value;
	}

	inline static int32_t get_offset_of__dairstealoNallvur_4() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____dairstealoNallvur_4)); }
	inline float get__dairstealoNallvur_4() const { return ____dairstealoNallvur_4; }
	inline float* get_address_of__dairstealoNallvur_4() { return &____dairstealoNallvur_4; }
	inline void set__dairstealoNallvur_4(float value)
	{
		____dairstealoNallvur_4 = value;
	}

	inline static int32_t get_offset_of__poroo_5() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____poroo_5)); }
	inline String_t* get__poroo_5() const { return ____poroo_5; }
	inline String_t** get_address_of__poroo_5() { return &____poroo_5; }
	inline void set__poroo_5(String_t* value)
	{
		____poroo_5 = value;
		Il2CppCodeGenWriteBarrier(&____poroo_5, value);
	}

	inline static int32_t get_offset_of__nurcibe_6() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____nurcibe_6)); }
	inline uint32_t get__nurcibe_6() const { return ____nurcibe_6; }
	inline uint32_t* get_address_of__nurcibe_6() { return &____nurcibe_6; }
	inline void set__nurcibe_6(uint32_t value)
	{
		____nurcibe_6 = value;
	}

	inline static int32_t get_offset_of__jemiPorcera_7() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____jemiPorcera_7)); }
	inline String_t* get__jemiPorcera_7() const { return ____jemiPorcera_7; }
	inline String_t** get_address_of__jemiPorcera_7() { return &____jemiPorcera_7; }
	inline void set__jemiPorcera_7(String_t* value)
	{
		____jemiPorcera_7 = value;
		Il2CppCodeGenWriteBarrier(&____jemiPorcera_7, value);
	}

	inline static int32_t get_offset_of__saigeetri_8() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____saigeetri_8)); }
	inline float get__saigeetri_8() const { return ____saigeetri_8; }
	inline float* get_address_of__saigeetri_8() { return &____saigeetri_8; }
	inline void set__saigeetri_8(float value)
	{
		____saigeetri_8 = value;
	}

	inline static int32_t get_offset_of__gastousisLalljere_9() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____gastousisLalljere_9)); }
	inline float get__gastousisLalljere_9() const { return ____gastousisLalljere_9; }
	inline float* get_address_of__gastousisLalljere_9() { return &____gastousisLalljere_9; }
	inline void set__gastousisLalljere_9(float value)
	{
		____gastousisLalljere_9 = value;
	}

	inline static int32_t get_offset_of__teafeala_10() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____teafeala_10)); }
	inline float get__teafeala_10() const { return ____teafeala_10; }
	inline float* get_address_of__teafeala_10() { return &____teafeala_10; }
	inline void set__teafeala_10(float value)
	{
		____teafeala_10 = value;
	}

	inline static int32_t get_offset_of__stayyarar_11() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____stayyarar_11)); }
	inline int32_t get__stayyarar_11() const { return ____stayyarar_11; }
	inline int32_t* get_address_of__stayyarar_11() { return &____stayyarar_11; }
	inline void set__stayyarar_11(int32_t value)
	{
		____stayyarar_11 = value;
	}

	inline static int32_t get_offset_of__pallmeDinafee_12() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____pallmeDinafee_12)); }
	inline bool get__pallmeDinafee_12() const { return ____pallmeDinafee_12; }
	inline bool* get_address_of__pallmeDinafee_12() { return &____pallmeDinafee_12; }
	inline void set__pallmeDinafee_12(bool value)
	{
		____pallmeDinafee_12 = value;
	}

	inline static int32_t get_offset_of__jaydrall_13() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____jaydrall_13)); }
	inline float get__jaydrall_13() const { return ____jaydrall_13; }
	inline float* get_address_of__jaydrall_13() { return &____jaydrall_13; }
	inline void set__jaydrall_13(float value)
	{
		____jaydrall_13 = value;
	}

	inline static int32_t get_offset_of__wecirkeSemfem_14() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____wecirkeSemfem_14)); }
	inline bool get__wecirkeSemfem_14() const { return ____wecirkeSemfem_14; }
	inline bool* get_address_of__wecirkeSemfem_14() { return &____wecirkeSemfem_14; }
	inline void set__wecirkeSemfem_14(bool value)
	{
		____wecirkeSemfem_14 = value;
	}

	inline static int32_t get_offset_of__nalsisCapemow_15() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____nalsisCapemow_15)); }
	inline uint32_t get__nalsisCapemow_15() const { return ____nalsisCapemow_15; }
	inline uint32_t* get_address_of__nalsisCapemow_15() { return &____nalsisCapemow_15; }
	inline void set__nalsisCapemow_15(uint32_t value)
	{
		____nalsisCapemow_15 = value;
	}

	inline static int32_t get_offset_of__rarjea_16() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____rarjea_16)); }
	inline float get__rarjea_16() const { return ____rarjea_16; }
	inline float* get_address_of__rarjea_16() { return &____rarjea_16; }
	inline void set__rarjea_16(float value)
	{
		____rarjea_16 = value;
	}

	inline static int32_t get_offset_of__tufecas_17() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____tufecas_17)); }
	inline int32_t get__tufecas_17() const { return ____tufecas_17; }
	inline int32_t* get_address_of__tufecas_17() { return &____tufecas_17; }
	inline void set__tufecas_17(int32_t value)
	{
		____tufecas_17 = value;
	}

	inline static int32_t get_offset_of__kaiweTeewirke_18() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____kaiweTeewirke_18)); }
	inline int32_t get__kaiweTeewirke_18() const { return ____kaiweTeewirke_18; }
	inline int32_t* get_address_of__kaiweTeewirke_18() { return &____kaiweTeewirke_18; }
	inline void set__kaiweTeewirke_18(int32_t value)
	{
		____kaiweTeewirke_18 = value;
	}

	inline static int32_t get_offset_of__drutoNempalcere_19() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____drutoNempalcere_19)); }
	inline uint32_t get__drutoNempalcere_19() const { return ____drutoNempalcere_19; }
	inline uint32_t* get_address_of__drutoNempalcere_19() { return &____drutoNempalcere_19; }
	inline void set__drutoNempalcere_19(uint32_t value)
	{
		____drutoNempalcere_19 = value;
	}

	inline static int32_t get_offset_of__drasisLereyi_20() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____drasisLereyi_20)); }
	inline float get__drasisLereyi_20() const { return ____drasisLereyi_20; }
	inline float* get_address_of__drasisLereyi_20() { return &____drasisLereyi_20; }
	inline void set__drasisLereyi_20(float value)
	{
		____drasisLereyi_20 = value;
	}

	inline static int32_t get_offset_of__chere_21() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____chere_21)); }
	inline bool get__chere_21() const { return ____chere_21; }
	inline bool* get_address_of__chere_21() { return &____chere_21; }
	inline void set__chere_21(bool value)
	{
		____chere_21 = value;
	}

	inline static int32_t get_offset_of__bileNearchisqe_22() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____bileNearchisqe_22)); }
	inline float get__bileNearchisqe_22() const { return ____bileNearchisqe_22; }
	inline float* get_address_of__bileNearchisqe_22() { return &____bileNearchisqe_22; }
	inline void set__bileNearchisqe_22(float value)
	{
		____bileNearchisqe_22 = value;
	}

	inline static int32_t get_offset_of__jirbasCawmibe_23() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____jirbasCawmibe_23)); }
	inline String_t* get__jirbasCawmibe_23() const { return ____jirbasCawmibe_23; }
	inline String_t** get_address_of__jirbasCawmibe_23() { return &____jirbasCawmibe_23; }
	inline void set__jirbasCawmibe_23(String_t* value)
	{
		____jirbasCawmibe_23 = value;
		Il2CppCodeGenWriteBarrier(&____jirbasCawmibe_23, value);
	}

	inline static int32_t get_offset_of__drowpoo_24() { return static_cast<int32_t>(offsetof(M_fawouStearcurrar52_t1461196931, ____drowpoo_24)); }
	inline String_t* get__drowpoo_24() const { return ____drowpoo_24; }
	inline String_t** get_address_of__drowpoo_24() { return &____drowpoo_24; }
	inline void set__drowpoo_24(String_t* value)
	{
		____drowpoo_24 = value;
		Il2CppCodeGenWriteBarrier(&____drowpoo_24, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
