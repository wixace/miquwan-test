﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AssetBundleCreateRequestGenerated
struct UnityEngine_AssetBundleCreateRequestGenerated_t2642628262;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_AssetBundleCreateRequestGenerated::.ctor()
extern "C"  void UnityEngine_AssetBundleCreateRequestGenerated__ctor_m2865794437 (UnityEngine_AssetBundleCreateRequestGenerated_t2642628262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleCreateRequestGenerated::AssetBundleCreateRequest_AssetBundleCreateRequest1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AssetBundleCreateRequestGenerated_AssetBundleCreateRequest_AssetBundleCreateRequest1_m1907752557 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AssetBundleCreateRequestGenerated::AssetBundleCreateRequest_assetBundle(JSVCall)
extern "C"  void UnityEngine_AssetBundleCreateRequestGenerated_AssetBundleCreateRequest_assetBundle_m1678186580 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AssetBundleCreateRequestGenerated::__Register()
extern "C"  void UnityEngine_AssetBundleCreateRequestGenerated___Register_m2065013346 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AssetBundleCreateRequestGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_AssetBundleCreateRequestGenerated_ilo_attachFinalizerObject1_m2358390802 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
