﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23374811140.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr_ProductInfo1305991238.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3143780667_gshared (KeyValuePair_2_t3374811140 * __this, Il2CppObject * ___key0, ProductInfo_t1305991238  ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3143780667(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3374811140 *, Il2CppObject *, ProductInfo_t1305991238 , const MethodInfo*))KeyValuePair_2__ctor_m3143780667_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2376238157_gshared (KeyValuePair_2_t3374811140 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m2376238157(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t3374811140 *, const MethodInfo*))KeyValuePair_2_get_Key_m2376238157_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3324835854_gshared (KeyValuePair_2_t3374811140 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m3324835854(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3374811140 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Key_m3324835854_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>::get_Value()
extern "C"  ProductInfo_t1305991238  KeyValuePair_2_get_Value_m2609530573_gshared (KeyValuePair_2_t3374811140 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m2609530573(__this, method) ((  ProductInfo_t1305991238  (*) (KeyValuePair_2_t3374811140 *, const MethodInfo*))KeyValuePair_2_get_Value_m2609530573_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1813091598_gshared (KeyValuePair_2_t3374811140 * __this, ProductInfo_t1305991238  ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m1813091598(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3374811140 *, ProductInfo_t1305991238 , const MethodInfo*))KeyValuePair_2_set_Value_m1813091598_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,ProductsCfgMgr/ProductInfo>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m4080585812_gshared (KeyValuePair_2_t3374811140 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m4080585812(__this, method) ((  String_t* (*) (KeyValuePair_2_t3374811140 *, const MethodInfo*))KeyValuePair_2_ToString_m4080585812_gshared)(__this, method)
