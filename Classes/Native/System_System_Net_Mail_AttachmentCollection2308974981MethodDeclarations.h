﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.Mail.AttachmentCollection
struct AttachmentCollection_t2308974981;
// System.Net.Mail.Attachment
struct Attachment_t2730920775;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Net_Mail_Attachment2730920775.h"

// System.Void System.Net.Mail.AttachmentCollection::.ctor()
extern "C"  void AttachmentCollection__ctor_m1668887476 (AttachmentCollection_t2308974981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.AttachmentCollection::Dispose()
extern "C"  void AttachmentCollection_Dispose_m1676346097 (AttachmentCollection_t2308974981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.AttachmentCollection::ClearItems()
extern "C"  void AttachmentCollection_ClearItems_m3816014979 (AttachmentCollection_t2308974981 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.AttachmentCollection::InsertItem(System.Int32,System.Net.Mail.Attachment)
extern "C"  void AttachmentCollection_InsertItem_m4159506607 (AttachmentCollection_t2308974981 * __this, int32_t ___index0, Attachment_t2730920775 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.AttachmentCollection::RemoveItem(System.Int32)
extern "C"  void AttachmentCollection_RemoveItem_m263774872 (AttachmentCollection_t2308974981 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Net.Mail.AttachmentCollection::SetItem(System.Int32,System.Net.Mail.Attachment)
extern "C"  void AttachmentCollection_SetItem_m3122887332 (AttachmentCollection_t2308974981 * __this, int32_t ___index0, Attachment_t2730920775 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
