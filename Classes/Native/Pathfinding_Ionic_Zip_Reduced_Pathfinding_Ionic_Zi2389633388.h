﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Ionic.Zip.ZipFile
struct ZipFile_t1348418467;
// Pathfinding.Ionic.Zip.ZipOutputStream
struct ZipOutputStream_t2875537416;
// Pathfinding.Ionic.Zip.ZipInputStream
struct ZipInputStream_t3820423253;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Ionic.Zip.ZipContainer
struct  ZipContainer_t2389633388  : public Il2CppObject
{
public:
	// Pathfinding.Ionic.Zip.ZipFile Pathfinding.Ionic.Zip.ZipContainer::_zf
	ZipFile_t1348418467 * ____zf_0;
	// Pathfinding.Ionic.Zip.ZipOutputStream Pathfinding.Ionic.Zip.ZipContainer::_zos
	ZipOutputStream_t2875537416 * ____zos_1;
	// Pathfinding.Ionic.Zip.ZipInputStream Pathfinding.Ionic.Zip.ZipContainer::_zis
	ZipInputStream_t3820423253 * ____zis_2;

public:
	inline static int32_t get_offset_of__zf_0() { return static_cast<int32_t>(offsetof(ZipContainer_t2389633388, ____zf_0)); }
	inline ZipFile_t1348418467 * get__zf_0() const { return ____zf_0; }
	inline ZipFile_t1348418467 ** get_address_of__zf_0() { return &____zf_0; }
	inline void set__zf_0(ZipFile_t1348418467 * value)
	{
		____zf_0 = value;
		Il2CppCodeGenWriteBarrier(&____zf_0, value);
	}

	inline static int32_t get_offset_of__zos_1() { return static_cast<int32_t>(offsetof(ZipContainer_t2389633388, ____zos_1)); }
	inline ZipOutputStream_t2875537416 * get__zos_1() const { return ____zos_1; }
	inline ZipOutputStream_t2875537416 ** get_address_of__zos_1() { return &____zos_1; }
	inline void set__zos_1(ZipOutputStream_t2875537416 * value)
	{
		____zos_1 = value;
		Il2CppCodeGenWriteBarrier(&____zos_1, value);
	}

	inline static int32_t get_offset_of__zis_2() { return static_cast<int32_t>(offsetof(ZipContainer_t2389633388, ____zis_2)); }
	inline ZipInputStream_t3820423253 * get__zis_2() const { return ____zis_2; }
	inline ZipInputStream_t3820423253 ** get_address_of__zis_2() { return &____zis_2; }
	inline void set__zis_2(ZipInputStream_t3820423253 * value)
	{
		____zis_2 = value;
		Il2CppCodeGenWriteBarrier(&____zis_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
