﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>
struct Dictionary_2_t2731505821;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3346441875.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int3,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1755075600_gshared (Enumerator_t3346441875 * __this, Dictionary_2_t2731505821 * ___host0, const MethodInfo* method);
#define Enumerator__ctor_m1755075600(__this, ___host0, method) ((  void (*) (Enumerator_t3346441875 *, Dictionary_2_t2731505821 *, const MethodInfo*))Enumerator__ctor_m1755075600_gshared)(__this, ___host0, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int3,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3450522257_gshared (Enumerator_t3346441875 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3450522257(__this, method) ((  Il2CppObject * (*) (Enumerator_t3346441875 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3450522257_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int3,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2677461669_gshared (Enumerator_t3346441875 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m2677461669(__this, method) ((  void (*) (Enumerator_t3346441875 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2677461669_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int3,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m2704817842_gshared (Enumerator_t3346441875 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2704817842(__this, method) ((  void (*) (Enumerator_t3346441875 *, const MethodInfo*))Enumerator_Dispose_m2704817842_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int3,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2567230737_gshared (Enumerator_t3346441875 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2567230737(__this, method) ((  bool (*) (Enumerator_t3346441875 *, const MethodInfo*))Enumerator_MoveNext_m2567230737_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<Pathfinding.Int3,System.Int32>::get_Current()
extern "C"  Int3_t1974045594  Enumerator_get_Current_m1506628899_gshared (Enumerator_t3346441875 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1506628899(__this, method) ((  Int3_t1974045594  (*) (Enumerator_t3346441875 *, const MethodInfo*))Enumerator_get_Current_m1506628899_gshared)(__this, method)
