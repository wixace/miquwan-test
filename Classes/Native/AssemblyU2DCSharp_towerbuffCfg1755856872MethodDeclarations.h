﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// towerbuffCfg
struct towerbuffCfg_t1755856872;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void towerbuffCfg::.ctor()
extern "C"  void towerbuffCfg__ctor_m3515160499 (towerbuffCfg_t1755856872 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void towerbuffCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void towerbuffCfg_Init_m2666260210 (towerbuffCfg_t1755856872 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
