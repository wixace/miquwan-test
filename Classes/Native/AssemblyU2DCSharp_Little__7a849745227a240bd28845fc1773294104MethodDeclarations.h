﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._7a849745227a240bd28845fc59065605
struct _7a849745227a240bd28845fc59065605_t1773294104;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._7a849745227a240bd28845fc59065605::.ctor()
extern "C"  void _7a849745227a240bd28845fc59065605__ctor_m2272243477 (_7a849745227a240bd28845fc59065605_t1773294104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7a849745227a240bd28845fc59065605::_7a849745227a240bd28845fc59065605m2(System.Int32)
extern "C"  int32_t _7a849745227a240bd28845fc59065605__7a849745227a240bd28845fc59065605m2_m1644763801 (_7a849745227a240bd28845fc59065605_t1773294104 * __this, int32_t ____7a849745227a240bd28845fc59065605a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7a849745227a240bd28845fc59065605::_7a849745227a240bd28845fc59065605m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _7a849745227a240bd28845fc59065605__7a849745227a240bd28845fc59065605m_m996650045 (_7a849745227a240bd28845fc59065605_t1773294104 * __this, int32_t ____7a849745227a240bd28845fc59065605a0, int32_t ____7a849745227a240bd28845fc5906560561, int32_t ____7a849745227a240bd28845fc59065605c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
