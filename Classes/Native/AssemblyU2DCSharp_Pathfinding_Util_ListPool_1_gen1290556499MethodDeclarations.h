﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen2456319425MethodDeclarations.h"

// System.Void Pathfinding.Util.ListPool`1<Pathfinding.MeshNode>::.cctor()
#define ListPool_1__cctor_m3334974104(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m2656574753_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<Pathfinding.MeshNode>::Claim()
#define ListPool_1_Claim_m1326685412(__this /* static, unused */, method) ((  List_1_t78271701 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_Claim_m2151275825_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<Pathfinding.MeshNode>::Claim(System.Int32)
#define ListPool_1_Claim_m2781420597(__this /* static, unused */, ___capacity0, method) ((  List_1_t78271701 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))ListPool_1_Claim_m3438825730_gshared)(__this /* static, unused */, ___capacity0, method)
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.MeshNode>::Warmup(System.Int32,System.Int32)
#define ListPool_1_Warmup_m1343895223(__this /* static, unused */, ___count0, ___size1, method) ((  void (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))ListPool_1_Warmup_m3738927182_gshared)(__this /* static, unused */, ___count0, ___size1, method)
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.MeshNode>::Release(System.Collections.Generic.List`1<T>)
#define ListPool_1_Release_m2566723058(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, List_1_t78271701 *, const MethodInfo*))ListPool_1_Release_m442610953_gshared)(__this /* static, unused */, ___list0, method)
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.MeshNode>::Clear()
#define ListPool_1_Clear_m2378421216(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_Clear_m3464916023_gshared)(__this /* static, unused */, method)
// System.Int32 Pathfinding.Util.ListPool`1<Pathfinding.MeshNode>::GetSize()
#define ListPool_1_GetSize_m3129720856(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_GetSize_m4059414611_gshared)(__this /* static, unused */, method)
