﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._ce6b223f29cd3d8e11fb5a77643b2966
struct _ce6b223f29cd3d8e11fb5a77643b2966_t538541436;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._ce6b223f29cd3d8e11fb5a77643b2966::.ctor()
extern "C"  void _ce6b223f29cd3d8e11fb5a77643b2966__ctor_m3617133617 (_ce6b223f29cd3d8e11fb5a77643b2966_t538541436 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._ce6b223f29cd3d8e11fb5a77643b2966::_ce6b223f29cd3d8e11fb5a77643b2966m2(System.Int32)
extern "C"  int32_t _ce6b223f29cd3d8e11fb5a77643b2966__ce6b223f29cd3d8e11fb5a77643b2966m2_m1708671001 (_ce6b223f29cd3d8e11fb5a77643b2966_t538541436 * __this, int32_t ____ce6b223f29cd3d8e11fb5a77643b2966a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._ce6b223f29cd3d8e11fb5a77643b2966::_ce6b223f29cd3d8e11fb5a77643b2966m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _ce6b223f29cd3d8e11fb5a77643b2966__ce6b223f29cd3d8e11fb5a77643b2966m_m606451389 (_ce6b223f29cd3d8e11fb5a77643b2966_t538541436 * __this, int32_t ____ce6b223f29cd3d8e11fb5a77643b2966a0, int32_t ____ce6b223f29cd3d8e11fb5a77643b2966361, int32_t ____ce6b223f29cd3d8e11fb5a77643b2966c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
