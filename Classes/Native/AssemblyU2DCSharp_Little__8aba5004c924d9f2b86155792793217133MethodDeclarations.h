﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._8aba5004c924d9f2b8615579bcaf5ac2
struct _8aba5004c924d9f2b8615579bcaf5ac2_t2793217133;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._8aba5004c924d9f2b8615579bcaf5ac2::.ctor()
extern "C"  void _8aba5004c924d9f2b8615579bcaf5ac2__ctor_m495629408 (_8aba5004c924d9f2b8615579bcaf5ac2_t2793217133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8aba5004c924d9f2b8615579bcaf5ac2::_8aba5004c924d9f2b8615579bcaf5ac2m2(System.Int32)
extern "C"  int32_t _8aba5004c924d9f2b8615579bcaf5ac2__8aba5004c924d9f2b8615579bcaf5ac2m2_m3524711033 (_8aba5004c924d9f2b8615579bcaf5ac2_t2793217133 * __this, int32_t ____8aba5004c924d9f2b8615579bcaf5ac2a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._8aba5004c924d9f2b8615579bcaf5ac2::_8aba5004c924d9f2b8615579bcaf5ac2m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _8aba5004c924d9f2b8615579bcaf5ac2__8aba5004c924d9f2b8615579bcaf5ac2m_m4237201501 (_8aba5004c924d9f2b8615579bcaf5ac2_t2793217133 * __this, int32_t ____8aba5004c924d9f2b8615579bcaf5ac2a0, int32_t ____8aba5004c924d9f2b8615579bcaf5ac241, int32_t ____8aba5004c924d9f2b8615579bcaf5ac2c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
