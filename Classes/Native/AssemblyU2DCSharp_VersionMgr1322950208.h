﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// VersionMgr
struct VersionMgr_t1322950208;
// VersionTag
struct VersionTag_t1322956738;
// System.String
struct String_t;
// VersionInfo[]
struct VersionInfoU5BU5D_t1155590563;
// System.Collections.Generic.Dictionary`2<System.String,VersionInfo>
struct Dictionary_2_t3177056456;
// VersionInfo
struct VersionInfo_t2356638086;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// VersionMgr
struct  VersionMgr_t1322950208  : public Il2CppObject
{
public:
	// VersionTag VersionMgr::vsTag
	VersionTag_t1322956738 * ___vsTag_2;
	// System.String VersionMgr::pkgSavePath
	String_t* ___pkgSavePath_3;
	// System.String VersionMgr::rzText
	String_t* ___rzText_4;
	// System.String VersionMgr::app_id
	String_t* ___app_id_5;
	// System.String VersionMgr::cch_id
	String_t* ___cch_id_6;
	// System.String VersionMgr::md_id
	String_t* ___md_id_7;
	// System.String VersionMgr::reyun_app_id
	String_t* ___reyun_app_id_8;
	// System.String VersionMgr::reyun_track_app_id
	String_t* ___reyun_track_app_id_9;
	// System.String VersionMgr::reyun_trackingIO_app_id
	String_t* ___reyun_trackingIO_app_id_10;
	// System.UInt32 VersionMgr::userType
	uint32_t ___userType_11;
	// System.String VersionMgr::show_name
	String_t* ___show_name_12;
	// System.String VersionMgr::pidurl
	String_t* ___pidurl_13;
	// System.String VersionMgr::sdkAppKey
	String_t* ___sdkAppKey_14;
	// System.String VersionMgr::sdkAppid
	String_t* ___sdkAppid_15;
	// System.String VersionMgr::sdkCchid
	String_t* ___sdkCchid_16;
	// System.String VersionMgr::sdkMdid
	String_t* ___sdkMdid_17;
	// System.Boolean VersionMgr::<isSdkLogin>k__BackingField
	bool ___U3CisSdkLoginU3Ek__BackingField_19;
	// System.Boolean VersionMgr::<isWaiwang>k__BackingField
	bool ___U3CisWaiwangU3Ek__BackingField_20;
	// System.Boolean VersionMgr::<isHide2000>k__BackingField
	bool ___U3CisHide2000U3Ek__BackingField_21;
	// System.String VersionMgr::<loadingbgName>k__BackingField
	String_t* ___U3CloadingbgNameU3Ek__BackingField_22;
	// VersionInfo[] VersionMgr::<vsArr>k__BackingField
	VersionInfoU5BU5D_t1155590563* ___U3CvsArrU3Ek__BackingField_23;
	// System.Collections.Generic.Dictionary`2<System.String,VersionInfo> VersionMgr::<vsDic>k__BackingField
	Dictionary_2_t3177056456 * ___U3CvsDicU3Ek__BackingField_24;
	// VersionInfo VersionMgr::<currentVS>k__BackingField
	VersionInfo_t2356638086 * ___U3CcurrentVSU3Ek__BackingField_25;
	// System.Boolean VersionMgr::<isSame>k__BackingField
	bool ___U3CisSameU3Ek__BackingField_26;
	// System.Boolean VersionMgr::<isReview>k__BackingField
	bool ___U3CisReviewU3Ek__BackingField_27;
	// System.Boolean VersionMgr::<isHideGuide>k__BackingField
	bool ___U3CisHideGuideU3Ek__BackingField_28;

public:
	inline static int32_t get_offset_of_vsTag_2() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___vsTag_2)); }
	inline VersionTag_t1322956738 * get_vsTag_2() const { return ___vsTag_2; }
	inline VersionTag_t1322956738 ** get_address_of_vsTag_2() { return &___vsTag_2; }
	inline void set_vsTag_2(VersionTag_t1322956738 * value)
	{
		___vsTag_2 = value;
		Il2CppCodeGenWriteBarrier(&___vsTag_2, value);
	}

	inline static int32_t get_offset_of_pkgSavePath_3() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___pkgSavePath_3)); }
	inline String_t* get_pkgSavePath_3() const { return ___pkgSavePath_3; }
	inline String_t** get_address_of_pkgSavePath_3() { return &___pkgSavePath_3; }
	inline void set_pkgSavePath_3(String_t* value)
	{
		___pkgSavePath_3 = value;
		Il2CppCodeGenWriteBarrier(&___pkgSavePath_3, value);
	}

	inline static int32_t get_offset_of_rzText_4() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___rzText_4)); }
	inline String_t* get_rzText_4() const { return ___rzText_4; }
	inline String_t** get_address_of_rzText_4() { return &___rzText_4; }
	inline void set_rzText_4(String_t* value)
	{
		___rzText_4 = value;
		Il2CppCodeGenWriteBarrier(&___rzText_4, value);
	}

	inline static int32_t get_offset_of_app_id_5() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___app_id_5)); }
	inline String_t* get_app_id_5() const { return ___app_id_5; }
	inline String_t** get_address_of_app_id_5() { return &___app_id_5; }
	inline void set_app_id_5(String_t* value)
	{
		___app_id_5 = value;
		Il2CppCodeGenWriteBarrier(&___app_id_5, value);
	}

	inline static int32_t get_offset_of_cch_id_6() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___cch_id_6)); }
	inline String_t* get_cch_id_6() const { return ___cch_id_6; }
	inline String_t** get_address_of_cch_id_6() { return &___cch_id_6; }
	inline void set_cch_id_6(String_t* value)
	{
		___cch_id_6 = value;
		Il2CppCodeGenWriteBarrier(&___cch_id_6, value);
	}

	inline static int32_t get_offset_of_md_id_7() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___md_id_7)); }
	inline String_t* get_md_id_7() const { return ___md_id_7; }
	inline String_t** get_address_of_md_id_7() { return &___md_id_7; }
	inline void set_md_id_7(String_t* value)
	{
		___md_id_7 = value;
		Il2CppCodeGenWriteBarrier(&___md_id_7, value);
	}

	inline static int32_t get_offset_of_reyun_app_id_8() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___reyun_app_id_8)); }
	inline String_t* get_reyun_app_id_8() const { return ___reyun_app_id_8; }
	inline String_t** get_address_of_reyun_app_id_8() { return &___reyun_app_id_8; }
	inline void set_reyun_app_id_8(String_t* value)
	{
		___reyun_app_id_8 = value;
		Il2CppCodeGenWriteBarrier(&___reyun_app_id_8, value);
	}

	inline static int32_t get_offset_of_reyun_track_app_id_9() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___reyun_track_app_id_9)); }
	inline String_t* get_reyun_track_app_id_9() const { return ___reyun_track_app_id_9; }
	inline String_t** get_address_of_reyun_track_app_id_9() { return &___reyun_track_app_id_9; }
	inline void set_reyun_track_app_id_9(String_t* value)
	{
		___reyun_track_app_id_9 = value;
		Il2CppCodeGenWriteBarrier(&___reyun_track_app_id_9, value);
	}

	inline static int32_t get_offset_of_reyun_trackingIO_app_id_10() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___reyun_trackingIO_app_id_10)); }
	inline String_t* get_reyun_trackingIO_app_id_10() const { return ___reyun_trackingIO_app_id_10; }
	inline String_t** get_address_of_reyun_trackingIO_app_id_10() { return &___reyun_trackingIO_app_id_10; }
	inline void set_reyun_trackingIO_app_id_10(String_t* value)
	{
		___reyun_trackingIO_app_id_10 = value;
		Il2CppCodeGenWriteBarrier(&___reyun_trackingIO_app_id_10, value);
	}

	inline static int32_t get_offset_of_userType_11() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___userType_11)); }
	inline uint32_t get_userType_11() const { return ___userType_11; }
	inline uint32_t* get_address_of_userType_11() { return &___userType_11; }
	inline void set_userType_11(uint32_t value)
	{
		___userType_11 = value;
	}

	inline static int32_t get_offset_of_show_name_12() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___show_name_12)); }
	inline String_t* get_show_name_12() const { return ___show_name_12; }
	inline String_t** get_address_of_show_name_12() { return &___show_name_12; }
	inline void set_show_name_12(String_t* value)
	{
		___show_name_12 = value;
		Il2CppCodeGenWriteBarrier(&___show_name_12, value);
	}

	inline static int32_t get_offset_of_pidurl_13() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___pidurl_13)); }
	inline String_t* get_pidurl_13() const { return ___pidurl_13; }
	inline String_t** get_address_of_pidurl_13() { return &___pidurl_13; }
	inline void set_pidurl_13(String_t* value)
	{
		___pidurl_13 = value;
		Il2CppCodeGenWriteBarrier(&___pidurl_13, value);
	}

	inline static int32_t get_offset_of_sdkAppKey_14() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___sdkAppKey_14)); }
	inline String_t* get_sdkAppKey_14() const { return ___sdkAppKey_14; }
	inline String_t** get_address_of_sdkAppKey_14() { return &___sdkAppKey_14; }
	inline void set_sdkAppKey_14(String_t* value)
	{
		___sdkAppKey_14 = value;
		Il2CppCodeGenWriteBarrier(&___sdkAppKey_14, value);
	}

	inline static int32_t get_offset_of_sdkAppid_15() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___sdkAppid_15)); }
	inline String_t* get_sdkAppid_15() const { return ___sdkAppid_15; }
	inline String_t** get_address_of_sdkAppid_15() { return &___sdkAppid_15; }
	inline void set_sdkAppid_15(String_t* value)
	{
		___sdkAppid_15 = value;
		Il2CppCodeGenWriteBarrier(&___sdkAppid_15, value);
	}

	inline static int32_t get_offset_of_sdkCchid_16() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___sdkCchid_16)); }
	inline String_t* get_sdkCchid_16() const { return ___sdkCchid_16; }
	inline String_t** get_address_of_sdkCchid_16() { return &___sdkCchid_16; }
	inline void set_sdkCchid_16(String_t* value)
	{
		___sdkCchid_16 = value;
		Il2CppCodeGenWriteBarrier(&___sdkCchid_16, value);
	}

	inline static int32_t get_offset_of_sdkMdid_17() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___sdkMdid_17)); }
	inline String_t* get_sdkMdid_17() const { return ___sdkMdid_17; }
	inline String_t** get_address_of_sdkMdid_17() { return &___sdkMdid_17; }
	inline void set_sdkMdid_17(String_t* value)
	{
		___sdkMdid_17 = value;
		Il2CppCodeGenWriteBarrier(&___sdkMdid_17, value);
	}

	inline static int32_t get_offset_of_U3CisSdkLoginU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___U3CisSdkLoginU3Ek__BackingField_19)); }
	inline bool get_U3CisSdkLoginU3Ek__BackingField_19() const { return ___U3CisSdkLoginU3Ek__BackingField_19; }
	inline bool* get_address_of_U3CisSdkLoginU3Ek__BackingField_19() { return &___U3CisSdkLoginU3Ek__BackingField_19; }
	inline void set_U3CisSdkLoginU3Ek__BackingField_19(bool value)
	{
		___U3CisSdkLoginU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CisWaiwangU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___U3CisWaiwangU3Ek__BackingField_20)); }
	inline bool get_U3CisWaiwangU3Ek__BackingField_20() const { return ___U3CisWaiwangU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CisWaiwangU3Ek__BackingField_20() { return &___U3CisWaiwangU3Ek__BackingField_20; }
	inline void set_U3CisWaiwangU3Ek__BackingField_20(bool value)
	{
		___U3CisWaiwangU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CisHide2000U3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___U3CisHide2000U3Ek__BackingField_21)); }
	inline bool get_U3CisHide2000U3Ek__BackingField_21() const { return ___U3CisHide2000U3Ek__BackingField_21; }
	inline bool* get_address_of_U3CisHide2000U3Ek__BackingField_21() { return &___U3CisHide2000U3Ek__BackingField_21; }
	inline void set_U3CisHide2000U3Ek__BackingField_21(bool value)
	{
		___U3CisHide2000U3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CloadingbgNameU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___U3CloadingbgNameU3Ek__BackingField_22)); }
	inline String_t* get_U3CloadingbgNameU3Ek__BackingField_22() const { return ___U3CloadingbgNameU3Ek__BackingField_22; }
	inline String_t** get_address_of_U3CloadingbgNameU3Ek__BackingField_22() { return &___U3CloadingbgNameU3Ek__BackingField_22; }
	inline void set_U3CloadingbgNameU3Ek__BackingField_22(String_t* value)
	{
		___U3CloadingbgNameU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier(&___U3CloadingbgNameU3Ek__BackingField_22, value);
	}

	inline static int32_t get_offset_of_U3CvsArrU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___U3CvsArrU3Ek__BackingField_23)); }
	inline VersionInfoU5BU5D_t1155590563* get_U3CvsArrU3Ek__BackingField_23() const { return ___U3CvsArrU3Ek__BackingField_23; }
	inline VersionInfoU5BU5D_t1155590563** get_address_of_U3CvsArrU3Ek__BackingField_23() { return &___U3CvsArrU3Ek__BackingField_23; }
	inline void set_U3CvsArrU3Ek__BackingField_23(VersionInfoU5BU5D_t1155590563* value)
	{
		___U3CvsArrU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier(&___U3CvsArrU3Ek__BackingField_23, value);
	}

	inline static int32_t get_offset_of_U3CvsDicU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___U3CvsDicU3Ek__BackingField_24)); }
	inline Dictionary_2_t3177056456 * get_U3CvsDicU3Ek__BackingField_24() const { return ___U3CvsDicU3Ek__BackingField_24; }
	inline Dictionary_2_t3177056456 ** get_address_of_U3CvsDicU3Ek__BackingField_24() { return &___U3CvsDicU3Ek__BackingField_24; }
	inline void set_U3CvsDicU3Ek__BackingField_24(Dictionary_2_t3177056456 * value)
	{
		___U3CvsDicU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier(&___U3CvsDicU3Ek__BackingField_24, value);
	}

	inline static int32_t get_offset_of_U3CcurrentVSU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___U3CcurrentVSU3Ek__BackingField_25)); }
	inline VersionInfo_t2356638086 * get_U3CcurrentVSU3Ek__BackingField_25() const { return ___U3CcurrentVSU3Ek__BackingField_25; }
	inline VersionInfo_t2356638086 ** get_address_of_U3CcurrentVSU3Ek__BackingField_25() { return &___U3CcurrentVSU3Ek__BackingField_25; }
	inline void set_U3CcurrentVSU3Ek__BackingField_25(VersionInfo_t2356638086 * value)
	{
		___U3CcurrentVSU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcurrentVSU3Ek__BackingField_25, value);
	}

	inline static int32_t get_offset_of_U3CisSameU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___U3CisSameU3Ek__BackingField_26)); }
	inline bool get_U3CisSameU3Ek__BackingField_26() const { return ___U3CisSameU3Ek__BackingField_26; }
	inline bool* get_address_of_U3CisSameU3Ek__BackingField_26() { return &___U3CisSameU3Ek__BackingField_26; }
	inline void set_U3CisSameU3Ek__BackingField_26(bool value)
	{
		___U3CisSameU3Ek__BackingField_26 = value;
	}

	inline static int32_t get_offset_of_U3CisReviewU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___U3CisReviewU3Ek__BackingField_27)); }
	inline bool get_U3CisReviewU3Ek__BackingField_27() const { return ___U3CisReviewU3Ek__BackingField_27; }
	inline bool* get_address_of_U3CisReviewU3Ek__BackingField_27() { return &___U3CisReviewU3Ek__BackingField_27; }
	inline void set_U3CisReviewU3Ek__BackingField_27(bool value)
	{
		___U3CisReviewU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3CisHideGuideU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208, ___U3CisHideGuideU3Ek__BackingField_28)); }
	inline bool get_U3CisHideGuideU3Ek__BackingField_28() const { return ___U3CisHideGuideU3Ek__BackingField_28; }
	inline bool* get_address_of_U3CisHideGuideU3Ek__BackingField_28() { return &___U3CisHideGuideU3Ek__BackingField_28; }
	inline void set_U3CisHideGuideU3Ek__BackingField_28(bool value)
	{
		___U3CisHideGuideU3Ek__BackingField_28 = value;
	}
};

struct VersionMgr_t1322950208_StaticFields
{
public:
	// VersionMgr VersionMgr::_instance
	VersionMgr_t1322950208 * ____instance_0;
	// System.Boolean VersionMgr::enabled
	bool ___enabled_1;
	// System.Boolean VersionMgr::isIosFirst
	bool ___isIosFirst_18;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208_StaticFields, ____instance_0)); }
	inline VersionMgr_t1322950208 * get__instance_0() const { return ____instance_0; }
	inline VersionMgr_t1322950208 ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(VersionMgr_t1322950208 * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier(&____instance_0, value);
	}

	inline static int32_t get_offset_of_enabled_1() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208_StaticFields, ___enabled_1)); }
	inline bool get_enabled_1() const { return ___enabled_1; }
	inline bool* get_address_of_enabled_1() { return &___enabled_1; }
	inline void set_enabled_1(bool value)
	{
		___enabled_1 = value;
	}

	inline static int32_t get_offset_of_isIosFirst_18() { return static_cast<int32_t>(offsetof(VersionMgr_t1322950208_StaticFields, ___isIosFirst_18)); }
	inline bool get_isIosFirst_18() const { return ___isIosFirst_18; }
	inline bool* get_address_of_isIosFirst_18() { return &___isIosFirst_18; }
	inline void set_isIosFirst_18(bool value)
	{
		___isIosFirst_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
