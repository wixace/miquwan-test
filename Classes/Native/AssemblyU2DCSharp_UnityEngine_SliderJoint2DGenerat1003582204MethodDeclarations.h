﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_SliderJoint2DGenerated
struct UnityEngine_SliderJoint2DGenerated_t1003582204;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_SliderJoint2DGenerated::.ctor()
extern "C"  void UnityEngine_SliderJoint2DGenerated__ctor_m3726440735 (UnityEngine_SliderJoint2DGenerated_t1003582204 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SliderJoint2DGenerated::SliderJoint2D_SliderJoint2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SliderJoint2DGenerated_SliderJoint2D_SliderJoint2D1_m795469443 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SliderJoint2DGenerated::SliderJoint2D_autoConfigureAngle(JSVCall)
extern "C"  void UnityEngine_SliderJoint2DGenerated_SliderJoint2D_autoConfigureAngle_m323088418 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SliderJoint2DGenerated::SliderJoint2D_angle(JSVCall)
extern "C"  void UnityEngine_SliderJoint2DGenerated_SliderJoint2D_angle_m3974931163 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SliderJoint2DGenerated::SliderJoint2D_useMotor(JSVCall)
extern "C"  void UnityEngine_SliderJoint2DGenerated_SliderJoint2D_useMotor_m2810821712 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SliderJoint2DGenerated::SliderJoint2D_useLimits(JSVCall)
extern "C"  void UnityEngine_SliderJoint2DGenerated_SliderJoint2D_useLimits_m3560602799 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SliderJoint2DGenerated::SliderJoint2D_motor(JSVCall)
extern "C"  void UnityEngine_SliderJoint2DGenerated_SliderJoint2D_motor_m4232272249 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SliderJoint2DGenerated::SliderJoint2D_limits(JSVCall)
extern "C"  void UnityEngine_SliderJoint2DGenerated_SliderJoint2D_limits_m380929190 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SliderJoint2DGenerated::SliderJoint2D_limitState(JSVCall)
extern "C"  void UnityEngine_SliderJoint2DGenerated_SliderJoint2D_limitState_m1761869032 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SliderJoint2DGenerated::SliderJoint2D_referenceAngle(JSVCall)
extern "C"  void UnityEngine_SliderJoint2DGenerated_SliderJoint2D_referenceAngle_m2173406838 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SliderJoint2DGenerated::SliderJoint2D_jointTranslation(JSVCall)
extern "C"  void UnityEngine_SliderJoint2DGenerated_SliderJoint2D_jointTranslation_m2468906967 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SliderJoint2DGenerated::SliderJoint2D_jointSpeed(JSVCall)
extern "C"  void UnityEngine_SliderJoint2DGenerated_SliderJoint2D_jointSpeed_m1163326593 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SliderJoint2DGenerated::SliderJoint2D_GetMotorForce__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_SliderJoint2DGenerated_SliderJoint2D_GetMotorForce__Single_m3744253865 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SliderJoint2DGenerated::__Register()
extern "C"  void UnityEngine_SliderJoint2DGenerated___Register_m345923336 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_SliderJoint2DGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_SliderJoint2DGenerated_ilo_attachFinalizerObject1_m1331337632 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_SliderJoint2DGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_SliderJoint2DGenerated_ilo_getObject2_m1840189617 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_SliderJoint2DGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_SliderJoint2DGenerated_ilo_setObject3_m2703832013 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SliderJoint2DGenerated::ilo_setEnum4(System.Int32,System.Int32)
extern "C"  void UnityEngine_SliderJoint2DGenerated_ilo_setEnum4_m502391005 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_SliderJoint2DGenerated::ilo_setSingle5(System.Int32,System.Single)
extern "C"  void UnityEngine_SliderJoint2DGenerated_ilo_setSingle5_m63366393 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_SliderJoint2DGenerated::ilo_getSingle6(System.Int32)
extern "C"  float UnityEngine_SliderJoint2DGenerated_ilo_getSingle6_m182862253 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
