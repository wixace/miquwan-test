﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Collections.Hashtable
struct Hashtable_t1407064410;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Common.JSONTokener
struct  JSONTokener_t276688996  : public Il2CppObject
{
public:
	// System.String Common.JSONTokener::ins
	String_t* ___ins_0;
	// System.Collections.Hashtable Common.JSONTokener::nameValuePairs
	Hashtable_t1407064410 * ___nameValuePairs_1;
	// System.Int32 Common.JSONTokener::pos
	int32_t ___pos_2;

public:
	inline static int32_t get_offset_of_ins_0() { return static_cast<int32_t>(offsetof(JSONTokener_t276688996, ___ins_0)); }
	inline String_t* get_ins_0() const { return ___ins_0; }
	inline String_t** get_address_of_ins_0() { return &___ins_0; }
	inline void set_ins_0(String_t* value)
	{
		___ins_0 = value;
		Il2CppCodeGenWriteBarrier(&___ins_0, value);
	}

	inline static int32_t get_offset_of_nameValuePairs_1() { return static_cast<int32_t>(offsetof(JSONTokener_t276688996, ___nameValuePairs_1)); }
	inline Hashtable_t1407064410 * get_nameValuePairs_1() const { return ___nameValuePairs_1; }
	inline Hashtable_t1407064410 ** get_address_of_nameValuePairs_1() { return &___nameValuePairs_1; }
	inline void set_nameValuePairs_1(Hashtable_t1407064410 * value)
	{
		___nameValuePairs_1 = value;
		Il2CppCodeGenWriteBarrier(&___nameValuePairs_1, value);
	}

	inline static int32_t get_offset_of_pos_2() { return static_cast<int32_t>(offsetof(JSONTokener_t276688996, ___pos_2)); }
	inline int32_t get_pos_2() const { return ___pos_2; }
	inline int32_t* get_address_of_pos_2() { return &___pos_2; }
	inline void set_pos_2(int32_t value)
	{
		___pos_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
