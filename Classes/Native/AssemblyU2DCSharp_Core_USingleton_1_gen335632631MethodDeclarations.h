﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Core_USingleton_1_gen2177109755MethodDeclarations.h"

// System.Void Core.USingleton`1<PuzzleManager>::.ctor()
#define USingleton_1__ctor_m3456921702(__this, method) ((  void (*) (USingleton_1_t335632631 *, const MethodInfo*))USingleton_1__ctor_m3702815765_gshared)(__this, method)
// System.Void Core.USingleton`1<PuzzleManager>::.cctor()
#define USingleton_1__cctor_m3603261447(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))USingleton_1__cctor_m2636042808_gshared)(__this /* static, unused */, method)
// T Core.USingleton`1<PuzzleManager>::get_Instance()
#define USingleton_1_get_Instance_m3781985403(__this /* static, unused */, method) ((  PuzzleManager_t2329339247 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))USingleton_1_get_Instance_m899663340_gshared)(__this /* static, unused */, method)
// System.Void Core.USingleton`1<PuzzleManager>::set_Instance(T)
#define USingleton_1_set_Instance_m880635118(__this /* static, unused */, ___value0, method) ((  void (*) (Il2CppObject * /* static, unused */, PuzzleManager_t2329339247 *, const MethodInfo*))USingleton_1_set_Instance_m1722964381_gshared)(__this /* static, unused */, ___value0, method)
