﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.UInt64Serializer
struct UInt64Serializer_t3321882308;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"

// System.Void ProtoBuf.Serializers.UInt64Serializer::.ctor(ProtoBuf.Meta.TypeModel)
extern "C"  void UInt64Serializer__ctor_m751396280 (UInt64Serializer_t3321882308 * __this, TypeModel_t2730011105 * ___model0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.UInt64Serializer::.cctor()
extern "C"  void UInt64Serializer__cctor_m296464188 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.UInt64Serializer::ProtoBuf.Serializers.IProtoSerializer.get_RequiresOldValue()
extern "C"  bool UInt64Serializer_ProtoBuf_Serializers_IProtoSerializer_get_RequiresOldValue_m1287797869 (UInt64Serializer_t3321882308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.UInt64Serializer::ProtoBuf.Serializers.IProtoSerializer.get_ReturnsValue()
extern "C"  bool UInt64Serializer_ProtoBuf_Serializers_IProtoSerializer_get_ReturnsValue_m1841261955 (UInt64Serializer_t3321882308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.UInt64Serializer::get_ExpectedType()
extern "C"  Type_t * UInt64Serializer_get_ExpectedType_m1271179604 (UInt64Serializer_t3321882308 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.UInt64Serializer::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * UInt64Serializer_Read_m2244125246 (UInt64Serializer_t3321882308 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.UInt64Serializer::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void UInt64Serializer_Write_m53597128 (UInt64Serializer_t3321882308 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
