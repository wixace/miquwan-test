﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ImageEffectBase
struct ImageEffectBase_t3731393437;
// UnityEngine.Material
struct Material_t3870600107;

#include "codegen/il2cpp-codegen.h"

// System.Void ImageEffectBase::.ctor()
extern "C"  void ImageEffectBase__ctor_m2296049466 (ImageEffectBase_t3731393437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageEffectBase::Start()
extern "C"  void ImageEffectBase_Start_m1243187258 (ImageEffectBase_t3731393437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material ImageEffectBase::get_material()
extern "C"  Material_t3870600107 * ImageEffectBase_get_material_m2726075187 (ImageEffectBase_t3731393437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ImageEffectBase::OnDisable()
extern "C"  void ImageEffectBase_OnDisable_m3305379489 (ImageEffectBase_t3731393437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
