﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._6e3781b05aefe221cf8c75d23881c76d
struct _6e3781b05aefe221cf8c75d23881c76d_t184429126;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._6e3781b05aefe221cf8c75d23881c76d::.ctor()
extern "C"  void _6e3781b05aefe221cf8c75d23881c76d__ctor_m120138407 (_6e3781b05aefe221cf8c75d23881c76d_t184429126 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6e3781b05aefe221cf8c75d23881c76d::_6e3781b05aefe221cf8c75d23881c76dm2(System.Int32)
extern "C"  int32_t _6e3781b05aefe221cf8c75d23881c76d__6e3781b05aefe221cf8c75d23881c76dm2_m3609967577 (_6e3781b05aefe221cf8c75d23881c76d_t184429126 * __this, int32_t ____6e3781b05aefe221cf8c75d23881c76da0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6e3781b05aefe221cf8c75d23881c76d::_6e3781b05aefe221cf8c75d23881c76dm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _6e3781b05aefe221cf8c75d23881c76d__6e3781b05aefe221cf8c75d23881c76dm_m2485187325 (_6e3781b05aefe221cf8c75d23881c76d_t184429126 * __this, int32_t ____6e3781b05aefe221cf8c75d23881c76da0, int32_t ____6e3781b05aefe221cf8c75d23881c76d771, int32_t ____6e3781b05aefe221cf8c75d23881c76dc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
