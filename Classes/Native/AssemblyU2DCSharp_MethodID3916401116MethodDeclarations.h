﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MethodID
struct MethodID_t3916401116;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// TypeFlag[]
struct TypeFlagU5BU5D_t693965763;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_TypeFlag3682875878.h"

// System.Void MethodID::.ctor(System.String,System.String,TypeFlag,System.String[],TypeFlag[],System.Int32[])
extern "C"  void MethodID__ctor_m3467459762 (MethodID_t3916401116 * __this, String_t* ___name0, String_t* ___retTypeName1, int32_t ___returnTypeFlag2, StringU5BU5D_t4054002952* ___parameterTypes3, TypeFlagU5BU5D_t693965763* ___typeFlags4, Int32U5BU5D_t3230847821* ___genericParameterPositions5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
