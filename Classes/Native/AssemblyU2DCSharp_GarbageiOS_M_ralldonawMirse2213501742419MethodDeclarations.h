﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_ralldonawMirse221
struct M_ralldonawMirse221_t3501742419;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_ralldonawMirse221::.ctor()
extern "C"  void M_ralldonawMirse221__ctor_m787414384 (M_ralldonawMirse221_t3501742419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_ralldonawMirse221::M_wameSefi0(System.String[],System.Int32)
extern "C"  void M_ralldonawMirse221_M_wameSefi0_m1938239212 (M_ralldonawMirse221_t3501742419 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
