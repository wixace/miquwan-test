﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// CSSkillData[]
struct CSSkillDataU5BU5D_t1071816810;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// herosCfg
struct herosCfg_t3676934635;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_HERO_STATE3944453100.h"
#include "AssemblyU2DCSharp_HERO_LINEUP1808772468.h"
#include "AssemblyU2DCSharp_HERO_EVOLVE_LEVEL3342825031.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CSHeroUnit
struct  CSHeroUnit_t3764358446  : public Il2CppObject
{
public:
	// System.String CSHeroUnit::uuid
	String_t* ___uuid_0;
	// System.UInt32 CSHeroUnit::_id
	uint32_t ____id_1;
	// System.String CSHeroUnit::NameQty
	String_t* ___NameQty_2;
	// HERO_STATE CSHeroUnit::state
	int32_t ___state_3;
	// HERO_LINEUP CSHeroUnit::lineup
	int32_t ___lineup_4;
	// HERO_EVOLVE_LEVEL CSHeroUnit::quatily
	int32_t ___quatily_5;
	// CSSkillData[] CSHeroUnit::skillList
	CSSkillDataU5BU5D_t1071816810* ___skillList_6;
	// System.Int32[] CSHeroUnit::godEquipAwakenSkillIDs
	Int32U5BU5D_t3230847821* ___godEquipAwakenSkillIDs_7;
	// System.Int32[] CSHeroUnit::godEquipAwakenSkillGradeIDs
	Int32U5BU5D_t3230847821* ___godEquipAwakenSkillGradeIDs_8;
	// System.Single CSHeroUnit::atkrange
	float ___atkrange_9;
	// System.Single CSHeroUnit::movespeed
	float ___movespeed_10;
	// herosCfg CSHeroUnit::_config
	herosCfg_t3676934635 * ____config_11;

public:
	inline static int32_t get_offset_of_uuid_0() { return static_cast<int32_t>(offsetof(CSHeroUnit_t3764358446, ___uuid_0)); }
	inline String_t* get_uuid_0() const { return ___uuid_0; }
	inline String_t** get_address_of_uuid_0() { return &___uuid_0; }
	inline void set_uuid_0(String_t* value)
	{
		___uuid_0 = value;
		Il2CppCodeGenWriteBarrier(&___uuid_0, value);
	}

	inline static int32_t get_offset_of__id_1() { return static_cast<int32_t>(offsetof(CSHeroUnit_t3764358446, ____id_1)); }
	inline uint32_t get__id_1() const { return ____id_1; }
	inline uint32_t* get_address_of__id_1() { return &____id_1; }
	inline void set__id_1(uint32_t value)
	{
		____id_1 = value;
	}

	inline static int32_t get_offset_of_NameQty_2() { return static_cast<int32_t>(offsetof(CSHeroUnit_t3764358446, ___NameQty_2)); }
	inline String_t* get_NameQty_2() const { return ___NameQty_2; }
	inline String_t** get_address_of_NameQty_2() { return &___NameQty_2; }
	inline void set_NameQty_2(String_t* value)
	{
		___NameQty_2 = value;
		Il2CppCodeGenWriteBarrier(&___NameQty_2, value);
	}

	inline static int32_t get_offset_of_state_3() { return static_cast<int32_t>(offsetof(CSHeroUnit_t3764358446, ___state_3)); }
	inline int32_t get_state_3() const { return ___state_3; }
	inline int32_t* get_address_of_state_3() { return &___state_3; }
	inline void set_state_3(int32_t value)
	{
		___state_3 = value;
	}

	inline static int32_t get_offset_of_lineup_4() { return static_cast<int32_t>(offsetof(CSHeroUnit_t3764358446, ___lineup_4)); }
	inline int32_t get_lineup_4() const { return ___lineup_4; }
	inline int32_t* get_address_of_lineup_4() { return &___lineup_4; }
	inline void set_lineup_4(int32_t value)
	{
		___lineup_4 = value;
	}

	inline static int32_t get_offset_of_quatily_5() { return static_cast<int32_t>(offsetof(CSHeroUnit_t3764358446, ___quatily_5)); }
	inline int32_t get_quatily_5() const { return ___quatily_5; }
	inline int32_t* get_address_of_quatily_5() { return &___quatily_5; }
	inline void set_quatily_5(int32_t value)
	{
		___quatily_5 = value;
	}

	inline static int32_t get_offset_of_skillList_6() { return static_cast<int32_t>(offsetof(CSHeroUnit_t3764358446, ___skillList_6)); }
	inline CSSkillDataU5BU5D_t1071816810* get_skillList_6() const { return ___skillList_6; }
	inline CSSkillDataU5BU5D_t1071816810** get_address_of_skillList_6() { return &___skillList_6; }
	inline void set_skillList_6(CSSkillDataU5BU5D_t1071816810* value)
	{
		___skillList_6 = value;
		Il2CppCodeGenWriteBarrier(&___skillList_6, value);
	}

	inline static int32_t get_offset_of_godEquipAwakenSkillIDs_7() { return static_cast<int32_t>(offsetof(CSHeroUnit_t3764358446, ___godEquipAwakenSkillIDs_7)); }
	inline Int32U5BU5D_t3230847821* get_godEquipAwakenSkillIDs_7() const { return ___godEquipAwakenSkillIDs_7; }
	inline Int32U5BU5D_t3230847821** get_address_of_godEquipAwakenSkillIDs_7() { return &___godEquipAwakenSkillIDs_7; }
	inline void set_godEquipAwakenSkillIDs_7(Int32U5BU5D_t3230847821* value)
	{
		___godEquipAwakenSkillIDs_7 = value;
		Il2CppCodeGenWriteBarrier(&___godEquipAwakenSkillIDs_7, value);
	}

	inline static int32_t get_offset_of_godEquipAwakenSkillGradeIDs_8() { return static_cast<int32_t>(offsetof(CSHeroUnit_t3764358446, ___godEquipAwakenSkillGradeIDs_8)); }
	inline Int32U5BU5D_t3230847821* get_godEquipAwakenSkillGradeIDs_8() const { return ___godEquipAwakenSkillGradeIDs_8; }
	inline Int32U5BU5D_t3230847821** get_address_of_godEquipAwakenSkillGradeIDs_8() { return &___godEquipAwakenSkillGradeIDs_8; }
	inline void set_godEquipAwakenSkillGradeIDs_8(Int32U5BU5D_t3230847821* value)
	{
		___godEquipAwakenSkillGradeIDs_8 = value;
		Il2CppCodeGenWriteBarrier(&___godEquipAwakenSkillGradeIDs_8, value);
	}

	inline static int32_t get_offset_of_atkrange_9() { return static_cast<int32_t>(offsetof(CSHeroUnit_t3764358446, ___atkrange_9)); }
	inline float get_atkrange_9() const { return ___atkrange_9; }
	inline float* get_address_of_atkrange_9() { return &___atkrange_9; }
	inline void set_atkrange_9(float value)
	{
		___atkrange_9 = value;
	}

	inline static int32_t get_offset_of_movespeed_10() { return static_cast<int32_t>(offsetof(CSHeroUnit_t3764358446, ___movespeed_10)); }
	inline float get_movespeed_10() const { return ___movespeed_10; }
	inline float* get_address_of_movespeed_10() { return &___movespeed_10; }
	inline void set_movespeed_10(float value)
	{
		___movespeed_10 = value;
	}

	inline static int32_t get_offset_of__config_11() { return static_cast<int32_t>(offsetof(CSHeroUnit_t3764358446, ____config_11)); }
	inline herosCfg_t3676934635 * get__config_11() const { return ____config_11; }
	inline herosCfg_t3676934635 ** get_address_of__config_11() { return &____config_11; }
	inline void set__config_11(herosCfg_t3676934635 * value)
	{
		____config_11 = value;
		Il2CppCodeGenWriteBarrier(&____config_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
