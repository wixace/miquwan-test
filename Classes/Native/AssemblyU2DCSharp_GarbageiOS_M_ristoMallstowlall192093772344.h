﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_ristoMallstowlall199
struct  M_ristoMallstowlall199_t2093772344  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_ristoMallstowlall199::_rarusis
	bool ____rarusis_0;
	// System.Single GarbageiOS.M_ristoMallstowlall199::_gemnebel
	float ____gemnebel_1;
	// System.Int32 GarbageiOS.M_ristoMallstowlall199::_gearsellemGoumairkou
	int32_t ____gearsellemGoumairkou_2;
	// System.UInt32 GarbageiOS.M_ristoMallstowlall199::_jersosoo
	uint32_t ____jersosoo_3;
	// System.Int32 GarbageiOS.M_ristoMallstowlall199::_sowurgall
	int32_t ____sowurgall_4;
	// System.UInt32 GarbageiOS.M_ristoMallstowlall199::_leanelar
	uint32_t ____leanelar_5;
	// System.String GarbageiOS.M_ristoMallstowlall199::_moucharcou
	String_t* ____moucharcou_6;
	// System.String GarbageiOS.M_ristoMallstowlall199::_werezusel
	String_t* ____werezusel_7;
	// System.String GarbageiOS.M_ristoMallstowlall199::_rellocearHele
	String_t* ____rellocearHele_8;
	// System.Boolean GarbageiOS.M_ristoMallstowlall199::_horxowjallJarno
	bool ____horxowjallJarno_9;
	// System.String GarbageiOS.M_ristoMallstowlall199::_sturkouriHuna
	String_t* ____sturkouriHuna_10;
	// System.Int32 GarbageiOS.M_ristoMallstowlall199::_hircarai
	int32_t ____hircarai_11;
	// System.Int32 GarbageiOS.M_ristoMallstowlall199::_sawmafeSerefe
	int32_t ____sawmafeSerefe_12;
	// System.UInt32 GarbageiOS.M_ristoMallstowlall199::_ribi
	uint32_t ____ribi_13;
	// System.String GarbageiOS.M_ristoMallstowlall199::_whoustumou
	String_t* ____whoustumou_14;
	// System.Int32 GarbageiOS.M_ristoMallstowlall199::_hirjalCegis
	int32_t ____hirjalCegis_15;
	// System.Int32 GarbageiOS.M_ristoMallstowlall199::_cerepeeborPermosa
	int32_t ____cerepeeborPermosa_16;
	// System.UInt32 GarbageiOS.M_ristoMallstowlall199::_bemra
	uint32_t ____bemra_17;
	// System.Int32 GarbageiOS.M_ristoMallstowlall199::_laleastereHanailay
	int32_t ____laleastereHanailay_18;
	// System.Single GarbageiOS.M_ristoMallstowlall199::_carpisSaysoo
	float ____carpisSaysoo_19;

public:
	inline static int32_t get_offset_of__rarusis_0() { return static_cast<int32_t>(offsetof(M_ristoMallstowlall199_t2093772344, ____rarusis_0)); }
	inline bool get__rarusis_0() const { return ____rarusis_0; }
	inline bool* get_address_of__rarusis_0() { return &____rarusis_0; }
	inline void set__rarusis_0(bool value)
	{
		____rarusis_0 = value;
	}

	inline static int32_t get_offset_of__gemnebel_1() { return static_cast<int32_t>(offsetof(M_ristoMallstowlall199_t2093772344, ____gemnebel_1)); }
	inline float get__gemnebel_1() const { return ____gemnebel_1; }
	inline float* get_address_of__gemnebel_1() { return &____gemnebel_1; }
	inline void set__gemnebel_1(float value)
	{
		____gemnebel_1 = value;
	}

	inline static int32_t get_offset_of__gearsellemGoumairkou_2() { return static_cast<int32_t>(offsetof(M_ristoMallstowlall199_t2093772344, ____gearsellemGoumairkou_2)); }
	inline int32_t get__gearsellemGoumairkou_2() const { return ____gearsellemGoumairkou_2; }
	inline int32_t* get_address_of__gearsellemGoumairkou_2() { return &____gearsellemGoumairkou_2; }
	inline void set__gearsellemGoumairkou_2(int32_t value)
	{
		____gearsellemGoumairkou_2 = value;
	}

	inline static int32_t get_offset_of__jersosoo_3() { return static_cast<int32_t>(offsetof(M_ristoMallstowlall199_t2093772344, ____jersosoo_3)); }
	inline uint32_t get__jersosoo_3() const { return ____jersosoo_3; }
	inline uint32_t* get_address_of__jersosoo_3() { return &____jersosoo_3; }
	inline void set__jersosoo_3(uint32_t value)
	{
		____jersosoo_3 = value;
	}

	inline static int32_t get_offset_of__sowurgall_4() { return static_cast<int32_t>(offsetof(M_ristoMallstowlall199_t2093772344, ____sowurgall_4)); }
	inline int32_t get__sowurgall_4() const { return ____sowurgall_4; }
	inline int32_t* get_address_of__sowurgall_4() { return &____sowurgall_4; }
	inline void set__sowurgall_4(int32_t value)
	{
		____sowurgall_4 = value;
	}

	inline static int32_t get_offset_of__leanelar_5() { return static_cast<int32_t>(offsetof(M_ristoMallstowlall199_t2093772344, ____leanelar_5)); }
	inline uint32_t get__leanelar_5() const { return ____leanelar_5; }
	inline uint32_t* get_address_of__leanelar_5() { return &____leanelar_5; }
	inline void set__leanelar_5(uint32_t value)
	{
		____leanelar_5 = value;
	}

	inline static int32_t get_offset_of__moucharcou_6() { return static_cast<int32_t>(offsetof(M_ristoMallstowlall199_t2093772344, ____moucharcou_6)); }
	inline String_t* get__moucharcou_6() const { return ____moucharcou_6; }
	inline String_t** get_address_of__moucharcou_6() { return &____moucharcou_6; }
	inline void set__moucharcou_6(String_t* value)
	{
		____moucharcou_6 = value;
		Il2CppCodeGenWriteBarrier(&____moucharcou_6, value);
	}

	inline static int32_t get_offset_of__werezusel_7() { return static_cast<int32_t>(offsetof(M_ristoMallstowlall199_t2093772344, ____werezusel_7)); }
	inline String_t* get__werezusel_7() const { return ____werezusel_7; }
	inline String_t** get_address_of__werezusel_7() { return &____werezusel_7; }
	inline void set__werezusel_7(String_t* value)
	{
		____werezusel_7 = value;
		Il2CppCodeGenWriteBarrier(&____werezusel_7, value);
	}

	inline static int32_t get_offset_of__rellocearHele_8() { return static_cast<int32_t>(offsetof(M_ristoMallstowlall199_t2093772344, ____rellocearHele_8)); }
	inline String_t* get__rellocearHele_8() const { return ____rellocearHele_8; }
	inline String_t** get_address_of__rellocearHele_8() { return &____rellocearHele_8; }
	inline void set__rellocearHele_8(String_t* value)
	{
		____rellocearHele_8 = value;
		Il2CppCodeGenWriteBarrier(&____rellocearHele_8, value);
	}

	inline static int32_t get_offset_of__horxowjallJarno_9() { return static_cast<int32_t>(offsetof(M_ristoMallstowlall199_t2093772344, ____horxowjallJarno_9)); }
	inline bool get__horxowjallJarno_9() const { return ____horxowjallJarno_9; }
	inline bool* get_address_of__horxowjallJarno_9() { return &____horxowjallJarno_9; }
	inline void set__horxowjallJarno_9(bool value)
	{
		____horxowjallJarno_9 = value;
	}

	inline static int32_t get_offset_of__sturkouriHuna_10() { return static_cast<int32_t>(offsetof(M_ristoMallstowlall199_t2093772344, ____sturkouriHuna_10)); }
	inline String_t* get__sturkouriHuna_10() const { return ____sturkouriHuna_10; }
	inline String_t** get_address_of__sturkouriHuna_10() { return &____sturkouriHuna_10; }
	inline void set__sturkouriHuna_10(String_t* value)
	{
		____sturkouriHuna_10 = value;
		Il2CppCodeGenWriteBarrier(&____sturkouriHuna_10, value);
	}

	inline static int32_t get_offset_of__hircarai_11() { return static_cast<int32_t>(offsetof(M_ristoMallstowlall199_t2093772344, ____hircarai_11)); }
	inline int32_t get__hircarai_11() const { return ____hircarai_11; }
	inline int32_t* get_address_of__hircarai_11() { return &____hircarai_11; }
	inline void set__hircarai_11(int32_t value)
	{
		____hircarai_11 = value;
	}

	inline static int32_t get_offset_of__sawmafeSerefe_12() { return static_cast<int32_t>(offsetof(M_ristoMallstowlall199_t2093772344, ____sawmafeSerefe_12)); }
	inline int32_t get__sawmafeSerefe_12() const { return ____sawmafeSerefe_12; }
	inline int32_t* get_address_of__sawmafeSerefe_12() { return &____sawmafeSerefe_12; }
	inline void set__sawmafeSerefe_12(int32_t value)
	{
		____sawmafeSerefe_12 = value;
	}

	inline static int32_t get_offset_of__ribi_13() { return static_cast<int32_t>(offsetof(M_ristoMallstowlall199_t2093772344, ____ribi_13)); }
	inline uint32_t get__ribi_13() const { return ____ribi_13; }
	inline uint32_t* get_address_of__ribi_13() { return &____ribi_13; }
	inline void set__ribi_13(uint32_t value)
	{
		____ribi_13 = value;
	}

	inline static int32_t get_offset_of__whoustumou_14() { return static_cast<int32_t>(offsetof(M_ristoMallstowlall199_t2093772344, ____whoustumou_14)); }
	inline String_t* get__whoustumou_14() const { return ____whoustumou_14; }
	inline String_t** get_address_of__whoustumou_14() { return &____whoustumou_14; }
	inline void set__whoustumou_14(String_t* value)
	{
		____whoustumou_14 = value;
		Il2CppCodeGenWriteBarrier(&____whoustumou_14, value);
	}

	inline static int32_t get_offset_of__hirjalCegis_15() { return static_cast<int32_t>(offsetof(M_ristoMallstowlall199_t2093772344, ____hirjalCegis_15)); }
	inline int32_t get__hirjalCegis_15() const { return ____hirjalCegis_15; }
	inline int32_t* get_address_of__hirjalCegis_15() { return &____hirjalCegis_15; }
	inline void set__hirjalCegis_15(int32_t value)
	{
		____hirjalCegis_15 = value;
	}

	inline static int32_t get_offset_of__cerepeeborPermosa_16() { return static_cast<int32_t>(offsetof(M_ristoMallstowlall199_t2093772344, ____cerepeeborPermosa_16)); }
	inline int32_t get__cerepeeborPermosa_16() const { return ____cerepeeborPermosa_16; }
	inline int32_t* get_address_of__cerepeeborPermosa_16() { return &____cerepeeborPermosa_16; }
	inline void set__cerepeeborPermosa_16(int32_t value)
	{
		____cerepeeborPermosa_16 = value;
	}

	inline static int32_t get_offset_of__bemra_17() { return static_cast<int32_t>(offsetof(M_ristoMallstowlall199_t2093772344, ____bemra_17)); }
	inline uint32_t get__bemra_17() const { return ____bemra_17; }
	inline uint32_t* get_address_of__bemra_17() { return &____bemra_17; }
	inline void set__bemra_17(uint32_t value)
	{
		____bemra_17 = value;
	}

	inline static int32_t get_offset_of__laleastereHanailay_18() { return static_cast<int32_t>(offsetof(M_ristoMallstowlall199_t2093772344, ____laleastereHanailay_18)); }
	inline int32_t get__laleastereHanailay_18() const { return ____laleastereHanailay_18; }
	inline int32_t* get_address_of__laleastereHanailay_18() { return &____laleastereHanailay_18; }
	inline void set__laleastereHanailay_18(int32_t value)
	{
		____laleastereHanailay_18 = value;
	}

	inline static int32_t get_offset_of__carpisSaysoo_19() { return static_cast<int32_t>(offsetof(M_ristoMallstowlall199_t2093772344, ____carpisSaysoo_19)); }
	inline float get__carpisSaysoo_19() const { return ____carpisSaysoo_19; }
	inline float* get_address_of__carpisSaysoo_19() { return &____carpisSaysoo_19; }
	inline void set__carpisSaysoo_19(float value)
	{
		____carpisSaysoo_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
