﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Interpolate
struct Interpolate_t3952247009;
// UnityEngine.Transform
struct Transform_t1659122786;
// System.Collections.Generic.IEnumerable`1<System.Single>
struct IEnumerable_1_t3297864633;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// Interpolate/Function
struct Function_t4102301030;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
struct IEnumerable_1_t3288012227;
// UnityEngine.Transform[]
struct TransformU5BU5D_t3792884695;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_Interpolate_Function4102301030.h"
#include "AssemblyU2DCSharp_Interpolate_EaseType1933341142.h"

// System.Void Interpolate::.ctor()
extern "C"  void Interpolate__ctor_m3103090602 (Interpolate_t3952247009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Interpolate::Identity(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  Interpolate_Identity_m1527847099 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Interpolate::TransformDotPosition(UnityEngine.Transform)
extern "C"  Vector3_t4282066566  Interpolate_TransformDotPosition_m3509932887 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Single> Interpolate::NewTimer(System.Single)
extern "C"  Il2CppObject* Interpolate_NewTimer_m183222111 (Il2CppObject * __this /* static, unused */, float ___duration0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<System.Single> Interpolate::NewCounter(System.Int32,System.Int32,System.Int32)
extern "C"  Il2CppObject* Interpolate_NewCounter_m3238559348 (Il2CppObject * __this /* static, unused */, int32_t ___start0, int32_t ___end1, int32_t ___step2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Interpolate::NewEase(Interpolate/Function,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Il2CppObject * Interpolate_NewEase_m3580516963 (Il2CppObject * __this /* static, unused */, Function_t4102301030 * ___ease0, Vector3_t4282066566  ___start1, Vector3_t4282066566  ___end2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Interpolate::NewEase(Interpolate/Function,UnityEngine.Vector3,UnityEngine.Vector3,System.Int32)
extern "C"  Il2CppObject * Interpolate_NewEase_m4267468185 (Il2CppObject * __this /* static, unused */, Function_t4102301030 * ___ease0, Vector3_t4282066566  ___start1, Vector3_t4282066566  ___end2, int32_t ___slices3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Interpolate::NewEase(Interpolate/Function,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Collections.Generic.IEnumerable`1<System.Single>)
extern "C"  Il2CppObject * Interpolate_NewEase_m393665135 (Il2CppObject * __this /* static, unused */, Function_t4102301030 * ___ease0, Vector3_t4282066566  ___start1, Vector3_t4282066566  ___end2, float ___total3, Il2CppObject* ___driver4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Interpolate::Ease(Interpolate/Function,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern "C"  Vector3_t4282066566  Interpolate_Ease_m4031191960 (Il2CppObject * __this /* static, unused */, Function_t4102301030 * ___ease0, Vector3_t4282066566  ___start1, Vector3_t4282066566  ___distance2, float ___elapsedTime3, float ___duration4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Interpolate/Function Interpolate::Ease(Interpolate/EaseType)
extern "C"  Function_t4102301030 * Interpolate_Ease_m1281592251 (Il2CppObject * __this /* static, unused */, int32_t ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3> Interpolate::NewBezier(Interpolate/Function,UnityEngine.Transform[],System.Single)
extern "C"  Il2CppObject* Interpolate_NewBezier_m3975705792 (Il2CppObject * __this /* static, unused */, Function_t4102301030 * ___ease0, TransformU5BU5D_t3792884695* ___nodes1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3> Interpolate::NewBezier(Interpolate/Function,UnityEngine.Transform[],System.Int32)
extern "C"  Il2CppObject* Interpolate_NewBezier_m262343580 (Il2CppObject * __this /* static, unused */, Function_t4102301030 * ___ease0, TransformU5BU5D_t3792884695* ___nodes1, int32_t ___slices2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3> Interpolate::NewBezier(Interpolate/Function,UnityEngine.Vector3[],System.Single)
extern "C"  Il2CppObject* Interpolate_NewBezier_m2032737628 (Il2CppObject * __this /* static, unused */, Function_t4102301030 * ___ease0, Vector3U5BU5D_t215400611* ___points1, float ___duration2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3> Interpolate::NewBezier(Interpolate/Function,UnityEngine.Vector3[],System.Int32)
extern "C"  Il2CppObject* Interpolate_NewBezier_m615309184 (Il2CppObject * __this /* static, unused */, Function_t4102301030 * ___ease0, Vector3U5BU5D_t215400611* ___points1, int32_t ___slices2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Interpolate::Bezier(Interpolate/Function,UnityEngine.Vector3[],System.Single,System.Single)
extern "C"  Vector3_t4282066566  Interpolate_Bezier_m1705917844 (Il2CppObject * __this /* static, unused */, Function_t4102301030 * ___ease0, Vector3U5BU5D_t215400611* ___points1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3> Interpolate::NewCatmullRom(UnityEngine.Transform[],System.Int32,System.Boolean)
extern "C"  Il2CppObject* Interpolate_NewCatmullRom_m12570434 (Il2CppObject * __this /* static, unused */, TransformU5BU5D_t3792884695* ___nodes0, int32_t ___slices1, bool ___loop2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3> Interpolate::NewCatmullRom(UnityEngine.Vector3[],System.Int32,System.Boolean)
extern "C"  Il2CppObject* Interpolate_NewCatmullRom_m1719696158 (Il2CppObject * __this /* static, unused */, Vector3U5BU5D_t215400611* ___points0, int32_t ___slices1, bool ___loop2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Interpolate::CatmullRom(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern "C"  Vector3_t4282066566  Interpolate_CatmullRom_m1128313226 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___previous0, Vector3_t4282066566  ___start1, Vector3_t4282066566  ___end2, Vector3_t4282066566  ___next3, float ___elapsedTime4, float ___duration5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::Linear(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_Linear_m2960741959 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::EaseInQuad(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_EaseInQuad_m2851964914 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::EaseOutQuad(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_EaseOutQuad_m2980369023 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::EaseInOutQuad(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_EaseInOutQuad_m2243924228 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::EaseInCubic(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_EaseInCubic_m1904380175 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::EaseOutCubic(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_EaseOutCubic_m1589940258 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::EaseInOutCubic(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_EaseInOutCubic_m234988093 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::EaseInQuart(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_EaseInQuart_m2030801978 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::EaseOutQuart(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_EaseOutQuart_m1716362061 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::EaseInOutQuart(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_EaseInOutQuart_m361409896 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::EaseInQuint(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_EaseInQuint_m1716698158 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::EaseOutQuint(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_EaseOutQuint_m1402258241 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::EaseInOutQuint(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_EaseInOutQuint_m47306076 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::EaseInSine(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_EaseInSine_m1021558124 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::EaseOutSine(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_EaseOutSine_m1149962233 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::EaseInOutSine(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_EaseInOutSine_m413517438 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::EaseInExpo(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_EaseInExpo_m123494823 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::EaseOutExpo(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_EaseOutExpo_m251898932 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::EaseInOutExpo(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_EaseInOutExpo_m3810421433 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::EaseInCirc(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_EaseInCirc_m3196500514 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::EaseOutCirc(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_EaseOutCirc_m3324904623 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::EaseInOutCirc(System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_EaseInOutCirc_m2588459828 (Il2CppObject * __this /* static, unused */, float ___start0, float ___distance1, float ___elapsedTime2, float ___duration3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Interpolate::ilo_NewEase1(Interpolate/Function,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Collections.Generic.IEnumerable`1<System.Single>)
extern "C"  Il2CppObject * Interpolate_ilo_NewEase1_m3977924729 (Il2CppObject * __this /* static, unused */, Function_t4102301030 * ___ease0, Vector3_t4282066566  ___start1, Vector3_t4282066566  ___end2, float ___total3, Il2CppObject* ___driver4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Interpolate::ilo_Invoke2(Interpolate/Function,System.Single,System.Single,System.Single,System.Single)
extern "C"  float Interpolate_ilo_Invoke2_m153315113 (Il2CppObject * __this /* static, unused */, Function_t4102301030 * ____this0, float ___a1, float ___b2, float ___c3, float ___d4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
