﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated/<ListA1_Find_GetDelegate_member13_arg0>c__AnonStorey7F`1<System.Object>
struct U3CListA1_Find_GetDelegate_member13_arg0U3Ec__AnonStorey7F_1_t2977357153;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Collections_Generic_List77Generated/<ListA1_Find_GetDelegate_member13_arg0>c__AnonStorey7F`1<System.Object>::.ctor()
extern "C"  void U3CListA1_Find_GetDelegate_member13_arg0U3Ec__AnonStorey7F_1__ctor_m1133187664_gshared (U3CListA1_Find_GetDelegate_member13_arg0U3Ec__AnonStorey7F_1_t2977357153 * __this, const MethodInfo* method);
#define U3CListA1_Find_GetDelegate_member13_arg0U3Ec__AnonStorey7F_1__ctor_m1133187664(__this, method) ((  void (*) (U3CListA1_Find_GetDelegate_member13_arg0U3Ec__AnonStorey7F_1_t2977357153 *, const MethodInfo*))U3CListA1_Find_GetDelegate_member13_arg0U3Ec__AnonStorey7F_1__ctor_m1133187664_gshared)(__this, method)
// System.Boolean System_Collections_Generic_List77Generated/<ListA1_Find_GetDelegate_member13_arg0>c__AnonStorey7F`1<System.Object>::<>m__A4(T)
extern "C"  bool U3CListA1_Find_GetDelegate_member13_arg0U3Ec__AnonStorey7F_1_U3CU3Em__A4_m1779230246_gshared (U3CListA1_Find_GetDelegate_member13_arg0U3Ec__AnonStorey7F_1_t2977357153 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
#define U3CListA1_Find_GetDelegate_member13_arg0U3Ec__AnonStorey7F_1_U3CU3Em__A4_m1779230246(__this, ___obj0, method) ((  bool (*) (U3CListA1_Find_GetDelegate_member13_arg0U3Ec__AnonStorey7F_1_t2977357153 *, Il2CppObject *, const MethodInfo*))U3CListA1_Find_GetDelegate_member13_arg0U3Ec__AnonStorey7F_1_U3CU3Em__A4_m1779230246_gshared)(__this, ___obj0, method)
