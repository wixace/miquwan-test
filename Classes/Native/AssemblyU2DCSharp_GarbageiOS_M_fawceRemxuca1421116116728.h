﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_fawceRemxuca142
struct  M_fawceRemxuca142_t1116116728  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_fawceRemxuca142::_nallserehoCetaima
	bool ____nallserehoCetaima_0;
	// System.UInt32 GarbageiOS.M_fawceRemxuca142::_jarall
	uint32_t ____jarall_1;
	// System.String GarbageiOS.M_fawceRemxuca142::_telsem
	String_t* ____telsem_2;
	// System.UInt32 GarbageiOS.M_fawceRemxuca142::_madeMesi
	uint32_t ____madeMesi_3;
	// System.UInt32 GarbageiOS.M_fawceRemxuca142::_fowvaray
	uint32_t ____fowvaray_4;
	// System.Boolean GarbageiOS.M_fawceRemxuca142::_gicudasKoroobou
	bool ____gicudasKoroobou_5;
	// System.String GarbageiOS.M_fawceRemxuca142::_towfuherTowchorka
	String_t* ____towfuherTowchorka_6;
	// System.Int32 GarbageiOS.M_fawceRemxuca142::_lodisjawHisre
	int32_t ____lodisjawHisre_7;
	// System.UInt32 GarbageiOS.M_fawceRemxuca142::_doonahirSawsem
	uint32_t ____doonahirSawsem_8;
	// System.UInt32 GarbageiOS.M_fawceRemxuca142::_vofarxor
	uint32_t ____vofarxor_9;
	// System.String GarbageiOS.M_fawceRemxuca142::_fuhearearGagi
	String_t* ____fuhearearGagi_10;
	// System.UInt32 GarbageiOS.M_fawceRemxuca142::_cookearSelbas
	uint32_t ____cookearSelbas_11;
	// System.Single GarbageiOS.M_fawceRemxuca142::_wexal
	float ____wexal_12;
	// System.String GarbageiOS.M_fawceRemxuca142::_howdor
	String_t* ____howdor_13;
	// System.Boolean GarbageiOS.M_fawceRemxuca142::_chowpaliNooqoolu
	bool ____chowpaliNooqoolu_14;
	// System.Int32 GarbageiOS.M_fawceRemxuca142::_najearrer
	int32_t ____najearrer_15;
	// System.Single GarbageiOS.M_fawceRemxuca142::_tawzairdo
	float ____tawzairdo_16;
	// System.UInt32 GarbageiOS.M_fawceRemxuca142::_yaraGeawemmel
	uint32_t ____yaraGeawemmel_17;
	// System.Single GarbageiOS.M_fawceRemxuca142::_chawlea
	float ____chawlea_18;
	// System.Int32 GarbageiOS.M_fawceRemxuca142::_bobeha
	int32_t ____bobeha_19;
	// System.String GarbageiOS.M_fawceRemxuca142::_walltearRijo
	String_t* ____walltearRijo_20;
	// System.Int32 GarbageiOS.M_fawceRemxuca142::_pererur
	int32_t ____pererur_21;

public:
	inline static int32_t get_offset_of__nallserehoCetaima_0() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____nallserehoCetaima_0)); }
	inline bool get__nallserehoCetaima_0() const { return ____nallserehoCetaima_0; }
	inline bool* get_address_of__nallserehoCetaima_0() { return &____nallserehoCetaima_0; }
	inline void set__nallserehoCetaima_0(bool value)
	{
		____nallserehoCetaima_0 = value;
	}

	inline static int32_t get_offset_of__jarall_1() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____jarall_1)); }
	inline uint32_t get__jarall_1() const { return ____jarall_1; }
	inline uint32_t* get_address_of__jarall_1() { return &____jarall_1; }
	inline void set__jarall_1(uint32_t value)
	{
		____jarall_1 = value;
	}

	inline static int32_t get_offset_of__telsem_2() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____telsem_2)); }
	inline String_t* get__telsem_2() const { return ____telsem_2; }
	inline String_t** get_address_of__telsem_2() { return &____telsem_2; }
	inline void set__telsem_2(String_t* value)
	{
		____telsem_2 = value;
		Il2CppCodeGenWriteBarrier(&____telsem_2, value);
	}

	inline static int32_t get_offset_of__madeMesi_3() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____madeMesi_3)); }
	inline uint32_t get__madeMesi_3() const { return ____madeMesi_3; }
	inline uint32_t* get_address_of__madeMesi_3() { return &____madeMesi_3; }
	inline void set__madeMesi_3(uint32_t value)
	{
		____madeMesi_3 = value;
	}

	inline static int32_t get_offset_of__fowvaray_4() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____fowvaray_4)); }
	inline uint32_t get__fowvaray_4() const { return ____fowvaray_4; }
	inline uint32_t* get_address_of__fowvaray_4() { return &____fowvaray_4; }
	inline void set__fowvaray_4(uint32_t value)
	{
		____fowvaray_4 = value;
	}

	inline static int32_t get_offset_of__gicudasKoroobou_5() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____gicudasKoroobou_5)); }
	inline bool get__gicudasKoroobou_5() const { return ____gicudasKoroobou_5; }
	inline bool* get_address_of__gicudasKoroobou_5() { return &____gicudasKoroobou_5; }
	inline void set__gicudasKoroobou_5(bool value)
	{
		____gicudasKoroobou_5 = value;
	}

	inline static int32_t get_offset_of__towfuherTowchorka_6() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____towfuherTowchorka_6)); }
	inline String_t* get__towfuherTowchorka_6() const { return ____towfuherTowchorka_6; }
	inline String_t** get_address_of__towfuherTowchorka_6() { return &____towfuherTowchorka_6; }
	inline void set__towfuherTowchorka_6(String_t* value)
	{
		____towfuherTowchorka_6 = value;
		Il2CppCodeGenWriteBarrier(&____towfuherTowchorka_6, value);
	}

	inline static int32_t get_offset_of__lodisjawHisre_7() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____lodisjawHisre_7)); }
	inline int32_t get__lodisjawHisre_7() const { return ____lodisjawHisre_7; }
	inline int32_t* get_address_of__lodisjawHisre_7() { return &____lodisjawHisre_7; }
	inline void set__lodisjawHisre_7(int32_t value)
	{
		____lodisjawHisre_7 = value;
	}

	inline static int32_t get_offset_of__doonahirSawsem_8() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____doonahirSawsem_8)); }
	inline uint32_t get__doonahirSawsem_8() const { return ____doonahirSawsem_8; }
	inline uint32_t* get_address_of__doonahirSawsem_8() { return &____doonahirSawsem_8; }
	inline void set__doonahirSawsem_8(uint32_t value)
	{
		____doonahirSawsem_8 = value;
	}

	inline static int32_t get_offset_of__vofarxor_9() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____vofarxor_9)); }
	inline uint32_t get__vofarxor_9() const { return ____vofarxor_9; }
	inline uint32_t* get_address_of__vofarxor_9() { return &____vofarxor_9; }
	inline void set__vofarxor_9(uint32_t value)
	{
		____vofarxor_9 = value;
	}

	inline static int32_t get_offset_of__fuhearearGagi_10() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____fuhearearGagi_10)); }
	inline String_t* get__fuhearearGagi_10() const { return ____fuhearearGagi_10; }
	inline String_t** get_address_of__fuhearearGagi_10() { return &____fuhearearGagi_10; }
	inline void set__fuhearearGagi_10(String_t* value)
	{
		____fuhearearGagi_10 = value;
		Il2CppCodeGenWriteBarrier(&____fuhearearGagi_10, value);
	}

	inline static int32_t get_offset_of__cookearSelbas_11() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____cookearSelbas_11)); }
	inline uint32_t get__cookearSelbas_11() const { return ____cookearSelbas_11; }
	inline uint32_t* get_address_of__cookearSelbas_11() { return &____cookearSelbas_11; }
	inline void set__cookearSelbas_11(uint32_t value)
	{
		____cookearSelbas_11 = value;
	}

	inline static int32_t get_offset_of__wexal_12() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____wexal_12)); }
	inline float get__wexal_12() const { return ____wexal_12; }
	inline float* get_address_of__wexal_12() { return &____wexal_12; }
	inline void set__wexal_12(float value)
	{
		____wexal_12 = value;
	}

	inline static int32_t get_offset_of__howdor_13() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____howdor_13)); }
	inline String_t* get__howdor_13() const { return ____howdor_13; }
	inline String_t** get_address_of__howdor_13() { return &____howdor_13; }
	inline void set__howdor_13(String_t* value)
	{
		____howdor_13 = value;
		Il2CppCodeGenWriteBarrier(&____howdor_13, value);
	}

	inline static int32_t get_offset_of__chowpaliNooqoolu_14() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____chowpaliNooqoolu_14)); }
	inline bool get__chowpaliNooqoolu_14() const { return ____chowpaliNooqoolu_14; }
	inline bool* get_address_of__chowpaliNooqoolu_14() { return &____chowpaliNooqoolu_14; }
	inline void set__chowpaliNooqoolu_14(bool value)
	{
		____chowpaliNooqoolu_14 = value;
	}

	inline static int32_t get_offset_of__najearrer_15() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____najearrer_15)); }
	inline int32_t get__najearrer_15() const { return ____najearrer_15; }
	inline int32_t* get_address_of__najearrer_15() { return &____najearrer_15; }
	inline void set__najearrer_15(int32_t value)
	{
		____najearrer_15 = value;
	}

	inline static int32_t get_offset_of__tawzairdo_16() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____tawzairdo_16)); }
	inline float get__tawzairdo_16() const { return ____tawzairdo_16; }
	inline float* get_address_of__tawzairdo_16() { return &____tawzairdo_16; }
	inline void set__tawzairdo_16(float value)
	{
		____tawzairdo_16 = value;
	}

	inline static int32_t get_offset_of__yaraGeawemmel_17() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____yaraGeawemmel_17)); }
	inline uint32_t get__yaraGeawemmel_17() const { return ____yaraGeawemmel_17; }
	inline uint32_t* get_address_of__yaraGeawemmel_17() { return &____yaraGeawemmel_17; }
	inline void set__yaraGeawemmel_17(uint32_t value)
	{
		____yaraGeawemmel_17 = value;
	}

	inline static int32_t get_offset_of__chawlea_18() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____chawlea_18)); }
	inline float get__chawlea_18() const { return ____chawlea_18; }
	inline float* get_address_of__chawlea_18() { return &____chawlea_18; }
	inline void set__chawlea_18(float value)
	{
		____chawlea_18 = value;
	}

	inline static int32_t get_offset_of__bobeha_19() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____bobeha_19)); }
	inline int32_t get__bobeha_19() const { return ____bobeha_19; }
	inline int32_t* get_address_of__bobeha_19() { return &____bobeha_19; }
	inline void set__bobeha_19(int32_t value)
	{
		____bobeha_19 = value;
	}

	inline static int32_t get_offset_of__walltearRijo_20() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____walltearRijo_20)); }
	inline String_t* get__walltearRijo_20() const { return ____walltearRijo_20; }
	inline String_t** get_address_of__walltearRijo_20() { return &____walltearRijo_20; }
	inline void set__walltearRijo_20(String_t* value)
	{
		____walltearRijo_20 = value;
		Il2CppCodeGenWriteBarrier(&____walltearRijo_20, value);
	}

	inline static int32_t get_offset_of__pererur_21() { return static_cast<int32_t>(offsetof(M_fawceRemxuca142_t1116116728, ____pererur_21)); }
	inline int32_t get__pererur_21() const { return ____pererur_21; }
	inline int32_t* get_address_of__pererur_21() { return &____pererur_21; }
	inline void set__pererur_21(int32_t value)
	{
		____pererur_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
