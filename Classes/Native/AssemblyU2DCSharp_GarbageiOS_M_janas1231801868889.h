﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_janas123
struct  M_janas123_t1801868889  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_janas123::_chatikel
	String_t* ____chatikel_0;
	// System.UInt32 GarbageiOS.M_janas123::_miwhoore
	uint32_t ____miwhoore_1;
	// System.UInt32 GarbageiOS.M_janas123::_kaykaMemall
	uint32_t ____kaykaMemall_2;
	// System.Boolean GarbageiOS.M_janas123::_kootobairSasri
	bool ____kootobairSasri_3;
	// System.Boolean GarbageiOS.M_janas123::_caircirJirchor
	bool ____caircirJirchor_4;
	// System.Boolean GarbageiOS.M_janas123::_waymearNoonahu
	bool ____waymearNoonahu_5;
	// System.Boolean GarbageiOS.M_janas123::_birhoka
	bool ____birhoka_6;
	// System.String GarbageiOS.M_janas123::_burur
	String_t* ____burur_7;
	// System.Single GarbageiOS.M_janas123::_suwheevasTeeturkas
	float ____suwheevasTeeturkas_8;
	// System.UInt32 GarbageiOS.M_janas123::_moxere
	uint32_t ____moxere_9;
	// System.Single GarbageiOS.M_janas123::_mapallge
	float ____mapallge_10;
	// System.Int32 GarbageiOS.M_janas123::_chelher
	int32_t ____chelher_11;
	// System.UInt32 GarbageiOS.M_janas123::_sumowdraPairva
	uint32_t ____sumowdraPairva_12;
	// System.String GarbageiOS.M_janas123::_naljiStalsere
	String_t* ____naljiStalsere_13;
	// System.Boolean GarbageiOS.M_janas123::_faycooKosowo
	bool ____faycooKosowo_14;
	// System.String GarbageiOS.M_janas123::_weresiSecowdu
	String_t* ____weresiSecowdu_15;
	// System.UInt32 GarbageiOS.M_janas123::_zahelmallChearcou
	uint32_t ____zahelmallChearcou_16;
	// System.UInt32 GarbageiOS.M_janas123::_porgetal
	uint32_t ____porgetal_17;
	// System.Boolean GarbageiOS.M_janas123::_draljou
	bool ____draljou_18;
	// System.Boolean GarbageiOS.M_janas123::_jeasirmerGooto
	bool ____jeasirmerGooto_19;
	// System.Int32 GarbageiOS.M_janas123::_jayjairstu
	int32_t ____jayjairstu_20;

public:
	inline static int32_t get_offset_of__chatikel_0() { return static_cast<int32_t>(offsetof(M_janas123_t1801868889, ____chatikel_0)); }
	inline String_t* get__chatikel_0() const { return ____chatikel_0; }
	inline String_t** get_address_of__chatikel_0() { return &____chatikel_0; }
	inline void set__chatikel_0(String_t* value)
	{
		____chatikel_0 = value;
		Il2CppCodeGenWriteBarrier(&____chatikel_0, value);
	}

	inline static int32_t get_offset_of__miwhoore_1() { return static_cast<int32_t>(offsetof(M_janas123_t1801868889, ____miwhoore_1)); }
	inline uint32_t get__miwhoore_1() const { return ____miwhoore_1; }
	inline uint32_t* get_address_of__miwhoore_1() { return &____miwhoore_1; }
	inline void set__miwhoore_1(uint32_t value)
	{
		____miwhoore_1 = value;
	}

	inline static int32_t get_offset_of__kaykaMemall_2() { return static_cast<int32_t>(offsetof(M_janas123_t1801868889, ____kaykaMemall_2)); }
	inline uint32_t get__kaykaMemall_2() const { return ____kaykaMemall_2; }
	inline uint32_t* get_address_of__kaykaMemall_2() { return &____kaykaMemall_2; }
	inline void set__kaykaMemall_2(uint32_t value)
	{
		____kaykaMemall_2 = value;
	}

	inline static int32_t get_offset_of__kootobairSasri_3() { return static_cast<int32_t>(offsetof(M_janas123_t1801868889, ____kootobairSasri_3)); }
	inline bool get__kootobairSasri_3() const { return ____kootobairSasri_3; }
	inline bool* get_address_of__kootobairSasri_3() { return &____kootobairSasri_3; }
	inline void set__kootobairSasri_3(bool value)
	{
		____kootobairSasri_3 = value;
	}

	inline static int32_t get_offset_of__caircirJirchor_4() { return static_cast<int32_t>(offsetof(M_janas123_t1801868889, ____caircirJirchor_4)); }
	inline bool get__caircirJirchor_4() const { return ____caircirJirchor_4; }
	inline bool* get_address_of__caircirJirchor_4() { return &____caircirJirchor_4; }
	inline void set__caircirJirchor_4(bool value)
	{
		____caircirJirchor_4 = value;
	}

	inline static int32_t get_offset_of__waymearNoonahu_5() { return static_cast<int32_t>(offsetof(M_janas123_t1801868889, ____waymearNoonahu_5)); }
	inline bool get__waymearNoonahu_5() const { return ____waymearNoonahu_5; }
	inline bool* get_address_of__waymearNoonahu_5() { return &____waymearNoonahu_5; }
	inline void set__waymearNoonahu_5(bool value)
	{
		____waymearNoonahu_5 = value;
	}

	inline static int32_t get_offset_of__birhoka_6() { return static_cast<int32_t>(offsetof(M_janas123_t1801868889, ____birhoka_6)); }
	inline bool get__birhoka_6() const { return ____birhoka_6; }
	inline bool* get_address_of__birhoka_6() { return &____birhoka_6; }
	inline void set__birhoka_6(bool value)
	{
		____birhoka_6 = value;
	}

	inline static int32_t get_offset_of__burur_7() { return static_cast<int32_t>(offsetof(M_janas123_t1801868889, ____burur_7)); }
	inline String_t* get__burur_7() const { return ____burur_7; }
	inline String_t** get_address_of__burur_7() { return &____burur_7; }
	inline void set__burur_7(String_t* value)
	{
		____burur_7 = value;
		Il2CppCodeGenWriteBarrier(&____burur_7, value);
	}

	inline static int32_t get_offset_of__suwheevasTeeturkas_8() { return static_cast<int32_t>(offsetof(M_janas123_t1801868889, ____suwheevasTeeturkas_8)); }
	inline float get__suwheevasTeeturkas_8() const { return ____suwheevasTeeturkas_8; }
	inline float* get_address_of__suwheevasTeeturkas_8() { return &____suwheevasTeeturkas_8; }
	inline void set__suwheevasTeeturkas_8(float value)
	{
		____suwheevasTeeturkas_8 = value;
	}

	inline static int32_t get_offset_of__moxere_9() { return static_cast<int32_t>(offsetof(M_janas123_t1801868889, ____moxere_9)); }
	inline uint32_t get__moxere_9() const { return ____moxere_9; }
	inline uint32_t* get_address_of__moxere_9() { return &____moxere_9; }
	inline void set__moxere_9(uint32_t value)
	{
		____moxere_9 = value;
	}

	inline static int32_t get_offset_of__mapallge_10() { return static_cast<int32_t>(offsetof(M_janas123_t1801868889, ____mapallge_10)); }
	inline float get__mapallge_10() const { return ____mapallge_10; }
	inline float* get_address_of__mapallge_10() { return &____mapallge_10; }
	inline void set__mapallge_10(float value)
	{
		____mapallge_10 = value;
	}

	inline static int32_t get_offset_of__chelher_11() { return static_cast<int32_t>(offsetof(M_janas123_t1801868889, ____chelher_11)); }
	inline int32_t get__chelher_11() const { return ____chelher_11; }
	inline int32_t* get_address_of__chelher_11() { return &____chelher_11; }
	inline void set__chelher_11(int32_t value)
	{
		____chelher_11 = value;
	}

	inline static int32_t get_offset_of__sumowdraPairva_12() { return static_cast<int32_t>(offsetof(M_janas123_t1801868889, ____sumowdraPairva_12)); }
	inline uint32_t get__sumowdraPairva_12() const { return ____sumowdraPairva_12; }
	inline uint32_t* get_address_of__sumowdraPairva_12() { return &____sumowdraPairva_12; }
	inline void set__sumowdraPairva_12(uint32_t value)
	{
		____sumowdraPairva_12 = value;
	}

	inline static int32_t get_offset_of__naljiStalsere_13() { return static_cast<int32_t>(offsetof(M_janas123_t1801868889, ____naljiStalsere_13)); }
	inline String_t* get__naljiStalsere_13() const { return ____naljiStalsere_13; }
	inline String_t** get_address_of__naljiStalsere_13() { return &____naljiStalsere_13; }
	inline void set__naljiStalsere_13(String_t* value)
	{
		____naljiStalsere_13 = value;
		Il2CppCodeGenWriteBarrier(&____naljiStalsere_13, value);
	}

	inline static int32_t get_offset_of__faycooKosowo_14() { return static_cast<int32_t>(offsetof(M_janas123_t1801868889, ____faycooKosowo_14)); }
	inline bool get__faycooKosowo_14() const { return ____faycooKosowo_14; }
	inline bool* get_address_of__faycooKosowo_14() { return &____faycooKosowo_14; }
	inline void set__faycooKosowo_14(bool value)
	{
		____faycooKosowo_14 = value;
	}

	inline static int32_t get_offset_of__weresiSecowdu_15() { return static_cast<int32_t>(offsetof(M_janas123_t1801868889, ____weresiSecowdu_15)); }
	inline String_t* get__weresiSecowdu_15() const { return ____weresiSecowdu_15; }
	inline String_t** get_address_of__weresiSecowdu_15() { return &____weresiSecowdu_15; }
	inline void set__weresiSecowdu_15(String_t* value)
	{
		____weresiSecowdu_15 = value;
		Il2CppCodeGenWriteBarrier(&____weresiSecowdu_15, value);
	}

	inline static int32_t get_offset_of__zahelmallChearcou_16() { return static_cast<int32_t>(offsetof(M_janas123_t1801868889, ____zahelmallChearcou_16)); }
	inline uint32_t get__zahelmallChearcou_16() const { return ____zahelmallChearcou_16; }
	inline uint32_t* get_address_of__zahelmallChearcou_16() { return &____zahelmallChearcou_16; }
	inline void set__zahelmallChearcou_16(uint32_t value)
	{
		____zahelmallChearcou_16 = value;
	}

	inline static int32_t get_offset_of__porgetal_17() { return static_cast<int32_t>(offsetof(M_janas123_t1801868889, ____porgetal_17)); }
	inline uint32_t get__porgetal_17() const { return ____porgetal_17; }
	inline uint32_t* get_address_of__porgetal_17() { return &____porgetal_17; }
	inline void set__porgetal_17(uint32_t value)
	{
		____porgetal_17 = value;
	}

	inline static int32_t get_offset_of__draljou_18() { return static_cast<int32_t>(offsetof(M_janas123_t1801868889, ____draljou_18)); }
	inline bool get__draljou_18() const { return ____draljou_18; }
	inline bool* get_address_of__draljou_18() { return &____draljou_18; }
	inline void set__draljou_18(bool value)
	{
		____draljou_18 = value;
	}

	inline static int32_t get_offset_of__jeasirmerGooto_19() { return static_cast<int32_t>(offsetof(M_janas123_t1801868889, ____jeasirmerGooto_19)); }
	inline bool get__jeasirmerGooto_19() const { return ____jeasirmerGooto_19; }
	inline bool* get_address_of__jeasirmerGooto_19() { return &____jeasirmerGooto_19; }
	inline void set__jeasirmerGooto_19(bool value)
	{
		____jeasirmerGooto_19 = value;
	}

	inline static int32_t get_offset_of__jayjairstu_20() { return static_cast<int32_t>(offsetof(M_janas123_t1801868889, ____jayjairstu_20)); }
	inline int32_t get__jayjairstu_20() const { return ____jayjairstu_20; }
	inline int32_t* get_address_of__jayjairstu_20() { return &____jayjairstu_20; }
	inline void set__jayjairstu_20(int32_t value)
	{
		____jayjairstu_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
