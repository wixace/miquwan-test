﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_UIBehaviourGenerated
struct UnityEngine_EventSystems_UIBehaviourGenerated_t2471244691;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_EventSystems_UIBehaviourGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_UIBehaviourGenerated__ctor_m1187373880 (UnityEngine_EventSystems_UIBehaviourGenerated_t2471244691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_UIBehaviourGenerated::UIBehaviour_IsActive(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_UIBehaviourGenerated_UIBehaviour_IsActive_m607838382 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_UIBehaviourGenerated::UIBehaviour_IsDestroyed(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_UIBehaviourGenerated_UIBehaviour_IsDestroyed_m2524711411 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_UIBehaviourGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_UIBehaviourGenerated___Register_m1302387855 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_UIBehaviourGenerated::ilo_setBooleanS1(System.Int32,System.Boolean)
extern "C"  void UnityEngine_EventSystems_UIBehaviourGenerated_ilo_setBooleanS1_m784931753 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
