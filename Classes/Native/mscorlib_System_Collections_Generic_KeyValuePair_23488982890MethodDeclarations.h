﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23488982890.h"
#include "AssemblyU2DCSharp_SoundStatus3592938945.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3861120107_gshared (KeyValuePair_2_t3488982890 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m3861120107(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t3488982890 *, int32_t, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m3861120107_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1294005021_gshared (KeyValuePair_2_t3488982890 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1294005021(__this, method) ((  int32_t (*) (KeyValuePair_2_t3488982890 *, const MethodInfo*))KeyValuePair_2_get_Key_m1294005021_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m4239326942_gshared (KeyValuePair_2_t3488982890 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m4239326942(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3488982890 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m4239326942_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m1077960093_gshared (KeyValuePair_2_t3488982890 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1077960093(__this, method) ((  int32_t (*) (KeyValuePair_2_t3488982890 *, const MethodInfo*))KeyValuePair_2_get_Value_m1077960093_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m4052988894_gshared (KeyValuePair_2_t3488982890 * __this, int32_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m4052988894(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t3488982890 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m4052988894_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,SoundStatus>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m266981764_gshared (KeyValuePair_2_t3488982890 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m266981764(__this, method) ((  String_t* (*) (KeyValuePair_2_t3488982890 *, const MethodInfo*))KeyValuePair_2_ToString_m266981764_gshared)(__this, method)
