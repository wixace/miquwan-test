﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<JSDelayCallExec>c__Iterator3C
struct U3CJSDelayCallExecU3Ec__Iterator3C_t3648384768;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<JSDelayCallExec>c__Iterator3C::.ctor()
extern "C"  void U3CJSDelayCallExecU3Ec__Iterator3C__ctor_m2124448667 (U3CJSDelayCallExecU3Ec__Iterator3C_t3648384768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BehaviourUtil/<JSDelayCallExec>c__Iterator3C::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CJSDelayCallExecU3Ec__Iterator3C_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1231739297 (U3CJSDelayCallExecU3Ec__Iterator3C_t3648384768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BehaviourUtil/<JSDelayCallExec>c__Iterator3C::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CJSDelayCallExecU3Ec__Iterator3C_System_Collections_IEnumerator_get_Current_m496910133 (U3CJSDelayCallExecU3Ec__Iterator3C_t3648384768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BehaviourUtil/<JSDelayCallExec>c__Iterator3C::MoveNext()
extern "C"  bool U3CJSDelayCallExecU3Ec__Iterator3C_MoveNext_m2538211937 (U3CJSDelayCallExecU3Ec__Iterator3C_t3648384768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/<JSDelayCallExec>c__Iterator3C::Dispose()
extern "C"  void U3CJSDelayCallExecU3Ec__Iterator3C_Dispose_m1383986456 (U3CJSDelayCallExecU3Ec__Iterator3C_t3648384768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtil/<JSDelayCallExec>c__Iterator3C::Reset()
extern "C"  void U3CJSDelayCallExecU3Ec__Iterator3C_Reset_m4065848904 (U3CJSDelayCallExecU3Ec__Iterator3C_t3648384768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
