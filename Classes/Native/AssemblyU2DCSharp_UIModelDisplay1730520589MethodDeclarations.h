﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIModelDisplay
struct UIModelDisplay_t1730520589;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIModelDisplay1730520589.h"

// System.Void UIModelDisplay::.ctor()
extern "C"  void UIModelDisplay__ctor_m1067820334 (UIModelDisplay_t1730520589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplay::Attached()
extern "C"  bool UIModelDisplay_Attached_m3083260462 (UIModelDisplay_t1730520589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplay::Render()
extern "C"  void UIModelDisplay_Render_m2947079468 (UIModelDisplay_t1730520589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplay::Attach()
extern "C"  void UIModelDisplay_Attach_m3980663323 (UIModelDisplay_t1730520589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplay::Detach()
extern "C"  void UIModelDisplay_Detach_m191506409 (UIModelDisplay_t1730520589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplay::set_LightState(System.Boolean)
extern "C"  void UIModelDisplay_set_LightState_m440549413 (UIModelDisplay_t1730520589 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplay::get_LightState()
extern "C"  bool UIModelDisplay_get_LightState_m1907845998 (UIModelDisplay_t1730520589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplay::get_isHaveLight()
extern "C"  bool UIModelDisplay_get_isHaveLight_m2694924499 (UIModelDisplay_t1730520589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplay::OpenLight()
extern "C"  void UIModelDisplay_OpenLight_m377380536 (UIModelDisplay_t1730520589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplay::CloseLight()
extern "C"  void UIModelDisplay_CloseLight_m2749067540 (UIModelDisplay_t1730520589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIModelDisplay::Clear()
extern "C"  void UIModelDisplay_Clear_m2768920921 (UIModelDisplay_t1730520589 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIModelDisplay::ilo_Attached1(UIModelDisplay)
extern "C"  bool UIModelDisplay_ilo_Attached1_m3963205029 (Il2CppObject * __this /* static, unused */, UIModelDisplay_t1730520589 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
