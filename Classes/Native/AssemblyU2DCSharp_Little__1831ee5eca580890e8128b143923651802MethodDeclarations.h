﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._1831ee5eca580890e8128b14461a6560
struct _1831ee5eca580890e8128b14461a6560_t3923651802;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._1831ee5eca580890e8128b14461a6560::.ctor()
extern "C"  void _1831ee5eca580890e8128b14461a6560__ctor_m2980105875 (_1831ee5eca580890e8128b14461a6560_t3923651802 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1831ee5eca580890e8128b14461a6560::_1831ee5eca580890e8128b14461a6560m2(System.Int32)
extern "C"  int32_t _1831ee5eca580890e8128b14461a6560__1831ee5eca580890e8128b14461a6560m2_m2475299673 (_1831ee5eca580890e8128b14461a6560_t3923651802 * __this, int32_t ____1831ee5eca580890e8128b14461a6560a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._1831ee5eca580890e8128b14461a6560::_1831ee5eca580890e8128b14461a6560m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _1831ee5eca580890e8128b14461a6560__1831ee5eca580890e8128b14461a6560m_m2775321469 (_1831ee5eca580890e8128b14461a6560_t3923651802 * __this, int32_t ____1831ee5eca580890e8128b14461a6560a0, int32_t ____1831ee5eca580890e8128b14461a6560821, int32_t ____1831ee5eca580890e8128b14461a6560c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
