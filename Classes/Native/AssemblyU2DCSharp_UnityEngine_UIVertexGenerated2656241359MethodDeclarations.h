﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_UIVertexGenerated
struct UnityEngine_UIVertexGenerated_t2656241359;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_UIVertexGenerated::.ctor()
extern "C"  void UnityEngine_UIVertexGenerated__ctor_m3843146620 (UnityEngine_UIVertexGenerated_t2656241359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UIVertexGenerated::.cctor()
extern "C"  void UnityEngine_UIVertexGenerated__cctor_m2691332017 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_UIVertexGenerated::UIVertex_UIVertex1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_UIVertexGenerated_UIVertex_UIVertex1_m3163573622 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UIVertexGenerated::UIVertex_position(JSVCall)
extern "C"  void UnityEngine_UIVertexGenerated_UIVertex_position_m1233321725 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UIVertexGenerated::UIVertex_normal(JSVCall)
extern "C"  void UnityEngine_UIVertexGenerated_UIVertex_normal_m3307819263 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UIVertexGenerated::UIVertex_color(JSVCall)
extern "C"  void UnityEngine_UIVertexGenerated_UIVertex_color_m3632906499 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UIVertexGenerated::UIVertex_uv0(JSVCall)
extern "C"  void UnityEngine_UIVertexGenerated_UIVertex_uv0_m3296223287 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UIVertexGenerated::UIVertex_uv1(JSVCall)
extern "C"  void UnityEngine_UIVertexGenerated_UIVertex_uv1_m3099709782 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UIVertexGenerated::UIVertex_tangent(JSVCall)
extern "C"  void UnityEngine_UIVertexGenerated_UIVertex_tangent_m390909025 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UIVertexGenerated::UIVertex_simpleVert(JSVCall)
extern "C"  void UnityEngine_UIVertexGenerated_UIVertex_simpleVert_m3026985827 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UIVertexGenerated::__Register()
extern "C"  void UnityEngine_UIVertexGenerated___Register_m2546632395 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UIVertexGenerated::ilo_setVector3S1(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_UIVertexGenerated_ilo_setVector3S1_m588864843 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_UIVertexGenerated::ilo_changeJSObj2(System.Int32,System.Object)
extern "C"  void UnityEngine_UIVertexGenerated_ilo_changeJSObj2_m1815806958 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_UIVertexGenerated::ilo_getObject3(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_UIVertexGenerated_ilo_getObject3_m100649899 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_UIVertexGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_UIVertexGenerated_ilo_setObject4_m577325813 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
