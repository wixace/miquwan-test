﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<HatredCtrl/stValue>
struct List_1_t499163666;
// System.Collections.Generic.IEnumerable`1<HatredCtrl/stValue>
struct IEnumerable_1_t2431891071;
// System.Collections.Generic.IEnumerator`1<HatredCtrl/stValue>
struct IEnumerator_1_t1042843163;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<HatredCtrl/stValue>
struct ICollection_1_t25568101;
// System.Collections.ObjectModel.ReadOnlyCollection`1<HatredCtrl/stValue>
struct ReadOnlyCollection_1_t688055650;
// HatredCtrl/stValue[]
struct stValueU5BU5D_t1121401207;
// System.Predicate`1<HatredCtrl/stValue>
struct Predicate_1_t3037002293;
// System.Collections.Generic.IComparer`1<HatredCtrl/stValue>
struct IComparer_1_t1705992156;
// System.Comparison`1<HatredCtrl/stValue>
struct Comparison_1_t2142306597;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat518836436.h"

// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::.ctor()
extern "C"  void List_1__ctor_m1110490349_gshared (List_1_t499163666 * __this, const MethodInfo* method);
#define List_1__ctor_m1110490349(__this, method) ((  void (*) (List_1_t499163666 *, const MethodInfo*))List_1__ctor_m1110490349_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m669112329_gshared (List_1_t499163666 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m669112329(__this, ___collection0, method) ((  void (*) (List_1_t499163666 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m669112329_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m3418592007_gshared (List_1_t499163666 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m3418592007(__this, ___capacity0, method) ((  void (*) (List_1_t499163666 *, int32_t, const MethodInfo*))List_1__ctor_m3418592007_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::.cctor()
extern "C"  void List_1__cctor_m4031734711_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m4031734711(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m4031734711_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<HatredCtrl/stValue>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m48205376_gshared (List_1_t499163666 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m48205376(__this, method) ((  Il2CppObject* (*) (List_1_t499163666 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m48205376_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m2558070606_gshared (List_1_t499163666 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m2558070606(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t499163666 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m2558070606_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<HatredCtrl/stValue>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m1474590813_gshared (List_1_t499163666 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1474590813(__this, method) ((  Il2CppObject * (*) (List_1_t499163666 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1474590813_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HatredCtrl/stValue>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m3942061440_gshared (List_1_t499163666 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m3942061440(__this, ___item0, method) ((  int32_t (*) (List_1_t499163666 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3942061440_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<HatredCtrl/stValue>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1008148288_gshared (List_1_t499163666 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1008148288(__this, ___item0, method) ((  bool (*) (List_1_t499163666 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1008148288_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<HatredCtrl/stValue>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m1974504408_gshared (List_1_t499163666 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m1974504408(__this, ___item0, method) ((  int32_t (*) (List_1_t499163666 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m1974504408_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m1160322123_gshared (List_1_t499163666 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m1160322123(__this, ___index0, ___item1, method) ((  void (*) (List_1_t499163666 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m1160322123_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m1790668541_gshared (List_1_t499163666 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1790668541(__this, ___item0, method) ((  void (*) (List_1_t499163666 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1790668541_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<HatredCtrl/stValue>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3587505985_gshared (List_1_t499163666 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3587505985(__this, method) ((  bool (*) (List_1_t499163666 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3587505985_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HatredCtrl/stValue>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m3329290268_gshared (List_1_t499163666 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m3329290268(__this, method) ((  bool (*) (List_1_t499163666 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3329290268_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<HatredCtrl/stValue>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m2128259790_gshared (List_1_t499163666 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m2128259790(__this, method) ((  Il2CppObject * (*) (List_1_t499163666 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m2128259790_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HatredCtrl/stValue>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3837214383_gshared (List_1_t499163666 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3837214383(__this, method) ((  bool (*) (List_1_t499163666 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3837214383_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HatredCtrl/stValue>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m162147946_gshared (List_1_t499163666 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m162147946(__this, method) ((  bool (*) (List_1_t499163666 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m162147946_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<HatredCtrl/stValue>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m1918836949_gshared (List_1_t499163666 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m1918836949(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t499163666 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m1918836949_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m3286983010_gshared (List_1_t499163666 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m3286983010(__this, ___index0, ___value1, method) ((  void (*) (List_1_t499163666 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3286983010_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::Add(T)
extern "C"  void List_1_Add_m804851165_gshared (List_1_t499163666 * __this, stValue_t3425945410  ___item0, const MethodInfo* method);
#define List_1_Add_m804851165(__this, ___item0, method) ((  void (*) (List_1_t499163666 *, stValue_t3425945410 , const MethodInfo*))List_1_Add_m804851165_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1646879428_gshared (List_1_t499163666 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1646879428(__this, ___newCount0, method) ((  void (*) (List_1_t499163666 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1646879428_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m2415009443_gshared (List_1_t499163666 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m2415009443(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t499163666 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m2415009443_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m2587732418_gshared (List_1_t499163666 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m2587732418(__this, ___collection0, method) ((  void (*) (List_1_t499163666 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2587732418_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m1848704642_gshared (List_1_t499163666 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m1848704642(__this, ___enumerable0, method) ((  void (*) (List_1_t499163666 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1848704642_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m338126549_gshared (List_1_t499163666 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m338126549(__this, ___collection0, method) ((  void (*) (List_1_t499163666 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m338126549_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<HatredCtrl/stValue>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t688055650 * List_1_AsReadOnly_m2708046608_gshared (List_1_t499163666 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m2708046608(__this, method) ((  ReadOnlyCollection_1_t688055650 * (*) (List_1_t499163666 *, const MethodInfo*))List_1_AsReadOnly_m2708046608_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HatredCtrl/stValue>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m4194816451_gshared (List_1_t499163666 * __this, stValue_t3425945410  ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m4194816451(__this, ___item0, method) ((  int32_t (*) (List_1_t499163666 *, stValue_t3425945410 , const MethodInfo*))List_1_BinarySearch_m4194816451_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::Clear()
extern "C"  void List_1_Clear_m2816539361_gshared (List_1_t499163666 * __this, const MethodInfo* method);
#define List_1_Clear_m2816539361(__this, method) ((  void (*) (List_1_t499163666 *, const MethodInfo*))List_1_Clear_m2816539361_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<HatredCtrl/stValue>::Contains(T)
extern "C"  bool List_1_Contains_m627512787_gshared (List_1_t499163666 * __this, stValue_t3425945410  ___item0, const MethodInfo* method);
#define List_1_Contains_m627512787(__this, ___item0, method) ((  bool (*) (List_1_t499163666 *, stValue_t3425945410 , const MethodInfo*))List_1_Contains_m627512787_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2877401785_gshared (List_1_t499163666 * __this, stValueU5BU5D_t1121401207* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2877401785(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t499163666 *, stValueU5BU5D_t1121401207*, int32_t, const MethodInfo*))List_1_CopyTo_m2877401785_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<HatredCtrl/stValue>::Find(System.Predicate`1<T>)
extern "C"  stValue_t3425945410  List_1_Find_m1354750957_gshared (List_1_t499163666 * __this, Predicate_1_t3037002293 * ___match0, const MethodInfo* method);
#define List_1_Find_m1354750957(__this, ___match0, method) ((  stValue_t3425945410  (*) (List_1_t499163666 *, Predicate_1_t3037002293 *, const MethodInfo*))List_1_Find_m1354750957_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m1913812042_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t3037002293 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m1913812042(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t3037002293 *, const MethodInfo*))List_1_CheckMatch_m1913812042_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<HatredCtrl/stValue>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m3322482471_gshared (List_1_t499163666 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t3037002293 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m3322482471(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t499163666 *, int32_t, int32_t, Predicate_1_t3037002293 *, const MethodInfo*))List_1_GetIndex_m3322482471_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<HatredCtrl/stValue>::GetEnumerator()
extern "C"  Enumerator_t518836436  List_1_GetEnumerator_m4116067728_gshared (List_1_t499163666 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m4116067728(__this, method) ((  Enumerator_t518836436  (*) (List_1_t499163666 *, const MethodInfo*))List_1_GetEnumerator_m4116067728_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HatredCtrl/stValue>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m2760711493_gshared (List_1_t499163666 * __this, stValue_t3425945410  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m2760711493(__this, ___item0, method) ((  int32_t (*) (List_1_t499163666 *, stValue_t3425945410 , const MethodInfo*))List_1_IndexOf_m2760711493_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2651043856_gshared (List_1_t499163666 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2651043856(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t499163666 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2651043856_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2623768969_gshared (List_1_t499163666 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2623768969(__this, ___index0, method) ((  void (*) (List_1_t499163666 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2623768969_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m2429361008_gshared (List_1_t499163666 * __this, int32_t ___index0, stValue_t3425945410  ___item1, const MethodInfo* method);
#define List_1_Insert_m2429361008(__this, ___index0, ___item1, method) ((  void (*) (List_1_t499163666 *, int32_t, stValue_t3425945410 , const MethodInfo*))List_1_Insert_m2429361008_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m551457765_gshared (List_1_t499163666 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m551457765(__this, ___collection0, method) ((  void (*) (List_1_t499163666 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m551457765_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<HatredCtrl/stValue>::Remove(T)
extern "C"  bool List_1_Remove_m3472699022_gshared (List_1_t499163666 * __this, stValue_t3425945410  ___item0, const MethodInfo* method);
#define List_1_Remove_m3472699022(__this, ___item0, method) ((  bool (*) (List_1_t499163666 *, stValue_t3425945410 , const MethodInfo*))List_1_Remove_m3472699022_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<HatredCtrl/stValue>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m3886183240_gshared (List_1_t499163666 * __this, Predicate_1_t3037002293 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m3886183240(__this, ___match0, method) ((  int32_t (*) (List_1_t499163666 *, Predicate_1_t3037002293 *, const MethodInfo*))List_1_RemoveAll_m3886183240_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m303213878_gshared (List_1_t499163666 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m303213878(__this, ___index0, method) ((  void (*) (List_1_t499163666 *, int32_t, const MethodInfo*))List_1_RemoveAt_m303213878_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m1586060633_gshared (List_1_t499163666 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m1586060633(__this, ___index0, ___count1, method) ((  void (*) (List_1_t499163666 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m1586060633_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::Reverse()
extern "C"  void List_1_Reverse_m2527616374_gshared (List_1_t499163666 * __this, const MethodInfo* method);
#define List_1_Reverse_m2527616374(__this, method) ((  void (*) (List_1_t499163666 *, const MethodInfo*))List_1_Reverse_m2527616374_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::Sort()
extern "C"  void List_1_Sort_m1521927660_gshared (List_1_t499163666 * __this, const MethodInfo* method);
#define List_1_Sort_m1521927660(__this, method) ((  void (*) (List_1_t499163666 *, const MethodInfo*))List_1_Sort_m1521927660_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m3973912952_gshared (List_1_t499163666 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m3973912952(__this, ___comparer0, method) ((  void (*) (List_1_t499163666 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3973912952_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m140794175_gshared (List_1_t499163666 * __this, Comparison_1_t2142306597 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m140794175(__this, ___comparison0, method) ((  void (*) (List_1_t499163666 *, Comparison_1_t2142306597 *, const MethodInfo*))List_1_Sort_m140794175_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<HatredCtrl/stValue>::ToArray()
extern "C"  stValueU5BU5D_t1121401207* List_1_ToArray_m2521177039_gshared (List_1_t499163666 * __this, const MethodInfo* method);
#define List_1_ToArray_m2521177039(__this, method) ((  stValueU5BU5D_t1121401207* (*) (List_1_t499163666 *, const MethodInfo*))List_1_ToArray_m2521177039_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::TrimExcess()
extern "C"  void List_1_TrimExcess_m3527197829_gshared (List_1_t499163666 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m3527197829(__this, method) ((  void (*) (List_1_t499163666 *, const MethodInfo*))List_1_TrimExcess_m3527197829_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<HatredCtrl/stValue>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m312666869_gshared (List_1_t499163666 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m312666869(__this, method) ((  int32_t (*) (List_1_t499163666 *, const MethodInfo*))List_1_get_Capacity_m312666869_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m3152035542_gshared (List_1_t499163666 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m3152035542(__this, ___value0, method) ((  void (*) (List_1_t499163666 *, int32_t, const MethodInfo*))List_1_set_Capacity_m3152035542_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<HatredCtrl/stValue>::get_Count()
extern "C"  int32_t List_1_get_Count_m4084706559_gshared (List_1_t499163666 * __this, const MethodInfo* method);
#define List_1_get_Count_m4084706559(__this, method) ((  int32_t (*) (List_1_t499163666 *, const MethodInfo*))List_1_get_Count_m4084706559_gshared)(__this, method)
// T System.Collections.Generic.List`1<HatredCtrl/stValue>::get_Item(System.Int32)
extern "C"  stValue_t3425945410  List_1_get_Item_m2836900982_gshared (List_1_t499163666 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m2836900982(__this, ___index0, method) ((  stValue_t3425945410  (*) (List_1_t499163666 *, int32_t, const MethodInfo*))List_1_get_Item_m2836900982_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<HatredCtrl/stValue>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m4258283591_gshared (List_1_t499163666 * __this, int32_t ___index0, stValue_t3425945410  ___value1, const MethodInfo* method);
#define List_1_set_Item_m4258283591(__this, ___index0, ___value1, method) ((  void (*) (List_1_t499163666 *, int32_t, stValue_t3425945410 , const MethodInfo*))List_1_set_Item_m4258283591_gshared)(__this, ___index0, ___value1, method)
