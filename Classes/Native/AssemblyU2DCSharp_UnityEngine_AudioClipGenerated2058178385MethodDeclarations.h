﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_AudioClipGenerated
struct UnityEngine_AudioClipGenerated_t2058178385;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t83861602;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t4244274966;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Object
struct Il2CppObject;
// System.Delegate
struct Delegate_t3310234105;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_AudioClipGenerated::.ctor()
extern "C"  void UnityEngine_AudioClipGenerated__ctor_m2350053482 (UnityEngine_AudioClipGenerated_t2058178385 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioClipGenerated::AudioClip_AudioClip1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioClipGenerated_AudioClip_AudioClip1_m1805752386 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioClipGenerated::AudioClip_length(JSVCall)
extern "C"  void UnityEngine_AudioClipGenerated_AudioClip_length_m1506319406 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioClipGenerated::AudioClip_samples(JSVCall)
extern "C"  void UnityEngine_AudioClipGenerated_AudioClip_samples_m3773627023 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioClipGenerated::AudioClip_channels(JSVCall)
extern "C"  void UnityEngine_AudioClipGenerated_AudioClip_channels_m3882000132 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioClipGenerated::AudioClip_frequency(JSVCall)
extern "C"  void UnityEngine_AudioClipGenerated_AudioClip_frequency_m1855262236 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioClipGenerated::AudioClip_loadType(JSVCall)
extern "C"  void UnityEngine_AudioClipGenerated_AudioClip_loadType_m1842314132 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioClipGenerated::AudioClip_preloadAudioData(JSVCall)
extern "C"  void UnityEngine_AudioClipGenerated_AudioClip_preloadAudioData_m3085399069 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioClipGenerated::AudioClip_loadState(JSVCall)
extern "C"  void UnityEngine_AudioClipGenerated_AudioClip_loadState_m1698504973 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioClipGenerated::AudioClip_loadInBackground(JSVCall)
extern "C"  void UnityEngine_AudioClipGenerated_AudioClip_loadInBackground_m4137498299 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioClipGenerated::AudioClip_GetData__Single_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioClipGenerated_AudioClip_GetData__Single_Array__Int32_m2566335491 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioClipGenerated::AudioClip_LoadAudioData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioClipGenerated_AudioClip_LoadAudioData_m1578904933 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioClipGenerated::AudioClip_SetData__Single_Array__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioClipGenerated_AudioClip_SetData__Single_Array__Int32_m3974821239 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioClipGenerated::AudioClip_UnloadAudioData(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioClipGenerated_AudioClip_UnloadAudioData_m2493225388 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip/PCMReaderCallback UnityEngine_AudioClipGenerated::AudioClip_Create_GetDelegate_member4_arg5(CSRepresentedObject)
extern "C"  PCMReaderCallback_t83861602 * UnityEngine_AudioClipGenerated_AudioClip_Create_GetDelegate_member4_arg5_m560447539 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine_AudioClipGenerated::AudioClip_Create_GetDelegate_member4_arg6(CSRepresentedObject)
extern "C"  PCMSetPositionCallback_t4244274966 * UnityEngine_AudioClipGenerated_AudioClip_Create_GetDelegate_member4_arg6_m489097544 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioClipGenerated::AudioClip_Create__String__Int32__Int32__Int32__Boolean__PCMReaderCallback__PCMSetPositionCallback(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioClipGenerated_AudioClip_Create__String__Int32__Int32__Int32__Boolean__PCMReaderCallback__PCMSetPositionCallback_m3102934140 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip/PCMReaderCallback UnityEngine_AudioClipGenerated::AudioClip_Create_GetDelegate_member5_arg5(CSRepresentedObject)
extern "C"  PCMReaderCallback_t83861602 * UnityEngine_AudioClipGenerated_AudioClip_Create_GetDelegate_member5_arg5_m1522061556 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioClipGenerated::AudioClip_Create__String__Int32__Int32__Int32__Boolean__PCMReaderCallback(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioClipGenerated_AudioClip_Create__String__Int32__Int32__Int32__Boolean__PCMReaderCallback_m3503537702 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioClipGenerated::AudioClip_Create__String__Int32__Int32__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_AudioClipGenerated_AudioClip_Create__String__Int32__Int32__Int32__Boolean_m3610202878 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioClipGenerated::__Register()
extern "C"  void UnityEngine_AudioClipGenerated___Register_m2072721053 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] UnityEngine_AudioClipGenerated::<AudioClip_GetData__Single_Array__Int32>m__184()
extern "C"  SingleU5BU5D_t2316563989* UnityEngine_AudioClipGenerated_U3CAudioClip_GetData__Single_Array__Int32U3Em__184_m3532112257 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] UnityEngine_AudioClipGenerated::<AudioClip_SetData__Single_Array__Int32>m__185()
extern "C"  SingleU5BU5D_t2316563989* UnityEngine_AudioClipGenerated_U3CAudioClip_SetData__Single_Array__Int32U3Em__185_m3194335054 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip/PCMReaderCallback UnityEngine_AudioClipGenerated::<AudioClip_Create__String__Int32__Int32__Int32__Boolean__PCMReaderCallback__PCMSetPositionCallback>m__188()
extern "C"  PCMReaderCallback_t83861602 * UnityEngine_AudioClipGenerated_U3CAudioClip_Create__String__Int32__Int32__Int32__Boolean__PCMReaderCallback__PCMSetPositionCallbackU3Em__188_m3819709661 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine_AudioClipGenerated::<AudioClip_Create__String__Int32__Int32__Int32__Boolean__PCMReaderCallback__PCMSetPositionCallback>m__189()
extern "C"  PCMSetPositionCallback_t4244274966 * UnityEngine_AudioClipGenerated_U3CAudioClip_Create__String__Int32__Int32__Int32__Boolean__PCMReaderCallback__PCMSetPositionCallbackU3Em__189_m3837567464 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip/PCMReaderCallback UnityEngine_AudioClipGenerated::<AudioClip_Create__String__Int32__Int32__Int32__Boolean__PCMReaderCallback>m__18B()
extern "C"  PCMReaderCallback_t83861602 * UnityEngine_AudioClipGenerated_U3CAudioClip_Create__String__Int32__Int32__Int32__Boolean__PCMReaderCallbackU3Em__18B_m4068082557 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AudioClipGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_AudioClipGenerated_ilo_getObject1_m1434950760 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioClipGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_AudioClipGenerated_ilo_addJSCSRel2_m2589197991 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioClipGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_AudioClipGenerated_ilo_setSingle3_m3737010380 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioClipGenerated::ilo_setEnum4(System.Int32,System.Int32)
extern "C"  void UnityEngine_AudioClipGenerated_ilo_setEnum4_m2053108776 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioClipGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void UnityEngine_AudioClipGenerated_ilo_setBooleanS5_m1803643351 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AudioClipGenerated::ilo_getInt326(System.Int32)
extern "C"  int32_t UnityEngine_AudioClipGenerated_ilo_getInt326_m2997507958 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_AudioClipGenerated::ilo_addJSFunCSDelegateRel7(System.Int32,System.Delegate)
extern "C"  void UnityEngine_AudioClipGenerated_ilo_addJSFunCSDelegateRel7_m996502830 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioClipGenerated::ilo_getBooleanS8(System.Int32)
extern "C"  bool UnityEngine_AudioClipGenerated_ilo_getBooleanS8_m552928745 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AudioClipGenerated::ilo_setObject9(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_AudioClipGenerated_ilo_setObject9_m3868903912 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_AudioClipGenerated::ilo_getStringS10(System.Int32)
extern "C"  String_t* UnityEngine_AudioClipGenerated_ilo_getStringS10_m1375904172 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AudioClipGenerated::ilo_getArrayLength11(System.Int32)
extern "C"  int32_t UnityEngine_AudioClipGenerated_ilo_getArrayLength11_m3895414485 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_AudioClipGenerated::ilo_getSingle12(System.Int32)
extern "C"  float UnityEngine_AudioClipGenerated_ilo_getSingle12_m1731519929 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_AudioClipGenerated::ilo_getElement13(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_AudioClipGenerated_ilo_getElement13_m3008612771 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_AudioClipGenerated::ilo_isFunctionS14(System.Int32)
extern "C"  bool UnityEngine_AudioClipGenerated_ilo_isFunctionS14_m1607136646 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_AudioClipGenerated::ilo_getObject15(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_AudioClipGenerated_ilo_getObject15_m315235424 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AudioClip/PCMSetPositionCallback UnityEngine_AudioClipGenerated::ilo_AudioClip_Create_GetDelegate_member4_arg616(CSRepresentedObject)
extern "C"  PCMSetPositionCallback_t4244274966 * UnityEngine_AudioClipGenerated_ilo_AudioClip_Create_GetDelegate_member4_arg616_m3682583350 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
