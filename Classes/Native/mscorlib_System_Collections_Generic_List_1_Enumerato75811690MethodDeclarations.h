﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<shaderBuff>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1236270315(__this, ___l0, method) ((  void (*) (Enumerator_t75811690 *, List_1_t56138920 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<shaderBuff>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4173951111(__this, method) ((  void (*) (Enumerator_t75811690 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<shaderBuff>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2021283325(__this, method) ((  Il2CppObject * (*) (Enumerator_t75811690 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<shaderBuff>::Dispose()
#define Enumerator_Dispose_m3469620624(__this, method) ((  void (*) (Enumerator_t75811690 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<shaderBuff>::VerifyState()
#define Enumerator_VerifyState_m332180681(__this, method) ((  void (*) (Enumerator_t75811690 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<shaderBuff>::MoveNext()
#define Enumerator_MoveNext_m2732823607(__this, method) ((  bool (*) (Enumerator_t75811690 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<shaderBuff>::get_Current()
#define Enumerator_get_Current_m2287070498(__this, method) ((  shaderBuff_t2982920664 * (*) (Enumerator_t75811690 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
