﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.IDictionary`2<System.Reflection.ParameterInfo,System.Object>
struct IDictionary_2_t2530398666;
// System.Collections.Generic.IDictionary`2<Newtonsoft.Json.Serialization.JsonProperty,System.Object>
struct IDictionary_2_t1553015650;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Serialization.JsonSerializerInternalReader
struct JsonSerializerInternalReader_t3659144454;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Reflection.ParameterInfo,System.Object>,System.String>
struct Func_2_t1230433651;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<CreateObjectFromNonDefaultConstructor>c__AnonStorey135
struct  U3CCreateObjectFromNonDefaultConstructorU3Ec__AnonStorey135_t2130884323  : public Il2CppObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.Reflection.ParameterInfo,System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<CreateObjectFromNonDefaultConstructor>c__AnonStorey135::constructorParameters
	Il2CppObject* ___constructorParameters_0;
	// System.Collections.Generic.IDictionary`2<Newtonsoft.Json.Serialization.JsonProperty,System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<CreateObjectFromNonDefaultConstructor>c__AnonStorey135::remainingPropertyValues
	Il2CppObject* ___remainingPropertyValues_1;
	// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<CreateObjectFromNonDefaultConstructor>c__AnonStorey135::createdObject
	Il2CppObject * ___createdObject_2;
	// Newtonsoft.Json.Serialization.JsonSerializerInternalReader Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<CreateObjectFromNonDefaultConstructor>c__AnonStorey135::<>f__this
	JsonSerializerInternalReader_t3659144454 * ___U3CU3Ef__this_3;

public:
	inline static int32_t get_offset_of_constructorParameters_0() { return static_cast<int32_t>(offsetof(U3CCreateObjectFromNonDefaultConstructorU3Ec__AnonStorey135_t2130884323, ___constructorParameters_0)); }
	inline Il2CppObject* get_constructorParameters_0() const { return ___constructorParameters_0; }
	inline Il2CppObject** get_address_of_constructorParameters_0() { return &___constructorParameters_0; }
	inline void set_constructorParameters_0(Il2CppObject* value)
	{
		___constructorParameters_0 = value;
		Il2CppCodeGenWriteBarrier(&___constructorParameters_0, value);
	}

	inline static int32_t get_offset_of_remainingPropertyValues_1() { return static_cast<int32_t>(offsetof(U3CCreateObjectFromNonDefaultConstructorU3Ec__AnonStorey135_t2130884323, ___remainingPropertyValues_1)); }
	inline Il2CppObject* get_remainingPropertyValues_1() const { return ___remainingPropertyValues_1; }
	inline Il2CppObject** get_address_of_remainingPropertyValues_1() { return &___remainingPropertyValues_1; }
	inline void set_remainingPropertyValues_1(Il2CppObject* value)
	{
		___remainingPropertyValues_1 = value;
		Il2CppCodeGenWriteBarrier(&___remainingPropertyValues_1, value);
	}

	inline static int32_t get_offset_of_createdObject_2() { return static_cast<int32_t>(offsetof(U3CCreateObjectFromNonDefaultConstructorU3Ec__AnonStorey135_t2130884323, ___createdObject_2)); }
	inline Il2CppObject * get_createdObject_2() const { return ___createdObject_2; }
	inline Il2CppObject ** get_address_of_createdObject_2() { return &___createdObject_2; }
	inline void set_createdObject_2(Il2CppObject * value)
	{
		___createdObject_2 = value;
		Il2CppCodeGenWriteBarrier(&___createdObject_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_3() { return static_cast<int32_t>(offsetof(U3CCreateObjectFromNonDefaultConstructorU3Ec__AnonStorey135_t2130884323, ___U3CU3Ef__this_3)); }
	inline JsonSerializerInternalReader_t3659144454 * get_U3CU3Ef__this_3() const { return ___U3CU3Ef__this_3; }
	inline JsonSerializerInternalReader_t3659144454 ** get_address_of_U3CU3Ef__this_3() { return &___U3CU3Ef__this_3; }
	inline void set_U3CU3Ef__this_3(JsonSerializerInternalReader_t3659144454 * value)
	{
		___U3CU3Ef__this_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_3, value);
	}
};

struct U3CCreateObjectFromNonDefaultConstructorU3Ec__AnonStorey135_t2130884323_StaticFields
{
public:
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Reflection.ParameterInfo,System.Object>,System.String> Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<CreateObjectFromNonDefaultConstructor>c__AnonStorey135::<>f__am$cache4
	Func_2_t1230433651 * ___U3CU3Ef__amU24cache4_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(U3CCreateObjectFromNonDefaultConstructorU3Ec__AnonStorey135_t2130884323_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline Func_2_t1230433651 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline Func_2_t1230433651 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(Func_2_t1230433651 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
