﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Base_St344225277.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SevenZip.Compression.LZMA.Encoder/Optimal
struct  Optimal_t1633575781  : public Il2CppObject
{
public:
	// SevenZip.Compression.LZMA.Base/State SevenZip.Compression.LZMA.Encoder/Optimal::State
	State_t344225277  ___State_0;
	// System.Boolean SevenZip.Compression.LZMA.Encoder/Optimal::Prev1IsChar
	bool ___Prev1IsChar_1;
	// System.Boolean SevenZip.Compression.LZMA.Encoder/Optimal::Prev2
	bool ___Prev2_2;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder/Optimal::PosPrev2
	uint32_t ___PosPrev2_3;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder/Optimal::BackPrev2
	uint32_t ___BackPrev2_4;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder/Optimal::Price
	uint32_t ___Price_5;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder/Optimal::PosPrev
	uint32_t ___PosPrev_6;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder/Optimal::BackPrev
	uint32_t ___BackPrev_7;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder/Optimal::Backs0
	uint32_t ___Backs0_8;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder/Optimal::Backs1
	uint32_t ___Backs1_9;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder/Optimal::Backs2
	uint32_t ___Backs2_10;
	// System.UInt32 SevenZip.Compression.LZMA.Encoder/Optimal::Backs3
	uint32_t ___Backs3_11;

public:
	inline static int32_t get_offset_of_State_0() { return static_cast<int32_t>(offsetof(Optimal_t1633575781, ___State_0)); }
	inline State_t344225277  get_State_0() const { return ___State_0; }
	inline State_t344225277 * get_address_of_State_0() { return &___State_0; }
	inline void set_State_0(State_t344225277  value)
	{
		___State_0 = value;
	}

	inline static int32_t get_offset_of_Prev1IsChar_1() { return static_cast<int32_t>(offsetof(Optimal_t1633575781, ___Prev1IsChar_1)); }
	inline bool get_Prev1IsChar_1() const { return ___Prev1IsChar_1; }
	inline bool* get_address_of_Prev1IsChar_1() { return &___Prev1IsChar_1; }
	inline void set_Prev1IsChar_1(bool value)
	{
		___Prev1IsChar_1 = value;
	}

	inline static int32_t get_offset_of_Prev2_2() { return static_cast<int32_t>(offsetof(Optimal_t1633575781, ___Prev2_2)); }
	inline bool get_Prev2_2() const { return ___Prev2_2; }
	inline bool* get_address_of_Prev2_2() { return &___Prev2_2; }
	inline void set_Prev2_2(bool value)
	{
		___Prev2_2 = value;
	}

	inline static int32_t get_offset_of_PosPrev2_3() { return static_cast<int32_t>(offsetof(Optimal_t1633575781, ___PosPrev2_3)); }
	inline uint32_t get_PosPrev2_3() const { return ___PosPrev2_3; }
	inline uint32_t* get_address_of_PosPrev2_3() { return &___PosPrev2_3; }
	inline void set_PosPrev2_3(uint32_t value)
	{
		___PosPrev2_3 = value;
	}

	inline static int32_t get_offset_of_BackPrev2_4() { return static_cast<int32_t>(offsetof(Optimal_t1633575781, ___BackPrev2_4)); }
	inline uint32_t get_BackPrev2_4() const { return ___BackPrev2_4; }
	inline uint32_t* get_address_of_BackPrev2_4() { return &___BackPrev2_4; }
	inline void set_BackPrev2_4(uint32_t value)
	{
		___BackPrev2_4 = value;
	}

	inline static int32_t get_offset_of_Price_5() { return static_cast<int32_t>(offsetof(Optimal_t1633575781, ___Price_5)); }
	inline uint32_t get_Price_5() const { return ___Price_5; }
	inline uint32_t* get_address_of_Price_5() { return &___Price_5; }
	inline void set_Price_5(uint32_t value)
	{
		___Price_5 = value;
	}

	inline static int32_t get_offset_of_PosPrev_6() { return static_cast<int32_t>(offsetof(Optimal_t1633575781, ___PosPrev_6)); }
	inline uint32_t get_PosPrev_6() const { return ___PosPrev_6; }
	inline uint32_t* get_address_of_PosPrev_6() { return &___PosPrev_6; }
	inline void set_PosPrev_6(uint32_t value)
	{
		___PosPrev_6 = value;
	}

	inline static int32_t get_offset_of_BackPrev_7() { return static_cast<int32_t>(offsetof(Optimal_t1633575781, ___BackPrev_7)); }
	inline uint32_t get_BackPrev_7() const { return ___BackPrev_7; }
	inline uint32_t* get_address_of_BackPrev_7() { return &___BackPrev_7; }
	inline void set_BackPrev_7(uint32_t value)
	{
		___BackPrev_7 = value;
	}

	inline static int32_t get_offset_of_Backs0_8() { return static_cast<int32_t>(offsetof(Optimal_t1633575781, ___Backs0_8)); }
	inline uint32_t get_Backs0_8() const { return ___Backs0_8; }
	inline uint32_t* get_address_of_Backs0_8() { return &___Backs0_8; }
	inline void set_Backs0_8(uint32_t value)
	{
		___Backs0_8 = value;
	}

	inline static int32_t get_offset_of_Backs1_9() { return static_cast<int32_t>(offsetof(Optimal_t1633575781, ___Backs1_9)); }
	inline uint32_t get_Backs1_9() const { return ___Backs1_9; }
	inline uint32_t* get_address_of_Backs1_9() { return &___Backs1_9; }
	inline void set_Backs1_9(uint32_t value)
	{
		___Backs1_9 = value;
	}

	inline static int32_t get_offset_of_Backs2_10() { return static_cast<int32_t>(offsetof(Optimal_t1633575781, ___Backs2_10)); }
	inline uint32_t get_Backs2_10() const { return ___Backs2_10; }
	inline uint32_t* get_address_of_Backs2_10() { return &___Backs2_10; }
	inline void set_Backs2_10(uint32_t value)
	{
		___Backs2_10 = value;
	}

	inline static int32_t get_offset_of_Backs3_11() { return static_cast<int32_t>(offsetof(Optimal_t1633575781, ___Backs3_11)); }
	inline uint32_t get_Backs3_11() const { return ___Backs3_11; }
	inline uint32_t* get_address_of_Backs3_11() { return &___Backs3_11; }
	inline void set_Backs3_11(uint32_t value)
	{
		___Backs3_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
