﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Poly2Tri.TriangulationPoint
struct TriangulationPoint_t3810082933;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3810082933.h"
#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Orientat3899194662.h"

// System.Void Pathfinding.Poly2Tri.TriangulationUtil::.cctor()
extern "C"  void TriangulationUtil__cctor_m3997719299 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Poly2Tri.TriangulationUtil::SmartIncircle(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  bool TriangulationUtil_SmartIncircle_m2900908198 (Il2CppObject * __this /* static, unused */, TriangulationPoint_t3810082933 * ___pa0, TriangulationPoint_t3810082933 * ___pb1, TriangulationPoint_t3810082933 * ___pc2, TriangulationPoint_t3810082933 * ___pd3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Poly2Tri.TriangulationUtil::InScanArea(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  bool TriangulationUtil_InScanArea_m680109047 (Il2CppObject * __this /* static, unused */, TriangulationPoint_t3810082933 * ___pa0, TriangulationPoint_t3810082933 * ___pb1, TriangulationPoint_t3810082933 * ___pc2, TriangulationPoint_t3810082933 * ___pd3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Poly2Tri.Orientation Pathfinding.Poly2Tri.TriangulationUtil::Orient2d(Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint,Pathfinding.Poly2Tri.TriangulationPoint)
extern "C"  int32_t TriangulationUtil_Orient2d_m3842222219 (Il2CppObject * __this /* static, unused */, TriangulationPoint_t3810082933 * ___pa0, TriangulationPoint_t3810082933 * ___pb1, TriangulationPoint_t3810082933 * ___pc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
