﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_FileInfoBase3368634971.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FileInfoCrc
struct  FileInfoCrc_t1217045770  : public FileInfoBase_t3368634971
{
public:
	// System.String FileInfoCrc::mDataMD5
	String_t* ___mDataMD5_1;
	// System.String FileInfoCrc::mCrc32
	String_t* ___mCrc32_2;

public:
	inline static int32_t get_offset_of_mDataMD5_1() { return static_cast<int32_t>(offsetof(FileInfoCrc_t1217045770, ___mDataMD5_1)); }
	inline String_t* get_mDataMD5_1() const { return ___mDataMD5_1; }
	inline String_t** get_address_of_mDataMD5_1() { return &___mDataMD5_1; }
	inline void set_mDataMD5_1(String_t* value)
	{
		___mDataMD5_1 = value;
		Il2CppCodeGenWriteBarrier(&___mDataMD5_1, value);
	}

	inline static int32_t get_offset_of_mCrc32_2() { return static_cast<int32_t>(offsetof(FileInfoCrc_t1217045770, ___mCrc32_2)); }
	inline String_t* get_mCrc32_2() const { return ___mCrc32_2; }
	inline String_t** get_address_of_mCrc32_2() { return &___mCrc32_2; }
	inline void set_mCrc32_2(String_t* value)
	{
		___mCrc32_2 = value;
		Il2CppCodeGenWriteBarrier(&___mCrc32_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
