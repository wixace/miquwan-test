﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginZxhy
struct  PluginZxhy_t1606628034  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginZxhy::token
	String_t* ___token_2;
	// Mihua.SDK.PayInfo PluginZxhy::info
	PayInfo_t1775308120 * ___info_3;
	// System.Boolean PluginZxhy::isOut
	bool ___isOut_4;

public:
	inline static int32_t get_offset_of_token_2() { return static_cast<int32_t>(offsetof(PluginZxhy_t1606628034, ___token_2)); }
	inline String_t* get_token_2() const { return ___token_2; }
	inline String_t** get_address_of_token_2() { return &___token_2; }
	inline void set_token_2(String_t* value)
	{
		___token_2 = value;
		Il2CppCodeGenWriteBarrier(&___token_2, value);
	}

	inline static int32_t get_offset_of_info_3() { return static_cast<int32_t>(offsetof(PluginZxhy_t1606628034, ___info_3)); }
	inline PayInfo_t1775308120 * get_info_3() const { return ___info_3; }
	inline PayInfo_t1775308120 ** get_address_of_info_3() { return &___info_3; }
	inline void set_info_3(PayInfo_t1775308120 * value)
	{
		___info_3 = value;
		Il2CppCodeGenWriteBarrier(&___info_3, value);
	}

	inline static int32_t get_offset_of_isOut_4() { return static_cast<int32_t>(offsetof(PluginZxhy_t1606628034, ___isOut_4)); }
	inline bool get_isOut_4() const { return ___isOut_4; }
	inline bool* get_address_of_isOut_4() { return &___isOut_4; }
	inline void set_isOut_4(bool value)
	{
		___isOut_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
