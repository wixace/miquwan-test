﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1190435706MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,effectCfg>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3007514949(__this, ___dictionary0, method) ((  void (*) (Enumerator_t4140865818 *, Dictionary_2_t2823542426 *, const MethodInfo*))Enumerator__ctor_m2377115088_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,effectCfg>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m420801990(__this, method) ((  Il2CppObject * (*) (Enumerator_t4140865818 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1037642267_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,effectCfg>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m466986576(__this, method) ((  void (*) (Enumerator_t4140865818 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2809374949_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,effectCfg>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2805935239(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t4140865818 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2434214620_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,effectCfg>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3599777314(__this, method) ((  Il2CppObject * (*) (Enumerator_t4140865818 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3735627447_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,effectCfg>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2985761844(__this, method) ((  Il2CppObject * (*) (Enumerator_t4140865818 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m393753481_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,effectCfg>::MoveNext()
#define Enumerator_MoveNext_m425616640(__this, method) ((  bool (*) (Enumerator_t4140865818 *, const MethodInfo*))Enumerator_MoveNext_m1213995029_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,effectCfg>::get_Current()
#define Enumerator_get_Current_m3050960572(__this, method) ((  KeyValuePair_2_t2722323132  (*) (Enumerator_t4140865818 *, const MethodInfo*))Enumerator_get_Current_m1399860359_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,effectCfg>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m447502985(__this, method) ((  int32_t (*) (Enumerator_t4140865818 *, const MethodInfo*))Enumerator_get_CurrentKey_m1767398110_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,effectCfg>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1841983881(__this, method) ((  effectCfg_t2826279187 * (*) (Enumerator_t4140865818 *, const MethodInfo*))Enumerator_get_CurrentValue_m3384846750_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,effectCfg>::Reset()
#define Enumerator_Reset_m3700657175(__this, method) ((  void (*) (Enumerator_t4140865818 *, const MethodInfo*))Enumerator_Reset_m1080084514_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,effectCfg>::VerifyState()
#define Enumerator_VerifyState_m3179097824(__this, method) ((  void (*) (Enumerator_t4140865818 *, const MethodInfo*))Enumerator_VerifyState_m2404513451_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,effectCfg>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m4136133192(__this, method) ((  void (*) (Enumerator_t4140865818 *, const MethodInfo*))Enumerator_VerifyCurrent_m2789892947_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,effectCfg>::Dispose()
#define Enumerator_Dispose_m2622053159(__this, method) ((  void (*) (Enumerator_t4140865818 *, const MethodInfo*))Enumerator_Dispose_m1102561394_gshared)(__this, method)
