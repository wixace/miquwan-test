﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_gowchur105
struct  M_gowchur105_t1562199253  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_gowchur105::_lowtaDejearbu
	bool ____lowtaDejearbu_0;
	// System.UInt32 GarbageiOS.M_gowchur105::_couse
	uint32_t ____couse_1;
	// System.Boolean GarbageiOS.M_gowchur105::_loolouhaTrijou
	bool ____loolouhaTrijou_2;
	// System.Single GarbageiOS.M_gowchur105::_febu
	float ____febu_3;
	// System.UInt32 GarbageiOS.M_gowchur105::_stefalmayGeredel
	uint32_t ____stefalmayGeredel_4;
	// System.UInt32 GarbageiOS.M_gowchur105::_qazealarTeje
	uint32_t ____qazealarTeje_5;
	// System.UInt32 GarbageiOS.M_gowchur105::_rereeyou
	uint32_t ____rereeyou_6;
	// System.Boolean GarbageiOS.M_gowchur105::_keanuJirall
	bool ____keanuJirall_7;
	// System.Int32 GarbageiOS.M_gowchur105::_fawdowzurSase
	int32_t ____fawdowzurSase_8;
	// System.UInt32 GarbageiOS.M_gowchur105::_tracoo
	uint32_t ____tracoo_9;
	// System.Single GarbageiOS.M_gowchur105::_zearmallDesairre
	float ____zearmallDesairre_10;
	// System.Single GarbageiOS.M_gowchur105::_seegeZeebaxea
	float ____seegeZeebaxea_11;
	// System.String GarbageiOS.M_gowchur105::_zajuter
	String_t* ____zajuter_12;
	// System.Single GarbageiOS.M_gowchur105::_duteteDejucaw
	float ____duteteDejucaw_13;
	// System.Int32 GarbageiOS.M_gowchur105::_cuseeger
	int32_t ____cuseeger_14;
	// System.Int32 GarbageiOS.M_gowchur105::_manaihe
	int32_t ____manaihe_15;
	// System.Single GarbageiOS.M_gowchur105::_troocetallQearrihu
	float ____troocetallQearrihu_16;
	// System.Int32 GarbageiOS.M_gowchur105::_droutazel
	int32_t ____droutazel_17;
	// System.UInt32 GarbageiOS.M_gowchur105::_tranisZoris
	uint32_t ____tranisZoris_18;
	// System.Single GarbageiOS.M_gowchur105::_lismemveTrurvalrall
	float ____lismemveTrurvalrall_19;
	// System.UInt32 GarbageiOS.M_gowchur105::_jaljiTelsor
	uint32_t ____jaljiTelsor_20;
	// System.UInt32 GarbageiOS.M_gowchur105::_drisvu
	uint32_t ____drisvu_21;
	// System.String GarbageiOS.M_gowchur105::_verewhair
	String_t* ____verewhair_22;
	// System.Boolean GarbageiOS.M_gowchur105::_taljairJaqu
	bool ____taljairJaqu_23;
	// System.Boolean GarbageiOS.M_gowchur105::_mawrou
	bool ____mawrou_24;
	// System.UInt32 GarbageiOS.M_gowchur105::_tremascur
	uint32_t ____tremascur_25;
	// System.Int32 GarbageiOS.M_gowchur105::_wazeaBurcea
	int32_t ____wazeaBurcea_26;
	// System.UInt32 GarbageiOS.M_gowchur105::_coozeDoujeni
	uint32_t ____coozeDoujeni_27;
	// System.UInt32 GarbageiOS.M_gowchur105::_rouhouciRasurgis
	uint32_t ____rouhouciRasurgis_28;
	// System.Single GarbageiOS.M_gowchur105::_sairlirGearderral
	float ____sairlirGearderral_29;
	// System.Boolean GarbageiOS.M_gowchur105::_kilenirCeparzo
	bool ____kilenirCeparzo_30;
	// System.String GarbageiOS.M_gowchur105::_gesukeeWheeparje
	String_t* ____gesukeeWheeparje_31;
	// System.Boolean GarbageiOS.M_gowchur105::_daypel
	bool ____daypel_32;
	// System.Single GarbageiOS.M_gowchur105::_mereke
	float ____mereke_33;
	// System.String GarbageiOS.M_gowchur105::_terwupeWesaysoo
	String_t* ____terwupeWesaysoo_34;
	// System.Int32 GarbageiOS.M_gowchur105::_moserselSowcasee
	int32_t ____moserselSowcasee_35;
	// System.Single GarbageiOS.M_gowchur105::_haypasve
	float ____haypasve_36;
	// System.Single GarbageiOS.M_gowchur105::_memdoukeeRifar
	float ____memdoukeeRifar_37;
	// System.Single GarbageiOS.M_gowchur105::_pilehuZerow
	float ____pilehuZerow_38;
	// System.UInt32 GarbageiOS.M_gowchur105::_jorzer
	uint32_t ____jorzer_39;
	// System.Boolean GarbageiOS.M_gowchur105::_drowchor
	bool ____drowchor_40;
	// System.Boolean GarbageiOS.M_gowchur105::_jembebouJedu
	bool ____jembebouJedu_41;
	// System.Int32 GarbageiOS.M_gowchur105::_whebehe
	int32_t ____whebehe_42;
	// System.String GarbageiOS.M_gowchur105::_hesatel
	String_t* ____hesatel_43;
	// System.String GarbageiOS.M_gowchur105::_pusabuMayso
	String_t* ____pusabuMayso_44;
	// System.Single GarbageiOS.M_gowchur105::_pownou
	float ____pownou_45;
	// System.Boolean GarbageiOS.M_gowchur105::_jeleeli
	bool ____jeleeli_46;
	// System.Single GarbageiOS.M_gowchur105::_natertaiPelrertaw
	float ____natertaiPelrertaw_47;
	// System.String GarbageiOS.M_gowchur105::_doukeepisDrucheatay
	String_t* ____doukeepisDrucheatay_48;

public:
	inline static int32_t get_offset_of__lowtaDejearbu_0() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____lowtaDejearbu_0)); }
	inline bool get__lowtaDejearbu_0() const { return ____lowtaDejearbu_0; }
	inline bool* get_address_of__lowtaDejearbu_0() { return &____lowtaDejearbu_0; }
	inline void set__lowtaDejearbu_0(bool value)
	{
		____lowtaDejearbu_0 = value;
	}

	inline static int32_t get_offset_of__couse_1() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____couse_1)); }
	inline uint32_t get__couse_1() const { return ____couse_1; }
	inline uint32_t* get_address_of__couse_1() { return &____couse_1; }
	inline void set__couse_1(uint32_t value)
	{
		____couse_1 = value;
	}

	inline static int32_t get_offset_of__loolouhaTrijou_2() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____loolouhaTrijou_2)); }
	inline bool get__loolouhaTrijou_2() const { return ____loolouhaTrijou_2; }
	inline bool* get_address_of__loolouhaTrijou_2() { return &____loolouhaTrijou_2; }
	inline void set__loolouhaTrijou_2(bool value)
	{
		____loolouhaTrijou_2 = value;
	}

	inline static int32_t get_offset_of__febu_3() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____febu_3)); }
	inline float get__febu_3() const { return ____febu_3; }
	inline float* get_address_of__febu_3() { return &____febu_3; }
	inline void set__febu_3(float value)
	{
		____febu_3 = value;
	}

	inline static int32_t get_offset_of__stefalmayGeredel_4() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____stefalmayGeredel_4)); }
	inline uint32_t get__stefalmayGeredel_4() const { return ____stefalmayGeredel_4; }
	inline uint32_t* get_address_of__stefalmayGeredel_4() { return &____stefalmayGeredel_4; }
	inline void set__stefalmayGeredel_4(uint32_t value)
	{
		____stefalmayGeredel_4 = value;
	}

	inline static int32_t get_offset_of__qazealarTeje_5() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____qazealarTeje_5)); }
	inline uint32_t get__qazealarTeje_5() const { return ____qazealarTeje_5; }
	inline uint32_t* get_address_of__qazealarTeje_5() { return &____qazealarTeje_5; }
	inline void set__qazealarTeje_5(uint32_t value)
	{
		____qazealarTeje_5 = value;
	}

	inline static int32_t get_offset_of__rereeyou_6() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____rereeyou_6)); }
	inline uint32_t get__rereeyou_6() const { return ____rereeyou_6; }
	inline uint32_t* get_address_of__rereeyou_6() { return &____rereeyou_6; }
	inline void set__rereeyou_6(uint32_t value)
	{
		____rereeyou_6 = value;
	}

	inline static int32_t get_offset_of__keanuJirall_7() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____keanuJirall_7)); }
	inline bool get__keanuJirall_7() const { return ____keanuJirall_7; }
	inline bool* get_address_of__keanuJirall_7() { return &____keanuJirall_7; }
	inline void set__keanuJirall_7(bool value)
	{
		____keanuJirall_7 = value;
	}

	inline static int32_t get_offset_of__fawdowzurSase_8() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____fawdowzurSase_8)); }
	inline int32_t get__fawdowzurSase_8() const { return ____fawdowzurSase_8; }
	inline int32_t* get_address_of__fawdowzurSase_8() { return &____fawdowzurSase_8; }
	inline void set__fawdowzurSase_8(int32_t value)
	{
		____fawdowzurSase_8 = value;
	}

	inline static int32_t get_offset_of__tracoo_9() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____tracoo_9)); }
	inline uint32_t get__tracoo_9() const { return ____tracoo_9; }
	inline uint32_t* get_address_of__tracoo_9() { return &____tracoo_9; }
	inline void set__tracoo_9(uint32_t value)
	{
		____tracoo_9 = value;
	}

	inline static int32_t get_offset_of__zearmallDesairre_10() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____zearmallDesairre_10)); }
	inline float get__zearmallDesairre_10() const { return ____zearmallDesairre_10; }
	inline float* get_address_of__zearmallDesairre_10() { return &____zearmallDesairre_10; }
	inline void set__zearmallDesairre_10(float value)
	{
		____zearmallDesairre_10 = value;
	}

	inline static int32_t get_offset_of__seegeZeebaxea_11() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____seegeZeebaxea_11)); }
	inline float get__seegeZeebaxea_11() const { return ____seegeZeebaxea_11; }
	inline float* get_address_of__seegeZeebaxea_11() { return &____seegeZeebaxea_11; }
	inline void set__seegeZeebaxea_11(float value)
	{
		____seegeZeebaxea_11 = value;
	}

	inline static int32_t get_offset_of__zajuter_12() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____zajuter_12)); }
	inline String_t* get__zajuter_12() const { return ____zajuter_12; }
	inline String_t** get_address_of__zajuter_12() { return &____zajuter_12; }
	inline void set__zajuter_12(String_t* value)
	{
		____zajuter_12 = value;
		Il2CppCodeGenWriteBarrier(&____zajuter_12, value);
	}

	inline static int32_t get_offset_of__duteteDejucaw_13() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____duteteDejucaw_13)); }
	inline float get__duteteDejucaw_13() const { return ____duteteDejucaw_13; }
	inline float* get_address_of__duteteDejucaw_13() { return &____duteteDejucaw_13; }
	inline void set__duteteDejucaw_13(float value)
	{
		____duteteDejucaw_13 = value;
	}

	inline static int32_t get_offset_of__cuseeger_14() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____cuseeger_14)); }
	inline int32_t get__cuseeger_14() const { return ____cuseeger_14; }
	inline int32_t* get_address_of__cuseeger_14() { return &____cuseeger_14; }
	inline void set__cuseeger_14(int32_t value)
	{
		____cuseeger_14 = value;
	}

	inline static int32_t get_offset_of__manaihe_15() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____manaihe_15)); }
	inline int32_t get__manaihe_15() const { return ____manaihe_15; }
	inline int32_t* get_address_of__manaihe_15() { return &____manaihe_15; }
	inline void set__manaihe_15(int32_t value)
	{
		____manaihe_15 = value;
	}

	inline static int32_t get_offset_of__troocetallQearrihu_16() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____troocetallQearrihu_16)); }
	inline float get__troocetallQearrihu_16() const { return ____troocetallQearrihu_16; }
	inline float* get_address_of__troocetallQearrihu_16() { return &____troocetallQearrihu_16; }
	inline void set__troocetallQearrihu_16(float value)
	{
		____troocetallQearrihu_16 = value;
	}

	inline static int32_t get_offset_of__droutazel_17() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____droutazel_17)); }
	inline int32_t get__droutazel_17() const { return ____droutazel_17; }
	inline int32_t* get_address_of__droutazel_17() { return &____droutazel_17; }
	inline void set__droutazel_17(int32_t value)
	{
		____droutazel_17 = value;
	}

	inline static int32_t get_offset_of__tranisZoris_18() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____tranisZoris_18)); }
	inline uint32_t get__tranisZoris_18() const { return ____tranisZoris_18; }
	inline uint32_t* get_address_of__tranisZoris_18() { return &____tranisZoris_18; }
	inline void set__tranisZoris_18(uint32_t value)
	{
		____tranisZoris_18 = value;
	}

	inline static int32_t get_offset_of__lismemveTrurvalrall_19() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____lismemveTrurvalrall_19)); }
	inline float get__lismemveTrurvalrall_19() const { return ____lismemveTrurvalrall_19; }
	inline float* get_address_of__lismemveTrurvalrall_19() { return &____lismemveTrurvalrall_19; }
	inline void set__lismemveTrurvalrall_19(float value)
	{
		____lismemveTrurvalrall_19 = value;
	}

	inline static int32_t get_offset_of__jaljiTelsor_20() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____jaljiTelsor_20)); }
	inline uint32_t get__jaljiTelsor_20() const { return ____jaljiTelsor_20; }
	inline uint32_t* get_address_of__jaljiTelsor_20() { return &____jaljiTelsor_20; }
	inline void set__jaljiTelsor_20(uint32_t value)
	{
		____jaljiTelsor_20 = value;
	}

	inline static int32_t get_offset_of__drisvu_21() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____drisvu_21)); }
	inline uint32_t get__drisvu_21() const { return ____drisvu_21; }
	inline uint32_t* get_address_of__drisvu_21() { return &____drisvu_21; }
	inline void set__drisvu_21(uint32_t value)
	{
		____drisvu_21 = value;
	}

	inline static int32_t get_offset_of__verewhair_22() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____verewhair_22)); }
	inline String_t* get__verewhair_22() const { return ____verewhair_22; }
	inline String_t** get_address_of__verewhair_22() { return &____verewhair_22; }
	inline void set__verewhair_22(String_t* value)
	{
		____verewhair_22 = value;
		Il2CppCodeGenWriteBarrier(&____verewhair_22, value);
	}

	inline static int32_t get_offset_of__taljairJaqu_23() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____taljairJaqu_23)); }
	inline bool get__taljairJaqu_23() const { return ____taljairJaqu_23; }
	inline bool* get_address_of__taljairJaqu_23() { return &____taljairJaqu_23; }
	inline void set__taljairJaqu_23(bool value)
	{
		____taljairJaqu_23 = value;
	}

	inline static int32_t get_offset_of__mawrou_24() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____mawrou_24)); }
	inline bool get__mawrou_24() const { return ____mawrou_24; }
	inline bool* get_address_of__mawrou_24() { return &____mawrou_24; }
	inline void set__mawrou_24(bool value)
	{
		____mawrou_24 = value;
	}

	inline static int32_t get_offset_of__tremascur_25() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____tremascur_25)); }
	inline uint32_t get__tremascur_25() const { return ____tremascur_25; }
	inline uint32_t* get_address_of__tremascur_25() { return &____tremascur_25; }
	inline void set__tremascur_25(uint32_t value)
	{
		____tremascur_25 = value;
	}

	inline static int32_t get_offset_of__wazeaBurcea_26() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____wazeaBurcea_26)); }
	inline int32_t get__wazeaBurcea_26() const { return ____wazeaBurcea_26; }
	inline int32_t* get_address_of__wazeaBurcea_26() { return &____wazeaBurcea_26; }
	inline void set__wazeaBurcea_26(int32_t value)
	{
		____wazeaBurcea_26 = value;
	}

	inline static int32_t get_offset_of__coozeDoujeni_27() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____coozeDoujeni_27)); }
	inline uint32_t get__coozeDoujeni_27() const { return ____coozeDoujeni_27; }
	inline uint32_t* get_address_of__coozeDoujeni_27() { return &____coozeDoujeni_27; }
	inline void set__coozeDoujeni_27(uint32_t value)
	{
		____coozeDoujeni_27 = value;
	}

	inline static int32_t get_offset_of__rouhouciRasurgis_28() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____rouhouciRasurgis_28)); }
	inline uint32_t get__rouhouciRasurgis_28() const { return ____rouhouciRasurgis_28; }
	inline uint32_t* get_address_of__rouhouciRasurgis_28() { return &____rouhouciRasurgis_28; }
	inline void set__rouhouciRasurgis_28(uint32_t value)
	{
		____rouhouciRasurgis_28 = value;
	}

	inline static int32_t get_offset_of__sairlirGearderral_29() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____sairlirGearderral_29)); }
	inline float get__sairlirGearderral_29() const { return ____sairlirGearderral_29; }
	inline float* get_address_of__sairlirGearderral_29() { return &____sairlirGearderral_29; }
	inline void set__sairlirGearderral_29(float value)
	{
		____sairlirGearderral_29 = value;
	}

	inline static int32_t get_offset_of__kilenirCeparzo_30() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____kilenirCeparzo_30)); }
	inline bool get__kilenirCeparzo_30() const { return ____kilenirCeparzo_30; }
	inline bool* get_address_of__kilenirCeparzo_30() { return &____kilenirCeparzo_30; }
	inline void set__kilenirCeparzo_30(bool value)
	{
		____kilenirCeparzo_30 = value;
	}

	inline static int32_t get_offset_of__gesukeeWheeparje_31() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____gesukeeWheeparje_31)); }
	inline String_t* get__gesukeeWheeparje_31() const { return ____gesukeeWheeparje_31; }
	inline String_t** get_address_of__gesukeeWheeparje_31() { return &____gesukeeWheeparje_31; }
	inline void set__gesukeeWheeparje_31(String_t* value)
	{
		____gesukeeWheeparje_31 = value;
		Il2CppCodeGenWriteBarrier(&____gesukeeWheeparje_31, value);
	}

	inline static int32_t get_offset_of__daypel_32() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____daypel_32)); }
	inline bool get__daypel_32() const { return ____daypel_32; }
	inline bool* get_address_of__daypel_32() { return &____daypel_32; }
	inline void set__daypel_32(bool value)
	{
		____daypel_32 = value;
	}

	inline static int32_t get_offset_of__mereke_33() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____mereke_33)); }
	inline float get__mereke_33() const { return ____mereke_33; }
	inline float* get_address_of__mereke_33() { return &____mereke_33; }
	inline void set__mereke_33(float value)
	{
		____mereke_33 = value;
	}

	inline static int32_t get_offset_of__terwupeWesaysoo_34() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____terwupeWesaysoo_34)); }
	inline String_t* get__terwupeWesaysoo_34() const { return ____terwupeWesaysoo_34; }
	inline String_t** get_address_of__terwupeWesaysoo_34() { return &____terwupeWesaysoo_34; }
	inline void set__terwupeWesaysoo_34(String_t* value)
	{
		____terwupeWesaysoo_34 = value;
		Il2CppCodeGenWriteBarrier(&____terwupeWesaysoo_34, value);
	}

	inline static int32_t get_offset_of__moserselSowcasee_35() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____moserselSowcasee_35)); }
	inline int32_t get__moserselSowcasee_35() const { return ____moserselSowcasee_35; }
	inline int32_t* get_address_of__moserselSowcasee_35() { return &____moserselSowcasee_35; }
	inline void set__moserselSowcasee_35(int32_t value)
	{
		____moserselSowcasee_35 = value;
	}

	inline static int32_t get_offset_of__haypasve_36() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____haypasve_36)); }
	inline float get__haypasve_36() const { return ____haypasve_36; }
	inline float* get_address_of__haypasve_36() { return &____haypasve_36; }
	inline void set__haypasve_36(float value)
	{
		____haypasve_36 = value;
	}

	inline static int32_t get_offset_of__memdoukeeRifar_37() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____memdoukeeRifar_37)); }
	inline float get__memdoukeeRifar_37() const { return ____memdoukeeRifar_37; }
	inline float* get_address_of__memdoukeeRifar_37() { return &____memdoukeeRifar_37; }
	inline void set__memdoukeeRifar_37(float value)
	{
		____memdoukeeRifar_37 = value;
	}

	inline static int32_t get_offset_of__pilehuZerow_38() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____pilehuZerow_38)); }
	inline float get__pilehuZerow_38() const { return ____pilehuZerow_38; }
	inline float* get_address_of__pilehuZerow_38() { return &____pilehuZerow_38; }
	inline void set__pilehuZerow_38(float value)
	{
		____pilehuZerow_38 = value;
	}

	inline static int32_t get_offset_of__jorzer_39() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____jorzer_39)); }
	inline uint32_t get__jorzer_39() const { return ____jorzer_39; }
	inline uint32_t* get_address_of__jorzer_39() { return &____jorzer_39; }
	inline void set__jorzer_39(uint32_t value)
	{
		____jorzer_39 = value;
	}

	inline static int32_t get_offset_of__drowchor_40() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____drowchor_40)); }
	inline bool get__drowchor_40() const { return ____drowchor_40; }
	inline bool* get_address_of__drowchor_40() { return &____drowchor_40; }
	inline void set__drowchor_40(bool value)
	{
		____drowchor_40 = value;
	}

	inline static int32_t get_offset_of__jembebouJedu_41() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____jembebouJedu_41)); }
	inline bool get__jembebouJedu_41() const { return ____jembebouJedu_41; }
	inline bool* get_address_of__jembebouJedu_41() { return &____jembebouJedu_41; }
	inline void set__jembebouJedu_41(bool value)
	{
		____jembebouJedu_41 = value;
	}

	inline static int32_t get_offset_of__whebehe_42() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____whebehe_42)); }
	inline int32_t get__whebehe_42() const { return ____whebehe_42; }
	inline int32_t* get_address_of__whebehe_42() { return &____whebehe_42; }
	inline void set__whebehe_42(int32_t value)
	{
		____whebehe_42 = value;
	}

	inline static int32_t get_offset_of__hesatel_43() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____hesatel_43)); }
	inline String_t* get__hesatel_43() const { return ____hesatel_43; }
	inline String_t** get_address_of__hesatel_43() { return &____hesatel_43; }
	inline void set__hesatel_43(String_t* value)
	{
		____hesatel_43 = value;
		Il2CppCodeGenWriteBarrier(&____hesatel_43, value);
	}

	inline static int32_t get_offset_of__pusabuMayso_44() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____pusabuMayso_44)); }
	inline String_t* get__pusabuMayso_44() const { return ____pusabuMayso_44; }
	inline String_t** get_address_of__pusabuMayso_44() { return &____pusabuMayso_44; }
	inline void set__pusabuMayso_44(String_t* value)
	{
		____pusabuMayso_44 = value;
		Il2CppCodeGenWriteBarrier(&____pusabuMayso_44, value);
	}

	inline static int32_t get_offset_of__pownou_45() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____pownou_45)); }
	inline float get__pownou_45() const { return ____pownou_45; }
	inline float* get_address_of__pownou_45() { return &____pownou_45; }
	inline void set__pownou_45(float value)
	{
		____pownou_45 = value;
	}

	inline static int32_t get_offset_of__jeleeli_46() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____jeleeli_46)); }
	inline bool get__jeleeli_46() const { return ____jeleeli_46; }
	inline bool* get_address_of__jeleeli_46() { return &____jeleeli_46; }
	inline void set__jeleeli_46(bool value)
	{
		____jeleeli_46 = value;
	}

	inline static int32_t get_offset_of__natertaiPelrertaw_47() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____natertaiPelrertaw_47)); }
	inline float get__natertaiPelrertaw_47() const { return ____natertaiPelrertaw_47; }
	inline float* get_address_of__natertaiPelrertaw_47() { return &____natertaiPelrertaw_47; }
	inline void set__natertaiPelrertaw_47(float value)
	{
		____natertaiPelrertaw_47 = value;
	}

	inline static int32_t get_offset_of__doukeepisDrucheatay_48() { return static_cast<int32_t>(offsetof(M_gowchur105_t1562199253, ____doukeepisDrucheatay_48)); }
	inline String_t* get__doukeepisDrucheatay_48() const { return ____doukeepisDrucheatay_48; }
	inline String_t** get_address_of__doukeepisDrucheatay_48() { return &____doukeepisDrucheatay_48; }
	inline void set__doukeepisDrucheatay_48(String_t* value)
	{
		____doukeepisDrucheatay_48 = value;
		Il2CppCodeGenWriteBarrier(&____doukeepisDrucheatay_48, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
