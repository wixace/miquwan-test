﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._0270873a44d08c8d1bc068ea2101ebf6
struct _0270873a44d08c8d1bc068ea2101ebf6_t4016600813;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._0270873a44d08c8d1bc068ea2101ebf6::.ctor()
extern "C"  void _0270873a44d08c8d1bc068ea2101ebf6__ctor_m2742888928 (_0270873a44d08c8d1bc068ea2101ebf6_t4016600813 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._0270873a44d08c8d1bc068ea2101ebf6::_0270873a44d08c8d1bc068ea2101ebf6m2(System.Int32)
extern "C"  int32_t _0270873a44d08c8d1bc068ea2101ebf6__0270873a44d08c8d1bc068ea2101ebf6m2_m1908720249 (_0270873a44d08c8d1bc068ea2101ebf6_t4016600813 * __this, int32_t ____0270873a44d08c8d1bc068ea2101ebf6a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._0270873a44d08c8d1bc068ea2101ebf6::_0270873a44d08c8d1bc068ea2101ebf6m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _0270873a44d08c8d1bc068ea2101ebf6__0270873a44d08c8d1bc068ea2101ebf6m_m661561437 (_0270873a44d08c8d1bc068ea2101ebf6_t4016600813 * __this, int32_t ____0270873a44d08c8d1bc068ea2101ebf6a0, int32_t ____0270873a44d08c8d1bc068ea2101ebf6841, int32_t ____0270873a44d08c8d1bc068ea2101ebf6c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
