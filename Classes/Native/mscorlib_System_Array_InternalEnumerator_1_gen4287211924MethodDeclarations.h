﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen4287211924.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21209901952.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2645097291_gshared (InternalEnumerator_1_t4287211924 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2645097291(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t4287211924 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2645097291_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2805934197_gshared (InternalEnumerator_1_t4287211924 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2805934197(__this, method) ((  void (*) (InternalEnumerator_1_t4287211924 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m2805934197_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m624816993_gshared (InternalEnumerator_1_t4287211924 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m624816993(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t4287211924 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m624816993_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3095336674_gshared (InternalEnumerator_1_t4287211924 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3095336674(__this, method) ((  void (*) (InternalEnumerator_1_t4287211924 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3095336674_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3405326561_gshared (InternalEnumerator_1_t4287211924 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3405326561(__this, method) ((  bool (*) (InternalEnumerator_1_t4287211924 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3405326561_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<Entity.Behavior.EBehaviorID,System.Object>>::get_Current()
extern "C"  KeyValuePair_2_t1209901952  InternalEnumerator_1_get_Current_m1209109650_gshared (InternalEnumerator_1_t4287211924 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1209109650(__this, method) ((  KeyValuePair_2_t1209901952  (*) (InternalEnumerator_1_t4287211924 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1209109650_gshared)(__this, method)
