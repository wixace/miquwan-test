﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Pathfinding.IntRect>
struct List_1_t88276517;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat107949287.h"
#include "AssemblyU2DCSharp_Pathfinding_IntRect3015058261.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.IntRect>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1501480368_gshared (Enumerator_t107949287 * __this, List_1_t88276517 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m1501480368(__this, ___l0, method) ((  void (*) (Enumerator_t107949287 *, List_1_t88276517 *, const MethodInfo*))Enumerator__ctor_m1501480368_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.IntRect>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m223838370_gshared (Enumerator_t107949287 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m223838370(__this, method) ((  void (*) (Enumerator_t107949287 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m223838370_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.IntRect>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3705500686_gshared (Enumerator_t107949287 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3705500686(__this, method) ((  Il2CppObject * (*) (Enumerator_t107949287 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3705500686_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.IntRect>::Dispose()
extern "C"  void Enumerator_Dispose_m574865301_gshared (Enumerator_t107949287 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m574865301(__this, method) ((  void (*) (Enumerator_t107949287 *, const MethodInfo*))Enumerator_Dispose_m574865301_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.IntRect>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1035185230_gshared (Enumerator_t107949287 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1035185230(__this, method) ((  void (*) (Enumerator_t107949287 *, const MethodInfo*))Enumerator_VerifyState_m1035185230_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.IntRect>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3737121038_gshared (Enumerator_t107949287 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3737121038(__this, method) ((  bool (*) (Enumerator_t107949287 *, const MethodInfo*))Enumerator_MoveNext_m3737121038_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.IntRect>::get_Current()
extern "C"  IntRect_t3015058261  Enumerator_get_Current_m3127249157_gshared (Enumerator_t107949287 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3127249157(__this, method) ((  IntRect_t3015058261  (*) (Enumerator_t107949287 *, const MethodInfo*))Enumerator_get_Current_m3127249157_gshared)(__this, method)
