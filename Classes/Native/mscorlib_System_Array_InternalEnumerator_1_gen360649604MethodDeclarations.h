﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen360649604.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_Resolution1578306928.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Resolution>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m234142068_gshared (InternalEnumerator_1_t360649604 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m234142068(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t360649604 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m234142068_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Resolution>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m991559596_gshared (InternalEnumerator_1_t360649604 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m991559596(__this, method) ((  void (*) (InternalEnumerator_1_t360649604 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m991559596_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Resolution>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m192945816_gshared (InternalEnumerator_1_t360649604 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m192945816(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t360649604 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m192945816_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Resolution>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2696004683_gshared (InternalEnumerator_1_t360649604 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2696004683(__this, method) ((  void (*) (InternalEnumerator_1_t360649604 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2696004683_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Resolution>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1348575512_gshared (InternalEnumerator_1_t360649604 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1348575512(__this, method) ((  bool (*) (InternalEnumerator_1_t360649604 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1348575512_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Resolution>::get_Current()
extern "C"  Resolution_t1578306928  InternalEnumerator_1_get_Current_m1896018363_gshared (InternalEnumerator_1_t360649604 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m1896018363(__this, method) ((  Resolution_t1578306928  (*) (InternalEnumerator_1_t360649604 *, const MethodInfo*))InternalEnumerator_1_get_Current_m1896018363_gshared)(__this, method)
