﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIInputGenerated
struct UIInputGenerated_t228411545;
// JSVCall
struct JSVCall_t3708497963;
// UIInput/OnValidate
struct OnValidate_t2690340430;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;
// UIInput
struct UIInput_t289134998;
// UITexture
struct UITexture_t3903132647;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_UIInput289134998.h"

// System.Void UIInputGenerated::.ctor()
extern "C"  void UIInputGenerated__ctor_m2198698530 (UIInputGenerated_t228411545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIInputGenerated::UIInput_UIInput1(JSVCall,System.Int32)
extern "C"  bool UIInputGenerated_UIInput_UIInput1_m630580330 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_current(JSVCall)
extern "C"  void UIInputGenerated_UIInput_current_m2115475255 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_selection(JSVCall)
extern "C"  void UIInputGenerated_UIInput_selection_m1396780548 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_label(JSVCall)
extern "C"  void UIInputGenerated_UIInput_label_m3674376860 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_inputType(JSVCall)
extern "C"  void UIInputGenerated_UIInput_inputType_m1212736620 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_onReturnKey(JSVCall)
extern "C"  void UIInputGenerated_UIInput_onReturnKey_m1042529696 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_keyboardType(JSVCall)
extern "C"  void UIInputGenerated_UIInput_keyboardType_m1327264283 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_hideInput(JSVCall)
extern "C"  void UIInputGenerated_UIInput_hideInput_m4234984296 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_selectAllTextOnFocus(JSVCall)
extern "C"  void UIInputGenerated_UIInput_selectAllTextOnFocus_m3427241621 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_validation(JSVCall)
extern "C"  void UIInputGenerated_UIInput_validation_m583432675 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_characterLimit(JSVCall)
extern "C"  void UIInputGenerated_UIInput_characterLimit_m895943018 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_savedAs(JSVCall)
extern "C"  void UIInputGenerated_UIInput_savedAs_m3568465815 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_activeTextColor(JSVCall)
extern "C"  void UIInputGenerated_UIInput_activeTextColor_m1042007744 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_caretColor(JSVCall)
extern "C"  void UIInputGenerated_UIInput_caretColor_m2721680540 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_selectionColor(JSVCall)
extern "C"  void UIInputGenerated_UIInput_selectionColor_m3651471333 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_onSubmit(JSVCall)
extern "C"  void UIInputGenerated_UIInput_onSubmit_m447195845 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_onChange(JSVCall)
extern "C"  void UIInputGenerated_UIInput_onChange_m2747621933 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIInput/OnValidate UIInputGenerated::UIInput_onValidate_GetDelegate_member16_arg0(CSRepresentedObject)
extern "C"  OnValidate_t2690340430 * UIInputGenerated_UIInput_onValidate_GetDelegate_member16_arg0_m199885462 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_onValidate(JSVCall)
extern "C"  void UIInputGenerated_UIInput_onValidate_m2370622919 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_defaultText(JSVCall)
extern "C"  void UIInputGenerated_UIInput_defaultText_m2060267586 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_inputShouldBeHidden(JSVCall)
extern "C"  void UIInputGenerated_UIInput_inputShouldBeHidden_m2709954502 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_value(JSVCall)
extern "C"  void UIInputGenerated_UIInput_value_m2598761663 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_isSelected(JSVCall)
extern "C"  void UIInputGenerated_UIInput_isSelected_m2499215863 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_cursorPosition(JSVCall)
extern "C"  void UIInputGenerated_UIInput_cursorPosition_m3626116989 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_selectionStart(JSVCall)
extern "C"  void UIInputGenerated_UIInput_selectionStart_m4071596486 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_selectionEnd(JSVCall)
extern "C"  void UIInputGenerated_UIInput_selectionEnd_m2630288877 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::UIInput_caret(JSVCall)
extern "C"  void UIInputGenerated_UIInput_caret_m127685357 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIInputGenerated::UIInput_LoadValue(JSVCall,System.Int32)
extern "C"  bool UIInputGenerated_UIInput_LoadValue_m1333467742 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIInputGenerated::UIInput_RemoveFocus(JSVCall,System.Int32)
extern "C"  bool UIInputGenerated_UIInput_RemoveFocus_m3806228391 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIInputGenerated::UIInput_SaveValue(JSVCall,System.Int32)
extern "C"  bool UIInputGenerated_UIInput_SaveValue_m751171431 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIInputGenerated::UIInput_Submit(JSVCall,System.Int32)
extern "C"  bool UIInputGenerated_UIInput_Submit_m3010231047 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIInputGenerated::UIInput_UpdateLabel(JSVCall,System.Int32)
extern "C"  bool UIInputGenerated_UIInput_UpdateLabel_m278370654 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIInputGenerated::UIInput_Validate__String(JSVCall,System.Int32)
extern "C"  bool UIInputGenerated_UIInput_Validate__String_m1589562198 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::__Register()
extern "C"  void UIInputGenerated___Register_m737478629 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIInput/OnValidate UIInputGenerated::<UIInput_onValidate>m__151()
extern "C"  OnValidate_t2690340430 * UIInputGenerated_U3CUIInput_onValidateU3Em__151_m4270860789 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIInputGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UIInputGenerated_ilo_getObject1_m3610816048 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIInputGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIInputGenerated_ilo_getObject2_m3234276750 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIInputGenerated::ilo_getEnum3(System.Int32)
extern "C"  int32_t UIInputGenerated_ilo_getEnum3_m3148031920 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIInputGenerated::ilo_getBooleanS4(System.Int32)
extern "C"  bool UIInputGenerated_ilo_getBooleanS4_m2839164717 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void UIInputGenerated_ilo_setBooleanS5_m1279582607 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::ilo_setStringS6(System.Int32,System.String)
extern "C"  void UIInputGenerated_ilo_setStringS6_m2217368176 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIInputGenerated::ilo_getStringS7(System.Int32)
extern "C"  String_t* UIInputGenerated_ilo_getStringS7_m2776191718 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIInputGenerated::ilo_setObject8(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIInputGenerated_ilo_setObject8_m2463667567 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIInputGenerated::ilo_get_defaultText9(UIInput)
extern "C"  String_t* UIInputGenerated_ilo_get_defaultText9_m920138896 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIInputGenerated::ilo_get_value10(UIInput)
extern "C"  String_t* UIInputGenerated_ilo_get_value10_m3044287115 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIInputGenerated::ilo_get_isSelected11(UIInput)
extern "C"  bool UIInputGenerated_ilo_get_isSelected11_m2244347205 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::ilo_setInt3212(System.Int32,System.Int32)
extern "C"  void UIInputGenerated_ilo_setInt3212_m1941512964 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIInputGenerated::ilo_get_selectionEnd13(UIInput)
extern "C"  int32_t UIInputGenerated_ilo_get_selectionEnd13_m2238435231 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UITexture UIInputGenerated::ilo_get_caret14(UIInput)
extern "C"  UITexture_t3903132647 * UIInputGenerated_ilo_get_caret14_m3535696798 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::ilo_LoadValue15(UIInput)
extern "C"  void UIInputGenerated_ilo_LoadValue15_m3854303520 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::ilo_RemoveFocus16(UIInput)
extern "C"  void UIInputGenerated_ilo_RemoveFocus16_m4104932118 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::ilo_Submit17(UIInput)
extern "C"  void UIInputGenerated_ilo_Submit17_m3034928425 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIInputGenerated::ilo_UpdateLabel18(UIInput)
extern "C"  void UIInputGenerated_ilo_UpdateLabel18_m1841229821 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UIInputGenerated::ilo_Validate19(UIInput,System.String)
extern "C"  String_t* UIInputGenerated_ilo_Validate19_m3492691874 (Il2CppObject * __this /* static, unused */, UIInput_t289134998 * ____this0, String_t* ___val1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
