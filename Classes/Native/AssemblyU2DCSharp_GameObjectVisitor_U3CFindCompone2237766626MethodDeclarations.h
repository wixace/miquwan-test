﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameObjectVisitor/<FindComponentInChildren>c__AnonStorey165`1<System.Object>
struct U3CFindComponentInChildrenU3Ec__AnonStorey165_1_t2237766626;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void GameObjectVisitor/<FindComponentInChildren>c__AnonStorey165`1<System.Object>::.ctor()
extern "C"  void U3CFindComponentInChildrenU3Ec__AnonStorey165_1__ctor_m1501514937_gshared (U3CFindComponentInChildrenU3Ec__AnonStorey165_1_t2237766626 * __this, const MethodInfo* method);
#define U3CFindComponentInChildrenU3Ec__AnonStorey165_1__ctor_m1501514937(__this, method) ((  void (*) (U3CFindComponentInChildrenU3Ec__AnonStorey165_1_t2237766626 *, const MethodInfo*))U3CFindComponentInChildrenU3Ec__AnonStorey165_1__ctor_m1501514937_gshared)(__this, method)
// System.Boolean GameObjectVisitor/<FindComponentInChildren>c__AnonStorey165`1<System.Object>::<>m__48F(UnityEngine.GameObject)
extern "C"  bool U3CFindComponentInChildrenU3Ec__AnonStorey165_1_U3CU3Em__48F_m817890382_gshared (U3CFindComponentInChildrenU3Ec__AnonStorey165_1_t2237766626 * __this, GameObject_t3674682005 * ___obj0, const MethodInfo* method);
#define U3CFindComponentInChildrenU3Ec__AnonStorey165_1_U3CU3Em__48F_m817890382(__this, ___obj0, method) ((  bool (*) (U3CFindComponentInChildrenU3Ec__AnonStorey165_1_t2237766626 *, GameObject_t3674682005 *, const MethodInfo*))U3CFindComponentInChildrenU3Ec__AnonStorey165_1_U3CU3Em__48F_m817890382_gshared)(__this, ___obj0, method)
