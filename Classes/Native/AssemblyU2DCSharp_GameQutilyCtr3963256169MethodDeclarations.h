﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameQutilyCtr
struct GameQutilyCtr_t3963256169;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t1536434148;
// System.String
struct String_t;
// GlobalGOMgr
struct GlobalGOMgr_t803081773;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GameQuality2185543821.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_GameQutilyCtr3963256169.h"
#include "AssemblyU2DCSharp_GlobalGOMgr803081773.h"

// System.Void GameQutilyCtr::.ctor()
extern "C"  void GameQutilyCtr__ctor_m706472482 (GameQutilyCtr_t3963256169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameQutilyCtr::set_gameQuality(GameQuality)
extern "C"  void GameQutilyCtr_set_gameQuality_m818837013 (GameQutilyCtr_t3963256169 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameQuality GameQutilyCtr::get_gameQuality()
extern "C"  int32_t GameQutilyCtr_get_gameQuality_m3137222026 (GameQutilyCtr_t3963256169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameQutilyCtr::Finalize()
extern "C"  void GameQutilyCtr_Finalize_m1284467456 (GameQutilyCtr_t3963256169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameQutilyCtr::SetGameQuatily(CEvent.ZEvent)
extern "C"  void GameQutilyCtr_SetGameQuatily_m1273803214 (GameQutilyCtr_t3963256169 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GameQutilyCtr::BinStaticParticle(UnityEngine.ParticleSystem[])
extern "C"  int32_t GameQutilyCtr_BinStaticParticle_m1760938139 (GameQutilyCtr_t3963256169 * __this, ParticleSystemU5BU5D_t1536434148* ___particles0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameQutilyCtr::UnBinStaticParticle(System.Int32)
extern "C"  void GameQutilyCtr_UnBinStaticParticle_m2052993331 (GameQutilyCtr_t3963256169 * __this, int32_t ___uId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String GameQutilyCtr::GetTrackParticleName(System.String)
extern "C"  String_t* GameQutilyCtr_GetTrackParticleName_m1998593437 (GameQutilyCtr_t3963256169 * __this, String_t* ___effectName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameQutilyCtr::SetScreenResolution()
extern "C"  void GameQutilyCtr_SetScreenResolution_m2833951034 (GameQutilyCtr_t3963256169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameQutilyCtr::SetParticleQuality()
extern "C"  void GameQutilyCtr_SetParticleQuality_m3143586361 (GameQutilyCtr_t3963256169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameQutilyCtr::SetStaticParticleState(System.Boolean)
extern "C"  void GameQutilyCtr_SetStaticParticleState_m3410880180 (GameQutilyCtr_t3963256169 * __this, bool ___isShow0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameQutilyCtr::ShowStaticParticle(System.Int32)
extern "C"  void GameQutilyCtr_ShowStaticParticle_m2107165988 (GameQutilyCtr_t3963256169 * __this, int32_t ___uId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameQutilyCtr::HideStaticParticle(System.Int32)
extern "C"  void GameQutilyCtr_HideStaticParticle_m3378865641 (GameQutilyCtr_t3963256169 * __this, int32_t ___uId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameQutilyCtr::SetStaticParticleState(System.Int32,System.Boolean)
extern "C"  void GameQutilyCtr_SetStaticParticleState_m1443015663 (GameQutilyCtr_t3963256169 * __this, int32_t ___uId0, bool ___isActive1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameQutilyCtr::Clear()
extern "C"  void GameQutilyCtr_Clear_m2407573069 (GameQutilyCtr_t3963256169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GameQuality GameQutilyCtr::ilo_get_gameQuality1(GameQutilyCtr)
extern "C"  int32_t GameQutilyCtr_ilo_get_gameQuality1_m1323933549 (Il2CppObject * __this /* static, unused */, GameQutilyCtr_t3963256169 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// GlobalGOMgr GameQutilyCtr::ilo_get_GlobalGOMgr2()
extern "C"  GlobalGOMgr_t803081773 * GameQutilyCtr_ilo_get_GlobalGOMgr2_m3371533149 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameQutilyCtr::ilo_PlayOverEffect3(GlobalGOMgr)
extern "C"  void GameQutilyCtr_ilo_PlayOverEffect3_m2490334942 (Il2CppObject * __this /* static, unused */, GlobalGOMgr_t803081773 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameQutilyCtr::ilo_SetStaticParticleState4(GameQutilyCtr,System.Boolean)
extern "C"  void GameQutilyCtr_ilo_SetStaticParticleState4_m4267909210 (Il2CppObject * __this /* static, unused */, GameQutilyCtr_t3963256169 * ____this0, bool ___isShow1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
