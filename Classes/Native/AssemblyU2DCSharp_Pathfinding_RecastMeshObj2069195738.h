﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.RecastBBTree
struct RecastBBTree_t4266206694;
// System.Collections.Generic.List`1<Pathfinding.RecastMeshObj>
struct List_1_t3437381290;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RecastMeshObj
struct  RecastMeshObj_t2069195738  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Bounds Pathfinding.RecastMeshObj::bounds
	Bounds_t2711641849  ___bounds_4;
	// System.Boolean Pathfinding.RecastMeshObj::dynamic
	bool ___dynamic_5;
	// System.Int32 Pathfinding.RecastMeshObj::area
	int32_t ___area_6;
	// System.Boolean Pathfinding.RecastMeshObj::_dynamic
	bool ____dynamic_7;
	// System.Boolean Pathfinding.RecastMeshObj::registered
	bool ___registered_8;

public:
	inline static int32_t get_offset_of_bounds_4() { return static_cast<int32_t>(offsetof(RecastMeshObj_t2069195738, ___bounds_4)); }
	inline Bounds_t2711641849  get_bounds_4() const { return ___bounds_4; }
	inline Bounds_t2711641849 * get_address_of_bounds_4() { return &___bounds_4; }
	inline void set_bounds_4(Bounds_t2711641849  value)
	{
		___bounds_4 = value;
	}

	inline static int32_t get_offset_of_dynamic_5() { return static_cast<int32_t>(offsetof(RecastMeshObj_t2069195738, ___dynamic_5)); }
	inline bool get_dynamic_5() const { return ___dynamic_5; }
	inline bool* get_address_of_dynamic_5() { return &___dynamic_5; }
	inline void set_dynamic_5(bool value)
	{
		___dynamic_5 = value;
	}

	inline static int32_t get_offset_of_area_6() { return static_cast<int32_t>(offsetof(RecastMeshObj_t2069195738, ___area_6)); }
	inline int32_t get_area_6() const { return ___area_6; }
	inline int32_t* get_address_of_area_6() { return &___area_6; }
	inline void set_area_6(int32_t value)
	{
		___area_6 = value;
	}

	inline static int32_t get_offset_of__dynamic_7() { return static_cast<int32_t>(offsetof(RecastMeshObj_t2069195738, ____dynamic_7)); }
	inline bool get__dynamic_7() const { return ____dynamic_7; }
	inline bool* get_address_of__dynamic_7() { return &____dynamic_7; }
	inline void set__dynamic_7(bool value)
	{
		____dynamic_7 = value;
	}

	inline static int32_t get_offset_of_registered_8() { return static_cast<int32_t>(offsetof(RecastMeshObj_t2069195738, ___registered_8)); }
	inline bool get_registered_8() const { return ___registered_8; }
	inline bool* get_address_of_registered_8() { return &___registered_8; }
	inline void set_registered_8(bool value)
	{
		___registered_8 = value;
	}
};

struct RecastMeshObj_t2069195738_StaticFields
{
public:
	// Pathfinding.RecastBBTree Pathfinding.RecastMeshObj::tree
	RecastBBTree_t4266206694 * ___tree_2;
	// System.Collections.Generic.List`1<Pathfinding.RecastMeshObj> Pathfinding.RecastMeshObj::dynamicMeshObjs
	List_1_t3437381290 * ___dynamicMeshObjs_3;

public:
	inline static int32_t get_offset_of_tree_2() { return static_cast<int32_t>(offsetof(RecastMeshObj_t2069195738_StaticFields, ___tree_2)); }
	inline RecastBBTree_t4266206694 * get_tree_2() const { return ___tree_2; }
	inline RecastBBTree_t4266206694 ** get_address_of_tree_2() { return &___tree_2; }
	inline void set_tree_2(RecastBBTree_t4266206694 * value)
	{
		___tree_2 = value;
		Il2CppCodeGenWriteBarrier(&___tree_2, value);
	}

	inline static int32_t get_offset_of_dynamicMeshObjs_3() { return static_cast<int32_t>(offsetof(RecastMeshObj_t2069195738_StaticFields, ___dynamicMeshObjs_3)); }
	inline List_1_t3437381290 * get_dynamicMeshObjs_3() const { return ___dynamicMeshObjs_3; }
	inline List_1_t3437381290 ** get_address_of_dynamicMeshObjs_3() { return &___dynamicMeshObjs_3; }
	inline void set_dynamicMeshObjs_3(List_1_t3437381290 * value)
	{
		___dynamicMeshObjs_3 = value;
		Il2CppCodeGenWriteBarrier(&___dynamicMeshObjs_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
