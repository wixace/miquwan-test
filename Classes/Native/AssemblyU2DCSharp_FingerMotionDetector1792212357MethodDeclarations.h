﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FingerMotionDetector
struct FingerMotionDetector_t1792212357;
// FingerEventDetector`1/FingerEventHandler<FingerMotionEvent>
struct FingerEventHandler_t1620768800;
// FingerMotionEvent
struct FingerMotionEvent_t3307437371;
// FingerGestures/Finger
struct Finger_t182428197;
// FingerEvent
struct FingerEvent_t252338321;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_FingerMotionEvent3307437371.h"
#include "AssemblyU2DCSharp_FingerMotionDetector_EventType1050220746.h"
#include "AssemblyU2DCSharp_FingerMotionPhase3317175324.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_FingerGestures_Finger182428197.h"
#include "AssemblyU2DCSharp_FingerEvent252338321.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_FingerGestures_FingerPhase2639332950.h"
#include "AssemblyU2DCSharp_FingerMotionDetector1792212357.h"

// System.Void FingerMotionDetector::.ctor()
extern "C"  void FingerMotionDetector__ctor_m931775158 (FingerMotionDetector_t1792212357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerMotionDetector::add_OnFingerMove(FingerEventDetector`1/FingerEventHandler<FingerMotionEvent>)
extern "C"  void FingerMotionDetector_add_OnFingerMove_m1040514286 (FingerMotionDetector_t1792212357 * __this, FingerEventHandler_t1620768800 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerMotionDetector::remove_OnFingerMove(FingerEventDetector`1/FingerEventHandler<FingerMotionEvent>)
extern "C"  void FingerMotionDetector_remove_OnFingerMove_m911647435 (FingerMotionDetector_t1792212357 * __this, FingerEventHandler_t1620768800 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerMotionDetector::add_OnFingerStationary(FingerEventDetector`1/FingerEventHandler<FingerMotionEvent>)
extern "C"  void FingerMotionDetector_add_OnFingerStationary_m2050580779 (FingerMotionDetector_t1792212357 * __this, FingerEventHandler_t1620768800 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerMotionDetector::remove_OnFingerStationary(FingerEventDetector`1/FingerEventHandler<FingerMotionEvent>)
extern "C"  void FingerMotionDetector_remove_OnFingerStationary_m1086090568 (FingerMotionDetector_t1792212357 * __this, FingerEventHandler_t1620768800 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FingerMotionDetector::FireEvent(FingerMotionEvent,FingerMotionDetector/EventType,FingerMotionPhase,UnityEngine.Vector2,System.Boolean)
extern "C"  bool FingerMotionDetector_FireEvent_m1059152688 (FingerMotionDetector_t1792212357 * __this, FingerMotionEvent_t3307437371 * ___e0, int32_t ___eventType1, int32_t ___phase2, Vector2_t4282066565  ___position3, bool ___updateSelection4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerMotionDetector::ProcessFinger(FingerGestures/Finger)
extern "C"  void FingerMotionDetector_ProcessFinger_m3596376353 (FingerMotionDetector_t1792212357 * __this, Finger_t182428197 * ___finger0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerMotionDetector::ilo_set_Phase1(FingerMotionEvent,FingerMotionPhase)
extern "C"  void FingerMotionDetector_ilo_set_Phase1_m2094523091 (Il2CppObject * __this /* static, unused */, FingerMotionEvent_t3307437371 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FingerMotionDetector::ilo_set_Name2(FingerEvent,System.String)
extern "C"  void FingerMotionDetector_ilo_set_Name2_m1601704754 (Il2CppObject * __this /* static, unused */, FingerEvent_t252338321 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/FingerPhase FingerMotionDetector::ilo_get_Phase3(FingerGestures/Finger)
extern "C"  int32_t FingerMotionDetector_ilo_get_Phase3_m1406060036 (Il2CppObject * __this /* static, unused */, Finger_t182428197 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FingerGestures/FingerPhase FingerMotionDetector::ilo_get_PreviousPhase4(FingerGestures/Finger)
extern "C"  int32_t FingerMotionDetector_ilo_get_PreviousPhase4_m2138870060 (Il2CppObject * __this /* static, unused */, Finger_t182428197 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 FingerMotionDetector::ilo_get_Position5(FingerGestures/Finger)
extern "C"  Vector2_t4282066565  FingerMotionDetector_ilo_get_Position5_m104508228 (Il2CppObject * __this /* static, unused */, Finger_t182428197 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean FingerMotionDetector::ilo_FireEvent6(FingerMotionDetector,FingerMotionEvent,FingerMotionDetector/EventType,FingerMotionPhase,UnityEngine.Vector2,System.Boolean)
extern "C"  bool FingerMotionDetector_ilo_FireEvent6_m2271430864 (Il2CppObject * __this /* static, unused */, FingerMotionDetector_t1792212357 * ____this0, FingerMotionEvent_t3307437371 * ___e1, int32_t ___eventType2, int32_t ___phase3, Vector2_t4282066565  ___position4, bool ___updateSelection5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
