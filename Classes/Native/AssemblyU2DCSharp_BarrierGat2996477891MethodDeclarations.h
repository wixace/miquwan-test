﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BarrierGat
struct BarrierGat_t2996477891;
// CombatEntity
struct CombatEntity_t684137495;
// monstersCfg
struct monstersCfg_t1542396363;
// JSCLevelMonsterConfig
struct JSCLevelMonsterConfig_t1924079698;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// Monster
struct Monster_t2901270458;
// NpcMgr
struct NpcMgr_t2339534743;
// AstarPath
struct AstarPath_t4090270936;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_monstersCfg1542396363.h"
#include "AssemblyU2DCSharp_JSCLevelMonsterConfig1924079698.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_Monster2901270458.h"
#include "AssemblyU2DCSharp_BarrierGat2996477891.h"
#include "AssemblyU2DCSharp_NpcMgr2339534743.h"
#include "AssemblyU2DCSharp_AstarPath4090270936.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"

// System.Void BarrierGat::.ctor()
extern "C"  void BarrierGat__ctor_m3886351416 (BarrierGat_t2996477891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarrierGat::OnTriggerFun(System.Int32)
extern "C"  void BarrierGat_OnTriggerFun_m3697107907 (BarrierGat_t2996477891 * __this, int32_t ___effectId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarrierGat::OnTriggerFun(CombatEntity,System.Int32)
extern "C"  void BarrierGat_OnTriggerFun_m1494587900 (BarrierGat_t2996477891 * __this, CombatEntity_t684137495 * ___main0, int32_t ___effectId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarrierGat::OnTriggerBehaivir(CombatEntity)
extern "C"  void BarrierGat_OnTriggerBehaivir_m3236305258 (BarrierGat_t2996477891 * __this, CombatEntity_t684137495 * ___main0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarrierGat::Init()
extern "C"  void BarrierGat_Init_m492544796 (BarrierGat_t2996477891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarrierGat::Create(monstersCfg,JSCLevelMonsterConfig)
extern "C"  void BarrierGat_Create_m3854775769 (BarrierGat_t2996477891 * __this, monstersCfg_t1542396363 * ___mcfg0, JSCLevelMonsterConfig_t1924079698 * ___lmcfg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarrierGat::EventOpened(CEvent.ZEvent)
extern "C"  void BarrierGat_EventOpened_m1696029858 (BarrierGat_t2996477891 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarrierGat::EventClosed(CEvent.ZEvent)
extern "C"  void BarrierGat_EventClosed_m249937215 (BarrierGat_t2996477891 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarrierGat::SetBarrierGateState(System.Boolean)
extern "C"  void BarrierGat_SetBarrierGateState_m2217536158 (BarrierGat_t2996477891 * __this, bool ___active0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarrierGat::UpdateHpBar()
extern "C"  void BarrierGat_UpdateHpBar_m651910072 (BarrierGat_t2996477891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarrierGat::CalcuFinalProp()
extern "C"  void BarrierGat_CalcuFinalProp_m2517967653 (BarrierGat_t2996477891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarrierGat::Update()
extern "C"  void BarrierGat_Update_m1944671701 (BarrierGat_t2996477891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarrierGat::Clear()
extern "C"  void BarrierGat_Clear_m1292484707 (BarrierGat_t2996477891 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarrierGat::ilo_Create1(Monster,monstersCfg,JSCLevelMonsterConfig)
extern "C"  void BarrierGat_ilo_Create1_m1977828635 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, monstersCfg_t1542396363 * ___mcfg1, JSCLevelMonsterConfig_t1924079698 * ___lmcfg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarrierGat::ilo_set_isNotSelected2(CombatEntity,System.Boolean)
extern "C"  void BarrierGat_ilo_set_isNotSelected2_m3014344074 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarrierGat::ilo_SetBarrierGateState3(BarrierGat,System.Boolean)
extern "C"  void BarrierGat_ilo_SetBarrierGateState3_m3375949791 (Il2CppObject * __this /* static, unused */, BarrierGat_t2996477891 * ____this0, bool ___active1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NpcMgr BarrierGat::ilo_get_instance4()
extern "C"  NpcMgr_t2339534743 * BarrierGat_ilo_get_instance4_m400390259 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BarrierGat::ilo_get_curRound5(NpcMgr)
extern "C"  int32_t BarrierGat_ilo_get_curRound5_m2928561404 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BarrierGat::ilo_UpdateGraphs6(AstarPath,UnityEngine.Bounds)
extern "C"  void BarrierGat_ilo_UpdateGraphs6_m2422243627 (Il2CppObject * __this /* static, unused */, AstarPath_t4090270936 * ____this0, Bounds_t2711641849  ___bounds1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
