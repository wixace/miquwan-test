﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIEventListenerGenerated/<UIEventListener_onClick_GetDelegate_member2_arg0>c__AnonStoreyB6
struct U3CUIEventListener_onClick_GetDelegate_member2_arg0U3Ec__AnonStoreyB6_t1617523615;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UIEventListenerGenerated/<UIEventListener_onClick_GetDelegate_member2_arg0>c__AnonStoreyB6::.ctor()
extern "C"  void U3CUIEventListener_onClick_GetDelegate_member2_arg0U3Ec__AnonStoreyB6__ctor_m2410731484 (U3CUIEventListener_onClick_GetDelegate_member2_arg0U3Ec__AnonStoreyB6_t1617523615 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIEventListenerGenerated/<UIEventListener_onClick_GetDelegate_member2_arg0>c__AnonStoreyB6::<>m__130(UnityEngine.GameObject)
extern "C"  void U3CUIEventListener_onClick_GetDelegate_member2_arg0U3Ec__AnonStoreyB6_U3CU3Em__130_m1296175843 (U3CUIEventListener_onClick_GetDelegate_member2_arg0U3Ec__AnonStoreyB6_t1617523615 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
