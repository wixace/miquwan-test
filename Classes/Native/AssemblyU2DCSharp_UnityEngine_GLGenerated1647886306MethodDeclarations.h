﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GLGenerated
struct UnityEngine_GLGenerated_t1647886306;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_GLGenerated::.ctor()
extern "C"  void UnityEngine_GLGenerated__ctor_m3339869641 (UnityEngine_GLGenerated_t1647886306 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_GL1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_GL1_m4193150121 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GLGenerated::GL_TRIANGLES(JSVCall)
extern "C"  void UnityEngine_GLGenerated_GL_TRIANGLES_m63311803 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GLGenerated::GL_TRIANGLE_STRIP(JSVCall)
extern "C"  void UnityEngine_GLGenerated_GL_TRIANGLE_STRIP_m3739298693 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GLGenerated::GL_QUADS(JSVCall)
extern "C"  void UnityEngine_GLGenerated_GL_QUADS_m3977051194 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GLGenerated::GL_LINES(JSVCall)
extern "C"  void UnityEngine_GLGenerated_GL_LINES_m1316869799 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GLGenerated::GL_modelview(JSVCall)
extern "C"  void UnityEngine_GLGenerated_GL_modelview_m3734401304 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GLGenerated::GL_wireframe(JSVCall)
extern "C"  void UnityEngine_GLGenerated_GL_wireframe_m897046430 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GLGenerated::GL_sRGBWrite(JSVCall)
extern "C"  void UnityEngine_GLGenerated_GL_sRGBWrite_m3409760161 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GLGenerated::GL_invertCulling(JSVCall)
extern "C"  void UnityEngine_GLGenerated_GL_invertCulling_m1460407148 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_Begin__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_Begin__Int32_m1749387786 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_Clear__Boolean__Boolean__Color__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_Clear__Boolean__Boolean__Color__Single_m754959747 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_Clear__Boolean__Boolean__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_Clear__Boolean__Boolean__Color_m189030715 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_ClearWithSkybox__Boolean__Camera(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_ClearWithSkybox__Boolean__Camera_m3413226837 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_Color__Color(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_Color__Color_m3726612645 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_End(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_End_m832851000 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_GetGPUProjectionMatrix__Matrix4x4__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_GetGPUProjectionMatrix__Matrix4x4__Boolean_m760995268 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_InvalidateState(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_InvalidateState_m3291845427 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_IssuePluginEvent__IntPtr__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_IssuePluginEvent__IntPtr__Int32_m2193273790 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_LoadIdentity(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_LoadIdentity_m2896613065 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_LoadOrtho(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_LoadOrtho_m2797075855 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_LoadPixelMatrix__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_LoadPixelMatrix__Single__Single__Single__Single_m98311966 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_LoadPixelMatrix(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_LoadPixelMatrix_m1433864702 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_LoadProjectionMatrix__Matrix4x4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_LoadProjectionMatrix__Matrix4x4_m1538589622 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_MultiTexCoord__Int32__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_MultiTexCoord__Int32__Vector3_m2710356934 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_MultiTexCoord2__Int32__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_MultiTexCoord2__Int32__Single__Single_m2976307056 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_MultiTexCoord3__Int32__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_MultiTexCoord3__Int32__Single__Single__Single_m3901928791 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_MultMatrix__Matrix4x4(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_MultMatrix__Matrix4x4_m2388079867 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_PopMatrix(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_PopMatrix_m1577518223 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_PushMatrix(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_PushMatrix_m2839702592 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_RenderTargetBarrier(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_RenderTargetBarrier_m1785267341 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_TexCoord__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_TexCoord__Vector3_m3035869983 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_TexCoord2__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_TexCoord2__Single__Single_m2331482385 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_TexCoord3__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_TexCoord3__Single__Single__Single_m1681583514 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_Vertex__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_Vertex__Vector3_m1042853065 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_Vertex3__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_Vertex3__Single__Single__Single_m3475723076 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::GL_Viewport__Rect(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GLGenerated_GL_Viewport__Rect_m3165963151 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GLGenerated::__Register()
extern "C"  void UnityEngine_GLGenerated___Register_m1132418206 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GLGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_GLGenerated_ilo_getObject1_m185973261 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_GLGenerated_ilo_attachFinalizerObject2_m838796175 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GLGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void UnityEngine_GLGenerated_ilo_setInt323_m2293228011 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GLGenerated::ilo_getBooleanS4(System.Int32)
extern "C"  bool UnityEngine_GLGenerated_ilo_getBooleanS4_m70813630 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GLGenerated::ilo_getInt325(System.Int32)
extern "C"  int32_t UnityEngine_GLGenerated_ilo_getInt325_m1172571888 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_GLGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_GLGenerated_ilo_getObject6_m3508323585 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_GLGenerated::ilo_getSingle7(System.Int32)
extern "C"  float UnityEngine_GLGenerated_ilo_getSingle7_m2774614892 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
