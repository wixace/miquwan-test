﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginAiLe
struct PluginAiLe_t1605867956;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// System.String
struct String_t;
// Mihua.SDK.CreatRoleInfo
struct CreatRoleInfo_t2858846543;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Mihua.SDK.EnterGameInfo
struct EnterGameInfo_t1897532954;
// VersionMgr
struct VersionMgr_t1322950208;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PluginAiLe1605867956.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"

// System.Void PluginAiLe::.ctor()
extern "C"  void PluginAiLe__ctor_m2721294695 (PluginAiLe_t1605867956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::Init()
extern "C"  void PluginAiLe_Init_m316414989 (PluginAiLe_t1605867956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::CreateRole(CEvent.ZEvent)
extern "C"  void PluginAiLe_CreateRole_m3674708940 (PluginAiLe_t1605867956 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::EnterGame(CEvent.ZEvent)
extern "C"  void PluginAiLe_EnterGame_m1157831660 (PluginAiLe_t1605867956 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::RoleUpgrade(CEvent.ZEvent)
extern "C"  void PluginAiLe_RoleUpgrade_m615158672 (PluginAiLe_t1605867956 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginAiLe::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginAiLe_ReqSDKHttpLogin_m1210972540 (PluginAiLe_t1605867956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginAiLe::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginAiLe_IsLoginSuccess_m604098316 (PluginAiLe_t1605867956 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::OpenUserLogin()
extern "C"  void PluginAiLe_OpenUserLogin_m3451679929 (PluginAiLe_t1605867956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::UserPay(CEvent.ZEvent)
extern "C"  void PluginAiLe_UserPay_m1983253657 (PluginAiLe_t1605867956 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::OnLoginSuccess(System.String)
extern "C"  void PluginAiLe_OnLoginSuccess_m4004612396 (PluginAiLe_t1605867956 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::OnLogoutSuccess(System.String)
extern "C"  void PluginAiLe_OnLogoutSuccess_m3887442659 (PluginAiLe_t1605867956 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::OnLogout(System.String)
extern "C"  void PluginAiLe_OnLogout_m3090210236 (PluginAiLe_t1605867956 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::initSdk()
extern "C"  void PluginAiLe_initSdk_m1198295087 (PluginAiLe_t1605867956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::login()
extern "C"  void PluginAiLe_login_m2243309518 (PluginAiLe_t1605867956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::logout()
extern "C"  void PluginAiLe_logout_m828941031 (PluginAiLe_t1605867956 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::updateRoleInfo(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginAiLe_updateRoleInfo_m2947410384 (PluginAiLe_t1605867956 * __this, String_t* ___dataType0, String_t* ___serverID1, String_t* ___serverName2, String_t* ___roleID3, String_t* ___roleName4, String_t* ___roleLevel5, String_t* ___roleVip6, String_t* ___roleBalence7, String_t* ___partyName8, String_t* ___rolelevelCtime9, String_t* ___rolelevelMtime10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::pay(System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginAiLe_pay_m3523417005 (PluginAiLe_t1605867956 * __this, String_t* ___gameOrderId0, String_t* ___serverID1, String_t* ___key_serverName2, String_t* ___productId3, String_t* ___productName4, String_t* ___key_productdesc5, String_t* ___key_ext6, String_t* ___key_productPrice7, String_t* ___key_roleID8, String_t* ___key_roleName9, String_t* ___key_currencyName10, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::<OnLogoutSuccess>m__405()
extern "C"  void PluginAiLe_U3COnLogoutSuccessU3Em__405_m2510705681 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::<OnLogout>m__406()
extern "C"  void PluginAiLe_U3COnLogoutU3Em__406_m2647964131 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::ilo_initSdk1(PluginAiLe)
extern "C"  void PluginAiLe_ilo_initSdk1_m875593539 (Il2CppObject * __this /* static, unused */, PluginAiLe_t1605867956 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.CreatRoleInfo PluginAiLe::ilo_Parse2(System.Object[])
extern "C"  CreatRoleInfo_t2858846543 * PluginAiLe_ilo_Parse2_m1953926483 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.EnterGameInfo PluginAiLe::ilo_Parse3(System.Object[])
extern "C"  EnterGameInfo_t1897532954 * PluginAiLe_ilo_Parse3_m703204669 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::ilo_updateRoleInfo4(PluginAiLe,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginAiLe_ilo_updateRoleInfo4_m4158071769 (Il2CppObject * __this /* static, unused */, PluginAiLe_t1605867956 * ____this0, String_t* ___dataType1, String_t* ___serverID2, String_t* ___serverName3, String_t* ___roleID4, String_t* ___roleName5, String_t* ___roleLevel6, String_t* ___roleVip7, String_t* ___roleBalence8, String_t* ___partyName9, String_t* ___rolelevelCtime10, String_t* ___rolelevelMtime11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginAiLe::ilo_get_Instance5()
extern "C"  VersionMgr_t1322950208 * PluginAiLe_ilo_get_Instance5_m334821356 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginAiLe::ilo_get_isSdkLogin6(VersionMgr)
extern "C"  bool PluginAiLe_ilo_get_isSdkLogin6_m4206369906 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::ilo_OpenUserLogin7(PluginAiLe)
extern "C"  void PluginAiLe_ilo_OpenUserLogin7_m2236337471 (Il2CppObject * __this /* static, unused */, PluginAiLe_t1605867956 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::ilo_pay8(PluginAiLe,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginAiLe_ilo_pay8_m2676923904 (Il2CppObject * __this /* static, unused */, PluginAiLe_t1605867956 * ____this0, String_t* ___gameOrderId1, String_t* ___serverID2, String_t* ___key_serverName3, String_t* ___productId4, String_t* ___productName5, String_t* ___key_productdesc6, String_t* ___key_ext7, String_t* ___key_productPrice8, String_t* ___key_roleID9, String_t* ___key_roleName10, String_t* ___key_currencyName11, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::ilo_logout9(PluginAiLe)
extern "C"  void PluginAiLe_ilo_logout9_m3907838413 (Il2CppObject * __this /* static, unused */, PluginAiLe_t1605867956 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginAiLe::ilo_ReqSDKHttpLogin10(PluginsSdkMgr)
extern "C"  void PluginAiLe_ilo_ReqSDKHttpLogin10_m46396510 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
