﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen434202009.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Matrix4x4>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1415297725_gshared (InternalEnumerator_1_t434202009 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1415297725(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t434202009 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1415297725_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Matrix4x4>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m609189827_gshared (InternalEnumerator_1_t434202009 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m609189827(__this, method) ((  void (*) (InternalEnumerator_1_t434202009 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m609189827_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Matrix4x4>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1257179193_gshared (InternalEnumerator_1_t434202009 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1257179193(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t434202009 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1257179193_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Matrix4x4>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m1820531924_gshared (InternalEnumerator_1_t434202009 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m1820531924(__this, method) ((  void (*) (InternalEnumerator_1_t434202009 *, const MethodInfo*))InternalEnumerator_1_Dispose_m1820531924_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Matrix4x4>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3150681459_gshared (InternalEnumerator_1_t434202009 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3150681459(__this, method) ((  bool (*) (InternalEnumerator_1_t434202009 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3150681459_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Matrix4x4>::get_Current()
extern "C"  Matrix4x4_t1651859333  InternalEnumerator_1_get_Current_m3875115622_gshared (InternalEnumerator_1_t434202009 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3875115622(__this, method) ((  Matrix4x4_t1651859333  (*) (InternalEnumerator_1_t434202009 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3875115622_gshared)(__this, method)
