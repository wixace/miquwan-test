﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIScrollViewGenerated/<UIScrollView_onDragStarted_GetDelegate_member16_arg0>c__AnonStoreyCB
struct U3CUIScrollView_onDragStarted_GetDelegate_member16_arg0U3Ec__AnonStoreyCB_t565422666;

#include "codegen/il2cpp-codegen.h"

// System.Void UIScrollViewGenerated/<UIScrollView_onDragStarted_GetDelegate_member16_arg0>c__AnonStoreyCB::.ctor()
extern "C"  void U3CUIScrollView_onDragStarted_GetDelegate_member16_arg0U3Ec__AnonStoreyCB__ctor_m1853076577 (U3CUIScrollView_onDragStarted_GetDelegate_member16_arg0U3Ec__AnonStoreyCB_t565422666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIScrollViewGenerated/<UIScrollView_onDragStarted_GetDelegate_member16_arg0>c__AnonStoreyCB::<>m__15B()
extern "C"  void U3CUIScrollView_onDragStarted_GetDelegate_member16_arg0U3Ec__AnonStoreyCB_U3CU3Em__15B_m324619094 (U3CUIScrollView_onDragStarted_GetDelegate_member16_arg0U3Ec__AnonStoreyCB_t565422666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
