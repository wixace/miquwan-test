﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ParticleEmitter
struct ParticleEmitter_t605915198;
// UnityEngine.Particle[]
struct ParticleU5BU5D_t2879271823;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_ParticleEmitter605915198.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_InternalEmitParticleArgume1613430166.h"

// System.Boolean UnityEngine.ParticleEmitter::get_emit()
extern "C"  bool ParticleEmitter_get_emit_m4010660137 (ParticleEmitter_t605915198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::set_emit(System.Boolean)
extern "C"  void ParticleEmitter_set_emit_m3583864954 (ParticleEmitter_t605915198 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleEmitter::get_minSize()
extern "C"  float ParticleEmitter_get_minSize_m2662883621 (ParticleEmitter_t605915198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::set_minSize(System.Single)
extern "C"  void ParticleEmitter_set_minSize_m1291301542 (ParticleEmitter_t605915198 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleEmitter::get_maxSize()
extern "C"  float ParticleEmitter_get_maxSize_m1890405047 (ParticleEmitter_t605915198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::set_maxSize(System.Single)
extern "C"  void ParticleEmitter_set_maxSize_m4102186580 (ParticleEmitter_t605915198 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleEmitter::get_minEnergy()
extern "C"  float ParticleEmitter_get_minEnergy_m156410156 (ParticleEmitter_t605915198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::set_minEnergy(System.Single)
extern "C"  void ParticleEmitter_set_minEnergy_m3136976383 (ParticleEmitter_t605915198 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleEmitter::get_maxEnergy()
extern "C"  float ParticleEmitter_get_maxEnergy_m833842750 (ParticleEmitter_t605915198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::set_maxEnergy(System.Single)
extern "C"  void ParticleEmitter_set_maxEnergy_m2863068717 (ParticleEmitter_t605915198 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleEmitter::get_minEmission()
extern "C"  float ParticleEmitter_get_minEmission_m1393525675 (ParticleEmitter_t605915198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::set_minEmission(System.Single)
extern "C"  void ParticleEmitter_set_minEmission_m2206751200 (ParticleEmitter_t605915198 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleEmitter::get_maxEmission()
extern "C"  float ParticleEmitter_get_maxEmission_m3866186813 (ParticleEmitter_t605915198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::set_maxEmission(System.Single)
extern "C"  void ParticleEmitter_set_maxEmission_m974489230 (ParticleEmitter_t605915198 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleEmitter::get_emitterVelocityScale()
extern "C"  float ParticleEmitter_get_emitterVelocityScale_m3981566735 (ParticleEmitter_t605915198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::set_emitterVelocityScale(System.Single)
extern "C"  void ParticleEmitter_set_emitterVelocityScale_m346964220 (ParticleEmitter_t605915198 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ParticleEmitter::get_worldVelocity()
extern "C"  Vector3_t4282066566  ParticleEmitter_get_worldVelocity_m2850425327 (ParticleEmitter_t605915198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::set_worldVelocity(UnityEngine.Vector3)
extern "C"  void ParticleEmitter_set_worldVelocity_m3050260920 (ParticleEmitter_t605915198 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::INTERNAL_get_worldVelocity(UnityEngine.Vector3&)
extern "C"  void ParticleEmitter_INTERNAL_get_worldVelocity_m4205872938 (ParticleEmitter_t605915198 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::INTERNAL_set_worldVelocity(UnityEngine.Vector3&)
extern "C"  void ParticleEmitter_INTERNAL_set_worldVelocity_m1723604534 (ParticleEmitter_t605915198 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ParticleEmitter::get_localVelocity()
extern "C"  Vector3_t4282066566  ParticleEmitter_get_localVelocity_m642683528 (ParticleEmitter_t605915198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::set_localVelocity(UnityEngine.Vector3)
extern "C"  void ParticleEmitter_set_localVelocity_m973267071 (ParticleEmitter_t605915198 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::INTERNAL_get_localVelocity(UnityEngine.Vector3&)
extern "C"  void ParticleEmitter_INTERNAL_get_localVelocity_m4243573059 (ParticleEmitter_t605915198 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::INTERNAL_set_localVelocity(UnityEngine.Vector3&)
extern "C"  void ParticleEmitter_INTERNAL_set_localVelocity_m1761304655 (ParticleEmitter_t605915198 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.ParticleEmitter::get_rndVelocity()
extern "C"  Vector3_t4282066566  ParticleEmitter_get_rndVelocity_m3619717189 (ParticleEmitter_t605915198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::set_rndVelocity(UnityEngine.Vector3)
extern "C"  void ParticleEmitter_set_rndVelocity_m411650850 (ParticleEmitter_t605915198 * __this, Vector3_t4282066566  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::INTERNAL_get_rndVelocity(UnityEngine.Vector3&)
extern "C"  void ParticleEmitter_INTERNAL_get_rndVelocity_m2267168896 (ParticleEmitter_t605915198 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::INTERNAL_set_rndVelocity(UnityEngine.Vector3&)
extern "C"  void ParticleEmitter_INTERNAL_set_rndVelocity_m1929390732 (ParticleEmitter_t605915198 * __this, Vector3_t4282066566 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleEmitter::get_useWorldSpace()
extern "C"  bool ParticleEmitter_get_useWorldSpace_m350915143 (ParticleEmitter_t605915198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::set_useWorldSpace(System.Boolean)
extern "C"  void ParticleEmitter_set_useWorldSpace_m1086537508 (ParticleEmitter_t605915198 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleEmitter::get_rndRotation()
extern "C"  bool ParticleEmitter_get_rndRotation_m541260306 (ParticleEmitter_t605915198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::set_rndRotation(System.Boolean)
extern "C"  void ParticleEmitter_set_rndRotation_m1911841071 (ParticleEmitter_t605915198 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleEmitter::get_angularVelocity()
extern "C"  float ParticleEmitter_get_angularVelocity_m2664243345 (ParticleEmitter_t605915198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::set_angularVelocity(System.Single)
extern "C"  void ParticleEmitter_set_angularVelocity_m3241435834 (ParticleEmitter_t605915198 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.ParticleEmitter::get_rndAngularVelocity()
extern "C"  float ParticleEmitter_get_rndAngularVelocity_m1322349895 (ParticleEmitter_t605915198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::set_rndAngularVelocity(System.Single)
extern "C"  void ParticleEmitter_set_rndAngularVelocity_m2445576644 (ParticleEmitter_t605915198 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Particle[] UnityEngine.ParticleEmitter::get_particles()
extern "C"  ParticleU5BU5D_t2879271823* ParticleEmitter_get_particles_m3452132547 (ParticleEmitter_t605915198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::set_particles(UnityEngine.Particle[])
extern "C"  void ParticleEmitter_set_particles_m3604943904 (ParticleEmitter_t605915198 * __this, ParticleU5BU5D_t2879271823* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.ParticleEmitter::get_particleCount()
extern "C"  int32_t ParticleEmitter_get_particleCount_m4205319439 (ParticleEmitter_t605915198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::ClearParticles()
extern "C"  void ParticleEmitter_ClearParticles_m2603034675 (ParticleEmitter_t605915198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::INTERNAL_CALL_ClearParticles(UnityEngine.ParticleEmitter)
extern "C"  void ParticleEmitter_INTERNAL_CALL_ClearParticles_m987904111 (Il2CppObject * __this /* static, unused */, ParticleEmitter_t605915198 * ___self0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::Emit()
extern "C"  void ParticleEmitter_Emit_m3580269286 (ParticleEmitter_t605915198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::Emit(System.Int32)
extern "C"  void ParticleEmitter_Emit_m107028279 (ParticleEmitter_t605915198 * __this, int32_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::Emit(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.Color)
extern "C"  void ParticleEmitter_Emit_m3129119664 (ParticleEmitter_t605915198 * __this, Vector3_t4282066566  ___pos0, Vector3_t4282066566  ___velocity1, float ___size2, float ___energy3, Color_t4194546905  ___color4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::Emit(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,UnityEngine.Color,System.Single,System.Single)
extern "C"  void ParticleEmitter_Emit_m2426850490 (ParticleEmitter_t605915198 * __this, Vector3_t4282066566  ___pos0, Vector3_t4282066566  ___velocity1, float ___size2, float ___energy3, Color_t4194546905  ___color4, float ___rotation5, float ___angularVelocity6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::Emit2(System.Int32)
extern "C"  void ParticleEmitter_Emit2_m4107792319 (ParticleEmitter_t605915198 * __this, int32_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::Emit3(UnityEngine.InternalEmitParticleArguments&)
extern "C"  void ParticleEmitter_Emit3_m532233042 (ParticleEmitter_t605915198 * __this, InternalEmitParticleArguments_t1613430166 * ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::Simulate(System.Single)
extern "C"  void ParticleEmitter_Simulate_m3873926932 (ParticleEmitter_t605915198 * __this, float ___deltaTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.ParticleEmitter::get_enabled()
extern "C"  bool ParticleEmitter_get_enabled_m2663008973 (ParticleEmitter_t605915198 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.ParticleEmitter::set_enabled(System.Boolean)
extern "C"  void ParticleEmitter_set_enabled_m3784102314 (ParticleEmitter_t605915198 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
