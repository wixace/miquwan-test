﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_sabidiTosair31
struct M_sabidiTosair31_t2924213914;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_sabidiTosair31::.ctor()
extern "C"  void M_sabidiTosair31__ctor_m3547885273 (M_sabidiTosair31_t2924213914 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sabidiTosair31::M_derehisCerni0(System.String[],System.Int32)
extern "C"  void M_sabidiTosair31_M_derehisCerni0_m3209768159 (M_sabidiTosair31_t2924213914 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sabidiTosair31::M_jesaycaPermare1(System.String[],System.Int32)
extern "C"  void M_sabidiTosair31_M_jesaycaPermare1_m203804343 (M_sabidiTosair31_t2924213914 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sabidiTosair31::M_dretuli2(System.String[],System.Int32)
extern "C"  void M_sabidiTosair31_M_dretuli2_m2920051921 (M_sabidiTosair31_t2924213914 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
