﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GUI/ScrollViewState
struct ScrollViewState_t2687015188;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine.GUI/ScrollViewState::.ctor()
extern "C"  void ScrollViewState__ctor_m739544590 (ScrollViewState_t2687015188 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUI/ScrollViewState::ScrollTo(UnityEngine.Rect)
extern "C"  void ScrollViewState_ScrollTo_m1828381923 (ScrollViewState_t2687015188 * __this, Rect_t4241904616  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.GUI/ScrollViewState::ScrollTowards(UnityEngine.Rect,System.Single)
extern "C"  bool ScrollViewState_ScrollTowards_m485862347 (ScrollViewState_t2687015188 * __this, Rect_t4241904616  ___position0, float ___maxDelta1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.GUI/ScrollViewState::ScrollNeeded(UnityEngine.Rect)
extern "C"  Vector2_t4282066565  ScrollViewState_ScrollNeeded_m4066071778 (ScrollViewState_t2687015188 * __this, Rect_t4241904616  ___position0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
