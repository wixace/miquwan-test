﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// skillCfg
struct skillCfg_t2142425171;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void skillCfg::.ctor()
extern "C"  void skillCfg__ctor_m4039613352 (skillCfg_t2142425171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void skillCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void skillCfg_Init_m768674525 (skillCfg_t2142425171 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
