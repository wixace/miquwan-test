﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtilGenerated/<BehaviourUtil_JSDelayCall_GetDelegate_member9_arg1>c__AnonStorey4E
struct U3CBehaviourUtil_JSDelayCall_GetDelegate_member9_arg1U3Ec__AnonStorey4E_t1240890141;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void BehaviourUtilGenerated/<BehaviourUtil_JSDelayCall_GetDelegate_member9_arg1>c__AnonStorey4E::.ctor()
extern "C"  void U3CBehaviourUtil_JSDelayCall_GetDelegate_member9_arg1U3Ec__AnonStorey4E__ctor_m1392598558 (U3CBehaviourUtil_JSDelayCall_GetDelegate_member9_arg1U3Ec__AnonStorey4E_t1240890141 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BehaviourUtilGenerated/<BehaviourUtil_JSDelayCall_GetDelegate_member9_arg1>c__AnonStorey4E::<>m__10(System.Object,System.Object)
extern "C"  void U3CBehaviourUtil_JSDelayCall_GetDelegate_member9_arg1U3Ec__AnonStorey4E_U3CU3Em__10_m112744924 (U3CBehaviourUtil_JSDelayCall_GetDelegate_member9_arg1U3Ec__AnonStorey4E_t1240890141 * __this, Il2CppObject * ___arg10, Il2CppObject * ___arg21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
