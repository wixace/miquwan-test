﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSApi/OnObjCollected
struct OnObjCollected_t2008609135;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void JSApi/OnObjCollected::.ctor(System.Object,System.IntPtr)
extern "C"  void OnObjCollected__ctor_m83435158 (OnObjCollected_t2008609135 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi/OnObjCollected::Invoke(System.Int32)
extern "C"  void OnObjCollected_Invoke_m838952897 (OnObjCollected_t2008609135 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult JSApi/OnObjCollected::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnObjCollected_BeginInvoke_m4100456506 (OnObjCollected_t2008609135 * __this, int32_t ___id0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSApi/OnObjCollected::EndInvoke(System.IAsyncResult)
extern "C"  void OnObjCollected_EndInvoke_m278875046 (OnObjCollected_t2008609135 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
