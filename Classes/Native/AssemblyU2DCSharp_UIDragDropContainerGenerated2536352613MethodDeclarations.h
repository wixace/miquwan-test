﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIDragDropContainerGenerated
struct UIDragDropContainerGenerated_t2536352613;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UIDragDropContainerGenerated::.ctor()
extern "C"  void UIDragDropContainerGenerated__ctor_m2929531094 (UIDragDropContainerGenerated_t2536352613 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDragDropContainerGenerated::UIDragDropContainer_UIDragDropContainer1(JSVCall,System.Int32)
extern "C"  bool UIDragDropContainerGenerated_UIDragDropContainer_UIDragDropContainer1_m969369166 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropContainerGenerated::UIDragDropContainer_reparentTarget(JSVCall)
extern "C"  void UIDragDropContainerGenerated_UIDragDropContainer_reparentTarget_m3213955158 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropContainerGenerated::__Register()
extern "C"  void UIDragDropContainerGenerated___Register_m2996861361 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
