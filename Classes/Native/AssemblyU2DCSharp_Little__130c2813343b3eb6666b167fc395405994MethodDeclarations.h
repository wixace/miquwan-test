﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._130c2813343b3eb6666b167fc2b38138
struct _130c2813343b3eb6666b167fc2b38138_t395405994;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__130c2813343b3eb6666b167fc395405994.h"

// System.Void Little._130c2813343b3eb6666b167fc2b38138::.ctor()
extern "C"  void _130c2813343b3eb6666b167fc2b38138__ctor_m955128515 (_130c2813343b3eb6666b167fc2b38138_t395405994 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._130c2813343b3eb6666b167fc2b38138::_130c2813343b3eb6666b167fc2b38138m2(System.Int32)
extern "C"  int32_t _130c2813343b3eb6666b167fc2b38138__130c2813343b3eb6666b167fc2b38138m2_m2136654169 (_130c2813343b3eb6666b167fc2b38138_t395405994 * __this, int32_t ____130c2813343b3eb6666b167fc2b38138a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._130c2813343b3eb6666b167fc2b38138::_130c2813343b3eb6666b167fc2b38138m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _130c2813343b3eb6666b167fc2b38138__130c2813343b3eb6666b167fc2b38138m_m1511726461 (_130c2813343b3eb6666b167fc2b38138_t395405994 * __this, int32_t ____130c2813343b3eb6666b167fc2b38138a0, int32_t ____130c2813343b3eb6666b167fc2b3813801, int32_t ____130c2813343b3eb6666b167fc2b38138c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._130c2813343b3eb6666b167fc2b38138::ilo__130c2813343b3eb6666b167fc2b38138m21(Little._130c2813343b3eb6666b167fc2b38138,System.Int32)
extern "C"  int32_t _130c2813343b3eb6666b167fc2b38138_ilo__130c2813343b3eb6666b167fc2b38138m21_m2403099569 (Il2CppObject * __this /* static, unused */, _130c2813343b3eb6666b167fc2b38138_t395405994 * ____this0, int32_t ____130c2813343b3eb6666b167fc2b38138a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
