﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct List_1_t2448783290;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct IEnumerable_1_t86543399;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct IEnumerator_1_t2992462787;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.ICollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct ICollection_1_t1975187725;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct ReadOnlyCollection_1_t2637675274;
// UnityEngine.Rendering.ReflectionProbeBlendInfo[]
struct ReflectionProbeBlendInfoU5BU5D_t558780463;
// System.Predicate`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct Predicate_1_t691654621;
// System.Collections.Generic.IComparer`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct IComparer_1_t3655611780;
// System.Comparison`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>
struct Comparison_1_t4091926221;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Rendering_ReflectionProbeB1080597738.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2468456060.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::.ctor()
extern "C"  void List_1__ctor_m1625577031_gshared (List_1_t2448783290 * __this, const MethodInfo* method);
#define List_1__ctor_m1625577031(__this, method) ((  void (*) (List_1_t2448783290 *, const MethodInfo*))List_1__ctor_m1625577031_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1__ctor_m3185119192_gshared (List_1_t2448783290 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1__ctor_m3185119192(__this, ___collection0, method) ((  void (*) (List_1_t2448783290 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m3185119192_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m1811405080_gshared (List_1_t2448783290 * __this, int32_t ___capacity0, const MethodInfo* method);
#define List_1__ctor_m1811405080(__this, ___capacity0, method) ((  void (*) (List_1_t2448783290 *, int32_t, const MethodInfo*))List_1__ctor_m1811405080_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::.cctor()
extern "C"  void List_1__cctor_m2666151494_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m2666151494(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m2666151494_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2432449937_gshared (List_1_t2448783290 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2432449937(__this, method) ((  Il2CppObject* (*) (List_1_t2448783290 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2432449937_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void List_1_System_Collections_ICollection_CopyTo_m3194941661_gshared (List_1_t2448783290 * __this, Il2CppArray * ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m3194941661(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2448783290 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m3194941661_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * List_1_System_Collections_IEnumerable_GetEnumerator_m3077565612_gshared (List_1_t2448783290 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m3077565612(__this, method) ((  Il2CppObject * (*) (List_1_t2448783290 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m3077565612_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_Add_m2721707665_gshared (List_1_t2448783290 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m2721707665(__this, ___item0, method) ((  int32_t (*) (List_1_t2448783290 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m2721707665_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.Contains(System.Object)
extern "C"  bool List_1_System_Collections_IList_Contains_m1735693775_gshared (List_1_t2448783290 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m1735693775(__this, ___item0, method) ((  bool (*) (List_1_t2448783290 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m1735693775_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t List_1_System_Collections_IList_IndexOf_m529169513_gshared (List_1_t2448783290 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m529169513(__this, ___item0, method) ((  int32_t (*) (List_1_t2448783290 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m529169513_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_Insert_m3284286812_gshared (List_1_t2448783290 * __this, int32_t ___index0, Il2CppObject * ___item1, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m3284286812(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2448783290 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m3284286812_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.Remove(System.Object)
extern "C"  void List_1_System_Collections_IList_Remove_m1877796428_gshared (List_1_t2448783290 * __this, Il2CppObject * ___item0, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m1877796428(__this, ___item0, method) ((  void (*) (List_1_t2448783290 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1877796428_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3340160784_gshared (List_1_t2448783290 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3340160784(__this, method) ((  bool (*) (List_1_t2448783290 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3340160784_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool List_1_System_Collections_ICollection_get_IsSynchronized_m936950573_gshared (List_1_t2448783290 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m936950573(__this, method) ((  bool (*) (List_1_t2448783290 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m936950573_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * List_1_System_Collections_ICollection_get_SyncRoot_m1499905055_gshared (List_1_t2448783290 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m1499905055(__this, method) ((  Il2CppObject * (*) (List_1_t2448783290 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m1499905055_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool List_1_System_Collections_IList_get_IsFixedSize_m3872951422_gshared (List_1_t2448783290 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m3872951422(__this, method) ((  bool (*) (List_1_t2448783290 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m3872951422_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool List_1_System_Collections_IList_get_IsReadOnly_m1548774075_gshared (List_1_t2448783290 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m1548774075(__this, method) ((  bool (*) (List_1_t2448783290 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m1548774075_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * List_1_System_Collections_IList_get_Item_m3660325350_gshared (List_1_t2448783290 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m3660325350(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2448783290 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3660325350_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void List_1_System_Collections_IList_set_Item_m12616243_gshared (List_1_t2448783290 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m12616243(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2448783290 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m12616243_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Add(T)
extern "C"  void List_1_Add_m3367716184_gshared (List_1_t2448783290 * __this, ReflectionProbeBlendInfo_t1080597738  ___item0, const MethodInfo* method);
#define List_1_Add_m3367716184(__this, ___item0, method) ((  void (*) (List_1_t2448783290 *, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))List_1_Add_m3367716184_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::GrowIfNeeded(System.Int32)
extern "C"  void List_1_GrowIfNeeded_m1471043475_gshared (List_1_t2448783290 * __this, int32_t ___newCount0, const MethodInfo* method);
#define List_1_GrowIfNeeded_m1471043475(__this, ___newCount0, method) ((  void (*) (List_1_t2448783290 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m1471043475_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::CheckRange(System.Int32,System.Int32)
extern "C"  void List_1_CheckRange_m1244750452_gshared (List_1_t2448783290 * __this, int32_t ___idx0, int32_t ___count1, const MethodInfo* method);
#define List_1_CheckRange_m1244750452(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t2448783290 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m1244750452_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C"  void List_1_AddCollection_m3764626065_gshared (List_1_t2448783290 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddCollection_m3764626065(__this, ___collection0, method) ((  void (*) (List_1_t2448783290 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m3764626065_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddEnumerable_m3025598289_gshared (List_1_t2448783290 * __this, Il2CppObject* ___enumerable0, const MethodInfo* method);
#define List_1_AddEnumerable_m3025598289(__this, ___enumerable0, method) ((  void (*) (List_1_t2448783290 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m3025598289_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_AddRange_m3224299686_gshared (List_1_t2448783290 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_AddRange_m3224299686(__this, ___collection0, method) ((  void (*) (List_1_t2448783290 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m3224299686_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::AsReadOnly()
extern "C"  ReadOnlyCollection_1_t2637675274 * List_1_AsReadOnly_m1937808671_gshared (List_1_t2448783290 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m1937808671(__this, method) ((  ReadOnlyCollection_1_t2637675274 * (*) (List_1_t2448783290 *, const MethodInfo*))List_1_AsReadOnly_m1937808671_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::BinarySearch(T)
extern "C"  int32_t List_1_BinarySearch_m2072004180_gshared (List_1_t2448783290 * __this, ReflectionProbeBlendInfo_t1080597738  ___item0, const MethodInfo* method);
#define List_1_BinarySearch_m2072004180(__this, ___item0, method) ((  int32_t (*) (List_1_t2448783290 *, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))List_1_BinarySearch_m2072004180_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Clear()
extern "C"  void List_1_Clear_m3326677618_gshared (List_1_t2448783290 * __this, const MethodInfo* method);
#define List_1_Clear_m3326677618(__this, method) ((  void (*) (List_1_t2448783290 *, const MethodInfo*))List_1_Clear_m3326677618_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Contains(T)
extern "C"  bool List_1_Contains_m2614887908_gshared (List_1_t2448783290 * __this, ReflectionProbeBlendInfo_t1080597738  ___item0, const MethodInfo* method);
#define List_1_Contains_m2614887908(__this, ___item0, method) ((  bool (*) (List_1_t2448783290 *, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))List_1_Contains_m2614887908_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::CopyTo(T[],System.Int32)
extern "C"  void List_1_CopyTo_m2618001224_gshared (List_1_t2448783290 * __this, ReflectionProbeBlendInfoU5BU5D_t558780463* ___array0, int32_t ___arrayIndex1, const MethodInfo* method);
#define List_1_CopyTo_m2618001224(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2448783290 *, ReflectionProbeBlendInfoU5BU5D_t558780463*, int32_t, const MethodInfo*))List_1_CopyTo_m2618001224_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Find(System.Predicate`1<T>)
extern "C"  ReflectionProbeBlendInfo_t1080597738  List_1_Find_m2679250046_gshared (List_1_t2448783290 * __this, Predicate_1_t691654621 * ___match0, const MethodInfo* method);
#define List_1_Find_m2679250046(__this, ___match0, method) ((  ReflectionProbeBlendInfo_t1080597738  (*) (List_1_t2448783290 *, Predicate_1_t691654621 *, const MethodInfo*))List_1_Find_m2679250046_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::CheckMatch(System.Predicate`1<T>)
extern "C"  void List_1_CheckMatch_m2649114779_gshared (Il2CppObject * __this /* static, unused */, Predicate_1_t691654621 * ___match0, const MethodInfo* method);
#define List_1_CheckMatch_m2649114779(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t691654621 *, const MethodInfo*))List_1_CheckMatch_m2649114779_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C"  int32_t List_1_GetIndex_m1908424312_gshared (List_1_t2448783290 * __this, int32_t ___startIndex0, int32_t ___count1, Predicate_1_t691654621 * ___match2, const MethodInfo* method);
#define List_1_GetIndex_m1908424312(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2448783290 *, int32_t, int32_t, Predicate_1_t691654621 *, const MethodInfo*))List_1_GetIndex_m1908424312_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::GetEnumerator()
extern "C"  Enumerator_t2468456060  List_1_GetEnumerator_m2627581217_gshared (List_1_t2448783290 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m2627581217(__this, method) ((  Enumerator_t2468456060  (*) (List_1_t2448783290 *, const MethodInfo*))List_1_GetEnumerator_m2627581217_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::IndexOf(T)
extern "C"  int32_t List_1_IndexOf_m3459273236_gshared (List_1_t2448783290 * __this, ReflectionProbeBlendInfo_t1080597738  ___item0, const MethodInfo* method);
#define List_1_IndexOf_m3459273236(__this, ___item0, method) ((  int32_t (*) (List_1_t2448783290 *, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))List_1_IndexOf_m3459273236_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Shift(System.Int32,System.Int32)
extern "C"  void List_1_Shift_m2536215967_gshared (List_1_t2448783290 * __this, int32_t ___start0, int32_t ___delta1, const MethodInfo* method);
#define List_1_Shift_m2536215967(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2448783290 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m2536215967_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::CheckIndex(System.Int32)
extern "C"  void List_1_CheckIndex_m2364368408_gshared (List_1_t2448783290 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_CheckIndex_m2364368408(__this, ___index0, method) ((  void (*) (List_1_t2448783290 *, int32_t, const MethodInfo*))List_1_CheckIndex_m2364368408_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Insert(System.Int32,T)
extern "C"  void List_1_Insert_m3019034559_gshared (List_1_t2448783290 * __this, int32_t ___index0, ReflectionProbeBlendInfo_t1080597738  ___item1, const MethodInfo* method);
#define List_1_Insert_m3019034559(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2448783290 *, int32_t, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))List_1_Insert_m3019034559_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C"  void List_1_CheckCollection_m1969853684_gshared (List_1_t2448783290 * __this, Il2CppObject* ___collection0, const MethodInfo* method);
#define List_1_CheckCollection_m1969853684(__this, ___collection0, method) ((  void (*) (List_1_t2448783290 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m1969853684_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Remove(T)
extern "C"  bool List_1_Remove_m3479236319_gshared (List_1_t2448783290 * __this, ReflectionProbeBlendInfo_t1080597738  ___item0, const MethodInfo* method);
#define List_1_Remove_m3479236319(__this, ___item0, method) ((  bool (*) (List_1_t2448783290 *, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))List_1_Remove_m3479236319_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::RemoveAll(System.Predicate`1<T>)
extern "C"  int32_t List_1_RemoveAll_m4131492567_gshared (List_1_t2448783290 * __this, Predicate_1_t691654621 * ___match0, const MethodInfo* method);
#define List_1_RemoveAll_m4131492567(__this, ___match0, method) ((  int32_t (*) (List_1_t2448783290 *, Predicate_1_t691654621 *, const MethodInfo*))List_1_RemoveAll_m4131492567_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::RemoveAt(System.Int32)
extern "C"  void List_1_RemoveAt_m892887429_gshared (List_1_t2448783290 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_RemoveAt_m892887429(__this, ___index0, method) ((  void (*) (List_1_t2448783290 *, int32_t, const MethodInfo*))List_1_RemoveAt_m892887429_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::RemoveRange(System.Int32,System.Int32)
extern "C"  void List_1_RemoveRange_m3962737576_gshared (List_1_t2448783290 * __this, int32_t ___index0, int32_t ___count1, const MethodInfo* method);
#define List_1_RemoveRange_m3962737576(__this, ___index0, ___count1, method) ((  void (*) (List_1_t2448783290 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m3962737576_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Reverse()
extern "C"  void List_1_Reverse_m3144209607_gshared (List_1_t2448783290 * __this, const MethodInfo* method);
#define List_1_Reverse_m3144209607(__this, method) ((  void (*) (List_1_t2448783290 *, const MethodInfo*))List_1_Reverse_m3144209607_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Sort()
extern "C"  void List_1_Sort_m3893688379_gshared (List_1_t2448783290 * __this, const MethodInfo* method);
#define List_1_Sort_m3893688379(__this, method) ((  void (*) (List_1_t2448783290 *, const MethodInfo*))List_1_Sort_m3893688379_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Sort(System.Collections.Generic.IComparer`1<T>)
extern "C"  void List_1_Sort_m2379910153_gshared (List_1_t2448783290 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define List_1_Sort_m2379910153(__this, ___comparer0, method) ((  void (*) (List_1_t2448783290 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m2379910153_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::Sort(System.Comparison`1<T>)
extern "C"  void List_1_Sort_m2961135182_gshared (List_1_t2448783290 * __this, Comparison_1_t4091926221 * ___comparison0, const MethodInfo* method);
#define List_1_Sort_m2961135182(__this, ___comparison0, method) ((  void (*) (List_1_t2448783290 *, Comparison_1_t4091926221 *, const MethodInfo*))List_1_Sort_m2961135182_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::ToArray()
extern "C"  ReflectionProbeBlendInfoU5BU5D_t558780463* List_1_ToArray_m3479516576_gshared (List_1_t2448783290 * __this, const MethodInfo* method);
#define List_1_ToArray_m3479516576(__this, method) ((  ReflectionProbeBlendInfoU5BU5D_t558780463* (*) (List_1_t2448783290 *, const MethodInfo*))List_1_ToArray_m3479516576_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::TrimExcess()
extern "C"  void List_1_TrimExcess_m2881077140_gshared (List_1_t2448783290 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m2881077140(__this, method) ((  void (*) (List_1_t2448783290 *, const MethodInfo*))List_1_TrimExcess_m2881077140_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::get_Capacity()
extern "C"  int32_t List_1_get_Capacity_m2599493700_gshared (List_1_t2448783290 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2599493700(__this, method) ((  int32_t (*) (List_1_t2448783290 *, const MethodInfo*))List_1_get_Capacity_m2599493700_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::set_Capacity(System.Int32)
extern "C"  void List_1_set_Capacity_m2976199589_gshared (List_1_t2448783290 * __this, int32_t ___value0, const MethodInfo* method);
#define List_1_set_Capacity_m2976199589(__this, ___value0, method) ((  void (*) (List_1_t2448783290 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2976199589_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::get_Count()
extern "C"  int32_t List_1_get_Count_m3975562727_gshared (List_1_t2448783290 * __this, const MethodInfo* method);
#define List_1_get_Count_m3975562727(__this, method) ((  int32_t (*) (List_1_t2448783290 *, const MethodInfo*))List_1_get_Count_m3975562727_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::get_Item(System.Int32)
extern "C"  ReflectionProbeBlendInfo_t1080597738  List_1_get_Item_m3786147691_gshared (List_1_t2448783290 * __this, int32_t ___index0, const MethodInfo* method);
#define List_1_get_Item_m3786147691(__this, ___index0, method) ((  ReflectionProbeBlendInfo_t1080597738  (*) (List_1_t2448783290 *, int32_t, const MethodInfo*))List_1_get_Item_m3786147691_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rendering.ReflectionProbeBlendInfo>::set_Item(System.Int32,T)
extern "C"  void List_1_set_Item_m3998883030_gshared (List_1_t2448783290 * __this, int32_t ___index0, ReflectionProbeBlendInfo_t1080597738  ___value1, const MethodInfo* method);
#define List_1_set_Item_m3998883030(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2448783290 *, int32_t, ReflectionProbeBlendInfo_t1080597738 , const MethodInfo*))List_1_set_Item_m3998883030_gshared)(__this, ___index0, ___value1, method)
