﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>
struct Collection_1_t1159503752;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// Pathfinding.Int3[]
struct Int3U5BU5D_t516284607;
// System.Collections.Generic.IEnumerator`1<Pathfinding.Int3>
struct IEnumerator_1_t3885910643;
// System.Collections.Generic.IList`1<Pathfinding.Int3>
struct IList_1_t373725501;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::.ctor()
extern "C"  void Collection_1__ctor_m3350620373_gshared (Collection_1_t1159503752 * __this, const MethodInfo* method);
#define Collection_1__ctor_m3350620373(__this, method) ((  void (*) (Collection_1_t1159503752 *, const MethodInfo*))Collection_1__ctor_m3350620373_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1112484034_gshared (Collection_1_t1159503752 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1112484034(__this, method) ((  bool (*) (Collection_1_t1159503752 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1112484034_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m3875533327_gshared (Collection_1_t1159503752 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m3875533327(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1159503752 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m3875533327_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m1222930270_gshared (Collection_1_t1159503752 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m1222930270(__this, method) ((  Il2CppObject * (*) (Collection_1_t1159503752 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m1222930270_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2620260383_gshared (Collection_1_t1159503752 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m2620260383(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1159503752 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m2620260383_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m2869822977_gshared (Collection_1_t1159503752 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m2869822977(__this, ___value0, method) ((  bool (*) (Collection_1_t1159503752 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m2869822977_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m2250444535_gshared (Collection_1_t1159503752 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m2250444535(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1159503752 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2250444535_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m1016972522_gshared (Collection_1_t1159503752 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m1016972522(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1159503752 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m1016972522_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m1315319806_gshared (Collection_1_t1159503752 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m1315319806(__this, ___value0, method) ((  void (*) (Collection_1_t1159503752 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m1315319806_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m3567257019_gshared (Collection_1_t1159503752 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3567257019(__this, method) ((  bool (*) (Collection_1_t1159503752 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m3567257019_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m2863825069_gshared (Collection_1_t1159503752 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m2863825069(__this, method) ((  Il2CppObject * (*) (Collection_1_t1159503752 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m2863825069_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m1972275760_gshared (Collection_1_t1159503752 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m1972275760(__this, method) ((  bool (*) (Collection_1_t1159503752 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m1972275760_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m1626009289_gshared (Collection_1_t1159503752 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1626009289(__this, method) ((  bool (*) (Collection_1_t1159503752 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1626009289_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m127466228_gshared (Collection_1_t1159503752 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m127466228(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1159503752 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m127466228_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m2966969921_gshared (Collection_1_t1159503752 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m2966969921(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1159503752 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2966969921_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::Add(T)
extern "C"  void Collection_1_Add_m1899342090_gshared (Collection_1_t1159503752 * __this, Int3_t1974045594  ___item0, const MethodInfo* method);
#define Collection_1_Add_m1899342090(__this, ___item0, method) ((  void (*) (Collection_1_t1159503752 *, Int3_t1974045594 , const MethodInfo*))Collection_1_Add_m1899342090_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::Clear()
extern "C"  void Collection_1_Clear_m756753664_gshared (Collection_1_t1159503752 * __this, const MethodInfo* method);
#define Collection_1_Clear_m756753664(__this, method) ((  void (*) (Collection_1_t1159503752 *, const MethodInfo*))Collection_1_Clear_m756753664_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1182768130_gshared (Collection_1_t1159503752 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1182768130(__this, method) ((  void (*) (Collection_1_t1159503752 *, const MethodInfo*))Collection_1_ClearItems_m1182768130_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::Contains(T)
extern "C"  bool Collection_1_Contains_m4026234482_gshared (Collection_1_t1159503752 * __this, Int3_t1974045594  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m4026234482(__this, ___item0, method) ((  bool (*) (Collection_1_t1159503752 *, Int3_t1974045594 , const MethodInfo*))Collection_1_Contains_m4026234482_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3192595066_gshared (Collection_1_t1159503752 * __this, Int3U5BU5D_t516284607* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3192595066(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1159503752 *, Int3U5BU5D_t516284607*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3192595066_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m3158397769_gshared (Collection_1_t1159503752 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m3158397769(__this, method) ((  Il2CppObject* (*) (Collection_1_t1159503752 *, const MethodInfo*))Collection_1_GetEnumerator_m3158397769_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2487035590_gshared (Collection_1_t1159503752 * __this, Int3_t1974045594  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m2487035590(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1159503752 *, Int3_t1974045594 , const MethodInfo*))Collection_1_IndexOf_m2487035590_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m4016279409_gshared (Collection_1_t1159503752 * __this, int32_t ___index0, Int3_t1974045594  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m4016279409(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1159503752 *, int32_t, Int3_t1974045594 , const MethodInfo*))Collection_1_Insert_m4016279409_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m3438517540_gshared (Collection_1_t1159503752 * __this, int32_t ___index0, Int3_t1974045594  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m3438517540(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1159503752 *, int32_t, Int3_t1974045594 , const MethodInfo*))Collection_1_InsertItem_m3438517540_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m3831391296_gshared (Collection_1_t1159503752 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m3831391296(__this, method) ((  Il2CppObject* (*) (Collection_1_t1159503752 *, const MethodInfo*))Collection_1_get_Items_m3831391296_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::Remove(T)
extern "C"  bool Collection_1_Remove_m3659475693_gshared (Collection_1_t1159503752 * __this, Int3_t1974045594  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m3659475693(__this, ___item0, method) ((  bool (*) (Collection_1_t1159503752 *, Int3_t1974045594 , const MethodInfo*))Collection_1_Remove_m3659475693_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1890132279_gshared (Collection_1_t1159503752 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m1890132279(__this, ___index0, method) ((  void (*) (Collection_1_t1159503752 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1890132279_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2545578903_gshared (Collection_1_t1159503752 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2545578903(__this, ___index0, method) ((  void (*) (Collection_1_t1159503752 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2545578903_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3900966773_gshared (Collection_1_t1159503752 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3900966773(__this, method) ((  int32_t (*) (Collection_1_t1159503752 *, const MethodInfo*))Collection_1_get_Count_m3900966773_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::get_Item(System.Int32)
extern "C"  Int3_t1974045594  Collection_1_get_Item_m1469014429_gshared (Collection_1_t1159503752 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m1469014429(__this, ___index0, method) ((  Int3_t1974045594  (*) (Collection_1_t1159503752 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1469014429_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m278509576_gshared (Collection_1_t1159503752 * __this, int32_t ___index0, Int3_t1974045594  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m278509576(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1159503752 *, int32_t, Int3_t1974045594 , const MethodInfo*))Collection_1_set_Item_m278509576_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m1267199857_gshared (Collection_1_t1159503752 * __this, int32_t ___index0, Int3_t1974045594  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m1267199857(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1159503752 *, int32_t, Int3_t1974045594 , const MethodInfo*))Collection_1_SetItem_m1267199857_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m488573306_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m488573306(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m488573306_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::ConvertItem(System.Object)
extern "C"  Int3_t1974045594  Collection_1_ConvertItem_m2866084348_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m2866084348(__this /* static, unused */, ___item0, method) ((  Int3_t1974045594  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m2866084348_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m2547192122_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m2547192122(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m2547192122_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m1267178762_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m1267178762(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m1267178762_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<Pathfinding.Int3>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m641783253_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m641783253(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m641783253_gshared)(__this /* static, unused */, ___list0, method)
