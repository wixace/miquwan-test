﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_Collection3356274529MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::.ctor()
#define Collection_1__ctor_m3633879813(__this, method) ((  void (*) (Collection_1_t222222051 *, const MethodInfo*))Collection_1__ctor_m1690372513_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1168932864(__this, method) ((  bool (*) (Collection_1_t222222051 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1624016570_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Collection_1_System_Collections_ICollection_CopyTo_m4053364617(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t222222051 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m1285013187_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.IEnumerable.GetEnumerator()
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m3653408068(__this, method) ((  Il2CppObject * (*) (Collection_1_t222222051 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2828471038_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.IList.Add(System.Object)
#define Collection_1_System_Collections_IList_Add_m1767687597(__this, ___value0, method) ((  int32_t (*) (Collection_1_t222222051 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1708617267_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.IList.Contains(System.Object)
#define Collection_1_System_Collections_IList_Contains_m1901441727(__this, ___value0, method) ((  bool (*) (Collection_1_t222222051 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m504494585_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.IList.IndexOf(System.Object)
#define Collection_1_System_Collections_IList_IndexOf_m3963116933(__this, ___value0, method) ((  int32_t (*) (Collection_1_t222222051 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1652511499_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.IList.Insert(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_Insert_m1997947696(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t222222051 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2187188150_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.IList.Remove(System.Object)
#define Collection_1_System_Collections_IList_Remove_m3672111096(__this, ___value0, method) ((  void (*) (Collection_1_t222222051 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m2625864114_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.ICollection.get_IsSynchronized()
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m3816765501(__this, method) ((  bool (*) (Collection_1_t222222051 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1302589123_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.ICollection.get_SyncRoot()
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m682578345(__this, method) ((  Il2CppObject * (*) (Collection_1_t222222051 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m1873829551_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.IList.get_IsFixedSize()
#define Collection_1_System_Collections_IList_get_IsFixedSize_m3276676462(__this, method) ((  bool (*) (Collection_1_t222222051 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2813777960_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.IList.get_IsReadOnly()
#define Collection_1_System_Collections_IList_get_IsReadOnly_m1668086731(__this, method) ((  bool (*) (Collection_1_t222222051 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m1376059857_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.IList.get_Item(System.Int32)
#define Collection_1_System_Collections_IList_get_Item_m4188372208(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t222222051 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m2224513142_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define Collection_1_System_Collections_IList_set_Item_m791307015(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t222222051 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m2262756877_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::Add(T)
#define Collection_1_Add_m595751684(__this, ___item0, method) ((  void (*) (Collection_1_t222222051 *, AlternateView_t1036763893 *, const MethodInfo*))Collection_1_Add_m321765054_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::Clear()
#define Collection_1_Clear_m3295124038(__this, method) ((  void (*) (Collection_1_t222222051 *, const MethodInfo*))Collection_1_Clear_m3391473100_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::ClearItems()
#define Collection_1_ClearItems_m387270610(__this, method) ((  void (*) (Collection_1_t222222051 *, const MethodInfo*))Collection_1_ClearItems_m2738199222_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::Contains(T)
#define Collection_1_Contains_m2131809012(__this, ___item0, method) ((  bool (*) (Collection_1_t222222051 *, AlternateView_t1036763893 *, const MethodInfo*))Collection_1_Contains_m1050871674_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::CopyTo(T[],System.Int32)
#define Collection_1_CopyTo_m2495291892(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t222222051 *, AlternateViewU5BU5D_t4260417944*, int32_t, const MethodInfo*))Collection_1_CopyTo_m1746187054_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::GetEnumerator()
#define Collection_1_GetEnumerator_m814966592(__this, method) ((  Il2CppObject* (*) (Collection_1_t222222051 *, const MethodInfo*))Collection_1_GetEnumerator_m625631581_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::IndexOf(T)
#define Collection_1_IndexOf_m1591197560(__this, ___item0, method) ((  int32_t (*) (Collection_1_t222222051 *, AlternateView_t1036763893 *, const MethodInfo*))Collection_1_IndexOf_m3101447730_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::Insert(System.Int32,T)
#define Collection_1_Insert_m1462071918(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t222222051 *, int32_t, AlternateView_t1036763893 *, const MethodInfo*))Collection_1_Insert_m1208073509_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::InsertItem(System.Int32,T)
#define Collection_1_InsertItem_m3657597723(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t222222051 *, int32_t, AlternateView_t1036763893 *, const MethodInfo*))Collection_1_InsertItem_m714854616_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::get_Items()
#define Collection_1_get_Items_m624764762(__this, method) ((  Il2CppObject* (*) (Collection_1_t222222051 *, const MethodInfo*))Collection_1_get_Items_m1477178336_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::Remove(T)
#define Collection_1_Remove_m1274832106(__this, ___item0, method) ((  bool (*) (Collection_1_t222222051 *, AlternateView_t1036763893 *, const MethodInfo*))Collection_1_Remove_m2181520885_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::RemoveAt(System.Int32)
#define Collection_1_RemoveAt_m807843633(__this, ___index0, method) ((  void (*) (Collection_1_t222222051 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3376893675_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::RemoveItem(System.Int32)
#define Collection_1_RemoveItem_m2157766503(__this, ___index0, method) ((  void (*) (Collection_1_t222222051 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1099170891_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::get_Count()
#define Collection_1_get_Count_m187320921(__this, method) ((  int32_t (*) (Collection_1_t222222051 *, const MethodInfo*))Collection_1_get_Count_m1472906633_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::get_Item(System.Int32)
#define Collection_1_get_Item_m3113339996(__this, ___index0, method) ((  AlternateView_t1036763893 * (*) (Collection_1_t222222051 *, int32_t, const MethodInfo*))Collection_1_get_Item_m2356360623_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::set_Item(System.Int32,T)
#define Collection_1_set_Item_m3876173698(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t222222051 *, int32_t, AlternateView_t1036763893 *, const MethodInfo*))Collection_1_set_Item_m3127068860_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::SetItem(System.Int32,T)
#define Collection_1_SetItem_m240628750(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t222222051 *, int32_t, AlternateView_t1036763893 *, const MethodInfo*))Collection_1_SetItem_m112162877_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::IsValidItem(System.Object)
#define Collection_1_IsValidItem_m3644381368(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m1993492338_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::ConvertItem(System.Object)
#define Collection_1_ConvertItem_m406154324(__this /* static, unused */, ___item0, method) ((  AlternateView_t1036763893 * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1655469326_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::CheckWritable(System.Collections.Generic.IList`1<T>)
#define Collection_1_CheckWritable_m3329324468(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m651250670_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::IsSynchronized(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsSynchronized_m526661388(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m2469749778_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Net.Mail.AlternateView>::IsFixedSize(System.Collections.Generic.IList`1<T>)
#define Collection_1_IsFixedSize_m2035305235(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3893865421_gshared)(__this /* static, unused */, ___list0, method)
