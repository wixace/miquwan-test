﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUIGenerated/<GUI_ModalWindow_GetDelegate_member54_arg2>c__AnonStoreyE8
struct U3CGUI_ModalWindow_GetDelegate_member54_arg2U3Ec__AnonStoreyE8_t1284979851;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_GUIGenerated/<GUI_ModalWindow_GetDelegate_member54_arg2>c__AnonStoreyE8::.ctor()
extern "C"  void U3CGUI_ModalWindow_GetDelegate_member54_arg2U3Ec__AnonStoreyE8__ctor_m4261380928 (U3CGUI_ModalWindow_GetDelegate_member54_arg2U3Ec__AnonStoreyE8_t1284979851 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUIGenerated/<GUI_ModalWindow_GetDelegate_member54_arg2>c__AnonStoreyE8::<>m__1BA(System.Int32)
extern "C"  void U3CGUI_ModalWindow_GetDelegate_member54_arg2U3Ec__AnonStoreyE8_U3CU3Em__1BA_m1905148154 (U3CGUI_ModalWindow_GetDelegate_member54_arg2U3Ec__AnonStoreyE8_t1284979851 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
