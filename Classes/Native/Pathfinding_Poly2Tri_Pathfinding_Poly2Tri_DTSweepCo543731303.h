﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Poly2Tri.AdvancingFront
struct AdvancingFront_t240556382;
// Pathfinding.Poly2Tri.DTSweepBasin
struct DTSweepBasin_t2452859313;
// Pathfinding.Poly2Tri.DTSweepEdgeEvent
struct DTSweepEdgeEvent_t3441061589;
// Pathfinding.Poly2Tri.DTSweepPointComparator
struct DTSweepPointComparator_t3174805566;
// Pathfinding.Poly2Tri.TriangulationPoint
struct TriangulationPoint_t3810082933;

#include "Pathfinding_Poly2Tri_Pathfinding_Poly2Tri_Triangul3528662164.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Poly2Tri.DTSweepContext
struct  DTSweepContext_t543731303  : public TriangulationContext_t3528662164
{
public:
	// System.Single Pathfinding.Poly2Tri.DTSweepContext::ALPHA
	float ___ALPHA_7;
	// Pathfinding.Poly2Tri.AdvancingFront Pathfinding.Poly2Tri.DTSweepContext::Front
	AdvancingFront_t240556382 * ___Front_8;
	// Pathfinding.Poly2Tri.DTSweepBasin Pathfinding.Poly2Tri.DTSweepContext::Basin
	DTSweepBasin_t2452859313 * ___Basin_9;
	// Pathfinding.Poly2Tri.DTSweepEdgeEvent Pathfinding.Poly2Tri.DTSweepContext::EdgeEvent
	DTSweepEdgeEvent_t3441061589 * ___EdgeEvent_10;
	// Pathfinding.Poly2Tri.DTSweepPointComparator Pathfinding.Poly2Tri.DTSweepContext::_comparator
	DTSweepPointComparator_t3174805566 * ____comparator_11;
	// Pathfinding.Poly2Tri.TriangulationPoint Pathfinding.Poly2Tri.DTSweepContext::<Head>k__BackingField
	TriangulationPoint_t3810082933 * ___U3CHeadU3Ek__BackingField_12;
	// Pathfinding.Poly2Tri.TriangulationPoint Pathfinding.Poly2Tri.DTSweepContext::<Tail>k__BackingField
	TriangulationPoint_t3810082933 * ___U3CTailU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_ALPHA_7() { return static_cast<int32_t>(offsetof(DTSweepContext_t543731303, ___ALPHA_7)); }
	inline float get_ALPHA_7() const { return ___ALPHA_7; }
	inline float* get_address_of_ALPHA_7() { return &___ALPHA_7; }
	inline void set_ALPHA_7(float value)
	{
		___ALPHA_7 = value;
	}

	inline static int32_t get_offset_of_Front_8() { return static_cast<int32_t>(offsetof(DTSweepContext_t543731303, ___Front_8)); }
	inline AdvancingFront_t240556382 * get_Front_8() const { return ___Front_8; }
	inline AdvancingFront_t240556382 ** get_address_of_Front_8() { return &___Front_8; }
	inline void set_Front_8(AdvancingFront_t240556382 * value)
	{
		___Front_8 = value;
		Il2CppCodeGenWriteBarrier(&___Front_8, value);
	}

	inline static int32_t get_offset_of_Basin_9() { return static_cast<int32_t>(offsetof(DTSweepContext_t543731303, ___Basin_9)); }
	inline DTSweepBasin_t2452859313 * get_Basin_9() const { return ___Basin_9; }
	inline DTSweepBasin_t2452859313 ** get_address_of_Basin_9() { return &___Basin_9; }
	inline void set_Basin_9(DTSweepBasin_t2452859313 * value)
	{
		___Basin_9 = value;
		Il2CppCodeGenWriteBarrier(&___Basin_9, value);
	}

	inline static int32_t get_offset_of_EdgeEvent_10() { return static_cast<int32_t>(offsetof(DTSweepContext_t543731303, ___EdgeEvent_10)); }
	inline DTSweepEdgeEvent_t3441061589 * get_EdgeEvent_10() const { return ___EdgeEvent_10; }
	inline DTSweepEdgeEvent_t3441061589 ** get_address_of_EdgeEvent_10() { return &___EdgeEvent_10; }
	inline void set_EdgeEvent_10(DTSweepEdgeEvent_t3441061589 * value)
	{
		___EdgeEvent_10 = value;
		Il2CppCodeGenWriteBarrier(&___EdgeEvent_10, value);
	}

	inline static int32_t get_offset_of__comparator_11() { return static_cast<int32_t>(offsetof(DTSweepContext_t543731303, ____comparator_11)); }
	inline DTSweepPointComparator_t3174805566 * get__comparator_11() const { return ____comparator_11; }
	inline DTSweepPointComparator_t3174805566 ** get_address_of__comparator_11() { return &____comparator_11; }
	inline void set__comparator_11(DTSweepPointComparator_t3174805566 * value)
	{
		____comparator_11 = value;
		Il2CppCodeGenWriteBarrier(&____comparator_11, value);
	}

	inline static int32_t get_offset_of_U3CHeadU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(DTSweepContext_t543731303, ___U3CHeadU3Ek__BackingField_12)); }
	inline TriangulationPoint_t3810082933 * get_U3CHeadU3Ek__BackingField_12() const { return ___U3CHeadU3Ek__BackingField_12; }
	inline TriangulationPoint_t3810082933 ** get_address_of_U3CHeadU3Ek__BackingField_12() { return &___U3CHeadU3Ek__BackingField_12; }
	inline void set_U3CHeadU3Ek__BackingField_12(TriangulationPoint_t3810082933 * value)
	{
		___U3CHeadU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier(&___U3CHeadU3Ek__BackingField_12, value);
	}

	inline static int32_t get_offset_of_U3CTailU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(DTSweepContext_t543731303, ___U3CTailU3Ek__BackingField_13)); }
	inline TriangulationPoint_t3810082933 * get_U3CTailU3Ek__BackingField_13() const { return ___U3CTailU3Ek__BackingField_13; }
	inline TriangulationPoint_t3810082933 ** get_address_of_U3CTailU3Ek__BackingField_13() { return &___U3CTailU3Ek__BackingField_13; }
	inline void set_U3CTailU3Ek__BackingField_13(TriangulationPoint_t3810082933 * value)
	{
		___U3CTailU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier(&___U3CTailU3Ek__BackingField_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
