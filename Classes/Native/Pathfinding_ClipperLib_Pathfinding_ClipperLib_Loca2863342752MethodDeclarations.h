﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.ClipperLib.LocalMinima
struct LocalMinima_t2863342752;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.ClipperLib.LocalMinima::.ctor()
extern "C"  void LocalMinima__ctor_m1959824229 (LocalMinima_t2863342752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
