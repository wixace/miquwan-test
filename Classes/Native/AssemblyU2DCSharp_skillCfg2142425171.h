﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "AssemblyU2DCSharp_CsCfgBase69924517.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// skillCfg
struct  skillCfg_t2142425171  : public CsCfgBase_t69924517
{
public:
	// System.Int32 skillCfg::id
	int32_t ___id_0;
	// System.String skillCfg::show_super_name
	String_t* ___show_super_name_1;
	// System.String skillCfg::name
	String_t* ___name_2;
	// System.Int32 skillCfg::type
	int32_t ___type_3;
	// System.Int32 skillCfg::fun_type
	int32_t ___fun_type_4;
	// System.Int32 skillCfg::add_anger
	int32_t ___add_anger_5;
	// System.Boolean skillCfg::last_type
	bool ___last_type_6;
	// System.Single skillCfg::lasttime
	float ___lasttime_7;
	// System.Single skillCfg::damage_time
	float ___damage_time_8;
	// System.Int32 skillCfg::hitrate
	int32_t ___hitrate_9;
	// System.Int32 skillCfg::Hurt_times
	int32_t ___Hurt_times_10;
	// System.Int32 skillCfg::effectCount
	int32_t ___effectCount_11;
	// System.Single skillCfg::coefficient
	float ___coefficient_12;
	// System.Int32 skillCfg::hatryratio
	int32_t ___hatryratio_13;
	// System.Single skillCfg::move_coefficient
	float ___move_coefficient_14;
	// System.Int32 skillCfg::trigger
	int32_t ___trigger_15;
	// System.Int32 skillCfg::triggertype
	int32_t ___triggertype_16;
	// System.Int32 skillCfg::probability
	int32_t ___probability_17;
	// System.Int32 skillCfg::preskill
	int32_t ___preskill_18;
	// System.Int32 skillCfg::followskill
	int32_t ___followskill_19;
	// System.Boolean skillCfg::if_target
	bool ___if_target_20;
	// System.Single skillCfg::distance
	float ___distance_21;
	// System.Boolean skillCfg::isshoot
	bool ___isshoot_22;
	// System.Single skillCfg::shootdis
	float ___shootdis_23;
	// System.Single skillCfg::shootspeed
	float ___shootspeed_24;
	// System.Int32 skillCfg::target
	int32_t ___target_25;
	// System.Int32 skillCfg::target_num
	int32_t ___target_num_26;
	// System.Int32 skillCfg::region_type
	int32_t ___region_type_27;
	// System.Single skillCfg::region_radius
	float ___region_radius_28;
	// System.Single skillCfg::region_angle
	float ___region_angle_29;
	// System.Single skillCfg::region_length
	float ___region_length_30;
	// System.Single skillCfg::region_width
	float ___region_width_31;
	// System.String skillCfg::action
	String_t* ___action_32;
	// System.String skillCfg::endaction
	String_t* ___endaction_33;
	// System.String skillCfg::actiontime
	String_t* ___actiontime_34;
	// System.Single skillCfg::cdtime
	float ___cdtime_35;
	// System.Single skillCfg::cdstarttime
	float ___cdstarttime_36;
	// System.Boolean skillCfg::is_rand_atk
	bool ___is_rand_atk_37;
	// System.Int32 skillCfg::relate_cd_skillID
	int32_t ___relate_cd_skillID_38;
	// System.Boolean skillCfg::isProgressBar
	bool ___isProgressBar_39;
	// System.Single skillCfg::progressTime
	float ___progressTime_40;
	// System.Boolean skillCfg::breakskill
	bool ___breakskill_41;
	// System.String skillCfg::icon
	String_t* ___icon_42;
	// System.String skillCfg::effect_sing
	String_t* ___effect_sing_43;
	// System.String skillCfg::sing_showtime
	String_t* ___sing_showtime_44;
	// System.String skillCfg::sing_playspeed
	String_t* ___sing_playspeed_45;
	// System.String skillCfg::sound
	String_t* ___sound_46;
	// System.String skillCfg::soundTime
	String_t* ___soundTime_47;
	// System.String skillCfg::effect_skill
	String_t* ___effect_skill_48;
	// System.String skillCfg::skill_showtime
	String_t* ___skill_showtime_49;
	// System.String skillCfg::skill_playspeed
	String_t* ___skill_playspeed_50;
	// System.String skillCfg::effect_skill_event
	String_t* ___effect_skill_event_51;
	// System.Int32 skillCfg::damage_separation
	int32_t ___damage_separation_52;
	// System.Int32 skillCfg::effect_kick
	int32_t ___effect_kick_53;
	// System.Single skillCfg::kick_showtime
	float ___kick_showtime_54;
	// System.Single skillCfg::activetime
	float ___activetime_55;
	// System.Int32 skillCfg::activecount
	int32_t ___activecount_56;
	// System.String skillCfg::active_spacing
	String_t* ___active_spacing_57;
	// System.String skillCfg::black_effect
	String_t* ___black_effect_58;
	// System.Single skillCfg::zoomintime
	float ___zoomintime_59;
	// System.String skillCfg::zoominoffset
	String_t* ___zoominoffset_60;
	// System.Single skillCfg::waittime
	float ___waittime_61;
	// System.Single skillCfg::zoomouttime
	float ___zoomouttime_62;
	// System.Int32 skillCfg::shock_event
	int32_t ___shock_event_63;
	// System.Single skillCfg::shock_time
	float ___shock_time_64;
	// System.Int32 skillCfg::shock_type
	int32_t ___shock_type_65;
	// System.Int32 skillCfg::shock_fps
	int32_t ___shock_fps_66;
	// System.Single skillCfg::shock_lastingtime
	float ___shock_lastingtime_67;
	// System.Single skillCfg::shock_strength
	float ___shock_strength_68;
	// System.Boolean skillCfg::falseSkill
	bool ___falseSkill_69;
	// System.String skillCfg::skillIdArr
	String_t* ___skillIdArr_70;
	// System.String skillCfg::buffid
	String_t* ___buffid_71;
	// System.String skillCfg::buffprob
	String_t* ___buffprob_72;
	// System.String skillCfg::buffevent
	String_t* ___buffevent_73;
	// System.String skillCfg::bufftarget
	String_t* ___bufftarget_74;
	// System.String skillCfg::buffbreak
	String_t* ___buffbreak_75;
	// System.Int32 skillCfg::move_event
	int32_t ___move_event_76;
	// System.Single skillCfg::move_start_time
	float ___move_start_time_77;
	// System.Int32 skillCfg::movetype
	int32_t ___movetype_78;
	// System.Single skillCfg::movetime
	float ___movetime_79;
	// System.Single skillCfg::movespeed
	float ___movespeed_80;
	// System.String skillCfg::movepara
	String_t* ___movepara_81;
	// System.String skillCfg::moveaction
	String_t* ___moveaction_82;
	// System.Single skillCfg::movehight
	float ___movehight_83;
	// System.Single skillCfg::radiusrage
	float ___radiusrage_84;
	// System.Int32 skillCfg::sputter_id
	int32_t ___sputter_id_85;
	// System.Int32 skillCfg::sputter
	int32_t ___sputter_86;
	// System.Single skillCfg::sputter_radius
	float ___sputter_radius_87;
	// System.Int32 skillCfg::sputter_num
	int32_t ___sputter_num_88;
	// System.Single skillCfg::sputter_hurt
	float ___sputter_hurt_89;
	// System.String skillCfg::leader_skill_att
	String_t* ___leader_skill_att_90;
	// System.Int32 skillCfg::leader_skill_per
	int32_t ___leader_skill_per_91;
	// System.Int32 skillCfg::leader_skill_value
	int32_t ___leader_skill_value_92;
	// System.String skillCfg::summonid
	String_t* ___summonid_93;
	// System.String skillCfg::summon_time
	String_t* ___summon_time_94;
	// System.String skillCfg::summon_attackper
	String_t* ___summon_attackper_95;
	// System.Int32 skillCfg::summonid_target
	int32_t ___summonid_target_96;
	// System.String skillCfg::SkillDes
	String_t* ___SkillDes_97;
	// System.String skillCfg::skillbrief
	String_t* ___skillbrief_98;
	// System.String skillCfg::openTermDes
	String_t* ___openTermDes_99;
	// System.Int32 skillCfg::evolveLevel
	int32_t ___evolveLevel_100;
	// System.Int32 skillCfg::break_or_not
	int32_t ___break_or_not_101;
	// System.Int32 skillCfg::upgrade_begin
	int32_t ___upgrade_begin_102;
	// System.Int32 skillCfg::upgrade_end
	int32_t ___upgrade_end_103;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_show_super_name_1() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___show_super_name_1)); }
	inline String_t* get_show_super_name_1() const { return ___show_super_name_1; }
	inline String_t** get_address_of_show_super_name_1() { return &___show_super_name_1; }
	inline void set_show_super_name_1(String_t* value)
	{
		___show_super_name_1 = value;
		Il2CppCodeGenWriteBarrier(&___show_super_name_1, value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier(&___name_2, value);
	}

	inline static int32_t get_offset_of_type_3() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___type_3)); }
	inline int32_t get_type_3() const { return ___type_3; }
	inline int32_t* get_address_of_type_3() { return &___type_3; }
	inline void set_type_3(int32_t value)
	{
		___type_3 = value;
	}

	inline static int32_t get_offset_of_fun_type_4() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___fun_type_4)); }
	inline int32_t get_fun_type_4() const { return ___fun_type_4; }
	inline int32_t* get_address_of_fun_type_4() { return &___fun_type_4; }
	inline void set_fun_type_4(int32_t value)
	{
		___fun_type_4 = value;
	}

	inline static int32_t get_offset_of_add_anger_5() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___add_anger_5)); }
	inline int32_t get_add_anger_5() const { return ___add_anger_5; }
	inline int32_t* get_address_of_add_anger_5() { return &___add_anger_5; }
	inline void set_add_anger_5(int32_t value)
	{
		___add_anger_5 = value;
	}

	inline static int32_t get_offset_of_last_type_6() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___last_type_6)); }
	inline bool get_last_type_6() const { return ___last_type_6; }
	inline bool* get_address_of_last_type_6() { return &___last_type_6; }
	inline void set_last_type_6(bool value)
	{
		___last_type_6 = value;
	}

	inline static int32_t get_offset_of_lasttime_7() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___lasttime_7)); }
	inline float get_lasttime_7() const { return ___lasttime_7; }
	inline float* get_address_of_lasttime_7() { return &___lasttime_7; }
	inline void set_lasttime_7(float value)
	{
		___lasttime_7 = value;
	}

	inline static int32_t get_offset_of_damage_time_8() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___damage_time_8)); }
	inline float get_damage_time_8() const { return ___damage_time_8; }
	inline float* get_address_of_damage_time_8() { return &___damage_time_8; }
	inline void set_damage_time_8(float value)
	{
		___damage_time_8 = value;
	}

	inline static int32_t get_offset_of_hitrate_9() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___hitrate_9)); }
	inline int32_t get_hitrate_9() const { return ___hitrate_9; }
	inline int32_t* get_address_of_hitrate_9() { return &___hitrate_9; }
	inline void set_hitrate_9(int32_t value)
	{
		___hitrate_9 = value;
	}

	inline static int32_t get_offset_of_Hurt_times_10() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___Hurt_times_10)); }
	inline int32_t get_Hurt_times_10() const { return ___Hurt_times_10; }
	inline int32_t* get_address_of_Hurt_times_10() { return &___Hurt_times_10; }
	inline void set_Hurt_times_10(int32_t value)
	{
		___Hurt_times_10 = value;
	}

	inline static int32_t get_offset_of_effectCount_11() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___effectCount_11)); }
	inline int32_t get_effectCount_11() const { return ___effectCount_11; }
	inline int32_t* get_address_of_effectCount_11() { return &___effectCount_11; }
	inline void set_effectCount_11(int32_t value)
	{
		___effectCount_11 = value;
	}

	inline static int32_t get_offset_of_coefficient_12() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___coefficient_12)); }
	inline float get_coefficient_12() const { return ___coefficient_12; }
	inline float* get_address_of_coefficient_12() { return &___coefficient_12; }
	inline void set_coefficient_12(float value)
	{
		___coefficient_12 = value;
	}

	inline static int32_t get_offset_of_hatryratio_13() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___hatryratio_13)); }
	inline int32_t get_hatryratio_13() const { return ___hatryratio_13; }
	inline int32_t* get_address_of_hatryratio_13() { return &___hatryratio_13; }
	inline void set_hatryratio_13(int32_t value)
	{
		___hatryratio_13 = value;
	}

	inline static int32_t get_offset_of_move_coefficient_14() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___move_coefficient_14)); }
	inline float get_move_coefficient_14() const { return ___move_coefficient_14; }
	inline float* get_address_of_move_coefficient_14() { return &___move_coefficient_14; }
	inline void set_move_coefficient_14(float value)
	{
		___move_coefficient_14 = value;
	}

	inline static int32_t get_offset_of_trigger_15() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___trigger_15)); }
	inline int32_t get_trigger_15() const { return ___trigger_15; }
	inline int32_t* get_address_of_trigger_15() { return &___trigger_15; }
	inline void set_trigger_15(int32_t value)
	{
		___trigger_15 = value;
	}

	inline static int32_t get_offset_of_triggertype_16() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___triggertype_16)); }
	inline int32_t get_triggertype_16() const { return ___triggertype_16; }
	inline int32_t* get_address_of_triggertype_16() { return &___triggertype_16; }
	inline void set_triggertype_16(int32_t value)
	{
		___triggertype_16 = value;
	}

	inline static int32_t get_offset_of_probability_17() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___probability_17)); }
	inline int32_t get_probability_17() const { return ___probability_17; }
	inline int32_t* get_address_of_probability_17() { return &___probability_17; }
	inline void set_probability_17(int32_t value)
	{
		___probability_17 = value;
	}

	inline static int32_t get_offset_of_preskill_18() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___preskill_18)); }
	inline int32_t get_preskill_18() const { return ___preskill_18; }
	inline int32_t* get_address_of_preskill_18() { return &___preskill_18; }
	inline void set_preskill_18(int32_t value)
	{
		___preskill_18 = value;
	}

	inline static int32_t get_offset_of_followskill_19() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___followskill_19)); }
	inline int32_t get_followskill_19() const { return ___followskill_19; }
	inline int32_t* get_address_of_followskill_19() { return &___followskill_19; }
	inline void set_followskill_19(int32_t value)
	{
		___followskill_19 = value;
	}

	inline static int32_t get_offset_of_if_target_20() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___if_target_20)); }
	inline bool get_if_target_20() const { return ___if_target_20; }
	inline bool* get_address_of_if_target_20() { return &___if_target_20; }
	inline void set_if_target_20(bool value)
	{
		___if_target_20 = value;
	}

	inline static int32_t get_offset_of_distance_21() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___distance_21)); }
	inline float get_distance_21() const { return ___distance_21; }
	inline float* get_address_of_distance_21() { return &___distance_21; }
	inline void set_distance_21(float value)
	{
		___distance_21 = value;
	}

	inline static int32_t get_offset_of_isshoot_22() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___isshoot_22)); }
	inline bool get_isshoot_22() const { return ___isshoot_22; }
	inline bool* get_address_of_isshoot_22() { return &___isshoot_22; }
	inline void set_isshoot_22(bool value)
	{
		___isshoot_22 = value;
	}

	inline static int32_t get_offset_of_shootdis_23() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___shootdis_23)); }
	inline float get_shootdis_23() const { return ___shootdis_23; }
	inline float* get_address_of_shootdis_23() { return &___shootdis_23; }
	inline void set_shootdis_23(float value)
	{
		___shootdis_23 = value;
	}

	inline static int32_t get_offset_of_shootspeed_24() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___shootspeed_24)); }
	inline float get_shootspeed_24() const { return ___shootspeed_24; }
	inline float* get_address_of_shootspeed_24() { return &___shootspeed_24; }
	inline void set_shootspeed_24(float value)
	{
		___shootspeed_24 = value;
	}

	inline static int32_t get_offset_of_target_25() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___target_25)); }
	inline int32_t get_target_25() const { return ___target_25; }
	inline int32_t* get_address_of_target_25() { return &___target_25; }
	inline void set_target_25(int32_t value)
	{
		___target_25 = value;
	}

	inline static int32_t get_offset_of_target_num_26() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___target_num_26)); }
	inline int32_t get_target_num_26() const { return ___target_num_26; }
	inline int32_t* get_address_of_target_num_26() { return &___target_num_26; }
	inline void set_target_num_26(int32_t value)
	{
		___target_num_26 = value;
	}

	inline static int32_t get_offset_of_region_type_27() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___region_type_27)); }
	inline int32_t get_region_type_27() const { return ___region_type_27; }
	inline int32_t* get_address_of_region_type_27() { return &___region_type_27; }
	inline void set_region_type_27(int32_t value)
	{
		___region_type_27 = value;
	}

	inline static int32_t get_offset_of_region_radius_28() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___region_radius_28)); }
	inline float get_region_radius_28() const { return ___region_radius_28; }
	inline float* get_address_of_region_radius_28() { return &___region_radius_28; }
	inline void set_region_radius_28(float value)
	{
		___region_radius_28 = value;
	}

	inline static int32_t get_offset_of_region_angle_29() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___region_angle_29)); }
	inline float get_region_angle_29() const { return ___region_angle_29; }
	inline float* get_address_of_region_angle_29() { return &___region_angle_29; }
	inline void set_region_angle_29(float value)
	{
		___region_angle_29 = value;
	}

	inline static int32_t get_offset_of_region_length_30() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___region_length_30)); }
	inline float get_region_length_30() const { return ___region_length_30; }
	inline float* get_address_of_region_length_30() { return &___region_length_30; }
	inline void set_region_length_30(float value)
	{
		___region_length_30 = value;
	}

	inline static int32_t get_offset_of_region_width_31() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___region_width_31)); }
	inline float get_region_width_31() const { return ___region_width_31; }
	inline float* get_address_of_region_width_31() { return &___region_width_31; }
	inline void set_region_width_31(float value)
	{
		___region_width_31 = value;
	}

	inline static int32_t get_offset_of_action_32() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___action_32)); }
	inline String_t* get_action_32() const { return ___action_32; }
	inline String_t** get_address_of_action_32() { return &___action_32; }
	inline void set_action_32(String_t* value)
	{
		___action_32 = value;
		Il2CppCodeGenWriteBarrier(&___action_32, value);
	}

	inline static int32_t get_offset_of_endaction_33() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___endaction_33)); }
	inline String_t* get_endaction_33() const { return ___endaction_33; }
	inline String_t** get_address_of_endaction_33() { return &___endaction_33; }
	inline void set_endaction_33(String_t* value)
	{
		___endaction_33 = value;
		Il2CppCodeGenWriteBarrier(&___endaction_33, value);
	}

	inline static int32_t get_offset_of_actiontime_34() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___actiontime_34)); }
	inline String_t* get_actiontime_34() const { return ___actiontime_34; }
	inline String_t** get_address_of_actiontime_34() { return &___actiontime_34; }
	inline void set_actiontime_34(String_t* value)
	{
		___actiontime_34 = value;
		Il2CppCodeGenWriteBarrier(&___actiontime_34, value);
	}

	inline static int32_t get_offset_of_cdtime_35() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___cdtime_35)); }
	inline float get_cdtime_35() const { return ___cdtime_35; }
	inline float* get_address_of_cdtime_35() { return &___cdtime_35; }
	inline void set_cdtime_35(float value)
	{
		___cdtime_35 = value;
	}

	inline static int32_t get_offset_of_cdstarttime_36() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___cdstarttime_36)); }
	inline float get_cdstarttime_36() const { return ___cdstarttime_36; }
	inline float* get_address_of_cdstarttime_36() { return &___cdstarttime_36; }
	inline void set_cdstarttime_36(float value)
	{
		___cdstarttime_36 = value;
	}

	inline static int32_t get_offset_of_is_rand_atk_37() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___is_rand_atk_37)); }
	inline bool get_is_rand_atk_37() const { return ___is_rand_atk_37; }
	inline bool* get_address_of_is_rand_atk_37() { return &___is_rand_atk_37; }
	inline void set_is_rand_atk_37(bool value)
	{
		___is_rand_atk_37 = value;
	}

	inline static int32_t get_offset_of_relate_cd_skillID_38() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___relate_cd_skillID_38)); }
	inline int32_t get_relate_cd_skillID_38() const { return ___relate_cd_skillID_38; }
	inline int32_t* get_address_of_relate_cd_skillID_38() { return &___relate_cd_skillID_38; }
	inline void set_relate_cd_skillID_38(int32_t value)
	{
		___relate_cd_skillID_38 = value;
	}

	inline static int32_t get_offset_of_isProgressBar_39() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___isProgressBar_39)); }
	inline bool get_isProgressBar_39() const { return ___isProgressBar_39; }
	inline bool* get_address_of_isProgressBar_39() { return &___isProgressBar_39; }
	inline void set_isProgressBar_39(bool value)
	{
		___isProgressBar_39 = value;
	}

	inline static int32_t get_offset_of_progressTime_40() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___progressTime_40)); }
	inline float get_progressTime_40() const { return ___progressTime_40; }
	inline float* get_address_of_progressTime_40() { return &___progressTime_40; }
	inline void set_progressTime_40(float value)
	{
		___progressTime_40 = value;
	}

	inline static int32_t get_offset_of_breakskill_41() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___breakskill_41)); }
	inline bool get_breakskill_41() const { return ___breakskill_41; }
	inline bool* get_address_of_breakskill_41() { return &___breakskill_41; }
	inline void set_breakskill_41(bool value)
	{
		___breakskill_41 = value;
	}

	inline static int32_t get_offset_of_icon_42() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___icon_42)); }
	inline String_t* get_icon_42() const { return ___icon_42; }
	inline String_t** get_address_of_icon_42() { return &___icon_42; }
	inline void set_icon_42(String_t* value)
	{
		___icon_42 = value;
		Il2CppCodeGenWriteBarrier(&___icon_42, value);
	}

	inline static int32_t get_offset_of_effect_sing_43() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___effect_sing_43)); }
	inline String_t* get_effect_sing_43() const { return ___effect_sing_43; }
	inline String_t** get_address_of_effect_sing_43() { return &___effect_sing_43; }
	inline void set_effect_sing_43(String_t* value)
	{
		___effect_sing_43 = value;
		Il2CppCodeGenWriteBarrier(&___effect_sing_43, value);
	}

	inline static int32_t get_offset_of_sing_showtime_44() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___sing_showtime_44)); }
	inline String_t* get_sing_showtime_44() const { return ___sing_showtime_44; }
	inline String_t** get_address_of_sing_showtime_44() { return &___sing_showtime_44; }
	inline void set_sing_showtime_44(String_t* value)
	{
		___sing_showtime_44 = value;
		Il2CppCodeGenWriteBarrier(&___sing_showtime_44, value);
	}

	inline static int32_t get_offset_of_sing_playspeed_45() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___sing_playspeed_45)); }
	inline String_t* get_sing_playspeed_45() const { return ___sing_playspeed_45; }
	inline String_t** get_address_of_sing_playspeed_45() { return &___sing_playspeed_45; }
	inline void set_sing_playspeed_45(String_t* value)
	{
		___sing_playspeed_45 = value;
		Il2CppCodeGenWriteBarrier(&___sing_playspeed_45, value);
	}

	inline static int32_t get_offset_of_sound_46() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___sound_46)); }
	inline String_t* get_sound_46() const { return ___sound_46; }
	inline String_t** get_address_of_sound_46() { return &___sound_46; }
	inline void set_sound_46(String_t* value)
	{
		___sound_46 = value;
		Il2CppCodeGenWriteBarrier(&___sound_46, value);
	}

	inline static int32_t get_offset_of_soundTime_47() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___soundTime_47)); }
	inline String_t* get_soundTime_47() const { return ___soundTime_47; }
	inline String_t** get_address_of_soundTime_47() { return &___soundTime_47; }
	inline void set_soundTime_47(String_t* value)
	{
		___soundTime_47 = value;
		Il2CppCodeGenWriteBarrier(&___soundTime_47, value);
	}

	inline static int32_t get_offset_of_effect_skill_48() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___effect_skill_48)); }
	inline String_t* get_effect_skill_48() const { return ___effect_skill_48; }
	inline String_t** get_address_of_effect_skill_48() { return &___effect_skill_48; }
	inline void set_effect_skill_48(String_t* value)
	{
		___effect_skill_48 = value;
		Il2CppCodeGenWriteBarrier(&___effect_skill_48, value);
	}

	inline static int32_t get_offset_of_skill_showtime_49() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___skill_showtime_49)); }
	inline String_t* get_skill_showtime_49() const { return ___skill_showtime_49; }
	inline String_t** get_address_of_skill_showtime_49() { return &___skill_showtime_49; }
	inline void set_skill_showtime_49(String_t* value)
	{
		___skill_showtime_49 = value;
		Il2CppCodeGenWriteBarrier(&___skill_showtime_49, value);
	}

	inline static int32_t get_offset_of_skill_playspeed_50() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___skill_playspeed_50)); }
	inline String_t* get_skill_playspeed_50() const { return ___skill_playspeed_50; }
	inline String_t** get_address_of_skill_playspeed_50() { return &___skill_playspeed_50; }
	inline void set_skill_playspeed_50(String_t* value)
	{
		___skill_playspeed_50 = value;
		Il2CppCodeGenWriteBarrier(&___skill_playspeed_50, value);
	}

	inline static int32_t get_offset_of_effect_skill_event_51() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___effect_skill_event_51)); }
	inline String_t* get_effect_skill_event_51() const { return ___effect_skill_event_51; }
	inline String_t** get_address_of_effect_skill_event_51() { return &___effect_skill_event_51; }
	inline void set_effect_skill_event_51(String_t* value)
	{
		___effect_skill_event_51 = value;
		Il2CppCodeGenWriteBarrier(&___effect_skill_event_51, value);
	}

	inline static int32_t get_offset_of_damage_separation_52() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___damage_separation_52)); }
	inline int32_t get_damage_separation_52() const { return ___damage_separation_52; }
	inline int32_t* get_address_of_damage_separation_52() { return &___damage_separation_52; }
	inline void set_damage_separation_52(int32_t value)
	{
		___damage_separation_52 = value;
	}

	inline static int32_t get_offset_of_effect_kick_53() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___effect_kick_53)); }
	inline int32_t get_effect_kick_53() const { return ___effect_kick_53; }
	inline int32_t* get_address_of_effect_kick_53() { return &___effect_kick_53; }
	inline void set_effect_kick_53(int32_t value)
	{
		___effect_kick_53 = value;
	}

	inline static int32_t get_offset_of_kick_showtime_54() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___kick_showtime_54)); }
	inline float get_kick_showtime_54() const { return ___kick_showtime_54; }
	inline float* get_address_of_kick_showtime_54() { return &___kick_showtime_54; }
	inline void set_kick_showtime_54(float value)
	{
		___kick_showtime_54 = value;
	}

	inline static int32_t get_offset_of_activetime_55() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___activetime_55)); }
	inline float get_activetime_55() const { return ___activetime_55; }
	inline float* get_address_of_activetime_55() { return &___activetime_55; }
	inline void set_activetime_55(float value)
	{
		___activetime_55 = value;
	}

	inline static int32_t get_offset_of_activecount_56() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___activecount_56)); }
	inline int32_t get_activecount_56() const { return ___activecount_56; }
	inline int32_t* get_address_of_activecount_56() { return &___activecount_56; }
	inline void set_activecount_56(int32_t value)
	{
		___activecount_56 = value;
	}

	inline static int32_t get_offset_of_active_spacing_57() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___active_spacing_57)); }
	inline String_t* get_active_spacing_57() const { return ___active_spacing_57; }
	inline String_t** get_address_of_active_spacing_57() { return &___active_spacing_57; }
	inline void set_active_spacing_57(String_t* value)
	{
		___active_spacing_57 = value;
		Il2CppCodeGenWriteBarrier(&___active_spacing_57, value);
	}

	inline static int32_t get_offset_of_black_effect_58() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___black_effect_58)); }
	inline String_t* get_black_effect_58() const { return ___black_effect_58; }
	inline String_t** get_address_of_black_effect_58() { return &___black_effect_58; }
	inline void set_black_effect_58(String_t* value)
	{
		___black_effect_58 = value;
		Il2CppCodeGenWriteBarrier(&___black_effect_58, value);
	}

	inline static int32_t get_offset_of_zoomintime_59() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___zoomintime_59)); }
	inline float get_zoomintime_59() const { return ___zoomintime_59; }
	inline float* get_address_of_zoomintime_59() { return &___zoomintime_59; }
	inline void set_zoomintime_59(float value)
	{
		___zoomintime_59 = value;
	}

	inline static int32_t get_offset_of_zoominoffset_60() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___zoominoffset_60)); }
	inline String_t* get_zoominoffset_60() const { return ___zoominoffset_60; }
	inline String_t** get_address_of_zoominoffset_60() { return &___zoominoffset_60; }
	inline void set_zoominoffset_60(String_t* value)
	{
		___zoominoffset_60 = value;
		Il2CppCodeGenWriteBarrier(&___zoominoffset_60, value);
	}

	inline static int32_t get_offset_of_waittime_61() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___waittime_61)); }
	inline float get_waittime_61() const { return ___waittime_61; }
	inline float* get_address_of_waittime_61() { return &___waittime_61; }
	inline void set_waittime_61(float value)
	{
		___waittime_61 = value;
	}

	inline static int32_t get_offset_of_zoomouttime_62() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___zoomouttime_62)); }
	inline float get_zoomouttime_62() const { return ___zoomouttime_62; }
	inline float* get_address_of_zoomouttime_62() { return &___zoomouttime_62; }
	inline void set_zoomouttime_62(float value)
	{
		___zoomouttime_62 = value;
	}

	inline static int32_t get_offset_of_shock_event_63() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___shock_event_63)); }
	inline int32_t get_shock_event_63() const { return ___shock_event_63; }
	inline int32_t* get_address_of_shock_event_63() { return &___shock_event_63; }
	inline void set_shock_event_63(int32_t value)
	{
		___shock_event_63 = value;
	}

	inline static int32_t get_offset_of_shock_time_64() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___shock_time_64)); }
	inline float get_shock_time_64() const { return ___shock_time_64; }
	inline float* get_address_of_shock_time_64() { return &___shock_time_64; }
	inline void set_shock_time_64(float value)
	{
		___shock_time_64 = value;
	}

	inline static int32_t get_offset_of_shock_type_65() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___shock_type_65)); }
	inline int32_t get_shock_type_65() const { return ___shock_type_65; }
	inline int32_t* get_address_of_shock_type_65() { return &___shock_type_65; }
	inline void set_shock_type_65(int32_t value)
	{
		___shock_type_65 = value;
	}

	inline static int32_t get_offset_of_shock_fps_66() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___shock_fps_66)); }
	inline int32_t get_shock_fps_66() const { return ___shock_fps_66; }
	inline int32_t* get_address_of_shock_fps_66() { return &___shock_fps_66; }
	inline void set_shock_fps_66(int32_t value)
	{
		___shock_fps_66 = value;
	}

	inline static int32_t get_offset_of_shock_lastingtime_67() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___shock_lastingtime_67)); }
	inline float get_shock_lastingtime_67() const { return ___shock_lastingtime_67; }
	inline float* get_address_of_shock_lastingtime_67() { return &___shock_lastingtime_67; }
	inline void set_shock_lastingtime_67(float value)
	{
		___shock_lastingtime_67 = value;
	}

	inline static int32_t get_offset_of_shock_strength_68() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___shock_strength_68)); }
	inline float get_shock_strength_68() const { return ___shock_strength_68; }
	inline float* get_address_of_shock_strength_68() { return &___shock_strength_68; }
	inline void set_shock_strength_68(float value)
	{
		___shock_strength_68 = value;
	}

	inline static int32_t get_offset_of_falseSkill_69() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___falseSkill_69)); }
	inline bool get_falseSkill_69() const { return ___falseSkill_69; }
	inline bool* get_address_of_falseSkill_69() { return &___falseSkill_69; }
	inline void set_falseSkill_69(bool value)
	{
		___falseSkill_69 = value;
	}

	inline static int32_t get_offset_of_skillIdArr_70() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___skillIdArr_70)); }
	inline String_t* get_skillIdArr_70() const { return ___skillIdArr_70; }
	inline String_t** get_address_of_skillIdArr_70() { return &___skillIdArr_70; }
	inline void set_skillIdArr_70(String_t* value)
	{
		___skillIdArr_70 = value;
		Il2CppCodeGenWriteBarrier(&___skillIdArr_70, value);
	}

	inline static int32_t get_offset_of_buffid_71() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___buffid_71)); }
	inline String_t* get_buffid_71() const { return ___buffid_71; }
	inline String_t** get_address_of_buffid_71() { return &___buffid_71; }
	inline void set_buffid_71(String_t* value)
	{
		___buffid_71 = value;
		Il2CppCodeGenWriteBarrier(&___buffid_71, value);
	}

	inline static int32_t get_offset_of_buffprob_72() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___buffprob_72)); }
	inline String_t* get_buffprob_72() const { return ___buffprob_72; }
	inline String_t** get_address_of_buffprob_72() { return &___buffprob_72; }
	inline void set_buffprob_72(String_t* value)
	{
		___buffprob_72 = value;
		Il2CppCodeGenWriteBarrier(&___buffprob_72, value);
	}

	inline static int32_t get_offset_of_buffevent_73() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___buffevent_73)); }
	inline String_t* get_buffevent_73() const { return ___buffevent_73; }
	inline String_t** get_address_of_buffevent_73() { return &___buffevent_73; }
	inline void set_buffevent_73(String_t* value)
	{
		___buffevent_73 = value;
		Il2CppCodeGenWriteBarrier(&___buffevent_73, value);
	}

	inline static int32_t get_offset_of_bufftarget_74() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___bufftarget_74)); }
	inline String_t* get_bufftarget_74() const { return ___bufftarget_74; }
	inline String_t** get_address_of_bufftarget_74() { return &___bufftarget_74; }
	inline void set_bufftarget_74(String_t* value)
	{
		___bufftarget_74 = value;
		Il2CppCodeGenWriteBarrier(&___bufftarget_74, value);
	}

	inline static int32_t get_offset_of_buffbreak_75() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___buffbreak_75)); }
	inline String_t* get_buffbreak_75() const { return ___buffbreak_75; }
	inline String_t** get_address_of_buffbreak_75() { return &___buffbreak_75; }
	inline void set_buffbreak_75(String_t* value)
	{
		___buffbreak_75 = value;
		Il2CppCodeGenWriteBarrier(&___buffbreak_75, value);
	}

	inline static int32_t get_offset_of_move_event_76() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___move_event_76)); }
	inline int32_t get_move_event_76() const { return ___move_event_76; }
	inline int32_t* get_address_of_move_event_76() { return &___move_event_76; }
	inline void set_move_event_76(int32_t value)
	{
		___move_event_76 = value;
	}

	inline static int32_t get_offset_of_move_start_time_77() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___move_start_time_77)); }
	inline float get_move_start_time_77() const { return ___move_start_time_77; }
	inline float* get_address_of_move_start_time_77() { return &___move_start_time_77; }
	inline void set_move_start_time_77(float value)
	{
		___move_start_time_77 = value;
	}

	inline static int32_t get_offset_of_movetype_78() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___movetype_78)); }
	inline int32_t get_movetype_78() const { return ___movetype_78; }
	inline int32_t* get_address_of_movetype_78() { return &___movetype_78; }
	inline void set_movetype_78(int32_t value)
	{
		___movetype_78 = value;
	}

	inline static int32_t get_offset_of_movetime_79() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___movetime_79)); }
	inline float get_movetime_79() const { return ___movetime_79; }
	inline float* get_address_of_movetime_79() { return &___movetime_79; }
	inline void set_movetime_79(float value)
	{
		___movetime_79 = value;
	}

	inline static int32_t get_offset_of_movespeed_80() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___movespeed_80)); }
	inline float get_movespeed_80() const { return ___movespeed_80; }
	inline float* get_address_of_movespeed_80() { return &___movespeed_80; }
	inline void set_movespeed_80(float value)
	{
		___movespeed_80 = value;
	}

	inline static int32_t get_offset_of_movepara_81() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___movepara_81)); }
	inline String_t* get_movepara_81() const { return ___movepara_81; }
	inline String_t** get_address_of_movepara_81() { return &___movepara_81; }
	inline void set_movepara_81(String_t* value)
	{
		___movepara_81 = value;
		Il2CppCodeGenWriteBarrier(&___movepara_81, value);
	}

	inline static int32_t get_offset_of_moveaction_82() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___moveaction_82)); }
	inline String_t* get_moveaction_82() const { return ___moveaction_82; }
	inline String_t** get_address_of_moveaction_82() { return &___moveaction_82; }
	inline void set_moveaction_82(String_t* value)
	{
		___moveaction_82 = value;
		Il2CppCodeGenWriteBarrier(&___moveaction_82, value);
	}

	inline static int32_t get_offset_of_movehight_83() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___movehight_83)); }
	inline float get_movehight_83() const { return ___movehight_83; }
	inline float* get_address_of_movehight_83() { return &___movehight_83; }
	inline void set_movehight_83(float value)
	{
		___movehight_83 = value;
	}

	inline static int32_t get_offset_of_radiusrage_84() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___radiusrage_84)); }
	inline float get_radiusrage_84() const { return ___radiusrage_84; }
	inline float* get_address_of_radiusrage_84() { return &___radiusrage_84; }
	inline void set_radiusrage_84(float value)
	{
		___radiusrage_84 = value;
	}

	inline static int32_t get_offset_of_sputter_id_85() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___sputter_id_85)); }
	inline int32_t get_sputter_id_85() const { return ___sputter_id_85; }
	inline int32_t* get_address_of_sputter_id_85() { return &___sputter_id_85; }
	inline void set_sputter_id_85(int32_t value)
	{
		___sputter_id_85 = value;
	}

	inline static int32_t get_offset_of_sputter_86() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___sputter_86)); }
	inline int32_t get_sputter_86() const { return ___sputter_86; }
	inline int32_t* get_address_of_sputter_86() { return &___sputter_86; }
	inline void set_sputter_86(int32_t value)
	{
		___sputter_86 = value;
	}

	inline static int32_t get_offset_of_sputter_radius_87() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___sputter_radius_87)); }
	inline float get_sputter_radius_87() const { return ___sputter_radius_87; }
	inline float* get_address_of_sputter_radius_87() { return &___sputter_radius_87; }
	inline void set_sputter_radius_87(float value)
	{
		___sputter_radius_87 = value;
	}

	inline static int32_t get_offset_of_sputter_num_88() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___sputter_num_88)); }
	inline int32_t get_sputter_num_88() const { return ___sputter_num_88; }
	inline int32_t* get_address_of_sputter_num_88() { return &___sputter_num_88; }
	inline void set_sputter_num_88(int32_t value)
	{
		___sputter_num_88 = value;
	}

	inline static int32_t get_offset_of_sputter_hurt_89() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___sputter_hurt_89)); }
	inline float get_sputter_hurt_89() const { return ___sputter_hurt_89; }
	inline float* get_address_of_sputter_hurt_89() { return &___sputter_hurt_89; }
	inline void set_sputter_hurt_89(float value)
	{
		___sputter_hurt_89 = value;
	}

	inline static int32_t get_offset_of_leader_skill_att_90() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___leader_skill_att_90)); }
	inline String_t* get_leader_skill_att_90() const { return ___leader_skill_att_90; }
	inline String_t** get_address_of_leader_skill_att_90() { return &___leader_skill_att_90; }
	inline void set_leader_skill_att_90(String_t* value)
	{
		___leader_skill_att_90 = value;
		Il2CppCodeGenWriteBarrier(&___leader_skill_att_90, value);
	}

	inline static int32_t get_offset_of_leader_skill_per_91() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___leader_skill_per_91)); }
	inline int32_t get_leader_skill_per_91() const { return ___leader_skill_per_91; }
	inline int32_t* get_address_of_leader_skill_per_91() { return &___leader_skill_per_91; }
	inline void set_leader_skill_per_91(int32_t value)
	{
		___leader_skill_per_91 = value;
	}

	inline static int32_t get_offset_of_leader_skill_value_92() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___leader_skill_value_92)); }
	inline int32_t get_leader_skill_value_92() const { return ___leader_skill_value_92; }
	inline int32_t* get_address_of_leader_skill_value_92() { return &___leader_skill_value_92; }
	inline void set_leader_skill_value_92(int32_t value)
	{
		___leader_skill_value_92 = value;
	}

	inline static int32_t get_offset_of_summonid_93() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___summonid_93)); }
	inline String_t* get_summonid_93() const { return ___summonid_93; }
	inline String_t** get_address_of_summonid_93() { return &___summonid_93; }
	inline void set_summonid_93(String_t* value)
	{
		___summonid_93 = value;
		Il2CppCodeGenWriteBarrier(&___summonid_93, value);
	}

	inline static int32_t get_offset_of_summon_time_94() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___summon_time_94)); }
	inline String_t* get_summon_time_94() const { return ___summon_time_94; }
	inline String_t** get_address_of_summon_time_94() { return &___summon_time_94; }
	inline void set_summon_time_94(String_t* value)
	{
		___summon_time_94 = value;
		Il2CppCodeGenWriteBarrier(&___summon_time_94, value);
	}

	inline static int32_t get_offset_of_summon_attackper_95() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___summon_attackper_95)); }
	inline String_t* get_summon_attackper_95() const { return ___summon_attackper_95; }
	inline String_t** get_address_of_summon_attackper_95() { return &___summon_attackper_95; }
	inline void set_summon_attackper_95(String_t* value)
	{
		___summon_attackper_95 = value;
		Il2CppCodeGenWriteBarrier(&___summon_attackper_95, value);
	}

	inline static int32_t get_offset_of_summonid_target_96() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___summonid_target_96)); }
	inline int32_t get_summonid_target_96() const { return ___summonid_target_96; }
	inline int32_t* get_address_of_summonid_target_96() { return &___summonid_target_96; }
	inline void set_summonid_target_96(int32_t value)
	{
		___summonid_target_96 = value;
	}

	inline static int32_t get_offset_of_SkillDes_97() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___SkillDes_97)); }
	inline String_t* get_SkillDes_97() const { return ___SkillDes_97; }
	inline String_t** get_address_of_SkillDes_97() { return &___SkillDes_97; }
	inline void set_SkillDes_97(String_t* value)
	{
		___SkillDes_97 = value;
		Il2CppCodeGenWriteBarrier(&___SkillDes_97, value);
	}

	inline static int32_t get_offset_of_skillbrief_98() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___skillbrief_98)); }
	inline String_t* get_skillbrief_98() const { return ___skillbrief_98; }
	inline String_t** get_address_of_skillbrief_98() { return &___skillbrief_98; }
	inline void set_skillbrief_98(String_t* value)
	{
		___skillbrief_98 = value;
		Il2CppCodeGenWriteBarrier(&___skillbrief_98, value);
	}

	inline static int32_t get_offset_of_openTermDes_99() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___openTermDes_99)); }
	inline String_t* get_openTermDes_99() const { return ___openTermDes_99; }
	inline String_t** get_address_of_openTermDes_99() { return &___openTermDes_99; }
	inline void set_openTermDes_99(String_t* value)
	{
		___openTermDes_99 = value;
		Il2CppCodeGenWriteBarrier(&___openTermDes_99, value);
	}

	inline static int32_t get_offset_of_evolveLevel_100() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___evolveLevel_100)); }
	inline int32_t get_evolveLevel_100() const { return ___evolveLevel_100; }
	inline int32_t* get_address_of_evolveLevel_100() { return &___evolveLevel_100; }
	inline void set_evolveLevel_100(int32_t value)
	{
		___evolveLevel_100 = value;
	}

	inline static int32_t get_offset_of_break_or_not_101() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___break_or_not_101)); }
	inline int32_t get_break_or_not_101() const { return ___break_or_not_101; }
	inline int32_t* get_address_of_break_or_not_101() { return &___break_or_not_101; }
	inline void set_break_or_not_101(int32_t value)
	{
		___break_or_not_101 = value;
	}

	inline static int32_t get_offset_of_upgrade_begin_102() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___upgrade_begin_102)); }
	inline int32_t get_upgrade_begin_102() const { return ___upgrade_begin_102; }
	inline int32_t* get_address_of_upgrade_begin_102() { return &___upgrade_begin_102; }
	inline void set_upgrade_begin_102(int32_t value)
	{
		___upgrade_begin_102 = value;
	}

	inline static int32_t get_offset_of_upgrade_end_103() { return static_cast<int32_t>(offsetof(skillCfg_t2142425171, ___upgrade_end_103)); }
	inline int32_t get_upgrade_end_103() const { return ___upgrade_end_103; }
	inline int32_t* get_address_of_upgrade_end_103() { return &___upgrade_end_103; }
	inline void set_upgrade_end_103(int32_t value)
	{
		___upgrade_end_103 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
