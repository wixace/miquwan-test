﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>
struct Dictionary_2_t509053110;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t666883479;
// System.Collections.Generic.IDictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>
struct IDictionary_2_t86926455;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t770439062;
// System.Collections.Generic.ICollection`1<Mihua.Assets.SubAssetMgr/ErrorInfo>
struct ICollection_1_t3528571197;
// System.Collections.ICollection
struct ICollection_t2643922881;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>[]
struct KeyValuePair_2U5BU5D_t4239437001;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>>
struct IEnumerator_1_t2319698865;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t951828701;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>
struct KeyCollection_t2135812561;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>
struct ValueCollection_t3504626119;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_407833816.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Mihua_Assets_SubAssetMgr_ErrorIn2633981210.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E1826376502.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor()
extern "C"  void Dictionary_2__ctor_m3318300227_gshared (Dictionary_2_t509053110 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m3318300227(__this, method) ((  void (*) (Dictionary_2_t509053110 *, const MethodInfo*))Dictionary_2__ctor_m3318300227_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2782251322_gshared (Dictionary_2_t509053110 * __this, Il2CppObject* ___comparer0, const MethodInfo* method);
#define Dictionary_2__ctor_m2782251322(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t509053110 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2782251322_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C"  void Dictionary_2__ctor_m366170933_gshared (Dictionary_2_t509053110 * __this, Il2CppObject* ___dictionary0, const MethodInfo* method);
#define Dictionary_2__ctor_m366170933(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t509053110 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m366170933_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Int32)
extern "C"  void Dictionary_2__ctor_m2662639380_gshared (Dictionary_2_t509053110 * __this, int32_t ___capacity0, const MethodInfo* method);
#define Dictionary_2__ctor_m2662639380(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t509053110 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m2662639380_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2__ctor_m2075640808_gshared (Dictionary_2_t509053110 * __this, Il2CppObject* ___dictionary0, Il2CppObject* ___comparer1, const MethodInfo* method);
#define Dictionary_2__ctor_m2075640808(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t509053110 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m2075640808_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2__ctor_m1754346756_gshared (Dictionary_2_t509053110 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2__ctor_m1754346756(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t509053110 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m1754346756_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2234218741_gshared (Dictionary_2_t509053110 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2234218741(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t509053110 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m2234218741_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1862147473_gshared (Dictionary_2_t509053110 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1862147473(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t509053110 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1862147473_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IDictionary.get_Keys()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Keys_m3473965249_gshared (Dictionary_2_t509053110 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m3473965249(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t509053110 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m3473965249_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IDictionary.get_Values()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Values_m3658570095_gshared (Dictionary_2_t509053110 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Values_m3658570095(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t509053110 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m3658570095_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IDictionary.get_IsFixedSize()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1503625146_gshared (Dictionary_2_t509053110 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1503625146(__this, method) ((  bool (*) (Dictionary_2_t509053110 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m1503625146_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IDictionary.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1333796863_gshared (Dictionary_2_t509053110 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1333796863(__this, method) ((  bool (*) (Dictionary_2_t509053110 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1333796863_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IDictionary.get_Item(System.Object)
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_get_Item_m1093339931_gshared (Dictionary_2_t509053110 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m1093339931(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t509053110 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m1093339931_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_set_Item_m453415370_gshared (Dictionary_2_t509053110 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m453415370(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t509053110 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m453415370_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Add_m1097479751_gshared (Dictionary_2_t509053110 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m1097479751(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t509053110 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m1097479751_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IDictionary.Contains(System.Object)
extern "C"  bool Dictionary_2_System_Collections_IDictionary_Contains_m502213643_gshared (Dictionary_2_t509053110 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m502213643(__this, ___key0, method) ((  bool (*) (Dictionary_2_t509053110 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m502213643_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IDictionary.Remove(System.Object)
extern "C"  void Dictionary_2_System_Collections_IDictionary_Remove_m2602164232_gshared (Dictionary_2_t509053110 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m2602164232(__this, ___key0, method) ((  void (*) (Dictionary_2_t509053110 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m2602164232_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1794766633_gshared (Dictionary_2_t509053110 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1794766633(__this, method) ((  bool (*) (Dictionary_2_t509053110 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m1794766633_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m250774043_gshared (Dictionary_2_t509053110 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m250774043(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t509053110 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m250774043_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3997760045_gshared (Dictionary_2_t509053110 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3997760045(__this, method) ((  bool (*) (Dictionary_2_t509053110 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m3997760045_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1494214814_gshared (Dictionary_2_t509053110 * __this, KeyValuePair_2_t407833816  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1494214814(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t509053110 *, KeyValuePair_2_t407833816 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m1494214814_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1299713192_gshared (Dictionary_2_t509053110 * __this, KeyValuePair_2_t407833816  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1299713192(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t509053110 *, KeyValuePair_2_t407833816 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m1299713192_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4163579138_gshared (Dictionary_2_t509053110 * __this, KeyValuePair_2U5BU5D_t4239437001* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4163579138(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t509053110 *, KeyValuePair_2U5BU5D_t4239437001*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4163579138_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m762396109_gshared (Dictionary_2_t509053110 * __this, KeyValuePair_2_t407833816  ___keyValuePair0, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m762396109(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t509053110 *, KeyValuePair_2_t407833816 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m762396109_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Dictionary_2_System_Collections_ICollection_CopyTo_m457878625_gshared (Dictionary_2_t509053110 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m457878625(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t509053110 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m457878625_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1709602608_gshared (Dictionary_2_t509053110 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1709602608(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t509053110 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m1709602608_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C"  Il2CppObject* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2221967143_gshared (Dictionary_2_t509053110 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2221967143(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t509053110 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m2221967143_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IDictionary.GetEnumerator()
extern "C"  Il2CppObject * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2565445492_gshared (Dictionary_2_t509053110 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2565445492(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t509053110 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m2565445492_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Count()
extern "C"  int32_t Dictionary_2_get_Count_m488109027_gshared (Dictionary_2_t509053110 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m488109027(__this, method) ((  int32_t (*) (Dictionary_2_t509053110 *, const MethodInfo*))Dictionary_2_get_Count_m488109027_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Item(TKey)
extern "C"  ErrorInfo_t2633981210  Dictionary_2_get_Item_m57275460_gshared (Dictionary_2_t509053110 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_get_Item_m57275460(__this, ___key0, method) ((  ErrorInfo_t2633981210  (*) (Dictionary_2_t509053110 *, Il2CppObject *, const MethodInfo*))Dictionary_2_get_Item_m57275460_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::set_Item(TKey,TValue)
extern "C"  void Dictionary_2_set_Item_m4196778691_gshared (Dictionary_2_t509053110 * __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
#define Dictionary_2_set_Item_m4196778691(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t509053110 *, Il2CppObject *, ErrorInfo_t2633981210 , const MethodInfo*))Dictionary_2_set_Item_m4196778691_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C"  void Dictionary_2_Init_m438992315_gshared (Dictionary_2_t509053110 * __this, int32_t ___capacity0, Il2CppObject* ___hcp1, const MethodInfo* method);
#define Dictionary_2_Init_m438992315(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t509053110 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m438992315_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::InitArrays(System.Int32)
extern "C"  void Dictionary_2_InitArrays_m2999110492_gshared (Dictionary_2_t509053110 * __this, int32_t ___size0, const MethodInfo* method);
#define Dictionary_2_InitArrays_m2999110492(__this, ___size0, method) ((  void (*) (Dictionary_2_t509053110 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m2999110492_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::CopyToCheck(System.Array,System.Int32)
extern "C"  void Dictionary_2_CopyToCheck_m3403716184_gshared (Dictionary_2_t509053110 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m3403716184(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t509053110 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3403716184_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::make_pair(TKey,TValue)
extern "C"  KeyValuePair_2_t407833816  Dictionary_2_make_pair_m302789612_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
#define Dictionary_2_make_pair_m302789612(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t407833816  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, ErrorInfo_t2633981210 , const MethodInfo*))Dictionary_2_make_pair_m302789612_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::pick_key(TKey,TValue)
extern "C"  Il2CppObject * Dictionary_2_pick_key_m490152530_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_key_m490152530(__this /* static, unused */, ___key0, ___value1, method) ((  Il2CppObject * (*) (Il2CppObject * /* static, unused */, Il2CppObject *, ErrorInfo_t2633981210 , const MethodInfo*))Dictionary_2_pick_key_m490152530_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::pick_value(TKey,TValue)
extern "C"  ErrorInfo_t2633981210  Dictionary_2_pick_value_m3185606830_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
#define Dictionary_2_pick_value_m3185606830(__this /* static, unused */, ___key0, ___value1, method) ((  ErrorInfo_t2633981210  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, ErrorInfo_t2633981210 , const MethodInfo*))Dictionary_2_pick_value_m3185606830_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C"  void Dictionary_2_CopyTo_m812075639_gshared (Dictionary_2_t509053110 * __this, KeyValuePair_2U5BU5D_t4239437001* ___array0, int32_t ___index1, const MethodInfo* method);
#define Dictionary_2_CopyTo_m812075639(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t509053110 *, KeyValuePair_2U5BU5D_t4239437001*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m812075639_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::Resize()
extern "C"  void Dictionary_2_Resize_m4140855893_gshared (Dictionary_2_t509053110 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m4140855893(__this, method) ((  void (*) (Dictionary_2_t509053110 *, const MethodInfo*))Dictionary_2_Resize_m4140855893_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::Add(TKey,TValue)
extern "C"  void Dictionary_2_Add_m3506196946_gshared (Dictionary_2_t509053110 * __this, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
#define Dictionary_2_Add_m3506196946(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t509053110 *, Il2CppObject *, ErrorInfo_t2633981210 , const MethodInfo*))Dictionary_2_Add_m3506196946_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::Clear()
extern "C"  void Dictionary_2_Clear_m724433518_gshared (Dictionary_2_t509053110 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m724433518(__this, method) ((  void (*) (Dictionary_2_t509053110 *, const MethodInfo*))Dictionary_2_Clear_m724433518_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::ContainsKey(TKey)
extern "C"  bool Dictionary_2_ContainsKey_m1784341272_gshared (Dictionary_2_t509053110 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m1784341272(__this, ___key0, method) ((  bool (*) (Dictionary_2_t509053110 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ContainsKey_m1784341272_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::ContainsValue(TValue)
extern "C"  bool Dictionary_2_ContainsValue_m568257304_gshared (Dictionary_2_t509053110 * __this, ErrorInfo_t2633981210  ___value0, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m568257304(__this, ___value0, method) ((  bool (*) (Dictionary_2_t509053110 *, ErrorInfo_t2633981210 , const MethodInfo*))Dictionary_2_ContainsValue_m568257304_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Dictionary_2_GetObjectData_m1138702433_gshared (Dictionary_2_t509053110 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m1138702433(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t509053110 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m1138702433_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::OnDeserialization(System.Object)
extern "C"  void Dictionary_2_OnDeserialization_m3629982883_gshared (Dictionary_2_t509053110 * __this, Il2CppObject * ___sender0, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m3629982883(__this, ___sender0, method) ((  void (*) (Dictionary_2_t509053110 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3629982883_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::Remove(TKey)
extern "C"  bool Dictionary_2_Remove_m2061092568_gshared (Dictionary_2_t509053110 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_Remove_m2061092568(__this, ___key0, method) ((  bool (*) (Dictionary_2_t509053110 *, Il2CppObject *, const MethodInfo*))Dictionary_2_Remove_m2061092568_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::TryGetValue(TKey,TValue&)
extern "C"  bool Dictionary_2_TryGetValue_m348038001_gshared (Dictionary_2_t509053110 * __this, Il2CppObject * ___key0, ErrorInfo_t2633981210 * ___value1, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m348038001(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t509053110 *, Il2CppObject *, ErrorInfo_t2633981210 *, const MethodInfo*))Dictionary_2_TryGetValue_m348038001_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Keys()
extern "C"  KeyCollection_t2135812561 * Dictionary_2_get_Keys_m39947722_gshared (Dictionary_2_t509053110 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m39947722(__this, method) ((  KeyCollection_t2135812561 * (*) (Dictionary_2_t509053110 *, const MethodInfo*))Dictionary_2_get_Keys_m39947722_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Values()
extern "C"  ValueCollection_t3504626119 * Dictionary_2_get_Values_m4181021670_gshared (Dictionary_2_t509053110 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m4181021670(__this, method) ((  ValueCollection_t3504626119 * (*) (Dictionary_2_t509053110 *, const MethodInfo*))Dictionary_2_get_Values_m4181021670_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::ToTKey(System.Object)
extern "C"  Il2CppObject * Dictionary_2_ToTKey_m4234978733_gshared (Dictionary_2_t509053110 * __this, Il2CppObject * ___key0, const MethodInfo* method);
#define Dictionary_2_ToTKey_m4234978733(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t509053110 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m4234978733_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::ToTValue(System.Object)
extern "C"  ErrorInfo_t2633981210  Dictionary_2_ToTValue_m1497920969_gshared (Dictionary_2_t509053110 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Dictionary_2_ToTValue_m1497920969(__this, ___value0, method) ((  ErrorInfo_t2633981210  (*) (Dictionary_2_t509053110 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m1497920969_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C"  bool Dictionary_2_ContainsKeyValuePair_m626537659_gshared (Dictionary_2_t509053110 * __this, KeyValuePair_2_t407833816  ___pair0, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m626537659(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t509053110 *, KeyValuePair_2_t407833816 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m626537659_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::GetEnumerator()
extern "C"  Enumerator_t1826376502  Dictionary_2_GetEnumerator_m889517070_gshared (Dictionary_2_t509053110 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m889517070(__this, method) ((  Enumerator_t1826376502  (*) (Dictionary_2_t509053110 *, const MethodInfo*))Dictionary_2_GetEnumerator_m889517070_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,Mihua.Assets.SubAssetMgr/ErrorInfo>::<CopyTo>m__0(TKey,TValue)
extern "C"  DictionaryEntry_t1751606614  Dictionary_2_U3CCopyToU3Em__0_m573617413_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___key0, ErrorInfo_t2633981210  ___value1, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m573617413(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, ErrorInfo_t2633981210 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m573617413_gshared)(__this /* static, unused */, ___key0, ___value1, method)
