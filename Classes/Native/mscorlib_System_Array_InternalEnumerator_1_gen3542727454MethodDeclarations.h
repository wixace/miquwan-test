﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen3542727454.h"
#include "mscorlib_System_Array1146569071.h"
#include "UnityEngine_UnityEngine_Particle465417482.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Particle>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m2301420302_gshared (InternalEnumerator_1_t3542727454 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m2301420302(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t3542727454 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m2301420302_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Particle>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m127019986_gshared (InternalEnumerator_1_t3542727454 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m127019986(__this, method) ((  void (*) (InternalEnumerator_1_t3542727454 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m127019986_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Particle>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1622217982_gshared (InternalEnumerator_1_t3542727454 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1622217982(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t3542727454 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m1622217982_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Particle>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m2146059877_gshared (InternalEnumerator_1_t3542727454 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m2146059877(__this, method) ((  void (*) (InternalEnumerator_1_t3542727454 *, const MethodInfo*))InternalEnumerator_1_Dispose_m2146059877_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Particle>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m320919998_gshared (InternalEnumerator_1_t3542727454 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m320919998(__this, method) ((  bool (*) (InternalEnumerator_1_t3542727454 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m320919998_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Particle>::get_Current()
extern "C"  Particle_t465417482  InternalEnumerator_1_get_Current_m964606613_gshared (InternalEnumerator_1_t3542727454 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m964606613(__this, method) ((  Particle_t465417482  (*) (InternalEnumerator_1_t3542727454 *, const MethodInfo*))InternalEnumerator_1_get_Current_m964606613_gshared)(__this, method)
