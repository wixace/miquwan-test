﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSRepresentedObject/<CreateDestructAction>c__AnonStoreyFB
struct U3CCreateDestructActionU3Ec__AnonStoreyFB_t742030960;

#include "codegen/il2cpp-codegen.h"

// System.Void CSRepresentedObject/<CreateDestructAction>c__AnonStoreyFB::.ctor()
extern "C"  void U3CCreateDestructActionU3Ec__AnonStoreyFB__ctor_m752575739 (U3CCreateDestructActionU3Ec__AnonStoreyFB_t742030960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSRepresentedObject/<CreateDestructAction>c__AnonStoreyFB::<>m__31E()
extern "C"  void U3CCreateDestructActionU3Ec__AnonStoreyFB_U3CU3Em__31E_m3086222661 (U3CCreateDestructActionU3Ec__AnonStoreyFB_t742030960 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
