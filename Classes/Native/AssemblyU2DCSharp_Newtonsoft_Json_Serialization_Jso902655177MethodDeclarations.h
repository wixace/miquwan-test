﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JsonProperty
struct JsonProperty_t902655177;
// System.String
struct String_t;
// Newtonsoft.Json.Serialization.IValueProvider
struct IValueProvider_t175677893;
// System.Type
struct Type_t;
// Newtonsoft.Json.JsonConverter
struct JsonConverter_t2159686854;
// System.Object
struct Il2CppObject;
// System.Predicate`1<System.Object>
struct Predicate_1_t3781873254;
// System.Action`2<System.Object,System.Object>
struct Action_2_t4293064463;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Nullable_1_gen1237965023.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonConverter2159686854.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Required3921306327.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"
#include "mscorlib_System_Nullable_1_gen2838778904.h"
#include "mscorlib_System_Nullable_1_gen1653574568.h"
#include "mscorlib_System_Nullable_1_gen2845787645.h"
#include "mscorlib_System_Nullable_1_gen140208118.h"
#include "mscorlib_System_Nullable_1_gen2443451997.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso902655177.h"

// System.Void Newtonsoft.Json.Serialization.JsonProperty::.ctor()
extern "C"  void JsonProperty__ctor_m838248715 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonProperty::get_PropertyName()
extern "C"  String_t* JsonProperty_get_PropertyName_m3204019141 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_PropertyName(System.String)
extern "C"  void JsonProperty_set_PropertyName_m2432916588 (JsonProperty_t902655177 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.Serialization.JsonProperty::get_Order()
extern "C"  Nullable_1_t1237965023  JsonProperty_get_Order_m405679167 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Order(System.Nullable`1<System.Int32>)
extern "C"  void JsonProperty_set_Order_m3005805556 (JsonProperty_t902655177 * __this, Nullable_1_t1237965023  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonProperty::get_UnderlyingName()
extern "C"  String_t* JsonProperty_get_UnderlyingName_m1594652237 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_UnderlyingName(System.String)
extern "C"  void JsonProperty_set_UnderlyingName_m2731585316 (JsonProperty_t902655177 * __this, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.IValueProvider Newtonsoft.Json.Serialization.JsonProperty::get_ValueProvider()
extern "C"  Il2CppObject * JsonProperty_get_ValueProvider_m184459761 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ValueProvider(Newtonsoft.Json.Serialization.IValueProvider)
extern "C"  void JsonProperty_set_ValueProvider_m669931970 (JsonProperty_t902655177 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type Newtonsoft.Json.Serialization.JsonProperty::get_PropertyType()
extern "C"  Type_t * JsonProperty_get_PropertyType_m3384045419 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_PropertyType(System.Type)
extern "C"  void JsonProperty_set_PropertyType_m2543013652 (JsonProperty_t902655177 * __this, Type_t * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::get_Converter()
extern "C"  JsonConverter_t2159686854 * JsonProperty_get_Converter_m4115912016 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Converter(Newtonsoft.Json.JsonConverter)
extern "C"  void JsonProperty_set_Converter_m4168865283 (JsonProperty_t902655177 * __this, JsonConverter_t2159686854 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::get_MemberConverter()
extern "C"  JsonConverter_t2159686854 * JsonProperty_get_MemberConverter_m1774595862 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_MemberConverter(Newtonsoft.Json.JsonConverter)
extern "C"  void JsonProperty_set_MemberConverter_m1519107837 (JsonProperty_t902655177 * __this, JsonConverter_t2159686854 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_Ignored()
extern "C"  bool JsonProperty_get_Ignored_m3044358814 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Ignored(System.Boolean)
extern "C"  void JsonProperty_set_Ignored_m780750293 (JsonProperty_t902655177 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_Readable()
extern "C"  bool JsonProperty_get_Readable_m3183624454 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Readable(System.Boolean)
extern "C"  void JsonProperty_set_Readable_m1472311741 (JsonProperty_t902655177 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::get_Writable()
extern "C"  bool JsonProperty_get_Writable_m2551723190 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Writable(System.Boolean)
extern "C"  void JsonProperty_set_Writable_m101413229 (JsonProperty_t902655177 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Serialization.JsonProperty::get_DefaultValue()
extern "C"  Il2CppObject * JsonProperty_get_DefaultValue_m2243659911 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_DefaultValue(System.Object)
extern "C"  void JsonProperty_set_DefaultValue_m2024814414 (JsonProperty_t902655177 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Required Newtonsoft.Json.Serialization.JsonProperty::get_Required()
extern "C"  int32_t JsonProperty_get_Required_m2136426250 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_Required(Newtonsoft.Json.Required)
extern "C"  void JsonProperty_set_Required_m3485786369 (JsonProperty_t902655177 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonProperty::get_IsReference()
extern "C"  Nullable_1_t560925241  JsonProperty_get_IsReference_m2980617816 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_IsReference(System.Nullable`1<System.Boolean>)
extern "C"  void JsonProperty_set_IsReference_m2855740859 (JsonProperty_t902655177 * __this, Nullable_1_t560925241  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.Serialization.JsonProperty::get_NullValueHandling()
extern "C"  Nullable_1_t2838778904  JsonProperty_get_NullValueHandling_m1339160097 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_NullValueHandling(System.Nullable`1<Newtonsoft.Json.NullValueHandling>)
extern "C"  void JsonProperty_set_NullValueHandling_m3246714444 (JsonProperty_t902655177 * __this, Nullable_1_t2838778904  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling> Newtonsoft.Json.Serialization.JsonProperty::get_DefaultValueHandling()
extern "C"  Nullable_1_t1653574568  JsonProperty_get_DefaultValueHandling_m2477507009 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_DefaultValueHandling(System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>)
extern "C"  void JsonProperty_set_DefaultValueHandling_m723588126 (JsonProperty_t902655177 * __this, Nullable_1_t1653574568  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonProperty::get_ReferenceLoopHandling()
extern "C"  Nullable_1_t2845787645  JsonProperty_get_ReferenceLoopHandling_m3595109505 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ReferenceLoopHandling(System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>)
extern "C"  void JsonProperty_set_ReferenceLoopHandling_m449726742 (JsonProperty_t902655177 * __this, Nullable_1_t2845787645  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling> Newtonsoft.Json.Serialization.JsonProperty::get_ObjectCreationHandling()
extern "C"  Nullable_1_t140208118  JsonProperty_get_ObjectCreationHandling_m3387899485 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ObjectCreationHandling(System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>)
extern "C"  void JsonProperty_set_ObjectCreationHandling_m3299715486 (JsonProperty_t902655177 * __this, Nullable_1_t140208118  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonProperty::get_TypeNameHandling()
extern "C"  Nullable_1_t2443451997  JsonProperty_get_TypeNameHandling_m2920599915 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_TypeNameHandling(System.Nullable`1<Newtonsoft.Json.TypeNameHandling>)
extern "C"  void JsonProperty_set_TypeNameHandling_m1061207678 (JsonProperty_t902655177 * __this, Nullable_1_t2443451997  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_ShouldSerialize()
extern "C"  Predicate_1_t3781873254 * JsonProperty_get_ShouldSerialize_m765152429 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_ShouldSerialize(System.Predicate`1<System.Object>)
extern "C"  void JsonProperty_set_ShouldSerialize_m2201931334 (JsonProperty_t902655177 * __this, Predicate_1_t3781873254 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_GetIsSpecified()
extern "C"  Predicate_1_t3781873254 * JsonProperty_get_GetIsSpecified_m4141211410 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_GetIsSpecified(System.Predicate`1<System.Object>)
extern "C"  void JsonProperty_set_GetIsSpecified_m1272462105 (JsonProperty_t902655177 * __this, Predicate_1_t3781873254 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Action`2<System.Object,System.Object> Newtonsoft.Json.Serialization.JsonProperty::get_SetIsSpecified()
extern "C"  Action_2_t4293064463 * JsonProperty_get_SetIsSpecified_m1732768832 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JsonProperty::set_SetIsSpecified(System.Action`2<System.Object,System.Object>)
extern "C"  void JsonProperty_set_SetIsSpecified_m840109303 (JsonProperty_t902655177 * __this, Action_2_t4293064463 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonProperty::ToString()
extern "C"  String_t* JsonProperty_ToString_m2046350376 (JsonProperty_t902655177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Serialization.JsonProperty::ilo_get_PropertyName1(Newtonsoft.Json.Serialization.JsonProperty)
extern "C"  String_t* JsonProperty_ilo_get_PropertyName1_m399303947 (Il2CppObject * __this /* static, unused */, JsonProperty_t902655177 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
