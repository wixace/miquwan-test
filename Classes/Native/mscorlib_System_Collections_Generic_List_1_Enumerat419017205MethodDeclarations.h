﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>
struct List_1_t399344435;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat419017205.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.IntPoint>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3963183348_gshared (Enumerator_t419017205 * __this, List_1_t399344435 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3963183348(__this, ___l0, method) ((  void (*) (Enumerator_t419017205 *, List_1_t399344435 *, const MethodInfo*))Enumerator__ctor_m3963183348_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.IntPoint>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m854715102_gshared (Enumerator_t419017205 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m854715102(__this, method) ((  void (*) (Enumerator_t419017205 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m854715102_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.IntPoint>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m435632330_gshared (Enumerator_t419017205 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m435632330(__this, method) ((  Il2CppObject * (*) (Enumerator_t419017205 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m435632330_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.IntPoint>::Dispose()
extern "C"  void Enumerator_Dispose_m1997958105_gshared (Enumerator_t419017205 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1997958105(__this, method) ((  void (*) (Enumerator_t419017205 *, const MethodInfo*))Enumerator_Dispose_m1997958105_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.IntPoint>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1427019410_gshared (Enumerator_t419017205 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1427019410(__this, method) ((  void (*) (Enumerator_t419017205 *, const MethodInfo*))Enumerator_VerifyState_m1427019410_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.IntPoint>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2619891786_gshared (Enumerator_t419017205 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2619891786(__this, method) ((  bool (*) (Enumerator_t419017205 *, const MethodInfo*))Enumerator_MoveNext_m2619891786_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.ClipperLib.IntPoint>::get_Current()
extern "C"  IntPoint_t3326126179  Enumerator_get_Current_m2208877769_gshared (Enumerator_t419017205 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2208877769(__this, method) ((  IntPoint_t3326126179  (*) (Enumerator_t419017205 *, const MethodInfo*))Enumerator_get_Current_m2208877769_gshared)(__this, method)
