﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._384d1559d6898a2a5a6e1a165cca38cd
struct _384d1559d6898a2a5a6e1a165cca38cd_t2772600067;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__384d1559d6898a2a5a6e1a162772600067.h"

// System.Void Little._384d1559d6898a2a5a6e1a165cca38cd::.ctor()
extern "C"  void _384d1559d6898a2a5a6e1a165cca38cd__ctor_m3848443018 (_384d1559d6898a2a5a6e1a165cca38cd_t2772600067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._384d1559d6898a2a5a6e1a165cca38cd::_384d1559d6898a2a5a6e1a165cca38cdm2(System.Int32)
extern "C"  int32_t _384d1559d6898a2a5a6e1a165cca38cd__384d1559d6898a2a5a6e1a165cca38cdm2_m3578813113 (_384d1559d6898a2a5a6e1a165cca38cd_t2772600067 * __this, int32_t ____384d1559d6898a2a5a6e1a165cca38cda0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._384d1559d6898a2a5a6e1a165cca38cd::_384d1559d6898a2a5a6e1a165cca38cdm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _384d1559d6898a2a5a6e1a165cca38cd__384d1559d6898a2a5a6e1a165cca38cdm_m3321024541 (_384d1559d6898a2a5a6e1a165cca38cd_t2772600067 * __this, int32_t ____384d1559d6898a2a5a6e1a165cca38cda0, int32_t ____384d1559d6898a2a5a6e1a165cca38cd861, int32_t ____384d1559d6898a2a5a6e1a165cca38cdc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._384d1559d6898a2a5a6e1a165cca38cd::ilo__384d1559d6898a2a5a6e1a165cca38cdm21(Little._384d1559d6898a2a5a6e1a165cca38cd,System.Int32)
extern "C"  int32_t _384d1559d6898a2a5a6e1a165cca38cd_ilo__384d1559d6898a2a5a6e1a165cca38cdm21_m1061966890 (Il2CppObject * __this /* static, unused */, _384d1559d6898a2a5a6e1a165cca38cd_t2772600067 * ____this0, int32_t ____384d1559d6898a2a5a6e1a165cca38cda1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
