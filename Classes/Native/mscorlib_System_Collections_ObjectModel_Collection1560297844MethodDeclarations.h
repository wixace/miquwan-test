﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>
struct Collection_1_t1560297844;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// UnityEngine.MeshSubsetCombineUtility/SubMeshInstance[]
struct SubMeshInstanceU5BU5D_t2339421603;
// System.Collections.Generic.IEnumerator`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>
struct IEnumerator_1_t4286704735;
// System.Collections.Generic.IList`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>
struct IList_1_t774519593;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_MeshSubsetCombineUtility_S2374839686.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::.ctor()
extern "C"  void Collection_1__ctor_m115325197_gshared (Collection_1_t1560297844 * __this, const MethodInfo* method);
#define Collection_1__ctor_m115325197(__this, method) ((  void (*) (Collection_1_t1560297844 *, const MethodInfo*))Collection_1__ctor_m115325197_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m152524426_gshared (Collection_1_t1560297844 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m152524426(__this, method) ((  bool (*) (Collection_1_t1560297844 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m152524426_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m655208663_gshared (Collection_1_t1560297844 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m655208663(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1560297844 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m655208663_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m3891069734_gshared (Collection_1_t1560297844 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m3891069734(__this, method) ((  Il2CppObject * (*) (Collection_1_t1560297844 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m3891069734_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m2772973143_gshared (Collection_1_t1560297844 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m2772973143(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1560297844 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m2772973143_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m2564246985_gshared (Collection_1_t1560297844 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m2564246985(__this, ___value0, method) ((  bool (*) (Collection_1_t1560297844 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m2564246985_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m1850173743_gshared (Collection_1_t1560297844 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m1850173743(__this, ___value0, method) ((  int32_t (*) (Collection_1_t1560297844 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m1850173743_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2778065186_gshared (Collection_1_t1560297844 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2778065186(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1560297844 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2778065186_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m753897670_gshared (Collection_1_t1560297844 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m753897670(__this, ___value0, method) ((  void (*) (Collection_1_t1560297844 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m753897670_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m1188579571_gshared (Collection_1_t1560297844 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m1188579571(__this, method) ((  bool (*) (Collection_1_t1560297844 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m1188579571_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m617500773_gshared (Collection_1_t1560297844 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m617500773(__this, method) ((  Il2CppObject * (*) (Collection_1_t1560297844 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m617500773_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2797291512_gshared (Collection_1_t1560297844 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2797291512(__this, method) ((  bool (*) (Collection_1_t1560297844 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2797291512_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m2345359361_gshared (Collection_1_t1560297844 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2345359361(__this, method) ((  bool (*) (Collection_1_t1560297844 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m2345359361_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m1778367660_gshared (Collection_1_t1560297844 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1778367660(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t1560297844 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1778367660_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m3159905401_gshared (Collection_1_t1560297844 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m3159905401(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1560297844 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m3159905401_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Add(T)
extern "C"  void Collection_1_Add_m2349167058_gshared (Collection_1_t1560297844 * __this, SubMeshInstance_t2374839686  ___item0, const MethodInfo* method);
#define Collection_1_Add_m2349167058(__this, ___item0, method) ((  void (*) (Collection_1_t1560297844 *, SubMeshInstance_t2374839686 , const MethodInfo*))Collection_1_Add_m2349167058_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Clear()
extern "C"  void Collection_1_Clear_m1816425784_gshared (Collection_1_t1560297844 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1816425784(__this, method) ((  void (*) (Collection_1_t1560297844 *, const MethodInfo*))Collection_1_Clear_m1816425784_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::ClearItems()
extern "C"  void Collection_1_ClearItems_m4231507658_gshared (Collection_1_t1560297844 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m4231507658(__this, method) ((  void (*) (Collection_1_t1560297844 *, const MethodInfo*))Collection_1_ClearItems_m4231507658_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Contains(T)
extern "C"  bool Collection_1_Contains_m2741064106_gshared (Collection_1_t1560297844 * __this, SubMeshInstance_t2374839686  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m2741064106(__this, ___item0, method) ((  bool (*) (Collection_1_t1560297844 *, SubMeshInstance_t2374839686 , const MethodInfo*))Collection_1_Contains_m2741064106_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m752175426_gshared (Collection_1_t1560297844 * __this, SubMeshInstanceU5BU5D_t2339421603* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m752175426(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t1560297844 *, SubMeshInstanceU5BU5D_t2339421603*, int32_t, const MethodInfo*))Collection_1_CopyTo_m752175426_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m2055146881_gshared (Collection_1_t1560297844 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m2055146881(__this, method) ((  Il2CppObject* (*) (Collection_1_t1560297844 *, const MethodInfo*))Collection_1_GetEnumerator_m2055146881_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m2103732622_gshared (Collection_1_t1560297844 * __this, SubMeshInstance_t2374839686  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m2103732622(__this, ___item0, method) ((  int32_t (*) (Collection_1_t1560297844 *, SubMeshInstance_t2374839686 , const MethodInfo*))Collection_1_IndexOf_m2103732622_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m1180223545_gshared (Collection_1_t1560297844 * __this, int32_t ___index0, SubMeshInstance_t2374839686  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m1180223545(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1560297844 *, int32_t, SubMeshInstance_t2374839686 , const MethodInfo*))Collection_1_Insert_m1180223545_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m3247387116_gshared (Collection_1_t1560297844 * __this, int32_t ___index0, SubMeshInstance_t2374839686  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m3247387116(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1560297844 *, int32_t, SubMeshInstance_t2374839686 , const MethodInfo*))Collection_1_InsertItem_m3247387116_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m1273025912_gshared (Collection_1_t1560297844 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m1273025912(__this, method) ((  Il2CppObject* (*) (Collection_1_t1560297844 *, const MethodInfo*))Collection_1_get_Items_m1273025912_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::Remove(T)
extern "C"  bool Collection_1_Remove_m87192613_gshared (Collection_1_t1560297844 * __this, SubMeshInstance_t2374839686  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m87192613(__this, ___item0, method) ((  bool (*) (Collection_1_t1560297844 *, SubMeshInstance_t2374839686 , const MethodInfo*))Collection_1_Remove_m87192613_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m3349043711_gshared (Collection_1_t1560297844 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m3349043711(__this, ___index0, method) ((  void (*) (Collection_1_t1560297844 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m3349043711_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m105159263_gshared (Collection_1_t1560297844 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m105159263(__this, ___index0, method) ((  void (*) (Collection_1_t1560297844 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m105159263_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m608509357_gshared (Collection_1_t1560297844 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m608509357(__this, method) ((  int32_t (*) (Collection_1_t1560297844 *, const MethodInfo*))Collection_1_get_Count_m608509357_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::get_Item(System.Int32)
extern "C"  SubMeshInstance_t2374839686  Collection_1_get_Item_m619159013_gshared (Collection_1_t1560297844 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m619159013(__this, ___index0, method) ((  SubMeshInstance_t2374839686  (*) (Collection_1_t1560297844 *, int32_t, const MethodInfo*))Collection_1_get_Item_m619159013_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m2133057232_gshared (Collection_1_t1560297844 * __this, int32_t ___index0, SubMeshInstance_t2374839686  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m2133057232(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t1560297844 *, int32_t, SubMeshInstance_t2374839686 , const MethodInfo*))Collection_1_set_Item_m2133057232_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m3543781289_gshared (Collection_1_t1560297844 * __this, int32_t ___index0, SubMeshInstance_t2374839686  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m3543781289(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t1560297844 *, int32_t, SubMeshInstance_t2374839686 , const MethodInfo*))Collection_1_SetItem_m3543781289_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m3016775490_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m3016775490(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m3016775490_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::ConvertItem(System.Object)
extern "C"  SubMeshInstance_t2374839686  Collection_1_ConvertItem_m1571148356_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m1571148356(__this /* static, unused */, ___item0, method) ((  SubMeshInstance_t2374839686  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1571148356_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m1437776898_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m1437776898(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m1437776898_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m280752706_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m280752706(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m280752706_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/SubMeshInstance>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m3335998365_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m3335998365(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m3335998365_gshared)(__this /* static, unused */, ___list0, method)
