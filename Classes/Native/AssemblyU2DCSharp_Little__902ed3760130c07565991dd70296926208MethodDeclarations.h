﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._902ed3760130c07565991dd70d32741b
struct _902ed3760130c07565991dd70d32741b_t296926208;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__902ed3760130c07565991dd70296926208.h"

// System.Void Little._902ed3760130c07565991dd70d32741b::.ctor()
extern "C"  void _902ed3760130c07565991dd70d32741b__ctor_m1633937965 (_902ed3760130c07565991dd70d32741b_t296926208 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._902ed3760130c07565991dd70d32741b::_902ed3760130c07565991dd70d32741bm2(System.Int32)
extern "C"  int32_t _902ed3760130c07565991dd70d32741b__902ed3760130c07565991dd70d32741bm2_m2342883737 (_902ed3760130c07565991dd70d32741b_t296926208 * __this, int32_t ____902ed3760130c07565991dd70d32741ba0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._902ed3760130c07565991dd70d32741b::_902ed3760130c07565991dd70d32741bm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _902ed3760130c07565991dd70d32741b__902ed3760130c07565991dd70d32741bm_m3740210493 (_902ed3760130c07565991dd70d32741b_t296926208 * __this, int32_t ____902ed3760130c07565991dd70d32741ba0, int32_t ____902ed3760130c07565991dd70d32741b41, int32_t ____902ed3760130c07565991dd70d32741bc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._902ed3760130c07565991dd70d32741b::ilo__902ed3760130c07565991dd70d32741bm21(Little._902ed3760130c07565991dd70d32741b,System.Int32)
extern "C"  int32_t _902ed3760130c07565991dd70d32741b_ilo__902ed3760130c07565991dd70d32741bm21_m1353114055 (Il2CppObject * __this /* static, unused */, _902ed3760130c07565991dd70d32741b_t296926208 * ____this0, int32_t ____902ed3760130c07565991dd70d32741ba1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
