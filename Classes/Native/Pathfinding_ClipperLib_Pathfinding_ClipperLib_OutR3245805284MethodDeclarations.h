﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.ClipperLib.OutRec
struct OutRec_t3245805284;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.ClipperLib.OutRec::.ctor()
extern "C"  void OutRec__ctor_m2716404681 (OutRec_t3245805284 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
