﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ReplayOtherPlayer
struct ReplayOtherPlayer_t1774842762;
// System.String
struct String_t;
// ReplayHeroUnit
struct ReplayHeroUnit_t605237701;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_ReplayHeroUnit605237701.h"

// System.Void ReplayOtherPlayer::.ctor()
extern "C"  void ReplayOtherPlayer__ctor_m2250380577 (ReplayOtherPlayer_t1774842762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ReplayOtherPlayer::ParserJson(System.String)
extern "C"  void ReplayOtherPlayer_ParserJson_m2948533464 (ReplayOtherPlayer_t1774842762 * __this, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayOtherPlayer::GetJsonStr()
extern "C"  String_t* ReplayOtherPlayer_GetJsonStr_m2554282803 (ReplayOtherPlayer_t1774842762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ReplayHeroUnit ReplayOtherPlayer::GetHeroUnit(System.UInt32)
extern "C"  ReplayHeroUnit_t605237701 * ReplayOtherPlayer_GetHeroUnit_m2189399599 (ReplayOtherPlayer_t1774842762 * __this, uint32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String ReplayOtherPlayer::ilo_GetJsonStr1(ReplayHeroUnit)
extern "C"  String_t* ReplayOtherPlayer_ilo_GetJsonStr1_m352606472 (Il2CppObject * __this /* static, unused */, ReplayHeroUnit_t605237701 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
