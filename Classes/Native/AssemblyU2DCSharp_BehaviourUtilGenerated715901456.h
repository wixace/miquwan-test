﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MethodID
struct MethodID_t3916401116;
// JSDataExchangeMgr/DGetV`1<System.Action>
struct DGetV_1_t3648885239;
// JSDataExchangeMgr/DGetV`1<BehaviourUtil/JSDelayFun4>
struct DGetV_1_t952362796;
// JSDataExchangeMgr/DGetV`1<BehaviourUtil/JSDelayFun3>
struct DGetV_1_t952362795;
// JSDataExchangeMgr/DGetV`1<BehaviourUtil/JSDelayFun2>
struct DGetV_1_t952362794;
// JSDataExchangeMgr/DGetV`1<BehaviourUtil/JSDelayFun1>
struct DGetV_1_t952362793;
// JSDataExchangeMgr/DGetV`1<BehaviourUtil/JSDelayFun0>
struct DGetV_1_t952362792;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BehaviourUtilGenerated
struct  BehaviourUtilGenerated_t715901456  : public Il2CppObject
{
public:

public:
};

struct BehaviourUtilGenerated_t715901456_StaticFields
{
public:
	// MethodID BehaviourUtilGenerated::methodID1
	MethodID_t3916401116 * ___methodID1_0;
	// MethodID BehaviourUtilGenerated::methodID2
	MethodID_t3916401116 * ___methodID2_1;
	// MethodID BehaviourUtilGenerated::methodID3
	MethodID_t3916401116 * ___methodID3_2;
	// MethodID BehaviourUtilGenerated::methodID4
	MethodID_t3916401116 * ___methodID4_3;
	// JSDataExchangeMgr/DGetV`1<System.Action> BehaviourUtilGenerated::<>f__am$cache4
	DGetV_1_t3648885239 * ___U3CU3Ef__amU24cache4_4;
	// JSDataExchangeMgr/DGetV`1<BehaviourUtil/JSDelayFun4> BehaviourUtilGenerated::<>f__am$cache5
	DGetV_1_t952362796 * ___U3CU3Ef__amU24cache5_5;
	// JSDataExchangeMgr/DGetV`1<BehaviourUtil/JSDelayFun3> BehaviourUtilGenerated::<>f__am$cache6
	DGetV_1_t952362795 * ___U3CU3Ef__amU24cache6_6;
	// JSDataExchangeMgr/DGetV`1<BehaviourUtil/JSDelayFun2> BehaviourUtilGenerated::<>f__am$cache7
	DGetV_1_t952362794 * ___U3CU3Ef__amU24cache7_7;
	// JSDataExchangeMgr/DGetV`1<BehaviourUtil/JSDelayFun1> BehaviourUtilGenerated::<>f__am$cache8
	DGetV_1_t952362793 * ___U3CU3Ef__amU24cache8_8;
	// JSDataExchangeMgr/DGetV`1<BehaviourUtil/JSDelayFun0> BehaviourUtilGenerated::<>f__am$cache9
	DGetV_1_t952362792 * ___U3CU3Ef__amU24cache9_9;

public:
	inline static int32_t get_offset_of_methodID1_0() { return static_cast<int32_t>(offsetof(BehaviourUtilGenerated_t715901456_StaticFields, ___methodID1_0)); }
	inline MethodID_t3916401116 * get_methodID1_0() const { return ___methodID1_0; }
	inline MethodID_t3916401116 ** get_address_of_methodID1_0() { return &___methodID1_0; }
	inline void set_methodID1_0(MethodID_t3916401116 * value)
	{
		___methodID1_0 = value;
		Il2CppCodeGenWriteBarrier(&___methodID1_0, value);
	}

	inline static int32_t get_offset_of_methodID2_1() { return static_cast<int32_t>(offsetof(BehaviourUtilGenerated_t715901456_StaticFields, ___methodID2_1)); }
	inline MethodID_t3916401116 * get_methodID2_1() const { return ___methodID2_1; }
	inline MethodID_t3916401116 ** get_address_of_methodID2_1() { return &___methodID2_1; }
	inline void set_methodID2_1(MethodID_t3916401116 * value)
	{
		___methodID2_1 = value;
		Il2CppCodeGenWriteBarrier(&___methodID2_1, value);
	}

	inline static int32_t get_offset_of_methodID3_2() { return static_cast<int32_t>(offsetof(BehaviourUtilGenerated_t715901456_StaticFields, ___methodID3_2)); }
	inline MethodID_t3916401116 * get_methodID3_2() const { return ___methodID3_2; }
	inline MethodID_t3916401116 ** get_address_of_methodID3_2() { return &___methodID3_2; }
	inline void set_methodID3_2(MethodID_t3916401116 * value)
	{
		___methodID3_2 = value;
		Il2CppCodeGenWriteBarrier(&___methodID3_2, value);
	}

	inline static int32_t get_offset_of_methodID4_3() { return static_cast<int32_t>(offsetof(BehaviourUtilGenerated_t715901456_StaticFields, ___methodID4_3)); }
	inline MethodID_t3916401116 * get_methodID4_3() const { return ___methodID4_3; }
	inline MethodID_t3916401116 ** get_address_of_methodID4_3() { return &___methodID4_3; }
	inline void set_methodID4_3(MethodID_t3916401116 * value)
	{
		___methodID4_3 = value;
		Il2CppCodeGenWriteBarrier(&___methodID4_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_4() { return static_cast<int32_t>(offsetof(BehaviourUtilGenerated_t715901456_StaticFields, ___U3CU3Ef__amU24cache4_4)); }
	inline DGetV_1_t3648885239 * get_U3CU3Ef__amU24cache4_4() const { return ___U3CU3Ef__amU24cache4_4; }
	inline DGetV_1_t3648885239 ** get_address_of_U3CU3Ef__amU24cache4_4() { return &___U3CU3Ef__amU24cache4_4; }
	inline void set_U3CU3Ef__amU24cache4_4(DGetV_1_t3648885239 * value)
	{
		___U3CU3Ef__amU24cache4_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache4_4, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_5() { return static_cast<int32_t>(offsetof(BehaviourUtilGenerated_t715901456_StaticFields, ___U3CU3Ef__amU24cache5_5)); }
	inline DGetV_1_t952362796 * get_U3CU3Ef__amU24cache5_5() const { return ___U3CU3Ef__amU24cache5_5; }
	inline DGetV_1_t952362796 ** get_address_of_U3CU3Ef__amU24cache5_5() { return &___U3CU3Ef__amU24cache5_5; }
	inline void set_U3CU3Ef__amU24cache5_5(DGetV_1_t952362796 * value)
	{
		___U3CU3Ef__amU24cache5_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache5_5, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_6() { return static_cast<int32_t>(offsetof(BehaviourUtilGenerated_t715901456_StaticFields, ___U3CU3Ef__amU24cache6_6)); }
	inline DGetV_1_t952362795 * get_U3CU3Ef__amU24cache6_6() const { return ___U3CU3Ef__amU24cache6_6; }
	inline DGetV_1_t952362795 ** get_address_of_U3CU3Ef__amU24cache6_6() { return &___U3CU3Ef__amU24cache6_6; }
	inline void set_U3CU3Ef__amU24cache6_6(DGetV_1_t952362795 * value)
	{
		___U3CU3Ef__amU24cache6_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_6, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_7() { return static_cast<int32_t>(offsetof(BehaviourUtilGenerated_t715901456_StaticFields, ___U3CU3Ef__amU24cache7_7)); }
	inline DGetV_1_t952362794 * get_U3CU3Ef__amU24cache7_7() const { return ___U3CU3Ef__amU24cache7_7; }
	inline DGetV_1_t952362794 ** get_address_of_U3CU3Ef__amU24cache7_7() { return &___U3CU3Ef__amU24cache7_7; }
	inline void set_U3CU3Ef__amU24cache7_7(DGetV_1_t952362794 * value)
	{
		___U3CU3Ef__amU24cache7_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_7, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_8() { return static_cast<int32_t>(offsetof(BehaviourUtilGenerated_t715901456_StaticFields, ___U3CU3Ef__amU24cache8_8)); }
	inline DGetV_1_t952362793 * get_U3CU3Ef__amU24cache8_8() const { return ___U3CU3Ef__amU24cache8_8; }
	inline DGetV_1_t952362793 ** get_address_of_U3CU3Ef__amU24cache8_8() { return &___U3CU3Ef__amU24cache8_8; }
	inline void set_U3CU3Ef__amU24cache8_8(DGetV_1_t952362793 * value)
	{
		___U3CU3Ef__amU24cache8_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_9() { return static_cast<int32_t>(offsetof(BehaviourUtilGenerated_t715901456_StaticFields, ___U3CU3Ef__amU24cache9_9)); }
	inline DGetV_1_t952362792 * get_U3CU3Ef__amU24cache9_9() const { return ___U3CU3Ef__amU24cache9_9; }
	inline DGetV_1_t952362792 ** get_address_of_U3CU3Ef__amU24cache9_9() { return &___U3CU3Ef__amU24cache9_9; }
	inline void set_U3CU3Ef__amU24cache9_9(DGetV_1_t952362792 * value)
	{
		___U3CU3Ef__amU24cache9_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
