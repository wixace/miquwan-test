﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Serialization.AstarSerializer/<DeserializeExtraInfo>c__AnonStorey111
struct U3CDeserializeExtraInfoU3Ec__AnonStorey111_t3805251455;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.Serialization.AstarSerializer/<DeserializeExtraInfo>c__AnonStorey111::.ctor()
extern "C"  void U3CDeserializeExtraInfoU3Ec__AnonStorey111__ctor_m3481263612 (U3CDeserializeExtraInfoU3Ec__AnonStorey111_t3805251455 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Serialization.AstarSerializer/<DeserializeExtraInfo>c__AnonStorey111::<>m__344(Pathfinding.GraphNode)
extern "C"  bool U3CDeserializeExtraInfoU3Ec__AnonStorey111_U3CU3Em__344_m2817664712 (U3CDeserializeExtraInfoU3Ec__AnonStorey111_t3805251455 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
