﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AppeggView
struct AppeggView_t3951466281;
// System.String
struct String_t;
// UnityEngine.Events.UnityAction
struct UnityAction_t594794173;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Events_UnityAction594794173.h"

// System.Void AppeggView::.ctor()
extern "C"  void AppeggView__ctor_m3095527314 (AppeggView_t3951466281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppeggView::Init()
extern "C"  void AppeggView_Init_m1436865666 (AppeggView_t3951466281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppeggView::Hide()
extern "C"  void AppeggView_Hide_m1403455540 (AppeggView_t3951466281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppeggView::Show()
extern "C"  void AppeggView_Show_m1717797679 (AppeggView_t3951466281 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppeggView::SetPrimaryText(System.String)
extern "C"  void AppeggView_SetPrimaryText_m684124643 (AppeggView_t3951466281 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppeggView::SetSecondaryText(System.String)
extern "C"  void AppeggView_SetSecondaryText_m3485517041 (AppeggView_t3951466281 * __this, String_t* ___str0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppeggView::SetPrimaryButtonListener(UnityEngine.Events.UnityAction)
extern "C"  void AppeggView_SetPrimaryButtonListener_m971318315 (AppeggView_t3951466281 * __this, UnityAction_t594794173 * ___act0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AppeggView::SetSecondaryButtonListener(UnityEngine.Events.UnityAction)
extern "C"  void AppeggView_SetSecondaryButtonListener_m4228436061 (AppeggView_t3951466281 * __this, UnityAction_t594794173 * ___act0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
