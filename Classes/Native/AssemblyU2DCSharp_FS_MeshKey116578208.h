﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Material
struct Material_t3870600107;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FS_MeshKey
struct  FS_MeshKey_t116578208  : public Il2CppObject
{
public:
	// System.Boolean FS_MeshKey::isStatic
	bool ___isStatic_0;
	// UnityEngine.Material FS_MeshKey::mat
	Material_t3870600107 * ___mat_1;

public:
	inline static int32_t get_offset_of_isStatic_0() { return static_cast<int32_t>(offsetof(FS_MeshKey_t116578208, ___isStatic_0)); }
	inline bool get_isStatic_0() const { return ___isStatic_0; }
	inline bool* get_address_of_isStatic_0() { return &___isStatic_0; }
	inline void set_isStatic_0(bool value)
	{
		___isStatic_0 = value;
	}

	inline static int32_t get_offset_of_mat_1() { return static_cast<int32_t>(offsetof(FS_MeshKey_t116578208, ___mat_1)); }
	inline Material_t3870600107 * get_mat_1() const { return ___mat_1; }
	inline Material_t3870600107 ** get_address_of_mat_1() { return &___mat_1; }
	inline void set_mat_1(Material_t3870600107 * value)
	{
		___mat_1 = value;
		Il2CppCodeGenWriteBarrier(&___mat_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
