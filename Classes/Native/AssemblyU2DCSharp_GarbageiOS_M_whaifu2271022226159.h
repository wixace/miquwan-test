﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_whaifu227
struct  M_whaifu227_t1022226159  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_whaifu227::_tifeFiwha
	uint32_t ____tifeFiwha_0;
	// System.Int32 GarbageiOS.M_whaifu227::_teanehere
	int32_t ____teanehere_1;
	// System.UInt32 GarbageiOS.M_whaifu227::_liqeene
	uint32_t ____liqeene_2;
	// System.UInt32 GarbageiOS.M_whaifu227::_dinasjee
	uint32_t ____dinasjee_3;
	// System.Single GarbageiOS.M_whaifu227::_wester
	float ____wester_4;
	// System.UInt32 GarbageiOS.M_whaifu227::_roozarlair
	uint32_t ____roozarlair_5;
	// System.Single GarbageiOS.M_whaifu227::_deakalte
	float ____deakalte_6;
	// System.Boolean GarbageiOS.M_whaifu227::_cucePemal
	bool ____cucePemal_7;
	// System.Single GarbageiOS.M_whaifu227::_zoqo
	float ____zoqo_8;
	// System.Int32 GarbageiOS.M_whaifu227::_taipalfer
	int32_t ____taipalfer_9;
	// System.UInt32 GarbageiOS.M_whaifu227::_jeardisdouFairor
	uint32_t ____jeardisdouFairor_10;
	// System.Int32 GarbageiOS.M_whaifu227::_seseno
	int32_t ____seseno_11;
	// System.Single GarbageiOS.M_whaifu227::_bawstai
	float ____bawstai_12;
	// System.Single GarbageiOS.M_whaifu227::_heregisjayMijallsa
	float ____heregisjayMijallsa_13;
	// System.Single GarbageiOS.M_whaifu227::_terqaw
	float ____terqaw_14;
	// System.Single GarbageiOS.M_whaifu227::_tedayForbiszee
	float ____tedayForbiszee_15;
	// System.Int32 GarbageiOS.M_whaifu227::_pairheretre
	int32_t ____pairheretre_16;
	// System.UInt32 GarbageiOS.M_whaifu227::_trori
	uint32_t ____trori_17;
	// System.String GarbageiOS.M_whaifu227::_stearralcisCijer
	String_t* ____stearralcisCijer_18;
	// System.Int32 GarbageiOS.M_whaifu227::_yealasPisha
	int32_t ____yealasPisha_19;
	// System.Int32 GarbageiOS.M_whaifu227::_wouqe
	int32_t ____wouqe_20;
	// System.Boolean GarbageiOS.M_whaifu227::_sairmi
	bool ____sairmi_21;

public:
	inline static int32_t get_offset_of__tifeFiwha_0() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____tifeFiwha_0)); }
	inline uint32_t get__tifeFiwha_0() const { return ____tifeFiwha_0; }
	inline uint32_t* get_address_of__tifeFiwha_0() { return &____tifeFiwha_0; }
	inline void set__tifeFiwha_0(uint32_t value)
	{
		____tifeFiwha_0 = value;
	}

	inline static int32_t get_offset_of__teanehere_1() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____teanehere_1)); }
	inline int32_t get__teanehere_1() const { return ____teanehere_1; }
	inline int32_t* get_address_of__teanehere_1() { return &____teanehere_1; }
	inline void set__teanehere_1(int32_t value)
	{
		____teanehere_1 = value;
	}

	inline static int32_t get_offset_of__liqeene_2() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____liqeene_2)); }
	inline uint32_t get__liqeene_2() const { return ____liqeene_2; }
	inline uint32_t* get_address_of__liqeene_2() { return &____liqeene_2; }
	inline void set__liqeene_2(uint32_t value)
	{
		____liqeene_2 = value;
	}

	inline static int32_t get_offset_of__dinasjee_3() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____dinasjee_3)); }
	inline uint32_t get__dinasjee_3() const { return ____dinasjee_3; }
	inline uint32_t* get_address_of__dinasjee_3() { return &____dinasjee_3; }
	inline void set__dinasjee_3(uint32_t value)
	{
		____dinasjee_3 = value;
	}

	inline static int32_t get_offset_of__wester_4() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____wester_4)); }
	inline float get__wester_4() const { return ____wester_4; }
	inline float* get_address_of__wester_4() { return &____wester_4; }
	inline void set__wester_4(float value)
	{
		____wester_4 = value;
	}

	inline static int32_t get_offset_of__roozarlair_5() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____roozarlair_5)); }
	inline uint32_t get__roozarlair_5() const { return ____roozarlair_5; }
	inline uint32_t* get_address_of__roozarlair_5() { return &____roozarlair_5; }
	inline void set__roozarlair_5(uint32_t value)
	{
		____roozarlair_5 = value;
	}

	inline static int32_t get_offset_of__deakalte_6() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____deakalte_6)); }
	inline float get__deakalte_6() const { return ____deakalte_6; }
	inline float* get_address_of__deakalte_6() { return &____deakalte_6; }
	inline void set__deakalte_6(float value)
	{
		____deakalte_6 = value;
	}

	inline static int32_t get_offset_of__cucePemal_7() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____cucePemal_7)); }
	inline bool get__cucePemal_7() const { return ____cucePemal_7; }
	inline bool* get_address_of__cucePemal_7() { return &____cucePemal_7; }
	inline void set__cucePemal_7(bool value)
	{
		____cucePemal_7 = value;
	}

	inline static int32_t get_offset_of__zoqo_8() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____zoqo_8)); }
	inline float get__zoqo_8() const { return ____zoqo_8; }
	inline float* get_address_of__zoqo_8() { return &____zoqo_8; }
	inline void set__zoqo_8(float value)
	{
		____zoqo_8 = value;
	}

	inline static int32_t get_offset_of__taipalfer_9() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____taipalfer_9)); }
	inline int32_t get__taipalfer_9() const { return ____taipalfer_9; }
	inline int32_t* get_address_of__taipalfer_9() { return &____taipalfer_9; }
	inline void set__taipalfer_9(int32_t value)
	{
		____taipalfer_9 = value;
	}

	inline static int32_t get_offset_of__jeardisdouFairor_10() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____jeardisdouFairor_10)); }
	inline uint32_t get__jeardisdouFairor_10() const { return ____jeardisdouFairor_10; }
	inline uint32_t* get_address_of__jeardisdouFairor_10() { return &____jeardisdouFairor_10; }
	inline void set__jeardisdouFairor_10(uint32_t value)
	{
		____jeardisdouFairor_10 = value;
	}

	inline static int32_t get_offset_of__seseno_11() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____seseno_11)); }
	inline int32_t get__seseno_11() const { return ____seseno_11; }
	inline int32_t* get_address_of__seseno_11() { return &____seseno_11; }
	inline void set__seseno_11(int32_t value)
	{
		____seseno_11 = value;
	}

	inline static int32_t get_offset_of__bawstai_12() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____bawstai_12)); }
	inline float get__bawstai_12() const { return ____bawstai_12; }
	inline float* get_address_of__bawstai_12() { return &____bawstai_12; }
	inline void set__bawstai_12(float value)
	{
		____bawstai_12 = value;
	}

	inline static int32_t get_offset_of__heregisjayMijallsa_13() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____heregisjayMijallsa_13)); }
	inline float get__heregisjayMijallsa_13() const { return ____heregisjayMijallsa_13; }
	inline float* get_address_of__heregisjayMijallsa_13() { return &____heregisjayMijallsa_13; }
	inline void set__heregisjayMijallsa_13(float value)
	{
		____heregisjayMijallsa_13 = value;
	}

	inline static int32_t get_offset_of__terqaw_14() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____terqaw_14)); }
	inline float get__terqaw_14() const { return ____terqaw_14; }
	inline float* get_address_of__terqaw_14() { return &____terqaw_14; }
	inline void set__terqaw_14(float value)
	{
		____terqaw_14 = value;
	}

	inline static int32_t get_offset_of__tedayForbiszee_15() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____tedayForbiszee_15)); }
	inline float get__tedayForbiszee_15() const { return ____tedayForbiszee_15; }
	inline float* get_address_of__tedayForbiszee_15() { return &____tedayForbiszee_15; }
	inline void set__tedayForbiszee_15(float value)
	{
		____tedayForbiszee_15 = value;
	}

	inline static int32_t get_offset_of__pairheretre_16() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____pairheretre_16)); }
	inline int32_t get__pairheretre_16() const { return ____pairheretre_16; }
	inline int32_t* get_address_of__pairheretre_16() { return &____pairheretre_16; }
	inline void set__pairheretre_16(int32_t value)
	{
		____pairheretre_16 = value;
	}

	inline static int32_t get_offset_of__trori_17() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____trori_17)); }
	inline uint32_t get__trori_17() const { return ____trori_17; }
	inline uint32_t* get_address_of__trori_17() { return &____trori_17; }
	inline void set__trori_17(uint32_t value)
	{
		____trori_17 = value;
	}

	inline static int32_t get_offset_of__stearralcisCijer_18() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____stearralcisCijer_18)); }
	inline String_t* get__stearralcisCijer_18() const { return ____stearralcisCijer_18; }
	inline String_t** get_address_of__stearralcisCijer_18() { return &____stearralcisCijer_18; }
	inline void set__stearralcisCijer_18(String_t* value)
	{
		____stearralcisCijer_18 = value;
		Il2CppCodeGenWriteBarrier(&____stearralcisCijer_18, value);
	}

	inline static int32_t get_offset_of__yealasPisha_19() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____yealasPisha_19)); }
	inline int32_t get__yealasPisha_19() const { return ____yealasPisha_19; }
	inline int32_t* get_address_of__yealasPisha_19() { return &____yealasPisha_19; }
	inline void set__yealasPisha_19(int32_t value)
	{
		____yealasPisha_19 = value;
	}

	inline static int32_t get_offset_of__wouqe_20() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____wouqe_20)); }
	inline int32_t get__wouqe_20() const { return ____wouqe_20; }
	inline int32_t* get_address_of__wouqe_20() { return &____wouqe_20; }
	inline void set__wouqe_20(int32_t value)
	{
		____wouqe_20 = value;
	}

	inline static int32_t get_offset_of__sairmi_21() { return static_cast<int32_t>(offsetof(M_whaifu227_t1022226159, ____sairmi_21)); }
	inline bool get__sairmi_21() const { return ____sairmi_21; }
	inline bool* get_address_of__sairmi_21() { return &____sairmi_21; }
	inline void set__sairmi_21(bool value)
	{
		____sairmi_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
