﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// FPSCounter
struct FPSCounter_t1808341235;
// UILabel
struct UILabel_t291504320;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FPSCounter
struct  FPSCounter_t1808341235  : public MonoBehaviour_t667441552
{
public:
	// UILabel FPSCounter::fpsInfo_txt
	UILabel_t291504320 * ___fpsInfo_txt_3;
	// System.Single FPSCounter::updateInterval
	float ___updateInterval_4;
	// System.UInt32 FPSCounter::uframes
	uint32_t ___uframes_5;
	// System.Single FPSCounter::utimer
	float ___utimer_6;
	// System.UInt32 FPSCounter::ufps
	uint32_t ___ufps_7;
	// System.Single FPSCounter::ultimer
	float ___ultimer_8;
	// System.UInt32 FPSCounter::fframes
	uint32_t ___fframes_9;
	// System.Single FPSCounter::ftimer
	float ___ftimer_10;
	// System.UInt32 FPSCounter::ffps
	uint32_t ___ffps_11;
	// System.Single FPSCounter::fltimer
	float ___fltimer_12;
	// System.Single FPSCounter::qualityUFPS
	float ___qualityUFPS_13;
	// UnityEngine.Rect FPSCounter::size
	Rect_t4241904616  ___size_14;
	// System.Boolean FPSCounter::CanSettingGameQuality
	bool ___CanSettingGameQuality_15;
	// System.Int32 FPSCounter::qualityLevel
	int32_t ___qualityLevel_16;
	// System.String FPSCounter::qualityStr
	String_t* ___qualityStr_17;

public:
	inline static int32_t get_offset_of_fpsInfo_txt_3() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341235, ___fpsInfo_txt_3)); }
	inline UILabel_t291504320 * get_fpsInfo_txt_3() const { return ___fpsInfo_txt_3; }
	inline UILabel_t291504320 ** get_address_of_fpsInfo_txt_3() { return &___fpsInfo_txt_3; }
	inline void set_fpsInfo_txt_3(UILabel_t291504320 * value)
	{
		___fpsInfo_txt_3 = value;
		Il2CppCodeGenWriteBarrier(&___fpsInfo_txt_3, value);
	}

	inline static int32_t get_offset_of_updateInterval_4() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341235, ___updateInterval_4)); }
	inline float get_updateInterval_4() const { return ___updateInterval_4; }
	inline float* get_address_of_updateInterval_4() { return &___updateInterval_4; }
	inline void set_updateInterval_4(float value)
	{
		___updateInterval_4 = value;
	}

	inline static int32_t get_offset_of_uframes_5() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341235, ___uframes_5)); }
	inline uint32_t get_uframes_5() const { return ___uframes_5; }
	inline uint32_t* get_address_of_uframes_5() { return &___uframes_5; }
	inline void set_uframes_5(uint32_t value)
	{
		___uframes_5 = value;
	}

	inline static int32_t get_offset_of_utimer_6() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341235, ___utimer_6)); }
	inline float get_utimer_6() const { return ___utimer_6; }
	inline float* get_address_of_utimer_6() { return &___utimer_6; }
	inline void set_utimer_6(float value)
	{
		___utimer_6 = value;
	}

	inline static int32_t get_offset_of_ufps_7() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341235, ___ufps_7)); }
	inline uint32_t get_ufps_7() const { return ___ufps_7; }
	inline uint32_t* get_address_of_ufps_7() { return &___ufps_7; }
	inline void set_ufps_7(uint32_t value)
	{
		___ufps_7 = value;
	}

	inline static int32_t get_offset_of_ultimer_8() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341235, ___ultimer_8)); }
	inline float get_ultimer_8() const { return ___ultimer_8; }
	inline float* get_address_of_ultimer_8() { return &___ultimer_8; }
	inline void set_ultimer_8(float value)
	{
		___ultimer_8 = value;
	}

	inline static int32_t get_offset_of_fframes_9() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341235, ___fframes_9)); }
	inline uint32_t get_fframes_9() const { return ___fframes_9; }
	inline uint32_t* get_address_of_fframes_9() { return &___fframes_9; }
	inline void set_fframes_9(uint32_t value)
	{
		___fframes_9 = value;
	}

	inline static int32_t get_offset_of_ftimer_10() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341235, ___ftimer_10)); }
	inline float get_ftimer_10() const { return ___ftimer_10; }
	inline float* get_address_of_ftimer_10() { return &___ftimer_10; }
	inline void set_ftimer_10(float value)
	{
		___ftimer_10 = value;
	}

	inline static int32_t get_offset_of_ffps_11() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341235, ___ffps_11)); }
	inline uint32_t get_ffps_11() const { return ___ffps_11; }
	inline uint32_t* get_address_of_ffps_11() { return &___ffps_11; }
	inline void set_ffps_11(uint32_t value)
	{
		___ffps_11 = value;
	}

	inline static int32_t get_offset_of_fltimer_12() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341235, ___fltimer_12)); }
	inline float get_fltimer_12() const { return ___fltimer_12; }
	inline float* get_address_of_fltimer_12() { return &___fltimer_12; }
	inline void set_fltimer_12(float value)
	{
		___fltimer_12 = value;
	}

	inline static int32_t get_offset_of_qualityUFPS_13() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341235, ___qualityUFPS_13)); }
	inline float get_qualityUFPS_13() const { return ___qualityUFPS_13; }
	inline float* get_address_of_qualityUFPS_13() { return &___qualityUFPS_13; }
	inline void set_qualityUFPS_13(float value)
	{
		___qualityUFPS_13 = value;
	}

	inline static int32_t get_offset_of_size_14() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341235, ___size_14)); }
	inline Rect_t4241904616  get_size_14() const { return ___size_14; }
	inline Rect_t4241904616 * get_address_of_size_14() { return &___size_14; }
	inline void set_size_14(Rect_t4241904616  value)
	{
		___size_14 = value;
	}

	inline static int32_t get_offset_of_CanSettingGameQuality_15() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341235, ___CanSettingGameQuality_15)); }
	inline bool get_CanSettingGameQuality_15() const { return ___CanSettingGameQuality_15; }
	inline bool* get_address_of_CanSettingGameQuality_15() { return &___CanSettingGameQuality_15; }
	inline void set_CanSettingGameQuality_15(bool value)
	{
		___CanSettingGameQuality_15 = value;
	}

	inline static int32_t get_offset_of_qualityLevel_16() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341235, ___qualityLevel_16)); }
	inline int32_t get_qualityLevel_16() const { return ___qualityLevel_16; }
	inline int32_t* get_address_of_qualityLevel_16() { return &___qualityLevel_16; }
	inline void set_qualityLevel_16(int32_t value)
	{
		___qualityLevel_16 = value;
	}

	inline static int32_t get_offset_of_qualityStr_17() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341235, ___qualityStr_17)); }
	inline String_t* get_qualityStr_17() const { return ___qualityStr_17; }
	inline String_t** get_address_of_qualityStr_17() { return &___qualityStr_17; }
	inline void set_qualityStr_17(String_t* value)
	{
		___qualityStr_17 = value;
		Il2CppCodeGenWriteBarrier(&___qualityStr_17, value);
	}
};

struct FPSCounter_t1808341235_StaticFields
{
public:
	// FPSCounter FPSCounter::_fps
	FPSCounter_t1808341235 * ____fps_2;

public:
	inline static int32_t get_offset_of__fps_2() { return static_cast<int32_t>(offsetof(FPSCounter_t1808341235_StaticFields, ____fps_2)); }
	inline FPSCounter_t1808341235 * get__fps_2() const { return ____fps_2; }
	inline FPSCounter_t1808341235 ** get_address_of__fps_2() { return &____fps_2; }
	inline void set__fps_2(FPSCounter_t1808341235 * value)
	{
		____fps_2 = value;
		Il2CppCodeGenWriteBarrier(&____fps_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
