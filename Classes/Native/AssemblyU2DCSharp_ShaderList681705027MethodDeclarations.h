﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ShaderList
struct ShaderList_t681705027;

#include "codegen/il2cpp-codegen.h"

// System.Void ShaderList::.ctor()
extern "C"  void ShaderList__ctor_m1707079096 (ShaderList_t681705027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
