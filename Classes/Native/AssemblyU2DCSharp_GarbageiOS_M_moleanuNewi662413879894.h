﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_moleanuNewi66
struct  M_moleanuNewi66_t2413879894  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_moleanuNewi66::_teevi
	bool ____teevi_0;
	// System.Boolean GarbageiOS.M_moleanuNewi66::_semeapaw
	bool ____semeapaw_1;
	// System.String GarbageiOS.M_moleanuNewi66::_hasxai
	String_t* ____hasxai_2;
	// System.Single GarbageiOS.M_moleanuNewi66::_werdawiDowca
	float ____werdawiDowca_3;
	// System.Boolean GarbageiOS.M_moleanuNewi66::_sejeeRardice
	bool ____sejeeRardice_4;
	// System.Boolean GarbageiOS.M_moleanuNewi66::_qalllelsereTisdrootoo
	bool ____qalllelsereTisdrootoo_5;
	// System.Boolean GarbageiOS.M_moleanuNewi66::_hemasbemCougicu
	bool ____hemasbemCougicu_6;
	// System.Single GarbageiOS.M_moleanuNewi66::_caircewhea
	float ____caircewhea_7;
	// System.Single GarbageiOS.M_moleanuNewi66::_wawili
	float ____wawili_8;
	// System.Single GarbageiOS.M_moleanuNewi66::_mupear
	float ____mupear_9;
	// System.String GarbageiOS.M_moleanuNewi66::_lecou
	String_t* ____lecou_10;
	// System.UInt32 GarbageiOS.M_moleanuNewi66::_drallworpayCutearjo
	uint32_t ____drallworpayCutearjo_11;
	// System.Int32 GarbageiOS.M_moleanuNewi66::_jouzouqaSawge
	int32_t ____jouzouqaSawge_12;
	// System.Int32 GarbageiOS.M_moleanuNewi66::_jerlear
	int32_t ____jerlear_13;
	// System.UInt32 GarbageiOS.M_moleanuNewi66::_rooba
	uint32_t ____rooba_14;
	// System.Single GarbageiOS.M_moleanuNewi66::_hekemporRekow
	float ____hekemporRekow_15;

public:
	inline static int32_t get_offset_of__teevi_0() { return static_cast<int32_t>(offsetof(M_moleanuNewi66_t2413879894, ____teevi_0)); }
	inline bool get__teevi_0() const { return ____teevi_0; }
	inline bool* get_address_of__teevi_0() { return &____teevi_0; }
	inline void set__teevi_0(bool value)
	{
		____teevi_0 = value;
	}

	inline static int32_t get_offset_of__semeapaw_1() { return static_cast<int32_t>(offsetof(M_moleanuNewi66_t2413879894, ____semeapaw_1)); }
	inline bool get__semeapaw_1() const { return ____semeapaw_1; }
	inline bool* get_address_of__semeapaw_1() { return &____semeapaw_1; }
	inline void set__semeapaw_1(bool value)
	{
		____semeapaw_1 = value;
	}

	inline static int32_t get_offset_of__hasxai_2() { return static_cast<int32_t>(offsetof(M_moleanuNewi66_t2413879894, ____hasxai_2)); }
	inline String_t* get__hasxai_2() const { return ____hasxai_2; }
	inline String_t** get_address_of__hasxai_2() { return &____hasxai_2; }
	inline void set__hasxai_2(String_t* value)
	{
		____hasxai_2 = value;
		Il2CppCodeGenWriteBarrier(&____hasxai_2, value);
	}

	inline static int32_t get_offset_of__werdawiDowca_3() { return static_cast<int32_t>(offsetof(M_moleanuNewi66_t2413879894, ____werdawiDowca_3)); }
	inline float get__werdawiDowca_3() const { return ____werdawiDowca_3; }
	inline float* get_address_of__werdawiDowca_3() { return &____werdawiDowca_3; }
	inline void set__werdawiDowca_3(float value)
	{
		____werdawiDowca_3 = value;
	}

	inline static int32_t get_offset_of__sejeeRardice_4() { return static_cast<int32_t>(offsetof(M_moleanuNewi66_t2413879894, ____sejeeRardice_4)); }
	inline bool get__sejeeRardice_4() const { return ____sejeeRardice_4; }
	inline bool* get_address_of__sejeeRardice_4() { return &____sejeeRardice_4; }
	inline void set__sejeeRardice_4(bool value)
	{
		____sejeeRardice_4 = value;
	}

	inline static int32_t get_offset_of__qalllelsereTisdrootoo_5() { return static_cast<int32_t>(offsetof(M_moleanuNewi66_t2413879894, ____qalllelsereTisdrootoo_5)); }
	inline bool get__qalllelsereTisdrootoo_5() const { return ____qalllelsereTisdrootoo_5; }
	inline bool* get_address_of__qalllelsereTisdrootoo_5() { return &____qalllelsereTisdrootoo_5; }
	inline void set__qalllelsereTisdrootoo_5(bool value)
	{
		____qalllelsereTisdrootoo_5 = value;
	}

	inline static int32_t get_offset_of__hemasbemCougicu_6() { return static_cast<int32_t>(offsetof(M_moleanuNewi66_t2413879894, ____hemasbemCougicu_6)); }
	inline bool get__hemasbemCougicu_6() const { return ____hemasbemCougicu_6; }
	inline bool* get_address_of__hemasbemCougicu_6() { return &____hemasbemCougicu_6; }
	inline void set__hemasbemCougicu_6(bool value)
	{
		____hemasbemCougicu_6 = value;
	}

	inline static int32_t get_offset_of__caircewhea_7() { return static_cast<int32_t>(offsetof(M_moleanuNewi66_t2413879894, ____caircewhea_7)); }
	inline float get__caircewhea_7() const { return ____caircewhea_7; }
	inline float* get_address_of__caircewhea_7() { return &____caircewhea_7; }
	inline void set__caircewhea_7(float value)
	{
		____caircewhea_7 = value;
	}

	inline static int32_t get_offset_of__wawili_8() { return static_cast<int32_t>(offsetof(M_moleanuNewi66_t2413879894, ____wawili_8)); }
	inline float get__wawili_8() const { return ____wawili_8; }
	inline float* get_address_of__wawili_8() { return &____wawili_8; }
	inline void set__wawili_8(float value)
	{
		____wawili_8 = value;
	}

	inline static int32_t get_offset_of__mupear_9() { return static_cast<int32_t>(offsetof(M_moleanuNewi66_t2413879894, ____mupear_9)); }
	inline float get__mupear_9() const { return ____mupear_9; }
	inline float* get_address_of__mupear_9() { return &____mupear_9; }
	inline void set__mupear_9(float value)
	{
		____mupear_9 = value;
	}

	inline static int32_t get_offset_of__lecou_10() { return static_cast<int32_t>(offsetof(M_moleanuNewi66_t2413879894, ____lecou_10)); }
	inline String_t* get__lecou_10() const { return ____lecou_10; }
	inline String_t** get_address_of__lecou_10() { return &____lecou_10; }
	inline void set__lecou_10(String_t* value)
	{
		____lecou_10 = value;
		Il2CppCodeGenWriteBarrier(&____lecou_10, value);
	}

	inline static int32_t get_offset_of__drallworpayCutearjo_11() { return static_cast<int32_t>(offsetof(M_moleanuNewi66_t2413879894, ____drallworpayCutearjo_11)); }
	inline uint32_t get__drallworpayCutearjo_11() const { return ____drallworpayCutearjo_11; }
	inline uint32_t* get_address_of__drallworpayCutearjo_11() { return &____drallworpayCutearjo_11; }
	inline void set__drallworpayCutearjo_11(uint32_t value)
	{
		____drallworpayCutearjo_11 = value;
	}

	inline static int32_t get_offset_of__jouzouqaSawge_12() { return static_cast<int32_t>(offsetof(M_moleanuNewi66_t2413879894, ____jouzouqaSawge_12)); }
	inline int32_t get__jouzouqaSawge_12() const { return ____jouzouqaSawge_12; }
	inline int32_t* get_address_of__jouzouqaSawge_12() { return &____jouzouqaSawge_12; }
	inline void set__jouzouqaSawge_12(int32_t value)
	{
		____jouzouqaSawge_12 = value;
	}

	inline static int32_t get_offset_of__jerlear_13() { return static_cast<int32_t>(offsetof(M_moleanuNewi66_t2413879894, ____jerlear_13)); }
	inline int32_t get__jerlear_13() const { return ____jerlear_13; }
	inline int32_t* get_address_of__jerlear_13() { return &____jerlear_13; }
	inline void set__jerlear_13(int32_t value)
	{
		____jerlear_13 = value;
	}

	inline static int32_t get_offset_of__rooba_14() { return static_cast<int32_t>(offsetof(M_moleanuNewi66_t2413879894, ____rooba_14)); }
	inline uint32_t get__rooba_14() const { return ____rooba_14; }
	inline uint32_t* get_address_of__rooba_14() { return &____rooba_14; }
	inline void set__rooba_14(uint32_t value)
	{
		____rooba_14 = value;
	}

	inline static int32_t get_offset_of__hekemporRekow_15() { return static_cast<int32_t>(offsetof(M_moleanuNewi66_t2413879894, ____hekemporRekow_15)); }
	inline float get__hekemporRekow_15() const { return ____hekemporRekow_15; }
	inline float* get_address_of__hekemporRekow_15() { return &____hekemporRekow_15; }
	inline void set__hekemporRekow_15(float value)
	{
		____hekemporRekow_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
