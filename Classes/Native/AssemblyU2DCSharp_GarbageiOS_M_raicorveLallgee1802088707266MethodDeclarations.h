﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_raicorveLallgee180
struct M_raicorveLallgee180_t2088707266;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_raicorveLallgee1802088707266.h"

// System.Void GarbageiOS.M_raicorveLallgee180::.ctor()
extern "C"  void M_raicorveLallgee180__ctor_m559348145 (M_raicorveLallgee180_t2088707266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_raicorveLallgee180::M_norhelBoutoo0(System.String[],System.Int32)
extern "C"  void M_raicorveLallgee180_M_norhelBoutoo0_m2954687322 (M_raicorveLallgee180_t2088707266 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_raicorveLallgee180::M_qeesilouJanisdris1(System.String[],System.Int32)
extern "C"  void M_raicorveLallgee180_M_qeesilouJanisdris1_m776415743 (M_raicorveLallgee180_t2088707266 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_raicorveLallgee180::M_virazer2(System.String[],System.Int32)
extern "C"  void M_raicorveLallgee180_M_virazer2_m3169307017 (M_raicorveLallgee180_t2088707266 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_raicorveLallgee180::ilo_M_qeesilouJanisdris11(GarbageiOS.M_raicorveLallgee180,System.String[],System.Int32)
extern "C"  void M_raicorveLallgee180_ilo_M_qeesilouJanisdris11_m3774878077 (Il2CppObject * __this /* static, unused */, M_raicorveLallgee180_t2088707266 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
