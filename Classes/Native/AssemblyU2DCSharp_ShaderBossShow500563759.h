﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Shader
struct Shader_t3191267369;
// UnityEngine.Material
struct Material_t3870600107;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ShaderBossShow
struct  ShaderBossShow_t500563759  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Shader ShaderBossShow::curShader
	Shader_t3191267369 * ___curShader_2;
	// System.Single ShaderBossShow::grayScaleAmount
	float ___grayScaleAmount_3;
	// UnityEngine.Material ShaderBossShow::curMaterial
	Material_t3870600107 * ___curMaterial_4;

public:
	inline static int32_t get_offset_of_curShader_2() { return static_cast<int32_t>(offsetof(ShaderBossShow_t500563759, ___curShader_2)); }
	inline Shader_t3191267369 * get_curShader_2() const { return ___curShader_2; }
	inline Shader_t3191267369 ** get_address_of_curShader_2() { return &___curShader_2; }
	inline void set_curShader_2(Shader_t3191267369 * value)
	{
		___curShader_2 = value;
		Il2CppCodeGenWriteBarrier(&___curShader_2, value);
	}

	inline static int32_t get_offset_of_grayScaleAmount_3() { return static_cast<int32_t>(offsetof(ShaderBossShow_t500563759, ___grayScaleAmount_3)); }
	inline float get_grayScaleAmount_3() const { return ___grayScaleAmount_3; }
	inline float* get_address_of_grayScaleAmount_3() { return &___grayScaleAmount_3; }
	inline void set_grayScaleAmount_3(float value)
	{
		___grayScaleAmount_3 = value;
	}

	inline static int32_t get_offset_of_curMaterial_4() { return static_cast<int32_t>(offsetof(ShaderBossShow_t500563759, ___curMaterial_4)); }
	inline Material_t3870600107 * get_curMaterial_4() const { return ___curMaterial_4; }
	inline Material_t3870600107 ** get_address_of_curMaterial_4() { return &___curMaterial_4; }
	inline void set_curMaterial_4(Material_t3870600107 * value)
	{
		___curMaterial_4 = value;
		Il2CppCodeGenWriteBarrier(&___curMaterial_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
