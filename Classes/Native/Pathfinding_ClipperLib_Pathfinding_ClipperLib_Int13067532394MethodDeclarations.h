﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// Pathfinding.ClipperLib.Int128
struct Int128_t3067532394;
struct Int128_t3067532394_marshaled_pinvoke;
struct Int128_t3067532394_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_Int13067532394.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Pathfinding.ClipperLib.Int128::.ctor(System.Int64)
extern "C"  void Int128__ctor_m3350950229 (Int128_t3067532394 * __this, int64_t ____lo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ClipperLib.Int128::.ctor(System.Int64,System.UInt64)
extern "C"  void Int128__ctor_m82640458 (Int128_t3067532394 * __this, int64_t ____hi0, uint64_t ____lo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Int128::Equals(System.Object)
extern "C"  bool Int128_Equals_m177562640 (Int128_t3067532394 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Pathfinding.ClipperLib.Int128::GetHashCode()
extern "C"  int32_t Int128_GetHashCode_m551020328 (Int128_t3067532394 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ClipperLib.Int128 Pathfinding.ClipperLib.Int128::Int128Mul(System.Int64,System.Int64)
extern "C"  Int128_t3067532394  Int128_Int128Mul_m44987946 (Il2CppObject * __this /* static, unused */, int64_t ___lhs0, int64_t ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Int128::op_Equality(Pathfinding.ClipperLib.Int128,Pathfinding.ClipperLib.Int128)
extern "C"  bool Int128_op_Equality_m3976304897 (Il2CppObject * __this /* static, unused */, Int128_t3067532394  ___val10, Int128_t3067532394  ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Int128::op_GreaterThan(Pathfinding.ClipperLib.Int128,Pathfinding.ClipperLib.Int128)
extern "C"  bool Int128_op_GreaterThan_m2422758740 (Il2CppObject * __this /* static, unused */, Int128_t3067532394  ___val10, Int128_t3067532394  ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ClipperLib.Int128::op_LessThan(Pathfinding.ClipperLib.Int128,Pathfinding.ClipperLib.Int128)
extern "C"  bool Int128_op_LessThan_m3057609505 (Il2CppObject * __this /* static, unused */, Int128_t3067532394  ___val10, Int128_t3067532394  ___val21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ClipperLib.Int128 Pathfinding.ClipperLib.Int128::op_Addition(Pathfinding.ClipperLib.Int128,Pathfinding.ClipperLib.Int128)
extern "C"  Int128_t3067532394  Int128_op_Addition_m2329452092 (Il2CppObject * __this /* static, unused */, Int128_t3067532394  ___lhs0, Int128_t3067532394  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ClipperLib.Int128 Pathfinding.ClipperLib.Int128::op_Subtraction(Pathfinding.ClipperLib.Int128,Pathfinding.ClipperLib.Int128)
extern "C"  Int128_t3067532394  Int128_op_Subtraction_m2448771870 (Il2CppObject * __this /* static, unused */, Int128_t3067532394  ___lhs0, Int128_t3067532394  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ClipperLib.Int128 Pathfinding.ClipperLib.Int128::op_UnaryNegation(Pathfinding.ClipperLib.Int128)
extern "C"  Int128_t3067532394  Int128_op_UnaryNegation_m2803597574 (Il2CppObject * __this /* static, unused */, Int128_t3067532394  ___val0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ClipperLib.Int128 Pathfinding.ClipperLib.Int128::op_Division(Pathfinding.ClipperLib.Int128,Pathfinding.ClipperLib.Int128)
extern "C"  Int128_t3067532394  Int128_op_Division_m2463004203 (Il2CppObject * __this /* static, unused */, Int128_t3067532394  ___lhs0, Int128_t3067532394  ___rhs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct Int128_t3067532394;
struct Int128_t3067532394_marshaled_pinvoke;

extern "C" void Int128_t3067532394_marshal_pinvoke(const Int128_t3067532394& unmarshaled, Int128_t3067532394_marshaled_pinvoke& marshaled);
extern "C" void Int128_t3067532394_marshal_pinvoke_back(const Int128_t3067532394_marshaled_pinvoke& marshaled, Int128_t3067532394& unmarshaled);
extern "C" void Int128_t3067532394_marshal_pinvoke_cleanup(Int128_t3067532394_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct Int128_t3067532394;
struct Int128_t3067532394_marshaled_com;

extern "C" void Int128_t3067532394_marshal_com(const Int128_t3067532394& unmarshaled, Int128_t3067532394_marshaled_com& marshaled);
extern "C" void Int128_t3067532394_marshal_com_back(const Int128_t3067532394_marshaled_com& marshaled, Int128_t3067532394& unmarshaled);
extern "C" void Int128_t3067532394_marshal_com_cleanup(Int128_t3067532394_marshaled_com& marshaled);
