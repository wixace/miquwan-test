﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimedEffect
struct  TimedEffect_t1335807880  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean TimedEffect::isplay
	bool ___isplay_2;
	// System.Single TimedEffect::curtime
	float ___curtime_3;
	// System.Action TimedEffect::timeOutCall
	Action_t3771233898 * ___timeOutCall_4;
	// System.Single TimedEffect::<lasttime>k__BackingField
	float ___U3ClasttimeU3Ek__BackingField_5;
	// System.Single TimedEffect::<delaytime>k__BackingField
	float ___U3CdelaytimeU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_isplay_2() { return static_cast<int32_t>(offsetof(TimedEffect_t1335807880, ___isplay_2)); }
	inline bool get_isplay_2() const { return ___isplay_2; }
	inline bool* get_address_of_isplay_2() { return &___isplay_2; }
	inline void set_isplay_2(bool value)
	{
		___isplay_2 = value;
	}

	inline static int32_t get_offset_of_curtime_3() { return static_cast<int32_t>(offsetof(TimedEffect_t1335807880, ___curtime_3)); }
	inline float get_curtime_3() const { return ___curtime_3; }
	inline float* get_address_of_curtime_3() { return &___curtime_3; }
	inline void set_curtime_3(float value)
	{
		___curtime_3 = value;
	}

	inline static int32_t get_offset_of_timeOutCall_4() { return static_cast<int32_t>(offsetof(TimedEffect_t1335807880, ___timeOutCall_4)); }
	inline Action_t3771233898 * get_timeOutCall_4() const { return ___timeOutCall_4; }
	inline Action_t3771233898 ** get_address_of_timeOutCall_4() { return &___timeOutCall_4; }
	inline void set_timeOutCall_4(Action_t3771233898 * value)
	{
		___timeOutCall_4 = value;
		Il2CppCodeGenWriteBarrier(&___timeOutCall_4, value);
	}

	inline static int32_t get_offset_of_U3ClasttimeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(TimedEffect_t1335807880, ___U3ClasttimeU3Ek__BackingField_5)); }
	inline float get_U3ClasttimeU3Ek__BackingField_5() const { return ___U3ClasttimeU3Ek__BackingField_5; }
	inline float* get_address_of_U3ClasttimeU3Ek__BackingField_5() { return &___U3ClasttimeU3Ek__BackingField_5; }
	inline void set_U3ClasttimeU3Ek__BackingField_5(float value)
	{
		___U3ClasttimeU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CdelaytimeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(TimedEffect_t1335807880, ___U3CdelaytimeU3Ek__BackingField_6)); }
	inline float get_U3CdelaytimeU3Ek__BackingField_6() const { return ___U3CdelaytimeU3Ek__BackingField_6; }
	inline float* get_address_of_U3CdelaytimeU3Ek__BackingField_6() { return &___U3CdelaytimeU3Ek__BackingField_6; }
	inline void set_U3CdelaytimeU3Ek__BackingField_6(float value)
	{
		___U3CdelaytimeU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
