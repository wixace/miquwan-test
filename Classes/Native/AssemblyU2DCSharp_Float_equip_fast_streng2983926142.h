﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UILabel
struct UILabel_t291504320;
// UITexture
struct UITexture_t3903132647;
// UnityEngine.Transform
struct Transform_t1659122786;
// TweenPosition
struct TweenPosition_t3684358292;
// TweenScale
struct TweenScale_t2936666559;
// TweenAlpha
struct TweenAlpha_t2920325587;

#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Float_equip_fast_streng
struct  Float_equip_fast_streng_t2983926142  : public FloatTextUnit_t2362298029
{
public:
	// UnityEngine.GameObject Float_equip_fast_streng::_obj_master
	GameObject_t3674682005 * ____obj_master_3;
	// UnityEngine.GameObject Float_equip_fast_streng::_obj_streng
	GameObject_t3674682005 * ____obj_streng_4;
	// UILabel Float_equip_fast_streng::_label_master_tile
	UILabel_t291504320 * ____label_master_tile_5;
	// UILabel Float_equip_fast_streng::_label_master_left
	UILabel_t291504320 * ____label_master_left_6;
	// UILabel Float_equip_fast_streng::_label_master_right
	UILabel_t291504320 * ____label_master_right_7;
	// UILabel Float_equip_fast_streng::_label_value_left
	UILabel_t291504320 * ____label_value_left_8;
	// UILabel Float_equip_fast_streng::_label_value_right
	UILabel_t291504320 * ____label_value_right_9;
	// UILabel Float_equip_fast_streng::Label_addCrit
	UILabel_t291504320 * ___Label_addCrit_10;
	// UILabel Float_equip_fast_streng::Label_costCopper
	UILabel_t291504320 * ___Label_costCopper_11;
	// UITexture Float_equip_fast_streng::_sprite_bg
	UITexture_t3903132647 * ____sprite_bg_12;
	// UnityEngine.Transform Float_equip_fast_streng::effectArr
	Transform_t1659122786 * ___effectArr_13;
	// UnityEngine.Vector3 Float_equip_fast_streng::AttBeforPos
	Vector3_t4282066566  ___AttBeforPos_14;
	// UnityEngine.Vector3 Float_equip_fast_streng::MasterBeforPos
	Vector3_t4282066566  ___MasterBeforPos_15;
	// UnityEngine.Vector3 Float_equip_fast_streng::attPos
	Vector3_t4282066566  ___attPos_16;
	// TweenPosition Float_equip_fast_streng::Atp
	TweenPosition_t3684358292 * ___Atp_17;
	// TweenScale Float_equip_fast_streng::Ats
	TweenScale_t2936666559 * ___Ats_18;
	// TweenAlpha Float_equip_fast_streng::Ata
	TweenAlpha_t2920325587 * ___Ata_19;
	// TweenPosition Float_equip_fast_streng::Mtp
	TweenPosition_t3684358292 * ___Mtp_20;
	// TweenScale Float_equip_fast_streng::Mts
	TweenScale_t2936666559 * ___Mts_21;
	// TweenAlpha Float_equip_fast_streng::Mta
	TweenAlpha_t2920325587 * ___Mta_22;
	// TweenPosition Float_equip_fast_streng::Atp02
	TweenPosition_t3684358292 * ___Atp02_23;
	// TweenScale Float_equip_fast_streng::Ats02
	TweenScale_t2936666559 * ___Ats02_24;
	// TweenAlpha Float_equip_fast_streng::Ata02
	TweenAlpha_t2920325587 * ___Ata02_25;
	// TweenPosition Float_equip_fast_streng::Mtp02
	TweenPosition_t3684358292 * ___Mtp02_26;
	// TweenScale Float_equip_fast_streng::Mts02
	TweenScale_t2936666559 * ___Mts02_27;
	// TweenAlpha Float_equip_fast_streng::Mta02
	TweenAlpha_t2920325587 * ___Mta02_28;

public:
	inline static int32_t get_offset_of__obj_master_3() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ____obj_master_3)); }
	inline GameObject_t3674682005 * get__obj_master_3() const { return ____obj_master_3; }
	inline GameObject_t3674682005 ** get_address_of__obj_master_3() { return &____obj_master_3; }
	inline void set__obj_master_3(GameObject_t3674682005 * value)
	{
		____obj_master_3 = value;
		Il2CppCodeGenWriteBarrier(&____obj_master_3, value);
	}

	inline static int32_t get_offset_of__obj_streng_4() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ____obj_streng_4)); }
	inline GameObject_t3674682005 * get__obj_streng_4() const { return ____obj_streng_4; }
	inline GameObject_t3674682005 ** get_address_of__obj_streng_4() { return &____obj_streng_4; }
	inline void set__obj_streng_4(GameObject_t3674682005 * value)
	{
		____obj_streng_4 = value;
		Il2CppCodeGenWriteBarrier(&____obj_streng_4, value);
	}

	inline static int32_t get_offset_of__label_master_tile_5() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ____label_master_tile_5)); }
	inline UILabel_t291504320 * get__label_master_tile_5() const { return ____label_master_tile_5; }
	inline UILabel_t291504320 ** get_address_of__label_master_tile_5() { return &____label_master_tile_5; }
	inline void set__label_master_tile_5(UILabel_t291504320 * value)
	{
		____label_master_tile_5 = value;
		Il2CppCodeGenWriteBarrier(&____label_master_tile_5, value);
	}

	inline static int32_t get_offset_of__label_master_left_6() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ____label_master_left_6)); }
	inline UILabel_t291504320 * get__label_master_left_6() const { return ____label_master_left_6; }
	inline UILabel_t291504320 ** get_address_of__label_master_left_6() { return &____label_master_left_6; }
	inline void set__label_master_left_6(UILabel_t291504320 * value)
	{
		____label_master_left_6 = value;
		Il2CppCodeGenWriteBarrier(&____label_master_left_6, value);
	}

	inline static int32_t get_offset_of__label_master_right_7() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ____label_master_right_7)); }
	inline UILabel_t291504320 * get__label_master_right_7() const { return ____label_master_right_7; }
	inline UILabel_t291504320 ** get_address_of__label_master_right_7() { return &____label_master_right_7; }
	inline void set__label_master_right_7(UILabel_t291504320 * value)
	{
		____label_master_right_7 = value;
		Il2CppCodeGenWriteBarrier(&____label_master_right_7, value);
	}

	inline static int32_t get_offset_of__label_value_left_8() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ____label_value_left_8)); }
	inline UILabel_t291504320 * get__label_value_left_8() const { return ____label_value_left_8; }
	inline UILabel_t291504320 ** get_address_of__label_value_left_8() { return &____label_value_left_8; }
	inline void set__label_value_left_8(UILabel_t291504320 * value)
	{
		____label_value_left_8 = value;
		Il2CppCodeGenWriteBarrier(&____label_value_left_8, value);
	}

	inline static int32_t get_offset_of__label_value_right_9() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ____label_value_right_9)); }
	inline UILabel_t291504320 * get__label_value_right_9() const { return ____label_value_right_9; }
	inline UILabel_t291504320 ** get_address_of__label_value_right_9() { return &____label_value_right_9; }
	inline void set__label_value_right_9(UILabel_t291504320 * value)
	{
		____label_value_right_9 = value;
		Il2CppCodeGenWriteBarrier(&____label_value_right_9, value);
	}

	inline static int32_t get_offset_of_Label_addCrit_10() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ___Label_addCrit_10)); }
	inline UILabel_t291504320 * get_Label_addCrit_10() const { return ___Label_addCrit_10; }
	inline UILabel_t291504320 ** get_address_of_Label_addCrit_10() { return &___Label_addCrit_10; }
	inline void set_Label_addCrit_10(UILabel_t291504320 * value)
	{
		___Label_addCrit_10 = value;
		Il2CppCodeGenWriteBarrier(&___Label_addCrit_10, value);
	}

	inline static int32_t get_offset_of_Label_costCopper_11() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ___Label_costCopper_11)); }
	inline UILabel_t291504320 * get_Label_costCopper_11() const { return ___Label_costCopper_11; }
	inline UILabel_t291504320 ** get_address_of_Label_costCopper_11() { return &___Label_costCopper_11; }
	inline void set_Label_costCopper_11(UILabel_t291504320 * value)
	{
		___Label_costCopper_11 = value;
		Il2CppCodeGenWriteBarrier(&___Label_costCopper_11, value);
	}

	inline static int32_t get_offset_of__sprite_bg_12() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ____sprite_bg_12)); }
	inline UITexture_t3903132647 * get__sprite_bg_12() const { return ____sprite_bg_12; }
	inline UITexture_t3903132647 ** get_address_of__sprite_bg_12() { return &____sprite_bg_12; }
	inline void set__sprite_bg_12(UITexture_t3903132647 * value)
	{
		____sprite_bg_12 = value;
		Il2CppCodeGenWriteBarrier(&____sprite_bg_12, value);
	}

	inline static int32_t get_offset_of_effectArr_13() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ___effectArr_13)); }
	inline Transform_t1659122786 * get_effectArr_13() const { return ___effectArr_13; }
	inline Transform_t1659122786 ** get_address_of_effectArr_13() { return &___effectArr_13; }
	inline void set_effectArr_13(Transform_t1659122786 * value)
	{
		___effectArr_13 = value;
		Il2CppCodeGenWriteBarrier(&___effectArr_13, value);
	}

	inline static int32_t get_offset_of_AttBeforPos_14() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ___AttBeforPos_14)); }
	inline Vector3_t4282066566  get_AttBeforPos_14() const { return ___AttBeforPos_14; }
	inline Vector3_t4282066566 * get_address_of_AttBeforPos_14() { return &___AttBeforPos_14; }
	inline void set_AttBeforPos_14(Vector3_t4282066566  value)
	{
		___AttBeforPos_14 = value;
	}

	inline static int32_t get_offset_of_MasterBeforPos_15() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ___MasterBeforPos_15)); }
	inline Vector3_t4282066566  get_MasterBeforPos_15() const { return ___MasterBeforPos_15; }
	inline Vector3_t4282066566 * get_address_of_MasterBeforPos_15() { return &___MasterBeforPos_15; }
	inline void set_MasterBeforPos_15(Vector3_t4282066566  value)
	{
		___MasterBeforPos_15 = value;
	}

	inline static int32_t get_offset_of_attPos_16() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ___attPos_16)); }
	inline Vector3_t4282066566  get_attPos_16() const { return ___attPos_16; }
	inline Vector3_t4282066566 * get_address_of_attPos_16() { return &___attPos_16; }
	inline void set_attPos_16(Vector3_t4282066566  value)
	{
		___attPos_16 = value;
	}

	inline static int32_t get_offset_of_Atp_17() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ___Atp_17)); }
	inline TweenPosition_t3684358292 * get_Atp_17() const { return ___Atp_17; }
	inline TweenPosition_t3684358292 ** get_address_of_Atp_17() { return &___Atp_17; }
	inline void set_Atp_17(TweenPosition_t3684358292 * value)
	{
		___Atp_17 = value;
		Il2CppCodeGenWriteBarrier(&___Atp_17, value);
	}

	inline static int32_t get_offset_of_Ats_18() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ___Ats_18)); }
	inline TweenScale_t2936666559 * get_Ats_18() const { return ___Ats_18; }
	inline TweenScale_t2936666559 ** get_address_of_Ats_18() { return &___Ats_18; }
	inline void set_Ats_18(TweenScale_t2936666559 * value)
	{
		___Ats_18 = value;
		Il2CppCodeGenWriteBarrier(&___Ats_18, value);
	}

	inline static int32_t get_offset_of_Ata_19() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ___Ata_19)); }
	inline TweenAlpha_t2920325587 * get_Ata_19() const { return ___Ata_19; }
	inline TweenAlpha_t2920325587 ** get_address_of_Ata_19() { return &___Ata_19; }
	inline void set_Ata_19(TweenAlpha_t2920325587 * value)
	{
		___Ata_19 = value;
		Il2CppCodeGenWriteBarrier(&___Ata_19, value);
	}

	inline static int32_t get_offset_of_Mtp_20() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ___Mtp_20)); }
	inline TweenPosition_t3684358292 * get_Mtp_20() const { return ___Mtp_20; }
	inline TweenPosition_t3684358292 ** get_address_of_Mtp_20() { return &___Mtp_20; }
	inline void set_Mtp_20(TweenPosition_t3684358292 * value)
	{
		___Mtp_20 = value;
		Il2CppCodeGenWriteBarrier(&___Mtp_20, value);
	}

	inline static int32_t get_offset_of_Mts_21() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ___Mts_21)); }
	inline TweenScale_t2936666559 * get_Mts_21() const { return ___Mts_21; }
	inline TweenScale_t2936666559 ** get_address_of_Mts_21() { return &___Mts_21; }
	inline void set_Mts_21(TweenScale_t2936666559 * value)
	{
		___Mts_21 = value;
		Il2CppCodeGenWriteBarrier(&___Mts_21, value);
	}

	inline static int32_t get_offset_of_Mta_22() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ___Mta_22)); }
	inline TweenAlpha_t2920325587 * get_Mta_22() const { return ___Mta_22; }
	inline TweenAlpha_t2920325587 ** get_address_of_Mta_22() { return &___Mta_22; }
	inline void set_Mta_22(TweenAlpha_t2920325587 * value)
	{
		___Mta_22 = value;
		Il2CppCodeGenWriteBarrier(&___Mta_22, value);
	}

	inline static int32_t get_offset_of_Atp02_23() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ___Atp02_23)); }
	inline TweenPosition_t3684358292 * get_Atp02_23() const { return ___Atp02_23; }
	inline TweenPosition_t3684358292 ** get_address_of_Atp02_23() { return &___Atp02_23; }
	inline void set_Atp02_23(TweenPosition_t3684358292 * value)
	{
		___Atp02_23 = value;
		Il2CppCodeGenWriteBarrier(&___Atp02_23, value);
	}

	inline static int32_t get_offset_of_Ats02_24() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ___Ats02_24)); }
	inline TweenScale_t2936666559 * get_Ats02_24() const { return ___Ats02_24; }
	inline TweenScale_t2936666559 ** get_address_of_Ats02_24() { return &___Ats02_24; }
	inline void set_Ats02_24(TweenScale_t2936666559 * value)
	{
		___Ats02_24 = value;
		Il2CppCodeGenWriteBarrier(&___Ats02_24, value);
	}

	inline static int32_t get_offset_of_Ata02_25() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ___Ata02_25)); }
	inline TweenAlpha_t2920325587 * get_Ata02_25() const { return ___Ata02_25; }
	inline TweenAlpha_t2920325587 ** get_address_of_Ata02_25() { return &___Ata02_25; }
	inline void set_Ata02_25(TweenAlpha_t2920325587 * value)
	{
		___Ata02_25 = value;
		Il2CppCodeGenWriteBarrier(&___Ata02_25, value);
	}

	inline static int32_t get_offset_of_Mtp02_26() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ___Mtp02_26)); }
	inline TweenPosition_t3684358292 * get_Mtp02_26() const { return ___Mtp02_26; }
	inline TweenPosition_t3684358292 ** get_address_of_Mtp02_26() { return &___Mtp02_26; }
	inline void set_Mtp02_26(TweenPosition_t3684358292 * value)
	{
		___Mtp02_26 = value;
		Il2CppCodeGenWriteBarrier(&___Mtp02_26, value);
	}

	inline static int32_t get_offset_of_Mts02_27() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ___Mts02_27)); }
	inline TweenScale_t2936666559 * get_Mts02_27() const { return ___Mts02_27; }
	inline TweenScale_t2936666559 ** get_address_of_Mts02_27() { return &___Mts02_27; }
	inline void set_Mts02_27(TweenScale_t2936666559 * value)
	{
		___Mts02_27 = value;
		Il2CppCodeGenWriteBarrier(&___Mts02_27, value);
	}

	inline static int32_t get_offset_of_Mta02_28() { return static_cast<int32_t>(offsetof(Float_equip_fast_streng_t2983926142, ___Mta02_28)); }
	inline TweenAlpha_t2920325587 * get_Mta02_28() const { return ___Mta02_28; }
	inline TweenAlpha_t2920325587 ** get_address_of_Mta02_28() { return &___Mta02_28; }
	inline void set_Mta02_28(TweenAlpha_t2920325587 * value)
	{
		___Mta02_28 = value;
		Il2CppCodeGenWriteBarrier(&___Mta02_28, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
