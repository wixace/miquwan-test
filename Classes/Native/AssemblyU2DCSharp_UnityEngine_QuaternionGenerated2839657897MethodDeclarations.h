﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_QuaternionGenerated
struct UnityEngine_QuaternionGenerated_t2839657897;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine_QuaternionGenerated::.ctor()
extern "C"  void UnityEngine_QuaternionGenerated__ctor_m2730199010 (UnityEngine_QuaternionGenerated_t2839657897 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QuaternionGenerated::.cctor()
extern "C"  void UnityEngine_QuaternionGenerated__cctor_m2549694475 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_Quaternion1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_Quaternion1_m1504968144 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_Quaternion2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_Quaternion2_m2749732625 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QuaternionGenerated::Quaternion_kEpsilon(JSVCall)
extern "C"  void UnityEngine_QuaternionGenerated_Quaternion_kEpsilon_m1135109735 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QuaternionGenerated::Quaternion_x(JSVCall)
extern "C"  void UnityEngine_QuaternionGenerated_Quaternion_x_m3235347854 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QuaternionGenerated::Quaternion_y(JSVCall)
extern "C"  void UnityEngine_QuaternionGenerated_Quaternion_y_m3038834349 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QuaternionGenerated::Quaternion_z(JSVCall)
extern "C"  void UnityEngine_QuaternionGenerated_Quaternion_z_m2842320844 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QuaternionGenerated::Quaternion_w(JSVCall)
extern "C"  void UnityEngine_QuaternionGenerated_Quaternion_w_m3431861359 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QuaternionGenerated::Quaternion_Item_Int32(JSVCall)
extern "C"  void UnityEngine_QuaternionGenerated_Quaternion_Item_Int32_m3466023908 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QuaternionGenerated::Quaternion_identity(JSVCall)
extern "C"  void UnityEngine_QuaternionGenerated_Quaternion_identity_m1030207752 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QuaternionGenerated::Quaternion_eulerAngles(JSVCall)
extern "C"  void UnityEngine_QuaternionGenerated_Quaternion_eulerAngles_m3999135325 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_Equals__Object(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_Equals__Object_m831237859 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_GetHashCode(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_GetHashCode_m1128811534 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_Set__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_Set__Single__Single__Single__Single_m1421343039 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_SetFromToRotation__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_SetFromToRotation__Vector3__Vector3_m280442274 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_SetLookRotation__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_SetLookRotation__Vector3__Vector3_m871183356 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_SetLookRotation__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_SetLookRotation__Vector3_m193613590 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_ToAngleAxis__Single__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_ToAngleAxis__Single__Vector3_m2365589364 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_ToString__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_ToString__String_m3281847074 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_ToString(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_ToString_m3785663953 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_Angle__Quaternion__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_Angle__Quaternion__Quaternion_m963364364 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_AngleAxis__Single__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_AngleAxis__Single__Vector3_m1919646681 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_Dot__Quaternion__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_Dot__Quaternion__Quaternion_m2518929154 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_Euler__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_Euler__Single__Single__Single_m3827686590 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_Euler__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_Euler__Vector3_m3464098860 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_FromToRotation__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_FromToRotation__Vector3__Vector3_m603359688 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_Inverse__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_Inverse__Quaternion_m1135855883 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_Lerp__Quaternion__Quaternion__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_Lerp__Quaternion__Quaternion__Single_m2417032992 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_LerpUnclamped__Quaternion__Quaternion__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_LerpUnclamped__Quaternion__Quaternion__Single_m2488682347 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_LookRotation__Vector3__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_LookRotation__Vector3__Vector3_m1260345762 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_LookRotation__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_LookRotation__Vector3_m601233072 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_op_Equality__Quaternion__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_op_Equality__Quaternion__Quaternion_m3542359377 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_op_Inequality__Quaternion__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_op_Inequality__Quaternion__Quaternion_m2665923126 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_op_Multiply__Quaternion__Vector3(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_op_Multiply__Quaternion__Vector3_m2309401749 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_op_Multiply__Quaternion__Quaternion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_op_Multiply__Quaternion__Quaternion_m2568082683 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_RotateTowards__Quaternion__Quaternion__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_RotateTowards__Quaternion__Quaternion__Single_m205883394 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_Slerp__Quaternion__Quaternion__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_Slerp__Quaternion__Quaternion__Single_m2702018987 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::Quaternion_SlerpUnclamped__Quaternion__Quaternion__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_Quaternion_SlerpUnclamped__Quaternion__Quaternion__Single_m431319360 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QuaternionGenerated::__Register()
extern "C"  void UnityEngine_QuaternionGenerated___Register_m531206693 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_QuaternionGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_QuaternionGenerated_ilo_attachFinalizerObject1_m3925035605 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QuaternionGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_QuaternionGenerated_ilo_addJSCSRel2_m1925986847 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_QuaternionGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_QuaternionGenerated_ilo_getSingle3_m382465519 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QuaternionGenerated::ilo_setVector3S4(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_QuaternionGenerated_ilo_setVector3S4_m3040433256 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QuaternionGenerated::ilo_changeJSObj5(System.Int32,System.Object)
extern "C"  void UnityEngine_QuaternionGenerated_ilo_changeJSObj5_m3069564107 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_QuaternionGenerated::ilo_getWhatever6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_QuaternionGenerated_ilo_getWhatever6_m3737164101 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QuaternionGenerated::ilo_setArgIndex7(System.Int32)
extern "C"  void UnityEngine_QuaternionGenerated_ilo_setArgIndex7_m3852702687 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QuaternionGenerated::ilo_setStringS8(System.Int32,System.String)
extern "C"  void UnityEngine_QuaternionGenerated_ilo_setStringS8_m2064929074 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_QuaternionGenerated::ilo_getObject9(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_QuaternionGenerated_ilo_getObject9_m3064805835 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QuaternionGenerated::ilo_setSingle10(System.Int32,System.Single)
extern "C"  void UnityEngine_QuaternionGenerated_ilo_setSingle10_m1895231148 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_QuaternionGenerated::ilo_setObject11(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_QuaternionGenerated_ilo_setObject11_m24271313 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine_QuaternionGenerated::ilo_getVector3S12(System.Int32)
extern "C"  Vector3_t4282066566  UnityEngine_QuaternionGenerated_ilo_getVector3S12_m2045209138 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_QuaternionGenerated::ilo_setBooleanS13(System.Int32,System.Boolean)
extern "C"  void UnityEngine_QuaternionGenerated_ilo_setBooleanS13_m2107110832 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
