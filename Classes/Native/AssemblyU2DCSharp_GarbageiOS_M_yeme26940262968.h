﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_yeme26
struct  M_yeme26_t940262968  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_yeme26::_dounearsere
	uint32_t ____dounearsere_0;
	// System.Boolean GarbageiOS.M_yeme26::_reartu
	bool ____reartu_1;
	// System.UInt32 GarbageiOS.M_yeme26::_kidereso
	uint32_t ____kidereso_2;
	// System.UInt32 GarbageiOS.M_yeme26::_nerniTricawtro
	uint32_t ____nerniTricawtro_3;
	// System.Boolean GarbageiOS.M_yeme26::_basa
	bool ____basa_4;
	// System.Single GarbageiOS.M_yeme26::_temereSowhi
	float ____temereSowhi_5;

public:
	inline static int32_t get_offset_of__dounearsere_0() { return static_cast<int32_t>(offsetof(M_yeme26_t940262968, ____dounearsere_0)); }
	inline uint32_t get__dounearsere_0() const { return ____dounearsere_0; }
	inline uint32_t* get_address_of__dounearsere_0() { return &____dounearsere_0; }
	inline void set__dounearsere_0(uint32_t value)
	{
		____dounearsere_0 = value;
	}

	inline static int32_t get_offset_of__reartu_1() { return static_cast<int32_t>(offsetof(M_yeme26_t940262968, ____reartu_1)); }
	inline bool get__reartu_1() const { return ____reartu_1; }
	inline bool* get_address_of__reartu_1() { return &____reartu_1; }
	inline void set__reartu_1(bool value)
	{
		____reartu_1 = value;
	}

	inline static int32_t get_offset_of__kidereso_2() { return static_cast<int32_t>(offsetof(M_yeme26_t940262968, ____kidereso_2)); }
	inline uint32_t get__kidereso_2() const { return ____kidereso_2; }
	inline uint32_t* get_address_of__kidereso_2() { return &____kidereso_2; }
	inline void set__kidereso_2(uint32_t value)
	{
		____kidereso_2 = value;
	}

	inline static int32_t get_offset_of__nerniTricawtro_3() { return static_cast<int32_t>(offsetof(M_yeme26_t940262968, ____nerniTricawtro_3)); }
	inline uint32_t get__nerniTricawtro_3() const { return ____nerniTricawtro_3; }
	inline uint32_t* get_address_of__nerniTricawtro_3() { return &____nerniTricawtro_3; }
	inline void set__nerniTricawtro_3(uint32_t value)
	{
		____nerniTricawtro_3 = value;
	}

	inline static int32_t get_offset_of__basa_4() { return static_cast<int32_t>(offsetof(M_yeme26_t940262968, ____basa_4)); }
	inline bool get__basa_4() const { return ____basa_4; }
	inline bool* get_address_of__basa_4() { return &____basa_4; }
	inline void set__basa_4(bool value)
	{
		____basa_4 = value;
	}

	inline static int32_t get_offset_of__temereSowhi_5() { return static_cast<int32_t>(offsetof(M_yeme26_t940262968, ____temereSowhi_5)); }
	inline float get__temereSowhi_5() const { return ____temereSowhi_5; }
	inline float* get_address_of__temereSowhi_5() { return &____temereSowhi_5; }
	inline void set__temereSowhi_5(float value)
	{
		____temereSowhi_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
