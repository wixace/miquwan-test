﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_chiryaNecaibe148
struct  M_chiryaNecaibe148_t522537700  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_chiryaNecaibe148::_reelaTedair
	uint32_t ____reelaTedair_0;
	// System.UInt32 GarbageiOS.M_chiryaNecaibe148::_gizallCuca
	uint32_t ____gizallCuca_1;
	// System.String GarbageiOS.M_chiryaNecaibe148::_kartralTelboupem
	String_t* ____kartralTelboupem_2;
	// System.Boolean GarbageiOS.M_chiryaNecaibe148::_drulearpiXumoo
	bool ____drulearpiXumoo_3;
	// System.String GarbageiOS.M_chiryaNecaibe148::_heejawMeejourair
	String_t* ____heejawMeejourair_4;
	// System.UInt32 GarbageiOS.M_chiryaNecaibe148::_rowturBijal
	uint32_t ____rowturBijal_5;
	// System.Boolean GarbageiOS.M_chiryaNecaibe148::_norhallchoDurere
	bool ____norhallchoDurere_6;
	// System.Boolean GarbageiOS.M_chiryaNecaibe148::_nounarwowLikowel
	bool ____nounarwowLikowel_7;
	// System.UInt32 GarbageiOS.M_chiryaNecaibe148::_lite
	uint32_t ____lite_8;
	// System.String GarbageiOS.M_chiryaNecaibe148::_xawmairkearMirheye
	String_t* ____xawmairkearMirheye_9;
	// System.Single GarbageiOS.M_chiryaNecaibe148::_jejay
	float ____jejay_10;
	// System.String GarbageiOS.M_chiryaNecaibe148::_tortearsall
	String_t* ____tortearsall_11;
	// System.Single GarbageiOS.M_chiryaNecaibe148::_draja
	float ____draja_12;
	// System.Int32 GarbageiOS.M_chiryaNecaibe148::_cirter
	int32_t ____cirter_13;
	// System.String GarbageiOS.M_chiryaNecaibe148::_wirsa
	String_t* ____wirsa_14;
	// System.Boolean GarbageiOS.M_chiryaNecaibe148::_stallcearna
	bool ____stallcearna_15;
	// System.Single GarbageiOS.M_chiryaNecaibe148::_jusaiwisTesellou
	float ____jusaiwisTesellou_16;
	// System.String GarbageiOS.M_chiryaNecaibe148::_rede
	String_t* ____rede_17;
	// System.Int32 GarbageiOS.M_chiryaNecaibe148::_zaltasQudearno
	int32_t ____zaltasQudearno_18;
	// System.Int32 GarbageiOS.M_chiryaNecaibe148::_camistaQopa
	int32_t ____camistaQopa_19;
	// System.UInt32 GarbageiOS.M_chiryaNecaibe148::_wirdehalGisgerebou
	uint32_t ____wirdehalGisgerebou_20;
	// System.Single GarbageiOS.M_chiryaNecaibe148::_narme
	float ____narme_21;
	// System.Boolean GarbageiOS.M_chiryaNecaibe148::_lidrouPusallo
	bool ____lidrouPusallo_22;
	// System.Boolean GarbageiOS.M_chiryaNecaibe148::_sorhuge
	bool ____sorhuge_23;
	// System.Single GarbageiOS.M_chiryaNecaibe148::_drerewhejay
	float ____drerewhejay_24;
	// System.Single GarbageiOS.M_chiryaNecaibe148::_heenelNona
	float ____heenelNona_25;
	// System.Int32 GarbageiOS.M_chiryaNecaibe148::_chearbe
	int32_t ____chearbe_26;
	// System.Boolean GarbageiOS.M_chiryaNecaibe148::_ceseejall
	bool ____ceseejall_27;
	// System.UInt32 GarbageiOS.M_chiryaNecaibe148::_repurTesellaw
	uint32_t ____repurTesellaw_28;
	// System.UInt32 GarbageiOS.M_chiryaNecaibe148::_tistibi
	uint32_t ____tistibi_29;
	// System.UInt32 GarbageiOS.M_chiryaNecaibe148::_hallzarNahi
	uint32_t ____hallzarNahi_30;
	// System.Int32 GarbageiOS.M_chiryaNecaibe148::_temwacerMaspall
	int32_t ____temwacerMaspall_31;
	// System.Boolean GarbageiOS.M_chiryaNecaibe148::_sidayTenu
	bool ____sidayTenu_32;
	// System.Int32 GarbageiOS.M_chiryaNecaibe148::_stearurDortro
	int32_t ____stearurDortro_33;
	// System.String GarbageiOS.M_chiryaNecaibe148::_trucaijou
	String_t* ____trucaijou_34;
	// System.UInt32 GarbageiOS.M_chiryaNecaibe148::_fowdrairTaymee
	uint32_t ____fowdrairTaymee_35;
	// System.Single GarbageiOS.M_chiryaNecaibe148::_laisur
	float ____laisur_36;
	// System.UInt32 GarbageiOS.M_chiryaNecaibe148::_sirstaiKallhomu
	uint32_t ____sirstaiKallhomu_37;
	// System.String GarbageiOS.M_chiryaNecaibe148::_baybir
	String_t* ____baybir_38;
	// System.String GarbageiOS.M_chiryaNecaibe148::_jisayNarkal
	String_t* ____jisayNarkal_39;
	// System.Single GarbageiOS.M_chiryaNecaibe148::_searlowre
	float ____searlowre_40;
	// System.Single GarbageiOS.M_chiryaNecaibe148::_wawkaWearyal
	float ____wawkaWearyal_41;
	// System.String GarbageiOS.M_chiryaNecaibe148::_xereyallRermofa
	String_t* ____xereyallRermofa_42;
	// System.Int32 GarbageiOS.M_chiryaNecaibe148::_stefaygarNurmou
	int32_t ____stefaygarNurmou_43;
	// System.Boolean GarbageiOS.M_chiryaNecaibe148::_sasju
	bool ____sasju_44;
	// System.Int32 GarbageiOS.M_chiryaNecaibe148::_dapirJayawhi
	int32_t ____dapirJayawhi_45;
	// System.Int32 GarbageiOS.M_chiryaNecaibe148::_cherxouGasar
	int32_t ____cherxouGasar_46;
	// System.Int32 GarbageiOS.M_chiryaNecaibe148::_dowsehow
	int32_t ____dowsehow_47;
	// System.Single GarbageiOS.M_chiryaNecaibe148::_xaytojiCacadru
	float ____xaytojiCacadru_48;
	// System.String GarbageiOS.M_chiryaNecaibe148::_heremKekernur
	String_t* ____heremKekernur_49;
	// System.Single GarbageiOS.M_chiryaNecaibe148::_parvarrirGemcow
	float ____parvarrirGemcow_50;
	// System.String GarbageiOS.M_chiryaNecaibe148::_hemcel
	String_t* ____hemcel_51;
	// System.UInt32 GarbageiOS.M_chiryaNecaibe148::_decusaFourairwo
	uint32_t ____decusaFourairwo_52;
	// System.Boolean GarbageiOS.M_chiryaNecaibe148::_penousearWallhea
	bool ____penousearWallhea_53;
	// System.Single GarbageiOS.M_chiryaNecaibe148::_tetouSijusay
	float ____tetouSijusay_54;

public:
	inline static int32_t get_offset_of__reelaTedair_0() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____reelaTedair_0)); }
	inline uint32_t get__reelaTedair_0() const { return ____reelaTedair_0; }
	inline uint32_t* get_address_of__reelaTedair_0() { return &____reelaTedair_0; }
	inline void set__reelaTedair_0(uint32_t value)
	{
		____reelaTedair_0 = value;
	}

	inline static int32_t get_offset_of__gizallCuca_1() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____gizallCuca_1)); }
	inline uint32_t get__gizallCuca_1() const { return ____gizallCuca_1; }
	inline uint32_t* get_address_of__gizallCuca_1() { return &____gizallCuca_1; }
	inline void set__gizallCuca_1(uint32_t value)
	{
		____gizallCuca_1 = value;
	}

	inline static int32_t get_offset_of__kartralTelboupem_2() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____kartralTelboupem_2)); }
	inline String_t* get__kartralTelboupem_2() const { return ____kartralTelboupem_2; }
	inline String_t** get_address_of__kartralTelboupem_2() { return &____kartralTelboupem_2; }
	inline void set__kartralTelboupem_2(String_t* value)
	{
		____kartralTelboupem_2 = value;
		Il2CppCodeGenWriteBarrier(&____kartralTelboupem_2, value);
	}

	inline static int32_t get_offset_of__drulearpiXumoo_3() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____drulearpiXumoo_3)); }
	inline bool get__drulearpiXumoo_3() const { return ____drulearpiXumoo_3; }
	inline bool* get_address_of__drulearpiXumoo_3() { return &____drulearpiXumoo_3; }
	inline void set__drulearpiXumoo_3(bool value)
	{
		____drulearpiXumoo_3 = value;
	}

	inline static int32_t get_offset_of__heejawMeejourair_4() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____heejawMeejourair_4)); }
	inline String_t* get__heejawMeejourair_4() const { return ____heejawMeejourair_4; }
	inline String_t** get_address_of__heejawMeejourair_4() { return &____heejawMeejourair_4; }
	inline void set__heejawMeejourair_4(String_t* value)
	{
		____heejawMeejourair_4 = value;
		Il2CppCodeGenWriteBarrier(&____heejawMeejourair_4, value);
	}

	inline static int32_t get_offset_of__rowturBijal_5() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____rowturBijal_5)); }
	inline uint32_t get__rowturBijal_5() const { return ____rowturBijal_5; }
	inline uint32_t* get_address_of__rowturBijal_5() { return &____rowturBijal_5; }
	inline void set__rowturBijal_5(uint32_t value)
	{
		____rowturBijal_5 = value;
	}

	inline static int32_t get_offset_of__norhallchoDurere_6() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____norhallchoDurere_6)); }
	inline bool get__norhallchoDurere_6() const { return ____norhallchoDurere_6; }
	inline bool* get_address_of__norhallchoDurere_6() { return &____norhallchoDurere_6; }
	inline void set__norhallchoDurere_6(bool value)
	{
		____norhallchoDurere_6 = value;
	}

	inline static int32_t get_offset_of__nounarwowLikowel_7() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____nounarwowLikowel_7)); }
	inline bool get__nounarwowLikowel_7() const { return ____nounarwowLikowel_7; }
	inline bool* get_address_of__nounarwowLikowel_7() { return &____nounarwowLikowel_7; }
	inline void set__nounarwowLikowel_7(bool value)
	{
		____nounarwowLikowel_7 = value;
	}

	inline static int32_t get_offset_of__lite_8() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____lite_8)); }
	inline uint32_t get__lite_8() const { return ____lite_8; }
	inline uint32_t* get_address_of__lite_8() { return &____lite_8; }
	inline void set__lite_8(uint32_t value)
	{
		____lite_8 = value;
	}

	inline static int32_t get_offset_of__xawmairkearMirheye_9() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____xawmairkearMirheye_9)); }
	inline String_t* get__xawmairkearMirheye_9() const { return ____xawmairkearMirheye_9; }
	inline String_t** get_address_of__xawmairkearMirheye_9() { return &____xawmairkearMirheye_9; }
	inline void set__xawmairkearMirheye_9(String_t* value)
	{
		____xawmairkearMirheye_9 = value;
		Il2CppCodeGenWriteBarrier(&____xawmairkearMirheye_9, value);
	}

	inline static int32_t get_offset_of__jejay_10() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____jejay_10)); }
	inline float get__jejay_10() const { return ____jejay_10; }
	inline float* get_address_of__jejay_10() { return &____jejay_10; }
	inline void set__jejay_10(float value)
	{
		____jejay_10 = value;
	}

	inline static int32_t get_offset_of__tortearsall_11() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____tortearsall_11)); }
	inline String_t* get__tortearsall_11() const { return ____tortearsall_11; }
	inline String_t** get_address_of__tortearsall_11() { return &____tortearsall_11; }
	inline void set__tortearsall_11(String_t* value)
	{
		____tortearsall_11 = value;
		Il2CppCodeGenWriteBarrier(&____tortearsall_11, value);
	}

	inline static int32_t get_offset_of__draja_12() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____draja_12)); }
	inline float get__draja_12() const { return ____draja_12; }
	inline float* get_address_of__draja_12() { return &____draja_12; }
	inline void set__draja_12(float value)
	{
		____draja_12 = value;
	}

	inline static int32_t get_offset_of__cirter_13() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____cirter_13)); }
	inline int32_t get__cirter_13() const { return ____cirter_13; }
	inline int32_t* get_address_of__cirter_13() { return &____cirter_13; }
	inline void set__cirter_13(int32_t value)
	{
		____cirter_13 = value;
	}

	inline static int32_t get_offset_of__wirsa_14() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____wirsa_14)); }
	inline String_t* get__wirsa_14() const { return ____wirsa_14; }
	inline String_t** get_address_of__wirsa_14() { return &____wirsa_14; }
	inline void set__wirsa_14(String_t* value)
	{
		____wirsa_14 = value;
		Il2CppCodeGenWriteBarrier(&____wirsa_14, value);
	}

	inline static int32_t get_offset_of__stallcearna_15() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____stallcearna_15)); }
	inline bool get__stallcearna_15() const { return ____stallcearna_15; }
	inline bool* get_address_of__stallcearna_15() { return &____stallcearna_15; }
	inline void set__stallcearna_15(bool value)
	{
		____stallcearna_15 = value;
	}

	inline static int32_t get_offset_of__jusaiwisTesellou_16() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____jusaiwisTesellou_16)); }
	inline float get__jusaiwisTesellou_16() const { return ____jusaiwisTesellou_16; }
	inline float* get_address_of__jusaiwisTesellou_16() { return &____jusaiwisTesellou_16; }
	inline void set__jusaiwisTesellou_16(float value)
	{
		____jusaiwisTesellou_16 = value;
	}

	inline static int32_t get_offset_of__rede_17() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____rede_17)); }
	inline String_t* get__rede_17() const { return ____rede_17; }
	inline String_t** get_address_of__rede_17() { return &____rede_17; }
	inline void set__rede_17(String_t* value)
	{
		____rede_17 = value;
		Il2CppCodeGenWriteBarrier(&____rede_17, value);
	}

	inline static int32_t get_offset_of__zaltasQudearno_18() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____zaltasQudearno_18)); }
	inline int32_t get__zaltasQudearno_18() const { return ____zaltasQudearno_18; }
	inline int32_t* get_address_of__zaltasQudearno_18() { return &____zaltasQudearno_18; }
	inline void set__zaltasQudearno_18(int32_t value)
	{
		____zaltasQudearno_18 = value;
	}

	inline static int32_t get_offset_of__camistaQopa_19() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____camistaQopa_19)); }
	inline int32_t get__camistaQopa_19() const { return ____camistaQopa_19; }
	inline int32_t* get_address_of__camistaQopa_19() { return &____camistaQopa_19; }
	inline void set__camistaQopa_19(int32_t value)
	{
		____camistaQopa_19 = value;
	}

	inline static int32_t get_offset_of__wirdehalGisgerebou_20() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____wirdehalGisgerebou_20)); }
	inline uint32_t get__wirdehalGisgerebou_20() const { return ____wirdehalGisgerebou_20; }
	inline uint32_t* get_address_of__wirdehalGisgerebou_20() { return &____wirdehalGisgerebou_20; }
	inline void set__wirdehalGisgerebou_20(uint32_t value)
	{
		____wirdehalGisgerebou_20 = value;
	}

	inline static int32_t get_offset_of__narme_21() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____narme_21)); }
	inline float get__narme_21() const { return ____narme_21; }
	inline float* get_address_of__narme_21() { return &____narme_21; }
	inline void set__narme_21(float value)
	{
		____narme_21 = value;
	}

	inline static int32_t get_offset_of__lidrouPusallo_22() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____lidrouPusallo_22)); }
	inline bool get__lidrouPusallo_22() const { return ____lidrouPusallo_22; }
	inline bool* get_address_of__lidrouPusallo_22() { return &____lidrouPusallo_22; }
	inline void set__lidrouPusallo_22(bool value)
	{
		____lidrouPusallo_22 = value;
	}

	inline static int32_t get_offset_of__sorhuge_23() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____sorhuge_23)); }
	inline bool get__sorhuge_23() const { return ____sorhuge_23; }
	inline bool* get_address_of__sorhuge_23() { return &____sorhuge_23; }
	inline void set__sorhuge_23(bool value)
	{
		____sorhuge_23 = value;
	}

	inline static int32_t get_offset_of__drerewhejay_24() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____drerewhejay_24)); }
	inline float get__drerewhejay_24() const { return ____drerewhejay_24; }
	inline float* get_address_of__drerewhejay_24() { return &____drerewhejay_24; }
	inline void set__drerewhejay_24(float value)
	{
		____drerewhejay_24 = value;
	}

	inline static int32_t get_offset_of__heenelNona_25() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____heenelNona_25)); }
	inline float get__heenelNona_25() const { return ____heenelNona_25; }
	inline float* get_address_of__heenelNona_25() { return &____heenelNona_25; }
	inline void set__heenelNona_25(float value)
	{
		____heenelNona_25 = value;
	}

	inline static int32_t get_offset_of__chearbe_26() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____chearbe_26)); }
	inline int32_t get__chearbe_26() const { return ____chearbe_26; }
	inline int32_t* get_address_of__chearbe_26() { return &____chearbe_26; }
	inline void set__chearbe_26(int32_t value)
	{
		____chearbe_26 = value;
	}

	inline static int32_t get_offset_of__ceseejall_27() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____ceseejall_27)); }
	inline bool get__ceseejall_27() const { return ____ceseejall_27; }
	inline bool* get_address_of__ceseejall_27() { return &____ceseejall_27; }
	inline void set__ceseejall_27(bool value)
	{
		____ceseejall_27 = value;
	}

	inline static int32_t get_offset_of__repurTesellaw_28() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____repurTesellaw_28)); }
	inline uint32_t get__repurTesellaw_28() const { return ____repurTesellaw_28; }
	inline uint32_t* get_address_of__repurTesellaw_28() { return &____repurTesellaw_28; }
	inline void set__repurTesellaw_28(uint32_t value)
	{
		____repurTesellaw_28 = value;
	}

	inline static int32_t get_offset_of__tistibi_29() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____tistibi_29)); }
	inline uint32_t get__tistibi_29() const { return ____tistibi_29; }
	inline uint32_t* get_address_of__tistibi_29() { return &____tistibi_29; }
	inline void set__tistibi_29(uint32_t value)
	{
		____tistibi_29 = value;
	}

	inline static int32_t get_offset_of__hallzarNahi_30() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____hallzarNahi_30)); }
	inline uint32_t get__hallzarNahi_30() const { return ____hallzarNahi_30; }
	inline uint32_t* get_address_of__hallzarNahi_30() { return &____hallzarNahi_30; }
	inline void set__hallzarNahi_30(uint32_t value)
	{
		____hallzarNahi_30 = value;
	}

	inline static int32_t get_offset_of__temwacerMaspall_31() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____temwacerMaspall_31)); }
	inline int32_t get__temwacerMaspall_31() const { return ____temwacerMaspall_31; }
	inline int32_t* get_address_of__temwacerMaspall_31() { return &____temwacerMaspall_31; }
	inline void set__temwacerMaspall_31(int32_t value)
	{
		____temwacerMaspall_31 = value;
	}

	inline static int32_t get_offset_of__sidayTenu_32() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____sidayTenu_32)); }
	inline bool get__sidayTenu_32() const { return ____sidayTenu_32; }
	inline bool* get_address_of__sidayTenu_32() { return &____sidayTenu_32; }
	inline void set__sidayTenu_32(bool value)
	{
		____sidayTenu_32 = value;
	}

	inline static int32_t get_offset_of__stearurDortro_33() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____stearurDortro_33)); }
	inline int32_t get__stearurDortro_33() const { return ____stearurDortro_33; }
	inline int32_t* get_address_of__stearurDortro_33() { return &____stearurDortro_33; }
	inline void set__stearurDortro_33(int32_t value)
	{
		____stearurDortro_33 = value;
	}

	inline static int32_t get_offset_of__trucaijou_34() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____trucaijou_34)); }
	inline String_t* get__trucaijou_34() const { return ____trucaijou_34; }
	inline String_t** get_address_of__trucaijou_34() { return &____trucaijou_34; }
	inline void set__trucaijou_34(String_t* value)
	{
		____trucaijou_34 = value;
		Il2CppCodeGenWriteBarrier(&____trucaijou_34, value);
	}

	inline static int32_t get_offset_of__fowdrairTaymee_35() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____fowdrairTaymee_35)); }
	inline uint32_t get__fowdrairTaymee_35() const { return ____fowdrairTaymee_35; }
	inline uint32_t* get_address_of__fowdrairTaymee_35() { return &____fowdrairTaymee_35; }
	inline void set__fowdrairTaymee_35(uint32_t value)
	{
		____fowdrairTaymee_35 = value;
	}

	inline static int32_t get_offset_of__laisur_36() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____laisur_36)); }
	inline float get__laisur_36() const { return ____laisur_36; }
	inline float* get_address_of__laisur_36() { return &____laisur_36; }
	inline void set__laisur_36(float value)
	{
		____laisur_36 = value;
	}

	inline static int32_t get_offset_of__sirstaiKallhomu_37() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____sirstaiKallhomu_37)); }
	inline uint32_t get__sirstaiKallhomu_37() const { return ____sirstaiKallhomu_37; }
	inline uint32_t* get_address_of__sirstaiKallhomu_37() { return &____sirstaiKallhomu_37; }
	inline void set__sirstaiKallhomu_37(uint32_t value)
	{
		____sirstaiKallhomu_37 = value;
	}

	inline static int32_t get_offset_of__baybir_38() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____baybir_38)); }
	inline String_t* get__baybir_38() const { return ____baybir_38; }
	inline String_t** get_address_of__baybir_38() { return &____baybir_38; }
	inline void set__baybir_38(String_t* value)
	{
		____baybir_38 = value;
		Il2CppCodeGenWriteBarrier(&____baybir_38, value);
	}

	inline static int32_t get_offset_of__jisayNarkal_39() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____jisayNarkal_39)); }
	inline String_t* get__jisayNarkal_39() const { return ____jisayNarkal_39; }
	inline String_t** get_address_of__jisayNarkal_39() { return &____jisayNarkal_39; }
	inline void set__jisayNarkal_39(String_t* value)
	{
		____jisayNarkal_39 = value;
		Il2CppCodeGenWriteBarrier(&____jisayNarkal_39, value);
	}

	inline static int32_t get_offset_of__searlowre_40() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____searlowre_40)); }
	inline float get__searlowre_40() const { return ____searlowre_40; }
	inline float* get_address_of__searlowre_40() { return &____searlowre_40; }
	inline void set__searlowre_40(float value)
	{
		____searlowre_40 = value;
	}

	inline static int32_t get_offset_of__wawkaWearyal_41() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____wawkaWearyal_41)); }
	inline float get__wawkaWearyal_41() const { return ____wawkaWearyal_41; }
	inline float* get_address_of__wawkaWearyal_41() { return &____wawkaWearyal_41; }
	inline void set__wawkaWearyal_41(float value)
	{
		____wawkaWearyal_41 = value;
	}

	inline static int32_t get_offset_of__xereyallRermofa_42() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____xereyallRermofa_42)); }
	inline String_t* get__xereyallRermofa_42() const { return ____xereyallRermofa_42; }
	inline String_t** get_address_of__xereyallRermofa_42() { return &____xereyallRermofa_42; }
	inline void set__xereyallRermofa_42(String_t* value)
	{
		____xereyallRermofa_42 = value;
		Il2CppCodeGenWriteBarrier(&____xereyallRermofa_42, value);
	}

	inline static int32_t get_offset_of__stefaygarNurmou_43() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____stefaygarNurmou_43)); }
	inline int32_t get__stefaygarNurmou_43() const { return ____stefaygarNurmou_43; }
	inline int32_t* get_address_of__stefaygarNurmou_43() { return &____stefaygarNurmou_43; }
	inline void set__stefaygarNurmou_43(int32_t value)
	{
		____stefaygarNurmou_43 = value;
	}

	inline static int32_t get_offset_of__sasju_44() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____sasju_44)); }
	inline bool get__sasju_44() const { return ____sasju_44; }
	inline bool* get_address_of__sasju_44() { return &____sasju_44; }
	inline void set__sasju_44(bool value)
	{
		____sasju_44 = value;
	}

	inline static int32_t get_offset_of__dapirJayawhi_45() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____dapirJayawhi_45)); }
	inline int32_t get__dapirJayawhi_45() const { return ____dapirJayawhi_45; }
	inline int32_t* get_address_of__dapirJayawhi_45() { return &____dapirJayawhi_45; }
	inline void set__dapirJayawhi_45(int32_t value)
	{
		____dapirJayawhi_45 = value;
	}

	inline static int32_t get_offset_of__cherxouGasar_46() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____cherxouGasar_46)); }
	inline int32_t get__cherxouGasar_46() const { return ____cherxouGasar_46; }
	inline int32_t* get_address_of__cherxouGasar_46() { return &____cherxouGasar_46; }
	inline void set__cherxouGasar_46(int32_t value)
	{
		____cherxouGasar_46 = value;
	}

	inline static int32_t get_offset_of__dowsehow_47() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____dowsehow_47)); }
	inline int32_t get__dowsehow_47() const { return ____dowsehow_47; }
	inline int32_t* get_address_of__dowsehow_47() { return &____dowsehow_47; }
	inline void set__dowsehow_47(int32_t value)
	{
		____dowsehow_47 = value;
	}

	inline static int32_t get_offset_of__xaytojiCacadru_48() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____xaytojiCacadru_48)); }
	inline float get__xaytojiCacadru_48() const { return ____xaytojiCacadru_48; }
	inline float* get_address_of__xaytojiCacadru_48() { return &____xaytojiCacadru_48; }
	inline void set__xaytojiCacadru_48(float value)
	{
		____xaytojiCacadru_48 = value;
	}

	inline static int32_t get_offset_of__heremKekernur_49() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____heremKekernur_49)); }
	inline String_t* get__heremKekernur_49() const { return ____heremKekernur_49; }
	inline String_t** get_address_of__heremKekernur_49() { return &____heremKekernur_49; }
	inline void set__heremKekernur_49(String_t* value)
	{
		____heremKekernur_49 = value;
		Il2CppCodeGenWriteBarrier(&____heremKekernur_49, value);
	}

	inline static int32_t get_offset_of__parvarrirGemcow_50() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____parvarrirGemcow_50)); }
	inline float get__parvarrirGemcow_50() const { return ____parvarrirGemcow_50; }
	inline float* get_address_of__parvarrirGemcow_50() { return &____parvarrirGemcow_50; }
	inline void set__parvarrirGemcow_50(float value)
	{
		____parvarrirGemcow_50 = value;
	}

	inline static int32_t get_offset_of__hemcel_51() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____hemcel_51)); }
	inline String_t* get__hemcel_51() const { return ____hemcel_51; }
	inline String_t** get_address_of__hemcel_51() { return &____hemcel_51; }
	inline void set__hemcel_51(String_t* value)
	{
		____hemcel_51 = value;
		Il2CppCodeGenWriteBarrier(&____hemcel_51, value);
	}

	inline static int32_t get_offset_of__decusaFourairwo_52() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____decusaFourairwo_52)); }
	inline uint32_t get__decusaFourairwo_52() const { return ____decusaFourairwo_52; }
	inline uint32_t* get_address_of__decusaFourairwo_52() { return &____decusaFourairwo_52; }
	inline void set__decusaFourairwo_52(uint32_t value)
	{
		____decusaFourairwo_52 = value;
	}

	inline static int32_t get_offset_of__penousearWallhea_53() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____penousearWallhea_53)); }
	inline bool get__penousearWallhea_53() const { return ____penousearWallhea_53; }
	inline bool* get_address_of__penousearWallhea_53() { return &____penousearWallhea_53; }
	inline void set__penousearWallhea_53(bool value)
	{
		____penousearWallhea_53 = value;
	}

	inline static int32_t get_offset_of__tetouSijusay_54() { return static_cast<int32_t>(offsetof(M_chiryaNecaibe148_t522537700, ____tetouSijusay_54)); }
	inline float get__tetouSijusay_54() const { return ____tetouSijusay_54; }
	inline float* get_address_of__tetouSijusay_54() { return &____tetouSijusay_54; }
	inline void set__tetouSijusay_54(float value)
	{
		____tetouSijusay_54 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
