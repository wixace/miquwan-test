﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Poly2Tri.AdvancingFrontNode
struct AdvancingFrontNode_t688967424;
// Pathfinding.Poly2Tri.TriangulationPoint
struct TriangulationPoint_t3810082933;
// Pathfinding.Poly2Tri.DelaunayTriangle
struct DelaunayTriangle_t2835103587;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Poly2Tri.AdvancingFrontNode
struct  AdvancingFrontNode_t688967424  : public Il2CppObject
{
public:
	// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.AdvancingFrontNode::Next
	AdvancingFrontNode_t688967424 * ___Next_0;
	// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.AdvancingFrontNode::Prev
	AdvancingFrontNode_t688967424 * ___Prev_1;
	// System.Double Pathfinding.Poly2Tri.AdvancingFrontNode::Value
	double ___Value_2;
	// Pathfinding.Poly2Tri.TriangulationPoint Pathfinding.Poly2Tri.AdvancingFrontNode::Point
	TriangulationPoint_t3810082933 * ___Point_3;
	// Pathfinding.Poly2Tri.DelaunayTriangle Pathfinding.Poly2Tri.AdvancingFrontNode::Triangle
	DelaunayTriangle_t2835103587 * ___Triangle_4;

public:
	inline static int32_t get_offset_of_Next_0() { return static_cast<int32_t>(offsetof(AdvancingFrontNode_t688967424, ___Next_0)); }
	inline AdvancingFrontNode_t688967424 * get_Next_0() const { return ___Next_0; }
	inline AdvancingFrontNode_t688967424 ** get_address_of_Next_0() { return &___Next_0; }
	inline void set_Next_0(AdvancingFrontNode_t688967424 * value)
	{
		___Next_0 = value;
		Il2CppCodeGenWriteBarrier(&___Next_0, value);
	}

	inline static int32_t get_offset_of_Prev_1() { return static_cast<int32_t>(offsetof(AdvancingFrontNode_t688967424, ___Prev_1)); }
	inline AdvancingFrontNode_t688967424 * get_Prev_1() const { return ___Prev_1; }
	inline AdvancingFrontNode_t688967424 ** get_address_of_Prev_1() { return &___Prev_1; }
	inline void set_Prev_1(AdvancingFrontNode_t688967424 * value)
	{
		___Prev_1 = value;
		Il2CppCodeGenWriteBarrier(&___Prev_1, value);
	}

	inline static int32_t get_offset_of_Value_2() { return static_cast<int32_t>(offsetof(AdvancingFrontNode_t688967424, ___Value_2)); }
	inline double get_Value_2() const { return ___Value_2; }
	inline double* get_address_of_Value_2() { return &___Value_2; }
	inline void set_Value_2(double value)
	{
		___Value_2 = value;
	}

	inline static int32_t get_offset_of_Point_3() { return static_cast<int32_t>(offsetof(AdvancingFrontNode_t688967424, ___Point_3)); }
	inline TriangulationPoint_t3810082933 * get_Point_3() const { return ___Point_3; }
	inline TriangulationPoint_t3810082933 ** get_address_of_Point_3() { return &___Point_3; }
	inline void set_Point_3(TriangulationPoint_t3810082933 * value)
	{
		___Point_3 = value;
		Il2CppCodeGenWriteBarrier(&___Point_3, value);
	}

	inline static int32_t get_offset_of_Triangle_4() { return static_cast<int32_t>(offsetof(AdvancingFrontNode_t688967424, ___Triangle_4)); }
	inline DelaunayTriangle_t2835103587 * get_Triangle_4() const { return ___Triangle_4; }
	inline DelaunayTriangle_t2835103587 ** get_address_of_Triangle_4() { return &___Triangle_4; }
	inline void set_Triangle_4(DelaunayTriangle_t2835103587 * value)
	{
		___Triangle_4 = value;
		Il2CppCodeGenWriteBarrier(&___Triangle_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
