﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweenOrthoSizeGenerated
struct TweenOrthoSizeGenerated_t818132833;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Camera
struct Camera_t2727095145;
// TweenOrthoSize
struct TweenOrthoSize_t1845504462;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_TweenOrthoSize1845504462.h"

// System.Void TweenOrthoSizeGenerated::.ctor()
extern "C"  void TweenOrthoSizeGenerated__ctor_m3227280682 (TweenOrthoSizeGenerated_t818132833 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenOrthoSizeGenerated::TweenOrthoSize_TweenOrthoSize1(JSVCall,System.Int32)
extern "C"  bool TweenOrthoSizeGenerated_TweenOrthoSize_TweenOrthoSize1_m3866050312 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenOrthoSizeGenerated::TweenOrthoSize_from(JSVCall)
extern "C"  void TweenOrthoSizeGenerated_TweenOrthoSize_from_m1529993572 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenOrthoSizeGenerated::TweenOrthoSize_to(JSVCall)
extern "C"  void TweenOrthoSizeGenerated_TweenOrthoSize_to_m3772805043 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenOrthoSizeGenerated::TweenOrthoSize_cachedCamera(JSVCall)
extern "C"  void TweenOrthoSizeGenerated_TweenOrthoSize_cachedCamera_m726658759 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenOrthoSizeGenerated::TweenOrthoSize_value(JSVCall)
extern "C"  void TweenOrthoSizeGenerated_TweenOrthoSize_value_m1070559213 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenOrthoSizeGenerated::TweenOrthoSize_SetEndToCurrentValue(JSVCall,System.Int32)
extern "C"  bool TweenOrthoSizeGenerated_TweenOrthoSize_SetEndToCurrentValue_m3061458345 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenOrthoSizeGenerated::TweenOrthoSize_SetStartToCurrentValue(JSVCall,System.Int32)
extern "C"  bool TweenOrthoSizeGenerated_TweenOrthoSize_SetStartToCurrentValue_m3034816304 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenOrthoSizeGenerated::TweenOrthoSize_Begin__GameObject__Single__Single(JSVCall,System.Int32)
extern "C"  bool TweenOrthoSizeGenerated_TweenOrthoSize_Begin__GameObject__Single__Single_m1677832207 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenOrthoSizeGenerated::__Register()
extern "C"  void TweenOrthoSizeGenerated___Register_m535282141 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TweenOrthoSizeGenerated::ilo_getSingle1(System.Int32)
extern "C"  float TweenOrthoSizeGenerated_ilo_getSingle1_m3287864613 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera TweenOrthoSizeGenerated::ilo_get_cachedCamera2(TweenOrthoSize)
extern "C"  Camera_t2727095145 * TweenOrthoSizeGenerated_ilo_get_cachedCamera2_m761343594 (Il2CppObject * __this /* static, unused */, TweenOrthoSize_t1845504462 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TweenOrthoSizeGenerated::ilo_get_value3(TweenOrthoSize)
extern "C"  float TweenOrthoSizeGenerated_ilo_get_value3_m4208399294 (Il2CppObject * __this /* static, unused */, TweenOrthoSize_t1845504462 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenOrthoSizeGenerated::ilo_SetStartToCurrentValue4(TweenOrthoSize)
extern "C"  void TweenOrthoSizeGenerated_ilo_SetStartToCurrentValue4_m2249157864 (Il2CppObject * __this /* static, unused */, TweenOrthoSize_t1845504462 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
