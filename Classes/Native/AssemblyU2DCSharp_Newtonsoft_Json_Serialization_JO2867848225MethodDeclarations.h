﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Serialization.JObjectContract
struct JObjectContract_t2867848225;
// System.Type
struct Type_t;
// Newtonsoft.Json.Serialization.JsonPropertyCollection
struct JsonPropertyCollection_t717767559;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t4136801618;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_MemberSerializat1550301796.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Serialization_Jso717767559.h"
#include "mscorlib_System_Reflection_ConstructorInfo4136801618.h"

// System.Void Newtonsoft.Json.Serialization.JObjectContract::.ctor(System.Type)
extern "C"  void JObjectContract__ctor_m2501509654 (JObjectContract_t2867848225 * __this, Type_t * ___underlyingType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.MemberSerialization Newtonsoft.Json.Serialization.JObjectContract::get_MemberSerialization()
extern "C"  int32_t JObjectContract_get_MemberSerialization_m3256601952 (JObjectContract_t2867848225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JObjectContract::set_MemberSerialization(Newtonsoft.Json.MemberSerialization)
extern "C"  void JObjectContract_set_MemberSerialization_m1276163735 (JObjectContract_t2867848225 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JObjectContract::get_Properties()
extern "C"  JsonPropertyCollection_t717767559 * JObjectContract_get_Properties_m2450119054 (JObjectContract_t2867848225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JObjectContract::set_Properties(Newtonsoft.Json.Serialization.JsonPropertyCollection)
extern "C"  void JObjectContract_set_Properties_m780071427 (JObjectContract_t2867848225 * __this, JsonPropertyCollection_t717767559 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JObjectContract::get_ConstructorParameters()
extern "C"  JsonPropertyCollection_t717767559 * JObjectContract_get_ConstructorParameters_m1602878763 (JObjectContract_t2867848225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JObjectContract::set_ConstructorParameters(Newtonsoft.Json.Serialization.JsonPropertyCollection)
extern "C"  void JObjectContract_set_ConstructorParameters_m3955839546 (JObjectContract_t2867848225 * __this, JsonPropertyCollection_t717767559 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.JObjectContract::get_OverrideConstructor()
extern "C"  ConstructorInfo_t4136801618 * JObjectContract_get_OverrideConstructor_m2759524181 (JObjectContract_t2867848225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JObjectContract::set_OverrideConstructor(System.Reflection.ConstructorInfo)
extern "C"  void JObjectContract_set_OverrideConstructor_m1775847260 (JObjectContract_t2867848225 * __this, ConstructorInfo_t4136801618 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.JObjectContract::get_ParametrizedConstructor()
extern "C"  ConstructorInfo_t4136801618 * JObjectContract_get_ParametrizedConstructor_m2233128411 (JObjectContract_t2867848225 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Serialization.JObjectContract::set_ParametrizedConstructor(System.Reflection.ConstructorInfo)
extern "C"  void JObjectContract_set_ParametrizedConstructor_m3473174550 (JObjectContract_t2867848225 * __this, ConstructorInfo_t4136801618 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
