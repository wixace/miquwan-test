﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.ProceduralPropertyDescription
struct ProceduralPropertyDescription_t3210075808;
struct ProceduralPropertyDescription_t3210075808_marshaled_pinvoke;
struct ProceduralPropertyDescription_t3210075808_marshaled_com;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.ProceduralPropertyDescription::.ctor()
extern "C"  void ProceduralPropertyDescription__ctor_m4023142639 (ProceduralPropertyDescription_t3210075808 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct ProceduralPropertyDescription_t3210075808;
struct ProceduralPropertyDescription_t3210075808_marshaled_pinvoke;

extern "C" void ProceduralPropertyDescription_t3210075808_marshal_pinvoke(const ProceduralPropertyDescription_t3210075808& unmarshaled, ProceduralPropertyDescription_t3210075808_marshaled_pinvoke& marshaled);
extern "C" void ProceduralPropertyDescription_t3210075808_marshal_pinvoke_back(const ProceduralPropertyDescription_t3210075808_marshaled_pinvoke& marshaled, ProceduralPropertyDescription_t3210075808& unmarshaled);
extern "C" void ProceduralPropertyDescription_t3210075808_marshal_pinvoke_cleanup(ProceduralPropertyDescription_t3210075808_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct ProceduralPropertyDescription_t3210075808;
struct ProceduralPropertyDescription_t3210075808_marshaled_com;

extern "C" void ProceduralPropertyDescription_t3210075808_marshal_com(const ProceduralPropertyDescription_t3210075808& unmarshaled, ProceduralPropertyDescription_t3210075808_marshaled_com& marshaled);
extern "C" void ProceduralPropertyDescription_t3210075808_marshal_com_back(const ProceduralPropertyDescription_t3210075808_marshaled_com& marshaled, ProceduralPropertyDescription_t3210075808& unmarshaled);
extern "C" void ProceduralPropertyDescription_t3210075808_marshal_com_cleanup(ProceduralPropertyDescription_t3210075808_marshaled_com& marshaled);
