﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_mouqeresoo183
struct M_mouqeresoo183_t557512979;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"

// System.Void GarbageiOS.M_mouqeresoo183::.ctor()
extern "C"  void M_mouqeresoo183__ctor_m2913067440 (M_mouqeresoo183_t557512979 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_mouqeresoo183::M_tore0(System.String[],System.Int32)
extern "C"  void M_mouqeresoo183_M_tore0_m3408934773 (M_mouqeresoo183_t557512979 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_mouqeresoo183::M_faleahe1(System.String[],System.Int32)
extern "C"  void M_mouqeresoo183_M_faleahe1_m2685037220 (M_mouqeresoo183_t557512979 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
