﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Float_equip_streng
struct Float_equip_streng_t2474838303;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// UILabel
struct UILabel_t291504320;
// System.String
struct String_t;
// FloatTextUnit
struct FloatTextUnit_t2362298029;
// EventDelegate/Callback
struct Callback_t1094463061;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_FLOAT_TEXT_ID2199345770.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"
#include "AssemblyU2DCSharp_EventDelegate_Callback1094463061.h"
#include "AssemblyU2DCSharp_UITweener_Method1078127180.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void Float_equip_streng::.ctor()
extern "C"  void Float_equip_streng__ctor_m2984364636 (Float_equip_streng_t2474838303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng::OnAwake(UnityEngine.GameObject)
extern "C"  void Float_equip_streng_OnAwake_m2467387928 (Float_equip_streng_t2474838303 * __this, GameObject_t3674682005 * ___viewGO0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng::OnDestroy()
extern "C"  void Float_equip_streng_OnDestroy_m2362380629 (Float_equip_streng_t2474838303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FLOAT_TEXT_ID Float_equip_streng::FloatID()
extern "C"  int32_t Float_equip_streng_FloatID_m3874201832 (Float_equip_streng_t2474838303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng::Init()
extern "C"  void Float_equip_streng_Init_m186353784 (Float_equip_streng_t2474838303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng::SetFile(System.Object[])
extern "C"  void Float_equip_streng_SetFile_m2849331706 (Float_equip_streng_t2474838303 * __this, ObjectU5BU5D_t1108656482* ___args0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng::PlayBaojiEffect()
extern "C"  void Float_equip_streng_PlayBaojiEffect_m3257379110 (Float_equip_streng_t2474838303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng::Death()
extern "C"  void Float_equip_streng_Death_m1074459886 (Float_equip_streng_t2474838303 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng::ilo_set_text1(UILabel,System.String)
extern "C"  void Float_equip_streng_ilo_set_text1_m2673279456 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng::ilo_MoveScale2(FloatTextUnit,System.Single,System.Single,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void Float_equip_streng_ilo_MoveScale2_m1431726504 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, float ___from1, float ___to2, float ___duration3, float ___delay4, Callback_t1094463061 * ___call5, GameObject_t3674682005 * ___go6, int32_t ___method7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng::ilo_MovePosition3(FloatTextUnit,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,EventDelegate/Callback,UnityEngine.GameObject,UITweener/Method)
extern "C"  void Float_equip_streng_ilo_MovePosition3_m2685533720 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, Vector3_t4282066566  ___from1, Vector3_t4282066566  ___to2, float ___duration3, float ___delay4, Callback_t1094463061 * ___call5, GameObject_t3674682005 * ___go6, int32_t ___method7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng::ilo_Play4(FloatTextUnit)
extern "C"  void Float_equip_streng_ilo_Play4_m433300254 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Float_equip_streng::ilo_set_IsLife5(FloatTextUnit,System.Boolean)
extern "C"  void Float_equip_streng_ilo_set_IsLife5_m2326684657 (Il2CppObject * __this /* static, unused */, FloatTextUnit_t2362298029 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
