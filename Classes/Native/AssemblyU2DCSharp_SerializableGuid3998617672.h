﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Byte[]
struct ByteU5BU5D_t4260760469;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SerializableGuid
struct  SerializableGuid_t3998617672  : public Il2CppObject
{
public:
	// System.Byte[] SerializableGuid::m_GuidBytes
	ByteU5BU5D_t4260760469* ___m_GuidBytes_0;

public:
	inline static int32_t get_offset_of_m_GuidBytes_0() { return static_cast<int32_t>(offsetof(SerializableGuid_t3998617672, ___m_GuidBytes_0)); }
	inline ByteU5BU5D_t4260760469* get_m_GuidBytes_0() const { return ___m_GuidBytes_0; }
	inline ByteU5BU5D_t4260760469** get_address_of_m_GuidBytes_0() { return &___m_GuidBytes_0; }
	inline void set_m_GuidBytes_0(ByteU5BU5D_t4260760469* value)
	{
		___m_GuidBytes_0 = value;
		Il2CppCodeGenWriteBarrier(&___m_GuidBytes_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
