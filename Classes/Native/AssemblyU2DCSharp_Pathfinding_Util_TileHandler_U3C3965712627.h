﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Util.TileHandler
struct TileHandler_t1505605502;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Util.TileHandler/<ClearTile>c__AnonStorey121
struct  U3CClearTileU3Ec__AnonStorey121_t3965712627  : public Il2CppObject
{
public:
	// System.Int32 Pathfinding.Util.TileHandler/<ClearTile>c__AnonStorey121::x
	int32_t ___x_0;
	// System.Int32 Pathfinding.Util.TileHandler/<ClearTile>c__AnonStorey121::z
	int32_t ___z_1;
	// Pathfinding.Util.TileHandler Pathfinding.Util.TileHandler/<ClearTile>c__AnonStorey121::<>f__this
	TileHandler_t1505605502 * ___U3CU3Ef__this_2;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(U3CClearTileU3Ec__AnonStorey121_t3965712627, ___x_0)); }
	inline int32_t get_x_0() const { return ___x_0; }
	inline int32_t* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(int32_t value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_z_1() { return static_cast<int32_t>(offsetof(U3CClearTileU3Ec__AnonStorey121_t3965712627, ___z_1)); }
	inline int32_t get_z_1() const { return ___z_1; }
	inline int32_t* get_address_of_z_1() { return &___z_1; }
	inline void set_z_1(int32_t value)
	{
		___z_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_2() { return static_cast<int32_t>(offsetof(U3CClearTileU3Ec__AnonStorey121_t3965712627, ___U3CU3Ef__this_2)); }
	inline TileHandler_t1505605502 * get_U3CU3Ef__this_2() const { return ___U3CU3Ef__this_2; }
	inline TileHandler_t1505605502 ** get_address_of_U3CU3Ef__this_2() { return &___U3CU3Ef__this_2; }
	inline void set_U3CU3Ef__this_2(TileHandler_t1505605502 * value)
	{
		___U3CU3Ef__this_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
