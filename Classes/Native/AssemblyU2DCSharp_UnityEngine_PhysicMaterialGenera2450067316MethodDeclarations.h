﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_PhysicMaterialGenerated
struct UnityEngine_PhysicMaterialGenerated_t2450067316;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_PhysicMaterialGenerated::.ctor()
extern "C"  void UnityEngine_PhysicMaterialGenerated__ctor_m783963767 (UnityEngine_PhysicMaterialGenerated_t2450067316 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicMaterialGenerated::PhysicMaterial_PhysicMaterial1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicMaterialGenerated_PhysicMaterial_PhysicMaterial1_m3655744379 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicMaterialGenerated::PhysicMaterial_PhysicMaterial2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PhysicMaterialGenerated_PhysicMaterial_PhysicMaterial2_m605541564 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicMaterialGenerated::PhysicMaterial_dynamicFriction(JSVCall)
extern "C"  void UnityEngine_PhysicMaterialGenerated_PhysicMaterial_dynamicFriction_m1100241325 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicMaterialGenerated::PhysicMaterial_staticFriction(JSVCall)
extern "C"  void UnityEngine_PhysicMaterialGenerated_PhysicMaterial_staticFriction_m996555422 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicMaterialGenerated::PhysicMaterial_bounciness(JSVCall)
extern "C"  void UnityEngine_PhysicMaterialGenerated_PhysicMaterial_bounciness_m1455042979 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicMaterialGenerated::PhysicMaterial_frictionCombine(JSVCall)
extern "C"  void UnityEngine_PhysicMaterialGenerated_PhysicMaterial_frictionCombine_m1693856417 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicMaterialGenerated::PhysicMaterial_bounceCombine(JSVCall)
extern "C"  void UnityEngine_PhysicMaterialGenerated_PhysicMaterial_bounceCombine_m1859119 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicMaterialGenerated::__Register()
extern "C"  void UnityEngine_PhysicMaterialGenerated___Register_m3875379376 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_PhysicMaterialGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_PhysicMaterialGenerated_ilo_getObject1_m3831045919 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PhysicMaterialGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_PhysicMaterialGenerated_ilo_attachFinalizerObject2_m3005482657 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicMaterialGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UnityEngine_PhysicMaterialGenerated_ilo_addJSCSRel3_m4261021365 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_PhysicMaterialGenerated::ilo_getSingle4(System.Int32)
extern "C"  float UnityEngine_PhysicMaterialGenerated_ilo_getSingle4_m1169716923 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PhysicMaterialGenerated::ilo_setEnum5(System.Int32,System.Int32)
extern "C"  void UnityEngine_PhysicMaterialGenerated_ilo_setEnum5_m1289020820 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_PhysicMaterialGenerated::ilo_getEnum6(System.Int32)
extern "C"  int32_t UnityEngine_PhysicMaterialGenerated_ilo_getEnum6_m1145784098 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
