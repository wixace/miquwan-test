﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Func`2<System.Reflection.ParameterInfo,System.Type>
struct Func_2_t4078497774;
// System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>>
struct Func_2_t1863747261;
// System.Func`3<System.Type,System.Reflection.MethodInfo,System.Reflection.MethodInfo>
struct Func_3_t3525172786;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.TypeExtensions
struct  TypeExtensions_t889461758  : public Il2CppObject
{
public:

public:
};

struct TypeExtensions_t889461758_StaticFields
{
public:
	// System.Func`2<System.Reflection.ParameterInfo,System.Type> Newtonsoft.Json.Utilities.TypeExtensions::<>f__am$cache0
	Func_2_t4078497774 * ___U3CU3Ef__amU24cache0_0;
	// System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>> Newtonsoft.Json.Utilities.TypeExtensions::<>f__am$cache1
	Func_2_t1863747261 * ___U3CU3Ef__amU24cache1_1;
	// System.Func`3<System.Type,System.Reflection.MethodInfo,System.Reflection.MethodInfo> Newtonsoft.Json.Utilities.TypeExtensions::<>f__am$cache2
	Func_3_t3525172786 * ___U3CU3Ef__amU24cache2_2;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_0() { return static_cast<int32_t>(offsetof(TypeExtensions_t889461758_StaticFields, ___U3CU3Ef__amU24cache0_0)); }
	inline Func_2_t4078497774 * get_U3CU3Ef__amU24cache0_0() const { return ___U3CU3Ef__amU24cache0_0; }
	inline Func_2_t4078497774 ** get_address_of_U3CU3Ef__amU24cache0_0() { return &___U3CU3Ef__amU24cache0_0; }
	inline void set_U3CU3Ef__amU24cache0_0(Func_2_t4078497774 * value)
	{
		___U3CU3Ef__amU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache0_0, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_1() { return static_cast<int32_t>(offsetof(TypeExtensions_t889461758_StaticFields, ___U3CU3Ef__amU24cache1_1)); }
	inline Func_2_t1863747261 * get_U3CU3Ef__amU24cache1_1() const { return ___U3CU3Ef__amU24cache1_1; }
	inline Func_2_t1863747261 ** get_address_of_U3CU3Ef__amU24cache1_1() { return &___U3CU3Ef__amU24cache1_1; }
	inline void set_U3CU3Ef__amU24cache1_1(Func_2_t1863747261 * value)
	{
		___U3CU3Ef__amU24cache1_1 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_1, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_2() { return static_cast<int32_t>(offsetof(TypeExtensions_t889461758_StaticFields, ___U3CU3Ef__amU24cache2_2)); }
	inline Func_3_t3525172786 * get_U3CU3Ef__amU24cache2_2() const { return ___U3CU3Ef__amU24cache2_2; }
	inline Func_3_t3525172786 ** get_address_of_U3CU3Ef__amU24cache2_2() { return &___U3CU3Ef__amU24cache2_2; }
	inline void set_U3CU3Ef__amU24cache2_2(Func_3_t3525172786 * value)
	{
		___U3CU3Ef__amU24cache2_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
