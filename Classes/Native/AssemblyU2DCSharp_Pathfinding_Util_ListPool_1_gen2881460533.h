﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.NavmeshAdd>>
struct List_1_t3037361287;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Util.ListPool`1<Pathfinding.NavmeshAdd>
struct  ListPool_1_t2881460533  : public Il2CppObject
{
public:

public:
};

struct ListPool_1_t2881460533_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Collections.Generic.List`1<T>> Pathfinding.Util.ListPool`1::pool
	List_1_t3037361287 * ___pool_1;

public:
	inline static int32_t get_offset_of_pool_1() { return static_cast<int32_t>(offsetof(ListPool_1_t2881460533_StaticFields, ___pool_1)); }
	inline List_1_t3037361287 * get_pool_1() const { return ___pool_1; }
	inline List_1_t3037361287 ** get_address_of_pool_1() { return &___pool_1; }
	inline void set_pool_1(List_1_t3037361287 * value)
	{
		___pool_1 = value;
		Il2CppCodeGenWriteBarrier(&___pool_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
