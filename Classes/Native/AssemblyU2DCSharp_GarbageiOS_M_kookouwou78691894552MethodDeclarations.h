﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_kookouwou78
struct M_kookouwou78_t691894552;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_kookouwou78691894552.h"

// System.Void GarbageiOS.M_kookouwou78::.ctor()
extern "C"  void M_kookouwou78__ctor_m2676473163 (M_kookouwou78_t691894552 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kookouwou78::M_mijair0(System.String[],System.Int32)
extern "C"  void M_kookouwou78_M_mijair0_m1102634050 (M_kookouwou78_t691894552 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kookouwou78::M_ninepi1(System.String[],System.Int32)
extern "C"  void M_kookouwou78_M_ninepi1_m2707385524 (M_kookouwou78_t691894552 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kookouwou78::M_xairelNave2(System.String[],System.Int32)
extern "C"  void M_kookouwou78_M_xairelNave2_m3430803685 (M_kookouwou78_t691894552 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_kookouwou78::ilo_M_mijair01(GarbageiOS.M_kookouwou78,System.String[],System.Int32)
extern "C"  void M_kookouwou78_ilo_M_mijair01_m1780309138 (Il2CppObject * __this /* static, unused */, M_kookouwou78_t691894552 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
