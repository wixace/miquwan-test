﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._7e7b48767e3eb88b91aab91b561d48eb
struct _7e7b48767e3eb88b91aab91b561d48eb_t3816314986;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._7e7b48767e3eb88b91aab91b561d48eb::.ctor()
extern "C"  void _7e7b48767e3eb88b91aab91b561d48eb__ctor_m2761336067 (_7e7b48767e3eb88b91aab91b561d48eb_t3816314986 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7e7b48767e3eb88b91aab91b561d48eb::_7e7b48767e3eb88b91aab91b561d48ebm2(System.Int32)
extern "C"  int32_t _7e7b48767e3eb88b91aab91b561d48eb__7e7b48767e3eb88b91aab91b561d48ebm2_m1894115673 (_7e7b48767e3eb88b91aab91b561d48eb_t3816314986 * __this, int32_t ____7e7b48767e3eb88b91aab91b561d48eba0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7e7b48767e3eb88b91aab91b561d48eb::_7e7b48767e3eb88b91aab91b561d48ebm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _7e7b48767e3eb88b91aab91b561d48eb__7e7b48767e3eb88b91aab91b561d48ebm_m2683106685 (_7e7b48767e3eb88b91aab91b561d48eb_t3816314986 * __this, int32_t ____7e7b48767e3eb88b91aab91b561d48eba0, int32_t ____7e7b48767e3eb88b91aab91b561d48eb61, int32_t ____7e7b48767e3eb88b91aab91b561d48ebc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
