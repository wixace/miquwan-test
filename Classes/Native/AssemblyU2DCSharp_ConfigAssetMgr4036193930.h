﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ResToABName
struct ResToABName_t627124519;
// UnityEngine.WWW
struct WWW_t3134621005;

#include "AssemblyU2DCSharp_Singleton_1_gen4289009323.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ConfigAssetMgr
struct  ConfigAssetMgr_t4036193930  : public Singleton_1_t4289009323
{
public:
	// ResToABName ConfigAssetMgr::r2abName
	ResToABName_t627124519 * ___r2abName_1;
	// UnityEngine.WWW ConfigAssetMgr::www
	WWW_t3134621005 * ___www_2;
	// System.Int32 ConfigAssetMgr::loadedCount
	int32_t ___loadedCount_3;
	// System.Int32 ConfigAssetMgr::totalLoadCount
	int32_t ___totalLoadCount_4;
	// System.Boolean ConfigAssetMgr::<isLoaded>k__BackingField
	bool ___U3CisLoadedU3Ek__BackingField_5;
	// System.Boolean ConfigAssetMgr::<isLoading>k__BackingField
	bool ___U3CisLoadingU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_r2abName_1() { return static_cast<int32_t>(offsetof(ConfigAssetMgr_t4036193930, ___r2abName_1)); }
	inline ResToABName_t627124519 * get_r2abName_1() const { return ___r2abName_1; }
	inline ResToABName_t627124519 ** get_address_of_r2abName_1() { return &___r2abName_1; }
	inline void set_r2abName_1(ResToABName_t627124519 * value)
	{
		___r2abName_1 = value;
		Il2CppCodeGenWriteBarrier(&___r2abName_1, value);
	}

	inline static int32_t get_offset_of_www_2() { return static_cast<int32_t>(offsetof(ConfigAssetMgr_t4036193930, ___www_2)); }
	inline WWW_t3134621005 * get_www_2() const { return ___www_2; }
	inline WWW_t3134621005 ** get_address_of_www_2() { return &___www_2; }
	inline void set_www_2(WWW_t3134621005 * value)
	{
		___www_2 = value;
		Il2CppCodeGenWriteBarrier(&___www_2, value);
	}

	inline static int32_t get_offset_of_loadedCount_3() { return static_cast<int32_t>(offsetof(ConfigAssetMgr_t4036193930, ___loadedCount_3)); }
	inline int32_t get_loadedCount_3() const { return ___loadedCount_3; }
	inline int32_t* get_address_of_loadedCount_3() { return &___loadedCount_3; }
	inline void set_loadedCount_3(int32_t value)
	{
		___loadedCount_3 = value;
	}

	inline static int32_t get_offset_of_totalLoadCount_4() { return static_cast<int32_t>(offsetof(ConfigAssetMgr_t4036193930, ___totalLoadCount_4)); }
	inline int32_t get_totalLoadCount_4() const { return ___totalLoadCount_4; }
	inline int32_t* get_address_of_totalLoadCount_4() { return &___totalLoadCount_4; }
	inline void set_totalLoadCount_4(int32_t value)
	{
		___totalLoadCount_4 = value;
	}

	inline static int32_t get_offset_of_U3CisLoadedU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ConfigAssetMgr_t4036193930, ___U3CisLoadedU3Ek__BackingField_5)); }
	inline bool get_U3CisLoadedU3Ek__BackingField_5() const { return ___U3CisLoadedU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CisLoadedU3Ek__BackingField_5() { return &___U3CisLoadedU3Ek__BackingField_5; }
	inline void set_U3CisLoadedU3Ek__BackingField_5(bool value)
	{
		___U3CisLoadedU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CisLoadingU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ConfigAssetMgr_t4036193930, ___U3CisLoadingU3Ek__BackingField_6)); }
	inline bool get_U3CisLoadingU3Ek__BackingField_6() const { return ___U3CisLoadingU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CisLoadingU3Ek__BackingField_6() { return &___U3CisLoadingU3Ek__BackingField_6; }
	inline void set_U3CisLoadingU3Ek__BackingField_6(bool value)
	{
		___U3CisLoadingU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
