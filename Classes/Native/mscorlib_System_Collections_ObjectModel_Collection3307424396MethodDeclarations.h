﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>
struct Collection_1_t3307424396;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// UnityEngine.MeshSubsetCombineUtility/MeshInstance[]
struct MeshInstanceU5BU5D_t1318761771;
// System.Collections.Generic.IEnumerator`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>
struct IEnumerator_1_t1738863991;
// System.Collections.Generic.IList`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>
struct IList_1_t2521646145;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_MeshSubsetCombineUtility_M4121966238.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::.ctor()
extern "C"  void Collection_1__ctor_m325228897_gshared (Collection_1_t3307424396 * __this, const MethodInfo* method);
#define Collection_1__ctor_m325228897(__this, method) ((  void (*) (Collection_1_t3307424396 *, const MethodInfo*))Collection_1__ctor_m325228897_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3813361146_gshared (Collection_1_t3307424396 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3813361146(__this, method) ((  bool (*) (Collection_1_t3307424396 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3813361146_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m181561091_gshared (Collection_1_t3307424396 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m181561091(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3307424396 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m181561091_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m450687038_gshared (Collection_1_t3307424396 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m450687038(__this, method) ((  Il2CppObject * (*) (Collection_1_t3307424396 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m450687038_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m1127005171_gshared (Collection_1_t3307424396 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m1127005171(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3307424396 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m1127005171_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1423624505_gshared (Collection_1_t3307424396 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1423624505(__this, ___value0, method) ((  bool (*) (Collection_1_t3307424396 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1423624505_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m3573006539_gshared (Collection_1_t3307424396 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m3573006539(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3307424396 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m3573006539_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m2276132726_gshared (Collection_1_t3307424396 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m2276132726(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3307424396 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m2276132726_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m3133389298_gshared (Collection_1_t3307424396 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m3133389298(__this, ___value0, method) ((  void (*) (Collection_1_t3307424396 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m3133389298_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m2685523843_gshared (Collection_1_t3307424396 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m2685523843(__this, method) ((  bool (*) (Collection_1_t3307424396 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m2685523843_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m4121175279_gshared (Collection_1_t3307424396 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m4121175279(__this, method) ((  Il2CppObject * (*) (Collection_1_t3307424396 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m4121175279_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m2619086696_gshared (Collection_1_t3307424396 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m2619086696(__this, method) ((  bool (*) (Collection_1_t3307424396 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m2619086696_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m677042833_gshared (Collection_1_t3307424396 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m677042833(__this, method) ((  bool (*) (Collection_1_t3307424396 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m677042833_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m1555207862_gshared (Collection_1_t3307424396 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1555207862(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3307424396 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1555207862_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m1839148493_gshared (Collection_1_t3307424396 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m1839148493(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3307424396 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m1839148493_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Add(T)
extern "C"  void Collection_1_Add_m3325769470_gshared (Collection_1_t3307424396 * __this, MeshInstance_t4121966238  ___item0, const MethodInfo* method);
#define Collection_1_Add_m3325769470(__this, ___item0, method) ((  void (*) (Collection_1_t3307424396 *, MeshInstance_t4121966238 , const MethodInfo*))Collection_1_Add_m3325769470_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Clear()
extern "C"  void Collection_1_Clear_m2026329484_gshared (Collection_1_t3307424396 * __this, const MethodInfo* method);
#define Collection_1_Clear_m2026329484(__this, method) ((  void (*) (Collection_1_t3307424396 *, const MethodInfo*))Collection_1_Clear_m2026329484_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::ClearItems()
extern "C"  void Collection_1_ClearItems_m1037558518_gshared (Collection_1_t3307424396 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m1037558518(__this, method) ((  void (*) (Collection_1_t3307424396 *, const MethodInfo*))Collection_1_ClearItems_m1037558518_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Contains(T)
extern "C"  bool Collection_1_Contains_m1975038010_gshared (Collection_1_t3307424396 * __this, MeshInstance_t4121966238  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m1975038010(__this, ___item0, method) ((  bool (*) (Collection_1_t3307424396 *, MeshInstance_t4121966238 , const MethodInfo*))Collection_1_Contains_m1975038010_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m2588711790_gshared (Collection_1_t3307424396 * __this, MeshInstanceU5BU5D_t1318761771* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m2588711790(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3307424396 *, MeshInstanceU5BU5D_t1318761771*, int32_t, const MethodInfo*))Collection_1_CopyTo_m2588711790_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m337071901_gshared (Collection_1_t3307424396 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m337071901(__this, method) ((  Il2CppObject* (*) (Collection_1_t3307424396 *, const MethodInfo*))Collection_1_GetEnumerator_m337071901_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m3175043186_gshared (Collection_1_t3307424396 * __this, MeshInstance_t4121966238  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m3175043186(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3307424396 *, MeshInstance_t4121966238 , const MethodInfo*))Collection_1_IndexOf_m3175043186_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m194426213_gshared (Collection_1_t3307424396 * __this, int32_t ___index0, MeshInstance_t4121966238  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m194426213(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3307424396 *, int32_t, MeshInstance_t4121966238 , const MethodInfo*))Collection_1_Insert_m194426213_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m2927274264_gshared (Collection_1_t3307424396 * __this, int32_t ___index0, MeshInstance_t4121966238  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m2927274264(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3307424396 *, int32_t, MeshInstance_t4121966238 , const MethodInfo*))Collection_1_InsertItem_m2927274264_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m339527328_gshared (Collection_1_t3307424396 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m339527328(__this, method) ((  Il2CppObject* (*) (Collection_1_t3307424396 *, const MethodInfo*))Collection_1_get_Items_m339527328_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Remove(T)
extern "C"  bool Collection_1_Remove_m2414884533_gshared (Collection_1_t3307424396 * __this, MeshInstance_t4121966238  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m2414884533(__this, ___item0, method) ((  bool (*) (Collection_1_t3307424396 *, MeshInstance_t4121966238 , const MethodInfo*))Collection_1_Remove_m2414884533_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m2363246379_gshared (Collection_1_t3307424396 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m2363246379(__this, ___index0, method) ((  void (*) (Collection_1_t3307424396 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m2363246379_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m1941695627_gshared (Collection_1_t3307424396 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m1941695627(__this, ___index0, method) ((  void (*) (Collection_1_t3307424396 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m1941695627_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3754365769_gshared (Collection_1_t3307424396 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3754365769(__this, method) ((  int32_t (*) (Collection_1_t3307424396 *, const MethodInfo*))Collection_1_get_Count_m3754365769_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::get_Item(System.Int32)
extern "C"  MeshInstance_t4121966238  Collection_1_get_Item_m3534740079_gshared (Collection_1_t3307424396 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m3534740079(__this, ___index0, method) ((  MeshInstance_t4121966238  (*) (Collection_1_t3307424396 *, int32_t, const MethodInfo*))Collection_1_get_Item_m3534740079_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m3969593596_gshared (Collection_1_t3307424396 * __this, int32_t ___index0, MeshInstance_t4121966238  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m3969593596(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3307424396 *, int32_t, MeshInstance_t4121966238 , const MethodInfo*))Collection_1_set_Item_m3969593596_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m3048835069_gshared (Collection_1_t3307424396 * __this, int32_t ___index0, MeshInstance_t4121966238  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m3048835069(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3307424396 *, int32_t, MeshInstance_t4121966238 , const MethodInfo*))Collection_1_SetItem_m3048835069_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m2048788146_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m2048788146(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m2048788146_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::ConvertItem(System.Object)
extern "C"  MeshInstance_t4121966238  Collection_1_ConvertItem_m1015593422_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m1015593422(__this /* static, unused */, ___item0, method) ((  MeshInstance_t4121966238  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m1015593422_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m731272238_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m731272238(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m731272238_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m30153938_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m30153938(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m30153938_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m2414455565_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m2414455565(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m2414455565_gshared)(__this /* static, unused */, ___list0, method)
