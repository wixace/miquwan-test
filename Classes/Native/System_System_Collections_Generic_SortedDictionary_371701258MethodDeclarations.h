﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.SortedDictionary`2<System.Int32,System.Int32>
struct SortedDictionary_2_t3586408994;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_SortedDictionary_371701258.h"

// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.SortedDictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m1005224741_gshared (Enumerator_t371701258 * __this, SortedDictionary_2_t3586408994 * ___dic0, const MethodInfo* method);
#define Enumerator__ctor_m1005224741(__this, ___dic0, method) ((  void (*) (Enumerator_t371701258 *, SortedDictionary_2_t3586408994 *, const MethodInfo*))Enumerator__ctor_m1005224741_gshared)(__this, ___dic0, method)
// System.Object System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1424458649_gshared (Enumerator_t371701258 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1424458649(__this, method) ((  Il2CppObject * (*) (Enumerator_t371701258 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1424458649_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3559322349_gshared (Enumerator_t371701258 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3559322349(__this, method) ((  void (*) (Enumerator_t371701258 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3559322349_gshared)(__this, method)
// TKey System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m4024291823_gshared (Enumerator_t371701258 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m4024291823(__this, method) ((  int32_t (*) (Enumerator_t371701258 *, const MethodInfo*))Enumerator_get_Current_m4024291823_gshared)(__this, method)
// System.Boolean System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1794199749_gshared (Enumerator_t371701258 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m1794199749(__this, method) ((  bool (*) (Enumerator_t371701258 *, const MethodInfo*))Enumerator_MoveNext_m1794199749_gshared)(__this, method)
// System.Void System.Collections.Generic.SortedDictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m1118681450_gshared (Enumerator_t371701258 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1118681450(__this, method) ((  void (*) (Enumerator_t371701258 *, const MethodInfo*))Enumerator_Dispose_m1118681450_gshared)(__this, method)
