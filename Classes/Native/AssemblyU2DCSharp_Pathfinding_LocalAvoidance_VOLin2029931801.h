﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.LocalAvoidance/VO
struct VO_t2172220293;

#include "mscorlib_System_ValueType1744280289.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.LocalAvoidance/VOLine
struct  VOLine_t2029931801 
{
public:
	// Pathfinding.LocalAvoidance/VO Pathfinding.LocalAvoidance/VOLine::vo
	VO_t2172220293 * ___vo_0;
	// UnityEngine.Vector3 Pathfinding.LocalAvoidance/VOLine::start
	Vector3_t4282066566  ___start_1;
	// UnityEngine.Vector3 Pathfinding.LocalAvoidance/VOLine::end
	Vector3_t4282066566  ___end_2;
	// System.Boolean Pathfinding.LocalAvoidance/VOLine::inf
	bool ___inf_3;
	// System.Int32 Pathfinding.LocalAvoidance/VOLine::id
	int32_t ___id_4;
	// System.Boolean Pathfinding.LocalAvoidance/VOLine::wrongSide
	bool ___wrongSide_5;

public:
	inline static int32_t get_offset_of_vo_0() { return static_cast<int32_t>(offsetof(VOLine_t2029931801, ___vo_0)); }
	inline VO_t2172220293 * get_vo_0() const { return ___vo_0; }
	inline VO_t2172220293 ** get_address_of_vo_0() { return &___vo_0; }
	inline void set_vo_0(VO_t2172220293 * value)
	{
		___vo_0 = value;
		Il2CppCodeGenWriteBarrier(&___vo_0, value);
	}

	inline static int32_t get_offset_of_start_1() { return static_cast<int32_t>(offsetof(VOLine_t2029931801, ___start_1)); }
	inline Vector3_t4282066566  get_start_1() const { return ___start_1; }
	inline Vector3_t4282066566 * get_address_of_start_1() { return &___start_1; }
	inline void set_start_1(Vector3_t4282066566  value)
	{
		___start_1 = value;
	}

	inline static int32_t get_offset_of_end_2() { return static_cast<int32_t>(offsetof(VOLine_t2029931801, ___end_2)); }
	inline Vector3_t4282066566  get_end_2() const { return ___end_2; }
	inline Vector3_t4282066566 * get_address_of_end_2() { return &___end_2; }
	inline void set_end_2(Vector3_t4282066566  value)
	{
		___end_2 = value;
	}

	inline static int32_t get_offset_of_inf_3() { return static_cast<int32_t>(offsetof(VOLine_t2029931801, ___inf_3)); }
	inline bool get_inf_3() const { return ___inf_3; }
	inline bool* get_address_of_inf_3() { return &___inf_3; }
	inline void set_inf_3(bool value)
	{
		___inf_3 = value;
	}

	inline static int32_t get_offset_of_id_4() { return static_cast<int32_t>(offsetof(VOLine_t2029931801, ___id_4)); }
	inline int32_t get_id_4() const { return ___id_4; }
	inline int32_t* get_address_of_id_4() { return &___id_4; }
	inline void set_id_4(int32_t value)
	{
		___id_4 = value;
	}

	inline static int32_t get_offset_of_wrongSide_5() { return static_cast<int32_t>(offsetof(VOLine_t2029931801, ___wrongSide_5)); }
	inline bool get_wrongSide_5() const { return ___wrongSide_5; }
	inline bool* get_address_of_wrongSide_5() { return &___wrongSide_5; }
	inline void set_wrongSide_5(bool value)
	{
		___wrongSide_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Pathfinding.LocalAvoidance/VOLine
struct VOLine_t2029931801_marshaled_pinvoke
{
	VO_t2172220293 * ___vo_0;
	Vector3_t4282066566_marshaled_pinvoke ___start_1;
	Vector3_t4282066566_marshaled_pinvoke ___end_2;
	int32_t ___inf_3;
	int32_t ___id_4;
	int32_t ___wrongSide_5;
};
// Native definition for marshalling of: Pathfinding.LocalAvoidance/VOLine
struct VOLine_t2029931801_marshaled_com
{
	VO_t2172220293 * ___vo_0;
	Vector3_t4282066566_marshaled_com ___start_1;
	Vector3_t4282066566_marshaled_com ___end_2;
	int32_t ___inf_3;
	int32_t ___id_4;
	int32_t ___wrongSide_5;
};
