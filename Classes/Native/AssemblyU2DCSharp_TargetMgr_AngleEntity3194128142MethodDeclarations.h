﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TargetMgr/AngleEntity
struct AngleEntity_t3194128142;

#include "codegen/il2cpp-codegen.h"

// System.Void TargetMgr/AngleEntity::.ctor()
extern "C"  void AngleEntity__ctor_m2851111453 (AngleEntity_t3194128142 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
