﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AstarPath/<GetNearest>c__AnonStorey108
struct U3CGetNearestU3Ec__AnonStorey108_t1269292067;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void AstarPath/<GetNearest>c__AnonStorey108::.ctor()
extern "C"  void U3CGetNearestU3Ec__AnonStorey108__ctor_m1904943064 (U3CGetNearestU3Ec__AnonStorey108_t1269292067 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AstarPath/<GetNearest>c__AnonStorey108::<>m__337(Pathfinding.GraphNode)
extern "C"  bool U3CGetNearestU3Ec__AnonStorey108_U3CU3Em__337_m628765504 (U3CGetNearestU3Ec__AnonStorey108_t1269292067 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
