﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.HingeJoint
struct HingeJoint_t1734115043;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_JointMotor1311955663.h"
#include "UnityEngine_UnityEngine_JointLimits3539604472.h"
#include "UnityEngine_UnityEngine_JointSpring3746621933.h"

// System.Void UnityEngine.HingeJoint::.ctor()
extern "C"  void HingeJoint__ctor_m4053721006 (HingeJoint_t1734115043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointMotor UnityEngine.HingeJoint::get_motor()
extern "C"  JointMotor_t1311955663  HingeJoint_get_motor_m3630651051 (HingeJoint_t1734115043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint::set_motor(UnityEngine.JointMotor)
extern "C"  void HingeJoint_set_motor_m3606147906 (HingeJoint_t1734115043 * __this, JointMotor_t1311955663  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint::INTERNAL_get_motor(UnityEngine.JointMotor&)
extern "C"  void HingeJoint_INTERNAL_get_motor_m1024697678 (HingeJoint_t1734115043 * __this, JointMotor_t1311955663 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint::INTERNAL_set_motor(UnityEngine.JointMotor&)
extern "C"  void HingeJoint_INTERNAL_set_motor_m445699778 (HingeJoint_t1734115043 * __this, JointMotor_t1311955663 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointLimits UnityEngine.HingeJoint::get_limits()
extern "C"  JointLimits_t3539604472  HingeJoint_get_limits_m3479048991 (HingeJoint_t1734115043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint::set_limits(UnityEngine.JointLimits)
extern "C"  void HingeJoint_set_limits_m1161805436 (HingeJoint_t1734115043 * __this, JointLimits_t3539604472  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint::INTERNAL_get_limits(UnityEngine.JointLimits&)
extern "C"  void HingeJoint_INTERNAL_get_limits_m2107945044 (HingeJoint_t1734115043 * __this, JointLimits_t3539604472 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint::INTERNAL_set_limits(UnityEngine.JointLimits&)
extern "C"  void HingeJoint_INTERNAL_set_limits_m4036711624 (HingeJoint_t1734115043 * __this, JointLimits_t3539604472 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.JointSpring UnityEngine.HingeJoint::get_spring()
extern "C"  JointSpring_t3746621933  HingeJoint_get_spring_m320958207 (HingeJoint_t1734115043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint::set_spring(UnityEngine.JointSpring)
extern "C"  void HingeJoint_set_spring_m3142778578 (HingeJoint_t1734115043 * __this, JointSpring_t3746621933  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint::INTERNAL_get_spring(UnityEngine.JointSpring&)
extern "C"  void HingeJoint_INTERNAL_get_spring_m3388570302 (HingeJoint_t1734115043 * __this, JointSpring_t3746621933 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint::INTERNAL_set_spring(UnityEngine.JointSpring&)
extern "C"  void HingeJoint_INTERNAL_set_spring_m1022369586 (HingeJoint_t1734115043 * __this, JointSpring_t3746621933 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.HingeJoint::get_useMotor()
extern "C"  bool HingeJoint_get_useMotor_m1242335571 (HingeJoint_t1734115043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint::set_useMotor(System.Boolean)
extern "C"  void HingeJoint_set_useMotor_m1343052312 (HingeJoint_t1734115043 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.HingeJoint::get_useLimits()
extern "C"  bool HingeJoint_get_useLimits_m1173985692 (HingeJoint_t1734115043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint::set_useLimits(System.Boolean)
extern "C"  void HingeJoint_set_useLimits_m495449925 (HingeJoint_t1734115043 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.HingeJoint::get_useSpring()
extern "C"  bool HingeJoint_get_useSpring_m2549270097 (HingeJoint_t1734115043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.HingeJoint::set_useSpring(System.Boolean)
extern "C"  void HingeJoint_set_useSpring_m1176912698 (HingeJoint_t1734115043 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.HingeJoint::get_velocity()
extern "C"  float HingeJoint_get_velocity_m2085773032 (HingeJoint_t1734115043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.HingeJoint::get_angle()
extern "C"  float HingeJoint_get_angle_m776644874 (HingeJoint_t1734115043 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
