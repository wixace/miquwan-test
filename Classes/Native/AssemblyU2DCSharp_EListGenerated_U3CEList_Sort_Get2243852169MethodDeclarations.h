﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// EListGenerated/<EList_Sort_GetDelegate_member1_arg1>c__AnonStorey5A`1<System.Object>
struct U3CEList_Sort_GetDelegate_member1_arg1U3Ec__AnonStorey5A_1_t2243852169;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void EListGenerated/<EList_Sort_GetDelegate_member1_arg1>c__AnonStorey5A`1<System.Object>::.ctor()
extern "C"  void U3CEList_Sort_GetDelegate_member1_arg1U3Ec__AnonStorey5A_1__ctor_m4243710648_gshared (U3CEList_Sort_GetDelegate_member1_arg1U3Ec__AnonStorey5A_1_t2243852169 * __this, const MethodInfo* method);
#define U3CEList_Sort_GetDelegate_member1_arg1U3Ec__AnonStorey5A_1__ctor_m4243710648(__this, method) ((  void (*) (U3CEList_Sort_GetDelegate_member1_arg1U3Ec__AnonStorey5A_1_t2243852169 *, const MethodInfo*))U3CEList_Sort_GetDelegate_member1_arg1U3Ec__AnonStorey5A_1__ctor_m4243710648_gshared)(__this, method)
// System.Int32 EListGenerated/<EList_Sort_GetDelegate_member1_arg1>c__AnonStorey5A`1<System.Object>::<>m__40(T,T)
extern "C"  int32_t U3CEList_Sort_GetDelegate_member1_arg1U3Ec__AnonStorey5A_1_U3CU3Em__40_m2340541927_gshared (U3CEList_Sort_GetDelegate_member1_arg1U3Ec__AnonStorey5A_1_t2243852169 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method);
#define U3CEList_Sort_GetDelegate_member1_arg1U3Ec__AnonStorey5A_1_U3CU3Em__40_m2340541927(__this, ___x0, ___y1, method) ((  int32_t (*) (U3CEList_Sort_GetDelegate_member1_arg1U3Ec__AnonStorey5A_1_t2243852169 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CEList_Sort_GetDelegate_member1_arg1U3Ec__AnonStorey5A_1_U3CU3Em__40_m2340541927_gshared)(__this, ___x0, ___y1, method)
