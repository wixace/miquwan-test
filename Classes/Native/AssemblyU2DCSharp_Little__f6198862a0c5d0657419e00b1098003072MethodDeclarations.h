﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._f6198862a0c5d0657419e00bf21db60b
struct _f6198862a0c5d0657419e00bf21db60b_t1098003072;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._f6198862a0c5d0657419e00bf21db60b::.ctor()
extern "C"  void _f6198862a0c5d0657419e00bf21db60b__ctor_m2002895789 (_f6198862a0c5d0657419e00bf21db60b_t1098003072 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f6198862a0c5d0657419e00bf21db60b::_f6198862a0c5d0657419e00bf21db60bm2(System.Int32)
extern "C"  int32_t _f6198862a0c5d0657419e00bf21db60b__f6198862a0c5d0657419e00bf21db60bm2_m1610482073 (_f6198862a0c5d0657419e00bf21db60b_t1098003072 * __this, int32_t ____f6198862a0c5d0657419e00bf21db60ba0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._f6198862a0c5d0657419e00bf21db60b::_f6198862a0c5d0657419e00bf21db60bm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _f6198862a0c5d0657419e00bf21db60b__f6198862a0c5d0657419e00bf21db60bm_m838116669 (_f6198862a0c5d0657419e00bf21db60b_t1098003072 * __this, int32_t ____f6198862a0c5d0657419e00bf21db60ba0, int32_t ____f6198862a0c5d0657419e00bf21db60b781, int32_t ____f6198862a0c5d0657419e00bf21db60bc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
