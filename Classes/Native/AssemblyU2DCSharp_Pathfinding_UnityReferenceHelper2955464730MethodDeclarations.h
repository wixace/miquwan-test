﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.UnityReferenceHelper
struct UnityReferenceHelper_t2955464730;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_UnityReferenceHelper2955464730.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_Guid3584625871.h"

// System.Void Pathfinding.UnityReferenceHelper::.ctor()
extern "C"  void UnityReferenceHelper__ctor_m2027756429 (UnityReferenceHelper_t2955464730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Pathfinding.UnityReferenceHelper::GetGUID()
extern "C"  String_t* UnityReferenceHelper_GetGUID_m9892679 (UnityReferenceHelper_t2955464730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.UnityReferenceHelper::Awake()
extern "C"  void UnityReferenceHelper_Awake_m2265361648 (UnityReferenceHelper_t2955464730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.UnityReferenceHelper::Reset()
extern "C"  void UnityReferenceHelper_Reset_m3969156666 (UnityReferenceHelper_t2955464730 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.UnityReferenceHelper::ilo_Reset1(Pathfinding.UnityReferenceHelper)
extern "C"  void UnityReferenceHelper_ilo_Reset1_m3393068542 (Il2CppObject * __this /* static, unused */, UnityReferenceHelper_t2955464730 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.Util.Guid Pathfinding.UnityReferenceHelper::ilo_NewGuid2()
extern "C"  Guid_t3584625871  UnityReferenceHelper_ilo_NewGuid2_m588491611 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
