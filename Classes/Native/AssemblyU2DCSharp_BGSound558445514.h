﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioSource
struct AudioSource_t1740077639;

#include "AssemblyU2DCSharp_ISound2170003014.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BGSound
struct  BGSound_t558445514  : public ISound_t2170003014
{
public:
	// UnityEngine.AudioSource BGSound::audioSource
	AudioSource_t1740077639 * ___audioSource_2;
	// System.Int32 BGSound::curID
	int32_t ___curID_3;
	// System.Boolean BGSound::isPlaying
	bool ___isPlaying_4;

public:
	inline static int32_t get_offset_of_audioSource_2() { return static_cast<int32_t>(offsetof(BGSound_t558445514, ___audioSource_2)); }
	inline AudioSource_t1740077639 * get_audioSource_2() const { return ___audioSource_2; }
	inline AudioSource_t1740077639 ** get_address_of_audioSource_2() { return &___audioSource_2; }
	inline void set_audioSource_2(AudioSource_t1740077639 * value)
	{
		___audioSource_2 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_2, value);
	}

	inline static int32_t get_offset_of_curID_3() { return static_cast<int32_t>(offsetof(BGSound_t558445514, ___curID_3)); }
	inline int32_t get_curID_3() const { return ___curID_3; }
	inline int32_t* get_address_of_curID_3() { return &___curID_3; }
	inline void set_curID_3(int32_t value)
	{
		___curID_3 = value;
	}

	inline static int32_t get_offset_of_isPlaying_4() { return static_cast<int32_t>(offsetof(BGSound_t558445514, ___isPlaying_4)); }
	inline bool get_isPlaying_4() const { return ___isPlaying_4; }
	inline bool* get_address_of_isPlaying_4() { return &___isPlaying_4; }
	inline void set_isPlaying_4(bool value)
	{
		___isPlaying_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
