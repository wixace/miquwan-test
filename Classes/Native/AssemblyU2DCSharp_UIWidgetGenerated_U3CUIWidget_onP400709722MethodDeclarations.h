﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIWidgetGenerated/<UIWidget_onPostFill_GetDelegate_member1_arg0>c__AnonStoreyD5
struct U3CUIWidget_onPostFill_GetDelegate_member1_arg0U3Ec__AnonStoreyD5_t400709722;
// UIWidget
struct UIWidget_t769069560;
// BetterList`1<UnityEngine.Vector3>
struct BetterList_1_t1484067282;
// BetterList`1<UnityEngine.Vector2>
struct BetterList_1_t1484067281;
// BetterList`1<UnityEngine.Color32>
struct BetterList_1_t2095821700;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"

// System.Void UIWidgetGenerated/<UIWidget_onPostFill_GetDelegate_member1_arg0>c__AnonStoreyD5::.ctor()
extern "C"  void U3CUIWidget_onPostFill_GetDelegate_member1_arg0U3Ec__AnonStoreyD5__ctor_m97449041 (U3CUIWidget_onPostFill_GetDelegate_member1_arg0U3Ec__AnonStoreyD5_t400709722 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated/<UIWidget_onPostFill_GetDelegate_member1_arg0>c__AnonStoreyD5::<>m__16F(UIWidget,System.Int32,BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void U3CUIWidget_onPostFill_GetDelegate_member1_arg0U3Ec__AnonStoreyD5_U3CU3Em__16F_m3103981173 (U3CUIWidget_onPostFill_GetDelegate_member1_arg0U3Ec__AnonStoreyD5_t400709722 * __this, UIWidget_t769069560 * ___widget0, int32_t ___bufferOffset1, BetterList_1_t1484067282 * ___verts2, BetterList_1_t1484067281 * ___uvs3, BetterList_1_t2095821700 * ___cols4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
