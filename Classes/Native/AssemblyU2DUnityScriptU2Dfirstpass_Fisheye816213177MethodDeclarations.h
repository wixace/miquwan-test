﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Fisheye
struct Fisheye_t816213177;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void Fisheye::.ctor()
extern "C"  void Fisheye__ctor_m1686144073 (Fisheye_t816213177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Fisheye::CheckResources()
extern "C"  bool Fisheye_CheckResources_m794321010 (Fisheye_t816213177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fisheye::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void Fisheye_OnRenderImage_m3798398581 (Fisheye_t816213177 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Fisheye::Main()
extern "C"  void Fisheye_Main_m2048096084 (Fisheye_t816213177 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
