﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// enemy_dropCfg
struct enemy_dropCfg_t1026960702;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_t200133079;
// CombatEntity
struct CombatEntity_t684137495;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnemyDropItem
struct  EnemyDropItem_t1468754474  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean EnemyDropItem::isFree
	bool ___isFree_2;
	// System.UInt32 EnemyDropItem::CreateEffectDelayId
	uint32_t ___CreateEffectDelayId_3;
	// UnityEngine.Vector3 EnemyDropItem::targetPos
	Vector3_t4282066566  ___targetPos_4;
	// enemy_dropCfg EnemyDropItem::_enemyDropCfg
	enemy_dropCfg_t1026960702 * ____enemyDropCfg_5;
	// System.String EnemyDropItem::_effect_name
	String_t* ____effect_name_6;
	// UnityEngine.GameObject EnemyDropItem::_effect_3d
	GameObject_t3674682005 * ____effect_3d_7;
	// System.Single EnemyDropItem::_num
	float ____num_8;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> EnemyDropItem::_effectDic
	Dictionary_2_t200133079 * ____effectDic_9;
	// CombatEntity EnemyDropItem::_hero_low_hp
	CombatEntity_t684137495 * ____hero_low_hp_10;
	// CombatEntity EnemyDropItem::_hero_main
	CombatEntity_t684137495 * ____hero_main_11;
	// System.Boolean EnemyDropItem::_isPlay
	bool ____isPlay_12;
	// System.Boolean EnemyDropItem::_isParabola
	bool ____isParabola_13;
	// System.Int32 EnemyDropItem::_index
	int32_t ____index_14;
	// System.Single EnemyDropItem::dis
	float ___dis_15;
	// System.UInt32 EnemyDropItem::_delayPlayId
	uint32_t ____delayPlayId_16;
	// System.Boolean EnemyDropItem::<IsCreatEffectFinish>k__BackingField
	bool ___U3CIsCreatEffectFinishU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_isFree_2() { return static_cast<int32_t>(offsetof(EnemyDropItem_t1468754474, ___isFree_2)); }
	inline bool get_isFree_2() const { return ___isFree_2; }
	inline bool* get_address_of_isFree_2() { return &___isFree_2; }
	inline void set_isFree_2(bool value)
	{
		___isFree_2 = value;
	}

	inline static int32_t get_offset_of_CreateEffectDelayId_3() { return static_cast<int32_t>(offsetof(EnemyDropItem_t1468754474, ___CreateEffectDelayId_3)); }
	inline uint32_t get_CreateEffectDelayId_3() const { return ___CreateEffectDelayId_3; }
	inline uint32_t* get_address_of_CreateEffectDelayId_3() { return &___CreateEffectDelayId_3; }
	inline void set_CreateEffectDelayId_3(uint32_t value)
	{
		___CreateEffectDelayId_3 = value;
	}

	inline static int32_t get_offset_of_targetPos_4() { return static_cast<int32_t>(offsetof(EnemyDropItem_t1468754474, ___targetPos_4)); }
	inline Vector3_t4282066566  get_targetPos_4() const { return ___targetPos_4; }
	inline Vector3_t4282066566 * get_address_of_targetPos_4() { return &___targetPos_4; }
	inline void set_targetPos_4(Vector3_t4282066566  value)
	{
		___targetPos_4 = value;
	}

	inline static int32_t get_offset_of__enemyDropCfg_5() { return static_cast<int32_t>(offsetof(EnemyDropItem_t1468754474, ____enemyDropCfg_5)); }
	inline enemy_dropCfg_t1026960702 * get__enemyDropCfg_5() const { return ____enemyDropCfg_5; }
	inline enemy_dropCfg_t1026960702 ** get_address_of__enemyDropCfg_5() { return &____enemyDropCfg_5; }
	inline void set__enemyDropCfg_5(enemy_dropCfg_t1026960702 * value)
	{
		____enemyDropCfg_5 = value;
		Il2CppCodeGenWriteBarrier(&____enemyDropCfg_5, value);
	}

	inline static int32_t get_offset_of__effect_name_6() { return static_cast<int32_t>(offsetof(EnemyDropItem_t1468754474, ____effect_name_6)); }
	inline String_t* get__effect_name_6() const { return ____effect_name_6; }
	inline String_t** get_address_of__effect_name_6() { return &____effect_name_6; }
	inline void set__effect_name_6(String_t* value)
	{
		____effect_name_6 = value;
		Il2CppCodeGenWriteBarrier(&____effect_name_6, value);
	}

	inline static int32_t get_offset_of__effect_3d_7() { return static_cast<int32_t>(offsetof(EnemyDropItem_t1468754474, ____effect_3d_7)); }
	inline GameObject_t3674682005 * get__effect_3d_7() const { return ____effect_3d_7; }
	inline GameObject_t3674682005 ** get_address_of__effect_3d_7() { return &____effect_3d_7; }
	inline void set__effect_3d_7(GameObject_t3674682005 * value)
	{
		____effect_3d_7 = value;
		Il2CppCodeGenWriteBarrier(&____effect_3d_7, value);
	}

	inline static int32_t get_offset_of__num_8() { return static_cast<int32_t>(offsetof(EnemyDropItem_t1468754474, ____num_8)); }
	inline float get__num_8() const { return ____num_8; }
	inline float* get_address_of__num_8() { return &____num_8; }
	inline void set__num_8(float value)
	{
		____num_8 = value;
	}

	inline static int32_t get_offset_of__effectDic_9() { return static_cast<int32_t>(offsetof(EnemyDropItem_t1468754474, ____effectDic_9)); }
	inline Dictionary_2_t200133079 * get__effectDic_9() const { return ____effectDic_9; }
	inline Dictionary_2_t200133079 ** get_address_of__effectDic_9() { return &____effectDic_9; }
	inline void set__effectDic_9(Dictionary_2_t200133079 * value)
	{
		____effectDic_9 = value;
		Il2CppCodeGenWriteBarrier(&____effectDic_9, value);
	}

	inline static int32_t get_offset_of__hero_low_hp_10() { return static_cast<int32_t>(offsetof(EnemyDropItem_t1468754474, ____hero_low_hp_10)); }
	inline CombatEntity_t684137495 * get__hero_low_hp_10() const { return ____hero_low_hp_10; }
	inline CombatEntity_t684137495 ** get_address_of__hero_low_hp_10() { return &____hero_low_hp_10; }
	inline void set__hero_low_hp_10(CombatEntity_t684137495 * value)
	{
		____hero_low_hp_10 = value;
		Il2CppCodeGenWriteBarrier(&____hero_low_hp_10, value);
	}

	inline static int32_t get_offset_of__hero_main_11() { return static_cast<int32_t>(offsetof(EnemyDropItem_t1468754474, ____hero_main_11)); }
	inline CombatEntity_t684137495 * get__hero_main_11() const { return ____hero_main_11; }
	inline CombatEntity_t684137495 ** get_address_of__hero_main_11() { return &____hero_main_11; }
	inline void set__hero_main_11(CombatEntity_t684137495 * value)
	{
		____hero_main_11 = value;
		Il2CppCodeGenWriteBarrier(&____hero_main_11, value);
	}

	inline static int32_t get_offset_of__isPlay_12() { return static_cast<int32_t>(offsetof(EnemyDropItem_t1468754474, ____isPlay_12)); }
	inline bool get__isPlay_12() const { return ____isPlay_12; }
	inline bool* get_address_of__isPlay_12() { return &____isPlay_12; }
	inline void set__isPlay_12(bool value)
	{
		____isPlay_12 = value;
	}

	inline static int32_t get_offset_of__isParabola_13() { return static_cast<int32_t>(offsetof(EnemyDropItem_t1468754474, ____isParabola_13)); }
	inline bool get__isParabola_13() const { return ____isParabola_13; }
	inline bool* get_address_of__isParabola_13() { return &____isParabola_13; }
	inline void set__isParabola_13(bool value)
	{
		____isParabola_13 = value;
	}

	inline static int32_t get_offset_of__index_14() { return static_cast<int32_t>(offsetof(EnemyDropItem_t1468754474, ____index_14)); }
	inline int32_t get__index_14() const { return ____index_14; }
	inline int32_t* get_address_of__index_14() { return &____index_14; }
	inline void set__index_14(int32_t value)
	{
		____index_14 = value;
	}

	inline static int32_t get_offset_of_dis_15() { return static_cast<int32_t>(offsetof(EnemyDropItem_t1468754474, ___dis_15)); }
	inline float get_dis_15() const { return ___dis_15; }
	inline float* get_address_of_dis_15() { return &___dis_15; }
	inline void set_dis_15(float value)
	{
		___dis_15 = value;
	}

	inline static int32_t get_offset_of__delayPlayId_16() { return static_cast<int32_t>(offsetof(EnemyDropItem_t1468754474, ____delayPlayId_16)); }
	inline uint32_t get__delayPlayId_16() const { return ____delayPlayId_16; }
	inline uint32_t* get_address_of__delayPlayId_16() { return &____delayPlayId_16; }
	inline void set__delayPlayId_16(uint32_t value)
	{
		____delayPlayId_16 = value;
	}

	inline static int32_t get_offset_of_U3CIsCreatEffectFinishU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(EnemyDropItem_t1468754474, ___U3CIsCreatEffectFinishU3Ek__BackingField_17)); }
	inline bool get_U3CIsCreatEffectFinishU3Ek__BackingField_17() const { return ___U3CIsCreatEffectFinishU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CIsCreatEffectFinishU3Ek__BackingField_17() { return &___U3CIsCreatEffectFinishU3Ek__BackingField_17; }
	inline void set_U3CIsCreatEffectFinishU3Ek__BackingField_17(bool value)
	{
		___U3CIsCreatEffectFinishU3Ek__BackingField_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
