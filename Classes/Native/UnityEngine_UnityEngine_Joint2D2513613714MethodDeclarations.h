﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Joint2D
struct Joint2D_t2513613714;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t1743771669;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rigidbody2D1743771669.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Joint2D2513613714.h"

// System.Void UnityEngine.Joint2D::.ctor()
extern "C"  void Joint2D__ctor_m1078408573 (Joint2D_t2513613714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rigidbody2D UnityEngine.Joint2D::get_connectedBody()
extern "C"  Rigidbody2D_t1743771669 * Joint2D_get_connectedBody_m4114977064 (Joint2D_t2513613714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint2D::set_connectedBody(UnityEngine.Rigidbody2D)
extern "C"  void Joint2D_set_connectedBody_m2661778977 (Joint2D_t2513613714 * __this, Rigidbody2D_t1743771669 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Joint2D::get_enableCollision()
extern "C"  bool Joint2D_get_enableCollision_m282509607 (Joint2D_t2513613714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint2D::set_enableCollision(System.Boolean)
extern "C"  void Joint2D_set_enableCollision_m1327650436 (Joint2D_t2513613714 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Joint2D::get_breakForce()
extern "C"  float Joint2D_get_breakForce_m3277162864 (Joint2D_t2513613714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint2D::set_breakForce(System.Single)
extern "C"  void Joint2D_set_breakForce_m2461889595 (Joint2D_t2513613714 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Joint2D::get_breakTorque()
extern "C"  float Joint2D_get_breakTorque_m1450806471 (Joint2D_t2513613714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint2D::set_breakTorque(System.Single)
extern "C"  void Joint2D_set_breakTorque_m3971500612 (Joint2D_t2513613714 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Joint2D::get_reactionForce()
extern "C"  Vector2_t4282066565  Joint2D_get_reactionForce_m2999776751 (Joint2D_t2513613714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Joint2D::get_reactionTorque()
extern "C"  float Joint2D_get_reactionTorque_m1288556183 (Joint2D_t2513613714 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Joint2D::GetReactionForce(System.Single)
extern "C"  Vector2_t4282066565  Joint2D_GetReactionForce_m4158905523 (Joint2D_t2513613714 * __this, float ___timeStep0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Joint2D::Joint2D_CUSTOM_INTERNAL_GetReactionForce(UnityEngine.Joint2D,System.Single,UnityEngine.Vector2&)
extern "C"  void Joint2D_Joint2D_CUSTOM_INTERNAL_GetReactionForce_m374589616 (Il2CppObject * __this /* static, unused */, Joint2D_t2513613714 * ___joint0, float ___timeStep1, Vector2_t4282066565 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Joint2D::GetReactionTorque(System.Single)
extern "C"  float Joint2D_GetReactionTorque_m2345656507 (Joint2D_t2513613714 * __this, float ___timeStep0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Joint2D::INTERNAL_CALL_GetReactionTorque(UnityEngine.Joint2D,System.Single)
extern "C"  float Joint2D_INTERNAL_CALL_GetReactionTorque_m2988236353 (Il2CppObject * __this /* static, unused */, Joint2D_t2513613714 * ___self0, float ___timeStep1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
