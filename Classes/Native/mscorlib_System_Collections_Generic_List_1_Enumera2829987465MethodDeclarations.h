﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Action`2<System.Byte[],System.String>>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1554335193(__this, ___l0, method) ((  void (*) (Enumerator_t2829987465 *, List_1_t2810314695 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Action`2<System.Byte[],System.String>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m3119106777(__this, method) ((  void (*) (Enumerator_t2829987465 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Action`2<System.Byte[],System.String>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m2094022031(__this, method) ((  Il2CppObject * (*) (Enumerator_t2829987465 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Action`2<System.Byte[],System.String>>::Dispose()
#define Enumerator_Dispose_m3633977854(__this, method) ((  void (*) (Enumerator_t2829987465 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Action`2<System.Byte[],System.String>>::VerifyState()
#define Enumerator_VerifyState_m3541346871(__this, method) ((  void (*) (Enumerator_t2829987465 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Action`2<System.Byte[],System.String>>::MoveNext()
#define Enumerator_MoveNext_m751189257(__this, method) ((  bool (*) (Enumerator_t2829987465 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Action`2<System.Byte[],System.String>>::get_Current()
#define Enumerator_get_Current_m2170720848(__this, method) ((  Action_2_t1442129143 * (*) (Enumerator_t2829987465 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
