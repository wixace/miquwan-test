﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.LayerGridGraph[]
struct LayerGridGraphU5BU5D_t628435936;

#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.LevelGridNode
struct  LevelGridNode_t489265774  : public GraphNode_t23612370
{
public:
	// System.UInt16 Pathfinding.LevelGridNode::gridFlags
	uint16_t ___gridFlags_24;
	// System.Int32 Pathfinding.LevelGridNode::nodeInGridIndex
	int32_t ___nodeInGridIndex_25;
	// System.UInt32 Pathfinding.LevelGridNode::gridConnections
	uint32_t ___gridConnections_26;

public:
	inline static int32_t get_offset_of_gridFlags_24() { return static_cast<int32_t>(offsetof(LevelGridNode_t489265774, ___gridFlags_24)); }
	inline uint16_t get_gridFlags_24() const { return ___gridFlags_24; }
	inline uint16_t* get_address_of_gridFlags_24() { return &___gridFlags_24; }
	inline void set_gridFlags_24(uint16_t value)
	{
		___gridFlags_24 = value;
	}

	inline static int32_t get_offset_of_nodeInGridIndex_25() { return static_cast<int32_t>(offsetof(LevelGridNode_t489265774, ___nodeInGridIndex_25)); }
	inline int32_t get_nodeInGridIndex_25() const { return ___nodeInGridIndex_25; }
	inline int32_t* get_address_of_nodeInGridIndex_25() { return &___nodeInGridIndex_25; }
	inline void set_nodeInGridIndex_25(int32_t value)
	{
		___nodeInGridIndex_25 = value;
	}

	inline static int32_t get_offset_of_gridConnections_26() { return static_cast<int32_t>(offsetof(LevelGridNode_t489265774, ___gridConnections_26)); }
	inline uint32_t get_gridConnections_26() const { return ___gridConnections_26; }
	inline uint32_t* get_address_of_gridConnections_26() { return &___gridConnections_26; }
	inline void set_gridConnections_26(uint32_t value)
	{
		___gridConnections_26 = value;
	}
};

struct LevelGridNode_t489265774_StaticFields
{
public:
	// Pathfinding.LayerGridGraph[] Pathfinding.LevelGridNode::_gridGraphs
	LayerGridGraphU5BU5D_t628435936* ____gridGraphs_23;
	// Pathfinding.LayerGridGraph[] Pathfinding.LevelGridNode::gridGraphs
	LayerGridGraphU5BU5D_t628435936* ___gridGraphs_27;

public:
	inline static int32_t get_offset_of__gridGraphs_23() { return static_cast<int32_t>(offsetof(LevelGridNode_t489265774_StaticFields, ____gridGraphs_23)); }
	inline LayerGridGraphU5BU5D_t628435936* get__gridGraphs_23() const { return ____gridGraphs_23; }
	inline LayerGridGraphU5BU5D_t628435936** get_address_of__gridGraphs_23() { return &____gridGraphs_23; }
	inline void set__gridGraphs_23(LayerGridGraphU5BU5D_t628435936* value)
	{
		____gridGraphs_23 = value;
		Il2CppCodeGenWriteBarrier(&____gridGraphs_23, value);
	}

	inline static int32_t get_offset_of_gridGraphs_27() { return static_cast<int32_t>(offsetof(LevelGridNode_t489265774_StaticFields, ___gridGraphs_27)); }
	inline LayerGridGraphU5BU5D_t628435936* get_gridGraphs_27() const { return ___gridGraphs_27; }
	inline LayerGridGraphU5BU5D_t628435936** get_address_of_gridGraphs_27() { return &___gridGraphs_27; }
	inline void set_gridGraphs_27(LayerGridGraphU5BU5D_t628435936* value)
	{
		___gridGraphs_27 = value;
		Il2CppCodeGenWriteBarrier(&___gridGraphs_27, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
