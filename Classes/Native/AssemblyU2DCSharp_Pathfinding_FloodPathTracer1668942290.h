﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.FloodPath
struct FloodPath_t3766979749;

#include "AssemblyU2DCSharp_Pathfinding_ABPath1187561148.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.FloodPathTracer
struct  FloodPathTracer_t1668942290  : public ABPath_t1187561148
{
public:
	// Pathfinding.FloodPath Pathfinding.FloodPathTracer::flood
	FloodPath_t3766979749 * ___flood_52;

public:
	inline static int32_t get_offset_of_flood_52() { return static_cast<int32_t>(offsetof(FloodPathTracer_t1668942290, ___flood_52)); }
	inline FloodPath_t3766979749 * get_flood_52() const { return ___flood_52; }
	inline FloodPath_t3766979749 ** get_address_of_flood_52() { return &___flood_52; }
	inline void set_flood_52(FloodPath_t3766979749 * value)
	{
		___flood_52 = value;
		Il2CppCodeGenWriteBarrier(&___flood_52, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
