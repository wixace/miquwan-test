﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._30e1c6a8b28f9ee4088e99226bf455b4
struct _30e1c6a8b28f9ee4088e99226bf455b4_t3983376134;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._30e1c6a8b28f9ee4088e99226bf455b4::.ctor()
extern "C"  void _30e1c6a8b28f9ee4088e99226bf455b4__ctor_m3251449319 (_30e1c6a8b28f9ee4088e99226bf455b4_t3983376134 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._30e1c6a8b28f9ee4088e99226bf455b4::_30e1c6a8b28f9ee4088e99226bf455b4m2(System.Int32)
extern "C"  int32_t _30e1c6a8b28f9ee4088e99226bf455b4__30e1c6a8b28f9ee4088e99226bf455b4m2_m2049278937 (_30e1c6a8b28f9ee4088e99226bf455b4_t3983376134 * __this, int32_t ____30e1c6a8b28f9ee4088e99226bf455b4a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._30e1c6a8b28f9ee4088e99226bf455b4::_30e1c6a8b28f9ee4088e99226bf455b4m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _30e1c6a8b28f9ee4088e99226bf455b4__30e1c6a8b28f9ee4088e99226bf455b4m_m3560450813 (_30e1c6a8b28f9ee4088e99226bf455b4_t3983376134 * __this, int32_t ____30e1c6a8b28f9ee4088e99226bf455b4a0, int32_t ____30e1c6a8b28f9ee4088e99226bf455b4301, int32_t ____30e1c6a8b28f9ee4088e99226bf455b4c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
