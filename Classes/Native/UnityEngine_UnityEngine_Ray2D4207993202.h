﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Ray2D
struct  Ray2D_t4207993202 
{
public:
	// UnityEngine.Vector2 UnityEngine.Ray2D::m_Origin
	Vector2_t4282066565  ___m_Origin_0;
	// UnityEngine.Vector2 UnityEngine.Ray2D::m_Direction
	Vector2_t4282066565  ___m_Direction_1;

public:
	inline static int32_t get_offset_of_m_Origin_0() { return static_cast<int32_t>(offsetof(Ray2D_t4207993202, ___m_Origin_0)); }
	inline Vector2_t4282066565  get_m_Origin_0() const { return ___m_Origin_0; }
	inline Vector2_t4282066565 * get_address_of_m_Origin_0() { return &___m_Origin_0; }
	inline void set_m_Origin_0(Vector2_t4282066565  value)
	{
		___m_Origin_0 = value;
	}

	inline static int32_t get_offset_of_m_Direction_1() { return static_cast<int32_t>(offsetof(Ray2D_t4207993202, ___m_Direction_1)); }
	inline Vector2_t4282066565  get_m_Direction_1() const { return ___m_Direction_1; }
	inline Vector2_t4282066565 * get_address_of_m_Direction_1() { return &___m_Direction_1; }
	inline void set_m_Direction_1(Vector2_t4282066565  value)
	{
		___m_Direction_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: UnityEngine.Ray2D
struct Ray2D_t4207993202_marshaled_pinvoke
{
	Vector2_t4282066565_marshaled_pinvoke ___m_Origin_0;
	Vector2_t4282066565_marshaled_pinvoke ___m_Direction_1;
};
// Native definition for marshalling of: UnityEngine.Ray2D
struct Ray2D_t4207993202_marshaled_com
{
	Vector2_t4282066565_marshaled_com ___m_Origin_0;
	Vector2_t4282066565_marshaled_com ___m_Direction_1;
};
