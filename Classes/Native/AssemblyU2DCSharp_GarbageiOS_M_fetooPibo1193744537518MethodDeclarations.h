﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_fetooPibo119
struct M_fetooPibo119_t3744537518;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_fetooPibo1193744537518.h"

// System.Void GarbageiOS.M_fetooPibo119::.ctor()
extern "C"  void M_fetooPibo119__ctor_m583359045 (M_fetooPibo119_t3744537518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fetooPibo119::M_kaiwaper0(System.String[],System.Int32)
extern "C"  void M_fetooPibo119_M_kaiwaper0_m1727501528 (M_fetooPibo119_t3744537518 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fetooPibo119::M_dracaimawHemawzay1(System.String[],System.Int32)
extern "C"  void M_fetooPibo119_M_dracaimawHemawzay1_m79475938 (M_fetooPibo119_t3744537518 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fetooPibo119::M_jarga2(System.String[],System.Int32)
extern "C"  void M_fetooPibo119_M_jarga2_m4149079173 (M_fetooPibo119_t3744537518 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fetooPibo119::M_yego3(System.String[],System.Int32)
extern "C"  void M_fetooPibo119_M_yego3_m4056159815 (M_fetooPibo119_t3744537518 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fetooPibo119::M_powwearbawNourisnou4(System.String[],System.Int32)
extern "C"  void M_fetooPibo119_M_powwearbawNourisnou4_m2204714065 (M_fetooPibo119_t3744537518 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fetooPibo119::M_steecuxiTecel5(System.String[],System.Int32)
extern "C"  void M_fetooPibo119_M_steecuxiTecel5_m1371736072 (M_fetooPibo119_t3744537518 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fetooPibo119::M_peastemcaTesouvor6(System.String[],System.Int32)
extern "C"  void M_fetooPibo119_M_peastemcaTesouvor6_m4170039162 (M_fetooPibo119_t3744537518 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fetooPibo119::ilo_M_dracaimawHemawzay11(GarbageiOS.M_fetooPibo119,System.String[],System.Int32)
extern "C"  void M_fetooPibo119_ilo_M_dracaimawHemawzay11_m3335749606 (Il2CppObject * __this /* static, unused */, M_fetooPibo119_t3744537518 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fetooPibo119::ilo_M_jarga22(GarbageiOS.M_fetooPibo119,System.String[],System.Int32)
extern "C"  void M_fetooPibo119_ilo_M_jarga22_m2657746660 (Il2CppObject * __this /* static, unused */, M_fetooPibo119_t3744537518 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_fetooPibo119::ilo_M_steecuxiTecel53(GarbageiOS.M_fetooPibo119,System.String[],System.Int32)
extern "C"  void M_fetooPibo119_ilo_M_steecuxiTecel53_m1460622722 (Il2CppObject * __this /* static, unused */, M_fetooPibo119_t3744537518 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
