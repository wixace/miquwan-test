﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.LineRenderer
struct LineRenderer_t1892709339;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_LineRenderer1892709339.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine.LineRenderer::.ctor()
extern "C"  void LineRenderer__ctor_m2125251958 (LineRenderer_t1892709339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::SetWidth(System.Single,System.Single)
extern "C"  void LineRenderer_SetWidth_m3078052350 (LineRenderer_t1892709339 * __this, float ___start0, float ___end1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::INTERNAL_CALL_SetWidth(UnityEngine.LineRenderer,System.Single,System.Single)
extern "C"  void LineRenderer_INTERNAL_CALL_SetWidth_m1482634509 (Il2CppObject * __this /* static, unused */, LineRenderer_t1892709339 * ___self0, float ___start1, float ___end2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::SetColors(UnityEngine.Color,UnityEngine.Color)
extern "C"  void LineRenderer_SetColors_m2531998784 (LineRenderer_t1892709339 * __this, Color_t4194546905  ___start0, Color_t4194546905  ___end1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::INTERNAL_CALL_SetColors(UnityEngine.LineRenderer,UnityEngine.Color&,UnityEngine.Color&)
extern "C"  void LineRenderer_INTERNAL_CALL_SetColors_m3942994585 (Il2CppObject * __this /* static, unused */, LineRenderer_t1892709339 * ___self0, Color_t4194546905 * ___start1, Color_t4194546905 * ___end2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::SetVertexCount(System.Int32)
extern "C"  void LineRenderer_SetVertexCount_m2812277096 (LineRenderer_t1892709339 * __this, int32_t ___count0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::INTERNAL_CALL_SetVertexCount(UnityEngine.LineRenderer,System.Int32)
extern "C"  void LineRenderer_INTERNAL_CALL_SetVertexCount_m3210243855 (Il2CppObject * __this /* static, unused */, LineRenderer_t1892709339 * ___self0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::SetPosition(System.Int32,UnityEngine.Vector3)
extern "C"  void LineRenderer_SetPosition_m2462184867 (LineRenderer_t1892709339 * __this, int32_t ___index0, Vector3_t4282066566  ___position1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::INTERNAL_CALL_SetPosition(UnityEngine.LineRenderer,System.Int32,UnityEngine.Vector3&)
extern "C"  void LineRenderer_INTERNAL_CALL_SetPosition_m436266356 (Il2CppObject * __this /* static, unused */, LineRenderer_t1892709339 * ___self0, int32_t ___index1, Vector3_t4282066566 * ___position2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::SetPositions(UnityEngine.Vector3[])
extern "C"  void LineRenderer_SetPositions_m386132545 (LineRenderer_t1892709339 * __this, Vector3U5BU5D_t215400611* ___positions0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.LineRenderer::get_useWorldSpace()
extern "C"  bool LineRenderer_get_useWorldSpace_m1531977952 (LineRenderer_t1892709339 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.LineRenderer::set_useWorldSpace(System.Boolean)
extern "C"  void LineRenderer_set_useWorldSpace_m1345789193 (LineRenderer_t1892709339 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
