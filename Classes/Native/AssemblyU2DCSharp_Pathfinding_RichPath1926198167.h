﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Pathfinding.RichPathPart>
struct List_1_t3889170682;
// Seeker
struct Seeker_t2472610117;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RichPath
struct  RichPath_t1926198167  : public Il2CppObject
{
public:
	// System.Int32 Pathfinding.RichPath::currentPart
	int32_t ___currentPart_0;
	// System.Collections.Generic.List`1<Pathfinding.RichPathPart> Pathfinding.RichPath::parts
	List_1_t3889170682 * ___parts_1;
	// Seeker Pathfinding.RichPath::seeker
	Seeker_t2472610117 * ___seeker_2;

public:
	inline static int32_t get_offset_of_currentPart_0() { return static_cast<int32_t>(offsetof(RichPath_t1926198167, ___currentPart_0)); }
	inline int32_t get_currentPart_0() const { return ___currentPart_0; }
	inline int32_t* get_address_of_currentPart_0() { return &___currentPart_0; }
	inline void set_currentPart_0(int32_t value)
	{
		___currentPart_0 = value;
	}

	inline static int32_t get_offset_of_parts_1() { return static_cast<int32_t>(offsetof(RichPath_t1926198167, ___parts_1)); }
	inline List_1_t3889170682 * get_parts_1() const { return ___parts_1; }
	inline List_1_t3889170682 ** get_address_of_parts_1() { return &___parts_1; }
	inline void set_parts_1(List_1_t3889170682 * value)
	{
		___parts_1 = value;
		Il2CppCodeGenWriteBarrier(&___parts_1, value);
	}

	inline static int32_t get_offset_of_seeker_2() { return static_cast<int32_t>(offsetof(RichPath_t1926198167, ___seeker_2)); }
	inline Seeker_t2472610117 * get_seeker_2() const { return ___seeker_2; }
	inline Seeker_t2472610117 ** get_address_of_seeker_2() { return &___seeker_2; }
	inline void set_seeker_2(Seeker_t2472610117 * value)
	{
		___seeker_2 = value;
		Il2CppCodeGenWriteBarrier(&___seeker_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
