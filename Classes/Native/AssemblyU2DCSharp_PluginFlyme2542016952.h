﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginFlyme
struct  PluginFlyme_t2542016952  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginFlyme::roleId
	String_t* ___roleId_2;
	// System.String PluginFlyme::roleName
	String_t* ___roleName_3;
	// System.String PluginFlyme::roleLevel
	String_t* ___roleLevel_4;
	// System.String PluginFlyme::serverId
	String_t* ___serverId_5;
	// System.String PluginFlyme::serverName
	String_t* ___serverName_6;
	// System.String PluginFlyme::tokenId
	String_t* ___tokenId_7;
	// System.String PluginFlyme::userName
	String_t* ___userName_8;
	// System.String PluginFlyme::session
	String_t* ___session_9;
	// System.String PluginFlyme::userId
	String_t* ___userId_10;
	// System.String PluginFlyme::channelId
	String_t* ___channelId_11;
	// System.Single PluginFlyme::lastSdkLoginOkTime
	float ___lastSdkLoginOkTime_12;

public:
	inline static int32_t get_offset_of_roleId_2() { return static_cast<int32_t>(offsetof(PluginFlyme_t2542016952, ___roleId_2)); }
	inline String_t* get_roleId_2() const { return ___roleId_2; }
	inline String_t** get_address_of_roleId_2() { return &___roleId_2; }
	inline void set_roleId_2(String_t* value)
	{
		___roleId_2 = value;
		Il2CppCodeGenWriteBarrier(&___roleId_2, value);
	}

	inline static int32_t get_offset_of_roleName_3() { return static_cast<int32_t>(offsetof(PluginFlyme_t2542016952, ___roleName_3)); }
	inline String_t* get_roleName_3() const { return ___roleName_3; }
	inline String_t** get_address_of_roleName_3() { return &___roleName_3; }
	inline void set_roleName_3(String_t* value)
	{
		___roleName_3 = value;
		Il2CppCodeGenWriteBarrier(&___roleName_3, value);
	}

	inline static int32_t get_offset_of_roleLevel_4() { return static_cast<int32_t>(offsetof(PluginFlyme_t2542016952, ___roleLevel_4)); }
	inline String_t* get_roleLevel_4() const { return ___roleLevel_4; }
	inline String_t** get_address_of_roleLevel_4() { return &___roleLevel_4; }
	inline void set_roleLevel_4(String_t* value)
	{
		___roleLevel_4 = value;
		Il2CppCodeGenWriteBarrier(&___roleLevel_4, value);
	}

	inline static int32_t get_offset_of_serverId_5() { return static_cast<int32_t>(offsetof(PluginFlyme_t2542016952, ___serverId_5)); }
	inline String_t* get_serverId_5() const { return ___serverId_5; }
	inline String_t** get_address_of_serverId_5() { return &___serverId_5; }
	inline void set_serverId_5(String_t* value)
	{
		___serverId_5 = value;
		Il2CppCodeGenWriteBarrier(&___serverId_5, value);
	}

	inline static int32_t get_offset_of_serverName_6() { return static_cast<int32_t>(offsetof(PluginFlyme_t2542016952, ___serverName_6)); }
	inline String_t* get_serverName_6() const { return ___serverName_6; }
	inline String_t** get_address_of_serverName_6() { return &___serverName_6; }
	inline void set_serverName_6(String_t* value)
	{
		___serverName_6 = value;
		Il2CppCodeGenWriteBarrier(&___serverName_6, value);
	}

	inline static int32_t get_offset_of_tokenId_7() { return static_cast<int32_t>(offsetof(PluginFlyme_t2542016952, ___tokenId_7)); }
	inline String_t* get_tokenId_7() const { return ___tokenId_7; }
	inline String_t** get_address_of_tokenId_7() { return &___tokenId_7; }
	inline void set_tokenId_7(String_t* value)
	{
		___tokenId_7 = value;
		Il2CppCodeGenWriteBarrier(&___tokenId_7, value);
	}

	inline static int32_t get_offset_of_userName_8() { return static_cast<int32_t>(offsetof(PluginFlyme_t2542016952, ___userName_8)); }
	inline String_t* get_userName_8() const { return ___userName_8; }
	inline String_t** get_address_of_userName_8() { return &___userName_8; }
	inline void set_userName_8(String_t* value)
	{
		___userName_8 = value;
		Il2CppCodeGenWriteBarrier(&___userName_8, value);
	}

	inline static int32_t get_offset_of_session_9() { return static_cast<int32_t>(offsetof(PluginFlyme_t2542016952, ___session_9)); }
	inline String_t* get_session_9() const { return ___session_9; }
	inline String_t** get_address_of_session_9() { return &___session_9; }
	inline void set_session_9(String_t* value)
	{
		___session_9 = value;
		Il2CppCodeGenWriteBarrier(&___session_9, value);
	}

	inline static int32_t get_offset_of_userId_10() { return static_cast<int32_t>(offsetof(PluginFlyme_t2542016952, ___userId_10)); }
	inline String_t* get_userId_10() const { return ___userId_10; }
	inline String_t** get_address_of_userId_10() { return &___userId_10; }
	inline void set_userId_10(String_t* value)
	{
		___userId_10 = value;
		Il2CppCodeGenWriteBarrier(&___userId_10, value);
	}

	inline static int32_t get_offset_of_channelId_11() { return static_cast<int32_t>(offsetof(PluginFlyme_t2542016952, ___channelId_11)); }
	inline String_t* get_channelId_11() const { return ___channelId_11; }
	inline String_t** get_address_of_channelId_11() { return &___channelId_11; }
	inline void set_channelId_11(String_t* value)
	{
		___channelId_11 = value;
		Il2CppCodeGenWriteBarrier(&___channelId_11, value);
	}

	inline static int32_t get_offset_of_lastSdkLoginOkTime_12() { return static_cast<int32_t>(offsetof(PluginFlyme_t2542016952, ___lastSdkLoginOkTime_12)); }
	inline float get_lastSdkLoginOkTime_12() const { return ___lastSdkLoginOkTime_12; }
	inline float* get_address_of_lastSdkLoginOkTime_12() { return &___lastSdkLoginOkTime_12; }
	inline void set_lastSdkLoginOkTime_12(float value)
	{
		___lastSdkLoginOkTime_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
