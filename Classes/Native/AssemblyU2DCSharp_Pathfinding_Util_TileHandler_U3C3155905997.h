﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Util.TileHandler/TileType
struct TileType_t2364590013;
// Pathfinding.Util.TileHandler
struct TileHandler_t1505605502;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Util.TileHandler/<LoadTile>c__AnonStorey122
struct  U3CLoadTileU3Ec__AnonStorey122_t3155905997  : public Il2CppObject
{
public:
	// System.Int32 Pathfinding.Util.TileHandler/<LoadTile>c__AnonStorey122::index
	int32_t ___index_0;
	// System.Int32 Pathfinding.Util.TileHandler/<LoadTile>c__AnonStorey122::yoffset
	int32_t ___yoffset_1;
	// System.Int32 Pathfinding.Util.TileHandler/<LoadTile>c__AnonStorey122::rotation
	int32_t ___rotation_2;
	// Pathfinding.Util.TileHandler/TileType Pathfinding.Util.TileHandler/<LoadTile>c__AnonStorey122::tile
	TileType_t2364590013 * ___tile_3;
	// System.Int32 Pathfinding.Util.TileHandler/<LoadTile>c__AnonStorey122::x
	int32_t ___x_4;
	// System.Int32 Pathfinding.Util.TileHandler/<LoadTile>c__AnonStorey122::z
	int32_t ___z_5;
	// Pathfinding.Util.TileHandler Pathfinding.Util.TileHandler/<LoadTile>c__AnonStorey122::<>f__this
	TileHandler_t1505605502 * ___U3CU3Ef__this_6;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(U3CLoadTileU3Ec__AnonStorey122_t3155905997, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_yoffset_1() { return static_cast<int32_t>(offsetof(U3CLoadTileU3Ec__AnonStorey122_t3155905997, ___yoffset_1)); }
	inline int32_t get_yoffset_1() const { return ___yoffset_1; }
	inline int32_t* get_address_of_yoffset_1() { return &___yoffset_1; }
	inline void set_yoffset_1(int32_t value)
	{
		___yoffset_1 = value;
	}

	inline static int32_t get_offset_of_rotation_2() { return static_cast<int32_t>(offsetof(U3CLoadTileU3Ec__AnonStorey122_t3155905997, ___rotation_2)); }
	inline int32_t get_rotation_2() const { return ___rotation_2; }
	inline int32_t* get_address_of_rotation_2() { return &___rotation_2; }
	inline void set_rotation_2(int32_t value)
	{
		___rotation_2 = value;
	}

	inline static int32_t get_offset_of_tile_3() { return static_cast<int32_t>(offsetof(U3CLoadTileU3Ec__AnonStorey122_t3155905997, ___tile_3)); }
	inline TileType_t2364590013 * get_tile_3() const { return ___tile_3; }
	inline TileType_t2364590013 ** get_address_of_tile_3() { return &___tile_3; }
	inline void set_tile_3(TileType_t2364590013 * value)
	{
		___tile_3 = value;
		Il2CppCodeGenWriteBarrier(&___tile_3, value);
	}

	inline static int32_t get_offset_of_x_4() { return static_cast<int32_t>(offsetof(U3CLoadTileU3Ec__AnonStorey122_t3155905997, ___x_4)); }
	inline int32_t get_x_4() const { return ___x_4; }
	inline int32_t* get_address_of_x_4() { return &___x_4; }
	inline void set_x_4(int32_t value)
	{
		___x_4 = value;
	}

	inline static int32_t get_offset_of_z_5() { return static_cast<int32_t>(offsetof(U3CLoadTileU3Ec__AnonStorey122_t3155905997, ___z_5)); }
	inline int32_t get_z_5() const { return ___z_5; }
	inline int32_t* get_address_of_z_5() { return &___z_5; }
	inline void set_z_5(int32_t value)
	{
		___z_5 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_6() { return static_cast<int32_t>(offsetof(U3CLoadTileU3Ec__AnonStorey122_t3155905997, ___U3CU3Ef__this_6)); }
	inline TileHandler_t1505605502 * get_U3CU3Ef__this_6() const { return ___U3CU3Ef__this_6; }
	inline TileHandler_t1505605502 ** get_address_of_U3CU3Ef__this_6() { return &___U3CU3Ef__this_6; }
	inline void set_U3CU3Ef__this_6(TileHandler_t1505605502 * value)
	{
		___U3CU3Ef__this_6 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
