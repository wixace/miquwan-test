﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.Net.UrlLoader
struct UrlLoader_t2490729496;

#include "AssemblyU2DCSharp_FlowControl_FlowBase3680091731.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlowControl.FlowIosFirst
struct  FlowIosFirst_t2537460645  : public FlowBase_t3680091731
{
public:
	// System.String FlowControl.FlowIosFirst::ncnUrl
	String_t* ___ncnUrl_7;
	// System.String FlowControl.FlowIosFirst::cnUrl
	String_t* ___cnUrl_8;
	// System.Boolean FlowControl.FlowIosFirst::isZHLL
	bool ___isZHLL_9;
	// System.Boolean FlowControl.FlowIosFirst::isZHLLReq
	bool ___isZHLLReq_10;
	// Mihua.Net.UrlLoader FlowControl.FlowIosFirst::req
	UrlLoader_t2490729496 * ___req_11;

public:
	inline static int32_t get_offset_of_ncnUrl_7() { return static_cast<int32_t>(offsetof(FlowIosFirst_t2537460645, ___ncnUrl_7)); }
	inline String_t* get_ncnUrl_7() const { return ___ncnUrl_7; }
	inline String_t** get_address_of_ncnUrl_7() { return &___ncnUrl_7; }
	inline void set_ncnUrl_7(String_t* value)
	{
		___ncnUrl_7 = value;
		Il2CppCodeGenWriteBarrier(&___ncnUrl_7, value);
	}

	inline static int32_t get_offset_of_cnUrl_8() { return static_cast<int32_t>(offsetof(FlowIosFirst_t2537460645, ___cnUrl_8)); }
	inline String_t* get_cnUrl_8() const { return ___cnUrl_8; }
	inline String_t** get_address_of_cnUrl_8() { return &___cnUrl_8; }
	inline void set_cnUrl_8(String_t* value)
	{
		___cnUrl_8 = value;
		Il2CppCodeGenWriteBarrier(&___cnUrl_8, value);
	}

	inline static int32_t get_offset_of_isZHLL_9() { return static_cast<int32_t>(offsetof(FlowIosFirst_t2537460645, ___isZHLL_9)); }
	inline bool get_isZHLL_9() const { return ___isZHLL_9; }
	inline bool* get_address_of_isZHLL_9() { return &___isZHLL_9; }
	inline void set_isZHLL_9(bool value)
	{
		___isZHLL_9 = value;
	}

	inline static int32_t get_offset_of_isZHLLReq_10() { return static_cast<int32_t>(offsetof(FlowIosFirst_t2537460645, ___isZHLLReq_10)); }
	inline bool get_isZHLLReq_10() const { return ___isZHLLReq_10; }
	inline bool* get_address_of_isZHLLReq_10() { return &___isZHLLReq_10; }
	inline void set_isZHLLReq_10(bool value)
	{
		___isZHLLReq_10 = value;
	}

	inline static int32_t get_offset_of_req_11() { return static_cast<int32_t>(offsetof(FlowIosFirst_t2537460645, ___req_11)); }
	inline UrlLoader_t2490729496 * get_req_11() const { return ___req_11; }
	inline UrlLoader_t2490729496 ** get_address_of_req_11() { return &___req_11; }
	inline void set_req_11(UrlLoader_t2490729496 * value)
	{
		___req_11 = value;
		Il2CppCodeGenWriteBarrier(&___req_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
