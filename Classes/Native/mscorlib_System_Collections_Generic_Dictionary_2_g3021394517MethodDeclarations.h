﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g4168079610MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::.ctor()
#define Dictionary_2__ctor_m4068978627(__this, method) ((  void (*) (Dictionary_2_t3021394517 *, const MethodInfo*))Dictionary_2__ctor_m1050910858_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m1628956227(__this, ___comparer0, method) ((  void (*) (Dictionary_2_t3021394517 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m3610002771_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
#define Dictionary_2__ctor_m3268728652(__this, ___dictionary0, method) ((  void (*) (Dictionary_2_t3021394517 *, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m256662076_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::.ctor(System.Int32)
#define Dictionary_2__ctor_m2021081373(__this, ___capacity0, method) ((  void (*) (Dictionary_2_t3021394517 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m3273912365_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m4078510129(__this, ___dictionary0, ___comparer1, method) ((  void (*) (Dictionary_2_t3021394517 *, Il2CppObject*, Il2CppObject*, const MethodInfo*))Dictionary_2__ctor_m1930793793_gshared)(__this, ___dictionary0, ___comparer1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m1347671309(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3021394517 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2__ctor_m1549788189_gshared)(__this, ___info0, ___context1, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1216286988(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3021394517 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m1428264956_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m1107915368(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3021394517 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m2955279704_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.IDictionary.get_Keys()
#define Dictionary_2_System_Collections_IDictionary_get_Keys_m2914083594(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3021394517 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Keys_m4198131226_gshared)(__this, method)
// System.Collections.ICollection System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.IDictionary.get_Values()
#define Dictionary_2_System_Collections_IDictionary_get_Values_m2483211640(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3021394517 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Values_m3797372040_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.IDictionary.get_IsFixedSize()
#define Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m2135462097(__this, method) ((  bool (*) (Dictionary_2_t3021394517 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsFixedSize_m3627613121_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.IDictionary.get_IsReadOnly()
#define Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1215631368(__this, method) ((  bool (*) (Dictionary_2_t3021394517 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_IsReadOnly_m1263765272_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m3626311346(__this, ___key0, method) ((  Il2CppObject * (*) (Dictionary_2_t3021394517 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m2843055522_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m3530165985(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3021394517 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m2020057553_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m617916112(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3021394517 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m2894265824_gshared)(__this, ___key0, ___value1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m2876690402(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3021394517 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m2093434578_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m3283118879(__this, ___key0, method) ((  void (*) (Dictionary_2_t3021394517 *, Il2CppObject *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m127000079_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m7715506(__this, method) ((  bool (*) (Dictionary_2_t3021394517 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m4062325186_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m3877812388(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3021394517 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m4065571764_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m272994422(__this, method) ((  bool (*) (Dictionary_2_t3021394517 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m840305542_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m3769154613(__this, ___keyValuePair0, method) ((  void (*) (Dictionary_2_t3021394517 *, KeyValuePair_2_t2920175223 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m2185230117_gshared)(__this, ___keyValuePair0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m2246937841(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3021394517 *, KeyValuePair_2_t2920175223 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m3713378305_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m2278877401(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3021394517 *, KeyValuePair_2U5BU5D_t1831945678*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m4220340169_gshared)(__this, ___array0, ___index1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m1219247190(__this, ___keyValuePair0, method) ((  bool (*) (Dictionary_2_t3021394517 *, KeyValuePair_2_t2920175223 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m3330268006_gshared)(__this, ___keyValuePair0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m2771275000(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3021394517 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m323672040_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m3233897223(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3021394517 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m464503287_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m4180855934(__this, method) ((  Il2CppObject* (*) (Dictionary_2_t3021394517 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m1289662318_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m1662089163(__this, method) ((  Il2CppObject * (*) (Dictionary_2_t3021394517 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m3187662523_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::get_Count()
#define Dictionary_2_get_Count_m580367445(__this, method) ((  int32_t (*) (Dictionary_2_t3021394517 *, const MethodInfo*))Dictionary_2_get_Count_m655926012_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::get_Item(TKey)
#define Dictionary_2_get_Item_m2156269185(__this, ___key0, method) ((  superskillCfg_t3024131278 * (*) (Dictionary_2_t3021394517 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m2361773736_gshared)(__this, ___key0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m1839857292(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3021394517 *, int32_t, superskillCfg_t3024131278 *, const MethodInfo*))Dictionary_2_set_Item_m1170572283_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m1019499844(__this, ___capacity0, ___hcp1, method) ((  void (*) (Dictionary_2_t3021394517 *, int32_t, Il2CppObject*, const MethodInfo*))Dictionary_2_Init_m3161627732_gshared)(__this, ___capacity0, ___hcp1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m2079067891(__this, ___size0, method) ((  void (*) (Dictionary_2_t3021394517 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m3089254883_gshared)(__this, ___size0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m1166296495(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3021394517 *, Il2CppArray *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m3741359263_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m2515891267(__this /* static, unused */, ___key0, ___value1, method) ((  KeyValuePair_2_t2920175223  (*) (Il2CppObject * /* static, unused */, int32_t, superskillCfg_t3024131278 *, const MethodInfo*))Dictionary_2_make_pair_m2338171699_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::pick_key(TKey,TValue)
#define Dictionary_2_pick_key_m15011355(__this /* static, unused */, ___key0, ___value1, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, int32_t, superskillCfg_t3024131278 *, const MethodInfo*))Dictionary_2_pick_key_m1394751787_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m2495386807(__this /* static, unused */, ___key0, ___value1, method) ((  superskillCfg_t3024131278 * (*) (Il2CppObject * /* static, unused */, int32_t, superskillCfg_t3024131278 *, const MethodInfo*))Dictionary_2_pick_value_m1281047495_gshared)(__this /* static, unused */, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m3879311168(__this, ___array0, ___index1, method) ((  void (*) (Dictionary_2_t3021394517 *, KeyValuePair_2U5BU5D_t1831945678*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m2503627344_gshared)(__this, ___array0, ___index1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::Resize()
#define Dictionary_2_Resize_m4270441452(__this, method) ((  void (*) (Dictionary_2_t3021394517 *, const MethodInfo*))Dictionary_2_Resize_m1861476060_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::Add(TKey,TValue)
#define Dictionary_2_Add_m2114141571(__this, ___key0, ___value1, method) ((  void (*) (Dictionary_2_t3021394517 *, int32_t, superskillCfg_t3024131278 *, const MethodInfo*))Dictionary_2_Add_m2232043353_gshared)(__this, ___key0, ___value1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::Clear()
#define Dictionary_2_Clear_m2529729015(__this, method) ((  void (*) (Dictionary_2_t3021394517 *, const MethodInfo*))Dictionary_2_Clear_m3560399111_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m2418537734(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3021394517 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m2612169713_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m3369555041(__this, ___value0, method) ((  bool (*) (Dictionary_2_t3021394517 *, superskillCfg_t3024131278 *, const MethodInfo*))Dictionary_2_ContainsValue_m454328177_gshared)(__this, ___value0, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m1164558186(__this, ___info0, ___context1, method) ((  void (*) (Dictionary_2_t3021394517 *, SerializationInfo_t2185721892 *, StreamingContext_t2761351129 , const MethodInfo*))Dictionary_2_GetObjectData_m3426598522_gshared)(__this, ___info0, ___context1, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m3320897850(__this, ___sender0, method) ((  void (*) (Dictionary_2_t3021394517 *, Il2CppObject *, const MethodInfo*))Dictionary_2_OnDeserialization_m3983879210_gshared)(__this, ___sender0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::Remove(TKey)
#define Dictionary_2_Remove_m1676482415(__this, ___key0, method) ((  bool (*) (Dictionary_2_t3021394517 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m183515743_gshared)(__this, ___key0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m1264533562(__this, ___key0, ___value1, method) ((  bool (*) (Dictionary_2_t3021394517 *, int32_t, superskillCfg_t3024131278 **, const MethodInfo*))Dictionary_2_TryGetValue_m2256091851_gshared)(__this, ___key0, ___value1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::get_Keys()
#define Dictionary_2_get_Keys_m4149083809(__this, method) ((  KeyCollection_t353186672 * (*) (Dictionary_2_t3021394517 *, const MethodInfo*))Dictionary_2_get_Keys_m4120714641_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::get_Values()
#define Dictionary_2_get_Values_m3308052861(__this, method) ((  ValueCollection_t1722000230 * (*) (Dictionary_2_t3021394517 *, const MethodInfo*))Dictionary_2_get_Values_m1815086189_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m3759837558(__this, ___key0, method) ((  int32_t (*) (Dictionary_2_t3021394517 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTKey_m844610694_gshared)(__this, ___key0, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m807700946(__this, ___value0, method) ((  superskillCfg_t3024131278 * (*) (Dictionary_2_t3021394517 *, Il2CppObject *, const MethodInfo*))Dictionary_2_ToTValue_m3888328930_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m2816167250(__this, ___pair0, method) ((  bool (*) (Dictionary_2_t3021394517 *, KeyValuePair_2_t2920175223 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m139391042_gshared)(__this, ___pair0, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m3314900285(__this, method) ((  Enumerator_t43750613  (*) (Dictionary_2_t3021394517 *, const MethodInfo*))Dictionary_2_GetEnumerator_m3720989159_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,superskillCfg>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m1686078350(__this /* static, unused */, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Il2CppObject * /* static, unused */, int32_t, superskillCfg_t3024131278 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m2937104030_gshared)(__this /* static, unused */, ___key0, ___value1, method)
