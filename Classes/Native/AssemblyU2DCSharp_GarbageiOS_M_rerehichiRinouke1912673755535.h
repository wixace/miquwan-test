﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_rerehichiRinouke191
struct  M_rerehichiRinouke191_t2673755535  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_rerehichiRinouke191::_ciporstar
	uint32_t ____ciporstar_0;
	// System.Int32 GarbageiOS.M_rerehichiRinouke191::_sizatem
	int32_t ____sizatem_1;
	// System.Single GarbageiOS.M_rerehichiRinouke191::_terrelere
	float ____terrelere_2;
	// System.String GarbageiOS.M_rerehichiRinouke191::_hesaDoqapa
	String_t* ____hesaDoqapa_3;
	// System.String GarbageiOS.M_rerehichiRinouke191::_turma
	String_t* ____turma_4;
	// System.Int32 GarbageiOS.M_rerehichiRinouke191::_maiwoutra
	int32_t ____maiwoutra_5;
	// System.UInt32 GarbageiOS.M_rerehichiRinouke191::_sorwha
	uint32_t ____sorwha_6;
	// System.Int32 GarbageiOS.M_rerehichiRinouke191::_rirzisDayca
	int32_t ____rirzisDayca_7;
	// System.Int32 GarbageiOS.M_rerehichiRinouke191::_kaspepa
	int32_t ____kaspepa_8;
	// System.String GarbageiOS.M_rerehichiRinouke191::_herehel
	String_t* ____herehel_9;
	// System.Int32 GarbageiOS.M_rerehichiRinouke191::_berecayNoukall
	int32_t ____berecayNoukall_10;
	// System.Single GarbageiOS.M_rerehichiRinouke191::_citalro
	float ____citalro_11;
	// System.String GarbageiOS.M_rerehichiRinouke191::_keta
	String_t* ____keta_12;
	// System.Int32 GarbageiOS.M_rerehichiRinouke191::_semelyearSawai
	int32_t ____semelyearSawai_13;
	// System.Single GarbageiOS.M_rerehichiRinouke191::_chirorDirli
	float ____chirorDirli_14;
	// System.Single GarbageiOS.M_rerehichiRinouke191::_surle
	float ____surle_15;
	// System.UInt32 GarbageiOS.M_rerehichiRinouke191::_radetaWaireser
	uint32_t ____radetaWaireser_16;
	// System.UInt32 GarbageiOS.M_rerehichiRinouke191::_beare
	uint32_t ____beare_17;
	// System.String GarbageiOS.M_rerehichiRinouke191::_mebarCepisall
	String_t* ____mebarCepisall_18;
	// System.UInt32 GarbageiOS.M_rerehichiRinouke191::_jemnasHapijee
	uint32_t ____jemnasHapijee_19;
	// System.Int32 GarbageiOS.M_rerehichiRinouke191::_dowzur
	int32_t ____dowzur_20;
	// System.UInt32 GarbageiOS.M_rerehichiRinouke191::_tretir
	uint32_t ____tretir_21;
	// System.UInt32 GarbageiOS.M_rerehichiRinouke191::_poutelsaNosere
	uint32_t ____poutelsaNosere_22;
	// System.Boolean GarbageiOS.M_rerehichiRinouke191::_remtawwhallWihearsir
	bool ____remtawwhallWihearsir_23;
	// System.Int32 GarbageiOS.M_rerehichiRinouke191::_wotuJocinoo
	int32_t ____wotuJocinoo_24;
	// System.Int32 GarbageiOS.M_rerehichiRinouke191::_kusezai
	int32_t ____kusezai_25;
	// System.Single GarbageiOS.M_rerehichiRinouke191::_callsalldouWhukal
	float ____callsalldouWhukal_26;
	// System.Int32 GarbageiOS.M_rerehichiRinouke191::_nawrerstePifay
	int32_t ____nawrerstePifay_27;
	// System.Int32 GarbageiOS.M_rerehichiRinouke191::_cedai
	int32_t ____cedai_28;
	// System.Int32 GarbageiOS.M_rerehichiRinouke191::_tiyoocaCheabegis
	int32_t ____tiyoocaCheabegis_29;
	// System.String GarbageiOS.M_rerehichiRinouke191::_zuretair
	String_t* ____zuretair_30;
	// System.Boolean GarbageiOS.M_rerehichiRinouke191::_cuwurLasi
	bool ____cuwurLasi_31;

public:
	inline static int32_t get_offset_of__ciporstar_0() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____ciporstar_0)); }
	inline uint32_t get__ciporstar_0() const { return ____ciporstar_0; }
	inline uint32_t* get_address_of__ciporstar_0() { return &____ciporstar_0; }
	inline void set__ciporstar_0(uint32_t value)
	{
		____ciporstar_0 = value;
	}

	inline static int32_t get_offset_of__sizatem_1() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____sizatem_1)); }
	inline int32_t get__sizatem_1() const { return ____sizatem_1; }
	inline int32_t* get_address_of__sizatem_1() { return &____sizatem_1; }
	inline void set__sizatem_1(int32_t value)
	{
		____sizatem_1 = value;
	}

	inline static int32_t get_offset_of__terrelere_2() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____terrelere_2)); }
	inline float get__terrelere_2() const { return ____terrelere_2; }
	inline float* get_address_of__terrelere_2() { return &____terrelere_2; }
	inline void set__terrelere_2(float value)
	{
		____terrelere_2 = value;
	}

	inline static int32_t get_offset_of__hesaDoqapa_3() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____hesaDoqapa_3)); }
	inline String_t* get__hesaDoqapa_3() const { return ____hesaDoqapa_3; }
	inline String_t** get_address_of__hesaDoqapa_3() { return &____hesaDoqapa_3; }
	inline void set__hesaDoqapa_3(String_t* value)
	{
		____hesaDoqapa_3 = value;
		Il2CppCodeGenWriteBarrier(&____hesaDoqapa_3, value);
	}

	inline static int32_t get_offset_of__turma_4() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____turma_4)); }
	inline String_t* get__turma_4() const { return ____turma_4; }
	inline String_t** get_address_of__turma_4() { return &____turma_4; }
	inline void set__turma_4(String_t* value)
	{
		____turma_4 = value;
		Il2CppCodeGenWriteBarrier(&____turma_4, value);
	}

	inline static int32_t get_offset_of__maiwoutra_5() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____maiwoutra_5)); }
	inline int32_t get__maiwoutra_5() const { return ____maiwoutra_5; }
	inline int32_t* get_address_of__maiwoutra_5() { return &____maiwoutra_5; }
	inline void set__maiwoutra_5(int32_t value)
	{
		____maiwoutra_5 = value;
	}

	inline static int32_t get_offset_of__sorwha_6() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____sorwha_6)); }
	inline uint32_t get__sorwha_6() const { return ____sorwha_6; }
	inline uint32_t* get_address_of__sorwha_6() { return &____sorwha_6; }
	inline void set__sorwha_6(uint32_t value)
	{
		____sorwha_6 = value;
	}

	inline static int32_t get_offset_of__rirzisDayca_7() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____rirzisDayca_7)); }
	inline int32_t get__rirzisDayca_7() const { return ____rirzisDayca_7; }
	inline int32_t* get_address_of__rirzisDayca_7() { return &____rirzisDayca_7; }
	inline void set__rirzisDayca_7(int32_t value)
	{
		____rirzisDayca_7 = value;
	}

	inline static int32_t get_offset_of__kaspepa_8() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____kaspepa_8)); }
	inline int32_t get__kaspepa_8() const { return ____kaspepa_8; }
	inline int32_t* get_address_of__kaspepa_8() { return &____kaspepa_8; }
	inline void set__kaspepa_8(int32_t value)
	{
		____kaspepa_8 = value;
	}

	inline static int32_t get_offset_of__herehel_9() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____herehel_9)); }
	inline String_t* get__herehel_9() const { return ____herehel_9; }
	inline String_t** get_address_of__herehel_9() { return &____herehel_9; }
	inline void set__herehel_9(String_t* value)
	{
		____herehel_9 = value;
		Il2CppCodeGenWriteBarrier(&____herehel_9, value);
	}

	inline static int32_t get_offset_of__berecayNoukall_10() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____berecayNoukall_10)); }
	inline int32_t get__berecayNoukall_10() const { return ____berecayNoukall_10; }
	inline int32_t* get_address_of__berecayNoukall_10() { return &____berecayNoukall_10; }
	inline void set__berecayNoukall_10(int32_t value)
	{
		____berecayNoukall_10 = value;
	}

	inline static int32_t get_offset_of__citalro_11() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____citalro_11)); }
	inline float get__citalro_11() const { return ____citalro_11; }
	inline float* get_address_of__citalro_11() { return &____citalro_11; }
	inline void set__citalro_11(float value)
	{
		____citalro_11 = value;
	}

	inline static int32_t get_offset_of__keta_12() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____keta_12)); }
	inline String_t* get__keta_12() const { return ____keta_12; }
	inline String_t** get_address_of__keta_12() { return &____keta_12; }
	inline void set__keta_12(String_t* value)
	{
		____keta_12 = value;
		Il2CppCodeGenWriteBarrier(&____keta_12, value);
	}

	inline static int32_t get_offset_of__semelyearSawai_13() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____semelyearSawai_13)); }
	inline int32_t get__semelyearSawai_13() const { return ____semelyearSawai_13; }
	inline int32_t* get_address_of__semelyearSawai_13() { return &____semelyearSawai_13; }
	inline void set__semelyearSawai_13(int32_t value)
	{
		____semelyearSawai_13 = value;
	}

	inline static int32_t get_offset_of__chirorDirli_14() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____chirorDirli_14)); }
	inline float get__chirorDirli_14() const { return ____chirorDirli_14; }
	inline float* get_address_of__chirorDirli_14() { return &____chirorDirli_14; }
	inline void set__chirorDirli_14(float value)
	{
		____chirorDirli_14 = value;
	}

	inline static int32_t get_offset_of__surle_15() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____surle_15)); }
	inline float get__surle_15() const { return ____surle_15; }
	inline float* get_address_of__surle_15() { return &____surle_15; }
	inline void set__surle_15(float value)
	{
		____surle_15 = value;
	}

	inline static int32_t get_offset_of__radetaWaireser_16() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____radetaWaireser_16)); }
	inline uint32_t get__radetaWaireser_16() const { return ____radetaWaireser_16; }
	inline uint32_t* get_address_of__radetaWaireser_16() { return &____radetaWaireser_16; }
	inline void set__radetaWaireser_16(uint32_t value)
	{
		____radetaWaireser_16 = value;
	}

	inline static int32_t get_offset_of__beare_17() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____beare_17)); }
	inline uint32_t get__beare_17() const { return ____beare_17; }
	inline uint32_t* get_address_of__beare_17() { return &____beare_17; }
	inline void set__beare_17(uint32_t value)
	{
		____beare_17 = value;
	}

	inline static int32_t get_offset_of__mebarCepisall_18() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____mebarCepisall_18)); }
	inline String_t* get__mebarCepisall_18() const { return ____mebarCepisall_18; }
	inline String_t** get_address_of__mebarCepisall_18() { return &____mebarCepisall_18; }
	inline void set__mebarCepisall_18(String_t* value)
	{
		____mebarCepisall_18 = value;
		Il2CppCodeGenWriteBarrier(&____mebarCepisall_18, value);
	}

	inline static int32_t get_offset_of__jemnasHapijee_19() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____jemnasHapijee_19)); }
	inline uint32_t get__jemnasHapijee_19() const { return ____jemnasHapijee_19; }
	inline uint32_t* get_address_of__jemnasHapijee_19() { return &____jemnasHapijee_19; }
	inline void set__jemnasHapijee_19(uint32_t value)
	{
		____jemnasHapijee_19 = value;
	}

	inline static int32_t get_offset_of__dowzur_20() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____dowzur_20)); }
	inline int32_t get__dowzur_20() const { return ____dowzur_20; }
	inline int32_t* get_address_of__dowzur_20() { return &____dowzur_20; }
	inline void set__dowzur_20(int32_t value)
	{
		____dowzur_20 = value;
	}

	inline static int32_t get_offset_of__tretir_21() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____tretir_21)); }
	inline uint32_t get__tretir_21() const { return ____tretir_21; }
	inline uint32_t* get_address_of__tretir_21() { return &____tretir_21; }
	inline void set__tretir_21(uint32_t value)
	{
		____tretir_21 = value;
	}

	inline static int32_t get_offset_of__poutelsaNosere_22() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____poutelsaNosere_22)); }
	inline uint32_t get__poutelsaNosere_22() const { return ____poutelsaNosere_22; }
	inline uint32_t* get_address_of__poutelsaNosere_22() { return &____poutelsaNosere_22; }
	inline void set__poutelsaNosere_22(uint32_t value)
	{
		____poutelsaNosere_22 = value;
	}

	inline static int32_t get_offset_of__remtawwhallWihearsir_23() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____remtawwhallWihearsir_23)); }
	inline bool get__remtawwhallWihearsir_23() const { return ____remtawwhallWihearsir_23; }
	inline bool* get_address_of__remtawwhallWihearsir_23() { return &____remtawwhallWihearsir_23; }
	inline void set__remtawwhallWihearsir_23(bool value)
	{
		____remtawwhallWihearsir_23 = value;
	}

	inline static int32_t get_offset_of__wotuJocinoo_24() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____wotuJocinoo_24)); }
	inline int32_t get__wotuJocinoo_24() const { return ____wotuJocinoo_24; }
	inline int32_t* get_address_of__wotuJocinoo_24() { return &____wotuJocinoo_24; }
	inline void set__wotuJocinoo_24(int32_t value)
	{
		____wotuJocinoo_24 = value;
	}

	inline static int32_t get_offset_of__kusezai_25() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____kusezai_25)); }
	inline int32_t get__kusezai_25() const { return ____kusezai_25; }
	inline int32_t* get_address_of__kusezai_25() { return &____kusezai_25; }
	inline void set__kusezai_25(int32_t value)
	{
		____kusezai_25 = value;
	}

	inline static int32_t get_offset_of__callsalldouWhukal_26() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____callsalldouWhukal_26)); }
	inline float get__callsalldouWhukal_26() const { return ____callsalldouWhukal_26; }
	inline float* get_address_of__callsalldouWhukal_26() { return &____callsalldouWhukal_26; }
	inline void set__callsalldouWhukal_26(float value)
	{
		____callsalldouWhukal_26 = value;
	}

	inline static int32_t get_offset_of__nawrerstePifay_27() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____nawrerstePifay_27)); }
	inline int32_t get__nawrerstePifay_27() const { return ____nawrerstePifay_27; }
	inline int32_t* get_address_of__nawrerstePifay_27() { return &____nawrerstePifay_27; }
	inline void set__nawrerstePifay_27(int32_t value)
	{
		____nawrerstePifay_27 = value;
	}

	inline static int32_t get_offset_of__cedai_28() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____cedai_28)); }
	inline int32_t get__cedai_28() const { return ____cedai_28; }
	inline int32_t* get_address_of__cedai_28() { return &____cedai_28; }
	inline void set__cedai_28(int32_t value)
	{
		____cedai_28 = value;
	}

	inline static int32_t get_offset_of__tiyoocaCheabegis_29() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____tiyoocaCheabegis_29)); }
	inline int32_t get__tiyoocaCheabegis_29() const { return ____tiyoocaCheabegis_29; }
	inline int32_t* get_address_of__tiyoocaCheabegis_29() { return &____tiyoocaCheabegis_29; }
	inline void set__tiyoocaCheabegis_29(int32_t value)
	{
		____tiyoocaCheabegis_29 = value;
	}

	inline static int32_t get_offset_of__zuretair_30() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____zuretair_30)); }
	inline String_t* get__zuretair_30() const { return ____zuretair_30; }
	inline String_t** get_address_of__zuretair_30() { return &____zuretair_30; }
	inline void set__zuretair_30(String_t* value)
	{
		____zuretair_30 = value;
		Il2CppCodeGenWriteBarrier(&____zuretair_30, value);
	}

	inline static int32_t get_offset_of__cuwurLasi_31() { return static_cast<int32_t>(offsetof(M_rerehichiRinouke191_t2673755535, ____cuwurLasi_31)); }
	inline bool get__cuwurLasi_31() const { return ____cuwurLasi_31; }
	inline bool* get_address_of__cuwurLasi_31() { return &____cuwurLasi_31; }
	inline void set__cuwurLasi_31(bool value)
	{
		____cuwurLasi_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
