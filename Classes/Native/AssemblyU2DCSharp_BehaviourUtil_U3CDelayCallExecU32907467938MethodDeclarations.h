﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Single,System.Single,System.Single>
struct U3CDelayCallExecU3Ec__Iterator41_3_t2907467938;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Single,System.Single,System.Single>::.ctor()
extern "C"  void U3CDelayCallExecU3Ec__Iterator41_3__ctor_m1273100119_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t2907467938 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3__ctor_m1273100119(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator41_3_t2907467938 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3__ctor_m1273100119_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Single,System.Single,System.Single>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4264634203_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t2907467938 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4264634203(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator41_3_t2907467938 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m4264634203_gshared)(__this, method)
// System.Object BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Single,System.Single,System.Single>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_IEnumerator_get_Current_m656097519_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t2907467938 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_IEnumerator_get_Current_m656097519(__this, method) ((  Il2CppObject * (*) (U3CDelayCallExecU3Ec__Iterator41_3_t2907467938 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_System_Collections_IEnumerator_get_Current_m656097519_gshared)(__this, method)
// System.Boolean BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Single,System.Single,System.Single>::MoveNext()
extern "C"  bool U3CDelayCallExecU3Ec__Iterator41_3_MoveNext_m3462233917_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t2907467938 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_MoveNext_m3462233917(__this, method) ((  bool (*) (U3CDelayCallExecU3Ec__Iterator41_3_t2907467938 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_MoveNext_m3462233917_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Single,System.Single,System.Single>::Dispose()
extern "C"  void U3CDelayCallExecU3Ec__Iterator41_3_Dispose_m3576785364_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t2907467938 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_Dispose_m3576785364(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator41_3_t2907467938 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_Dispose_m3576785364_gshared)(__this, method)
// System.Void BehaviourUtil/<DelayCallExec>c__Iterator41`3<System.Single,System.Single,System.Single>::Reset()
extern "C"  void U3CDelayCallExecU3Ec__Iterator41_3_Reset_m3214500356_gshared (U3CDelayCallExecU3Ec__Iterator41_3_t2907467938 * __this, const MethodInfo* method);
#define U3CDelayCallExecU3Ec__Iterator41_3_Reset_m3214500356(__this, method) ((  void (*) (U3CDelayCallExecU3Ec__Iterator41_3_t2907467938 *, const MethodInfo*))U3CDelayCallExecU3Ec__Iterator41_3_Reset_m3214500356_gshared)(__this, method)
