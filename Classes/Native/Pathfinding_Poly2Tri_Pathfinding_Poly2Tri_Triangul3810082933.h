﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Pathfinding.Poly2Tri.DTSweepConstraint>
struct List_1_t433497279;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Poly2Tri.TriangulationPoint
struct  TriangulationPoint_t3810082933  : public Il2CppObject
{
public:
	// System.Double Pathfinding.Poly2Tri.TriangulationPoint::X
	double ___X_0;
	// System.Double Pathfinding.Poly2Tri.TriangulationPoint::Y
	double ___Y_1;
	// System.Collections.Generic.List`1<Pathfinding.Poly2Tri.DTSweepConstraint> Pathfinding.Poly2Tri.TriangulationPoint::<Edges>k__BackingField
	List_1_t433497279 * ___U3CEdgesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_X_0() { return static_cast<int32_t>(offsetof(TriangulationPoint_t3810082933, ___X_0)); }
	inline double get_X_0() const { return ___X_0; }
	inline double* get_address_of_X_0() { return &___X_0; }
	inline void set_X_0(double value)
	{
		___X_0 = value;
	}

	inline static int32_t get_offset_of_Y_1() { return static_cast<int32_t>(offsetof(TriangulationPoint_t3810082933, ___Y_1)); }
	inline double get_Y_1() const { return ___Y_1; }
	inline double* get_address_of_Y_1() { return &___Y_1; }
	inline void set_Y_1(double value)
	{
		___Y_1 = value;
	}

	inline static int32_t get_offset_of_U3CEdgesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(TriangulationPoint_t3810082933, ___U3CEdgesU3Ek__BackingField_2)); }
	inline List_1_t433497279 * get_U3CEdgesU3Ek__BackingField_2() const { return ___U3CEdgesU3Ek__BackingField_2; }
	inline List_1_t433497279 ** get_address_of_U3CEdgesU3Ek__BackingField_2() { return &___U3CEdgesU3Ek__BackingField_2; }
	inline void set_U3CEdgesU3Ek__BackingField_2(List_1_t433497279 * value)
	{
		___U3CEdgesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier(&___U3CEdgesU3Ek__BackingField_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
