﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Monster2901270458.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Boss
struct  Boss_t2076557  : public Monster_t2901270458
{
public:
	// System.Single Boss::baseTime
	float ___baseTime_338;
	// System.Boolean Boss::IsShowHp
	bool ___IsShowHp_339;

public:
	inline static int32_t get_offset_of_baseTime_338() { return static_cast<int32_t>(offsetof(Boss_t2076557, ___baseTime_338)); }
	inline float get_baseTime_338() const { return ___baseTime_338; }
	inline float* get_address_of_baseTime_338() { return &___baseTime_338; }
	inline void set_baseTime_338(float value)
	{
		___baseTime_338 = value;
	}

	inline static int32_t get_offset_of_IsShowHp_339() { return static_cast<int32_t>(offsetof(Boss_t2076557, ___IsShowHp_339)); }
	inline bool get_IsShowHp_339() const { return ___IsShowHp_339; }
	inline bool* get_address_of_IsShowHp_339() { return &___IsShowHp_339; }
	inline void set_IsShowHp_339(bool value)
	{
		___IsShowHp_339 = value;
	}
};

struct Boss_t2076557_StaticFields
{
public:
	// System.Single Boss::CheckDamageTime
	float ___CheckDamageTime_337;

public:
	inline static int32_t get_offset_of_CheckDamageTime_337() { return static_cast<int32_t>(offsetof(Boss_t2076557_StaticFields, ___CheckDamageTime_337)); }
	inline float get_CheckDamageTime_337() const { return ___CheckDamageTime_337; }
	inline float* get_address_of_CheckDamageTime_337() { return &___CheckDamageTime_337; }
	inline void set_CheckDamageTime_337(float value)
	{
		___CheckDamageTime_337 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
