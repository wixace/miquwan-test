﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_TextEditorGenerated
struct UnityEngine_TextEditorGenerated_t4027589453;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "mscorlib_System_String7231557.h"

// System.Void UnityEngine_TextEditorGenerated::.ctor()
extern "C"  void UnityEngine_TextEditorGenerated__ctor_m2623224510 (UnityEngine_TextEditorGenerated_t4027589453 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_TextEditor1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_TextEditor1_m219829748 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextEditorGenerated::TextEditor_keyboardOnScreen(JSVCall)
extern "C"  void UnityEngine_TextEditorGenerated_TextEditor_keyboardOnScreen_m2120373652 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextEditorGenerated::TextEditor_controlID(JSVCall)
extern "C"  void UnityEngine_TextEditorGenerated_TextEditor_controlID_m3247905102 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextEditorGenerated::TextEditor_style(JSVCall)
extern "C"  void UnityEngine_TextEditorGenerated_TextEditor_style_m2634432245 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextEditorGenerated::TextEditor_multiline(JSVCall)
extern "C"  void UnityEngine_TextEditorGenerated_TextEditor_multiline_m3208427385 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextEditorGenerated::TextEditor_hasHorizontalCursorPos(JSVCall)
extern "C"  void UnityEngine_TextEditorGenerated_TextEditor_hasHorizontalCursorPos_m1744812838 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextEditorGenerated::TextEditor_isPasswordField(JSVCall)
extern "C"  void UnityEngine_TextEditorGenerated_TextEditor_isPasswordField_m1148835601 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextEditorGenerated::TextEditor_scrollOffset(JSVCall)
extern "C"  void UnityEngine_TextEditorGenerated_TextEditor_scrollOffset_m3263407046 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextEditorGenerated::TextEditor_graphicalCursorPos(JSVCall)
extern "C"  void UnityEngine_TextEditorGenerated_TextEditor_graphicalCursorPos_m892140187 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextEditorGenerated::TextEditor_graphicalSelectCursorPos(JSVCall)
extern "C"  void UnityEngine_TextEditorGenerated_TextEditor_graphicalSelectCursorPos_m343949943 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextEditorGenerated::TextEditor_text(JSVCall)
extern "C"  void UnityEngine_TextEditorGenerated_TextEditor_text_m3848540921 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextEditorGenerated::TextEditor_position(JSVCall)
extern "C"  void UnityEngine_TextEditorGenerated_TextEditor_position_m1550613117 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextEditorGenerated::TextEditor_cursorIndex(JSVCall)
extern "C"  void UnityEngine_TextEditorGenerated_TextEditor_cursorIndex_m360544746 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextEditorGenerated::TextEditor_selectIndex(JSVCall)
extern "C"  void UnityEngine_TextEditorGenerated_TextEditor_selectIndex_m3508519184 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextEditorGenerated::TextEditor_hasSelection(JSVCall)
extern "C"  void UnityEngine_TextEditorGenerated_TextEditor_hasSelection_m1785724340 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextEditorGenerated::TextEditor_SelectedText(JSVCall)
extern "C"  void UnityEngine_TextEditorGenerated_TextEditor_SelectedText_m2260268030 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_Backspace(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_Backspace_m3921829116 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_CanPaste(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_CanPaste_m1670487880 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_Copy(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_Copy_m766279322 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_Cut(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_Cut_m2678450047 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_DblClickSnap__DblClickSnapping(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_DblClickSnap__DblClickSnapping_m1354552735 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_Delete(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_Delete_m1288340304 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_DeleteLineBack(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_DeleteLineBack_m1352020267 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_DeleteSelection(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_DeleteSelection_m1083391518 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_DeleteWordBack(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_DeleteWordBack_m3654547649 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_DeleteWordForward(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_DeleteWordForward_m1718050669 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_DetectFocusChange(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_DetectFocusChange_m3765986306 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_DrawCursor__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_DrawCursor__String_m2496513904 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_ExpandSelectGraphicalLineEnd(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_ExpandSelectGraphicalLineEnd_m2588120431 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_ExpandSelectGraphicalLineStart(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_ExpandSelectGraphicalLineStart_m2572614902 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_FindStartOfNextWord__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_FindStartOfNextWord__Int32_m4134975606 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_HandleKeyEvent__Event(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_HandleKeyEvent__Event_m1891732468 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_Insert__Char(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_Insert__Char_m3066770164 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_IsOverSelection__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_IsOverSelection__Vector2_m4269799718 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_MouseDragSelectsWholeWords__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_MouseDragSelectsWholeWords__Boolean_m2021859285 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_MoveAltCursorToPosition__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_MoveAltCursorToPosition__Vector2_m575556034 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_MoveCursorToPosition__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_MoveCursorToPosition__Vector2_m4258280897 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_MoveDown(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_MoveDown_m809557688 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_MoveGraphicalLineEnd(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_MoveGraphicalLineEnd_m3126605002 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_MoveGraphicalLineStart(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_MoveGraphicalLineStart_m365244817 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_MoveLeft(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_MoveLeft_m372740189 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_MoveLineEnd(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_MoveLineEnd_m3123942771 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_MoveLineStart(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_MoveLineStart_m2101808122 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_MoveParagraphBackward(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_MoveParagraphBackward_m4233823069 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_MoveParagraphForward(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_MoveParagraphForward_m4169541197 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_MoveRight(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_MoveRight_m854149192 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_MoveSelectionToAltCursor(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_MoveSelectionToAltCursor_m3483997326 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_MoveTextEnd(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_MoveTextEnd_m2589270618 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_MoveTextStart(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_MoveTextStart_m3677944609 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_MoveToEndOfPreviousWord(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_MoveToEndOfPreviousWord_m3954131076 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_MoveToStartOfNextWord(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_MoveToStartOfNextWord_m1509379591 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_MoveUp(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_MoveUp_m3216831793 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_MoveWordLeft(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_MoveWordLeft_m3817083015 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_MoveWordRight(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_MoveWordRight_m254594398 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_OnFocus(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_OnFocus_m943355446 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_OnLostFocus(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_OnLostFocus_m1121341938 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_Paste(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_Paste_m226838032 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_ReplaceSelection__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_ReplaceSelection__String_m3831539566 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_SaveBackup(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_SaveBackup_m641308228 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_SelectAll(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_SelectAll_m4001827586 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_SelectCurrentParagraph(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_SelectCurrentParagraph_m4177586742 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_SelectCurrentWord(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_SelectCurrentWord_m3583862788 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_SelectDown(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_SelectDown_m4196680483 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_SelectGraphicalLineEnd(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_SelectGraphicalLineEnd_m2802667701 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_SelectGraphicalLineStart(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_SelectGraphicalLineStart_m2594111164 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_SelectLeft(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_SelectLeft_m3759862984 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_SelectNone(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_SelectNone_m2496993593 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_SelectParagraphBackward(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_SelectParagraphBackward_m2781701330 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_SelectParagraphForward(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_SelectParagraphForward_m3845603896 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_SelectRight(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_SelectRight_m2775740733 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_SelectTextEnd(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_SelectTextEnd_m2402804239 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_SelectTextStart(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_SelectTextStart_m577413526 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_SelectToEndOfPreviousWord(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_SelectToEndOfPreviousWord_m34543801 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_SelectToPosition__Vector2(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_SelectToPosition__Vector2_m153360620 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_SelectToStartOfNextWord(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_SelectToStartOfNextWord_m57257852 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_SelectUp(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_SelectUp_m427063388 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_SelectWordLeft(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_SelectWordLeft_m2331592562 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_SelectWordRight(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_SelectWordRight_m1449030611 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_Undo(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_Undo_m348216009 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::TextEditor_UpdateScrollOffsetIfNeeded(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_TextEditor_UpdateScrollOffsetIfNeeded_m1488896800 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextEditorGenerated::__Register()
extern "C"  void UnityEngine_TextEditorGenerated___Register_m2272479433 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextEditorGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_TextEditorGenerated_ilo_addJSCSRel1_m3034892922 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_TextEditorGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_TextEditorGenerated_ilo_getObject2_m1987978088 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextEditorGenerated::ilo_setInt323(System.Int32,System.Int32)
extern "C"  void UnityEngine_TextEditorGenerated_ilo_setInt323_m1213806486 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextEditorGenerated::ilo_getBooleanS4(System.Int32)
extern "C"  bool UnityEngine_TextEditorGenerated_ilo_getBooleanS4_m1439193897 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine_TextEditorGenerated::ilo_getVector2S5(System.Int32)
extern "C"  Vector2_t4282066565  UnityEngine_TextEditorGenerated_ilo_getVector2S5_m2856465162 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextEditorGenerated::ilo_setVector2S6(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_TextEditorGenerated_ilo_setVector2S6_m749902830 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_TextEditorGenerated::ilo_setObject7(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_TextEditorGenerated_ilo_setObject7_m2266548022 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_TextEditorGenerated::ilo_getInt328(System.Int32)
extern "C"  int32_t UnityEngine_TextEditorGenerated_ilo_getInt328_m1343432744 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextEditorGenerated::ilo_setStringS9(System.Int32,System.String)
extern "C"  void UnityEngine_TextEditorGenerated_ilo_setStringS9_m265071247 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextEditorGenerated::ilo_setBooleanS10(System.Int32,System.Boolean)
extern "C"  void UnityEngine_TextEditorGenerated_ilo_setBooleanS10_m2890465399 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_TextEditorGenerated::ilo_getStringS11(System.Int32)
extern "C"  String_t* UnityEngine_TextEditorGenerated_ilo_getStringS11_m2143398323 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
