﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Security.Cryptography.ICryptoTransform
struct ICryptoTransform_t153068654;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ICSharpCode.SharpZipLib.Zip.Compression.Deflater
struct Deflater_t2454063301;
// System.IO.Stream
struct Stream_t1561764144;

#include "mscorlib_System_IO_Stream1561764144.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream
struct  DeflaterOutputStream_t537202536  : public Stream_t1561764144
{
public:
	// System.Security.Cryptography.ICryptoTransform ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::cryptoTransform_
	Il2CppObject * ___cryptoTransform__1;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::AESAuthCode
	ByteU5BU5D_t4260760469* ___AESAuthCode_2;
	// System.Byte[] ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::buffer_
	ByteU5BU5D_t4260760469* ___buffer__3;
	// ICSharpCode.SharpZipLib.Zip.Compression.Deflater ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::deflater_
	Deflater_t2454063301 * ___deflater__4;
	// System.IO.Stream ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::baseOutputStream_
	Stream_t1561764144 * ___baseOutputStream__5;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::isClosed_
	bool ___isClosed__6;
	// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::isStreamOwner_
	bool ___isStreamOwner__7;

public:
	inline static int32_t get_offset_of_cryptoTransform__1() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_t537202536, ___cryptoTransform__1)); }
	inline Il2CppObject * get_cryptoTransform__1() const { return ___cryptoTransform__1; }
	inline Il2CppObject ** get_address_of_cryptoTransform__1() { return &___cryptoTransform__1; }
	inline void set_cryptoTransform__1(Il2CppObject * value)
	{
		___cryptoTransform__1 = value;
		Il2CppCodeGenWriteBarrier(&___cryptoTransform__1, value);
	}

	inline static int32_t get_offset_of_AESAuthCode_2() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_t537202536, ___AESAuthCode_2)); }
	inline ByteU5BU5D_t4260760469* get_AESAuthCode_2() const { return ___AESAuthCode_2; }
	inline ByteU5BU5D_t4260760469** get_address_of_AESAuthCode_2() { return &___AESAuthCode_2; }
	inline void set_AESAuthCode_2(ByteU5BU5D_t4260760469* value)
	{
		___AESAuthCode_2 = value;
		Il2CppCodeGenWriteBarrier(&___AESAuthCode_2, value);
	}

	inline static int32_t get_offset_of_buffer__3() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_t537202536, ___buffer__3)); }
	inline ByteU5BU5D_t4260760469* get_buffer__3() const { return ___buffer__3; }
	inline ByteU5BU5D_t4260760469** get_address_of_buffer__3() { return &___buffer__3; }
	inline void set_buffer__3(ByteU5BU5D_t4260760469* value)
	{
		___buffer__3 = value;
		Il2CppCodeGenWriteBarrier(&___buffer__3, value);
	}

	inline static int32_t get_offset_of_deflater__4() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_t537202536, ___deflater__4)); }
	inline Deflater_t2454063301 * get_deflater__4() const { return ___deflater__4; }
	inline Deflater_t2454063301 ** get_address_of_deflater__4() { return &___deflater__4; }
	inline void set_deflater__4(Deflater_t2454063301 * value)
	{
		___deflater__4 = value;
		Il2CppCodeGenWriteBarrier(&___deflater__4, value);
	}

	inline static int32_t get_offset_of_baseOutputStream__5() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_t537202536, ___baseOutputStream__5)); }
	inline Stream_t1561764144 * get_baseOutputStream__5() const { return ___baseOutputStream__5; }
	inline Stream_t1561764144 ** get_address_of_baseOutputStream__5() { return &___baseOutputStream__5; }
	inline void set_baseOutputStream__5(Stream_t1561764144 * value)
	{
		___baseOutputStream__5 = value;
		Il2CppCodeGenWriteBarrier(&___baseOutputStream__5, value);
	}

	inline static int32_t get_offset_of_isClosed__6() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_t537202536, ___isClosed__6)); }
	inline bool get_isClosed__6() const { return ___isClosed__6; }
	inline bool* get_address_of_isClosed__6() { return &___isClosed__6; }
	inline void set_isClosed__6(bool value)
	{
		___isClosed__6 = value;
	}

	inline static int32_t get_offset_of_isStreamOwner__7() { return static_cast<int32_t>(offsetof(DeflaterOutputStream_t537202536, ___isStreamOwner__7)); }
	inline bool get_isStreamOwner__7() const { return ___isStreamOwner__7; }
	inline bool* get_address_of_isStreamOwner__7() { return &___isStreamOwner__7; }
	inline void set_isStreamOwner__7(bool value)
	{
		___isStreamOwner__7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
