﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour>
struct List_1_t670419272;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Voxels.VoxelContourSet
struct  VoxelContourSet_t2502455108  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<Pathfinding.Voxels.VoxelContour> Pathfinding.Voxels.VoxelContourSet::conts
	List_1_t670419272 * ___conts_0;
	// UnityEngine.Bounds Pathfinding.Voxels.VoxelContourSet::bounds
	Bounds_t2711641849  ___bounds_1;

public:
	inline static int32_t get_offset_of_conts_0() { return static_cast<int32_t>(offsetof(VoxelContourSet_t2502455108, ___conts_0)); }
	inline List_1_t670419272 * get_conts_0() const { return ___conts_0; }
	inline List_1_t670419272 ** get_address_of_conts_0() { return &___conts_0; }
	inline void set_conts_0(List_1_t670419272 * value)
	{
		___conts_0 = value;
		Il2CppCodeGenWriteBarrier(&___conts_0, value);
	}

	inline static int32_t get_offset_of_bounds_1() { return static_cast<int32_t>(offsetof(VoxelContourSet_t2502455108, ___bounds_1)); }
	inline Bounds_t2711641849  get_bounds_1() const { return ___bounds_1; }
	inline Bounds_t2711641849 * get_address_of_bounds_1() { return &___bounds_1; }
	inline void set_bounds_1(Bounds_t2711641849  value)
	{
		___bounds_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
