﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NumEffect
struct NumEffect_t2824221847;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UIFont
struct UIFont_t2503090435;
// System.String
struct String_t;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// UILabel
struct UILabel_t291504320;
// UITweener
struct UITweener_t105489188;
// UIWidget
struct UIWidget_t769069560;
// HeadUpBloodMgr
struct HeadUpBloodMgr_t2330866585;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_UIFont2503090435.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_HeadUpBloodMgr_LabelType4067300664.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "AssemblyU2DCSharp_UITweener105489188.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_HeadUpBloodMgr2330866585.h"
#include "AssemblyU2DCSharp_NumEffect2824221847.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void NumEffect::.ctor()
extern "C"  void NumEffect__ctor_m3620805300 (NumEffect_t2824221847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NumEffect::OnAwake(UnityEngine.GameObject)
extern "C"  void NumEffect_OnAwake_m615195248 (NumEffect_t2824221847 * __this, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NumEffect::SetData(UIFont,System.String,HeadUpBloodMgr/LabelType,System.Boolean)
extern "C"  void NumEffect_SetData_m766876614 (NumEffect_t2824221847 * __this, UIFont_t2503090435 * ___font0, String_t* ___text1, int32_t ___labelType2, bool ___ishero3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NumEffect::SetData(System.String,HeadUpBloodMgr/LabelType,System.Boolean,System.Int32)
extern "C"  void NumEffect_SetData_m1265217860 (NumEffect_t2824221847 * __this, String_t* ___text0, int32_t ___labelType1, bool ___ishero2, int32_t ___index3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NumEffect::Send()
extern "C"  void NumEffect_Send_m484997528 (NumEffect_t2824221847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NumEffect::OnStart()
extern "C"  void NumEffect_OnStart_m4051846293 (NumEffect_t2824221847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NumEffect::OnEnd()
extern "C"  void NumEffect_OnEnd_m3115127630 (NumEffect_t2824221847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NumEffect::OnUpdate()
extern "C"  void NumEffect_OnUpdate_m1059035672 (NumEffect_t2824221847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NumEffect::OnClear(CEvent.ZEvent)
extern "C"  void NumEffect_OnClear_m3894854843 (NumEffect_t2824221847 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NumEffect::Clear()
extern "C"  void NumEffect_Clear_m1026938591 (NumEffect_t2824221847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NumEffect::<Send>m__3D5()
extern "C"  void NumEffect_U3CSendU3Em__3D5_m2109503425 (NumEffect_t2824221847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NumEffect::ilo_set_text1(UILabel,System.String)
extern "C"  void NumEffect_ilo_set_text1_m79999112 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NumEffect::ilo_ResetToBeginning2(UITweener)
extern "C"  void NumEffect_ilo_ResetToBeginning2_m3828916624 (Il2CppObject * __this /* static, unused */, UITweener_t105489188 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NumEffect::ilo_set_depth3(UIWidget,System.Int32)
extern "C"  void NumEffect_ilo_set_depth3_m2055860703 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NumEffect::ilo_ResetNumEffect4(HeadUpBloodMgr,NumEffect)
extern "C"  void NumEffect_ilo_ResetNumEffect4_m804618887 (Il2CppObject * __this /* static, unused */, HeadUpBloodMgr_t2330866585 * ____this0, NumEffect_t2824221847 * ___numEff1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 NumEffect::ilo_WorldToNgui5(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  NumEffect_ilo_WorldToNgui5_m1410845365 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ____pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NumEffect::ilo_RemoveEventListener6(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void NumEffect_ilo_RemoveEventListener6_m2573348308 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NumEffect::ilo_Clear7(NumEffect)
extern "C"  void NumEffect_ilo_Clear7_m3478558222 (Il2CppObject * __this /* static, unused */, NumEffect_t2824221847 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
