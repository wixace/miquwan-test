﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_capirpeMoukole126
struct M_capirpeMoukole126_t2533643221;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_capirpeMoukole1262533643221.h"

// System.Void GarbageiOS.M_capirpeMoukole126::.ctor()
extern "C"  void M_capirpeMoukole126__ctor_m315077934 (M_capirpeMoukole126_t2533643221 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_capirpeMoukole126::M_maiqejor0(System.String[],System.Int32)
extern "C"  void M_capirpeMoukole126_M_maiqejor0_m2495762461 (M_capirpeMoukole126_t2533643221 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_capirpeMoukole126::M_keecee1(System.String[],System.Int32)
extern "C"  void M_capirpeMoukole126_M_keecee1_m1752589066 (M_capirpeMoukole126_t2533643221 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_capirpeMoukole126::M_horpall2(System.String[],System.Int32)
extern "C"  void M_capirpeMoukole126_M_horpall2_m939872277 (M_capirpeMoukole126_t2533643221 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_capirpeMoukole126::M_miho3(System.String[],System.Int32)
extern "C"  void M_capirpeMoukole126_M_miho3_m4038656737 (M_capirpeMoukole126_t2533643221 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_capirpeMoukole126::M_porcormalHiguhai4(System.String[],System.Int32)
extern "C"  void M_capirpeMoukole126_M_porcormalHiguhai4_m493805961 (M_capirpeMoukole126_t2533643221 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_capirpeMoukole126::M_lascemvouGoutow5(System.String[],System.Int32)
extern "C"  void M_capirpeMoukole126_M_lascemvouGoutow5_m1685859638 (M_capirpeMoukole126_t2533643221 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_capirpeMoukole126::ilo_M_miho31(GarbageiOS.M_capirpeMoukole126,System.String[],System.Int32)
extern "C"  void M_capirpeMoukole126_ilo_M_miho31_m1588192366 (Il2CppObject * __this /* static, unused */, M_capirpeMoukole126_t2533643221 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_capirpeMoukole126::ilo_M_porcormalHiguhai42(GarbageiOS.M_capirpeMoukole126,System.String[],System.Int32)
extern "C"  void M_capirpeMoukole126_ilo_M_porcormalHiguhai42_m2667820853 (Il2CppObject * __this /* static, unused */, M_capirpeMoukole126_t2533643221 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
