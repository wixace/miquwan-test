﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.OffMeshLink
struct OffMeshLink_t754643148;
// UnityEngine.OffMeshLinkData
struct OffMeshLinkData_t3329424662;
struct OffMeshLinkData_t3329424662_marshaled_pinvoke;
struct OffMeshLinkData_t3329424662_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_OffMeshLinkData3329424662.h"
#include "UnityEngine_UnityEngine_OffMeshLinkType3329924262.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Boolean UnityEngine.OffMeshLinkData::get_valid()
extern "C"  bool OffMeshLinkData_get_valid_m65760688 (OffMeshLinkData_t3329424662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.OffMeshLinkData::get_activated()
extern "C"  bool OffMeshLinkData_get_activated_m2446685381 (OffMeshLinkData_t3329424662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.OffMeshLinkType UnityEngine.OffMeshLinkData::get_linkType()
extern "C"  int32_t OffMeshLinkData_get_linkType_m2013391310 (OffMeshLinkData_t3329424662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.OffMeshLinkData::get_startPos()
extern "C"  Vector3_t4282066566  OffMeshLinkData_get_startPos_m3321591404 (OffMeshLinkData_t3329424662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.OffMeshLinkData::get_endPos()
extern "C"  Vector3_t4282066566  OffMeshLinkData_get_endPos_m3187054547 (OffMeshLinkData_t3329424662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.OffMeshLink UnityEngine.OffMeshLinkData::get_offMeshLink()
extern "C"  OffMeshLink_t754643148 * OffMeshLinkData_get_offMeshLink_m2795909016 (OffMeshLinkData_t3329424662 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.OffMeshLink UnityEngine.OffMeshLinkData::GetOffMeshLinkInternal(System.Int32)
extern "C"  OffMeshLink_t754643148 * OffMeshLinkData_GetOffMeshLinkInternal_m3177159301 (OffMeshLinkData_t3329424662 * __this, int32_t ___instanceID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct OffMeshLinkData_t3329424662;
struct OffMeshLinkData_t3329424662_marshaled_pinvoke;

extern "C" void OffMeshLinkData_t3329424662_marshal_pinvoke(const OffMeshLinkData_t3329424662& unmarshaled, OffMeshLinkData_t3329424662_marshaled_pinvoke& marshaled);
extern "C" void OffMeshLinkData_t3329424662_marshal_pinvoke_back(const OffMeshLinkData_t3329424662_marshaled_pinvoke& marshaled, OffMeshLinkData_t3329424662& unmarshaled);
extern "C" void OffMeshLinkData_t3329424662_marshal_pinvoke_cleanup(OffMeshLinkData_t3329424662_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct OffMeshLinkData_t3329424662;
struct OffMeshLinkData_t3329424662_marshaled_com;

extern "C" void OffMeshLinkData_t3329424662_marshal_com(const OffMeshLinkData_t3329424662& unmarshaled, OffMeshLinkData_t3329424662_marshaled_com& marshaled);
extern "C" void OffMeshLinkData_t3329424662_marshal_com_back(const OffMeshLinkData_t3329424662_marshaled_com& marshaled, OffMeshLinkData_t3329424662& unmarshaled);
extern "C" void OffMeshLinkData_t3329424662_marshal_com_cleanup(OffMeshLinkData_t3329424662_marshaled_com& marshaled);
