﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_noumougairWereco179
struct  M_noumougairWereco179_t2633418148  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_noumougairWereco179::_whearlaibis
	int32_t ____whearlaibis_0;
	// System.Single GarbageiOS.M_noumougairWereco179::_jaca
	float ____jaca_1;
	// System.UInt32 GarbageiOS.M_noumougairWereco179::_tratrasmea
	uint32_t ____tratrasmea_2;
	// System.String GarbageiOS.M_noumougairWereco179::_norwisjay
	String_t* ____norwisjay_3;
	// System.Int32 GarbageiOS.M_noumougairWereco179::_jefoujas
	int32_t ____jefoujas_4;
	// System.Single GarbageiOS.M_noumougairWereco179::_hawfalsaRoutouyir
	float ____hawfalsaRoutouyir_5;
	// System.Boolean GarbageiOS.M_noumougairWereco179::_xemouwhurSimear
	bool ____xemouwhurSimear_6;
	// System.Int32 GarbageiOS.M_noumougairWereco179::_seawheKowhowpall
	int32_t ____seawheKowhowpall_7;
	// System.Single GarbageiOS.M_noumougairWereco179::_fawreaHairpe
	float ____fawreaHairpe_8;
	// System.String GarbageiOS.M_noumougairWereco179::_daroowhouNeredarjai
	String_t* ____daroowhouNeredarjai_9;
	// System.UInt32 GarbageiOS.M_noumougairWereco179::_femdalstear
	uint32_t ____femdalstear_10;
	// System.Int32 GarbageiOS.M_noumougairWereco179::_bemeJele
	int32_t ____bemeJele_11;
	// System.Boolean GarbageiOS.M_noumougairWereco179::_diqear
	bool ____diqear_12;
	// System.Single GarbageiOS.M_noumougairWereco179::_sornallrurGibear
	float ____sornallrurGibear_13;
	// System.UInt32 GarbageiOS.M_noumougairWereco179::_daypexereHoumear
	uint32_t ____daypexereHoumear_14;
	// System.Int32 GarbageiOS.M_noumougairWereco179::_haskislis
	int32_t ____haskislis_15;
	// System.Single GarbageiOS.M_noumougairWereco179::_telcouhaChijawsta
	float ____telcouhaChijawsta_16;
	// System.Single GarbageiOS.M_noumougairWereco179::_meje
	float ____meje_17;
	// System.Boolean GarbageiOS.M_noumougairWereco179::_pursaQudrer
	bool ____pursaQudrer_18;
	// System.Single GarbageiOS.M_noumougairWereco179::_zorajaw
	float ____zorajaw_19;
	// System.String GarbageiOS.M_noumougairWereco179::_vurkazarPurnur
	String_t* ____vurkazarPurnur_20;
	// System.Boolean GarbageiOS.M_noumougairWereco179::_tasyouraPotemfis
	bool ____tasyouraPotemfis_21;
	// System.Single GarbageiOS.M_noumougairWereco179::_zatar
	float ____zatar_22;
	// System.Int32 GarbageiOS.M_noumougairWereco179::_neme
	int32_t ____neme_23;
	// System.Single GarbageiOS.M_noumougairWereco179::_kemdempo
	float ____kemdempo_24;
	// System.UInt32 GarbageiOS.M_noumougairWereco179::_galdreCorsee
	uint32_t ____galdreCorsee_25;
	// System.Int32 GarbageiOS.M_noumougairWereco179::_sairnawri
	int32_t ____sairnawri_26;
	// System.UInt32 GarbageiOS.M_noumougairWereco179::_mobeaRelbemcas
	uint32_t ____mobeaRelbemcas_27;
	// System.Boolean GarbageiOS.M_noumougairWereco179::_lusa
	bool ____lusa_28;
	// System.Single GarbageiOS.M_noumougairWereco179::_kamor
	float ____kamor_29;
	// System.Single GarbageiOS.M_noumougairWereco179::_dreheePurstallja
	float ____dreheePurstallja_30;
	// System.Int32 GarbageiOS.M_noumougairWereco179::_howgajer
	int32_t ____howgajer_31;
	// System.String GarbageiOS.M_noumougairWereco179::_nasnooVeahou
	String_t* ____nasnooVeahou_32;
	// System.UInt32 GarbageiOS.M_noumougairWereco179::_mitouRembo
	uint32_t ____mitouRembo_33;
	// System.Boolean GarbageiOS.M_noumougairWereco179::_putounem
	bool ____putounem_34;
	// System.UInt32 GarbageiOS.M_noumougairWereco179::_yorxeSajadou
	uint32_t ____yorxeSajadou_35;
	// System.Single GarbageiOS.M_noumougairWereco179::_neremu
	float ____neremu_36;
	// System.Single GarbageiOS.M_noumougairWereco179::_yipairrorMacefel
	float ____yipairrorMacefel_37;
	// System.Boolean GarbageiOS.M_noumougairWereco179::_sivou
	bool ____sivou_38;
	// System.Int32 GarbageiOS.M_noumougairWereco179::_fallmou
	int32_t ____fallmou_39;

public:
	inline static int32_t get_offset_of__whearlaibis_0() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____whearlaibis_0)); }
	inline int32_t get__whearlaibis_0() const { return ____whearlaibis_0; }
	inline int32_t* get_address_of__whearlaibis_0() { return &____whearlaibis_0; }
	inline void set__whearlaibis_0(int32_t value)
	{
		____whearlaibis_0 = value;
	}

	inline static int32_t get_offset_of__jaca_1() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____jaca_1)); }
	inline float get__jaca_1() const { return ____jaca_1; }
	inline float* get_address_of__jaca_1() { return &____jaca_1; }
	inline void set__jaca_1(float value)
	{
		____jaca_1 = value;
	}

	inline static int32_t get_offset_of__tratrasmea_2() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____tratrasmea_2)); }
	inline uint32_t get__tratrasmea_2() const { return ____tratrasmea_2; }
	inline uint32_t* get_address_of__tratrasmea_2() { return &____tratrasmea_2; }
	inline void set__tratrasmea_2(uint32_t value)
	{
		____tratrasmea_2 = value;
	}

	inline static int32_t get_offset_of__norwisjay_3() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____norwisjay_3)); }
	inline String_t* get__norwisjay_3() const { return ____norwisjay_3; }
	inline String_t** get_address_of__norwisjay_3() { return &____norwisjay_3; }
	inline void set__norwisjay_3(String_t* value)
	{
		____norwisjay_3 = value;
		Il2CppCodeGenWriteBarrier(&____norwisjay_3, value);
	}

	inline static int32_t get_offset_of__jefoujas_4() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____jefoujas_4)); }
	inline int32_t get__jefoujas_4() const { return ____jefoujas_4; }
	inline int32_t* get_address_of__jefoujas_4() { return &____jefoujas_4; }
	inline void set__jefoujas_4(int32_t value)
	{
		____jefoujas_4 = value;
	}

	inline static int32_t get_offset_of__hawfalsaRoutouyir_5() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____hawfalsaRoutouyir_5)); }
	inline float get__hawfalsaRoutouyir_5() const { return ____hawfalsaRoutouyir_5; }
	inline float* get_address_of__hawfalsaRoutouyir_5() { return &____hawfalsaRoutouyir_5; }
	inline void set__hawfalsaRoutouyir_5(float value)
	{
		____hawfalsaRoutouyir_5 = value;
	}

	inline static int32_t get_offset_of__xemouwhurSimear_6() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____xemouwhurSimear_6)); }
	inline bool get__xemouwhurSimear_6() const { return ____xemouwhurSimear_6; }
	inline bool* get_address_of__xemouwhurSimear_6() { return &____xemouwhurSimear_6; }
	inline void set__xemouwhurSimear_6(bool value)
	{
		____xemouwhurSimear_6 = value;
	}

	inline static int32_t get_offset_of__seawheKowhowpall_7() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____seawheKowhowpall_7)); }
	inline int32_t get__seawheKowhowpall_7() const { return ____seawheKowhowpall_7; }
	inline int32_t* get_address_of__seawheKowhowpall_7() { return &____seawheKowhowpall_7; }
	inline void set__seawheKowhowpall_7(int32_t value)
	{
		____seawheKowhowpall_7 = value;
	}

	inline static int32_t get_offset_of__fawreaHairpe_8() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____fawreaHairpe_8)); }
	inline float get__fawreaHairpe_8() const { return ____fawreaHairpe_8; }
	inline float* get_address_of__fawreaHairpe_8() { return &____fawreaHairpe_8; }
	inline void set__fawreaHairpe_8(float value)
	{
		____fawreaHairpe_8 = value;
	}

	inline static int32_t get_offset_of__daroowhouNeredarjai_9() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____daroowhouNeredarjai_9)); }
	inline String_t* get__daroowhouNeredarjai_9() const { return ____daroowhouNeredarjai_9; }
	inline String_t** get_address_of__daroowhouNeredarjai_9() { return &____daroowhouNeredarjai_9; }
	inline void set__daroowhouNeredarjai_9(String_t* value)
	{
		____daroowhouNeredarjai_9 = value;
		Il2CppCodeGenWriteBarrier(&____daroowhouNeredarjai_9, value);
	}

	inline static int32_t get_offset_of__femdalstear_10() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____femdalstear_10)); }
	inline uint32_t get__femdalstear_10() const { return ____femdalstear_10; }
	inline uint32_t* get_address_of__femdalstear_10() { return &____femdalstear_10; }
	inline void set__femdalstear_10(uint32_t value)
	{
		____femdalstear_10 = value;
	}

	inline static int32_t get_offset_of__bemeJele_11() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____bemeJele_11)); }
	inline int32_t get__bemeJele_11() const { return ____bemeJele_11; }
	inline int32_t* get_address_of__bemeJele_11() { return &____bemeJele_11; }
	inline void set__bemeJele_11(int32_t value)
	{
		____bemeJele_11 = value;
	}

	inline static int32_t get_offset_of__diqear_12() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____diqear_12)); }
	inline bool get__diqear_12() const { return ____diqear_12; }
	inline bool* get_address_of__diqear_12() { return &____diqear_12; }
	inline void set__diqear_12(bool value)
	{
		____diqear_12 = value;
	}

	inline static int32_t get_offset_of__sornallrurGibear_13() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____sornallrurGibear_13)); }
	inline float get__sornallrurGibear_13() const { return ____sornallrurGibear_13; }
	inline float* get_address_of__sornallrurGibear_13() { return &____sornallrurGibear_13; }
	inline void set__sornallrurGibear_13(float value)
	{
		____sornallrurGibear_13 = value;
	}

	inline static int32_t get_offset_of__daypexereHoumear_14() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____daypexereHoumear_14)); }
	inline uint32_t get__daypexereHoumear_14() const { return ____daypexereHoumear_14; }
	inline uint32_t* get_address_of__daypexereHoumear_14() { return &____daypexereHoumear_14; }
	inline void set__daypexereHoumear_14(uint32_t value)
	{
		____daypexereHoumear_14 = value;
	}

	inline static int32_t get_offset_of__haskislis_15() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____haskislis_15)); }
	inline int32_t get__haskislis_15() const { return ____haskislis_15; }
	inline int32_t* get_address_of__haskislis_15() { return &____haskislis_15; }
	inline void set__haskislis_15(int32_t value)
	{
		____haskislis_15 = value;
	}

	inline static int32_t get_offset_of__telcouhaChijawsta_16() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____telcouhaChijawsta_16)); }
	inline float get__telcouhaChijawsta_16() const { return ____telcouhaChijawsta_16; }
	inline float* get_address_of__telcouhaChijawsta_16() { return &____telcouhaChijawsta_16; }
	inline void set__telcouhaChijawsta_16(float value)
	{
		____telcouhaChijawsta_16 = value;
	}

	inline static int32_t get_offset_of__meje_17() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____meje_17)); }
	inline float get__meje_17() const { return ____meje_17; }
	inline float* get_address_of__meje_17() { return &____meje_17; }
	inline void set__meje_17(float value)
	{
		____meje_17 = value;
	}

	inline static int32_t get_offset_of__pursaQudrer_18() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____pursaQudrer_18)); }
	inline bool get__pursaQudrer_18() const { return ____pursaQudrer_18; }
	inline bool* get_address_of__pursaQudrer_18() { return &____pursaQudrer_18; }
	inline void set__pursaQudrer_18(bool value)
	{
		____pursaQudrer_18 = value;
	}

	inline static int32_t get_offset_of__zorajaw_19() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____zorajaw_19)); }
	inline float get__zorajaw_19() const { return ____zorajaw_19; }
	inline float* get_address_of__zorajaw_19() { return &____zorajaw_19; }
	inline void set__zorajaw_19(float value)
	{
		____zorajaw_19 = value;
	}

	inline static int32_t get_offset_of__vurkazarPurnur_20() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____vurkazarPurnur_20)); }
	inline String_t* get__vurkazarPurnur_20() const { return ____vurkazarPurnur_20; }
	inline String_t** get_address_of__vurkazarPurnur_20() { return &____vurkazarPurnur_20; }
	inline void set__vurkazarPurnur_20(String_t* value)
	{
		____vurkazarPurnur_20 = value;
		Il2CppCodeGenWriteBarrier(&____vurkazarPurnur_20, value);
	}

	inline static int32_t get_offset_of__tasyouraPotemfis_21() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____tasyouraPotemfis_21)); }
	inline bool get__tasyouraPotemfis_21() const { return ____tasyouraPotemfis_21; }
	inline bool* get_address_of__tasyouraPotemfis_21() { return &____tasyouraPotemfis_21; }
	inline void set__tasyouraPotemfis_21(bool value)
	{
		____tasyouraPotemfis_21 = value;
	}

	inline static int32_t get_offset_of__zatar_22() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____zatar_22)); }
	inline float get__zatar_22() const { return ____zatar_22; }
	inline float* get_address_of__zatar_22() { return &____zatar_22; }
	inline void set__zatar_22(float value)
	{
		____zatar_22 = value;
	}

	inline static int32_t get_offset_of__neme_23() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____neme_23)); }
	inline int32_t get__neme_23() const { return ____neme_23; }
	inline int32_t* get_address_of__neme_23() { return &____neme_23; }
	inline void set__neme_23(int32_t value)
	{
		____neme_23 = value;
	}

	inline static int32_t get_offset_of__kemdempo_24() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____kemdempo_24)); }
	inline float get__kemdempo_24() const { return ____kemdempo_24; }
	inline float* get_address_of__kemdempo_24() { return &____kemdempo_24; }
	inline void set__kemdempo_24(float value)
	{
		____kemdempo_24 = value;
	}

	inline static int32_t get_offset_of__galdreCorsee_25() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____galdreCorsee_25)); }
	inline uint32_t get__galdreCorsee_25() const { return ____galdreCorsee_25; }
	inline uint32_t* get_address_of__galdreCorsee_25() { return &____galdreCorsee_25; }
	inline void set__galdreCorsee_25(uint32_t value)
	{
		____galdreCorsee_25 = value;
	}

	inline static int32_t get_offset_of__sairnawri_26() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____sairnawri_26)); }
	inline int32_t get__sairnawri_26() const { return ____sairnawri_26; }
	inline int32_t* get_address_of__sairnawri_26() { return &____sairnawri_26; }
	inline void set__sairnawri_26(int32_t value)
	{
		____sairnawri_26 = value;
	}

	inline static int32_t get_offset_of__mobeaRelbemcas_27() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____mobeaRelbemcas_27)); }
	inline uint32_t get__mobeaRelbemcas_27() const { return ____mobeaRelbemcas_27; }
	inline uint32_t* get_address_of__mobeaRelbemcas_27() { return &____mobeaRelbemcas_27; }
	inline void set__mobeaRelbemcas_27(uint32_t value)
	{
		____mobeaRelbemcas_27 = value;
	}

	inline static int32_t get_offset_of__lusa_28() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____lusa_28)); }
	inline bool get__lusa_28() const { return ____lusa_28; }
	inline bool* get_address_of__lusa_28() { return &____lusa_28; }
	inline void set__lusa_28(bool value)
	{
		____lusa_28 = value;
	}

	inline static int32_t get_offset_of__kamor_29() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____kamor_29)); }
	inline float get__kamor_29() const { return ____kamor_29; }
	inline float* get_address_of__kamor_29() { return &____kamor_29; }
	inline void set__kamor_29(float value)
	{
		____kamor_29 = value;
	}

	inline static int32_t get_offset_of__dreheePurstallja_30() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____dreheePurstallja_30)); }
	inline float get__dreheePurstallja_30() const { return ____dreheePurstallja_30; }
	inline float* get_address_of__dreheePurstallja_30() { return &____dreheePurstallja_30; }
	inline void set__dreheePurstallja_30(float value)
	{
		____dreheePurstallja_30 = value;
	}

	inline static int32_t get_offset_of__howgajer_31() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____howgajer_31)); }
	inline int32_t get__howgajer_31() const { return ____howgajer_31; }
	inline int32_t* get_address_of__howgajer_31() { return &____howgajer_31; }
	inline void set__howgajer_31(int32_t value)
	{
		____howgajer_31 = value;
	}

	inline static int32_t get_offset_of__nasnooVeahou_32() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____nasnooVeahou_32)); }
	inline String_t* get__nasnooVeahou_32() const { return ____nasnooVeahou_32; }
	inline String_t** get_address_of__nasnooVeahou_32() { return &____nasnooVeahou_32; }
	inline void set__nasnooVeahou_32(String_t* value)
	{
		____nasnooVeahou_32 = value;
		Il2CppCodeGenWriteBarrier(&____nasnooVeahou_32, value);
	}

	inline static int32_t get_offset_of__mitouRembo_33() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____mitouRembo_33)); }
	inline uint32_t get__mitouRembo_33() const { return ____mitouRembo_33; }
	inline uint32_t* get_address_of__mitouRembo_33() { return &____mitouRembo_33; }
	inline void set__mitouRembo_33(uint32_t value)
	{
		____mitouRembo_33 = value;
	}

	inline static int32_t get_offset_of__putounem_34() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____putounem_34)); }
	inline bool get__putounem_34() const { return ____putounem_34; }
	inline bool* get_address_of__putounem_34() { return &____putounem_34; }
	inline void set__putounem_34(bool value)
	{
		____putounem_34 = value;
	}

	inline static int32_t get_offset_of__yorxeSajadou_35() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____yorxeSajadou_35)); }
	inline uint32_t get__yorxeSajadou_35() const { return ____yorxeSajadou_35; }
	inline uint32_t* get_address_of__yorxeSajadou_35() { return &____yorxeSajadou_35; }
	inline void set__yorxeSajadou_35(uint32_t value)
	{
		____yorxeSajadou_35 = value;
	}

	inline static int32_t get_offset_of__neremu_36() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____neremu_36)); }
	inline float get__neremu_36() const { return ____neremu_36; }
	inline float* get_address_of__neremu_36() { return &____neremu_36; }
	inline void set__neremu_36(float value)
	{
		____neremu_36 = value;
	}

	inline static int32_t get_offset_of__yipairrorMacefel_37() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____yipairrorMacefel_37)); }
	inline float get__yipairrorMacefel_37() const { return ____yipairrorMacefel_37; }
	inline float* get_address_of__yipairrorMacefel_37() { return &____yipairrorMacefel_37; }
	inline void set__yipairrorMacefel_37(float value)
	{
		____yipairrorMacefel_37 = value;
	}

	inline static int32_t get_offset_of__sivou_38() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____sivou_38)); }
	inline bool get__sivou_38() const { return ____sivou_38; }
	inline bool* get_address_of__sivou_38() { return &____sivou_38; }
	inline void set__sivou_38(bool value)
	{
		____sivou_38 = value;
	}

	inline static int32_t get_offset_of__fallmou_39() { return static_cast<int32_t>(offsetof(M_noumougairWereco179_t2633418148, ____fallmou_39)); }
	inline int32_t get__fallmou_39() const { return ____fallmou_39; }
	inline int32_t* get_address_of__fallmou_39() { return &____fallmou_39; }
	inline void set__fallmou_39(int32_t value)
	{
		____fallmou_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
