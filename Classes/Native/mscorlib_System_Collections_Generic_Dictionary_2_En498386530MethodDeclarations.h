﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,ProductsCfgMgr/ProductInfo>
struct Dictionary_2_t3476030434;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_En498386530.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23374811140.h"
#include "AssemblyU2DCSharp_ProductsCfgMgr_ProductInfo1305991238.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m2045912099_gshared (Enumerator_t498386530 * __this, Dictionary_2_t3476030434 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m2045912099(__this, ___dictionary0, method) ((  void (*) (Enumerator_t498386530 *, Dictionary_2_t3476030434 *, const MethodInfo*))Enumerator__ctor_m2045912099_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m4277870440_gshared (Enumerator_t498386530 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m4277870440(__this, method) ((  Il2CppObject * (*) (Enumerator_t498386530 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m4277870440_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m3521232050_gshared (Enumerator_t498386530 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m3521232050(__this, method) ((  void (*) (Enumerator_t498386530 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m3521232050_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2593332137_gshared (Enumerator_t498386530 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2593332137(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t498386530 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2593332137_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3271503428_gshared (Enumerator_t498386530 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3271503428(__this, method) ((  Il2CppObject * (*) (Enumerator_t498386530 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3271503428_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1047170006_gshared (Enumerator_t498386530 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1047170006(__this, method) ((  Il2CppObject * (*) (Enumerator_t498386530 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m1047170006_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m923395554_gshared (Enumerator_t498386530 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m923395554(__this, method) ((  bool (*) (Enumerator_t498386530 *, const MethodInfo*))Enumerator_MoveNext_m923395554_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::get_Current()
extern "C"  KeyValuePair_2_t3374811140  Enumerator_get_Current_m2585040154_gshared (Enumerator_t498386530 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2585040154(__this, method) ((  KeyValuePair_2_t3374811140  (*) (Enumerator_t498386530 *, const MethodInfo*))Enumerator_get_Current_m2585040154_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::get_CurrentKey()
extern "C"  Il2CppObject * Enumerator_get_CurrentKey_m405132907_gshared (Enumerator_t498386530 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m405132907(__this, method) ((  Il2CppObject * (*) (Enumerator_t498386530 *, const MethodInfo*))Enumerator_get_CurrentKey_m405132907_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::get_CurrentValue()
extern "C"  ProductInfo_t1305991238  Enumerator_get_CurrentValue_m1663135083_gshared (Enumerator_t498386530 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m1663135083(__this, method) ((  ProductInfo_t1305991238  (*) (Enumerator_t498386530 *, const MethodInfo*))Enumerator_get_CurrentValue_m1663135083_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::Reset()
extern "C"  void Enumerator_Reset_m2431203829_gshared (Enumerator_t498386530 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2431203829(__this, method) ((  void (*) (Enumerator_t498386530 *, const MethodInfo*))Enumerator_Reset_m2431203829_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::VerifyState()
extern "C"  void Enumerator_VerifyState_m2479128126_gshared (Enumerator_t498386530 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m2479128126(__this, method) ((  void (*) (Enumerator_t498386530 *, const MethodInfo*))Enumerator_VerifyState_m2479128126_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m1480151590_gshared (Enumerator_t498386530 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1480151590(__this, method) ((  void (*) (Enumerator_t498386530 *, const MethodInfo*))Enumerator_VerifyCurrent_m1480151590_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,ProductsCfgMgr/ProductInfo>::Dispose()
extern "C"  void Enumerator_Dispose_m2448099717_gshared (Enumerator_t498386530 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m2448099717(__this, method) ((  void (*) (Enumerator_t498386530 *, const MethodInfo*))Enumerator_Dispose_m2448099717_gshared)(__this, method)
