﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_saraiSeecigea35
struct  M_saraiSeecigea35_t3093906936  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_saraiSeecigea35::_heabaceeBogoopa
	String_t* ____heabaceeBogoopa_0;
	// System.UInt32 GarbageiOS.M_saraiSeecigea35::_cebo
	uint32_t ____cebo_1;
	// System.Single GarbageiOS.M_saraiSeecigea35::_zasow
	float ____zasow_2;
	// System.Int32 GarbageiOS.M_saraiSeecigea35::_nousesoKozaikoo
	int32_t ____nousesoKozaikoo_3;
	// System.Int32 GarbageiOS.M_saraiSeecigea35::_nalatri
	int32_t ____nalatri_4;
	// System.Boolean GarbageiOS.M_saraiSeecigea35::_lalxasfear
	bool ____lalxasfear_5;
	// System.String GarbageiOS.M_saraiSeecigea35::_sojatal
	String_t* ____sojatal_6;
	// System.Boolean GarbageiOS.M_saraiSeecigea35::_vineepu
	bool ____vineepu_7;
	// System.String GarbageiOS.M_saraiSeecigea35::_larwhudu
	String_t* ____larwhudu_8;
	// System.UInt32 GarbageiOS.M_saraiSeecigea35::_rasjalworMaytearrur
	uint32_t ____rasjalworMaytearrur_9;
	// System.Boolean GarbageiOS.M_saraiSeecigea35::_banou
	bool ____banou_10;
	// System.String GarbageiOS.M_saraiSeecigea35::_torqeegouJacai
	String_t* ____torqeegouJacai_11;
	// System.Int32 GarbageiOS.M_saraiSeecigea35::_jababe
	int32_t ____jababe_12;
	// System.Boolean GarbageiOS.M_saraiSeecigea35::_mawtuwearCerecoo
	bool ____mawtuwearCerecoo_13;
	// System.UInt32 GarbageiOS.M_saraiSeecigea35::_powbulouWeepawje
	uint32_t ____powbulouWeepawje_14;
	// System.Boolean GarbageiOS.M_saraiSeecigea35::_ballweparMumaybere
	bool ____ballweparMumaybere_15;
	// System.Int32 GarbageiOS.M_saraiSeecigea35::_reljearSisku
	int32_t ____reljearSisku_16;
	// System.UInt32 GarbageiOS.M_saraiSeecigea35::_wawmiworSarjor
	uint32_t ____wawmiworSarjor_17;
	// System.Boolean GarbageiOS.M_saraiSeecigea35::_nejecarNoter
	bool ____nejecarNoter_18;
	// System.UInt32 GarbageiOS.M_saraiSeecigea35::_nutaiFarpowcer
	uint32_t ____nutaiFarpowcer_19;
	// System.Boolean GarbageiOS.M_saraiSeecigea35::_wheeperirTimelhi
	bool ____wheeperirTimelhi_20;
	// System.String GarbageiOS.M_saraiSeecigea35::_sedraiKorjearbe
	String_t* ____sedraiKorjearbe_21;
	// System.Int32 GarbageiOS.M_saraiSeecigea35::_foozenee
	int32_t ____foozenee_22;
	// System.UInt32 GarbageiOS.M_saraiSeecigea35::_dougimal
	uint32_t ____dougimal_23;
	// System.UInt32 GarbageiOS.M_saraiSeecigea35::_whearhalStallwe
	uint32_t ____whearhalStallwe_24;
	// System.UInt32 GarbageiOS.M_saraiSeecigea35::_nerqalchawGurwis
	uint32_t ____nerqalchawGurwis_25;
	// System.UInt32 GarbageiOS.M_saraiSeecigea35::_madou
	uint32_t ____madou_26;
	// System.String GarbageiOS.M_saraiSeecigea35::_sejisooFowrirhu
	String_t* ____sejisooFowrirhu_27;
	// System.Single GarbageiOS.M_saraiSeecigea35::_stalllorpea
	float ____stalllorpea_28;
	// System.UInt32 GarbageiOS.M_saraiSeecigea35::_jismiBirnee
	uint32_t ____jismiBirnee_29;
	// System.Boolean GarbageiOS.M_saraiSeecigea35::_caidrenereKergar
	bool ____caidrenereKergar_30;
	// System.UInt32 GarbageiOS.M_saraiSeecigea35::_tarcavair
	uint32_t ____tarcavair_31;
	// System.Single GarbageiOS.M_saraiSeecigea35::_gouseyow
	float ____gouseyow_32;
	// System.UInt32 GarbageiOS.M_saraiSeecigea35::_caybereXereherekow
	uint32_t ____caybereXereherekow_33;
	// System.String GarbageiOS.M_saraiSeecigea35::_toujalni
	String_t* ____toujalni_34;
	// System.Single GarbageiOS.M_saraiSeecigea35::_telmeljea
	float ____telmeljea_35;
	// System.String GarbageiOS.M_saraiSeecigea35::_jaisayyal
	String_t* ____jaisayyal_36;
	// System.UInt32 GarbageiOS.M_saraiSeecigea35::_nawsecu
	uint32_t ____nawsecu_37;
	// System.Boolean GarbageiOS.M_saraiSeecigea35::_pawsu
	bool ____pawsu_38;
	// System.Int32 GarbageiOS.M_saraiSeecigea35::_neaweliReadrem
	int32_t ____neaweliReadrem_39;
	// System.Single GarbageiOS.M_saraiSeecigea35::_hurallpuLurbi
	float ____hurallpuLurbi_40;
	// System.String GarbageiOS.M_saraiSeecigea35::_hasfasPanetair
	String_t* ____hasfasPanetair_41;
	// System.Int32 GarbageiOS.M_saraiSeecigea35::_mernas
	int32_t ____mernas_42;
	// System.UInt32 GarbageiOS.M_saraiSeecigea35::_yedrurKuke
	uint32_t ____yedrurKuke_43;
	// System.Single GarbageiOS.M_saraiSeecigea35::_naytease
	float ____naytease_44;
	// System.UInt32 GarbageiOS.M_saraiSeecigea35::_cheargicallJesir
	uint32_t ____cheargicallJesir_45;
	// System.UInt32 GarbageiOS.M_saraiSeecigea35::_kalldrasemHowramoo
	uint32_t ____kalldrasemHowramoo_46;

public:
	inline static int32_t get_offset_of__heabaceeBogoopa_0() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____heabaceeBogoopa_0)); }
	inline String_t* get__heabaceeBogoopa_0() const { return ____heabaceeBogoopa_0; }
	inline String_t** get_address_of__heabaceeBogoopa_0() { return &____heabaceeBogoopa_0; }
	inline void set__heabaceeBogoopa_0(String_t* value)
	{
		____heabaceeBogoopa_0 = value;
		Il2CppCodeGenWriteBarrier(&____heabaceeBogoopa_0, value);
	}

	inline static int32_t get_offset_of__cebo_1() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____cebo_1)); }
	inline uint32_t get__cebo_1() const { return ____cebo_1; }
	inline uint32_t* get_address_of__cebo_1() { return &____cebo_1; }
	inline void set__cebo_1(uint32_t value)
	{
		____cebo_1 = value;
	}

	inline static int32_t get_offset_of__zasow_2() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____zasow_2)); }
	inline float get__zasow_2() const { return ____zasow_2; }
	inline float* get_address_of__zasow_2() { return &____zasow_2; }
	inline void set__zasow_2(float value)
	{
		____zasow_2 = value;
	}

	inline static int32_t get_offset_of__nousesoKozaikoo_3() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____nousesoKozaikoo_3)); }
	inline int32_t get__nousesoKozaikoo_3() const { return ____nousesoKozaikoo_3; }
	inline int32_t* get_address_of__nousesoKozaikoo_3() { return &____nousesoKozaikoo_3; }
	inline void set__nousesoKozaikoo_3(int32_t value)
	{
		____nousesoKozaikoo_3 = value;
	}

	inline static int32_t get_offset_of__nalatri_4() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____nalatri_4)); }
	inline int32_t get__nalatri_4() const { return ____nalatri_4; }
	inline int32_t* get_address_of__nalatri_4() { return &____nalatri_4; }
	inline void set__nalatri_4(int32_t value)
	{
		____nalatri_4 = value;
	}

	inline static int32_t get_offset_of__lalxasfear_5() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____lalxasfear_5)); }
	inline bool get__lalxasfear_5() const { return ____lalxasfear_5; }
	inline bool* get_address_of__lalxasfear_5() { return &____lalxasfear_5; }
	inline void set__lalxasfear_5(bool value)
	{
		____lalxasfear_5 = value;
	}

	inline static int32_t get_offset_of__sojatal_6() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____sojatal_6)); }
	inline String_t* get__sojatal_6() const { return ____sojatal_6; }
	inline String_t** get_address_of__sojatal_6() { return &____sojatal_6; }
	inline void set__sojatal_6(String_t* value)
	{
		____sojatal_6 = value;
		Il2CppCodeGenWriteBarrier(&____sojatal_6, value);
	}

	inline static int32_t get_offset_of__vineepu_7() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____vineepu_7)); }
	inline bool get__vineepu_7() const { return ____vineepu_7; }
	inline bool* get_address_of__vineepu_7() { return &____vineepu_7; }
	inline void set__vineepu_7(bool value)
	{
		____vineepu_7 = value;
	}

	inline static int32_t get_offset_of__larwhudu_8() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____larwhudu_8)); }
	inline String_t* get__larwhudu_8() const { return ____larwhudu_8; }
	inline String_t** get_address_of__larwhudu_8() { return &____larwhudu_8; }
	inline void set__larwhudu_8(String_t* value)
	{
		____larwhudu_8 = value;
		Il2CppCodeGenWriteBarrier(&____larwhudu_8, value);
	}

	inline static int32_t get_offset_of__rasjalworMaytearrur_9() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____rasjalworMaytearrur_9)); }
	inline uint32_t get__rasjalworMaytearrur_9() const { return ____rasjalworMaytearrur_9; }
	inline uint32_t* get_address_of__rasjalworMaytearrur_9() { return &____rasjalworMaytearrur_9; }
	inline void set__rasjalworMaytearrur_9(uint32_t value)
	{
		____rasjalworMaytearrur_9 = value;
	}

	inline static int32_t get_offset_of__banou_10() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____banou_10)); }
	inline bool get__banou_10() const { return ____banou_10; }
	inline bool* get_address_of__banou_10() { return &____banou_10; }
	inline void set__banou_10(bool value)
	{
		____banou_10 = value;
	}

	inline static int32_t get_offset_of__torqeegouJacai_11() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____torqeegouJacai_11)); }
	inline String_t* get__torqeegouJacai_11() const { return ____torqeegouJacai_11; }
	inline String_t** get_address_of__torqeegouJacai_11() { return &____torqeegouJacai_11; }
	inline void set__torqeegouJacai_11(String_t* value)
	{
		____torqeegouJacai_11 = value;
		Il2CppCodeGenWriteBarrier(&____torqeegouJacai_11, value);
	}

	inline static int32_t get_offset_of__jababe_12() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____jababe_12)); }
	inline int32_t get__jababe_12() const { return ____jababe_12; }
	inline int32_t* get_address_of__jababe_12() { return &____jababe_12; }
	inline void set__jababe_12(int32_t value)
	{
		____jababe_12 = value;
	}

	inline static int32_t get_offset_of__mawtuwearCerecoo_13() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____mawtuwearCerecoo_13)); }
	inline bool get__mawtuwearCerecoo_13() const { return ____mawtuwearCerecoo_13; }
	inline bool* get_address_of__mawtuwearCerecoo_13() { return &____mawtuwearCerecoo_13; }
	inline void set__mawtuwearCerecoo_13(bool value)
	{
		____mawtuwearCerecoo_13 = value;
	}

	inline static int32_t get_offset_of__powbulouWeepawje_14() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____powbulouWeepawje_14)); }
	inline uint32_t get__powbulouWeepawje_14() const { return ____powbulouWeepawje_14; }
	inline uint32_t* get_address_of__powbulouWeepawje_14() { return &____powbulouWeepawje_14; }
	inline void set__powbulouWeepawje_14(uint32_t value)
	{
		____powbulouWeepawje_14 = value;
	}

	inline static int32_t get_offset_of__ballweparMumaybere_15() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____ballweparMumaybere_15)); }
	inline bool get__ballweparMumaybere_15() const { return ____ballweparMumaybere_15; }
	inline bool* get_address_of__ballweparMumaybere_15() { return &____ballweparMumaybere_15; }
	inline void set__ballweparMumaybere_15(bool value)
	{
		____ballweparMumaybere_15 = value;
	}

	inline static int32_t get_offset_of__reljearSisku_16() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____reljearSisku_16)); }
	inline int32_t get__reljearSisku_16() const { return ____reljearSisku_16; }
	inline int32_t* get_address_of__reljearSisku_16() { return &____reljearSisku_16; }
	inline void set__reljearSisku_16(int32_t value)
	{
		____reljearSisku_16 = value;
	}

	inline static int32_t get_offset_of__wawmiworSarjor_17() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____wawmiworSarjor_17)); }
	inline uint32_t get__wawmiworSarjor_17() const { return ____wawmiworSarjor_17; }
	inline uint32_t* get_address_of__wawmiworSarjor_17() { return &____wawmiworSarjor_17; }
	inline void set__wawmiworSarjor_17(uint32_t value)
	{
		____wawmiworSarjor_17 = value;
	}

	inline static int32_t get_offset_of__nejecarNoter_18() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____nejecarNoter_18)); }
	inline bool get__nejecarNoter_18() const { return ____nejecarNoter_18; }
	inline bool* get_address_of__nejecarNoter_18() { return &____nejecarNoter_18; }
	inline void set__nejecarNoter_18(bool value)
	{
		____nejecarNoter_18 = value;
	}

	inline static int32_t get_offset_of__nutaiFarpowcer_19() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____nutaiFarpowcer_19)); }
	inline uint32_t get__nutaiFarpowcer_19() const { return ____nutaiFarpowcer_19; }
	inline uint32_t* get_address_of__nutaiFarpowcer_19() { return &____nutaiFarpowcer_19; }
	inline void set__nutaiFarpowcer_19(uint32_t value)
	{
		____nutaiFarpowcer_19 = value;
	}

	inline static int32_t get_offset_of__wheeperirTimelhi_20() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____wheeperirTimelhi_20)); }
	inline bool get__wheeperirTimelhi_20() const { return ____wheeperirTimelhi_20; }
	inline bool* get_address_of__wheeperirTimelhi_20() { return &____wheeperirTimelhi_20; }
	inline void set__wheeperirTimelhi_20(bool value)
	{
		____wheeperirTimelhi_20 = value;
	}

	inline static int32_t get_offset_of__sedraiKorjearbe_21() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____sedraiKorjearbe_21)); }
	inline String_t* get__sedraiKorjearbe_21() const { return ____sedraiKorjearbe_21; }
	inline String_t** get_address_of__sedraiKorjearbe_21() { return &____sedraiKorjearbe_21; }
	inline void set__sedraiKorjearbe_21(String_t* value)
	{
		____sedraiKorjearbe_21 = value;
		Il2CppCodeGenWriteBarrier(&____sedraiKorjearbe_21, value);
	}

	inline static int32_t get_offset_of__foozenee_22() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____foozenee_22)); }
	inline int32_t get__foozenee_22() const { return ____foozenee_22; }
	inline int32_t* get_address_of__foozenee_22() { return &____foozenee_22; }
	inline void set__foozenee_22(int32_t value)
	{
		____foozenee_22 = value;
	}

	inline static int32_t get_offset_of__dougimal_23() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____dougimal_23)); }
	inline uint32_t get__dougimal_23() const { return ____dougimal_23; }
	inline uint32_t* get_address_of__dougimal_23() { return &____dougimal_23; }
	inline void set__dougimal_23(uint32_t value)
	{
		____dougimal_23 = value;
	}

	inline static int32_t get_offset_of__whearhalStallwe_24() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____whearhalStallwe_24)); }
	inline uint32_t get__whearhalStallwe_24() const { return ____whearhalStallwe_24; }
	inline uint32_t* get_address_of__whearhalStallwe_24() { return &____whearhalStallwe_24; }
	inline void set__whearhalStallwe_24(uint32_t value)
	{
		____whearhalStallwe_24 = value;
	}

	inline static int32_t get_offset_of__nerqalchawGurwis_25() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____nerqalchawGurwis_25)); }
	inline uint32_t get__nerqalchawGurwis_25() const { return ____nerqalchawGurwis_25; }
	inline uint32_t* get_address_of__nerqalchawGurwis_25() { return &____nerqalchawGurwis_25; }
	inline void set__nerqalchawGurwis_25(uint32_t value)
	{
		____nerqalchawGurwis_25 = value;
	}

	inline static int32_t get_offset_of__madou_26() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____madou_26)); }
	inline uint32_t get__madou_26() const { return ____madou_26; }
	inline uint32_t* get_address_of__madou_26() { return &____madou_26; }
	inline void set__madou_26(uint32_t value)
	{
		____madou_26 = value;
	}

	inline static int32_t get_offset_of__sejisooFowrirhu_27() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____sejisooFowrirhu_27)); }
	inline String_t* get__sejisooFowrirhu_27() const { return ____sejisooFowrirhu_27; }
	inline String_t** get_address_of__sejisooFowrirhu_27() { return &____sejisooFowrirhu_27; }
	inline void set__sejisooFowrirhu_27(String_t* value)
	{
		____sejisooFowrirhu_27 = value;
		Il2CppCodeGenWriteBarrier(&____sejisooFowrirhu_27, value);
	}

	inline static int32_t get_offset_of__stalllorpea_28() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____stalllorpea_28)); }
	inline float get__stalllorpea_28() const { return ____stalllorpea_28; }
	inline float* get_address_of__stalllorpea_28() { return &____stalllorpea_28; }
	inline void set__stalllorpea_28(float value)
	{
		____stalllorpea_28 = value;
	}

	inline static int32_t get_offset_of__jismiBirnee_29() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____jismiBirnee_29)); }
	inline uint32_t get__jismiBirnee_29() const { return ____jismiBirnee_29; }
	inline uint32_t* get_address_of__jismiBirnee_29() { return &____jismiBirnee_29; }
	inline void set__jismiBirnee_29(uint32_t value)
	{
		____jismiBirnee_29 = value;
	}

	inline static int32_t get_offset_of__caidrenereKergar_30() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____caidrenereKergar_30)); }
	inline bool get__caidrenereKergar_30() const { return ____caidrenereKergar_30; }
	inline bool* get_address_of__caidrenereKergar_30() { return &____caidrenereKergar_30; }
	inline void set__caidrenereKergar_30(bool value)
	{
		____caidrenereKergar_30 = value;
	}

	inline static int32_t get_offset_of__tarcavair_31() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____tarcavair_31)); }
	inline uint32_t get__tarcavair_31() const { return ____tarcavair_31; }
	inline uint32_t* get_address_of__tarcavair_31() { return &____tarcavair_31; }
	inline void set__tarcavair_31(uint32_t value)
	{
		____tarcavair_31 = value;
	}

	inline static int32_t get_offset_of__gouseyow_32() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____gouseyow_32)); }
	inline float get__gouseyow_32() const { return ____gouseyow_32; }
	inline float* get_address_of__gouseyow_32() { return &____gouseyow_32; }
	inline void set__gouseyow_32(float value)
	{
		____gouseyow_32 = value;
	}

	inline static int32_t get_offset_of__caybereXereherekow_33() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____caybereXereherekow_33)); }
	inline uint32_t get__caybereXereherekow_33() const { return ____caybereXereherekow_33; }
	inline uint32_t* get_address_of__caybereXereherekow_33() { return &____caybereXereherekow_33; }
	inline void set__caybereXereherekow_33(uint32_t value)
	{
		____caybereXereherekow_33 = value;
	}

	inline static int32_t get_offset_of__toujalni_34() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____toujalni_34)); }
	inline String_t* get__toujalni_34() const { return ____toujalni_34; }
	inline String_t** get_address_of__toujalni_34() { return &____toujalni_34; }
	inline void set__toujalni_34(String_t* value)
	{
		____toujalni_34 = value;
		Il2CppCodeGenWriteBarrier(&____toujalni_34, value);
	}

	inline static int32_t get_offset_of__telmeljea_35() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____telmeljea_35)); }
	inline float get__telmeljea_35() const { return ____telmeljea_35; }
	inline float* get_address_of__telmeljea_35() { return &____telmeljea_35; }
	inline void set__telmeljea_35(float value)
	{
		____telmeljea_35 = value;
	}

	inline static int32_t get_offset_of__jaisayyal_36() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____jaisayyal_36)); }
	inline String_t* get__jaisayyal_36() const { return ____jaisayyal_36; }
	inline String_t** get_address_of__jaisayyal_36() { return &____jaisayyal_36; }
	inline void set__jaisayyal_36(String_t* value)
	{
		____jaisayyal_36 = value;
		Il2CppCodeGenWriteBarrier(&____jaisayyal_36, value);
	}

	inline static int32_t get_offset_of__nawsecu_37() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____nawsecu_37)); }
	inline uint32_t get__nawsecu_37() const { return ____nawsecu_37; }
	inline uint32_t* get_address_of__nawsecu_37() { return &____nawsecu_37; }
	inline void set__nawsecu_37(uint32_t value)
	{
		____nawsecu_37 = value;
	}

	inline static int32_t get_offset_of__pawsu_38() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____pawsu_38)); }
	inline bool get__pawsu_38() const { return ____pawsu_38; }
	inline bool* get_address_of__pawsu_38() { return &____pawsu_38; }
	inline void set__pawsu_38(bool value)
	{
		____pawsu_38 = value;
	}

	inline static int32_t get_offset_of__neaweliReadrem_39() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____neaweliReadrem_39)); }
	inline int32_t get__neaweliReadrem_39() const { return ____neaweliReadrem_39; }
	inline int32_t* get_address_of__neaweliReadrem_39() { return &____neaweliReadrem_39; }
	inline void set__neaweliReadrem_39(int32_t value)
	{
		____neaweliReadrem_39 = value;
	}

	inline static int32_t get_offset_of__hurallpuLurbi_40() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____hurallpuLurbi_40)); }
	inline float get__hurallpuLurbi_40() const { return ____hurallpuLurbi_40; }
	inline float* get_address_of__hurallpuLurbi_40() { return &____hurallpuLurbi_40; }
	inline void set__hurallpuLurbi_40(float value)
	{
		____hurallpuLurbi_40 = value;
	}

	inline static int32_t get_offset_of__hasfasPanetair_41() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____hasfasPanetair_41)); }
	inline String_t* get__hasfasPanetair_41() const { return ____hasfasPanetair_41; }
	inline String_t** get_address_of__hasfasPanetair_41() { return &____hasfasPanetair_41; }
	inline void set__hasfasPanetair_41(String_t* value)
	{
		____hasfasPanetair_41 = value;
		Il2CppCodeGenWriteBarrier(&____hasfasPanetair_41, value);
	}

	inline static int32_t get_offset_of__mernas_42() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____mernas_42)); }
	inline int32_t get__mernas_42() const { return ____mernas_42; }
	inline int32_t* get_address_of__mernas_42() { return &____mernas_42; }
	inline void set__mernas_42(int32_t value)
	{
		____mernas_42 = value;
	}

	inline static int32_t get_offset_of__yedrurKuke_43() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____yedrurKuke_43)); }
	inline uint32_t get__yedrurKuke_43() const { return ____yedrurKuke_43; }
	inline uint32_t* get_address_of__yedrurKuke_43() { return &____yedrurKuke_43; }
	inline void set__yedrurKuke_43(uint32_t value)
	{
		____yedrurKuke_43 = value;
	}

	inline static int32_t get_offset_of__naytease_44() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____naytease_44)); }
	inline float get__naytease_44() const { return ____naytease_44; }
	inline float* get_address_of__naytease_44() { return &____naytease_44; }
	inline void set__naytease_44(float value)
	{
		____naytease_44 = value;
	}

	inline static int32_t get_offset_of__cheargicallJesir_45() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____cheargicallJesir_45)); }
	inline uint32_t get__cheargicallJesir_45() const { return ____cheargicallJesir_45; }
	inline uint32_t* get_address_of__cheargicallJesir_45() { return &____cheargicallJesir_45; }
	inline void set__cheargicallJesir_45(uint32_t value)
	{
		____cheargicallJesir_45 = value;
	}

	inline static int32_t get_offset_of__kalldrasemHowramoo_46() { return static_cast<int32_t>(offsetof(M_saraiSeecigea35_t3093906936, ____kalldrasemHowramoo_46)); }
	inline uint32_t get__kalldrasemHowramoo_46() const { return ____kalldrasemHowramoo_46; }
	inline uint32_t* get_address_of__kalldrasemHowramoo_46() { return &____kalldrasemHowramoo_46; }
	inline void set__kalldrasemHowramoo_46(uint32_t value)
	{
		____kalldrasemHowramoo_46 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
