﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._91c8f6ddaa2dc58e696f6bf2ccc3859e
struct _91c8f6ddaa2dc58e696f6bf2ccc3859e_t984443691;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._91c8f6ddaa2dc58e696f6bf2ccc3859e::.ctor()
extern "C"  void _91c8f6ddaa2dc58e696f6bf2ccc3859e__ctor_m3998489442 (_91c8f6ddaa2dc58e696f6bf2ccc3859e_t984443691 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._91c8f6ddaa2dc58e696f6bf2ccc3859e::_91c8f6ddaa2dc58e696f6bf2ccc3859em2(System.Int32)
extern "C"  int32_t _91c8f6ddaa2dc58e696f6bf2ccc3859e__91c8f6ddaa2dc58e696f6bf2ccc3859em2_m2308532665 (_91c8f6ddaa2dc58e696f6bf2ccc3859e_t984443691 * __this, int32_t ____91c8f6ddaa2dc58e696f6bf2ccc3859ea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._91c8f6ddaa2dc58e696f6bf2ccc3859e::_91c8f6ddaa2dc58e696f6bf2ccc3859em(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _91c8f6ddaa2dc58e696f6bf2ccc3859e__91c8f6ddaa2dc58e696f6bf2ccc3859em_m27828509 (_91c8f6ddaa2dc58e696f6bf2ccc3859e_t984443691 * __this, int32_t ____91c8f6ddaa2dc58e696f6bf2ccc3859ea0, int32_t ____91c8f6ddaa2dc58e696f6bf2ccc3859e141, int32_t ____91c8f6ddaa2dc58e696f6bf2ccc3859ec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
