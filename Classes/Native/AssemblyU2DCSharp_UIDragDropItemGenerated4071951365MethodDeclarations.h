﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIDragDropItemGenerated
struct UIDragDropItemGenerated_t4071951365;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UIDragDropItemGenerated::.ctor()
extern "C"  void UIDragDropItemGenerated__ctor_m901122310 (UIDragDropItemGenerated_t4071951365 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDragDropItemGenerated::UIDragDropItem_UIDragDropItem1(JSVCall,System.Int32)
extern "C"  bool UIDragDropItemGenerated_UIDragDropItem_UIDragDropItem1_m1843115052 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItemGenerated::UIDragDropItem_restriction(JSVCall)
extern "C"  void UIDragDropItemGenerated_UIDragDropItem_restriction_m110099058 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItemGenerated::UIDragDropItem_cloneOnDrag(JSVCall)
extern "C"  void UIDragDropItemGenerated_UIDragDropItem_cloneOnDrag_m4152012622 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItemGenerated::UIDragDropItem_pressAndHoldDelay(JSVCall)
extern "C"  void UIDragDropItemGenerated_UIDragDropItem_pressAndHoldDelay_m3571163374 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIDragDropItemGenerated::UIDragDropItem_StopDragging__GameObject(JSVCall,System.Int32)
extern "C"  bool UIDragDropItemGenerated_UIDragDropItem_StopDragging__GameObject_m2869973087 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItemGenerated::__Register()
extern "C"  void UIDragDropItemGenerated___Register_m3079022465 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIDragDropItemGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UIDragDropItemGenerated_ilo_getObject1_m2446878448 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItemGenerated::ilo_setEnum2(System.Int32,System.Int32)
extern "C"  void UIDragDropItemGenerated_ilo_setEnum2_m563578374 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragDropItemGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void UIDragDropItemGenerated_ilo_setBooleanS3_m1301663285 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIDragDropItemGenerated::ilo_getSingle4(System.Int32)
extern "C"  float UIDragDropItemGenerated_ilo_getSingle4_m512864652 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
