﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Net.CredentialCache/CredentialCacheKey
struct CredentialCacheKey_t3855037160;
// System.String
struct String_t;
// System.Uri
struct Uri_t1116831938;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Int32 System.Net.CredentialCache/CredentialCacheKey::get_Length()
extern "C"  int32_t CredentialCacheKey_get_Length_m3054526753 (CredentialCacheKey_t3855037160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.CredentialCache/CredentialCacheKey::get_AbsPath()
extern "C"  String_t* CredentialCacheKey_get_AbsPath_m2857234225 (CredentialCacheKey_t3855037160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Uri System.Net.CredentialCache/CredentialCacheKey::get_UriPrefix()
extern "C"  Uri_t1116831938 * CredentialCacheKey_get_UriPrefix_m4200746567 (CredentialCacheKey_t3855037160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.CredentialCache/CredentialCacheKey::get_AuthType()
extern "C"  String_t* CredentialCacheKey_get_AuthType_m3893816426 (CredentialCacheKey_t3855037160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Net.CredentialCache/CredentialCacheKey::GetHashCode()
extern "C"  int32_t CredentialCacheKey_GetHashCode_m2782141569 (CredentialCacheKey_t3855037160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Net.CredentialCache/CredentialCacheKey::Equals(System.Object)
extern "C"  bool CredentialCacheKey_Equals_m2605006121 (CredentialCacheKey_t3855037160 * __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Net.CredentialCache/CredentialCacheKey::ToString()
extern "C"  String_t* CredentialCacheKey_ToString_m4049508811 (CredentialCacheKey_t3855037160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
