﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICameraGenerated/<UICamera_onDrop_GetDelegate_member53_arg0>c__AnonStoreyAE
struct U3CUICamera_onDrop_GetDelegate_member53_arg0U3Ec__AnonStoreyAE_t3434040988;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UICameraGenerated/<UICamera_onDrop_GetDelegate_member53_arg0>c__AnonStoreyAE::.ctor()
extern "C"  void U3CUICamera_onDrop_GetDelegate_member53_arg0U3Ec__AnonStoreyAE__ctor_m4141126527 (U3CUICamera_onDrop_GetDelegate_member53_arg0U3Ec__AnonStoreyAE_t3434040988 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated/<UICamera_onDrop_GetDelegate_member53_arg0>c__AnonStoreyAE::<>m__120(UnityEngine.GameObject,UnityEngine.GameObject)
extern "C"  void U3CUICamera_onDrop_GetDelegate_member53_arg0U3Ec__AnonStoreyAE_U3CU3Em__120_m2744988893 (U3CUICamera_onDrop_GetDelegate_member53_arg0U3Ec__AnonStoreyAE_t3434040988 * __this, GameObject_t3674682005 * ___go0, GameObject_t3674682005 * ___obj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
