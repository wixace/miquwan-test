﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginYiJie
struct PluginYiJie_t2559429187;
// System.String
struct String_t;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// SFJSONObject
struct SFJSONObject_t2643764442;
// System.Object
struct Il2CppObject;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t2362096582;
// VersionMgr
struct VersionMgr_t1322950208;
// System.Action
struct Action_t3771233898;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "AssemblyU2DCSharp_SFJSONObject2643764442.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PluginsSdkMgr3884624670.h"
#include "AssemblyU2DCSharp_VersionMgr1322950208.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_PluginYiJie2559429187.h"

// System.Void PluginYiJie::.ctor()
extern "C"  void PluginYiJie__ctor_m3954805896 (PluginYiJie_t2559429187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::exitInterface(System.IntPtr,System.String,System.String)
extern "C"  void PluginYiJie_exitInterface_m111266603 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, String_t* ___gameObject1, String_t* ___listener2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::onCreate_listener(System.IntPtr,System.String,System.String)
extern "C"  void PluginYiJie_onCreate_listener_m3663822478 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, String_t* ___gameObject1, String_t* ___listener2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::login(System.IntPtr,System.String)
extern "C"  void PluginYiJie_login_m3343825217 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, String_t* ___customParams1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::logout(System.IntPtr,System.String)
extern "C"  void PluginYiJie_logout_m573556202 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, String_t* ___customParams1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::charge(System.IntPtr,System.String,System.String,System.Int32,System.Int32,System.String,System.String,System.String)
extern "C"  void PluginYiJie_charge_m3942225488 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, String_t* ___gameObject1, String_t* ___itemName2, int32_t ___unitPrice3, int32_t ___count4, String_t* ___callBackInfo5, String_t* ___callBackUrl6, String_t* ___payResultListener7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::pay(System.IntPtr,System.String,System.Int32,System.String,System.Int32,System.String,System.String,System.String)
extern "C"  void PluginYiJie_pay_m615261754 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, String_t* ___gameObject1, int32_t ___unitPrice2, String_t* ___unitName3, int32_t ___count4, String_t* ___callBackInfo5, String_t* ___callBackUrl6, String_t* ___payResultListener7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::payExtend(System.IntPtr,System.String,System.Int32,System.String,System.String,System.String,System.Int32,System.String,System.String,System.String)
extern "C"  void PluginYiJie_payExtend_m1149502856 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, String_t* ___gameObject1, int32_t ___unitPrice2, String_t* ___unitName3, String_t* ___itemCode4, String_t* ___remain5, int32_t ___count6, String_t* ___callBackInfo7, String_t* ___callBackUrl8, String_t* ___payResultListener9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::setRoleData(System.IntPtr,System.String,System.String,System.String,System.String,System.String)
extern "C"  void PluginYiJie_setRoleData_m4024880696 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, String_t* ___roleId1, String_t* ___roleName2, String_t* ___roleLevel3, String_t* ___zoneId4, String_t* ___zoneName5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::setData(System.IntPtr,System.String,System.String)
extern "C"  void PluginYiJie_setData_m2724201818 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, String_t* ___key1, String_t* ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::setLoginListener(System.IntPtr,System.String,System.String)
extern "C"  void PluginYiJie_setLoginListener_m65798485 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, String_t* ___gameObject1, String_t* ___listener2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYiJie::isMusicEnabled(System.IntPtr)
extern "C"  bool PluginYiJie_isMusicEnabled_m2272736582 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::Init()
extern "C"  void PluginYiJie_Init_m3681341644 (PluginYiJie_t2559429187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::OnDestory()
extern "C"  void PluginYiJie_OnDestory_m3006854171 (PluginYiJie_t2559429187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::clear()
extern "C"  void PluginYiJie_clear_m3991253203 (PluginYiJie_t2559429187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::CreatRole(CEvent.ZEvent)
extern "C"  void PluginYiJie_CreatRole_m2044567702 (PluginYiJie_t2559429187 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::RoleUp(CEvent.ZEvent)
extern "C"  void PluginYiJie_RoleUp_m1065509678 (PluginYiJie_t2559429187 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::RoleEnterGame(CEvent.ZEvent)
extern "C"  void PluginYiJie_RoleEnterGame_m3254086657 (PluginYiJie_t2559429187 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::UserPay(CEvent.ZEvent)
extern "C"  void PluginYiJie_UserPay_m3181077336 (PluginYiJie_t2559429187 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.Dictionary`2<System.String,System.String> PluginYiJie::ReqSDKHttpLogin()
extern "C"  Dictionary_2_t827649927 * PluginYiJie_ReqSDKHttpLogin_m3939532793 (PluginYiJie_t2559429187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYiJie::IsLoginSuccess(System.Boolean)
extern "C"  bool PluginYiJie_IsLoginSuccess_m3040154083 (PluginYiJie_t2559429187 * __this, bool ___isOpenUseLogin0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::OnLogoutSuccess(System.String)
extern "C"  void PluginYiJie_OnLogoutSuccess_m2524172962 (PluginYiJie_t2559429187 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::OnLogoutFail(System.String)
extern "C"  void PluginYiJie_OnLogoutFail_m3539038687 (PluginYiJie_t2559429187 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::onBackPressed_YJ(System.String)
extern "C"  void PluginYiJie_onBackPressed_YJ_m511105682 (PluginYiJie_t2559429187 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::Yj_Login()
extern "C"  void PluginYiJie_Yj_Login_m3779264535 (PluginYiJie_t2559429187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::Yj_Logout()
extern "C"  void PluginYiJie_Yj_Logout_m1198906302 (PluginYiJie_t2559429187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::InitResponse(System.String)
extern "C"  void PluginYiJie_InitResponse_m954150453 (PluginYiJie_t2559429187 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::LoginResult(System.String)
extern "C"  void PluginYiJie_LoginResult_m1433528630 (PluginYiJie_t2559429187 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::ExitResult(System.String)
extern "C"  void PluginYiJie_ExitResult_m1874125003 (PluginYiJie_t2559429187 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::PayResult(System.String)
extern "C"  void PluginYiJie_PayResult_m1302218775 (PluginYiJie_t2559429187 * __this, String_t* ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::<OnLogoutSuccess>m__46F()
extern "C"  void PluginYiJie_U3COnLogoutSuccessU3Em__46F_m4287826685 (PluginYiJie_t2559429187 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginYiJie_ilo_AddEventListener1_m1847784300 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::ilo_onCreate_listener2(System.IntPtr,System.String,System.String)
extern "C"  void PluginYiJie_ilo_onCreate_listener2_m4269345379 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, String_t* ___gameObject1, String_t* ___listener2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::ilo_RemoveEventListener3(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginYiJie_ilo_RemoveEventListener3_m2720524363 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::ilo_put4(SFJSONObject,System.String,System.Object)
extern "C"  void PluginYiJie_ilo_put4_m2469009604 (Il2CppObject * __this /* static, unused */, SFJSONObject_t2643764442 * ____this0, String_t* ___key1, Il2CppObject * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::ilo_setData5(System.IntPtr,System.String,System.String)
extern "C"  void PluginYiJie_ilo_setData5_m1743158644 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, String_t* ___key1, String_t* ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginYiJie::ilo_get_PluginsSdkMgr6()
extern "C"  PluginsSdkMgr_t3884624670 * PluginYiJie_ilo_get_PluginsSdkMgr6_m1432596955 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AndroidJavaObject PluginYiJie::ilo_get_androidJavaObject7(PluginsSdkMgr)
extern "C"  AndroidJavaObject_t2362096582 * PluginYiJie_ilo_get_androidJavaObject7_m1495037743 (Il2CppObject * __this /* static, unused */, PluginsSdkMgr_t3884624670 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginYiJie::ilo_get_isSdkLogin8(VersionMgr)
extern "C"  bool PluginYiJie_ilo_get_isSdkLogin8_m1827881341 (Il2CppObject * __this /* static, unused */, VersionMgr_t1322950208 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// VersionMgr PluginYiJie::ilo_get_Instance9()
extern "C"  VersionMgr_t1322950208 * PluginYiJie_ilo_get_Instance9_m212207275 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::ilo_Logout10(System.Action)
extern "C"  void PluginYiJie_ilo_Logout10_m3941016165 (Il2CppObject * __this /* static, unused */, Action_t3771233898 * ___call0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::ilo_Log11(System.Object,System.Boolean)
extern "C"  void PluginYiJie_ilo_Log11_m3271190496 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___message0, bool ___isShowStack1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::ilo_login12(System.IntPtr,System.String)
extern "C"  void PluginYiJie_ilo_login12_m2793791859 (Il2CppObject * __this /* static, unused */, IntPtr_t ___context0, String_t* ___customParams1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PluginYiJie::ilo_get13(SFJSONObject,System.String)
extern "C"  Il2CppObject * PluginYiJie_ilo_get13_m2806373122 (Il2CppObject * __this /* static, unused */, SFJSONObject_t2643764442 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginYiJie::ilo_OnLogoutSuccess14(PluginYiJie,System.String)
extern "C"  void PluginYiJie_ilo_OnLogoutSuccess14_m1283456923 (Il2CppObject * __this /* static, unused */, PluginYiJie_t2559429187 * ____this0, String_t* ___message1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
