﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginYouMa
struct  PluginYouMa_t2559648384  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginYouMa::uid
	String_t* ___uid_2;
	// System.String PluginYouMa::session
	String_t* ___session_3;
	// System.String PluginYouMa::loginTime
	String_t* ___loginTime_4;
	// System.Boolean PluginYouMa::isOpenUserSwitch
	bool ___isOpenUserSwitch_5;
	// System.String[] PluginYouMa::sdkInfo
	StringU5BU5D_t4054002952* ___sdkInfo_6;
	// Mihua.SDK.PayInfo PluginYouMa::info
	PayInfo_t1775308120 * ___info_7;
	// System.String PluginYouMa::urlScheme
	String_t* ___urlScheme_8;

public:
	inline static int32_t get_offset_of_uid_2() { return static_cast<int32_t>(offsetof(PluginYouMa_t2559648384, ___uid_2)); }
	inline String_t* get_uid_2() const { return ___uid_2; }
	inline String_t** get_address_of_uid_2() { return &___uid_2; }
	inline void set_uid_2(String_t* value)
	{
		___uid_2 = value;
		Il2CppCodeGenWriteBarrier(&___uid_2, value);
	}

	inline static int32_t get_offset_of_session_3() { return static_cast<int32_t>(offsetof(PluginYouMa_t2559648384, ___session_3)); }
	inline String_t* get_session_3() const { return ___session_3; }
	inline String_t** get_address_of_session_3() { return &___session_3; }
	inline void set_session_3(String_t* value)
	{
		___session_3 = value;
		Il2CppCodeGenWriteBarrier(&___session_3, value);
	}

	inline static int32_t get_offset_of_loginTime_4() { return static_cast<int32_t>(offsetof(PluginYouMa_t2559648384, ___loginTime_4)); }
	inline String_t* get_loginTime_4() const { return ___loginTime_4; }
	inline String_t** get_address_of_loginTime_4() { return &___loginTime_4; }
	inline void set_loginTime_4(String_t* value)
	{
		___loginTime_4 = value;
		Il2CppCodeGenWriteBarrier(&___loginTime_4, value);
	}

	inline static int32_t get_offset_of_isOpenUserSwitch_5() { return static_cast<int32_t>(offsetof(PluginYouMa_t2559648384, ___isOpenUserSwitch_5)); }
	inline bool get_isOpenUserSwitch_5() const { return ___isOpenUserSwitch_5; }
	inline bool* get_address_of_isOpenUserSwitch_5() { return &___isOpenUserSwitch_5; }
	inline void set_isOpenUserSwitch_5(bool value)
	{
		___isOpenUserSwitch_5 = value;
	}

	inline static int32_t get_offset_of_sdkInfo_6() { return static_cast<int32_t>(offsetof(PluginYouMa_t2559648384, ___sdkInfo_6)); }
	inline StringU5BU5D_t4054002952* get_sdkInfo_6() const { return ___sdkInfo_6; }
	inline StringU5BU5D_t4054002952** get_address_of_sdkInfo_6() { return &___sdkInfo_6; }
	inline void set_sdkInfo_6(StringU5BU5D_t4054002952* value)
	{
		___sdkInfo_6 = value;
		Il2CppCodeGenWriteBarrier(&___sdkInfo_6, value);
	}

	inline static int32_t get_offset_of_info_7() { return static_cast<int32_t>(offsetof(PluginYouMa_t2559648384, ___info_7)); }
	inline PayInfo_t1775308120 * get_info_7() const { return ___info_7; }
	inline PayInfo_t1775308120 ** get_address_of_info_7() { return &___info_7; }
	inline void set_info_7(PayInfo_t1775308120 * value)
	{
		___info_7 = value;
		Il2CppCodeGenWriteBarrier(&___info_7, value);
	}

	inline static int32_t get_offset_of_urlScheme_8() { return static_cast<int32_t>(offsetof(PluginYouMa_t2559648384, ___urlScheme_8)); }
	inline String_t* get_urlScheme_8() const { return ___urlScheme_8; }
	inline String_t** get_address_of_urlScheme_8() { return &___urlScheme_8; }
	inline void set_urlScheme_8(String_t* value)
	{
		___urlScheme_8 = value;
		Il2CppCodeGenWriteBarrier(&___urlScheme_8, value);
	}
};

struct PluginYouMa_t2559648384_StaticFields
{
public:
	// System.Action PluginYouMa::<>f__am$cache7
	Action_t3771233898 * ___U3CU3Ef__amU24cache7_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_9() { return static_cast<int32_t>(offsetof(PluginYouMa_t2559648384_StaticFields, ___U3CU3Ef__amU24cache7_9)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache7_9() const { return ___U3CU3Ef__amU24cache7_9; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache7_9() { return &___U3CU3Ef__amU24cache7_9; }
	inline void set_U3CU3Ef__amU24cache7_9(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache7_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
