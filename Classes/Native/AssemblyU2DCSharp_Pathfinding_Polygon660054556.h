﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Polygon
struct  Polygon_t660054556  : public Il2CppObject
{
public:

public:
};

struct Polygon_t660054556_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> Pathfinding.Polygon::hullCache
	List_1_t1355284822 * ___hullCache_0;

public:
	inline static int32_t get_offset_of_hullCache_0() { return static_cast<int32_t>(offsetof(Polygon_t660054556_StaticFields, ___hullCache_0)); }
	inline List_1_t1355284822 * get_hullCache_0() const { return ___hullCache_0; }
	inline List_1_t1355284822 ** get_address_of_hullCache_0() { return &___hullCache_0; }
	inline void set_hullCache_0(List_1_t1355284822 * value)
	{
		___hullCache_0 = value;
		Il2CppCodeGenWriteBarrier(&___hullCache_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
