﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UILabel
struct UILabel_t291504320;
// EventDelegate/Callback
struct Callback_t1094463061;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RollNumber
struct  RollNumber_t957354566  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean RollNumber::_isPlay
	bool ____isPlay_2;
	// System.Single RollNumber::_time
	float ____time_3;
	// System.Single RollNumber::_start
	float ____start_4;
	// System.Single RollNumber::_end
	float ____end_5;
	// System.Single RollNumber::_delay
	float ____delay_6;
	// System.Single RollNumber::_speed
	float ____speed_7;
	// System.Boolean RollNumber::_isAdd
	bool ____isAdd_8;
	// UILabel RollNumber::_label
	UILabel_t291504320 * ____label_9;
	// EventDelegate/Callback RollNumber::_callBack
	Callback_t1094463061 * ____callBack_10;
	// System.Single RollNumber::_temTime
	float ____temTime_11;

public:
	inline static int32_t get_offset_of__isPlay_2() { return static_cast<int32_t>(offsetof(RollNumber_t957354566, ____isPlay_2)); }
	inline bool get__isPlay_2() const { return ____isPlay_2; }
	inline bool* get_address_of__isPlay_2() { return &____isPlay_2; }
	inline void set__isPlay_2(bool value)
	{
		____isPlay_2 = value;
	}

	inline static int32_t get_offset_of__time_3() { return static_cast<int32_t>(offsetof(RollNumber_t957354566, ____time_3)); }
	inline float get__time_3() const { return ____time_3; }
	inline float* get_address_of__time_3() { return &____time_3; }
	inline void set__time_3(float value)
	{
		____time_3 = value;
	}

	inline static int32_t get_offset_of__start_4() { return static_cast<int32_t>(offsetof(RollNumber_t957354566, ____start_4)); }
	inline float get__start_4() const { return ____start_4; }
	inline float* get_address_of__start_4() { return &____start_4; }
	inline void set__start_4(float value)
	{
		____start_4 = value;
	}

	inline static int32_t get_offset_of__end_5() { return static_cast<int32_t>(offsetof(RollNumber_t957354566, ____end_5)); }
	inline float get__end_5() const { return ____end_5; }
	inline float* get_address_of__end_5() { return &____end_5; }
	inline void set__end_5(float value)
	{
		____end_5 = value;
	}

	inline static int32_t get_offset_of__delay_6() { return static_cast<int32_t>(offsetof(RollNumber_t957354566, ____delay_6)); }
	inline float get__delay_6() const { return ____delay_6; }
	inline float* get_address_of__delay_6() { return &____delay_6; }
	inline void set__delay_6(float value)
	{
		____delay_6 = value;
	}

	inline static int32_t get_offset_of__speed_7() { return static_cast<int32_t>(offsetof(RollNumber_t957354566, ____speed_7)); }
	inline float get__speed_7() const { return ____speed_7; }
	inline float* get_address_of__speed_7() { return &____speed_7; }
	inline void set__speed_7(float value)
	{
		____speed_7 = value;
	}

	inline static int32_t get_offset_of__isAdd_8() { return static_cast<int32_t>(offsetof(RollNumber_t957354566, ____isAdd_8)); }
	inline bool get__isAdd_8() const { return ____isAdd_8; }
	inline bool* get_address_of__isAdd_8() { return &____isAdd_8; }
	inline void set__isAdd_8(bool value)
	{
		____isAdd_8 = value;
	}

	inline static int32_t get_offset_of__label_9() { return static_cast<int32_t>(offsetof(RollNumber_t957354566, ____label_9)); }
	inline UILabel_t291504320 * get__label_9() const { return ____label_9; }
	inline UILabel_t291504320 ** get_address_of__label_9() { return &____label_9; }
	inline void set__label_9(UILabel_t291504320 * value)
	{
		____label_9 = value;
		Il2CppCodeGenWriteBarrier(&____label_9, value);
	}

	inline static int32_t get_offset_of__callBack_10() { return static_cast<int32_t>(offsetof(RollNumber_t957354566, ____callBack_10)); }
	inline Callback_t1094463061 * get__callBack_10() const { return ____callBack_10; }
	inline Callback_t1094463061 ** get_address_of__callBack_10() { return &____callBack_10; }
	inline void set__callBack_10(Callback_t1094463061 * value)
	{
		____callBack_10 = value;
		Il2CppCodeGenWriteBarrier(&____callBack_10, value);
	}

	inline static int32_t get_offset_of__temTime_11() { return static_cast<int32_t>(offsetof(RollNumber_t957354566, ____temTime_11)); }
	inline float get__temTime_11() const { return ____temTime_11; }
	inline float* get_address_of__temTime_11() { return &____temTime_11; }
	inline void set__temTime_11(float value)
	{
		____temTime_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
