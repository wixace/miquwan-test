﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIButtonScale
struct UIButtonScale_t1560517124;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DCSharp_UIButtonScale1560517124.h"

// System.Void UIButtonScale::.ctor()
extern "C"  void UIButtonScale__ctor_m2085211111 (UIButtonScale_t1560517124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonScale::Start()
extern "C"  void UIButtonScale_Start_m1032348903 (UIButtonScale_t1560517124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonScale::OnEnable()
extern "C"  void UIButtonScale_OnEnable_m188816959 (UIButtonScale_t1560517124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonScale::OnDisable()
extern "C"  void UIButtonScale_OnDisable_m1999295694 (UIButtonScale_t1560517124 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonScale::OnPress(System.Boolean)
extern "C"  void UIButtonScale_OnPress_m2879896160 (UIButtonScale_t1560517124 * __this, bool ___isPressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonScale::OnHover(System.Boolean)
extern "C"  void UIButtonScale_OnHover_m665242969 (UIButtonScale_t1560517124 * __this, bool ___isOver0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonScale::OnSelect(System.Boolean)
extern "C"  void UIButtonScale_OnSelect_m3004200687 (UIButtonScale_t1560517124 * __this, bool ___isSelected0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIButtonScale::ilo_IsHighlighted1(UnityEngine.GameObject)
extern "C"  bool UIButtonScale_ilo_IsHighlighted1_m3112300412 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIButtonScale::ilo_Start2(UIButtonScale)
extern "C"  void UIButtonScale_ilo_Start2_m823626094 (Il2CppObject * __this /* static, unused */, UIButtonScale_t1560517124 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
