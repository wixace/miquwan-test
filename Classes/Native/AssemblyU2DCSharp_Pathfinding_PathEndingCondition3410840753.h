﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Path
struct Path_t1974241691;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.PathEndingCondition
struct  PathEndingCondition_t3410840753  : public Il2CppObject
{
public:
	// Pathfinding.Path Pathfinding.PathEndingCondition::p
	Path_t1974241691 * ___p_0;

public:
	inline static int32_t get_offset_of_p_0() { return static_cast<int32_t>(offsetof(PathEndingCondition_t3410840753, ___p_0)); }
	inline Path_t1974241691 * get_p_0() const { return ___p_0; }
	inline Path_t1974241691 ** get_address_of_p_0() { return &___p_0; }
	inline void set_p_0(Path_t1974241691 * value)
	{
		___p_0 = value;
		Il2CppCodeGenWriteBarrier(&___p_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
