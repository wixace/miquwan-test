﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerator_1_t1029143704;
// Newtonsoft.Json.Linq.IJEnumerable`1<Newtonsoft.Json.Linq.JToken>
struct IJEnumerable_1_t1747579902;
// System.Object
struct Il2CppObject;
// Newtonsoft.Json.Linq.JTokenEqualityComparer
struct JTokenEqualityComparer_t73863462;
// Newtonsoft.Json.Linq.JContainer
struct JContainer_t3364442311;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerable_1_t2418191612;
// System.String
struct String_t;
// Newtonsoft.Json.JsonConverter[]
struct JsonConverterU5BU5D_t638349667;
// Newtonsoft.Json.Linq.JValue
struct JValue_t3413677367;
// Newtonsoft.Json.JsonReader
struct JsonReader_t816925123;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t251850770;
// Newtonsoft.Json.IJsonLineInfo
struct IJsonLineInfo_t1008519681;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// System.IFormatProvider
struct IFormatProvider_t192740775;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t972330355;
// Newtonsoft.Json.Linq.JProperty
struct JProperty_t2616415645;
// Newtonsoft.Json.Linq.JArray
struct JArray_t3394795039;
// Newtonsoft.Json.Linq.JConstructor
struct JConstructor_t3493545088;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JContainer3364442311.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JEnumerable_213262205.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Formatting732683613.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonSerializer251850770.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonReader816925123.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_DateTimeOffset3884714306.h"
#include "mscorlib_System_Nullable_1_gen560925241.h"
#include "mscorlib_System_Nullable_1_gen72820554.h"
#include "mscorlib_System_Nullable_1_gen3968840829.h"
#include "mscorlib_System_Nullable_1_gen2038477154.h"
#include "mscorlib_System_Nullable_1_gen3952353088.h"
#include "mscorlib_System_Nullable_1_gen1237965023.h"
#include "mscorlib_System_Nullable_1_gen1237964965.h"
#include "mscorlib_System_Nullable_1_gen108794446.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "mscorlib_System_Nullable_1_gen1237965118.h"
#include "mscorlib_System_Nullable_1_gen81078199.h"
#include "mscorlib_System_Decimal1954350631.h"
#include "mscorlib_System_Nullable_1_gen108794504.h"
#include "mscorlib_System_Nullable_1_gen108794599.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonWriter972330355.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JProperty2616415645.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JTokenType3916897561.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_JsonToken4173078175.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JValue3413677367.h"

// System.Void Newtonsoft.Json.Linq.JToken::.ctor()
extern "C"  void JToken__ctor_m1047797667 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Newtonsoft.Json.Linq.JToken::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * JToken_System_Collections_IEnumerable_GetEnumerator_m3416458846 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken::System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>.GetEnumerator()
extern "C"  Il2CppObject* JToken_System_Collections_Generic_IEnumerableU3CNewtonsoft_Json_Linq_JTokenU3E_GetEnumerator_m2770609517 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.IJEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken::Newtonsoft.Json.Linq.IJEnumerable<Newtonsoft.Json.Linq.JToken>.get_Item(System.Object)
extern "C"  Il2CppObject* JToken_Newtonsoft_Json_Linq_IJEnumerableU3CNewtonsoft_Json_Linq_JTokenU3E_get_Item_m3017039931 (JToken_t3412245951 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken::Newtonsoft.Json.IJsonLineInfo.HasLineInfo()
extern "C"  bool JToken_Newtonsoft_Json_IJsonLineInfo_HasLineInfo_m2234057057 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JToken::Newtonsoft.Json.IJsonLineInfo.get_LineNumber()
extern "C"  int32_t JToken_Newtonsoft_Json_IJsonLineInfo_get_LineNumber_m1367047785 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JToken::Newtonsoft.Json.IJsonLineInfo.get_LinePosition()
extern "C"  int32_t JToken_Newtonsoft_Json_IJsonLineInfo_get_LinePosition_m1247520905 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JToken::System.ICloneable.Clone()
extern "C"  Il2CppObject * JToken_System_ICloneable_Clone_m1624712146 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenEqualityComparer Newtonsoft.Json.Linq.JToken::get_EqualityComparer()
extern "C"  JTokenEqualityComparer_t73863462 * JToken_get_EqualityComparer_m1143877261 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JToken::get_Parent()
extern "C"  JContainer_t3364442311 * JToken_get_Parent_m3292816209 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::set_Parent(Newtonsoft.Json.Linq.JContainer)
extern "C"  void JToken_set_Parent_m1659024378 (JToken_t3412245951 * __this, JContainer_t3364442311 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Root()
extern "C"  JToken_t3412245951 * JToken_get_Root_m747224705 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken::DeepEquals(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern "C"  bool JToken_DeepEquals_m250220534 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___t10, JToken_t3412245951 * ___t21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Next()
extern "C"  JToken_t3412245951 * JToken_get_Next_m623741010 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::set_Next(Newtonsoft.Json.Linq.JToken)
extern "C"  void JToken_set_Next_m3234908569 (JToken_t3412245951 * __this, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Previous()
extern "C"  JToken_t3412245951 * JToken_get_Previous_m3737913430 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::set_Previous(Newtonsoft.Json.Linq.JToken)
extern "C"  void JToken_set_Previous_m2275475093 (JToken_t3412245951 * __this, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::AddAfterSelf(System.Object)
extern "C"  void JToken_AddAfterSelf_m2356140908 (JToken_t3412245951 * __this, Il2CppObject * ___content0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::AddBeforeSelf(System.Object)
extern "C"  void JToken_AddBeforeSelf_m3674496519 (JToken_t3412245951 * __this, Il2CppObject * ___content0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken::Ancestors()
extern "C"  Il2CppObject* JToken_Ancestors_m3299695317 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken::AfterSelf()
extern "C"  Il2CppObject* JToken_AfterSelf_m2368156445 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken::BeforeSelf()
extern "C"  Il2CppObject* JToken_BeforeSelf_m2454599512 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Item(System.Object)
extern "C"  JToken_t3412245951 * JToken_get_Item_m3562576482 (JToken_t3412245951 * __this, Il2CppObject * ___key0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::set_Item(System.Object,Newtonsoft.Json.Linq.JToken)
extern "C"  void JToken_set_Item_m2997554823 (JToken_t3412245951 * __this, Il2CppObject * ___key0, JToken_t3412245951 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_First()
extern "C"  JToken_t3412245951 * JToken_get_First_m3755026003 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::get_Last()
extern "C"  JToken_t3412245951 * JToken_get_Last_m562639669 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken::Children()
extern "C"  JEnumerable_1_t213262205  JToken_Children_m1101021973 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::Remove()
extern "C"  void JToken_Remove_m2308400325 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::Replace(Newtonsoft.Json.Linq.JToken)
extern "C"  void JToken_Replace_m589935253 (JToken_t3412245951 * __this, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JToken::ToString()
extern "C"  String_t* JToken_ToString_m1817954602 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JToken::ToString(Newtonsoft.Json.Formatting,Newtonsoft.Json.JsonConverter[])
extern "C"  String_t* JToken_ToString_m4073222775 (JToken_t3412245951 * __this, int32_t ___formatting0, JsonConverterU5BU5D_t638349667* ___converters1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JValue Newtonsoft.Json.Linq.JToken::EnsureValue(Newtonsoft.Json.Linq.JToken)
extern "C"  JValue_t3413677367 * JToken_EnsureValue_m277118051 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JToken::GetType(Newtonsoft.Json.Linq.JToken)
extern "C"  String_t* JToken_GetType_m1598095702 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken::IsNullable(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JToken_IsNullable_m4221743666 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken::ValidateFloat(Newtonsoft.Json.Linq.JToken,System.Boolean)
extern "C"  bool JToken_ValidateFloat_m1161356590 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___o0, bool ___nullable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken::ValidateInteger(Newtonsoft.Json.Linq.JToken,System.Boolean)
extern "C"  bool JToken_ValidateInteger_m1803399568 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___o0, bool ___nullable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken::ValidateDate(Newtonsoft.Json.Linq.JToken,System.Boolean)
extern "C"  bool JToken_ValidateDate_m3643051460 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___o0, bool ___nullable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken::ValidateBoolean(Newtonsoft.Json.Linq.JToken,System.Boolean)
extern "C"  bool JToken_ValidateBoolean_m3755894138 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___o0, bool ___nullable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken::ValidateString(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JToken_ValidateString_m3822913430 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken::ValidateBytes(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JToken_ValidateBytes_m466850496 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonReader Newtonsoft.Json.Linq.JToken::CreateReader()
extern "C"  JsonReader_t816925123 * JToken_CreateReader_m3702421475 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::FromObjectInternal(System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  JToken_t3412245951 * JToken_FromObjectInternal_m772727007 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, JsonSerializer_t251850770 * ___jsonSerializer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::FromObject(System.Object)
extern "C"  JToken_t3412245951 * JToken_FromObject_m4246847189 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::FromObject(System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  JToken_t3412245951 * JToken_FromObject_m1626943170 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, JsonSerializer_t251850770 * ___jsonSerializer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::ReadFrom(Newtonsoft.Json.JsonReader)
extern "C"  JToken_t3412245951 * JToken_ReadFrom_m1036205590 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::Parse(System.String)
extern "C"  JToken_t3412245951 * JToken_Parse_m487861859 (Il2CppObject * __this /* static, unused */, String_t* ___json0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::Load(Newtonsoft.Json.JsonReader)
extern "C"  JToken_t3412245951 * JToken_Load_m1690281500 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::SetLineInfo(Newtonsoft.Json.IJsonLineInfo)
extern "C"  void JToken_SetLineInfo_m2829068591 (JToken_t3412245951 * __this, Il2CppObject * ___lineInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::SetLineInfo(System.Int32,System.Int32)
extern "C"  void JToken_SetLineInfo_m641765825 (JToken_t3412245951 * __this, int32_t ___lineNumber0, int32_t ___linePosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::SelectToken(System.String)
extern "C"  JToken_t3412245951 * JToken_SelectToken_m2052913593 (JToken_t3412245951 * __this, String_t* ___path0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::SelectToken(System.String,System.Boolean)
extern "C"  JToken_t3412245951 * JToken_SelectToken_m4036726948 (JToken_t3412245951 * __this, String_t* ___path0, bool ___errorWhenNoMatch1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::DeepClone()
extern "C"  JToken_t3412245951 * JToken_DeepClone_m549274397 (JToken_t3412245951 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JToken_op_Explicit_m2876029123 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTimeOffset Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  DateTimeOffset_t3884714306  JToken_op_Explicit_m3533993201 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Boolean> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t560925241  JToken_op_Explicit_m3355482702 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int64 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  int64_t JToken_op_Explicit_m3910460104 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTime> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t72820554  JToken_op_Explicit_m3358098601 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.DateTimeOffset> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t3968840829  JToken_op_Explicit_m310991766 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Decimal> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t2038477154  JToken_op_Explicit_m214174469 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Double> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t3952353088  JToken_op_Explicit_m652623219 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  int32_t JToken_op_Explicit_m2614975913 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  int16_t JToken_op_Explicit_m603373807 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt16 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  uint16_t JToken_op_Explicit_m59997954 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int32> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t1237965023  JToken_op_Explicit_m98067304 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int16> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t1237964965  JToken_op_Explicit_m2162911458 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.UInt16> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t108794446  JToken_op_Explicit_m199544485 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.DateTime Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  DateTime_t4283661327  JToken_op_Explicit_m2364721598 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Int64> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t1237965118  JToken_op_Explicit_m1603371561 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.Single> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t81078199  JToken_op_Explicit_m3236317468 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Decimal Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Decimal_t1954350631  JToken_op_Explicit_m3467433260 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.UInt32> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t108794504  JToken_op_Explicit_m2429667627 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Nullable`1<System.UInt64> Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  Nullable_1_t108794599  JToken_op_Explicit_m3934971884 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  double JToken_op_Explicit_m3261202036 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  float JToken_op_Explicit_m2513263019 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  String_t* JToken_op_Explicit_m2433856180 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  uint32_t JToken_op_Explicit_m2071600060 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt64 Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  uint64_t JToken_op_Explicit_m3367084251 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] Newtonsoft.Json.Linq.JToken::op_Explicit(Newtonsoft.Json.Linq.JToken)
extern "C"  ByteU5BU5D_t4260760469* JToken_op_Explicit_m3177213517 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.Boolean)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m96488966 (Il2CppObject * __this /* static, unused */, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.DateTimeOffset)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m1678241078 (Il2CppObject * __this /* static, unused */, DateTimeOffset_t3884714306  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.Nullable`1<System.Boolean>)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m781775673 (Il2CppObject * __this /* static, unused */, Nullable_1_t560925241  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.Int64)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m4114755873 (Il2CppObject * __this /* static, unused */, int64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.Nullable`1<System.DateTime>)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m3923967968 (Il2CppObject * __this /* static, unused */, Nullable_1_t72820554  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.Nullable`1<System.DateTimeOffset>)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m1279144979 (Il2CppObject * __this /* static, unused */, Nullable_1_t3968840829  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.Nullable`1<System.Decimal>)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m3369956386 (Il2CppObject * __this /* static, unused */, Nullable_1_t2038477154  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.Nullable`1<System.Double>)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m2842305814 (Il2CppObject * __this /* static, unused */, Nullable_1_t3952353088  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.Int16)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m4114751130 (Il2CppObject * __this /* static, unused */, int16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.UInt16)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m4000514949 (Il2CppObject * __this /* static, unused */, uint16_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.Int32)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m4114752928 (Il2CppObject * __this /* static, unused */, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.Nullable`1<System.Int32>)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m203228575 (Il2CppObject * __this /* static, unused */, Nullable_1_t1237965023  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.DateTime)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m673394569 (Il2CppObject * __this /* static, unused */, DateTime_t4283661327  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.Nullable`1<System.Int64>)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m203319870 (Il2CppObject * __this /* static, unused */, Nullable_1_t1237965118  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.Nullable`1<System.Single>)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m1988815821 (Il2CppObject * __this /* static, unused */, Nullable_1_t81078199  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.Decimal)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m2950925309 (Il2CppObject * __this /* static, unused */, Decimal_t1954350631  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.Nullable`1<System.Int16>)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m203172837 (Il2CppObject * __this /* static, unused */, Nullable_1_t1237964965  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.Nullable`1<System.UInt16>)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m2854325412 (Il2CppObject * __this /* static, unused */, Nullable_1_t108794446  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.Nullable`1<System.UInt32>)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m2854381150 (Il2CppObject * __this /* static, unused */, Nullable_1_t108794504  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.Nullable`1<System.UInt64>)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m2854472445 (Il2CppObject * __this /* static, unused */, Nullable_1_t108794599  ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.Double)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m2891748563 (Il2CppObject * __this /* static, unused */, double ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.Single)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m3141311292 (Il2CppObject * __this /* static, unused */, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.String)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m3459987603 (Il2CppObject * __this /* static, unused */, String_t* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.UInt32)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m4000516747 (Il2CppObject * __this /* static, unused */, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.UInt64)
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m4000519692 (Il2CppObject * __this /* static, unused */, uint64_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::op_Implicit(System.Byte[])
extern "C"  JToken_t3412245951 * JToken_op_Implicit_m1402181978 (Il2CppObject * __this /* static, unused */, ByteU5BU5D_t4260760469* ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JEnumerable`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken::ilo_Children1(Newtonsoft.Json.Linq.JToken)
extern "C"  JEnumerable_1_t213262205  JToken_ilo_Children1_m1271451519 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::ilo_get_Item2(Newtonsoft.Json.Linq.JToken,System.Object)
extern "C"  JToken_t3412245951 * JToken_ilo_get_Item2_m2743042697 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, Il2CppObject * ___key1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JToken::ilo_get_Parent3(Newtonsoft.Json.Linq.JToken)
extern "C"  JContainer_t3364442311 * JToken_ilo_get_Parent3_m3306528825 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken::ilo_DeepEquals4(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Linq.JToken)
extern "C"  bool JToken_ilo_DeepEquals4_m3800066051 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, JToken_t3412245951 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Newtonsoft.Json.Linq.JToken::ilo_IndexOfItem5(Newtonsoft.Json.Linq.JContainer,Newtonsoft.Json.Linq.JToken)
extern "C"  int32_t JToken_ilo_IndexOfItem5_m4243943495 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, JToken_t3412245951 * ___item1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::ilo_AddInternal6(Newtonsoft.Json.Linq.JContainer,System.Int32,System.Object)
extern "C"  void JToken_ilo_AddInternal6_m3318687023 (Il2CppObject * __this /* static, unused */, JContainer_t3364442311 * ____this0, int32_t ___index1, Il2CppObject * ___content2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JToken::ilo_FormatWith7(System.String,System.IFormatProvider,System.Object[])
extern "C"  String_t* JToken_ilo_FormatWith7_m194978025 (Il2CppObject * __this /* static, unused */, String_t* ___format0, Il2CppObject * ___provider1, ObjectU5BU5D_t1108656482* ___args2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::ilo_set_Formatting8(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.Formatting)
extern "C"  void JToken_ilo_set_Formatting8_m2335431768 (Il2CppObject * __this /* static, unused */, JsonWriter_t972330355 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::ilo_get_Value9(Newtonsoft.Json.Linq.JProperty)
extern "C"  JToken_t3412245951 * JToken_ilo_get_Value9_m4029572372 (Il2CppObject * __this /* static, unused */, JProperty_t2616415645 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::ilo_ArgumentNotNull10(System.Object,System.String)
extern "C"  void JToken_ilo_ArgumentNotNull10_m2113299334 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___value0, String_t* ___parameterName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JToken::ilo_get_Type11(Newtonsoft.Json.Linq.JToken)
extern "C"  int32_t JToken_ilo_get_Type11_m3306400996 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken::ilo_IsNullable12(Newtonsoft.Json.Linq.JToken)
extern "C"  bool JToken_ilo_IsNullable12_m426824350 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___o0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::ilo_FromObjectInternal13(System.Object,Newtonsoft.Json.JsonSerializer)
extern "C"  JToken_t3412245951 * JToken_ilo_FromObjectInternal13_m3756989364 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___o0, JsonSerializer_t251850770 * ___jsonSerializer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.JsonToken Newtonsoft.Json.Linq.JToken::ilo_get_TokenType14(Newtonsoft.Json.JsonReader)
extern "C"  int32_t JToken_ilo_get_TokenType14_m2899280434 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JArray Newtonsoft.Json.Linq.JToken::ilo_Load15(Newtonsoft.Json.JsonReader)
extern "C"  JArray_t3394795039 * JToken_ilo_Load15_m2413249235 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JConstructor Newtonsoft.Json.Linq.JToken::ilo_Load16(Newtonsoft.Json.JsonReader)
extern "C"  JConstructor_t3493545088 * JToken_ilo_Load16_m3289329013 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JToken::ilo_get_Value17(Newtonsoft.Json.JsonReader)
extern "C"  Il2CppObject * JToken_ilo_get_Value17_m3283191857 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::ilo_ReadFrom18(Newtonsoft.Json.JsonReader)
extern "C"  JToken_t3412245951 * JToken_ilo_ReadFrom18_m3749591024 (Il2CppObject * __this /* static, unused */, JsonReader_t816925123 * ___reader0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken::ilo_HasLineInfo19(Newtonsoft.Json.IJsonLineInfo)
extern "C"  bool JToken_ilo_HasLineInfo19_m4042619726 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken::ilo_SetLineInfo20(Newtonsoft.Json.Linq.JToken,System.Int32,System.Int32)
extern "C"  void JToken_ilo_SetLineInfo20_m970927322 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, int32_t ___lineNumber1, int32_t ___linePosition2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::ilo_SelectToken21(Newtonsoft.Json.Linq.JToken,System.String,System.Boolean)
extern "C"  JToken_t3412245951 * JToken_ilo_SelectToken21_m1834056492 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, String_t* ___path1, bool ___errorWhenNoMatch2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::ilo_CloneToken22(Newtonsoft.Json.Linq.JToken)
extern "C"  JToken_t3412245951 * JToken_ilo_CloneToken22_m505429797 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken::ilo_ValidateDate23(Newtonsoft.Json.Linq.JToken,System.Boolean)
extern "C"  bool JToken_ilo_ValidateDate23_m4052847864 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___o0, bool ___nullable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Newtonsoft.Json.Linq.JToken::ilo_GetType24(Newtonsoft.Json.Linq.JToken)
extern "C"  String_t* JToken_ilo_GetType24_m3359720583 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___token0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JToken::ilo_get_Value25(Newtonsoft.Json.Linq.JValue)
extern "C"  Il2CppObject * JToken_ilo_get_Value25_m2317891268 (Il2CppObject * __this /* static, unused */, JValue_t3413677367 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken::ilo_ValidateBoolean26(Newtonsoft.Json.Linq.JToken,System.Boolean)
extern "C"  bool JToken_ilo_ValidateBoolean26_m1245247723 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___o0, bool ___nullable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken::ilo_ValidateInteger27(Newtonsoft.Json.Linq.JToken,System.Boolean)
extern "C"  bool JToken_ilo_ValidateInteger27_m97933826 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___o0, bool ___nullable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JValue Newtonsoft.Json.Linq.JToken::ilo_EnsureValue28(Newtonsoft.Json.Linq.JToken)
extern "C"  JValue_t3413677367 * JToken_ilo_EnsureValue28_m4234347856 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken::ilo_ValidateFloat29(Newtonsoft.Json.Linq.JToken,System.Boolean)
extern "C"  bool JToken_ilo_ValidateFloat29_m3798198882 (Il2CppObject * __this /* static, unused */, JToken_t3412245951 * ___o0, bool ___nullable1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
