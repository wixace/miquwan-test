﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Monster
struct Monster_t2901270458;
// AICtrl
struct AICtrl_t1930422963;
// CombatEntity
struct CombatEntity_t684137495;
// monstersCfg
struct monstersCfg_t1542396363;
// JSCLevelMonsterConfig
struct JSCLevelMonsterConfig_t1924079698;
// Portal
struct Portal_t2396353676;
// CSHeroUnit
struct CSHeroUnit_t3764358446;
// Pathfinding.Path
struct Path_t1974241691;
// System.String
struct String_t;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.Action
struct Action_t3771233898;
// HatredCtrl
struct HatredCtrl_t891253697;
// FightCtrl
struct FightCtrl_t648967803;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// CSCheckPointUnit
struct CSCheckPointUnit_t3129216924;
// GameMgr
struct GameMgr_t1469029542;
// EffectMgr
struct EffectMgr_t535289511;
// OnPathDelegate
struct OnPathDelegate_t598607977;
// System.String[]
struct StringU5BU5D_t4054002952;
// CSGameDataMgr
struct CSGameDataMgr_t2623305516;
// CSCampfightData
struct CSCampfightData_t179152873;
// System.Collections.Generic.List`1<EnemyDropData>
struct List_1_t2836773265;
// UnityEngine.SkinnedMeshRenderer
struct SkinnedMeshRenderer_t3986041494;
// Skin
struct Skin_t2578845;
// NpcMgr
struct NpcMgr_t2339534743;
// System.Collections.Generic.List`1<AIObject>
struct List_1_t1405465687;
// AIPath
struct AIPath_t1930792045;
// AnimationRunner
struct AnimationRunner_t1015409588;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// BuffCtrl
struct BuffCtrl_t2836564350;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// EnemyDropData
struct EnemyDropData_t1468587713;
// enemy_dropCfg
struct enemy_dropCfg_t1026960702;
// EnemyDropMgr
struct EnemyDropMgr_t2125592609;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_AICtrl1930422963.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_monstersCfg1542396363.h"
#include "AssemblyU2DCSharp_JSCLevelMonsterConfig1924079698.h"
#include "AssemblyU2DCSharp_Portal2396353676.h"
#include "AssemblyU2DCSharp_CSHeroUnit3764358446.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_HatredCtrl891253697.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"
#include "AssemblyU2DCSharp_Monster2901270458.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "AssemblyU2DCSharp_EffectMgr535289511.h"
#include "AssemblyU2DCSharp_OnPathDelegate598607977.h"
#include "AssemblyU2DCSharp_SEX_TYPE2342271891.h"
#include "AssemblyU2DCSharp_CSCampfightData179152873.h"
#include "AssemblyU2DCSharp_HERO_ELEMENT2692579223.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitTag299076696.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitCamp642829979.h"
#include "AssemblyU2DCSharp_Skin2578845.h"
#include "UnityEngine_UnityEngine_SkinnedMeshRenderer3986041494.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitType643359572.h"
#include "AssemblyU2DCSharp_FightCtrl648967803.h"
#include "AssemblyU2DCSharp_NpcMgr2339534743.h"
#include "AssemblyU2DCSharp_AIEnum_EAIEventtype3896289311.h"
#include "AssemblyU2DCSharp_AIPath1930792045.h"
#include "AssemblyU2DCSharp_EntityEnum_DeadType1438946868.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"
#include "AssemblyU2DCSharp_BuffCtrl2836564350.h"
#include "AssemblyU2DCSharp_EnemyDropData1468587713.h"
#include "AssemblyU2DCSharp_EnemyDropMgr2125592609.h"

// System.Void Monster::.ctor()
extern "C"  void Monster__ctor_m3066192113 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::set_birth_time(System.Single)
extern "C"  void Monster_set_birth_time_m3177979246 (Monster_t2901270458 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::get_birth_time()
extern "C"  float Monster_get_birth_time_m3340647837 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::set_cur_time(System.Single)
extern "C"  void Monster_set_cur_time_m2086498479 (Monster_t2901270458 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::get_cur_time()
extern "C"  float Monster_get_cur_time_m283854012 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::set_mapEditorType(System.Int32)
extern "C"  void Monster_set_mapEditorType_m1360020774 (Monster_t2901270458 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::get_mapEditorType()
extern "C"  int32_t Monster_get_mapEditorType_m4074894167 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::set_aiCtrl(AICtrl)
extern "C"  void Monster_set_aiCtrl_m101911024 (Monster_t2901270458 * __this, AICtrl_t1930422963 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AICtrl Monster::get_aiCtrl()
extern "C"  AICtrl_t1930422963 * Monster_get_aiCtrl_m1562825189 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::set_isInFinish(System.Boolean)
extern "C"  void Monster_set_isInFinish_m650153769 (Monster_t2901270458 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Monster::get_isInFinish()
extern "C"  bool Monster_get_isInFinish_m1704769162 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::set_isInNum(System.Boolean)
extern "C"  void Monster_set_isInNum_m379313440 (Monster_t2901270458 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Monster::get_isInNum()
extern "C"  bool Monster_get_isInNum_m1141552401 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::OnTriggerFun(System.Int32)
extern "C"  void Monster_OnTriggerFun_m2199810666 (Monster_t2901270458 * __this, int32_t ___effectId0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::OnTriggerFun(CombatEntity,System.Int32)
extern "C"  void Monster_OnTriggerFun_m3970552501 (Monster_t2901270458 * __this, CombatEntity_t684137495 * ___main0, int32_t ___effectId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::OnCheckTarget()
extern "C"  void Monster_OnCheckTarget_m1613156073 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::OnTriggerBehaivir(CombatEntity)
extern "C"  void Monster_OnTriggerBehaivir_m731323171 (Monster_t2901270458 * __this, CombatEntity_t684137495 * ___main0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::AddEventListenerOnTB()
extern "C"  void Monster_AddEventListenerOnTB_m1430034477 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::Create(monstersCfg,JSCLevelMonsterConfig,CombatEntity,System.Boolean,System.Boolean)
extern "C"  void Monster_Create_m4039148125 (Monster_t2901270458 * __this, monstersCfg_t1542396363 * ___mcfg0, JSCLevelMonsterConfig_t1924079698 * ___lmcfg1, CombatEntity_t684137495 * ___SummonTarget2, bool ____isSummon3, bool ___isFindPos4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ResidualHp(System.Single)
extern "C"  void Monster_ResidualHp_m699636875 (Monster_t2901270458 * __this, float ___r_hp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ResidualMp(System.Single)
extern "C"  void Monster_ResidualMp_m3171218064 (Monster_t2901270458 * __this, float ___r_mp0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::Create(monstersCfg,JSCLevelMonsterConfig)
extern "C"  void Monster_Create_m3844875346 (Monster_t2901270458 * __this, monstersCfg_t1542396363 * ___mcfg0, JSCLevelMonsterConfig_t1924079698 * ___lmcfg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::Create(Portal,monstersCfg,JSCLevelMonsterConfig)
extern "C"  void Monster_Create_m1732075966 (Monster_t2901270458 * __this, Portal_t2396353676 * ___por0, monstersCfg_t1542396363 * ___mcfg1, JSCLevelMonsterConfig_t1924079698 * ___lmcfg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::Create(CSHeroUnit,JSCLevelMonsterConfig)
extern "C"  void Monster_Create_m1558765531 (Monster_t2901270458 * __this, CSHeroUnit_t3764358446 * ___heroUnit0, JSCLevelMonsterConfig_t1924079698 * ___lmcfg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::OnPathComplete(Pathfinding.Path)
extern "C"  void Monster_OnPathComplete_m749099009 (Monster_t2901270458 * __this, Path_t1974241691 * ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::SetMosterAttribute(System.String,System.Int32)
extern "C"  void Monster_SetMosterAttribute_m515718994 (Monster_t2901270458 * __this, String_t* ___key0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::Update()
extern "C"  void Monster_Update_m2289537084 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::SendPlotNpcLifeTime()
extern "C"  void Monster_SendPlotNpcLifeTime_m3796304624 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::set_hp(System.Single)
extern "C"  void Monster_set_hp_m1719253587 (Monster_t2901270458 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::get_hp()
extern "C"  float Monster_get_hp_m3466121368 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::OnUseingSkill(System.Int32)
extern "C"  void Monster_OnUseingSkill_m3988658519 (Monster_t2901270458 * __this, int32_t ___skillID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::OnLevelTime()
extern "C"  void Monster_OnLevelTime_m74478689 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::OnAttckTime()
extern "C"  void Monster_OnAttckTime_m3250123750 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ATKDEBUFF()
extern "C"  void Monster_ATKDEBUFF_m1227641083 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::AIMonsterDead(System.Int32)
extern "C"  void Monster_AIMonsterDead_m21518998 (Monster_t2901270458 * __this, int32_t ___murderID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::AIBeAttacked()
extern "C"  void Monster_AIBeAttacked_m1791805477 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::AGAINATTACK()
extern "C"  void Monster_AGAINATTACK_m4045504311 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::AIOtherDead(CEvent.ZEvent)
extern "C"  void Monster_AIOtherDead_m3294750560 (Monster_t2901270458 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::Answer(CEvent.ZEvent)
extern "C"  void Monster_Answer_m761845290 (Monster_t2901270458 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ClosePlot(CEvent.ZEvent)
extern "C"  void Monster_ClosePlot_m2747376051 (Monster_t2901270458 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::MoceCameraEnd(CEvent.ZEvent)
extern "C"  void Monster_MoceCameraEnd_m987415610 (Monster_t2901270458 * __this, ZEvent_t3638018500 * ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::OnDelayTime()
extern "C"  void Monster_OnDelayTime_m2731202336 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::OnTargetReached()
extern "C"  void Monster_OnTargetReached_m3416442609 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::UpdateTargetPos(UnityEngine.Vector3,System.Action)
extern "C"  void Monster_UpdateTargetPos_m1727531431 (Monster_t2901270458 * __this, Vector3_t4282066566  ___targetPos0, Action_t3771233898 * ___OnTargetCall1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::UpdateMove()
extern "C"  void Monster_UpdateMove_m2830234221 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::Dead()
extern "C"  void Monster_Dead_m3223870871 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::BuffInHp()
extern "C"  float Monster_BuffInHp_m274267367 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::CalcuFinalProp()
extern "C"  void Monster_CalcuFinalProp_m1699874444 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::DropClear()
extern "C"  void Monster_DropClear_m999715885 (Monster_t2901270458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_OnTriggerFun1(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_OnTriggerFun1_m1009775687 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___effectId1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_isAttack2(CombatEntity,System.Boolean)
extern "C"  void Monster_ilo_set_isAttack2_m4185374741 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_InitHatred3(HatredCtrl)
extern "C"  void Monster_ilo_InitHatred3_m357187080 (Il2CppObject * __this /* static, unused */, HatredCtrl_t891253697 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightCtrl Monster::ilo_get_fightCtrl4(CombatEntity)
extern "C"  FightCtrl_t648967803 * Monster_ilo_get_fightCtrl4_m2400561113 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl Monster::ilo_get_bvrCtrl5(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * Monster_ilo_get_bvrCtrl5_m3093937126 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_TranBehavior6(Entity.Behavior.IBehaviorCtrl,Entity.Behavior.EBehaviorID,System.Object[])
extern "C"  void Monster_ilo_TranBehavior6_m1757043711 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, uint8_t ___id1, ObjectU5BU5D_t1108656482* ___arg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_IsCheckTarget7(CombatEntity,System.Boolean)
extern "C"  void Monster_ilo_set_IsCheckTarget7_m1074244521 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Monster::ilo_get_canFight8(CombatEntity)
extern "C"  bool Monster_ilo_get_canFight8_m2630548936 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::ilo_get_atkRange9(CombatEntity)
extern "C"  float Monster_ilo_get_atkRange9_m1062384668 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_AddEventListener10(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void Monster_ilo_AddEventListener10_m1219834001 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_isSummonMonster11(CombatEntity,System.Boolean)
extern "C"  void Monster_ilo_set_isSummonMonster11_m3216692678 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_Create12(Monster,monstersCfg,JSCLevelMonsterConfig)
extern "C"  void Monster_ilo_Create12_m173319216 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, monstersCfg_t1542396363 * ___mcfg1, JSCLevelMonsterConfig_t1924079698 * ___lmcfg2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Monster::ilo_get_IsBirthEffect13(CombatEntity)
extern "C"  bool Monster_ilo_get_IsBirthEffect13_m1419856728 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSCheckPointUnit Monster::ilo_get_checkPoint14(GameMgr)
extern "C"  CSCheckPointUnit_t3129216924 * Monster_ilo_get_checkPoint14_m1354995155 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_PlayEffect15(EffectMgr,System.Int32,CombatEntity)
extern "C"  void Monster_ilo_PlayEffect15_m2163608052 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, int32_t ___id1, CombatEntity_t684137495 * ___srcTf2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_SetViewModel16(CombatEntity,System.Boolean)
extern "C"  void Monster_ilo_SetViewModel16_m1921877991 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___isShow1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_RequestAstarPath17(CombatEntity,UnityEngine.Vector3,OnPathDelegate)
extern "C"  void Monster_ilo_RequestAstarPath17_m2434385740 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, Vector3_t4282066566  ___pos1, OnPathDelegate_t598607977 * ___onPathComplete2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_rage18(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_rage18_m3118800485 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_aiCtrl19(Monster,AICtrl)
extern "C"  void Monster_ilo_set_aiCtrl19_m547210329 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, AICtrl_t1930422963 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AICtrl Monster::ilo_get_aiCtrl20(Monster)
extern "C"  AICtrl_t1930422963 * Monster_ilo_get_aiCtrl20_m1260152482 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_Init21(AICtrl,System.String[])
extern "C"  void Monster_ilo_Init21_m3818464892 (Il2CppObject * __this /* static, unused */, AICtrl_t1930422963 * ____this0, StringU5BU5D_t4054002952* ___aiIDs1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_birth_time22(Monster,System.Single)
extern "C"  void Monster_ilo_set_birth_time22_m4154071501 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_sexType23(CombatEntity,SEX_TYPE)
extern "C"  void Monster_ilo_set_sexType23_m2732479274 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_findEnemyRange24(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_findEnemyRange24_m2709454753 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_halfwidth25(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_halfwidth25_m4025679715 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_atkRange26(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_atkRange26_m4252327994 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_islucency27(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_islucency27_m3039444187 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_dutytype28(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_dutytype28_m3322996213 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_isAim29(CombatEntity,System.Boolean)
extern "C"  void Monster_ilo_set_isAim29_m3596095219 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_atkedEff30(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_atkedEff30_m4258524192 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_deathEffect31(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_deathEffect31_m7678124 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_taixudeatheffect32(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_taixudeatheffect32_m3116446976 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_reviveEffectId33(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_reviveEffectId33_m3085329610 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_headBloodPos34(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_headBloodPos34_m2637239692 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_initBehavior35(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_initBehavior35_m992843303 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_damageType36(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_damageType36_m2669226655 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_id37(CombatEntity)
extern "C"  int32_t Monster_ilo_get_id37_m1307678473 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSGameDataMgr Monster::ilo_get_CSGameDataMgr38()
extern "C"  CSGameDataMgr_t2623305516 * Monster_ilo_get_CSGameDataMgr38_m337335659 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_GetMonsterLevel39(CSCampfightData,System.Int32,System.Boolean)
extern "C"  int32_t Monster_ilo_GetMonsterLevel39_m3749783231 (Il2CppObject * __this /* static, unused */, CSCampfightData_t179152873 * ____this0, int32_t ___id1, bool ___isView2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_level40(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_level40_m1061565775 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_atkPower41(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_atkPower41_m642892475 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_level42(CombatEntity)
extern "C"  int32_t Monster_ilo_get_level42_m2776687756 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::ilo_get_MD43(CombatEntity)
extern "C"  float Monster_ilo_get_MD43_m791342306 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_hits44(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_hits44_m2975463409 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_withstand45(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_withstand45_m1331388222 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::ilo_get_maxHp46(CombatEntity)
extern "C"  float Monster_ilo_get_maxHp46_m1026933718 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_maxHp47(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_maxHp47_m656329980 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_hp48(Monster,System.Single)
extern "C"  void Monster_ilo_set_hp48_m2361609390 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::ilo_get_atkPower49(CombatEntity)
extern "C"  float Monster_ilo_get_atkPower49_m3190929566 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_hurtL50(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_hurtL50_m1331443173 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_critHurt51(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_critHurt51_m420412694 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_superArmor52(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_superArmor52_m3215297738 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_superArmor53(CombatEntity)
extern "C"  int32_t Monster_ilo_get_superArmor53_m3816055084 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_element54(CombatEntity,HERO_ELEMENT)
extern "C"  void Monster_ilo_set_element54_m3834282444 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_modelScale55(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_modelScale55_m3338858290 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_angerSecDecNum56(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_angerSecDecNum56_m652273276 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_maxRage57(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_maxRage57_m2238777248 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_atkEffDelayTime58(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_atkEffDelayTime58_m794739475 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Monster::ilo_get_startPos59(CombatEntity)
extern "C"  Vector3_t4282066566  Monster_ilo_get_startPos59_m4223022292 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::ilo_get_scale60(CombatEntity)
extern "C"  float Monster_ilo_get_scale60_m3002526156 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<EnemyDropData> Monster::ilo_get_dropRwards61(CombatEntity)
extern "C"  List_1_t2836773265 * Monster_ilo_get_dropRwards61_m4143439333 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_unitTag62(CombatEntity,EntityEnum.UnitTag)
extern "C"  void Monster_ilo_set_unitTag62_m3972494912 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_unitCamp63(CombatEntity,EntityEnum.UnitCamp)
extern "C"  void Monster_ilo_set_unitCamp63_m3614499243 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_bvrCtrl64(CombatEntity,Entity.Behavior.IBehaviorCtrl)
extern "C"  void Monster_ilo_set_bvrCtrl64_m3070819110 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, IBehaviorCtrl_t4225040900 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SkinnedMeshRenderer Monster::ilo_get_skinMeshRender65(CombatEntity)
extern "C"  SkinnedMeshRenderer_t3986041494 * Monster_ilo_get_skinMeshRender65_m3964233709 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_InitSkin66(Skin,System.Int32,UnityEngine.SkinnedMeshRenderer)
extern "C"  void Monster_ilo_InitSkin66_m2281142852 (Il2CppObject * __this /* static, unused */, Skin_t2578845 * ____this0, int32_t ___skinID1, SkinnedMeshRenderer_t3986041494 * ___mrenderer2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_CalcuFinalProp67(Monster)
extern "C"  void Monster_ilo_CalcuFinalProp67_m3912460088 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EffectMgr Monster::ilo_get_EffectMgr68()
extern "C"  EffectMgr_t535289511 * Monster_ilo_get_EffectMgr68_m1385998248 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_PD69(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_PD69_m2988938728 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Monster::ilo_get_isStart70(GameMgr)
extern "C"  bool Monster_ilo_get_isStart70_m1207969716 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Monster::ilo_get_isWin71()
extern "C"  bool Monster_ilo_get_isWin71_m1434198803 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitType Monster::ilo_get_unitType72(CombatEntity)
extern "C"  int32_t Monster_ilo_get_unitType72_m1264713254 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_Fight73(FightCtrl)
extern "C"  void Monster_ilo_Fight73_m2991733103 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HERO_ELEMENT Monster::ilo_get_element74(CombatEntity)
extern "C"  int32_t Monster_ilo_get_element74_m2857234843 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Monster::ilo_IsCurElementNpc75(NpcMgr,HERO_ELEMENT)
extern "C"  bool Monster_ilo_IsCurElementNpc75_m154699037 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, int32_t ___element1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<AIObject> Monster::ilo_get_AIDelayList76(CombatEntity)
extern "C"  List_1_t1405465687 * Monster_ilo_get_AIDelayList76_m873608878 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_Update77(CombatEntity)
extern "C"  void Monster_ilo_Update77_m3279666072 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_DispatchEvent78(CEvent.ZEvent)
extern "C"  void Monster_ilo_DispatchEvent78_m895048446 (Il2CppObject * __this /* static, unused */, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_OnObservableRequest79(AICtrl,AIEnum.EAIEventtype,System.Int32,System.Int32)
extern "C"  void Monster_ilo_OnObservableRequest79_m4034260504 (Il2CppObject * __this /* static, unused */, AICtrl_t1930422963 * ____this0, uint8_t ___eType1, int32_t ___tParam12, int32_t ___tParam23, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::ilo_get_starAttackTime80(CombatEntity)
extern "C"  float Monster_ilo_get_starAttackTime80_m130227755 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_OnTargetReached81(CombatEntity)
extern "C"  void Monster_ilo_OnTargetReached81_m1305262432 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::ilo_get_runSpeed82(CombatEntity)
extern "C"  float Monster_ilo_get_runSpeed82_m2541633922 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_Arrive83(CombatEntity,UnityEngine.Vector3)
extern "C"  void Monster_ilo_Arrive83_m2863918484 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, Vector3_t4282066566  ___pos1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Monster::ilo_GetFeetPosition84(AIPath)
extern "C"  Vector3_t4282066566  Monster_ilo_GetFeetPosition84_m3660570642 (Il2CppObject * __this /* static, unused */, AIPath_t1930792045 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Monster::ilo_get_canMove85(CombatEntity)
extern "C"  bool Monster_ilo_get_canMove85_m1795328942 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AnimationRunner Monster::ilo_get_characterAnim86(CombatEntity)
extern "C"  AnimationRunner_t1015409588 * Monster_ilo_get_characterAnim86_m2151377779 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NpcMgr Monster::ilo_get_instance87()
extern "C"  NpcMgr_t2339534743 * Monster_ilo_get_instance87_m3580728029 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_ChangeHeroToDead88(NpcMgr,CombatEntity)
extern "C"  void Monster_ilo_ChangeHeroToDead88_m2903472951 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, CombatEntity_t684137495 * ___entity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_RemoveEventListener89(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void Monster_ilo_RemoveEventListener89_m3726599676 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___func1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.DeadType Monster::ilo_get_deadType90(CombatEntity)
extern "C"  int32_t Monster_ilo_get_deadType90_m3609654050 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_Dead91(CombatEntity)
extern "C"  void Monster_ilo_Dead91_m4187253931 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::ilo_get_hp92(Monster)
extern "C"  float Monster_ilo_get_hp92_m3991389268 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// monstersCfg Monster::ilo_GetmonstersCfg93(CSDatacfgManager,System.Int32)
extern "C"  monstersCfg_t1542396363 * Monster_ilo_GetmonstersCfg93_m1588877005 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BuffCtrl Monster::ilo_get_buffCtrl94(CombatEntity)
extern "C"  BuffCtrl_t2836564350 * Monster_ilo_get_buffCtrl94_m4062248946 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::ilo_BuffInHp95(Monster)
extern "C"  float Monster_ilo_BuffInHp95_m836484354 (Il2CppObject * __this /* static, unused */, Monster_t2901270458 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mBuffMD96(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mBuffMD96_m2128101079 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_MD97(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_MD97_m3372027942 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mBuffMaxHPNum98(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mBuffMaxHPNum98_m1617371356 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mBuffHits99(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mBuffHits99_m3426243427 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_wreck100(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_wreck100_m1620214544 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mBuffTenacityNum101(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mBuffTenacityNum101_m3294903326 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_tenacity102(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_tenacity102_m1166970801 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::ilo_get_mHPRstNum103(BuffCtrl)
extern "C"  float Monster_ilo_get_mHPRstNum103_m695438001 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_HPRstNum104(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_HPRstNum104_m2482216743 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::ilo_get_mHPSecDecPer105(BuffCtrl)
extern "C"  float Monster_ilo_get_mHPSecDecPer105_m599256288 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_HPSecDecPer106(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_HPSecDecPer106_m1525589988 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::ilo_get_mRunspeed107(BuffCtrl)
extern "C"  float Monster_ilo_get_mRunspeed107_m1727485428 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::ilo_get_mModelScale108(BuffCtrl)
extern "C"  float Monster_ilo_get_mModelScale108_m2787060880 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_SetModelScale109(CombatEntity)
extern "C"  void Monster_ilo_SetModelScale109_m85354022 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::ilo_get_mVampire110(BuffCtrl)
extern "C"  float Monster_ilo_get_mVampire110_m2350099344 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::ilo_get_mTreatmentEffect111(BuffCtrl)
extern "C"  float Monster_ilo_get_mTreatmentEffect111_m1264741142 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_cureP112(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_cureP112_m2360246424 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::ilo_get_mHurtEffectPlus113(BuffCtrl)
extern "C"  float Monster_ilo_get_mHurtEffectPlus113_m1830143409 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::ilo_get_mHurtEffectReduction114(BuffCtrl)
extern "C"  float Monster_ilo_get_mHurtEffectReduction114_m952792687 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_tauntDistance115(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_tauntDistance115_m515935843 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mRestrintCountryType116(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mRestrintCountryType116_m3114137831 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::ilo_get_mRestrintPer117(BuffCtrl)
extern "C"  float Monster_ilo_get_mRestrintPer117_m1636185817 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mRestrintStateType118(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mRestrintStateType118_m3551219278 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mNDPAddBuff119(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mNDPAddBuff119_m3905337397 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_NDPAddBuff120(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_NDPAddBuff120_m601789850 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_NDPAddBuff02121(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_NDPAddBuff02121_m1440689275 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_BeAttackedLaterSkillID122(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_BeAttackedLaterSkillID122_m3938914824 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_BeAttackedLaterEffectID123(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_BeAttackedLaterEffectID123_m425516519 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mDeathLaterHeroID124(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mDeathLaterHeroID124_m2539468028 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_DeathLaterHeroID125(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_DeathLaterHeroID125_m905056552 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_DeathLaterSpeed126(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_DeathLaterSpeed126_m3578949125 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_curAntiNum127(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_curAntiNum127_m3999354941 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_AntiEndEffectID128(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_AntiEndEffectID128_m3882209081 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_AuraAddHPRadius129(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_AuraAddHPRadius129_m1685048747 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_AuraAddHPNum130(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_AuraAddHPNum130_m1387182401 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mAuraAddHPPer131(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mAuraAddHPPer131_m919315342 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_AuraReduceHPPer132(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_AuraReduceHPPer132_m600868231 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> Monster::ilo_get_mAddDamageSkillIDs133(BuffCtrl)
extern "C"  List_1_t2522024052 * Monster_ilo_get_mAddDamageSkillIDs133_m1520290302 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> Monster::ilo_get_mAddDamageValue134(BuffCtrl)
extern "C"  List_1_t2522024052 * Monster_ilo_get_mAddDamageValue134_m758338003 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mHpLineBuffID135(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mHpLineBuffID135_m2940920319 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_hpLineBuffID136(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_hpLineBuffID136_m3501727973 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_runeTakeEffectNum137(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_runeTakeEffectNum137_m3210514282 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_runeTakeEffectBuffID138(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_runeTakeEffectBuffID138_m2865383769 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mRuneTakeEffectBuffID02139(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mRuneTakeEffectBuffID02139_m1122590731 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_runeTakeEffectBuffID02140(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_runeTakeEffectBuffID02140_m405637348 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mAntiDebuffDiscreteAddBuffID141(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mAntiDebuffDiscreteAddBuffID141_m3823344894 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mUnyielding142(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mUnyielding142_m56297591 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Monster::ilo_get_mPetrifiedChangeTime143(BuffCtrl)
extern "C"  float Monster_ilo_get_mPetrifiedChangeTime143_m27151593 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_petrifiedChangeTime144(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_petrifiedChangeTime144_m2213246093 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mChangeNewSkillID145(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mChangeNewSkillID145_m204287692 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mWhenCritAddBuffID146(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mWhenCritAddBuffID146_m3583363592 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_whenCritAddBuffID147(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_whenCritAddBuffID147_m1964029490 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_tankBuffID148(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_tankBuffID148_m3124520306 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mCleaveSkillID149(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mCleaveSkillID149_m2045544046 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_cleaveSkillID150(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_cleaveSkillID150_m667597527 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mBuffCritHurt151(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mBuffCritHurt151_m3938530663 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mReduceRageHpNum152(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mReduceRageHpNum152_m2301332787 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mHTBuffID153(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mHTBuffID153_m3419952875 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mDeathHarvestPer154(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mDeathHarvestPer154_m2727869080 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mHpLineCleaveAddBuff155(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mHpLineCleaveAddBuff155_m75819229 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_hpLineCleaveAddBuff156(CombatEntity,System.Int32)
extern "C"  void Monster_ilo_set_hpLineCleaveAddBuff156_m2462825789 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_mResistanceCountryPer157(BuffCtrl)
extern "C"  int32_t Monster_ilo_get_mResistanceCountryPer157_m4183600425 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_set_resistanceCountryPer158(CombatEntity,System.Single)
extern "C"  void Monster_ilo_set_resistanceCountryPer158_m1163575873 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSDatacfgManager Monster::ilo_get_CfgDataMgr159()
extern "C"  CSDatacfgManager_t1565254243 * Monster_ilo_get_CfgDataMgr159_m2531291736 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Monster::ilo_get_dropId160(EnemyDropData)
extern "C"  int32_t Monster_ilo_get_dropId160_m324443215 (Il2CppObject * __this /* static, unused */, EnemyDropData_t1468587713 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// enemy_dropCfg Monster::ilo_Getenemy_dropCfg161(CSDatacfgManager,System.Int32)
extern "C"  enemy_dropCfg_t1026960702 * Monster_ilo_Getenemy_dropCfg161_m3072743621 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster::ilo_AddDrop162(EnemyDropMgr,EnemyDropData,CombatEntity,System.Single)
extern "C"  void Monster_ilo_AddDrop162_m1163178727 (Il2CppObject * __this /* static, unused */, EnemyDropMgr_t2125592609 * ____this0, EnemyDropData_t1468587713 * ___data1, CombatEntity_t684137495 * ___entity2, float ___hurt3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
