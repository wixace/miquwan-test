﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ClothGenerated
struct UnityEngine_ClothGenerated_t2528545789;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.ClothSkinningCoefficient[]
struct ClothSkinningCoefficientU5BU5D_t3357176891;
// UnityEngine.CapsuleCollider[]
struct CapsuleColliderU5BU5D_t4217061582;
// UnityEngine.ClothSphereColliderPair[]
struct ClothSphereColliderPairU5BU5D_t3700479018;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_ClothGenerated::.ctor()
extern "C"  void UnityEngine_ClothGenerated__ctor_m3399682366 (UnityEngine_ClothGenerated_t2528545789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ClothGenerated::Cloth_Cloth1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ClothGenerated_Cloth_Cloth1_m3140486342 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::Cloth_sleepThreshold(JSVCall)
extern "C"  void UnityEngine_ClothGenerated_Cloth_sleepThreshold_m3872091304 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::Cloth_bendingStiffness(JSVCall)
extern "C"  void UnityEngine_ClothGenerated_Cloth_bendingStiffness_m3138213766 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::Cloth_stretchingStiffness(JSVCall)
extern "C"  void UnityEngine_ClothGenerated_Cloth_stretchingStiffness_m2460671118 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::Cloth_damping(JSVCall)
extern "C"  void UnityEngine_ClothGenerated_Cloth_damping_m2225767310 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::Cloth_externalAcceleration(JSVCall)
extern "C"  void UnityEngine_ClothGenerated_Cloth_externalAcceleration_m2861250929 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::Cloth_randomAcceleration(JSVCall)
extern "C"  void UnityEngine_ClothGenerated_Cloth_randomAcceleration_m1865804025 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::Cloth_useGravity(JSVCall)
extern "C"  void UnityEngine_ClothGenerated_Cloth_useGravity_m3372601749 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::Cloth_enabled(JSVCall)
extern "C"  void UnityEngine_ClothGenerated_Cloth_enabled_m1392462895 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::Cloth_vertices(JSVCall)
extern "C"  void UnityEngine_ClothGenerated_Cloth_vertices_m3334747683 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::Cloth_normals(JSVCall)
extern "C"  void UnityEngine_ClothGenerated_Cloth_normals_m3896743780 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::Cloth_friction(JSVCall)
extern "C"  void UnityEngine_ClothGenerated_Cloth_friction_m2171103298 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::Cloth_collisionMassScale(JSVCall)
extern "C"  void UnityEngine_ClothGenerated_Cloth_collisionMassScale_m537057112 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::Cloth_useContinuousCollision(JSVCall)
extern "C"  void UnityEngine_ClothGenerated_Cloth_useContinuousCollision_m4206478112 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::Cloth_useVirtualParticles(JSVCall)
extern "C"  void UnityEngine_ClothGenerated_Cloth_useVirtualParticles_m346197479 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::Cloth_coefficients(JSVCall)
extern "C"  void UnityEngine_ClothGenerated_Cloth_coefficients_m4247635678 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::Cloth_worldVelocityScale(JSVCall)
extern "C"  void UnityEngine_ClothGenerated_Cloth_worldVelocityScale_m1313576161 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::Cloth_worldAccelerationScale(JSVCall)
extern "C"  void UnityEngine_ClothGenerated_Cloth_worldAccelerationScale_m2188721988 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::Cloth_solverFrequency(JSVCall)
extern "C"  void UnityEngine_ClothGenerated_Cloth_solverFrequency_m3235935207 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::Cloth_capsuleColliders(JSVCall)
extern "C"  void UnityEngine_ClothGenerated_Cloth_capsuleColliders_m2500109546 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::Cloth_sphereColliders(JSVCall)
extern "C"  void UnityEngine_ClothGenerated_Cloth_sphereColliders_m3421177566 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ClothGenerated::Cloth_ClearTransformMotion(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ClothGenerated_Cloth_ClearTransformMotion_m1155345572 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ClothGenerated::Cloth_SetEnabledFading__Boolean__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ClothGenerated_Cloth_SetEnabledFading__Boolean__Single_m2796938379 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ClothGenerated::Cloth_SetEnabledFading__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ClothGenerated_Cloth_SetEnabledFading__Boolean_m2003699779 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::__Register()
extern "C"  void UnityEngine_ClothGenerated___Register_m2387118665 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ClothSkinningCoefficient[] UnityEngine_ClothGenerated::<Cloth_coefficients>m__199()
extern "C"  ClothSkinningCoefficientU5BU5D_t3357176891* UnityEngine_ClothGenerated_U3CCloth_coefficientsU3Em__199_m1971214563 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.CapsuleCollider[] UnityEngine_ClothGenerated::<Cloth_capsuleColliders>m__19A()
extern "C"  CapsuleColliderU5BU5D_t4217061582* UnityEngine_ClothGenerated_U3CCloth_capsuleCollidersU3Em__19A_m2680135940 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.ClothSphereColliderPair[] UnityEngine_ClothGenerated::<Cloth_sphereColliders>m__19B()
extern "C"  ClothSphereColliderPairU5BU5D_t3700479018* UnityEngine_ClothGenerated_U3CCloth_sphereCollidersU3Em__19B_m1869693443 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ClothGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_ClothGenerated_ilo_getObject1_m2993344276 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_ClothGenerated_ilo_addJSCSRel2_m2501328251 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_ClothGenerated_ilo_setSingle3_m3457081208 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_ClothGenerated::ilo_getSingle4(System.Int32)
extern "C"  float UnityEngine_ClothGenerated_ilo_getSingle4_m1149534828 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::ilo_setVector3S5(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_ClothGenerated_ilo_setVector3S5_m3621376333 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::ilo_setBooleanS6(System.Int32,System.Boolean)
extern "C"  void UnityEngine_ClothGenerated_ilo_setBooleanS6_m3271485258 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::ilo_moveSaveID2Arr7(System.Int32)
extern "C"  void UnityEngine_ClothGenerated_ilo_moveSaveID2Arr7_m208913241 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ClothGenerated::ilo_setObject8(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_ClothGenerated_ilo_setObject8_m833496659 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ClothGenerated::ilo_getBooleanS9(System.Int32)
extern "C"  bool UnityEngine_ClothGenerated_ilo_getBooleanS9_m1531620054 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ClothGenerated::ilo_setArrayS10(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_ClothGenerated_ilo_setArrayS10_m2781126011 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ClothGenerated::ilo_getElement11(System.Int32,System.Int32)
extern "C"  int32_t UnityEngine_ClothGenerated_ilo_getElement11_m3183827641 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
