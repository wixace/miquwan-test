﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIAnchor
struct UIAnchor_t143817321;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIAnchor143817321.h"

// System.Void UIAnchor::.ctor()
extern "C"  void UIAnchor__ctor_m3057052242 (UIAnchor_t143817321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAnchor::.cctor()
extern "C"  void UIAnchor__cctor_m4092210075 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAnchor::GetSafeAreaImpl(System.Single&,System.Single&,System.Single&,System.Single&)
extern "C"  void UIAnchor_GetSafeAreaImpl_m4039973990 (Il2CppObject * __this /* static, unused */, float* ___x0, float* ___y1, float* ___w2, float* ___h3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAnchor::InitSafeArea()
extern "C"  void UIAnchor_InitSafeArea_m1128205340 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAnchor::Awake()
extern "C"  void UIAnchor_Awake_m3294657461 (UIAnchor_t143817321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAnchor::SetSafeArea(System.Int32)
extern "C"  void UIAnchor_SetSafeArea_m3112855741 (UIAnchor_t143817321 * __this, int32_t ___yType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAnchor::OnDestroy()
extern "C"  void UIAnchor_OnDestroy_m554124875 (UIAnchor_t143817321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAnchor::ScreenSizeChanged()
extern "C"  void UIAnchor_ScreenSizeChanged_m666687127 (UIAnchor_t143817321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAnchor::Start()
extern "C"  void UIAnchor_Start_m2004190034 (UIAnchor_t143817321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAnchor::Update()
extern "C"  void UIAnchor_Update_m2006201083 (UIAnchor_t143817321 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIAnchor::ilo_Update1(UIAnchor)
extern "C"  void UIAnchor_ilo_Update1_m527536284 (Il2CppObject * __this /* static, unused */, UIAnchor_t143817321 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UIAnchor::ilo_FindCameraForLayer2(System.Int32)
extern "C"  Camera_t2727095145 * UIAnchor_ilo_FindCameraForLayer2_m3262504889 (Il2CppObject * __this /* static, unused */, int32_t ___layer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
