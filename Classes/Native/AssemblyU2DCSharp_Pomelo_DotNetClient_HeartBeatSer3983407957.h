﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Timers.Timer
struct Timer_t3701448099;
// Pomelo.DotNetClient.Protocol
struct Protocol_t400511732;

#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_DateTime4283661327.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pomelo.DotNetClient.HeartBeatService
struct  HeartBeatService_t3983407957  : public Il2CppObject
{
public:
	// System.Int32 Pomelo.DotNetClient.HeartBeatService::interval
	int32_t ___interval_0;
	// System.Int32 Pomelo.DotNetClient.HeartBeatService::timeout
	int32_t ___timeout_1;
	// System.Timers.Timer Pomelo.DotNetClient.HeartBeatService::timer
	Timer_t3701448099 * ___timer_2;
	// System.DateTime Pomelo.DotNetClient.HeartBeatService::lastTime
	DateTime_t4283661327  ___lastTime_3;
	// Pomelo.DotNetClient.Protocol Pomelo.DotNetClient.HeartBeatService::protocol
	Protocol_t400511732 * ___protocol_4;
	// System.Int32 Pomelo.DotNetClient.HeartBeatService::id
	int32_t ___id_6;

public:
	inline static int32_t get_offset_of_interval_0() { return static_cast<int32_t>(offsetof(HeartBeatService_t3983407957, ___interval_0)); }
	inline int32_t get_interval_0() const { return ___interval_0; }
	inline int32_t* get_address_of_interval_0() { return &___interval_0; }
	inline void set_interval_0(int32_t value)
	{
		___interval_0 = value;
	}

	inline static int32_t get_offset_of_timeout_1() { return static_cast<int32_t>(offsetof(HeartBeatService_t3983407957, ___timeout_1)); }
	inline int32_t get_timeout_1() const { return ___timeout_1; }
	inline int32_t* get_address_of_timeout_1() { return &___timeout_1; }
	inline void set_timeout_1(int32_t value)
	{
		___timeout_1 = value;
	}

	inline static int32_t get_offset_of_timer_2() { return static_cast<int32_t>(offsetof(HeartBeatService_t3983407957, ___timer_2)); }
	inline Timer_t3701448099 * get_timer_2() const { return ___timer_2; }
	inline Timer_t3701448099 ** get_address_of_timer_2() { return &___timer_2; }
	inline void set_timer_2(Timer_t3701448099 * value)
	{
		___timer_2 = value;
		Il2CppCodeGenWriteBarrier(&___timer_2, value);
	}

	inline static int32_t get_offset_of_lastTime_3() { return static_cast<int32_t>(offsetof(HeartBeatService_t3983407957, ___lastTime_3)); }
	inline DateTime_t4283661327  get_lastTime_3() const { return ___lastTime_3; }
	inline DateTime_t4283661327 * get_address_of_lastTime_3() { return &___lastTime_3; }
	inline void set_lastTime_3(DateTime_t4283661327  value)
	{
		___lastTime_3 = value;
	}

	inline static int32_t get_offset_of_protocol_4() { return static_cast<int32_t>(offsetof(HeartBeatService_t3983407957, ___protocol_4)); }
	inline Protocol_t400511732 * get_protocol_4() const { return ___protocol_4; }
	inline Protocol_t400511732 ** get_address_of_protocol_4() { return &___protocol_4; }
	inline void set_protocol_4(Protocol_t400511732 * value)
	{
		___protocol_4 = value;
		Il2CppCodeGenWriteBarrier(&___protocol_4, value);
	}

	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(HeartBeatService_t3983407957, ___id_6)); }
	inline int32_t get_id_6() const { return ___id_6; }
	inline int32_t* get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(int32_t value)
	{
		___id_6 = value;
	}
};

struct HeartBeatService_t3983407957_StaticFields
{
public:
	// System.Int32 Pomelo.DotNetClient.HeartBeatService::count
	int32_t ___count_5;

public:
	inline static int32_t get_offset_of_count_5() { return static_cast<int32_t>(offsetof(HeartBeatService_t3983407957_StaticFields, ___count_5)); }
	inline int32_t get_count_5() const { return ___count_5; }
	inline int32_t* get_address_of_count_5() { return &___count_5; }
	inline void set_count_5(int32_t value)
	{
		___count_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
