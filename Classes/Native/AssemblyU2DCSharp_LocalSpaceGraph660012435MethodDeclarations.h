﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LocalSpaceGraph
struct LocalSpaceGraph_t660012435;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"

// System.Void LocalSpaceGraph::.ctor()
extern "C"  void LocalSpaceGraph__ctor_m1720278840 (LocalSpaceGraph_t660012435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LocalSpaceGraph::Start()
extern "C"  void LocalSpaceGraph_Start_m667416632 (LocalSpaceGraph_t660012435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Matrix4x4 LocalSpaceGraph::GetMatrix()
extern "C"  Matrix4x4_t1651859333  LocalSpaceGraph_GetMatrix_m4045817320 (LocalSpaceGraph_t660012435 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
