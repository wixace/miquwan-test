﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// ScreenShake
struct ScreenShake_t1613343194;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Camera
struct Camera_t2727095145;
// CameraHelper
struct CameraHelper_t3196871507;
// CameraSmoothFollow
struct CameraSmoothFollow_t2624612068;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraHelper
struct  CameraHelper_t3196871507  : public MonoBehaviour_t667441552
{
public:
	// System.Single CameraHelper::fieldMin
	float ___fieldMin_4;
	// System.Single CameraHelper::timeScale
	float ___timeScale_5;
	// UnityEngine.Vector3 CameraHelper::initOffset
	Vector3_t4282066566  ___initOffset_6;
	// System.Single CameraHelper::zoomInDamping
	float ___zoomInDamping_7;
	// System.Single CameraHelper::zoomOutDamping
	float ___zoomOutDamping_8;
	// System.Single CameraHelper::zoomInTime
	float ___zoomInTime_9;
	// System.Single CameraHelper::mZoomInTime
	float ___mZoomInTime_10;
	// System.Single CameraHelper::mZoomOutTime
	float ___mZoomOutTime_11;
	// System.Single CameraHelper::waitTime
	float ___waitTime_12;
	// System.Boolean CameraHelper::bInScaler
	bool ___bInScaler_13;
	// System.Boolean CameraHelper::bZoomIn
	bool ___bZoomIn_14;
	// System.Single CameraHelper::curFieldView
	float ___curFieldView_15;
	// System.Single CameraHelper::fieldMax
	float ___fieldMax_16;
	// System.Single CameraHelper::baseTime
	float ___baseTime_17;
	// ScreenShake CameraHelper::ss
	ScreenShake_t1613343194 * ___ss_18;
	// UnityEngine.Transform CameraHelper::tf
	Transform_t1659122786 * ___tf_19;
	// UnityEngine.Camera CameraHelper::effectCamera
	Camera_t2727095145 * ___effectCamera_20;
	// System.Boolean CameraHelper::motionBluring
	bool ___motionBluring_22;
	// System.Boolean CameraHelper::isTeamSkillCamera
	bool ___isTeamSkillCamera_23;
	// System.Single CameraHelper::ry
	float ___ry_24;
	// System.Single CameraHelper::rx
	float ___rx_25;
	// System.Single CameraHelper::distance
	float ___distance_26;
	// System.Int32 CameraHelper::effid
	int32_t ___effid_27;
	// UnityEngine.Vector3 CameraHelper::lastPos
	Vector3_t4282066566  ___lastPos_28;
	// System.Single CameraHelper::lastFov
	float ___lastFov_29;
	// CameraSmoothFollow CameraHelper::<csf>k__BackingField
	CameraSmoothFollow_t2624612068 * ___U3CcsfU3Ek__BackingField_30;

public:
	inline static int32_t get_offset_of_fieldMin_4() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___fieldMin_4)); }
	inline float get_fieldMin_4() const { return ___fieldMin_4; }
	inline float* get_address_of_fieldMin_4() { return &___fieldMin_4; }
	inline void set_fieldMin_4(float value)
	{
		___fieldMin_4 = value;
	}

	inline static int32_t get_offset_of_timeScale_5() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___timeScale_5)); }
	inline float get_timeScale_5() const { return ___timeScale_5; }
	inline float* get_address_of_timeScale_5() { return &___timeScale_5; }
	inline void set_timeScale_5(float value)
	{
		___timeScale_5 = value;
	}

	inline static int32_t get_offset_of_initOffset_6() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___initOffset_6)); }
	inline Vector3_t4282066566  get_initOffset_6() const { return ___initOffset_6; }
	inline Vector3_t4282066566 * get_address_of_initOffset_6() { return &___initOffset_6; }
	inline void set_initOffset_6(Vector3_t4282066566  value)
	{
		___initOffset_6 = value;
	}

	inline static int32_t get_offset_of_zoomInDamping_7() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___zoomInDamping_7)); }
	inline float get_zoomInDamping_7() const { return ___zoomInDamping_7; }
	inline float* get_address_of_zoomInDamping_7() { return &___zoomInDamping_7; }
	inline void set_zoomInDamping_7(float value)
	{
		___zoomInDamping_7 = value;
	}

	inline static int32_t get_offset_of_zoomOutDamping_8() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___zoomOutDamping_8)); }
	inline float get_zoomOutDamping_8() const { return ___zoomOutDamping_8; }
	inline float* get_address_of_zoomOutDamping_8() { return &___zoomOutDamping_8; }
	inline void set_zoomOutDamping_8(float value)
	{
		___zoomOutDamping_8 = value;
	}

	inline static int32_t get_offset_of_zoomInTime_9() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___zoomInTime_9)); }
	inline float get_zoomInTime_9() const { return ___zoomInTime_9; }
	inline float* get_address_of_zoomInTime_9() { return &___zoomInTime_9; }
	inline void set_zoomInTime_9(float value)
	{
		___zoomInTime_9 = value;
	}

	inline static int32_t get_offset_of_mZoomInTime_10() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___mZoomInTime_10)); }
	inline float get_mZoomInTime_10() const { return ___mZoomInTime_10; }
	inline float* get_address_of_mZoomInTime_10() { return &___mZoomInTime_10; }
	inline void set_mZoomInTime_10(float value)
	{
		___mZoomInTime_10 = value;
	}

	inline static int32_t get_offset_of_mZoomOutTime_11() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___mZoomOutTime_11)); }
	inline float get_mZoomOutTime_11() const { return ___mZoomOutTime_11; }
	inline float* get_address_of_mZoomOutTime_11() { return &___mZoomOutTime_11; }
	inline void set_mZoomOutTime_11(float value)
	{
		___mZoomOutTime_11 = value;
	}

	inline static int32_t get_offset_of_waitTime_12() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___waitTime_12)); }
	inline float get_waitTime_12() const { return ___waitTime_12; }
	inline float* get_address_of_waitTime_12() { return &___waitTime_12; }
	inline void set_waitTime_12(float value)
	{
		___waitTime_12 = value;
	}

	inline static int32_t get_offset_of_bInScaler_13() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___bInScaler_13)); }
	inline bool get_bInScaler_13() const { return ___bInScaler_13; }
	inline bool* get_address_of_bInScaler_13() { return &___bInScaler_13; }
	inline void set_bInScaler_13(bool value)
	{
		___bInScaler_13 = value;
	}

	inline static int32_t get_offset_of_bZoomIn_14() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___bZoomIn_14)); }
	inline bool get_bZoomIn_14() const { return ___bZoomIn_14; }
	inline bool* get_address_of_bZoomIn_14() { return &___bZoomIn_14; }
	inline void set_bZoomIn_14(bool value)
	{
		___bZoomIn_14 = value;
	}

	inline static int32_t get_offset_of_curFieldView_15() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___curFieldView_15)); }
	inline float get_curFieldView_15() const { return ___curFieldView_15; }
	inline float* get_address_of_curFieldView_15() { return &___curFieldView_15; }
	inline void set_curFieldView_15(float value)
	{
		___curFieldView_15 = value;
	}

	inline static int32_t get_offset_of_fieldMax_16() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___fieldMax_16)); }
	inline float get_fieldMax_16() const { return ___fieldMax_16; }
	inline float* get_address_of_fieldMax_16() { return &___fieldMax_16; }
	inline void set_fieldMax_16(float value)
	{
		___fieldMax_16 = value;
	}

	inline static int32_t get_offset_of_baseTime_17() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___baseTime_17)); }
	inline float get_baseTime_17() const { return ___baseTime_17; }
	inline float* get_address_of_baseTime_17() { return &___baseTime_17; }
	inline void set_baseTime_17(float value)
	{
		___baseTime_17 = value;
	}

	inline static int32_t get_offset_of_ss_18() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___ss_18)); }
	inline ScreenShake_t1613343194 * get_ss_18() const { return ___ss_18; }
	inline ScreenShake_t1613343194 ** get_address_of_ss_18() { return &___ss_18; }
	inline void set_ss_18(ScreenShake_t1613343194 * value)
	{
		___ss_18 = value;
		Il2CppCodeGenWriteBarrier(&___ss_18, value);
	}

	inline static int32_t get_offset_of_tf_19() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___tf_19)); }
	inline Transform_t1659122786 * get_tf_19() const { return ___tf_19; }
	inline Transform_t1659122786 ** get_address_of_tf_19() { return &___tf_19; }
	inline void set_tf_19(Transform_t1659122786 * value)
	{
		___tf_19 = value;
		Il2CppCodeGenWriteBarrier(&___tf_19, value);
	}

	inline static int32_t get_offset_of_effectCamera_20() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___effectCamera_20)); }
	inline Camera_t2727095145 * get_effectCamera_20() const { return ___effectCamera_20; }
	inline Camera_t2727095145 ** get_address_of_effectCamera_20() { return &___effectCamera_20; }
	inline void set_effectCamera_20(Camera_t2727095145 * value)
	{
		___effectCamera_20 = value;
		Il2CppCodeGenWriteBarrier(&___effectCamera_20, value);
	}

	inline static int32_t get_offset_of_motionBluring_22() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___motionBluring_22)); }
	inline bool get_motionBluring_22() const { return ___motionBluring_22; }
	inline bool* get_address_of_motionBluring_22() { return &___motionBluring_22; }
	inline void set_motionBluring_22(bool value)
	{
		___motionBluring_22 = value;
	}

	inline static int32_t get_offset_of_isTeamSkillCamera_23() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___isTeamSkillCamera_23)); }
	inline bool get_isTeamSkillCamera_23() const { return ___isTeamSkillCamera_23; }
	inline bool* get_address_of_isTeamSkillCamera_23() { return &___isTeamSkillCamera_23; }
	inline void set_isTeamSkillCamera_23(bool value)
	{
		___isTeamSkillCamera_23 = value;
	}

	inline static int32_t get_offset_of_ry_24() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___ry_24)); }
	inline float get_ry_24() const { return ___ry_24; }
	inline float* get_address_of_ry_24() { return &___ry_24; }
	inline void set_ry_24(float value)
	{
		___ry_24 = value;
	}

	inline static int32_t get_offset_of_rx_25() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___rx_25)); }
	inline float get_rx_25() const { return ___rx_25; }
	inline float* get_address_of_rx_25() { return &___rx_25; }
	inline void set_rx_25(float value)
	{
		___rx_25 = value;
	}

	inline static int32_t get_offset_of_distance_26() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___distance_26)); }
	inline float get_distance_26() const { return ___distance_26; }
	inline float* get_address_of_distance_26() { return &___distance_26; }
	inline void set_distance_26(float value)
	{
		___distance_26 = value;
	}

	inline static int32_t get_offset_of_effid_27() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___effid_27)); }
	inline int32_t get_effid_27() const { return ___effid_27; }
	inline int32_t* get_address_of_effid_27() { return &___effid_27; }
	inline void set_effid_27(int32_t value)
	{
		___effid_27 = value;
	}

	inline static int32_t get_offset_of_lastPos_28() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___lastPos_28)); }
	inline Vector3_t4282066566  get_lastPos_28() const { return ___lastPos_28; }
	inline Vector3_t4282066566 * get_address_of_lastPos_28() { return &___lastPos_28; }
	inline void set_lastPos_28(Vector3_t4282066566  value)
	{
		___lastPos_28 = value;
	}

	inline static int32_t get_offset_of_lastFov_29() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___lastFov_29)); }
	inline float get_lastFov_29() const { return ___lastFov_29; }
	inline float* get_address_of_lastFov_29() { return &___lastFov_29; }
	inline void set_lastFov_29(float value)
	{
		___lastFov_29 = value;
	}

	inline static int32_t get_offset_of_U3CcsfU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507, ___U3CcsfU3Ek__BackingField_30)); }
	inline CameraSmoothFollow_t2624612068 * get_U3CcsfU3Ek__BackingField_30() const { return ___U3CcsfU3Ek__BackingField_30; }
	inline CameraSmoothFollow_t2624612068 ** get_address_of_U3CcsfU3Ek__BackingField_30() { return &___U3CcsfU3Ek__BackingField_30; }
	inline void set_U3CcsfU3Ek__BackingField_30(CameraSmoothFollow_t2624612068 * value)
	{
		___U3CcsfU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier(&___U3CcsfU3Ek__BackingField_30, value);
	}
};

struct CameraHelper_t3196871507_StaticFields
{
public:
	// CameraHelper CameraHelper::instance
	CameraHelper_t3196871507 * ___instance_21;

public:
	inline static int32_t get_offset_of_instance_21() { return static_cast<int32_t>(offsetof(CameraHelper_t3196871507_StaticFields, ___instance_21)); }
	inline CameraHelper_t3196871507 * get_instance_21() const { return ___instance_21; }
	inline CameraHelper_t3196871507 ** get_address_of_instance_21() { return &___instance_21; }
	inline void set_instance_21(CameraHelper_t3196871507 * value)
	{
		___instance_21 = value;
		Il2CppCodeGenWriteBarrier(&___instance_21, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
