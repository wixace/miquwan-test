﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSAISPlayerDataGenerated
struct CSAISPlayerDataGenerated_t437556457;
// JSVCall
struct JSVCall_t3708497963;
// CSHeroUnit[]
struct CSHeroUnitU5BU5D_t1342235227;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"

// System.Void CSAISPlayerDataGenerated::.ctor()
extern "C"  void CSAISPlayerDataGenerated__ctor_m1240166354 (CSAISPlayerDataGenerated_t437556457 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSAISPlayerDataGenerated::CSAISPlayerData_CSAISPlayerData1(JSVCall,System.Int32)
extern "C"  bool CSAISPlayerDataGenerated_CSAISPlayerData_CSAISPlayerData1_m2813698650 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSAISPlayerDataGenerated::CSAISPlayerData_roleId(JSVCall)
extern "C"  void CSAISPlayerDataGenerated_CSAISPlayerData_roleId_m1934575755 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSAISPlayerDataGenerated::CSAISPlayerData_name(JSVCall)
extern "C"  void CSAISPlayerDataGenerated_CSAISPlayerData_name_m3768362225 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSAISPlayerDataGenerated::CSAISPlayerData_unionName(JSVCall)
extern "C"  void CSAISPlayerDataGenerated_CSAISPlayerData_unionName_m495896822 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSAISPlayerDataGenerated::CSAISPlayerData_icon(JSVCall)
extern "C"  void CSAISPlayerDataGenerated_CSAISPlayerData_icon_m131018435 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSAISPlayerDataGenerated::CSAISPlayerData_rank(JSVCall)
extern "C"  void CSAISPlayerDataGenerated_CSAISPlayerData_rank_m3913685104 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSAISPlayerDataGenerated::CSAISPlayerData_lv(JSVCall)
extern "C"  void CSAISPlayerDataGenerated_CSAISPlayerData_lv_m1373400658 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSAISPlayerDataGenerated::CSAISPlayerData_vipLv(JSVCall)
extern "C"  void CSAISPlayerDataGenerated_CSAISPlayerData_vipLv_m3643458569 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSAISPlayerDataGenerated::CSAISPlayerData_exp(JSVCall)
extern "C"  void CSAISPlayerDataGenerated_CSAISPlayerData_exp_m3126027187 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSAISPlayerDataGenerated::CSAISPlayerData_power(JSVCall)
extern "C"  void CSAISPlayerDataGenerated_CSAISPlayerData_power_m336947019 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSAISPlayerDataGenerated::CSAISPlayerData_serviceId(JSVCall)
extern "C"  void CSAISPlayerDataGenerated_CSAISPlayerData_serviceId_m2203157760 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSAISPlayerDataGenerated::CSAISPlayerData_serviceName(JSVCall)
extern "C"  void CSAISPlayerDataGenerated_CSAISPlayerData_serviceName_m3744300112 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSAISPlayerDataGenerated::CSAISPlayerData_heroGroups(JSVCall)
extern "C"  void CSAISPlayerDataGenerated_CSAISPlayerData_heroGroups_m2321159150 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSAISPlayerDataGenerated::__Register()
extern "C"  void CSAISPlayerDataGenerated___Register_m1725767733 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSHeroUnit[] CSAISPlayerDataGenerated::<CSAISPlayerData_heroGroups>m__21()
extern "C"  CSHeroUnitU5BU5D_t1342235227* CSAISPlayerDataGenerated_U3CCSAISPlayerData_heroGroupsU3Em__21_m2119085780 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CSAISPlayerDataGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool CSAISPlayerDataGenerated_ilo_attachFinalizerObject1_m712283725 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSAISPlayerDataGenerated::ilo_setStringS2(System.Int32,System.String)
extern "C"  void CSAISPlayerDataGenerated_ilo_setStringS2_m1544791580 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String CSAISPlayerDataGenerated::ilo_getStringS3(System.Int32)
extern "C"  String_t* CSAISPlayerDataGenerated_ilo_getStringS3_m306212498 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 CSAISPlayerDataGenerated::ilo_getUInt324(System.Int32)
extern "C"  uint32_t CSAISPlayerDataGenerated_ilo_getUInt324_m3127704182 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSAISPlayerDataGenerated::ilo_setUInt325(System.Int32,System.UInt32)
extern "C"  void CSAISPlayerDataGenerated_ilo_setUInt325_m4270914436 (Il2CppObject * __this /* static, unused */, int32_t ___e0, uint32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CSAISPlayerDataGenerated::ilo_setArrayS6(System.Int32,System.Int32,System.Boolean)
extern "C"  void CSAISPlayerDataGenerated_ilo_setArrayS6_m2546708790 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 CSAISPlayerDataGenerated::ilo_getArrayLength7(System.Int32)
extern "C"  int32_t CSAISPlayerDataGenerated_ilo_getArrayLength7_m311656302 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
