﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_capirpeMoukole126
struct  M_capirpeMoukole126_t2533643221  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_capirpeMoukole126::_mawliMeekai
	String_t* ____mawliMeekai_0;
	// System.UInt32 GarbageiOS.M_capirpeMoukole126::_lihe
	uint32_t ____lihe_1;
	// System.Boolean GarbageiOS.M_capirpeMoukole126::_rawselkeSutougoo
	bool ____rawselkeSutougoo_2;
	// System.Single GarbageiOS.M_capirpeMoukole126::_pista
	float ____pista_3;
	// System.Boolean GarbageiOS.M_capirpeMoukole126::_sisdooDearpeni
	bool ____sisdooDearpeni_4;
	// System.Single GarbageiOS.M_capirpeMoukole126::_rewasteaJaswedree
	float ____rewasteaJaswedree_5;
	// System.Boolean GarbageiOS.M_capirpeMoukole126::_tairbiMeefaru
	bool ____tairbiMeefaru_6;
	// System.Single GarbageiOS.M_capirpeMoukole126::_yowlawa
	float ____yowlawa_7;
	// System.UInt32 GarbageiOS.M_capirpeMoukole126::_sohiraWuxowga
	uint32_t ____sohiraWuxowga_8;
	// System.UInt32 GarbageiOS.M_capirpeMoukole126::_sabupiMeesir
	uint32_t ____sabupiMeesir_9;
	// System.Single GarbageiOS.M_capirpeMoukole126::_drataraw
	float ____drataraw_10;
	// System.Boolean GarbageiOS.M_capirpeMoukole126::_ciqese
	bool ____ciqese_11;
	// System.String GarbageiOS.M_capirpeMoukole126::_paslarMese
	String_t* ____paslarMese_12;
	// System.UInt32 GarbageiOS.M_capirpeMoukole126::_pejerchou
	uint32_t ____pejerchou_13;
	// System.Int32 GarbageiOS.M_capirpeMoukole126::_mocu
	int32_t ____mocu_14;
	// System.UInt32 GarbageiOS.M_capirpeMoukole126::_vajisdeeBatirya
	uint32_t ____vajisdeeBatirya_15;
	// System.Single GarbageiOS.M_capirpeMoukole126::_kaygawYocihu
	float ____kaygawYocihu_16;
	// System.UInt32 GarbageiOS.M_capirpeMoukole126::_saybassi
	uint32_t ____saybassi_17;
	// System.Single GarbageiOS.M_capirpeMoukole126::_lirhe
	float ____lirhe_18;
	// System.UInt32 GarbageiOS.M_capirpeMoukole126::_xafee
	uint32_t ____xafee_19;
	// System.String GarbageiOS.M_capirpeMoukole126::_loumisSoni
	String_t* ____loumisSoni_20;
	// System.UInt32 GarbageiOS.M_capirpeMoukole126::_hereqearcou
	uint32_t ____hereqearcou_21;
	// System.UInt32 GarbageiOS.M_capirpeMoukole126::_fissairnar
	uint32_t ____fissairnar_22;
	// System.Boolean GarbageiOS.M_capirpeMoukole126::_stapiHisbou
	bool ____stapiHisbou_23;
	// System.Boolean GarbageiOS.M_capirpeMoukole126::_pahiKerder
	bool ____pahiKerder_24;
	// System.Single GarbageiOS.M_capirpeMoukole126::_nallxallserBoojea
	float ____nallxallserBoojea_25;
	// System.Single GarbageiOS.M_capirpeMoukole126::_wisnakis
	float ____wisnakis_26;
	// System.Boolean GarbageiOS.M_capirpeMoukole126::_vaydreejuFarza
	bool ____vaydreejuFarza_27;
	// System.String GarbageiOS.M_capirpeMoukole126::_droreroPawjojis
	String_t* ____droreroPawjojis_28;
	// System.Boolean GarbageiOS.M_capirpeMoukole126::_jomadeZowouye
	bool ____jomadeZowouye_29;
	// System.Boolean GarbageiOS.M_capirpeMoukole126::_culea
	bool ____culea_30;
	// System.UInt32 GarbageiOS.M_capirpeMoukole126::_gekeenair
	uint32_t ____gekeenair_31;
	// System.Single GarbageiOS.M_capirpeMoukole126::_wotixis
	float ____wotixis_32;
	// System.Int32 GarbageiOS.M_capirpeMoukole126::_chijemCasdootu
	int32_t ____chijemCasdootu_33;
	// System.UInt32 GarbageiOS.M_capirpeMoukole126::_meltur
	uint32_t ____meltur_34;
	// System.Int32 GarbageiOS.M_capirpeMoukole126::_yascouPibarbai
	int32_t ____yascouPibarbai_35;

public:
	inline static int32_t get_offset_of__mawliMeekai_0() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____mawliMeekai_0)); }
	inline String_t* get__mawliMeekai_0() const { return ____mawliMeekai_0; }
	inline String_t** get_address_of__mawliMeekai_0() { return &____mawliMeekai_0; }
	inline void set__mawliMeekai_0(String_t* value)
	{
		____mawliMeekai_0 = value;
		Il2CppCodeGenWriteBarrier(&____mawliMeekai_0, value);
	}

	inline static int32_t get_offset_of__lihe_1() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____lihe_1)); }
	inline uint32_t get__lihe_1() const { return ____lihe_1; }
	inline uint32_t* get_address_of__lihe_1() { return &____lihe_1; }
	inline void set__lihe_1(uint32_t value)
	{
		____lihe_1 = value;
	}

	inline static int32_t get_offset_of__rawselkeSutougoo_2() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____rawselkeSutougoo_2)); }
	inline bool get__rawselkeSutougoo_2() const { return ____rawselkeSutougoo_2; }
	inline bool* get_address_of__rawselkeSutougoo_2() { return &____rawselkeSutougoo_2; }
	inline void set__rawselkeSutougoo_2(bool value)
	{
		____rawselkeSutougoo_2 = value;
	}

	inline static int32_t get_offset_of__pista_3() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____pista_3)); }
	inline float get__pista_3() const { return ____pista_3; }
	inline float* get_address_of__pista_3() { return &____pista_3; }
	inline void set__pista_3(float value)
	{
		____pista_3 = value;
	}

	inline static int32_t get_offset_of__sisdooDearpeni_4() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____sisdooDearpeni_4)); }
	inline bool get__sisdooDearpeni_4() const { return ____sisdooDearpeni_4; }
	inline bool* get_address_of__sisdooDearpeni_4() { return &____sisdooDearpeni_4; }
	inline void set__sisdooDearpeni_4(bool value)
	{
		____sisdooDearpeni_4 = value;
	}

	inline static int32_t get_offset_of__rewasteaJaswedree_5() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____rewasteaJaswedree_5)); }
	inline float get__rewasteaJaswedree_5() const { return ____rewasteaJaswedree_5; }
	inline float* get_address_of__rewasteaJaswedree_5() { return &____rewasteaJaswedree_5; }
	inline void set__rewasteaJaswedree_5(float value)
	{
		____rewasteaJaswedree_5 = value;
	}

	inline static int32_t get_offset_of__tairbiMeefaru_6() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____tairbiMeefaru_6)); }
	inline bool get__tairbiMeefaru_6() const { return ____tairbiMeefaru_6; }
	inline bool* get_address_of__tairbiMeefaru_6() { return &____tairbiMeefaru_6; }
	inline void set__tairbiMeefaru_6(bool value)
	{
		____tairbiMeefaru_6 = value;
	}

	inline static int32_t get_offset_of__yowlawa_7() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____yowlawa_7)); }
	inline float get__yowlawa_7() const { return ____yowlawa_7; }
	inline float* get_address_of__yowlawa_7() { return &____yowlawa_7; }
	inline void set__yowlawa_7(float value)
	{
		____yowlawa_7 = value;
	}

	inline static int32_t get_offset_of__sohiraWuxowga_8() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____sohiraWuxowga_8)); }
	inline uint32_t get__sohiraWuxowga_8() const { return ____sohiraWuxowga_8; }
	inline uint32_t* get_address_of__sohiraWuxowga_8() { return &____sohiraWuxowga_8; }
	inline void set__sohiraWuxowga_8(uint32_t value)
	{
		____sohiraWuxowga_8 = value;
	}

	inline static int32_t get_offset_of__sabupiMeesir_9() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____sabupiMeesir_9)); }
	inline uint32_t get__sabupiMeesir_9() const { return ____sabupiMeesir_9; }
	inline uint32_t* get_address_of__sabupiMeesir_9() { return &____sabupiMeesir_9; }
	inline void set__sabupiMeesir_9(uint32_t value)
	{
		____sabupiMeesir_9 = value;
	}

	inline static int32_t get_offset_of__drataraw_10() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____drataraw_10)); }
	inline float get__drataraw_10() const { return ____drataraw_10; }
	inline float* get_address_of__drataraw_10() { return &____drataraw_10; }
	inline void set__drataraw_10(float value)
	{
		____drataraw_10 = value;
	}

	inline static int32_t get_offset_of__ciqese_11() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____ciqese_11)); }
	inline bool get__ciqese_11() const { return ____ciqese_11; }
	inline bool* get_address_of__ciqese_11() { return &____ciqese_11; }
	inline void set__ciqese_11(bool value)
	{
		____ciqese_11 = value;
	}

	inline static int32_t get_offset_of__paslarMese_12() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____paslarMese_12)); }
	inline String_t* get__paslarMese_12() const { return ____paslarMese_12; }
	inline String_t** get_address_of__paslarMese_12() { return &____paslarMese_12; }
	inline void set__paslarMese_12(String_t* value)
	{
		____paslarMese_12 = value;
		Il2CppCodeGenWriteBarrier(&____paslarMese_12, value);
	}

	inline static int32_t get_offset_of__pejerchou_13() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____pejerchou_13)); }
	inline uint32_t get__pejerchou_13() const { return ____pejerchou_13; }
	inline uint32_t* get_address_of__pejerchou_13() { return &____pejerchou_13; }
	inline void set__pejerchou_13(uint32_t value)
	{
		____pejerchou_13 = value;
	}

	inline static int32_t get_offset_of__mocu_14() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____mocu_14)); }
	inline int32_t get__mocu_14() const { return ____mocu_14; }
	inline int32_t* get_address_of__mocu_14() { return &____mocu_14; }
	inline void set__mocu_14(int32_t value)
	{
		____mocu_14 = value;
	}

	inline static int32_t get_offset_of__vajisdeeBatirya_15() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____vajisdeeBatirya_15)); }
	inline uint32_t get__vajisdeeBatirya_15() const { return ____vajisdeeBatirya_15; }
	inline uint32_t* get_address_of__vajisdeeBatirya_15() { return &____vajisdeeBatirya_15; }
	inline void set__vajisdeeBatirya_15(uint32_t value)
	{
		____vajisdeeBatirya_15 = value;
	}

	inline static int32_t get_offset_of__kaygawYocihu_16() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____kaygawYocihu_16)); }
	inline float get__kaygawYocihu_16() const { return ____kaygawYocihu_16; }
	inline float* get_address_of__kaygawYocihu_16() { return &____kaygawYocihu_16; }
	inline void set__kaygawYocihu_16(float value)
	{
		____kaygawYocihu_16 = value;
	}

	inline static int32_t get_offset_of__saybassi_17() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____saybassi_17)); }
	inline uint32_t get__saybassi_17() const { return ____saybassi_17; }
	inline uint32_t* get_address_of__saybassi_17() { return &____saybassi_17; }
	inline void set__saybassi_17(uint32_t value)
	{
		____saybassi_17 = value;
	}

	inline static int32_t get_offset_of__lirhe_18() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____lirhe_18)); }
	inline float get__lirhe_18() const { return ____lirhe_18; }
	inline float* get_address_of__lirhe_18() { return &____lirhe_18; }
	inline void set__lirhe_18(float value)
	{
		____lirhe_18 = value;
	}

	inline static int32_t get_offset_of__xafee_19() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____xafee_19)); }
	inline uint32_t get__xafee_19() const { return ____xafee_19; }
	inline uint32_t* get_address_of__xafee_19() { return &____xafee_19; }
	inline void set__xafee_19(uint32_t value)
	{
		____xafee_19 = value;
	}

	inline static int32_t get_offset_of__loumisSoni_20() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____loumisSoni_20)); }
	inline String_t* get__loumisSoni_20() const { return ____loumisSoni_20; }
	inline String_t** get_address_of__loumisSoni_20() { return &____loumisSoni_20; }
	inline void set__loumisSoni_20(String_t* value)
	{
		____loumisSoni_20 = value;
		Il2CppCodeGenWriteBarrier(&____loumisSoni_20, value);
	}

	inline static int32_t get_offset_of__hereqearcou_21() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____hereqearcou_21)); }
	inline uint32_t get__hereqearcou_21() const { return ____hereqearcou_21; }
	inline uint32_t* get_address_of__hereqearcou_21() { return &____hereqearcou_21; }
	inline void set__hereqearcou_21(uint32_t value)
	{
		____hereqearcou_21 = value;
	}

	inline static int32_t get_offset_of__fissairnar_22() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____fissairnar_22)); }
	inline uint32_t get__fissairnar_22() const { return ____fissairnar_22; }
	inline uint32_t* get_address_of__fissairnar_22() { return &____fissairnar_22; }
	inline void set__fissairnar_22(uint32_t value)
	{
		____fissairnar_22 = value;
	}

	inline static int32_t get_offset_of__stapiHisbou_23() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____stapiHisbou_23)); }
	inline bool get__stapiHisbou_23() const { return ____stapiHisbou_23; }
	inline bool* get_address_of__stapiHisbou_23() { return &____stapiHisbou_23; }
	inline void set__stapiHisbou_23(bool value)
	{
		____stapiHisbou_23 = value;
	}

	inline static int32_t get_offset_of__pahiKerder_24() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____pahiKerder_24)); }
	inline bool get__pahiKerder_24() const { return ____pahiKerder_24; }
	inline bool* get_address_of__pahiKerder_24() { return &____pahiKerder_24; }
	inline void set__pahiKerder_24(bool value)
	{
		____pahiKerder_24 = value;
	}

	inline static int32_t get_offset_of__nallxallserBoojea_25() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____nallxallserBoojea_25)); }
	inline float get__nallxallserBoojea_25() const { return ____nallxallserBoojea_25; }
	inline float* get_address_of__nallxallserBoojea_25() { return &____nallxallserBoojea_25; }
	inline void set__nallxallserBoojea_25(float value)
	{
		____nallxallserBoojea_25 = value;
	}

	inline static int32_t get_offset_of__wisnakis_26() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____wisnakis_26)); }
	inline float get__wisnakis_26() const { return ____wisnakis_26; }
	inline float* get_address_of__wisnakis_26() { return &____wisnakis_26; }
	inline void set__wisnakis_26(float value)
	{
		____wisnakis_26 = value;
	}

	inline static int32_t get_offset_of__vaydreejuFarza_27() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____vaydreejuFarza_27)); }
	inline bool get__vaydreejuFarza_27() const { return ____vaydreejuFarza_27; }
	inline bool* get_address_of__vaydreejuFarza_27() { return &____vaydreejuFarza_27; }
	inline void set__vaydreejuFarza_27(bool value)
	{
		____vaydreejuFarza_27 = value;
	}

	inline static int32_t get_offset_of__droreroPawjojis_28() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____droreroPawjojis_28)); }
	inline String_t* get__droreroPawjojis_28() const { return ____droreroPawjojis_28; }
	inline String_t** get_address_of__droreroPawjojis_28() { return &____droreroPawjojis_28; }
	inline void set__droreroPawjojis_28(String_t* value)
	{
		____droreroPawjojis_28 = value;
		Il2CppCodeGenWriteBarrier(&____droreroPawjojis_28, value);
	}

	inline static int32_t get_offset_of__jomadeZowouye_29() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____jomadeZowouye_29)); }
	inline bool get__jomadeZowouye_29() const { return ____jomadeZowouye_29; }
	inline bool* get_address_of__jomadeZowouye_29() { return &____jomadeZowouye_29; }
	inline void set__jomadeZowouye_29(bool value)
	{
		____jomadeZowouye_29 = value;
	}

	inline static int32_t get_offset_of__culea_30() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____culea_30)); }
	inline bool get__culea_30() const { return ____culea_30; }
	inline bool* get_address_of__culea_30() { return &____culea_30; }
	inline void set__culea_30(bool value)
	{
		____culea_30 = value;
	}

	inline static int32_t get_offset_of__gekeenair_31() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____gekeenair_31)); }
	inline uint32_t get__gekeenair_31() const { return ____gekeenair_31; }
	inline uint32_t* get_address_of__gekeenair_31() { return &____gekeenair_31; }
	inline void set__gekeenair_31(uint32_t value)
	{
		____gekeenair_31 = value;
	}

	inline static int32_t get_offset_of__wotixis_32() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____wotixis_32)); }
	inline float get__wotixis_32() const { return ____wotixis_32; }
	inline float* get_address_of__wotixis_32() { return &____wotixis_32; }
	inline void set__wotixis_32(float value)
	{
		____wotixis_32 = value;
	}

	inline static int32_t get_offset_of__chijemCasdootu_33() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____chijemCasdootu_33)); }
	inline int32_t get__chijemCasdootu_33() const { return ____chijemCasdootu_33; }
	inline int32_t* get_address_of__chijemCasdootu_33() { return &____chijemCasdootu_33; }
	inline void set__chijemCasdootu_33(int32_t value)
	{
		____chijemCasdootu_33 = value;
	}

	inline static int32_t get_offset_of__meltur_34() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____meltur_34)); }
	inline uint32_t get__meltur_34() const { return ____meltur_34; }
	inline uint32_t* get_address_of__meltur_34() { return &____meltur_34; }
	inline void set__meltur_34(uint32_t value)
	{
		____meltur_34 = value;
	}

	inline static int32_t get_offset_of__yascouPibarbai_35() { return static_cast<int32_t>(offsetof(M_capirpeMoukole126_t2533643221, ____yascouPibarbai_35)); }
	inline int32_t get__yascouPibarbai_35() const { return ____yascouPibarbai_35; }
	inline int32_t* get_address_of__yascouPibarbai_35() { return &____yascouPibarbai_35; }
	inline void set__yascouPibarbai_35(int32_t value)
	{
		____yascouPibarbai_35 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
