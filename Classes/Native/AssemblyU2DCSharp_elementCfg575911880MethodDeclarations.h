﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// elementCfg
struct elementCfg_t575911880;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void elementCfg::.ctor()
extern "C"  void elementCfg__ctor_m3222675923 (elementCfg_t575911880 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void elementCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void elementCfg_Init_m3159763154 (elementCfg_t575911880 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
