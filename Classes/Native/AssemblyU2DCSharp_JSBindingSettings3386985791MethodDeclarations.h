﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// JSBindingSettings
struct JSBindingSettings_t3386985791;
// System.Type
struct Type_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.String
struct String_t;
// System.Reflection.PropertyInfo[]
struct PropertyInfoU5BU5D_t4286713048;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_MemberInfo3995515898.h"
#include "mscorlib_System_String7231557.h"

// System.Void JSBindingSettings::.ctor()
extern "C"  void JSBindingSettings__ctor_m2418792204 (JSBindingSettings_t3386985791 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JSBindingSettings::.cctor()
extern "C"  void JSBindingSettings__cctor_m1486018081 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSBindingSettings::IsDiscard(System.Type,System.Reflection.MemberInfo)
extern "C"  bool JSBindingSettings_IsDiscard_m4222342771 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MemberInfo_t * ___memberInfo1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSBindingSettings::IsSupportByDotNet2SubSet(System.String)
extern "C"  bool JSBindingSettings_IsSupportByDotNet2SubSet_m819398234 (Il2CppObject * __this /* static, unused */, String_t* ___functionName0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean JSBindingSettings::NeedGenDefaultConstructor(System.Type)
extern "C"  bool JSBindingSettings_NeedGenDefaultConstructor_m525568712 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSBindingSettings::get_jsDir()
extern "C"  String_t* JSBindingSettings_get_jsDir_m3512496200 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSBindingSettings::get_mergedJsDir()
extern "C"  String_t* JSBindingSettings_get_mergedJsDir_m2592595260 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSBindingSettings::get_jscDir()
extern "C"  String_t* JSBindingSettings_get_jscDir_m2366336209 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSBindingSettings::get_csDir()
extern "C"  String_t* JSBindingSettings_get_csDir_m1594937729 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSBindingSettings::get_csGeneratedDir()
extern "C"  String_t* JSBindingSettings_get_csGeneratedDir_m1591927852 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSBindingSettings::get_sharpkitGeneratedFiles()
extern "C"  String_t* JSBindingSettings_get_sharpkitGeneratedFiles_m3029833874 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSBindingSettings::get_JsType_sharpkitGeneratedFiles()
extern "C"  String_t* JSBindingSettings_get_JsType_sharpkitGeneratedFiles_m262824244 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSBindingSettings::get_JsType_Javascript()
extern "C"  String_t* JSBindingSettings_get_JsType_Javascript_m1052896013 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSBindingSettings::get_monoBehaviour2JSComponentName()
extern "C"  String_t* JSBindingSettings_get_monoBehaviour2JSComponentName_m3590778123 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSBindingSettings::get_jsGeneratedDir()
extern "C"  String_t* JSBindingSettings_get_jsGeneratedDir_m2313155909 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSBindingSettings::get_jsGeneratedFiles()
extern "C"  String_t* JSBindingSettings_get_jsGeneratedFiles_m4216092399 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSBindingSettings::get_sharpKitGenFileFullDir()
extern "C"  String_t* JSBindingSettings_get_sharpKitGenFileFullDir_m3162035388 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSBindingSettings::get_SharpkitGeneratedFilesAll()
extern "C"  String_t* JSBindingSettings_get_SharpkitGeneratedFilesAll_m2361526513 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSBindingSettings::get_GeneratedFilesAll()
extern "C"  String_t* JSBindingSettings_get_GeneratedFilesAll_m1502550589 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo[] JSBindingSettings::GetTypeSerializedProperties(System.Type)
extern "C"  PropertyInfoU5BU5D_t4286713048* JSBindingSettings_GetTypeSerializedProperties_m1616732212 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String JSBindingSettings::ilo_get_jsDir1()
extern "C"  String_t* JSBindingSettings_ilo_get_jsDir1_m1284528702 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
