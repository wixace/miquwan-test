﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._6a8123d824a68f6bcd1bf45fd0a9917d
struct _6a8123d824a68f6bcd1bf45fd0a9917d_t2639511133;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__6a8123d824a68f6bcd1bf45f2639511133.h"

// System.Void Little._6a8123d824a68f6bcd1bf45fd0a9917d::.ctor()
extern "C"  void _6a8123d824a68f6bcd1bf45fd0a9917d__ctor_m2893234288 (_6a8123d824a68f6bcd1bf45fd0a9917d_t2639511133 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6a8123d824a68f6bcd1bf45fd0a9917d::_6a8123d824a68f6bcd1bf45fd0a9917dm2(System.Int32)
extern "C"  int32_t _6a8123d824a68f6bcd1bf45fd0a9917d__6a8123d824a68f6bcd1bf45fd0a9917dm2_m1515981945 (_6a8123d824a68f6bcd1bf45fd0a9917d_t2639511133 * __this, int32_t ____6a8123d824a68f6bcd1bf45fd0a9917da0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6a8123d824a68f6bcd1bf45fd0a9917d::_6a8123d824a68f6bcd1bf45fd0a9917dm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _6a8123d824a68f6bcd1bf45fd0a9917d__6a8123d824a68f6bcd1bf45fd0a9917dm_m415143517 (_6a8123d824a68f6bcd1bf45fd0a9917d_t2639511133 * __this, int32_t ____6a8123d824a68f6bcd1bf45fd0a9917da0, int32_t ____6a8123d824a68f6bcd1bf45fd0a9917d521, int32_t ____6a8123d824a68f6bcd1bf45fd0a9917dc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._6a8123d824a68f6bcd1bf45fd0a9917d::ilo__6a8123d824a68f6bcd1bf45fd0a9917dm21(Little._6a8123d824a68f6bcd1bf45fd0a9917d,System.Int32)
extern "C"  int32_t _6a8123d824a68f6bcd1bf45fd0a9917d_ilo__6a8123d824a68f6bcd1bf45fd0a9917dm21_m1197328068 (Il2CppObject * __this /* static, unused */, _6a8123d824a68f6bcd1bf45fd0a9917d_t2639511133 * ____this0, int32_t ____6a8123d824a68f6bcd1bf45fd0a9917da1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
