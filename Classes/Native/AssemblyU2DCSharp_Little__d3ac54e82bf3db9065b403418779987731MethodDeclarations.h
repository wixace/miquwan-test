﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._d3ac54e82bf3db9065b403418298fd85
struct _d3ac54e82bf3db9065b403418298fd85_t779987731;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__d3ac54e82bf3db9065b403418779987731.h"

// System.Void Little._d3ac54e82bf3db9065b403418298fd85::.ctor()
extern "C"  void _d3ac54e82bf3db9065b403418298fd85__ctor_m3119071354 (_d3ac54e82bf3db9065b403418298fd85_t779987731 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._d3ac54e82bf3db9065b403418298fd85::_d3ac54e82bf3db9065b403418298fd85m2(System.Int32)
extern "C"  int32_t _d3ac54e82bf3db9065b403418298fd85__d3ac54e82bf3db9065b403418298fd85m2_m964042937 (_d3ac54e82bf3db9065b403418298fd85_t779987731 * __this, int32_t ____d3ac54e82bf3db9065b403418298fd85a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._d3ac54e82bf3db9065b403418298fd85::_d3ac54e82bf3db9065b403418298fd85m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _d3ac54e82bf3db9065b403418298fd85__d3ac54e82bf3db9065b403418298fd85m_m4223126045 (_d3ac54e82bf3db9065b403418298fd85_t779987731 * __this, int32_t ____d3ac54e82bf3db9065b403418298fd85a0, int32_t ____d3ac54e82bf3db9065b403418298fd85641, int32_t ____d3ac54e82bf3db9065b403418298fd85c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._d3ac54e82bf3db9065b403418298fd85::ilo__d3ac54e82bf3db9065b403418298fd85m21(Little._d3ac54e82bf3db9065b403418298fd85,System.Int32)
extern "C"  int32_t _d3ac54e82bf3db9065b403418298fd85_ilo__d3ac54e82bf3db9065b403418298fd85m21_m3496373306 (Il2CppObject * __this /* static, unused */, _d3ac54e82bf3db9065b403418298fd85_t779987731 * ____this0, int32_t ____d3ac54e82bf3db9065b403418298fd85a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
