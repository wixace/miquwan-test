﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TweenPositionGenerated
struct TweenPositionGenerated_t1276790619;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// TweenPosition
struct TweenPosition_t3684358292;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_TweenPosition3684358292.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void TweenPositionGenerated::.ctor()
extern "C"  void TweenPositionGenerated__ctor_m2224882592 (TweenPositionGenerated_t1276790619 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenPositionGenerated::TweenPosition_TweenPosition1(JSVCall,System.Int32)
extern "C"  bool TweenPositionGenerated_TweenPosition_TweenPosition1_m4030835184 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenPositionGenerated::TweenPosition_from(JSVCall)
extern "C"  void TweenPositionGenerated_TweenPosition_from_m3752028174 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenPositionGenerated::TweenPosition_to(JSVCall)
extern "C"  void TweenPositionGenerated_TweenPosition_to_m4275675357 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenPositionGenerated::TweenPosition_worldSpace(JSVCall)
extern "C"  void TweenPositionGenerated_TweenPosition_worldSpace_m248584420 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenPositionGenerated::TweenPosition_cachedTransform(JSVCall)
extern "C"  void TweenPositionGenerated_TweenPosition_cachedTransform_m3503052330 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenPositionGenerated::TweenPosition_value(JSVCall)
extern "C"  void TweenPositionGenerated_TweenPosition_value_m1234155139 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenPositionGenerated::TweenPosition_SetEndToCurrentValue(JSVCall,System.Int32)
extern "C"  bool TweenPositionGenerated_TweenPosition_SetEndToCurrentValue_m293874751 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenPositionGenerated::TweenPosition_SetStartToCurrentValue(JSVCall,System.Int32)
extern "C"  bool TweenPositionGenerated_TweenPosition_SetStartToCurrentValue_m1971738694 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenPositionGenerated::TweenPosition_Begin__GameObject__Single__Vector3__Boolean(JSVCall,System.Int32)
extern "C"  bool TweenPositionGenerated_TweenPosition_Begin__GameObject__Single__Vector3__Boolean_m1724888009 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenPositionGenerated::TweenPosition_Begin__GameObject__Single__Vector3(JSVCall,System.Int32)
extern "C"  bool TweenPositionGenerated_TweenPosition_Begin__GameObject__Single__Vector3_m1943530113 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenPositionGenerated::__Register()
extern "C"  void TweenPositionGenerated___Register_m1790295335 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenPositionGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void TweenPositionGenerated_ilo_addJSCSRel1_m3356183132 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenPositionGenerated::ilo_setBooleanS2(System.Int32,System.Boolean)
extern "C"  void TweenPositionGenerated_ilo_setBooleanS2_m458840496 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean TweenPositionGenerated::ilo_getBooleanS3(System.Int32)
extern "C"  bool TweenPositionGenerated_ilo_getBooleanS3_m4010450606 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TweenPositionGenerated::ilo_get_value4(TweenPosition)
extern "C"  Vector3_t4282066566  TweenPositionGenerated_ilo_get_value4_m1026018529 (Il2CppObject * __this /* static, unused */, TweenPosition_t3684358292 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TweenPositionGenerated::ilo_set_value5(TweenPosition,UnityEngine.Vector3)
extern "C"  void TweenPositionGenerated_ilo_set_value5_m1957882521 (Il2CppObject * __this /* static, unused */, TweenPosition_t3684358292 * ____this0, Vector3_t4282066566  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object TweenPositionGenerated::ilo_getObject6(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * TweenPositionGenerated_ilo_getObject6_m2638995092 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single TweenPositionGenerated::ilo_getSingle7(System.Int32)
extern "C"  float TweenPositionGenerated_ilo_getSingle7_m736804365 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 TweenPositionGenerated::ilo_getVector3S8(System.Int32)
extern "C"  Vector3_t4282066566  TweenPositionGenerated_ilo_getVector3S8_m3416044385 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
