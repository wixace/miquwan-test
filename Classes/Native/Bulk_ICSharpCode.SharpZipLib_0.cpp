﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// ICSharpCode.SharpZipLib.Checksums.Adler32
struct Adler32_t2680377483;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// ICSharpCode.SharpZipLib.Checksums.Crc32
struct Crc32_t3523361801;
// ICSharpCode.SharpZipLib.Encryption.ZipAESTransform
struct ZipAESTransform_t496812608;
// ICSharpCode.SharpZipLib.GZip.GZipException
struct GZipException_t2414750427;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.String
struct String_t;
// ICSharpCode.SharpZipLib.GZip.GZipInputStream
struct GZipInputStream_t2489725366;
// System.IO.Stream
struct Stream_t1561764144;
// ICSharpCode.SharpZipLib.GZip.GZipOutputStream
struct GZipOutputStream_t2632250695;
// ICSharpCode.SharpZipLib.SharpZipBaseException
struct SharpZipBaseException_t2520014981;
// ICSharpCode.SharpZipLib.Zip.Compression.Deflater
struct Deflater_t2454063301;
// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterConstants
struct DeflaterConstants_t2584813498;
// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine
struct DeflaterEngine_t3684739431;
// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterPending
struct DeflaterPending_t1829109954;
// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman
struct DeflaterHuffman_t3769756376;
// ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree
struct Tree_t1054057453;
// System.Int16[]
struct Int16U5BU5D_t801762735;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ICSharpCode.SharpZipLib.Zip.Compression.Inflater
struct Inflater_t1975778921;
// ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader
struct InflaterDynHeader_t1952417709;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator
struct StreamManipulator_t2348681196;
// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree
struct InflaterHuffmanTree_t1141403250;
// ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer
struct PendingBuffer_t3572745737;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream
struct DeflaterOutputStream_t537202536;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;
// System.Object
struct Il2CppObject;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer
struct InflaterInputBuffer_t441930877;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream
struct InflaterInputStream_t928059325;
// ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow
struct OutputWindow_t617095569;
// ICSharpCode.SharpZipLib.Zip.ZipException
struct ZipException_t2326470748;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "ICSharpCode_SharpZipLib_U3CModuleU3E86524790.h"
#include "ICSharpCode_SharpZipLib_U3CModuleU3E86524790MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDe1292624594.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDe1292624594MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDet894410179.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDet894410179MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDet534724421.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDet534724421MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDe1956911858.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDe1956911858MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDet894410181.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDet894410181MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDe1956911883.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDe1956911883MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDet894410185.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDet894410185MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDet894410272.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDet894410272MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDet894410371.h"
#include "ICSharpCode_SharpZipLib_U3CPrivateImplementationDet894410371MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Ch2680377483.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Ch2680377483MethodDeclarations.h"
#include "mscorlib_System_Int641153838595.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Byte2862609660.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_ArgumentNullException3573189601MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_ArgumentNullException3573189601.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Ch3523361801.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Ch3523361801MethodDeclarations.h"
#include "mscorlib_System_Runtime_CompilerServices_RuntimeHe1593496111MethodDeclarations.h"
#include "mscorlib_System_RuntimeFieldHandle2347752062.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Enc496812608.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Enc496812608MethodDeclarations.h"
#include "mscorlib_System_Security_Cryptography_HashAlgorithm532578791MethodDeclarations.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Security_Cryptography_HMACSHA14024365272.h"
#include "mscorlib_System_Security_Cryptography_HashAlgorithm532578791.h"
#include "mscorlib_System_NotImplementedException1912495542MethodDeclarations.h"
#include "mscorlib_System_NotImplementedException1912495542.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_GZ2414750427.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_GZ2414750427MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Sh2520014981MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_GZ2489725366.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_GZ2489725366MethodDeclarations.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1975778921MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip928059325MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1975778921.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip928059325.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip441930877MethodDeclarations.h"
#include "mscorlib_System_IO_EndOfStreamException3956009005MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip441930877.h"
#include "mscorlib_System_IO_EndOfStreamException3956009005.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_GZ2632250695.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_GZ2632250695MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi2454063301MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip537202536MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi2454063301.h"
#include "mscorlib_System_InvalidOperationException1589641621MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_GZi278189375.h"
#include "mscorlib_System_InvalidOperationException1589641621.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip537202536.h"
#include "mscorlib_System_IO_Stream1561764144MethodDeclarations.h"
#include "mscorlib_System_DateTime4283661327MethodDeclarations.h"
#include "mscorlib_System_DateTime4283661327.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_GZi278189375MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Sh2520014981.h"
#include "mscorlib_System_ApplicationException3128023763MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1829109954MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3684739431MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1829109954.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3684739431.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1587604368.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3572745737MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi2584813498.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi2584813498MethodDeclarations.h"
#include "mscorlib_System_Math2862914300MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3769756376MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3769756376.h"
#include "mscorlib_System_Int161153838442.h"
#include "mscorlib_System_Array1146569071MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1054057453MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1054057453.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1587604368MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi2348681196MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip617095569MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi2348681196.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zip617095569.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1952417709.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1141403250.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1141403250MethodDeclarations.h"
#include "mscorlib_System_Exception3991598821.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi1952417709MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi3572745737.h"
#include "mscorlib_System_NotSupportedException1732551818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1732551818.h"
#include "mscorlib_System_IO_SeekOrigin4120335598.h"
#include "mscorlib_System_AsyncCallback1369114871.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi2326470748MethodDeclarations.h"
#include "ICSharpCode_SharpZipLib_ICSharpCode_SharpZipLib_Zi2326470748.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=10
extern "C" void __StaticArrayInitTypeSizeU3D10_t894410179_marshal_pinvoke(const __StaticArrayInitTypeSizeU3D10_t894410179& unmarshaled, __StaticArrayInitTypeSizeU3D10_t894410179_marshaled_pinvoke& marshaled)
{
}
extern "C" void __StaticArrayInitTypeSizeU3D10_t894410179_marshal_pinvoke_back(const __StaticArrayInitTypeSizeU3D10_t894410179_marshaled_pinvoke& marshaled, __StaticArrayInitTypeSizeU3D10_t894410179& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=10
extern "C" void __StaticArrayInitTypeSizeU3D10_t894410179_marshal_pinvoke_cleanup(__StaticArrayInitTypeSizeU3D10_t894410179_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=10
extern "C" void __StaticArrayInitTypeSizeU3D10_t894410179_marshal_com(const __StaticArrayInitTypeSizeU3D10_t894410179& unmarshaled, __StaticArrayInitTypeSizeU3D10_t894410179_marshaled_com& marshaled)
{
}
extern "C" void __StaticArrayInitTypeSizeU3D10_t894410179_marshal_com_back(const __StaticArrayInitTypeSizeU3D10_t894410179_marshaled_com& marshaled, __StaticArrayInitTypeSizeU3D10_t894410179& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=10
extern "C" void __StaticArrayInitTypeSizeU3D10_t894410179_marshal_com_cleanup(__StaticArrayInitTypeSizeU3D10_t894410179_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=1024
extern "C" void __StaticArrayInitTypeSizeU3D1024_t534724421_marshal_pinvoke(const __StaticArrayInitTypeSizeU3D1024_t534724421& unmarshaled, __StaticArrayInitTypeSizeU3D1024_t534724421_marshaled_pinvoke& marshaled)
{
}
extern "C" void __StaticArrayInitTypeSizeU3D1024_t534724421_marshal_pinvoke_back(const __StaticArrayInitTypeSizeU3D1024_t534724421_marshaled_pinvoke& marshaled, __StaticArrayInitTypeSizeU3D1024_t534724421& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=1024
extern "C" void __StaticArrayInitTypeSizeU3D1024_t534724421_marshal_pinvoke_cleanup(__StaticArrayInitTypeSizeU3D1024_t534724421_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=1024
extern "C" void __StaticArrayInitTypeSizeU3D1024_t534724421_marshal_com(const __StaticArrayInitTypeSizeU3D1024_t534724421& unmarshaled, __StaticArrayInitTypeSizeU3D1024_t534724421_marshaled_com& marshaled)
{
}
extern "C" void __StaticArrayInitTypeSizeU3D1024_t534724421_marshal_com_back(const __StaticArrayInitTypeSizeU3D1024_t534724421_marshaled_com& marshaled, __StaticArrayInitTypeSizeU3D1024_t534724421& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=1024
extern "C" void __StaticArrayInitTypeSizeU3D1024_t534724421_marshal_com_cleanup(__StaticArrayInitTypeSizeU3D1024_t534724421_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=116
extern "C" void __StaticArrayInitTypeSizeU3D116_t1956911858_marshal_pinvoke(const __StaticArrayInitTypeSizeU3D116_t1956911858& unmarshaled, __StaticArrayInitTypeSizeU3D116_t1956911858_marshaled_pinvoke& marshaled)
{
}
extern "C" void __StaticArrayInitTypeSizeU3D116_t1956911858_marshal_pinvoke_back(const __StaticArrayInitTypeSizeU3D116_t1956911858_marshaled_pinvoke& marshaled, __StaticArrayInitTypeSizeU3D116_t1956911858& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=116
extern "C" void __StaticArrayInitTypeSizeU3D116_t1956911858_marshal_pinvoke_cleanup(__StaticArrayInitTypeSizeU3D116_t1956911858_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=116
extern "C" void __StaticArrayInitTypeSizeU3D116_t1956911858_marshal_com(const __StaticArrayInitTypeSizeU3D116_t1956911858& unmarshaled, __StaticArrayInitTypeSizeU3D116_t1956911858_marshaled_com& marshaled)
{
}
extern "C" void __StaticArrayInitTypeSizeU3D116_t1956911858_marshal_com_back(const __StaticArrayInitTypeSizeU3D116_t1956911858_marshaled_com& marshaled, __StaticArrayInitTypeSizeU3D116_t1956911858& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=116
extern "C" void __StaticArrayInitTypeSizeU3D116_t1956911858_marshal_com_cleanup(__StaticArrayInitTypeSizeU3D116_t1956911858_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=12
extern "C" void __StaticArrayInitTypeSizeU3D12_t894410181_marshal_pinvoke(const __StaticArrayInitTypeSizeU3D12_t894410181& unmarshaled, __StaticArrayInitTypeSizeU3D12_t894410181_marshaled_pinvoke& marshaled)
{
}
extern "C" void __StaticArrayInitTypeSizeU3D12_t894410181_marshal_pinvoke_back(const __StaticArrayInitTypeSizeU3D12_t894410181_marshaled_pinvoke& marshaled, __StaticArrayInitTypeSizeU3D12_t894410181& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=12
extern "C" void __StaticArrayInitTypeSizeU3D12_t894410181_marshal_pinvoke_cleanup(__StaticArrayInitTypeSizeU3D12_t894410181_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=12
extern "C" void __StaticArrayInitTypeSizeU3D12_t894410181_marshal_com(const __StaticArrayInitTypeSizeU3D12_t894410181& unmarshaled, __StaticArrayInitTypeSizeU3D12_t894410181_marshaled_com& marshaled)
{
}
extern "C" void __StaticArrayInitTypeSizeU3D12_t894410181_marshal_com_back(const __StaticArrayInitTypeSizeU3D12_t894410181_marshaled_com& marshaled, __StaticArrayInitTypeSizeU3D12_t894410181& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=12
extern "C" void __StaticArrayInitTypeSizeU3D12_t894410181_marshal_com_cleanup(__StaticArrayInitTypeSizeU3D12_t894410181_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=120
extern "C" void __StaticArrayInitTypeSizeU3D120_t1956911883_marshal_pinvoke(const __StaticArrayInitTypeSizeU3D120_t1956911883& unmarshaled, __StaticArrayInitTypeSizeU3D120_t1956911883_marshaled_pinvoke& marshaled)
{
}
extern "C" void __StaticArrayInitTypeSizeU3D120_t1956911883_marshal_pinvoke_back(const __StaticArrayInitTypeSizeU3D120_t1956911883_marshaled_pinvoke& marshaled, __StaticArrayInitTypeSizeU3D120_t1956911883& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=120
extern "C" void __StaticArrayInitTypeSizeU3D120_t1956911883_marshal_pinvoke_cleanup(__StaticArrayInitTypeSizeU3D120_t1956911883_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=120
extern "C" void __StaticArrayInitTypeSizeU3D120_t1956911883_marshal_com(const __StaticArrayInitTypeSizeU3D120_t1956911883& unmarshaled, __StaticArrayInitTypeSizeU3D120_t1956911883_marshaled_com& marshaled)
{
}
extern "C" void __StaticArrayInitTypeSizeU3D120_t1956911883_marshal_com_back(const __StaticArrayInitTypeSizeU3D120_t1956911883_marshaled_com& marshaled, __StaticArrayInitTypeSizeU3D120_t1956911883& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=120
extern "C" void __StaticArrayInitTypeSizeU3D120_t1956911883_marshal_com_cleanup(__StaticArrayInitTypeSizeU3D120_t1956911883_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=16
extern "C" void __StaticArrayInitTypeSizeU3D16_t894410185_marshal_pinvoke(const __StaticArrayInitTypeSizeU3D16_t894410185& unmarshaled, __StaticArrayInitTypeSizeU3D16_t894410185_marshaled_pinvoke& marshaled)
{
}
extern "C" void __StaticArrayInitTypeSizeU3D16_t894410185_marshal_pinvoke_back(const __StaticArrayInitTypeSizeU3D16_t894410185_marshaled_pinvoke& marshaled, __StaticArrayInitTypeSizeU3D16_t894410185& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=16
extern "C" void __StaticArrayInitTypeSizeU3D16_t894410185_marshal_pinvoke_cleanup(__StaticArrayInitTypeSizeU3D16_t894410185_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=16
extern "C" void __StaticArrayInitTypeSizeU3D16_t894410185_marshal_com(const __StaticArrayInitTypeSizeU3D16_t894410185& unmarshaled, __StaticArrayInitTypeSizeU3D16_t894410185_marshaled_com& marshaled)
{
}
extern "C" void __StaticArrayInitTypeSizeU3D16_t894410185_marshal_com_back(const __StaticArrayInitTypeSizeU3D16_t894410185_marshaled_com& marshaled, __StaticArrayInitTypeSizeU3D16_t894410185& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=16
extern "C" void __StaticArrayInitTypeSizeU3D16_t894410185_marshal_com_cleanup(__StaticArrayInitTypeSizeU3D16_t894410185_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=40
extern "C" void __StaticArrayInitTypeSizeU3D40_t894410272_marshal_pinvoke(const __StaticArrayInitTypeSizeU3D40_t894410272& unmarshaled, __StaticArrayInitTypeSizeU3D40_t894410272_marshaled_pinvoke& marshaled)
{
}
extern "C" void __StaticArrayInitTypeSizeU3D40_t894410272_marshal_pinvoke_back(const __StaticArrayInitTypeSizeU3D40_t894410272_marshaled_pinvoke& marshaled, __StaticArrayInitTypeSizeU3D40_t894410272& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=40
extern "C" void __StaticArrayInitTypeSizeU3D40_t894410272_marshal_pinvoke_cleanup(__StaticArrayInitTypeSizeU3D40_t894410272_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=40
extern "C" void __StaticArrayInitTypeSizeU3D40_t894410272_marshal_com(const __StaticArrayInitTypeSizeU3D40_t894410272& unmarshaled, __StaticArrayInitTypeSizeU3D40_t894410272_marshaled_com& marshaled)
{
}
extern "C" void __StaticArrayInitTypeSizeU3D40_t894410272_marshal_com_back(const __StaticArrayInitTypeSizeU3D40_t894410272_marshaled_com& marshaled, __StaticArrayInitTypeSizeU3D40_t894410272& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=40
extern "C" void __StaticArrayInitTypeSizeU3D40_t894410272_marshal_com_cleanup(__StaticArrayInitTypeSizeU3D40_t894410272_marshaled_com& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=76
extern "C" void __StaticArrayInitTypeSizeU3D76_t894410371_marshal_pinvoke(const __StaticArrayInitTypeSizeU3D76_t894410371& unmarshaled, __StaticArrayInitTypeSizeU3D76_t894410371_marshaled_pinvoke& marshaled)
{
}
extern "C" void __StaticArrayInitTypeSizeU3D76_t894410371_marshal_pinvoke_back(const __StaticArrayInitTypeSizeU3D76_t894410371_marshaled_pinvoke& marshaled, __StaticArrayInitTypeSizeU3D76_t894410371& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=76
extern "C" void __StaticArrayInitTypeSizeU3D76_t894410371_marshal_pinvoke_cleanup(__StaticArrayInitTypeSizeU3D76_t894410371_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=76
extern "C" void __StaticArrayInitTypeSizeU3D76_t894410371_marshal_com(const __StaticArrayInitTypeSizeU3D76_t894410371& unmarshaled, __StaticArrayInitTypeSizeU3D76_t894410371_marshaled_com& marshaled)
{
}
extern "C" void __StaticArrayInitTypeSizeU3D76_t894410371_marshal_com_back(const __StaticArrayInitTypeSizeU3D76_t894410371_marshaled_com& marshaled, __StaticArrayInitTypeSizeU3D76_t894410371& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: <PrivateImplementationDetails>{8E8FA28D-216A-43EC-8DCB-2258D1F7BF00}/__StaticArrayInitTypeSize=76
extern "C" void __StaticArrayInitTypeSizeU3D76_t894410371_marshal_com_cleanup(__StaticArrayInitTypeSizeU3D76_t894410371_marshaled_com& marshaled)
{
}
// System.Int64 ICSharpCode.SharpZipLib.Checksums.Adler32::get_Value()
extern "C"  int64_t Adler32_get_Value_m2923380521 (Adler32_t2680377483 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_checksum_0();
		return (((int64_t)((uint64_t)L_0)));
	}
}
// System.Void ICSharpCode.SharpZipLib.Checksums.Adler32::.ctor()
extern "C"  void Adler32__ctor_m2455030518 (Adler32_t2680377483 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Adler32_Reset_m101463459(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Checksums.Adler32::Reset()
extern "C"  void Adler32_Reset_m101463459 (Adler32_t2680377483 * __this, const MethodInfo* method)
{
	{
		__this->set_checksum_0(1);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Checksums.Adler32::Update(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2916848704;
extern Il2CppCodeGenString* _stringLiteral3275187347;
extern Il2CppCodeGenString* _stringLiteral913675221;
extern Il2CppCodeGenString* _stringLiteral94851343;
extern Il2CppCodeGenString* _stringLiteral3848898242;
extern Il2CppCodeGenString* _stringLiteral25959552;
extern const uint32_t Adler32_Update_m2142069042_MetadataUsageId;
extern "C"  void Adler32_Update_m2142069042 (Adler32_t2680377483 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Adler32_Update_m2142069042_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		ByteU5BU5D_t4260760469* L_0 = ___buffer0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral2916848704, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		int32_t L_2 = ___offset1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0022;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1193970951(L_3, _stringLiteral3275187347, _stringLiteral913675221, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		int32_t L_4 = ___count2;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_5 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1193970951(L_5, _stringLiteral94851343, _stringLiteral913675221, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0036:
	{
		int32_t L_6 = ___offset1;
		ByteU5BU5D_t4260760469* L_7 = ___buffer0;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_004c;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_8 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1193970951(L_8, _stringLiteral3275187347, _stringLiteral3848898242, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_004c:
	{
		int32_t L_9 = ___offset1;
		int32_t L_10 = ___count2;
		ByteU5BU5D_t4260760469* L_11 = ___buffer0;
		NullCheck(L_11);
		if ((((int32_t)((int32_t)((int32_t)L_9+(int32_t)L_10))) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0064;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_12 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1193970951(L_12, _stringLiteral94851343, _stringLiteral25959552, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_12);
	}

IL_0064:
	{
		uint32_t L_13 = __this->get_checksum_0();
		V_0 = ((int32_t)((int32_t)L_13&(int32_t)((int32_t)65535)));
		uint32_t L_14 = __this->get_checksum_0();
		V_1 = ((int32_t)((uint32_t)L_14>>((int32_t)16)));
		goto IL_00bd;
	}

IL_007d:
	{
		V_2 = ((int32_t)3800);
		int32_t L_15 = V_2;
		int32_t L_16 = ___count2;
		if ((((int32_t)L_15) <= ((int32_t)L_16)))
		{
			goto IL_0089;
		}
	}
	{
		int32_t L_17 = ___count2;
		V_2 = L_17;
	}

IL_0089:
	{
		int32_t L_18 = ___count2;
		int32_t L_19 = V_2;
		___count2 = ((int32_t)((int32_t)L_18-(int32_t)L_19));
		goto IL_00a5;
	}

IL_0090:
	{
		uint32_t L_20 = V_0;
		ByteU5BU5D_t4260760469* L_21 = ___buffer0;
		int32_t L_22 = ___offset1;
		int32_t L_23 = L_22;
		___offset1 = ((int32_t)((int32_t)L_23+(int32_t)1));
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_23);
		int32_t L_24 = L_23;
		uint8_t L_25 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		V_0 = ((int32_t)((int32_t)L_20+(int32_t)((int32_t)((int32_t)L_25&(int32_t)((int32_t)255)))));
		uint32_t L_26 = V_1;
		uint32_t L_27 = V_0;
		V_1 = ((int32_t)((int32_t)L_26+(int32_t)L_27));
	}

IL_00a5:
	{
		int32_t L_28 = V_2;
		int32_t L_29 = ((int32_t)((int32_t)L_28-(int32_t)1));
		V_2 = L_29;
		if ((((int32_t)L_29) >= ((int32_t)0)))
		{
			goto IL_0090;
		}
	}
	{
		uint32_t L_30 = V_0;
		V_0 = ((int32_t)((uint32_t)(int32_t)L_30%(uint32_t)(int32_t)((int32_t)65521)));
		uint32_t L_31 = V_1;
		V_1 = ((int32_t)((uint32_t)(int32_t)L_31%(uint32_t)(int32_t)((int32_t)65521)));
	}

IL_00bd:
	{
		int32_t L_32 = ___count2;
		if ((((int32_t)L_32) > ((int32_t)0)))
		{
			goto IL_007d;
		}
	}
	{
		uint32_t L_33 = V_1;
		uint32_t L_34 = V_0;
		__this->set_checksum_0(((int32_t)((int32_t)((int32_t)((int32_t)L_33<<(int32_t)((int32_t)16)))|(int32_t)L_34)));
		return;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Checksums.Crc32::get_Value()
extern "C"  int64_t Crc32_get_Value_m2535696875 (Crc32_t3523361801 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_crc_1();
		return (((int64_t)((uint64_t)L_0)));
	}
}
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::Update(System.Int32)
extern Il2CppClass* Crc32_t3523361801_il2cpp_TypeInfo_var;
extern const uint32_t Crc32_Update_m3645794406_MetadataUsageId;
extern "C"  void Crc32_Update_m3645794406 (Crc32_t3523361801 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Crc32_Update_m3645794406_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint32_t L_0 = __this->get_crc_1();
		__this->set_crc_1(((int32_t)((int32_t)L_0^(int32_t)(-1))));
		IL2CPP_RUNTIME_CLASS_INIT(Crc32_t3523361801_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t3230734560* L_1 = ((Crc32_t3523361801_StaticFields*)Crc32_t3523361801_il2cpp_TypeInfo_var->static_fields)->get_CrcTable_0();
		uint32_t L_2 = __this->get_crc_1();
		int32_t L_3 = ___value0;
		if ((int64_t)(((int64_t)((int64_t)((int64_t)((int64_t)(((int64_t)((uint64_t)L_2)))^(int64_t)(((int64_t)((int64_t)L_3)))))&(int64_t)(((int64_t)((int64_t)((int32_t)255))))))) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception());
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, (il2cpp_array_size_t)(uintptr_t)(((intptr_t)((int64_t)((int64_t)((int64_t)((int64_t)(((int64_t)((uint64_t)L_2)))^(int64_t)(((int64_t)((int64_t)L_3)))))&(int64_t)(((int64_t)((int64_t)((int32_t)255)))))))));
		intptr_t L_4 = (((intptr_t)((int64_t)((int64_t)((int64_t)((int64_t)(((int64_t)((uint64_t)L_2)))^(int64_t)(((int64_t)((int64_t)L_3)))))&(int64_t)(((int64_t)((int64_t)((int32_t)255))))))));
		uint32_t L_5 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		uint32_t L_6 = __this->get_crc_1();
		__this->set_crc_1(((int32_t)((int32_t)L_5^(int32_t)((int32_t)((uint32_t)L_6>>8)))));
		uint32_t L_7 = __this->get_crc_1();
		__this->set_crc_1(((int32_t)((int32_t)L_7^(int32_t)(-1))));
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::Update(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppClass* Crc32_t3523361801_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2916848704;
extern Il2CppCodeGenString* _stringLiteral94851343;
extern Il2CppCodeGenString* _stringLiteral2840363441;
extern Il2CppCodeGenString* _stringLiteral3275187347;
extern const uint32_t Crc32_Update_m3381543860_MetadataUsageId;
extern "C"  void Crc32_Update_m3381543860 (Crc32_t3523361801 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Crc32_Update_m3381543860_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t4260760469* L_0 = ___buffer0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral2916848704, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		int32_t L_2 = ___count2;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0022;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1193970951(L_3, _stringLiteral94851343, _stringLiteral2840363441, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		int32_t L_4 = ___offset1;
		if ((((int32_t)L_4) < ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_5 = ___offset1;
		int32_t L_6 = ___count2;
		ByteU5BU5D_t4260760469* L_7 = ___buffer0;
		NullCheck(L_7);
		if ((((int32_t)((int32_t)((int32_t)L_5+(int32_t)L_6))) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_7)->max_length)))))))
		{
			goto IL_0039;
		}
	}

IL_002e:
	{
		ArgumentOutOfRangeException_t3816648464 * L_8 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_8, _stringLiteral3275187347, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0039:
	{
		uint32_t L_9 = __this->get_crc_1();
		__this->set_crc_1(((int32_t)((int32_t)L_9^(int32_t)(-1))));
		goto IL_0074;
	}

IL_0049:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Crc32_t3523361801_il2cpp_TypeInfo_var);
		UInt32U5BU5D_t3230734560* L_10 = ((Crc32_t3523361801_StaticFields*)Crc32_t3523361801_il2cpp_TypeInfo_var->static_fields)->get_CrcTable_0();
		uint32_t L_11 = __this->get_crc_1();
		ByteU5BU5D_t4260760469* L_12 = ___buffer0;
		int32_t L_13 = ___offset1;
		int32_t L_14 = L_13;
		___offset1 = ((int32_t)((int32_t)L_14+(int32_t)1));
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_14);
		int32_t L_15 = L_14;
		uint8_t L_16 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, (il2cpp_array_size_t)(uintptr_t)(((uintptr_t)((int32_t)((int32_t)((int32_t)((int32_t)L_11^(int32_t)L_16))&(int32_t)((int32_t)255))))));
		uintptr_t L_17 = (((uintptr_t)((int32_t)((int32_t)((int32_t)((int32_t)L_11^(int32_t)L_16))&(int32_t)((int32_t)255)))));
		uint32_t L_18 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		uint32_t L_19 = __this->get_crc_1();
		__this->set_crc_1(((int32_t)((int32_t)L_18^(int32_t)((int32_t)((uint32_t)L_19>>8)))));
	}

IL_0074:
	{
		int32_t L_20 = ___count2;
		int32_t L_21 = ((int32_t)((int32_t)L_20-(int32_t)1));
		___count2 = L_21;
		if ((((int32_t)L_21) >= ((int32_t)0)))
		{
			goto IL_0049;
		}
	}
	{
		uint32_t L_22 = __this->get_crc_1();
		__this->set_crc_1(((int32_t)((int32_t)L_22^(int32_t)(-1))));
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::.ctor()
extern "C"  void Crc32__ctor_m1138481016 (Crc32_t3523361801 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Checksums.Crc32::.cctor()
extern Il2CppClass* UInt32U5BU5D_t3230734560_il2cpp_TypeInfo_var;
extern Il2CppClass* Crc32_t3523361801_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004f9U2D1_0_FieldInfo_var;
extern const uint32_t Crc32__cctor_m451076917_MetadataUsageId;
extern "C"  void Crc32__cctor_m451076917 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Crc32__cctor_m451076917_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		UInt32U5BU5D_t3230734560* L_0 = ((UInt32U5BU5D_t3230734560*)SZArrayNew(UInt32U5BU5D_t3230734560_il2cpp_TypeInfo_var, (uint32_t)((int32_t)256)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004f9U2D1_0_FieldInfo_var), /*hidden argument*/NULL);
		((Crc32_t3523361801_StaticFields*)Crc32_t3523361801_il2cpp_TypeInfo_var->static_fields)->set_CrcTable_0(L_0);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32)
extern Il2CppClass* ICryptoTransform_t153068654_il2cpp_TypeInfo_var;
extern const uint32_t ZipAESTransform_TransformBlock_m201067254_MetadataUsageId;
extern "C"  int32_t ZipAESTransform_TransformBlock_m201067254 (ZipAESTransform_t496812608 * __this, ByteU5BU5D_t4260760469* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, ByteU5BU5D_t4260760469* ___outputBuffer3, int32_t ___outputOffset4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ZipAESTransform_TransformBlock_m201067254_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	uint8_t V_2 = 0x0;
	int32_t V_3 = 0;
	{
		bool L_0 = __this->get__writeMode_7();
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		HMACSHA1_t4024365272 * L_1 = __this->get__hmacsha1_5();
		ByteU5BU5D_t4260760469* L_2 = ___inputBuffer0;
		int32_t L_3 = ___inputOffset1;
		int32_t L_4 = ___inputCount2;
		ByteU5BU5D_t4260760469* L_5 = ___inputBuffer0;
		int32_t L_6 = ___inputOffset1;
		NullCheck(L_1);
		HashAlgorithm_TransformBlock_m3829135048(L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
	}

IL_0019:
	{
		V_0 = 0;
		goto IL_00a2;
	}

IL_0020:
	{
		int32_t L_7 = __this->get__encrPos_4();
		if ((!(((uint32_t)L_7) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_0078;
		}
	}
	{
		V_1 = 0;
		goto IL_0032;
	}

IL_002e:
	{
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0032:
	{
		ByteU5BU5D_t4260760469* L_9 = __this->get__counterNonce_2();
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		uint8_t* L_11 = ((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_10)));
		int32_t L_12 = (((int32_t)((uint8_t)((int32_t)((int32_t)(*(uint8_t*)L_11)+(int32_t)1)))));
		V_2 = L_12;
		(*(uint8_t*)L_11) = L_12;
		uint8_t L_13 = V_2;
		if (!L_13)
		{
			goto IL_002e;
		}
	}
	{
		Il2CppObject * L_14 = __this->get__encryptor_1();
		ByteU5BU5D_t4260760469* L_15 = __this->get__counterNonce_2();
		int32_t L_16 = __this->get__blockSize_0();
		ByteU5BU5D_t4260760469* L_17 = __this->get__encryptBuffer_3();
		NullCheck(L_14);
		InterfaceFuncInvoker5< int32_t, ByteU5BU5D_t4260760469*, int32_t, int32_t, ByteU5BU5D_t4260760469*, int32_t >::Invoke(1 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t153068654_il2cpp_TypeInfo_var, L_14, L_15, 0, L_16, L_17, 0);
		__this->set__encrPos_4(0);
	}

IL_0078:
	{
		ByteU5BU5D_t4260760469* L_18 = ___outputBuffer3;
		int32_t L_19 = V_0;
		int32_t L_20 = ___outputOffset4;
		ByteU5BU5D_t4260760469* L_21 = ___inputBuffer0;
		int32_t L_22 = V_0;
		int32_t L_23 = ___inputOffset1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, ((int32_t)((int32_t)L_22+(int32_t)L_23)));
		int32_t L_24 = ((int32_t)((int32_t)L_22+(int32_t)L_23));
		uint8_t L_25 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		ByteU5BU5D_t4260760469* L_26 = __this->get__encryptBuffer_3();
		int32_t L_27 = __this->get__encrPos_4();
		int32_t L_28 = L_27;
		V_3 = L_28;
		__this->set__encrPos_4(((int32_t)((int32_t)L_28+(int32_t)1)));
		int32_t L_29 = V_3;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_29);
		int32_t L_30 = L_29;
		uint8_t L_31 = (L_26)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19+(int32_t)L_20)));
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_19+(int32_t)L_20))), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_25^(int32_t)L_31))))));
		int32_t L_32 = V_0;
		V_0 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_00a2:
	{
		int32_t L_33 = V_0;
		int32_t L_34 = ___inputCount2;
		if ((((int32_t)L_33) < ((int32_t)L_34)))
		{
			goto IL_0020;
		}
	}
	{
		bool L_35 = __this->get__writeMode_7();
		if (!L_35)
		{
			goto IL_00c6;
		}
	}
	{
		HMACSHA1_t4024365272 * L_36 = __this->get__hmacsha1_5();
		ByteU5BU5D_t4260760469* L_37 = ___outputBuffer3;
		int32_t L_38 = ___outputOffset4;
		int32_t L_39 = ___inputCount2;
		ByteU5BU5D_t4260760469* L_40 = ___outputBuffer3;
		int32_t L_41 = ___outputOffset4;
		NullCheck(L_36);
		HashAlgorithm_TransformBlock_m3829135048(L_36, L_37, L_38, L_39, L_40, L_41, /*hidden argument*/NULL);
	}

IL_00c6:
	{
		int32_t L_42 = ___inputCount2;
		return L_42;
	}
}
// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::GetAuthCode()
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern const uint32_t ZipAESTransform_GetAuthCode_m1195602789_MetadataUsageId;
extern "C"  ByteU5BU5D_t4260760469* ZipAESTransform_GetAuthCode_m1195602789 (ZipAESTransform_t496812608 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ZipAESTransform_GetAuthCode_m1195602789_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	{
		bool L_0 = __this->get__finalised_6();
		if (L_0)
		{
			goto IL_0025;
		}
	}
	{
		V_0 = ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)0));
		HMACSHA1_t4024365272 * L_1 = __this->get__hmacsha1_5();
		ByteU5BU5D_t4260760469* L_2 = V_0;
		NullCheck(L_1);
		HashAlgorithm_TransformFinalBlock_m1193599374(L_1, L_2, 0, 0, /*hidden argument*/NULL);
		__this->set__finalised_6((bool)1);
	}

IL_0025:
	{
		HMACSHA1_t4024365272 * L_3 = __this->get__hmacsha1_5();
		NullCheck(L_3);
		ByteU5BU5D_t4260760469* L_4 = VirtFuncInvoker0< ByteU5BU5D_t4260760469* >::Invoke(9 /* System.Byte[] System.Security.Cryptography.HashAlgorithm::get_Hash() */, L_3);
		return L_4;
	}
}
// System.Byte[] ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::TransformFinalBlock(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* NotImplementedException_t1912495542_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral16361523;
extern const uint32_t ZipAESTransform_TransformFinalBlock_m3809853644_MetadataUsageId;
extern "C"  ByteU5BU5D_t4260760469* ZipAESTransform_TransformFinalBlock_m3809853644 (ZipAESTransform_t496812608 * __this, ByteU5BU5D_t4260760469* ___inputBuffer0, int32_t ___inputOffset1, int32_t ___inputCount2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ZipAESTransform_TransformFinalBlock_m3809853644_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotImplementedException_t1912495542 * L_0 = (NotImplementedException_t1912495542 *)il2cpp_codegen_object_new(NotImplementedException_t1912495542_il2cpp_TypeInfo_var);
		NotImplementedException__ctor_m495190705(L_0, _stringLiteral16361523, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::get_CanReuseTransform()
extern "C"  bool ZipAESTransform_get_CanReuseTransform_m2202231249 (ZipAESTransform_t496812608 * __this, const MethodInfo* method)
{
	{
		return (bool)1;
	}
}
// System.Void ICSharpCode.SharpZipLib.Encryption.ZipAESTransform::Dispose()
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t ZipAESTransform_Dispose_m1286558275_MetadataUsageId;
extern "C"  void ZipAESTransform_Dispose_m1286558275 (ZipAESTransform_t496812608 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ZipAESTransform_Dispose_m1286558275_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get__encryptor_1();
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_0);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.GZip.GZipException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void GZipException__ctor_m877423801 (GZipException_t2414750427 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t2185721892 * L_0 = ___info0;
		StreamingContext_t2761351129  L_1 = ___context1;
		SharpZipBaseException__ctor_m3034235335(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.GZip.GZipException::.ctor()
extern "C"  void GZipException__ctor_m4154838904 (GZipException_t2414750427 * __this, const MethodInfo* method)
{
	{
		SharpZipBaseException__ctor_m730469766(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.GZip.GZipException::.ctor(System.String)
extern "C"  void GZipException__ctor_m1759163018 (GZipException_t2414750427 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		SharpZipBaseException__ctor_m2797155132(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.GZip.GZipInputStream::.ctor(System.IO.Stream)
extern "C"  void GZipInputStream__ctor_m1410043348 (GZipInputStream_t2489725366 * __this, Stream_t1561764144 * ___baseInputStream0, const MethodInfo* method)
{
	{
		Stream_t1561764144 * L_0 = ___baseInputStream0;
		GZipInputStream__ctor_m4172261251(__this, L_0, ((int32_t)4096), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.GZip.GZipInputStream::.ctor(System.IO.Stream,System.Int32)
extern Il2CppClass* Inflater_t1975778921_il2cpp_TypeInfo_var;
extern const uint32_t GZipInputStream__ctor_m4172261251_MetadataUsageId;
extern "C"  void GZipInputStream__ctor_m4172261251 (GZipInputStream_t2489725366 * __this, Stream_t1561764144 * ___baseInputStream0, int32_t ___size1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GZipInputStream__ctor_m4172261251_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Stream_t1561764144 * L_0 = ___baseInputStream0;
		Inflater_t1975778921 * L_1 = (Inflater_t1975778921 *)il2cpp_codegen_object_new(Inflater_t1975778921_il2cpp_TypeInfo_var);
		Inflater__ctor_m422624816(L_1, (bool)1, /*hidden argument*/NULL);
		int32_t L_2 = ___size1;
		InflaterInputStream__ctor_m1038306487(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.GZip.GZipInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t GZipInputStream_Read_m562175424 (GZipInputStream_t2489725366 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	int32_t V_0 = 0;

IL_0000:
	{
		bool L_0 = __this->get_readGZIPHeader_7();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		bool L_1 = GZipInputStream_ReadHeader_m2762920156(__this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		return 0;
	}

IL_0012:
	{
		ByteU5BU5D_t4260760469* L_2 = ___buffer0;
		int32_t L_3 = ___offset1;
		int32_t L_4 = ___count2;
		int32_t L_5 = InflaterInputStream_Read_m13213145(__this, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		Crc32_t3523361801 * L_7 = __this->get_crc_6();
		ByteU5BU5D_t4260760469* L_8 = ___buffer0;
		int32_t L_9 = ___offset1;
		int32_t L_10 = V_0;
		NullCheck(L_7);
		Crc32_Update_m3381543860(L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
	}

IL_002e:
	{
		Inflater_t1975778921 * L_11 = ((InflaterInputStream_t928059325 *)__this)->get_inf_1();
		NullCheck(L_11);
		bool L_12 = Inflater_get_IsFinished_m3219719202(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0041;
		}
	}
	{
		GZipInputStream_ReadFooter_m3020263032(__this, /*hidden argument*/NULL);
	}

IL_0041:
	{
		int32_t L_13 = V_0;
		if ((((int32_t)L_13) <= ((int32_t)0)))
		{
			goto IL_0000;
		}
	}
	{
		int32_t L_14 = V_0;
		return L_14;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.GZip.GZipInputStream::ReadHeader()
extern Il2CppClass* Crc32_t3523361801_il2cpp_TypeInfo_var;
extern Il2CppClass* EndOfStreamException_t3956009005_il2cpp_TypeInfo_var;
extern Il2CppClass* GZipException_t2414750427_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1905238728;
extern Il2CppCodeGenString* _stringLiteral1865426209;
extern Il2CppCodeGenString* _stringLiteral3984301053;
extern Il2CppCodeGenString* _stringLiteral2326129075;
extern Il2CppCodeGenString* _stringLiteral2838091798;
extern Il2CppCodeGenString* _stringLiteral213654492;
extern const uint32_t GZipInputStream_ReadHeader_m2762920156_MetadataUsageId;
extern "C"  bool GZipInputStream_ReadHeader_m2762920156 (GZipInputStream_t2489725366 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GZipInputStream_ReadHeader_m2762920156_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Crc32_t3523361801 * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	{
		Crc32_t3523361801 * L_0 = (Crc32_t3523361801 *)il2cpp_codegen_object_new(Crc32_t3523361801_il2cpp_TypeInfo_var);
		Crc32__ctor_m1138481016(L_0, /*hidden argument*/NULL);
		__this->set_crc_6(L_0);
		InflaterInputBuffer_t441930877 * L_1 = ((InflaterInputStream_t928059325 *)__this)->get_inputBuffer_2();
		NullCheck(L_1);
		int32_t L_2 = InflaterInputBuffer_get_Available_m3610562184(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) > ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		InflaterInputBuffer_t441930877 * L_3 = ((InflaterInputStream_t928059325 *)__this)->get_inputBuffer_2();
		NullCheck(L_3);
		InflaterInputBuffer_Fill_m1169415305(L_3, /*hidden argument*/NULL);
		InflaterInputBuffer_t441930877 * L_4 = ((InflaterInputStream_t928059325 *)__this)->get_inputBuffer_2();
		NullCheck(L_4);
		int32_t L_5 = InflaterInputBuffer_get_Available_m3610562184(L_4, /*hidden argument*/NULL);
		if ((((int32_t)L_5) > ((int32_t)0)))
		{
			goto IL_0034;
		}
	}
	{
		return (bool)0;
	}

IL_0034:
	{
		Crc32_t3523361801 * L_6 = (Crc32_t3523361801 *)il2cpp_codegen_object_new(Crc32_t3523361801_il2cpp_TypeInfo_var);
		Crc32__ctor_m1138481016(L_6, /*hidden argument*/NULL);
		V_0 = L_6;
		InflaterInputBuffer_t441930877 * L_7 = ((InflaterInputStream_t928059325 *)__this)->get_inputBuffer_2();
		NullCheck(L_7);
		int32_t L_8 = InflaterInputBuffer_ReadLeByte_m1881028497(L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		int32_t L_9 = V_1;
		if ((((int32_t)L_9) >= ((int32_t)0)))
		{
			goto IL_0055;
		}
	}
	{
		EndOfStreamException_t3956009005 * L_10 = (EndOfStreamException_t3956009005 *)il2cpp_codegen_object_new(EndOfStreamException_t3956009005_il2cpp_TypeInfo_var);
		EndOfStreamException__ctor_m2857110102(L_10, _stringLiteral1905238728, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_10);
	}

IL_0055:
	{
		Crc32_t3523361801 * L_11 = V_0;
		int32_t L_12 = V_1;
		NullCheck(L_11);
		Crc32_Update_m3645794406(L_11, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_1;
		if ((((int32_t)L_13) == ((int32_t)((int32_t)31))))
		{
			goto IL_006c;
		}
	}
	{
		GZipException_t2414750427 * L_14 = (GZipException_t2414750427 *)il2cpp_codegen_object_new(GZipException_t2414750427_il2cpp_TypeInfo_var);
		GZipException__ctor_m1759163018(L_14, _stringLiteral1865426209, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_006c:
	{
		InflaterInputBuffer_t441930877 * L_15 = ((InflaterInputStream_t928059325 *)__this)->get_inputBuffer_2();
		NullCheck(L_15);
		int32_t L_16 = InflaterInputBuffer_ReadLeByte_m1881028497(L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		int32_t L_17 = V_1;
		if ((((int32_t)L_17) >= ((int32_t)0)))
		{
			goto IL_0087;
		}
	}
	{
		EndOfStreamException_t3956009005 * L_18 = (EndOfStreamException_t3956009005 *)il2cpp_codegen_object_new(EndOfStreamException_t3956009005_il2cpp_TypeInfo_var);
		EndOfStreamException__ctor_m2857110102(L_18, _stringLiteral1905238728, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_18);
	}

IL_0087:
	{
		int32_t L_19 = V_1;
		if ((((int32_t)L_19) == ((int32_t)((int32_t)139))))
		{
			goto IL_009a;
		}
	}
	{
		GZipException_t2414750427 * L_20 = (GZipException_t2414750427 *)il2cpp_codegen_object_new(GZipException_t2414750427_il2cpp_TypeInfo_var);
		GZipException__ctor_m1759163018(L_20, _stringLiteral3984301053, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_20);
	}

IL_009a:
	{
		Crc32_t3523361801 * L_21 = V_0;
		int32_t L_22 = V_1;
		NullCheck(L_21);
		Crc32_Update_m3645794406(L_21, L_22, /*hidden argument*/NULL);
		InflaterInputBuffer_t441930877 * L_23 = ((InflaterInputStream_t928059325 *)__this)->get_inputBuffer_2();
		NullCheck(L_23);
		int32_t L_24 = InflaterInputBuffer_ReadLeByte_m1881028497(L_23, /*hidden argument*/NULL);
		V_2 = L_24;
		int32_t L_25 = V_2;
		if ((((int32_t)L_25) >= ((int32_t)0)))
		{
			goto IL_00bc;
		}
	}
	{
		EndOfStreamException_t3956009005 * L_26 = (EndOfStreamException_t3956009005 *)il2cpp_codegen_object_new(EndOfStreamException_t3956009005_il2cpp_TypeInfo_var);
		EndOfStreamException__ctor_m2857110102(L_26, _stringLiteral1905238728, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_26);
	}

IL_00bc:
	{
		int32_t L_27 = V_2;
		if ((((int32_t)L_27) == ((int32_t)8)))
		{
			goto IL_00cb;
		}
	}
	{
		GZipException_t2414750427 * L_28 = (GZipException_t2414750427 *)il2cpp_codegen_object_new(GZipException_t2414750427_il2cpp_TypeInfo_var);
		GZipException__ctor_m1759163018(L_28, _stringLiteral2326129075, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_28);
	}

IL_00cb:
	{
		Crc32_t3523361801 * L_29 = V_0;
		int32_t L_30 = V_2;
		NullCheck(L_29);
		Crc32_Update_m3645794406(L_29, L_30, /*hidden argument*/NULL);
		InflaterInputBuffer_t441930877 * L_31 = ((InflaterInputStream_t928059325 *)__this)->get_inputBuffer_2();
		NullCheck(L_31);
		int32_t L_32 = InflaterInputBuffer_ReadLeByte_m1881028497(L_31, /*hidden argument*/NULL);
		V_3 = L_32;
		int32_t L_33 = V_3;
		if ((((int32_t)L_33) >= ((int32_t)0)))
		{
			goto IL_00ed;
		}
	}
	{
		EndOfStreamException_t3956009005 * L_34 = (EndOfStreamException_t3956009005 *)il2cpp_codegen_object_new(EndOfStreamException_t3956009005_il2cpp_TypeInfo_var);
		EndOfStreamException__ctor_m2857110102(L_34, _stringLiteral1905238728, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_34);
	}

IL_00ed:
	{
		Crc32_t3523361801 * L_35 = V_0;
		int32_t L_36 = V_3;
		NullCheck(L_35);
		Crc32_Update_m3645794406(L_35, L_36, /*hidden argument*/NULL);
		int32_t L_37 = V_3;
		if (!((int32_t)((int32_t)L_37&(int32_t)((int32_t)224))))
		{
			goto IL_0108;
		}
	}
	{
		GZipException_t2414750427 * L_38 = (GZipException_t2414750427 *)il2cpp_codegen_object_new(GZipException_t2414750427_il2cpp_TypeInfo_var);
		GZipException__ctor_m1759163018(L_38, _stringLiteral2838091798, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_38);
	}

IL_0108:
	{
		V_4 = 0;
		goto IL_0138;
	}

IL_010d:
	{
		InflaterInputBuffer_t441930877 * L_39 = ((InflaterInputStream_t928059325 *)__this)->get_inputBuffer_2();
		NullCheck(L_39);
		int32_t L_40 = InflaterInputBuffer_ReadLeByte_m1881028497(L_39, /*hidden argument*/NULL);
		V_5 = L_40;
		int32_t L_41 = V_5;
		if ((((int32_t)L_41) >= ((int32_t)0)))
		{
			goto IL_012a;
		}
	}
	{
		EndOfStreamException_t3956009005 * L_42 = (EndOfStreamException_t3956009005 *)il2cpp_codegen_object_new(EndOfStreamException_t3956009005_il2cpp_TypeInfo_var);
		EndOfStreamException__ctor_m2857110102(L_42, _stringLiteral1905238728, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_42);
	}

IL_012a:
	{
		Crc32_t3523361801 * L_43 = V_0;
		int32_t L_44 = V_5;
		NullCheck(L_43);
		Crc32_Update_m3645794406(L_43, L_44, /*hidden argument*/NULL);
		int32_t L_45 = V_4;
		V_4 = ((int32_t)((int32_t)L_45+(int32_t)1));
	}

IL_0138:
	{
		int32_t L_46 = V_4;
		if ((((int32_t)L_46) < ((int32_t)6)))
		{
			goto IL_010d;
		}
	}
	{
		int32_t L_47 = V_3;
		if (!((int32_t)((int32_t)L_47&(int32_t)4)))
		{
			goto IL_021f;
		}
	}
	{
		V_6 = 0;
		goto IL_0175;
	}

IL_014a:
	{
		InflaterInputBuffer_t441930877 * L_48 = ((InflaterInputStream_t928059325 *)__this)->get_inputBuffer_2();
		NullCheck(L_48);
		int32_t L_49 = InflaterInputBuffer_ReadLeByte_m1881028497(L_48, /*hidden argument*/NULL);
		V_7 = L_49;
		int32_t L_50 = V_7;
		if ((((int32_t)L_50) >= ((int32_t)0)))
		{
			goto IL_0167;
		}
	}
	{
		EndOfStreamException_t3956009005 * L_51 = (EndOfStreamException_t3956009005 *)il2cpp_codegen_object_new(EndOfStreamException_t3956009005_il2cpp_TypeInfo_var);
		EndOfStreamException__ctor_m2857110102(L_51, _stringLiteral1905238728, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_51);
	}

IL_0167:
	{
		Crc32_t3523361801 * L_52 = V_0;
		int32_t L_53 = V_7;
		NullCheck(L_52);
		Crc32_Update_m3645794406(L_52, L_53, /*hidden argument*/NULL);
		int32_t L_54 = V_6;
		V_6 = ((int32_t)((int32_t)L_54+(int32_t)1));
	}

IL_0175:
	{
		int32_t L_55 = V_6;
		if ((((int32_t)L_55) < ((int32_t)2)))
		{
			goto IL_014a;
		}
	}
	{
		InflaterInputBuffer_t441930877 * L_56 = ((InflaterInputStream_t928059325 *)__this)->get_inputBuffer_2();
		NullCheck(L_56);
		int32_t L_57 = InflaterInputBuffer_ReadLeByte_m1881028497(L_56, /*hidden argument*/NULL);
		if ((((int32_t)L_57) < ((int32_t)0)))
		{
			goto IL_0196;
		}
	}
	{
		InflaterInputBuffer_t441930877 * L_58 = ((InflaterInputStream_t928059325 *)__this)->get_inputBuffer_2();
		NullCheck(L_58);
		int32_t L_59 = InflaterInputBuffer_ReadLeByte_m1881028497(L_58, /*hidden argument*/NULL);
		if ((((int32_t)L_59) >= ((int32_t)0)))
		{
			goto IL_01a1;
		}
	}

IL_0196:
	{
		EndOfStreamException_t3956009005 * L_60 = (EndOfStreamException_t3956009005 *)il2cpp_codegen_object_new(EndOfStreamException_t3956009005_il2cpp_TypeInfo_var);
		EndOfStreamException__ctor_m2857110102(L_60, _stringLiteral1905238728, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_60);
	}

IL_01a1:
	{
		InflaterInputBuffer_t441930877 * L_61 = ((InflaterInputStream_t928059325 *)__this)->get_inputBuffer_2();
		NullCheck(L_61);
		int32_t L_62 = InflaterInputBuffer_ReadLeByte_m1881028497(L_61, /*hidden argument*/NULL);
		V_8 = L_62;
		InflaterInputBuffer_t441930877 * L_63 = ((InflaterInputStream_t928059325 *)__this)->get_inputBuffer_2();
		NullCheck(L_63);
		int32_t L_64 = InflaterInputBuffer_ReadLeByte_m1881028497(L_63, /*hidden argument*/NULL);
		V_9 = L_64;
		int32_t L_65 = V_8;
		if ((((int32_t)L_65) < ((int32_t)0)))
		{
			goto IL_01c5;
		}
	}
	{
		int32_t L_66 = V_9;
		if ((((int32_t)L_66) >= ((int32_t)0)))
		{
			goto IL_01d0;
		}
	}

IL_01c5:
	{
		EndOfStreamException_t3956009005 * L_67 = (EndOfStreamException_t3956009005 *)il2cpp_codegen_object_new(EndOfStreamException_t3956009005_il2cpp_TypeInfo_var);
		EndOfStreamException__ctor_m2857110102(L_67, _stringLiteral1905238728, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_67);
	}

IL_01d0:
	{
		Crc32_t3523361801 * L_68 = V_0;
		int32_t L_69 = V_8;
		NullCheck(L_68);
		Crc32_Update_m3645794406(L_68, L_69, /*hidden argument*/NULL);
		Crc32_t3523361801 * L_70 = V_0;
		int32_t L_71 = V_9;
		NullCheck(L_70);
		Crc32_Update_m3645794406(L_70, L_71, /*hidden argument*/NULL);
		int32_t L_72 = V_8;
		int32_t L_73 = V_9;
		V_10 = ((int32_t)((int32_t)((int32_t)((int32_t)L_72<<(int32_t)8))|(int32_t)L_73));
		V_11 = 0;
		goto IL_0219;
	}

IL_01ee:
	{
		InflaterInputBuffer_t441930877 * L_74 = ((InflaterInputStream_t928059325 *)__this)->get_inputBuffer_2();
		NullCheck(L_74);
		int32_t L_75 = InflaterInputBuffer_ReadLeByte_m1881028497(L_74, /*hidden argument*/NULL);
		V_12 = L_75;
		int32_t L_76 = V_12;
		if ((((int32_t)L_76) >= ((int32_t)0)))
		{
			goto IL_020b;
		}
	}
	{
		EndOfStreamException_t3956009005 * L_77 = (EndOfStreamException_t3956009005 *)il2cpp_codegen_object_new(EndOfStreamException_t3956009005_il2cpp_TypeInfo_var);
		EndOfStreamException__ctor_m2857110102(L_77, _stringLiteral1905238728, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_77);
	}

IL_020b:
	{
		Crc32_t3523361801 * L_78 = V_0;
		int32_t L_79 = V_12;
		NullCheck(L_78);
		Crc32_Update_m3645794406(L_78, L_79, /*hidden argument*/NULL);
		int32_t L_80 = V_11;
		V_11 = ((int32_t)((int32_t)L_80+(int32_t)1));
	}

IL_0219:
	{
		int32_t L_81 = V_11;
		int32_t L_82 = V_10;
		if ((((int32_t)L_81) < ((int32_t)L_82)))
		{
			goto IL_01ee;
		}
	}

IL_021f:
	{
		int32_t L_83 = V_3;
		if (!((int32_t)((int32_t)L_83&(int32_t)8)))
		{
			goto IL_0257;
		}
	}
	{
		goto IL_022e;
	}

IL_0226:
	{
		Crc32_t3523361801 * L_84 = V_0;
		int32_t L_85 = V_13;
		NullCheck(L_84);
		Crc32_Update_m3645794406(L_84, L_85, /*hidden argument*/NULL);
	}

IL_022e:
	{
		InflaterInputBuffer_t441930877 * L_86 = ((InflaterInputStream_t928059325 *)__this)->get_inputBuffer_2();
		NullCheck(L_86);
		int32_t L_87 = InflaterInputBuffer_ReadLeByte_m1881028497(L_86, /*hidden argument*/NULL);
		int32_t L_88 = L_87;
		V_13 = L_88;
		if ((((int32_t)L_88) > ((int32_t)0)))
		{
			goto IL_0226;
		}
	}
	{
		int32_t L_89 = V_13;
		if ((((int32_t)L_89) >= ((int32_t)0)))
		{
			goto IL_024f;
		}
	}
	{
		EndOfStreamException_t3956009005 * L_90 = (EndOfStreamException_t3956009005 *)il2cpp_codegen_object_new(EndOfStreamException_t3956009005_il2cpp_TypeInfo_var);
		EndOfStreamException__ctor_m2857110102(L_90, _stringLiteral1905238728, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_90);
	}

IL_024f:
	{
		Crc32_t3523361801 * L_91 = V_0;
		int32_t L_92 = V_13;
		NullCheck(L_91);
		Crc32_Update_m3645794406(L_91, L_92, /*hidden argument*/NULL);
	}

IL_0257:
	{
		int32_t L_93 = V_3;
		if (!((int32_t)((int32_t)L_93&(int32_t)((int32_t)16))))
		{
			goto IL_0290;
		}
	}
	{
		goto IL_0267;
	}

IL_025f:
	{
		Crc32_t3523361801 * L_94 = V_0;
		int32_t L_95 = V_14;
		NullCheck(L_94);
		Crc32_Update_m3645794406(L_94, L_95, /*hidden argument*/NULL);
	}

IL_0267:
	{
		InflaterInputBuffer_t441930877 * L_96 = ((InflaterInputStream_t928059325 *)__this)->get_inputBuffer_2();
		NullCheck(L_96);
		int32_t L_97 = InflaterInputBuffer_ReadLeByte_m1881028497(L_96, /*hidden argument*/NULL);
		int32_t L_98 = L_97;
		V_14 = L_98;
		if ((((int32_t)L_98) > ((int32_t)0)))
		{
			goto IL_025f;
		}
	}
	{
		int32_t L_99 = V_14;
		if ((((int32_t)L_99) >= ((int32_t)0)))
		{
			goto IL_0288;
		}
	}
	{
		EndOfStreamException_t3956009005 * L_100 = (EndOfStreamException_t3956009005 *)il2cpp_codegen_object_new(EndOfStreamException_t3956009005_il2cpp_TypeInfo_var);
		EndOfStreamException__ctor_m2857110102(L_100, _stringLiteral1905238728, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_100);
	}

IL_0288:
	{
		Crc32_t3523361801 * L_101 = V_0;
		int32_t L_102 = V_14;
		NullCheck(L_101);
		Crc32_Update_m3645794406(L_101, L_102, /*hidden argument*/NULL);
	}

IL_0290:
	{
		int32_t L_103 = V_3;
		if (!((int32_t)((int32_t)L_103&(int32_t)2)))
		{
			goto IL_02f4;
		}
	}
	{
		InflaterInputBuffer_t441930877 * L_104 = ((InflaterInputStream_t928059325 *)__this)->get_inputBuffer_2();
		NullCheck(L_104);
		int32_t L_105 = InflaterInputBuffer_ReadLeByte_m1881028497(L_104, /*hidden argument*/NULL);
		V_16 = L_105;
		int32_t L_106 = V_16;
		if ((((int32_t)L_106) >= ((int32_t)0)))
		{
			goto IL_02b2;
		}
	}
	{
		EndOfStreamException_t3956009005 * L_107 = (EndOfStreamException_t3956009005 *)il2cpp_codegen_object_new(EndOfStreamException_t3956009005_il2cpp_TypeInfo_var);
		EndOfStreamException__ctor_m2857110102(L_107, _stringLiteral1905238728, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_107);
	}

IL_02b2:
	{
		InflaterInputBuffer_t441930877 * L_108 = ((InflaterInputStream_t928059325 *)__this)->get_inputBuffer_2();
		NullCheck(L_108);
		int32_t L_109 = InflaterInputBuffer_ReadLeByte_m1881028497(L_108, /*hidden argument*/NULL);
		V_15 = L_109;
		int32_t L_110 = V_15;
		if ((((int32_t)L_110) >= ((int32_t)0)))
		{
			goto IL_02cf;
		}
	}
	{
		EndOfStreamException_t3956009005 * L_111 = (EndOfStreamException_t3956009005 *)il2cpp_codegen_object_new(EndOfStreamException_t3956009005_il2cpp_TypeInfo_var);
		EndOfStreamException__ctor_m2857110102(L_111, _stringLiteral1905238728, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_111);
	}

IL_02cf:
	{
		int32_t L_112 = V_16;
		int32_t L_113 = V_15;
		V_16 = ((int32_t)((int32_t)((int32_t)((int32_t)L_112<<(int32_t)8))|(int32_t)L_113));
		int32_t L_114 = V_16;
		Crc32_t3523361801 * L_115 = V_0;
		NullCheck(L_115);
		int64_t L_116 = Crc32_get_Value_m2535696875(L_115, /*hidden argument*/NULL);
		if ((((int32_t)L_114) == ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)L_116)))&(int32_t)((int32_t)65535))))))
		{
			goto IL_02f4;
		}
	}
	{
		GZipException_t2414750427 * L_117 = (GZipException_t2414750427 *)il2cpp_codegen_object_new(GZipException_t2414750427_il2cpp_TypeInfo_var);
		GZipException__ctor_m1759163018(L_117, _stringLiteral213654492, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_117);
	}

IL_02f4:
	{
		__this->set_readGZIPHeader_7((bool)1);
		return (bool)1;
	}
}
// System.Void ICSharpCode.SharpZipLib.GZip.GZipInputStream::ReadFooter()
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern Il2CppClass* EndOfStreamException_t3956009005_il2cpp_TypeInfo_var;
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* GZipException_t2414750427_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1857648086;
extern Il2CppCodeGenString* _stringLiteral1440698404;
extern Il2CppCodeGenString* _stringLiteral3694591504;
extern Il2CppCodeGenString* _stringLiteral107755755;
extern const uint32_t GZipInputStream_ReadFooter_m3020263032_MetadataUsageId;
extern "C"  void GZipInputStream_ReadFooter_m3020263032 (GZipInputStream_t2489725366 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GZipInputStream_ReadFooter_m3020263032_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	int64_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	uint32_t V_5 = 0;
	ObjectU5BU5D_t1108656482* V_6 = NULL;
	{
		V_0 = ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)8));
		Inflater_t1975778921 * L_0 = ((InflaterInputStream_t928059325 *)__this)->get_inf_1();
		NullCheck(L_0);
		int64_t L_1 = Inflater_get_TotalOut_m3201385003(L_0, /*hidden argument*/NULL);
		V_1 = ((int64_t)((int64_t)L_1&(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1)))))))));
		InflaterInputBuffer_t441930877 * L_2 = ((InflaterInputStream_t928059325 *)__this)->get_inputBuffer_2();
		InflaterInputBuffer_t441930877 * L_3 = L_2;
		NullCheck(L_3);
		int32_t L_4 = InflaterInputBuffer_get_Available_m3610562184(L_3, /*hidden argument*/NULL);
		Inflater_t1975778921 * L_5 = ((InflaterInputStream_t928059325 *)__this)->get_inf_1();
		NullCheck(L_5);
		int32_t L_6 = Inflater_get_RemainingInput_m39630580(L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		InflaterInputBuffer_set_Available_m1900611929(L_3, ((int32_t)((int32_t)L_4+(int32_t)L_6)), /*hidden argument*/NULL);
		Inflater_t1975778921 * L_7 = ((InflaterInputStream_t928059325 *)__this)->get_inf_1();
		NullCheck(L_7);
		Inflater_Reset_m1534606118(L_7, /*hidden argument*/NULL);
		V_2 = 8;
		goto IL_0066;
	}

IL_0042:
	{
		InflaterInputBuffer_t441930877 * L_8 = ((InflaterInputStream_t928059325 *)__this)->get_inputBuffer_2();
		ByteU5BU5D_t4260760469* L_9 = V_0;
		int32_t L_10 = V_2;
		int32_t L_11 = V_2;
		NullCheck(L_8);
		int32_t L_12 = InflaterInputBuffer_ReadClearTextBuffer_m1437123005(L_8, L_9, ((int32_t)((int32_t)8-(int32_t)L_10)), L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		int32_t L_13 = V_3;
		if ((((int32_t)L_13) > ((int32_t)0)))
		{
			goto IL_0062;
		}
	}
	{
		EndOfStreamException_t3956009005 * L_14 = (EndOfStreamException_t3956009005 *)il2cpp_codegen_object_new(EndOfStreamException_t3956009005_il2cpp_TypeInfo_var);
		EndOfStreamException__ctor_m2857110102(L_14, _stringLiteral1857648086, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_0062:
	{
		int32_t L_15 = V_2;
		int32_t L_16 = V_3;
		V_2 = ((int32_t)((int32_t)L_15-(int32_t)L_16));
	}

IL_0066:
	{
		int32_t L_17 = V_2;
		if ((((int32_t)L_17) > ((int32_t)0)))
		{
			goto IL_0042;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_18 = V_0;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 0);
		int32_t L_19 = 0;
		uint8_t L_20 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		ByteU5BU5D_t4260760469* L_21 = V_0;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 1);
		int32_t L_22 = 1;
		uint8_t L_23 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		ByteU5BU5D_t4260760469* L_24 = V_0;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 2);
		int32_t L_25 = 2;
		uint8_t L_26 = (L_24)->GetAt(static_cast<il2cpp_array_size_t>(L_25));
		ByteU5BU5D_t4260760469* L_27 = V_0;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 3);
		int32_t L_28 = 3;
		uint8_t L_29 = (L_27)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_20&(int32_t)((int32_t)255)))|(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_23&(int32_t)((int32_t)255)))<<(int32_t)8))))|(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_26&(int32_t)((int32_t)255)))<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)L_29<<(int32_t)((int32_t)24)))));
		int32_t L_30 = V_4;
		Crc32_t3523361801 * L_31 = __this->get_crc_6();
		NullCheck(L_31);
		int64_t L_32 = Crc32_get_Value_m2535696875(L_31, /*hidden argument*/NULL);
		if ((((int32_t)L_30) == ((int32_t)(((int32_t)((int32_t)L_32))))))
		{
			goto IL_00ec;
		}
	}
	{
		V_6 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		ObjectU5BU5D_t1108656482* L_33 = V_6;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, 0);
		ArrayElementTypeCheck (L_33, _stringLiteral1440698404);
		(L_33)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral1440698404);
		ObjectU5BU5D_t1108656482* L_34 = V_6;
		int32_t L_35 = V_4;
		int32_t L_36 = L_35;
		Il2CppObject * L_37 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_36);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, 1);
		ArrayElementTypeCheck (L_34, L_37);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_37);
		ObjectU5BU5D_t1108656482* L_38 = V_6;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 2);
		ArrayElementTypeCheck (L_38, _stringLiteral3694591504);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral3694591504);
		ObjectU5BU5D_t1108656482* L_39 = V_6;
		Crc32_t3523361801 * L_40 = __this->get_crc_6();
		NullCheck(L_40);
		int64_t L_41 = Crc32_get_Value_m2535696875(L_40, /*hidden argument*/NULL);
		int32_t L_42 = (((int32_t)((int32_t)L_41)));
		Il2CppObject * L_43 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_42);
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, 3);
		ArrayElementTypeCheck (L_39, L_43);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_43);
		ObjectU5BU5D_t1108656482* L_44 = V_6;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_45 = String_Concat_m3016520001(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		GZipException_t2414750427 * L_46 = (GZipException_t2414750427 *)il2cpp_codegen_object_new(GZipException_t2414750427_il2cpp_TypeInfo_var);
		GZipException__ctor_m1759163018(L_46, L_45, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_46);
	}

IL_00ec:
	{
		ByteU5BU5D_t4260760469* L_47 = V_0;
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, 4);
		int32_t L_48 = 4;
		uint8_t L_49 = (L_47)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		ByteU5BU5D_t4260760469* L_50 = V_0;
		NullCheck(L_50);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_50, 5);
		int32_t L_51 = 5;
		uint8_t L_52 = (L_50)->GetAt(static_cast<il2cpp_array_size_t>(L_51));
		ByteU5BU5D_t4260760469* L_53 = V_0;
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, 6);
		int32_t L_54 = 6;
		uint8_t L_55 = (L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_54));
		ByteU5BU5D_t4260760469* L_56 = V_0;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 7);
		int32_t L_57 = 7;
		uint8_t L_58 = (L_56)->GetAt(static_cast<il2cpp_array_size_t>(L_57));
		V_5 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_49&(int32_t)((int32_t)255)))|(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_52&(int32_t)((int32_t)255)))<<(int32_t)8))))|(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_55&(int32_t)((int32_t)255)))<<(int32_t)((int32_t)16)))))|(int32_t)((int32_t)((int32_t)L_58<<(int32_t)((int32_t)24)))));
		int64_t L_59 = V_1;
		uint32_t L_60 = V_5;
		if ((((int64_t)L_59) == ((int64_t)(((int64_t)((uint64_t)L_60))))))
		{
			goto IL_0128;
		}
	}
	{
		GZipException_t2414750427 * L_61 = (GZipException_t2414750427 *)il2cpp_codegen_object_new(GZipException_t2414750427_il2cpp_TypeInfo_var);
		GZipException__ctor_m1759163018(L_61, _stringLiteral107755755, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_61);
	}

IL_0128:
	{
		__this->set_readGZIPHeader_7((bool)0);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.GZip.GZipOutputStream::.ctor(System.IO.Stream)
extern "C"  void GZipOutputStream__ctor_m497768309 (GZipOutputStream_t2632250695 * __this, Stream_t1561764144 * ___baseOutputStream0, const MethodInfo* method)
{
	{
		Stream_t1561764144 * L_0 = ___baseOutputStream0;
		GZipOutputStream__ctor_m960121730(__this, L_0, ((int32_t)4096), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.GZip.GZipOutputStream::.ctor(System.IO.Stream,System.Int32)
extern Il2CppClass* Crc32_t3523361801_il2cpp_TypeInfo_var;
extern Il2CppClass* Deflater_t2454063301_il2cpp_TypeInfo_var;
extern const uint32_t GZipOutputStream__ctor_m960121730_MetadataUsageId;
extern "C"  void GZipOutputStream__ctor_m960121730 (GZipOutputStream_t2632250695 * __this, Stream_t1561764144 * ___baseOutputStream0, int32_t ___size1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GZipOutputStream__ctor_m960121730_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Crc32_t3523361801 * L_0 = (Crc32_t3523361801 *)il2cpp_codegen_object_new(Crc32_t3523361801_il2cpp_TypeInfo_var);
		Crc32__ctor_m1138481016(L_0, /*hidden argument*/NULL);
		__this->set_crc_8(L_0);
		Stream_t1561764144 * L_1 = ___baseOutputStream0;
		Deflater_t2454063301 * L_2 = (Deflater_t2454063301 *)il2cpp_codegen_object_new(Deflater_t2454063301_il2cpp_TypeInfo_var);
		Deflater__ctor_m4114082383(L_2, (-1), (bool)1, /*hidden argument*/NULL);
		int32_t L_3 = ___size1;
		DeflaterOutputStream__ctor_m3308520758(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.GZip.GZipOutputStream::Write(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1710460517;
extern const uint32_t GZipOutputStream_Write_m3786020206_MetadataUsageId;
extern "C"  void GZipOutputStream_Write_m3786020206 (GZipOutputStream_t2632250695 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GZipOutputStream_Write_m3786020206_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_state__9();
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		GZipOutputStream_WriteHeader_m687598088(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		int32_t L_1 = __this->get_state__9();
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0022;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_2 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_2, _stringLiteral1710460517, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0022:
	{
		Crc32_t3523361801 * L_3 = __this->get_crc_8();
		ByteU5BU5D_t4260760469* L_4 = ___buffer0;
		int32_t L_5 = ___offset1;
		int32_t L_6 = ___count2;
		NullCheck(L_3);
		Crc32_Update_m3381543860(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_7 = ___buffer0;
		int32_t L_8 = ___offset1;
		int32_t L_9 = ___count2;
		DeflaterOutputStream_Write_m3972695563(__this, L_7, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.GZip.GZipOutputStream::Close()
extern "C"  void GZipOutputStream_Close_m2584621556 (GZipOutputStream_t2632250695 * __this, const MethodInfo* method)
{
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker0::Invoke(27 /* System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Finish() */, __this);
		IL2CPP_LEAVE(0x2C, FINALLY_0008);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0008;
	}

FINALLY_0008:
	{ // begin finally (depth: 1)
		{
			int32_t L_0 = __this->get_state__9();
			if ((((int32_t)L_0) == ((int32_t)3)))
			{
				goto IL_002b;
			}
		}

IL_0011:
		{
			__this->set_state__9(3);
			bool L_1 = DeflaterOutputStream_get_IsStreamOwner_m3089361197(__this, /*hidden argument*/NULL);
			if (!L_1)
			{
				goto IL_002b;
			}
		}

IL_0020:
		{
			Stream_t1561764144 * L_2 = ((DeflaterOutputStream_t537202536 *)__this)->get_baseOutputStream__5();
			NullCheck(L_2);
			VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_2);
		}

IL_002b:
		{
			IL2CPP_END_FINALLY(8)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(8)
	{
		IL2CPP_JUMP_TBL(0x2C, IL_002c)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_002c:
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.GZip.GZipOutputStream::Finish()
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern const uint32_t GZipOutputStream_Finish_m1047423801_MetadataUsageId;
extern "C"  void GZipOutputStream_Finish_m1047423801 (GZipOutputStream_t2632250695 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GZipOutputStream_Finish_m1047423801_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	ByteU5BU5D_t4260760469* V_2 = NULL;
	ByteU5BU5D_t4260760469* V_3 = NULL;
	{
		int32_t L_0 = __this->get_state__9();
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		GZipOutputStream_WriteHeader_m687598088(__this, /*hidden argument*/NULL);
	}

IL_000e:
	{
		int32_t L_1 = __this->get_state__9();
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_0095;
		}
	}
	{
		__this->set_state__9(2);
		DeflaterOutputStream_Finish_m2029750678(__this, /*hidden argument*/NULL);
		Deflater_t2454063301 * L_2 = ((DeflaterOutputStream_t537202536 *)__this)->get_deflater__4();
		NullCheck(L_2);
		int64_t L_3 = Deflater_get_TotalIn_m2939095598(L_2, /*hidden argument*/NULL);
		V_0 = (((int32_t)((uint32_t)((int64_t)((int64_t)L_3&(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1))))))))))));
		Crc32_t3523361801 * L_4 = __this->get_crc_8();
		NullCheck(L_4);
		int64_t L_5 = Crc32_get_Value_m2535696875(L_4, /*hidden argument*/NULL);
		V_1 = (((int32_t)((uint32_t)((int64_t)((int64_t)L_5&(int64_t)(((int64_t)((uint64_t)(((uint32_t)((uint32_t)(-1))))))))))));
		V_3 = ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)8));
		ByteU5BU5D_t4260760469* L_6 = V_3;
		uint32_t L_7 = V_1;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 0);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)(((int32_t)((uint8_t)L_7))));
		ByteU5BU5D_t4260760469* L_8 = V_3;
		uint32_t L_9 = V_1;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_9>>8))))));
		ByteU5BU5D_t4260760469* L_10 = V_3;
		uint32_t L_11 = V_1;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 2);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(2), (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_11>>((int32_t)16)))))));
		ByteU5BU5D_t4260760469* L_12 = V_3;
		uint32_t L_13 = V_1;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_13>>((int32_t)24)))))));
		ByteU5BU5D_t4260760469* L_14 = V_3;
		uint32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 4);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(4), (uint8_t)(((int32_t)((uint8_t)L_15))));
		ByteU5BU5D_t4260760469* L_16 = V_3;
		uint32_t L_17 = V_0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 5);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(5), (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_17>>8))))));
		ByteU5BU5D_t4260760469* L_18 = V_3;
		uint32_t L_19 = V_0;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 6);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(6), (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_19>>((int32_t)16)))))));
		ByteU5BU5D_t4260760469* L_20 = V_3;
		uint32_t L_21 = V_0;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 7);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(7), (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_21>>((int32_t)24)))))));
		ByteU5BU5D_t4260760469* L_22 = V_3;
		V_2 = L_22;
		Stream_t1561764144 * L_23 = ((DeflaterOutputStream_t537202536 *)__this)->get_baseOutputStream__5();
		ByteU5BU5D_t4260760469* L_24 = V_2;
		ByteU5BU5D_t4260760469* L_25 = V_2;
		NullCheck(L_25);
		NullCheck(L_23);
		VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(21 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_23, L_24, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_25)->max_length)))));
	}

IL_0095:
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.GZip.GZipOutputStream::WriteHeader()
extern Il2CppClass* DateTime_t4283661327_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x6000152U2D1_1_FieldInfo_var;
extern const uint32_t GZipOutputStream_WriteHeader_m687598088_MetadataUsageId;
extern "C"  void GZipOutputStream_WriteHeader_m687598088 (GZipOutputStream_t2632250695 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GZipOutputStream_WriteHeader_m687598088_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ByteU5BU5D_t4260760469* V_1 = NULL;
	DateTime_t4283661327  V_2;
	memset(&V_2, 0, sizeof(V_2));
	DateTime_t4283661327  V_3;
	memset(&V_3, 0, sizeof(V_3));
	ByteU5BU5D_t4260760469* V_4 = NULL;
	{
		int32_t L_0 = __this->get_state__9();
		if (L_0)
		{
			goto IL_0081;
		}
	}
	{
		__this->set_state__9(1);
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t4283661327_il2cpp_TypeInfo_var);
		DateTime_t4283661327  L_1 = DateTime_get_Now_m1812131422(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_1;
		int64_t L_2 = DateTime_get_Ticks_m386468226((&V_2), /*hidden argument*/NULL);
		DateTime_t4283661327  L_3;
		memset(&L_3, 0, sizeof(L_3));
		DateTime__ctor_m145640619(&L_3, ((int32_t)1970), 1, 1, /*hidden argument*/NULL);
		V_3 = L_3;
		int64_t L_4 = DateTime_get_Ticks_m386468226((&V_3), /*hidden argument*/NULL);
		V_0 = (((int32_t)((int32_t)((int64_t)((int64_t)((int64_t)((int64_t)L_2-(int64_t)L_4))/(int64_t)(((int64_t)((int64_t)((int32_t)10000000)))))))));
		ByteU5BU5D_t4260760469* L_5 = ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x6000152U2D1_1_FieldInfo_var), /*hidden argument*/NULL);
		V_4 = L_5;
		ByteU5BU5D_t4260760469* L_6 = V_4;
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 4);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(4), (uint8_t)(((int32_t)((uint8_t)L_7))));
		ByteU5BU5D_t4260760469* L_8 = V_4;
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 5);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(5), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_9>>(int32_t)8))))));
		ByteU5BU5D_t4260760469* L_10 = V_4;
		int32_t L_11 = V_0;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 6);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(6), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_11>>(int32_t)((int32_t)16)))))));
		ByteU5BU5D_t4260760469* L_12 = V_4;
		int32_t L_13 = V_0;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 7);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(7), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_13>>(int32_t)((int32_t)24)))))));
		ByteU5BU5D_t4260760469* L_14 = V_4;
		V_1 = L_14;
		Stream_t1561764144 * L_15 = ((DeflaterOutputStream_t537202536 *)__this)->get_baseOutputStream__5();
		ByteU5BU5D_t4260760469* L_16 = V_1;
		ByteU5BU5D_t4260760469* L_17 = V_1;
		NullCheck(L_17);
		NullCheck(L_15);
		VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(21 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_15, L_16, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_17)->max_length)))));
	}

IL_0081:
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.SharpZipBaseException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void SharpZipBaseException__ctor_m3034235335 (SharpZipBaseException_t2520014981 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t2185721892 * L_0 = ___info0;
		StreamingContext_t2761351129  L_1 = ___context1;
		ApplicationException__ctor_m2651787575(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.SharpZipBaseException::.ctor()
extern "C"  void SharpZipBaseException__ctor_m730469766 (SharpZipBaseException_t2520014981 * __this, const MethodInfo* method)
{
	{
		ApplicationException__ctor_m659975414(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.SharpZipBaseException::.ctor(System.String)
extern "C"  void SharpZipBaseException__ctor_m2797155132 (SharpZipBaseException_t2520014981 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		ApplicationException__ctor_m1727689164(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::.ctor(System.Int32,System.Boolean)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppClass* DeflaterPending_t1829109954_il2cpp_TypeInfo_var;
extern Il2CppClass* DeflaterEngine_t3684739431_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral102865796;
extern const uint32_t Deflater__ctor_m4114082383_MetadataUsageId;
extern "C"  void Deflater__ctor_m4114082383 (Deflater_t2454063301 * __this, int32_t ___level0, bool ___noZlibHeaderOrFooter1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Deflater__ctor_m4114082383_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___level0;
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_000f;
		}
	}
	{
		___level0 = 6;
		goto IL_0023;
	}

IL_000f:
	{
		int32_t L_1 = ___level0;
		if ((((int32_t)L_1) < ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_2 = ___level0;
		if ((((int32_t)L_2) <= ((int32_t)((int32_t)9))))
		{
			goto IL_0023;
		}
	}

IL_0018:
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, _stringLiteral102865796, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		DeflaterPending_t1829109954 * L_4 = (DeflaterPending_t1829109954 *)il2cpp_codegen_object_new(DeflaterPending_t1829109954_il2cpp_TypeInfo_var);
		DeflaterPending__ctor_m1638983314(L_4, /*hidden argument*/NULL);
		__this->set_pending_4(L_4);
		DeflaterPending_t1829109954 * L_5 = __this->get_pending_4();
		DeflaterEngine_t3684739431 * L_6 = (DeflaterEngine_t3684739431 *)il2cpp_codegen_object_new(DeflaterEngine_t3684739431_il2cpp_TypeInfo_var);
		DeflaterEngine__ctor_m1885967597(L_6, L_5, /*hidden argument*/NULL);
		__this->set_engine_5(L_6);
		bool L_7 = ___noZlibHeaderOrFooter1;
		__this->set_noZlibHeaderOrFooter_1(L_7);
		Deflater_SetStrategy_m1748227658(__this, 0, /*hidden argument*/NULL);
		int32_t L_8 = ___level0;
		Deflater_SetLevel_m3168569498(__this, L_8, /*hidden argument*/NULL);
		Deflater_Reset_m3657832522(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::Reset()
extern "C"  void Deflater_Reset_m3657832522 (Deflater_t2454063301 * __this, const MethodInfo* method)
{
	Deflater_t2454063301 * G_B2_0 = NULL;
	Deflater_t2454063301 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	Deflater_t2454063301 * G_B3_1 = NULL;
	{
		bool L_0 = __this->get_noZlibHeaderOrFooter_1();
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_000c;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_000e;
	}

IL_000c:
	{
		G_B3_0 = ((int32_t)16);
		G_B3_1 = G_B2_0;
	}

IL_000e:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_state_2(G_B3_0);
		__this->set_totalOut_3((((int64_t)((int64_t)0))));
		DeflaterPending_t1829109954 * L_1 = __this->get_pending_4();
		NullCheck(L_1);
		PendingBuffer_Reset_m2437383704(L_1, /*hidden argument*/NULL);
		DeflaterEngine_t3684739431 * L_2 = __this->get_engine_5();
		NullCheck(L_2);
		DeflaterEngine_Reset_m71009448(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Deflater::get_TotalIn()
extern "C"  int64_t Deflater_get_TotalIn_m2939095598 (Deflater_t2454063301 * __this, const MethodInfo* method)
{
	{
		DeflaterEngine_t3684739431 * L_0 = __this->get_engine_5();
		NullCheck(L_0);
		int64_t L_1 = DeflaterEngine_get_TotalIn_m2394369484(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::Flush()
extern "C"  void Deflater_Flush_m1800379583 (Deflater_t2454063301 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_state_2();
		__this->set_state_2(((int32_t)((int32_t)L_0|(int32_t)4)));
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::Finish()
extern "C"  void Deflater_Finish_m1400398426 (Deflater_t2454063301 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_state_2();
		__this->set_state_2(((int32_t)((int32_t)L_0|(int32_t)((int32_t)12))));
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Deflater::get_IsFinished()
extern "C"  bool Deflater_get_IsFinished_m284667774 (Deflater_t2454063301 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_state_2();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)30)))))
		{
			goto IL_0016;
		}
	}
	{
		DeflaterPending_t1829109954 * L_1 = __this->get_pending_4();
		NullCheck(L_1);
		bool L_2 = PendingBuffer_get_IsFlushed_m2304700395(L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_0016:
	{
		return (bool)0;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Deflater::get_IsNeedingInput()
extern "C"  bool Deflater_get_IsNeedingInput_m862754346 (Deflater_t2454063301 * __this, const MethodInfo* method)
{
	{
		DeflaterEngine_t3684739431 * L_0 = __this->get_engine_5();
		NullCheck(L_0);
		bool L_1 = DeflaterEngine_NeedsInput_m1072053576(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::SetInput(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1435152625;
extern const uint32_t Deflater_SetInput_m1771564410_MetadataUsageId;
extern "C"  void Deflater_SetInput_m1771564410 (Deflater_t2454063301 * __this, ByteU5BU5D_t4260760469* ___input0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Deflater_SetInput_m1771564410_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_state_2();
		if (!((int32_t)((int32_t)L_0&(int32_t)8)))
		{
			goto IL_0015;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_1, _stringLiteral1435152625, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0015:
	{
		DeflaterEngine_t3684739431 * L_2 = __this->get_engine_5();
		ByteU5BU5D_t4260760469* L_3 = ___input0;
		int32_t L_4 = ___offset1;
		int32_t L_5 = ___count2;
		NullCheck(L_2);
		DeflaterEngine_SetInput_m3394045016(L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::SetLevel(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral102865796;
extern const uint32_t Deflater_SetLevel_m3168569498_MetadataUsageId;
extern "C"  void Deflater_SetLevel_m3168569498 (Deflater_t2454063301 * __this, int32_t ___level0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Deflater_SetLevel_m3168569498_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___level0;
		if ((!(((uint32_t)L_0) == ((uint32_t)(-1)))))
		{
			goto IL_0009;
		}
	}
	{
		___level0 = 6;
		goto IL_001d;
	}

IL_0009:
	{
		int32_t L_1 = ___level0;
		if ((((int32_t)L_1) < ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_2 = ___level0;
		if ((((int32_t)L_2) <= ((int32_t)((int32_t)9))))
		{
			goto IL_001d;
		}
	}

IL_0012:
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, _stringLiteral102865796, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001d:
	{
		int32_t L_4 = __this->get_level_0();
		int32_t L_5 = ___level0;
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_0039;
		}
	}
	{
		int32_t L_6 = ___level0;
		__this->set_level_0(L_6);
		DeflaterEngine_t3684739431 * L_7 = __this->get_engine_5();
		int32_t L_8 = ___level0;
		NullCheck(L_7);
		DeflaterEngine_SetLevel_m2670793852(L_7, L_8, /*hidden argument*/NULL);
	}

IL_0039:
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Deflater::SetStrategy(ICSharpCode.SharpZipLib.Zip.Compression.DeflateStrategy)
extern "C"  void Deflater_SetStrategy_m1748227658 (Deflater_t2454063301 * __this, int32_t ___strategy0, const MethodInfo* method)
{
	{
		DeflaterEngine_t3684739431 * L_0 = __this->get_engine_5();
		int32_t L_1 = ___strategy0;
		NullCheck(L_0);
		DeflaterEngine_set_Strategy_m971094401(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Deflater::Deflate(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1795239397;
extern const uint32_t Deflater_Deflate_m1433311247_MetadataUsageId;
extern "C"  int32_t Deflater_Deflate_m1433311247 (Deflater_t2454063301 * __this, ByteU5BU5D_t4260760469* ___output0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Deflater_Deflate_m1433311247_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		int32_t L_0 = ___length2;
		V_0 = L_0;
		int32_t L_1 = __this->get_state_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)127)))))
		{
			goto IL_0017;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_2 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_2, _stringLiteral1795239397, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0017:
	{
		int32_t L_3 = __this->get_state_2();
		if ((((int32_t)L_3) >= ((int32_t)((int32_t)16))))
		{
			goto IL_00be;
		}
	}
	{
		V_1 = ((int32_t)30720);
		int32_t L_4 = __this->get_level_0();
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)L_4-(int32_t)1))>>(int32_t)1));
		int32_t L_5 = V_2;
		if ((((int32_t)L_5) < ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_6 = V_2;
		if ((((int32_t)L_6) <= ((int32_t)3)))
		{
			goto IL_003f;
		}
	}

IL_003d:
	{
		V_2 = 3;
	}

IL_003f:
	{
		int32_t L_7 = V_1;
		int32_t L_8 = V_2;
		V_1 = ((int32_t)((int32_t)L_7|(int32_t)((int32_t)((int32_t)L_8<<(int32_t)6))));
		int32_t L_9 = __this->get_state_2();
		if (!((int32_t)((int32_t)L_9&(int32_t)1)))
		{
			goto IL_0054;
		}
	}
	{
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10|(int32_t)((int32_t)32)));
	}

IL_0054:
	{
		int32_t L_11 = V_1;
		int32_t L_12 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)((int32_t)((int32_t)((int32_t)31)-(int32_t)((int32_t)((int32_t)L_12%(int32_t)((int32_t)31)))))));
		DeflaterPending_t1829109954 * L_13 = __this->get_pending_4();
		int32_t L_14 = V_1;
		NullCheck(L_13);
		PendingBuffer_WriteShortMSB_m3421538425(L_13, L_14, /*hidden argument*/NULL);
		int32_t L_15 = __this->get_state_2();
		if (!((int32_t)((int32_t)L_15&(int32_t)1)))
		{
			goto IL_00ac;
		}
	}
	{
		DeflaterEngine_t3684739431 * L_16 = __this->get_engine_5();
		NullCheck(L_16);
		int32_t L_17 = DeflaterEngine_get_Adler_m434246074(L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		DeflaterEngine_t3684739431 * L_18 = __this->get_engine_5();
		NullCheck(L_18);
		DeflaterEngine_ResetAdler_m1548771120(L_18, /*hidden argument*/NULL);
		DeflaterPending_t1829109954 * L_19 = __this->get_pending_4();
		int32_t L_20 = V_3;
		NullCheck(L_19);
		PendingBuffer_WriteShortMSB_m3421538425(L_19, ((int32_t)((int32_t)L_20>>(int32_t)((int32_t)16))), /*hidden argument*/NULL);
		DeflaterPending_t1829109954 * L_21 = __this->get_pending_4();
		int32_t L_22 = V_3;
		NullCheck(L_21);
		PendingBuffer_WriteShortMSB_m3421538425(L_21, ((int32_t)((int32_t)L_22&(int32_t)((int32_t)65535))), /*hidden argument*/NULL);
	}

IL_00ac:
	{
		int32_t L_23 = __this->get_state_2();
		__this->set_state_2(((int32_t)((int32_t)((int32_t)16)|(int32_t)((int32_t)((int32_t)L_23&(int32_t)((int32_t)12))))));
	}

IL_00be:
	{
		DeflaterPending_t1829109954 * L_24 = __this->get_pending_4();
		ByteU5BU5D_t4260760469* L_25 = ___output0;
		int32_t L_26 = ___offset1;
		int32_t L_27 = ___length2;
		NullCheck(L_24);
		int32_t L_28 = PendingBuffer_Flush_m844807344(L_24, L_25, L_26, L_27, /*hidden argument*/NULL);
		V_4 = L_28;
		int32_t L_29 = ___offset1;
		int32_t L_30 = V_4;
		___offset1 = ((int32_t)((int32_t)L_29+(int32_t)L_30));
		int64_t L_31 = __this->get_totalOut_3();
		int32_t L_32 = V_4;
		__this->set_totalOut_3(((int64_t)((int64_t)L_31+(int64_t)(((int64_t)((int64_t)L_32))))));
		int32_t L_33 = ___length2;
		int32_t L_34 = V_4;
		___length2 = ((int32_t)((int32_t)L_33-(int32_t)L_34));
		int32_t L_35 = ___length2;
		if (!L_35)
		{
			goto IL_01de;
		}
	}
	{
		int32_t L_36 = __this->get_state_2();
		if ((((int32_t)L_36) == ((int32_t)((int32_t)30))))
		{
			goto IL_01de;
		}
	}
	{
		DeflaterEngine_t3684739431 * L_37 = __this->get_engine_5();
		int32_t L_38 = __this->get_state_2();
		int32_t L_39 = __this->get_state_2();
		NullCheck(L_37);
		bool L_40 = DeflaterEngine_Deflate_m2021511700(L_37, (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_38&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0), (bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_39&(int32_t)8))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		if (L_40)
		{
			goto IL_00be;
		}
	}
	{
		int32_t L_41 = __this->get_state_2();
		if ((!(((uint32_t)L_41) == ((uint32_t)((int32_t)16)))))
		{
			goto IL_0134;
		}
	}
	{
		int32_t L_42 = V_0;
		int32_t L_43 = ___length2;
		return ((int32_t)((int32_t)L_42-(int32_t)L_43));
	}

IL_0134:
	{
		int32_t L_44 = __this->get_state_2();
		if ((!(((uint32_t)L_44) == ((uint32_t)((int32_t)20)))))
		{
			goto IL_0181;
		}
	}
	{
		int32_t L_45 = __this->get_level_0();
		if (!L_45)
		{
			goto IL_0174;
		}
	}
	{
		DeflaterPending_t1829109954 * L_46 = __this->get_pending_4();
		NullCheck(L_46);
		int32_t L_47 = PendingBuffer_get_BitCount_m2974417016(L_46, /*hidden argument*/NULL);
		V_5 = ((int32_t)((int32_t)8+(int32_t)((int32_t)((int32_t)((-L_47))&(int32_t)7))));
		goto IL_016f;
	}

IL_015a:
	{
		DeflaterPending_t1829109954 * L_48 = __this->get_pending_4();
		NullCheck(L_48);
		PendingBuffer_WriteBits_m4156628504(L_48, 2, ((int32_t)10), /*hidden argument*/NULL);
		int32_t L_49 = V_5;
		V_5 = ((int32_t)((int32_t)L_49-(int32_t)((int32_t)10)));
	}

IL_016f:
	{
		int32_t L_50 = V_5;
		if ((((int32_t)L_50) > ((int32_t)0)))
		{
			goto IL_015a;
		}
	}

IL_0174:
	{
		__this->set_state_2(((int32_t)16));
		goto IL_00be;
	}

IL_0181:
	{
		int32_t L_51 = __this->get_state_2();
		if ((!(((uint32_t)L_51) == ((uint32_t)((int32_t)28)))))
		{
			goto IL_00be;
		}
	}
	{
		DeflaterPending_t1829109954 * L_52 = __this->get_pending_4();
		NullCheck(L_52);
		PendingBuffer_AlignToByte_m2130252305(L_52, /*hidden argument*/NULL);
		bool L_53 = __this->get_noZlibHeaderOrFooter_1();
		if (L_53)
		{
			goto IL_01d1;
		}
	}
	{
		DeflaterEngine_t3684739431 * L_54 = __this->get_engine_5();
		NullCheck(L_54);
		int32_t L_55 = DeflaterEngine_get_Adler_m434246074(L_54, /*hidden argument*/NULL);
		V_6 = L_55;
		DeflaterPending_t1829109954 * L_56 = __this->get_pending_4();
		int32_t L_57 = V_6;
		NullCheck(L_56);
		PendingBuffer_WriteShortMSB_m3421538425(L_56, ((int32_t)((int32_t)L_57>>(int32_t)((int32_t)16))), /*hidden argument*/NULL);
		DeflaterPending_t1829109954 * L_58 = __this->get_pending_4();
		int32_t L_59 = V_6;
		NullCheck(L_58);
		PendingBuffer_WriteShortMSB_m3421538425(L_58, ((int32_t)((int32_t)L_59&(int32_t)((int32_t)65535))), /*hidden argument*/NULL);
	}

IL_01d1:
	{
		__this->set_state_2(((int32_t)30));
		goto IL_00be;
	}

IL_01de:
	{
		int32_t L_60 = V_0;
		int32_t L_61 = ___length2;
		return ((int32_t)((int32_t)L_60-(int32_t)L_61));
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterConstants::.ctor()
extern "C"  void DeflaterConstants__ctor_m3674322010 (DeflaterConstants_t2584813498 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterConstants::.cctor()
extern Il2CppClass* DeflaterConstants_t2584813498_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004fcU2D1_2_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004fcU2D2_3_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004fcU2D3_4_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004fcU2D4_5_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004fcU2D5_6_FieldInfo_var;
extern const uint32_t DeflaterConstants__cctor_m1752736403_MetadataUsageId;
extern "C"  void DeflaterConstants__cctor_m1752736403 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterConstants__cctor_m1752736403_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Math_Min_m811624909(NULL /*static, unused*/, ((int32_t)65535), ((int32_t)65531), /*hidden argument*/NULL);
		((DeflaterConstants_t2584813498_StaticFields*)DeflaterConstants_t2584813498_il2cpp_TypeInfo_var->static_fields)->set_MAX_BLOCK_SIZE_0(L_0);
		Int32U5BU5D_t3230847821* L_1 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004fcU2D1_2_FieldInfo_var), /*hidden argument*/NULL);
		((DeflaterConstants_t2584813498_StaticFields*)DeflaterConstants_t2584813498_il2cpp_TypeInfo_var->static_fields)->set_GOOD_LENGTH_1(L_1);
		Int32U5BU5D_t3230847821* L_2 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_2, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004fcU2D2_3_FieldInfo_var), /*hidden argument*/NULL);
		((DeflaterConstants_t2584813498_StaticFields*)DeflaterConstants_t2584813498_il2cpp_TypeInfo_var->static_fields)->set_MAX_LAZY_2(L_2);
		Int32U5BU5D_t3230847821* L_3 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_3, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004fcU2D3_4_FieldInfo_var), /*hidden argument*/NULL);
		((DeflaterConstants_t2584813498_StaticFields*)DeflaterConstants_t2584813498_il2cpp_TypeInfo_var->static_fields)->set_NICE_LENGTH_3(L_3);
		Int32U5BU5D_t3230847821* L_4 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_4, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004fcU2D4_5_FieldInfo_var), /*hidden argument*/NULL);
		((DeflaterConstants_t2584813498_StaticFields*)DeflaterConstants_t2584813498_il2cpp_TypeInfo_var->static_fields)->set_MAX_CHAIN_4(L_4);
		Int32U5BU5D_t3230847821* L_5 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)10)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_5, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004fcU2D5_6_FieldInfo_var), /*hidden argument*/NULL);
		((DeflaterConstants_t2584813498_StaticFields*)DeflaterConstants_t2584813498_il2cpp_TypeInfo_var->static_fields)->set_COMPR_FUNC_5(L_5);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::.ctor(ICSharpCode.SharpZipLib.Zip.Compression.DeflaterPending)
extern Il2CppClass* DeflaterConstants_t2584813498_il2cpp_TypeInfo_var;
extern Il2CppClass* DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var;
extern Il2CppClass* Adler32_t2680377483_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16U5BU5D_t801762735_il2cpp_TypeInfo_var;
extern const uint32_t DeflaterEngine__ctor_m1885967597_MetadataUsageId;
extern "C"  void DeflaterEngine__ctor_m1885967597 (DeflaterEngine_t3684739431 * __this, DeflaterPending_t1829109954 * ___pending0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterEngine__ctor_m1885967597_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterConstants_t2584813498_il2cpp_TypeInfo_var);
		DeflaterConstants__ctor_m3674322010(__this, /*hidden argument*/NULL);
		DeflaterPending_t1829109954 * L_0 = ___pending0;
		__this->set_pending_26(L_0);
		DeflaterPending_t1829109954 * L_1 = ___pending0;
		DeflaterHuffman_t3769756376 * L_2 = (DeflaterHuffman_t3769756376 *)il2cpp_codegen_object_new(DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var);
		DeflaterHuffman__ctor_m211969164(L_2, L_1, /*hidden argument*/NULL);
		__this->set_huffman_27(L_2);
		Adler32_t2680377483 * L_3 = (Adler32_t2680377483 *)il2cpp_codegen_object_new(Adler32_t2680377483_il2cpp_TypeInfo_var);
		Adler32__ctor_m2455030518(L_3, /*hidden argument*/NULL);
		__this->set_adler_28(L_3);
		__this->set_window_15(((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)((int32_t)65536))));
		__this->set_head_7(((Int16U5BU5D_t801762735*)SZArrayNew(Int16U5BU5D_t801762735_il2cpp_TypeInfo_var, (uint32_t)((int32_t)32768))));
		__this->set_prev_8(((Int16U5BU5D_t801762735*)SZArrayNew(Int16U5BU5D_t801762735_il2cpp_TypeInfo_var, (uint32_t)((int32_t)32768))));
		int32_t L_4 = 1;
		V_0 = L_4;
		__this->set_strstart_13(L_4);
		int32_t L_5 = V_0;
		__this->set_blockStart_12(L_5);
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::Deflate(System.Boolean,System.Boolean)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral659085960;
extern const uint32_t DeflaterEngine_Deflate_m2021511700_MetadataUsageId;
extern "C"  bool DeflaterEngine_Deflate_m2021511700 (DeflaterEngine_t3684739431 * __this, bool ___flush0, bool ___finish1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterEngine_Deflate_m2021511700_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	bool V_1 = false;
	int32_t V_2 = 0;
	int32_t G_B3_0 = 0;

IL_0000:
	{
		DeflaterEngine_FillWindow_m1132978300(__this, /*hidden argument*/NULL);
		bool L_0 = ___flush0;
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_1 = __this->get_inputOff_24();
		int32_t L_2 = __this->get_inputEnd_25();
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = 0;
	}

IL_001a:
	{
		V_1 = (bool)G_B3_0;
		int32_t L_3 = __this->get_compressionFunction_21();
		V_2 = L_3;
		int32_t L_4 = V_2;
		if (L_4 == 0)
		{
			goto IL_0036;
		}
		if (L_4 == 1)
		{
			goto IL_0041;
		}
		if (L_4 == 2)
		{
			goto IL_004c;
		}
	}
	{
		goto IL_0057;
	}

IL_0036:
	{
		bool L_5 = V_1;
		bool L_6 = ___finish1;
		bool L_7 = DeflaterEngine_DeflateStored_m865620657(__this, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		goto IL_0062;
	}

IL_0041:
	{
		bool L_8 = V_1;
		bool L_9 = ___finish1;
		bool L_10 = DeflaterEngine_DeflateFast_m1961244312(__this, L_8, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		goto IL_0062;
	}

IL_004c:
	{
		bool L_11 = V_1;
		bool L_12 = ___finish1;
		bool L_13 = DeflaterEngine_DeflateSlow_m4017565779(__this, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		goto IL_0062;
	}

IL_0057:
	{
		InvalidOperationException_t1589641621 * L_14 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_14, _stringLiteral659085960, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_0062:
	{
		DeflaterPending_t1829109954 * L_15 = __this->get_pending_26();
		NullCheck(L_15);
		bool L_16 = PendingBuffer_get_IsFlushed_m2304700395(L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0072;
		}
	}
	{
		bool L_17 = V_0;
		if (L_17)
		{
			goto IL_0000;
		}
	}

IL_0072:
	{
		bool L_18 = V_0;
		return L_18;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::SetInput(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2916848704;
extern Il2CppCodeGenString* _stringLiteral3275187347;
extern Il2CppCodeGenString* _stringLiteral94851343;
extern Il2CppCodeGenString* _stringLiteral169949703;
extern const uint32_t DeflaterEngine_SetInput_m3394045016_MetadataUsageId;
extern "C"  void DeflaterEngine_SetInput_m3394045016 (DeflaterEngine_t3684739431 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterEngine_SetInput_m3394045016_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		ByteU5BU5D_t4260760469* L_0 = ___buffer0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral2916848704, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		int32_t L_2 = ___offset1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_3, _stringLiteral3275187347, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_001d:
	{
		int32_t L_4 = ___count2;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_002c;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_5 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_5, _stringLiteral94851343, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_002c:
	{
		int32_t L_6 = __this->get_inputOff_24();
		int32_t L_7 = __this->get_inputEnd_25();
		if ((((int32_t)L_6) >= ((int32_t)L_7)))
		{
			goto IL_0045;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_8 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_8, _stringLiteral169949703, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0045:
	{
		int32_t L_9 = ___offset1;
		int32_t L_10 = ___count2;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)L_10));
		int32_t L_11 = ___offset1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) > ((int32_t)L_12)))
		{
			goto IL_0053;
		}
	}
	{
		int32_t L_13 = V_0;
		ByteU5BU5D_t4260760469* L_14 = ___buffer0;
		NullCheck(L_14);
		if ((((int32_t)L_13) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_005e;
		}
	}

IL_0053:
	{
		ArgumentOutOfRangeException_t3816648464 * L_15 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_15, _stringLiteral94851343, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_005e:
	{
		ByteU5BU5D_t4260760469* L_16 = ___buffer0;
		__this->set_inputBuf_22(L_16);
		int32_t L_17 = ___offset1;
		__this->set_inputOff_24(L_17);
		int32_t L_18 = V_0;
		__this->set_inputEnd_25(L_18);
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::NeedsInput()
extern "C"  bool DeflaterEngine_NeedsInput_m1072053576 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_inputEnd_25();
		int32_t L_1 = __this->get_inputOff_24();
		return (bool)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::Reset()
extern "C"  void DeflaterEngine_Reset_m71009448 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		DeflaterHuffman_t3769756376 * L_0 = __this->get_huffman_27();
		NullCheck(L_0);
		DeflaterHuffman_Reset_m1909678057(L_0, /*hidden argument*/NULL);
		Adler32_t2680377483 * L_1 = __this->get_adler_28();
		NullCheck(L_1);
		Adler32_Reset_m101463459(L_1, /*hidden argument*/NULL);
		int32_t L_2 = 1;
		V_2 = L_2;
		__this->set_strstart_13(L_2);
		int32_t L_3 = V_2;
		__this->set_blockStart_12(L_3);
		__this->set_lookahead_14(0);
		__this->set_totalIn_23((((int64_t)((int64_t)0))));
		__this->set_prevAvailable_11((bool)0);
		__this->set_matchLen_10(2);
		V_0 = 0;
		goto IL_0054;
	}

IL_0047:
	{
		Int16U5BU5D_t801762735* L_4 = __this->get_head_7();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (int16_t)0);
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0054:
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) < ((int32_t)((int32_t)32768))))
		{
			goto IL_0047;
		}
	}
	{
		V_1 = 0;
		goto IL_006d;
	}

IL_0060:
	{
		Int16U5BU5D_t801762735* L_8 = __this->get_prev_8();
		int32_t L_9 = V_1;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (int16_t)0);
		int32_t L_10 = V_1;
		V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_006d:
	{
		int32_t L_11 = V_1;
		if ((((int32_t)L_11) < ((int32_t)((int32_t)32768))))
		{
			goto IL_0060;
		}
	}
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::ResetAdler()
extern "C"  void DeflaterEngine_ResetAdler_m1548771120 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method)
{
	{
		Adler32_t2680377483 * L_0 = __this->get_adler_28();
		NullCheck(L_0);
		Adler32_Reset_m101463459(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::get_Adler()
extern "C"  int32_t DeflaterEngine_get_Adler_m434246074 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method)
{
	{
		Adler32_t2680377483 * L_0 = __this->get_adler_28();
		NullCheck(L_0);
		int64_t L_1 = Adler32_get_Value_m2923380521(L_0, /*hidden argument*/NULL);
		return (((int32_t)((int32_t)L_1)));
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::get_TotalIn()
extern "C"  int64_t DeflaterEngine_get_TotalIn_m2394369484 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = __this->get_totalIn_23();
		return L_0;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::set_Strategy(ICSharpCode.SharpZipLib.Zip.Compression.DeflateStrategy)
extern "C"  void DeflaterEngine_set_Strategy_m971094401 (DeflaterEngine_t3684739431 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_strategy_16(L_0);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::SetLevel(System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppClass* DeflaterConstants_t2584813498_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral102865796;
extern const uint32_t DeflaterEngine_SetLevel_m2670793852_MetadataUsageId;
extern "C"  void DeflaterEngine_SetLevel_m2670793852 (DeflaterEngine_t3684739431 * __this, int32_t ___level0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterEngine_SetLevel_m2670793852_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___level0;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0009;
		}
	}
	{
		int32_t L_1 = ___level0;
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)9))))
		{
			goto IL_0014;
		}
	}

IL_0009:
	{
		ArgumentOutOfRangeException_t3816648464 * L_2 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_2, _stringLiteral102865796, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterConstants_t2584813498_il2cpp_TypeInfo_var);
		Int32U5BU5D_t3230847821* L_3 = ((DeflaterConstants_t2584813498_StaticFields*)DeflaterConstants_t2584813498_il2cpp_TypeInfo_var->static_fields)->get_GOOD_LENGTH_1();
		int32_t L_4 = ___level0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		int32_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		__this->set_goodLength_20(L_6);
		Int32U5BU5D_t3230847821* L_7 = ((DeflaterConstants_t2584813498_StaticFields*)DeflaterConstants_t2584813498_il2cpp_TypeInfo_var->static_fields)->get_MAX_LAZY_2();
		int32_t L_8 = ___level0;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_max_lazy_18(L_10);
		Int32U5BU5D_t3230847821* L_11 = ((DeflaterConstants_t2584813498_StaticFields*)DeflaterConstants_t2584813498_il2cpp_TypeInfo_var->static_fields)->get_NICE_LENGTH_3();
		int32_t L_12 = ___level0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		__this->set_niceLength_19(L_14);
		Int32U5BU5D_t3230847821* L_15 = ((DeflaterConstants_t2584813498_StaticFields*)DeflaterConstants_t2584813498_il2cpp_TypeInfo_var->static_fields)->get_MAX_CHAIN_4();
		int32_t L_16 = ___level0;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		int32_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		__this->set_max_chain_17(L_18);
		Int32U5BU5D_t3230847821* L_19 = ((DeflaterConstants_t2584813498_StaticFields*)DeflaterConstants_t2584813498_il2cpp_TypeInfo_var->static_fields)->get_COMPR_FUNC_5();
		int32_t L_20 = ___level0;
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, L_20);
		int32_t L_21 = L_20;
		int32_t L_22 = (L_19)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		int32_t L_23 = __this->get_compressionFunction_21();
		if ((((int32_t)L_22) == ((int32_t)L_23)))
		{
			goto IL_0189;
		}
	}
	{
		int32_t L_24 = __this->get_compressionFunction_21();
		V_0 = L_24;
		int32_t L_25 = V_0;
		if (L_25 == 0)
		{
			goto IL_0078;
		}
		if (L_25 == 1)
		{
			goto IL_00c2;
		}
		if (L_25 == 2)
		{
			goto IL_0106;
		}
	}
	{
		goto IL_017c;
	}

IL_0078:
	{
		int32_t L_26 = __this->get_strstart_13();
		int32_t L_27 = __this->get_blockStart_12();
		if ((((int32_t)L_26) <= ((int32_t)L_27)))
		{
			goto IL_00b7;
		}
	}
	{
		DeflaterHuffman_t3769756376 * L_28 = __this->get_huffman_27();
		ByteU5BU5D_t4260760469* L_29 = __this->get_window_15();
		int32_t L_30 = __this->get_blockStart_12();
		int32_t L_31 = __this->get_strstart_13();
		int32_t L_32 = __this->get_blockStart_12();
		NullCheck(L_28);
		DeflaterHuffman_FlushStoredBlock_m2383394690(L_28, L_29, L_30, ((int32_t)((int32_t)L_31-(int32_t)L_32)), (bool)0, /*hidden argument*/NULL);
		int32_t L_33 = __this->get_strstart_13();
		__this->set_blockStart_12(L_33);
	}

IL_00b7:
	{
		DeflaterEngine_UpdateHash_m2011003296(__this, /*hidden argument*/NULL);
		goto IL_017c;
	}

IL_00c2:
	{
		int32_t L_34 = __this->get_strstart_13();
		int32_t L_35 = __this->get_blockStart_12();
		if ((((int32_t)L_34) <= ((int32_t)L_35)))
		{
			goto IL_017c;
		}
	}
	{
		DeflaterHuffman_t3769756376 * L_36 = __this->get_huffman_27();
		ByteU5BU5D_t4260760469* L_37 = __this->get_window_15();
		int32_t L_38 = __this->get_blockStart_12();
		int32_t L_39 = __this->get_strstart_13();
		int32_t L_40 = __this->get_blockStart_12();
		NullCheck(L_36);
		DeflaterHuffman_FlushBlock_m3510668965(L_36, L_37, L_38, ((int32_t)((int32_t)L_39-(int32_t)L_40)), (bool)0, /*hidden argument*/NULL);
		int32_t L_41 = __this->get_strstart_13();
		__this->set_blockStart_12(L_41);
		goto IL_017c;
	}

IL_0106:
	{
		bool L_42 = __this->get_prevAvailable_11();
		if (!L_42)
		{
			goto IL_012f;
		}
	}
	{
		DeflaterHuffman_t3769756376 * L_43 = __this->get_huffman_27();
		ByteU5BU5D_t4260760469* L_44 = __this->get_window_15();
		int32_t L_45 = __this->get_strstart_13();
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)((int32_t)L_45-(int32_t)1)));
		int32_t L_46 = ((int32_t)((int32_t)L_45-(int32_t)1));
		uint8_t L_47 = (L_44)->GetAt(static_cast<il2cpp_array_size_t>(L_46));
		NullCheck(L_43);
		DeflaterHuffman_TallyLit_m1601839602(L_43, ((int32_t)((int32_t)L_47&(int32_t)((int32_t)255))), /*hidden argument*/NULL);
	}

IL_012f:
	{
		int32_t L_48 = __this->get_strstart_13();
		int32_t L_49 = __this->get_blockStart_12();
		if ((((int32_t)L_48) <= ((int32_t)L_49)))
		{
			goto IL_016e;
		}
	}
	{
		DeflaterHuffman_t3769756376 * L_50 = __this->get_huffman_27();
		ByteU5BU5D_t4260760469* L_51 = __this->get_window_15();
		int32_t L_52 = __this->get_blockStart_12();
		int32_t L_53 = __this->get_strstart_13();
		int32_t L_54 = __this->get_blockStart_12();
		NullCheck(L_50);
		DeflaterHuffman_FlushBlock_m3510668965(L_50, L_51, L_52, ((int32_t)((int32_t)L_53-(int32_t)L_54)), (bool)0, /*hidden argument*/NULL);
		int32_t L_55 = __this->get_strstart_13();
		__this->set_blockStart_12(L_55);
	}

IL_016e:
	{
		__this->set_prevAvailable_11((bool)0);
		__this->set_matchLen_10(2);
	}

IL_017c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterConstants_t2584813498_il2cpp_TypeInfo_var);
		Int32U5BU5D_t3230847821* L_56 = ((DeflaterConstants_t2584813498_StaticFields*)DeflaterConstants_t2584813498_il2cpp_TypeInfo_var->static_fields)->get_COMPR_FUNC_5();
		int32_t L_57 = ___level0;
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, L_57);
		int32_t L_58 = L_57;
		int32_t L_59 = (L_56)->GetAt(static_cast<il2cpp_array_size_t>(L_58));
		__this->set_compressionFunction_21(L_59);
	}

IL_0189:
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::FillWindow()
extern "C"  void DeflaterEngine_FillWindow_m1132978300 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_strstart_13();
		if ((((int32_t)L_0) < ((int32_t)((int32_t)65274))))
		{
			goto IL_00b5;
		}
	}
	{
		DeflaterEngine_SlideWindow_m3591605722(__this, /*hidden argument*/NULL);
		goto IL_00b5;
	}

IL_001b:
	{
		int32_t L_1 = __this->get_lookahead_14();
		int32_t L_2 = __this->get_strstart_13();
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)65536)-(int32_t)L_1))-(int32_t)L_2));
		int32_t L_3 = V_0;
		int32_t L_4 = __this->get_inputEnd_25();
		int32_t L_5 = __this->get_inputOff_24();
		if ((((int32_t)L_3) <= ((int32_t)((int32_t)((int32_t)L_4-(int32_t)L_5)))))
		{
			goto IL_004d;
		}
	}
	{
		int32_t L_6 = __this->get_inputEnd_25();
		int32_t L_7 = __this->get_inputOff_24();
		V_0 = ((int32_t)((int32_t)L_6-(int32_t)L_7));
	}

IL_004d:
	{
		ByteU5BU5D_t4260760469* L_8 = __this->get_inputBuf_22();
		int32_t L_9 = __this->get_inputOff_24();
		ByteU5BU5D_t4260760469* L_10 = __this->get_window_15();
		int32_t L_11 = __this->get_strstart_13();
		int32_t L_12 = __this->get_lookahead_14();
		int32_t L_13 = V_0;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_8, L_9, (Il2CppArray *)(Il2CppArray *)L_10, ((int32_t)((int32_t)L_11+(int32_t)L_12)), L_13, /*hidden argument*/NULL);
		Adler32_t2680377483 * L_14 = __this->get_adler_28();
		ByteU5BU5D_t4260760469* L_15 = __this->get_inputBuf_22();
		int32_t L_16 = __this->get_inputOff_24();
		int32_t L_17 = V_0;
		NullCheck(L_14);
		Adler32_Update_m2142069042(L_14, L_15, L_16, L_17, /*hidden argument*/NULL);
		int32_t L_18 = __this->get_inputOff_24();
		int32_t L_19 = V_0;
		__this->set_inputOff_24(((int32_t)((int32_t)L_18+(int32_t)L_19)));
		int64_t L_20 = __this->get_totalIn_23();
		int32_t L_21 = V_0;
		__this->set_totalIn_23(((int64_t)((int64_t)L_20+(int64_t)(((int64_t)((int64_t)L_21))))));
		int32_t L_22 = __this->get_lookahead_14();
		int32_t L_23 = V_0;
		__this->set_lookahead_14(((int32_t)((int32_t)L_22+(int32_t)L_23)));
	}

IL_00b5:
	{
		int32_t L_24 = __this->get_lookahead_14();
		if ((((int32_t)L_24) >= ((int32_t)((int32_t)262))))
		{
			goto IL_00d3;
		}
	}
	{
		int32_t L_25 = __this->get_inputOff_24();
		int32_t L_26 = __this->get_inputEnd_25();
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_001b;
		}
	}

IL_00d3:
	{
		int32_t L_27 = __this->get_lookahead_14();
		if ((((int32_t)L_27) < ((int32_t)3)))
		{
			goto IL_00e2;
		}
	}
	{
		DeflaterEngine_UpdateHash_m2011003296(__this, /*hidden argument*/NULL);
	}

IL_00e2:
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::UpdateHash()
extern "C"  void DeflaterEngine_UpdateHash_m2011003296 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4260760469* L_0 = __this->get_window_15();
		int32_t L_1 = __this->get_strstart_13();
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		uint8_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		ByteU5BU5D_t4260760469* L_4 = __this->get_window_15();
		int32_t L_5 = __this->get_strstart_13();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, ((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = ((int32_t)((int32_t)L_5+(int32_t)1));
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		__this->set_ins_h_6(((int32_t)((int32_t)((int32_t)((int32_t)L_3<<(int32_t)5))^(int32_t)L_7)));
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::InsertString()
extern "C"  int32_t DeflaterEngine_InsertString_m1774160927 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method)
{
	int16_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_ins_h_6();
		ByteU5BU5D_t4260760469* L_1 = __this->get_window_15();
		int32_t L_2 = __this->get_strstart_13();
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, ((int32_t)((int32_t)L_2+(int32_t)2)));
		int32_t L_3 = ((int32_t)((int32_t)L_2+(int32_t)2));
		uint8_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_0<<(int32_t)5))^(int32_t)L_4))&(int32_t)((int32_t)32767)));
		Int16U5BU5D_t801762735* L_5 = __this->get_prev_8();
		int32_t L_6 = __this->get_strstart_13();
		Int16U5BU5D_t801762735* L_7 = __this->get_head_7();
		int32_t L_8 = V_1;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		int16_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		int16_t L_11 = L_10;
		V_0 = L_11;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)L_6&(int32_t)((int32_t)32767))));
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_6&(int32_t)((int32_t)32767)))), (int16_t)L_11);
		Int16U5BU5D_t801762735* L_12 = __this->get_head_7();
		int32_t L_13 = V_1;
		int32_t L_14 = __this->get_strstart_13();
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(L_13), (int16_t)(((int16_t)((int16_t)L_14))));
		int32_t L_15 = V_1;
		__this->set_ins_h_6(L_15);
		int16_t L_16 = V_0;
		return ((int32_t)((int32_t)L_16&(int32_t)((int32_t)65535)));
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::SlideWindow()
extern "C"  void DeflaterEngine_SlideWindow_m3591605722 (DeflaterEngine_t3684739431 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t G_B3_0 = 0;
	Int16U5BU5D_t801762735* G_B3_1 = NULL;
	int32_t G_B2_0 = 0;
	Int16U5BU5D_t801762735* G_B2_1 = NULL;
	int32_t G_B4_0 = 0;
	int32_t G_B4_1 = 0;
	Int16U5BU5D_t801762735* G_B4_2 = NULL;
	int32_t G_B9_0 = 0;
	Int16U5BU5D_t801762735* G_B9_1 = NULL;
	int32_t G_B8_0 = 0;
	Int16U5BU5D_t801762735* G_B8_1 = NULL;
	int32_t G_B10_0 = 0;
	int32_t G_B10_1 = 0;
	Int16U5BU5D_t801762735* G_B10_2 = NULL;
	{
		ByteU5BU5D_t4260760469* L_0 = __this->get_window_15();
		ByteU5BU5D_t4260760469* L_1 = __this->get_window_15();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, ((int32_t)32768), (Il2CppArray *)(Il2CppArray *)L_1, 0, ((int32_t)32768), /*hidden argument*/NULL);
		int32_t L_2 = __this->get_matchStart_9();
		__this->set_matchStart_9(((int32_t)((int32_t)L_2-(int32_t)((int32_t)32768))));
		int32_t L_3 = __this->get_strstart_13();
		__this->set_strstart_13(((int32_t)((int32_t)L_3-(int32_t)((int32_t)32768))));
		int32_t L_4 = __this->get_blockStart_12();
		__this->set_blockStart_12(((int32_t)((int32_t)L_4-(int32_t)((int32_t)32768))));
		V_0 = 0;
		goto IL_0084;
	}

IL_0056:
	{
		Int16U5BU5D_t801762735* L_5 = __this->get_head_7();
		int32_t L_6 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		int16_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_1 = ((int32_t)((int32_t)L_8&(int32_t)((int32_t)65535)));
		Int16U5BU5D_t801762735* L_9 = __this->get_head_7();
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		G_B2_0 = L_10;
		G_B2_1 = L_9;
		if ((((int32_t)L_11) >= ((int32_t)((int32_t)32768))))
		{
			G_B3_0 = L_10;
			G_B3_1 = L_9;
			goto IL_0077;
		}
	}
	{
		G_B4_0 = 0;
		G_B4_1 = G_B2_0;
		G_B4_2 = G_B2_1;
		goto IL_007e;
	}

IL_0077:
	{
		int32_t L_12 = V_1;
		G_B4_0 = ((int32_t)((int32_t)L_12-(int32_t)((int32_t)32768)));
		G_B4_1 = G_B3_0;
		G_B4_2 = G_B3_1;
	}

IL_007e:
	{
		NullCheck(G_B4_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B4_2, G_B4_1);
		(G_B4_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B4_1), (int16_t)(((int16_t)((int16_t)G_B4_0))));
		int32_t L_13 = V_0;
		V_0 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0084:
	{
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) < ((int32_t)((int32_t)32768))))
		{
			goto IL_0056;
		}
	}
	{
		V_2 = 0;
		goto IL_00be;
	}

IL_0090:
	{
		Int16U5BU5D_t801762735* L_15 = __this->get_prev_8();
		int32_t L_16 = V_2;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		int16_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_3 = ((int32_t)((int32_t)L_18&(int32_t)((int32_t)65535)));
		Int16U5BU5D_t801762735* L_19 = __this->get_prev_8();
		int32_t L_20 = V_2;
		int32_t L_21 = V_3;
		G_B8_0 = L_20;
		G_B8_1 = L_19;
		if ((((int32_t)L_21) >= ((int32_t)((int32_t)32768))))
		{
			G_B9_0 = L_20;
			G_B9_1 = L_19;
			goto IL_00b1;
		}
	}
	{
		G_B10_0 = 0;
		G_B10_1 = G_B8_0;
		G_B10_2 = G_B8_1;
		goto IL_00b8;
	}

IL_00b1:
	{
		int32_t L_22 = V_3;
		G_B10_0 = ((int32_t)((int32_t)L_22-(int32_t)((int32_t)32768)));
		G_B10_1 = G_B9_0;
		G_B10_2 = G_B9_1;
	}

IL_00b8:
	{
		NullCheck(G_B10_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B10_2, G_B10_1);
		(G_B10_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B10_1), (int16_t)(((int16_t)((int16_t)G_B10_0))));
		int32_t L_23 = V_2;
		V_2 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_00be:
	{
		int32_t L_24 = V_2;
		if ((((int32_t)L_24) < ((int32_t)((int32_t)32768))))
		{
			goto IL_0090;
		}
	}
	{
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::FindLongestMatch(System.Int32)
extern "C"  bool DeflaterEngine_FindLongestMatch_m707434080 (DeflaterEngine_t3684739431 * __this, int32_t ___curMatch0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Int16U5BU5D_t801762735* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	uint8_t V_9 = 0x0;
	uint8_t V_10 = 0x0;
	{
		int32_t L_0 = __this->get_max_chain_17();
		V_0 = L_0;
		int32_t L_1 = __this->get_niceLength_19();
		V_1 = L_1;
		Int16U5BU5D_t801762735* L_2 = __this->get_prev_8();
		V_2 = L_2;
		int32_t L_3 = __this->get_strstart_13();
		V_3 = L_3;
		int32_t L_4 = __this->get_strstart_13();
		int32_t L_5 = __this->get_matchLen_10();
		V_5 = ((int32_t)((int32_t)L_4+(int32_t)L_5));
		int32_t L_6 = __this->get_matchLen_10();
		int32_t L_7 = Math_Max_m1309380475(NULL /*static, unused*/, L_6, 2, /*hidden argument*/NULL);
		V_6 = L_7;
		int32_t L_8 = __this->get_strstart_13();
		int32_t L_9 = Math_Max_m1309380475(NULL /*static, unused*/, ((int32_t)((int32_t)L_8-(int32_t)((int32_t)32506))), 0, /*hidden argument*/NULL);
		V_7 = L_9;
		int32_t L_10 = __this->get_strstart_13();
		V_8 = ((int32_t)((int32_t)((int32_t)((int32_t)L_10+(int32_t)((int32_t)258)))-(int32_t)1));
		ByteU5BU5D_t4260760469* L_11 = __this->get_window_15();
		int32_t L_12 = V_5;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12-(int32_t)1)));
		int32_t L_13 = ((int32_t)((int32_t)L_12-(int32_t)1));
		uint8_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		V_9 = L_14;
		ByteU5BU5D_t4260760469* L_15 = __this->get_window_15();
		int32_t L_16 = V_5;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		uint8_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_10 = L_18;
		int32_t L_19 = V_6;
		int32_t L_20 = __this->get_goodLength_20();
		if ((((int32_t)L_19) < ((int32_t)L_20)))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_21 = V_0;
		V_0 = ((int32_t)((int32_t)L_21>>(int32_t)2));
	}

IL_0083:
	{
		int32_t L_22 = V_1;
		int32_t L_23 = __this->get_lookahead_14();
		if ((((int32_t)L_22) <= ((int32_t)L_23)))
		{
			goto IL_0093;
		}
	}
	{
		int32_t L_24 = __this->get_lookahead_14();
		V_1 = L_24;
	}

IL_0093:
	{
		ByteU5BU5D_t4260760469* L_25 = __this->get_window_15();
		int32_t L_26 = ___curMatch0;
		int32_t L_27 = V_6;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, ((int32_t)((int32_t)L_26+(int32_t)L_27)));
		int32_t L_28 = ((int32_t)((int32_t)L_26+(int32_t)L_27));
		uint8_t L_29 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		uint8_t L_30 = V_10;
		if ((!(((uint32_t)L_29) == ((uint32_t)L_30))))
		{
			goto IL_021e;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_31 = __this->get_window_15();
		int32_t L_32 = ___curMatch0;
		int32_t L_33 = V_6;
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, ((int32_t)((int32_t)((int32_t)((int32_t)L_32+(int32_t)L_33))-(int32_t)1)));
		int32_t L_34 = ((int32_t)((int32_t)((int32_t)((int32_t)L_32+(int32_t)L_33))-(int32_t)1));
		uint8_t L_35 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		uint8_t L_36 = V_9;
		if ((!(((uint32_t)L_35) == ((uint32_t)L_36))))
		{
			goto IL_021e;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_37 = __this->get_window_15();
		int32_t L_38 = ___curMatch0;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, L_38);
		int32_t L_39 = L_38;
		uint8_t L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		ByteU5BU5D_t4260760469* L_41 = __this->get_window_15();
		int32_t L_42 = V_3;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, L_42);
		int32_t L_43 = L_42;
		uint8_t L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		if ((!(((uint32_t)L_40) == ((uint32_t)L_44))))
		{
			goto IL_021e;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_45 = __this->get_window_15();
		int32_t L_46 = ___curMatch0;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, ((int32_t)((int32_t)L_46+(int32_t)1)));
		int32_t L_47 = ((int32_t)((int32_t)L_46+(int32_t)1));
		uint8_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		ByteU5BU5D_t4260760469* L_49 = __this->get_window_15();
		int32_t L_50 = V_3;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, ((int32_t)((int32_t)L_50+(int32_t)1)));
		int32_t L_51 = ((int32_t)((int32_t)L_50+(int32_t)1));
		uint8_t L_52 = (L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_51));
		if ((!(((uint32_t)L_48) == ((uint32_t)L_52))))
		{
			goto IL_021e;
		}
	}
	{
		int32_t L_53 = ___curMatch0;
		V_4 = ((int32_t)((int32_t)L_53+(int32_t)2));
		int32_t L_54 = V_3;
		V_3 = ((int32_t)((int32_t)L_54+(int32_t)2));
	}

IL_00f0:
	{
		ByteU5BU5D_t4260760469* L_55 = __this->get_window_15();
		int32_t L_56 = V_3;
		int32_t L_57 = ((int32_t)((int32_t)L_56+(int32_t)1));
		V_3 = L_57;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, L_57);
		int32_t L_58 = L_57;
		uint8_t L_59 = (L_55)->GetAt(static_cast<il2cpp_array_size_t>(L_58));
		ByteU5BU5D_t4260760469* L_60 = __this->get_window_15();
		int32_t L_61 = V_4;
		int32_t L_62 = ((int32_t)((int32_t)L_61+(int32_t)1));
		V_4 = L_62;
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, L_62);
		int32_t L_63 = L_62;
		uint8_t L_64 = (L_60)->GetAt(static_cast<il2cpp_array_size_t>(L_63));
		if ((!(((uint32_t)L_59) == ((uint32_t)L_64))))
		{
			goto IL_01e1;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_65 = __this->get_window_15();
		int32_t L_66 = V_3;
		int32_t L_67 = ((int32_t)((int32_t)L_66+(int32_t)1));
		V_3 = L_67;
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, L_67);
		int32_t L_68 = L_67;
		uint8_t L_69 = (L_65)->GetAt(static_cast<il2cpp_array_size_t>(L_68));
		ByteU5BU5D_t4260760469* L_70 = __this->get_window_15();
		int32_t L_71 = V_4;
		int32_t L_72 = ((int32_t)((int32_t)L_71+(int32_t)1));
		V_4 = L_72;
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, L_72);
		int32_t L_73 = L_72;
		uint8_t L_74 = (L_70)->GetAt(static_cast<il2cpp_array_size_t>(L_73));
		if ((!(((uint32_t)L_69) == ((uint32_t)L_74))))
		{
			goto IL_01e1;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_75 = __this->get_window_15();
		int32_t L_76 = V_3;
		int32_t L_77 = ((int32_t)((int32_t)L_76+(int32_t)1));
		V_3 = L_77;
		NullCheck(L_75);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_75, L_77);
		int32_t L_78 = L_77;
		uint8_t L_79 = (L_75)->GetAt(static_cast<il2cpp_array_size_t>(L_78));
		ByteU5BU5D_t4260760469* L_80 = __this->get_window_15();
		int32_t L_81 = V_4;
		int32_t L_82 = ((int32_t)((int32_t)L_81+(int32_t)1));
		V_4 = L_82;
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, L_82);
		int32_t L_83 = L_82;
		uint8_t L_84 = (L_80)->GetAt(static_cast<il2cpp_array_size_t>(L_83));
		if ((!(((uint32_t)L_79) == ((uint32_t)L_84))))
		{
			goto IL_01e1;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_85 = __this->get_window_15();
		int32_t L_86 = V_3;
		int32_t L_87 = ((int32_t)((int32_t)L_86+(int32_t)1));
		V_3 = L_87;
		NullCheck(L_85);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_85, L_87);
		int32_t L_88 = L_87;
		uint8_t L_89 = (L_85)->GetAt(static_cast<il2cpp_array_size_t>(L_88));
		ByteU5BU5D_t4260760469* L_90 = __this->get_window_15();
		int32_t L_91 = V_4;
		int32_t L_92 = ((int32_t)((int32_t)L_91+(int32_t)1));
		V_4 = L_92;
		NullCheck(L_90);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_90, L_92);
		int32_t L_93 = L_92;
		uint8_t L_94 = (L_90)->GetAt(static_cast<il2cpp_array_size_t>(L_93));
		if ((!(((uint32_t)L_89) == ((uint32_t)L_94))))
		{
			goto IL_01e1;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_95 = __this->get_window_15();
		int32_t L_96 = V_3;
		int32_t L_97 = ((int32_t)((int32_t)L_96+(int32_t)1));
		V_3 = L_97;
		NullCheck(L_95);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_95, L_97);
		int32_t L_98 = L_97;
		uint8_t L_99 = (L_95)->GetAt(static_cast<il2cpp_array_size_t>(L_98));
		ByteU5BU5D_t4260760469* L_100 = __this->get_window_15();
		int32_t L_101 = V_4;
		int32_t L_102 = ((int32_t)((int32_t)L_101+(int32_t)1));
		V_4 = L_102;
		NullCheck(L_100);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_100, L_102);
		int32_t L_103 = L_102;
		uint8_t L_104 = (L_100)->GetAt(static_cast<il2cpp_array_size_t>(L_103));
		if ((!(((uint32_t)L_99) == ((uint32_t)L_104))))
		{
			goto IL_01e1;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_105 = __this->get_window_15();
		int32_t L_106 = V_3;
		int32_t L_107 = ((int32_t)((int32_t)L_106+(int32_t)1));
		V_3 = L_107;
		NullCheck(L_105);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_105, L_107);
		int32_t L_108 = L_107;
		uint8_t L_109 = (L_105)->GetAt(static_cast<il2cpp_array_size_t>(L_108));
		ByteU5BU5D_t4260760469* L_110 = __this->get_window_15();
		int32_t L_111 = V_4;
		int32_t L_112 = ((int32_t)((int32_t)L_111+(int32_t)1));
		V_4 = L_112;
		NullCheck(L_110);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_110, L_112);
		int32_t L_113 = L_112;
		uint8_t L_114 = (L_110)->GetAt(static_cast<il2cpp_array_size_t>(L_113));
		if ((!(((uint32_t)L_109) == ((uint32_t)L_114))))
		{
			goto IL_01e1;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_115 = __this->get_window_15();
		int32_t L_116 = V_3;
		int32_t L_117 = ((int32_t)((int32_t)L_116+(int32_t)1));
		V_3 = L_117;
		NullCheck(L_115);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_115, L_117);
		int32_t L_118 = L_117;
		uint8_t L_119 = (L_115)->GetAt(static_cast<il2cpp_array_size_t>(L_118));
		ByteU5BU5D_t4260760469* L_120 = __this->get_window_15();
		int32_t L_121 = V_4;
		int32_t L_122 = ((int32_t)((int32_t)L_121+(int32_t)1));
		V_4 = L_122;
		NullCheck(L_120);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_120, L_122);
		int32_t L_123 = L_122;
		uint8_t L_124 = (L_120)->GetAt(static_cast<il2cpp_array_size_t>(L_123));
		if ((!(((uint32_t)L_119) == ((uint32_t)L_124))))
		{
			goto IL_01e1;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_125 = __this->get_window_15();
		int32_t L_126 = V_3;
		int32_t L_127 = ((int32_t)((int32_t)L_126+(int32_t)1));
		V_3 = L_127;
		NullCheck(L_125);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_125, L_127);
		int32_t L_128 = L_127;
		uint8_t L_129 = (L_125)->GetAt(static_cast<il2cpp_array_size_t>(L_128));
		ByteU5BU5D_t4260760469* L_130 = __this->get_window_15();
		int32_t L_131 = V_4;
		int32_t L_132 = ((int32_t)((int32_t)L_131+(int32_t)1));
		V_4 = L_132;
		NullCheck(L_130);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_130, L_132);
		int32_t L_133 = L_132;
		uint8_t L_134 = (L_130)->GetAt(static_cast<il2cpp_array_size_t>(L_133));
		if ((!(((uint32_t)L_129) == ((uint32_t)L_134))))
		{
			goto IL_01e1;
		}
	}
	{
		int32_t L_135 = V_3;
		int32_t L_136 = V_8;
		if ((((int32_t)L_135) < ((int32_t)L_136)))
		{
			goto IL_00f0;
		}
	}

IL_01e1:
	{
		int32_t L_137 = V_3;
		int32_t L_138 = V_5;
		if ((((int32_t)L_137) <= ((int32_t)L_138)))
		{
			goto IL_0217;
		}
	}
	{
		int32_t L_139 = ___curMatch0;
		__this->set_matchStart_9(L_139);
		int32_t L_140 = V_3;
		V_5 = L_140;
		int32_t L_141 = V_3;
		int32_t L_142 = __this->get_strstart_13();
		V_6 = ((int32_t)((int32_t)L_141-(int32_t)L_142));
		int32_t L_143 = V_6;
		int32_t L_144 = V_1;
		if ((((int32_t)L_143) >= ((int32_t)L_144)))
		{
			goto IL_023e;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_145 = __this->get_window_15();
		int32_t L_146 = V_5;
		NullCheck(L_145);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_145, ((int32_t)((int32_t)L_146-(int32_t)1)));
		int32_t L_147 = ((int32_t)((int32_t)L_146-(int32_t)1));
		uint8_t L_148 = (L_145)->GetAt(static_cast<il2cpp_array_size_t>(L_147));
		V_9 = L_148;
		ByteU5BU5D_t4260760469* L_149 = __this->get_window_15();
		int32_t L_150 = V_5;
		NullCheck(L_149);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_149, L_150);
		int32_t L_151 = L_150;
		uint8_t L_152 = (L_149)->GetAt(static_cast<il2cpp_array_size_t>(L_151));
		V_10 = L_152;
	}

IL_0217:
	{
		int32_t L_153 = __this->get_strstart_13();
		V_3 = L_153;
	}

IL_021e:
	{
		Int16U5BU5D_t801762735* L_154 = V_2;
		int32_t L_155 = ___curMatch0;
		NullCheck(L_154);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_154, ((int32_t)((int32_t)L_155&(int32_t)((int32_t)32767))));
		int32_t L_156 = ((int32_t)((int32_t)L_155&(int32_t)((int32_t)32767)));
		int16_t L_157 = (L_154)->GetAt(static_cast<il2cpp_array_size_t>(L_156));
		int32_t L_158 = ((int32_t)((int32_t)L_157&(int32_t)((int32_t)65535)));
		___curMatch0 = L_158;
		int32_t L_159 = V_7;
		if ((((int32_t)L_158) <= ((int32_t)L_159)))
		{
			goto IL_023e;
		}
	}
	{
		int32_t L_160 = V_0;
		int32_t L_161 = ((int32_t)((int32_t)L_160-(int32_t)1));
		V_0 = L_161;
		if (L_161)
		{
			goto IL_0093;
		}
	}

IL_023e:
	{
		int32_t L_162 = V_6;
		int32_t L_163 = __this->get_lookahead_14();
		int32_t L_164 = Math_Min_m811624909(NULL /*static, unused*/, L_162, L_163, /*hidden argument*/NULL);
		__this->set_matchLen_10(L_164);
		int32_t L_165 = __this->get_matchLen_10();
		return (bool)((((int32_t)((((int32_t)L_165) < ((int32_t)3))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateStored(System.Boolean,System.Boolean)
extern Il2CppClass* DeflaterConstants_t2584813498_il2cpp_TypeInfo_var;
extern const uint32_t DeflaterEngine_DeflateStored_m865620657_MetadataUsageId;
extern "C"  bool DeflaterEngine_DeflateStored_m865620657 (DeflaterEngine_t3684739431 * __this, bool ___flush0, bool ___finish1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterEngine_DeflateStored_m865620657_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		bool L_0 = ___flush0;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_1 = __this->get_lookahead_14();
		if (L_1)
		{
			goto IL_000d;
		}
	}
	{
		return (bool)0;
	}

IL_000d:
	{
		int32_t L_2 = __this->get_strstart_13();
		int32_t L_3 = __this->get_lookahead_14();
		__this->set_strstart_13(((int32_t)((int32_t)L_2+(int32_t)L_3)));
		__this->set_lookahead_14(0);
		int32_t L_4 = __this->get_strstart_13();
		int32_t L_5 = __this->get_blockStart_12();
		V_0 = ((int32_t)((int32_t)L_4-(int32_t)L_5));
		int32_t L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterConstants_t2584813498_il2cpp_TypeInfo_var);
		int32_t L_7 = ((DeflaterConstants_t2584813498_StaticFields*)DeflaterConstants_t2584813498_il2cpp_TypeInfo_var->static_fields)->get_MAX_BLOCK_SIZE_0();
		if ((((int32_t)L_6) >= ((int32_t)L_7)))
		{
			goto IL_0055;
		}
	}
	{
		int32_t L_8 = __this->get_blockStart_12();
		if ((((int32_t)L_8) >= ((int32_t)((int32_t)32768))))
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_9 = V_0;
		if ((((int32_t)L_9) >= ((int32_t)((int32_t)32506))))
		{
			goto IL_0055;
		}
	}

IL_0052:
	{
		bool L_10 = ___flush0;
		if (!L_10)
		{
			goto IL_0093;
		}
	}

IL_0055:
	{
		bool L_11 = ___finish1;
		V_1 = L_11;
		int32_t L_12 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterConstants_t2584813498_il2cpp_TypeInfo_var);
		int32_t L_13 = ((DeflaterConstants_t2584813498_StaticFields*)DeflaterConstants_t2584813498_il2cpp_TypeInfo_var->static_fields)->get_MAX_BLOCK_SIZE_0();
		if ((((int32_t)L_12) <= ((int32_t)L_13)))
		{
			goto IL_0067;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterConstants_t2584813498_il2cpp_TypeInfo_var);
		int32_t L_14 = ((DeflaterConstants_t2584813498_StaticFields*)DeflaterConstants_t2584813498_il2cpp_TypeInfo_var->static_fields)->get_MAX_BLOCK_SIZE_0();
		V_0 = L_14;
		V_1 = (bool)0;
	}

IL_0067:
	{
		DeflaterHuffman_t3769756376 * L_15 = __this->get_huffman_27();
		ByteU5BU5D_t4260760469* L_16 = __this->get_window_15();
		int32_t L_17 = __this->get_blockStart_12();
		int32_t L_18 = V_0;
		bool L_19 = V_1;
		NullCheck(L_15);
		DeflaterHuffman_FlushStoredBlock_m2383394690(L_15, L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
		int32_t L_20 = __this->get_blockStart_12();
		int32_t L_21 = V_0;
		__this->set_blockStart_12(((int32_t)((int32_t)L_20+(int32_t)L_21)));
		bool L_22 = V_1;
		return (bool)((((int32_t)L_22) == ((int32_t)0))? 1 : 0);
	}

IL_0093:
	{
		return (bool)1;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateFast(System.Boolean,System.Boolean)
extern "C"  bool DeflaterEngine_DeflateFast_m1961244312 (DeflaterEngine_t3684739431 * __this, bool ___flush0, bool ___finish1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	int32_t V_3 = 0;
	int32_t G_B27_0 = 0;
	{
		int32_t L_0 = __this->get_lookahead_14();
		if ((((int32_t)L_0) >= ((int32_t)((int32_t)262))))
		{
			goto IL_01ec;
		}
	}
	{
		bool L_1 = ___flush0;
		if (L_1)
		{
			goto IL_01ec;
		}
	}
	{
		return (bool)0;
	}

IL_0018:
	{
		int32_t L_2 = __this->get_lookahead_14();
		if (L_2)
		{
			goto IL_0053;
		}
	}
	{
		DeflaterHuffman_t3769756376 * L_3 = __this->get_huffman_27();
		ByteU5BU5D_t4260760469* L_4 = __this->get_window_15();
		int32_t L_5 = __this->get_blockStart_12();
		int32_t L_6 = __this->get_strstart_13();
		int32_t L_7 = __this->get_blockStart_12();
		bool L_8 = ___finish1;
		NullCheck(L_3);
		DeflaterHuffman_FlushBlock_m3510668965(L_3, L_4, L_5, ((int32_t)((int32_t)L_6-(int32_t)L_7)), L_8, /*hidden argument*/NULL);
		int32_t L_9 = __this->get_strstart_13();
		__this->set_blockStart_12(L_9);
		return (bool)0;
	}

IL_0053:
	{
		int32_t L_10 = __this->get_strstart_13();
		if ((((int32_t)L_10) <= ((int32_t)((int32_t)65274))))
		{
			goto IL_0066;
		}
	}
	{
		DeflaterEngine_SlideWindow_m3591605722(__this, /*hidden argument*/NULL);
	}

IL_0066:
	{
		int32_t L_11 = __this->get_lookahead_14();
		if ((((int32_t)L_11) < ((int32_t)3)))
		{
			goto IL_015e;
		}
	}
	{
		int32_t L_12 = DeflaterEngine_InsertString_m1774160927(__this, /*hidden argument*/NULL);
		int32_t L_13 = L_12;
		V_0 = L_13;
		if (!L_13)
		{
			goto IL_015e;
		}
	}
	{
		int32_t L_14 = __this->get_strategy_16();
		if ((((int32_t)L_14) == ((int32_t)2)))
		{
			goto IL_015e;
		}
	}
	{
		int32_t L_15 = __this->get_strstart_13();
		int32_t L_16 = V_0;
		if ((((int32_t)((int32_t)((int32_t)L_15-(int32_t)L_16))) > ((int32_t)((int32_t)32506))))
		{
			goto IL_015e;
		}
	}
	{
		int32_t L_17 = V_0;
		bool L_18 = DeflaterEngine_FindLongestMatch_m707434080(__this, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_015e;
		}
	}
	{
		DeflaterHuffman_t3769756376 * L_19 = __this->get_huffman_27();
		int32_t L_20 = __this->get_strstart_13();
		int32_t L_21 = __this->get_matchStart_9();
		int32_t L_22 = __this->get_matchLen_10();
		NullCheck(L_19);
		bool L_23 = DeflaterHuffman_TallyDist_m4230142696(L_19, ((int32_t)((int32_t)L_20-(int32_t)L_21)), L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		int32_t L_24 = __this->get_lookahead_14();
		int32_t L_25 = __this->get_matchLen_10();
		__this->set_lookahead_14(((int32_t)((int32_t)L_24-(int32_t)L_25)));
		int32_t L_26 = __this->get_matchLen_10();
		int32_t L_27 = __this->get_max_lazy_18();
		if ((((int32_t)L_26) > ((int32_t)L_27)))
		{
			goto IL_012d;
		}
	}
	{
		int32_t L_28 = __this->get_lookahead_14();
		if ((((int32_t)L_28) < ((int32_t)3)))
		{
			goto IL_012d;
		}
	}
	{
		goto IL_0109;
	}

IL_00f4:
	{
		int32_t L_29 = __this->get_strstart_13();
		__this->set_strstart_13(((int32_t)((int32_t)L_29+(int32_t)1)));
		DeflaterEngine_InsertString_m1774160927(__this, /*hidden argument*/NULL);
	}

IL_0109:
	{
		int32_t L_30 = __this->get_matchLen_10();
		int32_t L_31 = ((int32_t)((int32_t)L_30-(int32_t)1));
		V_3 = L_31;
		__this->set_matchLen_10(L_31);
		int32_t L_32 = V_3;
		if ((((int32_t)L_32) > ((int32_t)0)))
		{
			goto IL_00f4;
		}
	}
	{
		int32_t L_33 = __this->get_strstart_13();
		__this->set_strstart_13(((int32_t)((int32_t)L_33+(int32_t)1)));
		goto IL_014f;
	}

IL_012d:
	{
		int32_t L_34 = __this->get_strstart_13();
		int32_t L_35 = __this->get_matchLen_10();
		__this->set_strstart_13(((int32_t)((int32_t)L_34+(int32_t)L_35)));
		int32_t L_36 = __this->get_lookahead_14();
		if ((((int32_t)L_36) < ((int32_t)2)))
		{
			goto IL_014f;
		}
	}
	{
		DeflaterEngine_UpdateHash_m2011003296(__this, /*hidden argument*/NULL);
	}

IL_014f:
	{
		__this->set_matchLen_10(2);
		bool L_37 = V_1;
		if (L_37)
		{
			goto IL_0199;
		}
	}
	{
		goto IL_01ec;
	}

IL_015e:
	{
		DeflaterHuffman_t3769756376 * L_38 = __this->get_huffman_27();
		ByteU5BU5D_t4260760469* L_39 = __this->get_window_15();
		int32_t L_40 = __this->get_strstart_13();
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, L_40);
		int32_t L_41 = L_40;
		uint8_t L_42 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		NullCheck(L_38);
		DeflaterHuffman_TallyLit_m1601839602(L_38, ((int32_t)((int32_t)L_42&(int32_t)((int32_t)255))), /*hidden argument*/NULL);
		int32_t L_43 = __this->get_strstart_13();
		__this->set_strstart_13(((int32_t)((int32_t)L_43+(int32_t)1)));
		int32_t L_44 = __this->get_lookahead_14();
		__this->set_lookahead_14(((int32_t)((int32_t)L_44-(int32_t)1)));
	}

IL_0199:
	{
		DeflaterHuffman_t3769756376 * L_45 = __this->get_huffman_27();
		NullCheck(L_45);
		bool L_46 = DeflaterHuffman_IsFull_m2958018863(L_45, /*hidden argument*/NULL);
		if (!L_46)
		{
			goto IL_01ec;
		}
	}
	{
		bool L_47 = ___finish1;
		if (!L_47)
		{
			goto IL_01b4;
		}
	}
	{
		int32_t L_48 = __this->get_lookahead_14();
		G_B27_0 = ((((int32_t)L_48) == ((int32_t)0))? 1 : 0);
		goto IL_01b5;
	}

IL_01b4:
	{
		G_B27_0 = 0;
	}

IL_01b5:
	{
		V_2 = (bool)G_B27_0;
		DeflaterHuffman_t3769756376 * L_49 = __this->get_huffman_27();
		ByteU5BU5D_t4260760469* L_50 = __this->get_window_15();
		int32_t L_51 = __this->get_blockStart_12();
		int32_t L_52 = __this->get_strstart_13();
		int32_t L_53 = __this->get_blockStart_12();
		bool L_54 = V_2;
		NullCheck(L_49);
		DeflaterHuffman_FlushBlock_m3510668965(L_49, L_50, L_51, ((int32_t)((int32_t)L_52-(int32_t)L_53)), L_54, /*hidden argument*/NULL);
		int32_t L_55 = __this->get_strstart_13();
		__this->set_blockStart_12(L_55);
		bool L_56 = V_2;
		return (bool)((((int32_t)L_56) == ((int32_t)0))? 1 : 0);
	}

IL_01ec:
	{
		int32_t L_57 = __this->get_lookahead_14();
		if ((((int32_t)L_57) >= ((int32_t)((int32_t)262))))
		{
			goto IL_0018;
		}
	}
	{
		bool L_58 = ___flush0;
		if (L_58)
		{
			goto IL_0018;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateSlow(System.Boolean,System.Boolean)
extern "C"  bool DeflaterEngine_DeflateSlow_m4017565779 (DeflaterEngine_t3684739431 * __this, bool ___flush0, bool ___finish1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	bool V_4 = false;
	int32_t G_B36_0 = 0;
	{
		int32_t L_0 = __this->get_lookahead_14();
		if ((((int32_t)L_0) >= ((int32_t)((int32_t)262))))
		{
			goto IL_0253;
		}
	}
	{
		bool L_1 = ___flush0;
		if (L_1)
		{
			goto IL_0253;
		}
	}
	{
		return (bool)0;
	}

IL_0018:
	{
		int32_t L_2 = __this->get_lookahead_14();
		if (L_2)
		{
			goto IL_0083;
		}
	}
	{
		bool L_3 = __this->get_prevAvailable_11();
		if (!L_3)
		{
			goto IL_0049;
		}
	}
	{
		DeflaterHuffman_t3769756376 * L_4 = __this->get_huffman_27();
		ByteU5BU5D_t4260760469* L_5 = __this->get_window_15();
		int32_t L_6 = __this->get_strstart_13();
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, ((int32_t)((int32_t)L_6-(int32_t)1)));
		int32_t L_7 = ((int32_t)((int32_t)L_6-(int32_t)1));
		uint8_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_4);
		DeflaterHuffman_TallyLit_m1601839602(L_4, ((int32_t)((int32_t)L_8&(int32_t)((int32_t)255))), /*hidden argument*/NULL);
	}

IL_0049:
	{
		__this->set_prevAvailable_11((bool)0);
		DeflaterHuffman_t3769756376 * L_9 = __this->get_huffman_27();
		ByteU5BU5D_t4260760469* L_10 = __this->get_window_15();
		int32_t L_11 = __this->get_blockStart_12();
		int32_t L_12 = __this->get_strstart_13();
		int32_t L_13 = __this->get_blockStart_12();
		bool L_14 = ___finish1;
		NullCheck(L_9);
		DeflaterHuffman_FlushBlock_m3510668965(L_9, L_10, L_11, ((int32_t)((int32_t)L_12-(int32_t)L_13)), L_14, /*hidden argument*/NULL);
		int32_t L_15 = __this->get_strstart_13();
		__this->set_blockStart_12(L_15);
		return (bool)0;
	}

IL_0083:
	{
		int32_t L_16 = __this->get_strstart_13();
		if ((((int32_t)L_16) < ((int32_t)((int32_t)65274))))
		{
			goto IL_0096;
		}
	}
	{
		DeflaterEngine_SlideWindow_m3591605722(__this, /*hidden argument*/NULL);
	}

IL_0096:
	{
		int32_t L_17 = __this->get_matchStart_9();
		V_0 = L_17;
		int32_t L_18 = __this->get_matchLen_10();
		V_1 = L_18;
		int32_t L_19 = __this->get_lookahead_14();
		if ((((int32_t)L_19) < ((int32_t)3)))
		{
			goto IL_010e;
		}
	}
	{
		int32_t L_20 = DeflaterEngine_InsertString_m1774160927(__this, /*hidden argument*/NULL);
		V_2 = L_20;
		int32_t L_21 = __this->get_strategy_16();
		if ((((int32_t)L_21) == ((int32_t)2)))
		{
			goto IL_010e;
		}
	}
	{
		int32_t L_22 = V_2;
		if (!L_22)
		{
			goto IL_010e;
		}
	}
	{
		int32_t L_23 = __this->get_strstart_13();
		int32_t L_24 = V_2;
		if ((((int32_t)((int32_t)((int32_t)L_23-(int32_t)L_24))) > ((int32_t)((int32_t)32506))))
		{
			goto IL_010e;
		}
	}
	{
		int32_t L_25 = V_2;
		bool L_26 = DeflaterEngine_FindLongestMatch_m707434080(__this, L_25, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_010e;
		}
	}
	{
		int32_t L_27 = __this->get_matchLen_10();
		if ((((int32_t)L_27) > ((int32_t)5)))
		{
			goto IL_010e;
		}
	}
	{
		int32_t L_28 = __this->get_strategy_16();
		if ((((int32_t)L_28) == ((int32_t)1)))
		{
			goto IL_0107;
		}
	}
	{
		int32_t L_29 = __this->get_matchLen_10();
		if ((!(((uint32_t)L_29) == ((uint32_t)3))))
		{
			goto IL_010e;
		}
	}
	{
		int32_t L_30 = __this->get_strstart_13();
		int32_t L_31 = __this->get_matchStart_9();
		if ((((int32_t)((int32_t)((int32_t)L_30-(int32_t)L_31))) <= ((int32_t)((int32_t)4096))))
		{
			goto IL_010e;
		}
	}

IL_0107:
	{
		__this->set_matchLen_10(2);
	}

IL_010e:
	{
		int32_t L_32 = V_1;
		if ((((int32_t)L_32) < ((int32_t)3)))
		{
			goto IL_0199;
		}
	}
	{
		int32_t L_33 = __this->get_matchLen_10();
		int32_t L_34 = V_1;
		if ((((int32_t)L_33) > ((int32_t)L_34)))
		{
			goto IL_0199;
		}
	}
	{
		DeflaterHuffman_t3769756376 * L_35 = __this->get_huffman_27();
		int32_t L_36 = __this->get_strstart_13();
		int32_t L_37 = V_0;
		int32_t L_38 = V_1;
		NullCheck(L_35);
		DeflaterHuffman_TallyDist_m4230142696(L_35, ((int32_t)((int32_t)((int32_t)((int32_t)L_36-(int32_t)1))-(int32_t)L_37)), L_38, /*hidden argument*/NULL);
		int32_t L_39 = V_1;
		V_1 = ((int32_t)((int32_t)L_39-(int32_t)2));
	}

IL_0139:
	{
		int32_t L_40 = __this->get_strstart_13();
		__this->set_strstart_13(((int32_t)((int32_t)L_40+(int32_t)1)));
		int32_t L_41 = __this->get_lookahead_14();
		__this->set_lookahead_14(((int32_t)((int32_t)L_41-(int32_t)1)));
		int32_t L_42 = __this->get_lookahead_14();
		if ((((int32_t)L_42) < ((int32_t)3)))
		{
			goto IL_0165;
		}
	}
	{
		DeflaterEngine_InsertString_m1774160927(__this, /*hidden argument*/NULL);
	}

IL_0165:
	{
		int32_t L_43 = V_1;
		int32_t L_44 = ((int32_t)((int32_t)L_43-(int32_t)1));
		V_1 = L_44;
		if ((((int32_t)L_44) > ((int32_t)0)))
		{
			goto IL_0139;
		}
	}
	{
		int32_t L_45 = __this->get_strstart_13();
		__this->set_strstart_13(((int32_t)((int32_t)L_45+(int32_t)1)));
		int32_t L_46 = __this->get_lookahead_14();
		__this->set_lookahead_14(((int32_t)((int32_t)L_46-(int32_t)1)));
		__this->set_prevAvailable_11((bool)0);
		__this->set_matchLen_10(2);
		goto IL_01e5;
	}

IL_0199:
	{
		bool L_47 = __this->get_prevAvailable_11();
		if (!L_47)
		{
			goto IL_01c2;
		}
	}
	{
		DeflaterHuffman_t3769756376 * L_48 = __this->get_huffman_27();
		ByteU5BU5D_t4260760469* L_49 = __this->get_window_15();
		int32_t L_50 = __this->get_strstart_13();
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, ((int32_t)((int32_t)L_50-(int32_t)1)));
		int32_t L_51 = ((int32_t)((int32_t)L_50-(int32_t)1));
		uint8_t L_52 = (L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_51));
		NullCheck(L_48);
		DeflaterHuffman_TallyLit_m1601839602(L_48, ((int32_t)((int32_t)L_52&(int32_t)((int32_t)255))), /*hidden argument*/NULL);
	}

IL_01c2:
	{
		__this->set_prevAvailable_11((bool)1);
		int32_t L_53 = __this->get_strstart_13();
		__this->set_strstart_13(((int32_t)((int32_t)L_53+(int32_t)1)));
		int32_t L_54 = __this->get_lookahead_14();
		__this->set_lookahead_14(((int32_t)((int32_t)L_54-(int32_t)1)));
	}

IL_01e5:
	{
		DeflaterHuffman_t3769756376 * L_55 = __this->get_huffman_27();
		NullCheck(L_55);
		bool L_56 = DeflaterHuffman_IsFull_m2958018863(L_55, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_0253;
		}
	}
	{
		int32_t L_57 = __this->get_strstart_13();
		int32_t L_58 = __this->get_blockStart_12();
		V_3 = ((int32_t)((int32_t)L_57-(int32_t)L_58));
		bool L_59 = __this->get_prevAvailable_11();
		if (!L_59)
		{
			goto IL_020c;
		}
	}
	{
		int32_t L_60 = V_3;
		V_3 = ((int32_t)((int32_t)L_60-(int32_t)1));
	}

IL_020c:
	{
		bool L_61 = ___finish1;
		if (!L_61)
		{
			goto IL_0222;
		}
	}
	{
		int32_t L_62 = __this->get_lookahead_14();
		if (L_62)
		{
			goto IL_0222;
		}
	}
	{
		bool L_63 = __this->get_prevAvailable_11();
		G_B36_0 = ((((int32_t)L_63) == ((int32_t)0))? 1 : 0);
		goto IL_0223;
	}

IL_0222:
	{
		G_B36_0 = 0;
	}

IL_0223:
	{
		V_4 = (bool)G_B36_0;
		DeflaterHuffman_t3769756376 * L_64 = __this->get_huffman_27();
		ByteU5BU5D_t4260760469* L_65 = __this->get_window_15();
		int32_t L_66 = __this->get_blockStart_12();
		int32_t L_67 = V_3;
		bool L_68 = V_4;
		NullCheck(L_64);
		DeflaterHuffman_FlushBlock_m3510668965(L_64, L_65, L_66, L_67, L_68, /*hidden argument*/NULL);
		int32_t L_69 = __this->get_blockStart_12();
		int32_t L_70 = V_3;
		__this->set_blockStart_12(((int32_t)((int32_t)L_69+(int32_t)L_70)));
		bool L_71 = V_4;
		return (bool)((((int32_t)L_71) == ((int32_t)0))? 1 : 0);
	}

IL_0253:
	{
		int32_t L_72 = __this->get_lookahead_14();
		if ((((int32_t)L_72) >= ((int32_t)((int32_t)262))))
		{
			goto IL_0018;
		}
	}
	{
		bool L_73 = ___flush0;
		if (L_73)
		{
			goto IL_0018;
		}
	}
	{
		return (bool)1;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::.cctor()
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern Il2CppClass* DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16U5BU5D_t801762735_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x600029dU2D1_7_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x600029dU2D2_8_FieldInfo_var;
extern const uint32_t DeflaterHuffman__cctor_m2829483505_MetadataUsageId;
extern "C"  void DeflaterHuffman__cctor_m2829483505 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterHuffman__cctor_m2829483505_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Int32U5BU5D_t3230847821* L_0 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)19)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x600029dU2D1_7_FieldInfo_var), /*hidden argument*/NULL);
		((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->set_BL_ORDER_0(L_0);
		ByteU5BU5D_t4260760469* L_1 = ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x600029dU2D2_8_FieldInfo_var), /*hidden argument*/NULL);
		((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->set_bit4Reverse_1(L_1);
		((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->set_staticLCodes_2(((Int16U5BU5D_t801762735*)SZArrayNew(Int16U5BU5D_t801762735_il2cpp_TypeInfo_var, (uint32_t)((int32_t)286))));
		((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->set_staticLLength_3(((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)((int32_t)286))));
		V_0 = 0;
		goto IL_006e;
	}

IL_0050:
	{
		Int16U5BU5D_t801762735* L_2 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_staticLCodes_2();
		int32_t L_3 = V_0;
		int32_t L_4 = V_0;
		int16_t L_5 = DeflaterHuffman_BitReverse_m2340978984(NULL /*static, unused*/, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)48)+(int32_t)L_4))<<(int32_t)8)), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (int16_t)L_5);
		ByteU5BU5D_t4260760469* L_6 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_staticLLength_3();
		int32_t L_7 = V_0;
		int32_t L_8 = L_7;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (uint8_t)8);
	}

IL_006e:
	{
		int32_t L_9 = V_0;
		if ((((int32_t)L_9) < ((int32_t)((int32_t)144))))
		{
			goto IL_0050;
		}
	}
	{
		goto IL_009a;
	}

IL_0078:
	{
		Int16U5BU5D_t801762735* L_10 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_staticLCodes_2();
		int32_t L_11 = V_0;
		int32_t L_12 = V_0;
		int16_t L_13 = DeflaterHuffman_BitReverse_m2340978984(NULL /*static, unused*/, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)256)+(int32_t)L_12))<<(int32_t)7)), /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_11), (int16_t)L_13);
		ByteU5BU5D_t4260760469* L_14 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_staticLLength_3();
		int32_t L_15 = V_0;
		int32_t L_16 = L_15;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_16);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (uint8_t)((int32_t)9));
	}

IL_009a:
	{
		int32_t L_17 = V_0;
		if ((((int32_t)L_17) < ((int32_t)((int32_t)256))))
		{
			goto IL_0078;
		}
	}
	{
		goto IL_00c6;
	}

IL_00a4:
	{
		Int16U5BU5D_t801762735* L_18 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_staticLCodes_2();
		int32_t L_19 = V_0;
		int32_t L_20 = V_0;
		int16_t L_21 = DeflaterHuffman_BitReverse_m2340978984(NULL /*static, unused*/, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-256)+(int32_t)L_20))<<(int32_t)((int32_t)9))), /*hidden argument*/NULL);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_19), (int16_t)L_21);
		ByteU5BU5D_t4260760469* L_22 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_staticLLength_3();
		int32_t L_23 = V_0;
		int32_t L_24 = L_23;
		V_0 = ((int32_t)((int32_t)L_24+(int32_t)1));
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_24);
		(L_22)->SetAt(static_cast<il2cpp_array_size_t>(L_24), (uint8_t)7);
	}

IL_00c6:
	{
		int32_t L_25 = V_0;
		if ((((int32_t)L_25) < ((int32_t)((int32_t)280))))
		{
			goto IL_00a4;
		}
	}
	{
		goto IL_00ee;
	}

IL_00d0:
	{
		Int16U5BU5D_t801762735* L_26 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_staticLCodes_2();
		int32_t L_27 = V_0;
		int32_t L_28 = V_0;
		int16_t L_29 = DeflaterHuffman_BitReverse_m2340978984(NULL /*static, unused*/, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)-88)+(int32_t)L_28))<<(int32_t)8)), /*hidden argument*/NULL);
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(L_27), (int16_t)L_29);
		ByteU5BU5D_t4260760469* L_30 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_staticLLength_3();
		int32_t L_31 = V_0;
		int32_t L_32 = L_31;
		V_0 = ((int32_t)((int32_t)L_32+(int32_t)1));
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, L_32);
		(L_30)->SetAt(static_cast<il2cpp_array_size_t>(L_32), (uint8_t)8);
	}

IL_00ee:
	{
		int32_t L_33 = V_0;
		if ((((int32_t)L_33) < ((int32_t)((int32_t)286))))
		{
			goto IL_00d0;
		}
	}
	{
		((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->set_staticDCodes_4(((Int16U5BU5D_t801762735*)SZArrayNew(Int16U5BU5D_t801762735_il2cpp_TypeInfo_var, (uint32_t)((int32_t)30))));
		((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->set_staticDLength_5(((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)((int32_t)30))));
		V_0 = 0;
		goto IL_012e;
	}

IL_0112:
	{
		Int16U5BU5D_t801762735* L_34 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_staticDCodes_4();
		int32_t L_35 = V_0;
		int32_t L_36 = V_0;
		int16_t L_37 = DeflaterHuffman_BitReverse_m2340978984(NULL /*static, unused*/, ((int32_t)((int32_t)L_36<<(int32_t)((int32_t)11))), /*hidden argument*/NULL);
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_35);
		(L_34)->SetAt(static_cast<il2cpp_array_size_t>(L_35), (int16_t)L_37);
		ByteU5BU5D_t4260760469* L_38 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_staticDLength_5();
		int32_t L_39 = V_0;
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, L_39);
		(L_38)->SetAt(static_cast<il2cpp_array_size_t>(L_39), (uint8_t)5);
		int32_t L_40 = V_0;
		V_0 = ((int32_t)((int32_t)L_40+(int32_t)1));
	}

IL_012e:
	{
		int32_t L_41 = V_0;
		if ((((int32_t)L_41) < ((int32_t)((int32_t)30))))
		{
			goto IL_0112;
		}
	}
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::.ctor(ICSharpCode.SharpZipLib.Zip.Compression.DeflaterPending)
extern Il2CppClass* Tree_t1054057453_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16U5BU5D_t801762735_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern const uint32_t DeflaterHuffman__ctor_m211969164_MetadataUsageId;
extern "C"  void DeflaterHuffman__ctor_m211969164 (DeflaterHuffman_t3769756376 * __this, DeflaterPending_t1829109954 * ___pending0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterHuffman__ctor_m211969164_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		DeflaterPending_t1829109954 * L_0 = ___pending0;
		__this->set_pending_6(L_0);
		Tree_t1054057453 * L_1 = (Tree_t1054057453 *)il2cpp_codegen_object_new(Tree_t1054057453_il2cpp_TypeInfo_var);
		Tree__ctor_m3029332514(L_1, __this, ((int32_t)286), ((int32_t)257), ((int32_t)15), /*hidden argument*/NULL);
		__this->set_literalTree_7(L_1);
		Tree_t1054057453 * L_2 = (Tree_t1054057453 *)il2cpp_codegen_object_new(Tree_t1054057453_il2cpp_TypeInfo_var);
		Tree__ctor_m3029332514(L_2, __this, ((int32_t)30), 1, ((int32_t)15), /*hidden argument*/NULL);
		__this->set_distTree_8(L_2);
		Tree_t1054057453 * L_3 = (Tree_t1054057453 *)il2cpp_codegen_object_new(Tree_t1054057453_il2cpp_TypeInfo_var);
		Tree__ctor_m3029332514(L_3, __this, ((int32_t)19), 4, 7, /*hidden argument*/NULL);
		__this->set_blTree_9(L_3);
		__this->set_d_buf_10(((Int16U5BU5D_t801762735*)SZArrayNew(Int16U5BU5D_t801762735_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16384))));
		__this->set_l_buf_11(((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16384))));
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::Reset()
extern "C"  void DeflaterHuffman_Reset_m1909678057 (DeflaterHuffman_t3769756376 * __this, const MethodInfo* method)
{
	{
		__this->set_last_lit_12(0);
		__this->set_extra_bits_13(0);
		Tree_t1054057453 * L_0 = __this->get_literalTree_7();
		NullCheck(L_0);
		Tree_Reset_m757761098(L_0, /*hidden argument*/NULL);
		Tree_t1054057453 * L_1 = __this->get_distTree_8();
		NullCheck(L_1);
		Tree_Reset_m757761098(L_1, /*hidden argument*/NULL);
		Tree_t1054057453 * L_2 = __this->get_blTree_9();
		NullCheck(L_2);
		Tree_Reset_m757761098(L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::SendAllTrees(System.Int32)
extern Il2CppClass* DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var;
extern const uint32_t DeflaterHuffman_SendAllTrees_m1062228213_MetadataUsageId;
extern "C"  void DeflaterHuffman_SendAllTrees_m1062228213 (DeflaterHuffman_t3769756376 * __this, int32_t ___blTreeCodes0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterHuffman_SendAllTrees_m1062228213_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Tree_t1054057453 * L_0 = __this->get_blTree_9();
		NullCheck(L_0);
		Tree_BuildCodes_m614041567(L_0, /*hidden argument*/NULL);
		Tree_t1054057453 * L_1 = __this->get_literalTree_7();
		NullCheck(L_1);
		Tree_BuildCodes_m614041567(L_1, /*hidden argument*/NULL);
		Tree_t1054057453 * L_2 = __this->get_distTree_8();
		NullCheck(L_2);
		Tree_BuildCodes_m614041567(L_2, /*hidden argument*/NULL);
		DeflaterPending_t1829109954 * L_3 = __this->get_pending_6();
		Tree_t1054057453 * L_4 = __this->get_literalTree_7();
		NullCheck(L_4);
		int32_t L_5 = L_4->get_numCodes_3();
		NullCheck(L_3);
		PendingBuffer_WriteBits_m4156628504(L_3, ((int32_t)((int32_t)L_5-(int32_t)((int32_t)257))), 5, /*hidden argument*/NULL);
		DeflaterPending_t1829109954 * L_6 = __this->get_pending_6();
		Tree_t1054057453 * L_7 = __this->get_distTree_8();
		NullCheck(L_7);
		int32_t L_8 = L_7->get_numCodes_3();
		NullCheck(L_6);
		PendingBuffer_WriteBits_m4156628504(L_6, ((int32_t)((int32_t)L_8-(int32_t)1)), 5, /*hidden argument*/NULL);
		DeflaterPending_t1829109954 * L_9 = __this->get_pending_6();
		int32_t L_10 = ___blTreeCodes0;
		NullCheck(L_9);
		PendingBuffer_WriteBits_m4156628504(L_9, ((int32_t)((int32_t)L_10-(int32_t)4)), 4, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_008d;
	}

IL_006a:
	{
		DeflaterPending_t1829109954 * L_11 = __this->get_pending_6();
		Tree_t1054057453 * L_12 = __this->get_blTree_9();
		NullCheck(L_12);
		ByteU5BU5D_t4260760469* L_13 = L_12->get_length_1();
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var);
		Int32U5BU5D_t3230847821* L_14 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_BL_ORDER_0();
		int32_t L_15 = V_0;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, L_15);
		int32_t L_16 = L_15;
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_17);
		int32_t L_18 = L_17;
		uint8_t L_19 = (L_13)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_11);
		PendingBuffer_WriteBits_m4156628504(L_11, L_19, 3, /*hidden argument*/NULL);
		int32_t L_20 = V_0;
		V_0 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_008d:
	{
		int32_t L_21 = V_0;
		int32_t L_22 = ___blTreeCodes0;
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_006a;
		}
	}
	{
		Tree_t1054057453 * L_23 = __this->get_literalTree_7();
		Tree_t1054057453 * L_24 = __this->get_blTree_9();
		NullCheck(L_23);
		Tree_WriteTree_m1704667083(L_23, L_24, /*hidden argument*/NULL);
		Tree_t1054057453 * L_25 = __this->get_distTree_8();
		Tree_t1054057453 * L_26 = __this->get_blTree_9();
		NullCheck(L_25);
		Tree_WriteTree_m1704667083(L_25, L_26, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::CompressBlock()
extern Il2CppClass* DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var;
extern const uint32_t DeflaterHuffman_CompressBlock_m2252554469_MetadataUsageId;
extern "C"  void DeflaterHuffman_CompressBlock_m2252554469 (DeflaterHuffman_t3769756376 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterHuffman_CompressBlock_m2252554469_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		V_0 = 0;
		goto IL_00b2;
	}

IL_0007:
	{
		ByteU5BU5D_t4260760469* L_0 = __this->get_l_buf_11();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		uint8_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_1 = ((int32_t)((int32_t)L_3&(int32_t)((int32_t)255)));
		Int16U5BU5D_t801762735* L_4 = __this->get_d_buf_10();
		int32_t L_5 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		int16_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_2 = L_7;
		int32_t L_8 = V_2;
		int32_t L_9 = L_8;
		V_2 = ((int32_t)((int32_t)L_9-(int32_t)1));
		if (!L_9)
		{
			goto IL_00a2;
		}
	}
	{
		int32_t L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var);
		int32_t L_11 = DeflaterHuffman_Lcode_m114808528(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		V_3 = L_11;
		Tree_t1054057453 * L_12 = __this->get_literalTree_7();
		int32_t L_13 = V_3;
		NullCheck(L_12);
		Tree_WriteSymbol_m4024054307(L_12, L_13, /*hidden argument*/NULL);
		int32_t L_14 = V_3;
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)L_14-(int32_t)((int32_t)261)))/(int32_t)4));
		int32_t L_15 = V_4;
		if ((((int32_t)L_15) <= ((int32_t)0)))
		{
			goto IL_0066;
		}
	}
	{
		int32_t L_16 = V_4;
		if ((((int32_t)L_16) > ((int32_t)5)))
		{
			goto IL_0066;
		}
	}
	{
		DeflaterPending_t1829109954 * L_17 = __this->get_pending_6();
		int32_t L_18 = V_1;
		int32_t L_19 = V_4;
		int32_t L_20 = V_4;
		NullCheck(L_17);
		PendingBuffer_WriteBits_m4156628504(L_17, ((int32_t)((int32_t)L_18&(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_19&(int32_t)((int32_t)31)))))-(int32_t)1)))), L_20, /*hidden argument*/NULL);
	}

IL_0066:
	{
		int32_t L_21 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var);
		int32_t L_22 = DeflaterHuffman_Dcode_m2575616200(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		V_5 = L_22;
		Tree_t1054057453 * L_23 = __this->get_distTree_8();
		int32_t L_24 = V_5;
		NullCheck(L_23);
		Tree_WriteSymbol_m4024054307(L_23, L_24, /*hidden argument*/NULL);
		int32_t L_25 = V_5;
		V_4 = ((int32_t)((int32_t)((int32_t)((int32_t)L_25/(int32_t)2))-(int32_t)1));
		int32_t L_26 = V_4;
		if ((((int32_t)L_26) <= ((int32_t)0)))
		{
			goto IL_00ae;
		}
	}
	{
		DeflaterPending_t1829109954 * L_27 = __this->get_pending_6();
		int32_t L_28 = V_2;
		int32_t L_29 = V_4;
		int32_t L_30 = V_4;
		NullCheck(L_27);
		PendingBuffer_WriteBits_m4156628504(L_27, ((int32_t)((int32_t)L_28&(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_29&(int32_t)((int32_t)31)))))-(int32_t)1)))), L_30, /*hidden argument*/NULL);
		goto IL_00ae;
	}

IL_00a2:
	{
		Tree_t1054057453 * L_31 = __this->get_literalTree_7();
		int32_t L_32 = V_1;
		NullCheck(L_31);
		Tree_WriteSymbol_m4024054307(L_31, L_32, /*hidden argument*/NULL);
	}

IL_00ae:
	{
		int32_t L_33 = V_0;
		V_0 = ((int32_t)((int32_t)L_33+(int32_t)1));
	}

IL_00b2:
	{
		int32_t L_34 = V_0;
		int32_t L_35 = __this->get_last_lit_12();
		if ((((int32_t)L_34) < ((int32_t)L_35)))
		{
			goto IL_0007;
		}
	}
	{
		Tree_t1054057453 * L_36 = __this->get_literalTree_7();
		NullCheck(L_36);
		Tree_WriteSymbol_m4024054307(L_36, ((int32_t)256), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::FlushStoredBlock(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern "C"  void DeflaterHuffman_FlushStoredBlock_m2383394690 (DeflaterHuffman_t3769756376 * __this, ByteU5BU5D_t4260760469* ___stored0, int32_t ___storedOffset1, int32_t ___storedLength2, bool ___lastBlock3, const MethodInfo* method)
{
	DeflaterPending_t1829109954 * G_B2_0 = NULL;
	DeflaterPending_t1829109954 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	DeflaterPending_t1829109954 * G_B3_1 = NULL;
	{
		DeflaterPending_t1829109954 * L_0 = __this->get_pending_6();
		bool L_1 = ___lastBlock3;
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_000d;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_000e;
	}

IL_000d:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_000e:
	{
		NullCheck(G_B3_1);
		PendingBuffer_WriteBits_m4156628504(G_B3_1, G_B3_0, 3, /*hidden argument*/NULL);
		DeflaterPending_t1829109954 * L_2 = __this->get_pending_6();
		NullCheck(L_2);
		PendingBuffer_AlignToByte_m2130252305(L_2, /*hidden argument*/NULL);
		DeflaterPending_t1829109954 * L_3 = __this->get_pending_6();
		int32_t L_4 = ___storedLength2;
		NullCheck(L_3);
		PendingBuffer_WriteShort_m1294929767(L_3, L_4, /*hidden argument*/NULL);
		DeflaterPending_t1829109954 * L_5 = __this->get_pending_6();
		int32_t L_6 = ___storedLength2;
		NullCheck(L_5);
		PendingBuffer_WriteShort_m1294929767(L_5, ((~L_6)), /*hidden argument*/NULL);
		DeflaterPending_t1829109954 * L_7 = __this->get_pending_6();
		ByteU5BU5D_t4260760469* L_8 = ___stored0;
		int32_t L_9 = ___storedOffset1;
		int32_t L_10 = ___storedLength2;
		NullCheck(L_7);
		PendingBuffer_WriteBlock_m2658962210(L_7, L_8, L_9, L_10, /*hidden argument*/NULL);
		DeflaterHuffman_Reset_m1909678057(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::FlushBlock(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern Il2CppClass* DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var;
extern const uint32_t DeflaterHuffman_FlushBlock_m3510668965_MetadataUsageId;
extern "C"  void DeflaterHuffman_FlushBlock_m3510668965 (DeflaterHuffman_t3769756376 * __this, ByteU5BU5D_t4260760469* ___stored0, int32_t ___storedOffset1, int32_t ___storedLength2, bool ___lastBlock3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterHuffman_FlushBlock_m3510668965_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t G_B19_0 = 0;
	DeflaterPending_t1829109954 * G_B19_1 = NULL;
	int32_t G_B18_0 = 0;
	DeflaterPending_t1829109954 * G_B18_1 = NULL;
	int32_t G_B20_0 = 0;
	int32_t G_B20_1 = 0;
	DeflaterPending_t1829109954 * G_B20_2 = NULL;
	int32_t G_B23_0 = 0;
	DeflaterPending_t1829109954 * G_B23_1 = NULL;
	int32_t G_B22_0 = 0;
	DeflaterPending_t1829109954 * G_B22_1 = NULL;
	int32_t G_B24_0 = 0;
	int32_t G_B24_1 = 0;
	DeflaterPending_t1829109954 * G_B24_2 = NULL;
	{
		Tree_t1054057453 * L_0 = __this->get_literalTree_7();
		NullCheck(L_0);
		Int16U5BU5D_t801762735* L_1 = L_0->get_freqs_0();
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, ((int32_t)256));
		int16_t* L_2 = ((L_1)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)256))));
		(*(int16_t*)L_2) = (((int16_t)((int16_t)((int32_t)((int32_t)(*(int16_t*)L_2)+(int32_t)1)))));
		Tree_t1054057453 * L_3 = __this->get_literalTree_7();
		NullCheck(L_3);
		Tree_BuildTree_m3557342695(L_3, /*hidden argument*/NULL);
		Tree_t1054057453 * L_4 = __this->get_distTree_8();
		NullCheck(L_4);
		Tree_BuildTree_m3557342695(L_4, /*hidden argument*/NULL);
		Tree_t1054057453 * L_5 = __this->get_literalTree_7();
		Tree_t1054057453 * L_6 = __this->get_blTree_9();
		NullCheck(L_5);
		Tree_CalcBLFreq_m2156942961(L_5, L_6, /*hidden argument*/NULL);
		Tree_t1054057453 * L_7 = __this->get_distTree_8();
		Tree_t1054057453 * L_8 = __this->get_blTree_9();
		NullCheck(L_7);
		Tree_CalcBLFreq_m2156942961(L_7, L_8, /*hidden argument*/NULL);
		Tree_t1054057453 * L_9 = __this->get_blTree_9();
		NullCheck(L_9);
		Tree_BuildTree_m3557342695(L_9, /*hidden argument*/NULL);
		V_0 = 4;
		V_1 = ((int32_t)18);
		goto IL_008b;
	}

IL_006d:
	{
		Tree_t1054057453 * L_10 = __this->get_blTree_9();
		NullCheck(L_10);
		ByteU5BU5D_t4260760469* L_11 = L_10->get_length_1();
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var);
		Int32U5BU5D_t3230847821* L_12 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_BL_ORDER_0();
		int32_t L_13 = V_1;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
		int32_t L_14 = L_13;
		int32_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_15);
		int32_t L_16 = L_15;
		uint8_t L_17 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		if ((((int32_t)L_17) <= ((int32_t)0)))
		{
			goto IL_0087;
		}
	}
	{
		int32_t L_18 = V_1;
		V_0 = ((int32_t)((int32_t)L_18+(int32_t)1));
	}

IL_0087:
	{
		int32_t L_19 = V_1;
		V_1 = ((int32_t)((int32_t)L_19-(int32_t)1));
	}

IL_008b:
	{
		int32_t L_20 = V_1;
		int32_t L_21 = V_0;
		if ((((int32_t)L_20) > ((int32_t)L_21)))
		{
			goto IL_006d;
		}
	}
	{
		int32_t L_22 = V_0;
		Tree_t1054057453 * L_23 = __this->get_blTree_9();
		NullCheck(L_23);
		int32_t L_24 = Tree_GetEncodedLength_m866994417(L_23, /*hidden argument*/NULL);
		Tree_t1054057453 * L_25 = __this->get_literalTree_7();
		NullCheck(L_25);
		int32_t L_26 = Tree_GetEncodedLength_m866994417(L_25, /*hidden argument*/NULL);
		Tree_t1054057453 * L_27 = __this->get_distTree_8();
		NullCheck(L_27);
		int32_t L_28 = Tree_GetEncodedLength_m866994417(L_27, /*hidden argument*/NULL);
		int32_t L_29 = __this->get_extra_bits_13();
		V_2 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)14)+(int32_t)((int32_t)((int32_t)L_22*(int32_t)3))))+(int32_t)L_24))+(int32_t)L_26))+(int32_t)L_28))+(int32_t)L_29));
		int32_t L_30 = __this->get_extra_bits_13();
		V_3 = L_30;
		V_4 = 0;
		goto IL_00ed;
	}

IL_00cd:
	{
		int32_t L_31 = V_3;
		Tree_t1054057453 * L_32 = __this->get_literalTree_7();
		NullCheck(L_32);
		Int16U5BU5D_t801762735* L_33 = L_32->get_freqs_0();
		int32_t L_34 = V_4;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_34);
		int32_t L_35 = L_34;
		int16_t L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var);
		ByteU5BU5D_t4260760469* L_37 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_staticLLength_3();
		int32_t L_38 = V_4;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, L_38);
		int32_t L_39 = L_38;
		uint8_t L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		V_3 = ((int32_t)((int32_t)L_31+(int32_t)((int32_t)((int32_t)L_36*(int32_t)L_40))));
		int32_t L_41 = V_4;
		V_4 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_00ed:
	{
		int32_t L_42 = V_4;
		if ((((int32_t)L_42) < ((int32_t)((int32_t)286))))
		{
			goto IL_00cd;
		}
	}
	{
		V_5 = 0;
		goto IL_011b;
	}

IL_00fb:
	{
		int32_t L_43 = V_3;
		Tree_t1054057453 * L_44 = __this->get_distTree_8();
		NullCheck(L_44);
		Int16U5BU5D_t801762735* L_45 = L_44->get_freqs_0();
		int32_t L_46 = V_5;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		int32_t L_47 = L_46;
		int16_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var);
		ByteU5BU5D_t4260760469* L_49 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_staticDLength_5();
		int32_t L_50 = V_5;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, L_50);
		int32_t L_51 = L_50;
		uint8_t L_52 = (L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_51));
		V_3 = ((int32_t)((int32_t)L_43+(int32_t)((int32_t)((int32_t)L_48*(int32_t)L_52))));
		int32_t L_53 = V_5;
		V_5 = ((int32_t)((int32_t)L_53+(int32_t)1));
	}

IL_011b:
	{
		int32_t L_54 = V_5;
		if ((((int32_t)L_54) < ((int32_t)((int32_t)30))))
		{
			goto IL_00fb;
		}
	}
	{
		int32_t L_55 = V_2;
		int32_t L_56 = V_3;
		if ((((int32_t)L_55) < ((int32_t)L_56)))
		{
			goto IL_0127;
		}
	}
	{
		int32_t L_57 = V_3;
		V_2 = L_57;
	}

IL_0127:
	{
		int32_t L_58 = ___storedOffset1;
		if ((((int32_t)L_58) < ((int32_t)0)))
		{
			goto IL_013f;
		}
	}
	{
		int32_t L_59 = ___storedLength2;
		int32_t L_60 = V_2;
		if ((((int32_t)((int32_t)((int32_t)L_59+(int32_t)4))) >= ((int32_t)((int32_t)((int32_t)L_60>>(int32_t)3)))))
		{
			goto IL_013f;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_61 = ___stored0;
		int32_t L_62 = ___storedOffset1;
		int32_t L_63 = ___storedLength2;
		bool L_64 = ___lastBlock3;
		DeflaterHuffman_FlushStoredBlock_m2383394690(__this, L_61, L_62, L_63, L_64, /*hidden argument*/NULL);
		return;
	}

IL_013f:
	{
		int32_t L_65 = V_2;
		int32_t L_66 = V_3;
		if ((!(((uint32_t)L_65) == ((uint32_t)L_66))))
		{
			goto IL_0190;
		}
	}
	{
		DeflaterPending_t1829109954 * L_67 = __this->get_pending_6();
		bool L_68 = ___lastBlock3;
		G_B18_0 = 2;
		G_B18_1 = L_67;
		if (L_68)
		{
			G_B19_0 = 2;
			G_B19_1 = L_67;
			goto IL_0151;
		}
	}
	{
		G_B20_0 = 0;
		G_B20_1 = G_B18_0;
		G_B20_2 = G_B18_1;
		goto IL_0152;
	}

IL_0151:
	{
		G_B20_0 = 1;
		G_B20_1 = G_B19_0;
		G_B20_2 = G_B19_1;
	}

IL_0152:
	{
		NullCheck(G_B20_2);
		PendingBuffer_WriteBits_m4156628504(G_B20_2, ((int32_t)((int32_t)G_B20_1+(int32_t)G_B20_0)), 3, /*hidden argument*/NULL);
		Tree_t1054057453 * L_69 = __this->get_literalTree_7();
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var);
		Int16U5BU5D_t801762735* L_70 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_staticLCodes_2();
		ByteU5BU5D_t4260760469* L_71 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_staticLLength_3();
		NullCheck(L_69);
		Tree_SetStaticCodes_m469290505(L_69, L_70, L_71, /*hidden argument*/NULL);
		Tree_t1054057453 * L_72 = __this->get_distTree_8();
		Int16U5BU5D_t801762735* L_73 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_staticDCodes_4();
		ByteU5BU5D_t4260760469* L_74 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_staticDLength_5();
		NullCheck(L_72);
		Tree_SetStaticCodes_m469290505(L_72, L_73, L_74, /*hidden argument*/NULL);
		DeflaterHuffman_CompressBlock_m2252554469(__this, /*hidden argument*/NULL);
		DeflaterHuffman_Reset_m1909678057(__this, /*hidden argument*/NULL);
		return;
	}

IL_0190:
	{
		DeflaterPending_t1829109954 * L_75 = __this->get_pending_6();
		bool L_76 = ___lastBlock3;
		G_B22_0 = 4;
		G_B22_1 = L_75;
		if (L_76)
		{
			G_B23_0 = 4;
			G_B23_1 = L_75;
			goto IL_019e;
		}
	}
	{
		G_B24_0 = 0;
		G_B24_1 = G_B22_0;
		G_B24_2 = G_B22_1;
		goto IL_019f;
	}

IL_019e:
	{
		G_B24_0 = 1;
		G_B24_1 = G_B23_0;
		G_B24_2 = G_B23_1;
	}

IL_019f:
	{
		NullCheck(G_B24_2);
		PendingBuffer_WriteBits_m4156628504(G_B24_2, ((int32_t)((int32_t)G_B24_1+(int32_t)G_B24_0)), 3, /*hidden argument*/NULL);
		int32_t L_77 = V_0;
		DeflaterHuffman_SendAllTrees_m1062228213(__this, L_77, /*hidden argument*/NULL);
		DeflaterHuffman_CompressBlock_m2252554469(__this, /*hidden argument*/NULL);
		DeflaterHuffman_Reset_m1909678057(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::IsFull()
extern "C"  bool DeflaterHuffman_IsFull_m2958018863 (DeflaterHuffman_t3769756376 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_last_lit_12();
		return (bool)((((int32_t)((((int32_t)L_0) < ((int32_t)((int32_t)16384)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::TallyLit(System.Int32)
extern "C"  bool DeflaterHuffman_TallyLit_m1601839602 (DeflaterHuffman_t3769756376 * __this, int32_t ___literal0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		Int16U5BU5D_t801762735* L_0 = __this->get_d_buf_10();
		int32_t L_1 = __this->get_last_lit_12();
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (int16_t)0);
		ByteU5BU5D_t4260760469* L_2 = __this->get_l_buf_11();
		int32_t L_3 = __this->get_last_lit_12();
		int32_t L_4 = L_3;
		V_0 = L_4;
		__this->set_last_lit_12(((int32_t)((int32_t)L_4+(int32_t)1)));
		int32_t L_5 = V_0;
		int32_t L_6 = ___literal0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_5);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (uint8_t)(((int32_t)((uint8_t)L_6))));
		Tree_t1054057453 * L_7 = __this->get_literalTree_7();
		NullCheck(L_7);
		Int16U5BU5D_t801762735* L_8 = L_7->get_freqs_0();
		int32_t L_9 = ___literal0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int16_t* L_10 = ((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)));
		(*(int16_t*)L_10) = (((int16_t)((int16_t)((int32_t)((int32_t)(*(int16_t*)L_10)+(int32_t)1)))));
		bool L_11 = DeflaterHuffman_IsFull_m2958018863(__this, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::TallyDist(System.Int32,System.Int32)
extern Il2CppClass* DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var;
extern const uint32_t DeflaterHuffman_TallyDist_m4230142696_MetadataUsageId;
extern "C"  bool DeflaterHuffman_TallyDist_m4230142696 (DeflaterHuffman_t3769756376 * __this, int32_t ___distance0, int32_t ___length1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterHuffman_TallyDist_m4230142696_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		Int16U5BU5D_t801762735* L_0 = __this->get_d_buf_10();
		int32_t L_1 = __this->get_last_lit_12();
		int32_t L_2 = ___distance0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (int16_t)(((int16_t)((int16_t)L_2))));
		ByteU5BU5D_t4260760469* L_3 = __this->get_l_buf_11();
		int32_t L_4 = __this->get_last_lit_12();
		int32_t L_5 = L_4;
		V_2 = L_5;
		__this->set_last_lit_12(((int32_t)((int32_t)L_5+(int32_t)1)));
		int32_t L_6 = V_2;
		int32_t L_7 = ___length1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_7-(int32_t)3))))));
		int32_t L_8 = ___length1;
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var);
		int32_t L_9 = DeflaterHuffman_Lcode_m114808528(NULL /*static, unused*/, ((int32_t)((int32_t)L_8-(int32_t)3)), /*hidden argument*/NULL);
		V_0 = L_9;
		Tree_t1054057453 * L_10 = __this->get_literalTree_7();
		NullCheck(L_10);
		Int16U5BU5D_t801762735* L_11 = L_10->get_freqs_0();
		int32_t L_12 = V_0;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int16_t* L_13 = ((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)));
		(*(int16_t*)L_13) = (((int16_t)((int16_t)((int32_t)((int32_t)(*(int16_t*)L_13)+(int32_t)1)))));
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) < ((int32_t)((int32_t)265))))
		{
			goto IL_0079;
		}
	}
	{
		int32_t L_15 = V_0;
		if ((((int32_t)L_15) >= ((int32_t)((int32_t)285))))
		{
			goto IL_0079;
		}
	}
	{
		int32_t L_16 = __this->get_extra_bits_13();
		int32_t L_17 = V_0;
		__this->set_extra_bits_13(((int32_t)((int32_t)L_16+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_17-(int32_t)((int32_t)261)))/(int32_t)4)))));
	}

IL_0079:
	{
		int32_t L_18 = ___distance0;
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var);
		int32_t L_19 = DeflaterHuffman_Dcode_m2575616200(NULL /*static, unused*/, ((int32_t)((int32_t)L_18-(int32_t)1)), /*hidden argument*/NULL);
		V_1 = L_19;
		Tree_t1054057453 * L_20 = __this->get_distTree_8();
		NullCheck(L_20);
		Int16U5BU5D_t801762735* L_21 = L_20->get_freqs_0();
		int32_t L_22 = V_1;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int16_t* L_23 = ((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_22)));
		(*(int16_t*)L_23) = (((int16_t)((int16_t)((int32_t)((int32_t)(*(int16_t*)L_23)+(int32_t)1)))));
		int32_t L_24 = V_1;
		if ((((int32_t)L_24) < ((int32_t)4)))
		{
			goto IL_00b7;
		}
	}
	{
		int32_t L_25 = __this->get_extra_bits_13();
		int32_t L_26 = V_1;
		__this->set_extra_bits_13(((int32_t)((int32_t)L_25+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_26/(int32_t)2))-(int32_t)1)))));
	}

IL_00b7:
	{
		bool L_27 = DeflaterHuffman_IsFull_m2958018863(__this, /*hidden argument*/NULL);
		return L_27;
	}
}
// System.Int16 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::BitReverse(System.Int32)
extern Il2CppClass* DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var;
extern const uint32_t DeflaterHuffman_BitReverse_m2340978984_MetadataUsageId;
extern "C"  int16_t DeflaterHuffman_BitReverse_m2340978984 (Il2CppObject * __this /* static, unused */, int32_t ___toReverse0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterHuffman_BitReverse_m2340978984_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var);
		ByteU5BU5D_t4260760469* L_0 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_bit4Reverse_1();
		int32_t L_1 = ___toReverse0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, ((int32_t)((int32_t)L_1&(int32_t)((int32_t)15))));
		int32_t L_2 = ((int32_t)((int32_t)L_1&(int32_t)((int32_t)15)));
		uint8_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		ByteU5BU5D_t4260760469* L_4 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_bit4Reverse_1();
		int32_t L_5 = ___toReverse0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, ((int32_t)((int32_t)((int32_t)((int32_t)L_5>>(int32_t)4))&(int32_t)((int32_t)15))));
		int32_t L_6 = ((int32_t)((int32_t)((int32_t)((int32_t)L_5>>(int32_t)4))&(int32_t)((int32_t)15)));
		uint8_t L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		ByteU5BU5D_t4260760469* L_8 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_bit4Reverse_1();
		int32_t L_9 = ___toReverse0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, ((int32_t)((int32_t)((int32_t)((int32_t)L_9>>(int32_t)8))&(int32_t)((int32_t)15))));
		int32_t L_10 = ((int32_t)((int32_t)((int32_t)((int32_t)L_9>>(int32_t)8))&(int32_t)((int32_t)15)));
		uint8_t L_11 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_10));
		ByteU5BU5D_t4260760469* L_12 = ((DeflaterHuffman_t3769756376_StaticFields*)DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var->static_fields)->get_bit4Reverse_1();
		int32_t L_13 = ___toReverse0;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, ((int32_t)((int32_t)L_13>>(int32_t)((int32_t)12))));
		int32_t L_14 = ((int32_t)((int32_t)L_13>>(int32_t)((int32_t)12)));
		uint8_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		return (((int16_t)((int16_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_3<<(int32_t)((int32_t)12)))|(int32_t)((int32_t)((int32_t)L_7<<(int32_t)8))))|(int32_t)((int32_t)((int32_t)L_11<<(int32_t)4))))|(int32_t)L_15)))));
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::Lcode(System.Int32)
extern "C"  int32_t DeflaterHuffman_Lcode_m114808528 (Il2CppObject * __this /* static, unused */, int32_t ___length0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___length0;
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)255)))))
		{
			goto IL_000e;
		}
	}
	{
		return ((int32_t)285);
	}

IL_000e:
	{
		V_0 = ((int32_t)257);
		goto IL_001f;
	}

IL_0016:
	{
		int32_t L_1 = V_0;
		V_0 = ((int32_t)((int32_t)L_1+(int32_t)4));
		int32_t L_2 = ___length0;
		___length0 = ((int32_t)((int32_t)L_2>>(int32_t)1));
	}

IL_001f:
	{
		int32_t L_3 = ___length0;
		if ((((int32_t)L_3) >= ((int32_t)8)))
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_4 = V_0;
		int32_t L_5 = ___length0;
		return ((int32_t)((int32_t)L_4+(int32_t)L_5));
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman::Dcode(System.Int32)
extern "C"  int32_t DeflaterHuffman_Dcode_m2575616200 (Il2CppObject * __this /* static, unused */, int32_t ___distance0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_000d;
	}

IL_0004:
	{
		int32_t L_0 = V_0;
		V_0 = ((int32_t)((int32_t)L_0+(int32_t)2));
		int32_t L_1 = ___distance0;
		___distance0 = ((int32_t)((int32_t)L_1>>(int32_t)1));
	}

IL_000d:
	{
		int32_t L_2 = ___distance0;
		if ((((int32_t)L_2) >= ((int32_t)4)))
		{
			goto IL_0004;
		}
	}
	{
		int32_t L_3 = V_0;
		int32_t L_4 = ___distance0;
		return ((int32_t)((int32_t)L_3+(int32_t)L_4));
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::.ctor(ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman,System.Int32,System.Int32,System.Int32)
extern Il2CppClass* Int16U5BU5D_t801762735_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern const uint32_t Tree__ctor_m3029332514_MetadataUsageId;
extern "C"  void Tree__ctor_m3029332514 (Tree_t1054057453 * __this, DeflaterHuffman_t3769756376 * ___dh0, int32_t ___elems1, int32_t ___minCodes2, int32_t ___maxLength3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Tree__ctor_m3029332514_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		DeflaterHuffman_t3769756376 * L_0 = ___dh0;
		__this->set_dh_7(L_0);
		int32_t L_1 = ___minCodes2;
		__this->set_minNumCodes_2(L_1);
		int32_t L_2 = ___maxLength3;
		__this->set_maxLength_6(L_2);
		int32_t L_3 = ___elems1;
		__this->set_freqs_0(((Int16U5BU5D_t801762735*)SZArrayNew(Int16U5BU5D_t801762735_il2cpp_TypeInfo_var, (uint32_t)L_3)));
		int32_t L_4 = ___maxLength3;
		__this->set_bl_counts_5(((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)L_4)));
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::Reset()
extern "C"  void Tree_Reset_m757761098 (Tree_t1054057453 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0011;
	}

IL_0004:
	{
		Int16U5BU5D_t801762735* L_0 = __this->get_freqs_0();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_1), (int16_t)0);
		int32_t L_2 = V_0;
		V_0 = ((int32_t)((int32_t)L_2+(int32_t)1));
	}

IL_0011:
	{
		int32_t L_3 = V_0;
		Int16U5BU5D_t801762735* L_4 = __this->get_freqs_0();
		NullCheck(L_4);
		if ((((int32_t)L_3) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_4)->max_length)))))))
		{
			goto IL_0004;
		}
	}
	{
		__this->set_codes_4((Int16U5BU5D_t801762735*)NULL);
		__this->set_length_1((ByteU5BU5D_t4260760469*)NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::WriteSymbol(System.Int32)
extern "C"  void Tree_WriteSymbol_m4024054307 (Tree_t1054057453 * __this, int32_t ___code0, const MethodInfo* method)
{
	{
		DeflaterHuffman_t3769756376 * L_0 = __this->get_dh_7();
		NullCheck(L_0);
		DeflaterPending_t1829109954 * L_1 = L_0->get_pending_6();
		Int16U5BU5D_t801762735* L_2 = __this->get_codes_4();
		int32_t L_3 = ___code0;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		int16_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		ByteU5BU5D_t4260760469* L_6 = __this->get_length_1();
		int32_t L_7 = ___code0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		uint8_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		NullCheck(L_1);
		PendingBuffer_WriteBits_m4156628504(L_1, ((int32_t)((int32_t)L_5&(int32_t)((int32_t)65535))), L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::SetStaticCodes(System.Int16[],System.Byte[])
extern "C"  void Tree_SetStaticCodes_m469290505 (Tree_t1054057453 * __this, Int16U5BU5D_t801762735* ___staticCodes0, ByteU5BU5D_t4260760469* ___staticLengths1, const MethodInfo* method)
{
	{
		Int16U5BU5D_t801762735* L_0 = ___staticCodes0;
		__this->set_codes_4(L_0);
		ByteU5BU5D_t4260760469* L_1 = ___staticLengths1;
		__this->set_length_1(L_1);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::BuildCodes()
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16U5BU5D_t801762735_il2cpp_TypeInfo_var;
extern Il2CppClass* DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var;
extern const uint32_t Tree_BuildCodes_m614041567_MetadataUsageId;
extern "C"  void Tree_BuildCodes_m614041567 (Tree_t1054057453 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Tree_BuildCodes_m614041567_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t3230847821* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	{
		Int16U5BU5D_t801762735* L_0 = __this->get_freqs_0();
		NullCheck(L_0);
		int32_t L_1 = __this->get_maxLength_6();
		V_0 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)L_1));
		V_1 = 0;
		Int16U5BU5D_t801762735* L_2 = __this->get_freqs_0();
		NullCheck(L_2);
		__this->set_codes_4(((Int16U5BU5D_t801762735*)SZArrayNew(Int16U5BU5D_t801762735_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))))));
		V_2 = 0;
		goto IL_0049;
	}

IL_002e:
	{
		Int32U5BU5D_t3230847821* L_3 = V_0;
		int32_t L_4 = V_2;
		int32_t L_5 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (int32_t)L_5);
		int32_t L_6 = V_1;
		Int32U5BU5D_t3230847821* L_7 = __this->get_bl_counts_5();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		int32_t L_11 = V_2;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)((int32_t)((int32_t)L_10<<(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)15)-(int32_t)L_11))&(int32_t)((int32_t)31)))))));
		int32_t L_12 = V_2;
		V_2 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_13 = V_2;
		int32_t L_14 = __this->get_maxLength_6();
		if ((((int32_t)L_13) < ((int32_t)L_14)))
		{
			goto IL_002e;
		}
	}
	{
		V_3 = 0;
		goto IL_009c;
	}

IL_0056:
	{
		ByteU5BU5D_t4260760469* L_15 = __this->get_length_1();
		int32_t L_16 = V_3;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		uint8_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		V_4 = L_18;
		int32_t L_19 = V_4;
		if ((((int32_t)L_19) <= ((int32_t)0)))
		{
			goto IL_0098;
		}
	}
	{
		Int16U5BU5D_t801762735* L_20 = __this->get_codes_4();
		int32_t L_21 = V_3;
		Int32U5BU5D_t3230847821* L_22 = V_0;
		int32_t L_23 = V_4;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, ((int32_t)((int32_t)L_23-(int32_t)1)));
		int32_t L_24 = ((int32_t)((int32_t)L_23-(int32_t)1));
		int32_t L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var);
		int16_t L_26 = DeflaterHuffman_BitReverse_m2340978984(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(L_21), (int16_t)L_26);
		Int32U5BU5D_t3230847821* L_27 = V_0;
		int32_t L_28 = V_4;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, ((int32_t)((int32_t)L_28-(int32_t)1)));
		int32_t* L_29 = ((L_27)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_28-(int32_t)1)))));
		int32_t L_30 = V_4;
		(*(int32_t*)L_29) = ((int32_t)((int32_t)(*(int32_t*)L_29)+(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)16)-(int32_t)L_30))&(int32_t)((int32_t)31)))))));
	}

IL_0098:
	{
		int32_t L_31 = V_3;
		V_3 = ((int32_t)((int32_t)L_31+(int32_t)1));
	}

IL_009c:
	{
		int32_t L_32 = V_3;
		int32_t L_33 = __this->get_numCodes_3();
		if ((((int32_t)L_32) < ((int32_t)L_33)))
		{
			goto IL_0056;
		}
	}
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::BuildTree()
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern Il2CppClass* SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4020578006;
extern const uint32_t Tree_BuildTree_m3557342695_MetadataUsageId;
extern "C"  void Tree_BuildTree_m3557342695 (Tree_t1054057453 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Tree_BuildTree_m3557342695_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Int32U5BU5D_t3230847821* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	Int32U5BU5D_t3230847821* V_10 = NULL;
	Int32U5BU5D_t3230847821* V_11 = NULL;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	int32_t V_19 = 0;
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	int32_t G_B13_0 = 0;
	{
		Int16U5BU5D_t801762735* L_0 = __this->get_freqs_0();
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		int32_t L_1 = V_0;
		V_1 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)L_1));
		V_2 = 0;
		V_3 = 0;
		V_4 = 0;
		goto IL_0067;
	}

IL_0019:
	{
		Int16U5BU5D_t801762735* L_2 = __this->get_freqs_0();
		int32_t L_3 = V_4;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		int16_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_5 = L_5;
		int32_t L_6 = V_5;
		if (!L_6)
		{
			goto IL_0061;
		}
	}
	{
		int32_t L_7 = V_2;
		int32_t L_8 = L_7;
		V_2 = ((int32_t)((int32_t)L_8+(int32_t)1));
		V_6 = L_8;
		goto IL_003d;
	}

IL_0031:
	{
		Int32U5BU5D_t3230847821* L_9 = V_1;
		int32_t L_10 = V_6;
		Int32U5BU5D_t3230847821* L_11 = V_1;
		int32_t L_12 = V_7;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (int32_t)L_14);
		int32_t L_15 = V_7;
		V_6 = L_15;
	}

IL_003d:
	{
		int32_t L_16 = V_6;
		if ((((int32_t)L_16) <= ((int32_t)0)))
		{
			goto IL_0058;
		}
	}
	{
		Int16U5BU5D_t801762735* L_17 = __this->get_freqs_0();
		Int32U5BU5D_t3230847821* L_18 = V_1;
		int32_t L_19 = V_6;
		int32_t L_20 = ((int32_t)((int32_t)((int32_t)((int32_t)L_19-(int32_t)1))/(int32_t)2));
		V_7 = L_20;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_20);
		int32_t L_21 = L_20;
		int32_t L_22 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_22);
		int32_t L_23 = L_22;
		int16_t L_24 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		int32_t L_25 = V_5;
		if ((((int32_t)L_24) > ((int32_t)L_25)))
		{
			goto IL_0031;
		}
	}

IL_0058:
	{
		Int32U5BU5D_t3230847821* L_26 = V_1;
		int32_t L_27 = V_6;
		int32_t L_28 = V_4;
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, L_27);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(L_27), (int32_t)L_28);
		int32_t L_29 = V_4;
		V_3 = L_29;
	}

IL_0061:
	{
		int32_t L_30 = V_4;
		V_4 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_0067:
	{
		int32_t L_31 = V_4;
		int32_t L_32 = V_0;
		if ((((int32_t)L_31) < ((int32_t)L_32)))
		{
			goto IL_0019;
		}
	}
	{
		goto IL_0085;
	}

IL_006e:
	{
		int32_t L_33 = V_3;
		if ((((int32_t)L_33) < ((int32_t)2)))
		{
			goto IL_0075;
		}
	}
	{
		G_B13_0 = 0;
		goto IL_007a;
	}

IL_0075:
	{
		int32_t L_34 = V_3;
		int32_t L_35 = ((int32_t)((int32_t)L_34+(int32_t)1));
		V_3 = L_35;
		G_B13_0 = L_35;
	}

IL_007a:
	{
		V_8 = G_B13_0;
		Int32U5BU5D_t3230847821* L_36 = V_1;
		int32_t L_37 = V_2;
		int32_t L_38 = L_37;
		V_2 = ((int32_t)((int32_t)L_38+(int32_t)1));
		int32_t L_39 = V_8;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_38);
		(L_36)->SetAt(static_cast<il2cpp_array_size_t>(L_38), (int32_t)L_39);
	}

IL_0085:
	{
		int32_t L_40 = V_2;
		if ((((int32_t)L_40) < ((int32_t)2)))
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_41 = V_3;
		int32_t L_42 = __this->get_minNumCodes_2();
		int32_t L_43 = Math_Max_m1309380475(NULL /*static, unused*/, ((int32_t)((int32_t)L_41+(int32_t)1)), L_42, /*hidden argument*/NULL);
		__this->set_numCodes_3(L_43);
		int32_t L_44 = V_2;
		V_9 = L_44;
		int32_t L_45 = V_2;
		V_10 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)4*(int32_t)L_45))-(int32_t)2))));
		int32_t L_46 = V_2;
		V_11 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)2*(int32_t)L_46))-(int32_t)1))));
		int32_t L_47 = V_9;
		V_12 = L_47;
		V_13 = 0;
		goto IL_00f6;
	}

IL_00c1:
	{
		Int32U5BU5D_t3230847821* L_48 = V_1;
		int32_t L_49 = V_13;
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, L_49);
		int32_t L_50 = L_49;
		int32_t L_51 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		V_14 = L_51;
		Int32U5BU5D_t3230847821* L_52 = V_10;
		int32_t L_53 = V_13;
		int32_t L_54 = V_14;
		NullCheck(L_52);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_52, ((int32_t)((int32_t)2*(int32_t)L_53)));
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)2*(int32_t)L_53))), (int32_t)L_54);
		Int32U5BU5D_t3230847821* L_55 = V_10;
		int32_t L_56 = V_13;
		NullCheck(L_55);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_55, ((int32_t)((int32_t)((int32_t)((int32_t)2*(int32_t)L_56))+(int32_t)1)));
		(L_55)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)2*(int32_t)L_56))+(int32_t)1))), (int32_t)(-1));
		Int32U5BU5D_t3230847821* L_57 = V_11;
		int32_t L_58 = V_13;
		Int16U5BU5D_t801762735* L_59 = __this->get_freqs_0();
		int32_t L_60 = V_14;
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, L_60);
		int32_t L_61 = L_60;
		int16_t L_62 = (L_59)->GetAt(static_cast<il2cpp_array_size_t>(L_61));
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, L_58);
		(L_57)->SetAt(static_cast<il2cpp_array_size_t>(L_58), (int32_t)((int32_t)((int32_t)L_62<<(int32_t)8)));
		Int32U5BU5D_t3230847821* L_63 = V_1;
		int32_t L_64 = V_13;
		int32_t L_65 = V_13;
		NullCheck(L_63);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_63, L_64);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(L_64), (int32_t)L_65);
		int32_t L_66 = V_13;
		V_13 = ((int32_t)((int32_t)L_66+(int32_t)1));
	}

IL_00f6:
	{
		int32_t L_67 = V_13;
		int32_t L_68 = V_2;
		if ((((int32_t)L_67) < ((int32_t)L_68)))
		{
			goto IL_00c1;
		}
	}

IL_00fb:
	{
		Int32U5BU5D_t3230847821* L_69 = V_1;
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, 0);
		int32_t L_70 = 0;
		int32_t L_71 = (L_69)->GetAt(static_cast<il2cpp_array_size_t>(L_70));
		V_15 = L_71;
		Int32U5BU5D_t3230847821* L_72 = V_1;
		int32_t L_73 = V_2;
		int32_t L_74 = ((int32_t)((int32_t)L_73-(int32_t)1));
		V_2 = L_74;
		NullCheck(L_72);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_72, L_74);
		int32_t L_75 = L_74;
		int32_t L_76 = (L_72)->GetAt(static_cast<il2cpp_array_size_t>(L_75));
		V_16 = L_76;
		V_17 = 0;
		V_18 = 1;
		goto IL_0144;
	}

IL_0111:
	{
		int32_t L_77 = V_18;
		int32_t L_78 = V_2;
		if ((((int32_t)((int32_t)((int32_t)L_77+(int32_t)1))) >= ((int32_t)L_78)))
		{
			goto IL_0130;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_79 = V_11;
		Int32U5BU5D_t3230847821* L_80 = V_1;
		int32_t L_81 = V_18;
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, L_81);
		int32_t L_82 = L_81;
		int32_t L_83 = (L_80)->GetAt(static_cast<il2cpp_array_size_t>(L_82));
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, L_83);
		int32_t L_84 = L_83;
		int32_t L_85 = (L_79)->GetAt(static_cast<il2cpp_array_size_t>(L_84));
		Int32U5BU5D_t3230847821* L_86 = V_11;
		Int32U5BU5D_t3230847821* L_87 = V_1;
		int32_t L_88 = V_18;
		NullCheck(L_87);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_87, ((int32_t)((int32_t)L_88+(int32_t)1)));
		int32_t L_89 = ((int32_t)((int32_t)L_88+(int32_t)1));
		int32_t L_90 = (L_87)->GetAt(static_cast<il2cpp_array_size_t>(L_89));
		NullCheck(L_86);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_86, L_90);
		int32_t L_91 = L_90;
		int32_t L_92 = (L_86)->GetAt(static_cast<il2cpp_array_size_t>(L_91));
		if ((((int32_t)L_85) <= ((int32_t)L_92)))
		{
			goto IL_0130;
		}
	}
	{
		int32_t L_93 = V_18;
		V_18 = ((int32_t)((int32_t)L_93+(int32_t)1));
	}

IL_0130:
	{
		Int32U5BU5D_t3230847821* L_94 = V_1;
		int32_t L_95 = V_17;
		Int32U5BU5D_t3230847821* L_96 = V_1;
		int32_t L_97 = V_18;
		NullCheck(L_96);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_96, L_97);
		int32_t L_98 = L_97;
		int32_t L_99 = (L_96)->GetAt(static_cast<il2cpp_array_size_t>(L_98));
		NullCheck(L_94);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_94, L_95);
		(L_94)->SetAt(static_cast<il2cpp_array_size_t>(L_95), (int32_t)L_99);
		int32_t L_100 = V_18;
		V_17 = L_100;
		int32_t L_101 = V_18;
		V_18 = ((int32_t)((int32_t)((int32_t)((int32_t)L_101*(int32_t)2))+(int32_t)1));
	}

IL_0144:
	{
		int32_t L_102 = V_18;
		int32_t L_103 = V_2;
		if ((((int32_t)L_102) < ((int32_t)L_103)))
		{
			goto IL_0111;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_104 = V_11;
		int32_t L_105 = V_16;
		NullCheck(L_104);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_104, L_105);
		int32_t L_106 = L_105;
		int32_t L_107 = (L_104)->GetAt(static_cast<il2cpp_array_size_t>(L_106));
		V_19 = L_107;
		goto IL_015a;
	}

IL_0152:
	{
		Int32U5BU5D_t3230847821* L_108 = V_1;
		int32_t L_109 = V_18;
		Int32U5BU5D_t3230847821* L_110 = V_1;
		int32_t L_111 = V_17;
		NullCheck(L_110);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_110, L_111);
		int32_t L_112 = L_111;
		int32_t L_113 = (L_110)->GetAt(static_cast<il2cpp_array_size_t>(L_112));
		NullCheck(L_108);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_108, L_109);
		(L_108)->SetAt(static_cast<il2cpp_array_size_t>(L_109), (int32_t)L_113);
	}

IL_015a:
	{
		int32_t L_114 = V_17;
		int32_t L_115 = L_114;
		V_18 = L_115;
		if ((((int32_t)L_115) <= ((int32_t)0)))
		{
			goto IL_0174;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_116 = V_11;
		Int32U5BU5D_t3230847821* L_117 = V_1;
		int32_t L_118 = V_18;
		int32_t L_119 = ((int32_t)((int32_t)((int32_t)((int32_t)L_118-(int32_t)1))/(int32_t)2));
		V_17 = L_119;
		NullCheck(L_117);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_117, L_119);
		int32_t L_120 = L_119;
		int32_t L_121 = (L_117)->GetAt(static_cast<il2cpp_array_size_t>(L_120));
		NullCheck(L_116);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_116, L_121);
		int32_t L_122 = L_121;
		int32_t L_123 = (L_116)->GetAt(static_cast<il2cpp_array_size_t>(L_122));
		int32_t L_124 = V_19;
		if ((((int32_t)L_123) > ((int32_t)L_124)))
		{
			goto IL_0152;
		}
	}

IL_0174:
	{
		Int32U5BU5D_t3230847821* L_125 = V_1;
		int32_t L_126 = V_18;
		int32_t L_127 = V_16;
		NullCheck(L_125);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_125, L_126);
		(L_125)->SetAt(static_cast<il2cpp_array_size_t>(L_126), (int32_t)L_127);
		Int32U5BU5D_t3230847821* L_128 = V_1;
		NullCheck(L_128);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_128, 0);
		int32_t L_129 = 0;
		int32_t L_130 = (L_128)->GetAt(static_cast<il2cpp_array_size_t>(L_129));
		V_20 = L_130;
		int32_t L_131 = V_12;
		int32_t L_132 = L_131;
		V_12 = ((int32_t)((int32_t)L_132+(int32_t)1));
		V_16 = L_132;
		Int32U5BU5D_t3230847821* L_133 = V_10;
		int32_t L_134 = V_16;
		int32_t L_135 = V_15;
		NullCheck(L_133);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_133, ((int32_t)((int32_t)2*(int32_t)L_134)));
		(L_133)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)2*(int32_t)L_134))), (int32_t)L_135);
		Int32U5BU5D_t3230847821* L_136 = V_10;
		int32_t L_137 = V_16;
		int32_t L_138 = V_20;
		NullCheck(L_136);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_136, ((int32_t)((int32_t)((int32_t)((int32_t)2*(int32_t)L_137))+(int32_t)1)));
		(L_136)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)2*(int32_t)L_137))+(int32_t)1))), (int32_t)L_138);
		Int32U5BU5D_t3230847821* L_139 = V_11;
		int32_t L_140 = V_15;
		NullCheck(L_139);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_139, L_140);
		int32_t L_141 = L_140;
		int32_t L_142 = (L_139)->GetAt(static_cast<il2cpp_array_size_t>(L_141));
		Int32U5BU5D_t3230847821* L_143 = V_11;
		int32_t L_144 = V_20;
		NullCheck(L_143);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_143, L_144);
		int32_t L_145 = L_144;
		int32_t L_146 = (L_143)->GetAt(static_cast<il2cpp_array_size_t>(L_145));
		int32_t L_147 = Math_Min_m811624909(NULL /*static, unused*/, ((int32_t)((int32_t)L_142&(int32_t)((int32_t)255))), ((int32_t)((int32_t)L_146&(int32_t)((int32_t)255))), /*hidden argument*/NULL);
		V_21 = L_147;
		Int32U5BU5D_t3230847821* L_148 = V_11;
		int32_t L_149 = V_16;
		Int32U5BU5D_t3230847821* L_150 = V_11;
		int32_t L_151 = V_15;
		NullCheck(L_150);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_150, L_151);
		int32_t L_152 = L_151;
		int32_t L_153 = (L_150)->GetAt(static_cast<il2cpp_array_size_t>(L_152));
		Int32U5BU5D_t3230847821* L_154 = V_11;
		int32_t L_155 = V_20;
		NullCheck(L_154);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_154, L_155);
		int32_t L_156 = L_155;
		int32_t L_157 = (L_154)->GetAt(static_cast<il2cpp_array_size_t>(L_156));
		int32_t L_158 = V_21;
		int32_t L_159 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_153+(int32_t)L_157))-(int32_t)L_158))+(int32_t)1));
		V_19 = L_159;
		NullCheck(L_148);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_148, L_149);
		(L_148)->SetAt(static_cast<il2cpp_array_size_t>(L_149), (int32_t)L_159);
		V_17 = 0;
		V_18 = 1;
		goto IL_020c;
	}

IL_01d9:
	{
		int32_t L_160 = V_18;
		int32_t L_161 = V_2;
		if ((((int32_t)((int32_t)((int32_t)L_160+(int32_t)1))) >= ((int32_t)L_161)))
		{
			goto IL_01f8;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_162 = V_11;
		Int32U5BU5D_t3230847821* L_163 = V_1;
		int32_t L_164 = V_18;
		NullCheck(L_163);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_163, L_164);
		int32_t L_165 = L_164;
		int32_t L_166 = (L_163)->GetAt(static_cast<il2cpp_array_size_t>(L_165));
		NullCheck(L_162);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_162, L_166);
		int32_t L_167 = L_166;
		int32_t L_168 = (L_162)->GetAt(static_cast<il2cpp_array_size_t>(L_167));
		Int32U5BU5D_t3230847821* L_169 = V_11;
		Int32U5BU5D_t3230847821* L_170 = V_1;
		int32_t L_171 = V_18;
		NullCheck(L_170);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_170, ((int32_t)((int32_t)L_171+(int32_t)1)));
		int32_t L_172 = ((int32_t)((int32_t)L_171+(int32_t)1));
		int32_t L_173 = (L_170)->GetAt(static_cast<il2cpp_array_size_t>(L_172));
		NullCheck(L_169);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_169, L_173);
		int32_t L_174 = L_173;
		int32_t L_175 = (L_169)->GetAt(static_cast<il2cpp_array_size_t>(L_174));
		if ((((int32_t)L_168) <= ((int32_t)L_175)))
		{
			goto IL_01f8;
		}
	}
	{
		int32_t L_176 = V_18;
		V_18 = ((int32_t)((int32_t)L_176+(int32_t)1));
	}

IL_01f8:
	{
		Int32U5BU5D_t3230847821* L_177 = V_1;
		int32_t L_178 = V_17;
		Int32U5BU5D_t3230847821* L_179 = V_1;
		int32_t L_180 = V_18;
		NullCheck(L_179);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_179, L_180);
		int32_t L_181 = L_180;
		int32_t L_182 = (L_179)->GetAt(static_cast<il2cpp_array_size_t>(L_181));
		NullCheck(L_177);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_177, L_178);
		(L_177)->SetAt(static_cast<il2cpp_array_size_t>(L_178), (int32_t)L_182);
		int32_t L_183 = V_18;
		V_17 = L_183;
		int32_t L_184 = V_17;
		V_18 = ((int32_t)((int32_t)((int32_t)((int32_t)L_184*(int32_t)2))+(int32_t)1));
	}

IL_020c:
	{
		int32_t L_185 = V_18;
		int32_t L_186 = V_2;
		if ((((int32_t)L_185) < ((int32_t)L_186)))
		{
			goto IL_01d9;
		}
	}
	{
		goto IL_021b;
	}

IL_0213:
	{
		Int32U5BU5D_t3230847821* L_187 = V_1;
		int32_t L_188 = V_18;
		Int32U5BU5D_t3230847821* L_189 = V_1;
		int32_t L_190 = V_17;
		NullCheck(L_189);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_189, L_190);
		int32_t L_191 = L_190;
		int32_t L_192 = (L_189)->GetAt(static_cast<il2cpp_array_size_t>(L_191));
		NullCheck(L_187);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_187, L_188);
		(L_187)->SetAt(static_cast<il2cpp_array_size_t>(L_188), (int32_t)L_192);
	}

IL_021b:
	{
		int32_t L_193 = V_17;
		int32_t L_194 = L_193;
		V_18 = L_194;
		if ((((int32_t)L_194) <= ((int32_t)0)))
		{
			goto IL_0235;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_195 = V_11;
		Int32U5BU5D_t3230847821* L_196 = V_1;
		int32_t L_197 = V_18;
		int32_t L_198 = ((int32_t)((int32_t)((int32_t)((int32_t)L_197-(int32_t)1))/(int32_t)2));
		V_17 = L_198;
		NullCheck(L_196);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_196, L_198);
		int32_t L_199 = L_198;
		int32_t L_200 = (L_196)->GetAt(static_cast<il2cpp_array_size_t>(L_199));
		NullCheck(L_195);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_195, L_200);
		int32_t L_201 = L_200;
		int32_t L_202 = (L_195)->GetAt(static_cast<il2cpp_array_size_t>(L_201));
		int32_t L_203 = V_19;
		if ((((int32_t)L_202) > ((int32_t)L_203)))
		{
			goto IL_0213;
		}
	}

IL_0235:
	{
		Int32U5BU5D_t3230847821* L_204 = V_1;
		int32_t L_205 = V_18;
		int32_t L_206 = V_16;
		NullCheck(L_204);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_204, L_205);
		(L_204)->SetAt(static_cast<il2cpp_array_size_t>(L_205), (int32_t)L_206);
		int32_t L_207 = V_2;
		if ((((int32_t)L_207) > ((int32_t)1)))
		{
			goto IL_00fb;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_208 = V_1;
		NullCheck(L_208);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_208, 0);
		int32_t L_209 = 0;
		int32_t L_210 = (L_208)->GetAt(static_cast<il2cpp_array_size_t>(L_209));
		Int32U5BU5D_t3230847821* L_211 = V_10;
		NullCheck(L_211);
		if ((((int32_t)L_210) == ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_211)->max_length))))/(int32_t)2))-(int32_t)1)))))
		{
			goto IL_025a;
		}
	}
	{
		SharpZipBaseException_t2520014981 * L_212 = (SharpZipBaseException_t2520014981 *)il2cpp_codegen_object_new(SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m2797155132(L_212, _stringLiteral4020578006, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_212);
	}

IL_025a:
	{
		Int32U5BU5D_t3230847821* L_213 = V_10;
		Tree_BuildLength_m1615523614(__this, L_213, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::GetEncodedLength()
extern "C"  int32_t Tree_GetEncodedLength_m866994417 (Tree_t1054057453 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		goto IL_001e;
	}

IL_0006:
	{
		int32_t L_0 = V_0;
		Int16U5BU5D_t801762735* L_1 = __this->get_freqs_0();
		int32_t L_2 = V_1;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		int16_t L_4 = (L_1)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		ByteU5BU5D_t4260760469* L_5 = __this->get_length_1();
		int32_t L_6 = V_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t L_7 = L_6;
		uint8_t L_8 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		V_0 = ((int32_t)((int32_t)L_0+(int32_t)((int32_t)((int32_t)L_4*(int32_t)L_8))));
		int32_t L_9 = V_1;
		V_1 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_001e:
	{
		int32_t L_10 = V_1;
		Int16U5BU5D_t801762735* L_11 = __this->get_freqs_0();
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0006;
		}
	}
	{
		int32_t L_12 = V_0;
		return L_12;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::CalcBLFreq(ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree)
extern "C"  void Tree_CalcBLFreq_m2156942961 (Tree_t1054057453 * __this, Tree_t1054057453 * ___blTree0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		V_3 = (-1);
		V_4 = 0;
		goto IL_00f8;
	}

IL_000a:
	{
		V_2 = 1;
		ByteU5BU5D_t4260760469* L_0 = __this->get_length_1();
		int32_t L_1 = V_4;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		uint8_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_5 = L_3;
		int32_t L_4 = V_5;
		if (L_4)
		{
			goto IL_0025;
		}
	}
	{
		V_0 = ((int32_t)138);
		V_1 = 3;
		goto IL_004b;
	}

IL_0025:
	{
		V_0 = 6;
		V_1 = 3;
		int32_t L_5 = V_3;
		int32_t L_6 = V_5;
		if ((((int32_t)L_5) == ((int32_t)L_6)))
		{
			goto IL_004b;
		}
	}
	{
		Tree_t1054057453 * L_7 = ___blTree0;
		NullCheck(L_7);
		Int16U5BU5D_t801762735* L_8 = L_7->get_freqs_0();
		int32_t L_9 = V_5;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int16_t* L_10 = ((L_8)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_9)));
		(*(int16_t*)L_10) = (((int16_t)((int16_t)((int32_t)((int32_t)(*(int16_t*)L_10)+(int32_t)1)))));
		V_2 = 0;
	}

IL_004b:
	{
		int32_t L_11 = V_5;
		V_3 = L_11;
		int32_t L_12 = V_4;
		V_4 = ((int32_t)((int32_t)L_12+(int32_t)1));
		goto IL_0064;
	}

IL_0056:
	{
		int32_t L_13 = V_4;
		V_4 = ((int32_t)((int32_t)L_13+(int32_t)1));
		int32_t L_14 = V_2;
		int32_t L_15 = ((int32_t)((int32_t)L_14+(int32_t)1));
		V_2 = L_15;
		int32_t L_16 = V_0;
		if ((((int32_t)L_15) >= ((int32_t)L_16)))
		{
			goto IL_007a;
		}
	}

IL_0064:
	{
		int32_t L_17 = V_4;
		int32_t L_18 = __this->get_numCodes_3();
		if ((((int32_t)L_17) >= ((int32_t)L_18)))
		{
			goto IL_007a;
		}
	}
	{
		int32_t L_19 = V_3;
		ByteU5BU5D_t4260760469* L_20 = __this->get_length_1();
		int32_t L_21 = V_4;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		uint8_t L_23 = (L_20)->GetAt(static_cast<il2cpp_array_size_t>(L_22));
		if ((((int32_t)L_19) == ((int32_t)L_23)))
		{
			goto IL_0056;
		}
	}

IL_007a:
	{
		int32_t L_24 = V_2;
		int32_t L_25 = V_1;
		if ((((int32_t)L_24) >= ((int32_t)L_25)))
		{
			goto IL_009b;
		}
	}
	{
		Tree_t1054057453 * L_26 = ___blTree0;
		NullCheck(L_26);
		Int16U5BU5D_t801762735* L_27 = L_26->get_freqs_0();
		int32_t L_28 = V_3;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, L_28);
		int16_t* L_29 = ((L_27)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28)));
		int32_t L_30 = V_2;
		(*(int16_t*)L_29) = (((int16_t)((int16_t)((int32_t)((int32_t)(*(int16_t*)L_29)+(int32_t)(((int16_t)((int16_t)L_30))))))));
		goto IL_00f8;
	}

IL_009b:
	{
		int32_t L_31 = V_3;
		if (!L_31)
		{
			goto IL_00bb;
		}
	}
	{
		Tree_t1054057453 * L_32 = ___blTree0;
		NullCheck(L_32);
		Int16U5BU5D_t801762735* L_33 = L_32->get_freqs_0();
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, ((int32_t)16));
		int16_t* L_34 = ((L_33)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)16))));
		(*(int16_t*)L_34) = (((int16_t)((int16_t)((int32_t)((int32_t)(*(int16_t*)L_34)+(int32_t)1)))));
		goto IL_00f8;
	}

IL_00bb:
	{
		int32_t L_35 = V_2;
		if ((((int32_t)L_35) > ((int32_t)((int32_t)10))))
		{
			goto IL_00dd;
		}
	}
	{
		Tree_t1054057453 * L_36 = ___blTree0;
		NullCheck(L_36);
		Int16U5BU5D_t801762735* L_37 = L_36->get_freqs_0();
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, ((int32_t)17));
		int16_t* L_38 = ((L_37)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)17))));
		(*(int16_t*)L_38) = (((int16_t)((int16_t)((int32_t)((int32_t)(*(int16_t*)L_38)+(int32_t)1)))));
		goto IL_00f8;
	}

IL_00dd:
	{
		Tree_t1054057453 * L_39 = ___blTree0;
		NullCheck(L_39);
		Int16U5BU5D_t801762735* L_40 = L_39->get_freqs_0();
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, ((int32_t)18));
		int16_t* L_41 = ((L_40)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)18))));
		(*(int16_t*)L_41) = (((int16_t)((int16_t)((int32_t)((int32_t)(*(int16_t*)L_41)+(int32_t)1)))));
	}

IL_00f8:
	{
		int32_t L_42 = V_4;
		int32_t L_43 = __this->get_numCodes_3();
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_000a;
		}
	}
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::WriteTree(ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree)
extern "C"  void Tree_WriteTree_m1704667083 (Tree_t1054057453 * __this, Tree_t1054057453 * ___blTree0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		V_3 = (-1);
		V_4 = 0;
		goto IL_00df;
	}

IL_000a:
	{
		V_2 = 1;
		ByteU5BU5D_t4260760469* L_0 = __this->get_length_1();
		int32_t L_1 = V_4;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		uint8_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_5 = L_3;
		int32_t L_4 = V_5;
		if (L_4)
		{
			goto IL_0025;
		}
	}
	{
		V_0 = ((int32_t)138);
		V_1 = 3;
		goto IL_0038;
	}

IL_0025:
	{
		V_0 = 6;
		V_1 = 3;
		int32_t L_5 = V_3;
		int32_t L_6 = V_5;
		if ((((int32_t)L_5) == ((int32_t)L_6)))
		{
			goto IL_0038;
		}
	}
	{
		Tree_t1054057453 * L_7 = ___blTree0;
		int32_t L_8 = V_5;
		NullCheck(L_7);
		Tree_WriteSymbol_m4024054307(L_7, L_8, /*hidden argument*/NULL);
		V_2 = 0;
	}

IL_0038:
	{
		int32_t L_9 = V_5;
		V_3 = L_9;
		int32_t L_10 = V_4;
		V_4 = ((int32_t)((int32_t)L_10+(int32_t)1));
		goto IL_0051;
	}

IL_0043:
	{
		int32_t L_11 = V_4;
		V_4 = ((int32_t)((int32_t)L_11+(int32_t)1));
		int32_t L_12 = V_2;
		int32_t L_13 = ((int32_t)((int32_t)L_12+(int32_t)1));
		V_2 = L_13;
		int32_t L_14 = V_0;
		if ((((int32_t)L_13) >= ((int32_t)L_14)))
		{
			goto IL_0067;
		}
	}

IL_0051:
	{
		int32_t L_15 = V_4;
		int32_t L_16 = __this->get_numCodes_3();
		if ((((int32_t)L_15) >= ((int32_t)L_16)))
		{
			goto IL_0067;
		}
	}
	{
		int32_t L_17 = V_3;
		ByteU5BU5D_t4260760469* L_18 = __this->get_length_1();
		int32_t L_19 = V_4;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_19);
		int32_t L_20 = L_19;
		uint8_t L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		if ((((int32_t)L_17) == ((int32_t)L_21)))
		{
			goto IL_0043;
		}
	}

IL_0067:
	{
		int32_t L_22 = V_2;
		int32_t L_23 = V_1;
		if ((((int32_t)L_22) >= ((int32_t)L_23)))
		{
			goto IL_007e;
		}
	}
	{
		goto IL_0074;
	}

IL_006d:
	{
		Tree_t1054057453 * L_24 = ___blTree0;
		int32_t L_25 = V_3;
		NullCheck(L_24);
		Tree_WriteSymbol_m4024054307(L_24, L_25, /*hidden argument*/NULL);
	}

IL_0074:
	{
		int32_t L_26 = V_2;
		int32_t L_27 = L_26;
		V_2 = ((int32_t)((int32_t)L_27-(int32_t)1));
		if ((((int32_t)L_27) > ((int32_t)0)))
		{
			goto IL_006d;
		}
	}
	{
		goto IL_00df;
	}

IL_007e:
	{
		int32_t L_28 = V_3;
		if (!L_28)
		{
			goto IL_009f;
		}
	}
	{
		Tree_t1054057453 * L_29 = ___blTree0;
		NullCheck(L_29);
		Tree_WriteSymbol_m4024054307(L_29, ((int32_t)16), /*hidden argument*/NULL);
		DeflaterHuffman_t3769756376 * L_30 = __this->get_dh_7();
		NullCheck(L_30);
		DeflaterPending_t1829109954 * L_31 = L_30->get_pending_6();
		int32_t L_32 = V_2;
		NullCheck(L_31);
		PendingBuffer_WriteBits_m4156628504(L_31, ((int32_t)((int32_t)L_32-(int32_t)3)), 2, /*hidden argument*/NULL);
		goto IL_00df;
	}

IL_009f:
	{
		int32_t L_33 = V_2;
		if ((((int32_t)L_33) > ((int32_t)((int32_t)10))))
		{
			goto IL_00c2;
		}
	}
	{
		Tree_t1054057453 * L_34 = ___blTree0;
		NullCheck(L_34);
		Tree_WriteSymbol_m4024054307(L_34, ((int32_t)17), /*hidden argument*/NULL);
		DeflaterHuffman_t3769756376 * L_35 = __this->get_dh_7();
		NullCheck(L_35);
		DeflaterPending_t1829109954 * L_36 = L_35->get_pending_6();
		int32_t L_37 = V_2;
		NullCheck(L_36);
		PendingBuffer_WriteBits_m4156628504(L_36, ((int32_t)((int32_t)L_37-(int32_t)3)), 3, /*hidden argument*/NULL);
		goto IL_00df;
	}

IL_00c2:
	{
		Tree_t1054057453 * L_38 = ___blTree0;
		NullCheck(L_38);
		Tree_WriteSymbol_m4024054307(L_38, ((int32_t)18), /*hidden argument*/NULL);
		DeflaterHuffman_t3769756376 * L_39 = __this->get_dh_7();
		NullCheck(L_39);
		DeflaterPending_t1829109954 * L_40 = L_39->get_pending_6();
		int32_t L_41 = V_2;
		NullCheck(L_40);
		PendingBuffer_WriteBits_m4156628504(L_40, ((int32_t)((int32_t)L_41-(int32_t)((int32_t)11))), 7, /*hidden argument*/NULL);
	}

IL_00df:
	{
		int32_t L_42 = V_4;
		int32_t L_43 = __this->get_numCodes_3();
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_000a;
		}
	}
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::BuildLength(System.Int32[])
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern const uint32_t Tree_BuildLength_m1615523614_MetadataUsageId;
extern "C"  void Tree_BuildLength_m1615523614 (Tree_t1054057453 * __this, Int32U5BU5D_t3230847821* ___childs0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Tree_BuildLength_m1615523614_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Int32U5BU5D_t3230847821* V_4 = NULL;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	{
		Int16U5BU5D_t801762735* L_0 = __this->get_freqs_0();
		NullCheck(L_0);
		__this->set_length_1(((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length)))))));
		Int32U5BU5D_t3230847821* L_1 = ___childs0;
		NullCheck(L_1);
		V_0 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_1)->max_length))))/(int32_t)2));
		int32_t L_2 = V_0;
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_2+(int32_t)1))/(int32_t)2));
		V_2 = 0;
		V_3 = 0;
		goto IL_0032;
	}

IL_0025:
	{
		Int32U5BU5D_t3230847821* L_3 = __this->get_bl_counts_5();
		int32_t L_4 = V_3;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (int32_t)0);
		int32_t L_5 = V_3;
		V_3 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_6 = V_3;
		int32_t L_7 = __this->get_maxLength_6();
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0025;
		}
	}
	{
		int32_t L_8 = V_0;
		V_4 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)L_8));
		Int32U5BU5D_t3230847821* L_9 = V_4;
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, ((int32_t)((int32_t)L_10-(int32_t)1)));
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_10-(int32_t)1))), (int32_t)0);
		int32_t L_11 = V_0;
		V_5 = ((int32_t)((int32_t)L_11-(int32_t)1));
		goto IL_00d7;
	}

IL_0054:
	{
		Int32U5BU5D_t3230847821* L_12 = ___childs0;
		int32_t L_13 = V_5;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, ((int32_t)((int32_t)((int32_t)((int32_t)2*(int32_t)L_13))+(int32_t)1)));
		int32_t L_14 = ((int32_t)((int32_t)((int32_t)((int32_t)2*(int32_t)L_13))+(int32_t)1));
		int32_t L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		if ((((int32_t)L_15) == ((int32_t)(-1))))
		{
			goto IL_009b;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_16 = V_4;
		int32_t L_17 = V_5;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		int32_t L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		V_6 = ((int32_t)((int32_t)L_19+(int32_t)1));
		int32_t L_20 = V_6;
		int32_t L_21 = __this->get_maxLength_6();
		if ((((int32_t)L_20) <= ((int32_t)L_21)))
		{
			goto IL_007e;
		}
	}
	{
		int32_t L_22 = __this->get_maxLength_6();
		V_6 = L_22;
		int32_t L_23 = V_2;
		V_2 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_007e:
	{
		Int32U5BU5D_t3230847821* L_24 = V_4;
		Int32U5BU5D_t3230847821* L_25 = ___childs0;
		int32_t L_26 = V_5;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, ((int32_t)((int32_t)2*(int32_t)L_26)));
		int32_t L_27 = ((int32_t)((int32_t)2*(int32_t)L_26));
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		Int32U5BU5D_t3230847821* L_29 = V_4;
		Int32U5BU5D_t3230847821* L_30 = ___childs0;
		int32_t L_31 = V_5;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, ((int32_t)((int32_t)((int32_t)((int32_t)2*(int32_t)L_31))+(int32_t)1)));
		int32_t L_32 = ((int32_t)((int32_t)((int32_t)((int32_t)2*(int32_t)L_31))+(int32_t)1));
		int32_t L_33 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_32));
		int32_t L_34 = V_6;
		int32_t L_35 = L_34;
		V_13 = L_35;
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, L_33);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(L_33), (int32_t)L_35);
		int32_t L_36 = V_13;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_28);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(L_28), (int32_t)L_36);
		goto IL_00d1;
	}

IL_009b:
	{
		Int32U5BU5D_t3230847821* L_37 = V_4;
		int32_t L_38 = V_5;
		NullCheck(L_37);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_37, L_38);
		int32_t L_39 = L_38;
		int32_t L_40 = (L_37)->GetAt(static_cast<il2cpp_array_size_t>(L_39));
		V_7 = L_40;
		Int32U5BU5D_t3230847821* L_41 = __this->get_bl_counts_5();
		int32_t L_42 = V_7;
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, ((int32_t)((int32_t)L_42-(int32_t)1)));
		int32_t* L_43 = ((L_41)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_42-(int32_t)1)))));
		(*(int32_t*)L_43) = ((int32_t)((int32_t)(*(int32_t*)L_43)+(int32_t)1));
		ByteU5BU5D_t4260760469* L_44 = __this->get_length_1();
		Int32U5BU5D_t3230847821* L_45 = ___childs0;
		int32_t L_46 = V_5;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, ((int32_t)((int32_t)2*(int32_t)L_46)));
		int32_t L_47 = ((int32_t)((int32_t)2*(int32_t)L_46));
		int32_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		Int32U5BU5D_t3230847821* L_49 = V_4;
		int32_t L_50 = V_5;
		NullCheck(L_49);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_49, L_50);
		int32_t L_51 = L_50;
		int32_t L_52 = (L_49)->GetAt(static_cast<il2cpp_array_size_t>(L_51));
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, L_48);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(L_48), (uint8_t)(((int32_t)((uint8_t)L_52))));
	}

IL_00d1:
	{
		int32_t L_53 = V_5;
		V_5 = ((int32_t)((int32_t)L_53-(int32_t)1));
	}

IL_00d7:
	{
		int32_t L_54 = V_5;
		if ((((int32_t)L_54) >= ((int32_t)0)))
		{
			goto IL_0054;
		}
	}
	{
		int32_t L_55 = V_2;
		if (L_55)
		{
			goto IL_00e3;
		}
	}
	{
		return;
	}

IL_00e3:
	{
		int32_t L_56 = __this->get_maxLength_6();
		V_8 = ((int32_t)((int32_t)L_56-(int32_t)1));
	}

IL_00ed:
	{
		Int32U5BU5D_t3230847821* L_57 = __this->get_bl_counts_5();
		int32_t L_58 = V_8;
		int32_t L_59 = ((int32_t)((int32_t)L_58-(int32_t)1));
		V_8 = L_59;
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, L_59);
		int32_t L_60 = L_59;
		int32_t L_61 = (L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_60));
		if (!L_61)
		{
			goto IL_00ed;
		}
	}

IL_00fd:
	{
		Int32U5BU5D_t3230847821* L_62 = __this->get_bl_counts_5();
		int32_t L_63 = V_8;
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, L_63);
		int32_t* L_64 = ((L_62)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_63)));
		(*(int32_t*)L_64) = ((int32_t)((int32_t)(*(int32_t*)L_64)-(int32_t)1));
		Int32U5BU5D_t3230847821* L_65 = __this->get_bl_counts_5();
		int32_t L_66 = V_8;
		int32_t L_67 = ((int32_t)((int32_t)L_66+(int32_t)1));
		V_8 = L_67;
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, L_67);
		int32_t* L_68 = ((L_65)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_67)));
		(*(int32_t*)L_68) = ((int32_t)((int32_t)(*(int32_t*)L_68)+(int32_t)1));
		int32_t L_69 = V_2;
		int32_t L_70 = __this->get_maxLength_6();
		int32_t L_71 = V_8;
		V_2 = ((int32_t)((int32_t)L_69-(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_70-(int32_t)1))-(int32_t)L_71))&(int32_t)((int32_t)31)))))));
		int32_t L_72 = V_2;
		if ((((int32_t)L_72) <= ((int32_t)0)))
		{
			goto IL_0159;
		}
	}
	{
		int32_t L_73 = V_8;
		int32_t L_74 = __this->get_maxLength_6();
		if ((((int32_t)L_73) < ((int32_t)((int32_t)((int32_t)L_74-(int32_t)1)))))
		{
			goto IL_00fd;
		}
	}

IL_0159:
	{
		int32_t L_75 = V_2;
		if ((((int32_t)L_75) > ((int32_t)0)))
		{
			goto IL_00ed;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_76 = __this->get_bl_counts_5();
		int32_t L_77 = __this->get_maxLength_6();
		NullCheck(L_76);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_76, ((int32_t)((int32_t)L_77-(int32_t)1)));
		int32_t* L_78 = ((L_76)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_77-(int32_t)1)))));
		int32_t L_79 = V_2;
		(*(int32_t*)L_78) = ((int32_t)((int32_t)(*(int32_t*)L_78)+(int32_t)L_79));
		Int32U5BU5D_t3230847821* L_80 = __this->get_bl_counts_5();
		int32_t L_81 = __this->get_maxLength_6();
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, ((int32_t)((int32_t)L_81-(int32_t)2)));
		int32_t* L_82 = ((L_80)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_81-(int32_t)2)))));
		int32_t L_83 = V_2;
		(*(int32_t*)L_82) = ((int32_t)((int32_t)(*(int32_t*)L_82)-(int32_t)L_83));
		int32_t L_84 = V_1;
		V_9 = ((int32_t)((int32_t)2*(int32_t)L_84));
		int32_t L_85 = __this->get_maxLength_6();
		V_10 = L_85;
		goto IL_01f0;
	}

IL_01ac:
	{
		Int32U5BU5D_t3230847821* L_86 = __this->get_bl_counts_5();
		int32_t L_87 = V_10;
		NullCheck(L_86);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_86, ((int32_t)((int32_t)L_87-(int32_t)1)));
		int32_t L_88 = ((int32_t)((int32_t)L_87-(int32_t)1));
		int32_t L_89 = (L_86)->GetAt(static_cast<il2cpp_array_size_t>(L_88));
		V_11 = L_89;
		goto IL_01e5;
	}

IL_01bb:
	{
		Int32U5BU5D_t3230847821* L_90 = ___childs0;
		int32_t L_91 = V_9;
		int32_t L_92 = L_91;
		V_9 = ((int32_t)((int32_t)L_92+(int32_t)1));
		NullCheck(L_90);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_90, L_92);
		int32_t L_93 = L_92;
		int32_t L_94 = (L_90)->GetAt(static_cast<il2cpp_array_size_t>(L_93));
		V_12 = ((int32_t)((int32_t)2*(int32_t)L_94));
		Int32U5BU5D_t3230847821* L_95 = ___childs0;
		int32_t L_96 = V_12;
		NullCheck(L_95);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_95, ((int32_t)((int32_t)L_96+(int32_t)1)));
		int32_t L_97 = ((int32_t)((int32_t)L_96+(int32_t)1));
		int32_t L_98 = (L_95)->GetAt(static_cast<il2cpp_array_size_t>(L_97));
		if ((!(((uint32_t)L_98) == ((uint32_t)(-1)))))
		{
			goto IL_01e5;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_99 = __this->get_length_1();
		Int32U5BU5D_t3230847821* L_100 = ___childs0;
		int32_t L_101 = V_12;
		NullCheck(L_100);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_100, L_101);
		int32_t L_102 = L_101;
		int32_t L_103 = (L_100)->GetAt(static_cast<il2cpp_array_size_t>(L_102));
		int32_t L_104 = V_10;
		NullCheck(L_99);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_99, L_103);
		(L_99)->SetAt(static_cast<il2cpp_array_size_t>(L_103), (uint8_t)(((int32_t)((uint8_t)L_104))));
		int32_t L_105 = V_11;
		V_11 = ((int32_t)((int32_t)L_105-(int32_t)1));
	}

IL_01e5:
	{
		int32_t L_106 = V_11;
		if ((((int32_t)L_106) > ((int32_t)0)))
		{
			goto IL_01bb;
		}
	}
	{
		int32_t L_107 = V_10;
		V_10 = ((int32_t)((int32_t)L_107-(int32_t)1));
	}

IL_01f0:
	{
		int32_t L_108 = V_10;
		if (L_108)
		{
			goto IL_01ac;
		}
	}
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.DeflaterPending::.ctor()
extern "C"  void DeflaterPending__ctor_m1638983314 (DeflaterPending_t1829109954 * __this, const MethodInfo* method)
{
	{
		PendingBuffer__ctor_m3734046780(__this, ((int32_t)65536), /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::.ctor(System.Boolean)
extern Il2CppClass* Adler32_t2680377483_il2cpp_TypeInfo_var;
extern Il2CppClass* StreamManipulator_t2348681196_il2cpp_TypeInfo_var;
extern Il2CppClass* OutputWindow_t617095569_il2cpp_TypeInfo_var;
extern const uint32_t Inflater__ctor_m422624816_MetadataUsageId;
extern "C"  void Inflater__ctor_m422624816 (Inflater_t1975778921 * __this, bool ___noHeader0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Inflater__ctor_m422624816_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Inflater_t1975778921 * G_B2_0 = NULL;
	Inflater_t1975778921 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	Inflater_t1975778921 * G_B3_1 = NULL;
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		bool L_0 = ___noHeader0;
		__this->set_noHeader_13(L_0);
		Adler32_t2680377483 * L_1 = (Adler32_t2680377483 *)il2cpp_codegen_object_new(Adler32_t2680377483_il2cpp_TypeInfo_var);
		Adler32__ctor_m2455030518(L_1, /*hidden argument*/NULL);
		__this->set_adler_19(L_1);
		StreamManipulator_t2348681196 * L_2 = (StreamManipulator_t2348681196 *)il2cpp_codegen_object_new(StreamManipulator_t2348681196_il2cpp_TypeInfo_var);
		StreamManipulator__ctor_m2051970415(L_2, /*hidden argument*/NULL);
		__this->set_input_14(L_2);
		OutputWindow_t617095569 * L_3 = (OutputWindow_t617095569 *)il2cpp_codegen_object_new(OutputWindow_t617095569_il2cpp_TypeInfo_var);
		OutputWindow__ctor_m3237040600(L_3, /*hidden argument*/NULL);
		__this->set_outputWindow_15(L_3);
		bool L_4 = ___noHeader0;
		G_B1_0 = __this;
		if (L_4)
		{
			G_B2_0 = __this;
			goto IL_0035;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_0036;
	}

IL_0035:
	{
		G_B3_0 = 2;
		G_B3_1 = G_B2_0;
	}

IL_0036:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_mode_4(G_B3_0);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::Reset()
extern "C"  void Inflater_Reset_m1534606118 (Inflater_t1975778921 * __this, const MethodInfo* method)
{
	Inflater_t1975778921 * G_B2_0 = NULL;
	Inflater_t1975778921 * G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	Inflater_t1975778921 * G_B3_1 = NULL;
	{
		bool L_0 = __this->get_noHeader_13();
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_000c;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		goto IL_000d;
	}

IL_000c:
	{
		G_B3_0 = 2;
		G_B3_1 = G_B2_0;
	}

IL_000d:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_mode_4(G_B3_0);
		__this->set_totalIn_12((((int64_t)((int64_t)0))));
		__this->set_totalOut_11((((int64_t)((int64_t)0))));
		StreamManipulator_t2348681196 * L_1 = __this->get_input_14();
		NullCheck(L_1);
		StreamManipulator_Reset_m3993370652(L_1, /*hidden argument*/NULL);
		OutputWindow_t617095569 * L_2 = __this->get_outputWindow_15();
		NullCheck(L_2);
		OutputWindow_Reset_m883473541(L_2, /*hidden argument*/NULL);
		__this->set_dynHeader_16((InflaterDynHeader_t1952417709 *)NULL);
		__this->set_litlenTree_17((InflaterHuffmanTree_t1141403250 *)NULL);
		__this->set_distTree_18((InflaterHuffmanTree_t1141403250 *)NULL);
		__this->set_isLastBlock_10((bool)0);
		Adler32_t2680377483 * L_3 = __this->get_adler_19();
		NullCheck(L_3);
		Adler32_Reset_m101463459(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeHeader()
extern Il2CppClass* SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1471584684;
extern Il2CppCodeGenString* _stringLiteral1719092805;
extern const uint32_t Inflater_DecodeHeader_m579569528_MetadataUsageId;
extern "C"  bool Inflater_DecodeHeader_m579569528 (Inflater_t1975778921 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Inflater_DecodeHeader_m579569528_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		StreamManipulator_t2348681196 * L_0 = __this->get_input_14();
		NullCheck(L_0);
		int32_t L_1 = StreamManipulator_PeekBits_m2187722491(L_0, ((int32_t)16), /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		StreamManipulator_t2348681196 * L_3 = __this->get_input_14();
		NullCheck(L_3);
		StreamManipulator_DropBits_m3482227099(L_3, ((int32_t)16), /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_4<<(int32_t)8))|(int32_t)((int32_t)((int32_t)L_5>>(int32_t)8))))&(int32_t)((int32_t)65535)));
		int32_t L_6 = V_0;
		if (!((int32_t)((int32_t)L_6%(int32_t)((int32_t)31))))
		{
			goto IL_0040;
		}
	}
	{
		SharpZipBaseException_t2520014981 * L_7 = (SharpZipBaseException_t2520014981 *)il2cpp_codegen_object_new(SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m2797155132(L_7, _stringLiteral1471584684, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7);
	}

IL_0040:
	{
		int32_t L_8 = V_0;
		if ((((int32_t)((int32_t)((int32_t)L_8&(int32_t)((int32_t)3840)))) == ((int32_t)((int32_t)2048))))
		{
			goto IL_0059;
		}
	}
	{
		SharpZipBaseException_t2520014981 * L_9 = (SharpZipBaseException_t2520014981 *)il2cpp_codegen_object_new(SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m2797155132(L_9, _stringLiteral1719092805, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0059:
	{
		int32_t L_10 = V_0;
		if (((int32_t)((int32_t)L_10&(int32_t)((int32_t)32))))
		{
			goto IL_0068;
		}
	}
	{
		__this->set_mode_4(2);
		goto IL_0077;
	}

IL_0068:
	{
		__this->set_mode_4(1);
		__this->set_neededBits_6(((int32_t)32));
	}

IL_0077:
	{
		return (bool)1;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeDict()
extern "C"  bool Inflater_DecodeDict_m3876441313 (Inflater_t1975778921 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_003f;
	}

IL_0002:
	{
		StreamManipulator_t2348681196 * L_0 = __this->get_input_14();
		NullCheck(L_0);
		int32_t L_1 = StreamManipulator_PeekBits_m2187722491(L_0, 8, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		return (bool)0;
	}

IL_0015:
	{
		StreamManipulator_t2348681196 * L_3 = __this->get_input_14();
		NullCheck(L_3);
		StreamManipulator_DropBits_m3482227099(L_3, 8, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_readAdler_5();
		int32_t L_5 = V_0;
		__this->set_readAdler_5(((int32_t)((int32_t)((int32_t)((int32_t)L_4<<(int32_t)8))|(int32_t)L_5)));
		int32_t L_6 = __this->get_neededBits_6();
		__this->set_neededBits_6(((int32_t)((int32_t)L_6-(int32_t)8)));
	}

IL_003f:
	{
		int32_t L_7 = __this->get_neededBits_6();
		if ((((int32_t)L_7) > ((int32_t)0)))
		{
			goto IL_0002;
		}
	}
	{
		return (bool)0;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeHuffman()
extern Il2CppClass* Inflater_t1975778921_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppClass* SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3734609690;
extern Il2CppCodeGenString* _stringLiteral2957899034;
extern Il2CppCodeGenString* _stringLiteral1269055054;
extern const uint32_t Inflater_DecodeHuffman_m3108706180_MetadataUsageId;
extern "C"  bool Inflater_DecodeHuffman_m3108706180 (Inflater_t1975778921 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Inflater_DecodeHuffman_m3108706180_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		OutputWindow_t617095569 * L_0 = __this->get_outputWindow_15();
		NullCheck(L_0);
		int32_t L_1 = OutputWindow_GetFreeSpace_m3222198588(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_01d0;
	}

IL_0011:
	{
		int32_t L_2 = __this->get_mode_4();
		V_4 = L_2;
		int32_t L_3 = V_4;
		if (((int32_t)((int32_t)L_3-(int32_t)7)) == 0)
		{
			goto IL_0051;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)7)) == 1)
		{
			goto IL_00c5;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)7)) == 2)
		{
			goto IL_0114;
		}
		if (((int32_t)((int32_t)L_3-(int32_t)7)) == 3)
		{
			goto IL_0154;
		}
	}
	{
		goto IL_01c5;
	}

IL_0037:
	{
		OutputWindow_t617095569 * L_4 = __this->get_outputWindow_15();
		int32_t L_5 = V_1;
		NullCheck(L_4);
		OutputWindow_Write_m2128171238(L_4, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		int32_t L_7 = ((int32_t)((int32_t)L_6-(int32_t)1));
		V_0 = L_7;
		if ((((int32_t)L_7) >= ((int32_t)((int32_t)258))))
		{
			goto IL_0051;
		}
	}
	{
		return (bool)1;
	}

IL_0051:
	{
		InflaterHuffmanTree_t1141403250 * L_8 = __this->get_litlenTree_17();
		StreamManipulator_t2348681196 * L_9 = __this->get_input_14();
		NullCheck(L_8);
		int32_t L_10 = InflaterHuffmanTree_GetSymbol_m2276886475(L_8, L_9, /*hidden argument*/NULL);
		int32_t L_11 = L_10;
		V_1 = L_11;
		if (!((int32_t)((int32_t)L_11&(int32_t)((int32_t)-256))))
		{
			goto IL_0037;
		}
	}
	{
		int32_t L_12 = V_1;
		if ((((int32_t)L_12) >= ((int32_t)((int32_t)257))))
		{
			goto IL_0091;
		}
	}
	{
		int32_t L_13 = V_1;
		if ((((int32_t)L_13) >= ((int32_t)0)))
		{
			goto IL_007a;
		}
	}
	{
		return (bool)0;
	}

IL_007a:
	{
		__this->set_distTree_18((InflaterHuffmanTree_t1141403250 *)NULL);
		__this->set_litlenTree_17((InflaterHuffmanTree_t1141403250 *)NULL);
		__this->set_mode_4(2);
		return (bool)1;
	}

IL_0091:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(Inflater_t1975778921_il2cpp_TypeInfo_var);
		Int32U5BU5D_t3230847821* L_14 = ((Inflater_t1975778921_StaticFields*)Inflater_t1975778921_il2cpp_TypeInfo_var->static_fields)->get_CPLENS_0();
		int32_t L_15 = V_1;
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, ((int32_t)((int32_t)L_15-(int32_t)((int32_t)257))));
		int32_t L_16 = ((int32_t)((int32_t)L_15-(int32_t)((int32_t)257)));
		int32_t L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		__this->set_repLength_7(L_17);
		Int32U5BU5D_t3230847821* L_18 = ((Inflater_t1975778921_StaticFields*)Inflater_t1975778921_il2cpp_TypeInfo_var->static_fields)->get_CPLEXT_1();
		int32_t L_19 = V_1;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, ((int32_t)((int32_t)L_19-(int32_t)((int32_t)257))));
		int32_t L_20 = ((int32_t)((int32_t)L_19-(int32_t)((int32_t)257)));
		int32_t L_21 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		__this->set_neededBits_6(L_21);
		goto IL_00c5;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_00b9;
		throw e;
	}

CATCH_00b9:
	{ // begin catch(System.Exception)
		SharpZipBaseException_t2520014981 * L_22 = (SharpZipBaseException_t2520014981 *)il2cpp_codegen_object_new(SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m2797155132(L_22, _stringLiteral3734609690, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22);
	} // end catch (depth: 1)

IL_00c5:
	{
		int32_t L_23 = __this->get_neededBits_6();
		if ((((int32_t)L_23) <= ((int32_t)0)))
		{
			goto IL_010c;
		}
	}
	{
		__this->set_mode_4(8);
		StreamManipulator_t2348681196 * L_24 = __this->get_input_14();
		int32_t L_25 = __this->get_neededBits_6();
		NullCheck(L_24);
		int32_t L_26 = StreamManipulator_PeekBits_m2187722491(L_24, L_25, /*hidden argument*/NULL);
		V_2 = L_26;
		int32_t L_27 = V_2;
		if ((((int32_t)L_27) >= ((int32_t)0)))
		{
			goto IL_00ed;
		}
	}
	{
		return (bool)0;
	}

IL_00ed:
	{
		StreamManipulator_t2348681196 * L_28 = __this->get_input_14();
		int32_t L_29 = __this->get_neededBits_6();
		NullCheck(L_28);
		StreamManipulator_DropBits_m3482227099(L_28, L_29, /*hidden argument*/NULL);
		int32_t L_30 = __this->get_repLength_7();
		int32_t L_31 = V_2;
		__this->set_repLength_7(((int32_t)((int32_t)L_30+(int32_t)L_31)));
	}

IL_010c:
	{
		__this->set_mode_4(((int32_t)9));
	}

IL_0114:
	{
		InflaterHuffmanTree_t1141403250 * L_32 = __this->get_distTree_18();
		StreamManipulator_t2348681196 * L_33 = __this->get_input_14();
		NullCheck(L_32);
		int32_t L_34 = InflaterHuffmanTree_GetSymbol_m2276886475(L_32, L_33, /*hidden argument*/NULL);
		V_1 = L_34;
		int32_t L_35 = V_1;
		if ((((int32_t)L_35) >= ((int32_t)0)))
		{
			goto IL_012c;
		}
	}
	{
		return (bool)0;
	}

IL_012c:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(Inflater_t1975778921_il2cpp_TypeInfo_var);
		Int32U5BU5D_t3230847821* L_36 = ((Inflater_t1975778921_StaticFields*)Inflater_t1975778921_il2cpp_TypeInfo_var->static_fields)->get_CPDIST_2();
		int32_t L_37 = V_1;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, L_37);
		int32_t L_38 = L_37;
		int32_t L_39 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		__this->set_repDist_8(L_39);
		Int32U5BU5D_t3230847821* L_40 = ((Inflater_t1975778921_StaticFields*)Inflater_t1975778921_il2cpp_TypeInfo_var->static_fields)->get_CPDEXT_3();
		int32_t L_41 = V_1;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, L_41);
		int32_t L_42 = L_41;
		int32_t L_43 = (L_40)->GetAt(static_cast<il2cpp_array_size_t>(L_42));
		__this->set_neededBits_6(L_43);
		goto IL_0154;
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0148;
		throw e;
	}

CATCH_0148:
	{ // begin catch(System.Exception)
		SharpZipBaseException_t2520014981 * L_44 = (SharpZipBaseException_t2520014981 *)il2cpp_codegen_object_new(SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m2797155132(L_44, _stringLiteral2957899034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_44);
	} // end catch (depth: 1)

IL_0154:
	{
		int32_t L_45 = __this->get_neededBits_6();
		if ((((int32_t)L_45) <= ((int32_t)0)))
		{
			goto IL_019c;
		}
	}
	{
		__this->set_mode_4(((int32_t)10));
		StreamManipulator_t2348681196 * L_46 = __this->get_input_14();
		int32_t L_47 = __this->get_neededBits_6();
		NullCheck(L_46);
		int32_t L_48 = StreamManipulator_PeekBits_m2187722491(L_46, L_47, /*hidden argument*/NULL);
		V_3 = L_48;
		int32_t L_49 = V_3;
		if ((((int32_t)L_49) >= ((int32_t)0)))
		{
			goto IL_017d;
		}
	}
	{
		return (bool)0;
	}

IL_017d:
	{
		StreamManipulator_t2348681196 * L_50 = __this->get_input_14();
		int32_t L_51 = __this->get_neededBits_6();
		NullCheck(L_50);
		StreamManipulator_DropBits_m3482227099(L_50, L_51, /*hidden argument*/NULL);
		int32_t L_52 = __this->get_repDist_8();
		int32_t L_53 = V_3;
		__this->set_repDist_8(((int32_t)((int32_t)L_52+(int32_t)L_53)));
	}

IL_019c:
	{
		OutputWindow_t617095569 * L_54 = __this->get_outputWindow_15();
		int32_t L_55 = __this->get_repLength_7();
		int32_t L_56 = __this->get_repDist_8();
		NullCheck(L_54);
		OutputWindow_Repeat_m2942755967(L_54, L_55, L_56, /*hidden argument*/NULL);
		int32_t L_57 = V_0;
		int32_t L_58 = __this->get_repLength_7();
		V_0 = ((int32_t)((int32_t)L_57-(int32_t)L_58));
		__this->set_mode_4(7);
		goto IL_01d0;
	}

IL_01c5:
	{
		SharpZipBaseException_t2520014981 * L_59 = (SharpZipBaseException_t2520014981 *)il2cpp_codegen_object_new(SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m2797155132(L_59, _stringLiteral1269055054, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_59);
	}

IL_01d0:
	{
		int32_t L_60 = V_0;
		if ((((int32_t)L_60) >= ((int32_t)((int32_t)258))))
		{
			goto IL_0011;
		}
	}
	{
		return (bool)1;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::DecodeChksum()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3586058428;
extern Il2CppCodeGenString* _stringLiteral33179983;
extern const uint32_t Inflater_DecodeChksum_m3418579664_MetadataUsageId;
extern "C"  bool Inflater_DecodeChksum_m3418579664 (Inflater_t1975778921 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Inflater_DecodeChksum_m3418579664_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	ObjectU5BU5D_t1108656482* V_1 = NULL;
	{
		goto IL_003f;
	}

IL_0002:
	{
		StreamManipulator_t2348681196 * L_0 = __this->get_input_14();
		NullCheck(L_0);
		int32_t L_1 = StreamManipulator_PeekBits_m2187722491(L_0, 8, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		return (bool)0;
	}

IL_0015:
	{
		StreamManipulator_t2348681196 * L_3 = __this->get_input_14();
		NullCheck(L_3);
		StreamManipulator_DropBits_m3482227099(L_3, 8, /*hidden argument*/NULL);
		int32_t L_4 = __this->get_readAdler_5();
		int32_t L_5 = V_0;
		__this->set_readAdler_5(((int32_t)((int32_t)((int32_t)((int32_t)L_4<<(int32_t)8))|(int32_t)L_5)));
		int32_t L_6 = __this->get_neededBits_6();
		__this->set_neededBits_6(((int32_t)((int32_t)L_6-(int32_t)8)));
	}

IL_003f:
	{
		int32_t L_7 = __this->get_neededBits_6();
		if ((((int32_t)L_7) > ((int32_t)0)))
		{
			goto IL_0002;
		}
	}
	{
		Adler32_t2680377483 * L_8 = __this->get_adler_19();
		NullCheck(L_8);
		int64_t L_9 = Adler32_get_Value_m2923380521(L_8, /*hidden argument*/NULL);
		int32_t L_10 = __this->get_readAdler_5();
		if ((((int32_t)(((int32_t)((int32_t)L_9)))) == ((int32_t)L_10)))
		{
			goto IL_00a1;
		}
	}
	{
		V_1 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)4));
		ObjectU5BU5D_t1108656482* L_11 = V_1;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, _stringLiteral3586058428);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3586058428);
		ObjectU5BU5D_t1108656482* L_12 = V_1;
		Adler32_t2680377483 * L_13 = __this->get_adler_19();
		NullCheck(L_13);
		int64_t L_14 = Adler32_get_Value_m2923380521(L_13, /*hidden argument*/NULL);
		int32_t L_15 = (((int32_t)((int32_t)L_14)));
		Il2CppObject * L_16 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		ArrayElementTypeCheck (L_12, L_16);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_16);
		ObjectU5BU5D_t1108656482* L_17 = V_1;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 2);
		ArrayElementTypeCheck (L_17, _stringLiteral33179983);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral33179983);
		ObjectU5BU5D_t1108656482* L_18 = V_1;
		int32_t L_19 = __this->get_readAdler_5();
		int32_t L_20 = L_19;
		Il2CppObject * L_21 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 3);
		ArrayElementTypeCheck (L_18, L_21);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_21);
		ObjectU5BU5D_t1108656482* L_22 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m3016520001(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		SharpZipBaseException_t2520014981 * L_24 = (SharpZipBaseException_t2520014981 *)il2cpp_codegen_object_new(SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m2797155132(L_24, L_23, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	}

IL_00a1:
	{
		__this->set_mode_4(((int32_t)12));
		return (bool)0;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::Decode()
extern Il2CppClass* InflaterHuffmanTree_t1141403250_il2cpp_TypeInfo_var;
extern Il2CppClass* InflaterDynHeader_t1952417709_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2825245661;
extern Il2CppCodeGenString* _stringLiteral147232242;
extern Il2CppCodeGenString* _stringLiteral430352776;
extern const uint32_t Inflater_Decode_m2502049675_MetadataUsageId;
extern "C"  bool Inflater_Decode_m2502049675 (Inflater_t1975778921 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Inflater_Decode_m2502049675_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		int32_t L_0 = __this->get_mode_4();
		V_3 = L_0;
		int32_t L_1 = V_3;
		if (L_1 == 0)
		{
			goto IL_0046;
		}
		if (L_1 == 1)
		{
			goto IL_004d;
		}
		if (L_1 == 2)
		{
			goto IL_005b;
		}
		if (L_1 == 3)
		{
			goto IL_0136;
		}
		if (L_1 == 4)
		{
			goto IL_0167;
		}
		if (L_1 == 5)
		{
			goto IL_01a9;
		}
		if (L_1 == 6)
		{
			goto IL_01ef;
		}
		if (L_1 == 7)
		{
			goto IL_022d;
		}
		if (L_1 == 8)
		{
			goto IL_022d;
		}
		if (L_1 == 9)
		{
			goto IL_022d;
		}
		if (L_1 == 10)
		{
			goto IL_022d;
		}
		if (L_1 == 11)
		{
			goto IL_0054;
		}
		if (L_1 == 12)
		{
			goto IL_0234;
		}
	}
	{
		goto IL_0236;
	}

IL_0046:
	{
		bool L_2 = Inflater_DecodeHeader_m579569528(__this, /*hidden argument*/NULL);
		return L_2;
	}

IL_004d:
	{
		bool L_3 = Inflater_DecodeDict_m3876441313(__this, /*hidden argument*/NULL);
		return L_3;
	}

IL_0054:
	{
		bool L_4 = Inflater_DecodeChksum_m3418579664(__this, /*hidden argument*/NULL);
		return L_4;
	}

IL_005b:
	{
		bool L_5 = __this->get_isLastBlock_10();
		if (!L_5)
		{
			goto IL_0092;
		}
	}
	{
		bool L_6 = __this->get_noHeader_13();
		if (!L_6)
		{
			goto IL_0075;
		}
	}
	{
		__this->set_mode_4(((int32_t)12));
		return (bool)0;
	}

IL_0075:
	{
		StreamManipulator_t2348681196 * L_7 = __this->get_input_14();
		NullCheck(L_7);
		StreamManipulator_SkipToByteBoundary_m2882027201(L_7, /*hidden argument*/NULL);
		__this->set_neededBits_6(((int32_t)32));
		__this->set_mode_4(((int32_t)11));
		return (bool)1;
	}

IL_0092:
	{
		StreamManipulator_t2348681196 * L_8 = __this->get_input_14();
		NullCheck(L_8);
		int32_t L_9 = StreamManipulator_PeekBits_m2187722491(L_8, 3, /*hidden argument*/NULL);
		V_0 = L_9;
		int32_t L_10 = V_0;
		if ((((int32_t)L_10) >= ((int32_t)0)))
		{
			goto IL_00a5;
		}
	}
	{
		return (bool)0;
	}

IL_00a5:
	{
		StreamManipulator_t2348681196 * L_11 = __this->get_input_14();
		NullCheck(L_11);
		StreamManipulator_DropBits_m3482227099(L_11, 3, /*hidden argument*/NULL);
		int32_t L_12 = V_0;
		if (!((int32_t)((int32_t)L_12&(int32_t)1)))
		{
			goto IL_00bd;
		}
	}
	{
		__this->set_isLastBlock_10((bool)1);
	}

IL_00bd:
	{
		int32_t L_13 = V_0;
		V_4 = ((int32_t)((int32_t)L_13>>(int32_t)1));
		int32_t L_14 = V_4;
		if (L_14 == 0)
		{
			goto IL_00d7;
		}
		if (L_14 == 1)
		{
			goto IL_00eb;
		}
		if (L_14 == 2)
		{
			goto IL_010a;
		}
	}
	{
		goto IL_011e;
	}

IL_00d7:
	{
		StreamManipulator_t2348681196 * L_15 = __this->get_input_14();
		NullCheck(L_15);
		StreamManipulator_SkipToByteBoundary_m2882027201(L_15, /*hidden argument*/NULL);
		__this->set_mode_4(3);
		goto IL_0134;
	}

IL_00eb:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InflaterHuffmanTree_t1141403250_il2cpp_TypeInfo_var);
		InflaterHuffmanTree_t1141403250 * L_16 = ((InflaterHuffmanTree_t1141403250_StaticFields*)InflaterHuffmanTree_t1141403250_il2cpp_TypeInfo_var->static_fields)->get_defLitLenTree_1();
		__this->set_litlenTree_17(L_16);
		InflaterHuffmanTree_t1141403250 * L_17 = ((InflaterHuffmanTree_t1141403250_StaticFields*)InflaterHuffmanTree_t1141403250_il2cpp_TypeInfo_var->static_fields)->get_defDistTree_2();
		__this->set_distTree_18(L_17);
		__this->set_mode_4(7);
		goto IL_0134;
	}

IL_010a:
	{
		InflaterDynHeader_t1952417709 * L_18 = (InflaterDynHeader_t1952417709 *)il2cpp_codegen_object_new(InflaterDynHeader_t1952417709_il2cpp_TypeInfo_var);
		InflaterDynHeader__ctor_m648908231(L_18, /*hidden argument*/NULL);
		__this->set_dynHeader_16(L_18);
		__this->set_mode_4(6);
		goto IL_0134;
	}

IL_011e:
	{
		int32_t L_19 = V_0;
		int32_t L_20 = L_19;
		Il2CppObject * L_21 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m389863537(NULL /*static, unused*/, _stringLiteral2825245661, L_21, /*hidden argument*/NULL);
		SharpZipBaseException_t2520014981 * L_23 = (SharpZipBaseException_t2520014981 *)il2cpp_codegen_object_new(SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m2797155132(L_23, L_22, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_23);
	}

IL_0134:
	{
		return (bool)1;
	}

IL_0136:
	{
		StreamManipulator_t2348681196 * L_24 = __this->get_input_14();
		NullCheck(L_24);
		int32_t L_25 = StreamManipulator_PeekBits_m2187722491(L_24, ((int32_t)16), /*hidden argument*/NULL);
		int32_t L_26 = L_25;
		V_5 = L_26;
		__this->set_uncomprLen_9(L_26);
		int32_t L_27 = V_5;
		if ((((int32_t)L_27) >= ((int32_t)0)))
		{
			goto IL_0153;
		}
	}
	{
		return (bool)0;
	}

IL_0153:
	{
		StreamManipulator_t2348681196 * L_28 = __this->get_input_14();
		NullCheck(L_28);
		StreamManipulator_DropBits_m3482227099(L_28, ((int32_t)16), /*hidden argument*/NULL);
		__this->set_mode_4(4);
	}

IL_0167:
	{
		StreamManipulator_t2348681196 * L_29 = __this->get_input_14();
		NullCheck(L_29);
		int32_t L_30 = StreamManipulator_PeekBits_m2187722491(L_29, ((int32_t)16), /*hidden argument*/NULL);
		V_1 = L_30;
		int32_t L_31 = V_1;
		if ((((int32_t)L_31) >= ((int32_t)0)))
		{
			goto IL_017b;
		}
	}
	{
		return (bool)0;
	}

IL_017b:
	{
		StreamManipulator_t2348681196 * L_32 = __this->get_input_14();
		NullCheck(L_32);
		StreamManipulator_DropBits_m3482227099(L_32, ((int32_t)16), /*hidden argument*/NULL);
		int32_t L_33 = V_1;
		int32_t L_34 = __this->get_uncomprLen_9();
		if ((((int32_t)L_33) == ((int32_t)((int32_t)((int32_t)L_34^(int32_t)((int32_t)65535))))))
		{
			goto IL_01a2;
		}
	}
	{
		SharpZipBaseException_t2520014981 * L_35 = (SharpZipBaseException_t2520014981 *)il2cpp_codegen_object_new(SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m2797155132(L_35, _stringLiteral147232242, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_35);
	}

IL_01a2:
	{
		__this->set_mode_4(5);
	}

IL_01a9:
	{
		OutputWindow_t617095569 * L_36 = __this->get_outputWindow_15();
		StreamManipulator_t2348681196 * L_37 = __this->get_input_14();
		int32_t L_38 = __this->get_uncomprLen_9();
		NullCheck(L_36);
		int32_t L_39 = OutputWindow_CopyStored_m495876610(L_36, L_37, L_38, /*hidden argument*/NULL);
		V_2 = L_39;
		int32_t L_40 = __this->get_uncomprLen_9();
		int32_t L_41 = V_2;
		__this->set_uncomprLen_9(((int32_t)((int32_t)L_40-(int32_t)L_41)));
		int32_t L_42 = __this->get_uncomprLen_9();
		if (L_42)
		{
			goto IL_01e0;
		}
	}
	{
		__this->set_mode_4(2);
		return (bool)1;
	}

IL_01e0:
	{
		StreamManipulator_t2348681196 * L_43 = __this->get_input_14();
		NullCheck(L_43);
		bool L_44 = StreamManipulator_get_IsNeedingInput_m89298932(L_43, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_44) == ((int32_t)0))? 1 : 0);
	}

IL_01ef:
	{
		InflaterDynHeader_t1952417709 * L_45 = __this->get_dynHeader_16();
		StreamManipulator_t2348681196 * L_46 = __this->get_input_14();
		NullCheck(L_45);
		bool L_47 = InflaterDynHeader_Decode_m3398615020(L_45, L_46, /*hidden argument*/NULL);
		if (L_47)
		{
			goto IL_0204;
		}
	}
	{
		return (bool)0;
	}

IL_0204:
	{
		InflaterDynHeader_t1952417709 * L_48 = __this->get_dynHeader_16();
		NullCheck(L_48);
		InflaterHuffmanTree_t1141403250 * L_49 = InflaterDynHeader_BuildLitLenTree_m1654794856(L_48, /*hidden argument*/NULL);
		__this->set_litlenTree_17(L_49);
		InflaterDynHeader_t1952417709 * L_50 = __this->get_dynHeader_16();
		NullCheck(L_50);
		InflaterHuffmanTree_t1141403250 * L_51 = InflaterDynHeader_BuildDistTree_m3505675472(L_50, /*hidden argument*/NULL);
		__this->set_distTree_18(L_51);
		__this->set_mode_4(7);
	}

IL_022d:
	{
		bool L_52 = Inflater_DecodeHuffman_m3108706180(__this, /*hidden argument*/NULL);
		return L_52;
	}

IL_0234:
	{
		return (bool)0;
	}

IL_0236:
	{
		SharpZipBaseException_t2520014981 * L_53 = (SharpZipBaseException_t2520014981 *)il2cpp_codegen_object_new(SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m2797155132(L_53, _stringLiteral430352776, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_53);
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::SetInput(System.Byte[],System.Int32,System.Int32)
extern "C"  void Inflater_SetInput_m2977611094 (Inflater_t1975778921 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___index1, int32_t ___count2, const MethodInfo* method)
{
	{
		StreamManipulator_t2348681196 * L_0 = __this->get_input_14();
		ByteU5BU5D_t4260760469* L_1 = ___buffer0;
		int32_t L_2 = ___index1;
		int32_t L_3 = ___count2;
		NullCheck(L_0);
		StreamManipulator_SetInput_m695463628(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		int64_t L_4 = __this->get_totalIn_12();
		int32_t L_5 = ___count2;
		__this->set_totalIn_12(((int64_t)((int64_t)L_4+(int64_t)(((int64_t)((int64_t)L_5))))));
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::Inflate(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2916848704;
extern Il2CppCodeGenString* _stringLiteral94851343;
extern Il2CppCodeGenString* _stringLiteral1711958662;
extern Il2CppCodeGenString* _stringLiteral3275187347;
extern Il2CppCodeGenString* _stringLiteral2429943938;
extern Il2CppCodeGenString* _stringLiteral3419074339;
extern const uint32_t Inflater_Inflate_m2860834391_MetadataUsageId;
extern "C"  int32_t Inflater_Inflate_m2860834391 (Inflater_t1975778921 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Inflater_Inflate_m2860834391_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		ByteU5BU5D_t4260760469* L_0 = ___buffer0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral2916848704, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		int32_t L_2 = ___count2;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0022;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1193970951(L_3, _stringLiteral94851343, _stringLiteral1711958662, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		int32_t L_4 = ___offset1;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_5 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1193970951(L_5, _stringLiteral3275187347, _stringLiteral2429943938, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0036:
	{
		int32_t L_6 = ___offset1;
		int32_t L_7 = ___count2;
		ByteU5BU5D_t4260760469* L_8 = ___buffer0;
		NullCheck(L_8);
		if ((((int32_t)((int32_t)((int32_t)L_6+(int32_t)L_7))) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_0049;
		}
	}
	{
		ArgumentException_t928607144 * L_9 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_9, _stringLiteral3419074339, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0049:
	{
		int32_t L_10 = ___count2;
		if (L_10)
		{
			goto IL_005d;
		}
	}
	{
		bool L_11 = Inflater_get_IsFinished_m3219719202(__this, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_005b;
		}
	}
	{
		Inflater_Decode_m2502049675(__this, /*hidden argument*/NULL);
	}

IL_005b:
	{
		return 0;
	}

IL_005d:
	{
		V_0 = 0;
	}

IL_005f:
	{
		int32_t L_12 = __this->get_mode_4();
		if ((((int32_t)L_12) == ((int32_t)((int32_t)11))))
		{
			goto IL_00ac;
		}
	}
	{
		OutputWindow_t617095569 * L_13 = __this->get_outputWindow_15();
		ByteU5BU5D_t4260760469* L_14 = ___buffer0;
		int32_t L_15 = ___offset1;
		int32_t L_16 = ___count2;
		NullCheck(L_13);
		int32_t L_17 = OutputWindow_CopyOutput_m3388051483(L_13, L_14, L_15, L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		int32_t L_18 = V_1;
		if ((((int32_t)L_18) <= ((int32_t)0)))
		{
			goto IL_00ac;
		}
	}
	{
		Adler32_t2680377483 * L_19 = __this->get_adler_19();
		ByteU5BU5D_t4260760469* L_20 = ___buffer0;
		int32_t L_21 = ___offset1;
		int32_t L_22 = V_1;
		NullCheck(L_19);
		Adler32_Update_m2142069042(L_19, L_20, L_21, L_22, /*hidden argument*/NULL);
		int32_t L_23 = ___offset1;
		int32_t L_24 = V_1;
		___offset1 = ((int32_t)((int32_t)L_23+(int32_t)L_24));
		int32_t L_25 = V_0;
		int32_t L_26 = V_1;
		V_0 = ((int32_t)((int32_t)L_25+(int32_t)L_26));
		int64_t L_27 = __this->get_totalOut_11();
		int32_t L_28 = V_1;
		__this->set_totalOut_11(((int64_t)((int64_t)L_27+(int64_t)(((int64_t)((int64_t)L_28))))));
		int32_t L_29 = ___count2;
		int32_t L_30 = V_1;
		___count2 = ((int32_t)((int32_t)L_29-(int32_t)L_30));
		int32_t L_31 = ___count2;
		if (L_31)
		{
			goto IL_00ac;
		}
	}
	{
		int32_t L_32 = V_0;
		return L_32;
	}

IL_00ac:
	{
		bool L_33 = Inflater_Decode_m2502049675(__this, /*hidden argument*/NULL);
		if (L_33)
		{
			goto IL_005f;
		}
	}
	{
		OutputWindow_t617095569 * L_34 = __this->get_outputWindow_15();
		NullCheck(L_34);
		int32_t L_35 = OutputWindow_GetAvailable_m136666699(L_34, /*hidden argument*/NULL);
		if ((((int32_t)L_35) <= ((int32_t)0)))
		{
			goto IL_00cc;
		}
	}
	{
		int32_t L_36 = __this->get_mode_4();
		if ((!(((uint32_t)L_36) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_005f;
		}
	}

IL_00cc:
	{
		int32_t L_37 = V_0;
		return L_37;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_IsNeedingInput()
extern "C"  bool Inflater_get_IsNeedingInput_m2862282958 (Inflater_t1975778921 * __this, const MethodInfo* method)
{
	{
		StreamManipulator_t2348681196 * L_0 = __this->get_input_14();
		NullCheck(L_0);
		bool L_1 = StreamManipulator_get_IsNeedingInput_m89298932(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_IsNeedingDictionary()
extern "C"  bool Inflater_get_IsNeedingDictionary_m479189876 (Inflater_t1975778921 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mode_4();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = __this->get_neededBits_6();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0013:
	{
		return (bool)0;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_IsFinished()
extern "C"  bool Inflater_get_IsFinished_m3219719202 (Inflater_t1975778921 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_mode_4();
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_0019;
		}
	}
	{
		OutputWindow_t617095569 * L_1 = __this->get_outputWindow_15();
		NullCheck(L_1);
		int32_t L_2 = OutputWindow_GetAvailable_m136666699(L_1, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}

IL_0019:
	{
		return (bool)0;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_TotalOut()
extern "C"  int64_t Inflater_get_TotalOut_m3201385003 (Inflater_t1975778921 * __this, const MethodInfo* method)
{
	{
		int64_t L_0 = __this->get_totalOut_11();
		return L_0;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Inflater::get_RemainingInput()
extern "C"  int32_t Inflater_get_RemainingInput_m39630580 (Inflater_t1975778921 * __this, const MethodInfo* method)
{
	{
		StreamManipulator_t2348681196 * L_0 = __this->get_input_14();
		NullCheck(L_0);
		int32_t L_1 = StreamManipulator_get_AvailableBytes_m3731022420(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Inflater::.cctor()
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern Il2CppClass* Inflater_t1975778921_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004fdU2D1_9_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004fdU2D2_10_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004fdU2D3_11_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004fdU2D4_12_FieldInfo_var;
extern const uint32_t Inflater__cctor_m4087155284_MetadataUsageId;
extern "C"  void Inflater__cctor_m4087155284 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Inflater__cctor_m4087155284_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Int32U5BU5D_t3230847821* L_0 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)29)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004fdU2D1_9_FieldInfo_var), /*hidden argument*/NULL);
		((Inflater_t1975778921_StaticFields*)Inflater_t1975778921_il2cpp_TypeInfo_var->static_fields)->set_CPLENS_0(L_0);
		Int32U5BU5D_t3230847821* L_1 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)29)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004fdU2D2_10_FieldInfo_var), /*hidden argument*/NULL);
		((Inflater_t1975778921_StaticFields*)Inflater_t1975778921_il2cpp_TypeInfo_var->static_fields)->set_CPLEXT_1(L_1);
		Int32U5BU5D_t3230847821* L_2 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)30)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_2, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004fdU2D3_11_FieldInfo_var), /*hidden argument*/NULL);
		((Inflater_t1975778921_StaticFields*)Inflater_t1975778921_il2cpp_TypeInfo_var->static_fields)->set_CPDIST_2(L_2);
		Int32U5BU5D_t3230847821* L_3 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)30)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_3, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004fdU2D4_12_FieldInfo_var), /*hidden argument*/NULL);
		((Inflater_t1975778921_StaticFields*)Inflater_t1975778921_il2cpp_TypeInfo_var->static_fields)->set_CPDEXT_3(L_3);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::.ctor()
extern "C"  void InflaterDynHeader__ctor_m648908231 (InflaterDynHeader_t1952417709 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::Decode(ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator)
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern Il2CppClass* InflaterDynHeader_t1952417709_il2cpp_TypeInfo_var;
extern Il2CppClass* InflaterHuffmanTree_t1141403250_il2cpp_TypeInfo_var;
extern Il2CppClass* SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var;
extern const uint32_t InflaterDynHeader_Decode_m3398615020_MetadataUsageId;
extern "C"  bool InflaterDynHeader_Decode_m3398615020 (InflaterDynHeader_t1952417709 * __this, StreamManipulator_t2348681196 * ___input0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InflaterDynHeader_Decode_m3398615020_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	uint8_t V_6 = 0x0;
	int32_t V_7 = 0;

IL_0000:
	{
		int32_t L_0 = __this->get_mode_6();
		V_4 = L_0;
		int32_t L_1 = V_4;
		if (L_1 == 0)
		{
			goto IL_0029;
		}
		if (L_1 == 1)
		{
			goto IL_0061;
		}
		if (L_1 == 2)
		{
			goto IL_00b9;
		}
		if (L_1 == 3)
		{
			goto IL_013b;
		}
		if (L_1 == 4)
		{
			goto IL_01a8;
		}
		if (L_1 == 5)
		{
			goto IL_01ee;
		}
	}
	{
		goto IL_0000;
	}

IL_0029:
	{
		StreamManipulator_t2348681196 * L_2 = ___input0;
		NullCheck(L_2);
		int32_t L_3 = StreamManipulator_PeekBits_m2187722491(L_2, 5, /*hidden argument*/NULL);
		__this->set_lnum_7(L_3);
		int32_t L_4 = __this->get_lnum_7();
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_0041;
		}
	}
	{
		return (bool)0;
	}

IL_0041:
	{
		int32_t L_5 = __this->get_lnum_7();
		__this->set_lnum_7(((int32_t)((int32_t)L_5+(int32_t)((int32_t)257))));
		StreamManipulator_t2348681196 * L_6 = ___input0;
		NullCheck(L_6);
		StreamManipulator_DropBits_m3482227099(L_6, 5, /*hidden argument*/NULL);
		__this->set_mode_6(1);
	}

IL_0061:
	{
		StreamManipulator_t2348681196 * L_7 = ___input0;
		NullCheck(L_7);
		int32_t L_8 = StreamManipulator_PeekBits_m2187722491(L_7, 5, /*hidden argument*/NULL);
		__this->set_dnum_8(L_8);
		int32_t L_9 = __this->get_dnum_8();
		if ((((int32_t)L_9) >= ((int32_t)0)))
		{
			goto IL_0079;
		}
	}
	{
		return (bool)0;
	}

IL_0079:
	{
		int32_t L_10 = __this->get_dnum_8();
		__this->set_dnum_8(((int32_t)((int32_t)L_10+(int32_t)1)));
		StreamManipulator_t2348681196 * L_11 = ___input0;
		NullCheck(L_11);
		StreamManipulator_DropBits_m3482227099(L_11, 5, /*hidden argument*/NULL);
		int32_t L_12 = __this->get_lnum_7();
		int32_t L_13 = __this->get_dnum_8();
		__this->set_num_10(((int32_t)((int32_t)L_12+(int32_t)L_13)));
		int32_t L_14 = __this->get_num_10();
		__this->set_litdistLens_4(((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)L_14)));
		__this->set_mode_6(2);
	}

IL_00b9:
	{
		StreamManipulator_t2348681196 * L_15 = ___input0;
		NullCheck(L_15);
		int32_t L_16 = StreamManipulator_PeekBits_m2187722491(L_15, 4, /*hidden argument*/NULL);
		__this->set_blnum_9(L_16);
		int32_t L_17 = __this->get_blnum_9();
		if ((((int32_t)L_17) >= ((int32_t)0)))
		{
			goto IL_00d1;
		}
	}
	{
		return (bool)0;
	}

IL_00d1:
	{
		int32_t L_18 = __this->get_blnum_9();
		__this->set_blnum_9(((int32_t)((int32_t)L_18+(int32_t)4)));
		StreamManipulator_t2348681196 * L_19 = ___input0;
		NullCheck(L_19);
		StreamManipulator_DropBits_m3482227099(L_19, 4, /*hidden argument*/NULL);
		__this->set_blLens_3(((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)((int32_t)19))));
		__this->set_ptr_13(0);
		__this->set_mode_6(3);
		goto IL_013b;
	}

IL_0103:
	{
		StreamManipulator_t2348681196 * L_20 = ___input0;
		NullCheck(L_20);
		int32_t L_21 = StreamManipulator_PeekBits_m2187722491(L_20, 3, /*hidden argument*/NULL);
		V_0 = L_21;
		int32_t L_22 = V_0;
		if ((((int32_t)L_22) >= ((int32_t)0)))
		{
			goto IL_0111;
		}
	}
	{
		return (bool)0;
	}

IL_0111:
	{
		StreamManipulator_t2348681196 * L_23 = ___input0;
		NullCheck(L_23);
		StreamManipulator_DropBits_m3482227099(L_23, 3, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_24 = __this->get_blLens_3();
		IL2CPP_RUNTIME_CLASS_INIT(InflaterDynHeader_t1952417709_il2cpp_TypeInfo_var);
		Int32U5BU5D_t3230847821* L_25 = ((InflaterDynHeader_t1952417709_StaticFields*)InflaterDynHeader_t1952417709_il2cpp_TypeInfo_var->static_fields)->get_BL_ORDER_2();
		int32_t L_26 = __this->get_ptr_13();
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		int32_t L_29 = V_0;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, L_28);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(L_28), (uint8_t)(((int32_t)((uint8_t)L_29))));
		int32_t L_30 = __this->get_ptr_13();
		__this->set_ptr_13(((int32_t)((int32_t)L_30+(int32_t)1)));
	}

IL_013b:
	{
		int32_t L_31 = __this->get_ptr_13();
		int32_t L_32 = __this->get_blnum_9();
		if ((((int32_t)L_31) < ((int32_t)L_32)))
		{
			goto IL_0103;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_33 = __this->get_blLens_3();
		InflaterHuffmanTree_t1141403250 * L_34 = (InflaterHuffmanTree_t1141403250 *)il2cpp_codegen_object_new(InflaterHuffmanTree_t1141403250_il2cpp_TypeInfo_var);
		InflaterHuffmanTree__ctor_m1285149863(L_34, L_33, /*hidden argument*/NULL);
		__this->set_blTree_5(L_34);
		__this->set_blLens_3((ByteU5BU5D_t4260760469*)NULL);
		__this->set_ptr_13(0);
		__this->set_mode_6(4);
		goto IL_01a8;
	}

IL_0171:
	{
		ByteU5BU5D_t4260760469* L_35 = __this->get_litdistLens_4();
		int32_t L_36 = __this->get_ptr_13();
		int32_t L_37 = L_36;
		V_5 = L_37;
		__this->set_ptr_13(((int32_t)((int32_t)L_37+(int32_t)1)));
		int32_t L_38 = V_5;
		int32_t L_39 = V_1;
		int32_t L_40 = (((int32_t)((uint8_t)L_39)));
		V_6 = L_40;
		__this->set_lastLen_12(L_40);
		uint8_t L_41 = V_6;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, L_38);
		(L_35)->SetAt(static_cast<il2cpp_array_size_t>(L_38), (uint8_t)L_41);
		int32_t L_42 = __this->get_ptr_13();
		int32_t L_43 = __this->get_num_10();
		if ((!(((uint32_t)L_42) == ((uint32_t)L_43))))
		{
			goto IL_01a8;
		}
	}
	{
		return (bool)1;
	}

IL_01a8:
	{
		InflaterHuffmanTree_t1141403250 * L_44 = __this->get_blTree_5();
		StreamManipulator_t2348681196 * L_45 = ___input0;
		NullCheck(L_44);
		int32_t L_46 = InflaterHuffmanTree_GetSymbol_m2276886475(L_44, L_45, /*hidden argument*/NULL);
		int32_t L_47 = L_46;
		V_1 = L_47;
		if (!((int32_t)((int32_t)L_47&(int32_t)((int32_t)-16))))
		{
			goto IL_0171;
		}
	}
	{
		int32_t L_48 = V_1;
		if ((((int32_t)L_48) >= ((int32_t)0)))
		{
			goto IL_01c1;
		}
	}
	{
		return (bool)0;
	}

IL_01c1:
	{
		int32_t L_49 = V_1;
		if ((((int32_t)L_49) < ((int32_t)((int32_t)17))))
		{
			goto IL_01cf;
		}
	}
	{
		__this->set_lastLen_12(0);
		goto IL_01dd;
	}

IL_01cf:
	{
		int32_t L_50 = __this->get_ptr_13();
		if (L_50)
		{
			goto IL_01dd;
		}
	}
	{
		SharpZipBaseException_t2520014981 * L_51 = (SharpZipBaseException_t2520014981 *)il2cpp_codegen_object_new(SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m730469766(L_51, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_51);
	}

IL_01dd:
	{
		int32_t L_52 = V_1;
		__this->set_repSymbol_11(((int32_t)((int32_t)L_52-(int32_t)((int32_t)16))));
		__this->set_mode_6(5);
	}

IL_01ee:
	{
		IL2CPP_RUNTIME_CLASS_INIT(InflaterDynHeader_t1952417709_il2cpp_TypeInfo_var);
		Int32U5BU5D_t3230847821* L_53 = ((InflaterDynHeader_t1952417709_StaticFields*)InflaterDynHeader_t1952417709_il2cpp_TypeInfo_var->static_fields)->get_repBits_1();
		int32_t L_54 = __this->get_repSymbol_11();
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, L_54);
		int32_t L_55 = L_54;
		int32_t L_56 = (L_53)->GetAt(static_cast<il2cpp_array_size_t>(L_55));
		V_2 = L_56;
		StreamManipulator_t2348681196 * L_57 = ___input0;
		int32_t L_58 = V_2;
		NullCheck(L_57);
		int32_t L_59 = StreamManipulator_PeekBits_m2187722491(L_57, L_58, /*hidden argument*/NULL);
		V_3 = L_59;
		int32_t L_60 = V_3;
		if ((((int32_t)L_60) >= ((int32_t)0)))
		{
			goto IL_0209;
		}
	}
	{
		return (bool)0;
	}

IL_0209:
	{
		StreamManipulator_t2348681196 * L_61 = ___input0;
		int32_t L_62 = V_2;
		NullCheck(L_61);
		StreamManipulator_DropBits_m3482227099(L_61, L_62, /*hidden argument*/NULL);
		int32_t L_63 = V_3;
		IL2CPP_RUNTIME_CLASS_INIT(InflaterDynHeader_t1952417709_il2cpp_TypeInfo_var);
		Int32U5BU5D_t3230847821* L_64 = ((InflaterDynHeader_t1952417709_StaticFields*)InflaterDynHeader_t1952417709_il2cpp_TypeInfo_var->static_fields)->get_repMin_0();
		int32_t L_65 = __this->get_repSymbol_11();
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, L_65);
		int32_t L_66 = L_65;
		int32_t L_67 = (L_64)->GetAt(static_cast<il2cpp_array_size_t>(L_66));
		V_3 = ((int32_t)((int32_t)L_63+(int32_t)L_67));
		int32_t L_68 = __this->get_ptr_13();
		int32_t L_69 = V_3;
		int32_t L_70 = __this->get_num_10();
		if ((((int32_t)((int32_t)((int32_t)L_68+(int32_t)L_69))) <= ((int32_t)L_70)))
		{
			goto IL_0255;
		}
	}
	{
		SharpZipBaseException_t2520014981 * L_71 = (SharpZipBaseException_t2520014981 *)il2cpp_codegen_object_new(SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m730469766(L_71, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_71);
	}

IL_0235:
	{
		ByteU5BU5D_t4260760469* L_72 = __this->get_litdistLens_4();
		int32_t L_73 = __this->get_ptr_13();
		int32_t L_74 = L_73;
		V_7 = L_74;
		__this->set_ptr_13(((int32_t)((int32_t)L_74+(int32_t)1)));
		int32_t L_75 = V_7;
		uint8_t L_76 = __this->get_lastLen_12();
		NullCheck(L_72);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_72, L_75);
		(L_72)->SetAt(static_cast<il2cpp_array_size_t>(L_75), (uint8_t)L_76);
	}

IL_0255:
	{
		int32_t L_77 = V_3;
		int32_t L_78 = L_77;
		V_3 = ((int32_t)((int32_t)L_78-(int32_t)1));
		if ((((int32_t)L_78) > ((int32_t)0)))
		{
			goto IL_0235;
		}
	}
	{
		int32_t L_79 = __this->get_ptr_13();
		int32_t L_80 = __this->get_num_10();
		if ((!(((uint32_t)L_79) == ((uint32_t)L_80))))
		{
			goto IL_026d;
		}
	}
	{
		return (bool)1;
	}

IL_026d:
	{
		__this->set_mode_6(4);
		goto IL_0000;
	}
}
// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::BuildLitLenTree()
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern Il2CppClass* InflaterHuffmanTree_t1141403250_il2cpp_TypeInfo_var;
extern const uint32_t InflaterDynHeader_BuildLitLenTree_m1654794856_MetadataUsageId;
extern "C"  InflaterHuffmanTree_t1141403250 * InflaterDynHeader_BuildLitLenTree_m1654794856 (InflaterDynHeader_t1952417709 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InflaterDynHeader_BuildLitLenTree_m1654794856_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	{
		int32_t L_0 = __this->get_lnum_7();
		V_0 = ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)L_0));
		ByteU5BU5D_t4260760469* L_1 = __this->get_litdistLens_4();
		ByteU5BU5D_t4260760469* L_2 = V_0;
		int32_t L_3 = __this->get_lnum_7();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, 0, (Il2CppArray *)(Il2CppArray *)L_2, 0, L_3, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_4 = V_0;
		InflaterHuffmanTree_t1141403250 * L_5 = (InflaterHuffmanTree_t1141403250 *)il2cpp_codegen_object_new(InflaterHuffmanTree_t1141403250_il2cpp_TypeInfo_var);
		InflaterHuffmanTree__ctor_m1285149863(L_5, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::BuildDistTree()
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern Il2CppClass* InflaterHuffmanTree_t1141403250_il2cpp_TypeInfo_var;
extern const uint32_t InflaterDynHeader_BuildDistTree_m3505675472_MetadataUsageId;
extern "C"  InflaterHuffmanTree_t1141403250 * InflaterDynHeader_BuildDistTree_m3505675472 (InflaterDynHeader_t1952417709 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InflaterDynHeader_BuildDistTree_m3505675472_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	{
		int32_t L_0 = __this->get_dnum_8();
		V_0 = ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)L_0));
		ByteU5BU5D_t4260760469* L_1 = __this->get_litdistLens_4();
		int32_t L_2 = __this->get_lnum_7();
		ByteU5BU5D_t4260760469* L_3 = V_0;
		int32_t L_4 = __this->get_dnum_8();
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, L_2, (Il2CppArray *)(Il2CppArray *)L_3, 0, L_4, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_5 = V_0;
		InflaterHuffmanTree_t1141403250 * L_6 = (InflaterHuffmanTree_t1141403250 *)il2cpp_codegen_object_new(InflaterHuffmanTree_t1141403250_il2cpp_TypeInfo_var);
		InflaterHuffmanTree__ctor_m1285149863(L_6, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterDynHeader::.cctor()
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern Il2CppClass* InflaterDynHeader_t1952417709_il2cpp_TypeInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004feU2D1_13_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004feU2D2_14_FieldInfo_var;
extern FieldInfo* U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004feU2D3_15_FieldInfo_var;
extern const uint32_t InflaterDynHeader__cctor_m2454189766_MetadataUsageId;
extern "C"  void InflaterDynHeader__cctor_m2454189766 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InflaterDynHeader__cctor_m2454189766_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Int32U5BU5D_t3230847821* L_0 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004feU2D1_13_FieldInfo_var), /*hidden argument*/NULL);
		((InflaterDynHeader_t1952417709_StaticFields*)InflaterDynHeader_t1952417709_il2cpp_TypeInfo_var->static_fields)->set_repMin_0(L_0);
		Int32U5BU5D_t3230847821* L_1 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)3));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_1, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004feU2D2_14_FieldInfo_var), /*hidden argument*/NULL);
		((InflaterDynHeader_t1952417709_StaticFields*)InflaterDynHeader_t1952417709_il2cpp_TypeInfo_var->static_fields)->set_repBits_1(L_1);
		Int32U5BU5D_t3230847821* L_2 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)19)));
		RuntimeHelpers_InitializeArray_m2058365049(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_2, LoadFieldToken(U3CPrivateImplementationDetailsU3EU7B8E8FA28DU2D216AU2D43ECU2D8DCBU2D2258D1F7BF00U7D_t1292624594____U24U24method0x60004feU2D3_15_FieldInfo_var), /*hidden argument*/NULL);
		((InflaterDynHeader_t1952417709_StaticFields*)InflaterDynHeader_t1952417709_il2cpp_TypeInfo_var->static_fields)->set_BL_ORDER_2(L_2);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::.cctor()
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern Il2CppClass* InflaterHuffmanTree_t1141403250_il2cpp_TypeInfo_var;
extern Il2CppClass* Exception_t3991598821_il2cpp_TypeInfo_var;
extern Il2CppClass* SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2425386098;
extern const uint32_t InflaterHuffmanTree__cctor_m1082490251_MetadataUsageId;
extern "C"  void InflaterHuffmanTree__cctor_m1082490251 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InflaterHuffmanTree__cctor_m1082490251_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	int32_t V_1 = 0;
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		{
			V_0 = ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)((int32_t)288)));
			V_1 = 0;
			goto IL_0017;
		}

IL_000f:
		{
			ByteU5BU5D_t4260760469* L_0 = V_0;
			int32_t L_1 = V_1;
			int32_t L_2 = L_1;
			V_1 = ((int32_t)((int32_t)L_2+(int32_t)1));
			NullCheck(L_0);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_2);
			(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (uint8_t)8);
		}

IL_0017:
		{
			int32_t L_3 = V_1;
			if ((((int32_t)L_3) < ((int32_t)((int32_t)144))))
			{
				goto IL_000f;
			}
		}

IL_001f:
		{
			goto IL_002a;
		}

IL_0021:
		{
			ByteU5BU5D_t4260760469* L_4 = V_0;
			int32_t L_5 = V_1;
			int32_t L_6 = L_5;
			V_1 = ((int32_t)((int32_t)L_6+(int32_t)1));
			NullCheck(L_4);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_6);
			(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (uint8_t)((int32_t)9));
		}

IL_002a:
		{
			int32_t L_7 = V_1;
			if ((((int32_t)L_7) < ((int32_t)((int32_t)256))))
			{
				goto IL_0021;
			}
		}

IL_0032:
		{
			goto IL_003c;
		}

IL_0034:
		{
			ByteU5BU5D_t4260760469* L_8 = V_0;
			int32_t L_9 = V_1;
			int32_t L_10 = L_9;
			V_1 = ((int32_t)((int32_t)L_10+(int32_t)1));
			NullCheck(L_8);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_10);
			(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (uint8_t)7);
		}

IL_003c:
		{
			int32_t L_11 = V_1;
			if ((((int32_t)L_11) < ((int32_t)((int32_t)280))))
			{
				goto IL_0034;
			}
		}

IL_0044:
		{
			goto IL_004e;
		}

IL_0046:
		{
			ByteU5BU5D_t4260760469* L_12 = V_0;
			int32_t L_13 = V_1;
			int32_t L_14 = L_13;
			V_1 = ((int32_t)((int32_t)L_14+(int32_t)1));
			NullCheck(L_12);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_14);
			(L_12)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (uint8_t)8);
		}

IL_004e:
		{
			int32_t L_15 = V_1;
			if ((((int32_t)L_15) < ((int32_t)((int32_t)288))))
			{
				goto IL_0046;
			}
		}

IL_0056:
		{
			ByteU5BU5D_t4260760469* L_16 = V_0;
			InflaterHuffmanTree_t1141403250 * L_17 = (InflaterHuffmanTree_t1141403250 *)il2cpp_codegen_object_new(InflaterHuffmanTree_t1141403250_il2cpp_TypeInfo_var);
			InflaterHuffmanTree__ctor_m1285149863(L_17, L_16, /*hidden argument*/NULL);
			((InflaterHuffmanTree_t1141403250_StaticFields*)InflaterHuffmanTree_t1141403250_il2cpp_TypeInfo_var->static_fields)->set_defLitLenTree_1(L_17);
			V_0 = ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)((int32_t)32)));
			V_1 = 0;
			goto IL_0075;
		}

IL_006d:
		{
			ByteU5BU5D_t4260760469* L_18 = V_0;
			int32_t L_19 = V_1;
			int32_t L_20 = L_19;
			V_1 = ((int32_t)((int32_t)L_20+(int32_t)1));
			NullCheck(L_18);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_20);
			(L_18)->SetAt(static_cast<il2cpp_array_size_t>(L_20), (uint8_t)5);
		}

IL_0075:
		{
			int32_t L_21 = V_1;
			if ((((int32_t)L_21) < ((int32_t)((int32_t)32))))
			{
				goto IL_006d;
			}
		}

IL_007a:
		{
			ByteU5BU5D_t4260760469* L_22 = V_0;
			InflaterHuffmanTree_t1141403250 * L_23 = (InflaterHuffmanTree_t1141403250 *)il2cpp_codegen_object_new(InflaterHuffmanTree_t1141403250_il2cpp_TypeInfo_var);
			InflaterHuffmanTree__ctor_m1285149863(L_23, L_22, /*hidden argument*/NULL);
			((InflaterHuffmanTree_t1141403250_StaticFields*)InflaterHuffmanTree_t1141403250_il2cpp_TypeInfo_var->static_fields)->set_defDistTree_2(L_23);
			goto IL_0093;
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__exception_local = (Exception_t3991598821 *)e.ex;
		if(il2cpp_codegen_class_is_assignable_from (Exception_t3991598821_il2cpp_TypeInfo_var, e.ex->object.klass))
			goto CATCH_0087;
		throw e;
	}

CATCH_0087:
	{ // begin catch(System.Exception)
		SharpZipBaseException_t2520014981 * L_24 = (SharpZipBaseException_t2520014981 *)il2cpp_codegen_object_new(SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m2797155132(L_24, _stringLiteral2425386098, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_24);
	} // end catch (depth: 1)

IL_0093:
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::.ctor(System.Byte[])
extern "C"  void InflaterHuffmanTree__ctor_m1285149863 (InflaterHuffmanTree_t1141403250 * __this, ByteU5BU5D_t4260760469* ___codeLengths0, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		ByteU5BU5D_t4260760469* L_0 = ___codeLengths0;
		InflaterHuffmanTree_BuildTree_m994169629(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::BuildTree(System.Byte[])
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern Il2CppClass* Int16U5BU5D_t801762735_il2cpp_TypeInfo_var;
extern Il2CppClass* DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var;
extern const uint32_t InflaterHuffmanTree_BuildTree_m994169629_MetadataUsageId;
extern "C"  void InflaterHuffmanTree_BuildTree_m994169629 (InflaterHuffmanTree_t1141403250 * __this, ByteU5BU5D_t4260760469* ___codeLengths0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InflaterHuffmanTree_BuildTree_m994169629_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t3230847821* V_0 = NULL;
	Int32U5BU5D_t3230847821* V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	{
		V_0 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		V_1 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16)));
		V_2 = 0;
		goto IL_0034;
	}

IL_0014:
	{
		ByteU5BU5D_t4260760469* L_0 = ___codeLengths0;
		int32_t L_1 = V_2;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		uint8_t L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_3 = L_3;
		int32_t L_4 = V_3;
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0030;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_5 = V_0;
		int32_t L_6 = V_3;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_6);
		int32_t* L_7 = ((L_5)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_6)));
		(*(int32_t*)L_7) = ((int32_t)((int32_t)(*(int32_t*)L_7)+(int32_t)1));
	}

IL_0030:
	{
		int32_t L_8 = V_2;
		V_2 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0034:
	{
		int32_t L_9 = V_2;
		ByteU5BU5D_t4260760469* L_10 = ___codeLengths0;
		NullCheck(L_10);
		if ((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_10)->max_length)))))))
		{
			goto IL_0014;
		}
	}
	{
		V_4 = 0;
		V_5 = ((int32_t)512);
		V_6 = 1;
		goto IL_0096;
	}

IL_0049:
	{
		Int32U5BU5D_t3230847821* L_11 = V_1;
		int32_t L_12 = V_6;
		int32_t L_13 = V_4;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_12);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(L_12), (int32_t)L_13);
		int32_t L_14 = V_4;
		Int32U5BU5D_t3230847821* L_15 = V_0;
		int32_t L_16 = V_6;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		int32_t L_17 = L_16;
		int32_t L_18 = (L_15)->GetAt(static_cast<il2cpp_array_size_t>(L_17));
		int32_t L_19 = V_6;
		V_4 = ((int32_t)((int32_t)L_14+(int32_t)((int32_t)((int32_t)L_18<<(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)16)-(int32_t)L_19))&(int32_t)((int32_t)31)))))));
		int32_t L_20 = V_6;
		if ((((int32_t)L_20) < ((int32_t)((int32_t)10))))
		{
			goto IL_0090;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_21 = V_1;
		int32_t L_22 = V_6;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_22);
		int32_t L_23 = L_22;
		int32_t L_24 = (L_21)->GetAt(static_cast<il2cpp_array_size_t>(L_23));
		V_7 = ((int32_t)((int32_t)L_24&(int32_t)((int32_t)130944)));
		int32_t L_25 = V_4;
		V_8 = ((int32_t)((int32_t)L_25&(int32_t)((int32_t)130944)));
		int32_t L_26 = V_5;
		int32_t L_27 = V_8;
		int32_t L_28 = V_7;
		int32_t L_29 = V_6;
		V_5 = ((int32_t)((int32_t)L_26+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_27-(int32_t)L_28))>>(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)16)-(int32_t)L_29))&(int32_t)((int32_t)31)))))));
	}

IL_0090:
	{
		int32_t L_30 = V_6;
		V_6 = ((int32_t)((int32_t)L_30+(int32_t)1));
	}

IL_0096:
	{
		int32_t L_31 = V_6;
		if ((((int32_t)L_31) <= ((int32_t)((int32_t)15))))
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_32 = V_5;
		__this->set_tree_0(((Int16U5BU5D_t801762735*)SZArrayNew(Int16U5BU5D_t801762735_il2cpp_TypeInfo_var, (uint32_t)L_32)));
		V_9 = ((int32_t)512);
		V_10 = ((int32_t)15);
		goto IL_011e;
	}

IL_00b6:
	{
		int32_t L_33 = V_4;
		V_11 = ((int32_t)((int32_t)L_33&(int32_t)((int32_t)130944)));
		int32_t L_34 = V_4;
		Int32U5BU5D_t3230847821* L_35 = V_0;
		int32_t L_36 = V_10;
		NullCheck(L_35);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_35, L_36);
		int32_t L_37 = L_36;
		int32_t L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		int32_t L_39 = V_10;
		V_4 = ((int32_t)((int32_t)L_34-(int32_t)((int32_t)((int32_t)L_38<<(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)16)-(int32_t)L_39))&(int32_t)((int32_t)31)))))));
		int32_t L_40 = V_4;
		V_12 = ((int32_t)((int32_t)L_40&(int32_t)((int32_t)130944)));
		int32_t L_41 = V_12;
		V_13 = L_41;
		goto IL_0112;
	}

IL_00e2:
	{
		Int16U5BU5D_t801762735* L_42 = __this->get_tree_0();
		int32_t L_43 = V_13;
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var);
		int16_t L_44 = DeflaterHuffman_BitReverse_m2340978984(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		int32_t L_45 = V_9;
		int32_t L_46 = V_10;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, L_44);
		(L_42)->SetAt(static_cast<il2cpp_array_size_t>(L_44), (int16_t)(((int16_t)((int16_t)((int32_t)((int32_t)((int32_t)((int32_t)((-L_45))<<(int32_t)4))|(int32_t)L_46))))));
		int32_t L_47 = V_9;
		int32_t L_48 = V_10;
		V_9 = ((int32_t)((int32_t)L_47+(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_48-(int32_t)((int32_t)9)))&(int32_t)((int32_t)31)))))));
		int32_t L_49 = V_13;
		V_13 = ((int32_t)((int32_t)L_49+(int32_t)((int32_t)128)));
	}

IL_0112:
	{
		int32_t L_50 = V_13;
		int32_t L_51 = V_11;
		if ((((int32_t)L_50) < ((int32_t)L_51)))
		{
			goto IL_00e2;
		}
	}
	{
		int32_t L_52 = V_10;
		V_10 = ((int32_t)((int32_t)L_52-(int32_t)1));
	}

IL_011e:
	{
		int32_t L_53 = V_10;
		if ((((int32_t)L_53) >= ((int32_t)((int32_t)10))))
		{
			goto IL_00b6;
		}
	}
	{
		V_14 = 0;
		goto IL_01da;
	}

IL_012c:
	{
		ByteU5BU5D_t4260760469* L_54 = ___codeLengths0;
		int32_t L_55 = V_14;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, L_55);
		int32_t L_56 = L_55;
		uint8_t L_57 = (L_54)->GetAt(static_cast<il2cpp_array_size_t>(L_56));
		V_15 = L_57;
		int32_t L_58 = V_15;
		if (!L_58)
		{
			goto IL_01d4;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_59 = V_1;
		int32_t L_60 = V_15;
		NullCheck(L_59);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_59, L_60);
		int32_t L_61 = L_60;
		int32_t L_62 = (L_59)->GetAt(static_cast<il2cpp_array_size_t>(L_61));
		V_4 = L_62;
		int32_t L_63 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(DeflaterHuffman_t3769756376_il2cpp_TypeInfo_var);
		int16_t L_64 = DeflaterHuffman_BitReverse_m2340978984(NULL /*static, unused*/, L_63, /*hidden argument*/NULL);
		V_16 = L_64;
		int32_t L_65 = V_15;
		if ((((int32_t)L_65) > ((int32_t)((int32_t)9))))
		{
			goto IL_0176;
		}
	}

IL_014e:
	{
		Int16U5BU5D_t801762735* L_66 = __this->get_tree_0();
		int32_t L_67 = V_16;
		int32_t L_68 = V_14;
		int32_t L_69 = V_15;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, L_67);
		(L_66)->SetAt(static_cast<il2cpp_array_size_t>(L_67), (int16_t)(((int16_t)((int16_t)((int32_t)((int32_t)((int32_t)((int32_t)L_68<<(int32_t)4))|(int32_t)L_69))))));
		int32_t L_70 = V_16;
		int32_t L_71 = V_15;
		V_16 = ((int32_t)((int32_t)L_70+(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_71&(int32_t)((int32_t)31)))))));
		int32_t L_72 = V_16;
		if ((((int32_t)L_72) < ((int32_t)((int32_t)512))))
		{
			goto IL_014e;
		}
	}
	{
		goto IL_01c3;
	}

IL_0176:
	{
		Int16U5BU5D_t801762735* L_73 = __this->get_tree_0();
		int32_t L_74 = V_16;
		NullCheck(L_73);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_73, ((int32_t)((int32_t)L_74&(int32_t)((int32_t)511))));
		int32_t L_75 = ((int32_t)((int32_t)L_74&(int32_t)((int32_t)511)));
		int16_t L_76 = (L_73)->GetAt(static_cast<il2cpp_array_size_t>(L_75));
		V_17 = L_76;
		int32_t L_77 = V_17;
		V_18 = ((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_77&(int32_t)((int32_t)15)))&(int32_t)((int32_t)31)))));
		int32_t L_78 = V_17;
		V_17 = ((-((int32_t)((int32_t)L_78>>(int32_t)4))));
	}

IL_019a:
	{
		Int16U5BU5D_t801762735* L_79 = __this->get_tree_0();
		int32_t L_80 = V_17;
		int32_t L_81 = V_16;
		int32_t L_82 = V_14;
		int32_t L_83 = V_15;
		NullCheck(L_79);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_79, ((int32_t)((int32_t)L_80|(int32_t)((int32_t)((int32_t)L_81>>(int32_t)((int32_t)9))))));
		(L_79)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_80|(int32_t)((int32_t)((int32_t)L_81>>(int32_t)((int32_t)9)))))), (int16_t)(((int16_t)((int16_t)((int32_t)((int32_t)((int32_t)((int32_t)L_82<<(int32_t)4))|(int32_t)L_83))))));
		int32_t L_84 = V_16;
		int32_t L_85 = V_15;
		V_16 = ((int32_t)((int32_t)L_84+(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_85&(int32_t)((int32_t)31)))))));
		int32_t L_86 = V_16;
		int32_t L_87 = V_18;
		if ((((int32_t)L_86) < ((int32_t)L_87)))
		{
			goto IL_019a;
		}
	}

IL_01c3:
	{
		Int32U5BU5D_t3230847821* L_88 = V_1;
		int32_t L_89 = V_15;
		int32_t L_90 = V_4;
		int32_t L_91 = V_15;
		NullCheck(L_88);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_88, L_89);
		(L_88)->SetAt(static_cast<il2cpp_array_size_t>(L_89), (int32_t)((int32_t)((int32_t)L_90+(int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)16)-(int32_t)L_91))&(int32_t)((int32_t)31))))))));
	}

IL_01d4:
	{
		int32_t L_92 = V_14;
		V_14 = ((int32_t)((int32_t)L_92+(int32_t)1));
	}

IL_01da:
	{
		int32_t L_93 = V_14;
		ByteU5BU5D_t4260760469* L_94 = ___codeLengths0;
		NullCheck(L_94);
		if ((((int32_t)L_93) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_94)->max_length)))))))
		{
			goto IL_012c;
		}
	}
	{
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.InflaterHuffmanTree::GetSymbol(ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator)
extern "C"  int32_t InflaterHuffmanTree_GetSymbol_m2276886475 (InflaterHuffmanTree_t1141403250 * __this, StreamManipulator_t2348681196 * ___input0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	{
		StreamManipulator_t2348681196 * L_0 = ___input0;
		NullCheck(L_0);
		int32_t L_1 = StreamManipulator_PeekBits_m2187722491(L_0, ((int32_t)9), /*hidden argument*/NULL);
		int32_t L_2 = L_1;
		V_0 = L_2;
		if ((((int32_t)L_2) < ((int32_t)0)))
		{
			goto IL_0094;
		}
	}
	{
		Int16U5BU5D_t801762735* L_3 = __this->get_tree_0();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		int16_t L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		int16_t L_7 = L_6;
		V_1 = L_7;
		if ((((int32_t)L_7) < ((int32_t)0)))
		{
			goto IL_002b;
		}
	}
	{
		StreamManipulator_t2348681196 * L_8 = ___input0;
		int32_t L_9 = V_1;
		NullCheck(L_8);
		StreamManipulator_DropBits_m3482227099(L_8, ((int32_t)((int32_t)L_9&(int32_t)((int32_t)15))), /*hidden argument*/NULL);
		int32_t L_10 = V_1;
		return ((int32_t)((int32_t)L_10>>(int32_t)4));
	}

IL_002b:
	{
		int32_t L_11 = V_1;
		V_2 = ((-((int32_t)((int32_t)L_11>>(int32_t)4))));
		int32_t L_12 = V_1;
		V_3 = ((int32_t)((int32_t)L_12&(int32_t)((int32_t)15)));
		StreamManipulator_t2348681196 * L_13 = ___input0;
		int32_t L_14 = V_3;
		NullCheck(L_13);
		int32_t L_15 = StreamManipulator_PeekBits_m2187722491(L_13, L_14, /*hidden argument*/NULL);
		int32_t L_16 = L_15;
		V_0 = L_16;
		if ((((int32_t)L_16) < ((int32_t)0)))
		{
			goto IL_005d;
		}
	}
	{
		Int16U5BU5D_t801762735* L_17 = __this->get_tree_0();
		int32_t L_18 = V_2;
		int32_t L_19 = V_0;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, ((int32_t)((int32_t)L_18|(int32_t)((int32_t)((int32_t)L_19>>(int32_t)((int32_t)9))))));
		int32_t L_20 = ((int32_t)((int32_t)L_18|(int32_t)((int32_t)((int32_t)L_19>>(int32_t)((int32_t)9)))));
		int16_t L_21 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_20));
		V_1 = L_21;
		StreamManipulator_t2348681196 * L_22 = ___input0;
		int32_t L_23 = V_1;
		NullCheck(L_22);
		StreamManipulator_DropBits_m3482227099(L_22, ((int32_t)((int32_t)L_23&(int32_t)((int32_t)15))), /*hidden argument*/NULL);
		int32_t L_24 = V_1;
		return ((int32_t)((int32_t)L_24>>(int32_t)4));
	}

IL_005d:
	{
		StreamManipulator_t2348681196 * L_25 = ___input0;
		NullCheck(L_25);
		int32_t L_26 = StreamManipulator_get_AvailableBits_m521232351(L_25, /*hidden argument*/NULL);
		V_4 = L_26;
		StreamManipulator_t2348681196 * L_27 = ___input0;
		int32_t L_28 = V_4;
		NullCheck(L_27);
		int32_t L_29 = StreamManipulator_PeekBits_m2187722491(L_27, L_28, /*hidden argument*/NULL);
		V_0 = L_29;
		Int16U5BU5D_t801762735* L_30 = __this->get_tree_0();
		int32_t L_31 = V_2;
		int32_t L_32 = V_0;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, ((int32_t)((int32_t)L_31|(int32_t)((int32_t)((int32_t)L_32>>(int32_t)((int32_t)9))))));
		int32_t L_33 = ((int32_t)((int32_t)L_31|(int32_t)((int32_t)((int32_t)L_32>>(int32_t)((int32_t)9)))));
		int16_t L_34 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		V_1 = L_34;
		int32_t L_35 = V_1;
		int32_t L_36 = V_4;
		if ((((int32_t)((int32_t)((int32_t)L_35&(int32_t)((int32_t)15)))) > ((int32_t)L_36)))
		{
			goto IL_0092;
		}
	}
	{
		StreamManipulator_t2348681196 * L_37 = ___input0;
		int32_t L_38 = V_1;
		NullCheck(L_37);
		StreamManipulator_DropBits_m3482227099(L_37, ((int32_t)((int32_t)L_38&(int32_t)((int32_t)15))), /*hidden argument*/NULL);
		int32_t L_39 = V_1;
		return ((int32_t)((int32_t)L_39>>(int32_t)4));
	}

IL_0092:
	{
		return (-1);
	}

IL_0094:
	{
		StreamManipulator_t2348681196 * L_40 = ___input0;
		NullCheck(L_40);
		int32_t L_41 = StreamManipulator_get_AvailableBits_m521232351(L_40, /*hidden argument*/NULL);
		V_5 = L_41;
		StreamManipulator_t2348681196 * L_42 = ___input0;
		int32_t L_43 = V_5;
		NullCheck(L_42);
		int32_t L_44 = StreamManipulator_PeekBits_m2187722491(L_42, L_43, /*hidden argument*/NULL);
		V_0 = L_44;
		Int16U5BU5D_t801762735* L_45 = __this->get_tree_0();
		int32_t L_46 = V_0;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, L_46);
		int32_t L_47 = L_46;
		int16_t L_48 = (L_45)->GetAt(static_cast<il2cpp_array_size_t>(L_47));
		V_1 = L_48;
		int32_t L_49 = V_1;
		if ((((int32_t)L_49) < ((int32_t)0)))
		{
			goto IL_00c8;
		}
	}
	{
		int32_t L_50 = V_1;
		int32_t L_51 = V_5;
		if ((((int32_t)((int32_t)((int32_t)L_50&(int32_t)((int32_t)15)))) > ((int32_t)L_51)))
		{
			goto IL_00c8;
		}
	}
	{
		StreamManipulator_t2348681196 * L_52 = ___input0;
		int32_t L_53 = V_1;
		NullCheck(L_52);
		StreamManipulator_DropBits_m3482227099(L_52, ((int32_t)((int32_t)L_53&(int32_t)((int32_t)15))), /*hidden argument*/NULL);
		int32_t L_54 = V_1;
		return ((int32_t)((int32_t)L_54>>(int32_t)4));
	}

IL_00c8:
	{
		return (-1);
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::.ctor(System.Int32)
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern const uint32_t PendingBuffer__ctor_m3734046780_MetadataUsageId;
extern "C"  void PendingBuffer__ctor_m3734046780 (PendingBuffer_t3572745737 * __this, int32_t ___bufferSize0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PendingBuffer__ctor_m3734046780_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___bufferSize0;
		__this->set_buffer__0(((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)L_0)));
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::Reset()
extern "C"  void PendingBuffer_Reset_m2437383704 (PendingBuffer_t3572745737 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = 0;
		V_0 = L_0;
		__this->set_bitCount_4(L_0);
		int32_t L_1 = V_0;
		int32_t L_2 = L_1;
		V_1 = L_2;
		__this->set_end_2(L_2);
		int32_t L_3 = V_1;
		__this->set_start_1(L_3);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::WriteShort(System.Int32)
extern "C"  void PendingBuffer_WriteShort_m1294929767 (PendingBuffer_t3572745737 * __this, int32_t ___value0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		ByteU5BU5D_t4260760469* L_0 = __this->get_buffer__0();
		int32_t L_1 = __this->get_end_2();
		int32_t L_2 = L_1;
		V_0 = L_2;
		__this->set_end_2(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_0;
		int32_t L_4 = ___value0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (uint8_t)(((int32_t)((uint8_t)L_4))));
		ByteU5BU5D_t4260760469* L_5 = __this->get_buffer__0();
		int32_t L_6 = __this->get_end_2();
		int32_t L_7 = L_6;
		V_1 = L_7;
		__this->set_end_2(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_1;
		int32_t L_9 = ___value0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_9>>(int32_t)8))))));
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::WriteBlock(System.Byte[],System.Int32,System.Int32)
extern "C"  void PendingBuffer_WriteBlock_m2658962210 (PendingBuffer_t3572745737 * __this, ByteU5BU5D_t4260760469* ___block0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method)
{
	{
		ByteU5BU5D_t4260760469* L_0 = ___block0;
		int32_t L_1 = ___offset1;
		ByteU5BU5D_t4260760469* L_2 = __this->get_buffer__0();
		int32_t L_3 = __this->get_end_2();
		int32_t L_4 = ___length2;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_0, L_1, (Il2CppArray *)(Il2CppArray *)L_2, L_3, L_4, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_end_2();
		int32_t L_6 = ___length2;
		__this->set_end_2(((int32_t)((int32_t)L_5+(int32_t)L_6)));
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::get_BitCount()
extern "C"  int32_t PendingBuffer_get_BitCount_m2974417016 (PendingBuffer_t3572745737 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_bitCount_4();
		return L_0;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::AlignToByte()
extern "C"  void PendingBuffer_AlignToByte_m2130252305 (PendingBuffer_t3572745737 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_bitCount_4();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0052;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_1 = __this->get_buffer__0();
		int32_t L_2 = __this->get_end_2();
		int32_t L_3 = L_2;
		V_0 = L_3;
		__this->set_end_2(((int32_t)((int32_t)L_3+(int32_t)1)));
		int32_t L_4 = V_0;
		uint32_t L_5 = __this->get_bits_3();
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (uint8_t)(((int32_t)((uint8_t)L_5))));
		int32_t L_6 = __this->get_bitCount_4();
		if ((((int32_t)L_6) <= ((int32_t)8)))
		{
			goto IL_0052;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_7 = __this->get_buffer__0();
		int32_t L_8 = __this->get_end_2();
		int32_t L_9 = L_8;
		V_1 = L_9;
		__this->set_end_2(((int32_t)((int32_t)L_9+(int32_t)1)));
		int32_t L_10 = V_1;
		uint32_t L_11 = __this->get_bits_3();
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_11>>8))))));
	}

IL_0052:
	{
		__this->set_bits_3(0);
		__this->set_bitCount_4(0);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::WriteBits(System.Int32,System.Int32)
extern "C"  void PendingBuffer_WriteBits_m4156628504 (PendingBuffer_t3572745737 * __this, int32_t ___b0, int32_t ___count1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		uint32_t L_0 = __this->get_bits_3();
		int32_t L_1 = ___b0;
		int32_t L_2 = __this->get_bitCount_4();
		__this->set_bits_3(((int32_t)((int32_t)L_0|(int32_t)((int32_t)((int32_t)L_1<<(int32_t)((int32_t)((int32_t)L_2&(int32_t)((int32_t)31))))))));
		int32_t L_3 = __this->get_bitCount_4();
		int32_t L_4 = ___count1;
		__this->set_bitCount_4(((int32_t)((int32_t)L_3+(int32_t)L_4)));
		int32_t L_5 = __this->get_bitCount_4();
		if ((((int32_t)L_5) < ((int32_t)((int32_t)16))))
		{
			goto IL_008e;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_6 = __this->get_buffer__0();
		int32_t L_7 = __this->get_end_2();
		int32_t L_8 = L_7;
		V_0 = L_8;
		__this->set_end_2(((int32_t)((int32_t)L_8+(int32_t)1)));
		int32_t L_9 = V_0;
		uint32_t L_10 = __this->get_bits_3();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (uint8_t)(((int32_t)((uint8_t)L_10))));
		ByteU5BU5D_t4260760469* L_11 = __this->get_buffer__0();
		int32_t L_12 = __this->get_end_2();
		int32_t L_13 = L_12;
		V_1 = L_13;
		__this->set_end_2(((int32_t)((int32_t)L_13+(int32_t)1)));
		int32_t L_14 = V_1;
		uint32_t L_15 = __this->get_bits_3();
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(L_14), (uint8_t)(((int32_t)((uint8_t)((int32_t)((uint32_t)L_15>>8))))));
		uint32_t L_16 = __this->get_bits_3();
		__this->set_bits_3(((int32_t)((uint32_t)L_16>>((int32_t)16))));
		int32_t L_17 = __this->get_bitCount_4();
		__this->set_bitCount_4(((int32_t)((int32_t)L_17-(int32_t)((int32_t)16))));
	}

IL_008e:
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::WriteShortMSB(System.Int32)
extern "C"  void PendingBuffer_WriteShortMSB_m3421538425 (PendingBuffer_t3572745737 * __this, int32_t ___s0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		ByteU5BU5D_t4260760469* L_0 = __this->get_buffer__0();
		int32_t L_1 = __this->get_end_2();
		int32_t L_2 = L_1;
		V_0 = L_2;
		__this->set_end_2(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_0;
		int32_t L_4 = ___s0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_4>>(int32_t)8))))));
		ByteU5BU5D_t4260760469* L_5 = __this->get_buffer__0();
		int32_t L_6 = __this->get_end_2();
		int32_t L_7 = L_6;
		V_1 = L_7;
		__this->set_end_2(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_1;
		int32_t L_9 = ___s0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_8), (uint8_t)(((int32_t)((uint8_t)L_9))));
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::get_IsFlushed()
extern "C"  bool PendingBuffer_get_IsFlushed_m2304700395 (PendingBuffer_t3572745737 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_end_2();
		return (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.PendingBuffer::Flush(System.Byte[],System.Int32,System.Int32)
extern "C"  int32_t PendingBuffer_Flush_m844807344 (PendingBuffer_t3572745737 * __this, ByteU5BU5D_t4260760469* ___output0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_bitCount_4();
		if ((((int32_t)L_0) < ((int32_t)8)))
		{
			goto IL_0044;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_1 = __this->get_buffer__0();
		int32_t L_2 = __this->get_end_2();
		int32_t L_3 = L_2;
		V_0 = L_3;
		__this->set_end_2(((int32_t)((int32_t)L_3+(int32_t)1)));
		int32_t L_4 = V_0;
		uint32_t L_5 = __this->get_bits_3();
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (uint8_t)(((int32_t)((uint8_t)L_5))));
		uint32_t L_6 = __this->get_bits_3();
		__this->set_bits_3(((int32_t)((uint32_t)L_6>>8)));
		int32_t L_7 = __this->get_bitCount_4();
		__this->set_bitCount_4(((int32_t)((int32_t)L_7-(int32_t)8)));
	}

IL_0044:
	{
		int32_t L_8 = ___length2;
		int32_t L_9 = __this->get_end_2();
		int32_t L_10 = __this->get_start_1();
		if ((((int32_t)L_8) <= ((int32_t)((int32_t)((int32_t)L_9-(int32_t)L_10)))))
		{
			goto IL_0087;
		}
	}
	{
		int32_t L_11 = __this->get_end_2();
		int32_t L_12 = __this->get_start_1();
		___length2 = ((int32_t)((int32_t)L_11-(int32_t)L_12));
		ByteU5BU5D_t4260760469* L_13 = __this->get_buffer__0();
		int32_t L_14 = __this->get_start_1();
		ByteU5BU5D_t4260760469* L_15 = ___output0;
		int32_t L_16 = ___offset1;
		int32_t L_17 = ___length2;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_13, L_14, (Il2CppArray *)(Il2CppArray *)L_15, L_16, L_17, /*hidden argument*/NULL);
		__this->set_start_1(0);
		__this->set_end_2(0);
		goto IL_00a9;
	}

IL_0087:
	{
		ByteU5BU5D_t4260760469* L_18 = __this->get_buffer__0();
		int32_t L_19 = __this->get_start_1();
		ByteU5BU5D_t4260760469* L_20 = ___output0;
		int32_t L_21 = ___offset1;
		int32_t L_22 = ___length2;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_18, L_19, (Il2CppArray *)(Il2CppArray *)L_20, L_21, L_22, /*hidden argument*/NULL);
		int32_t L_23 = __this->get_start_1();
		int32_t L_24 = ___length2;
		__this->set_start_1(((int32_t)((int32_t)L_23+(int32_t)L_24)));
	}

IL_00a9:
	{
		int32_t L_25 = ___length2;
		return L_25;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::.ctor(System.IO.Stream,ICSharpCode.SharpZipLib.Zip.Compression.Deflater,System.Int32)
extern Il2CppClass* Stream_t1561764144_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4166220114;
extern Il2CppCodeGenString* _stringLiteral3906200980;
extern Il2CppCodeGenString* _stringLiteral653851047;
extern Il2CppCodeGenString* _stringLiteral1906231393;
extern const uint32_t DeflaterOutputStream__ctor_m3308520758_MetadataUsageId;
extern "C"  void DeflaterOutputStream__ctor_m3308520758 (DeflaterOutputStream_t537202536 * __this, Stream_t1561764144 * ___baseOutputStream0, Deflater_t2454063301 * ___deflater1, int32_t ___bufferSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterOutputStream__ctor_m3308520758_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_isStreamOwner__7((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Stream_t1561764144_il2cpp_TypeInfo_var);
		Stream__ctor_m1433294025(__this, /*hidden argument*/NULL);
		Stream_t1561764144 * L_0 = ___baseOutputStream0;
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral4166220114, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001b:
	{
		Stream_t1561764144 * L_2 = ___baseOutputStream0;
		NullCheck(L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Stream_t1561764144_il2cpp_TypeInfo_var);
		bool L_3 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.IO.Stream::get_CanWrite() */, L_2);
		if (L_3)
		{
			goto IL_0033;
		}
	}
	{
		ArgumentException_t928607144 * L_4 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_4, _stringLiteral3906200980, _stringLiteral4166220114, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0033:
	{
		Deflater_t2454063301 * L_5 = ___deflater1;
		if (L_5)
		{
			goto IL_0041;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_6 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_6, _stringLiteral653851047, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_6);
	}

IL_0041:
	{
		int32_t L_7 = ___bufferSize2;
		if ((((int32_t)L_7) >= ((int32_t)((int32_t)512))))
		{
			goto IL_0054;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_8 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_8, _stringLiteral1906231393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0054:
	{
		Stream_t1561764144 * L_9 = ___baseOutputStream0;
		__this->set_baseOutputStream__5(L_9);
		int32_t L_10 = ___bufferSize2;
		__this->set_buffer__3(((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)L_10)));
		Deflater_t2454063301 * L_11 = ___deflater1;
		__this->set_deflater__4(L_11);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Finish()
extern Il2CppClass* SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipAESTransform_t496812608_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1833313356;
extern const uint32_t DeflaterOutputStream_Finish_m2029750678_MetadataUsageId;
extern "C"  void DeflaterOutputStream_Finish_m2029750678 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterOutputStream_Finish_m2029750678_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		Deflater_t2454063301 * L_0 = __this->get_deflater__4();
		NullCheck(L_0);
		Deflater_Finish_m1400398426(L_0, /*hidden argument*/NULL);
		goto IL_0055;
	}

IL_000d:
	{
		Deflater_t2454063301 * L_1 = __this->get_deflater__4();
		ByteU5BU5D_t4260760469* L_2 = __this->get_buffer__3();
		ByteU5BU5D_t4260760469* L_3 = __this->get_buffer__3();
		NullCheck(L_3);
		NullCheck(L_1);
		int32_t L_4 = Deflater_Deflate_m1433311247(L_1, L_2, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_3)->max_length)))), /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) <= ((int32_t)0)))
		{
			goto IL_0062;
		}
	}
	{
		Il2CppObject * L_6 = __this->get_cryptoTransform__1();
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_7 = __this->get_buffer__3();
		int32_t L_8 = V_0;
		DeflaterOutputStream_EncryptBlock_m2314870302(__this, L_7, 0, L_8, /*hidden argument*/NULL);
	}

IL_0042:
	{
		Stream_t1561764144 * L_9 = __this->get_baseOutputStream__5();
		ByteU5BU5D_t4260760469* L_10 = __this->get_buffer__3();
		int32_t L_11 = V_0;
		NullCheck(L_9);
		VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(21 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_9, L_10, 0, L_11);
	}

IL_0055:
	{
		Deflater_t2454063301 * L_12 = __this->get_deflater__4();
		NullCheck(L_12);
		bool L_13 = Deflater_get_IsFinished_m284667774(L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_000d;
		}
	}

IL_0062:
	{
		Deflater_t2454063301 * L_14 = __this->get_deflater__4();
		NullCheck(L_14);
		bool L_15 = Deflater_get_IsFinished_m284667774(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_007a;
		}
	}
	{
		SharpZipBaseException_t2520014981 * L_16 = (SharpZipBaseException_t2520014981 *)il2cpp_codegen_object_new(SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m2797155132(L_16, _stringLiteral1833313356, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_16);
	}

IL_007a:
	{
		Stream_t1561764144 * L_17 = __this->get_baseOutputStream__5();
		NullCheck(L_17);
		VirtActionInvoker0::Invoke(16 /* System.Void System.IO.Stream::Flush() */, L_17);
		Il2CppObject * L_18 = __this->get_cryptoTransform__1();
		if (!L_18)
		{
			goto IL_00c2;
		}
	}
	{
		Il2CppObject * L_19 = __this->get_cryptoTransform__1();
		if (!((ZipAESTransform_t496812608 *)IsInstClass(L_19, ZipAESTransform_t496812608_il2cpp_TypeInfo_var)))
		{
			goto IL_00b0;
		}
	}
	{
		Il2CppObject * L_20 = __this->get_cryptoTransform__1();
		NullCheck(((ZipAESTransform_t496812608 *)CastclassClass(L_20, ZipAESTransform_t496812608_il2cpp_TypeInfo_var)));
		ByteU5BU5D_t4260760469* L_21 = ZipAESTransform_GetAuthCode_m1195602789(((ZipAESTransform_t496812608 *)CastclassClass(L_20, ZipAESTransform_t496812608_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		__this->set_AESAuthCode_2(L_21);
	}

IL_00b0:
	{
		Il2CppObject * L_22 = __this->get_cryptoTransform__1();
		NullCheck(L_22);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_22);
		__this->set_cryptoTransform__1((Il2CppObject *)NULL);
	}

IL_00c2:
	{
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_IsStreamOwner()
extern "C"  bool DeflaterOutputStream_get_IsStreamOwner_m3089361197 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isStreamOwner__7();
		return L_0;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::EncryptBlock(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ICryptoTransform_t153068654_il2cpp_TypeInfo_var;
extern const uint32_t DeflaterOutputStream_EncryptBlock_m2314870302_MetadataUsageId;
extern "C"  void DeflaterOutputStream_EncryptBlock_m2314870302 (DeflaterOutputStream_t537202536 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterOutputStream_EncryptBlock_m2314870302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_cryptoTransform__1();
		ByteU5BU5D_t4260760469* L_1 = ___buffer0;
		int32_t L_2 = ___length2;
		ByteU5BU5D_t4260760469* L_3 = ___buffer0;
		NullCheck(L_0);
		InterfaceFuncInvoker5< int32_t, ByteU5BU5D_t4260760469*, int32_t, int32_t, ByteU5BU5D_t4260760469*, int32_t >::Invoke(1 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t153068654_il2cpp_TypeInfo_var, L_0, L_1, 0, L_2, L_3, 0);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Deflate()
extern Il2CppClass* SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral217043460;
extern const uint32_t DeflaterOutputStream_Deflate_m995981802_MetadataUsageId;
extern "C"  void DeflaterOutputStream_Deflate_m995981802 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterOutputStream_Deflate_m995981802_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		goto IL_004a;
	}

IL_0002:
	{
		Deflater_t2454063301 * L_0 = __this->get_deflater__4();
		ByteU5BU5D_t4260760469* L_1 = __this->get_buffer__3();
		ByteU5BU5D_t4260760469* L_2 = __this->get_buffer__3();
		NullCheck(L_2);
		NullCheck(L_0);
		int32_t L_3 = Deflater_Deflate_m1433311247(L_0, L_1, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))), /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) <= ((int32_t)0)))
		{
			goto IL_0057;
		}
	}
	{
		Il2CppObject * L_5 = __this->get_cryptoTransform__1();
		if (!L_5)
		{
			goto IL_0037;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_6 = __this->get_buffer__3();
		int32_t L_7 = V_0;
		DeflaterOutputStream_EncryptBlock_m2314870302(__this, L_6, 0, L_7, /*hidden argument*/NULL);
	}

IL_0037:
	{
		Stream_t1561764144 * L_8 = __this->get_baseOutputStream__5();
		ByteU5BU5D_t4260760469* L_9 = __this->get_buffer__3();
		int32_t L_10 = V_0;
		NullCheck(L_8);
		VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(21 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, L_8, L_9, 0, L_10);
	}

IL_004a:
	{
		Deflater_t2454063301 * L_11 = __this->get_deflater__4();
		NullCheck(L_11);
		bool L_12 = Deflater_get_IsNeedingInput_m862754346(L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0002;
		}
	}

IL_0057:
	{
		Deflater_t2454063301 * L_13 = __this->get_deflater__4();
		NullCheck(L_13);
		bool L_14 = Deflater_get_IsNeedingInput_m862754346(L_13, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_006f;
		}
	}
	{
		SharpZipBaseException_t2520014981 * L_15 = (SharpZipBaseException_t2520014981 *)il2cpp_codegen_object_new(SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m2797155132(L_15, _stringLiteral217043460, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_006f:
	{
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanRead()
extern "C"  bool DeflaterOutputStream_get_CanRead_m2593188394 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanSeek()
extern "C"  bool DeflaterOutputStream_get_CanSeek_m2621943436 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanWrite()
extern "C"  bool DeflaterOutputStream_get_CanWrite_m3602082413 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method)
{
	{
		Stream_t1561764144 * L_0 = __this->get_baseOutputStream__5();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean System.IO.Stream::get_CanWrite() */, L_0);
		return L_1;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_Length()
extern "C"  int64_t DeflaterOutputStream_get_Length_m3173446335 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method)
{
	{
		Stream_t1561764144 * L_0 = __this->get_baseOutputStream__5();
		NullCheck(L_0);
		int64_t L_1 = VirtFuncInvoker0< int64_t >::Invoke(8 /* System.Int64 System.IO.Stream::get_Length() */, L_0);
		return L_1;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_Position()
extern "C"  int64_t DeflaterOutputStream_get_Position_m476649026 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method)
{
	{
		Stream_t1561764144 * L_0 = __this->get_baseOutputStream__5();
		NullCheck(L_0);
		int64_t L_1 = VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.Stream::get_Position() */, L_0);
		return L_1;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::set_Position(System.Int64)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3639572813;
extern const uint32_t DeflaterOutputStream_set_Position_m2790486651_MetadataUsageId;
extern "C"  void DeflaterOutputStream_set_Position_m2790486651 (DeflaterOutputStream_t537202536 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterOutputStream_set_Position_m2790486651_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, _stringLiteral3639572813, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3960510449;
extern const uint32_t DeflaterOutputStream_Seek_m1928174021_MetadataUsageId;
extern "C"  int64_t DeflaterOutputStream_Seek_m1928174021 (DeflaterOutputStream_t537202536 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterOutputStream_Seek_m1928174021_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, _stringLiteral3960510449, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::SetLength(System.Int64)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3392549489;
extern const uint32_t DeflaterOutputStream_SetLength_m1719647033_MetadataUsageId;
extern "C"  void DeflaterOutputStream_SetLength_m1719647033 (DeflaterOutputStream_t537202536 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterOutputStream_SetLength_m1719647033_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, _stringLiteral3392549489, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::ReadByte()
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2713636535;
extern const uint32_t DeflaterOutputStream_ReadByte_m1306229005_MetadataUsageId;
extern "C"  int32_t DeflaterOutputStream_ReadByte_m1306229005 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterOutputStream_ReadByte_m1306229005_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, _stringLiteral2713636535, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Read(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral741824143;
extern const uint32_t DeflaterOutputStream_Read_m1142977636_MetadataUsageId;
extern "C"  int32_t DeflaterOutputStream_Read_m1142977636 (DeflaterOutputStream_t537202536 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterOutputStream_Read_m1142977636_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, _stringLiteral741824143, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.IAsyncResult ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::BeginRead(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4124070606;
extern const uint32_t DeflaterOutputStream_BeginRead_m266793667_MetadataUsageId;
extern "C"  Il2CppObject * DeflaterOutputStream_BeginRead_m266793667 (DeflaterOutputStream_t537202536 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___state4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterOutputStream_BeginRead_m266793667_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, _stringLiteral4124070606, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.IAsyncResult ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral134734133;
extern const uint32_t DeflaterOutputStream_BeginWrite_m2153165670_MetadataUsageId;
extern "C"  Il2CppObject * DeflaterOutputStream_BeginWrite_m2153165670 (DeflaterOutputStream_t537202536 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___state4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterOutputStream_BeginWrite_m2153165670_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, _stringLiteral134734133, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Flush()
extern "C"  void DeflaterOutputStream_Flush_m158113283 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method)
{
	{
		Deflater_t2454063301 * L_0 = __this->get_deflater__4();
		NullCheck(L_0);
		Deflater_Flush_m1800379583(L_0, /*hidden argument*/NULL);
		DeflaterOutputStream_Deflate_m995981802(__this, /*hidden argument*/NULL);
		Stream_t1561764144 * L_1 = __this->get_baseOutputStream__5();
		NullCheck(L_1);
		VirtActionInvoker0::Invoke(16 /* System.Void System.IO.Stream::Flush() */, L_1);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Close()
extern Il2CppClass* IDisposable_t1423340799_il2cpp_TypeInfo_var;
extern const uint32_t DeflaterOutputStream_Close_m1785025527_MetadataUsageId;
extern "C"  void DeflaterOutputStream_Close_m1785025527 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterOutputStream_Close_m1785025527_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t3991598821 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t3991598821 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		bool L_0 = __this->get_isClosed__6();
		if (L_0)
		{
			goto IL_004b;
		}
	}
	{
		__this->set_isClosed__6((bool)1);
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		{
			VirtActionInvoker0::Invoke(27 /* System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Finish() */, __this);
			Il2CppObject * L_1 = __this->get_cryptoTransform__1();
			if (!L_1)
			{
				goto IL_0035;
			}
		}

IL_001d:
		{
			DeflaterOutputStream_GetAuthCodeIfAES_m3041480074(__this, /*hidden argument*/NULL);
			Il2CppObject * L_2 = __this->get_cryptoTransform__1();
			NullCheck(L_2);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t1423340799_il2cpp_TypeInfo_var, L_2);
			__this->set_cryptoTransform__1((Il2CppObject *)NULL);
		}

IL_0035:
		{
			IL2CPP_LEAVE(0x4B, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t3991598821 *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		{
			bool L_3 = __this->get_isStreamOwner__7();
			if (!L_3)
			{
				goto IL_004a;
			}
		}

IL_003f:
		{
			Stream_t1561764144 * L_4 = __this->get_baseOutputStream__5();
			NullCheck(L_4);
			VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_4);
		}

IL_004a:
		{
			IL2CPP_END_FINALLY(55)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_JUMP_TBL(0x4B, IL_004b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t3991598821 *)
	}

IL_004b:
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::GetAuthCodeIfAES()
extern Il2CppClass* ZipAESTransform_t496812608_il2cpp_TypeInfo_var;
extern const uint32_t DeflaterOutputStream_GetAuthCodeIfAES_m3041480074_MetadataUsageId;
extern "C"  void DeflaterOutputStream_GetAuthCodeIfAES_m3041480074 (DeflaterOutputStream_t537202536 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterOutputStream_GetAuthCodeIfAES_m3041480074_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Il2CppObject * L_0 = __this->get_cryptoTransform__1();
		if (!((ZipAESTransform_t496812608 *)IsInstClass(L_0, ZipAESTransform_t496812608_il2cpp_TypeInfo_var)))
		{
			goto IL_0023;
		}
	}
	{
		Il2CppObject * L_1 = __this->get_cryptoTransform__1();
		NullCheck(((ZipAESTransform_t496812608 *)CastclassClass(L_1, ZipAESTransform_t496812608_il2cpp_TypeInfo_var)));
		ByteU5BU5D_t4260760469* L_2 = ZipAESTransform_GetAuthCode_m1195602789(((ZipAESTransform_t496812608 *)CastclassClass(L_1, ZipAESTransform_t496812608_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		__this->set_AESAuthCode_2(L_2);
	}

IL_0023:
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::WriteByte(System.Byte)
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern const uint32_t DeflaterOutputStream_WriteByte_m1639156453_MetadataUsageId;
extern "C"  void DeflaterOutputStream_WriteByte_m1639156453 (DeflaterOutputStream_t537202536 * __this, uint8_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (DeflaterOutputStream_WriteByte_m1639156453_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ByteU5BU5D_t4260760469* V_0 = NULL;
	{
		V_0 = ((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)1));
		ByteU5BU5D_t4260760469* L_0 = V_0;
		uint8_t L_1 = ___value0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (uint8_t)L_1);
		ByteU5BU5D_t4260760469* L_2 = V_0;
		VirtActionInvoker3< ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(21 /* System.Void System.IO.Stream::Write(System.Byte[],System.Int32,System.Int32) */, __this, L_2, 0, 1);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Write(System.Byte[],System.Int32,System.Int32)
extern "C"  void DeflaterOutputStream_Write_m3972695563 (DeflaterOutputStream_t537202536 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	{
		Deflater_t2454063301 * L_0 = __this->get_deflater__4();
		ByteU5BU5D_t4260760469* L_1 = ___buffer0;
		int32_t L_2 = ___offset1;
		int32_t L_3 = ___count2;
		NullCheck(L_0);
		Deflater_SetInput_m1771564410(L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		DeflaterOutputStream_Deflate_m995981802(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::.ctor(System.IO.Stream,System.Int32)
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern const uint32_t InflaterInputBuffer__ctor_m3230220962_MetadataUsageId;
extern "C"  void InflaterInputBuffer__ctor_m3230220962 (InflaterInputBuffer_t441930877 * __this, Stream_t1561764144 * ___stream0, int32_t ___bufferSize1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputBuffer__ctor_m3230220962_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		Stream_t1561764144 * L_0 = ___stream0;
		__this->set_inputStream_6(L_0);
		int32_t L_1 = ___bufferSize1;
		if ((((int32_t)L_1) >= ((int32_t)((int32_t)1024))))
		{
			goto IL_001c;
		}
	}
	{
		___bufferSize1 = ((int32_t)1024);
	}

IL_001c:
	{
		int32_t L_2 = ___bufferSize1;
		__this->set_rawData_1(((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)L_2)));
		ByteU5BU5D_t4260760469* L_3 = __this->get_rawData_1();
		__this->set_clearText_3(L_3);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::get_RawLength()
extern "C"  int32_t InflaterInputBuffer_get_RawLength_m1307271085 (InflaterInputBuffer_t441930877 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_rawLength_0();
		return L_0;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::get_Available()
extern "C"  int32_t InflaterInputBuffer_get_Available_m3610562184 (InflaterInputBuffer_t441930877 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_available_4();
		return L_0;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::set_Available(System.Int32)
extern "C"  void InflaterInputBuffer_set_Available_m1900611929 (InflaterInputBuffer_t441930877 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_available_4(L_0);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::SetInflaterInput(ICSharpCode.SharpZipLib.Zip.Compression.Inflater)
extern "C"  void InflaterInputBuffer_SetInflaterInput_m3831681298 (InflaterInputBuffer_t441930877 * __this, Inflater_t1975778921 * ___inflater0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_available_4();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_002f;
		}
	}
	{
		Inflater_t1975778921 * L_1 = ___inflater0;
		ByteU5BU5D_t4260760469* L_2 = __this->get_clearText_3();
		int32_t L_3 = __this->get_clearTextLength_2();
		int32_t L_4 = __this->get_available_4();
		int32_t L_5 = __this->get_available_4();
		NullCheck(L_1);
		Inflater_SetInput_m2977611094(L_1, L_2, ((int32_t)((int32_t)L_3-(int32_t)L_4)), L_5, /*hidden argument*/NULL);
		__this->set_available_4(0);
	}

IL_002f:
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::Fill()
extern Il2CppClass* ICryptoTransform_t153068654_il2cpp_TypeInfo_var;
extern const uint32_t InflaterInputBuffer_Fill_m1169415305_MetadataUsageId;
extern "C"  void InflaterInputBuffer_Fill_m1169415305 (InflaterInputBuffer_t441930877 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputBuffer_Fill_m1169415305_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		__this->set_rawLength_0(0);
		ByteU5BU5D_t4260760469* L_0 = __this->get_rawData_1();
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))));
		goto IL_0041;
	}

IL_0012:
	{
		Stream_t1561764144 * L_1 = __this->get_inputStream_6();
		ByteU5BU5D_t4260760469* L_2 = __this->get_rawData_1();
		int32_t L_3 = __this->get_rawLength_0();
		int32_t L_4 = V_0;
		NullCheck(L_1);
		int32_t L_5 = VirtFuncInvoker3< int32_t, ByteU5BU5D_t4260760469*, int32_t, int32_t >::Invoke(17 /* System.Int32 System.IO.Stream::Read(System.Byte[],System.Int32,System.Int32) */, L_1, L_2, L_3, L_4);
		V_1 = L_5;
		int32_t L_6 = V_1;
		if ((((int32_t)L_6) <= ((int32_t)0)))
		{
			goto IL_0045;
		}
	}
	{
		int32_t L_7 = __this->get_rawLength_0();
		int32_t L_8 = V_1;
		__this->set_rawLength_0(((int32_t)((int32_t)L_7+(int32_t)L_8)));
		int32_t L_9 = V_0;
		int32_t L_10 = V_1;
		V_0 = ((int32_t)((int32_t)L_9-(int32_t)L_10));
	}

IL_0041:
	{
		int32_t L_11 = V_0;
		if ((((int32_t)L_11) > ((int32_t)0)))
		{
			goto IL_0012;
		}
	}

IL_0045:
	{
		Il2CppObject * L_12 = __this->get_cryptoTransform_5();
		if (!L_12)
		{
			goto IL_0074;
		}
	}
	{
		Il2CppObject * L_13 = __this->get_cryptoTransform_5();
		ByteU5BU5D_t4260760469* L_14 = __this->get_rawData_1();
		int32_t L_15 = __this->get_rawLength_0();
		ByteU5BU5D_t4260760469* L_16 = __this->get_clearText_3();
		NullCheck(L_13);
		int32_t L_17 = InterfaceFuncInvoker5< int32_t, ByteU5BU5D_t4260760469*, int32_t, int32_t, ByteU5BU5D_t4260760469*, int32_t >::Invoke(1 /* System.Int32 System.Security.Cryptography.ICryptoTransform::TransformBlock(System.Byte[],System.Int32,System.Int32,System.Byte[],System.Int32) */, ICryptoTransform_t153068654_il2cpp_TypeInfo_var, L_13, L_14, 0, L_15, L_16, 0);
		__this->set_clearTextLength_2(L_17);
		goto IL_0080;
	}

IL_0074:
	{
		int32_t L_18 = __this->get_rawLength_0();
		__this->set_clearTextLength_2(L_18);
	}

IL_0080:
	{
		int32_t L_19 = __this->get_clearTextLength_2();
		__this->set_available_4(L_19);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadClearTextBuffer(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3188603622;
extern const uint32_t InflaterInputBuffer_ReadClearTextBuffer_m1437123005_MetadataUsageId;
extern "C"  int32_t InflaterInputBuffer_ReadClearTextBuffer_m1437123005 (InflaterInputBuffer_t441930877 * __this, ByteU5BU5D_t4260760469* ___outBuffer0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputBuffer_ReadClearTextBuffer_m1437123005_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___length2;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_1 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, _stringLiteral3188603622, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000f:
	{
		int32_t L_2 = ___offset1;
		V_0 = L_2;
		int32_t L_3 = ___length2;
		V_1 = L_3;
		goto IL_006d;
	}

IL_0015:
	{
		int32_t L_4 = __this->get_available_4();
		if ((((int32_t)L_4) > ((int32_t)0)))
		{
			goto IL_002f;
		}
	}
	{
		InflaterInputBuffer_Fill_m1169415305(__this, /*hidden argument*/NULL);
		int32_t L_5 = __this->get_available_4();
		if ((((int32_t)L_5) > ((int32_t)0)))
		{
			goto IL_002f;
		}
	}
	{
		return 0;
	}

IL_002f:
	{
		int32_t L_6 = V_1;
		int32_t L_7 = __this->get_available_4();
		int32_t L_8 = Math_Min_m811624909(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		ByteU5BU5D_t4260760469* L_9 = __this->get_clearText_3();
		int32_t L_10 = __this->get_clearTextLength_2();
		int32_t L_11 = __this->get_available_4();
		ByteU5BU5D_t4260760469* L_12 = ___outBuffer0;
		int32_t L_13 = V_0;
		int32_t L_14 = V_2;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_9, ((int32_t)((int32_t)L_10-(int32_t)L_11)), (Il2CppArray *)(Il2CppArray *)L_12, L_13, L_14, /*hidden argument*/NULL);
		int32_t L_15 = V_0;
		int32_t L_16 = V_2;
		V_0 = ((int32_t)((int32_t)L_15+(int32_t)L_16));
		int32_t L_17 = V_1;
		int32_t L_18 = V_2;
		V_1 = ((int32_t)((int32_t)L_17-(int32_t)L_18));
		int32_t L_19 = __this->get_available_4();
		int32_t L_20 = V_2;
		__this->set_available_4(((int32_t)((int32_t)L_19-(int32_t)L_20)));
	}

IL_006d:
	{
		int32_t L_21 = V_1;
		if ((((int32_t)L_21) > ((int32_t)0)))
		{
			goto IL_0015;
		}
	}
	{
		int32_t L_22 = ___length2;
		return L_22;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeByte()
extern Il2CppClass* ZipException_t2326470748_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2947082148;
extern const uint32_t InflaterInputBuffer_ReadLeByte_m1881028497_MetadataUsageId;
extern "C"  int32_t InflaterInputBuffer_ReadLeByte_m1881028497 (InflaterInputBuffer_t441930877 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputBuffer_ReadLeByte_m1881028497_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint8_t V_0 = 0x0;
	{
		int32_t L_0 = __this->get_available_4();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		InflaterInputBuffer_Fill_m1169415305(__this, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_available_4();
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_0023;
		}
	}
	{
		ZipException_t2326470748 * L_2 = (ZipException_t2326470748 *)il2cpp_codegen_object_new(ZipException_t2326470748_il2cpp_TypeInfo_var);
		ZipException__ctor_m3813253924(L_2, _stringLiteral2947082148, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0023:
	{
		ByteU5BU5D_t4260760469* L_3 = __this->get_rawData_1();
		int32_t L_4 = __this->get_rawLength_0();
		int32_t L_5 = __this->get_available_4();
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, ((int32_t)((int32_t)L_4-(int32_t)L_5)));
		int32_t L_6 = ((int32_t)((int32_t)L_4-(int32_t)L_5));
		uint8_t L_7 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_0 = L_7;
		int32_t L_8 = __this->get_available_4();
		__this->set_available_4(((int32_t)((int32_t)L_8-(int32_t)1)));
		uint8_t L_9 = V_0;
		return L_9;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::.ctor(System.IO.Stream,ICSharpCode.SharpZipLib.Zip.Compression.Inflater,System.Int32)
extern Il2CppClass* Stream_t1561764144_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppClass* InflaterInputBuffer_t441930877_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2049374297;
extern Il2CppCodeGenString* _stringLiteral175566667;
extern Il2CppCodeGenString* _stringLiteral1906231393;
extern const uint32_t InflaterInputStream__ctor_m1038306487_MetadataUsageId;
extern "C"  void InflaterInputStream__ctor_m1038306487 (InflaterInputStream_t928059325 * __this, Stream_t1561764144 * ___baseInputStream0, Inflater_t1975778921 * ___inflater1, int32_t ___bufferSize2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputStream__ctor_m1038306487_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_isStreamOwner_5((bool)1);
		IL2CPP_RUNTIME_CLASS_INIT(Stream_t1561764144_il2cpp_TypeInfo_var);
		Stream__ctor_m1433294025(__this, /*hidden argument*/NULL);
		Stream_t1561764144 * L_0 = ___baseInputStream0;
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral2049374297, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_001b:
	{
		Inflater_t1975778921 * L_2 = ___inflater1;
		if (L_2)
		{
			goto IL_0029;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_3 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_3, _stringLiteral175566667, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0029:
	{
		int32_t L_4 = ___bufferSize2;
		if ((((int32_t)L_4) > ((int32_t)0)))
		{
			goto IL_0038;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_5 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_5, _stringLiteral1906231393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0038:
	{
		Stream_t1561764144 * L_6 = ___baseInputStream0;
		__this->set_baseInputStream_3(L_6);
		Inflater_t1975778921 * L_7 = ___inflater1;
		__this->set_inf_1(L_7);
		Stream_t1561764144 * L_8 = ___baseInputStream0;
		int32_t L_9 = ___bufferSize2;
		InflaterInputBuffer_t441930877 * L_10 = (InflaterInputBuffer_t441930877 *)il2cpp_codegen_object_new(InflaterInputBuffer_t441930877_il2cpp_TypeInfo_var);
		InflaterInputBuffer__ctor_m3230220962(L_10, L_8, L_9, /*hidden argument*/NULL);
		__this->set_inputBuffer_2(L_10);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Fill()
extern Il2CppClass* SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2225963181;
extern const uint32_t InflaterInputStream_Fill_m1742624713_MetadataUsageId;
extern "C"  void InflaterInputStream_Fill_m1742624713 (InflaterInputStream_t928059325 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputStream_Fill_m1742624713_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		InflaterInputBuffer_t441930877 * L_0 = __this->get_inputBuffer_2();
		NullCheck(L_0);
		int32_t L_1 = InflaterInputBuffer_get_Available_m3610562184(L_0, /*hidden argument*/NULL);
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_0032;
		}
	}
	{
		InflaterInputBuffer_t441930877 * L_2 = __this->get_inputBuffer_2();
		NullCheck(L_2);
		InflaterInputBuffer_Fill_m1169415305(L_2, /*hidden argument*/NULL);
		InflaterInputBuffer_t441930877 * L_3 = __this->get_inputBuffer_2();
		NullCheck(L_3);
		int32_t L_4 = InflaterInputBuffer_get_Available_m3610562184(L_3, /*hidden argument*/NULL);
		if ((((int32_t)L_4) > ((int32_t)0)))
		{
			goto IL_0032;
		}
	}
	{
		SharpZipBaseException_t2520014981 * L_5 = (SharpZipBaseException_t2520014981 *)il2cpp_codegen_object_new(SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m2797155132(L_5, _stringLiteral2225963181, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0032:
	{
		InflaterInputBuffer_t441930877 * L_6 = __this->get_inputBuffer_2();
		Inflater_t1975778921 * L_7 = __this->get_inf_1();
		NullCheck(L_6);
		InflaterInputBuffer_SetInflaterInput_m3831681298(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_CanRead()
extern "C"  bool InflaterInputStream_get_CanRead_m2089789867 (InflaterInputStream_t928059325 * __this, const MethodInfo* method)
{
	{
		Stream_t1561764144 * L_0 = __this->get_baseInputStream_3();
		NullCheck(L_0);
		bool L_1 = VirtFuncInvoker0< bool >::Invoke(5 /* System.Boolean System.IO.Stream::get_CanRead() */, L_0);
		return L_1;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_CanSeek()
extern "C"  bool InflaterInputStream_get_CanSeek_m2118544909 (InflaterInputStream_t928059325 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_CanWrite()
extern "C"  bool InflaterInputStream_get_CanWrite_m881629964 (InflaterInputStream_t928059325 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_Length()
extern "C"  int64_t InflaterInputStream_get_Length_m4052515848 (InflaterInputStream_t928059325 * __this, const MethodInfo* method)
{
	{
		InflaterInputBuffer_t441930877 * L_0 = __this->get_inputBuffer_2();
		NullCheck(L_0);
		int32_t L_1 = InflaterInputBuffer_get_RawLength_m1307271085(L_0, /*hidden argument*/NULL);
		return (((int64_t)((int64_t)L_1)));
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_Position()
extern "C"  int64_t InflaterInputStream_get_Position_m3448861003 (InflaterInputStream_t928059325 * __this, const MethodInfo* method)
{
	{
		Stream_t1561764144 * L_0 = __this->get_baseInputStream_3();
		NullCheck(L_0);
		int64_t L_1 = VirtFuncInvoker0< int64_t >::Invoke(9 /* System.Int64 System.IO.Stream::get_Position() */, L_0);
		return L_1;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::set_Position(System.Int64)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1705031371;
extern const uint32_t InflaterInputStream_set_Position_m271322046_MetadataUsageId;
extern "C"  void InflaterInputStream_set_Position_m271322046 (InflaterInputStream_t928059325 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputStream_set_Position_m271322046_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, _stringLiteral1705031371, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Flush()
extern "C"  void InflaterInputStream_Flush_m2576227744 (InflaterInputStream_t928059325 * __this, const MethodInfo* method)
{
	{
		Stream_t1561764144 * L_0 = __this->get_baseInputStream_3();
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(16 /* System.Void System.IO.Stream::Flush() */, L_0);
		return;
	}
}
// System.Int64 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral692093977;
extern const uint32_t InflaterInputStream_Seek_m804621020_MetadataUsageId;
extern "C"  int64_t InflaterInputStream_Seek_m804621020 (InflaterInputStream_t928059325 * __this, int64_t ___offset0, int32_t ___origin1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputStream_Seek_m804621020_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, _stringLiteral692093977, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::SetLength(System.Int64)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral802706856;
extern const uint32_t InflaterInputStream_SetLength_m1374707926_MetadataUsageId;
extern "C"  void InflaterInputStream_SetLength_m1374707926 (InflaterInputStream_t928059325 * __this, int64_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputStream_SetLength_m1374707926_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, _stringLiteral802706856, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Write(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3602765535;
extern const uint32_t InflaterInputStream_Write_m2976178126_MetadataUsageId;
extern "C"  void InflaterInputStream_Write_m2976178126 (InflaterInputStream_t928059325 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputStream_Write_m2976178126_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, _stringLiteral3602765535, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::WriteByte(System.Byte)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2842470151;
extern const uint32_t InflaterInputStream_WriteByte_m519650728_MetadataUsageId;
extern "C"  void InflaterInputStream_WriteByte_m519650728 (InflaterInputStream_t928059325 * __this, uint8_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputStream_WriteByte_m519650728_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, _stringLiteral2842470151, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.IAsyncResult ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::BeginWrite(System.Byte[],System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern Il2CppClass* NotSupportedException_t1732551818_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3104333656;
extern const uint32_t InflaterInputStream_BeginWrite_m1517792417_MetadataUsageId;
extern "C"  Il2CppObject * InflaterInputStream_BeginWrite_m1517792417 (InflaterInputStream_t928059325 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, AsyncCallback_t1369114871 * ___callback3, Il2CppObject * ___state4, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputStream_BeginWrite_m1517792417_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1732551818 * L_0 = (NotSupportedException_t1732551818 *)il2cpp_codegen_object_new(NotSupportedException_t1732551818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m133757637(L_0, _stringLiteral3104333656, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Close()
extern "C"  void InflaterInputStream_Close_m4203139988 (InflaterInputStream_t928059325 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isClosed_4();
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		__this->set_isClosed_4((bool)1);
		bool L_1 = __this->get_isStreamOwner_5();
		if (!L_1)
		{
			goto IL_0022;
		}
	}
	{
		Stream_t1561764144 * L_2 = __this->get_baseInputStream_3();
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(12 /* System.Void System.IO.Stream::Close() */, L_2);
	}

IL_0022:
	{
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var;
extern Il2CppClass* ZipException_t2326470748_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2993560799;
extern Il2CppCodeGenString* _stringLiteral2088968762;
extern const uint32_t InflaterInputStream_Read_m13213145_MetadataUsageId;
extern "C"  int32_t InflaterInputStream_Read_m13213145 (InflaterInputStream_t928059325 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (InflaterInputStream_Read_m13213145_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Inflater_t1975778921 * L_0 = __this->get_inf_1();
		NullCheck(L_0);
		bool L_1 = Inflater_get_IsNeedingDictionary_m479189876(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		SharpZipBaseException_t2520014981 * L_2 = (SharpZipBaseException_t2520014981 *)il2cpp_codegen_object_new(SharpZipBaseException_t2520014981_il2cpp_TypeInfo_var);
		SharpZipBaseException__ctor_m2797155132(L_2, _stringLiteral2993560799, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		int32_t L_3 = ___count2;
		V_0 = L_3;
	}

IL_001a:
	{
		Inflater_t1975778921 * L_4 = __this->get_inf_1();
		ByteU5BU5D_t4260760469* L_5 = ___buffer0;
		int32_t L_6 = ___offset1;
		int32_t L_7 = V_0;
		NullCheck(L_4);
		int32_t L_8 = Inflater_Inflate_m2860834391(L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		int32_t L_9 = ___offset1;
		int32_t L_10 = V_1;
		___offset1 = ((int32_t)((int32_t)L_9+(int32_t)L_10));
		int32_t L_11 = V_0;
		int32_t L_12 = V_1;
		V_0 = ((int32_t)((int32_t)L_11-(int32_t)L_12));
		int32_t L_13 = V_0;
		if (!L_13)
		{
			goto IL_0065;
		}
	}
	{
		Inflater_t1975778921 * L_14 = __this->get_inf_1();
		NullCheck(L_14);
		bool L_15 = Inflater_get_IsFinished_m3219719202(L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_0065;
		}
	}
	{
		Inflater_t1975778921 * L_16 = __this->get_inf_1();
		NullCheck(L_16);
		bool L_17 = Inflater_get_IsNeedingInput_m2862282958(L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0057;
		}
	}
	{
		InflaterInputStream_Fill_m1742624713(__this, /*hidden argument*/NULL);
		goto IL_001a;
	}

IL_0057:
	{
		int32_t L_18 = V_1;
		if (L_18)
		{
			goto IL_001a;
		}
	}
	{
		ZipException_t2326470748 * L_19 = (ZipException_t2326470748 *)il2cpp_codegen_object_new(ZipException_t2326470748_il2cpp_TypeInfo_var);
		ZipException__ctor_m3813253924(L_19, _stringLiteral2088968762, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19);
	}

IL_0065:
	{
		int32_t L_20 = ___count2;
		int32_t L_21 = V_0;
		return ((int32_t)((int32_t)L_20-(int32_t)L_21));
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::Write(System.Int32)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3524056223;
extern const uint32_t OutputWindow_Write_m2128171238_MetadataUsageId;
extern "C"  void OutputWindow_Write_m2128171238 (OutputWindow_t617095569 * __this, int32_t ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OutputWindow_Write_m2128171238_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_windowFilled_2();
		int32_t L_1 = L_0;
		V_0 = L_1;
		__this->set_windowFilled_2(((int32_t)((int32_t)L_1+(int32_t)1)));
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)((int32_t)32768)))))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, _stringLiteral3524056223, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		ByteU5BU5D_t4260760469* L_4 = __this->get_window_0();
		int32_t L_5 = __this->get_windowEnd_1();
		int32_t L_6 = L_5;
		V_1 = L_6;
		__this->set_windowEnd_1(((int32_t)((int32_t)L_6+(int32_t)1)));
		int32_t L_7 = V_1;
		int32_t L_8 = ___value0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (uint8_t)(((int32_t)((uint8_t)L_8))));
		int32_t L_9 = __this->get_windowEnd_1();
		__this->set_windowEnd_1(((int32_t)((int32_t)L_9&(int32_t)((int32_t)32767))));
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::SlowRepeat(System.Int32,System.Int32,System.Int32)
extern "C"  void OutputWindow_SlowRepeat_m3539234201 (OutputWindow_t617095569 * __this, int32_t ___repStart0, int32_t ___length1, int32_t ___distance2, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		goto IL_0042;
	}

IL_0002:
	{
		ByteU5BU5D_t4260760469* L_0 = __this->get_window_0();
		int32_t L_1 = __this->get_windowEnd_1();
		int32_t L_2 = L_1;
		V_0 = L_2;
		__this->set_windowEnd_1(((int32_t)((int32_t)L_2+(int32_t)1)));
		int32_t L_3 = V_0;
		ByteU5BU5D_t4260760469* L_4 = __this->get_window_0();
		int32_t L_5 = ___repStart0;
		int32_t L_6 = L_5;
		___repStart0 = ((int32_t)((int32_t)L_6+(int32_t)1));
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_6);
		int32_t L_7 = L_6;
		uint8_t L_8 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (uint8_t)L_8);
		int32_t L_9 = __this->get_windowEnd_1();
		__this->set_windowEnd_1(((int32_t)((int32_t)L_9&(int32_t)((int32_t)32767))));
		int32_t L_10 = ___repStart0;
		___repStart0 = ((int32_t)((int32_t)L_10&(int32_t)((int32_t)32767)));
	}

IL_0042:
	{
		int32_t L_11 = ___length1;
		int32_t L_12 = L_11;
		___length1 = ((int32_t)((int32_t)L_12-(int32_t)1));
		if ((((int32_t)L_12) > ((int32_t)0)))
		{
			goto IL_0002;
		}
	}
	{
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::Repeat(System.Int32,System.Int32)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3524056223;
extern const uint32_t OutputWindow_Repeat_m2942755967_MetadataUsageId;
extern "C"  void OutputWindow_Repeat_m2942755967 (OutputWindow_t617095569 * __this, int32_t ___length0, int32_t ___distance1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OutputWindow_Repeat_m2942755967_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		int32_t L_0 = __this->get_windowFilled_2();
		int32_t L_1 = ___length0;
		int32_t L_2 = ((int32_t)((int32_t)L_0+(int32_t)L_1));
		V_2 = L_2;
		__this->set_windowFilled_2(L_2);
		int32_t L_3 = V_2;
		if ((((int32_t)L_3) <= ((int32_t)((int32_t)32768))))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_4 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_4, _stringLiteral3524056223, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		int32_t L_5 = __this->get_windowEnd_1();
		int32_t L_6 = ___distance1;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_5-(int32_t)L_6))&(int32_t)((int32_t)32767)));
		int32_t L_7 = ___length0;
		V_1 = ((int32_t)((int32_t)((int32_t)32768)-(int32_t)L_7));
		int32_t L_8 = V_0;
		int32_t L_9 = V_1;
		if ((((int32_t)L_8) > ((int32_t)L_9)))
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_10 = __this->get_windowEnd_1();
		int32_t L_11 = V_1;
		if ((((int32_t)L_10) >= ((int32_t)L_11)))
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_12 = ___length0;
		int32_t L_13 = ___distance1;
		if ((((int32_t)L_12) > ((int32_t)L_13)))
		{
			goto IL_0097;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_14 = __this->get_window_0();
		int32_t L_15 = V_0;
		ByteU5BU5D_t4260760469* L_16 = __this->get_window_0();
		int32_t L_17 = __this->get_windowEnd_1();
		int32_t L_18 = ___length0;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_14, L_15, (Il2CppArray *)(Il2CppArray *)L_16, L_17, L_18, /*hidden argument*/NULL);
		int32_t L_19 = __this->get_windowEnd_1();
		int32_t L_20 = ___length0;
		__this->set_windowEnd_1(((int32_t)((int32_t)L_19+(int32_t)L_20)));
		return;
	}

IL_0073:
	{
		ByteU5BU5D_t4260760469* L_21 = __this->get_window_0();
		int32_t L_22 = __this->get_windowEnd_1();
		int32_t L_23 = L_22;
		V_3 = L_23;
		__this->set_windowEnd_1(((int32_t)((int32_t)L_23+(int32_t)1)));
		int32_t L_24 = V_3;
		ByteU5BU5D_t4260760469* L_25 = __this->get_window_0();
		int32_t L_26 = V_0;
		int32_t L_27 = L_26;
		V_0 = ((int32_t)((int32_t)L_27+(int32_t)1));
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_27);
		int32_t L_28 = L_27;
		uint8_t L_29 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_24);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(L_24), (uint8_t)L_29);
	}

IL_0097:
	{
		int32_t L_30 = ___length0;
		int32_t L_31 = L_30;
		___length0 = ((int32_t)((int32_t)L_31-(int32_t)1));
		if ((((int32_t)L_31) > ((int32_t)0)))
		{
			goto IL_0073;
		}
	}
	{
		return;
	}

IL_00a1:
	{
		int32_t L_32 = V_0;
		int32_t L_33 = ___length0;
		int32_t L_34 = ___distance1;
		OutputWindow_SlowRepeat_m3539234201(__this, L_32, L_33, L_34, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::CopyStored(ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator,System.Int32)
extern "C"  int32_t OutputWindow_CopyStored_m495876610 (OutputWindow_t617095569 * __this, StreamManipulator_t2348681196 * ___input0, int32_t ___length1, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = ___length1;
		int32_t L_1 = __this->get_windowFilled_2();
		int32_t L_2 = Math_Min_m811624909(NULL /*static, unused*/, L_0, ((int32_t)((int32_t)((int32_t)32768)-(int32_t)L_1)), /*hidden argument*/NULL);
		StreamManipulator_t2348681196 * L_3 = ___input0;
		NullCheck(L_3);
		int32_t L_4 = StreamManipulator_get_AvailableBytes_m3731022420(L_3, /*hidden argument*/NULL);
		int32_t L_5 = Math_Min_m811624909(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		___length1 = L_5;
		int32_t L_6 = __this->get_windowEnd_1();
		V_1 = ((int32_t)((int32_t)((int32_t)32768)-(int32_t)L_6));
		int32_t L_7 = ___length1;
		int32_t L_8 = V_1;
		if ((((int32_t)L_7) <= ((int32_t)L_8)))
		{
			goto IL_005d;
		}
	}
	{
		StreamManipulator_t2348681196 * L_9 = ___input0;
		ByteU5BU5D_t4260760469* L_10 = __this->get_window_0();
		int32_t L_11 = __this->get_windowEnd_1();
		int32_t L_12 = V_1;
		NullCheck(L_9);
		int32_t L_13 = StreamManipulator_CopyBytes_m2592044410(L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		int32_t L_14 = V_0;
		int32_t L_15 = V_1;
		if ((!(((uint32_t)L_14) == ((uint32_t)L_15))))
		{
			goto IL_0071;
		}
	}
	{
		int32_t L_16 = V_0;
		StreamManipulator_t2348681196 * L_17 = ___input0;
		ByteU5BU5D_t4260760469* L_18 = __this->get_window_0();
		int32_t L_19 = ___length1;
		int32_t L_20 = V_1;
		NullCheck(L_17);
		int32_t L_21 = StreamManipulator_CopyBytes_m2592044410(L_17, L_18, 0, ((int32_t)((int32_t)L_19-(int32_t)L_20)), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)L_21));
		goto IL_0071;
	}

IL_005d:
	{
		StreamManipulator_t2348681196 * L_22 = ___input0;
		ByteU5BU5D_t4260760469* L_23 = __this->get_window_0();
		int32_t L_24 = __this->get_windowEnd_1();
		int32_t L_25 = ___length1;
		NullCheck(L_22);
		int32_t L_26 = StreamManipulator_CopyBytes_m2592044410(L_22, L_23, L_24, L_25, /*hidden argument*/NULL);
		V_0 = L_26;
	}

IL_0071:
	{
		int32_t L_27 = __this->get_windowEnd_1();
		int32_t L_28 = V_0;
		__this->set_windowEnd_1(((int32_t)((int32_t)((int32_t)((int32_t)L_27+(int32_t)L_28))&(int32_t)((int32_t)32767))));
		int32_t L_29 = __this->get_windowFilled_2();
		int32_t L_30 = V_0;
		__this->set_windowFilled_2(((int32_t)((int32_t)L_29+(int32_t)L_30)));
		int32_t L_31 = V_0;
		return L_31;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::GetFreeSpace()
extern "C"  int32_t OutputWindow_GetFreeSpace_m3222198588 (OutputWindow_t617095569 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_windowFilled_2();
		return ((int32_t)((int32_t)((int32_t)32768)-(int32_t)L_0));
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::GetAvailable()
extern "C"  int32_t OutputWindow_GetAvailable_m136666699 (OutputWindow_t617095569 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_windowFilled_2();
		return L_0;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::CopyOutput(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t OutputWindow_CopyOutput_m3388051483_MetadataUsageId;
extern "C"  int32_t OutputWindow_CopyOutput_m3388051483 (OutputWindow_t617095569 * __this, ByteU5BU5D_t4260760469* ___output0, int32_t ___offset1, int32_t ___len2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OutputWindow_CopyOutput_m3388051483_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = __this->get_windowEnd_1();
		V_0 = L_0;
		int32_t L_1 = ___len2;
		int32_t L_2 = __this->get_windowFilled_2();
		if ((((int32_t)L_1) <= ((int32_t)L_2)))
		{
			goto IL_001a;
		}
	}
	{
		int32_t L_3 = __this->get_windowFilled_2();
		___len2 = L_3;
		goto IL_0030;
	}

IL_001a:
	{
		int32_t L_4 = __this->get_windowEnd_1();
		int32_t L_5 = __this->get_windowFilled_2();
		int32_t L_6 = ___len2;
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_4-(int32_t)L_5))+(int32_t)L_6))&(int32_t)((int32_t)32767)));
	}

IL_0030:
	{
		int32_t L_7 = ___len2;
		V_1 = L_7;
		int32_t L_8 = ___len2;
		int32_t L_9 = V_0;
		V_2 = ((int32_t)((int32_t)L_8-(int32_t)L_9));
		int32_t L_10 = V_2;
		if ((((int32_t)L_10) <= ((int32_t)0)))
		{
			goto IL_0057;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_11 = __this->get_window_0();
		int32_t L_12 = V_2;
		ByteU5BU5D_t4260760469* L_13 = ___output0;
		int32_t L_14 = ___offset1;
		int32_t L_15 = V_2;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_11, ((int32_t)((int32_t)((int32_t)32768)-(int32_t)L_12)), (Il2CppArray *)(Il2CppArray *)L_13, L_14, L_15, /*hidden argument*/NULL);
		int32_t L_16 = ___offset1;
		int32_t L_17 = V_2;
		___offset1 = ((int32_t)((int32_t)L_16+(int32_t)L_17));
		int32_t L_18 = V_0;
		___len2 = L_18;
	}

IL_0057:
	{
		ByteU5BU5D_t4260760469* L_19 = __this->get_window_0();
		int32_t L_20 = V_0;
		int32_t L_21 = ___len2;
		ByteU5BU5D_t4260760469* L_22 = ___output0;
		int32_t L_23 = ___offset1;
		int32_t L_24 = ___len2;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_19, ((int32_t)((int32_t)L_20-(int32_t)L_21)), (Il2CppArray *)(Il2CppArray *)L_22, L_23, L_24, /*hidden argument*/NULL);
		int32_t L_25 = __this->get_windowFilled_2();
		int32_t L_26 = V_1;
		__this->set_windowFilled_2(((int32_t)((int32_t)L_25-(int32_t)L_26)));
		int32_t L_27 = __this->get_windowFilled_2();
		if ((((int32_t)L_27) >= ((int32_t)0)))
		{
			goto IL_0085;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_28 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_28, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_28);
	}

IL_0085:
	{
		int32_t L_29 = V_1;
		return L_29;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::Reset()
extern "C"  void OutputWindow_Reset_m883473541 (OutputWindow_t617095569 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = 0;
		V_0 = L_0;
		__this->set_windowEnd_1(L_0);
		int32_t L_1 = V_0;
		__this->set_windowFilled_2(L_1);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.OutputWindow::.ctor()
extern Il2CppClass* ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var;
extern const uint32_t OutputWindow__ctor_m3237040600_MetadataUsageId;
extern "C"  void OutputWindow__ctor_m3237040600 (OutputWindow_t617095569 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OutputWindow__ctor_m3237040600_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_window_0(((ByteU5BU5D_t4260760469*)SZArrayNew(ByteU5BU5D_t4260760469_il2cpp_TypeInfo_var, (uint32_t)((int32_t)32768))));
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::.ctor()
extern "C"  void StreamManipulator__ctor_m2051970415 (StreamManipulator_t2348681196 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::PeekBits(System.Int32)
extern "C"  int32_t StreamManipulator_PeekBits_m2187722491 (StreamManipulator_t2348681196 * __this, int32_t ___bitCount0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = __this->get_bitsInBuffer__4();
		int32_t L_1 = ___bitCount0;
		if ((((int32_t)L_0) >= ((int32_t)L_1)))
		{
			goto IL_007e;
		}
	}
	{
		int32_t L_2 = __this->get_windowStart__1();
		int32_t L_3 = __this->get_windowEnd__2();
		if ((!(((uint32_t)L_2) == ((uint32_t)L_3))))
		{
			goto IL_0019;
		}
	}
	{
		return (-1);
	}

IL_0019:
	{
		uint32_t L_4 = __this->get_buffer__3();
		ByteU5BU5D_t4260760469* L_5 = __this->get_window__0();
		int32_t L_6 = __this->get_windowStart__1();
		int32_t L_7 = L_6;
		V_0 = L_7;
		__this->set_windowStart__1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		uint8_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		ByteU5BU5D_t4260760469* L_11 = __this->get_window__0();
		int32_t L_12 = __this->get_windowStart__1();
		int32_t L_13 = L_12;
		V_1 = L_13;
		__this->set_windowStart__1(((int32_t)((int32_t)L_13+(int32_t)1)));
		int32_t L_14 = V_1;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_14);
		int32_t L_15 = L_14;
		uint8_t L_16 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_15));
		int32_t L_17 = __this->get_bitsInBuffer__4();
		__this->set_buffer__3(((int32_t)((int32_t)L_4|(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_10&(int32_t)((int32_t)255)))|(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_16&(int32_t)((int32_t)255)))<<(int32_t)8))))<<(int32_t)((int32_t)((int32_t)L_17&(int32_t)((int32_t)31))))))));
		int32_t L_18 = __this->get_bitsInBuffer__4();
		__this->set_bitsInBuffer__4(((int32_t)((int32_t)L_18+(int32_t)((int32_t)16))));
	}

IL_007e:
	{
		uint32_t L_19 = __this->get_buffer__3();
		int32_t L_20 = ___bitCount0;
		return (((int32_t)((int32_t)((int64_t)((int64_t)(((int64_t)((uint64_t)L_19)))&(int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)1<<(int32_t)((int32_t)((int32_t)L_20&(int32_t)((int32_t)31)))))-(int32_t)1))))))))));
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::DropBits(System.Int32)
extern "C"  void StreamManipulator_DropBits_m3482227099 (StreamManipulator_t2348681196 * __this, int32_t ___bitCount0, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_buffer__3();
		int32_t L_1 = ___bitCount0;
		__this->set_buffer__3(((int32_t)((uint32_t)L_0>>((int32_t)((int32_t)L_1&(int32_t)((int32_t)31))))));
		int32_t L_2 = __this->get_bitsInBuffer__4();
		int32_t L_3 = ___bitCount0;
		__this->set_bitsInBuffer__4(((int32_t)((int32_t)L_2-(int32_t)L_3)));
		return;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_AvailableBits()
extern "C"  int32_t StreamManipulator_get_AvailableBits_m521232351 (StreamManipulator_t2348681196 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_bitsInBuffer__4();
		return L_0;
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_AvailableBytes()
extern "C"  int32_t StreamManipulator_get_AvailableBytes_m3731022420 (StreamManipulator_t2348681196 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_windowEnd__2();
		int32_t L_1 = __this->get_windowStart__1();
		int32_t L_2 = __this->get_bitsInBuffer__4();
		return ((int32_t)((int32_t)((int32_t)((int32_t)L_0-(int32_t)L_1))+(int32_t)((int32_t)((int32_t)L_2>>(int32_t)3))));
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::SkipToByteBoundary()
extern "C"  void StreamManipulator_SkipToByteBoundary_m2882027201 (StreamManipulator_t2348681196 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = __this->get_buffer__3();
		int32_t L_1 = __this->get_bitsInBuffer__4();
		__this->set_buffer__3(((int32_t)((uint32_t)L_0>>((int32_t)((int32_t)((int32_t)((int32_t)L_1&(int32_t)7))&(int32_t)((int32_t)31))))));
		int32_t L_2 = __this->get_bitsInBuffer__4();
		__this->set_bitsInBuffer__4(((int32_t)((int32_t)L_2&(int32_t)((int32_t)-8))));
		return;
	}
}
// System.Boolean ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_IsNeedingInput()
extern "C"  bool StreamManipulator_get_IsNeedingInput_m89298932 (StreamManipulator_t2348681196 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_windowStart__1();
		int32_t L_1 = __this->get_windowEnd__2();
		return (bool)((((int32_t)L_0) == ((int32_t)L_1))? 1 : 0);
	}
}
// System.Int32 ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::CopyBytes(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3188603622;
extern Il2CppCodeGenString* _stringLiteral888195455;
extern const uint32_t StreamManipulator_CopyBytes_m2592044410_MetadataUsageId;
extern "C"  int32_t StreamManipulator_CopyBytes_m2592044410 (StreamManipulator_t2348681196 * __this, ByteU5BU5D_t4260760469* ___output0, int32_t ___offset1, int32_t ___length2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StreamManipulator_CopyBytes_m2592044410_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		int32_t L_0 = ___length2;
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_000f;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_1 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_1, _stringLiteral3188603622, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000f:
	{
		int32_t L_2 = __this->get_bitsInBuffer__4();
		if (!((int32_t)((int32_t)L_2&(int32_t)7)))
		{
			goto IL_0024;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_3, _stringLiteral888195455, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0024:
	{
		V_0 = 0;
		goto IL_005c;
	}

IL_0028:
	{
		ByteU5BU5D_t4260760469* L_4 = ___output0;
		int32_t L_5 = ___offset1;
		int32_t L_6 = L_5;
		___offset1 = ((int32_t)((int32_t)L_6+(int32_t)1));
		uint32_t L_7 = __this->get_buffer__3();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_6);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (uint8_t)(((int32_t)((uint8_t)L_7))));
		uint32_t L_8 = __this->get_buffer__3();
		__this->set_buffer__3(((int32_t)((uint32_t)L_8>>8)));
		int32_t L_9 = __this->get_bitsInBuffer__4();
		__this->set_bitsInBuffer__4(((int32_t)((int32_t)L_9-(int32_t)8)));
		int32_t L_10 = ___length2;
		___length2 = ((int32_t)((int32_t)L_10-(int32_t)1));
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_005c:
	{
		int32_t L_12 = __this->get_bitsInBuffer__4();
		if ((((int32_t)L_12) <= ((int32_t)0)))
		{
			goto IL_0069;
		}
	}
	{
		int32_t L_13 = ___length2;
		if ((((int32_t)L_13) > ((int32_t)0)))
		{
			goto IL_0028;
		}
	}

IL_0069:
	{
		int32_t L_14 = ___length2;
		if (L_14)
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_15 = V_0;
		return L_15;
	}

IL_006e:
	{
		int32_t L_16 = __this->get_windowEnd__2();
		int32_t L_17 = __this->get_windowStart__1();
		V_1 = ((int32_t)((int32_t)L_16-(int32_t)L_17));
		int32_t L_18 = ___length2;
		int32_t L_19 = V_1;
		if ((((int32_t)L_18) <= ((int32_t)L_19)))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_20 = V_1;
		___length2 = L_20;
	}

IL_0083:
	{
		ByteU5BU5D_t4260760469* L_21 = __this->get_window__0();
		int32_t L_22 = __this->get_windowStart__1();
		ByteU5BU5D_t4260760469* L_23 = ___output0;
		int32_t L_24 = ___offset1;
		int32_t L_25 = ___length2;
		Array_Copy_m2598616668(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_21, L_22, (Il2CppArray *)(Il2CppArray *)L_23, L_24, L_25, /*hidden argument*/NULL);
		int32_t L_26 = __this->get_windowStart__1();
		int32_t L_27 = ___length2;
		__this->set_windowStart__1(((int32_t)((int32_t)L_26+(int32_t)L_27)));
		int32_t L_28 = __this->get_windowStart__1();
		int32_t L_29 = __this->get_windowEnd__2();
		if (!((int32_t)((int32_t)((int32_t)((int32_t)L_28-(int32_t)L_29))&(int32_t)1)))
		{
			goto IL_00e1;
		}
	}
	{
		ByteU5BU5D_t4260760469* L_30 = __this->get_window__0();
		int32_t L_31 = __this->get_windowStart__1();
		int32_t L_32 = L_31;
		V_2 = L_32;
		__this->set_windowStart__1(((int32_t)((int32_t)L_32+(int32_t)1)));
		int32_t L_33 = V_2;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, L_33);
		int32_t L_34 = L_33;
		uint8_t L_35 = (L_30)->GetAt(static_cast<il2cpp_array_size_t>(L_34));
		__this->set_buffer__3(((int32_t)((int32_t)L_35&(int32_t)((int32_t)255))));
		__this->set_bitsInBuffer__4(8);
	}

IL_00e1:
	{
		int32_t L_36 = V_0;
		int32_t L_37 = ___length2;
		return ((int32_t)((int32_t)L_36+(int32_t)L_37));
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::Reset()
extern "C"  void StreamManipulator_Reset_m3993370652 (StreamManipulator_t2348681196 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		__this->set_buffer__3(0);
		int32_t L_0 = 0;
		V_0 = L_0;
		__this->set_bitsInBuffer__4(L_0);
		int32_t L_1 = V_0;
		int32_t L_2 = L_1;
		V_1 = L_2;
		__this->set_windowEnd__2(L_2);
		int32_t L_3 = V_1;
		__this->set_windowStart__1(L_3);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.Compression.Streams.StreamManipulator::SetInput(System.Byte[],System.Int32,System.Int32)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2916848704;
extern Il2CppCodeGenString* _stringLiteral3275187347;
extern Il2CppCodeGenString* _stringLiteral2755219445;
extern Il2CppCodeGenString* _stringLiteral94851343;
extern Il2CppCodeGenString* _stringLiteral169949703;
extern const uint32_t StreamManipulator_SetInput_m695463628_MetadataUsageId;
extern "C"  void StreamManipulator_SetInput_m695463628 (StreamManipulator_t2348681196 * __this, ByteU5BU5D_t4260760469* ___buffer0, int32_t ___offset1, int32_t ___count2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (StreamManipulator_SetInput_m695463628_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		ByteU5BU5D_t4260760469* L_0 = ___buffer0;
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, _stringLiteral2916848704, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_000e:
	{
		int32_t L_2 = ___offset1;
		if ((((int32_t)L_2) >= ((int32_t)0)))
		{
			goto IL_0022;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_3 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1193970951(L_3, _stringLiteral3275187347, _stringLiteral2755219445, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0022:
	{
		int32_t L_4 = ___count2;
		if ((((int32_t)L_4) >= ((int32_t)0)))
		{
			goto IL_0036;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_5 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m1193970951(L_5, _stringLiteral94851343, _stringLiteral2755219445, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0036:
	{
		int32_t L_6 = __this->get_windowStart__1();
		int32_t L_7 = __this->get_windowEnd__2();
		if ((((int32_t)L_6) >= ((int32_t)L_7)))
		{
			goto IL_004f;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_8 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_8, _stringLiteral169949703, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_004f:
	{
		int32_t L_9 = ___offset1;
		int32_t L_10 = ___count2;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)L_10));
		int32_t L_11 = ___offset1;
		int32_t L_12 = V_0;
		if ((((int32_t)L_11) > ((int32_t)L_12)))
		{
			goto IL_005d;
		}
	}
	{
		int32_t L_13 = V_0;
		ByteU5BU5D_t4260760469* L_14 = ___buffer0;
		NullCheck(L_14);
		if ((((int32_t)L_13) <= ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length)))))))
		{
			goto IL_0068;
		}
	}

IL_005d:
	{
		ArgumentOutOfRangeException_t3816648464 * L_15 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_15, _stringLiteral94851343, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_15);
	}

IL_0068:
	{
		int32_t L_16 = ___count2;
		if (!((int32_t)((int32_t)L_16&(int32_t)1)))
		{
			goto IL_00a0;
		}
	}
	{
		uint32_t L_17 = __this->get_buffer__3();
		ByteU5BU5D_t4260760469* L_18 = ___buffer0;
		int32_t L_19 = ___offset1;
		int32_t L_20 = L_19;
		___offset1 = ((int32_t)((int32_t)L_20+(int32_t)1));
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, L_20);
		int32_t L_21 = L_20;
		uint8_t L_22 = (L_18)->GetAt(static_cast<il2cpp_array_size_t>(L_21));
		int32_t L_23 = __this->get_bitsInBuffer__4();
		__this->set_buffer__3(((int32_t)((int32_t)L_17|(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_22&(int32_t)((int32_t)255)))<<(int32_t)((int32_t)((int32_t)L_23&(int32_t)((int32_t)31))))))));
		int32_t L_24 = __this->get_bitsInBuffer__4();
		__this->set_bitsInBuffer__4(((int32_t)((int32_t)L_24+(int32_t)8)));
	}

IL_00a0:
	{
		ByteU5BU5D_t4260760469* L_25 = ___buffer0;
		__this->set_window__0(L_25);
		int32_t L_26 = ___offset1;
		__this->set_windowStart__1(L_26);
		int32_t L_27 = V_0;
		__this->set_windowEnd__2(L_27);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void ZipException__ctor_m4094346463 (ZipException_t2326470748 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	{
		SerializationInfo_t2185721892 * L_0 = ___info0;
		StreamingContext_t2761351129  L_1 = ___context1;
		SharpZipBaseException__ctor_m3034235335(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipException::.ctor()
extern "C"  void ZipException__ctor_m2625236126 (ZipException_t2326470748 * __this, const MethodInfo* method)
{
	{
		SharpZipBaseException__ctor_m730469766(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ICSharpCode.SharpZipLib.Zip.ZipException::.ctor(System.String)
extern "C"  void ZipException__ctor_m3813253924 (ZipException_t2326470748 * __this, String_t* ___message0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___message0;
		SharpZipBaseException__ctor_m2797155132(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
