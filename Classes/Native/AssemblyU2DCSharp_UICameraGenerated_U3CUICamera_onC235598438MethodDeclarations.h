﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICameraGenerated/<UICamera_onCustomInput_GetDelegate_member29_arg0>c__AnonStoreyA2
struct U3CUICamera_onCustomInput_GetDelegate_member29_arg0U3Ec__AnonStoreyA2_t235598438;

#include "codegen/il2cpp-codegen.h"

// System.Void UICameraGenerated/<UICamera_onCustomInput_GetDelegate_member29_arg0>c__AnonStoreyA2::.ctor()
extern "C"  void U3CUICamera_onCustomInput_GetDelegate_member29_arg0U3Ec__AnonStoreyA2__ctor_m364590021 (U3CUICamera_onCustomInput_GetDelegate_member29_arg0U3Ec__AnonStoreyA2_t235598438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated/<UICamera_onCustomInput_GetDelegate_member29_arg0>c__AnonStoreyA2::<>m__108()
extern "C"  void U3CUICamera_onCustomInput_GetDelegate_member29_arg0U3Ec__AnonStoreyA2_U3CU3Em__108_m2358801933 (U3CUICamera_onCustomInput_GetDelegate_member29_arg0U3Ec__AnonStoreyA2_t235598438 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
