﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Linq.JToken/<Ancestors>c__Iterator19
struct U3CAncestorsU3Ec__Iterator19_t2599550982;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerator_1_t1029143704;

#include "codegen/il2cpp-codegen.h"

// System.Void Newtonsoft.Json.Linq.JToken/<Ancestors>c__Iterator19::.ctor()
extern "C"  void U3CAncestorsU3Ec__Iterator19__ctor_m4137722197 (U3CAncestorsU3Ec__Iterator19_t2599550982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken/<Ancestors>c__Iterator19::System.Collections.Generic.IEnumerator<Newtonsoft.Json.Linq.JToken>.get_Current()
extern "C"  JToken_t3412245951 * U3CAncestorsU3Ec__Iterator19_System_Collections_Generic_IEnumeratorU3CNewtonsoft_Json_Linq_JTokenU3E_get_Current_m3268480702 (U3CAncestorsU3Ec__Iterator19_t2599550982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Newtonsoft.Json.Linq.JToken/<Ancestors>c__Iterator19::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CAncestorsU3Ec__Iterator19_System_Collections_IEnumerator_get_Current_m492619963 (U3CAncestorsU3Ec__Iterator19_t2599550982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Newtonsoft.Json.Linq.JToken/<Ancestors>c__Iterator19::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CAncestorsU3Ec__Iterator19_System_Collections_IEnumerable_GetEnumerator_m2218661372 (U3CAncestorsU3Ec__Iterator19_t2599550982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JToken/<Ancestors>c__Iterator19::System.Collections.Generic.IEnumerable<Newtonsoft.Json.Linq.JToken>.GetEnumerator()
extern "C"  Il2CppObject* U3CAncestorsU3Ec__Iterator19_System_Collections_Generic_IEnumerableU3CNewtonsoft_Json_Linq_JTokenU3E_GetEnumerator_m2995794943 (U3CAncestorsU3Ec__Iterator19_t2599550982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Linq.JToken/<Ancestors>c__Iterator19::MoveNext()
extern "C"  bool U3CAncestorsU3Ec__Iterator19_MoveNext_m2341037671 (U3CAncestorsU3Ec__Iterator19_t2599550982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken/<Ancestors>c__Iterator19::Dispose()
extern "C"  void U3CAncestorsU3Ec__Iterator19_Dispose_m3404565586 (U3CAncestorsU3Ec__Iterator19_t2599550982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Linq.JToken/<Ancestors>c__Iterator19::Reset()
extern "C"  void U3CAncestorsU3Ec__Iterator19_Reset_m1784155138 (U3CAncestorsU3Ec__Iterator19_t2599550982 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
