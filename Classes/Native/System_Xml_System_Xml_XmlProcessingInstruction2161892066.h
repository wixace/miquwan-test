﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "System_Xml_System_Xml_XmlLinkedNode901819716.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlProcessingInstruction
struct  XmlProcessingInstruction_t2161892066  : public XmlLinkedNode_t901819716
{
public:
	// System.String System.Xml.XmlProcessingInstruction::target
	String_t* ___target_8;
	// System.String System.Xml.XmlProcessingInstruction::data
	String_t* ___data_9;

public:
	inline static int32_t get_offset_of_target_8() { return static_cast<int32_t>(offsetof(XmlProcessingInstruction_t2161892066, ___target_8)); }
	inline String_t* get_target_8() const { return ___target_8; }
	inline String_t** get_address_of_target_8() { return &___target_8; }
	inline void set_target_8(String_t* value)
	{
		___target_8 = value;
		Il2CppCodeGenWriteBarrier(&___target_8, value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(XmlProcessingInstruction_t2161892066, ___data_9)); }
	inline String_t* get_data_9() const { return ___data_9; }
	inline String_t** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(String_t* value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier(&___data_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
