﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_supemtreSawkurde154
struct M_supemtreSawkurde154_t232865823;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_supemtreSawkurde154232865823.h"

// System.Void GarbageiOS.M_supemtreSawkurde154::.ctor()
extern "C"  void M_supemtreSawkurde154__ctor_m3241825828 (M_supemtreSawkurde154_t232865823 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_supemtreSawkurde154::M_jorsafirNasstal0(System.String[],System.Int32)
extern "C"  void M_supemtreSawkurde154_M_jorsafirNasstal0_m279076193 (M_supemtreSawkurde154_t232865823 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_supemtreSawkurde154::M_serewowi1(System.String[],System.Int32)
extern "C"  void M_supemtreSawkurde154_M_serewowi1_m2464544329 (M_supemtreSawkurde154_t232865823 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_supemtreSawkurde154::M_cestermeTritooce2(System.String[],System.Int32)
extern "C"  void M_supemtreSawkurde154_M_cestermeTritooce2_m2288701030 (M_supemtreSawkurde154_t232865823 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_supemtreSawkurde154::M_delpeKowduha3(System.String[],System.Int32)
extern "C"  void M_supemtreSawkurde154_M_delpeKowduha3_m1870003901 (M_supemtreSawkurde154_t232865823 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_supemtreSawkurde154::ilo_M_cestermeTritooce21(GarbageiOS.M_supemtreSawkurde154,System.String[],System.Int32)
extern "C"  void M_supemtreSawkurde154_ilo_M_cestermeTritooce21_m3884593085 (Il2CppObject * __this /* static, unused */, M_supemtreSawkurde154_t232865823 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
