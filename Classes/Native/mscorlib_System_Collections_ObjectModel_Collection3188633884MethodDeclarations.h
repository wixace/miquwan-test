﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>
struct Collection_1_t3188633884;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t528650843;
// System.Collections.Generic.IEnumerator`1<UnityEngine.RaycastHit>
struct IEnumerator_1_t1620073479;
// System.Collections.Generic.IList`1<UnityEngine.RaycastHit>
struct IList_1_t2402855633;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::.ctor()
extern "C"  void Collection_1__ctor_m245751336_gshared (Collection_1_t3188633884 * __this, const MethodInfo* method);
#define Collection_1__ctor_m245751336(__this, method) ((  void (*) (Collection_1_t3188633884 *, const MethodInfo*))Collection_1__ctor_m245751336_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3825121807_gshared (Collection_1_t3188633884 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3825121807(__this, method) ((  bool (*) (Collection_1_t3188633884 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3825121807_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void Collection_1_System_Collections_ICollection_CopyTo_m56663068_gshared (Collection_1_t3188633884 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m56663068(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3188633884 *, Il2CppArray *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m56663068_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * Collection_1_System_Collections_IEnumerable_GetEnumerator_m2885806891_gshared (Collection_1_t3188633884 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m2885806891(__this, method) ((  Il2CppObject * (*) (Collection_1_t3188633884 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m2885806891_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.IList.Add(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_Add_m748176114_gshared (Collection_1_t3188633884 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m748176114(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3188633884 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m748176114_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.IList.Contains(System.Object)
extern "C"  bool Collection_1_System_Collections_IList_Contains_m1830249358_gshared (Collection_1_t3188633884 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m1830249358(__this, ___value0, method) ((  bool (*) (Collection_1_t3188633884 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m1830249358_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.IList.IndexOf(System.Object)
extern "C"  int32_t Collection_1_System_Collections_IList_IndexOf_m2134487114_gshared (Collection_1_t3188633884 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m2134487114(__this, ___value0, method) ((  int32_t (*) (Collection_1_t3188633884 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m2134487114_gshared)(__this, ___value0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_Insert_m3610527421_gshared (Collection_1_t3188633884 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m3610527421(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3188633884 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m3610527421_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.IList.Remove(System.Object)
extern "C"  void Collection_1_System_Collections_IList_Remove_m375534411_gshared (Collection_1_t3188633884 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m375534411(__this, ___value0, method) ((  void (*) (Collection_1_t3188633884 *, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m375534411_gshared)(__this, ___value0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m343777550_gshared (Collection_1_t3188633884 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m343777550(__this, method) ((  bool (*) (Collection_1_t3188633884 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m343777550_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * Collection_1_System_Collections_ICollection_get_SyncRoot_m4282225664_gshared (Collection_1_t3188633884 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m4282225664(__this, method) ((  Il2CppObject * (*) (Collection_1_t3188633884 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m4282225664_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.IList.get_IsFixedSize()
extern "C"  bool Collection_1_System_Collections_IList_get_IsFixedSize_m3676067197_gshared (Collection_1_t3188633884 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m3676067197(__this, method) ((  bool (*) (Collection_1_t3188633884 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m3676067197_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.IList.get_IsReadOnly()
extern "C"  bool Collection_1_System_Collections_IList_get_IsReadOnly_m2650801628_gshared (Collection_1_t3188633884 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m2650801628(__this, method) ((  bool (*) (Collection_1_t3188633884 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m2650801628_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.IList.get_Item(System.Int32)
extern "C"  Il2CppObject * Collection_1_System_Collections_IList_get_Item_m1705805191_gshared (Collection_1_t3188633884 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m1705805191(__this, ___index0, method) ((  Il2CppObject * (*) (Collection_1_t3188633884 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m1705805191_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C"  void Collection_1_System_Collections_IList_set_Item_m4292196180_gshared (Collection_1_t3188633884 * __this, int32_t ___index0, Il2CppObject * ___value1, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m4292196180(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3188633884 *, int32_t, Il2CppObject *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m4292196180_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::Add(T)
extern "C"  void Collection_1_Add_m413711703_gshared (Collection_1_t3188633884 * __this, RaycastHit_t4003175726  ___item0, const MethodInfo* method);
#define Collection_1_Add_m413711703(__this, ___item0, method) ((  void (*) (Collection_1_t3188633884 *, RaycastHit_t4003175726 , const MethodInfo*))Collection_1_Add_m413711703_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::Clear()
extern "C"  void Collection_1_Clear_m1946851923_gshared (Collection_1_t3188633884 * __this, const MethodInfo* method);
#define Collection_1_Clear_m1946851923(__this, method) ((  void (*) (Collection_1_t3188633884 *, const MethodInfo*))Collection_1_Clear_m1946851923_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::ClearItems()
extern "C"  void Collection_1_ClearItems_m831750799_gshared (Collection_1_t3188633884 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m831750799(__this, method) ((  void (*) (Collection_1_t3188633884 *, const MethodInfo*))Collection_1_ClearItems_m831750799_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::Contains(T)
extern "C"  bool Collection_1_Contains_m4032635077_gshared (Collection_1_t3188633884 * __this, RaycastHit_t4003175726  ___item0, const MethodInfo* method);
#define Collection_1_Contains_m4032635077(__this, ___item0, method) ((  bool (*) (Collection_1_t3188633884 *, RaycastHit_t4003175726 , const MethodInfo*))Collection_1_Contains_m4032635077_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::CopyTo(T[],System.Int32)
extern "C"  void Collection_1_CopyTo_m3135282567_gshared (Collection_1_t3188633884 * __this, RaycastHitU5BU5D_t528650843* ___array0, int32_t ___index1, const MethodInfo* method);
#define Collection_1_CopyTo_m3135282567(__this, ___array0, ___index1, method) ((  void (*) (Collection_1_t3188633884 *, RaycastHitU5BU5D_t528650843*, int32_t, const MethodInfo*))Collection_1_CopyTo_m3135282567_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::GetEnumerator()
extern "C"  Il2CppObject* Collection_1_GetEnumerator_m323469468_gshared (Collection_1_t3188633884 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m323469468(__this, method) ((  Il2CppObject* (*) (Collection_1_t3188633884 *, const MethodInfo*))Collection_1_GetEnumerator_m323469468_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::IndexOf(T)
extern "C"  int32_t Collection_1_IndexOf_m1640771987_gshared (Collection_1_t3188633884 * __this, RaycastHit_t4003175726  ___item0, const MethodInfo* method);
#define Collection_1_IndexOf_m1640771987(__this, ___item0, method) ((  int32_t (*) (Collection_1_t3188633884 *, RaycastHit_t4003175726 , const MethodInfo*))Collection_1_IndexOf_m1640771987_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::Insert(System.Int32,T)
extern "C"  void Collection_1_Insert_m3645270462_gshared (Collection_1_t3188633884 * __this, int32_t ___index0, RaycastHit_t4003175726  ___item1, const MethodInfo* method);
#define Collection_1_Insert_m3645270462(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3188633884 *, int32_t, RaycastHit_t4003175726 , const MethodInfo*))Collection_1_Insert_m3645270462_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::InsertItem(System.Int32,T)
extern "C"  void Collection_1_InsertItem_m4195780849_gshared (Collection_1_t3188633884 * __this, int32_t ___index0, RaycastHit_t4003175726  ___item1, const MethodInfo* method);
#define Collection_1_InsertItem_m4195780849(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3188633884 *, int32_t, RaycastHit_t4003175726 , const MethodInfo*))Collection_1_InsertItem_m4195780849_gshared)(__this, ___index0, ___item1, method)
// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::get_Items()
extern "C"  Il2CppObject* Collection_1_get_Items_m191301523_gshared (Collection_1_t3188633884 * __this, const MethodInfo* method);
#define Collection_1_get_Items_m191301523(__this, method) ((  Il2CppObject* (*) (Collection_1_t3188633884 *, const MethodInfo*))Collection_1_get_Items_m191301523_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::Remove(T)
extern "C"  bool Collection_1_Remove_m964513280_gshared (Collection_1_t3188633884 * __this, RaycastHit_t4003175726  ___item0, const MethodInfo* method);
#define Collection_1_Remove_m964513280(__this, ___item0, method) ((  bool (*) (Collection_1_t3188633884 *, RaycastHit_t4003175726 , const MethodInfo*))Collection_1_Remove_m964513280_gshared)(__this, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::RemoveAt(System.Int32)
extern "C"  void Collection_1_RemoveAt_m1519123332_gshared (Collection_1_t3188633884 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveAt_m1519123332(__this, ___index0, method) ((  void (*) (Collection_1_t3188633884 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m1519123332_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::RemoveItem(System.Int32)
extern "C"  void Collection_1_RemoveItem_m2488266404_gshared (Collection_1_t3188633884 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_RemoveItem_m2488266404(__this, ___index0, method) ((  void (*) (Collection_1_t3188633884 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m2488266404_gshared)(__this, ___index0, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::get_Count()
extern "C"  int32_t Collection_1_get_Count_m3436598856_gshared (Collection_1_t3188633884 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m3436598856(__this, method) ((  int32_t (*) (Collection_1_t3188633884 *, const MethodInfo*))Collection_1_get_Count_m3436598856_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::get_Item(System.Int32)
extern "C"  RaycastHit_t4003175726  Collection_1_get_Item_m1757151018_gshared (Collection_1_t3188633884 * __this, int32_t ___index0, const MethodInfo* method);
#define Collection_1_get_Item_m1757151018(__this, ___index0, method) ((  RaycastHit_t4003175726  (*) (Collection_1_t3188633884 *, int32_t, const MethodInfo*))Collection_1_get_Item_m1757151018_gshared)(__this, ___index0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::set_Item(System.Int32,T)
extern "C"  void Collection_1_set_Item_m221197077_gshared (Collection_1_t3188633884 * __this, int32_t ___index0, RaycastHit_t4003175726  ___value1, const MethodInfo* method);
#define Collection_1_set_Item_m221197077(__this, ___index0, ___value1, method) ((  void (*) (Collection_1_t3188633884 *, int32_t, RaycastHit_t4003175726 , const MethodInfo*))Collection_1_set_Item_m221197077_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::SetItem(System.Int32,T)
extern "C"  void Collection_1_SetItem_m2650824388_gshared (Collection_1_t3188633884 * __this, int32_t ___index0, RaycastHit_t4003175726  ___item1, const MethodInfo* method);
#define Collection_1_SetItem_m2650824388(__this, ___index0, ___item1, method) ((  void (*) (Collection_1_t3188633884 *, int32_t, RaycastHit_t4003175726 , const MethodInfo*))Collection_1_SetItem_m2650824388_gshared)(__this, ___index0, ___item1, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::IsValidItem(System.Object)
extern "C"  bool Collection_1_IsValidItem_m775884487_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_IsValidItem_m775884487(__this /* static, unused */, ___item0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_IsValidItem_m775884487_gshared)(__this /* static, unused */, ___item0, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::ConvertItem(System.Object)
extern "C"  RaycastHit_t4003175726  Collection_1_ConvertItem_m4063103241_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * ___item0, const MethodInfo* method);
#define Collection_1_ConvertItem_m4063103241(__this /* static, unused */, ___item0, method) ((  RaycastHit_t4003175726  (*) (Il2CppObject * /* static, unused */, Il2CppObject *, const MethodInfo*))Collection_1_ConvertItem_m4063103241_gshared)(__this /* static, unused */, ___item0, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C"  void Collection_1_CheckWritable_m247506631_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_CheckWritable_m247506631(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_CheckWritable_m247506631_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsSynchronized_m174829469_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsSynchronized_m174829469(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsSynchronized_m174829469_gshared)(__this /* static, unused */, ___list0, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.RaycastHit>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C"  bool Collection_1_IsFixedSize_m2338915362_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject* ___list0, const MethodInfo* method);
#define Collection_1_IsFixedSize_m2338915362(__this /* static, unused */, ___list0, method) ((  bool (*) (Il2CppObject * /* static, unused */, Il2CppObject*, const MethodInfo*))Collection_1_IsFixedSize_m2338915362_gshared)(__this /* static, unused */, ___list0, method)
