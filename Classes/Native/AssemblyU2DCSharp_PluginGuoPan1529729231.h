﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginGuoPan
struct  PluginGuoPan_t1529729231  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginGuoPan::userId
	String_t* ___userId_2;
	// System.String PluginGuoPan::token
	String_t* ___token_3;
	// System.String PluginGuoPan::configId
	String_t* ___configId_4;
	// System.String PluginGuoPan::aAppID
	String_t* ___aAppID_5;
	// System.String PluginGuoPan::aSkey
	String_t* ___aSkey_6;
	// System.Boolean PluginGuoPan::isEnterGame
	bool ___isEnterGame_7;
	// Mihua.SDK.PayInfo PluginGuoPan::iapInfo
	PayInfo_t1775308120 * ___iapInfo_8;

public:
	inline static int32_t get_offset_of_userId_2() { return static_cast<int32_t>(offsetof(PluginGuoPan_t1529729231, ___userId_2)); }
	inline String_t* get_userId_2() const { return ___userId_2; }
	inline String_t** get_address_of_userId_2() { return &___userId_2; }
	inline void set_userId_2(String_t* value)
	{
		___userId_2 = value;
		Il2CppCodeGenWriteBarrier(&___userId_2, value);
	}

	inline static int32_t get_offset_of_token_3() { return static_cast<int32_t>(offsetof(PluginGuoPan_t1529729231, ___token_3)); }
	inline String_t* get_token_3() const { return ___token_3; }
	inline String_t** get_address_of_token_3() { return &___token_3; }
	inline void set_token_3(String_t* value)
	{
		___token_3 = value;
		Il2CppCodeGenWriteBarrier(&___token_3, value);
	}

	inline static int32_t get_offset_of_configId_4() { return static_cast<int32_t>(offsetof(PluginGuoPan_t1529729231, ___configId_4)); }
	inline String_t* get_configId_4() const { return ___configId_4; }
	inline String_t** get_address_of_configId_4() { return &___configId_4; }
	inline void set_configId_4(String_t* value)
	{
		___configId_4 = value;
		Il2CppCodeGenWriteBarrier(&___configId_4, value);
	}

	inline static int32_t get_offset_of_aAppID_5() { return static_cast<int32_t>(offsetof(PluginGuoPan_t1529729231, ___aAppID_5)); }
	inline String_t* get_aAppID_5() const { return ___aAppID_5; }
	inline String_t** get_address_of_aAppID_5() { return &___aAppID_5; }
	inline void set_aAppID_5(String_t* value)
	{
		___aAppID_5 = value;
		Il2CppCodeGenWriteBarrier(&___aAppID_5, value);
	}

	inline static int32_t get_offset_of_aSkey_6() { return static_cast<int32_t>(offsetof(PluginGuoPan_t1529729231, ___aSkey_6)); }
	inline String_t* get_aSkey_6() const { return ___aSkey_6; }
	inline String_t** get_address_of_aSkey_6() { return &___aSkey_6; }
	inline void set_aSkey_6(String_t* value)
	{
		___aSkey_6 = value;
		Il2CppCodeGenWriteBarrier(&___aSkey_6, value);
	}

	inline static int32_t get_offset_of_isEnterGame_7() { return static_cast<int32_t>(offsetof(PluginGuoPan_t1529729231, ___isEnterGame_7)); }
	inline bool get_isEnterGame_7() const { return ___isEnterGame_7; }
	inline bool* get_address_of_isEnterGame_7() { return &___isEnterGame_7; }
	inline void set_isEnterGame_7(bool value)
	{
		___isEnterGame_7 = value;
	}

	inline static int32_t get_offset_of_iapInfo_8() { return static_cast<int32_t>(offsetof(PluginGuoPan_t1529729231, ___iapInfo_8)); }
	inline PayInfo_t1775308120 * get_iapInfo_8() const { return ___iapInfo_8; }
	inline PayInfo_t1775308120 ** get_address_of_iapInfo_8() { return &___iapInfo_8; }
	inline void set_iapInfo_8(PayInfo_t1775308120 * value)
	{
		___iapInfo_8 = value;
		Il2CppCodeGenWriteBarrier(&___iapInfo_8, value);
	}
};

struct PluginGuoPan_t1529729231_StaticFields
{
public:
	// System.Action PluginGuoPan::<>f__am$cache7
	Action_t3771233898 * ___U3CU3Ef__amU24cache7_9;
	// System.Action PluginGuoPan::<>f__am$cache8
	Action_t3771233898 * ___U3CU3Ef__amU24cache8_10;
	// System.Action PluginGuoPan::<>f__am$cache9
	Action_t3771233898 * ___U3CU3Ef__amU24cache9_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_9() { return static_cast<int32_t>(offsetof(PluginGuoPan_t1529729231_StaticFields, ___U3CU3Ef__amU24cache7_9)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache7_9() const { return ___U3CU3Ef__amU24cache7_9; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache7_9() { return &___U3CU3Ef__amU24cache7_9; }
	inline void set_U3CU3Ef__amU24cache7_9(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache7_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_9, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_10() { return static_cast<int32_t>(offsetof(PluginGuoPan_t1529729231_StaticFields, ___U3CU3Ef__amU24cache8_10)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache8_10() const { return ___U3CU3Ef__amU24cache8_10; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache8_10() { return &___U3CU3Ef__amU24cache8_10; }
	inline void set_U3CU3Ef__amU24cache8_10(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache8_10 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache8_10, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_11() { return static_cast<int32_t>(offsetof(PluginGuoPan_t1529729231_StaticFields, ___U3CU3Ef__amU24cache9_11)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache9_11() const { return ___U3CU3Ef__amU24cache9_11; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache9_11() { return &___U3CU3Ef__amU24cache9_11; }
	inline void set_U3CU3Ef__amU24cache9_11(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache9_11 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache9_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
