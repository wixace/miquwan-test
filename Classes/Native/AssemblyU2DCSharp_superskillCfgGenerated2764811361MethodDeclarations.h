﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// superskillCfgGenerated
struct superskillCfgGenerated_t2764811361;
// JSVCall
struct JSVCall_t3708497963;
// System.String
struct String_t;
// superskillCfg
struct superskillCfg_t3024131278;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_superskillCfg3024131278.h"

// System.Void superskillCfgGenerated::.ctor()
extern "C"  void superskillCfgGenerated__ctor_m375776602 (superskillCfgGenerated_t2764811361 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean superskillCfgGenerated::superskillCfg_superskillCfg1(JSVCall,System.Int32)
extern "C"  bool superskillCfgGenerated_superskillCfg_superskillCfg1_m3339917954 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_id(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_id_m1123480241 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_name(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_name_m1261322689 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_icon(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_icon_m1918946195 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_btnicon(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_btnicon_m3315850283 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_flashicon(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_flashicon_m502140055 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_rageres(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_rageres_m3024430221 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_btnres(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_btnres_m635952680 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_type(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_type_m1503003122 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_skillid(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_skillid_m919942068 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_upnumber(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_upnumber_m1910946760 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_endskilltime(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_endskilltime_m3556718377 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_endskillid(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_endskillid_m657352859 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_captainendskillid(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_captainendskillid_m549251179 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_ui_mask(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_ui_mask_m2355771785 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_ui_effectid(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_ui_effectid_m2508831369 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_timeA(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_timeA_m3709683532 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_damageA(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_damageA_m3833032430 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_timeB(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_timeB_m3513170027 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_damageB(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_damageB_m3636518925 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_skillsection(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_skillsection_m3971794168 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_skillreducetime(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_skillreducetime_m339721628 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_skillgrade(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_skillgrade_m1702901254 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_skillonetime(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_skillonetime_m2458467690 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_leasttime(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_leasttime_m3364258538 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_hidetime(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_hidetime_m2293640893 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::superskillCfg_superskill_prompt(JSVCall)
extern "C"  void superskillCfgGenerated_superskillCfg_superskill_prompt_m1589432691 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean superskillCfgGenerated::superskillCfg_Init__DictionaryT2_String_String(JSVCall,System.Int32)
extern "C"  bool superskillCfgGenerated_superskillCfg_Init__DictionaryT2_String_String_m1698316483 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::__Register()
extern "C"  void superskillCfgGenerated___Register_m3853967277 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 superskillCfgGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t superskillCfgGenerated_ilo_getObject1_m501748600 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String superskillCfgGenerated::ilo_getStringS2(System.Int32)
extern "C"  String_t* superskillCfgGenerated_ilo_getStringS2_m2996936473 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::ilo_setStringS3(System.Int32,System.String)
extern "C"  void superskillCfgGenerated_ilo_setStringS3_m1102044197 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::ilo_setInt324(System.Int32,System.Int32)
extern "C"  void superskillCfgGenerated_ilo_setInt324_m4294465497 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean superskillCfgGenerated::ilo_getBooleanS5(System.Int32)
extern "C"  bool superskillCfgGenerated_ilo_getBooleanS5_m128794678 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::ilo_setSingle6(System.Int32,System.Single)
extern "C"  void superskillCfgGenerated_ilo_setSingle6_m3797441375 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single superskillCfgGenerated::ilo_getSingle7(System.Int32)
extern "C"  float superskillCfgGenerated_ilo_getSingle7_m1297224851 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void superskillCfgGenerated::ilo_Init8(superskillCfg,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void superskillCfgGenerated_ilo_Init8_m3915728864 (Il2CppObject * __this /* static, unused */, superskillCfg_t3024131278 * ____this0, Dictionary_2_t827649927 * ____info1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
