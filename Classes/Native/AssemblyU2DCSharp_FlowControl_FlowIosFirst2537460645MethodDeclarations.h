﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FlowControl.FlowIosFirst
struct FlowIosFirst_t2537460645;
// System.String
struct String_t;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// Mihua.Net.UrlLoader
struct UrlLoader_t2490729496;
// FlowControl.FlowBase
struct FlowBase_t3680091731;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Mihua_Net_UrlLoader2490729496.h"
#include "AssemblyU2DCSharp_FlowControl_FlowBase3680091731.h"

// System.Void FlowControl.FlowIosFirst::.ctor()
extern "C"  void FlowIosFirst__ctor_m2270567211 (FlowIosFirst_t2537460645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FlowControl.FlowIosFirst::mh_get_user_language_locale()
extern "C"  String_t* FlowIosFirst_mh_get_user_language_locale_m1296862848 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowIosFirst::Activate()
extern "C"  void FlowIosFirst_Activate_m728484076 (FlowIosFirst_t2537460645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowIosFirst::OnLoginServer(CEvent.ZEvent)
extern "C"  void FlowIosFirst_OnLoginServer_m1246276229 (FlowIosFirst_t2537460645 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowIosFirst::FlowUpdate(System.Single)
extern "C"  void FlowIosFirst_FlowUpdate_m1124111131 (FlowIosFirst_t2537460645 * __this, float ___duringTime0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowIosFirst::Request()
extern "C"  void FlowIosFirst_Request_m513038616 (FlowIosFirst_t2537460645 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FlowControl.FlowIosFirst::ilo_mh_get_user_language_locale1()
extern "C"  String_t* FlowIosFirst_ilo_mh_get_user_language_locale1_m1227575494 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowIosFirst::ilo_AddEventListener2(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void FlowIosFirst_ilo_AddEventListener2_m144425608 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String FlowControl.FlowIosFirst::ilo_get_error3(Mihua.Net.UrlLoader)
extern "C"  String_t* FlowIosFirst_ilo_get_error3_m142164532 (Il2CppObject * __this /* static, unused */, UrlLoader_t2490729496 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FlowControl.FlowIosFirst::ilo_set_isComplete4(FlowControl.FlowBase,System.Boolean)
extern "C"  void FlowIosFirst_ilo_set_isComplete4_m1556778385 (Il2CppObject * __this /* static, unused */, FlowBase_t3680091731 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
