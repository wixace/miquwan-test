﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.Serialization.AstarSerializer/<SerializeExtraInfo>c__AnonStorey10E
struct U3CSerializeExtraInfoU3Ec__AnonStorey10E_t889342229;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"

// System.Void Pathfinding.Serialization.AstarSerializer/<SerializeExtraInfo>c__AnonStorey10E::.ctor()
extern "C"  void U3CSerializeExtraInfoU3Ec__AnonStorey10E__ctor_m4235143974 (U3CSerializeExtraInfoU3Ec__AnonStorey10E_t889342229 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.Serialization.AstarSerializer/<SerializeExtraInfo>c__AnonStorey10E::<>m__341(Pathfinding.GraphNode)
extern "C"  bool U3CSerializeExtraInfoU3Ec__AnonStorey10E_U3CU3Em__341_m1045949525 (U3CSerializeExtraInfoU3Ec__AnonStorey10E_t889342229 * __this, GraphNode_t23612370 * ___node0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
