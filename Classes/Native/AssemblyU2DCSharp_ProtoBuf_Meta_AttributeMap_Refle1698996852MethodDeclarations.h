﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Meta.AttributeMap/ReflectionAttributeMap
struct ReflectionAttributeMap_t1698996852;
// System.Attribute
struct Attribute_t2523058482;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Attribute2523058482.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void ProtoBuf.Meta.AttributeMap/ReflectionAttributeMap::.ctor(System.Attribute)
extern "C"  void ReflectionAttributeMap__ctor_m229121914 (ReflectionAttributeMap_t1698996852 * __this, Attribute_t2523058482 * ___attribute0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Meta.AttributeMap/ReflectionAttributeMap::get_Target()
extern "C"  Il2CppObject * ReflectionAttributeMap_get_Target_m2222217394 (ReflectionAttributeMap_t1698996852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Meta.AttributeMap/ReflectionAttributeMap::get_AttributeType()
extern "C"  Type_t * ReflectionAttributeMap_get_AttributeType_m2936488892 (ReflectionAttributeMap_t1698996852 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Meta.AttributeMap/ReflectionAttributeMap::TryGet(System.String,System.Boolean,System.Object&)
extern "C"  bool ReflectionAttributeMap_TryGet_m587347215 (ReflectionAttributeMap_t1698996852 * __this, String_t* ___key0, bool ___publicOnly1, Il2CppObject ** ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
