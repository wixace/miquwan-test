﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginTlog
struct PluginTlog_t1606437955;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// Newtonsoft.Json.Linq.JObject
struct JObject_t1798544199;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// Newtonsoft.Json.Linq.JToken
struct JToken_t3412245951;
// System.Object
struct Il2CppObject;
// TimeMgr
struct TimeMgr_t350708715;
// PluginsSdkMgr
struct PluginsSdkMgr_t3884624670;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_PluginTlog1606437955.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JObject1798544199.h"
#include "AssemblyU2DCSharp_Newtonsoft_Json_Linq_JToken3412245951.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_TimeMgr350708715.h"

// System.Void PluginTlog::.ctor()
extern "C"  void PluginTlog__ctor_m2967037880 (PluginTlog_t1606437955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTlog::Init()
extern "C"  void PluginTlog_Init_m4065120156 (PluginTlog_t1606437955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTlog::OnInit(CEvent.ZEvent)
extern "C"  void PluginTlog_OnInit_m1080072384 (PluginTlog_t1606437955 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTlog::OnRoleProperty(CEvent.ZEvent)
extern "C"  void PluginTlog_OnRoleProperty_m1587870213 (PluginTlog_t1606437955 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTlog::OnTask(CEvent.ZEvent)
extern "C"  void PluginTlog_OnTask_m1405890475 (PluginTlog_t1606437955 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTlog::OnHonor(CEvent.ZEvent)
extern "C"  void PluginTlog_OnHonor_m2243279738 (PluginTlog_t1606437955 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject PluginTlog::GetJsonByid(System.String)
extern "C"  JObject_t1798544199 * PluginTlog_GetJsonByid_m2024155049 (PluginTlog_t1606437955 * __this, String_t* ___eventID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PluginTlog::SendLog(System.String)
extern "C"  Il2CppObject * PluginTlog_SendLog_m3219589464 (PluginTlog_t1606437955 * __this, String_t* ___jsonStr0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTlog::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void PluginTlog_ilo_AddEventListener1_m832860732 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Newtonsoft.Json.Linq.JObject PluginTlog::ilo_GetJsonByid2(PluginTlog,System.String)
extern "C"  JObject_t1798544199 * PluginTlog_ilo_GetJsonByid2_m616554631 (Il2CppObject * __this /* static, unused */, PluginTlog_t1606437955 * ____this0, String_t* ___eventID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTlog::ilo_Add3(Newtonsoft.Json.Linq.JObject,System.String,Newtonsoft.Json.Linq.JToken)
extern "C"  void PluginTlog_ilo_Add3_m3503173313 (Il2CppObject * __this /* static, unused */, JObject_t1798544199 * ____this0, String_t* ___propertyName1, JToken_t3412245951 * ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String PluginTlog::ilo_stringify4(System.Object)
extern "C"  String_t* PluginTlog_ilo_stringify4_m1292896105 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PluginTlog::ilo_SendLog5(PluginTlog,System.String)
extern "C"  Il2CppObject * PluginTlog_ilo_SendLog5_m1770443257 (Il2CppObject * __this /* static, unused */, PluginTlog_t1606437955 * ____this0, String_t* ___jsonStr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// TimeMgr PluginTlog::ilo_get_TimeMgr6()
extern "C"  TimeMgr_t350708715 * PluginTlog_ilo_get_TimeMgr6_m853307099 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Double PluginTlog::ilo_get_serverTime7(TimeMgr)
extern "C"  double PluginTlog_ilo_get_serverTime7_m3898152233 (Il2CppObject * __this /* static, unused */, TimeMgr_t350708715 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// PluginsSdkMgr PluginTlog::ilo_get_Instance8()
extern "C"  PluginsSdkMgr_t3884624670 * PluginTlog_ilo_get_Instance8_m2471905480 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
