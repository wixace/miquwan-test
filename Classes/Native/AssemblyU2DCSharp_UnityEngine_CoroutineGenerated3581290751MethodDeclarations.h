﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_CoroutineGenerated
struct UnityEngine_CoroutineGenerated_t3581290751;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine_CoroutineGenerated::.ctor()
extern "C"  void UnityEngine_CoroutineGenerated__ctor_m488186492 (UnityEngine_CoroutineGenerated_t3581290751 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_CoroutineGenerated::__Register()
extern "C"  void UnityEngine_CoroutineGenerated___Register_m1753647051 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
