﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_T3174675725MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.AnimationClip,System.Boolean,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m2325255725(__this, ___object0, ___method1, method) ((  void (*) (Transform_1_t1654179052 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m199821900_gshared)(__this, ___object0, ___method1, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.AnimationClip,System.Boolean,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m135032879(__this, ___key0, ___value1, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t1654179052 *, AnimationClip_t2007702890 *, bool, const MethodInfo*))Transform_1_Invoke_m3999618288_gshared)(__this, ___key0, ___value1, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.AnimationClip,System.Boolean,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m2330583822(__this, ___key0, ___value1, ___callback2, ___object3, method) ((  Il2CppObject * (*) (Transform_1_t1654179052 *, AnimationClip_t2007702890 *, bool, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Transform_1_BeginInvoke_m960240079_gshared)(__this, ___key0, ___value1, ___callback2, ___object3, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.AnimationClip,System.Boolean,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m3843394811(__this, ___result0, method) ((  DictionaryEntry_t1751606614  (*) (Transform_1_t1654179052 *, Il2CppObject *, const MethodInfo*))Transform_1_EndInvoke_m468964762_gshared)(__this, ___result0, method)
