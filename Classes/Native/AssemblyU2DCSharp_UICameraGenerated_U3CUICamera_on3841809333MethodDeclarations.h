﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICameraGenerated/<UICamera_onDragOver_GetDelegate_member50_arg0>c__AnonStoreyAB
struct U3CUICamera_onDragOver_GetDelegate_member50_arg0U3Ec__AnonStoreyAB_t3841809333;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UICameraGenerated/<UICamera_onDragOver_GetDelegate_member50_arg0>c__AnonStoreyAB::.ctor()
extern "C"  void U3CUICamera_onDragOver_GetDelegate_member50_arg0U3Ec__AnonStoreyAB__ctor_m1226729094 (U3CUICamera_onDragOver_GetDelegate_member50_arg0U3Ec__AnonStoreyAB_t3841809333 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated/<UICamera_onDragOver_GetDelegate_member50_arg0>c__AnonStoreyAB::<>m__11A(UnityEngine.GameObject,UnityEngine.GameObject)
extern "C"  void U3CUICamera_onDragOver_GetDelegate_member50_arg0U3Ec__AnonStoreyAB_U3CU3Em__11A_m495150002 (U3CUICamera_onDragOver_GetDelegate_member50_arg0U3Ec__AnonStoreyAB_t3841809333 * __this, GameObject_t3674682005 * ___go0, GameObject_t3674682005 * ___obj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
