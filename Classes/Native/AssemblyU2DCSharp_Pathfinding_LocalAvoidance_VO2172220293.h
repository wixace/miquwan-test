﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair>
struct List_1_t4189505471;

#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.LocalAvoidance/VO
struct  VO_t2172220293  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 Pathfinding.LocalAvoidance/VO::origin
	Vector3_t4282066566  ___origin_0;
	// UnityEngine.Vector3 Pathfinding.LocalAvoidance/VO::direction
	Vector3_t4282066566  ___direction_1;
	// System.Single Pathfinding.LocalAvoidance/VO::angle
	float ___angle_2;
	// System.Single Pathfinding.LocalAvoidance/VO::limit
	float ___limit_3;
	// UnityEngine.Vector3 Pathfinding.LocalAvoidance/VO::pLeft
	Vector3_t4282066566  ___pLeft_4;
	// UnityEngine.Vector3 Pathfinding.LocalAvoidance/VO::pRight
	Vector3_t4282066566  ___pRight_5;
	// UnityEngine.Vector3 Pathfinding.LocalAvoidance/VO::nLeft
	Vector3_t4282066566  ___nLeft_6;
	// UnityEngine.Vector3 Pathfinding.LocalAvoidance/VO::nRight
	Vector3_t4282066566  ___nRight_7;
	// System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair> Pathfinding.LocalAvoidance/VO::ints1
	List_1_t4189505471 * ___ints1_8;
	// System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair> Pathfinding.LocalAvoidance/VO::ints2
	List_1_t4189505471 * ___ints2_9;
	// System.Collections.Generic.List`1<Pathfinding.LocalAvoidance/IntersectionPair> Pathfinding.LocalAvoidance/VO::ints3
	List_1_t4189505471 * ___ints3_10;

public:
	inline static int32_t get_offset_of_origin_0() { return static_cast<int32_t>(offsetof(VO_t2172220293, ___origin_0)); }
	inline Vector3_t4282066566  get_origin_0() const { return ___origin_0; }
	inline Vector3_t4282066566 * get_address_of_origin_0() { return &___origin_0; }
	inline void set_origin_0(Vector3_t4282066566  value)
	{
		___origin_0 = value;
	}

	inline static int32_t get_offset_of_direction_1() { return static_cast<int32_t>(offsetof(VO_t2172220293, ___direction_1)); }
	inline Vector3_t4282066566  get_direction_1() const { return ___direction_1; }
	inline Vector3_t4282066566 * get_address_of_direction_1() { return &___direction_1; }
	inline void set_direction_1(Vector3_t4282066566  value)
	{
		___direction_1 = value;
	}

	inline static int32_t get_offset_of_angle_2() { return static_cast<int32_t>(offsetof(VO_t2172220293, ___angle_2)); }
	inline float get_angle_2() const { return ___angle_2; }
	inline float* get_address_of_angle_2() { return &___angle_2; }
	inline void set_angle_2(float value)
	{
		___angle_2 = value;
	}

	inline static int32_t get_offset_of_limit_3() { return static_cast<int32_t>(offsetof(VO_t2172220293, ___limit_3)); }
	inline float get_limit_3() const { return ___limit_3; }
	inline float* get_address_of_limit_3() { return &___limit_3; }
	inline void set_limit_3(float value)
	{
		___limit_3 = value;
	}

	inline static int32_t get_offset_of_pLeft_4() { return static_cast<int32_t>(offsetof(VO_t2172220293, ___pLeft_4)); }
	inline Vector3_t4282066566  get_pLeft_4() const { return ___pLeft_4; }
	inline Vector3_t4282066566 * get_address_of_pLeft_4() { return &___pLeft_4; }
	inline void set_pLeft_4(Vector3_t4282066566  value)
	{
		___pLeft_4 = value;
	}

	inline static int32_t get_offset_of_pRight_5() { return static_cast<int32_t>(offsetof(VO_t2172220293, ___pRight_5)); }
	inline Vector3_t4282066566  get_pRight_5() const { return ___pRight_5; }
	inline Vector3_t4282066566 * get_address_of_pRight_5() { return &___pRight_5; }
	inline void set_pRight_5(Vector3_t4282066566  value)
	{
		___pRight_5 = value;
	}

	inline static int32_t get_offset_of_nLeft_6() { return static_cast<int32_t>(offsetof(VO_t2172220293, ___nLeft_6)); }
	inline Vector3_t4282066566  get_nLeft_6() const { return ___nLeft_6; }
	inline Vector3_t4282066566 * get_address_of_nLeft_6() { return &___nLeft_6; }
	inline void set_nLeft_6(Vector3_t4282066566  value)
	{
		___nLeft_6 = value;
	}

	inline static int32_t get_offset_of_nRight_7() { return static_cast<int32_t>(offsetof(VO_t2172220293, ___nRight_7)); }
	inline Vector3_t4282066566  get_nRight_7() const { return ___nRight_7; }
	inline Vector3_t4282066566 * get_address_of_nRight_7() { return &___nRight_7; }
	inline void set_nRight_7(Vector3_t4282066566  value)
	{
		___nRight_7 = value;
	}

	inline static int32_t get_offset_of_ints1_8() { return static_cast<int32_t>(offsetof(VO_t2172220293, ___ints1_8)); }
	inline List_1_t4189505471 * get_ints1_8() const { return ___ints1_8; }
	inline List_1_t4189505471 ** get_address_of_ints1_8() { return &___ints1_8; }
	inline void set_ints1_8(List_1_t4189505471 * value)
	{
		___ints1_8 = value;
		Il2CppCodeGenWriteBarrier(&___ints1_8, value);
	}

	inline static int32_t get_offset_of_ints2_9() { return static_cast<int32_t>(offsetof(VO_t2172220293, ___ints2_9)); }
	inline List_1_t4189505471 * get_ints2_9() const { return ___ints2_9; }
	inline List_1_t4189505471 ** get_address_of_ints2_9() { return &___ints2_9; }
	inline void set_ints2_9(List_1_t4189505471 * value)
	{
		___ints2_9 = value;
		Il2CppCodeGenWriteBarrier(&___ints2_9, value);
	}

	inline static int32_t get_offset_of_ints3_10() { return static_cast<int32_t>(offsetof(VO_t2172220293, ___ints3_10)); }
	inline List_1_t4189505471 * get_ints3_10() const { return ___ints3_10; }
	inline List_1_t4189505471 ** get_address_of_ints3_10() { return &___ints3_10; }
	inline void set_ints3_10(List_1_t4189505471 * value)
	{
		___ints3_10 = value;
		Il2CppCodeGenWriteBarrier(&___ints3_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
