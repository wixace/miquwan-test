﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>
struct List_1_t399344435;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.Util.ListPool`1<Pathfinding.ClipperLib.IntPoint>::.cctor()
extern "C"  void ListPool_1__cctor_m2090068626_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1__cctor_m2090068626(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m2090068626_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<Pathfinding.ClipperLib.IntPoint>::Claim()
extern "C"  List_1_t399344435 * ListPool_1_Claim_m3142459616_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_Claim_m3142459616(__this /* static, unused */, method) ((  List_1_t399344435 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_Claim_m3142459616_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<Pathfinding.ClipperLib.IntPoint>::Claim(System.Int32)
extern "C"  List_1_t399344435 * ListPool_1_Claim_m1615955505_gshared (Il2CppObject * __this /* static, unused */, int32_t ___capacity0, const MethodInfo* method);
#define ListPool_1_Claim_m1615955505(__this /* static, unused */, ___capacity0, method) ((  List_1_t399344435 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))ListPool_1_Claim_m1615955505_gshared)(__this /* static, unused */, ___capacity0, method)
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.ClipperLib.IntPoint>::Warmup(System.Int32,System.Int32)
extern "C"  void ListPool_1_Warmup_m3246261245_gshared (Il2CppObject * __this /* static, unused */, int32_t ___count0, int32_t ___size1, const MethodInfo* method);
#define ListPool_1_Warmup_m3246261245(__this /* static, unused */, ___count0, ___size1, method) ((  void (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))ListPool_1_Warmup_m3246261245_gshared)(__this /* static, unused */, ___count0, ___size1, method)
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.ClipperLib.IntPoint>::Release(System.Collections.Generic.List`1<T>)
extern "C"  void ListPool_1_Release_m837364280_gshared (Il2CppObject * __this /* static, unused */, List_1_t399344435 * ___list0, const MethodInfo* method);
#define ListPool_1_Release_m837364280(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, List_1_t399344435 *, const MethodInfo*))ListPool_1_Release_m837364280_gshared)(__this /* static, unused */, ___list0, method)
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.ClipperLib.IntPoint>::Clear()
extern "C"  void ListPool_1_Clear_m1506978982_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_Clear_m1506978982(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_Clear_m1506978982_gshared)(__this /* static, unused */, method)
// System.Int32 Pathfinding.Util.ListPool`1<Pathfinding.ClipperLib.IntPoint>::GetSize()
extern "C"  int32_t ListPool_1_GetSize_m1975326850_gshared (Il2CppObject * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_GetSize_m1975326850(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_GetSize_m1975326850_gshared)(__this /* static, unused */, method)
