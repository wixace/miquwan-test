﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_PlayerPrefsGenerated
struct UnityEngine_PlayerPrefsGenerated_t2729421672;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_PlayerPrefsGenerated::.ctor()
extern "C"  void UnityEngine_PlayerPrefsGenerated__ctor_m2044502067 (UnityEngine_PlayerPrefsGenerated_t2729421672 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlayerPrefsGenerated::PlayerPrefs_PlayerPrefs1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlayerPrefsGenerated_PlayerPrefs_PlayerPrefs1_m1304773575 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlayerPrefsGenerated::PlayerPrefs_DeleteAll(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlayerPrefsGenerated_PlayerPrefs_DeleteAll_m2799595155 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlayerPrefsGenerated::PlayerPrefs_DeleteKey__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlayerPrefsGenerated_PlayerPrefs_DeleteKey__String_m1398320674 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlayerPrefsGenerated::PlayerPrefs_GetFloat__String__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlayerPrefsGenerated_PlayerPrefs_GetFloat__String__Single_m2544143428 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlayerPrefsGenerated::PlayerPrefs_GetFloat__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlayerPrefsGenerated_PlayerPrefs_GetFloat__String_m3696785660 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlayerPrefsGenerated::PlayerPrefs_GetInt__String__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlayerPrefsGenerated_PlayerPrefs_GetInt__String__Int32_m3401862561 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlayerPrefsGenerated::PlayerPrefs_GetInt__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlayerPrefsGenerated_PlayerPrefs_GetInt__String_m831480527 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlayerPrefsGenerated::PlayerPrefs_GetString__String__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlayerPrefsGenerated_PlayerPrefs_GetString__String__String_m2552896134 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlayerPrefsGenerated::PlayerPrefs_GetString__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlayerPrefsGenerated_PlayerPrefs_GetString__String_m1247895861 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlayerPrefsGenerated::PlayerPrefs_HasKey__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlayerPrefsGenerated_PlayerPrefs_HasKey__String_m1742011003 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlayerPrefsGenerated::PlayerPrefs_Save(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlayerPrefsGenerated_PlayerPrefs_Save_m2273484098 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlayerPrefsGenerated::PlayerPrefs_SetFloat__String__Single(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlayerPrefsGenerated_PlayerPrefs_SetFloat__String__Single_m1228877752 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlayerPrefsGenerated::PlayerPrefs_SetInt__String__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlayerPrefsGenerated_PlayerPrefs_SetInt__String__Int32_m2312470189 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_PlayerPrefsGenerated::PlayerPrefs_SetString__String__String(JSVCall,System.Int32)
extern "C"  bool UnityEngine_PlayerPrefsGenerated_PlayerPrefs_SetString__String__String_m434365842 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PlayerPrefsGenerated::__Register()
extern "C"  void UnityEngine_PlayerPrefsGenerated___Register_m330302580 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PlayerPrefsGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_PlayerPrefsGenerated_ilo_addJSCSRel1_m3624622447 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine_PlayerPrefsGenerated::ilo_getStringS2(System.Int32)
extern "C"  String_t* UnityEngine_PlayerPrefsGenerated_ilo_getStringS2_m3180165362 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_PlayerPrefsGenerated::ilo_getSingle3(System.Int32)
extern "C"  float UnityEngine_PlayerPrefsGenerated_ilo_getSingle3_m3943043926 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_PlayerPrefsGenerated::ilo_setInt324(System.Int32,System.Int32)
extern "C"  void UnityEngine_PlayerPrefsGenerated_ilo_setInt324_m1202592800 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_PlayerPrefsGenerated::ilo_getInt325(System.Int32)
extern "C"  int32_t UnityEngine_PlayerPrefsGenerated_ilo_getInt325_m2988265982 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
