﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Client
struct Client_t2021122027;
// Pomelo.DotNetClient.PomeloClient
struct PomeloClient_t4215271137;
// System.Action
struct Action_t3771233898;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pomelo_DotNetClient_NetWorkState3329123775.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Client
struct  Client_t2021122027  : public Il2CppObject
{
public:
	// Pomelo.DotNetClient.PomeloClient Client::pc
	PomeloClient_t4215271137 * ___pc_3;
	// Pomelo.DotNetClient.NetWorkState Client::state
	int32_t ___state_4;

public:
	inline static int32_t get_offset_of_pc_3() { return static_cast<int32_t>(offsetof(Client_t2021122027, ___pc_3)); }
	inline PomeloClient_t4215271137 * get_pc_3() const { return ___pc_3; }
	inline PomeloClient_t4215271137 ** get_address_of_pc_3() { return &___pc_3; }
	inline void set_pc_3(PomeloClient_t4215271137 * value)
	{
		___pc_3 = value;
		Il2CppCodeGenWriteBarrier(&___pc_3, value);
	}

	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(Client_t2021122027, ___state_4)); }
	inline int32_t get_state_4() const { return ___state_4; }
	inline int32_t* get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(int32_t value)
	{
		___state_4 = value;
	}
};

struct Client_t2021122027_StaticFields
{
public:
	// Client Client::instance
	Client_t2021122027 * ___instance_2;
	// System.Action Client::<>f__am$cache3
	Action_t3771233898 * ___U3CU3Ef__amU24cache3_5;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(Client_t2021122027_StaticFields, ___instance_2)); }
	inline Client_t2021122027 * get_instance_2() const { return ___instance_2; }
	inline Client_t2021122027 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(Client_t2021122027 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___instance_2, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_5() { return static_cast<int32_t>(offsetof(Client_t2021122027_StaticFields, ___U3CU3Ef__amU24cache3_5)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache3_5() const { return ___U3CU3Ef__amU24cache3_5; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache3_5() { return &___U3CU3Ef__amU24cache3_5; }
	inline void set_U3CU3Ef__amU24cache3_5(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache3_5 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache3_5, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
