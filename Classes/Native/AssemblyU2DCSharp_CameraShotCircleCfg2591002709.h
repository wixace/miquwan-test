﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// ProtoBuf.IExtension
struct IExtension_t1606339106;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraShotCircleCfg
struct  CameraShotCircleCfg_t2591002709  : public Il2CppObject
{
public:
	// System.Int32 CameraShotCircleCfg::_isCircle
	int32_t ____isCircle_0;
	// System.Int32 CameraShotCircleCfg::_heroOrNpcId
	int32_t ____heroOrNpcId_1;
	// System.Int32 CameraShotCircleCfg::_isHero
	int32_t ____isHero_2;
	// System.Single CameraShotCircleCfg::_posX
	float ____posX_3;
	// System.Single CameraShotCircleCfg::_posY
	float ____posY_4;
	// System.Single CameraShotCircleCfg::_posZ
	float ____posZ_5;
	// System.Single CameraShotCircleCfg::_speed
	float ____speed_6;
	// System.Single CameraShotCircleCfg::_distance
	float ____distance_7;
	// System.Single CameraShotCircleCfg::_high
	float ____high_8;
	// System.Single CameraShotCircleCfg::_angle
	float ____angle_9;
	// ProtoBuf.IExtension CameraShotCircleCfg::extensionObject
	Il2CppObject * ___extensionObject_10;

public:
	inline static int32_t get_offset_of__isCircle_0() { return static_cast<int32_t>(offsetof(CameraShotCircleCfg_t2591002709, ____isCircle_0)); }
	inline int32_t get__isCircle_0() const { return ____isCircle_0; }
	inline int32_t* get_address_of__isCircle_0() { return &____isCircle_0; }
	inline void set__isCircle_0(int32_t value)
	{
		____isCircle_0 = value;
	}

	inline static int32_t get_offset_of__heroOrNpcId_1() { return static_cast<int32_t>(offsetof(CameraShotCircleCfg_t2591002709, ____heroOrNpcId_1)); }
	inline int32_t get__heroOrNpcId_1() const { return ____heroOrNpcId_1; }
	inline int32_t* get_address_of__heroOrNpcId_1() { return &____heroOrNpcId_1; }
	inline void set__heroOrNpcId_1(int32_t value)
	{
		____heroOrNpcId_1 = value;
	}

	inline static int32_t get_offset_of__isHero_2() { return static_cast<int32_t>(offsetof(CameraShotCircleCfg_t2591002709, ____isHero_2)); }
	inline int32_t get__isHero_2() const { return ____isHero_2; }
	inline int32_t* get_address_of__isHero_2() { return &____isHero_2; }
	inline void set__isHero_2(int32_t value)
	{
		____isHero_2 = value;
	}

	inline static int32_t get_offset_of__posX_3() { return static_cast<int32_t>(offsetof(CameraShotCircleCfg_t2591002709, ____posX_3)); }
	inline float get__posX_3() const { return ____posX_3; }
	inline float* get_address_of__posX_3() { return &____posX_3; }
	inline void set__posX_3(float value)
	{
		____posX_3 = value;
	}

	inline static int32_t get_offset_of__posY_4() { return static_cast<int32_t>(offsetof(CameraShotCircleCfg_t2591002709, ____posY_4)); }
	inline float get__posY_4() const { return ____posY_4; }
	inline float* get_address_of__posY_4() { return &____posY_4; }
	inline void set__posY_4(float value)
	{
		____posY_4 = value;
	}

	inline static int32_t get_offset_of__posZ_5() { return static_cast<int32_t>(offsetof(CameraShotCircleCfg_t2591002709, ____posZ_5)); }
	inline float get__posZ_5() const { return ____posZ_5; }
	inline float* get_address_of__posZ_5() { return &____posZ_5; }
	inline void set__posZ_5(float value)
	{
		____posZ_5 = value;
	}

	inline static int32_t get_offset_of__speed_6() { return static_cast<int32_t>(offsetof(CameraShotCircleCfg_t2591002709, ____speed_6)); }
	inline float get__speed_6() const { return ____speed_6; }
	inline float* get_address_of__speed_6() { return &____speed_6; }
	inline void set__speed_6(float value)
	{
		____speed_6 = value;
	}

	inline static int32_t get_offset_of__distance_7() { return static_cast<int32_t>(offsetof(CameraShotCircleCfg_t2591002709, ____distance_7)); }
	inline float get__distance_7() const { return ____distance_7; }
	inline float* get_address_of__distance_7() { return &____distance_7; }
	inline void set__distance_7(float value)
	{
		____distance_7 = value;
	}

	inline static int32_t get_offset_of__high_8() { return static_cast<int32_t>(offsetof(CameraShotCircleCfg_t2591002709, ____high_8)); }
	inline float get__high_8() const { return ____high_8; }
	inline float* get_address_of__high_8() { return &____high_8; }
	inline void set__high_8(float value)
	{
		____high_8 = value;
	}

	inline static int32_t get_offset_of__angle_9() { return static_cast<int32_t>(offsetof(CameraShotCircleCfg_t2591002709, ____angle_9)); }
	inline float get__angle_9() const { return ____angle_9; }
	inline float* get_address_of__angle_9() { return &____angle_9; }
	inline void set__angle_9(float value)
	{
		____angle_9 = value;
	}

	inline static int32_t get_offset_of_extensionObject_10() { return static_cast<int32_t>(offsetof(CameraShotCircleCfg_t2591002709, ___extensionObject_10)); }
	inline Il2CppObject * get_extensionObject_10() const { return ___extensionObject_10; }
	inline Il2CppObject ** get_address_of_extensionObject_10() { return &___extensionObject_10; }
	inline void set_extensionObject_10(Il2CppObject * value)
	{
		___extensionObject_10 = value;
		Il2CppCodeGenWriteBarrier(&___extensionObject_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
