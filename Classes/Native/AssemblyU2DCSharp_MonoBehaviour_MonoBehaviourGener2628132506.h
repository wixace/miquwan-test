﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// MethodID
struct MethodID_t3916401116;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MonoBehaviour_MonoBehaviourGenerated
struct  MonoBehaviour_MonoBehaviourGenerated_t2628132506  : public Il2CppObject
{
public:

public:
};

struct MonoBehaviour_MonoBehaviourGenerated_t2628132506_StaticFields
{
public:
	// MethodID MonoBehaviour_MonoBehaviourGenerated::methodID0
	MethodID_t3916401116 * ___methodID0_0;
	// MethodID MonoBehaviour_MonoBehaviourGenerated::methodID1
	MethodID_t3916401116 * ___methodID1_1;

public:
	inline static int32_t get_offset_of_methodID0_0() { return static_cast<int32_t>(offsetof(MonoBehaviour_MonoBehaviourGenerated_t2628132506_StaticFields, ___methodID0_0)); }
	inline MethodID_t3916401116 * get_methodID0_0() const { return ___methodID0_0; }
	inline MethodID_t3916401116 ** get_address_of_methodID0_0() { return &___methodID0_0; }
	inline void set_methodID0_0(MethodID_t3916401116 * value)
	{
		___methodID0_0 = value;
		Il2CppCodeGenWriteBarrier(&___methodID0_0, value);
	}

	inline static int32_t get_offset_of_methodID1_1() { return static_cast<int32_t>(offsetof(MonoBehaviour_MonoBehaviourGenerated_t2628132506_StaticFields, ___methodID1_1)); }
	inline MethodID_t3916401116 * get_methodID1_1() const { return ___methodID1_1; }
	inline MethodID_t3916401116 ** get_address_of_methodID1_1() { return &___methodID1_1; }
	inline void set_methodID1_1(MethodID_t3916401116 * value)
	{
		___methodID1_1 = value;
		Il2CppCodeGenWriteBarrier(&___methodID1_1, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
