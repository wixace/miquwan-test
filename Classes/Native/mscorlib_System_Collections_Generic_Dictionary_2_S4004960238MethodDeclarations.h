﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Single>
struct ShimEnumerator_t4004960238;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Single>
struct Dictionary_2_t4289182211;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Single>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m3068591264_gshared (ShimEnumerator_t4004960238 * __this, Dictionary_2_t4289182211 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m3068591264(__this, ___host0, method) ((  void (*) (ShimEnumerator_t4004960238 *, Dictionary_2_t4289182211 *, const MethodInfo*))ShimEnumerator__ctor_m3068591264_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Single>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m4164679237_gshared (ShimEnumerator_t4004960238 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m4164679237(__this, method) ((  bool (*) (ShimEnumerator_t4004960238 *, const MethodInfo*))ShimEnumerator_MoveNext_m4164679237_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Single>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m2563254149_gshared (ShimEnumerator_t4004960238 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m2563254149(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t4004960238 *, const MethodInfo*))ShimEnumerator_get_Entry_m2563254149_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Single>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m1584420612_gshared (ShimEnumerator_t4004960238 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m1584420612(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4004960238 *, const MethodInfo*))ShimEnumerator_get_Key_m1584420612_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Single>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m3258221718_gshared (ShimEnumerator_t4004960238 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m3258221718(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4004960238 *, const MethodInfo*))ShimEnumerator_get_Value_m3258221718_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Single>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m1544603038_gshared (ShimEnumerator_t4004960238 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m1544603038(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t4004960238 *, const MethodInfo*))ShimEnumerator_get_Current_m1544603038_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Single>::Reset()
extern "C"  void ShimEnumerator_Reset_m1853531890_gshared (ShimEnumerator_t4004960238 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1853531890(__this, method) ((  void (*) (ShimEnumerator_t4004960238 *, const MethodInfo*))ShimEnumerator_Reset_m1853531890_gshared)(__this, method)
