﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UI3DWidgetMgr
struct UI3DWidgetMgr_t1313838767;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// UIPanel
struct UIPanel_t295209936;
// UICamera
struct UICamera_t189364953;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"
#include "AssemblyU2DCSharp_UICamera189364953.h"

// System.Void UI3DWidgetMgr::.ctor()
extern "C"  void UI3DWidgetMgr__ctor_m1204281244 (UI3DWidgetMgr_t1313838767 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UI3DWidgetMgr::GetRoot()
extern "C"  GameObject_t3674682005 * UI3DWidgetMgr_GetRoot_m933247367 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UI3DWidgetMgr::NguiToWorld(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t4282066566  UI3DWidgetMgr_NguiToWorld_m2610695616 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ____pos0, float ____z1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UI3DWidgetMgr::WorldToNgui(UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  UI3DWidgetMgr_WorldToNgui_m4195577291 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ____pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UI3DWidgetMgr::ilo_set_depth1(UIPanel,System.Int32)
extern "C"  void UI3DWidgetMgr_ilo_set_depth1_m1562547963 (Il2CppObject * __this /* static, unused */, UIPanel_t295209936 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UICamera UI3DWidgetMgr::ilo_get_eventHandler2()
extern "C"  UICamera_t189364953 * UI3DWidgetMgr_ilo_get_eventHandler2_m1767674692 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UI3DWidgetMgr::ilo_get_cachedCamera3(UICamera)
extern "C"  Camera_t2727095145 * UI3DWidgetMgr_ilo_get_cachedCamera3_m3956953714 (Il2CppObject * __this /* static, unused */, UICamera_t189364953 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
