﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V3504626119MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m1650085452(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2155005293 *, Dictionary_2_t3454399580 *, const MethodInfo*))ValueCollection__ctor_m1593514206_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1446704230(__this, ___item0, method) ((  void (*) (ValueCollection_t2155005293 *, ErrorInfo_t2633981210 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m2414710676_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m293012527(__this, method) ((  void (*) (ValueCollection_t2155005293 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m2971821661_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m93007300(__this, ___item0, method) ((  bool (*) (ValueCollection_t2155005293 *, ErrorInfo_t2633981210 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m3636485974_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1377625961(__this, ___item0, method) ((  bool (*) (ValueCollection_t2155005293 *, ErrorInfo_t2633981210 , const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m2847233403_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m1858740015(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2155005293 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m171930461_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m498431603(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2155005293 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2972088097_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m4127354306(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2155005293 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1070718448_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m2663150455(__this, method) ((  bool (*) (ValueCollection_t2155005293 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1911661833_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m2104685527(__this, method) ((  bool (*) (ValueCollection_t2155005293 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m1783132265_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m4266709833(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2155005293 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2782635355_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1256120211(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2155005293 *, ErrorInfoU5BU5D_t2864912703*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2611221925_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::GetEnumerator()
#define ValueCollection_GetEnumerator_m393135356(__this, method) ((  Enumerator_t1386232988  (*) (ValueCollection_t2155005293 *, const MethodInfo*))ValueCollection_GetEnumerator_m1194861198_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<System.String,Mihua.Assets.SubAssetMgr/ErrorInfo>::get_Count()
#define ValueCollection_get_Count_m647709329(__this, method) ((  int32_t (*) (ValueCollection_t2155005293 *, const MethodInfo*))ValueCollection_get_Count_m3905304867_gshared)(__this, method)
