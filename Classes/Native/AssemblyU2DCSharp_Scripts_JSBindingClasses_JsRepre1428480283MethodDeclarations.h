﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Scripts.JSBindingClasses.JsRepresentClass/<GeneratedJSFuntion2>c__AnonStoreyFE
struct U3CGeneratedJSFuntion2U3Ec__AnonStoreyFE_t1428480283;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Scripts.JSBindingClasses.JsRepresentClass/<GeneratedJSFuntion2>c__AnonStoreyFE::.ctor()
extern "C"  void U3CGeneratedJSFuntion2U3Ec__AnonStoreyFE__ctor_m930101728 (U3CGeneratedJSFuntion2U3Ec__AnonStoreyFE_t1428480283 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Scripts.JSBindingClasses.JsRepresentClass/<GeneratedJSFuntion2>c__AnonStoreyFE::<>m__322(System.Object,System.Object)
extern "C"  Il2CppObject * U3CGeneratedJSFuntion2U3Ec__AnonStoreyFE_U3CU3Em__322_m2159273217 (U3CGeneratedJSFuntion2U3Ec__AnonStoreyFE_t1428480283 * __this, Il2CppObject * ___object10, Il2CppObject * ___object21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
