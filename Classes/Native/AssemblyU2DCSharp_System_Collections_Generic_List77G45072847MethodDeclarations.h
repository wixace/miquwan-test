﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Collections_Generic_List77Generated
struct System_Collections_Generic_List77Generated_t45072847;
// JSVCall
struct JSVCall_t3708497963;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t4136801618;
// System.Type
struct Type_t;
// ConstructorID
struct ConstructorID_t3348888181;
// System.Object
struct Il2CppObject;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// PropertyID
struct PropertyID_t1067426256;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// MethodID
struct MethodID_t3916401116;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Delegate
struct Delegate_t3310234105;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_ConstructorID3348888181.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_PropertyID1067426256.h"
#include "AssemblyU2DCSharp_MethodID3916401116.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Delegate3310234105.h"

// System.Void System_Collections_Generic_List77Generated::.ctor()
extern "C"  void System_Collections_Generic_List77Generated__ctor_m2543914924 (System_Collections_Generic_List77Generated_t45072847 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_Generic_List77Generated::.cctor()
extern "C"  void System_Collections_Generic_List77Generated__cctor_m1069855105 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_ListA11(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_ListA11_m2826956206 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_ListA12(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_ListA12_m4071720687 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_ListA13(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_ListA13_m1021517872 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_Generic_List77Generated::ListA1_Capacity(JSVCall)
extern "C"  void System_Collections_Generic_List77Generated_ListA1_Capacity_m839469170 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_Generic_List77Generated::ListA1_Count(JSVCall)
extern "C"  void System_Collections_Generic_List77Generated_ListA1_Count_m1844914353 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_Generic_List77Generated::ListA1_Item_Int32(JSVCall)
extern "C"  void System_Collections_Generic_List77Generated_ListA1_Item_Int32_m3938493034 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_Add__T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_Add__T_m2992562602 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_AddRange__IEnumerableT1_T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_AddRange__IEnumerableT1_T_m557976914 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_AsReadOnly(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_AsReadOnly_m3605299307 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_BinarySearch__Int32__Int32__T__IComparerT1_T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_BinarySearch__Int32__Int32__T__IComparerT1_T_m3668049780 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_BinarySearch__T__IComparerT1_T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_BinarySearch__T__IComparerT1_T_m3343776116 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_BinarySearch__T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_BinarySearch__T_m2204807094 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_Clear(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_Clear_m409313848 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_Contains__T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_Contains__T_m3158881376 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_ConvertAllT1__ConverterT2_T_TOutput(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_ConvertAllT1__ConverterT2_T_TOutput_m1283902473 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_CopyTo__Int32__T_Array__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_CopyTo__Int32__T_Array__Int32__Int32_m1704450087 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_CopyTo__T_Array__Int32(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_CopyTo__T_Array__Int32_m1576485543 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_CopyTo__T_Array(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_CopyTo__T_Array_m3168233609 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_Exists__PredicateT1_T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_Exists__PredicateT1_T_m1900403000 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_Find__PredicateT1_T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_Find__PredicateT1_T_m3289398395 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_FindAll__PredicateT1_T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_FindAll__PredicateT1_T_m4126399992 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_FindIndex__Int32__Int32__PredicateT1_T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_FindIndex__Int32__Int32__PredicateT1_T_m4136195751 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_FindIndex__Int32__PredicateT1_T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_FindIndex__Int32__PredicateT1_T_m2733574879 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_FindIndex__PredicateT1_T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_FindIndex__PredicateT1_T_m2390959527 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_FindLast__PredicateT1_T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_FindLast__PredicateT1_T_m2072635141 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_FindLastIndex__Int32__Int32__PredicateT1_T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_FindLastIndex__Int32__Int32__PredicateT1_T_m2282290141 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_FindLastIndex__Int32__PredicateT1_T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_FindLastIndex__Int32__PredicateT1_T_m2178627177 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_FindLastIndex__PredicateT1_T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_FindLastIndex__PredicateT1_T_m2650833245 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_ForEach__ActionT1_T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_ForEach__ActionT1_T_m3551060541 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_GetEnumerator(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_GetEnumerator_m3715547621 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_GetRange__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_GetRange__Int32__Int32_m1498448382 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_IndexOf__T__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_IndexOf__T__Int32__Int32_m3799578754 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_IndexOf__T__Int32(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_IndexOf__T__Int32_m1660470030 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_IndexOf__T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_IndexOf__T_m2442296834 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_Insert__Int32__T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_Insert__Int32__T_m1506060182 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_InsertRange__Int32__IEnumerableT1_T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_InsertRange__Int32__IEnumerableT1_T_m3356107300 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_LastIndexOf__T__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_LastIndexOf__T__Int32__Int32_m1908325816 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_LastIndexOf__T__Int32(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_LastIndexOf__T__Int32_m1220726424 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_LastIndexOf__T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_LastIndexOf__T_m3322804664 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_Remove__T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_Remove__T_m1006648347 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_RemoveAll__PredicateT1_T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_RemoveAll__PredicateT1_T_m1303799875 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_RemoveAt__Int32(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_RemoveAt__Int32_m1996308162 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_RemoveRange__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_RemoveRange__Int32__Int32_m3873022340 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_Reverse__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_Reverse__Int32__Int32_m3033095629 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_Reverse(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_Reverse_m3305845389 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_Sort__Int32__Int32__IComparerT1_T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_Sort__Int32__Int32__IComparerT1_T_m3323704245 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_Sort__ComparisonT1_T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_Sort__ComparisonT1_T_m2500283024 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_Sort__IComparerT1_T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_Sort__IComparerT1_T_m4087466229 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_Sort(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_Sort_m2253123157 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_ToArray(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_ToArray_m3453612873 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_TrimExcess(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_TrimExcess_m20225966 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ListA1_TrueForAll__PredicateT1_T(JSVCall,System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ListA1_TrueForAll__PredicateT1_T_m1778714510 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_Generic_List77Generated::__Register()
extern "C"  void System_Collections_Generic_List77Generated___Register_m684939419 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System_Collections_Generic_List77Generated::<ListA1_CopyTo__Int32__T_Array__Int32__Int32>m__9F()
extern "C"  ObjectU5BU5D_t1108656482* System_Collections_Generic_List77Generated_U3CListA1_CopyTo__Int32__T_Array__Int32__Int32U3Em__9F_m3403053356 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System_Collections_Generic_List77Generated::<ListA1_CopyTo__T_Array__Int32>m__A0()
extern "C"  ObjectU5BU5D_t1108656482* System_Collections_Generic_List77Generated_U3CListA1_CopyTo__T_Array__Int32U3Em__A0_m443958222 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object[] System_Collections_Generic_List77Generated::<ListA1_CopyTo__T_Array>m__A1()
extern "C"  ObjectU5BU5D_t1108656482* System_Collections_Generic_List77Generated_U3CListA1_CopyTo__T_ArrayU3Em__A1_m2141220703 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.ConstructorInfo System_Collections_Generic_List77Generated::ilo_makeGenericConstructor1(System.Type,ConstructorID)
extern "C"  ConstructorInfo_t4136801618 * System_Collections_Generic_List77Generated_ilo_makeGenericConstructor1_m3543651912 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, ConstructorID_t3348888181 * ___constructorID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Collections_Generic_List77Generated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool System_Collections_Generic_List77Generated_ilo_attachFinalizerObject2_m1774829300 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Collections_Generic_List77Generated::ilo_getInt323(System.Int32)
extern "C"  int32_t System_Collections_Generic_List77Generated_ilo_getInt323_m1317519349 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_Generic_List77Generated::ilo_addJSCSRel4(System.Int32,System.Object)
extern "C"  void System_Collections_Generic_List77Generated_ilo_addJSCSRel4_m4006014443 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.PropertyInfo System_Collections_Generic_List77Generated::ilo_getProperty5(System.Type,PropertyID)
extern "C"  PropertyInfo_t * System_Collections_Generic_List77Generated_ilo_getProperty5_m1016447560 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, PropertyID_t1067426256 * ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_Generic_List77Generated::ilo_setInt326(System.Int32,System.Int32)
extern "C"  void System_Collections_Generic_List77Generated_ilo_setInt326_m2115587717 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System_Collections_Generic_List77Generated::ilo_getMethod7(System.Type,MethodID)
extern "C"  MethodInfo_t * System_Collections_Generic_List77Generated_ilo_getMethod7_m3157538982 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MethodID_t3916401116 * ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Collections_Generic_List77Generated::ilo_getWhatever8(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_Collections_Generic_List77Generated_ilo_getWhatever8_m1945299975 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_Collections_Generic_List77Generated::ilo_getObject9(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_Collections_Generic_List77Generated_ilo_getObject9_m3836200075 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System_Collections_Generic_List77Generated::ilo_makeGenericMethod10(System.Type,MethodID,System.Int32)
extern "C"  MethodInfo_t * System_Collections_Generic_List77Generated_ilo_makeGenericMethod10_m3139856720 (Il2CppObject * __this /* static, unused */, Type_t * ___type0, MethodID_t3916401116 * ___methodID1, int32_t ___TCount2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_Generic_List77Generated::ilo_setBooleanS11(System.Int32,System.Boolean)
extern "C"  void System_Collections_Generic_List77Generated_ilo_setBooleanS11_m123684712 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_Generic_List77Generated::ilo_addJSFunCSDelegateRel12(System.Int32,System.Delegate)
extern "C"  void System_Collections_Generic_List77Generated_ilo_addJSFunCSDelegateRel12_m2889727936 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Collections_Generic_List77Generated::ilo_setObject13(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_Collections_Generic_List77Generated_ilo_setObject13_m3562439361 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_Generic_List77Generated::ilo_setWhatever14(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  void System_Collections_Generic_List77Generated_ilo_setWhatever14_m1572307443 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Collections_Generic_List77Generated::ilo_moveSaveID2Arr15(System.Int32)
extern "C"  void System_Collections_Generic_List77Generated_ilo_moveSaveID2Arr15_m3649060920 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Collections_Generic_List77Generated::ilo_getObject16(System.Int32)
extern "C"  int32_t System_Collections_Generic_List77Generated_ilo_getObject16_m4262568052 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Collections_Generic_List77Generated::ilo_getElement17(System.Int32,System.Int32)
extern "C"  int32_t System_Collections_Generic_List77Generated_ilo_getElement17_m4255435361 (Il2CppObject * __this /* static, unused */, int32_t ___id0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
