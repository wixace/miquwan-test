﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MDragScrollView
struct MDragScrollView_t2304459027;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void MDragScrollView::.ctor()
extern "C"  void MDragScrollView__ctor_m1717111736 (MDragScrollView_t2304459027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MDragScrollView::Start()
extern "C"  void MDragScrollView_Start_m664249528 (MDragScrollView_t2304459027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MDragScrollView::OnDrag(UnityEngine.Vector2)
extern "C"  void MDragScrollView_OnDrag_m240993947 (MDragScrollView_t2304459027 * __this, Vector2_t4282066565  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MDragScrollView::OnPress()
extern "C"  void MDragScrollView_OnPress_m1542352954 (MDragScrollView_t2304459027 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
