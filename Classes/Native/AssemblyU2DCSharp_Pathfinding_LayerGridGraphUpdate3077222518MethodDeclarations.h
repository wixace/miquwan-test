﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.LayerGridGraphUpdate
struct LayerGridGraphUpdate_t3077222518;

#include "codegen/il2cpp-codegen.h"

// System.Void Pathfinding.LayerGridGraphUpdate::.ctor()
extern "C"  void LayerGridGraphUpdate__ctor_m404480689 (LayerGridGraphUpdate_t3077222518 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
