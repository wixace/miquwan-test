﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K3069027043MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m1600355124(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2434198250 *, Dictionary_2_t807438799 *, const MethodInfo*))KeyCollection__ctor_m482485400_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1390849442(__this, ___item0, method) ((  void (*) (KeyCollection_t2434198250 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4040471998_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2329924505(__this, method) ((  void (*) (KeyCollection_t2434198250 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3047764661_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m837989416(__this, ___item0, method) ((  bool (*) (KeyCollection_t2434198250 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2853804428_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m640552845(__this, ___item0, method) ((  bool (*) (KeyCollection_t2434198250 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2765553137_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2412712597(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2434198250 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2038988721_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m2137791115(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2434198250 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2054401959_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m3427193414(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2434198250 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m380150370_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m735035785(__this, method) ((  bool (*) (KeyCollection_t2434198250 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m903011821_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3491983739(__this, method) ((  bool (*) (KeyCollection_t2434198250 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m36174559_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m1183773863(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2434198250 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2889175819_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m896184809(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2434198250 *, MoveWayU5BU5D_t505035803*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m4121422669_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::GetEnumerator()
#define KeyCollection_GetEnumerator_m3691241228(__this, method) ((  Enumerator_t1422374853  (*) (KeyCollection_t2434198250 *, const MethodInfo*))KeyCollection_GetEnumerator_m3949864048_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,MScrollView/OnMoveCallBackFun>::get_Count()
#define KeyCollection_get_Count_m3246943681(__this, method) ((  int32_t (*) (KeyCollection_t2434198250 *, const MethodInfo*))KeyCollection_get_Count_m949761829_gshared)(__this, method)
