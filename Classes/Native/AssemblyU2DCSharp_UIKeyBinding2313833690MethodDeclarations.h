﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIKeyBinding
struct UIKeyBinding_t2313833690;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIKeyBinding2313833690.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UIKeyBinding::.ctor()
extern "C"  void UIKeyBinding__ctor_m1497201409 (UIKeyBinding_t2313833690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::Start()
extern "C"  void UIKeyBinding_Start_m444339201 (UIKeyBinding_t2313833690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::OnSubmit()
extern "C"  void UIKeyBinding_OnSubmit_m2549197370 (UIKeyBinding_t2313833690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIKeyBinding::IsModifierActive()
extern "C"  bool UIKeyBinding_IsModifierActive_m4070177950 (UIKeyBinding_t2313833690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::Update()
extern "C"  void UIKeyBinding_Update_m895465516 (UIKeyBinding_t2313833690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::OnBindingPress(System.Boolean)
extern "C"  void UIKeyBinding_OnBindingPress_m678603287 (UIKeyBinding_t2313833690 * __this, bool ___pressed0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::OnBindingClick()
extern "C"  void UIKeyBinding_OnBindingClick_m630288549 (UIKeyBinding_t2313833690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIKeyBinding::ilo_IsModifierActive1(UIKeyBinding)
extern "C"  bool UIKeyBinding_ilo_IsModifierActive1_m1898396616 (Il2CppObject * __this /* static, unused */, UIKeyBinding_t2313833690 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::ilo_OnBindingPress2(UIKeyBinding,System.Boolean)
extern "C"  void UIKeyBinding_ilo_OnBindingPress2_m2974150166 (Il2CppObject * __this /* static, unused */, UIKeyBinding_t2313833690 * ____this0, bool ___pressed1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::ilo_OnBindingClick3(UIKeyBinding)
extern "C"  void UIKeyBinding_ilo_OnBindingClick3_m350592227 (Il2CppObject * __this /* static, unused */, UIKeyBinding_t2313833690 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::ilo_set_selectedObject4(UnityEngine.GameObject)
extern "C"  void UIKeyBinding_ilo_set_selectedObject4_m1839642561 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIKeyBinding::ilo_Notify5(UnityEngine.GameObject,System.String,System.Object)
extern "C"  void UIKeyBinding_ilo_Notify5_m496708442 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, String_t* ___funcName1, Il2CppObject * ___obj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
