﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_WheelHitGenerated
struct UnityEngine_WheelHitGenerated_t1828605391;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void UnityEngine_WheelHitGenerated::.ctor()
extern "C"  void UnityEngine_WheelHitGenerated__ctor_m3307625596 (UnityEngine_WheelHitGenerated_t1828605391 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelHitGenerated::.cctor()
extern "C"  void UnityEngine_WheelHitGenerated__cctor_m3270049457 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_WheelHitGenerated::WheelHit_WheelHit1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_WheelHitGenerated_WheelHit_WheelHit1_m2628433014 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelHitGenerated::WheelHit_collider(JSVCall)
extern "C"  void UnityEngine_WheelHitGenerated_WheelHit_collider_m1949881490 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelHitGenerated::WheelHit_point(JSVCall)
extern "C"  void UnityEngine_WheelHitGenerated_WheelHit_point_m134405494 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelHitGenerated::WheelHit_normal(JSVCall)
extern "C"  void UnityEngine_WheelHitGenerated_WheelHit_normal_m2652942591 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelHitGenerated::WheelHit_forwardDir(JSVCall)
extern "C"  void UnityEngine_WheelHitGenerated_WheelHit_forwardDir_m4014132094 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelHitGenerated::WheelHit_sidewaysDir(JSVCall)
extern "C"  void UnityEngine_WheelHitGenerated_WheelHit_sidewaysDir_m2804603892 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelHitGenerated::WheelHit_force(JSVCall)
extern "C"  void UnityEngine_WheelHitGenerated_WheelHit_force_m3296099963 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelHitGenerated::WheelHit_forwardSlip(JSVCall)
extern "C"  void UnityEngine_WheelHitGenerated_WheelHit_forwardSlip_m2860700929 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelHitGenerated::WheelHit_sidewaysSlip(JSVCall)
extern "C"  void UnityEngine_WheelHitGenerated_WheelHit_sidewaysSlip_m4020032331 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelHitGenerated::__Register()
extern "C"  void UnityEngine_WheelHitGenerated___Register_m3115158987 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_WheelHitGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_WheelHitGenerated_ilo_getObject1_m213101498 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelHitGenerated::ilo_changeJSObj2(System.Int32,System.Object)
extern "C"  void UnityEngine_WheelHitGenerated_ilo_changeJSObj2_m675416814 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelHitGenerated::ilo_setVector3S3(System.Int32,UnityEngine.Vector3)
extern "C"  void UnityEngine_WheelHitGenerated_ilo_setVector3S3_m2536547277 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_WheelHitGenerated::ilo_setSingle4(System.Int32,System.Single)
extern "C"  void UnityEngine_WheelHitGenerated_ilo_setSingle4_m1016215547 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
