﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int2,System.Object>
struct ShimEnumerator_t683106684;
// System.Collections.Generic.Dictionary`2<Pathfinding.Int2,System.Object>
struct Dictionary_2_t967328657;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int2,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void ShimEnumerator__ctor_m4217858453_gshared (ShimEnumerator_t683106684 * __this, Dictionary_2_t967328657 * ___host0, const MethodInfo* method);
#define ShimEnumerator__ctor_m4217858453(__this, ___host0, method) ((  void (*) (ShimEnumerator_t683106684 *, Dictionary_2_t967328657 *, const MethodInfo*))ShimEnumerator__ctor_m4217858453_gshared)(__this, ___host0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int2,System.Object>::MoveNext()
extern "C"  bool ShimEnumerator_MoveNext_m1438164912_gshared (ShimEnumerator_t683106684 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m1438164912(__this, method) ((  bool (*) (ShimEnumerator_t683106684 *, const MethodInfo*))ShimEnumerator_MoveNext_m1438164912_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int2,System.Object>::get_Entry()
extern "C"  DictionaryEntry_t1751606614  ShimEnumerator_get_Entry_m1183514106_gshared (ShimEnumerator_t683106684 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m1183514106(__this, method) ((  DictionaryEntry_t1751606614  (*) (ShimEnumerator_t683106684 *, const MethodInfo*))ShimEnumerator_get_Entry_m1183514106_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int2,System.Object>::get_Key()
extern "C"  Il2CppObject * ShimEnumerator_get_Key_m702664121_gshared (ShimEnumerator_t683106684 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m702664121(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t683106684 *, const MethodInfo*))ShimEnumerator_get_Key_m702664121_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int2,System.Object>::get_Value()
extern "C"  Il2CppObject * ShimEnumerator_get_Value_m1998791179_gshared (ShimEnumerator_t683106684 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m1998791179(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t683106684 *, const MethodInfo*))ShimEnumerator_get_Value_m1998791179_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int2,System.Object>::get_Current()
extern "C"  Il2CppObject * ShimEnumerator_get_Current_m2412632531_gshared (ShimEnumerator_t683106684 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m2412632531(__this, method) ((  Il2CppObject * (*) (ShimEnumerator_t683106684 *, const MethodInfo*))ShimEnumerator_get_Current_m2412632531_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<Pathfinding.Int2,System.Object>::Reset()
extern "C"  void ShimEnumerator_Reset_m1770288231_gshared (ShimEnumerator_t683106684 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m1770288231(__this, method) ((  void (*) (ShimEnumerator_t683106684 *, const MethodInfo*))ShimEnumerator_Reset_m1770288231_gshared)(__this, method)
