﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_getowce218
struct M_getowce218_t3995948169;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_getowce2183995948169.h"

// System.Void GarbageiOS.M_getowce218::.ctor()
extern "C"  void M_getowce218__ctor_m3564503498 (M_getowce218_t3995948169 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_getowce218::M_rire0(System.String[],System.Int32)
extern "C"  void M_getowce218_M_rire0_m1128306131 (M_getowce218_t3995948169 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_getowce218::M_neaniStaivewas1(System.String[],System.Int32)
extern "C"  void M_getowce218_M_neaniStaivewas1_m1510793298 (M_getowce218_t3995948169 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_getowce218::M_nudou2(System.String[],System.Int32)
extern "C"  void M_getowce218_M_nudou2_m1594594482 (M_getowce218_t3995948169 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_getowce218::ilo_M_rire01(GarbageiOS.M_getowce218,System.String[],System.Int32)
extern "C"  void M_getowce218_ilo_M_rire01_m1448964662 (Il2CppObject * __this /* static, unused */, M_getowce218_t3995948169 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_getowce218::ilo_M_neaniStaivewas12(GarbageiOS.M_getowce218,System.String[],System.Int32)
extern "C"  void M_getowce218_ilo_M_neaniStaivewas12_m3338585624 (Il2CppObject * __this /* static, unused */, M_getowce218_t3995948169 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
