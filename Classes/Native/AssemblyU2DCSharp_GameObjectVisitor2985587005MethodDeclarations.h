﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.String
struct String_t;
// GameObjectVisitor/condition
struct condition_t3833526985;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t747900261;
// System.Action`1<UnityEngine.GameObject>
struct Action_1_t4070498141;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_GameObjectVisitor_condition3833526985.h"

// UnityEngine.GameObject GameObjectVisitor::FindChild(UnityEngine.GameObject,System.String)
extern "C"  GameObject_t3674682005 * GameObjectVisitor_FindChild_m2523642424 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject GameObjectVisitor::FindChildInCondition(UnityEngine.GameObject,GameObjectVisitor/condition)
extern "C"  GameObject_t3674682005 * GameObjectVisitor_FindChildInCondition_m2502392303 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, condition_t3833526985 * ___cond1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameObjectVisitor::FindChildren(UnityEngine.GameObject,System.String,System.Collections.Generic.List`1<UnityEngine.GameObject>)
extern "C"  void GameObjectVisitor_FindChildren_m2407405878 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, String_t* ___name1, List_1_t747900261 * ___objs2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameObjectVisitor::FindChildrenInCondition(UnityEngine.GameObject,System.Collections.Generic.List`1<UnityEngine.GameObject>,GameObjectVisitor/condition)
extern "C"  void GameObjectVisitor_FindChildrenInCondition_m1308914713 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, List_1_t747900261 * ___objs1, condition_t3833526985 * ___cond2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameObjectVisitor::VisitChildren(UnityEngine.GameObject,System.Action`1<UnityEngine.GameObject>,System.Int32)
extern "C"  void GameObjectVisitor_VisitChildren_m66085565 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, Action_1_t4070498141 * ___fn1, int32_t ___depth2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameObjectVisitor::RemoveAllChildren(UnityEngine.GameObject)
extern "C"  void GameObjectVisitor_RemoveAllChildren_m2667179904 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject GameObjectVisitor::ilo_FindChildInCondition1(UnityEngine.GameObject,GameObjectVisitor/condition)
extern "C"  GameObject_t3674682005 * GameObjectVisitor_ilo_FindChildInCondition1_m1632021959 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ____this0, condition_t3833526985 * ___cond1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
