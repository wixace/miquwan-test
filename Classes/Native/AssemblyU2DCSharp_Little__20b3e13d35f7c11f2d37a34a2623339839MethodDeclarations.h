﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._20b3e13d35f7c11f2d37a34ae55dcdd3
struct _20b3e13d35f7c11f2d37a34ae55dcdd3_t2623339839;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._20b3e13d35f7c11f2d37a34ae55dcdd3::.ctor()
extern "C"  void _20b3e13d35f7c11f2d37a34ae55dcdd3__ctor_m4190478286 (_20b3e13d35f7c11f2d37a34ae55dcdd3_t2623339839 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._20b3e13d35f7c11f2d37a34ae55dcdd3::_20b3e13d35f7c11f2d37a34ae55dcdd3m2(System.Int32)
extern "C"  int32_t _20b3e13d35f7c11f2d37a34ae55dcdd3__20b3e13d35f7c11f2d37a34ae55dcdd3m2_m2115080505 (_20b3e13d35f7c11f2d37a34ae55dcdd3_t2623339839 * __this, int32_t ____20b3e13d35f7c11f2d37a34ae55dcdd3a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._20b3e13d35f7c11f2d37a34ae55dcdd3::_20b3e13d35f7c11f2d37a34ae55dcdd3m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _20b3e13d35f7c11f2d37a34ae55dcdd3__20b3e13d35f7c11f2d37a34ae55dcdd3m_m2357455261 (_20b3e13d35f7c11f2d37a34ae55dcdd3_t2623339839 * __this, int32_t ____20b3e13d35f7c11f2d37a34ae55dcdd3a0, int32_t ____20b3e13d35f7c11f2d37a34ae55dcdd3701, int32_t ____20b3e13d35f7c11f2d37a34ae55dcdd3c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
