﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICenterOnClickGenerated
struct UICenterOnClickGenerated_t2283662031;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UICenterOnClickGenerated::.ctor()
extern "C"  void UICenterOnClickGenerated__ctor_m1921866924 (UICenterOnClickGenerated_t2283662031 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UICenterOnClickGenerated::UICenterOnClick_UICenterOnClick1(JSVCall,System.Int32)
extern "C"  bool UICenterOnClickGenerated_UICenterOnClick_UICenterOnClick1_m2623052044 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICenterOnClickGenerated::__Register()
extern "C"  void UICenterOnClickGenerated___Register_m2794400667 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICenterOnClickGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UICenterOnClickGenerated_ilo_addJSCSRel1_m3261140328 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
