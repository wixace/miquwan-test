﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializer/TypeResolver
struct TypeResolver_t356109658;
// System.Object
struct Il2CppObject;
// System.Type
struct Type_t;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void ProtoBuf.Serializer/TypeResolver::.ctor(System.Object,System.IntPtr)
extern "C"  void TypeResolver__ctor_m297305345 (TypeResolver_t356109658 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializer/TypeResolver::Invoke(System.Int32)
extern "C"  Type_t * TypeResolver_Invoke_m4213011718 (TypeResolver_t356109658 * __this, int32_t ___fieldNumber0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult ProtoBuf.Serializer/TypeResolver::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * TypeResolver_BeginInvoke_m1882472677 (TypeResolver_t356109658 * __this, int32_t ___fieldNumber0, AsyncCallback_t1369114871 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializer/TypeResolver::EndInvoke(System.IAsyncResult)
extern "C"  Type_t * TypeResolver_EndInvoke_m3849537195 (TypeResolver_t356109658 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
