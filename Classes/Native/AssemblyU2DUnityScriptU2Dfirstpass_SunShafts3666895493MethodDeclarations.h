﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SunShafts
struct SunShafts_t3666895493;
// UnityEngine.RenderTexture
struct RenderTexture_t1963041563;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RenderTexture1963041563.h"

// System.Void SunShafts::.ctor()
extern "C"  void SunShafts__ctor_m2031597629 (SunShafts_t3666895493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SunShafts::CheckResources()
extern "C"  bool SunShafts_CheckResources_m3695063166 (SunShafts_t3666895493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SunShafts::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern "C"  void SunShafts_OnRenderImage_m3230775809 (SunShafts_t3666895493 * __this, RenderTexture_t1963041563 * ___source0, RenderTexture_t1963041563 * ___destination1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SunShafts::Main()
extern "C"  void SunShafts_Main_m3029071072 (SunShafts_t3666895493 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
