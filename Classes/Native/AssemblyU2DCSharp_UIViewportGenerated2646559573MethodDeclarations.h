﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIViewportGenerated
struct UIViewportGenerated_t2646559573;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UIViewportGenerated::.ctor()
extern "C"  void UIViewportGenerated__ctor_m2881861558 (UIViewportGenerated_t2646559573 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIViewportGenerated::UIViewport_UIViewport1(JSVCall,System.Int32)
extern "C"  bool UIViewportGenerated_UIViewport_UIViewport1_m2272415228 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIViewportGenerated::UIViewport_sourceCamera(JSVCall)
extern "C"  void UIViewportGenerated_UIViewport_sourceCamera_m1137546926 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIViewportGenerated::UIViewport_topLeft(JSVCall)
extern "C"  void UIViewportGenerated_UIViewport_topLeft_m2502253762 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIViewportGenerated::UIViewport_bottomRight(JSVCall)
extern "C"  void UIViewportGenerated_UIViewport_bottomRight_m1029268653 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIViewportGenerated::UIViewport_fullSize(JSVCall)
extern "C"  void UIViewportGenerated_UIViewport_fullSize_m4038708382 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIViewportGenerated::__Register()
extern "C"  void UIViewportGenerated___Register_m3395823313 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIViewportGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UIViewportGenerated_ilo_attachFinalizerObject1_m882620161 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIViewportGenerated::ilo_getObject2(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIViewportGenerated_ilo_getObject2_m1149495280 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
