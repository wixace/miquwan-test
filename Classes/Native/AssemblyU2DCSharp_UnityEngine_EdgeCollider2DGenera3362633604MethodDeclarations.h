﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EdgeCollider2DGenerated
struct UnityEngine_EdgeCollider2DGenerated_t3362633604;
// JSVCall
struct JSVCall_t3708497963;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

// System.Void UnityEngine_EdgeCollider2DGenerated::.ctor()
extern "C"  void UnityEngine_EdgeCollider2DGenerated__ctor_m1604960359 (UnityEngine_EdgeCollider2DGenerated_t3362633604 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EdgeCollider2DGenerated::EdgeCollider2D_EdgeCollider2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EdgeCollider2DGenerated_EdgeCollider2D_EdgeCollider2D1_m2363397515 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EdgeCollider2DGenerated::EdgeCollider2D_edgeCount(JSVCall)
extern "C"  void UnityEngine_EdgeCollider2DGenerated_EdgeCollider2D_edgeCount_m1463613140 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EdgeCollider2DGenerated::EdgeCollider2D_pointCount(JSVCall)
extern "C"  void UnityEngine_EdgeCollider2DGenerated_EdgeCollider2D_pointCount_m2559910055 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EdgeCollider2DGenerated::EdgeCollider2D_points(JSVCall)
extern "C"  void UnityEngine_EdgeCollider2DGenerated_EdgeCollider2D_points_m332778915 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EdgeCollider2DGenerated::EdgeCollider2D_Reset(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EdgeCollider2DGenerated_EdgeCollider2D_Reset_m605935980 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EdgeCollider2DGenerated::__Register()
extern "C"  void UnityEngine_EdgeCollider2DGenerated___Register_m3117606080 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2[] UnityEngine_EdgeCollider2DGenerated::<EdgeCollider2D_points>m__1AB()
extern "C"  Vector2U5BU5D_t4024180168* UnityEngine_EdgeCollider2DGenerated_U3CEdgeCollider2D_pointsU3Em__1AB_m624033790 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EdgeCollider2DGenerated::ilo_addJSCSRel1(System.Int32,System.Object)
extern "C"  void UnityEngine_EdgeCollider2DGenerated_ilo_addJSCSRel1_m2480893859 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EdgeCollider2DGenerated::ilo_setInt322(System.Int32,System.Int32)
extern "C"  void UnityEngine_EdgeCollider2DGenerated_ilo_setInt322_m2429993902 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EdgeCollider2DGenerated::ilo_setVector2S3(System.Int32,UnityEngine.Vector2)
extern "C"  void UnityEngine_EdgeCollider2DGenerated_ilo_setVector2S3_m4041885538 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EdgeCollider2DGenerated::ilo_moveSaveID2Arr4(System.Int32)
extern "C"  void UnityEngine_EdgeCollider2DGenerated_ilo_moveSaveID2Arr4_m3646761599 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EdgeCollider2DGenerated::ilo_setArrayS5(System.Int32,System.Int32,System.Boolean)
extern "C"  void UnityEngine_EdgeCollider2DGenerated_ilo_setArrayS5_m875799168 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
