﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Poly2Tri.AdvancingFrontNode
struct AdvancingFrontNode_t688967424;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Poly2Tri.AdvancingFront
struct  AdvancingFront_t240556382  : public Il2CppObject
{
public:
	// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.AdvancingFront::Head
	AdvancingFrontNode_t688967424 * ___Head_0;
	// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.AdvancingFront::Tail
	AdvancingFrontNode_t688967424 * ___Tail_1;
	// Pathfinding.Poly2Tri.AdvancingFrontNode Pathfinding.Poly2Tri.AdvancingFront::Search
	AdvancingFrontNode_t688967424 * ___Search_2;

public:
	inline static int32_t get_offset_of_Head_0() { return static_cast<int32_t>(offsetof(AdvancingFront_t240556382, ___Head_0)); }
	inline AdvancingFrontNode_t688967424 * get_Head_0() const { return ___Head_0; }
	inline AdvancingFrontNode_t688967424 ** get_address_of_Head_0() { return &___Head_0; }
	inline void set_Head_0(AdvancingFrontNode_t688967424 * value)
	{
		___Head_0 = value;
		Il2CppCodeGenWriteBarrier(&___Head_0, value);
	}

	inline static int32_t get_offset_of_Tail_1() { return static_cast<int32_t>(offsetof(AdvancingFront_t240556382, ___Tail_1)); }
	inline AdvancingFrontNode_t688967424 * get_Tail_1() const { return ___Tail_1; }
	inline AdvancingFrontNode_t688967424 ** get_address_of_Tail_1() { return &___Tail_1; }
	inline void set_Tail_1(AdvancingFrontNode_t688967424 * value)
	{
		___Tail_1 = value;
		Il2CppCodeGenWriteBarrier(&___Tail_1, value);
	}

	inline static int32_t get_offset_of_Search_2() { return static_cast<int32_t>(offsetof(AdvancingFront_t240556382, ___Search_2)); }
	inline AdvancingFrontNode_t688967424 * get_Search_2() const { return ___Search_2; }
	inline AdvancingFrontNode_t688967424 ** get_address_of_Search_2() { return &___Search_2; }
	inline void set_Search_2(AdvancingFrontNode_t688967424 * value)
	{
		___Search_2 = value;
		Il2CppCodeGenWriteBarrier(&___Search_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
