﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Nova.PersistPathMgr
struct  PersistPathMgr_t636044869  : public Il2CppObject
{
public:

public:
};

struct PersistPathMgr_t636044869_StaticFields
{
public:
	// System.String Nova.PersistPathMgr::s_persistentDataPath
	String_t* ___s_persistentDataPath_0;

public:
	inline static int32_t get_offset_of_s_persistentDataPath_0() { return static_cast<int32_t>(offsetof(PersistPathMgr_t636044869_StaticFields, ___s_persistentDataPath_0)); }
	inline String_t* get_s_persistentDataPath_0() const { return ___s_persistentDataPath_0; }
	inline String_t** get_address_of_s_persistentDataPath_0() { return &___s_persistentDataPath_0; }
	inline void set_s_persistentDataPath_0(String_t* value)
	{
		___s_persistentDataPath_0 = value;
		Il2CppCodeGenWriteBarrier(&___s_persistentDataPath_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
