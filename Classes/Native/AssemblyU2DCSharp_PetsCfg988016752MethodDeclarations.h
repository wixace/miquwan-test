﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PetsCfg
struct PetsCfg_t988016752;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void PetsCfg::.ctor()
extern "C"  void PetsCfg__ctor_m3235167483 (PetsCfg_t988016752 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PetsCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void PetsCfg_Init_m332733610 (PetsCfg_t988016752 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
