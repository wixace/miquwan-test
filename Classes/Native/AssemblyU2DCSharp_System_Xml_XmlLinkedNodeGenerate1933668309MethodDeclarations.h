﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Xml_XmlLinkedNodeGenerated
struct System_Xml_XmlLinkedNodeGenerated_t1933668309;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Xml_XmlLinkedNodeGenerated::.ctor()
extern "C"  void System_Xml_XmlLinkedNodeGenerated__ctor_m1644391734 (System_Xml_XmlLinkedNodeGenerated_t1933668309 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlLinkedNodeGenerated::XmlLinkedNode_NextSibling(JSVCall)
extern "C"  void System_Xml_XmlLinkedNodeGenerated_XmlLinkedNode_NextSibling_m3817788401 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlLinkedNodeGenerated::XmlLinkedNode_PreviousSibling(JSVCall)
extern "C"  void System_Xml_XmlLinkedNodeGenerated_XmlLinkedNode_PreviousSibling_m3635019509 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XmlLinkedNodeGenerated::__Register()
extern "C"  void System_Xml_XmlLinkedNodeGenerated___Register_m702538065 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Xml_XmlLinkedNodeGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_Xml_XmlLinkedNodeGenerated_ilo_setObject1_m842589624 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
