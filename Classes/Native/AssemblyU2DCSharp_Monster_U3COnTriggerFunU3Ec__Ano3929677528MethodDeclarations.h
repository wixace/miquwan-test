﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Monster/<OnTriggerFun>c__AnonStorey152
struct U3COnTriggerFunU3Ec__AnonStorey152_t3929677528;

#include "codegen/il2cpp-codegen.h"

// System.Void Monster/<OnTriggerFun>c__AnonStorey152::.ctor()
extern "C"  void U3COnTriggerFunU3Ec__AnonStorey152__ctor_m3192920259 (U3COnTriggerFunU3Ec__AnonStorey152_t3929677528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Monster/<OnTriggerFun>c__AnonStorey152::<>m__3C7()
extern "C"  void U3COnTriggerFunU3Ec__AnonStorey152_U3CU3Em__3C7_m2478921373 (U3COnTriggerFunU3Ec__AnonStorey152_t3929677528 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
