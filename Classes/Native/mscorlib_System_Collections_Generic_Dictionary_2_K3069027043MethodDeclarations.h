﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,System.Object>
struct KeyCollection_t3069027043;
// System.Collections.Generic.Dictionary`2<MScrollView/MoveWay,System.Object>
struct Dictionary_2_t1442267592;
// System.Collections.Generic.IEnumerator`1<MScrollView/MoveWay>
struct IEnumerator_1_t1091839303;
// System.Array
struct Il2CppArray;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Object
struct Il2CppObject;
// MScrollView/MoveWay[]
struct MoveWayU5BU5D_t505035803;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_MScrollView_MoveWay3474941550.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2057203646.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void KeyCollection__ctor_m482485400_gshared (KeyCollection_t3069027043 * __this, Dictionary_2_t1442267592 * ___dictionary0, const MethodInfo* method);
#define KeyCollection__ctor_m482485400(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t3069027043 *, Dictionary_2_t1442267592 *, const MethodInfo*))KeyCollection__ctor_m482485400_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,System.Object>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4040471998_gshared (KeyCollection_t3069027043 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4040471998(__this, ___item0, method) ((  void (*) (KeyCollection_t3069027043 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m4040471998_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,System.Object>::System.Collections.Generic.ICollection<TKey>.Clear()
extern "C"  void KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3047764661_gshared (KeyCollection_t3069027043 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3047764661(__this, method) ((  void (*) (KeyCollection_t3069027043 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m3047764661_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,System.Object>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2853804428_gshared (KeyCollection_t3069027043 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2853804428(__this, ___item0, method) ((  bool (*) (KeyCollection_t3069027043 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m2853804428_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,System.Object>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2765553137_gshared (KeyCollection_t3069027043 * __this, int32_t ___item0, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2765553137(__this, ___item0, method) ((  bool (*) (KeyCollection_t3069027043 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m2765553137_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,System.Object>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
extern "C"  Il2CppObject* KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2038988721_gshared (KeyCollection_t3069027043 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2038988721(__this, method) ((  Il2CppObject* (*) (KeyCollection_t3069027043 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2038988721_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C"  void KeyCollection_System_Collections_ICollection_CopyTo_m2054401959_gshared (KeyCollection_t3069027043 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_CopyTo_m2054401959(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3069027043 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m2054401959_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * KeyCollection_System_Collections_IEnumerable_GetEnumerator_m380150370_gshared (KeyCollection_t3069027043 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m380150370(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3069027043 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m380150370_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,System.Object>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
extern "C"  bool KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m903011821_gshared (KeyCollection_t3069027043 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m903011821(__this, method) ((  bool (*) (KeyCollection_t3069027043 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m903011821_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C"  bool KeyCollection_System_Collections_ICollection_get_IsSynchronized_m36174559_gshared (KeyCollection_t3069027043 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m36174559(__this, method) ((  bool (*) (KeyCollection_t3069027043 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m36174559_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * KeyCollection_System_Collections_ICollection_get_SyncRoot_m2889175819_gshared (KeyCollection_t3069027043 * __this, const MethodInfo* method);
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m2889175819(__this, method) ((  Il2CppObject * (*) (KeyCollection_t3069027043 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2889175819_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,System.Object>::CopyTo(TKey[],System.Int32)
extern "C"  void KeyCollection_CopyTo_m4121422669_gshared (KeyCollection_t3069027043 * __this, MoveWayU5BU5D_t505035803* ___array0, int32_t ___index1, const MethodInfo* method);
#define KeyCollection_CopyTo_m4121422669(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t3069027043 *, MoveWayU5BU5D_t505035803*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m4121422669_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,System.Object>::GetEnumerator()
extern "C"  Enumerator_t2057203646  KeyCollection_GetEnumerator_m3949864048_gshared (KeyCollection_t3069027043 * __this, const MethodInfo* method);
#define KeyCollection_GetEnumerator_m3949864048(__this, method) ((  Enumerator_t2057203646  (*) (KeyCollection_t3069027043 *, const MethodInfo*))KeyCollection_GetEnumerator_m3949864048_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<MScrollView/MoveWay,System.Object>::get_Count()
extern "C"  int32_t KeyCollection_get_Count_m949761829_gshared (KeyCollection_t3069027043 * __this, const MethodInfo* method);
#define KeyCollection_get_Count_m949761829(__this, method) ((  int32_t (*) (KeyCollection_t3069027043 *, const MethodInfo*))KeyCollection_get_Count_m949761829_gshared)(__this, method)
