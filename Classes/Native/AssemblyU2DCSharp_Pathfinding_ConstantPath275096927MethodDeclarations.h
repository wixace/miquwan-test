﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.ConstantPath
struct ConstantPath_t275096927;
// OnPathDelegate
struct OnPathDelegate_t598607977;
// Pathfinding.Path
struct Path_t1974241691;
// System.String
struct String_t;
// Pathfinding.PathNode
struct PathNode_t417131581;
// Pathfinding.PathHandler
struct PathHandler_t918952263;
// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_OnPathDelegate598607977.h"
#include "AssemblyU2DCSharp_Pathfinding_Path1974241691.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_Pathfinding_PathHandler918952263.h"
#include "AssemblyU2DCSharp_Pathfinding_GraphNode23612370.h"
#include "AssemblyU2DCSharp_Pathfinding_PathNode417131581.h"
#include "AssemblyU2DCSharp_PathCompleteState1625108115.h"

// System.Void Pathfinding.ConstantPath::.ctor()
extern "C"  void ConstantPath__ctor_m3574004712 (ConstantPath_t275096927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConstantPath::.ctor(UnityEngine.Vector3,OnPathDelegate)
extern "C"  void ConstantPath__ctor_m887661420 (ConstantPath_t275096927 * __this, Vector3_t4282066566  ___start0, OnPathDelegate_t598607977 * ___callbackDelegate1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConstantPath::.ctor(UnityEngine.Vector3,System.Int32,OnPathDelegate)
extern "C"  void ConstantPath__ctor_m3995144247 (ConstantPath_t275096927 * __this, Vector3_t4282066566  ___start0, int32_t ___maxGScore1, OnPathDelegate_t598607977 * ___callbackDelegate2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ConstantPath::get_FloodingPath()
extern "C"  bool ConstantPath_get_FloodingPath_m1982048194 (ConstantPath_t275096927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ConstantPath Pathfinding.ConstantPath::Construct(UnityEngine.Vector3,System.Int32,OnPathDelegate)
extern "C"  ConstantPath_t275096927 * ConstantPath_Construct_m3836424940 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___start0, int32_t ___maxGScore1, OnPathDelegate_t598607977 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConstantPath::Setup(UnityEngine.Vector3,System.Int32,OnPathDelegate)
extern "C"  void ConstantPath_Setup_m2155816572 (ConstantPath_t275096927 * __this, Vector3_t4282066566  ___start0, int32_t ___maxGScore1, OnPathDelegate_t598607977 * ___callback2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConstantPath::OnEnterPool()
extern "C"  void ConstantPath_OnEnterPool_m2083488763 (ConstantPath_t275096927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConstantPath::Recycle()
extern "C"  void ConstantPath_Recycle_m3853290905 (ConstantPath_t275096927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConstantPath::Reset()
extern "C"  void ConstantPath_Reset_m1220437653 (ConstantPath_t275096927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConstantPath::Prepare()
extern "C"  void ConstantPath_Prepare_m1575639821 (ConstantPath_t275096927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConstantPath::Initialize()
extern "C"  void ConstantPath_Initialize_m1869515692 (ConstantPath_t275096927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConstantPath::Cleanup()
extern "C"  void ConstantPath_Cleanup_m1313452394 (ConstantPath_t275096927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConstantPath::CalculateStep(System.Int64)
extern "C"  void ConstantPath_CalculateStep_m3621230506 (ConstantPath_t275096927 * __this, int64_t ___targetTick0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConstantPath::ilo_Reset1(Pathfinding.Path)
extern "C"  void ConstantPath_ilo_Reset1_m3363123490 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConstantPath::ilo_LogError2(Pathfinding.Path,System.String)
extern "C"  void ConstantPath_ilo_LogError2_m1798184526 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, String_t* ___msg1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathNode Pathfinding.ConstantPath::ilo_GetPathNode3(Pathfinding.PathHandler,Pathfinding.GraphNode)
extern "C"  PathNode_t417131581 * ConstantPath_ilo_GetPathNode3_m622955532 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, GraphNode_t23612370 * ___node1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConstantPath::ilo_set_cost4(Pathfinding.PathNode,System.UInt32)
extern "C"  void ConstantPath_ilo_set_cost4_m2549938144 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConstantPath::ilo_set_H5(Pathfinding.PathNode,System.UInt32)
extern "C"  void ConstantPath_ilo_set_H5_m2528754172 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ConstantPath::ilo_HeapEmpty6(Pathfinding.PathHandler)
extern "C"  bool ConstantPath_ilo_HeapEmpty6_m4020567167 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConstantPath::ilo_set_CompleteState7(Pathfinding.Path,PathCompleteState)
extern "C"  void ConstantPath_ilo_set_CompleteState7_m4213894389 (Il2CppObject * __this /* static, unused */, Path_t1974241691 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.ConstantPath::ilo_set_flag18(Pathfinding.PathNode,System.Boolean)
extern "C"  void ConstantPath_ilo_set_flag18_m3311716431 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.ConstantPath::ilo_get_flag19(Pathfinding.PathNode)
extern "C"  bool ConstantPath_ilo_get_flag19_m4274767855 (Il2CppObject * __this /* static, unused */, PathNode_t417131581 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.PathNode Pathfinding.ConstantPath::ilo_PopNode10(Pathfinding.PathHandler)
extern "C"  PathNode_t417131581 * ConstantPath_ilo_PopNode10_m1445204958 (Il2CppObject * __this /* static, unused */, PathHandler_t918952263 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
