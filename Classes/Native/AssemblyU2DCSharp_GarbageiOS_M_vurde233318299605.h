﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_vurde23
struct  M_vurde23_t3318299605  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_vurde23::_fecu
	uint32_t ____fecu_0;
	// System.Single GarbageiOS.M_vurde23::_comoukaSatrobow
	float ____comoukaSatrobow_1;
	// System.Boolean GarbageiOS.M_vurde23::_laikiwoWibaw
	bool ____laikiwoWibaw_2;
	// System.UInt32 GarbageiOS.M_vurde23::_jaserwowMalge
	uint32_t ____jaserwowMalge_3;
	// System.Int32 GarbageiOS.M_vurde23::_tratis
	int32_t ____tratis_4;
	// System.Int32 GarbageiOS.M_vurde23::_nureru
	int32_t ____nureru_5;
	// System.Int32 GarbageiOS.M_vurde23::_joosooleMemlis
	int32_t ____joosooleMemlis_6;
	// System.Single GarbageiOS.M_vurde23::_vasstaihair
	float ____vasstaihair_7;
	// System.String GarbageiOS.M_vurde23::_stallreXurkaw
	String_t* ____stallreXurkaw_8;
	// System.Int32 GarbageiOS.M_vurde23::_jeahage
	int32_t ____jeahage_9;
	// System.String GarbageiOS.M_vurde23::_renelallHiswhu
	String_t* ____renelallHiswhu_10;
	// System.String GarbageiOS.M_vurde23::_bateVize
	String_t* ____bateVize_11;
	// System.Single GarbageiOS.M_vurde23::_trearcallpa
	float ____trearcallpa_12;
	// System.Boolean GarbageiOS.M_vurde23::_qirsislaw
	bool ____qirsislaw_13;
	// System.Int32 GarbageiOS.M_vurde23::_lekoutu
	int32_t ____lekoutu_14;
	// System.String GarbageiOS.M_vurde23::_rairfaw
	String_t* ____rairfaw_15;
	// System.Int32 GarbageiOS.M_vurde23::_berallYarce
	int32_t ____berallYarce_16;
	// System.Single GarbageiOS.M_vurde23::_xocirmuJelo
	float ____xocirmuJelo_17;
	// System.Int32 GarbageiOS.M_vurde23::_seweemurDrepowche
	int32_t ____seweemurDrepowche_18;
	// System.Single GarbageiOS.M_vurde23::_bewhirseLasge
	float ____bewhirseLasge_19;
	// System.Single GarbageiOS.M_vurde23::_naskadairKanurna
	float ____naskadairKanurna_20;
	// System.Single GarbageiOS.M_vurde23::_tawxirba
	float ____tawxirba_21;
	// System.String GarbageiOS.M_vurde23::_nownay
	String_t* ____nownay_22;
	// System.Int32 GarbageiOS.M_vurde23::_maypo
	int32_t ____maypo_23;
	// System.Boolean GarbageiOS.M_vurde23::_choochu
	bool ____choochu_24;
	// System.Int32 GarbageiOS.M_vurde23::_cojor
	int32_t ____cojor_25;
	// System.String GarbageiOS.M_vurde23::_bainou
	String_t* ____bainou_26;
	// System.Boolean GarbageiOS.M_vurde23::_sarrar
	bool ____sarrar_27;
	// System.Single GarbageiOS.M_vurde23::_halljelsta
	float ____halljelsta_28;

public:
	inline static int32_t get_offset_of__fecu_0() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____fecu_0)); }
	inline uint32_t get__fecu_0() const { return ____fecu_0; }
	inline uint32_t* get_address_of__fecu_0() { return &____fecu_0; }
	inline void set__fecu_0(uint32_t value)
	{
		____fecu_0 = value;
	}

	inline static int32_t get_offset_of__comoukaSatrobow_1() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____comoukaSatrobow_1)); }
	inline float get__comoukaSatrobow_1() const { return ____comoukaSatrobow_1; }
	inline float* get_address_of__comoukaSatrobow_1() { return &____comoukaSatrobow_1; }
	inline void set__comoukaSatrobow_1(float value)
	{
		____comoukaSatrobow_1 = value;
	}

	inline static int32_t get_offset_of__laikiwoWibaw_2() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____laikiwoWibaw_2)); }
	inline bool get__laikiwoWibaw_2() const { return ____laikiwoWibaw_2; }
	inline bool* get_address_of__laikiwoWibaw_2() { return &____laikiwoWibaw_2; }
	inline void set__laikiwoWibaw_2(bool value)
	{
		____laikiwoWibaw_2 = value;
	}

	inline static int32_t get_offset_of__jaserwowMalge_3() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____jaserwowMalge_3)); }
	inline uint32_t get__jaserwowMalge_3() const { return ____jaserwowMalge_3; }
	inline uint32_t* get_address_of__jaserwowMalge_3() { return &____jaserwowMalge_3; }
	inline void set__jaserwowMalge_3(uint32_t value)
	{
		____jaserwowMalge_3 = value;
	}

	inline static int32_t get_offset_of__tratis_4() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____tratis_4)); }
	inline int32_t get__tratis_4() const { return ____tratis_4; }
	inline int32_t* get_address_of__tratis_4() { return &____tratis_4; }
	inline void set__tratis_4(int32_t value)
	{
		____tratis_4 = value;
	}

	inline static int32_t get_offset_of__nureru_5() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____nureru_5)); }
	inline int32_t get__nureru_5() const { return ____nureru_5; }
	inline int32_t* get_address_of__nureru_5() { return &____nureru_5; }
	inline void set__nureru_5(int32_t value)
	{
		____nureru_5 = value;
	}

	inline static int32_t get_offset_of__joosooleMemlis_6() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____joosooleMemlis_6)); }
	inline int32_t get__joosooleMemlis_6() const { return ____joosooleMemlis_6; }
	inline int32_t* get_address_of__joosooleMemlis_6() { return &____joosooleMemlis_6; }
	inline void set__joosooleMemlis_6(int32_t value)
	{
		____joosooleMemlis_6 = value;
	}

	inline static int32_t get_offset_of__vasstaihair_7() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____vasstaihair_7)); }
	inline float get__vasstaihair_7() const { return ____vasstaihair_7; }
	inline float* get_address_of__vasstaihair_7() { return &____vasstaihair_7; }
	inline void set__vasstaihair_7(float value)
	{
		____vasstaihair_7 = value;
	}

	inline static int32_t get_offset_of__stallreXurkaw_8() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____stallreXurkaw_8)); }
	inline String_t* get__stallreXurkaw_8() const { return ____stallreXurkaw_8; }
	inline String_t** get_address_of__stallreXurkaw_8() { return &____stallreXurkaw_8; }
	inline void set__stallreXurkaw_8(String_t* value)
	{
		____stallreXurkaw_8 = value;
		Il2CppCodeGenWriteBarrier(&____stallreXurkaw_8, value);
	}

	inline static int32_t get_offset_of__jeahage_9() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____jeahage_9)); }
	inline int32_t get__jeahage_9() const { return ____jeahage_9; }
	inline int32_t* get_address_of__jeahage_9() { return &____jeahage_9; }
	inline void set__jeahage_9(int32_t value)
	{
		____jeahage_9 = value;
	}

	inline static int32_t get_offset_of__renelallHiswhu_10() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____renelallHiswhu_10)); }
	inline String_t* get__renelallHiswhu_10() const { return ____renelallHiswhu_10; }
	inline String_t** get_address_of__renelallHiswhu_10() { return &____renelallHiswhu_10; }
	inline void set__renelallHiswhu_10(String_t* value)
	{
		____renelallHiswhu_10 = value;
		Il2CppCodeGenWriteBarrier(&____renelallHiswhu_10, value);
	}

	inline static int32_t get_offset_of__bateVize_11() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____bateVize_11)); }
	inline String_t* get__bateVize_11() const { return ____bateVize_11; }
	inline String_t** get_address_of__bateVize_11() { return &____bateVize_11; }
	inline void set__bateVize_11(String_t* value)
	{
		____bateVize_11 = value;
		Il2CppCodeGenWriteBarrier(&____bateVize_11, value);
	}

	inline static int32_t get_offset_of__trearcallpa_12() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____trearcallpa_12)); }
	inline float get__trearcallpa_12() const { return ____trearcallpa_12; }
	inline float* get_address_of__trearcallpa_12() { return &____trearcallpa_12; }
	inline void set__trearcallpa_12(float value)
	{
		____trearcallpa_12 = value;
	}

	inline static int32_t get_offset_of__qirsislaw_13() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____qirsislaw_13)); }
	inline bool get__qirsislaw_13() const { return ____qirsislaw_13; }
	inline bool* get_address_of__qirsislaw_13() { return &____qirsislaw_13; }
	inline void set__qirsislaw_13(bool value)
	{
		____qirsislaw_13 = value;
	}

	inline static int32_t get_offset_of__lekoutu_14() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____lekoutu_14)); }
	inline int32_t get__lekoutu_14() const { return ____lekoutu_14; }
	inline int32_t* get_address_of__lekoutu_14() { return &____lekoutu_14; }
	inline void set__lekoutu_14(int32_t value)
	{
		____lekoutu_14 = value;
	}

	inline static int32_t get_offset_of__rairfaw_15() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____rairfaw_15)); }
	inline String_t* get__rairfaw_15() const { return ____rairfaw_15; }
	inline String_t** get_address_of__rairfaw_15() { return &____rairfaw_15; }
	inline void set__rairfaw_15(String_t* value)
	{
		____rairfaw_15 = value;
		Il2CppCodeGenWriteBarrier(&____rairfaw_15, value);
	}

	inline static int32_t get_offset_of__berallYarce_16() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____berallYarce_16)); }
	inline int32_t get__berallYarce_16() const { return ____berallYarce_16; }
	inline int32_t* get_address_of__berallYarce_16() { return &____berallYarce_16; }
	inline void set__berallYarce_16(int32_t value)
	{
		____berallYarce_16 = value;
	}

	inline static int32_t get_offset_of__xocirmuJelo_17() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____xocirmuJelo_17)); }
	inline float get__xocirmuJelo_17() const { return ____xocirmuJelo_17; }
	inline float* get_address_of__xocirmuJelo_17() { return &____xocirmuJelo_17; }
	inline void set__xocirmuJelo_17(float value)
	{
		____xocirmuJelo_17 = value;
	}

	inline static int32_t get_offset_of__seweemurDrepowche_18() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____seweemurDrepowche_18)); }
	inline int32_t get__seweemurDrepowche_18() const { return ____seweemurDrepowche_18; }
	inline int32_t* get_address_of__seweemurDrepowche_18() { return &____seweemurDrepowche_18; }
	inline void set__seweemurDrepowche_18(int32_t value)
	{
		____seweemurDrepowche_18 = value;
	}

	inline static int32_t get_offset_of__bewhirseLasge_19() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____bewhirseLasge_19)); }
	inline float get__bewhirseLasge_19() const { return ____bewhirseLasge_19; }
	inline float* get_address_of__bewhirseLasge_19() { return &____bewhirseLasge_19; }
	inline void set__bewhirseLasge_19(float value)
	{
		____bewhirseLasge_19 = value;
	}

	inline static int32_t get_offset_of__naskadairKanurna_20() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____naskadairKanurna_20)); }
	inline float get__naskadairKanurna_20() const { return ____naskadairKanurna_20; }
	inline float* get_address_of__naskadairKanurna_20() { return &____naskadairKanurna_20; }
	inline void set__naskadairKanurna_20(float value)
	{
		____naskadairKanurna_20 = value;
	}

	inline static int32_t get_offset_of__tawxirba_21() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____tawxirba_21)); }
	inline float get__tawxirba_21() const { return ____tawxirba_21; }
	inline float* get_address_of__tawxirba_21() { return &____tawxirba_21; }
	inline void set__tawxirba_21(float value)
	{
		____tawxirba_21 = value;
	}

	inline static int32_t get_offset_of__nownay_22() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____nownay_22)); }
	inline String_t* get__nownay_22() const { return ____nownay_22; }
	inline String_t** get_address_of__nownay_22() { return &____nownay_22; }
	inline void set__nownay_22(String_t* value)
	{
		____nownay_22 = value;
		Il2CppCodeGenWriteBarrier(&____nownay_22, value);
	}

	inline static int32_t get_offset_of__maypo_23() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____maypo_23)); }
	inline int32_t get__maypo_23() const { return ____maypo_23; }
	inline int32_t* get_address_of__maypo_23() { return &____maypo_23; }
	inline void set__maypo_23(int32_t value)
	{
		____maypo_23 = value;
	}

	inline static int32_t get_offset_of__choochu_24() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____choochu_24)); }
	inline bool get__choochu_24() const { return ____choochu_24; }
	inline bool* get_address_of__choochu_24() { return &____choochu_24; }
	inline void set__choochu_24(bool value)
	{
		____choochu_24 = value;
	}

	inline static int32_t get_offset_of__cojor_25() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____cojor_25)); }
	inline int32_t get__cojor_25() const { return ____cojor_25; }
	inline int32_t* get_address_of__cojor_25() { return &____cojor_25; }
	inline void set__cojor_25(int32_t value)
	{
		____cojor_25 = value;
	}

	inline static int32_t get_offset_of__bainou_26() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____bainou_26)); }
	inline String_t* get__bainou_26() const { return ____bainou_26; }
	inline String_t** get_address_of__bainou_26() { return &____bainou_26; }
	inline void set__bainou_26(String_t* value)
	{
		____bainou_26 = value;
		Il2CppCodeGenWriteBarrier(&____bainou_26, value);
	}

	inline static int32_t get_offset_of__sarrar_27() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____sarrar_27)); }
	inline bool get__sarrar_27() const { return ____sarrar_27; }
	inline bool* get_address_of__sarrar_27() { return &____sarrar_27; }
	inline void set__sarrar_27(bool value)
	{
		____sarrar_27 = value;
	}

	inline static int32_t get_offset_of__halljelsta_28() { return static_cast<int32_t>(offsetof(M_vurde23_t3318299605, ____halljelsta_28)); }
	inline float get__halljelsta_28() const { return ____halljelsta_28; }
	inline float* get_address_of__halljelsta_28() { return &____halljelsta_28; }
	inline void set__halljelsta_28(float value)
	{
		____halljelsta_28 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
