﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Singleton_1_gen128664468MethodDeclarations.h"

// System.Void Singleton`1<Mihua.Assets.UnzipAssetMgr>::.ctor()
#define Singleton_1__ctor_m3950200904(__this, method) ((  void (*) (Singleton_1_t1529481055 *, const MethodInfo*))Singleton_1__ctor_m3958676923_gshared)(__this, method)
// T Singleton`1<Mihua.Assets.UnzipAssetMgr>::get_Instance()
#define Singleton_1_get_Instance_m1524263547(__this /* static, unused */, method) ((  UnzipAssetMgr_t1276665662 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))Singleton_1_get_Instance_m1020946630_gshared)(__this /* static, unused */, method)
