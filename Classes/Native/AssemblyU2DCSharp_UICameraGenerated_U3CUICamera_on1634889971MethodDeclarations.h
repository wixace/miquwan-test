﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UICameraGenerated/<UICamera_onHover_GetDelegate_member44_arg0>c__AnonStoreyA5
struct U3CUICamera_onHover_GetDelegate_member44_arg0U3Ec__AnonStoreyA5_t1634889971;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void UICameraGenerated/<UICamera_onHover_GetDelegate_member44_arg0>c__AnonStoreyA5::.ctor()
extern "C"  void U3CUICamera_onHover_GetDelegate_member44_arg0U3Ec__AnonStoreyA5__ctor_m327225816 (U3CUICamera_onHover_GetDelegate_member44_arg0U3Ec__AnonStoreyA5_t1634889971 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UICameraGenerated/<UICamera_onHover_GetDelegate_member44_arg0>c__AnonStoreyA5::<>m__10E(UnityEngine.GameObject,System.Boolean)
extern "C"  void U3CUICamera_onHover_GetDelegate_member44_arg0U3Ec__AnonStoreyA5_U3CU3Em__10E_m2723253630 (U3CUICamera_onHover_GetDelegate_member44_arg0U3Ec__AnonStoreyA5_t1634889971 * __this, GameObject_t3674682005 * ___go0, bool ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
