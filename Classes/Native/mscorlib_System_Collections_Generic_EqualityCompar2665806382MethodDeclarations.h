﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Voxels.ExtraMesh>
struct DefaultComparer_t2665806382;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_ExtraMesh4218029715.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Voxels.ExtraMesh>::.ctor()
extern "C"  void DefaultComparer__ctor_m3066981330_gshared (DefaultComparer_t2665806382 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m3066981330(__this, method) ((  void (*) (DefaultComparer_t2665806382 *, const MethodInfo*))DefaultComparer__ctor_m3066981330_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Voxels.ExtraMesh>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m486737121_gshared (DefaultComparer_t2665806382 * __this, ExtraMesh_t4218029715  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m486737121(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2665806382 *, ExtraMesh_t4218029715 , const MethodInfo*))DefaultComparer_GetHashCode_m486737121_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<Pathfinding.Voxels.ExtraMesh>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m924052455_gshared (DefaultComparer_t2665806382 * __this, ExtraMesh_t4218029715  ___x0, ExtraMesh_t4218029715  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m924052455(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2665806382 *, ExtraMesh_t4218029715 , ExtraMesh_t4218029715 , const MethodInfo*))DefaultComparer_Equals_m924052455_gshared)(__this, ___x0, ___y1, method)
