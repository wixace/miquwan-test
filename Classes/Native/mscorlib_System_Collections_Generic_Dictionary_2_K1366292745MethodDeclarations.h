﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_K2247594085MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PushType,PluginPush/NotificationData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m3546351111(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t1366292745 *, Dictionary_2_t4034500590 *, const MethodInfo*))KeyCollection__ctor_m1606937486_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PushType,PluginPush/NotificationData>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1734109935(__this, ___item0, method) ((  void (*) (KeyCollection_t1366292745 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m3112168712_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PushType,PluginPush/NotificationData>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1078886438(__this, method) ((  void (*) (KeyCollection_t1366292745 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1567470719_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PushType,PluginPush/NotificationData>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m1446493631(__this, ___item0, method) ((  bool (*) (KeyCollection_t1366292745 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m680636102_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PushType,PluginPush/NotificationData>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m4270232292(__this, ___item0, method) ((  bool (*) (KeyCollection_t1366292745 *, int32_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1726421419_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<PushType,PluginPush/NotificationData>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m3992374712(__this, method) ((  Il2CppObject* (*) (KeyCollection_t1366292745 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m2086956113_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PushType,PluginPush/NotificationData>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m3865271192(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1366292745 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m3588346481_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<PushType,PluginPush/NotificationData>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2657416103(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1366292745 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1807839296_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PushType,PluginPush/NotificationData>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1392034144(__this, method) ((  bool (*) (KeyCollection_t1366292745 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m4137323687_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<PushType,PluginPush/NotificationData>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m1754989074(__this, method) ((  bool (*) (KeyCollection_t1366292745 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m712895001_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<PushType,PluginPush/NotificationData>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m3462586116(__this, method) ((  Il2CppObject * (*) (KeyCollection_t1366292745 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2112109963_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<PushType,PluginPush/NotificationData>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m3698346172(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t1366292745 *, PushTypeU5BU5D_t3978570141*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2136410691_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<PushType,PluginPush/NotificationData>::GetEnumerator()
#define KeyCollection_GetEnumerator_m261005001(__this, method) ((  Enumerator_t354469348  (*) (KeyCollection_t1366292745 *, const MethodInfo*))KeyCollection_GetEnumerator_m1549819792_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<PushType,PluginPush/NotificationData>::get_Count()
#define KeyCollection_get_Count_m2027478348(__this, method) ((  int32_t (*) (KeyCollection_t1366292745 *, const MethodInfo*))KeyCollection_get_Count_m440118739_gshared)(__this, method)
