﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23222658402MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m2106284244(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t2108115480 *, TriangulationPoint_t3810082933 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m2730552978_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m4041438356(__this, method) ((  TriangulationPoint_t3810082933 * (*) (KeyValuePair_2_t2108115480 *, const MethodInfo*))KeyValuePair_2_get_Key_m4285571350_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m542952661(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2108115480 *, TriangulationPoint_t3810082933 *, const MethodInfo*))KeyValuePair_2_set_Key_m1188304983_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m2924407892(__this, method) ((  int32_t (*) (KeyValuePair_2_t2108115480 *, const MethodInfo*))KeyValuePair_2_get_Value_m2690735574_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m3442191957(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t2108115480 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m137193687_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<Pathfinding.Poly2Tri.TriangulationPoint,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m2845364589(__this, method) ((  String_t* (*) (KeyValuePair_2_t2108115480 *, const MethodInfo*))KeyValuePair_2_ToString_m2052282219_gshared)(__this, method)
