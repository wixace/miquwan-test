﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_Entity_Behavior_IBehavior770859129.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Entity.Behavior.NpcAutoFightBvr
struct  NpcAutoFightBvr_t3149258780  : public IBehavior_t770859129
{
public:
	// System.Int32 Entity.Behavior.NpcAutoFightBvr::totalround
	int32_t ___totalround_3;
	// System.Single Entity.Behavior.NpcAutoFightBvr::_curTime
	float ____curTime_4;

public:
	inline static int32_t get_offset_of_totalround_3() { return static_cast<int32_t>(offsetof(NpcAutoFightBvr_t3149258780, ___totalround_3)); }
	inline int32_t get_totalround_3() const { return ___totalround_3; }
	inline int32_t* get_address_of_totalround_3() { return &___totalround_3; }
	inline void set_totalround_3(int32_t value)
	{
		___totalround_3 = value;
	}

	inline static int32_t get_offset_of__curTime_4() { return static_cast<int32_t>(offsetof(NpcAutoFightBvr_t3149258780, ____curTime_4)); }
	inline float get__curTime_4() const { return ____curTime_4; }
	inline float* get_address_of__curTime_4() { return &____curTime_4; }
	inline void set__curTime_4(float value)
	{
		____curTime_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
