﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_Xml_XPath_IXPathNavigableGenerated
struct System_Xml_XPath_IXPathNavigableGenerated_t1916366114;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System_Xml_XPath_IXPathNavigableGenerated::.ctor()
extern "C"  void System_Xml_XPath_IXPathNavigableGenerated__ctor_m2017439369 (System_Xml_XPath_IXPathNavigableGenerated_t1916366114 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_Xml_XPath_IXPathNavigableGenerated::IXPathNavigable_CreateNavigator(JSVCall,System.Int32)
extern "C"  bool System_Xml_XPath_IXPathNavigableGenerated_IXPathNavigable_CreateNavigator_m733758190 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_Xml_XPath_IXPathNavigableGenerated::__Register()
extern "C"  void System_Xml_XPath_IXPathNavigableGenerated___Register_m298220510 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_Xml_XPath_IXPathNavigableGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_Xml_XPath_IXPathNavigableGenerated_ilo_setObject1_m2359230533 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
