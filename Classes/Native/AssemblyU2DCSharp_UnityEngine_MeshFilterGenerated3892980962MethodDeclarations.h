﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_MeshFilterGenerated
struct UnityEngine_MeshFilterGenerated_t3892980962;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_MeshFilterGenerated::.ctor()
extern "C"  void UnityEngine_MeshFilterGenerated__ctor_m3243911369 (UnityEngine_MeshFilterGenerated_t3892980962 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_MeshFilterGenerated::MeshFilter_MeshFilter1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_MeshFilterGenerated_MeshFilter_MeshFilter1_m362773417 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshFilterGenerated::MeshFilter_mesh(JSVCall)
extern "C"  void UnityEngine_MeshFilterGenerated_MeshFilter_mesh_m457920281 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshFilterGenerated::MeshFilter_sharedMesh(JSVCall)
extern "C"  void UnityEngine_MeshFilterGenerated_MeshFilter_sharedMesh_m2526852788 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshFilterGenerated::__Register()
extern "C"  void UnityEngine_MeshFilterGenerated___Register_m90073502 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MeshFilterGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_MeshFilterGenerated_ilo_getObject1_m1125514509 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_MeshFilterGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_MeshFilterGenerated_ilo_addJSCSRel2_m3138040710 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_MeshFilterGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_MeshFilterGenerated_ilo_setObject3_m1574465799 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
