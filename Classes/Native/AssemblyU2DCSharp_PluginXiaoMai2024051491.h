﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// Mihua.SDK.PayInfo
struct PayInfo_t1775308120;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginXiaoMai
struct  PluginXiaoMai_t2024051491  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginXiaoMai::mem_id
	String_t* ___mem_id_2;
	// System.String PluginXiaoMai::user_token
	String_t* ___user_token_3;
	// System.String PluginXiaoMai::switchaccount
	String_t* ___switchaccount_4;
	// System.Boolean PluginXiaoMai::isOut
	bool ___isOut_5;
	// System.String PluginXiaoMai::configId
	String_t* ___configId_6;
	// Mihua.SDK.PayInfo PluginXiaoMai::iapInfo
	PayInfo_t1775308120 * ___iapInfo_7;

public:
	inline static int32_t get_offset_of_mem_id_2() { return static_cast<int32_t>(offsetof(PluginXiaoMai_t2024051491, ___mem_id_2)); }
	inline String_t* get_mem_id_2() const { return ___mem_id_2; }
	inline String_t** get_address_of_mem_id_2() { return &___mem_id_2; }
	inline void set_mem_id_2(String_t* value)
	{
		___mem_id_2 = value;
		Il2CppCodeGenWriteBarrier(&___mem_id_2, value);
	}

	inline static int32_t get_offset_of_user_token_3() { return static_cast<int32_t>(offsetof(PluginXiaoMai_t2024051491, ___user_token_3)); }
	inline String_t* get_user_token_3() const { return ___user_token_3; }
	inline String_t** get_address_of_user_token_3() { return &___user_token_3; }
	inline void set_user_token_3(String_t* value)
	{
		___user_token_3 = value;
		Il2CppCodeGenWriteBarrier(&___user_token_3, value);
	}

	inline static int32_t get_offset_of_switchaccount_4() { return static_cast<int32_t>(offsetof(PluginXiaoMai_t2024051491, ___switchaccount_4)); }
	inline String_t* get_switchaccount_4() const { return ___switchaccount_4; }
	inline String_t** get_address_of_switchaccount_4() { return &___switchaccount_4; }
	inline void set_switchaccount_4(String_t* value)
	{
		___switchaccount_4 = value;
		Il2CppCodeGenWriteBarrier(&___switchaccount_4, value);
	}

	inline static int32_t get_offset_of_isOut_5() { return static_cast<int32_t>(offsetof(PluginXiaoMai_t2024051491, ___isOut_5)); }
	inline bool get_isOut_5() const { return ___isOut_5; }
	inline bool* get_address_of_isOut_5() { return &___isOut_5; }
	inline void set_isOut_5(bool value)
	{
		___isOut_5 = value;
	}

	inline static int32_t get_offset_of_configId_6() { return static_cast<int32_t>(offsetof(PluginXiaoMai_t2024051491, ___configId_6)); }
	inline String_t* get_configId_6() const { return ___configId_6; }
	inline String_t** get_address_of_configId_6() { return &___configId_6; }
	inline void set_configId_6(String_t* value)
	{
		___configId_6 = value;
		Il2CppCodeGenWriteBarrier(&___configId_6, value);
	}

	inline static int32_t get_offset_of_iapInfo_7() { return static_cast<int32_t>(offsetof(PluginXiaoMai_t2024051491, ___iapInfo_7)); }
	inline PayInfo_t1775308120 * get_iapInfo_7() const { return ___iapInfo_7; }
	inline PayInfo_t1775308120 ** get_address_of_iapInfo_7() { return &___iapInfo_7; }
	inline void set_iapInfo_7(PayInfo_t1775308120 * value)
	{
		___iapInfo_7 = value;
		Il2CppCodeGenWriteBarrier(&___iapInfo_7, value);
	}
};

struct PluginXiaoMai_t2024051491_StaticFields
{
public:
	// System.Action PluginXiaoMai::<>f__am$cache6
	Action_t3771233898 * ___U3CU3Ef__amU24cache6_8;
	// System.Action PluginXiaoMai::<>f__am$cache7
	Action_t3771233898 * ___U3CU3Ef__amU24cache7_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_8() { return static_cast<int32_t>(offsetof(PluginXiaoMai_t2024051491_StaticFields, ___U3CU3Ef__amU24cache6_8)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache6_8() const { return ___U3CU3Ef__amU24cache6_8; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache6_8() { return &___U3CU3Ef__amU24cache6_8; }
	inline void set_U3CU3Ef__amU24cache6_8(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache6_8 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache6_8, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_9() { return static_cast<int32_t>(offsetof(PluginXiaoMai_t2024051491_StaticFields, ___U3CU3Ef__amU24cache7_9)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache7_9() const { return ___U3CU3Ef__amU24cache7_9; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache7_9() { return &___U3CU3Ef__amU24cache7_9; }
	inline void set_U3CU3Ef__amU24cache7_9(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache7_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache7_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
