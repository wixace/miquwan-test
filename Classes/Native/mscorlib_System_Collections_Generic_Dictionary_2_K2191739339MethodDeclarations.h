﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Ke962647886MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define KeyCollection__ctor_m2867284644(__this, ___dictionary0, method) ((  void (*) (KeyCollection_t2191739339 *, Dictionary_2_t564979888 *, const MethodInfo*))KeyCollection__ctor_m1657465302_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.Generic.ICollection<TKey>.Add(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m2017130546(__this, ___item0, method) ((  void (*) (KeyCollection_t2191739339 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Add_m1679368640_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.Generic.ICollection<TKey>.Clear()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m2598732329(__this, method) ((  void (*) (KeyCollection_t2191739339 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Clear_m1606203191_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.Generic.ICollection<TKey>.Contains(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m311284444(__this, ___item0, method) ((  bool (*) (KeyCollection_t2191739339 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Contains_m4028784330_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.Generic.ICollection<TKey>.Remove(TKey)
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m3098102593(__this, ___item0, method) ((  bool (*) (KeyCollection_t2191739339 *, uint8_t, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_Remove_m1814821551_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TKey> System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.Generic.IEnumerable<TKey>.GetEnumerator()
#define KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m1550164859(__this, method) ((  Il2CppObject* (*) (KeyCollection_t2191739339 *, const MethodInfo*))KeyCollection_System_Collections_Generic_IEnumerableU3CTKeyU3E_GetEnumerator_m4103627315_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define KeyCollection_System_Collections_ICollection_CopyTo_m3893391643(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2191739339 *, Il2CppArray *, int32_t, const MethodInfo*))KeyCollection_System_Collections_ICollection_CopyTo_m1307214121_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.IEnumerable.GetEnumerator()
#define KeyCollection_System_Collections_IEnumerable_GetEnumerator_m1028697706(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2191739339 *, const MethodInfo*))KeyCollection_System_Collections_IEnumerable_GetEnumerator_m2765238372_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.Generic.ICollection<TKey>.get_IsReadOnly()
#define KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m1377698621(__this, method) ((  bool (*) (KeyCollection_t2191739339 *, const MethodInfo*))KeyCollection_System_Collections_Generic_ICollectionU3CTKeyU3E_get_IsReadOnly_m482298795_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.ICollection.get_IsSynchronized()
#define KeyCollection_System_Collections_ICollection_get_IsSynchronized_m3218244655(__this, method) ((  bool (*) (KeyCollection_t2191739339 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_IsSynchronized_m959503901_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::System.Collections.ICollection.get_SyncRoot()
#define KeyCollection_System_Collections_ICollection_get_SyncRoot_m597442465(__this, method) ((  Il2CppObject * (*) (KeyCollection_t2191739339 *, const MethodInfo*))KeyCollection_System_Collections_ICollection_get_SyncRoot_m2273377481_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::CopyTo(TKey[],System.Int32)
#define KeyCollection_CopyTo_m2760393049(__this, ___array0, ___index1, method) ((  void (*) (KeyCollection_t2191739339 *, EAIEventtypeU5BU5D_t2693623942*, int32_t, const MethodInfo*))KeyCollection_CopyTo_m2376411275_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::GetEnumerator()
#define KeyCollection_GetEnumerator_m3023197222(__this, method) ((  Enumerator_t1179915942  (*) (KeyCollection_t2191739339 *, const MethodInfo*))KeyCollection_GetEnumerator_m626970286_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/KeyCollection<AIEnum.EAIEventtype,System.Collections.Generic.List`1<AIEventInfo>>::get_Count()
#define KeyCollection_get_Count_m389650153(__this, method) ((  int32_t (*) (KeyCollection_t2191739339 *, const MethodInfo*))KeyCollection_get_Count_m864530531_gshared)(__this, method)
