﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// NGUITextGenerated
struct NGUITextGenerated_t2993363413;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// BetterList`1<UnityEngine.Vector3>
struct BetterList_1_t1484067282;
// System.String
struct String_t;
// BetterList`1<System.Int32>
struct BetterList_1_t2650806512;
// BetterList`1<UnityEngine.Vector2>
struct BetterList_1_t1484067281;
// BetterList`1<UnityEngine.Color32>
struct BetterList_1_t2095821700;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

// System.Void NGUITextGenerated::.ctor()
extern "C"  void NGUITextGenerated__ctor_m3673150262 (NGUITextGenerated_t2993363413 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_bitmapFont(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_bitmapFont_m2185533616 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_dynamicFont(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_dynamicFont_m3321904528 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_glyph(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_glyph_m1827413330 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_fontSize(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_fontSize_m167249950 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_fontScale(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_fontScale_m1298849059 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_pixelDensity(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_pixelDensity_m1283543500 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_fontStyle(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_fontStyle_m3703950492 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_alignment(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_alignment_m1224097403 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_tint(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_tint_m3722475123 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_rectWidth(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_rectWidth_m1710909692 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_rectHeight(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_rectHeight_m2154036547 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_regionWidth(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_regionWidth_m3289783308 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_regionHeight(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_regionHeight_m3854478387 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_maxLines(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_maxLines_m3951684979 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_gradient(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_gradient_m124238302 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_gradientBottom(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_gradientBottom_m1790670323 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_gradientTop(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_gradientTop_m1781687449 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_encoding(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_encoding_m1451735835 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_spacingX(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_spacingX_m2773625401 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_spacingY(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_spacingY_m2577111896 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_premultiply(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_premultiply_m970980631 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_symbolStyle(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_symbolStyle_m1171375141 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_finalSize(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_finalSize_m1185808455 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_finalSpacingX(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_finalSpacingX_m3426639187 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_finalLineHeight(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_finalLineHeight_m488217037 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_baseline(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_baseline_m1186198121 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::NGUIText_useSymbols(JSVCall)
extern "C"  void NGUITextGenerated_NGUIText_useSymbols_m2417139002 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_Align__BetterListT1_Vector3__Int32__Single__Int32(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_Align__BetterListT1_Vector3__Int32__Single__Int32_m2727072894 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_CalculateOffsetToFit__String(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_CalculateOffsetToFit__String_m4157655147 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_CalculatePrintedSize__String(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_CalculatePrintedSize__String_m696314453 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_EncodeAlpha__Single(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_EncodeAlpha__Single_m2015615349 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_EncodeColor__String__Color(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_EncodeColor__String__Color_m580163906 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_EncodeColor__Color(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_EncodeColor__Color_m1158943411 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_EncodeColor24__Color(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_EncodeColor24__Color_m256010225 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_EncodeColor32__Color(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_EncodeColor32__Color_m584177908 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_EndLine__StringBuilder(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_EndLine__StringBuilder_m3604219416 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_GetApproximateCharacterIndex__BetterListT1_Vector3__BetterListT1_Int32__Vector2(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_GetApproximateCharacterIndex__BetterListT1_Vector3__BetterListT1_Int32__Vector2_m3870385047 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_GetEndOfLineThatFits__String(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_GetEndOfLineThatFits__String_m4234427719 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_GetExactCharacterIndex__BetterListT1_Vector3__BetterListT1_Int32__Vector2(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_GetExactCharacterIndex__BetterListT1_Vector3__BetterListT1_Int32__Vector2_m17042124 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_GetGlyph__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_GetGlyph__Int32__Int32_m1535286803 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_GetGlyphWidth__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_GetGlyphWidth__Int32__Int32_m1967360117 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_GetSymbol__String__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_GetSymbol__String__Int32__Int32_m1844710820 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_IsHex__Char(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_IsHex__Char_m2138576172 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_ParseAlpha__String__Int32(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_ParseAlpha__String__Int32_m446609239 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_ParseColor__String__Int32(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_ParseColor__String__Int32_m1750403186 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_ParseColor24__String__Int32(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_ParseColor24__String__Int32_m3687432816 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_ParseColor32__String__Int32(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_ParseColor32__String__Int32_m3794108531 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_ParseSymbol__String__Int32__BetterListT1_Color__Boolean__Int32__Boolean__Boolean__Boolean__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_ParseSymbol__String__Int32__BetterListT1_Color__Boolean__Int32__Boolean__Boolean__Boolean__Boolean__Boolean_m1318280518 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_ParseSymbol__String__Int32(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_ParseSymbol__String__Int32_m901122479 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_Prepare__String(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_Prepare__String_m2730421853 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_Print__String__BetterListT1_Vector3__BetterListT1_Vector2__BetterListT1_Color32(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_Print__String__BetterListT1_Vector3__BetterListT1_Vector2__BetterListT1_Color32_m650754856 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_PrintApproximateCharacterPositions__String__BetterListT1_Vector3__BetterListT1_Int32(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_PrintApproximateCharacterPositions__String__BetterListT1_Vector3__BetterListT1_Int32_m222692076 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_PrintCaretAndSelection__String__Int32__Int32__BetterListT1_Vector3__BetterListT1_Vector3(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_PrintCaretAndSelection__String__Int32__Int32__BetterListT1_Vector3__BetterListT1_Vector3_m1403314961 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_PrintExactCharacterPositions__String__BetterListT1_Vector3__BetterListT1_Int32(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_PrintExactCharacterPositions__String__BetterListT1_Vector3__BetterListT1_Int32_m664669783 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_StripSymbols__String(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_StripSymbols__String_m3762998801 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_Update__Boolean(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_Update__Boolean_m3113600644 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_Update(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_Update_m3229469030 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_WrapText__String__String__Boolean__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_WrapText__String__String__Boolean__Boolean__Boolean_m3996821716 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::NGUIText_WrapText__String__String__Boolean(JSVCall,System.Int32)
extern "C"  bool NGUITextGenerated_NGUIText_WrapText__String__String__Boolean_m3091625620 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::__Register()
extern "C"  void NGUITextGenerated___Register_m1925397329 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NGUITextGenerated::ilo_setObject1(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t NGUITextGenerated_ilo_setObject1_m913499064 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NGUITextGenerated::ilo_getSingle2(System.Int32)
extern "C"  float NGUITextGenerated_ilo_getSingle2_m3015796698 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::ilo_setEnum3(System.Int32,System.Int32)
extern "C"  void NGUITextGenerated_ilo_setEnum3_m1718458261 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NGUITextGenerated::ilo_getEnum4(System.Int32)
extern "C"  int32_t NGUITextGenerated_ilo_getEnum4_m3533402113 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object NGUITextGenerated::ilo_getObject5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * NGUITextGenerated_ilo_getObject5_m3430014771 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::ilo_setInt326(System.Int32,System.Int32)
extern "C"  void NGUITextGenerated_ilo_setInt326_m2181997627 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NGUITextGenerated::ilo_getInt327(System.Int32)
extern "C"  int32_t NGUITextGenerated_ilo_getInt327_m1099315935 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::ilo_getBooleanS8(System.Int32)
extern "C"  bool NGUITextGenerated_ilo_getBooleanS8_m3923442101 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::ilo_setSingle9(System.Int32,System.Single)
extern "C"  void NGUITextGenerated_ilo_setSingle9_m2997054854 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::ilo_Align10(BetterList`1<UnityEngine.Vector3>,System.Int32,System.Single,System.Int32)
extern "C"  void NGUITextGenerated_ilo_Align10_m3231297062 (Il2CppObject * __this /* static, unused */, BetterList_1_t1484067282 * ___verts0, int32_t ___indexOffset1, float ___printedWidth2, int32_t ___elements3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String NGUITextGenerated::ilo_getStringS11(System.Int32)
extern "C"  String_t* NGUITextGenerated_ilo_getStringS11_m4031625467 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NGUITextGenerated::ilo_CalculateOffsetToFit12(System.String)
extern "C"  int32_t NGUITextGenerated_ilo_CalculateOffsetToFit12_m3456124625 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::ilo_setVector2S13(System.Int32,UnityEngine.Vector2)
extern "C"  void NGUITextGenerated_ilo_setVector2S13_m388868526 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String NGUITextGenerated::ilo_EncodeColor14(System.String,UnityEngine.Color)
extern "C"  String_t* NGUITextGenerated_ilo_EncodeColor14_m296034158 (Il2CppObject * __this /* static, unused */, String_t* ___text0, Color_t4194546905  ___c1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::ilo_setStringS15(System.Int32,System.String)
extern "C"  void NGUITextGenerated_ilo_setStringS15_m3549322482 (Il2CppObject * __this /* static, unused */, int32_t ___e0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String NGUITextGenerated::ilo_EncodeColor3216(UnityEngine.Color)
extern "C"  String_t* NGUITextGenerated_ilo_EncodeColor3216_m1009602929 (Il2CppObject * __this /* static, unused */, Color_t4194546905  ___c0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NGUITextGenerated::ilo_GetApproximateCharacterIndex17(BetterList`1<UnityEngine.Vector3>,BetterList`1<System.Int32>,UnityEngine.Vector2)
extern "C"  int32_t NGUITextGenerated_ilo_GetApproximateCharacterIndex17_m1899282104 (Il2CppObject * __this /* static, unused */, BetterList_1_t1484067282 * ___verts0, BetterList_1_t2650806512 * ___indices1, Vector2_t4282066565  ___pos2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String NGUITextGenerated::ilo_GetEndOfLineThatFits18(System.String)
extern "C"  String_t* NGUITextGenerated_ilo_GetEndOfLineThatFits18_m1549013572 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NGUITextGenerated::ilo_GetGlyphWidth19(System.Int32,System.Int32)
extern "C"  float NGUITextGenerated_ilo_GetGlyphWidth19_m871225505 (Il2CppObject * __this /* static, unused */, int32_t ___ch0, int32_t ___prev1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 NGUITextGenerated::ilo_getChar20(System.Int32)
extern "C"  int16_t NGUITextGenerated_ilo_getChar20_m3083743172 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single NGUITextGenerated::ilo_ParseAlpha21(System.String,System.Int32)
extern "C"  float NGUITextGenerated_ilo_ParseAlpha21_m1854342964 (Il2CppObject * __this /* static, unused */, String_t* ___text0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color NGUITextGenerated::ilo_ParseColor3222(System.String,System.Int32)
extern "C"  Color_t4194546905  NGUITextGenerated_ilo_ParseColor3222_m2521159678 (Il2CppObject * __this /* static, unused */, String_t* ___text0, int32_t ___offset1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NGUITextGenerated::ilo_getArgIndex23()
extern "C"  int32_t NGUITextGenerated_ilo_getArgIndex23_m3935939458 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::ilo_Print24(System.String,BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void NGUITextGenerated_ilo_Print24_m2240810145 (Il2CppObject * __this /* static, unused */, String_t* ___text0, BetterList_1_t1484067282 * ___verts1, BetterList_1_t1484067281 * ___uvs2, BetterList_1_t2095821700 * ___cols3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::ilo_PrintApproximateCharacterPositions25(System.String,BetterList`1<UnityEngine.Vector3>,BetterList`1<System.Int32>)
extern "C"  void NGUITextGenerated_ilo_PrintApproximateCharacterPositions25_m2488100692 (Il2CppObject * __this /* static, unused */, String_t* ___text0, BetterList_1_t1484067282 * ___verts1, BetterList_1_t2650806512 * ___indices2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::ilo_PrintExactCharacterPositions26(System.String,BetterList`1<UnityEngine.Vector3>,BetterList`1<System.Int32>)
extern "C"  void NGUITextGenerated_ilo_PrintExactCharacterPositions26_m3959331840 (Il2CppObject * __this /* static, unused */, String_t* ___text0, BetterList_1_t1484067282 * ___verts1, BetterList_1_t2650806512 * ___indices2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String NGUITextGenerated::ilo_StripSymbols27(System.String)
extern "C"  String_t* NGUITextGenerated_ilo_StripSymbols27_m2751275100 (Il2CppObject * __this /* static, unused */, String_t* ___text0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 NGUITextGenerated::ilo_incArgIndex28()
extern "C"  int32_t NGUITextGenerated_ilo_incArgIndex28_m3093904079 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::ilo_WrapText29(System.String,System.String&,System.Boolean,System.Boolean,System.Boolean)
extern "C"  bool NGUITextGenerated_ilo_WrapText29_m2999186756 (Il2CppObject * __this /* static, unused */, String_t* ___text0, String_t** ___finalText1, bool ___keepCharCount2, bool ___wrapLineColors3, bool ___useEllipsis4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::ilo_setBooleanS30(System.Int32,System.Boolean)
extern "C"  void NGUITextGenerated_ilo_setBooleanS30_m3054756225 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void NGUITextGenerated::ilo_setArgIndex31(System.Int32)
extern "C"  void NGUITextGenerated_ilo_setArgIndex31_m2153627054 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean NGUITextGenerated::ilo_WrapText32(System.String,System.String&,System.Boolean)
extern "C"  bool NGUITextGenerated_ilo_WrapText32_m3200036044 (Il2CppObject * __this /* static, unused */, String_t* ___text0, String_t** ___finalText1, bool ___wrapLineColors2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
