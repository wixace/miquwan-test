﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// T4MObjSC
struct T4MObjSC_t1553158042;
// T4MLodObjSC
struct T4MLodObjSC_t2288258003;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_T4MLodObjSC2288258003.h"

// System.Void T4MObjSC::.ctor()
extern "C"  void T4MObjSC__ctor_m3211025473 (T4MObjSC_t1553158042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void T4MObjSC::Awake()
extern "C"  void T4MObjSC_Awake_m3448630692 (T4MObjSC_t1553158042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void T4MObjSC::OnGUI()
extern "C"  void T4MObjSC_OnGUI_m2706424123 (T4MObjSC_t1553158042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void T4MObjSC::LateUpdate()
extern "C"  void T4MObjSC_LateUpdate_m615870130 (T4MObjSC_t1553158042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void T4MObjSC::BillScrpt()
extern "C"  void T4MObjSC_BillScrpt_m71305342 (T4MObjSC_t1553158042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void T4MObjSC::BillLay()
extern "C"  void T4MObjSC_BillLay_m3457237692 (T4MObjSC_t1553158042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void T4MObjSC::LODScript()
extern "C"  void T4MObjSC_LODScript_m495476427 (T4MObjSC_t1553158042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void T4MObjSC::LODLay()
extern "C"  void T4MObjSC_LODLay_m3820056134 (T4MObjSC_t1553158042 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void T4MObjSC::ilo_ActivateLODScrpt1(T4MLodObjSC)
extern "C"  void T4MObjSC_ilo_ActivateLODScrpt1_m3478189338 (Il2CppObject * __this /* static, unused */, T4MLodObjSC_t2288258003 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
