﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_sarnaJejehe228
struct  M_sarnaJejehe228_t111731294  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_sarnaJejehe228::_kowniNiher
	bool ____kowniNiher_0;
	// System.Boolean GarbageiOS.M_sarnaJejehe228::_bearimaHarnoohe
	bool ____bearimaHarnoohe_1;
	// System.Boolean GarbageiOS.M_sarnaJejehe228::_sohoFodral
	bool ____sohoFodral_2;
	// System.Boolean GarbageiOS.M_sarnaJejehe228::_sirjooNenatair
	bool ____sirjooNenatair_3;
	// System.UInt32 GarbageiOS.M_sarnaJejehe228::_kawreejoPooreresa
	uint32_t ____kawreejoPooreresa_4;
	// System.Boolean GarbageiOS.M_sarnaJejehe228::_wafeDetai
	bool ____wafeDetai_5;
	// System.Boolean GarbageiOS.M_sarnaJejehe228::_poowo
	bool ____poowo_6;
	// System.UInt32 GarbageiOS.M_sarnaJejehe228::_dalldrur
	uint32_t ____dalldrur_7;
	// System.Int32 GarbageiOS.M_sarnaJejehe228::_kanircea
	int32_t ____kanircea_8;
	// System.Single GarbageiOS.M_sarnaJejehe228::_housope
	float ____housope_9;
	// System.String GarbageiOS.M_sarnaJejehe228::_lirsirqe
	String_t* ____lirsirqe_10;
	// System.String GarbageiOS.M_sarnaJejehe228::_cotasjaw
	String_t* ____cotasjaw_11;
	// System.Single GarbageiOS.M_sarnaJejehe228::_girbair
	float ____girbair_12;
	// System.Int32 GarbageiOS.M_sarnaJejehe228::_learpaCuta
	int32_t ____learpaCuta_13;
	// System.Single GarbageiOS.M_sarnaJejehe228::_warneaCurhi
	float ____warneaCurhi_14;
	// System.Int32 GarbageiOS.M_sarnaJejehe228::_kouwal
	int32_t ____kouwal_15;
	// System.Single GarbageiOS.M_sarnaJejehe228::_rastaPoodijir
	float ____rastaPoodijir_16;
	// System.String GarbageiOS.M_sarnaJejehe228::_moutastraPearferdar
	String_t* ____moutastraPearferdar_17;
	// System.Int32 GarbageiOS.M_sarnaJejehe228::_wouwhearSairnor
	int32_t ____wouwhearSairnor_18;

public:
	inline static int32_t get_offset_of__kowniNiher_0() { return static_cast<int32_t>(offsetof(M_sarnaJejehe228_t111731294, ____kowniNiher_0)); }
	inline bool get__kowniNiher_0() const { return ____kowniNiher_0; }
	inline bool* get_address_of__kowniNiher_0() { return &____kowniNiher_0; }
	inline void set__kowniNiher_0(bool value)
	{
		____kowniNiher_0 = value;
	}

	inline static int32_t get_offset_of__bearimaHarnoohe_1() { return static_cast<int32_t>(offsetof(M_sarnaJejehe228_t111731294, ____bearimaHarnoohe_1)); }
	inline bool get__bearimaHarnoohe_1() const { return ____bearimaHarnoohe_1; }
	inline bool* get_address_of__bearimaHarnoohe_1() { return &____bearimaHarnoohe_1; }
	inline void set__bearimaHarnoohe_1(bool value)
	{
		____bearimaHarnoohe_1 = value;
	}

	inline static int32_t get_offset_of__sohoFodral_2() { return static_cast<int32_t>(offsetof(M_sarnaJejehe228_t111731294, ____sohoFodral_2)); }
	inline bool get__sohoFodral_2() const { return ____sohoFodral_2; }
	inline bool* get_address_of__sohoFodral_2() { return &____sohoFodral_2; }
	inline void set__sohoFodral_2(bool value)
	{
		____sohoFodral_2 = value;
	}

	inline static int32_t get_offset_of__sirjooNenatair_3() { return static_cast<int32_t>(offsetof(M_sarnaJejehe228_t111731294, ____sirjooNenatair_3)); }
	inline bool get__sirjooNenatair_3() const { return ____sirjooNenatair_3; }
	inline bool* get_address_of__sirjooNenatair_3() { return &____sirjooNenatair_3; }
	inline void set__sirjooNenatair_3(bool value)
	{
		____sirjooNenatair_3 = value;
	}

	inline static int32_t get_offset_of__kawreejoPooreresa_4() { return static_cast<int32_t>(offsetof(M_sarnaJejehe228_t111731294, ____kawreejoPooreresa_4)); }
	inline uint32_t get__kawreejoPooreresa_4() const { return ____kawreejoPooreresa_4; }
	inline uint32_t* get_address_of__kawreejoPooreresa_4() { return &____kawreejoPooreresa_4; }
	inline void set__kawreejoPooreresa_4(uint32_t value)
	{
		____kawreejoPooreresa_4 = value;
	}

	inline static int32_t get_offset_of__wafeDetai_5() { return static_cast<int32_t>(offsetof(M_sarnaJejehe228_t111731294, ____wafeDetai_5)); }
	inline bool get__wafeDetai_5() const { return ____wafeDetai_5; }
	inline bool* get_address_of__wafeDetai_5() { return &____wafeDetai_5; }
	inline void set__wafeDetai_5(bool value)
	{
		____wafeDetai_5 = value;
	}

	inline static int32_t get_offset_of__poowo_6() { return static_cast<int32_t>(offsetof(M_sarnaJejehe228_t111731294, ____poowo_6)); }
	inline bool get__poowo_6() const { return ____poowo_6; }
	inline bool* get_address_of__poowo_6() { return &____poowo_6; }
	inline void set__poowo_6(bool value)
	{
		____poowo_6 = value;
	}

	inline static int32_t get_offset_of__dalldrur_7() { return static_cast<int32_t>(offsetof(M_sarnaJejehe228_t111731294, ____dalldrur_7)); }
	inline uint32_t get__dalldrur_7() const { return ____dalldrur_7; }
	inline uint32_t* get_address_of__dalldrur_7() { return &____dalldrur_7; }
	inline void set__dalldrur_7(uint32_t value)
	{
		____dalldrur_7 = value;
	}

	inline static int32_t get_offset_of__kanircea_8() { return static_cast<int32_t>(offsetof(M_sarnaJejehe228_t111731294, ____kanircea_8)); }
	inline int32_t get__kanircea_8() const { return ____kanircea_8; }
	inline int32_t* get_address_of__kanircea_8() { return &____kanircea_8; }
	inline void set__kanircea_8(int32_t value)
	{
		____kanircea_8 = value;
	}

	inline static int32_t get_offset_of__housope_9() { return static_cast<int32_t>(offsetof(M_sarnaJejehe228_t111731294, ____housope_9)); }
	inline float get__housope_9() const { return ____housope_9; }
	inline float* get_address_of__housope_9() { return &____housope_9; }
	inline void set__housope_9(float value)
	{
		____housope_9 = value;
	}

	inline static int32_t get_offset_of__lirsirqe_10() { return static_cast<int32_t>(offsetof(M_sarnaJejehe228_t111731294, ____lirsirqe_10)); }
	inline String_t* get__lirsirqe_10() const { return ____lirsirqe_10; }
	inline String_t** get_address_of__lirsirqe_10() { return &____lirsirqe_10; }
	inline void set__lirsirqe_10(String_t* value)
	{
		____lirsirqe_10 = value;
		Il2CppCodeGenWriteBarrier(&____lirsirqe_10, value);
	}

	inline static int32_t get_offset_of__cotasjaw_11() { return static_cast<int32_t>(offsetof(M_sarnaJejehe228_t111731294, ____cotasjaw_11)); }
	inline String_t* get__cotasjaw_11() const { return ____cotasjaw_11; }
	inline String_t** get_address_of__cotasjaw_11() { return &____cotasjaw_11; }
	inline void set__cotasjaw_11(String_t* value)
	{
		____cotasjaw_11 = value;
		Il2CppCodeGenWriteBarrier(&____cotasjaw_11, value);
	}

	inline static int32_t get_offset_of__girbair_12() { return static_cast<int32_t>(offsetof(M_sarnaJejehe228_t111731294, ____girbair_12)); }
	inline float get__girbair_12() const { return ____girbair_12; }
	inline float* get_address_of__girbair_12() { return &____girbair_12; }
	inline void set__girbair_12(float value)
	{
		____girbair_12 = value;
	}

	inline static int32_t get_offset_of__learpaCuta_13() { return static_cast<int32_t>(offsetof(M_sarnaJejehe228_t111731294, ____learpaCuta_13)); }
	inline int32_t get__learpaCuta_13() const { return ____learpaCuta_13; }
	inline int32_t* get_address_of__learpaCuta_13() { return &____learpaCuta_13; }
	inline void set__learpaCuta_13(int32_t value)
	{
		____learpaCuta_13 = value;
	}

	inline static int32_t get_offset_of__warneaCurhi_14() { return static_cast<int32_t>(offsetof(M_sarnaJejehe228_t111731294, ____warneaCurhi_14)); }
	inline float get__warneaCurhi_14() const { return ____warneaCurhi_14; }
	inline float* get_address_of__warneaCurhi_14() { return &____warneaCurhi_14; }
	inline void set__warneaCurhi_14(float value)
	{
		____warneaCurhi_14 = value;
	}

	inline static int32_t get_offset_of__kouwal_15() { return static_cast<int32_t>(offsetof(M_sarnaJejehe228_t111731294, ____kouwal_15)); }
	inline int32_t get__kouwal_15() const { return ____kouwal_15; }
	inline int32_t* get_address_of__kouwal_15() { return &____kouwal_15; }
	inline void set__kouwal_15(int32_t value)
	{
		____kouwal_15 = value;
	}

	inline static int32_t get_offset_of__rastaPoodijir_16() { return static_cast<int32_t>(offsetof(M_sarnaJejehe228_t111731294, ____rastaPoodijir_16)); }
	inline float get__rastaPoodijir_16() const { return ____rastaPoodijir_16; }
	inline float* get_address_of__rastaPoodijir_16() { return &____rastaPoodijir_16; }
	inline void set__rastaPoodijir_16(float value)
	{
		____rastaPoodijir_16 = value;
	}

	inline static int32_t get_offset_of__moutastraPearferdar_17() { return static_cast<int32_t>(offsetof(M_sarnaJejehe228_t111731294, ____moutastraPearferdar_17)); }
	inline String_t* get__moutastraPearferdar_17() const { return ____moutastraPearferdar_17; }
	inline String_t** get_address_of__moutastraPearferdar_17() { return &____moutastraPearferdar_17; }
	inline void set__moutastraPearferdar_17(String_t* value)
	{
		____moutastraPearferdar_17 = value;
		Il2CppCodeGenWriteBarrier(&____moutastraPearferdar_17, value);
	}

	inline static int32_t get_offset_of__wouwhearSairnor_18() { return static_cast<int32_t>(offsetof(M_sarnaJejehe228_t111731294, ____wouwhearSairnor_18)); }
	inline int32_t get__wouwhearSairnor_18() const { return ____wouwhearSairnor_18; }
	inline int32_t* get_address_of__wouwhearSairnor_18() { return &____wouwhearSairnor_18; }
	inline void set__wouwhearSairnor_18(int32_t value)
	{
		____wouwhearSairnor_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
