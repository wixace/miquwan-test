﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._a74631a3718d786f597a7e924f5b555f
struct _a74631a3718d786f597a7e924f5b555f_t3594651005;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._a74631a3718d786f597a7e924f5b555f::.ctor()
extern "C"  void _a74631a3718d786f597a7e924f5b555f__ctor_m2261974352 (_a74631a3718d786f597a7e924f5b555f_t3594651005 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a74631a3718d786f597a7e924f5b555f::_a74631a3718d786f597a7e924f5b555fm2(System.Int32)
extern "C"  int32_t _a74631a3718d786f597a7e924f5b555f__a74631a3718d786f597a7e924f5b555fm2_m1129166969 (_a74631a3718d786f597a7e924f5b555f_t3594651005 * __this, int32_t ____a74631a3718d786f597a7e924f5b555fa0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._a74631a3718d786f597a7e924f5b555f::_a74631a3718d786f597a7e924f5b555fm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _a74631a3718d786f597a7e924f5b555f__a74631a3718d786f597a7e924f5b555fm_m1959160413 (_a74631a3718d786f597a7e924f5b555f_t3594651005 * __this, int32_t ____a74631a3718d786f597a7e924f5b555fa0, int32_t ____a74631a3718d786f597a7e924f5b555f421, int32_t ____a74631a3718d786f597a7e924f5b555fc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
