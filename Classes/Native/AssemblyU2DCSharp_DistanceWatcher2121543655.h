﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DistanceWatcher
struct  DistanceWatcher_t2121543655  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Transform DistanceWatcher::reference
	Transform_t1659122786 * ___reference_2;

public:
	inline static int32_t get_offset_of_reference_2() { return static_cast<int32_t>(offsetof(DistanceWatcher_t2121543655, ___reference_2)); }
	inline Transform_t1659122786 * get_reference_2() const { return ___reference_2; }
	inline Transform_t1659122786 ** get_address_of_reference_2() { return &___reference_2; }
	inline void set_reference_2(Transform_t1659122786 * value)
	{
		___reference_2 = value;
		Il2CppCodeGenWriteBarrier(&___reference_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
