﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.TypeSerializer
struct TypeSerializer_t1237465094;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// ProtoBuf.Serializers.IProtoSerializer[]
struct IProtoSerializerU5BU5D_t3735579626;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t2824366364;
// ProtoBuf.Meta.CallbackSet
struct CallbackSet_t3590429999;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.SerializationContext
struct SerializationContext_t3997850667;
// ProtoBuf.Serializers.IProtoSerializer
struct IProtoSerializer_t3033312651;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// ProtoBuf.IExtensible
struct IExtensible_t1056931882;
// ProtoBuf.Serializers.IProtoTypeSerializer
struct IProtoTypeSerializer_t321624293;
// System.Exception
struct Exception_t3991598821;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_CallbackSet3590429999.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel_Callback2866957669.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_SerializationContext3997850667.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"
#include "AssemblyU2DCSharp_ProtoBuf_Serializers_TypeSeriali1237465094.h"

// System.Void ProtoBuf.Serializers.TypeSerializer::.ctor(ProtoBuf.Meta.TypeModel,System.Type,System.Int32[],ProtoBuf.Serializers.IProtoSerializer[],System.Reflection.MethodInfo[],System.Boolean,System.Boolean,ProtoBuf.Meta.CallbackSet,System.Type,System.Reflection.MethodInfo)
extern "C"  void TypeSerializer__ctor_m1140404683 (TypeSerializer_t1237465094 * __this, TypeModel_t2730011105 * ___model0, Type_t * ___forType1, Int32U5BU5D_t3230847821* ___fieldNumbers2, IProtoSerializerU5BU5D_t3735579626* ___serializers3, MethodInfoU5BU5D_t2824366364* ___baseCtorCallbacks4, bool ___isRootType5, bool ___useConstructor6, CallbackSet_t3590429999 * ___callbacks7, Type_t * ___constructType8, MethodInfo_t * ___factory9, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.TypeSerializer::.cctor()
extern "C"  void TypeSerializer__cctor_m3784506366 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.TypeSerializer::ProtoBuf.Serializers.IProtoTypeSerializer.CanCreateInstance()
extern "C"  bool TypeSerializer_ProtoBuf_Serializers_IProtoTypeSerializer_CanCreateInstance_m2802205839 (TypeSerializer_t1237465094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.TypeSerializer::ProtoBuf.Serializers.IProtoTypeSerializer.CreateInstance(ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * TypeSerializer_ProtoBuf_Serializers_IProtoTypeSerializer_CreateInstance_m2797880316 (TypeSerializer_t1237465094 * __this, ProtoReader_t3962509489 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.TypeSerializer::ProtoBuf.Serializers.IProtoSerializer.get_RequiresOldValue()
extern "C"  bool TypeSerializer_ProtoBuf_Serializers_IProtoSerializer_get_RequiresOldValue_m2186071855 (TypeSerializer_t1237465094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.TypeSerializer::ProtoBuf.Serializers.IProtoSerializer.get_ReturnsValue()
extern "C"  bool TypeSerializer_ProtoBuf_Serializers_IProtoSerializer_get_ReturnsValue_m3010834757 (TypeSerializer_t1237465094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.TypeSerializer::HasCallbacks(ProtoBuf.Meta.TypeModel/CallbackType)
extern "C"  bool TypeSerializer_HasCallbacks_m2870980144 (TypeSerializer_t1237465094 * __this, int32_t ___callbackType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.TypeSerializer::get_ExpectedType()
extern "C"  Type_t * TypeSerializer_get_ExpectedType_m2709699862 (TypeSerializer_t1237465094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.TypeSerializer::get_CanHaveInheritance()
extern "C"  bool TypeSerializer_get_CanHaveInheritance_m4113518524 (TypeSerializer_t1237465094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.TypeSerializer::Callback(System.Object,ProtoBuf.Meta.TypeModel/CallbackType,ProtoBuf.SerializationContext)
extern "C"  void TypeSerializer_Callback_m388228401 (TypeSerializer_t1237465094 * __this, Il2CppObject * ___value0, int32_t ___callbackType1, SerializationContext_t3997850667 * ___context2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Serializers.IProtoSerializer ProtoBuf.Serializers.TypeSerializer::GetMoreSpecificSerializer(System.Object)
extern "C"  Il2CppObject * TypeSerializer_GetMoreSpecificSerializer_m142951110 (TypeSerializer_t1237465094 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.TypeSerializer::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void TypeSerializer_Write_m3430328134 (TypeSerializer_t1237465094 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.TypeSerializer::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * TypeSerializer_Read_m3991344576 (TypeSerializer_t1237465094 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.TypeSerializer::InvokeCallback(System.Reflection.MethodInfo,System.Object,ProtoBuf.SerializationContext)
extern "C"  Il2CppObject * TypeSerializer_InvokeCallback_m2505896706 (TypeSerializer_t1237465094 * __this, MethodInfo_t * ___method0, Il2CppObject * ___obj1, SerializationContext_t3997850667 * ___context2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.TypeSerializer::CreateInstance(ProtoBuf.ProtoReader,System.Boolean)
extern "C"  Il2CppObject * TypeSerializer_CreateInstance_m2923395444 (TypeSerializer_t1237465094 * __this, ProtoReader_t3962509489 * ___source0, bool ___includeLocalCallback1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.TypeSerializer::ilo_get_ExpectedType1(ProtoBuf.Serializers.IProtoSerializer)
extern "C"  Type_t * TypeSerializer_ilo_get_ExpectedType1_m1267287751 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.TypeSerializer::ilo_get_CanHaveInheritance2(ProtoBuf.Serializers.TypeSerializer)
extern "C"  bool TypeSerializer_ilo_get_CanHaveInheritance2_m663103089 (Il2CppObject * __this /* static, unused */, TypeSerializer_t1237465094 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.SerializationContext ProtoBuf.Serializers.TypeSerializer::ilo_get_Context3(ProtoBuf.ProtoWriter)
extern "C"  SerializationContext_t3997850667 * TypeSerializer_ilo_get_Context3_m1365788108 (Il2CppObject * __this /* static, unused */, ProtoWriter_t4117914721 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// ProtoBuf.Serializers.IProtoSerializer ProtoBuf.Serializers.TypeSerializer::ilo_GetMoreSpecificSerializer4(ProtoBuf.Serializers.TypeSerializer,System.Object)
extern "C"  Il2CppObject * TypeSerializer_ilo_GetMoreSpecificSerializer4_m3571678409 (Il2CppObject * __this /* static, unused */, TypeSerializer_t1237465094 * ____this0, Il2CppObject * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.TypeSerializer::ilo_AppendExtensionData5(ProtoBuf.IExtensible,ProtoBuf.ProtoWriter)
extern "C"  void TypeSerializer_ilo_AppendExtensionData5_m1609039489 (Il2CppObject * __this /* static, unused */, Il2CppObject * ___instance0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.TypeSerializer::ilo_CreateInstance6(ProtoBuf.Serializers.IProtoTypeSerializer,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * TypeSerializer_ilo_CreateInstance6_m1283531771 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.TypeSerializer::ilo_get_ReturnsValue7(ProtoBuf.Serializers.IProtoSerializer)
extern "C"  bool TypeSerializer_ilo_get_ReturnsValue7_m2260823939 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.TypeSerializer::ilo_Read8(ProtoBuf.Serializers.IProtoSerializer,System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * TypeSerializer_ilo_Read8_m3820202064 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___value1, ProtoReader_t3962509489 * ___source2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.TypeSerializer::ilo_SkipField9(ProtoBuf.ProtoReader)
extern "C"  void TypeSerializer_ilo_SkipField9_m3591204254 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 ProtoBuf.Serializers.TypeSerializer::ilo_ReadFieldHeader10(ProtoBuf.ProtoReader)
extern "C"  int32_t TypeSerializer_ilo_ReadFieldHeader10_m4139316720 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Exception ProtoBuf.Serializers.TypeSerializer::ilo_CreateInvalidCallbackSignature11(System.Reflection.MethodInfo)
extern "C"  Exception_t3991598821 * TypeSerializer_ilo_CreateInvalidCallbackSignature11_m1813893536 (Il2CppObject * __this /* static, unused */, MethodInfo_t * ___method0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
