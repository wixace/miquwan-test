﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen20875812.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"

// System.Void System.Array/InternalEnumerator`1<Entity.Behavior.EBehaviorID>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m3498346470_gshared (InternalEnumerator_1_t20875812 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m3498346470(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t20875812 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m3498346470_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Entity.Behavior.EBehaviorID>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1488775162_gshared (InternalEnumerator_1_t20875812 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1488775162(__this, method) ((  void (*) (InternalEnumerator_1_t20875812 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m1488775162_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Entity.Behavior.EBehaviorID>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3968880_gshared (InternalEnumerator_1_t20875812 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3968880(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t20875812 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3968880_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Entity.Behavior.EBehaviorID>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m4022966589_gshared (InternalEnumerator_1_t20875812 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m4022966589(__this, method) ((  void (*) (InternalEnumerator_1_t20875812 *, const MethodInfo*))InternalEnumerator_1_Dispose_m4022966589_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Entity.Behavior.EBehaviorID>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m3635059882_gshared (InternalEnumerator_1_t20875812 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m3635059882(__this, method) ((  bool (*) (InternalEnumerator_1_t20875812 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m3635059882_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Entity.Behavior.EBehaviorID>::get_Current()
extern "C"  uint8_t InternalEnumerator_1_get_Current_m454960527_gshared (InternalEnumerator_1_t20875812 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m454960527(__this, method) ((  uint8_t (*) (InternalEnumerator_1_t20875812 *, const MethodInfo*))InternalEnumerator_1_get_Current_m454960527_gshared)(__this, method)
