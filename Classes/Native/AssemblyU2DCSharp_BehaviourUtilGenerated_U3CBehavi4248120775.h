﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.MethodInfo
struct MethodInfo_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BehaviourUtilGenerated/<BehaviourUtil_DelayCallT4__Single__ActionT4_T1_T2_T3_T4__T1__T2__T3__T4>c__AnonStorey44
struct  U3CBehaviourUtil_DelayCallT4__Single__ActionT4_T1_T2_T3_T4__T1__T2__T3__T4U3Ec__AnonStorey44_t4248120775  : public Il2CppObject
{
public:
	// System.Reflection.MethodInfo BehaviourUtilGenerated/<BehaviourUtil_DelayCallT4__Single__ActionT4_T1_T2_T3_T4__T1__T2__T3__T4>c__AnonStorey44::method
	MethodInfo_t * ___method_0;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(U3CBehaviourUtil_DelayCallT4__Single__ActionT4_T1_T2_T3_T4__T1__T2__T3__T4U3Ec__AnonStorey44_t4248120775, ___method_0)); }
	inline MethodInfo_t * get_method_0() const { return ___method_0; }
	inline MethodInfo_t ** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(MethodInfo_t * value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier(&___method_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
