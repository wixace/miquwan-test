﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AstarDebugger
struct AstarDebugger_t914461524;
// System.Text.StringBuilder
struct StringBuilder_t243639308;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Matrix4x41651859333.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_AstarDebugger_PathTypeDebug4028073209.h"
#include "mscorlib_System_Text_StringBuilder243639308.h"

// System.Void AstarDebugger::.ctor()
extern "C"  void AstarDebugger__ctor_m3311881367 (AstarDebugger_t914461524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarDebugger::Start()
extern "C"  void AstarDebugger_Start_m2259019159 (AstarDebugger_t914461524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarDebugger::Update()
extern "C"  void AstarDebugger_Update_m1315969366 (AstarDebugger_t914461524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarDebugger::DrawGraphLine(System.Int32,UnityEngine.Matrix4x4,System.Single,System.Single,System.Single,System.Single,UnityEngine.Color)
extern "C"  void AstarDebugger_DrawGraphLine_m3149468588 (AstarDebugger_t914461524 * __this, int32_t ___index0, Matrix4x4_t1651859333  ___m1, float ___x12, float ___x23, float ___y14, float ___y25, Color_t4194546905  ___col6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarDebugger::Cross(UnityEngine.Vector3)
extern "C"  void AstarDebugger_Cross_m2174123972 (AstarDebugger_t914461524 * __this, Vector3_t4282066566  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarDebugger::OnGUI()
extern "C"  void AstarDebugger_OnGUI_m2807280017 (AstarDebugger_t914461524 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AstarDebugger::ilo_MapTo1(System.Single,System.Single,System.Single)
extern "C"  float AstarDebugger_ilo_MapTo1_m1531487399 (Il2CppObject * __this /* static, unused */, float ___startMin0, float ___startMax1, float ___value2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AstarDebugger::ilo_Print2(AstarDebugger/PathTypeDebug&,System.Text.StringBuilder)
extern "C"  void AstarDebugger_ilo_Print2_m490688348 (Il2CppObject * __this /* static, unused */, PathTypeDebug_t4028073209 * ____this0, StringBuilder_t243639308 * ___text1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single AstarDebugger::ilo_MapTo3(System.Single,System.Single,System.Single,System.Single,System.Single)
extern "C"  float AstarDebugger_ilo_MapTo3_m3412049647 (Il2CppObject * __this /* static, unused */, float ___startMin0, float ___startMax1, float ___targetMin2, float ___targetMax3, float ___value4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
