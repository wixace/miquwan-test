﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_jewe150
struct  M_jewe150_t804721123  : public Il2CppObject
{
public:
	// System.UInt32 GarbageiOS.M_jewe150::_pearvedrayRawci
	uint32_t ____pearvedrayRawci_0;
	// System.UInt32 GarbageiOS.M_jewe150::_zedafis
	uint32_t ____zedafis_1;
	// System.UInt32 GarbageiOS.M_jewe150::_kaycecay
	uint32_t ____kaycecay_2;
	// System.UInt32 GarbageiOS.M_jewe150::_sorrealea
	uint32_t ____sorrealea_3;
	// System.Boolean GarbageiOS.M_jewe150::_hairrear
	bool ____hairrear_4;
	// System.Int32 GarbageiOS.M_jewe150::_remreaKeafasall
	int32_t ____remreaKeafasall_5;
	// System.UInt32 GarbageiOS.M_jewe150::_raleme
	uint32_t ____raleme_6;
	// System.Single GarbageiOS.M_jewe150::_locoru
	float ____locoru_7;
	// System.String GarbageiOS.M_jewe150::_geevaDooce
	String_t* ____geevaDooce_8;
	// System.UInt32 GarbageiOS.M_jewe150::_mallchemMirpisvi
	uint32_t ____mallchemMirpisvi_9;
	// System.Boolean GarbageiOS.M_jewe150::_payluza
	bool ____payluza_10;
	// System.Int32 GarbageiOS.M_jewe150::_joujo
	int32_t ____joujo_11;
	// System.Boolean GarbageiOS.M_jewe150::_jistakarCoba
	bool ____jistakarCoba_12;
	// System.String GarbageiOS.M_jewe150::_tomas
	String_t* ____tomas_13;
	// System.String GarbageiOS.M_jewe150::_rallmer
	String_t* ____rallmer_14;
	// System.Single GarbageiOS.M_jewe150::_canallmi
	float ____canallmi_15;
	// System.Single GarbageiOS.M_jewe150::_jebeeRissaspair
	float ____jebeeRissaspair_16;
	// System.Int32 GarbageiOS.M_jewe150::_najearDrevaschee
	int32_t ____najearDrevaschee_17;
	// System.Single GarbageiOS.M_jewe150::_lererasJewair
	float ____lererasJewair_18;
	// System.Int32 GarbageiOS.M_jewe150::_trosi
	int32_t ____trosi_19;
	// System.Single GarbageiOS.M_jewe150::_learjow
	float ____learjow_20;
	// System.Single GarbageiOS.M_jewe150::_gelirxas
	float ____gelirxas_21;
	// System.UInt32 GarbageiOS.M_jewe150::_gembedreKaiqo
	uint32_t ____gembedreKaiqo_22;
	// System.Single GarbageiOS.M_jewe150::_kerezearpou
	float ____kerezearpou_23;
	// System.Int32 GarbageiOS.M_jewe150::_chanotawMugotu
	int32_t ____chanotawMugotu_24;
	// System.String GarbageiOS.M_jewe150::_dasqorsarGaili
	String_t* ____dasqorsarGaili_25;
	// System.Int32 GarbageiOS.M_jewe150::_mirapear
	int32_t ____mirapear_26;
	// System.Single GarbageiOS.M_jewe150::_tenoyuRanai
	float ____tenoyuRanai_27;
	// System.Int32 GarbageiOS.M_jewe150::_cirnowkaToumair
	int32_t ____cirnowkaToumair_28;
	// System.UInt32 GarbageiOS.M_jewe150::_recalKalne
	uint32_t ____recalKalne_29;
	// System.Boolean GarbageiOS.M_jewe150::_luruwouDaynarnel
	bool ____luruwouDaynarnel_30;
	// System.String GarbageiOS.M_jewe150::_stouhi
	String_t* ____stouhi_31;
	// System.Single GarbageiOS.M_jewe150::_cezasDurnagee
	float ____cezasDurnagee_32;

public:
	inline static int32_t get_offset_of__pearvedrayRawci_0() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____pearvedrayRawci_0)); }
	inline uint32_t get__pearvedrayRawci_0() const { return ____pearvedrayRawci_0; }
	inline uint32_t* get_address_of__pearvedrayRawci_0() { return &____pearvedrayRawci_0; }
	inline void set__pearvedrayRawci_0(uint32_t value)
	{
		____pearvedrayRawci_0 = value;
	}

	inline static int32_t get_offset_of__zedafis_1() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____zedafis_1)); }
	inline uint32_t get__zedafis_1() const { return ____zedafis_1; }
	inline uint32_t* get_address_of__zedafis_1() { return &____zedafis_1; }
	inline void set__zedafis_1(uint32_t value)
	{
		____zedafis_1 = value;
	}

	inline static int32_t get_offset_of__kaycecay_2() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____kaycecay_2)); }
	inline uint32_t get__kaycecay_2() const { return ____kaycecay_2; }
	inline uint32_t* get_address_of__kaycecay_2() { return &____kaycecay_2; }
	inline void set__kaycecay_2(uint32_t value)
	{
		____kaycecay_2 = value;
	}

	inline static int32_t get_offset_of__sorrealea_3() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____sorrealea_3)); }
	inline uint32_t get__sorrealea_3() const { return ____sorrealea_3; }
	inline uint32_t* get_address_of__sorrealea_3() { return &____sorrealea_3; }
	inline void set__sorrealea_3(uint32_t value)
	{
		____sorrealea_3 = value;
	}

	inline static int32_t get_offset_of__hairrear_4() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____hairrear_4)); }
	inline bool get__hairrear_4() const { return ____hairrear_4; }
	inline bool* get_address_of__hairrear_4() { return &____hairrear_4; }
	inline void set__hairrear_4(bool value)
	{
		____hairrear_4 = value;
	}

	inline static int32_t get_offset_of__remreaKeafasall_5() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____remreaKeafasall_5)); }
	inline int32_t get__remreaKeafasall_5() const { return ____remreaKeafasall_5; }
	inline int32_t* get_address_of__remreaKeafasall_5() { return &____remreaKeafasall_5; }
	inline void set__remreaKeafasall_5(int32_t value)
	{
		____remreaKeafasall_5 = value;
	}

	inline static int32_t get_offset_of__raleme_6() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____raleme_6)); }
	inline uint32_t get__raleme_6() const { return ____raleme_6; }
	inline uint32_t* get_address_of__raleme_6() { return &____raleme_6; }
	inline void set__raleme_6(uint32_t value)
	{
		____raleme_6 = value;
	}

	inline static int32_t get_offset_of__locoru_7() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____locoru_7)); }
	inline float get__locoru_7() const { return ____locoru_7; }
	inline float* get_address_of__locoru_7() { return &____locoru_7; }
	inline void set__locoru_7(float value)
	{
		____locoru_7 = value;
	}

	inline static int32_t get_offset_of__geevaDooce_8() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____geevaDooce_8)); }
	inline String_t* get__geevaDooce_8() const { return ____geevaDooce_8; }
	inline String_t** get_address_of__geevaDooce_8() { return &____geevaDooce_8; }
	inline void set__geevaDooce_8(String_t* value)
	{
		____geevaDooce_8 = value;
		Il2CppCodeGenWriteBarrier(&____geevaDooce_8, value);
	}

	inline static int32_t get_offset_of__mallchemMirpisvi_9() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____mallchemMirpisvi_9)); }
	inline uint32_t get__mallchemMirpisvi_9() const { return ____mallchemMirpisvi_9; }
	inline uint32_t* get_address_of__mallchemMirpisvi_9() { return &____mallchemMirpisvi_9; }
	inline void set__mallchemMirpisvi_9(uint32_t value)
	{
		____mallchemMirpisvi_9 = value;
	}

	inline static int32_t get_offset_of__payluza_10() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____payluza_10)); }
	inline bool get__payluza_10() const { return ____payluza_10; }
	inline bool* get_address_of__payluza_10() { return &____payluza_10; }
	inline void set__payluza_10(bool value)
	{
		____payluza_10 = value;
	}

	inline static int32_t get_offset_of__joujo_11() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____joujo_11)); }
	inline int32_t get__joujo_11() const { return ____joujo_11; }
	inline int32_t* get_address_of__joujo_11() { return &____joujo_11; }
	inline void set__joujo_11(int32_t value)
	{
		____joujo_11 = value;
	}

	inline static int32_t get_offset_of__jistakarCoba_12() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____jistakarCoba_12)); }
	inline bool get__jistakarCoba_12() const { return ____jistakarCoba_12; }
	inline bool* get_address_of__jistakarCoba_12() { return &____jistakarCoba_12; }
	inline void set__jistakarCoba_12(bool value)
	{
		____jistakarCoba_12 = value;
	}

	inline static int32_t get_offset_of__tomas_13() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____tomas_13)); }
	inline String_t* get__tomas_13() const { return ____tomas_13; }
	inline String_t** get_address_of__tomas_13() { return &____tomas_13; }
	inline void set__tomas_13(String_t* value)
	{
		____tomas_13 = value;
		Il2CppCodeGenWriteBarrier(&____tomas_13, value);
	}

	inline static int32_t get_offset_of__rallmer_14() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____rallmer_14)); }
	inline String_t* get__rallmer_14() const { return ____rallmer_14; }
	inline String_t** get_address_of__rallmer_14() { return &____rallmer_14; }
	inline void set__rallmer_14(String_t* value)
	{
		____rallmer_14 = value;
		Il2CppCodeGenWriteBarrier(&____rallmer_14, value);
	}

	inline static int32_t get_offset_of__canallmi_15() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____canallmi_15)); }
	inline float get__canallmi_15() const { return ____canallmi_15; }
	inline float* get_address_of__canallmi_15() { return &____canallmi_15; }
	inline void set__canallmi_15(float value)
	{
		____canallmi_15 = value;
	}

	inline static int32_t get_offset_of__jebeeRissaspair_16() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____jebeeRissaspair_16)); }
	inline float get__jebeeRissaspair_16() const { return ____jebeeRissaspair_16; }
	inline float* get_address_of__jebeeRissaspair_16() { return &____jebeeRissaspair_16; }
	inline void set__jebeeRissaspair_16(float value)
	{
		____jebeeRissaspair_16 = value;
	}

	inline static int32_t get_offset_of__najearDrevaschee_17() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____najearDrevaschee_17)); }
	inline int32_t get__najearDrevaschee_17() const { return ____najearDrevaschee_17; }
	inline int32_t* get_address_of__najearDrevaschee_17() { return &____najearDrevaschee_17; }
	inline void set__najearDrevaschee_17(int32_t value)
	{
		____najearDrevaschee_17 = value;
	}

	inline static int32_t get_offset_of__lererasJewair_18() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____lererasJewair_18)); }
	inline float get__lererasJewair_18() const { return ____lererasJewair_18; }
	inline float* get_address_of__lererasJewair_18() { return &____lererasJewair_18; }
	inline void set__lererasJewair_18(float value)
	{
		____lererasJewair_18 = value;
	}

	inline static int32_t get_offset_of__trosi_19() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____trosi_19)); }
	inline int32_t get__trosi_19() const { return ____trosi_19; }
	inline int32_t* get_address_of__trosi_19() { return &____trosi_19; }
	inline void set__trosi_19(int32_t value)
	{
		____trosi_19 = value;
	}

	inline static int32_t get_offset_of__learjow_20() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____learjow_20)); }
	inline float get__learjow_20() const { return ____learjow_20; }
	inline float* get_address_of__learjow_20() { return &____learjow_20; }
	inline void set__learjow_20(float value)
	{
		____learjow_20 = value;
	}

	inline static int32_t get_offset_of__gelirxas_21() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____gelirxas_21)); }
	inline float get__gelirxas_21() const { return ____gelirxas_21; }
	inline float* get_address_of__gelirxas_21() { return &____gelirxas_21; }
	inline void set__gelirxas_21(float value)
	{
		____gelirxas_21 = value;
	}

	inline static int32_t get_offset_of__gembedreKaiqo_22() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____gembedreKaiqo_22)); }
	inline uint32_t get__gembedreKaiqo_22() const { return ____gembedreKaiqo_22; }
	inline uint32_t* get_address_of__gembedreKaiqo_22() { return &____gembedreKaiqo_22; }
	inline void set__gembedreKaiqo_22(uint32_t value)
	{
		____gembedreKaiqo_22 = value;
	}

	inline static int32_t get_offset_of__kerezearpou_23() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____kerezearpou_23)); }
	inline float get__kerezearpou_23() const { return ____kerezearpou_23; }
	inline float* get_address_of__kerezearpou_23() { return &____kerezearpou_23; }
	inline void set__kerezearpou_23(float value)
	{
		____kerezearpou_23 = value;
	}

	inline static int32_t get_offset_of__chanotawMugotu_24() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____chanotawMugotu_24)); }
	inline int32_t get__chanotawMugotu_24() const { return ____chanotawMugotu_24; }
	inline int32_t* get_address_of__chanotawMugotu_24() { return &____chanotawMugotu_24; }
	inline void set__chanotawMugotu_24(int32_t value)
	{
		____chanotawMugotu_24 = value;
	}

	inline static int32_t get_offset_of__dasqorsarGaili_25() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____dasqorsarGaili_25)); }
	inline String_t* get__dasqorsarGaili_25() const { return ____dasqorsarGaili_25; }
	inline String_t** get_address_of__dasqorsarGaili_25() { return &____dasqorsarGaili_25; }
	inline void set__dasqorsarGaili_25(String_t* value)
	{
		____dasqorsarGaili_25 = value;
		Il2CppCodeGenWriteBarrier(&____dasqorsarGaili_25, value);
	}

	inline static int32_t get_offset_of__mirapear_26() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____mirapear_26)); }
	inline int32_t get__mirapear_26() const { return ____mirapear_26; }
	inline int32_t* get_address_of__mirapear_26() { return &____mirapear_26; }
	inline void set__mirapear_26(int32_t value)
	{
		____mirapear_26 = value;
	}

	inline static int32_t get_offset_of__tenoyuRanai_27() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____tenoyuRanai_27)); }
	inline float get__tenoyuRanai_27() const { return ____tenoyuRanai_27; }
	inline float* get_address_of__tenoyuRanai_27() { return &____tenoyuRanai_27; }
	inline void set__tenoyuRanai_27(float value)
	{
		____tenoyuRanai_27 = value;
	}

	inline static int32_t get_offset_of__cirnowkaToumair_28() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____cirnowkaToumair_28)); }
	inline int32_t get__cirnowkaToumair_28() const { return ____cirnowkaToumair_28; }
	inline int32_t* get_address_of__cirnowkaToumair_28() { return &____cirnowkaToumair_28; }
	inline void set__cirnowkaToumair_28(int32_t value)
	{
		____cirnowkaToumair_28 = value;
	}

	inline static int32_t get_offset_of__recalKalne_29() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____recalKalne_29)); }
	inline uint32_t get__recalKalne_29() const { return ____recalKalne_29; }
	inline uint32_t* get_address_of__recalKalne_29() { return &____recalKalne_29; }
	inline void set__recalKalne_29(uint32_t value)
	{
		____recalKalne_29 = value;
	}

	inline static int32_t get_offset_of__luruwouDaynarnel_30() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____luruwouDaynarnel_30)); }
	inline bool get__luruwouDaynarnel_30() const { return ____luruwouDaynarnel_30; }
	inline bool* get_address_of__luruwouDaynarnel_30() { return &____luruwouDaynarnel_30; }
	inline void set__luruwouDaynarnel_30(bool value)
	{
		____luruwouDaynarnel_30 = value;
	}

	inline static int32_t get_offset_of__stouhi_31() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____stouhi_31)); }
	inline String_t* get__stouhi_31() const { return ____stouhi_31; }
	inline String_t** get_address_of__stouhi_31() { return &____stouhi_31; }
	inline void set__stouhi_31(String_t* value)
	{
		____stouhi_31 = value;
		Il2CppCodeGenWriteBarrier(&____stouhi_31, value);
	}

	inline static int32_t get_offset_of__cezasDurnagee_32() { return static_cast<int32_t>(offsetof(M_jewe150_t804721123, ____cezasDurnagee_32)); }
	inline float get__cezasDurnagee_32() const { return ____cezasDurnagee_32; }
	inline float* get_address_of__cezasDurnagee_32() { return &____cezasDurnagee_32; }
	inline void set__cezasDurnagee_32(float value)
	{
		____cezasDurnagee_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
