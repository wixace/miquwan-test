﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_Util_ListPool_1_gen2456319425MethodDeclarations.h"

// System.Void Pathfinding.Util.ListPool`1<Pathfinding.RecastMeshObj>::.cctor()
#define ListPool_1__cctor_m4113221105(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m2656574753_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<Pathfinding.RecastMeshObj>::Claim()
#define ListPool_1_Claim_m3296723425(__this /* static, unused */, method) ((  List_1_t3437381290 * (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_Claim_m2151275825_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> Pathfinding.Util.ListPool`1<Pathfinding.RecastMeshObj>::Claim(System.Int32)
#define ListPool_1_Claim_m1060957618(__this /* static, unused */, ___capacity0, method) ((  List_1_t3437381290 * (*) (Il2CppObject * /* static, unused */, int32_t, const MethodInfo*))ListPool_1_Claim_m3438825730_gshared)(__this /* static, unused */, ___capacity0, method)
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.RecastMeshObj>::Warmup(System.Int32,System.Int32)
#define ListPool_1_Warmup_m2540265854(__this /* static, unused */, ___count0, ___size1, method) ((  void (*) (Il2CppObject * /* static, unused */, int32_t, int32_t, const MethodInfo*))ListPool_1_Warmup_m3738927182_gshared)(__this /* static, unused */, ___count0, ___size1, method)
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.RecastMeshObj>::Release(System.Collections.Generic.List`1<T>)
#define ListPool_1_Release_m1219972665(__this /* static, unused */, ___list0, method) ((  void (*) (Il2CppObject * /* static, unused */, List_1_t3437381290 *, const MethodInfo*))ListPool_1_Release_m442610953_gshared)(__this /* static, unused */, ___list0, method)
// System.Void Pathfinding.Util.ListPool`1<Pathfinding.RecastMeshObj>::Clear()
#define ListPool_1_Clear_m3511904615(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_Clear_m3464916023_gshared)(__this /* static, unused */, method)
// System.Int32 Pathfinding.Util.ListPool`1<Pathfinding.RecastMeshObj>::GetSize()
#define ListPool_1_GetSize_m2622731907(__this /* static, unused */, method) ((  int32_t (*) (Il2CppObject * /* static, unused */, const MethodInfo*))ListPool_1_GetSize_m4059414611_gshared)(__this /* static, unused */, method)
