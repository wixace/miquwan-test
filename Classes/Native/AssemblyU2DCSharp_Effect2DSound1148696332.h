﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<SoundObj>
struct List_1_t3175472216;
// System.Collections.Generic.List`1<SoundCfg>
struct List_1_t3175460805;

#include "AssemblyU2DCSharp_ISound2170003014.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Effect2DSound
struct  Effect2DSound_t1148696332  : public ISound_t2170003014
{
public:
	// System.Collections.Generic.List`1<SoundObj> Effect2DSound::idleAudioList
	List_1_t3175472216 * ___idleAudioList_2;
	// System.Collections.Generic.List`1<SoundObj> Effect2DSound::workAudioList
	List_1_t3175472216 * ___workAudioList_3;
	// System.Collections.Generic.List`1<SoundCfg> Effect2DSound::loopList
	List_1_t3175460805 * ___loopList_4;

public:
	inline static int32_t get_offset_of_idleAudioList_2() { return static_cast<int32_t>(offsetof(Effect2DSound_t1148696332, ___idleAudioList_2)); }
	inline List_1_t3175472216 * get_idleAudioList_2() const { return ___idleAudioList_2; }
	inline List_1_t3175472216 ** get_address_of_idleAudioList_2() { return &___idleAudioList_2; }
	inline void set_idleAudioList_2(List_1_t3175472216 * value)
	{
		___idleAudioList_2 = value;
		Il2CppCodeGenWriteBarrier(&___idleAudioList_2, value);
	}

	inline static int32_t get_offset_of_workAudioList_3() { return static_cast<int32_t>(offsetof(Effect2DSound_t1148696332, ___workAudioList_3)); }
	inline List_1_t3175472216 * get_workAudioList_3() const { return ___workAudioList_3; }
	inline List_1_t3175472216 ** get_address_of_workAudioList_3() { return &___workAudioList_3; }
	inline void set_workAudioList_3(List_1_t3175472216 * value)
	{
		___workAudioList_3 = value;
		Il2CppCodeGenWriteBarrier(&___workAudioList_3, value);
	}

	inline static int32_t get_offset_of_loopList_4() { return static_cast<int32_t>(offsetof(Effect2DSound_t1148696332, ___loopList_4)); }
	inline List_1_t3175460805 * get_loopList_4() const { return ___loopList_4; }
	inline List_1_t3175460805 ** get_address_of_loopList_4() { return &___loopList_4; }
	inline void set_loopList_4(List_1_t3175460805 * value)
	{
		___loopList_4 = value;
		Il2CppCodeGenWriteBarrier(&___loopList_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
