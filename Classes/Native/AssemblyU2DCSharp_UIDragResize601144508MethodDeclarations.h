﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIDragResize
struct UIDragResize_t601144508;
// UIWidget
struct UIWidget_t769069560;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UIWidget_Pivot240933195.h"

// System.Void UIDragResize::.ctor()
extern "C"  void UIDragResize__ctor_m3346385247 (UIDragResize_t601144508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragResize::OnDragStart()
extern "C"  void UIDragResize_OnDragStart_m944686636 (UIDragResize_t601144508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragResize::OnDrag(UnityEngine.Vector2)
extern "C"  void UIDragResize_OnDrag_m4001475266 (UIDragResize_t601144508 * __this, Vector2_t4282066565  ___delta0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragResize::OnDragEnd()
extern "C"  void UIDragResize_OnDragEnd_m537595557 (UIDragResize_t601144508 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIDragResize::ilo_get_width1(UIWidget)
extern "C"  int32_t UIDragResize_ilo_get_width1_m2283674978 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIDragResize::ilo_get_height2(UIWidget)
extern "C"  int32_t UIDragResize_ilo_get_height2_m4245131270 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragResize::ilo_set_width3(UIWidget,System.Int32)
extern "C"  void UIDragResize_ilo_set_width3_m3341626701 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIDragResize::ilo_ResizeWidget4(UIWidget,UIWidget/Pivot,System.Single,System.Single,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void UIDragResize_ilo_ResizeWidget4_m3739808471 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ___w0, int32_t ___pivot1, float ___x2, float ___y3, int32_t ___minWidth4, int32_t ___minHeight5, int32_t ___maxWidth6, int32_t ___maxHeight7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
