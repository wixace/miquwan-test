﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FileInfoBase
struct  FileInfoBase_t3368634971  : public Il2CppObject
{
public:
	// System.String FileInfoBase::mDataPath
	String_t* ___mDataPath_0;

public:
	inline static int32_t get_offset_of_mDataPath_0() { return static_cast<int32_t>(offsetof(FileInfoBase_t3368634971, ___mDataPath_0)); }
	inline String_t* get_mDataPath_0() const { return ___mDataPath_0; }
	inline String_t** get_address_of_mDataPath_0() { return &___mDataPath_0; }
	inline void set_mDataPath_0(String_t* value)
	{
		___mDataPath_0 = value;
		Il2CppCodeGenWriteBarrier(&___mDataPath_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
