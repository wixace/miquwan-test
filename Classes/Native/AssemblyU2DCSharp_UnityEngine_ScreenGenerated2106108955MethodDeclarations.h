﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_ScreenGenerated
struct UnityEngine_ScreenGenerated_t2106108955;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_ScreenGenerated::.ctor()
extern "C"  void UnityEngine_ScreenGenerated__ctor_m3627052464 (UnityEngine_ScreenGenerated_t2106108955 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ScreenGenerated::Screen_Screen1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ScreenGenerated_Screen_Screen1_m3959560450 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ScreenGenerated::Screen_resolutions(JSVCall)
extern "C"  void UnityEngine_ScreenGenerated_Screen_resolutions_m3008727903 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ScreenGenerated::Screen_currentResolution(JSVCall)
extern "C"  void UnityEngine_ScreenGenerated_Screen_currentResolution_m3778248545 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ScreenGenerated::Screen_width(JSVCall)
extern "C"  void UnityEngine_ScreenGenerated_Screen_width_m2174978496 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ScreenGenerated::Screen_height(JSVCall)
extern "C"  void UnityEngine_ScreenGenerated_Screen_height_m3655267583 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ScreenGenerated::Screen_dpi(JSVCall)
extern "C"  void UnityEngine_ScreenGenerated_Screen_dpi_m3848641545 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ScreenGenerated::Screen_fullScreen(JSVCall)
extern "C"  void UnityEngine_ScreenGenerated_Screen_fullScreen_m3119188171 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ScreenGenerated::Screen_autorotateToPortrait(JSVCall)
extern "C"  void UnityEngine_ScreenGenerated_Screen_autorotateToPortrait_m3815516070 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ScreenGenerated::Screen_autorotateToPortraitUpsideDown(JSVCall)
extern "C"  void UnityEngine_ScreenGenerated_Screen_autorotateToPortraitUpsideDown_m1262635986 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ScreenGenerated::Screen_autorotateToLandscapeLeft(JSVCall)
extern "C"  void UnityEngine_ScreenGenerated_Screen_autorotateToLandscapeLeft_m1465775081 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ScreenGenerated::Screen_autorotateToLandscapeRight(JSVCall)
extern "C"  void UnityEngine_ScreenGenerated_Screen_autorotateToLandscapeRight_m1416793504 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ScreenGenerated::Screen_orientation(JSVCall)
extern "C"  void UnityEngine_ScreenGenerated_Screen_orientation_m545307894 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ScreenGenerated::Screen_sleepTimeout(JSVCall)
extern "C"  void UnityEngine_ScreenGenerated_Screen_sleepTimeout_m2490871548 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ScreenGenerated::Screen_SetResolution__Int32__Int32__Boolean__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ScreenGenerated_Screen_SetResolution__Int32__Int32__Boolean__Int32_m983258193 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ScreenGenerated::Screen_SetResolution__Int32__Int32__Boolean(JSVCall,System.Int32)
extern "C"  bool UnityEngine_ScreenGenerated_Screen_SetResolution__Int32__Int32__Boolean_m3016065567 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ScreenGenerated::__Register()
extern "C"  void UnityEngine_ScreenGenerated___Register_m1476533527 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ScreenGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_ScreenGenerated_ilo_getObject1_m2559166342 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ScreenGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_ScreenGenerated_ilo_attachFinalizerObject2_m786957960 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ScreenGenerated::ilo_addJSCSRel3(System.Int32,System.Object)
extern "C"  void UnityEngine_ScreenGenerated_ilo_addJSCSRel3_m416200046 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ScreenGenerated::ilo_moveSaveID2Arr4(System.Int32)
extern "C"  void UnityEngine_ScreenGenerated_ilo_moveSaveID2Arr4_m919821960 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ScreenGenerated::ilo_setInt325(System.Int32,System.Int32)
extern "C"  void UnityEngine_ScreenGenerated_ilo_setInt325_m2031860130 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_ScreenGenerated::ilo_setBooleanS6(System.Int32,System.Boolean)
extern "C"  void UnityEngine_ScreenGenerated_ilo_setBooleanS6_m567240252 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_ScreenGenerated::ilo_getBooleanS7(System.Int32)
extern "C"  bool UnityEngine_ScreenGenerated_ilo_getBooleanS7_m1387494586 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_ScreenGenerated::ilo_getInt328(System.Int32)
extern "C"  int32_t UnityEngine_ScreenGenerated_ilo_getInt328_m922625306 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
