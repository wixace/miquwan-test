﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_peesalzel182
struct M_peesalzel182_t1693384776;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_peesalzel1821693384776.h"

// System.Void GarbageiOS.M_peesalzel182::.ctor()
extern "C"  void M_peesalzel182__ctor_m513147627 (M_peesalzel182_t1693384776 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_peesalzel182::M_jursovea0(System.String[],System.Int32)
extern "C"  void M_peesalzel182_M_jursovea0_m300695215 (M_peesalzel182_t1693384776 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_peesalzel182::M_sairmofai1(System.String[],System.Int32)
extern "C"  void M_peesalzel182_M_sairmofai1_m3152768382 (M_peesalzel182_t1693384776 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_peesalzel182::M_goojairnear2(System.String[],System.Int32)
extern "C"  void M_peesalzel182_M_goojairnear2_m185855557 (M_peesalzel182_t1693384776 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_peesalzel182::M_seepemMefem3(System.String[],System.Int32)
extern "C"  void M_peesalzel182_M_seepemMefem3_m1040792196 (M_peesalzel182_t1693384776 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_peesalzel182::M_buxoopaiLisrelow4(System.String[],System.Int32)
extern "C"  void M_peesalzel182_M_buxoopaiLisrelow4_m1126651780 (M_peesalzel182_t1693384776 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_peesalzel182::ilo_M_seepemMefem31(GarbageiOS.M_peesalzel182,System.String[],System.Int32)
extern "C"  void M_peesalzel182_ilo_M_seepemMefem31_m1395172062 (Il2CppObject * __this /* static, unused */, M_peesalzel182_t1693384776 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
