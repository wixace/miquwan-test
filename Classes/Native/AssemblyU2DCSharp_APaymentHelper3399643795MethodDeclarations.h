﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// APaymentHelper
struct APaymentHelper_t3399643795;

#include "codegen/il2cpp-codegen.h"

// System.Void APaymentHelper::.ctor()
extern "C"  void APaymentHelper__ctor_m1816104808 (APaymentHelper_t3399643795 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
