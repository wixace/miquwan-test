﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.Int16Serializer
struct Int16Serializer_t3590372032;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"

// System.Void ProtoBuf.Serializers.Int16Serializer::.ctor(ProtoBuf.Meta.TypeModel)
extern "C"  void Int16Serializer__ctor_m1831556 (Int16Serializer_t3590372032 * __this, TypeModel_t2730011105 * ___model0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.Int16Serializer::.cctor()
extern "C"  void Int16Serializer__cctor_m1518853384 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.Int16Serializer::ProtoBuf.Serializers.IProtoSerializer.get_RequiresOldValue()
extern "C"  bool Int16Serializer_ProtoBuf_Serializers_IProtoSerializer_get_RequiresOldValue_m3645794593 (Int16Serializer_t3590372032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.Int16Serializer::ProtoBuf.Serializers.IProtoSerializer.get_ReturnsValue()
extern "C"  bool Int16Serializer_ProtoBuf_Serializers_IProtoSerializer_get_ReturnsValue_m1881543735 (Int16Serializer_t3590372032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.Int16Serializer::get_ExpectedType()
extern "C"  Type_t * Int16Serializer_get_ExpectedType_m2038408980 (Int16Serializer_t3590372032 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.Int16Serializer::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * Int16Serializer_Read_m2301479252 (Int16Serializer_t3590372032 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.Int16Serializer::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void Int16Serializer_Write_m2426376060 (Int16Serializer_t3590372032 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___dest1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int16 ProtoBuf.Serializers.Int16Serializer::ilo_ReadInt161(ProtoBuf.ProtoReader)
extern "C"  int16_t Int16Serializer_ilo_ReadInt161_m3409017765 (Il2CppObject * __this /* static, unused */, ProtoReader_t3962509489 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
