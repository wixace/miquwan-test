﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// T4MLodObjSC
struct T4MLodObjSC_t2288258003;

#include "codegen/il2cpp-codegen.h"

// System.Void T4MLodObjSC::.ctor()
extern "C"  void T4MLodObjSC__ctor_m2074873080 (T4MLodObjSC_t2288258003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void T4MLodObjSC::ActivateLODScrpt()
extern "C"  void T4MLodObjSC_ActivateLODScrpt_m1527864260 (T4MLodObjSC_t2288258003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void T4MLodObjSC::ActivateLODLay()
extern "C"  void T4MLodObjSC_ActivateLODLay_m495628162 (T4MLodObjSC_t2288258003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void T4MLodObjSC::AFLODLay()
extern "C"  void T4MLodObjSC_AFLODLay_m202193108 (T4MLodObjSC_t2288258003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void T4MLodObjSC::AFLODScrpt()
extern "C"  void T4MLodObjSC_AFLODScrpt_m3004618902 (T4MLodObjSC_t2288258003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
