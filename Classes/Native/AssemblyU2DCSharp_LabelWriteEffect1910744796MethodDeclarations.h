﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LabelWriteEffect
struct LabelWriteEffect_t1910744796;
// System.String
struct String_t;
// System.Action
struct Action_t3771233898;
// UILabel
struct UILabel_t291504320;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"
#include "System_Core_System_Action3771233898.h"
#include "AssemblyU2DCSharp_UILabel291504320.h"
#include "AssemblyU2DCSharp_LabelWriteEffect1910744796.h"

// System.Void LabelWriteEffect::.ctor()
extern "C"  void LabelWriteEffect__ctor_m1950971711 (LabelWriteEffect_t1910744796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LabelWriteEffect::set_isPlaying(System.Boolean)
extern "C"  void LabelWriteEffect_set_isPlaying_m3355307323 (LabelWriteEffect_t1910744796 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LabelWriteEffect::get_isPlaying()
extern "C"  bool LabelWriteEffect_get_isPlaying_m1105664900 (LabelWriteEffect_t1910744796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LabelWriteEffect::Awake()
extern "C"  void LabelWriteEffect_Awake_m2188576930 (LabelWriteEffect_t1910744796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LabelWriteEffect::Play(System.String,System.Single)
extern "C"  void LabelWriteEffect_Play_m1629810286 (LabelWriteEffect_t1910744796 * __this, String_t* ___text0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LabelWriteEffect::Stop()
extern "C"  void LabelWriteEffect_Stop_m1137762727 (LabelWriteEffect_t1910744796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LabelWriteEffect::AddFunctionList(System.Action)
extern "C"  void LabelWriteEffect_AddFunctionList_m3886903945 (LabelWriteEffect_t1910744796 * __this, Action_t3771233898 * ___at0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LabelWriteEffect::ClearFunction()
extern "C"  void LabelWriteEffect_ClearFunction_m3670373474 (LabelWriteEffect_t1910744796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LabelWriteEffect::CallFunctionList()
extern "C"  void LabelWriteEffect_CallFunctionList_m1919165753 (LabelWriteEffect_t1910744796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single LabelWriteEffect::get_TimeLen()
extern "C"  float LabelWriteEffect_get_TimeLen_m2608127344 (LabelWriteEffect_t1910744796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LabelWriteEffect::Update()
extern "C"  void LabelWriteEffect_Update_m2077442990 (LabelWriteEffect_t1910744796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LabelWriteEffect::OnDestroy()
extern "C"  void LabelWriteEffect_OnDestroy_m1207932088 (LabelWriteEffect_t1910744796 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String LabelWriteEffect::ilo_get_text1(UILabel)
extern "C"  String_t* LabelWriteEffect_ilo_get_text1_m97606416 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LabelWriteEffect::ilo_set_text2(UILabel,System.String)
extern "C"  void LabelWriteEffect_ilo_set_text2_m3267138236 (Il2CppObject * __this /* static, unused */, UILabel_t291504320 * ____this0, String_t* ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LabelWriteEffect::ilo_set_isPlaying3(LabelWriteEffect,System.Boolean)
extern "C"  void LabelWriteEffect_ilo_set_isPlaying3_m3545124501 (Il2CppObject * __this /* static, unused */, LabelWriteEffect_t1910744796 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
