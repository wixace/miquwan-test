﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PointCloudRegognizer/NormalizedTemplate
struct NormalizedTemplate_t2023934299;

#include "codegen/il2cpp-codegen.h"

// System.Void PointCloudRegognizer/NormalizedTemplate::.ctor()
extern "C"  void NormalizedTemplate__ctor_m3340639344 (NormalizedTemplate_t2023934299 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
