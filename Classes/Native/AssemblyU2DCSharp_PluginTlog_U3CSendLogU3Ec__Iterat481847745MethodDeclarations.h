﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PluginTlog/<SendLog>c__Iterator35
struct U3CSendLogU3Ec__Iterator35_t481847745;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void PluginTlog/<SendLog>c__Iterator35::.ctor()
extern "C"  void U3CSendLogU3Ec__Iterator35__ctor_m2684935370 (U3CSendLogU3Ec__Iterator35_t481847745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PluginTlog/<SendLog>c__Iterator35::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CSendLogU3Ec__Iterator35_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3490927112 (U3CSendLogU3Ec__Iterator35_t481847745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PluginTlog/<SendLog>c__Iterator35::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CSendLogU3Ec__Iterator35_System_Collections_IEnumerator_get_Current_m821716380 (U3CSendLogU3Ec__Iterator35_t481847745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PluginTlog/<SendLog>c__Iterator35::MoveNext()
extern "C"  bool U3CSendLogU3Ec__Iterator35_MoveNext_m1106153962 (U3CSendLogU3Ec__Iterator35_t481847745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTlog/<SendLog>c__Iterator35::Dispose()
extern "C"  void U3CSendLogU3Ec__Iterator35_Dispose_m3140796039 (U3CSendLogU3Ec__Iterator35_t481847745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PluginTlog/<SendLog>c__Iterator35::Reset()
extern "C"  void U3CSendLogU3Ec__Iterator35_Reset_m331368311 (U3CSendLogU3Ec__Iterator35_t481847745 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
