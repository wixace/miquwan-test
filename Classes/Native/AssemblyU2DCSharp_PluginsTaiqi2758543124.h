﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PluginsTaiqi/LoginResult
struct LoginResult_t2998189643;
// System.Action
struct Action_t3771233898;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginsTaiqi
struct  PluginsTaiqi_t2758543124  : public MonoBehaviour_t667441552
{
public:
	// PluginsTaiqi/LoginResult PluginsTaiqi::loginResult
	LoginResult_t2998189643 * ___loginResult_2;

public:
	inline static int32_t get_offset_of_loginResult_2() { return static_cast<int32_t>(offsetof(PluginsTaiqi_t2758543124, ___loginResult_2)); }
	inline LoginResult_t2998189643 * get_loginResult_2() const { return ___loginResult_2; }
	inline LoginResult_t2998189643 ** get_address_of_loginResult_2() { return &___loginResult_2; }
	inline void set_loginResult_2(LoginResult_t2998189643 * value)
	{
		___loginResult_2 = value;
		Il2CppCodeGenWriteBarrier(&___loginResult_2, value);
	}
};

struct PluginsTaiqi_t2758543124_StaticFields
{
public:
	// System.Action PluginsTaiqi::<>f__am$cache1
	Action_t3771233898 * ___U3CU3Ef__amU24cache1_3;
	// System.Action PluginsTaiqi::<>f__am$cache2
	Action_t3771233898 * ___U3CU3Ef__amU24cache2_4;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_3() { return static_cast<int32_t>(offsetof(PluginsTaiqi_t2758543124_StaticFields, ___U3CU3Ef__amU24cache1_3)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache1_3() const { return ___U3CU3Ef__amU24cache1_3; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache1_3() { return &___U3CU3Ef__amU24cache1_3; }
	inline void set_U3CU3Ef__amU24cache1_3(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache1_3 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache1_3, value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_4() { return static_cast<int32_t>(offsetof(PluginsTaiqi_t2758543124_StaticFields, ___U3CU3Ef__amU24cache2_4)); }
	inline Action_t3771233898 * get_U3CU3Ef__amU24cache2_4() const { return ___U3CU3Ef__amU24cache2_4; }
	inline Action_t3771233898 ** get_address_of_U3CU3Ef__amU24cache2_4() { return &___U3CU3Ef__amU24cache2_4; }
	inline void set_U3CU3Ef__amU24cache2_4(Action_t3771233898 * value)
	{
		___U3CU3Ef__amU24cache2_4 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__amU24cache2_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
