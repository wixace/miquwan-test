﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ProtoBuf.Serializers.SurrogateSerializer
struct SurrogateSerializer_t3383047748;
// ProtoBuf.Meta.TypeModel
struct TypeModel_t2730011105;
// System.Type
struct Type_t;
// ProtoBuf.Serializers.IProtoTypeSerializer
struct IProtoTypeSerializer_t321624293;
// System.Object
struct Il2CppObject;
// ProtoBuf.ProtoReader
struct ProtoReader_t3962509489;
// ProtoBuf.SerializationContext
struct SerializationContext_t3997850667;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// ProtoBuf.ProtoWriter
struct ProtoWriter_t4117914721;
// ProtoBuf.Serializers.IProtoSerializer
struct IProtoSerializer_t3033312651;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel2730011105.h"
#include "mscorlib_System_Type2863145774.h"
#include "AssemblyU2DCSharp_ProtoBuf_Meta_TypeModel_Callback2866957669.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoReader3962509489.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_ProtoBuf_SerializationContext3997850667.h"
#include "mscorlib_System_Reflection_MethodInfo318736065.h"
#include "AssemblyU2DCSharp_ProtoBuf_ProtoWriter4117914721.h"

// System.Void ProtoBuf.Serializers.SurrogateSerializer::.ctor(ProtoBuf.Meta.TypeModel,System.Type,System.Type,ProtoBuf.Serializers.IProtoTypeSerializer)
extern "C"  void SurrogateSerializer__ctor_m2523880447 (SurrogateSerializer_t3383047748 * __this, TypeModel_t2730011105 * ___model0, Type_t * ___forType1, Type_t * ___declaredType2, Il2CppObject * ___rootTail3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.SurrogateSerializer::ProtoBuf.Serializers.IProtoTypeSerializer.HasCallbacks(ProtoBuf.Meta.TypeModel/CallbackType)
extern "C"  bool SurrogateSerializer_ProtoBuf_Serializers_IProtoTypeSerializer_HasCallbacks_m95555897 (SurrogateSerializer_t3383047748 * __this, int32_t ___callbackType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.SurrogateSerializer::ProtoBuf.Serializers.IProtoTypeSerializer.CanCreateInstance()
extern "C"  bool SurrogateSerializer_ProtoBuf_Serializers_IProtoTypeSerializer_CanCreateInstance_m747174617 (SurrogateSerializer_t3383047748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.SurrogateSerializer::ProtoBuf.Serializers.IProtoTypeSerializer.CreateInstance(ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * SurrogateSerializer_ProtoBuf_Serializers_IProtoTypeSerializer_CreateInstance_m396338580 (SurrogateSerializer_t3383047748 * __this, ProtoReader_t3962509489 * ___source0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.SurrogateSerializer::ProtoBuf.Serializers.IProtoTypeSerializer.Callback(System.Object,ProtoBuf.Meta.TypeModel/CallbackType,ProtoBuf.SerializationContext)
extern "C"  void SurrogateSerializer_ProtoBuf_Serializers_IProtoTypeSerializer_Callback_m4022755922 (SurrogateSerializer_t3383047748 * __this, Il2CppObject * ___value0, int32_t ___callbackType1, SerializationContext_t3997850667 * ___context2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.SurrogateSerializer::get_ReturnsValue()
extern "C"  bool SurrogateSerializer_get_ReturnsValue_m1279049998 (SurrogateSerializer_t3383047748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.SurrogateSerializer::get_RequiresOldValue()
extern "C"  bool SurrogateSerializer_get_RequiresOldValue_m1040503416 (SurrogateSerializer_t3383047748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type ProtoBuf.Serializers.SurrogateSerializer::get_ExpectedType()
extern "C"  Type_t * SurrogateSerializer_get_ExpectedType_m2081646488 (SurrogateSerializer_t3383047748 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.SurrogateSerializer::HasCast(ProtoBuf.Meta.TypeModel,System.Type,System.Type,System.Type,System.Reflection.MethodInfo&)
extern "C"  bool SurrogateSerializer_HasCast_m2576729047 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Type_t * ___type1, Type_t * ___from2, Type_t * ___to3, MethodInfo_t ** ___op4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo ProtoBuf.Serializers.SurrogateSerializer::GetConversion(ProtoBuf.Meta.TypeModel,System.Boolean)
extern "C"  MethodInfo_t * SurrogateSerializer_GetConversion_m1381181109 (SurrogateSerializer_t3383047748 * __this, TypeModel_t2730011105 * ___model0, bool ___toTail1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ProtoBuf.Serializers.SurrogateSerializer::Write(System.Object,ProtoBuf.ProtoWriter)
extern "C"  void SurrogateSerializer_Write_m1793506680 (SurrogateSerializer_t3383047748 * __this, Il2CppObject * ___value0, ProtoWriter_t4117914721 * ___writer1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.SurrogateSerializer::Read(System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * SurrogateSerializer_Read_m1759897944 (SurrogateSerializer_t3383047748 * __this, Il2CppObject * ___value0, ProtoReader_t3962509489 * ___source1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ProtoBuf.Serializers.SurrogateSerializer::ilo_HasCast1(ProtoBuf.Meta.TypeModel,System.Type,System.Type,System.Type,System.Reflection.MethodInfo&)
extern "C"  bool SurrogateSerializer_ilo_HasCast1_m190427801 (Il2CppObject * __this /* static, unused */, TypeModel_t2730011105 * ___model0, Type_t * ___type1, Type_t * ___from2, Type_t * ___to3, MethodInfo_t ** ___op4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ProtoBuf.Serializers.SurrogateSerializer::ilo_Read2(ProtoBuf.Serializers.IProtoSerializer,System.Object,ProtoBuf.ProtoReader)
extern "C"  Il2CppObject * SurrogateSerializer_ilo_Read2_m387482930 (Il2CppObject * __this /* static, unused */, Il2CppObject * ____this0, Il2CppObject * ___value1, ProtoReader_t3962509489 * ___source2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
