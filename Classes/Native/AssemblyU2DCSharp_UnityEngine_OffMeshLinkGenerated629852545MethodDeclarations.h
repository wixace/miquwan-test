﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_OffMeshLinkGenerated
struct UnityEngine_OffMeshLinkGenerated_t629852545;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_OffMeshLinkGenerated::.ctor()
extern "C"  void UnityEngine_OffMeshLinkGenerated__ctor_m1942965818 (UnityEngine_OffMeshLinkGenerated_t629852545 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_OffMeshLinkGenerated::OffMeshLink_OffMeshLink1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_OffMeshLinkGenerated_OffMeshLink_OffMeshLink1_m2006231826 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OffMeshLinkGenerated::OffMeshLink_activated(JSVCall)
extern "C"  void UnityEngine_OffMeshLinkGenerated_OffMeshLink_activated_m1178747751 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OffMeshLinkGenerated::OffMeshLink_occupied(JSVCall)
extern "C"  void UnityEngine_OffMeshLinkGenerated_OffMeshLink_occupied_m2213060054 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OffMeshLinkGenerated::OffMeshLink_costOverride(JSVCall)
extern "C"  void UnityEngine_OffMeshLinkGenerated_OffMeshLink_costOverride_m2420086043 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OffMeshLinkGenerated::OffMeshLink_biDirectional(JSVCall)
extern "C"  void UnityEngine_OffMeshLinkGenerated_OffMeshLink_biDirectional_m1761503061 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OffMeshLinkGenerated::OffMeshLink_area(JSVCall)
extern "C"  void UnityEngine_OffMeshLinkGenerated_OffMeshLink_area_m4015141831 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OffMeshLinkGenerated::OffMeshLink_autoUpdatePositions(JSVCall)
extern "C"  void UnityEngine_OffMeshLinkGenerated_OffMeshLink_autoUpdatePositions_m2882971622 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OffMeshLinkGenerated::OffMeshLink_startTransform(JSVCall)
extern "C"  void UnityEngine_OffMeshLinkGenerated_OffMeshLink_startTransform_m1161129546 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OffMeshLinkGenerated::OffMeshLink_endTransform(JSVCall)
extern "C"  void UnityEngine_OffMeshLinkGenerated_OffMeshLink_endTransform_m2051095043 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_OffMeshLinkGenerated::OffMeshLink_UpdatePositions(JSVCall,System.Int32)
extern "C"  bool UnityEngine_OffMeshLinkGenerated_OffMeshLink_UpdatePositions_m4278590156 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OffMeshLinkGenerated::__Register()
extern "C"  void UnityEngine_OffMeshLinkGenerated___Register_m2016150221 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_OffMeshLinkGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_OffMeshLinkGenerated_ilo_attachFinalizerObject1_m1175007461 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_OffMeshLinkGenerated::ilo_getSingle2(System.Int32)
extern "C"  float UnityEngine_OffMeshLinkGenerated_ilo_getSingle2_m888722798 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_OffMeshLinkGenerated::ilo_setBooleanS3(System.Int32,System.Boolean)
extern "C"  void UnityEngine_OffMeshLinkGenerated_ilo_setBooleanS3_m1204128873 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_OffMeshLinkGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_OffMeshLinkGenerated_ilo_setObject4_m1247128403 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_OffMeshLinkGenerated::ilo_getObject5(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_OffMeshLinkGenerated_ilo_getObject5_m3974533241 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
