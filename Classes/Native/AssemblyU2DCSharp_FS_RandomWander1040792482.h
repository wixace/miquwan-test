﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FS_RandomWander
struct  FS_RandomWander_t1040792482  : public MonoBehaviour_t667441552
{
public:
	// System.Single FS_RandomWander::speed
	float ___speed_2;
	// System.Single FS_RandomWander::directionChangeInterval
	float ___directionChangeInterval_3;
	// System.Single FS_RandomWander::maxHeadingChange
	float ___maxHeadingChange_4;
	// System.Single FS_RandomWander::dist
	float ___dist_5;
	// System.Single FS_RandomWander::heading
	float ___heading_6;
	// UnityEngine.Vector3 FS_RandomWander::targetRotation
	Vector3_t4282066566  ___targetRotation_7;

public:
	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(FS_RandomWander_t1040792482, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_directionChangeInterval_3() { return static_cast<int32_t>(offsetof(FS_RandomWander_t1040792482, ___directionChangeInterval_3)); }
	inline float get_directionChangeInterval_3() const { return ___directionChangeInterval_3; }
	inline float* get_address_of_directionChangeInterval_3() { return &___directionChangeInterval_3; }
	inline void set_directionChangeInterval_3(float value)
	{
		___directionChangeInterval_3 = value;
	}

	inline static int32_t get_offset_of_maxHeadingChange_4() { return static_cast<int32_t>(offsetof(FS_RandomWander_t1040792482, ___maxHeadingChange_4)); }
	inline float get_maxHeadingChange_4() const { return ___maxHeadingChange_4; }
	inline float* get_address_of_maxHeadingChange_4() { return &___maxHeadingChange_4; }
	inline void set_maxHeadingChange_4(float value)
	{
		___maxHeadingChange_4 = value;
	}

	inline static int32_t get_offset_of_dist_5() { return static_cast<int32_t>(offsetof(FS_RandomWander_t1040792482, ___dist_5)); }
	inline float get_dist_5() const { return ___dist_5; }
	inline float* get_address_of_dist_5() { return &___dist_5; }
	inline void set_dist_5(float value)
	{
		___dist_5 = value;
	}

	inline static int32_t get_offset_of_heading_6() { return static_cast<int32_t>(offsetof(FS_RandomWander_t1040792482, ___heading_6)); }
	inline float get_heading_6() const { return ___heading_6; }
	inline float* get_address_of_heading_6() { return &___heading_6; }
	inline void set_heading_6(float value)
	{
		___heading_6 = value;
	}

	inline static int32_t get_offset_of_targetRotation_7() { return static_cast<int32_t>(offsetof(FS_RandomWander_t1040792482, ___targetRotation_7)); }
	inline Vector3_t4282066566  get_targetRotation_7() const { return ___targetRotation_7; }
	inline Vector3_t4282066566 * get_address_of_targetRotation_7() { return &___targetRotation_7; }
	inline void set_targetRotation_7(Vector3_t4282066566  value)
	{
		___targetRotation_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
