﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t4054002952;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1974256870;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PluginZongYou
struct  PluginZongYou_t3982613182  : public MonoBehaviour_t667441552
{
public:
	// System.String PluginZongYou::uname
	String_t* ___uname_2;
	// System.String PluginZongYou::uid
	String_t* ___uid_3;
	// System.String[] PluginZongYou::sdkInfos
	StringU5BU5D_t4054002952* ___sdkInfos_4;
	// System.String PluginZongYou::configId
	String_t* ___configId_5;
	// System.Boolean PluginZongYou::isOut
	bool ___isOut_6;

public:
	inline static int32_t get_offset_of_uname_2() { return static_cast<int32_t>(offsetof(PluginZongYou_t3982613182, ___uname_2)); }
	inline String_t* get_uname_2() const { return ___uname_2; }
	inline String_t** get_address_of_uname_2() { return &___uname_2; }
	inline void set_uname_2(String_t* value)
	{
		___uname_2 = value;
		Il2CppCodeGenWriteBarrier(&___uname_2, value);
	}

	inline static int32_t get_offset_of_uid_3() { return static_cast<int32_t>(offsetof(PluginZongYou_t3982613182, ___uid_3)); }
	inline String_t* get_uid_3() const { return ___uid_3; }
	inline String_t** get_address_of_uid_3() { return &___uid_3; }
	inline void set_uid_3(String_t* value)
	{
		___uid_3 = value;
		Il2CppCodeGenWriteBarrier(&___uid_3, value);
	}

	inline static int32_t get_offset_of_sdkInfos_4() { return static_cast<int32_t>(offsetof(PluginZongYou_t3982613182, ___sdkInfos_4)); }
	inline StringU5BU5D_t4054002952* get_sdkInfos_4() const { return ___sdkInfos_4; }
	inline StringU5BU5D_t4054002952** get_address_of_sdkInfos_4() { return &___sdkInfos_4; }
	inline void set_sdkInfos_4(StringU5BU5D_t4054002952* value)
	{
		___sdkInfos_4 = value;
		Il2CppCodeGenWriteBarrier(&___sdkInfos_4, value);
	}

	inline static int32_t get_offset_of_configId_5() { return static_cast<int32_t>(offsetof(PluginZongYou_t3982613182, ___configId_5)); }
	inline String_t* get_configId_5() const { return ___configId_5; }
	inline String_t** get_address_of_configId_5() { return &___configId_5; }
	inline void set_configId_5(String_t* value)
	{
		___configId_5 = value;
		Il2CppCodeGenWriteBarrier(&___configId_5, value);
	}

	inline static int32_t get_offset_of_isOut_6() { return static_cast<int32_t>(offsetof(PluginZongYou_t3982613182, ___isOut_6)); }
	inline bool get_isOut_6() const { return ___isOut_6; }
	inline bool* get_address_of_isOut_6() { return &___isOut_6; }
	inline void set_isOut_6(bool value)
	{
		___isOut_6 = value;
	}
};

struct PluginZongYou_t3982613182_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> PluginZongYou::<>f__switch$map26
	Dictionary_2_t1974256870 * ___U3CU3Ef__switchU24map26_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map26_7() { return static_cast<int32_t>(offsetof(PluginZongYou_t3982613182_StaticFields, ___U3CU3Ef__switchU24map26_7)); }
	inline Dictionary_2_t1974256870 * get_U3CU3Ef__switchU24map26_7() const { return ___U3CU3Ef__switchU24map26_7; }
	inline Dictionary_2_t1974256870 ** get_address_of_U3CU3Ef__switchU24map26_7() { return &___U3CU3Ef__switchU24map26_7; }
	inline void set_U3CU3Ef__switchU24map26_7(Dictionary_2_t1974256870 * value)
	{
		___U3CU3Ef__switchU24map26_7 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__switchU24map26_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
