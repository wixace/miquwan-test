﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t3674682005;
// UILabel
struct UILabel_t291504320;
// UISprite
struct UISprite_t661437049;

#include "AssemblyU2DCSharp_FloatTextUnit2362298029.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Float_exchange_copper
struct  Float_exchange_copper_t452915890  : public FloatTextUnit_t2362298029
{
public:
	// UnityEngine.GameObject Float_exchange_copper::_obj_one
	GameObject_t3674682005 * ____obj_one_3;
	// UnityEngine.GameObject Float_exchange_copper::_obj_one_normal
	GameObject_t3674682005 * ____obj_one_normal_4;
	// UnityEngine.GameObject Float_exchange_copper::_obj_one_crit
	GameObject_t3674682005 * ____obj_one_crit_5;
	// UILabel Float_exchange_copper::_label_one_copper
	UILabel_t291504320 * ____label_one_copper_6;
	// UISprite Float_exchange_copper::_sprite_one_crit_count
	UISprite_t661437049 * ____sprite_one_crit_count_7;
	// UnityEngine.GameObject Float_exchange_copper::_obj_ten
	GameObject_t3674682005 * ____obj_ten_8;
	// UILabel Float_exchange_copper::_label_ten_copper
	UILabel_t291504320 * ____label_ten_copper_9;
	// UILabel Float_exchange_copper::_label_ten_crit_count
	UILabel_t291504320 * ____label_ten_crit_count_10;
	// UnityEngine.GameObject Float_exchange_copper::_obj_bj
	GameObject_t3674682005 * ____obj_bj_11;

public:
	inline static int32_t get_offset_of__obj_one_3() { return static_cast<int32_t>(offsetof(Float_exchange_copper_t452915890, ____obj_one_3)); }
	inline GameObject_t3674682005 * get__obj_one_3() const { return ____obj_one_3; }
	inline GameObject_t3674682005 ** get_address_of__obj_one_3() { return &____obj_one_3; }
	inline void set__obj_one_3(GameObject_t3674682005 * value)
	{
		____obj_one_3 = value;
		Il2CppCodeGenWriteBarrier(&____obj_one_3, value);
	}

	inline static int32_t get_offset_of__obj_one_normal_4() { return static_cast<int32_t>(offsetof(Float_exchange_copper_t452915890, ____obj_one_normal_4)); }
	inline GameObject_t3674682005 * get__obj_one_normal_4() const { return ____obj_one_normal_4; }
	inline GameObject_t3674682005 ** get_address_of__obj_one_normal_4() { return &____obj_one_normal_4; }
	inline void set__obj_one_normal_4(GameObject_t3674682005 * value)
	{
		____obj_one_normal_4 = value;
		Il2CppCodeGenWriteBarrier(&____obj_one_normal_4, value);
	}

	inline static int32_t get_offset_of__obj_one_crit_5() { return static_cast<int32_t>(offsetof(Float_exchange_copper_t452915890, ____obj_one_crit_5)); }
	inline GameObject_t3674682005 * get__obj_one_crit_5() const { return ____obj_one_crit_5; }
	inline GameObject_t3674682005 ** get_address_of__obj_one_crit_5() { return &____obj_one_crit_5; }
	inline void set__obj_one_crit_5(GameObject_t3674682005 * value)
	{
		____obj_one_crit_5 = value;
		Il2CppCodeGenWriteBarrier(&____obj_one_crit_5, value);
	}

	inline static int32_t get_offset_of__label_one_copper_6() { return static_cast<int32_t>(offsetof(Float_exchange_copper_t452915890, ____label_one_copper_6)); }
	inline UILabel_t291504320 * get__label_one_copper_6() const { return ____label_one_copper_6; }
	inline UILabel_t291504320 ** get_address_of__label_one_copper_6() { return &____label_one_copper_6; }
	inline void set__label_one_copper_6(UILabel_t291504320 * value)
	{
		____label_one_copper_6 = value;
		Il2CppCodeGenWriteBarrier(&____label_one_copper_6, value);
	}

	inline static int32_t get_offset_of__sprite_one_crit_count_7() { return static_cast<int32_t>(offsetof(Float_exchange_copper_t452915890, ____sprite_one_crit_count_7)); }
	inline UISprite_t661437049 * get__sprite_one_crit_count_7() const { return ____sprite_one_crit_count_7; }
	inline UISprite_t661437049 ** get_address_of__sprite_one_crit_count_7() { return &____sprite_one_crit_count_7; }
	inline void set__sprite_one_crit_count_7(UISprite_t661437049 * value)
	{
		____sprite_one_crit_count_7 = value;
		Il2CppCodeGenWriteBarrier(&____sprite_one_crit_count_7, value);
	}

	inline static int32_t get_offset_of__obj_ten_8() { return static_cast<int32_t>(offsetof(Float_exchange_copper_t452915890, ____obj_ten_8)); }
	inline GameObject_t3674682005 * get__obj_ten_8() const { return ____obj_ten_8; }
	inline GameObject_t3674682005 ** get_address_of__obj_ten_8() { return &____obj_ten_8; }
	inline void set__obj_ten_8(GameObject_t3674682005 * value)
	{
		____obj_ten_8 = value;
		Il2CppCodeGenWriteBarrier(&____obj_ten_8, value);
	}

	inline static int32_t get_offset_of__label_ten_copper_9() { return static_cast<int32_t>(offsetof(Float_exchange_copper_t452915890, ____label_ten_copper_9)); }
	inline UILabel_t291504320 * get__label_ten_copper_9() const { return ____label_ten_copper_9; }
	inline UILabel_t291504320 ** get_address_of__label_ten_copper_9() { return &____label_ten_copper_9; }
	inline void set__label_ten_copper_9(UILabel_t291504320 * value)
	{
		____label_ten_copper_9 = value;
		Il2CppCodeGenWriteBarrier(&____label_ten_copper_9, value);
	}

	inline static int32_t get_offset_of__label_ten_crit_count_10() { return static_cast<int32_t>(offsetof(Float_exchange_copper_t452915890, ____label_ten_crit_count_10)); }
	inline UILabel_t291504320 * get__label_ten_crit_count_10() const { return ____label_ten_crit_count_10; }
	inline UILabel_t291504320 ** get_address_of__label_ten_crit_count_10() { return &____label_ten_crit_count_10; }
	inline void set__label_ten_crit_count_10(UILabel_t291504320 * value)
	{
		____label_ten_crit_count_10 = value;
		Il2CppCodeGenWriteBarrier(&____label_ten_crit_count_10, value);
	}

	inline static int32_t get_offset_of__obj_bj_11() { return static_cast<int32_t>(offsetof(Float_exchange_copper_t452915890, ____obj_bj_11)); }
	inline GameObject_t3674682005 * get__obj_bj_11() const { return ____obj_bj_11; }
	inline GameObject_t3674682005 ** get_address_of__obj_bj_11() { return &____obj_bj_11; }
	inline void set__obj_bj_11(GameObject_t3674682005 * value)
	{
		____obj_bj_11 = value;
		Il2CppCodeGenWriteBarrier(&____obj_bj_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
