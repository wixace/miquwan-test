﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/MeshInstance>
struct DefaultComparer_t2569742905;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_MeshSubsetCombineUtility_M4121966238.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::.ctor()
extern "C"  void DefaultComparer__ctor_m1780700700_gshared (DefaultComparer_t2569742905 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m1780700700(__this, method) ((  void (*) (DefaultComparer_t2569742905 *, const MethodInfo*))DefaultComparer__ctor_m1780700700_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::GetHashCode(T)
extern "C"  int32_t DefaultComparer_GetHashCode_m4242040079_gshared (DefaultComparer_t2569742905 * __this, MeshInstance_t4121966238  ___obj0, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m4242040079(__this, ___obj0, method) ((  int32_t (*) (DefaultComparer_t2569742905 *, MeshInstance_t4121966238 , const MethodInfo*))DefaultComparer_GetHashCode_m4242040079_gshared)(__this, ___obj0, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.MeshSubsetCombineUtility/MeshInstance>::Equals(T,T)
extern "C"  bool DefaultComparer_Equals_m1290398829_gshared (DefaultComparer_t2569742905 * __this, MeshInstance_t4121966238  ___x0, MeshInstance_t4121966238  ___y1, const MethodInfo* method);
#define DefaultComparer_Equals_m1290398829(__this, ___x0, ___y1, method) ((  bool (*) (DefaultComparer_t2569742905 *, MeshInstance_t4121966238 , MeshInstance_t4121966238 , const MethodInfo*))DefaultComparer_Equals_m1290398829_gshared)(__this, ___x0, ___y1, method)
