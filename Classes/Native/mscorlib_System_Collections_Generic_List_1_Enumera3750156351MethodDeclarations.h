﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<FloatTextUnit>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m1623625428(__this, ___l0, method) ((  void (*) (Enumerator_t3750156351 *, List_1_t3730483581 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FloatTextUnit>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m4170682622(__this, method) ((  void (*) (Enumerator_t3750156351 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<FloatTextUnit>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m494115754(__this, method) ((  Il2CppObject * (*) (Enumerator_t3750156351 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FloatTextUnit>::Dispose()
#define Enumerator_Dispose_m1843728825(__this, method) ((  void (*) (Enumerator_t3750156351 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FloatTextUnit>::VerifyState()
#define Enumerator_VerifyState_m1448561778(__this, method) ((  void (*) (Enumerator_t3750156351 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<FloatTextUnit>::MoveNext()
#define Enumerator_MoveNext_m2326817258(__this, method) ((  bool (*) (Enumerator_t3750156351 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<FloatTextUnit>::get_Current()
#define Enumerator_get_Current_m4124697705(__this, method) ((  FloatTextUnit_t2362298029 * (*) (Enumerator_t3750156351 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
