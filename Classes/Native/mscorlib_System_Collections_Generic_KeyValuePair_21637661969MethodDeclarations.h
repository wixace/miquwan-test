﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21637661969.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2617982879_gshared (KeyValuePair_2_t1637661969 * __this, uint16_t ___key0, Il2CppObject * ___value1, const MethodInfo* method);
#define KeyValuePair_2__ctor_m2617982879(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t1637661969 *, uint16_t, Il2CppObject *, const MethodInfo*))KeyValuePair_2__ctor_m2617982879_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>::get_Key()
extern "C"  uint16_t KeyValuePair_2_get_Key_m1818977513_gshared (KeyValuePair_2_t1637661969 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m1818977513(__this, method) ((  uint16_t (*) (KeyValuePair_2_t1637661969 *, const MethodInfo*))KeyValuePair_2_get_Key_m1818977513_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2765394730_gshared (KeyValuePair_2_t1637661969 * __this, uint16_t ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m2765394730(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1637661969 *, uint16_t, const MethodInfo*))KeyValuePair_2_set_Key_m2765394730_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1147557709_gshared (KeyValuePair_2_t1637661969 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m1147557709(__this, method) ((  Il2CppObject * (*) (KeyValuePair_2_t1637661969 *, const MethodInfo*))KeyValuePair_2_get_Value_m1147557709_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m687751722_gshared (KeyValuePair_2_t1637661969 * __this, Il2CppObject * ___value0, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m687751722(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t1637661969 *, Il2CppObject *, const MethodInfo*))KeyValuePair_2_set_Value_m687751722_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.UInt16,System.Object>::ToString()
extern "C"  String_t* KeyValuePair_2_ToString_m3996047390_gshared (KeyValuePair_2_t1637661969 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m3996047390(__this, method) ((  String_t* (*) (KeyValuePair_2_t1637661969 *, const MethodInfo*))KeyValuePair_2_ToString_m3996047390_gshared)(__this, method)
