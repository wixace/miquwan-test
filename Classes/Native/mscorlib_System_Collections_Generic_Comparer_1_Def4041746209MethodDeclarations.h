﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<Entity.Behavior.EBehaviorID>
struct DefaultComparer_t4041746209;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Entity_Behavior_EBehaviorID1238533136.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<Entity.Behavior.EBehaviorID>::.ctor()
extern "C"  void DefaultComparer__ctor_m2018285061_gshared (DefaultComparer_t4041746209 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m2018285061(__this, method) ((  void (*) (DefaultComparer_t4041746209 *, const MethodInfo*))DefaultComparer__ctor_m2018285061_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<Entity.Behavior.EBehaviorID>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m2449698890_gshared (DefaultComparer_t4041746209 * __this, uint8_t ___x0, uint8_t ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m2449698890(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t4041746209 *, uint8_t, uint8_t, const MethodInfo*))DefaultComparer_Compare_m2449698890_gshared)(__this, ___x0, ___y1, method)
