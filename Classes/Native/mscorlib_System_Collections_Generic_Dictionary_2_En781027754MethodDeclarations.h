﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E2618340702MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,HatredCtrl/stValue>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m3261130824(__this, ___dictionary0, method) ((  void (*) (Enumerator_t781027754 *, Dictionary_2_t3758671658 *, const MethodInfo*))Enumerator__ctor_m1640369951_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,HatredCtrl/stValue>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m184890841(__this, method) ((  Il2CppObject * (*) (Enumerator_t781027754 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3140493548_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,HatredCtrl/stValue>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1142885229(__this, method) ((  void (*) (Enumerator_t781027754 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m2071292982_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,HatredCtrl/stValue>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m1533487798(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t781027754 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m3903294253_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,HatredCtrl/stValue>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m1111550965(__this, method) ((  Il2CppObject * (*) (Enumerator_t781027754 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m4033215688_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,HatredCtrl/stValue>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m4097024327(__this, method) ((  Il2CppObject * (*) (Enumerator_t781027754 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m2908211546_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,HatredCtrl/stValue>::MoveNext()
#define Enumerator_MoveNext_m1863165328(__this, method) ((  bool (*) (Enumerator_t781027754 *, const MethodInfo*))Enumerator_MoveNext_m3843515750_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,HatredCtrl/stValue>::get_Current()
#define Enumerator_get_Current_m1462687631(__this, method) ((  KeyValuePair_2_t3657452364  (*) (Enumerator_t781027754 *, const MethodInfo*))Enumerator_get_Current_m2556378390_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,HatredCtrl/stValue>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m619797222(__this, method) ((  CombatEntity_t684137495 * (*) (Enumerator_t781027754 *, const MethodInfo*))Enumerator_get_CurrentKey_m726024431_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,HatredCtrl/stValue>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m1947141386(__this, method) ((  stValue_t3425945410  (*) (Enumerator_t781027754 *, const MethodInfo*))Enumerator_get_CurrentValue_m453661935_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,HatredCtrl/stValue>::Reset()
#define Enumerator_Reset_m45717146(__this, method) ((  void (*) (Enumerator_t781027754 *, const MethodInfo*))Enumerator_Reset_m439249649_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,HatredCtrl/stValue>::VerifyState()
#define Enumerator_VerifyState_m1061423907(__this, method) ((  void (*) (Enumerator_t781027754 *, const MethodInfo*))Enumerator_VerifyState_m2466775610_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,HatredCtrl/stValue>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m571029963(__this, method) ((  void (*) (Enumerator_t781027754 *, const MethodInfo*))Enumerator_VerifyCurrent_m2494285602_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<CombatEntity,HatredCtrl/stValue>::Dispose()
#define Enumerator_Dispose_m950842145(__this, method) ((  void (*) (Enumerator_t781027754 *, const MethodInfo*))Enumerator_Dispose_m3735546753_gshared)(__this, method)
