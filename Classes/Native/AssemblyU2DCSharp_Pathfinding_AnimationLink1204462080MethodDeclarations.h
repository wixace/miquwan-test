﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.AnimationLink
struct AnimationLink_t1204462080;
// UnityEngine.Transform
struct Transform_t1659122786;
// System.String
struct String_t;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1355284822;
// Pathfinding.NodeLink2
struct NodeLink2_t1645404664;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_Pathfinding_NodeLink21645404664.h"

// System.Void Pathfinding.AnimationLink::.ctor()
extern "C"  void AnimationLink__ctor_m920673079 (AnimationLink_t1204462080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Pathfinding.AnimationLink::SearchRec(UnityEngine.Transform,System.String)
extern "C"  Transform_t1659122786 * AnimationLink_SearchRec_m498642654 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___tr0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AnimationLink::CalculateOffsets(System.Collections.Generic.List`1<UnityEngine.Vector3>,UnityEngine.Vector3&)
extern "C"  void AnimationLink_CalculateOffsets_m2400662261 (AnimationLink_t1204462080 * __this, List_1_t1355284822 * ___trace0, Vector3_t4282066566 * ___endPosition1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AnimationLink::OnDrawGizmosSelected()
extern "C"  void AnimationLink_OnDrawGizmosSelected_m4177659396 (AnimationLink_t1204462080 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform Pathfinding.AnimationLink::ilo_SearchRec1(UnityEngine.Transform,System.String)
extern "C"  Transform_t1659122786 * AnimationLink_ilo_SearchRec1_m2553185518 (Il2CppObject * __this /* static, unused */, Transform_t1659122786 * ___tr0, String_t* ___name1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.AnimationLink::ilo_OnDrawGizmosSelected2(Pathfinding.NodeLink2)
extern "C"  void AnimationLink_ilo_OnDrawGizmosSelected2_m2575040297 (Il2CppObject * __this /* static, unused */, NodeLink2_t1645404664 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
