﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BuffCtrl
struct BuffCtrl_t2836564350;
// CombatEntity
struct CombatEntity_t684137495;
// buffCfg
struct buffCfg_t227963665;
// Buff
struct Buff_t2081907;
// Skill
struct Skill_t79944241;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.List`1<BuffState>
struct List_1_t3417094830;
// BuffState
struct BuffState_t2048909278;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// FightEnum.ESkillBuffEvent[]
struct ESkillBuffEventU5BU5D_t2768361140;
// NpcMgr
struct NpcMgr_t2339534743;
// HeroMgr
struct HeroMgr_t2475965342;
// System.Collections.Generic.List`1<CombatEntity>
struct List_1_t2052323047;
// GameMgr
struct GameMgr_t1469029542;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// System.String
struct String_t;
// FightCtrl
struct FightCtrl_t648967803;
// AnimationRunner
struct AnimationRunner_t1015409588;
// Entity.Behavior.IBehaviorCtrl
struct IBehaviorCtrl_t4225040900;
// Blood
struct Blood_t64280026;
// EffectMgr
struct EffectMgr_t535289511;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CombatEntity684137495.h"
#include "AssemblyU2DCSharp_buffCfg227963665.h"
#include "AssemblyU2DCSharp_BuffEnum_EBuffFunType4189275319.h"
#include "AssemblyU2DCSharp_Buff2081907.h"
#include "AssemblyU2DCSharp_BuffEnum_EBuffState2159802479.h"
#include "AssemblyU2DCSharp_Skill79944241.h"
#include "AssemblyU2DCSharp_FightEnum_ESkillBuffEvent465856265.h"
#include "AssemblyU2DCSharp_FightEnum_ESkillBuffTarget2441814200.h"
#include "AssemblyU2DCSharp_BuffState2048909278.h"
#include "AssemblyU2DCSharp_BuffCtrl2836564350.h"
#include "AssemblyU2DCSharp_EntityEnum_UnitType643359572.h"
#include "AssemblyU2DCSharp_NpcMgr2339534743.h"
#include "AssemblyU2DCSharp_HeroMgr2475965342.h"
#include "AssemblyU2DCSharp_HERO_ELEMENT2692579223.h"
#include "AssemblyU2DCSharp_HERO_COUNTRY1018279985.h"
#include "AssemblyU2DCSharp_GameMgr1469029542.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_HeadUpBloodMgr_LabelType4067300664.h"
#include "AssemblyU2DCSharp_FightCtrl648967803.h"
#include "AssemblyU2DCSharp_AnimationRunner1015409588.h"
#include "AssemblyU2DCSharp_AnimationRunner_AniType1006238651.h"
#include "AssemblyU2DCSharp_Entity_Behavior_IBehaviorCtrl4225040900.h"
#include "AssemblyU2DCSharp_EffectMgr535289511.h"
#include "AssemblyU2DCSharp_FightEnum_EFightRst3627658870.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "AssemblyU2DCSharp_FightEnum_EDamageType2535357340.h"

// System.Void BuffCtrl::.ctor(CombatEntity)
extern "C"  void BuffCtrl__ctor_m2326264742 (BuffCtrl_t2836564350 * __this, CombatEntity_t684137495 * ___entity0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::.cctor()
extern "C"  void BuffCtrl__cctor_m2443584624 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::Update()
extern "C"  void BuffCtrl_Update_m357575632 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::GetPetrifiedLastTime()
extern "C"  bool BuffCtrl_GetPetrifiedLastTime_m3070195552 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::GetGodDownLastTime()
extern "C"  bool BuffCtrl_GetGodDownLastTime_m2182743782 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::RemoveTickBuff(System.Single)
extern "C"  void BuffCtrl_RemoveTickBuff_m1564076816 (BuffCtrl_t2836564350 * __this, float ___time0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::RecycleBuff(System.Int32,System.Boolean)
extern "C"  void BuffCtrl_RecycleBuff_m3644307691 (BuffCtrl_t2836564350 * __this, int32_t ___index0, bool ___isRemoveEffect1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::GetNDPBuffID(System.Int32)
extern "C"  int32_t BuffCtrl_GetNDPBuffID_m1454191416 (BuffCtrl_t2836564350 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::GetBuffIconState()
extern "C"  int32_t BuffCtrl_GetBuffIconState_m2728214500 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::RemoveNDPBuff(System.Int32)
extern "C"  void BuffCtrl_RemoveNDPBuff_m3423694549 (BuffCtrl_t2836564350 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::RemoveBuffByID(System.Int32)
extern "C"  void BuffCtrl_RemoveBuffByID_m1803862433 (BuffCtrl_t2836564350 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::FreezeBuff()
extern "C"  void BuffCtrl_FreezeBuff_m4067790673 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::MorphBuff()
extern "C"  void BuffCtrl_MorphBuff_m552671670 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::AnitControlBuff()
extern "C"  void BuffCtrl_AnitControlBuff_m4061703059 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::IsControlBuff(buffCfg)
extern "C"  bool BuffCtrl_IsControlBuff_m3726311124 (BuffCtrl_t2836564350 * __this, buffCfg_t227963665 * ___buffDB0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::IsControlFun(System.Int32)
extern "C"  bool BuffCtrl_IsControlFun_m1312199416 (BuffCtrl_t2836564350 * __this, int32_t ___fun0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::IsHaveFunBuff(BuffEnum.EBuffFunType)
extern "C"  bool BuffCtrl_IsHaveFunBuff_m1889166512 (BuffCtrl_t2836564350 * __this, int32_t ___fun0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::IsHaveFunBuff(Buff,BuffEnum.EBuffFunType)
extern "C"  bool BuffCtrl_IsHaveFunBuff_m1774468549 (BuffCtrl_t2836564350 * __this, Buff_t2081907 * ___buff0, int32_t ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::StateChange(BuffEnum.EBuffState)
extern "C"  int32_t BuffCtrl_StateChange_m1643241777 (BuffCtrl_t2836564350 * __this, uint64_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::AddSkillBuff(Skill,FightEnum.ESkillBuffEvent,System.Boolean)
extern "C"  void BuffCtrl_AddSkillBuff_m2990906040 (BuffCtrl_t2836564350 * __this, Skill_t79944241 * ___skill0, uint8_t ___buffEvent1, bool ___isMutex2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::AddSkillBuff(System.Int32,FightEnum.ESkillBuffTarget,System.Int32,Skill)
extern "C"  void BuffCtrl_AddSkillBuff_m976532436 (BuffCtrl_t2836564350 * __this, int32_t ___id0, uint8_t ___target1, int32_t ___skillBuffHit2, Skill_t79944241 * ___skill3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::AddSkillBuff(System.Int32,System.Int32,CombatEntity,Skill)
extern "C"  void BuffCtrl_AddSkillBuff_m1918973230 (BuffCtrl_t2836564350 * __this, int32_t ___id0, int32_t ___skillBuffHit1, CombatEntity_t684137495 * ___src2, Skill_t79944241 * ___skill3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::AddBuff(System.Int32,CombatEntity)
extern "C"  void BuffCtrl_AddBuff_m777140207 (BuffCtrl_t2836564350 * __this, int32_t ___id0, CombatEntity_t684137495 * ___src1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::CachaBuffAction(Buff,CombatEntity)
extern "C"  void BuffCtrl_CachaBuffAction_m2886138496 (BuffCtrl_t2836564350 * __this, Buff_t2081907 * ___buff0, CombatEntity_t684137495 * ___src1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::RealBuffAction(Buff,System.Boolean)
extern "C"  void BuffCtrl_RealBuffAction_m2464859266 (BuffCtrl_t2836564350 * __this, Buff_t2081907 * ___buff0, bool ___isSkill1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::AddBuff(System.Int32)
extern "C"  void BuffCtrl_AddBuff_m2308528096 (BuffCtrl_t2836564350 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::FiltrateTarget(Buff)
extern "C"  bool BuffCtrl_FiltrateTarget_m2652681028 (BuffCtrl_t2836564350 * __this, Buff_t2081907 * ___buff0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::AddBuff(Buff)
extern "C"  bool BuffCtrl_AddBuff_m1001680584 (BuffCtrl_t2836564350 * __this, Buff_t2081907 * ___buff0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::AddEffect(buffCfg,CombatEntity)
extern "C"  void BuffCtrl_AddEffect_m3893564795 (BuffCtrl_t2836564350 * __this, buffCfg_t227963665 * ___buffInfo0, CombatEntity_t684137495 * ___src1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::RemoveEffect(buffCfg)
extern "C"  void BuffCtrl_RemoveEffect_m2062454085 (BuffCtrl_t2836564350 * __this, buffCfg_t227963665 * ___buffInfo0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::HasBuffState(BuffEnum.EBuffState)
extern "C"  bool BuffCtrl_HasBuffState_m2477048480 (BuffCtrl_t2836564350 * __this, uint64_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::buffClear()
extern "C"  void BuffCtrl_buffClear_m2968313429 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::IsHaveBuff(System.Int32)
extern "C"  bool BuffCtrl_IsHaveBuff_m198969137 (BuffCtrl_t2836564350 * __this, int32_t ___buffID0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_curnum()
extern "C"  int32_t BuffCtrl_get_curnum_m1392207940 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_curnum(System.Int32)
extern "C"  void BuffCtrl_set_curnum_m637037435 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mAnitNum()
extern "C"  int32_t BuffCtrl_get_mAnitNum_m1131462175 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mAnitNum(System.Int32)
extern "C"  void BuffCtrl_set_mAnitNum_m2971103062 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mAuraAddHPRadius()
extern "C"  int32_t BuffCtrl_get_mAuraAddHPRadius_m1752658761 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mAuraAddHPRadius(System.Int32)
extern "C"  void BuffCtrl_set_mAuraAddHPRadius_m3408392064 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mAuraReduceHPRadius()
extern "C"  int32_t BuffCtrl_get_mAuraReduceHPRadius_m3350189908 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mAuraReduceHPRadius(System.Int32)
extern "C"  void BuffCtrl_set_mAuraReduceHPRadius_m1893572735 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mHpLineNum()
extern "C"  int32_t BuffCtrl_get_mHpLineNum_m1653146011 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mHpLineNum(System.Int32)
extern "C"  void BuffCtrl_set_mHpLineNum_m3306641234 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mRuneTakeEffectNum()
extern "C"  int32_t BuffCtrl_get_mRuneTakeEffectNum_m987600037 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mRuneTakeEffectNum(System.Int32)
extern "C"  void BuffCtrl_set_mRuneTakeEffectNum_m1653695836 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::get_mGodDownTweenTime()
extern "C"  float BuffCtrl_get_mGodDownTweenTime_m977014125 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mGodDownTweenTime(System.Single)
extern "C"  void BuffCtrl_set_mGodDownTweenTime_m875362822 (BuffCtrl_t2836564350 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mAntiDebuffDiscreteAddBuffID()
extern "C"  int32_t BuffCtrl_get_mAntiDebuffDiscreteAddBuffID_m90563921 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mAntiDebuffDiscreteAddBuffID(System.Int32)
extern "C"  void BuffCtrl_set_mAntiDebuffDiscreteAddBuffID_m1476799624 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mAntiDebuffDiscreteID()
extern "C"  int32_t BuffCtrl_get_mAntiDebuffDiscreteID_m2052702971 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mAntiDebuffDiscreteID(System.Int32)
extern "C"  void BuffCtrl_set_mAntiDebuffDiscreteID_m321076006 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::get_mPetrifiedChangeTime()
extern "C"  float BuffCtrl_get_mPetrifiedChangeTime_m3764565956 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mPetrifiedChangeTime(System.Single)
extern "C"  void BuffCtrl_set_mPetrifiedChangeTime_m2950093503 (BuffCtrl_t2836564350 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mChangeOldSkillID()
extern "C"  int32_t BuffCtrl_get_mChangeOldSkillID_m911071814 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mChangeOldSkillID(System.Int32)
extern "C"  void BuffCtrl_set_mChangeOldSkillID_m2427156209 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mWhenCritAddBuffID()
extern "C"  int32_t BuffCtrl_get_mWhenCritAddBuffID_m59651948 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mWhenCritAddBuffID(System.Int32)
extern "C"  void BuffCtrl_set_mWhenCritAddBuffID_m4114159011 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mThumpSkillID()
extern "C"  int32_t BuffCtrl_get_mThumpSkillID_m511226713 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mThumpSkillID(System.Int32)
extern "C"  void BuffCtrl_set_mThumpSkillID_m2903804036 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mTankBuffID()
extern "C"  int32_t BuffCtrl_get_mTankBuffID_m3348059369 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mTankBuffID(System.Int32)
extern "C"  void BuffCtrl_set_mTankBuffID_m3531661460 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mReduceRageHpNum()
extern "C"  int32_t BuffCtrl_get_mReduceRageHpNum_m1854102492 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mReduceRageHpNum(System.Int32)
extern "C"  void BuffCtrl_set_mReduceRageHpNum_m3419706259 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mGhostIndex()
extern "C"  int32_t BuffCtrl_get_mGhostIndex_m1278763892 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mGhostIndex(System.Int32)
extern "C"  void BuffCtrl_set_mGhostIndex_m1294562975 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mSusanooIndex()
extern "C"  int32_t BuffCtrl_get_mSusanooIndex_m2748551813 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mSusanooIndex(System.Int32)
extern "C"  void BuffCtrl_set_mSusanooIndex_m480118192 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::get_mDistorTionDelayTime()
extern "C"  float BuffCtrl_get_mDistorTionDelayTime_m941777154 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mDistorTionDelayTime(System.Single)
extern "C"  void BuffCtrl_set_mDistorTionDelayTime_m1145611841 (BuffCtrl_t2836564350 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mDeathHarvestLine()
extern "C"  int32_t BuffCtrl_get_mDeathHarvestLine_m4045022810 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mDeathHarvestLine(System.Int32)
extern "C"  void BuffCtrl_set_mDeathHarvestLine_m27411717 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mResistanceCountryType()
extern "C"  int32_t BuffCtrl_get_mResistanceCountryType_m408998792 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mResistanceCountryType(System.Int32)
extern "C"  void BuffCtrl_set_mResistanceCountryType_m2205844671 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mTauntDis()
extern "C"  int32_t BuffCtrl_get_mTauntDis_m2750312177 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mTauntDis(System.Int32)
extern "C"  void BuffCtrl_set_mTauntDis_m171859228 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffATNum()
extern "C"  int32_t BuffCtrl_get_mBuffATNum_m410057009 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffATNum(System.Int32)
extern "C"  void BuffCtrl_set_mBuffATNum_m76193256 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffPDNum()
extern "C"  int32_t BuffCtrl_get_mBuffPDNum_m379643920 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffPDNum(System.Int32)
extern "C"  void BuffCtrl_set_mBuffPDNum_m3362272839 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffMDNum()
extern "C"  int32_t BuffCtrl_get_mBuffMDNum_m2012100173 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffMDNum(System.Int32)
extern "C"  void BuffCtrl_set_mBuffMDNum_m1600721156 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffMaxHPNum()
extern "C"  int32_t BuffCtrl_get_mBuffMaxHPNum_m3438914910 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffMaxHPNum(System.Int32)
extern "C"  void BuffCtrl_set_mBuffMaxHPNum_m947757833 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffHitsNum()
extern "C"  int32_t BuffCtrl_get_mBuffHitsNum_m681668324 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffHitsNum(System.Int32)
extern "C"  void BuffCtrl_set_mBuffHitsNum_m1070103963 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffDodgeNum()
extern "C"  int32_t BuffCtrl_get_mBuffDodgeNum_m3225564627 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffDodgeNum(System.Int32)
extern "C"  void BuffCtrl_set_mBuffDodgeNum_m488383486 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffWreckNum()
extern "C"  int32_t BuffCtrl_get_mBuffWreckNum_m1023949752 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffWreckNum(System.Int32)
extern "C"  void BuffCtrl_set_mBuffWreckNum_m2335668835 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffWithstandNum()
extern "C"  int32_t BuffCtrl_get_mBuffWithstandNum_m3762178298 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffWithstandNum(System.Int32)
extern "C"  void BuffCtrl_set_mBuffWithstandNum_m3548870565 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffCritNum()
extern "C"  int32_t BuffCtrl_get_mBuffCritNum_m2785420426 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffCritNum(System.Int32)
extern "C"  void BuffCtrl_set_mBuffCritNum_m331082305 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffTenacityNum()
extern "C"  int32_t BuffCtrl_get_mBuffTenacityNum_m3111275957 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffTenacityNum(System.Int32)
extern "C"  void BuffCtrl_set_mBuffTenacityNum_m2817689580 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 BuffCtrl::get_mBuffRHurtNum()
extern "C"  uint32_t BuffCtrl_get_mBuffRHurtNum_m1431650652 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffRHurtNum(System.UInt32)
extern "C"  void BuffCtrl_set_mBuffRHurtNum_m692586807 (BuffCtrl_t2836564350 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffAT()
extern "C"  int32_t BuffCtrl_get_mBuffAT_m3043440375 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffAT(System.Int32)
extern "C"  void BuffCtrl_set_mBuffAT_m3581156002 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffPD()
extern "C"  int32_t BuffCtrl_get_mBuffPD_m3043871864 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffPD(System.Int32)
extern "C"  void BuffCtrl_set_mBuffPD_m1867085475 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffMD()
extern "C"  int32_t BuffCtrl_get_mBuffMD_m3043782491 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffMD(System.Int32)
extern "C"  void BuffCtrl_set_mBuffMD_m3398688006 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffMaxHP()
extern "C"  int32_t BuffCtrl_get_mBuffMaxHP_m2879764970 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffMaxHP(System.Int32)
extern "C"  void BuffCtrl_set_mBuffMaxHP_m189731105 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffHits()
extern "C"  int32_t BuffCtrl_get_mBuffHits_m95606308 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffHits(System.Int32)
extern "C"  void BuffCtrl_set_mBuffHits_m565874639 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffDodge()
extern "C"  int32_t BuffCtrl_get_mBuffDodge_m3865447829 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffDodge(System.Int32)
extern "C"  void BuffCtrl_set_mBuffDodge_m4071636044 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffWreck()
extern "C"  int32_t BuffCtrl_get_mBuffWreck_m3634846160 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffWreck(System.Int32)
extern "C"  void BuffCtrl_set_mBuffWreck_m1771177991 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffWithstand()
extern "C"  int32_t BuffCtrl_get_mBuffWithstand_m1808016334 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffWithstand(System.Int32)
extern "C"  void BuffCtrl_set_mBuffWithstand_m1331356165 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffCrit()
extern "C"  int32_t BuffCtrl_get_mBuffCrit_m4255412798 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffCrit(System.Int32)
extern "C"  void BuffCtrl_set_mBuffCrit_m3379614953 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffTenacity()
extern "C"  int32_t BuffCtrl_get_mBuffTenacity_m161861875 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffTenacity(System.Int32)
extern "C"  void BuffCtrl_set_mBuffTenacity_m2404388638 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 BuffCtrl::get_mBuffRHurt()
extern "C"  uint32_t BuffCtrl_get_mBuffRHurt_m486332076 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffRHurt(System.UInt32)
extern "C"  void BuffCtrl_set_mBuffRHurt_m2973051989 (BuffCtrl_t2836564350 * __this, uint32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::get_mHPSecDecPer()
extern "C"  float BuffCtrl_get_mHPSecDecPer_m3481189747 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mHPSecDecPer(System.Single)
extern "C"  void BuffCtrl_set_mHPSecDecPer_m2324406640 (BuffCtrl_t2836564350 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::get_mHPSecDecNum()
extern "C"  float BuffCtrl_get_mHPSecDecNum_m3479814556 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mHPSecDecNum(System.Single)
extern "C"  void BuffCtrl_set_mHPSecDecNum_m2754373607 (BuffCtrl_t2836564350 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::get_mHPRstNum()
extern "C"  float BuffCtrl_get_mHPRstNum_m1279199278 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mHPRstNum(System.Single)
extern "C"  void BuffCtrl_set_mHPRstNum_m3591399653 (BuffCtrl_t2836564350 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::get_mHPRstPer()
extern "C"  float BuffCtrl_get_mHPRstPer_m1280574469 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mHPRstPer(System.Single)
extern "C"  void BuffCtrl_set_mHPRstPer_m3161432686 (BuffCtrl_t2836564350 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::get_mAttackSpeed()
extern "C"  float BuffCtrl_get_mAttackSpeed_m4054744110 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mAttackSpeed(System.Single)
extern "C"  void BuffCtrl_set_mAttackSpeed_m1164110485 (BuffCtrl_t2836564350 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::get_mModelScale()
extern "C"  float BuffCtrl_get_mModelScale_m577286804 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mModelScale(System.Single)
extern "C"  void BuffCtrl_set_mModelScale_m3427715647 (BuffCtrl_t2836564350 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::get_mTreatmentEffect()
extern "C"  float BuffCtrl_get_mTreatmentEffect_m918532088 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mTreatmentEffect(System.Single)
extern "C"  void BuffCtrl_set_mTreatmentEffect_m2274411275 (BuffCtrl_t2836564350 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::get_mHurtEffectPlus()
extern "C"  float BuffCtrl_get_mHurtEffectPlus_m1402061805 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mHurtEffectPlus(System.Single)
extern "C"  void BuffCtrl_set_mHurtEffectPlus_m639556998 (BuffCtrl_t2836564350 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::get_mHurtEffectReduction()
extern "C"  float BuffCtrl_get_mHurtEffectReduction_m3196176930 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mHurtEffectReduction(System.Single)
extern "C"  void BuffCtrl_set_mHurtEffectReduction_m2209681697 (BuffCtrl_t2836564350 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::get_mVampire()
extern "C"  float BuffCtrl_get_mVampire_m1908292765 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mVampire(System.Single)
extern "C"  void BuffCtrl_set_mVampire_m66617990 (BuffCtrl_t2836564350 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mAngerSecDecNum()
extern "C"  int32_t BuffCtrl_get_mAngerSecDecNum_m1305830591 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mAngerSecDecNum(System.Int32)
extern "C"  void BuffCtrl_set_mAngerSecDecNum_m1782735466 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::get_mRunspeed()
extern "C"  float BuffCtrl_get_mRunspeed_m2618014031 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mRunspeed(System.Single)
extern "C"  void BuffCtrl_set_mRunspeed_m130176356 (BuffCtrl_t2836564350 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mHidePart()
extern "C"  int32_t BuffCtrl_get_mHidePart_m3569539014 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mHidePart(System.Int32)
extern "C"  void BuffCtrl_set_mHidePart_m1253781105 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::get_mHPSecDecNumATK()
extern "C"  float BuffCtrl_get_mHPSecDecNumATK_m3849223934 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mHPSecDecNumATK(System.Single)
extern "C"  void BuffCtrl_set_mHPSecDecNumATK_m2076468757 (BuffCtrl_t2836564350 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::get_mHPRstNumATK()
extern "C"  float BuffCtrl_get_mHPRstNumATK_m3700283180 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mHPRstNumATK(System.Single)
extern "C"  void BuffCtrl_set_mHPRstNumATK_m1339284567 (BuffCtrl_t2836564350 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mRestrintCountryType()
extern "C"  int32_t BuffCtrl_get_mRestrintCountryType_m1052361392 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mRestrintCountryType(System.Int32)
extern "C"  void BuffCtrl_set_mRestrintCountryType_m1856682343 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::get_mRestrintPer()
extern "C"  float BuffCtrl_get_mRestrintPer_m1539046747 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mRestrintPer(System.Single)
extern "C"  void BuffCtrl_set_mRestrintPer_m3473635976 (BuffCtrl_t2836564350 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mRestrintStateType()
extern "C"  int32_t BuffCtrl_get_mRestrintStateType_m2032249643 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mRestrintStateType(System.Int32)
extern "C"  void BuffCtrl_set_mRestrintStateType_m1979310818 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::get_mRestrintStatePer()
extern "C"  float BuffCtrl_get_mRestrintStatePer_m3703655536 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mRestrintStatePer(System.Single)
extern "C"  void BuffCtrl_set_mRestrintStatePer_m3422517219 (BuffCtrl_t2836564350 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mAtkPerHp()
extern "C"  int32_t BuffCtrl_get_mAtkPerHp_m715722942 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mAtkPerHp(System.Int32)
extern "C"  void BuffCtrl_set_mAtkPerHp_m2952508265 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mAtkNumHp()
extern "C"  int32_t BuffCtrl_get_mAtkNumHp_m3689131687 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mAtkNumHp(System.Int32)
extern "C"  void BuffCtrl_set_mAtkNumHp_m3396582354 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mNDPAddBuff()
extern "C"  int32_t BuffCtrl_get_mNDPAddBuff_m3552074731 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mNDPAddBuff(System.Int32)
extern "C"  void BuffCtrl_set_mNDPAddBuff_m1532851350 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBeAttackedLaterSkillID()
extern "C"  int32_t BuffCtrl_get_mBeAttackedLaterSkillID_m1940393115 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBeAttackedLaterSkillID(System.Int32)
extern "C"  void BuffCtrl_set_mBeAttackedLaterSkillID_m3185878598 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBeAttackedLaterEffectID()
extern "C"  int32_t BuffCtrl_get_mBeAttackedLaterEffectID_m152373951 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBeAttackedLaterEffectID(System.Int32)
extern "C"  void BuffCtrl_set_mBeAttackedLaterEffectID_m2576891894 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mDeathLaterEffectID()
extern "C"  int32_t BuffCtrl_get_mDeathLaterEffectID_m182550197 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mDeathLaterEffectID(System.Int32)
extern "C"  void BuffCtrl_set_mDeathLaterEffectID_m1265778272 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mDeathLaterHeroID()
extern "C"  int32_t BuffCtrl_get_mDeathLaterHeroID_m3629394590 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mDeathLaterHeroID(System.Int32)
extern "C"  void BuffCtrl_set_mDeathLaterHeroID_m1220805449 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBeattackedLaterSpeed()
extern "C"  int32_t BuffCtrl_get_mBeattackedLaterSpeed_m1642102358 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBeattackedLaterSpeed(System.Int32)
extern "C"  void BuffCtrl_set_mBeattackedLaterSpeed_m1610401281 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mDeathLaterSpeed()
extern "C"  int32_t BuffCtrl_get_mDeathLaterSpeed_m1038135872 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mDeathLaterSpeed(System.Int32)
extern "C"  void BuffCtrl_set_mDeathLaterSpeed_m2802807799 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mAnitEndEffectID()
extern "C"  int32_t BuffCtrl_get_mAnitEndEffectID_m2956806848 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mAnitEndEffectID(System.Int32)
extern "C"  void BuffCtrl_set_mAnitEndEffectID_m2093698167 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mAuraAddHPNum()
extern "C"  int32_t BuffCtrl_get_mAuraAddHPNum_m2327618705 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mAuraAddHPNum(System.Int32)
extern "C"  void BuffCtrl_set_mAuraAddHPNum_m1386243516 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mAuraAddHPPer()
extern "C"  int32_t BuffCtrl_get_mAuraAddHPPer_m2328993896 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mAuraAddHPPer(System.Int32)
extern "C"  void BuffCtrl_set_mAuraAddHPPer_m2619299603 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mAuraReduceHPNum()
extern "C"  int32_t BuffCtrl_get_mAuraReduceHPNum_m2692133990 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mAuraReduceHPNum(System.Int32)
extern "C"  void BuffCtrl_set_mAuraReduceHPNum_m1176857885 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mAuraReduceHPPer()
extern "C"  int32_t BuffCtrl_get_mAuraReduceHPPer_m2693509181 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mAuraReduceHPPer(System.Int32)
extern "C"  void BuffCtrl_set_mAuraReduceHPPer_m2409913972 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mActiveSkillDamageAddPer()
extern "C"  int32_t BuffCtrl_get_mActiveSkillDamageAddPer_m2853225831 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mActiveSkillDamageAddPer(System.Int32)
extern "C"  void BuffCtrl_set_mActiveSkillDamageAddPer_m1388920478 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mAddAngerRatio()
extern "C"  int32_t BuffCtrl_get_mAddAngerRatio_m4264409558 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mAddAngerRatio(System.Int32)
extern "C"  void BuffCtrl_set_mAddAngerRatio_m3324750861 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::get_mFreeze()
extern "C"  bool BuffCtrl_get_mFreeze_m3959343650 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mFreeze(System.Boolean)
extern "C"  void BuffCtrl_set_mFreeze_m783193 (BuffCtrl_t2836564350 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::get_mMorph()
extern "C"  bool BuffCtrl_get_mMorph_m3495726719 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mMorph(System.Boolean)
extern "C"  void BuffCtrl_set_mMorph_m1088993910 (BuffCtrl_t2836564350 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::get_mAntiControl()
extern "C"  bool BuffCtrl_get_mAntiControl_m3295468498 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mAntiControl(System.Boolean)
extern "C"  void BuffCtrl_set_mAntiControl_m9317001 (BuffCtrl_t2836564350 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> BuffCtrl::get_mAddDamageSkillIDs()
extern "C"  List_1_t2522024052 * BuffCtrl_get_mAddDamageSkillIDs_m1424119420 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mAddDamageSkillIDs(System.Collections.Generic.List`1<System.Int32>)
extern "C"  void BuffCtrl_set_mAddDamageSkillIDs_m4029136243 (BuffCtrl_t2836564350 * __this, List_1_t2522024052 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> BuffCtrl::get_mAddDamageValue()
extern "C"  List_1_t2522024052 * BuffCtrl_get_mAddDamageValue_m2971692606 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mAddDamageValue(System.Collections.Generic.List`1<System.Int32>)
extern "C"  void BuffCtrl_set_mAddDamageValue_m296638293 (BuffCtrl_t2836564350 * __this, List_1_t2522024052 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mHpLineBuffID()
extern "C"  int32_t BuffCtrl_get_mHpLineBuffID_m3196784411 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mHpLineBuffID(System.Int32)
extern "C"  void BuffCtrl_set_mHpLineBuffID_m348213574 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mControlBuffTimeBonus()
extern "C"  int32_t BuffCtrl_get_mControlBuffTimeBonus_m522131475 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mControlBuffTimeBonus(System.Int32)
extern "C"  void BuffCtrl_set_mControlBuffTimeBonus_m2514070590 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mControlBuffTimeBonusPer()
extern "C"  int32_t BuffCtrl_get_mControlBuffTimeBonusPer_m2781032044 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mControlBuffTimeBonusPer(System.Int32)
extern "C"  void BuffCtrl_set_mControlBuffTimeBonusPer_m3115665443 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> BuffCtrl::get_mAddDamageValuePer()
extern "C"  List_1_t2522024052 * BuffCtrl_get_mAddDamageValuePer_m1867359329 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mAddDamageValuePer(System.Collections.Generic.List`1<System.Int32>)
extern "C"  void BuffCtrl_set_mAddDamageValuePer_m3813645550 (BuffCtrl_t2836564350 * __this, List_1_t2522024052 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mRuneTakeEffectBuffID()
extern "C"  int32_t BuffCtrl_get_mRuneTakeEffectBuffID_m1485711313 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mRuneTakeEffectBuffID(System.Int32)
extern "C"  void BuffCtrl_set_mRuneTakeEffectBuffID_m3546877692 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mRuneTakeEffectBuffID01()
extern "C"  int32_t BuffCtrl_get_mRuneTakeEffectBuffID01_m1839676818 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mRuneTakeEffectBuffID01(System.Int32)
extern "C"  void BuffCtrl_set_mRuneTakeEffectBuffID01_m1661410493 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mRuneTakeEffectBuffID02()
extern "C"  int32_t BuffCtrl_get_mRuneTakeEffectBuffID02_m1839677779 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mRuneTakeEffectBuffID02(System.Int32)
extern "C"  void BuffCtrl_set_mRuneTakeEffectBuffID02_m3168962302 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mStable()
extern "C"  int32_t BuffCtrl_get_mStable_m1573398828 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mStable(System.Int32)
extern "C"  void BuffCtrl_set_mStable_m723150167 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mNotInvade()
extern "C"  int32_t BuffCtrl_get_mNotInvade_m942523925 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mNotInvade(System.Int32)
extern "C"  void BuffCtrl_set_mNotInvade_m1076311244 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mUnyielding()
extern "C"  int32_t BuffCtrl_get_mUnyielding_m167773183 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mUnyielding(System.Int32)
extern "C"  void BuffCtrl_set_mUnyielding_m4255489706 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mDiscreteBuffIDTime()
extern "C"  int32_t BuffCtrl_get_mDiscreteBuffIDTime_m2532789285 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mDiscreteBuffIDTime(System.Int32)
extern "C"  void BuffCtrl_set_mDiscreteBuffIDTime_m2777637840 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mChangeNewSkillID()
extern "C"  int32_t BuffCtrl_get_mChangeNewSkillID_m1036957421 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mChangeNewSkillID(System.Int32)
extern "C"  void BuffCtrl_set_mChangeNewSkillID_m2302382360 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mChangeSkillPro()
extern "C"  int32_t BuffCtrl_get_mChangeSkillPro_m720810013 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mChangeSkillPro(System.Int32)
extern "C"  void BuffCtrl_set_mChangeSkillPro_m2498203848 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mReduceCDSkillID()
extern "C"  int32_t BuffCtrl_get_mReduceCDSkillID_m2938493686 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mReduceCDSkillID(System.Int32)
extern "C"  void BuffCtrl_set_mReduceCDSkillID_m1660894125 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mThumpAddValue()
extern "C"  int32_t BuffCtrl_get_mThumpAddValue_m3517240741 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mThumpAddValue(System.Int32)
extern "C"  void BuffCtrl_set_mThumpAddValue_m3435432284 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mShieldOverSkillID()
extern "C"  int32_t BuffCtrl_get_mShieldOverSkillID_m2818308128 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mShieldOverSkillID(System.Int32)
extern "C"  void BuffCtrl_set_mShieldOverSkillID_m1462200407 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mCleaveSkillID()
extern "C"  int32_t BuffCtrl_get_mCleaveSkillID_m4078563753 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mCleaveSkillID(System.Int32)
extern "C"  void BuffCtrl_set_mCleaveSkillID_m1971044192 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mCleaveBuffID()
extern "C"  int32_t BuffCtrl_get_mCleaveBuffID_m1453923891 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mCleaveBuffID(System.Int32)
extern "C"  void BuffCtrl_set_mCleaveBuffID_m2039053406 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mHpLineUpBuffID()
extern "C"  int32_t BuffCtrl_get_mHpLineUpBuffID_m895429590 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mHpLineUpBuffID(System.Int32)
extern "C"  void BuffCtrl_set_mHpLineUpBuffID_m2485075713 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffCritPer()
extern "C"  int32_t BuffCtrl_get_mBuffCritPer_m2786795617 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffCritPer(System.Int32)
extern "C"  void BuffCtrl_set_mBuffCritPer_m1564138392 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mBuffCritHurt()
extern "C"  int32_t BuffCtrl_get_mBuffCritHurt_m277134381 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mBuffCritHurt(System.Int32)
extern "C"  void BuffCtrl_set_mBuffCritHurt_m2312500056 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mAddHpNum()
extern "C"  int32_t BuffCtrl_get_mAddHpNum_m2028369582 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mAddHpNum(System.Int32)
extern "C"  void BuffCtrl_set_mAddHpNum_m3609178457 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mHTBuffID()
extern "C"  int32_t BuffCtrl_get_mHTBuffID_m1398748907 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mHTBuffID(System.Int32)
extern "C"  void BuffCtrl_set_mHTBuffID_m3618700822 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mDeathHarvestPer()
extern "C"  int32_t BuffCtrl_get_mDeathHarvestPer_m1796629465 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mDeathHarvestPer(System.Int32)
extern "C"  void BuffCtrl_set_mDeathHarvestPer_m3499264016 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mHpLineCleaveAddBuff()
extern "C"  int32_t BuffCtrl_get_mHpLineCleaveAddBuff_m2415438933 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mHpLineCleaveAddBuff(System.Int32)
extern "C"  void BuffCtrl_set_mHpLineCleaveAddBuff_m3460780940 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mResistanceCountryPer()
extern "C"  int32_t BuffCtrl_get_mResistanceCountryPer_m978734961 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mResistanceCountryPer(System.Int32)
extern "C"  void BuffCtrl_set_mResistanceCountryPer_m1360184988 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::get_mNDPAddBuff02()
extern "C"  int32_t BuffCtrl_get_mNDPAddBuff02_m3340031725 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::set_mNDPAddBuff02(System.Int32)
extern "C"  void BuffCtrl_set_mNDPAddBuff02_m424930840 (BuffCtrl_t2836564350 * __this, int32_t ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::CalcuBuffEffect()
extern "C"  void BuffCtrl_CalcuBuffEffect_m527812383 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::SyncData()
extern "C"  void BuffCtrl_SyncData_m1986272108 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::CalcuBuffFunEffect(System.Int32,System.Int32,System.Int32,System.Boolean,Buff)
extern "C"  void BuffCtrl_CalcuBuffFunEffect_m3157833611 (BuffCtrl_t2836564350 * __this, int32_t ___funID0, int32_t ___para1, int32_t ___showwordfortype2, bool ___isFirst3, Buff_t2081907 * ___buff4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::AddBuffState(Buff,BuffEnum.EBuffState)
extern "C"  void BuffCtrl_AddBuffState_m1687748490 (BuffCtrl_t2836564350 * __this, Buff_t2081907 * ___buff0, uint64_t ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<BuffState> BuffCtrl::GetBuffState(BuffEnum.EBuffState)
extern "C"  List_1_t3417094830 * BuffCtrl_GetBuffState_m3650735889 (BuffCtrl_t2836564350 * __this, uint64_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<BuffState> BuffCtrl::GetBuffStateByClone(BuffEnum.EBuffState,System.Int32)
extern "C"  List_1_t3417094830 * BuffCtrl_GetBuffStateByClone_m1109382162 (BuffCtrl_t2836564350 * __this, uint64_t ___state0, int32_t ___buffID1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<BuffState> BuffCtrl::GetBuffStateByClone(BuffEnum.EBuffState)
extern "C"  List_1_t3417094830 * BuffCtrl_GetBuffStateByClone_m3919783141 (BuffCtrl_t2836564350 * __this, uint64_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::RemoveBuffStateByID(System.Int32)
extern "C"  void BuffCtrl_RemoveBuffStateByID_m849735928 (BuffCtrl_t2836564350 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::RemoveBuffStateByState(BuffEnum.EBuffState)
extern "C"  void BuffCtrl_RemoveBuffStateByState_m505567256 (BuffCtrl_t2836564350 * __this, uint64_t ___state0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::RemoveBuffState(System.Int32)
extern "C"  void BuffCtrl_RemoveBuffState_m3348906662 (BuffCtrl_t2836564350 * __this, int32_t ___index0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ClearBuffState()
extern "C"  void BuffCtrl_ClearBuffState_m324535096 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::CheckTimeBomb(System.Int32)
extern "C"  void BuffCtrl_CheckTimeBomb_m2430651075 (BuffCtrl_t2836564350 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::TimeBombAction()
extern "C"  void BuffCtrl_TimeBombAction_m3695041580 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::CheckPlagueDust()
extern "C"  void BuffCtrl_CheckPlagueDust_m2399982247 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::PlagueDustAction(BuffState)
extern "C"  void BuffCtrl_PlagueDustAction_m502618515 (BuffCtrl_t2836564350 * __this, BuffState_t2048909278 * ___buffState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::CheckInfection(Buff)
extern "C"  void BuffCtrl_CheckInfection_m458647967 (BuffCtrl_t2836564350 * __this, Buff_t2081907 * ___buff0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::InfectionAction(Buff)
extern "C"  void BuffCtrl_InfectionAction_m476725649 (BuffCtrl_t2836564350 * __this, Buff_t2081907 * ___buff0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::CheckFierceWind(Skill)
extern "C"  void BuffCtrl_CheckFierceWind_m2120617572 (BuffCtrl_t2836564350 * __this, Skill_t79944241 * ___skill0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::FierceWindAction(BuffState)
extern "C"  void BuffCtrl_FierceWindAction_m1323723549 (BuffCtrl_t2836564350 * __this, BuffState_t2048909278 * ___buffState0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ReduceHpByBuff()
extern "C"  void BuffCtrl_ReduceHpByBuff_m1109766463 (BuffCtrl_t2836564350 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::DalayAddBuff(System.Boolean,CombatEntity,System.Int32)
extern "C"  void BuffCtrl_DalayAddBuff_m299059 (BuffCtrl_t2836564350 * __this, bool ___isHero0, CombatEntity_t684137495 * ___target1, int32_t ___buffID2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_RemoveTickBuff1(BuffCtrl,System.Single)
extern "C"  void BuffCtrl_ilo_RemoveTickBuff1_m1319383244 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, float ___time1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::ilo_get_godDownTweenTime2(CombatEntity)
extern "C"  float BuffCtrl_ilo_get_godDownTweenTime2_m2455610068 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::ilo_HasBuffState3(BuffCtrl,BuffEnum.EBuffState)
extern "C"  bool BuffCtrl_ilo_HasBuffState3_m4021686814 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, uint64_t ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_curAntiNum4(CombatEntity)
extern "C"  int32_t BuffCtrl_ilo_get_curAntiNum4_m3636705514 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::ilo_HasFun5(Buff,BuffEnum.EBuffFunType)
extern "C"  bool BuffCtrl_ilo_HasFun5_m3927760930 (Il2CppObject * __this /* static, unused */, Buff_t2081907 * ____this0, int32_t ___funType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_CheckTimeBomb6(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_CheckTimeBomb6_m4078328540 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_TimeBombAction7(BuffCtrl)
extern "C"  void BuffCtrl_ilo_TimeBombAction7_m3876771868 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_CalcuFinalProp8(CombatEntity)
extern "C"  void BuffCtrl_ilo_CalcuFinalProp8_m2133564656 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_SyncData9(BuffCtrl)
extern "C"  void BuffCtrl_ilo_SyncData9_m321063966 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_RecycleBuff10(BuffCtrl,System.Int32,System.Boolean)
extern "C"  void BuffCtrl_ilo_RecycleBuff10_m4266374869 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___index1, bool ___isRemoveEffect2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::ilo_IsControlBuff11(BuffCtrl,buffCfg)
extern "C"  bool BuffCtrl_ilo_IsControlBuff11_m2442626007 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, buffCfg_t227963665 * ___buffDB1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::ilo_IsControlFun12(BuffCtrl,System.Int32)
extern "C"  bool BuffCtrl_ilo_IsControlFun12_m3338010618 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] BuffCtrl::ilo_get_buffprob13(Skill)
extern "C"  Int32U5BU5D_t3230847821* BuffCtrl_ilo_get_buffprob13_m1687573352 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_buffHitPer14(Skill)
extern "C"  int32_t BuffCtrl_ilo_get_buffHitPer14_m3189709680 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightEnum.ESkillBuffEvent[] BuffCtrl::ilo_get_buffevent15(Skill)
extern "C"  ESkillBuffEventU5BU5D_t2768361140* BuffCtrl_ilo_get_buffevent15_m2018860586 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] BuffCtrl::ilo_get_buffid16(Skill)
extern "C"  Int32U5BU5D_t3230847821* BuffCtrl_ilo_get_buffid16_m2145997503 (Il2CppObject * __this /* static, unused */, Skill_t79944241 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_AddSkillBuff17(BuffCtrl,System.Int32,System.Int32,CombatEntity,Skill)
extern "C"  void BuffCtrl_ilo_AddSkillBuff17_m1268226725 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___id1, int32_t ___skillBuffHit2, CombatEntity_t684137495 * ___src3, Skill_t79944241 * ___skill4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// EntityEnum.UnitType BuffCtrl::ilo_get_unitType18(CombatEntity)
extern "C"  int32_t BuffCtrl_ilo_get_unitType18_m2681692412 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_GetHeroIntelligenceNum19(NpcMgr,System.Int32)
extern "C"  int32_t BuffCtrl_ilo_GetHeroIntelligenceNum19_m1391493735 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, int32_t ___intelligence1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_AddSkillBuff20(BuffCtrl,System.Int32,System.Int32,CombatEntity,Skill)
extern "C"  void BuffCtrl_ilo_AddSkillBuff20_m1756323725 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___id1, int32_t ___skillBuffHit2, CombatEntity_t684137495 * ___src3, Skill_t79944241 * ___skill4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// NpcMgr BuffCtrl::ilo_get_instance21()
extern "C"  NpcMgr_t2339534743 * BuffCtrl_ilo_get_instance21_m1083055613 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::ilo_IsAllElement22(NpcMgr)
extern "C"  bool BuffCtrl_ilo_IsAllElement22_m897109660 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::ilo_IsAllElement23(HeroMgr)
extern "C"  bool BuffCtrl_ilo_IsAllElement23_m3559570304 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HeroMgr BuffCtrl::ilo_get_instance24()
extern "C"  HeroMgr_t2475965342 * BuffCtrl_ilo_get_instance24_m2640195823 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_GetHeroIntelligenceNum25(HeroMgr,System.Int32)
extern "C"  int32_t BuffCtrl_ilo_GetHeroIntelligenceNum25_m2862670021 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, int32_t ___intelligence1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> BuffCtrl::ilo_GetAllHero26(HeroMgr)
extern "C"  List_1_t2052323047 * BuffCtrl_ilo_GetAllHero26_m3326799559 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> BuffCtrl::ilo_GetHeroNpcList27(NpcMgr)
extern "C"  List_1_t2052323047 * BuffCtrl_ilo_GetHeroNpcList27_m48987649 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BuffCtrl BuffCtrl::ilo_get_buffCtrl28(CombatEntity)
extern "C"  BuffCtrl_t2836564350 * BuffCtrl_ilo_get_buffCtrl28_m998704079 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> BuffCtrl::ilo_GetAllMonsters29(NpcMgr)
extern "C"  List_1_t2052323047 * BuffCtrl_ilo_GetAllMonsters29_m1876018328 (Il2CppObject * __this /* static, unused */, NpcMgr_t2339534743 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::ilo_get_atkPower30(CombatEntity)
extern "C"  float BuffCtrl_ilo_get_atkPower30_m673637154 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::ilo_IsThreeElement31(HeroMgr,HERO_ELEMENT)
extern "C"  bool BuffCtrl_ilo_IsThreeElement31_m225850319 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, int32_t ___element1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::ilo_IsThreeCountry32(HeroMgr,HERO_COUNTRY)
extern "C"  bool BuffCtrl_ilo_IsThreeCountry32_m2551525968 (Il2CppObject * __this /* static, unused */, HeroMgr_t2475965342 * ____this0, int32_t ___contyr1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> BuffCtrl::ilo_GetAllAlly33(GameMgr,CombatEntity)
extern "C"  List_1_t2052323047 * BuffCtrl_ilo_GetAllAlly33_m1997952906 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, CombatEntity_t684137495 * ___entity1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_PerHP34(CombatEntity)
extern "C"  int32_t BuffCtrl_ilo_get_PerHP34_m3157483520 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSDatacfgManager BuffCtrl::ilo_get_CfgDataMgr35()
extern "C"  CSDatacfgManager_t1565254243 * BuffCtrl_ilo_get_CfgDataMgr35_m4154756585 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_id36(CombatEntity)
extern "C"  int32_t BuffCtrl_ilo_get_id36_m945261368 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_awakenLv37(CombatEntity)
extern "C"  int32_t BuffCtrl_ilo_get_awakenLv37_m3287357265 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::ilo_get_controlBuffTimeBonusPer38(CombatEntity)
extern "C"  float BuffCtrl_ilo_get_controlBuffTimeBonusPer38_m732060444 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_Setup39(Buff,buffCfg,System.Int32,System.Single,System.Single)
extern "C"  void BuffCtrl_ilo_Setup39_m715654414 (Il2CppObject * __this /* static, unused */, Buff_t2081907 * ____this0, buffCfg_t227963665 * ___buffDB1, int32_t ___awakenLv2, float ___timeBonus3, float ___timeBonusPer4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_CachaBuffAction40(BuffCtrl,Buff,CombatEntity)
extern "C"  void BuffCtrl_ilo_CachaBuffAction40_m3953783593 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, Buff_t2081907 * ___buff1, CombatEntity_t684137495 * ___src2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_RealBuffAction41(BuffCtrl,Buff,System.Boolean)
extern "C"  void BuffCtrl_ilo_RealBuffAction41_m3386264330 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, Buff_t2081907 * ___buff1, bool ___isSkill2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_PlayHpTextEffect42(CombatEntity,System.String,HeadUpBloodMgr/LabelType,System.Int32)
extern "C"  void BuffCtrl_ilo_PlayHpTextEffect42_m4161599712 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, String_t* ___delta1, int32_t ___type2, int32_t ___index3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_CalcuBuffEffect43(BuffCtrl)
extern "C"  void BuffCtrl_ilo_CalcuBuffEffect43_m18839693 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::ilo_get_isInfection44(CombatEntity)
extern "C"  bool BuffCtrl_ilo_get_isInfection44_m1831303677 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_AddBuff45(BuffCtrl,System.Int32,CombatEntity)
extern "C"  void BuffCtrl_ilo_AddBuff45_m489657495 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___id1, CombatEntity_t684137495 * ___src2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// HERO_ELEMENT BuffCtrl::ilo_get_element46(CombatEntity)
extern "C"  int32_t BuffCtrl_ilo_get_element46_m1752632352 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::ilo_get_antiBuff47(CombatEntity)
extern "C"  bool BuffCtrl_ilo_get_antiBuff47_m1276678712 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_PlayHpTextEffect48(CombatEntity,System.Single,HeadUpBloodMgr/LabelType)
extern "C"  void BuffCtrl_ilo_PlayHpTextEffect48_m3783392904 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___delta1, int32_t ___type2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_antiDebuffDiscreteID49(CombatEntity)
extern "C"  int32_t BuffCtrl_ilo_get_antiDebuffDiscreteID49_m729903497 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_BreakSkill50(FightCtrl,System.Boolean)
extern "C"  void BuffCtrl_ilo_BreakSkill50_m792975533 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, bool ___isForce1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> BuffCtrl::ilo_get_mAddDamageValue51(BuffCtrl)
extern "C"  List_1_t2522024052 * BuffCtrl_ilo_get_mAddDamageValue51_m2933491529 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> BuffCtrl::ilo_get_mAddDamageSkillIDs52(BuffCtrl)
extern "C"  List_1_t2522024052 * BuffCtrl_ilo_get_mAddDamageSkillIDs52_m1704368846 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<System.Int32> BuffCtrl::ilo_get_mAddDamageValuePer53(BuffCtrl)
extern "C"  List_1_t2522024052 * BuffCtrl_ilo_get_mAddDamageValuePer53_m1165902132 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::ilo_IsHaveFunBuff54(BuffCtrl,BuffEnum.EBuffFunType)
extern "C"  bool BuffCtrl_ilo_IsHaveFunBuff54_m1239385318 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::ilo_get_mFreeze55(BuffCtrl)
extern "C"  bool BuffCtrl_ilo_get_mFreeze55_m2152109873 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::ilo_get_mMorph56(BuffCtrl)
extern "C"  bool BuffCtrl_ilo_get_mMorph56_m21049749 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mAuraAddHPRadius57(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mAuraAddHPRadius57_m522798545 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mBuffPDNum58(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mBuffPDNum58_m2693259593 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mBuffMDNum59(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mBuffMDNum59_m2592304459 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mBuffHitsNum60(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mBuffHitsNum60_m944286782 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mBuffWreckNum61(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mBuffWreckNum61_m1418592923 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mBuffCritNum62(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mBuffCritNum62_m1701714326 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mBuffWreck63(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mBuffWreck63_m1093007919 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mBuffCrit64(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mBuffCrit64_m2638698098 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mBuffTenacity65(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mBuffTenacity65_m2480600700 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mBuffRHurt66(BuffCtrl,System.UInt32)
extern "C"  void BuffCtrl_ilo_set_mBuffRHurt66_m3687967470 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mHPSecDecNumATK67(BuffCtrl,System.Single)
extern "C"  void BuffCtrl_ilo_set_mHPSecDecNumATK67_m2001665963 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mHPRstNum68(BuffCtrl,System.Single)
extern "C"  void BuffCtrl_ilo_set_mHPRstNum68_m3651267996 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mHPRstNumATK69(BuffCtrl,System.Single)
extern "C"  void BuffCtrl_ilo_set_mHPRstNumATK69_m3444110833 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mModelScale70(BuffCtrl,System.Single)
extern "C"  void BuffCtrl_ilo_set_mModelScale70_m283988953 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mTreatmentEffect71(BuffCtrl,System.Single)
extern "C"  void BuffCtrl_ilo_set_mTreatmentEffect71_m1067880532 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mHurtEffectReduction72(BuffCtrl,System.Single)
extern "C"  void BuffCtrl_ilo_set_mHurtEffectReduction72_m2541081599 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mVampire73(BuffCtrl,System.Single)
extern "C"  void BuffCtrl_ilo_set_mVampire73_m1671635707 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mTauntDis74(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mTauntDis74_m919145824 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mAngerSecDecNum75(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mAngerSecDecNum75_m1402410993 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mRestrintCountryType76(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mRestrintCountryType76_m4166681133 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mRestrintPer77(BuffCtrl,System.Single)
extern "C"  void BuffCtrl_ilo_set_mRestrintPer77_m2352740413 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mNDPAddBuff78(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mNDPAddBuff78_m1485910114 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mBeAttackedLaterEffectID79(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mBeAttackedLaterEffectID79_m4085312091 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mDeathLaterSpeed80(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mDeathLaterSpeed80_m1916870628 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mAuraAddHPNum81(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mAuraAddHPNum81_m605507492 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mAuraReduceHPPer82(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mAuraReduceHPPer82_m1075237637 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mMorph83(BuffCtrl,System.Boolean)
extern "C"  void BuffCtrl_ilo_set_mMorph83_m453143406 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mAntiControl84(BuffCtrl,System.Boolean)
extern "C"  void BuffCtrl_ilo_set_mAntiControl84_m2502663898 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mRuneTakeEffectBuffID0285(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mRuneTakeEffectBuffID0285_m2050750686 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mAntiDebuffDiscreteAddBuffID86(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mAntiDebuffDiscreteAddBuffID86_m546984045 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mWhenCritAddBuffID87(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mWhenCritAddBuffID87_m2180950929 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mThumpAddValue88(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mThumpAddValue88_m2837751703 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mAddHpNum89(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mAddHpNum89_m3541547103 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mGhostIndex90(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mGhostIndex90_m1612898755 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mHTBuffID91(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mHTBuffID91_m2370427275 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mHpLineCleaveAddBuff92(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mHpLineCleaveAddBuff92_m297856366 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_CalcuBuffFunEffect93(BuffCtrl,System.Int32,System.Int32,System.Int32,System.Boolean,Buff)
extern "C"  void BuffCtrl_ilo_CalcuBuffFunEffect93_m430351002 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___funID1, int32_t ___para2, int32_t ___showwordfortype3, bool ___isFirst4, Buff_t2081907 * ___buff5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_StateChange94(BuffCtrl,BuffEnum.EBuffState)
extern "C"  int32_t BuffCtrl_ilo_StateChange94_m829479825 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, uint64_t ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_Play95(AnimationRunner,AnimationRunner/AniType,System.Single,System.Boolean)
extern "C"  void BuffCtrl_ilo_Play95_m3735329555 (Il2CppObject * __this /* static, unused */, AnimationRunner_t1015409588 * ____this0, int32_t ___aniType1, float ___playTime2, bool ___isskill3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_PauseBvr96(Entity.Behavior.IBehaviorCtrl)
extern "C"  void BuffCtrl_ilo_PauseBvr96_m2016194174 (Il2CppObject * __this /* static, unused */, IBehaviorCtrl_t4225040900 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_SetNotSelected97(CombatEntity,System.Boolean)
extern "C"  void BuffCtrl_ilo_SetNotSelected97_m2051101936 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Blood BuffCtrl::ilo_get_blood98(CombatEntity)
extern "C"  Blood_t64280026 * BuffCtrl_ilo_get_blood98_m4096800744 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_SetViewModel99(CombatEntity,System.Boolean)
extern "C"  void BuffCtrl_ilo_SetViewModel99_m3391623864 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___isShow1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Entity.Behavior.IBehaviorCtrl BuffCtrl::ilo_get_bvrCtrl100(CombatEntity)
extern "C"  IBehaviorCtrl_t4225040900 * BuffCtrl_ilo_get_bvrCtrl100_m3393378546 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_CloseTaunt101(CombatEntity)
extern "C"  void BuffCtrl_ilo_CloseTaunt101_m3067247501 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_inUseAntiNum102(CombatEntity,System.Int32)
extern "C"  void BuffCtrl_ilo_set_inUseAntiNum102_m2713166290 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_curShieldNum103(CombatEntity,System.Int32)
extern "C"  void BuffCtrl_ilo_set_curShieldNum103_m3026390188 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_mAnitEndEffectID104(BuffCtrl)
extern "C"  int32_t BuffCtrl_ilo_get_mAnitEndEffectID104_m11111942 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_PlayEffect105(EffectMgr,System.Int32,CombatEntity)
extern "C"  void BuffCtrl_ilo_PlayEffect105_m974654026 (Il2CppObject * __this /* static, unused */, EffectMgr_t535289511 * ____this0, int32_t ___id1, CombatEntity_t684137495 * ___srcTf2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mAnitEndEffectID106(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mAnitEndEffectID106_m4034781365 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FightEnum.EFightRst BuffCtrl::ilo_SetNextSkillID107(FightCtrl,System.Int32,System.Boolean)
extern "C"  int32_t BuffCtrl_ilo_SetNextSkillID107_m3868294332 (Il2CppObject * __this /* static, unused */, FightCtrl_t648967803 * ____this0, int32_t ___skillid1, bool ___isClearCD2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_ExitPetrified108(CombatEntity)
extern "C"  void BuffCtrl_ilo_ExitPetrified108_m1445828418 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_EnterTankState109(CombatEntity)
extern "C"  void BuffCtrl_ilo_EnterTankState109_m2155464124 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mReduceRageHpNum110(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mReduceRageHpNum110_m55595416 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_mGhostIndex111(BuffCtrl)
extern "C"  int32_t BuffCtrl_ilo_get_mGhostIndex111_m1890911604 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_SetDistortion112(CombatEntity,System.Boolean,System.Single)
extern "C"  void BuffCtrl_ilo_SetDistortion112_m883722764 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, bool ___isShow1, float ___delayTime2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_ExitPartBloom113(CombatEntity)
extern "C"  void BuffCtrl_ilo_ExitPartBloom113_m786391108 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mAntiDebuffDiscreteID114(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mAntiDebuffDiscreteID114_m2236890145 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mChangeOldSkillID115(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mChangeOldSkillID115_m4167412939 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mThumpSkillID116(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mThumpSkillID116_m2665531197 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::ilo_get_srcAtkPower117(Buff)
extern "C"  float BuffCtrl_ilo_get_srcAtkPower117_m1987502850 (Il2CppObject * __this /* static, unused */, Buff_t2081907 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mDeathHarvestLine118(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mDeathHarvestLine118_m2916657532 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mResistanceCountryType119(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mResistanceCountryType119_m1994613851 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_AddBuffState120(BuffCtrl,Buff,BuffEnum.EBuffState)
extern "C"  void BuffCtrl_ilo_AddBuffState120_m1128919440 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, Buff_t2081907 * ___buff1, uint64_t ___state2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_mTauntDis121(BuffCtrl)
extern "C"  int32_t BuffCtrl_ilo_get_mTauntDis121_m334385142 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::ilo_get_maxHp122(CombatEntity)
extern "C"  float BuffCtrl_ilo_get_maxHp122_m321010717 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_mBuffMaxHPNum123(BuffCtrl)
extern "C"  int32_t BuffCtrl_ilo_get_mBuffMaxHPNum123_m3626282859 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mBuffMaxHPNum124(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mBuffMaxHPNum124_m431124869 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_mBuffWreckNum125(BuffCtrl)
extern "C"  int32_t BuffCtrl_ilo_get_mBuffWreckNum125_m3679382099 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_mBuffCritNum126(BuffCtrl)
extern "C"  int32_t BuffCtrl_ilo_get_mBuffCritNum126_m2930270396 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_mBuffTenacityNum127(BuffCtrl)
extern "C"  int32_t BuffCtrl_ilo_get_mBuffTenacityNum127_m582218482 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mBuffTenacityNum128(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mBuffTenacityNum128_m3454684266 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mBuffRHurtNum129(BuffCtrl,System.UInt32)
extern "C"  void BuffCtrl_ilo_set_mBuffRHurtNum129_m3326002112 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, uint32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_mBuffAT130(BuffCtrl)
extern "C"  int32_t BuffCtrl_ilo_get_mBuffAT130_m2403425166 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mBuffPD131(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mBuffPD131_m3402318915 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mBuffMD132(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mBuffMD132_m4099521797 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mBuffHits133(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mBuffHits133_m2092671469 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_mBuffWreck134(BuffCtrl)
extern "C"  int32_t BuffCtrl_ilo_get_mBuffWreck134_m1150601939 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_mBuffWithstand135(BuffCtrl)
extern "C"  int32_t BuffCtrl_ilo_get_mBuffWithstand135_m323054166 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mBuffWithstand136(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mBuffWithstand136_m3979368710 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_mBuffTenacity137(BuffCtrl)
extern "C"  int32_t BuffCtrl_ilo_get_mBuffTenacity137_m1463704857 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 BuffCtrl::ilo_get_mBuffRHurt138(BuffCtrl)
extern "C"  uint32_t BuffCtrl_ilo_get_mBuffRHurt138_m2089645563 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mHPSecDecPer139(BuffCtrl,System.Single)
extern "C"  void BuffCtrl_ilo_set_mHPSecDecPer139_m1841613650 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::ilo_get_mHPRstNum140(BuffCtrl)
extern "C"  float BuffCtrl_ilo_get_mHPRstNum140_m25888662 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::ilo_get_srcCureP141(Buff)
extern "C"  float BuffCtrl_ilo_get_srcCureP141_m3460996807 (Il2CppObject * __this /* static, unused */, Buff_t2081907 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::ilo_get_mModelScale142(BuffCtrl)
extern "C"  float BuffCtrl_ilo_get_mModelScale142_m3150403122 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::ilo_get_mHurtEffectPlus143(BuffCtrl)
extern "C"  float BuffCtrl_ilo_get_mHurtEffectPlus143_m1700676730 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mHurtEffectPlus144(BuffCtrl,System.Single)
extern "C"  void BuffCtrl_ilo_set_mHurtEffectPlus144_m2571919240 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::ilo_get_mHurtEffectReduction145(BuffCtrl)
extern "C"  float BuffCtrl_ilo_get_mHurtEffectReduction145_m1092277793 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::ilo_get_mHPRstNumATK146(BuffCtrl)
extern "C"  float BuffCtrl_ilo_get_mHPRstNumATK146_m1458949784 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single BuffCtrl::ilo_get_mRestrintStatePer147(BuffCtrl)
extern "C"  float BuffCtrl_ilo_get_mRestrintStatePer147_m3341198939 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mDeathLaterHeroID148(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mDeathLaterHeroID148_m1907099843 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_mAuraAddHPPer149(BuffCtrl)
extern "C"  int32_t BuffCtrl_ilo_get_mAuraAddHPPer149_m1469500773 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mAuraReduceHPNum150(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mAuraReduceHPNum150_m965107750 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_mAuraReduceHPPer151(BuffCtrl)
extern "C"  int32_t BuffCtrl_ilo_get_mAuraReduceHPPer151_m3009538817 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mAddAngerRatio152(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mAddAngerRatio152_m2610214292 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mHpLineBuffID153(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mHpLineBuffID153_m497615974 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mControlBuffTimeBonus154(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mControlBuffTimeBonus154_m3925015869 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_mControlBuffTimeBonusPer155(BuffCtrl)
extern "C"  int32_t BuffCtrl_ilo_get_mControlBuffTimeBonusPer155_m1826183030 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mControlBuffTimeBonusPer156(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mControlBuffTimeBonusPer156_m2246940774 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_AddRage157(CombatEntity,System.Single)
extern "C"  void BuffCtrl_ilo_AddRage157_m2635212461 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___delta1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mRuneTakeEffectBuffID01158(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mRuneTakeEffectBuffID01158_m3260357496 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mStable159(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mStable159_m3140802033 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_mNotInvade160(BuffCtrl)
extern "C"  int32_t BuffCtrl_ilo_get_mNotInvade160_m3715536519 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_discreteBuffID161(CombatEntity,System.Int32)
extern "C"  void BuffCtrl_ilo_set_discreteBuffID161_m3771317758 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mChangeNewSkillID162(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mChangeNewSkillID162_m3733035226 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mReduceCDSkillID163(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mReduceCDSkillID163_m3926118484 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_mReduceCDSkillID164(BuffCtrl)
extern "C"  int32_t BuffCtrl_ilo_get_mReduceCDSkillID164_m3681140810 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_curShieldNum165(CombatEntity)
extern "C"  int32_t BuffCtrl_ilo_get_curShieldNum165_m3129256429 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mCleaveBuffID166(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mCleaveBuffID166_m1829127452 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mHpLineUpBuffID167(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mHpLineUpBuffID167_m1618129630 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 BuffCtrl::ilo_get_mAddHpNum168(BuffCtrl)
extern "C"  int32_t BuffCtrl_ilo_get_mAddHpNum168_m2930407644 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mDeathHarvestPer169(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mDeathHarvestPer169_m2646210929 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_set_mNDPAddBuff02170(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_set_mNDPAddBuff02170_m1795744669 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_Setup171(BuffState,System.Int32,BuffEnum.EBuffState,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  void BuffCtrl_ilo_Setup171_m471617075 (Il2CppObject * __this /* static, unused */, BuffState_t2048909278 * ____this0, int32_t ____buffID1, uint64_t ____buffState2, int32_t ____para13, int32_t ____para24, int32_t ____para35, int32_t ____para46, int32_t ____srcID7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// BuffState BuffCtrl::ilo_Clone172(BuffState)
extern "C"  BuffState_t2048909278 * BuffCtrl_ilo_Clone172_m3440421078 (Il2CppObject * __this /* static, unused */, BuffState_t2048909278 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_RemoveBuffState173(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_RemoveBuffState173_m3494074120 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::ilo_get_isTimeBomb174(CombatEntity)
extern "C"  bool BuffCtrl_ilo_get_isTimeBomb174_m3089766601 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 BuffCtrl::ilo_get_position175(CombatEntity)
extern "C"  Vector3_t4282066566  BuffCtrl_ilo_get_position175_m1718389300 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_CircleHit176(UnityEngine.Vector3,System.Single,CombatEntity,System.Collections.Generic.List`1<CombatEntity>,System.Boolean)
extern "C"  void BuffCtrl_ilo_CircleHit176_m3943038018 (Il2CppObject * __this /* static, unused */, Vector3_t4282066566  ___centre0, float ___radius1, CombatEntity_t684137495 * ___entity2, List_1_t2052323047 * ___hitList3, bool ___iswish4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_PlagueDustAction177(BuffCtrl,BuffState)
extern "C"  void BuffCtrl_ilo_PlagueDustAction177_m2466626799 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, BuffState_t2048909278 * ___buffState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_AddBuff178(BuffCtrl,System.Int32)
extern "C"  void BuffCtrl_ilo_AddBuff178_m2273291421 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_AddBuff179(BuffCtrl,System.Int32,CombatEntity)
extern "C"  void BuffCtrl_ilo_AddBuff179_m280629715 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, int32_t ___id1, CombatEntity_t684137495 * ___src2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<BuffState> BuffCtrl::ilo_GetBuffState180(BuffCtrl,BuffEnum.EBuffState)
extern "C"  List_1_t3417094830 * BuffCtrl_ilo_GetBuffState180_m833898245 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, uint64_t ___state1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<CombatEntity> BuffCtrl::ilo_GetAllEnemy181(GameMgr,CombatEntity,System.Boolean)
extern "C"  List_1_t2052323047 * BuffCtrl_ilo_GetAllEnemy181_m2658969961 (Il2CppObject * __this /* static, unused */, GameMgr_t1469029542 * ____this0, CombatEntity_t684137495 * ___entity1, bool ___isshaix2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::ilo_get_isFierceWind182(CombatEntity)
extern "C"  bool BuffCtrl_ilo_get_isFierceWind182_m2432415835 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_FierceWindAction183(BuffCtrl,BuffState)
extern "C"  void BuffCtrl_ilo_FierceWindAction183_m2091430932 (Il2CppObject * __this /* static, unused */, BuffCtrl_t2836564350 * ____this0, BuffState_t2048909278 * ___buffState1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BuffCtrl::ilo_ReduceHP184(CombatEntity,System.Single,CombatEntity,FightEnum.EDamageType,System.Boolean,System.Boolean,System.Int32,System.Int32)
extern "C"  void BuffCtrl_ilo_ReduceHP184_m2581911765 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, float ___delta1, CombatEntity_t684137495 * ___src2, int32_t ___damageType3, bool ___isRepaly4, bool ___isActionSkill5, int32_t ___neardeathprotect6, int32_t ___buffId7, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean BuffCtrl::ilo_get_isDeath185(CombatEntity)
extern "C"  bool BuffCtrl_ilo_get_isDeath185_m454965348 (Il2CppObject * __this /* static, unused */, CombatEntity_t684137495 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
