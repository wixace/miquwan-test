﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.Creator`1<System.Object>
struct Creator_1_t376933243;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void Newtonsoft.Json.Utilities.Creator`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Creator_1__ctor_m3802881599_gshared (Creator_1_t376933243 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method);
#define Creator_1__ctor_m3802881599(__this, ___object0, ___method1, method) ((  void (*) (Creator_1_t376933243 *, Il2CppObject *, IntPtr_t, const MethodInfo*))Creator_1__ctor_m3802881599_gshared)(__this, ___object0, ___method1, method)
// T Newtonsoft.Json.Utilities.Creator`1<System.Object>::Invoke()
extern "C"  Il2CppObject * Creator_1_Invoke_m563291736_gshared (Creator_1_t376933243 * __this, const MethodInfo* method);
#define Creator_1_Invoke_m563291736(__this, method) ((  Il2CppObject * (*) (Creator_1_t376933243 *, const MethodInfo*))Creator_1_Invoke_m563291736_gshared)(__this, method)
// System.IAsyncResult Newtonsoft.Json.Utilities.Creator`1<System.Object>::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Creator_1_BeginInvoke_m3043852426_gshared (Creator_1_t376933243 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method);
#define Creator_1_BeginInvoke_m3043852426(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (Creator_1_t376933243 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))Creator_1_BeginInvoke_m3043852426_gshared)(__this, ___callback0, ___object1, method)
// T Newtonsoft.Json.Utilities.Creator`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Creator_1_EndInvoke_m2142452110_gshared (Creator_1_t376933243 * __this, Il2CppObject * ___result0, const MethodInfo* method);
#define Creator_1_EndInvoke_m2142452110(__this, ___result0, method) ((  Il2CppObject * (*) (Creator_1_t376933243 *, Il2CppObject *, const MethodInfo*))Creator_1_EndInvoke_m2142452110_gshared)(__this, ___result0, method)
