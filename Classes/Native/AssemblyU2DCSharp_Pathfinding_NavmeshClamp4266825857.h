﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.GraphNode
struct GraphNode_t23612370;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.NavmeshClamp
struct  NavmeshClamp_t4266825857  : public MonoBehaviour_t667441552
{
public:
	// Pathfinding.GraphNode Pathfinding.NavmeshClamp::prevNode
	GraphNode_t23612370 * ___prevNode_2;
	// UnityEngine.Vector3 Pathfinding.NavmeshClamp::prevPos
	Vector3_t4282066566  ___prevPos_3;

public:
	inline static int32_t get_offset_of_prevNode_2() { return static_cast<int32_t>(offsetof(NavmeshClamp_t4266825857, ___prevNode_2)); }
	inline GraphNode_t23612370 * get_prevNode_2() const { return ___prevNode_2; }
	inline GraphNode_t23612370 ** get_address_of_prevNode_2() { return &___prevNode_2; }
	inline void set_prevNode_2(GraphNode_t23612370 * value)
	{
		___prevNode_2 = value;
		Il2CppCodeGenWriteBarrier(&___prevNode_2, value);
	}

	inline static int32_t get_offset_of_prevPos_3() { return static_cast<int32_t>(offsetof(NavmeshClamp_t4266825857, ___prevPos_3)); }
	inline Vector3_t4282066566  get_prevPos_3() const { return ___prevPos_3; }
	inline Vector3_t4282066566 * get_address_of_prevPos_3() { return &___prevPos_3; }
	inline void set_prevPos_3(Vector3_t4282066566  value)
	{
		___prevPos_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
