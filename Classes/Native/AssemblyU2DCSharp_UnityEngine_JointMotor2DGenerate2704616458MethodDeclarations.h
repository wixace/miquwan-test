﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_JointMotor2DGenerated
struct UnityEngine_JointMotor2DGenerated_t2704616458;
// JSVCall
struct JSVCall_t3708497963;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"

// System.Void UnityEngine_JointMotor2DGenerated::.ctor()
extern "C"  void UnityEngine_JointMotor2DGenerated__ctor_m3082263713 (UnityEngine_JointMotor2DGenerated_t2704616458 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointMotor2DGenerated::.cctor()
extern "C"  void UnityEngine_JointMotor2DGenerated__cctor_m578798380 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_JointMotor2DGenerated::JointMotor2D_JointMotor2D1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_JointMotor2DGenerated_JointMotor2D_JointMotor2D1_m1631237201 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointMotor2DGenerated::JointMotor2D_motorSpeed(JSVCall)
extern "C"  void UnityEngine_JointMotor2DGenerated_JointMotor2D_motorSpeed_m858712148 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointMotor2DGenerated::JointMotor2D_maxMotorTorque(JSVCall)
extern "C"  void UnityEngine_JointMotor2DGenerated_JointMotor2D_maxMotorTorque_m2247308395 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointMotor2DGenerated::__Register()
extern "C"  void UnityEngine_JointMotor2DGenerated___Register_m789028038 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_JointMotor2DGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UnityEngine_JointMotor2DGenerated_ilo_getObject1_m2396552117 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_JointMotor2DGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UnityEngine_JointMotor2DGenerated_ilo_attachFinalizerObject2_m436777143 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_JointMotor2DGenerated::ilo_setSingle3(System.Int32,System.Single)
extern "C"  void UnityEngine_JointMotor2DGenerated_ilo_setSingle3_m1783380277 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_JointMotor2DGenerated::ilo_getSingle4(System.Int32)
extern "C"  float UnityEngine_JointMotor2DGenerated_ilo_getSingle4_m3205180369 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
