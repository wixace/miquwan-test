﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::.ctor()
#define List_1__ctor_m1985784908(__this, method) ((  void (*) (List_1_t2994434301 *, const MethodInfo*))List_1__ctor_m1239231859_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::.ctor(System.Collections.Generic.IEnumerable`1<T>)
#define List_1__ctor_m2589889162(__this, ___collection0, method) ((  void (*) (List_1_t2994434301 *, Il2CppObject*, const MethodInfo*))List_1__ctor_m1160795371_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::.ctor(System.Int32)
#define List_1__ctor_m3747898534(__this, ___capacity0, method) ((  void (*) (List_1_t2994434301 *, int32_t, const MethodInfo*))List_1__ctor_m3643386469_gshared)(__this, ___capacity0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::.cctor()
#define List_1__cctor_m975265656(__this /* static, unused */, method) ((  void (*) (Il2CppObject * /* static, unused */, const MethodInfo*))List_1__cctor_m3826137881_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m4265344415(__this, method) ((  Il2CppObject* (*) (List_1_t2994434301 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2808422246_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m3032011791(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2994434301 *, Il2CppArray *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m4034025648_gshared)(__this, ___array0, ___arrayIndex1, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m1166782174(__this, method) ((  Il2CppObject * (*) (List_1_t2994434301 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m1841330603_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m656017183(__this, ___item0, method) ((  int32_t (*) (List_1_t2994434301 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Add_m3794749222_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m865644673(__this, ___item0, method) ((  bool (*) (List_1_t2994434301 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Contains_m2659633254_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m498308599(__this, ___item0, method) ((  int32_t (*) (List_1_t2994434301 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m3431692926_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m1398776554(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2994434301 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Insert_m2067529129_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m1274253822(__this, ___item0, method) ((  void (*) (List_1_t2994434301 *, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_Remove_m1644145887_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2003219266(__this, method) ((  bool (*) (List_1_t2994434301 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m1299706087_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m1431784251(__this, method) ((  bool (*) (List_1_t2994434301 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m3867536694_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m2540041581(__this, method) ((  Il2CppObject * (*) (List_1_t2994434301 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m4244374434_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m1007359152(__this, method) ((  bool (*) (List_1_t2994434301 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m432946261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m3950187593(__this, method) ((  bool (*) (List_1_t2994434301 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m2961826820_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m2504146868(__this, ___index0, method) ((  Il2CppObject * (*) (List_1_t2994434301 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m3985478825_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m513457217(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2994434301 *, int32_t, Il2CppObject *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m3234554688_gshared)(__this, ___index0, ___value1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::Add(T)
#define List_1_Add_m1680145724(__this, ___item0, method) ((  void (*) (List_1_t2994434301 *, TriangleMeshNode_t1626248749 *, const MethodInfo*))List_1_Add_m933592675_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m1426477125(__this, ___newCount0, method) ((  void (*) (List_1_t2994434301 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m4122600870_gshared)(__this, ___newCount0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::CheckRange(System.Int32,System.Int32)
#define List_1_CheckRange_m3526583938(__this, ___idx0, ___count1, method) ((  void (*) (List_1_t2994434301 *, int32_t, int32_t, const MethodInfo*))List_1_CheckRange_m590371457_gshared)(__this, ___idx0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m1071403843(__this, ___collection0, method) ((  void (*) (List_1_t2994434301 *, Il2CppObject*, const MethodInfo*))List_1_AddCollection_m2478449828_gshared)(__this, ___collection0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m332376067(__this, ___enumerable0, method) ((  void (*) (List_1_t2994434301 *, Il2CppObject*, const MethodInfo*))List_1_AddEnumerable_m1739422052_gshared)(__this, ___enumerable0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m351473844(__this, ___collection0, method) ((  void (*) (List_1_t2994434301 *, Il2CppObject*, const MethodInfo*))List_1_AddRange_m2229151411_gshared)(__this, ___collection0, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::AsReadOnly()
#define List_1_AsReadOnly_m2439245009(__this, method) ((  ReadOnlyCollection_1_t3183326285 * (*) (List_1_t2994434301 *, const MethodInfo*))List_1_AsReadOnly_m769820182_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::BinarySearch(T)
#define List_1_BinarySearch_m327358434(__this, ___item0, method) ((  int32_t (*) (List_1_t2994434301 *, TriangleMeshNode_t1626248749 *, const MethodInfo*))List_1_BinarySearch_m2761349225_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::Clear()
#define List_1_Clear_m3686885495(__this, method) ((  void (*) (List_1_t2994434301 *, const MethodInfo*))List_1_Clear_m2940332446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::Contains(T)
#define List_1_Contains_m2978364402(__this, ___item0, method) ((  bool (*) (List_1_t2994434301 *, TriangleMeshNode_t1626248749 *, const MethodInfo*))List_1_Contains_m4186092781_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m2416837754(__this, ___array0, ___arrayIndex1, method) ((  void (*) (List_1_t2994434301 *, TriangleMeshNodeU5BU5D_t2064970368*, int32_t, const MethodInfo*))List_1_CopyTo_m3988356635_gshared)(__this, ___array0, ___arrayIndex1, method)
// T System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::Find(System.Predicate`1<T>)
#define List_1_Find_m588557260(__this, ___match0, method) ((  TriangleMeshNode_t1626248749 * (*) (List_1_t2994434301 *, Predicate_1_t1237305632 *, const MethodInfo*))List_1_Find_m3379773421_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m2662892457(__this /* static, unused */, ___match0, method) ((  void (*) (Il2CppObject * /* static, unused */, Predicate_1_t1237305632 *, const MethodInfo*))List_1_CheckMatch_m3390394152_gshared)(__this /* static, unused */, ___match0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m2096709254(__this, ___startIndex0, ___count1, ___match2, method) ((  int32_t (*) (List_1_t2994434301 *, int32_t, int32_t, Predicate_1_t1237305632 *, const MethodInfo*))List_1_GetIndex_m4275988045_gshared)(__this, ___startIndex0, ___count1, ___match2, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::GetEnumerator()
#define List_1_GetEnumerator_m3658986543(__this, method) ((  Enumerator_t3014107071  (*) (List_1_t2994434301 *, const MethodInfo*))List_1_GetEnumerator_m1919240000_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::IndexOf(T)
#define List_1_IndexOf_m2732081094(__this, ___item0, method) ((  int32_t (*) (List_1_t2994434301 *, TriangleMeshNode_t1626248749 *, const MethodInfo*))List_1_IndexOf_m2776798407_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m3367944401(__this, ___start0, ___delta1, method) ((  void (*) (List_1_t2994434301 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m3807054194_gshared)(__this, ___start0, ___delta1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m2163204938(__this, ___index0, method) ((  void (*) (List_1_t2994434301 *, int32_t, const MethodInfo*))List_1_CheckIndex_m3734723819_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::Insert(System.Int32,T)
#define List_1_Insert_m2479101365(__this, ___index0, ___item1, method) ((  void (*) (List_1_t2994434301 *, int32_t, TriangleMeshNode_t1626248749 *, const MethodInfo*))List_1_Insert_m3427163986_gshared)(__this, ___index0, ___item1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m3648577830(__this, ___collection0, method) ((  void (*) (List_1_t2994434301 *, Il2CppObject*, const MethodInfo*))List_1_CheckCollection_m2905071175_gshared)(__this, ___collection0, method)
// System.Boolean System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::Remove(T)
#define List_1_Remove_m3265089645(__this, ___item0, method) ((  bool (*) (List_1_t2994434301 *, TriangleMeshNode_t1626248749 *, const MethodInfo*))List_1_Remove_m2747911208_gshared)(__this, ___item0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m1158041353(__this, ___match0, method) ((  int32_t (*) (List_1_t2994434301 *, Predicate_1_t1237305632 *, const MethodInfo*))List_1_RemoveAll_m2933443938_gshared)(__this, ___match0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m968655671(__this, ___index0, method) ((  void (*) (List_1_t2994434301 *, int32_t, const MethodInfo*))List_1_RemoveAt_m2208877785_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::RemoveRange(System.Int32,System.Int32)
#define List_1_RemoveRange_m102808835(__this, ___index0, ___count1, method) ((  void (*) (List_1_t2994434301 *, int32_t, int32_t, const MethodInfo*))List_1_RemoveRange_m856857915_gshared)(__this, ___index0, ___count1, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::Reverse()
#define List_1_Reverse_m2266356181(__this, method) ((  void (*) (List_1_t2994434301 *, const MethodInfo*))List_1_Reverse_m449081940_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::Sort()
#define List_1_Sort_m1621540333(__this, method) ((  void (*) (List_1_t2994434301 *, const MethodInfo*))List_1_Sort_m1168641486_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::Sort(System.Collections.Generic.IComparer`1<T>)
#define List_1_Sort_m849958551(__this, ___comparer0, method) ((  void (*) (List_1_t2994434301 *, Il2CppObject*, const MethodInfo*))List_1_Sort_m3726677974_gshared)(__this, ___comparer0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m3082545792(__this, ___comparison0, method) ((  void (*) (List_1_t2994434301 *, Comparison_1_t342609936 *, const MethodInfo*))List_1_Sort_m4192185249_gshared)(__this, ___comparison0, method)
// T[] System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::ToArray()
#define List_1_ToArray_m1971877870(__this, method) ((  TriangleMeshNodeU5BU5D_t2064970368* (*) (List_1_t2994434301 *, const MethodInfo*))List_1_ToArray_m1449333879_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::TrimExcess()
#define List_1_TrimExcess_m2805528518(__this, method) ((  void (*) (List_1_t2994434301 *, const MethodInfo*))List_1_TrimExcess_m2451380967_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::get_Capacity()
#define List_1_get_Capacity_m3759334253(__this, method) ((  int32_t (*) (List_1_t2994434301 *, const MethodInfo*))List_1_get_Capacity_m543520655_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m3709488512(__this, ___value0, method) ((  void (*) (List_1_t2994434301 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1332789688_gshared)(__this, ___value0, method)
// System.Int32 System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::get_Count()
#define List_1_get_Count_m279083870(__this, method) ((  int32_t (*) (List_1_t2994434301 *, const MethodInfo*))List_1_get_Count_m3549598589_gshared)(__this, method)
// T System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::get_Item(System.Int32)
#define List_1_get_Item_m2392728759(__this, ___index0, method) ((  TriangleMeshNode_t1626248749 * (*) (List_1_t2994434301 *, int32_t, const MethodInfo*))List_1_get_Item_m404118264_gshared)(__this, ___index0, method)
// System.Void System.Collections.Generic.List`1<Pathfinding.TriangleMeshNode>::set_Item(System.Int32,T)
#define List_1_set_Item_m3314253374(__this, ___index0, ___value1, method) ((  void (*) (List_1_t2994434301 *, int32_t, TriangleMeshNode_t1626248749 *, const MethodInfo*))List_1_set_Item_m1074271145_gshared)(__this, ___index0, ___value1, method)
