﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SoundMgr
struct SoundMgr_t1807284905;
// CEvent.ZEvent
struct ZEvent_t3638018500;
// System.String
struct String_t;
// CEvent.EventFunc`1<CEvent.ZEvent>
struct EventFunc_1_t1114914310;
// Effect2DSound
struct Effect2DSound_t1148696332;
// SceneMgr
struct SceneMgr_t3584105996;
// checkpointCfg
struct checkpointCfg_t2816107964;
// ISound
struct ISound_t2170003014;
// SoundCfg
struct SoundCfg_t1807275253;
// CSDatacfgManager
struct CSDatacfgManager_t1565254243;
// BGSound
struct BGSound_t558445514;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_CEvent_ZEvent3638018500.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DCSharp_SoundTypeID3626616740.h"
#include "AssemblyU2DCSharp_Effect2DSound1148696332.h"
#include "AssemblyU2DCSharp_SceneMgr3584105996.h"
#include "AssemblyU2DCSharp_ISound2170003014.h"
#include "AssemblyU2DCSharp_SoundMgr1807284905.h"
#include "AssemblyU2DCSharp_CSDatacfgManager1565254243.h"
#include "AssemblyU2DCSharp_BGSound558445514.h"

// System.Void SoundMgr::.ctor()
extern "C"  void SoundMgr__ctor_m1220325906 (SoundMgr_t1807284905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::Init()
extern "C"  void SoundMgr_Init_m1376375298 (SoundMgr_t1807284905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::OnOpenFace(CEvent.ZEvent)
extern "C"  void SoundMgr_OnOpenFace_m4140333571 (SoundMgr_t1807284905 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::OnFightWin(CEvent.ZEvent)
extern "C"  void SoundMgr_OnFightWin_m1909431678 (SoundMgr_t1807284905 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::OnFightLose(CEvent.ZEvent)
extern "C"  void SoundMgr_OnFightLose_m1016650149 (SoundMgr_t1807284905 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::OnStartLoadScene(CEvent.ZEvent)
extern "C"  void SoundMgr_OnStartLoadScene_m2641812262 (SoundMgr_t1807284905 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::OnEnterScene(CEvent.ZEvent)
extern "C"  void SoundMgr_OnEnterScene_m3113522582 (SoundMgr_t1807284905 * __this, ZEvent_t3638018500 * ___ev0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::SetBlackPause()
extern "C"  void SoundMgr_SetBlackPause_m1387159849 (SoundMgr_t1807284905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::SetBlackCancelPause()
extern "C"  void SoundMgr_SetBlackCancelPause_m2903597007 (SoundMgr_t1807284905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::Pause(System.String)
extern "C"  void SoundMgr_Pause_m1911104796 (SoundMgr_t1807284905 * __this, String_t* ___typeKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::PlaySound(System.Int32)
extern "C"  void SoundMgr_PlaySound_m4112662524 (SoundMgr_t1807284905 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::RePlay(System.String)
extern "C"  void SoundMgr_RePlay_m957377705 (SoundMgr_t1807284905 * __this, String_t* ___typeKey0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::SetMute(System.Boolean,SoundTypeID)
extern "C"  void SoundMgr_SetMute_m3493966346 (SoundMgr_t1807284905 * __this, bool ___mute0, int32_t ___soundType1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::RestoreMute(System.Boolean,System.Boolean)
extern "C"  void SoundMgr_RestoreMute_m4000699535 (SoundMgr_t1807284905 * __this, bool ___mute0, bool ___isChat1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SoundTypeID SoundMgr::getTypeID(System.Int32)
extern "C"  int32_t SoundMgr_getTypeID_m1662727197 (SoundMgr_t1807284905 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::StopSound()
extern "C"  void SoundMgr_StopSound_m2922648221 (SoundMgr_t1807284905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::StopSound(SoundTypeID)
extern "C"  void SoundMgr_StopSound_m1350224401 (SoundMgr_t1807284905 * __this, int32_t ___soundType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::StopSound(System.Int32)
extern "C"  void SoundMgr_StopSound_m2475803758 (SoundMgr_t1807284905 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::StopSoundByID(System.Int32)
extern "C"  void SoundMgr_StopSoundByID_m552947392 (SoundMgr_t1807284905 * __this, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::StopLoopSound()
extern "C"  void SoundMgr_StopLoopSound_m3097949401 (SoundMgr_t1807284905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::set_mute(System.Boolean)
extern "C"  void SoundMgr_set_mute_m615268895 (SoundMgr_t1807284905 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::ZUpdate()
extern "C"  void SoundMgr_ZUpdate_m4070620307 (SoundMgr_t1807284905 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::ilo_AddEventListener1(System.String,CEvent.EventFunc`1<CEvent.ZEvent>)
extern "C"  void SoundMgr_ilo_AddEventListener1_m2728084642 (Il2CppObject * __this /* static, unused */, String_t* ___eventType0, EventFunc_1_t1114914310 * ___fun1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::ilo_StopHero2(Effect2DSound)
extern "C"  void SoundMgr_ilo_StopHero2_m1800803283 (Il2CppObject * __this /* static, unused */, Effect2DSound_t1148696332 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SoundMgr SoundMgr::ilo_get_SoundMgr3()
extern "C"  SoundMgr_t1807284905 * SoundMgr_ilo_get_SoundMgr3_m2318450762 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SceneMgr SoundMgr::ilo_get_SceneMgr4()
extern "C"  SceneMgr_t3584105996 * SoundMgr_ilo_get_SceneMgr4_m346887659 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// checkpointCfg SoundMgr::ilo_get_checkPoint5(SceneMgr)
extern "C"  checkpointCfg_t2816107964 * SoundMgr_ilo_get_checkPoint5_m4099141534 (Il2CppObject * __this /* static, unused */, SceneMgr_t3584105996 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::ilo_Pause6(ISound)
extern "C"  void SoundMgr_ilo_Pause6_m715844063 (Il2CppObject * __this /* static, unused */, ISound_t2170003014 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SoundTypeID SoundMgr::ilo_getTypeID7(SoundMgr,System.Int32)
extern "C"  int32_t SoundMgr_ilo_getTypeID7_m1076023296 (Il2CppObject * __this /* static, unused */, SoundMgr_t1807284905 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// SoundCfg SoundMgr::ilo_GetSoundCfg8(CSDatacfgManager,System.Int32)
extern "C"  SoundCfg_t1807275253 * SoundMgr_ilo_GetSoundCfg8_m879909728 (Il2CppObject * __this /* static, unused */, CSDatacfgManager_t1565254243 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::ilo_RePlay9(ISound)
extern "C"  void SoundMgr_ilo_RePlay9_m2250770857 (Il2CppObject * __this /* static, unused */, ISound_t2170003014 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::ilo_set_mute10(Effect2DSound,System.Boolean)
extern "C"  void SoundMgr_ilo_set_mute10_m3845160977 (Il2CppObject * __this /* static, unused */, Effect2DSound_t1148696332 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::ilo_set_mute11(BGSound,System.Boolean)
extern "C"  void SoundMgr_ilo_set_mute11_m348208784 (Il2CppObject * __this /* static, unused */, BGSound_t558445514 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::ilo_StopByID12(ISound,System.Int32)
extern "C"  void SoundMgr_ilo_StopByID12_m1880478595 (Il2CppObject * __this /* static, unused */, ISound_t2170003014 * ____this0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSDatacfgManager SoundMgr::ilo_get_CfgDataMgr13()
extern "C"  CSDatacfgManager_t1565254243 * SoundMgr_ilo_get_CfgDataMgr13_m778304980 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::ilo_set_mute14(ISound,System.Boolean)
extern "C"  void SoundMgr_ilo_set_mute14_m979648069 (Il2CppObject * __this /* static, unused */, ISound_t2170003014 * ____this0, bool ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SoundMgr::ilo_Update15(ISound)
extern "C"  void SoundMgr_ilo_Update15_m1443904396 (Il2CppObject * __this /* static, unused */, ISound_t2170003014 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
