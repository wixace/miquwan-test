﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Pathfinding.NavmeshCut
struct NavmeshCut_t300992648;
// System.Action`1<Pathfinding.NavmeshCut>
struct Action_1_t696808784;
// System.Collections.Generic.List`1<Pathfinding.NavmeshCut>
struct List_1_t1669178200;
// System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>
struct List_1_t1767529987;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Pathfinding_NavmeshCut300992648.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "Pathfinding_ClipperLib_Pathfinding_ClipperLib_IntP3326126179.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void Pathfinding.NavmeshCut::.ctor()
extern "C"  void NavmeshCut__ctor_m2815491295 (NavmeshCut_t300992648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshCut::.cctor()
extern "C"  void NavmeshCut__cctor_m898788014 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshCut::add_OnDestroyCallback(System.Action`1<Pathfinding.NavmeshCut>)
extern "C"  void NavmeshCut_add_OnDestroyCallback_m374962439 (Il2CppObject * __this /* static, unused */, Action_1_t696808784 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshCut::remove_OnDestroyCallback(System.Action`1<Pathfinding.NavmeshCut>)
extern "C"  void NavmeshCut_remove_OnDestroyCallback_m1558457852 (Il2CppObject * __this /* static, unused */, Action_1_t696808784 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshCut::AddCut(Pathfinding.NavmeshCut)
extern "C"  void NavmeshCut_AddCut_m3957642666 (Il2CppObject * __this /* static, unused */, NavmeshCut_t300992648 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshCut::RemoveCut(Pathfinding.NavmeshCut)
extern "C"  void NavmeshCut_RemoveCut_m515281503 (Il2CppObject * __this /* static, unused */, NavmeshCut_t300992648 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Pathfinding.NavmeshCut> Pathfinding.NavmeshCut::GetAllInRange(UnityEngine.Bounds)
extern "C"  List_1_t1669178200 * NavmeshCut_GetAllInRange_m2368540687 (Il2CppObject * __this /* static, unused */, Bounds_t2711641849  ___b0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavmeshCut::Intersects(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool NavmeshCut_Intersects_m1786310201 (Il2CppObject * __this /* static, unused */, Bounds_t2711641849  ___b10, Bounds_t2711641849  ___b21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.List`1<Pathfinding.NavmeshCut> Pathfinding.NavmeshCut::GetAll()
extern "C"  List_1_t1669178200 * NavmeshCut_GetAll_m3866955647 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds Pathfinding.NavmeshCut::get_LastBounds()
extern "C"  Bounds_t2711641849  NavmeshCut_get_LastBounds_m2821679178 (NavmeshCut_t300992648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshCut::Awake()
extern "C"  void NavmeshCut_Awake_m3053096514 (NavmeshCut_t300992648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshCut::OnEnable()
extern "C"  void NavmeshCut_OnEnable_m1956424263 (NavmeshCut_t300992648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshCut::OnDestroy()
extern "C"  void NavmeshCut_OnDestroy_m3138079320 (NavmeshCut_t300992648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshCut::ForceUpdate()
extern "C"  void NavmeshCut_ForceUpdate_m1966781329 (NavmeshCut_t300992648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavmeshCut::RequiresUpdate()
extern "C"  bool NavmeshCut_RequiresUpdate_m2583812752 (NavmeshCut_t300992648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshCut::UsedForCut()
extern "C"  void NavmeshCut_UsedForCut_m1488042715 (NavmeshCut_t300992648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshCut::NotifyUpdated()
extern "C"  void NavmeshCut_NotifyUpdated_m2502749871 (NavmeshCut_t300992648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshCut::CalculateMeshContour()
extern "C"  void NavmeshCut_CalculateMeshContour_m249324460 (NavmeshCut_t300992648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds Pathfinding.NavmeshCut::GetBounds()
extern "C"  Bounds_t2711641849  NavmeshCut_GetBounds_m4117458935 (NavmeshCut_t300992648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshCut::GetContour(System.Collections.Generic.List`1<System.Collections.Generic.List`1<Pathfinding.ClipperLib.IntPoint>>)
extern "C"  void NavmeshCut_GetContour_m201370872 (NavmeshCut_t300992648 * __this, List_1_t1767529987 * ___buffer0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ClipperLib.IntPoint Pathfinding.NavmeshCut::V3ToIntPoint(UnityEngine.Vector3)
extern "C"  IntPoint_t3326126179  NavmeshCut_V3ToIntPoint_m3300228181 (NavmeshCut_t300992648 * __this, Vector3_t4282066566  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.NavmeshCut::IntPointToV3(Pathfinding.ClipperLib.IntPoint)
extern "C"  Vector3_t4282066566  NavmeshCut_IntPointToV3_m2510056365 (NavmeshCut_t300992648 * __this, IntPoint_t3326126179  ___p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshCut::OnDrawGizmos()
extern "C"  void NavmeshCut_OnDrawGizmos_m4220234593 (NavmeshCut_t300992648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshCut::OnDrawGizmosSelected()
extern "C"  void NavmeshCut_OnDrawGizmosSelected_m1314185052 (NavmeshCut_t300992648 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Pathfinding.NavmeshCut::ilo_Intersects1(UnityEngine.Bounds,UnityEngine.Bounds)
extern "C"  bool NavmeshCut_ilo_Intersects1_m2868687539 (Il2CppObject * __this /* static, unused */, Bounds_t2711641849  ___b10, Bounds_t2711641849  ___b21, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshCut::ilo_AddCut2(Pathfinding.NavmeshCut)
extern "C"  void NavmeshCut_ilo_AddCut2_m4196265247 (Il2CppObject * __this /* static, unused */, NavmeshCut_t300992648 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Pathfinding.ClipperLib.IntPoint Pathfinding.NavmeshCut::ilo_V3ToIntPoint3(Pathfinding.NavmeshCut,UnityEngine.Vector3)
extern "C"  IntPoint_t3326126179  NavmeshCut_ilo_V3ToIntPoint3_m2940180053 (Il2CppObject * __this /* static, unused */, NavmeshCut_t300992648 * ____this0, Vector3_t4282066566  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Pathfinding.NavmeshCut::ilo_CalculateMeshContour4(Pathfinding.NavmeshCut)
extern "C"  void NavmeshCut_ilo_CalculateMeshContour4_m766033691 (Il2CppObject * __this /* static, unused */, NavmeshCut_t300992648 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds Pathfinding.NavmeshCut::ilo_GetBounds5(Pathfinding.NavmeshCut)
extern "C"  Bounds_t2711641849  NavmeshCut_ilo_GetBounds5_m3575873463 (Il2CppObject * __this /* static, unused */, NavmeshCut_t300992648 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Pathfinding.NavmeshCut::ilo_IntPointToV36(Pathfinding.NavmeshCut,Pathfinding.ClipperLib.IntPoint)
extern "C"  Vector3_t4282066566  NavmeshCut_ilo_IntPointToV36_m2795013808 (Il2CppObject * __this /* static, unused */, NavmeshCut_t300992648 * ____this0, IntPoint_t3326126179  ___p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
