﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t2570496278;
// System.Type
struct Type_t;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_t2701878760;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_Reflection_FieldInfo3973053266.h"
#include "mscorlib_System_String7231557.h"

// System.Collections.Generic.IList`1<System.Object> Newtonsoft.Json.Utilities.EnumUtils::GetValues(System.Type)
extern "C"  Il2CppObject* EnumUtils_GetValues_m3157139055 (Il2CppObject * __this /* static, unused */, Type_t * ___enumType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<System.String> Newtonsoft.Json.Utilities.EnumUtils::GetNames(System.Type)
extern "C"  Il2CppObject* EnumUtils_GetNames_m3460881353 (Il2CppObject * __this /* static, unused */, Type_t * ___enumType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.EnumUtils::<GetValues>m__3A4(System.Reflection.FieldInfo)
extern "C"  bool EnumUtils_U3CGetValuesU3Em__3A4_m496720877 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___field0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Newtonsoft.Json.Utilities.EnumUtils::<GetNames>m__3A5(System.Reflection.FieldInfo)
extern "C"  bool EnumUtils_U3CGetNamesU3Em__3A5_m1086900392 (Il2CppObject * __this /* static, unused */, FieldInfo_t * ___field0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Newtonsoft.Json.Utilities.EnumUtils::ilo_ArgumentTypeIsEnum1(System.Type,System.String)
extern "C"  void EnumUtils_ilo_ArgumentTypeIsEnum1_m2221076839 (Il2CppObject * __this /* static, unused */, Type_t * ___enumType0, String_t* ___parameterName1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<System.Object> Newtonsoft.Json.Utilities.EnumUtils::ilo_GetValues2(System.Type)
extern "C"  Il2CppObject* EnumUtils_ilo_GetValues2_m1180491036 (Il2CppObject * __this /* static, unused */, Type_t * ___enumType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IList`1<System.String> Newtonsoft.Json.Utilities.EnumUtils::ilo_GetNames3(System.Type)
extern "C"  Il2CppObject* EnumUtils_ilo_GetNames3_m2133781543 (Il2CppObject * __this /* static, unused */, Type_t * ___enumType0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
