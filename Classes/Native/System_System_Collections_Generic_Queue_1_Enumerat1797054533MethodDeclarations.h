﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Queue`1<AstarPath/AstarWorkItem>
struct Queue_1_t507969021;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Queue_1_Enumerat1797054533.h"
#include "AssemblyU2DCSharp_AstarPath_AstarWorkItem2566693888.h"

// System.Void System.Collections.Generic.Queue`1/Enumerator<AstarPath/AstarWorkItem>::.ctor(System.Collections.Generic.Queue`1<T>)
extern "C"  void Enumerator__ctor_m2611456733_gshared (Enumerator_t1797054533 * __this, Queue_1_t507969021 * ___q0, const MethodInfo* method);
#define Enumerator__ctor_m2611456733(__this, ___q0, method) ((  void (*) (Enumerator_t1797054533 *, Queue_1_t507969021 *, const MethodInfo*))Enumerator__ctor_m2611456733_gshared)(__this, ___q0, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<AstarPath/AstarWorkItem>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4264009304_gshared (Enumerator_t1797054533 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m4264009304(__this, method) ((  void (*) (Enumerator_t1797054533 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m4264009304_gshared)(__this, method)
// System.Object System.Collections.Generic.Queue`1/Enumerator<AstarPath/AstarWorkItem>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3073639630_gshared (Enumerator_t1797054533 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3073639630(__this, method) ((  Il2CppObject * (*) (Enumerator_t1797054533 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3073639630_gshared)(__this, method)
// System.Void System.Collections.Generic.Queue`1/Enumerator<AstarPath/AstarWorkItem>::Dispose()
extern "C"  void Enumerator_Dispose_m1757537823_gshared (Enumerator_t1797054533 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m1757537823(__this, method) ((  void (*) (Enumerator_t1797054533 *, const MethodInfo*))Enumerator_Dispose_m1757537823_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Queue`1/Enumerator<AstarPath/AstarWorkItem>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3317200540_gshared (Enumerator_t1797054533 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m3317200540(__this, method) ((  bool (*) (Enumerator_t1797054533 *, const MethodInfo*))Enumerator_MoveNext_m3317200540_gshared)(__this, method)
// T System.Collections.Generic.Queue`1/Enumerator<AstarPath/AstarWorkItem>::get_Current()
extern "C"  AstarWorkItem_t2566693888  Enumerator_get_Current_m2090605809_gshared (Enumerator_t1797054533 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m2090605809(__this, method) ((  AstarWorkItem_t2566693888  (*) (Enumerator_t1797054533 *, const MethodInfo*))Enumerator_get_Current_m2090605809_gshared)(__this, method)
