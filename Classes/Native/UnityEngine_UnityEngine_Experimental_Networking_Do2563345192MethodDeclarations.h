﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Experimental.Networking.DownloadHandler
struct DownloadHandler_t2563345192;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
struct DownloadHandler_t2563345192_marshaled_pinvoke;
struct DownloadHandler_t2563345192_marshaled_com;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Experimental.Networking.DownloadHandler::.ctor()
extern "C"  void DownloadHandler__ctor_m4250862251 (DownloadHandler_t2563345192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.DownloadHandler::InternalCreateString()
extern "C"  void DownloadHandler_InternalCreateString_m4028573251 (DownloadHandler_t2563345192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.DownloadHandler::InternalDestroy()
extern "C"  void DownloadHandler_InternalDestroy_m1522578694 (DownloadHandler_t2563345192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.DownloadHandler::Finalize()
extern "C"  void DownloadHandler_Finalize_m429103575 (DownloadHandler_t2563345192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Experimental.Networking.DownloadHandler::Dispose()
extern "C"  void DownloadHandler_Dispose_m463007784 (DownloadHandler_t2563345192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Experimental.Networking.DownloadHandler::get_text()
extern "C"  String_t* DownloadHandler_get_text_m1850586956 (DownloadHandler_t2563345192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] UnityEngine.Experimental.Networking.DownloadHandler::GetData()
extern "C"  ByteU5BU5D_t4260760469* DownloadHandler_GetData_m3879174547 (DownloadHandler_t2563345192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Experimental.Networking.DownloadHandler::GetText()
extern "C"  String_t* DownloadHandler_GetText_m2705281999 (DownloadHandler_t2563345192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct DownloadHandler_t2563345192;
struct DownloadHandler_t2563345192_marshaled_pinvoke;

extern "C" void DownloadHandler_t2563345192_marshal_pinvoke(const DownloadHandler_t2563345192& unmarshaled, DownloadHandler_t2563345192_marshaled_pinvoke& marshaled);
extern "C" void DownloadHandler_t2563345192_marshal_pinvoke_back(const DownloadHandler_t2563345192_marshaled_pinvoke& marshaled, DownloadHandler_t2563345192& unmarshaled);
extern "C" void DownloadHandler_t2563345192_marshal_pinvoke_cleanup(DownloadHandler_t2563345192_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct DownloadHandler_t2563345192;
struct DownloadHandler_t2563345192_marshaled_com;

extern "C" void DownloadHandler_t2563345192_marshal_com(const DownloadHandler_t2563345192& unmarshaled, DownloadHandler_t2563345192_marshaled_com& marshaled);
extern "C" void DownloadHandler_t2563345192_marshal_com_back(const DownloadHandler_t2563345192_marshaled_com& marshaled, DownloadHandler_t2563345192& unmarshaled);
extern "C" void DownloadHandler_t2563345192_marshal_com_cleanup(DownloadHandler_t2563345192_marshaled_com& marshaled);
