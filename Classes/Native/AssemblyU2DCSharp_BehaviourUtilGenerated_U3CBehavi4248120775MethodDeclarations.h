﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BehaviourUtilGenerated/<BehaviourUtil_DelayCallT4__Single__ActionT4_T1_T2_T3_T4__T1__T2__T3__T4>c__AnonStorey44
struct U3CBehaviourUtil_DelayCallT4__Single__ActionT4_T1_T2_T3_T4__T1__T2__T3__T4U3Ec__AnonStorey44_t4248120775;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void BehaviourUtilGenerated/<BehaviourUtil_DelayCallT4__Single__ActionT4_T1_T2_T3_T4__T1__T2__T3__T4>c__AnonStorey44::.ctor()
extern "C"  void U3CBehaviourUtil_DelayCallT4__Single__ActionT4_T1_T2_T3_T4__T1__T2__T3__T4U3Ec__AnonStorey44__ctor_m2938849668 (U3CBehaviourUtil_DelayCallT4__Single__ActionT4_T1_T2_T3_T4__T1__T2__T3__T4U3Ec__AnonStorey44_t4248120775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object BehaviourUtilGenerated/<BehaviourUtil_DelayCallT4__Single__ActionT4_T1_T2_T3_T4__T1__T2__T3__T4>c__AnonStorey44::<>m__3()
extern "C"  Il2CppObject * U3CBehaviourUtil_DelayCallT4__Single__ActionT4_T1_T2_T3_T4__T1__T2__T3__T4U3Ec__AnonStorey44_U3CU3Em__3_m3355251091 (U3CBehaviourUtil_DelayCallT4__Single__ActionT4_T1_T2_T3_T4__T1__T2__T3__T4U3Ec__AnonStorey44_t4248120775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
