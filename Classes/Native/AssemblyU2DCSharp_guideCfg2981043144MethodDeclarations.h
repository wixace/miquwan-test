﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// guideCfg
struct guideCfg_t2981043144;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t827649927;

#include "codegen/il2cpp-codegen.h"

// System.Void guideCfg::.ctor()
extern "C"  void guideCfg__ctor_m1677747155 (guideCfg_t2981043144 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void guideCfg::Init(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern "C"  void guideCfg_Init_m1128048338 (guideCfg_t2981043144 * __this, Dictionary_2_t827649927 * ____info0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
