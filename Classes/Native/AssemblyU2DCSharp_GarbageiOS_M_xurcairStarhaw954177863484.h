﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_xurcairStarhaw95
struct  M_xurcairStarhaw95_t4177863484  : public Il2CppObject
{
public:
	// System.Single GarbageiOS.M_xurcairStarhaw95::_malldraw
	float ____malldraw_0;
	// System.String GarbageiOS.M_xurcairStarhaw95::_treredi
	String_t* ____treredi_1;
	// System.Single GarbageiOS.M_xurcairStarhaw95::_zemra
	float ____zemra_2;
	// System.String GarbageiOS.M_xurcairStarhaw95::_tassto
	String_t* ____tassto_3;
	// System.Single GarbageiOS.M_xurcairStarhaw95::_wersto
	float ____wersto_4;
	// System.UInt32 GarbageiOS.M_xurcairStarhaw95::_cooqadalJaljearrou
	uint32_t ____cooqadalJaljearrou_5;
	// System.String GarbageiOS.M_xurcairStarhaw95::_cirraro
	String_t* ____cirraro_6;
	// System.UInt32 GarbageiOS.M_xurcairStarhaw95::_waisunuBeebu
	uint32_t ____waisunuBeebu_7;
	// System.Int32 GarbageiOS.M_xurcairStarhaw95::_kelbo
	int32_t ____kelbo_8;
	// System.Boolean GarbageiOS.M_xurcairStarhaw95::_yalejawNisje
	bool ____yalejawNisje_9;
	// System.UInt32 GarbageiOS.M_xurcairStarhaw95::_gairboudraXemras
	uint32_t ____gairboudraXemras_10;
	// System.Int32 GarbageiOS.M_xurcairStarhaw95::_melwhairciPoorar
	int32_t ____melwhairciPoorar_11;
	// System.Boolean GarbageiOS.M_xurcairStarhaw95::_cepoubeRoufou
	bool ____cepoubeRoufou_12;
	// System.Int32 GarbageiOS.M_xurcairStarhaw95::_korxekearLairbea
	int32_t ____korxekearLairbea_13;
	// System.UInt32 GarbageiOS.M_xurcairStarhaw95::_mupilePeetrear
	uint32_t ____mupilePeetrear_14;
	// System.Boolean GarbageiOS.M_xurcairStarhaw95::_yajeeMarneacir
	bool ____yajeeMarneacir_15;
	// System.Boolean GarbageiOS.M_xurcairStarhaw95::_jabu
	bool ____jabu_16;
	// System.String GarbageiOS.M_xurcairStarhaw95::_larbege
	String_t* ____larbege_17;
	// System.Single GarbageiOS.M_xurcairStarhaw95::_stuqai
	float ____stuqai_18;
	// System.UInt32 GarbageiOS.M_xurcairStarhaw95::_veheSadachere
	uint32_t ____veheSadachere_19;
	// System.String GarbageiOS.M_xurcairStarhaw95::_teararwha
	String_t* ____teararwha_20;
	// System.Boolean GarbageiOS.M_xurcairStarhaw95::_nepur
	bool ____nepur_21;
	// System.Single GarbageiOS.M_xurcairStarhaw95::_surhinal
	float ____surhinal_22;
	// System.UInt32 GarbageiOS.M_xurcairStarhaw95::_nidoHawrair
	uint32_t ____nidoHawrair_23;
	// System.Boolean GarbageiOS.M_xurcairStarhaw95::_hitreQavu
	bool ____hitreQavu_24;
	// System.String GarbageiOS.M_xurcairStarhaw95::_goudair
	String_t* ____goudair_25;
	// System.Single GarbageiOS.M_xurcairStarhaw95::_pirsiRedir
	float ____pirsiRedir_26;
	// System.Single GarbageiOS.M_xurcairStarhaw95::_bomayjer
	float ____bomayjer_27;
	// System.String GarbageiOS.M_xurcairStarhaw95::_vowtrou
	String_t* ____vowtrou_28;
	// System.UInt32 GarbageiOS.M_xurcairStarhaw95::_wheemoojoo
	uint32_t ____wheemoojoo_29;
	// System.UInt32 GarbageiOS.M_xurcairStarhaw95::_dallkaw
	uint32_t ____dallkaw_30;
	// System.UInt32 GarbageiOS.M_xurcairStarhaw95::_yekeSepelee
	uint32_t ____yekeSepelee_31;
	// System.Boolean GarbageiOS.M_xurcairStarhaw95::_coutrouba
	bool ____coutrouba_32;
	// System.UInt32 GarbageiOS.M_xurcairStarhaw95::_tawevi
	uint32_t ____tawevi_33;
	// System.Single GarbageiOS.M_xurcairStarhaw95::_xelvuNaper
	float ____xelvuNaper_34;
	// System.Boolean GarbageiOS.M_xurcairStarhaw95::_supere
	bool ____supere_35;
	// System.Int32 GarbageiOS.M_xurcairStarhaw95::_woyoYerfare
	int32_t ____woyoYerfare_36;
	// System.String GarbageiOS.M_xurcairStarhaw95::_qijoCujowtrou
	String_t* ____qijoCujowtrou_37;
	// System.Int32 GarbageiOS.M_xurcairStarhaw95::_towmaZayjeva
	int32_t ____towmaZayjeva_38;
	// System.Single GarbageiOS.M_xurcairStarhaw95::_sirpisdar
	float ____sirpisdar_39;
	// System.Boolean GarbageiOS.M_xurcairStarhaw95::_serpuke
	bool ____serpuke_40;
	// System.Single GarbageiOS.M_xurcairStarhaw95::_masmatelDoulor
	float ____masmatelDoulor_41;
	// System.String GarbageiOS.M_xurcairStarhaw95::_maisa
	String_t* ____maisa_42;
	// System.Boolean GarbageiOS.M_xurcairStarhaw95::_jasmecaNeredefi
	bool ____jasmecaNeredefi_43;
	// System.Int32 GarbageiOS.M_xurcairStarhaw95::_saygemDouwo
	int32_t ____saygemDouwo_44;
	// System.Int32 GarbageiOS.M_xurcairStarhaw95::_vourur
	int32_t ____vourur_45;
	// System.Single GarbageiOS.M_xurcairStarhaw95::_rirnallli
	float ____rirnallli_46;
	// System.Boolean GarbageiOS.M_xurcairStarhaw95::_selkolaTrairsenay
	bool ____selkolaTrairsenay_47;
	// System.String GarbageiOS.M_xurcairStarhaw95::_fabahallBoreldre
	String_t* ____fabahallBoreldre_48;
	// System.Single GarbageiOS.M_xurcairStarhaw95::_guruNorgairmear
	float ____guruNorgairmear_49;
	// System.Boolean GarbageiOS.M_xurcairStarhaw95::_mepayVeceldaw
	bool ____mepayVeceldaw_50;

public:
	inline static int32_t get_offset_of__malldraw_0() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____malldraw_0)); }
	inline float get__malldraw_0() const { return ____malldraw_0; }
	inline float* get_address_of__malldraw_0() { return &____malldraw_0; }
	inline void set__malldraw_0(float value)
	{
		____malldraw_0 = value;
	}

	inline static int32_t get_offset_of__treredi_1() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____treredi_1)); }
	inline String_t* get__treredi_1() const { return ____treredi_1; }
	inline String_t** get_address_of__treredi_1() { return &____treredi_1; }
	inline void set__treredi_1(String_t* value)
	{
		____treredi_1 = value;
		Il2CppCodeGenWriteBarrier(&____treredi_1, value);
	}

	inline static int32_t get_offset_of__zemra_2() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____zemra_2)); }
	inline float get__zemra_2() const { return ____zemra_2; }
	inline float* get_address_of__zemra_2() { return &____zemra_2; }
	inline void set__zemra_2(float value)
	{
		____zemra_2 = value;
	}

	inline static int32_t get_offset_of__tassto_3() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____tassto_3)); }
	inline String_t* get__tassto_3() const { return ____tassto_3; }
	inline String_t** get_address_of__tassto_3() { return &____tassto_3; }
	inline void set__tassto_3(String_t* value)
	{
		____tassto_3 = value;
		Il2CppCodeGenWriteBarrier(&____tassto_3, value);
	}

	inline static int32_t get_offset_of__wersto_4() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____wersto_4)); }
	inline float get__wersto_4() const { return ____wersto_4; }
	inline float* get_address_of__wersto_4() { return &____wersto_4; }
	inline void set__wersto_4(float value)
	{
		____wersto_4 = value;
	}

	inline static int32_t get_offset_of__cooqadalJaljearrou_5() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____cooqadalJaljearrou_5)); }
	inline uint32_t get__cooqadalJaljearrou_5() const { return ____cooqadalJaljearrou_5; }
	inline uint32_t* get_address_of__cooqadalJaljearrou_5() { return &____cooqadalJaljearrou_5; }
	inline void set__cooqadalJaljearrou_5(uint32_t value)
	{
		____cooqadalJaljearrou_5 = value;
	}

	inline static int32_t get_offset_of__cirraro_6() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____cirraro_6)); }
	inline String_t* get__cirraro_6() const { return ____cirraro_6; }
	inline String_t** get_address_of__cirraro_6() { return &____cirraro_6; }
	inline void set__cirraro_6(String_t* value)
	{
		____cirraro_6 = value;
		Il2CppCodeGenWriteBarrier(&____cirraro_6, value);
	}

	inline static int32_t get_offset_of__waisunuBeebu_7() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____waisunuBeebu_7)); }
	inline uint32_t get__waisunuBeebu_7() const { return ____waisunuBeebu_7; }
	inline uint32_t* get_address_of__waisunuBeebu_7() { return &____waisunuBeebu_7; }
	inline void set__waisunuBeebu_7(uint32_t value)
	{
		____waisunuBeebu_7 = value;
	}

	inline static int32_t get_offset_of__kelbo_8() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____kelbo_8)); }
	inline int32_t get__kelbo_8() const { return ____kelbo_8; }
	inline int32_t* get_address_of__kelbo_8() { return &____kelbo_8; }
	inline void set__kelbo_8(int32_t value)
	{
		____kelbo_8 = value;
	}

	inline static int32_t get_offset_of__yalejawNisje_9() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____yalejawNisje_9)); }
	inline bool get__yalejawNisje_9() const { return ____yalejawNisje_9; }
	inline bool* get_address_of__yalejawNisje_9() { return &____yalejawNisje_9; }
	inline void set__yalejawNisje_9(bool value)
	{
		____yalejawNisje_9 = value;
	}

	inline static int32_t get_offset_of__gairboudraXemras_10() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____gairboudraXemras_10)); }
	inline uint32_t get__gairboudraXemras_10() const { return ____gairboudraXemras_10; }
	inline uint32_t* get_address_of__gairboudraXemras_10() { return &____gairboudraXemras_10; }
	inline void set__gairboudraXemras_10(uint32_t value)
	{
		____gairboudraXemras_10 = value;
	}

	inline static int32_t get_offset_of__melwhairciPoorar_11() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____melwhairciPoorar_11)); }
	inline int32_t get__melwhairciPoorar_11() const { return ____melwhairciPoorar_11; }
	inline int32_t* get_address_of__melwhairciPoorar_11() { return &____melwhairciPoorar_11; }
	inline void set__melwhairciPoorar_11(int32_t value)
	{
		____melwhairciPoorar_11 = value;
	}

	inline static int32_t get_offset_of__cepoubeRoufou_12() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____cepoubeRoufou_12)); }
	inline bool get__cepoubeRoufou_12() const { return ____cepoubeRoufou_12; }
	inline bool* get_address_of__cepoubeRoufou_12() { return &____cepoubeRoufou_12; }
	inline void set__cepoubeRoufou_12(bool value)
	{
		____cepoubeRoufou_12 = value;
	}

	inline static int32_t get_offset_of__korxekearLairbea_13() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____korxekearLairbea_13)); }
	inline int32_t get__korxekearLairbea_13() const { return ____korxekearLairbea_13; }
	inline int32_t* get_address_of__korxekearLairbea_13() { return &____korxekearLairbea_13; }
	inline void set__korxekearLairbea_13(int32_t value)
	{
		____korxekearLairbea_13 = value;
	}

	inline static int32_t get_offset_of__mupilePeetrear_14() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____mupilePeetrear_14)); }
	inline uint32_t get__mupilePeetrear_14() const { return ____mupilePeetrear_14; }
	inline uint32_t* get_address_of__mupilePeetrear_14() { return &____mupilePeetrear_14; }
	inline void set__mupilePeetrear_14(uint32_t value)
	{
		____mupilePeetrear_14 = value;
	}

	inline static int32_t get_offset_of__yajeeMarneacir_15() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____yajeeMarneacir_15)); }
	inline bool get__yajeeMarneacir_15() const { return ____yajeeMarneacir_15; }
	inline bool* get_address_of__yajeeMarneacir_15() { return &____yajeeMarneacir_15; }
	inline void set__yajeeMarneacir_15(bool value)
	{
		____yajeeMarneacir_15 = value;
	}

	inline static int32_t get_offset_of__jabu_16() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____jabu_16)); }
	inline bool get__jabu_16() const { return ____jabu_16; }
	inline bool* get_address_of__jabu_16() { return &____jabu_16; }
	inline void set__jabu_16(bool value)
	{
		____jabu_16 = value;
	}

	inline static int32_t get_offset_of__larbege_17() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____larbege_17)); }
	inline String_t* get__larbege_17() const { return ____larbege_17; }
	inline String_t** get_address_of__larbege_17() { return &____larbege_17; }
	inline void set__larbege_17(String_t* value)
	{
		____larbege_17 = value;
		Il2CppCodeGenWriteBarrier(&____larbege_17, value);
	}

	inline static int32_t get_offset_of__stuqai_18() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____stuqai_18)); }
	inline float get__stuqai_18() const { return ____stuqai_18; }
	inline float* get_address_of__stuqai_18() { return &____stuqai_18; }
	inline void set__stuqai_18(float value)
	{
		____stuqai_18 = value;
	}

	inline static int32_t get_offset_of__veheSadachere_19() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____veheSadachere_19)); }
	inline uint32_t get__veheSadachere_19() const { return ____veheSadachere_19; }
	inline uint32_t* get_address_of__veheSadachere_19() { return &____veheSadachere_19; }
	inline void set__veheSadachere_19(uint32_t value)
	{
		____veheSadachere_19 = value;
	}

	inline static int32_t get_offset_of__teararwha_20() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____teararwha_20)); }
	inline String_t* get__teararwha_20() const { return ____teararwha_20; }
	inline String_t** get_address_of__teararwha_20() { return &____teararwha_20; }
	inline void set__teararwha_20(String_t* value)
	{
		____teararwha_20 = value;
		Il2CppCodeGenWriteBarrier(&____teararwha_20, value);
	}

	inline static int32_t get_offset_of__nepur_21() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____nepur_21)); }
	inline bool get__nepur_21() const { return ____nepur_21; }
	inline bool* get_address_of__nepur_21() { return &____nepur_21; }
	inline void set__nepur_21(bool value)
	{
		____nepur_21 = value;
	}

	inline static int32_t get_offset_of__surhinal_22() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____surhinal_22)); }
	inline float get__surhinal_22() const { return ____surhinal_22; }
	inline float* get_address_of__surhinal_22() { return &____surhinal_22; }
	inline void set__surhinal_22(float value)
	{
		____surhinal_22 = value;
	}

	inline static int32_t get_offset_of__nidoHawrair_23() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____nidoHawrair_23)); }
	inline uint32_t get__nidoHawrair_23() const { return ____nidoHawrair_23; }
	inline uint32_t* get_address_of__nidoHawrair_23() { return &____nidoHawrair_23; }
	inline void set__nidoHawrair_23(uint32_t value)
	{
		____nidoHawrair_23 = value;
	}

	inline static int32_t get_offset_of__hitreQavu_24() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____hitreQavu_24)); }
	inline bool get__hitreQavu_24() const { return ____hitreQavu_24; }
	inline bool* get_address_of__hitreQavu_24() { return &____hitreQavu_24; }
	inline void set__hitreQavu_24(bool value)
	{
		____hitreQavu_24 = value;
	}

	inline static int32_t get_offset_of__goudair_25() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____goudair_25)); }
	inline String_t* get__goudair_25() const { return ____goudair_25; }
	inline String_t** get_address_of__goudair_25() { return &____goudair_25; }
	inline void set__goudair_25(String_t* value)
	{
		____goudair_25 = value;
		Il2CppCodeGenWriteBarrier(&____goudair_25, value);
	}

	inline static int32_t get_offset_of__pirsiRedir_26() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____pirsiRedir_26)); }
	inline float get__pirsiRedir_26() const { return ____pirsiRedir_26; }
	inline float* get_address_of__pirsiRedir_26() { return &____pirsiRedir_26; }
	inline void set__pirsiRedir_26(float value)
	{
		____pirsiRedir_26 = value;
	}

	inline static int32_t get_offset_of__bomayjer_27() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____bomayjer_27)); }
	inline float get__bomayjer_27() const { return ____bomayjer_27; }
	inline float* get_address_of__bomayjer_27() { return &____bomayjer_27; }
	inline void set__bomayjer_27(float value)
	{
		____bomayjer_27 = value;
	}

	inline static int32_t get_offset_of__vowtrou_28() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____vowtrou_28)); }
	inline String_t* get__vowtrou_28() const { return ____vowtrou_28; }
	inline String_t** get_address_of__vowtrou_28() { return &____vowtrou_28; }
	inline void set__vowtrou_28(String_t* value)
	{
		____vowtrou_28 = value;
		Il2CppCodeGenWriteBarrier(&____vowtrou_28, value);
	}

	inline static int32_t get_offset_of__wheemoojoo_29() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____wheemoojoo_29)); }
	inline uint32_t get__wheemoojoo_29() const { return ____wheemoojoo_29; }
	inline uint32_t* get_address_of__wheemoojoo_29() { return &____wheemoojoo_29; }
	inline void set__wheemoojoo_29(uint32_t value)
	{
		____wheemoojoo_29 = value;
	}

	inline static int32_t get_offset_of__dallkaw_30() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____dallkaw_30)); }
	inline uint32_t get__dallkaw_30() const { return ____dallkaw_30; }
	inline uint32_t* get_address_of__dallkaw_30() { return &____dallkaw_30; }
	inline void set__dallkaw_30(uint32_t value)
	{
		____dallkaw_30 = value;
	}

	inline static int32_t get_offset_of__yekeSepelee_31() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____yekeSepelee_31)); }
	inline uint32_t get__yekeSepelee_31() const { return ____yekeSepelee_31; }
	inline uint32_t* get_address_of__yekeSepelee_31() { return &____yekeSepelee_31; }
	inline void set__yekeSepelee_31(uint32_t value)
	{
		____yekeSepelee_31 = value;
	}

	inline static int32_t get_offset_of__coutrouba_32() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____coutrouba_32)); }
	inline bool get__coutrouba_32() const { return ____coutrouba_32; }
	inline bool* get_address_of__coutrouba_32() { return &____coutrouba_32; }
	inline void set__coutrouba_32(bool value)
	{
		____coutrouba_32 = value;
	}

	inline static int32_t get_offset_of__tawevi_33() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____tawevi_33)); }
	inline uint32_t get__tawevi_33() const { return ____tawevi_33; }
	inline uint32_t* get_address_of__tawevi_33() { return &____tawevi_33; }
	inline void set__tawevi_33(uint32_t value)
	{
		____tawevi_33 = value;
	}

	inline static int32_t get_offset_of__xelvuNaper_34() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____xelvuNaper_34)); }
	inline float get__xelvuNaper_34() const { return ____xelvuNaper_34; }
	inline float* get_address_of__xelvuNaper_34() { return &____xelvuNaper_34; }
	inline void set__xelvuNaper_34(float value)
	{
		____xelvuNaper_34 = value;
	}

	inline static int32_t get_offset_of__supere_35() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____supere_35)); }
	inline bool get__supere_35() const { return ____supere_35; }
	inline bool* get_address_of__supere_35() { return &____supere_35; }
	inline void set__supere_35(bool value)
	{
		____supere_35 = value;
	}

	inline static int32_t get_offset_of__woyoYerfare_36() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____woyoYerfare_36)); }
	inline int32_t get__woyoYerfare_36() const { return ____woyoYerfare_36; }
	inline int32_t* get_address_of__woyoYerfare_36() { return &____woyoYerfare_36; }
	inline void set__woyoYerfare_36(int32_t value)
	{
		____woyoYerfare_36 = value;
	}

	inline static int32_t get_offset_of__qijoCujowtrou_37() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____qijoCujowtrou_37)); }
	inline String_t* get__qijoCujowtrou_37() const { return ____qijoCujowtrou_37; }
	inline String_t** get_address_of__qijoCujowtrou_37() { return &____qijoCujowtrou_37; }
	inline void set__qijoCujowtrou_37(String_t* value)
	{
		____qijoCujowtrou_37 = value;
		Il2CppCodeGenWriteBarrier(&____qijoCujowtrou_37, value);
	}

	inline static int32_t get_offset_of__towmaZayjeva_38() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____towmaZayjeva_38)); }
	inline int32_t get__towmaZayjeva_38() const { return ____towmaZayjeva_38; }
	inline int32_t* get_address_of__towmaZayjeva_38() { return &____towmaZayjeva_38; }
	inline void set__towmaZayjeva_38(int32_t value)
	{
		____towmaZayjeva_38 = value;
	}

	inline static int32_t get_offset_of__sirpisdar_39() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____sirpisdar_39)); }
	inline float get__sirpisdar_39() const { return ____sirpisdar_39; }
	inline float* get_address_of__sirpisdar_39() { return &____sirpisdar_39; }
	inline void set__sirpisdar_39(float value)
	{
		____sirpisdar_39 = value;
	}

	inline static int32_t get_offset_of__serpuke_40() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____serpuke_40)); }
	inline bool get__serpuke_40() const { return ____serpuke_40; }
	inline bool* get_address_of__serpuke_40() { return &____serpuke_40; }
	inline void set__serpuke_40(bool value)
	{
		____serpuke_40 = value;
	}

	inline static int32_t get_offset_of__masmatelDoulor_41() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____masmatelDoulor_41)); }
	inline float get__masmatelDoulor_41() const { return ____masmatelDoulor_41; }
	inline float* get_address_of__masmatelDoulor_41() { return &____masmatelDoulor_41; }
	inline void set__masmatelDoulor_41(float value)
	{
		____masmatelDoulor_41 = value;
	}

	inline static int32_t get_offset_of__maisa_42() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____maisa_42)); }
	inline String_t* get__maisa_42() const { return ____maisa_42; }
	inline String_t** get_address_of__maisa_42() { return &____maisa_42; }
	inline void set__maisa_42(String_t* value)
	{
		____maisa_42 = value;
		Il2CppCodeGenWriteBarrier(&____maisa_42, value);
	}

	inline static int32_t get_offset_of__jasmecaNeredefi_43() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____jasmecaNeredefi_43)); }
	inline bool get__jasmecaNeredefi_43() const { return ____jasmecaNeredefi_43; }
	inline bool* get_address_of__jasmecaNeredefi_43() { return &____jasmecaNeredefi_43; }
	inline void set__jasmecaNeredefi_43(bool value)
	{
		____jasmecaNeredefi_43 = value;
	}

	inline static int32_t get_offset_of__saygemDouwo_44() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____saygemDouwo_44)); }
	inline int32_t get__saygemDouwo_44() const { return ____saygemDouwo_44; }
	inline int32_t* get_address_of__saygemDouwo_44() { return &____saygemDouwo_44; }
	inline void set__saygemDouwo_44(int32_t value)
	{
		____saygemDouwo_44 = value;
	}

	inline static int32_t get_offset_of__vourur_45() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____vourur_45)); }
	inline int32_t get__vourur_45() const { return ____vourur_45; }
	inline int32_t* get_address_of__vourur_45() { return &____vourur_45; }
	inline void set__vourur_45(int32_t value)
	{
		____vourur_45 = value;
	}

	inline static int32_t get_offset_of__rirnallli_46() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____rirnallli_46)); }
	inline float get__rirnallli_46() const { return ____rirnallli_46; }
	inline float* get_address_of__rirnallli_46() { return &____rirnallli_46; }
	inline void set__rirnallli_46(float value)
	{
		____rirnallli_46 = value;
	}

	inline static int32_t get_offset_of__selkolaTrairsenay_47() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____selkolaTrairsenay_47)); }
	inline bool get__selkolaTrairsenay_47() const { return ____selkolaTrairsenay_47; }
	inline bool* get_address_of__selkolaTrairsenay_47() { return &____selkolaTrairsenay_47; }
	inline void set__selkolaTrairsenay_47(bool value)
	{
		____selkolaTrairsenay_47 = value;
	}

	inline static int32_t get_offset_of__fabahallBoreldre_48() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____fabahallBoreldre_48)); }
	inline String_t* get__fabahallBoreldre_48() const { return ____fabahallBoreldre_48; }
	inline String_t** get_address_of__fabahallBoreldre_48() { return &____fabahallBoreldre_48; }
	inline void set__fabahallBoreldre_48(String_t* value)
	{
		____fabahallBoreldre_48 = value;
		Il2CppCodeGenWriteBarrier(&____fabahallBoreldre_48, value);
	}

	inline static int32_t get_offset_of__guruNorgairmear_49() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____guruNorgairmear_49)); }
	inline float get__guruNorgairmear_49() const { return ____guruNorgairmear_49; }
	inline float* get_address_of__guruNorgairmear_49() { return &____guruNorgairmear_49; }
	inline void set__guruNorgairmear_49(float value)
	{
		____guruNorgairmear_49 = value;
	}

	inline static int32_t get_offset_of__mepayVeceldaw_50() { return static_cast<int32_t>(offsetof(M_xurcairStarhaw95_t4177863484, ____mepayVeceldaw_50)); }
	inline bool get__mepayVeceldaw_50() const { return ____mepayVeceldaw_50; }
	inline bool* get_address_of__mepayVeceldaw_50() { return &____mepayVeceldaw_50; }
	inline void set__mepayVeceldaw_50(bool value)
	{
		____mepayVeceldaw_50 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
