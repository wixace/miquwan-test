﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_ValueType1744280289.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.RVO.Sampled.Agent/VO
struct  VO_t2757914308 
{
public:
	// UnityEngine.Vector2 Pathfinding.RVO.Sampled.Agent/VO::origin
	Vector2_t4282066565  ___origin_0;
	// UnityEngine.Vector2 Pathfinding.RVO.Sampled.Agent/VO::center
	Vector2_t4282066565  ___center_1;
	// UnityEngine.Vector2 Pathfinding.RVO.Sampled.Agent/VO::line1
	Vector2_t4282066565  ___line1_2;
	// UnityEngine.Vector2 Pathfinding.RVO.Sampled.Agent/VO::line2
	Vector2_t4282066565  ___line2_3;
	// UnityEngine.Vector2 Pathfinding.RVO.Sampled.Agent/VO::dir1
	Vector2_t4282066565  ___dir1_4;
	// UnityEngine.Vector2 Pathfinding.RVO.Sampled.Agent/VO::dir2
	Vector2_t4282066565  ___dir2_5;
	// UnityEngine.Vector2 Pathfinding.RVO.Sampled.Agent/VO::cutoffLine
	Vector2_t4282066565  ___cutoffLine_6;
	// UnityEngine.Vector2 Pathfinding.RVO.Sampled.Agent/VO::cutoffDir
	Vector2_t4282066565  ___cutoffDir_7;
	// System.Single Pathfinding.RVO.Sampled.Agent/VO::sqrCutoffDistance
	float ___sqrCutoffDistance_8;
	// System.Boolean Pathfinding.RVO.Sampled.Agent/VO::leftSide
	bool ___leftSide_9;
	// System.Boolean Pathfinding.RVO.Sampled.Agent/VO::colliding
	bool ___colliding_10;
	// System.Single Pathfinding.RVO.Sampled.Agent/VO::radius
	float ___radius_11;
	// System.Single Pathfinding.RVO.Sampled.Agent/VO::weightFactor
	float ___weightFactor_12;

public:
	inline static int32_t get_offset_of_origin_0() { return static_cast<int32_t>(offsetof(VO_t2757914308, ___origin_0)); }
	inline Vector2_t4282066565  get_origin_0() const { return ___origin_0; }
	inline Vector2_t4282066565 * get_address_of_origin_0() { return &___origin_0; }
	inline void set_origin_0(Vector2_t4282066565  value)
	{
		___origin_0 = value;
	}

	inline static int32_t get_offset_of_center_1() { return static_cast<int32_t>(offsetof(VO_t2757914308, ___center_1)); }
	inline Vector2_t4282066565  get_center_1() const { return ___center_1; }
	inline Vector2_t4282066565 * get_address_of_center_1() { return &___center_1; }
	inline void set_center_1(Vector2_t4282066565  value)
	{
		___center_1 = value;
	}

	inline static int32_t get_offset_of_line1_2() { return static_cast<int32_t>(offsetof(VO_t2757914308, ___line1_2)); }
	inline Vector2_t4282066565  get_line1_2() const { return ___line1_2; }
	inline Vector2_t4282066565 * get_address_of_line1_2() { return &___line1_2; }
	inline void set_line1_2(Vector2_t4282066565  value)
	{
		___line1_2 = value;
	}

	inline static int32_t get_offset_of_line2_3() { return static_cast<int32_t>(offsetof(VO_t2757914308, ___line2_3)); }
	inline Vector2_t4282066565  get_line2_3() const { return ___line2_3; }
	inline Vector2_t4282066565 * get_address_of_line2_3() { return &___line2_3; }
	inline void set_line2_3(Vector2_t4282066565  value)
	{
		___line2_3 = value;
	}

	inline static int32_t get_offset_of_dir1_4() { return static_cast<int32_t>(offsetof(VO_t2757914308, ___dir1_4)); }
	inline Vector2_t4282066565  get_dir1_4() const { return ___dir1_4; }
	inline Vector2_t4282066565 * get_address_of_dir1_4() { return &___dir1_4; }
	inline void set_dir1_4(Vector2_t4282066565  value)
	{
		___dir1_4 = value;
	}

	inline static int32_t get_offset_of_dir2_5() { return static_cast<int32_t>(offsetof(VO_t2757914308, ___dir2_5)); }
	inline Vector2_t4282066565  get_dir2_5() const { return ___dir2_5; }
	inline Vector2_t4282066565 * get_address_of_dir2_5() { return &___dir2_5; }
	inline void set_dir2_5(Vector2_t4282066565  value)
	{
		___dir2_5 = value;
	}

	inline static int32_t get_offset_of_cutoffLine_6() { return static_cast<int32_t>(offsetof(VO_t2757914308, ___cutoffLine_6)); }
	inline Vector2_t4282066565  get_cutoffLine_6() const { return ___cutoffLine_6; }
	inline Vector2_t4282066565 * get_address_of_cutoffLine_6() { return &___cutoffLine_6; }
	inline void set_cutoffLine_6(Vector2_t4282066565  value)
	{
		___cutoffLine_6 = value;
	}

	inline static int32_t get_offset_of_cutoffDir_7() { return static_cast<int32_t>(offsetof(VO_t2757914308, ___cutoffDir_7)); }
	inline Vector2_t4282066565  get_cutoffDir_7() const { return ___cutoffDir_7; }
	inline Vector2_t4282066565 * get_address_of_cutoffDir_7() { return &___cutoffDir_7; }
	inline void set_cutoffDir_7(Vector2_t4282066565  value)
	{
		___cutoffDir_7 = value;
	}

	inline static int32_t get_offset_of_sqrCutoffDistance_8() { return static_cast<int32_t>(offsetof(VO_t2757914308, ___sqrCutoffDistance_8)); }
	inline float get_sqrCutoffDistance_8() const { return ___sqrCutoffDistance_8; }
	inline float* get_address_of_sqrCutoffDistance_8() { return &___sqrCutoffDistance_8; }
	inline void set_sqrCutoffDistance_8(float value)
	{
		___sqrCutoffDistance_8 = value;
	}

	inline static int32_t get_offset_of_leftSide_9() { return static_cast<int32_t>(offsetof(VO_t2757914308, ___leftSide_9)); }
	inline bool get_leftSide_9() const { return ___leftSide_9; }
	inline bool* get_address_of_leftSide_9() { return &___leftSide_9; }
	inline void set_leftSide_9(bool value)
	{
		___leftSide_9 = value;
	}

	inline static int32_t get_offset_of_colliding_10() { return static_cast<int32_t>(offsetof(VO_t2757914308, ___colliding_10)); }
	inline bool get_colliding_10() const { return ___colliding_10; }
	inline bool* get_address_of_colliding_10() { return &___colliding_10; }
	inline void set_colliding_10(bool value)
	{
		___colliding_10 = value;
	}

	inline static int32_t get_offset_of_radius_11() { return static_cast<int32_t>(offsetof(VO_t2757914308, ___radius_11)); }
	inline float get_radius_11() const { return ___radius_11; }
	inline float* get_address_of_radius_11() { return &___radius_11; }
	inline void set_radius_11(float value)
	{
		___radius_11 = value;
	}

	inline static int32_t get_offset_of_weightFactor_12() { return static_cast<int32_t>(offsetof(VO_t2757914308, ___weightFactor_12)); }
	inline float get_weightFactor_12() const { return ___weightFactor_12; }
	inline float* get_address_of_weightFactor_12() { return &___weightFactor_12; }
	inline void set_weightFactor_12(float value)
	{
		___weightFactor_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Pathfinding.RVO.Sampled.Agent/VO
struct VO_t2757914308_marshaled_pinvoke
{
	Vector2_t4282066565_marshaled_pinvoke ___origin_0;
	Vector2_t4282066565_marshaled_pinvoke ___center_1;
	Vector2_t4282066565_marshaled_pinvoke ___line1_2;
	Vector2_t4282066565_marshaled_pinvoke ___line2_3;
	Vector2_t4282066565_marshaled_pinvoke ___dir1_4;
	Vector2_t4282066565_marshaled_pinvoke ___dir2_5;
	Vector2_t4282066565_marshaled_pinvoke ___cutoffLine_6;
	Vector2_t4282066565_marshaled_pinvoke ___cutoffDir_7;
	float ___sqrCutoffDistance_8;
	int32_t ___leftSide_9;
	int32_t ___colliding_10;
	float ___radius_11;
	float ___weightFactor_12;
};
// Native definition for marshalling of: Pathfinding.RVO.Sampled.Agent/VO
struct VO_t2757914308_marshaled_com
{
	Vector2_t4282066565_marshaled_com ___origin_0;
	Vector2_t4282066565_marshaled_com ___center_1;
	Vector2_t4282066565_marshaled_com ___line1_2;
	Vector2_t4282066565_marshaled_com ___line2_3;
	Vector2_t4282066565_marshaled_com ___dir1_4;
	Vector2_t4282066565_marshaled_com ___dir2_5;
	Vector2_t4282066565_marshaled_com ___cutoffLine_6;
	Vector2_t4282066565_marshaled_com ___cutoffDir_7;
	float ___sqrCutoffDistance_8;
	int32_t ___leftSide_9;
	int32_t ___colliding_10;
	float ___radius_11;
	float ___weightFactor_12;
};
