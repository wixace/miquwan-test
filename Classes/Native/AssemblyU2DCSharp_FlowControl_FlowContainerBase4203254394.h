﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.List`1<FlowControl.IFlowBase>
struct List_1_t2832946830;

#include "AssemblyU2DCSharp_FlowControl_FlowBase3680091731.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FlowControl.FlowContainerBase
struct  FlowContainerBase_t4203254394  : public FlowBase_t3680091731
{
public:
	// System.Collections.Generic.List`1<FlowControl.IFlowBase> FlowControl.FlowContainerBase::childs
	List_1_t2832946830 * ___childs_7;

public:
	inline static int32_t get_offset_of_childs_7() { return static_cast<int32_t>(offsetof(FlowContainerBase_t4203254394, ___childs_7)); }
	inline List_1_t2832946830 * get_childs_7() const { return ___childs_7; }
	inline List_1_t2832946830 ** get_address_of_childs_7() { return &___childs_7; }
	inline void set_childs_7(List_1_t2832946830 * value)
	{
		___childs_7 = value;
		Il2CppCodeGenWriteBarrier(&___childs_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
