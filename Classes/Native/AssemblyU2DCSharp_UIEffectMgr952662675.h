﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject>
struct Dictionary_2_t3671945244;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UIEffectMgr
struct  UIEffectMgr_t952662675  : public Il2CppObject
{
public:
	// System.Int32 UIEffectMgr::curEffid
	int32_t ___curEffid_1;
	// UnityEngine.GameObject UIEffectMgr::_effectParent
	GameObject_t3674682005 * ____effectParent_2;
	// UnityEngine.GameObject UIEffectMgr::effect
	GameObject_t3674682005 * ___effect_3;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityEngine.GameObject> UIEffectMgr::effectdir
	Dictionary_2_t3671945244 * ___effectdir_4;

public:
	inline static int32_t get_offset_of_curEffid_1() { return static_cast<int32_t>(offsetof(UIEffectMgr_t952662675, ___curEffid_1)); }
	inline int32_t get_curEffid_1() const { return ___curEffid_1; }
	inline int32_t* get_address_of_curEffid_1() { return &___curEffid_1; }
	inline void set_curEffid_1(int32_t value)
	{
		___curEffid_1 = value;
	}

	inline static int32_t get_offset_of__effectParent_2() { return static_cast<int32_t>(offsetof(UIEffectMgr_t952662675, ____effectParent_2)); }
	inline GameObject_t3674682005 * get__effectParent_2() const { return ____effectParent_2; }
	inline GameObject_t3674682005 ** get_address_of__effectParent_2() { return &____effectParent_2; }
	inline void set__effectParent_2(GameObject_t3674682005 * value)
	{
		____effectParent_2 = value;
		Il2CppCodeGenWriteBarrier(&____effectParent_2, value);
	}

	inline static int32_t get_offset_of_effect_3() { return static_cast<int32_t>(offsetof(UIEffectMgr_t952662675, ___effect_3)); }
	inline GameObject_t3674682005 * get_effect_3() const { return ___effect_3; }
	inline GameObject_t3674682005 ** get_address_of_effect_3() { return &___effect_3; }
	inline void set_effect_3(GameObject_t3674682005 * value)
	{
		___effect_3 = value;
		Il2CppCodeGenWriteBarrier(&___effect_3, value);
	}

	inline static int32_t get_offset_of_effectdir_4() { return static_cast<int32_t>(offsetof(UIEffectMgr_t952662675, ___effectdir_4)); }
	inline Dictionary_2_t3671945244 * get_effectdir_4() const { return ___effectdir_4; }
	inline Dictionary_2_t3671945244 ** get_address_of_effectdir_4() { return &___effectdir_4; }
	inline void set_effectdir_4(Dictionary_2_t3671945244 * value)
	{
		___effectdir_4 = value;
		Il2CppCodeGenWriteBarrier(&___effectdir_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
