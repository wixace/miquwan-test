﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Compression.LZMA.Decoder
struct Decoder_t548795302;
// System.IO.Stream
struct Stream_t1561764144;
// SevenZip.ICodeProgress
struct ICodeProgress_t453587813;
// System.Byte[]
struct ByteU5BU5D_t4260760469;
// SevenZip.Compression.LZ.OutWindow
struct OutWindow_t503839888;
// SevenZip.Compression.LZMA.Decoder/LenDecoder
struct LenDecoder_t4003695492;
// SevenZip.Compression.RangeCoder.Decoder
struct Decoder_t1102840654;
// SevenZip.Compression.LZMA.Decoder/LiteralDecoder
struct LiteralDecoder_t386054442;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IO_Stream1561764144.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZ_OutWindow503839888.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_4202293321.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_2730629963.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Decode4003695492.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_RangeCoder_1102840654.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Decoder386054442.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Base_St344225277.h"
#include "AssemblyU2DCSharp_SevenZip_Compression_LZMA_Decoder548795302.h"

// System.Void SevenZip.Compression.LZMA.Decoder::.ctor()
extern "C"  void Decoder__ctor_m728650865 (Decoder_t548795302 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder::SetDictionarySize(System.UInt32)
extern "C"  void Decoder_SetDictionarySize_m2532644082 (Decoder_t548795302 * __this, uint32_t ___dictionarySize0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder::SetLiteralProperties(System.Int32,System.Int32)
extern "C"  void Decoder_SetLiteralProperties_m453845203 (Decoder_t548795302 * __this, int32_t ___lp0, int32_t ___lc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder::SetPosBitsProperties(System.Int32)
extern "C"  void Decoder_SetPosBitsProperties_m1427645007 (Decoder_t548795302 * __this, int32_t ___pb0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder::Init(System.IO.Stream,System.IO.Stream)
extern "C"  void Decoder_Init_m1572581891 (Decoder_t548795302 * __this, Stream_t1561764144 * ___inStream0, Stream_t1561764144 * ___outStream1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder::Code(System.IO.Stream,System.IO.Stream,System.Int64,System.Int64,SevenZip.ICodeProgress)
extern "C"  void Decoder_Code_m4000682487 (Decoder_t548795302 * __this, Stream_t1561764144 * ___inStream0, Stream_t1561764144 * ___outStream1, int64_t ___inSize2, int64_t ___outSize3, Il2CppObject * ___progress4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder::SetDecoderProperties(System.Byte[])
extern "C"  void Decoder_SetDecoderProperties_m3786694401 (Decoder_t548795302 * __this, ByteU5BU5D_t4260760469* ___properties0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.Compression.LZMA.Decoder::Train(System.IO.Stream)
extern "C"  bool Decoder_Train_m304474082 (Decoder_t548795302 * __this, Stream_t1561764144 * ___stream0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder::ilo_Create1(SevenZip.Compression.LZ.OutWindow,System.UInt32)
extern "C"  void Decoder_ilo_Create1_m390190421 (Il2CppObject * __this /* static, unused */, OutWindow_t503839888 * ____this0, uint32_t ___windowSize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder::ilo_Init2(SevenZip.Compression.LZ.OutWindow,System.IO.Stream,System.Boolean)
extern "C"  void Decoder_ilo_Init2_m277646164 (Il2CppObject * __this /* static, unused */, OutWindow_t503839888 * ____this0, Stream_t1561764144 * ___stream1, bool ___solid2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder::ilo_Init3(SevenZip.Compression.RangeCoder.BitDecoder&)
extern "C"  void Decoder_ilo_Init3_m1750241320 (Il2CppObject * __this /* static, unused */, BitDecoder_t4202293321 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder::ilo_Init4(SevenZip.Compression.RangeCoder.BitTreeDecoder&)
extern "C"  void Decoder_ilo_Init4_m2575178697 (Il2CppObject * __this /* static, unused */, BitTreeDecoder_t2730629963 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder::ilo_Init5(SevenZip.Compression.LZMA.Decoder/LenDecoder)
extern "C"  void Decoder_ilo_Init5_m2743385565 (Il2CppObject * __this /* static, unused */, LenDecoder_t4003695492 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Decoder::ilo_Decode6(SevenZip.Compression.RangeCoder.BitDecoder&,SevenZip.Compression.RangeCoder.Decoder)
extern "C"  uint32_t Decoder_ilo_Decode6_m1934901300 (Il2CppObject * __this /* static, unused */, BitDecoder_t4202293321 * ____this0, Decoder_t1102840654 * ___rangeDecoder1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte SevenZip.Compression.LZMA.Decoder::ilo_DecodeWithMatchByte7(SevenZip.Compression.LZMA.Decoder/LiteralDecoder,SevenZip.Compression.RangeCoder.Decoder,System.UInt32,System.Byte,System.Byte)
extern "C"  uint8_t Decoder_ilo_DecodeWithMatchByte7_m801523608 (Il2CppObject * __this /* static, unused */, LiteralDecoder_t386054442 * ____this0, Decoder_t1102840654 * ___rangeDecoder1, uint32_t ___pos2, uint8_t ___prevByte3, uint8_t ___matchByte4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder::ilo_UpdateChar8(SevenZip.Compression.LZMA.Base/State&)
extern "C"  void Decoder_ilo_UpdateChar8_m1308458068 (Il2CppObject * __this /* static, unused */, State_t344225277 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte SevenZip.Compression.LZMA.Decoder::ilo_GetByte9(SevenZip.Compression.LZ.OutWindow,System.UInt32)
extern "C"  uint8_t Decoder_ilo_GetByte9_m813777713 (Il2CppObject * __this /* static, unused */, OutWindow_t503839888 * ____this0, uint32_t ___distance1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder::ilo_PutByte10(SevenZip.Compression.LZ.OutWindow,System.Byte)
extern "C"  void Decoder_ilo_PutByte10_m3951560901 (Il2CppObject * __this /* static, unused */, OutWindow_t503839888 * ____this0, uint8_t ___b1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Decoder::ilo_Decode11(SevenZip.Compression.LZMA.Decoder/LenDecoder,SevenZip.Compression.RangeCoder.Decoder,System.UInt32)
extern "C"  uint32_t Decoder_ilo_Decode11_m2746880757 (Il2CppObject * __this /* static, unused */, LenDecoder_t4003695492 * ____this0, Decoder_t1102840654 * ___rangeDecoder1, uint32_t ___posState2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder::ilo_UpdateMatch12(SevenZip.Compression.LZMA.Base/State&)
extern "C"  void Decoder_ilo_UpdateMatch12_m3861203280 (Il2CppObject * __this /* static, unused */, State_t344225277 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Decoder::ilo_DecodeDirectBits13(SevenZip.Compression.RangeCoder.Decoder,System.Int32)
extern "C"  uint32_t Decoder_ilo_DecodeDirectBits13_m3800394991 (Il2CppObject * __this /* static, unused */, Decoder_t1102840654 * ____this0, int32_t ___numTotalBits1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder::ilo_Flush14(SevenZip.Compression.LZ.OutWindow)
extern "C"  void Decoder_ilo_Flush14_m1100014127 (Il2CppObject * __this /* static, unused */, OutWindow_t503839888 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder::ilo_ReleaseStream15(SevenZip.Compression.RangeCoder.Decoder)
extern "C"  void Decoder_ilo_ReleaseStream15_m2676320043 (Il2CppObject * __this /* static, unused */, Decoder_t1102840654 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Decoder::ilo_SetDictionarySize16(SevenZip.Compression.LZMA.Decoder,System.UInt32)
extern "C"  void Decoder_ilo_SetDictionarySize16_m843171922 (Il2CppObject * __this /* static, unused */, Decoder_t548795302 * ____this0, uint32_t ___dictionarySize1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.Compression.LZMA.Decoder::ilo_Train17(SevenZip.Compression.LZ.OutWindow,System.IO.Stream)
extern "C"  bool Decoder_ilo_Train17_m3495026441 (Il2CppObject * __this /* static, unused */, OutWindow_t503839888 * ____this0, Stream_t1561764144 * ___stream1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
