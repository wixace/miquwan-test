﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._0320aa762d7354d96830f3480b871fcd
struct _0320aa762d7354d96830f3480b871fcd_t3090868466;

#include "codegen/il2cpp-codegen.h"

// System.Void Little._0320aa762d7354d96830f3480b871fcd::.ctor()
extern "C"  void _0320aa762d7354d96830f3480b871fcd__ctor_m549072251 (_0320aa762d7354d96830f3480b871fcd_t3090868466 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._0320aa762d7354d96830f3480b871fcd::_0320aa762d7354d96830f3480b871fcdm2(System.Int32)
extern "C"  int32_t _0320aa762d7354d96830f3480b871fcd__0320aa762d7354d96830f3480b871fcdm2_m3266042969 (_0320aa762d7354d96830f3480b871fcd_t3090868466 * __this, int32_t ____0320aa762d7354d96830f3480b871fcda0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._0320aa762d7354d96830f3480b871fcd::_0320aa762d7354d96830f3480b871fcdm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _0320aa762d7354d96830f3480b871fcd__0320aa762d7354d96830f3480b871fcdm_m2947441277 (_0320aa762d7354d96830f3480b871fcd_t3090868466 * __this, int32_t ____0320aa762d7354d96830f3480b871fcda0, int32_t ____0320aa762d7354d96830f3480b871fcd791, int32_t ____0320aa762d7354d96830f3480b871fcdc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
