﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Compression.LZMA.Encoder/Optimal
struct Optimal_t1633575781;

#include "codegen/il2cpp-codegen.h"

// System.Void SevenZip.Compression.LZMA.Encoder/Optimal::.ctor()
extern "C"  void Optimal__ctor_m3548910502 (Optimal_t1633575781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder/Optimal::MakeAsChar()
extern "C"  void Optimal_MakeAsChar_m3689394132 (Optimal_t1633575781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SevenZip.Compression.LZMA.Encoder/Optimal::MakeAsShortRep()
extern "C"  void Optimal_MakeAsShortRep_m3039613375 (Optimal_t1633575781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean SevenZip.Compression.LZMA.Encoder/Optimal::IsShortRep()
extern "C"  bool Optimal_IsShortRep_m2407924981 (Optimal_t1633575781 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
