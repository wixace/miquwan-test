﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OnVoidDelegate
struct OnVoidDelegate_t2787120856;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t2754620036;
// System.AsyncCallback
struct AsyncCallback_t1369114871;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "mscorlib_System_AsyncCallback1369114871.h"

// System.Void OnVoidDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void OnVoidDelegate__ctor_m2005062015 (OnVoidDelegate_t2787120856 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnVoidDelegate::Invoke()
extern "C"  void OnVoidDelegate_Invoke_m1629675161 (OnVoidDelegate_t2787120856 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.IAsyncResult OnVoidDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * OnVoidDelegate_BeginInvoke_m2852299594 (OnVoidDelegate_t2787120856 * __this, AsyncCallback_t1369114871 * ___callback0, Il2CppObject * ___object1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OnVoidDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void OnVoidDelegate_EndInvoke_m2529221391 (OnVoidDelegate_t2787120856 * __this, Il2CppObject * ___result0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
