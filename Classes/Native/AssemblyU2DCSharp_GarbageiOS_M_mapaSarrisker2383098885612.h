﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_mapaSarrisker238
struct  M_mapaSarrisker238_t3098885612  : public Il2CppObject
{
public:
	// System.Boolean GarbageiOS.M_mapaSarrisker238::_napearWhite
	bool ____napearWhite_0;
	// System.String GarbageiOS.M_mapaSarrisker238::_stopatouHelru
	String_t* ____stopatouHelru_1;
	// System.UInt32 GarbageiOS.M_mapaSarrisker238::_wewaSoomur
	uint32_t ____wewaSoomur_2;
	// System.Single GarbageiOS.M_mapaSarrisker238::_luturhir
	float ____luturhir_3;
	// System.UInt32 GarbageiOS.M_mapaSarrisker238::_kukusemWoosawjai
	uint32_t ____kukusemWoosawjai_4;
	// System.Single GarbageiOS.M_mapaSarrisker238::_voqaMalrea
	float ____voqaMalrea_5;
	// System.Boolean GarbageiOS.M_mapaSarrisker238::_mealuCherjalcar
	bool ____mealuCherjalcar_6;
	// System.Int32 GarbageiOS.M_mapaSarrisker238::_seaqirdayXiralhe
	int32_t ____seaqirdayXiralhe_7;
	// System.String GarbageiOS.M_mapaSarrisker238::_pairhur
	String_t* ____pairhur_8;
	// System.Boolean GarbageiOS.M_mapaSarrisker238::_dremjisBelpelhi
	bool ____dremjisBelpelhi_9;
	// System.UInt32 GarbageiOS.M_mapaSarrisker238::_laleacai
	uint32_t ____laleacai_10;
	// System.Boolean GarbageiOS.M_mapaSarrisker238::_jisremcalMizai
	bool ____jisremcalMizai_11;
	// System.Boolean GarbageiOS.M_mapaSarrisker238::_sairtraiNearris
	bool ____sairtraiNearris_12;
	// System.String GarbageiOS.M_mapaSarrisker238::_caivipallJadajere
	String_t* ____caivipallJadajere_13;

public:
	inline static int32_t get_offset_of__napearWhite_0() { return static_cast<int32_t>(offsetof(M_mapaSarrisker238_t3098885612, ____napearWhite_0)); }
	inline bool get__napearWhite_0() const { return ____napearWhite_0; }
	inline bool* get_address_of__napearWhite_0() { return &____napearWhite_0; }
	inline void set__napearWhite_0(bool value)
	{
		____napearWhite_0 = value;
	}

	inline static int32_t get_offset_of__stopatouHelru_1() { return static_cast<int32_t>(offsetof(M_mapaSarrisker238_t3098885612, ____stopatouHelru_1)); }
	inline String_t* get__stopatouHelru_1() const { return ____stopatouHelru_1; }
	inline String_t** get_address_of__stopatouHelru_1() { return &____stopatouHelru_1; }
	inline void set__stopatouHelru_1(String_t* value)
	{
		____stopatouHelru_1 = value;
		Il2CppCodeGenWriteBarrier(&____stopatouHelru_1, value);
	}

	inline static int32_t get_offset_of__wewaSoomur_2() { return static_cast<int32_t>(offsetof(M_mapaSarrisker238_t3098885612, ____wewaSoomur_2)); }
	inline uint32_t get__wewaSoomur_2() const { return ____wewaSoomur_2; }
	inline uint32_t* get_address_of__wewaSoomur_2() { return &____wewaSoomur_2; }
	inline void set__wewaSoomur_2(uint32_t value)
	{
		____wewaSoomur_2 = value;
	}

	inline static int32_t get_offset_of__luturhir_3() { return static_cast<int32_t>(offsetof(M_mapaSarrisker238_t3098885612, ____luturhir_3)); }
	inline float get__luturhir_3() const { return ____luturhir_3; }
	inline float* get_address_of__luturhir_3() { return &____luturhir_3; }
	inline void set__luturhir_3(float value)
	{
		____luturhir_3 = value;
	}

	inline static int32_t get_offset_of__kukusemWoosawjai_4() { return static_cast<int32_t>(offsetof(M_mapaSarrisker238_t3098885612, ____kukusemWoosawjai_4)); }
	inline uint32_t get__kukusemWoosawjai_4() const { return ____kukusemWoosawjai_4; }
	inline uint32_t* get_address_of__kukusemWoosawjai_4() { return &____kukusemWoosawjai_4; }
	inline void set__kukusemWoosawjai_4(uint32_t value)
	{
		____kukusemWoosawjai_4 = value;
	}

	inline static int32_t get_offset_of__voqaMalrea_5() { return static_cast<int32_t>(offsetof(M_mapaSarrisker238_t3098885612, ____voqaMalrea_5)); }
	inline float get__voqaMalrea_5() const { return ____voqaMalrea_5; }
	inline float* get_address_of__voqaMalrea_5() { return &____voqaMalrea_5; }
	inline void set__voqaMalrea_5(float value)
	{
		____voqaMalrea_5 = value;
	}

	inline static int32_t get_offset_of__mealuCherjalcar_6() { return static_cast<int32_t>(offsetof(M_mapaSarrisker238_t3098885612, ____mealuCherjalcar_6)); }
	inline bool get__mealuCherjalcar_6() const { return ____mealuCherjalcar_6; }
	inline bool* get_address_of__mealuCherjalcar_6() { return &____mealuCherjalcar_6; }
	inline void set__mealuCherjalcar_6(bool value)
	{
		____mealuCherjalcar_6 = value;
	}

	inline static int32_t get_offset_of__seaqirdayXiralhe_7() { return static_cast<int32_t>(offsetof(M_mapaSarrisker238_t3098885612, ____seaqirdayXiralhe_7)); }
	inline int32_t get__seaqirdayXiralhe_7() const { return ____seaqirdayXiralhe_7; }
	inline int32_t* get_address_of__seaqirdayXiralhe_7() { return &____seaqirdayXiralhe_7; }
	inline void set__seaqirdayXiralhe_7(int32_t value)
	{
		____seaqirdayXiralhe_7 = value;
	}

	inline static int32_t get_offset_of__pairhur_8() { return static_cast<int32_t>(offsetof(M_mapaSarrisker238_t3098885612, ____pairhur_8)); }
	inline String_t* get__pairhur_8() const { return ____pairhur_8; }
	inline String_t** get_address_of__pairhur_8() { return &____pairhur_8; }
	inline void set__pairhur_8(String_t* value)
	{
		____pairhur_8 = value;
		Il2CppCodeGenWriteBarrier(&____pairhur_8, value);
	}

	inline static int32_t get_offset_of__dremjisBelpelhi_9() { return static_cast<int32_t>(offsetof(M_mapaSarrisker238_t3098885612, ____dremjisBelpelhi_9)); }
	inline bool get__dremjisBelpelhi_9() const { return ____dremjisBelpelhi_9; }
	inline bool* get_address_of__dremjisBelpelhi_9() { return &____dremjisBelpelhi_9; }
	inline void set__dremjisBelpelhi_9(bool value)
	{
		____dremjisBelpelhi_9 = value;
	}

	inline static int32_t get_offset_of__laleacai_10() { return static_cast<int32_t>(offsetof(M_mapaSarrisker238_t3098885612, ____laleacai_10)); }
	inline uint32_t get__laleacai_10() const { return ____laleacai_10; }
	inline uint32_t* get_address_of__laleacai_10() { return &____laleacai_10; }
	inline void set__laleacai_10(uint32_t value)
	{
		____laleacai_10 = value;
	}

	inline static int32_t get_offset_of__jisremcalMizai_11() { return static_cast<int32_t>(offsetof(M_mapaSarrisker238_t3098885612, ____jisremcalMizai_11)); }
	inline bool get__jisremcalMizai_11() const { return ____jisremcalMizai_11; }
	inline bool* get_address_of__jisremcalMizai_11() { return &____jisremcalMizai_11; }
	inline void set__jisremcalMizai_11(bool value)
	{
		____jisremcalMizai_11 = value;
	}

	inline static int32_t get_offset_of__sairtraiNearris_12() { return static_cast<int32_t>(offsetof(M_mapaSarrisker238_t3098885612, ____sairtraiNearris_12)); }
	inline bool get__sairtraiNearris_12() const { return ____sairtraiNearris_12; }
	inline bool* get_address_of__sairtraiNearris_12() { return &____sairtraiNearris_12; }
	inline void set__sairtraiNearris_12(bool value)
	{
		____sairtraiNearris_12 = value;
	}

	inline static int32_t get_offset_of__caivipallJadajere_13() { return static_cast<int32_t>(offsetof(M_mapaSarrisker238_t3098885612, ____caivipallJadajere_13)); }
	inline String_t* get__caivipallJadajere_13() const { return ____caivipallJadajere_13; }
	inline String_t** get_address_of__caivipallJadajere_13() { return &____caivipallJadajere_13; }
	inline void set__caivipallJadajere_13(String_t* value)
	{
		____caivipallJadajere_13 = value;
		Il2CppCodeGenWriteBarrier(&____caivipallJadajere_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
