﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._3ee7250a0ab91594f1afc3fc47128f3c
struct _3ee7250a0ab91594f1afc3fc47128f3c_t1488333271;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__3ee7250a0ab91594f1afc3fc1488333271.h"

// System.Void Little._3ee7250a0ab91594f1afc3fc47128f3c::.ctor()
extern "C"  void _3ee7250a0ab91594f1afc3fc47128f3c__ctor_m3164151350 (_3ee7250a0ab91594f1afc3fc47128f3c_t1488333271 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._3ee7250a0ab91594f1afc3fc47128f3c::_3ee7250a0ab91594f1afc3fc47128f3cm2(System.Int32)
extern "C"  int32_t _3ee7250a0ab91594f1afc3fc47128f3c__3ee7250a0ab91594f1afc3fc47128f3cm2_m1637452345 (_3ee7250a0ab91594f1afc3fc47128f3c_t1488333271 * __this, int32_t ____3ee7250a0ab91594f1afc3fc47128f3ca0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._3ee7250a0ab91594f1afc3fc47128f3c::_3ee7250a0ab91594f1afc3fc47128f3cm(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _3ee7250a0ab91594f1afc3fc47128f3c__3ee7250a0ab91594f1afc3fc47128f3cm_m1085673629 (_3ee7250a0ab91594f1afc3fc47128f3c_t1488333271 * __this, int32_t ____3ee7250a0ab91594f1afc3fc47128f3ca0, int32_t ____3ee7250a0ab91594f1afc3fc47128f3c531, int32_t ____3ee7250a0ab91594f1afc3fc47128f3cc2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._3ee7250a0ab91594f1afc3fc47128f3c::ilo__3ee7250a0ab91594f1afc3fc47128f3cm21(Little._3ee7250a0ab91594f1afc3fc47128f3c,System.Int32)
extern "C"  int32_t _3ee7250a0ab91594f1afc3fc47128f3c_ilo__3ee7250a0ab91594f1afc3fc47128f3cm21_m3247247230 (Il2CppObject * __this /* static, unused */, _3ee7250a0ab91594f1afc3fc47128f3c_t1488333271 * ____this0, int32_t ____3ee7250a0ab91594f1afc3fc47128f3ca1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
