﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._7e290ab6fbd679a4eeb12757df0f4dd4
struct _7e290ab6fbd679a4eeb12757df0f4dd4_t762972708;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__7e290ab6fbd679a4eeb12757d762972708.h"

// System.Void Little._7e290ab6fbd679a4eeb12757df0f4dd4::.ctor()
extern "C"  void _7e290ab6fbd679a4eeb12757df0f4dd4__ctor_m1346913417 (_7e290ab6fbd679a4eeb12757df0f4dd4_t762972708 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7e290ab6fbd679a4eeb12757df0f4dd4::_7e290ab6fbd679a4eeb12757df0f4dd4m2(System.Int32)
extern "C"  int32_t _7e290ab6fbd679a4eeb12757df0f4dd4__7e290ab6fbd679a4eeb12757df0f4dd4m2_m3377184537 (_7e290ab6fbd679a4eeb12757df0f4dd4_t762972708 * __this, int32_t ____7e290ab6fbd679a4eeb12757df0f4dd4a0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7e290ab6fbd679a4eeb12757df0f4dd4::_7e290ab6fbd679a4eeb12757df0f4dd4m(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _7e290ab6fbd679a4eeb12757df0f4dd4__7e290ab6fbd679a4eeb12757df0f4dd4m_m1213142973 (_7e290ab6fbd679a4eeb12757df0f4dd4_t762972708 * __this, int32_t ____7e290ab6fbd679a4eeb12757df0f4dd4a0, int32_t ____7e290ab6fbd679a4eeb12757df0f4dd4651, int32_t ____7e290ab6fbd679a4eeb12757df0f4dd4c2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._7e290ab6fbd679a4eeb12757df0f4dd4::ilo__7e290ab6fbd679a4eeb12757df0f4dd4m21(Little._7e290ab6fbd679a4eeb12757df0f4dd4,System.Int32)
extern "C"  int32_t _7e290ab6fbd679a4eeb12757df0f4dd4_ilo__7e290ab6fbd679a4eeb12757df0f4dd4m21_m1632919659 (Il2CppObject * __this /* static, unused */, _7e290ab6fbd679a4eeb12757df0f4dd4_t762972708 * ____this0, int32_t ____7e290ab6fbd679a4eeb12757df0f4dd4a1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
