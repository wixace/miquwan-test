﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.LocalAvoidance/HalfPlane
struct  HalfPlane_t3916155773  : public Il2CppObject
{
public:
	// UnityEngine.Vector3 Pathfinding.LocalAvoidance/HalfPlane::point
	Vector3_t4282066566  ___point_0;
	// UnityEngine.Vector3 Pathfinding.LocalAvoidance/HalfPlane::normal
	Vector3_t4282066566  ___normal_1;

public:
	inline static int32_t get_offset_of_point_0() { return static_cast<int32_t>(offsetof(HalfPlane_t3916155773, ___point_0)); }
	inline Vector3_t4282066566  get_point_0() const { return ___point_0; }
	inline Vector3_t4282066566 * get_address_of_point_0() { return &___point_0; }
	inline void set_point_0(Vector3_t4282066566  value)
	{
		___point_0 = value;
	}

	inline static int32_t get_offset_of_normal_1() { return static_cast<int32_t>(offsetof(HalfPlane_t3916155773, ___normal_1)); }
	inline Vector3_t4282066566  get_normal_1() const { return ___normal_1; }
	inline Vector3_t4282066566 * get_address_of_normal_1() { return &___normal_1; }
	inline void set_normal_1(Vector3_t4282066566  value)
	{
		___normal_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
