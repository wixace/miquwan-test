﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.Int3[]
struct Int3U5BU5D_t516284607;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.Util.TileHandler/TileType
struct  TileType_t2364590013  : public Il2CppObject
{
public:
	// Pathfinding.Int3[] Pathfinding.Util.TileHandler/TileType::verts
	Int3U5BU5D_t516284607* ___verts_0;
	// System.Int32[] Pathfinding.Util.TileHandler/TileType::tris
	Int32U5BU5D_t3230847821* ___tris_1;
	// Pathfinding.Int3 Pathfinding.Util.TileHandler/TileType::offset
	Int3_t1974045594  ___offset_2;
	// System.Int32 Pathfinding.Util.TileHandler/TileType::lastYOffset
	int32_t ___lastYOffset_3;
	// System.Int32 Pathfinding.Util.TileHandler/TileType::lastRotation
	int32_t ___lastRotation_4;
	// System.Int32 Pathfinding.Util.TileHandler/TileType::width
	int32_t ___width_5;
	// System.Int32 Pathfinding.Util.TileHandler/TileType::depth
	int32_t ___depth_6;

public:
	inline static int32_t get_offset_of_verts_0() { return static_cast<int32_t>(offsetof(TileType_t2364590013, ___verts_0)); }
	inline Int3U5BU5D_t516284607* get_verts_0() const { return ___verts_0; }
	inline Int3U5BU5D_t516284607** get_address_of_verts_0() { return &___verts_0; }
	inline void set_verts_0(Int3U5BU5D_t516284607* value)
	{
		___verts_0 = value;
		Il2CppCodeGenWriteBarrier(&___verts_0, value);
	}

	inline static int32_t get_offset_of_tris_1() { return static_cast<int32_t>(offsetof(TileType_t2364590013, ___tris_1)); }
	inline Int32U5BU5D_t3230847821* get_tris_1() const { return ___tris_1; }
	inline Int32U5BU5D_t3230847821** get_address_of_tris_1() { return &___tris_1; }
	inline void set_tris_1(Int32U5BU5D_t3230847821* value)
	{
		___tris_1 = value;
		Il2CppCodeGenWriteBarrier(&___tris_1, value);
	}

	inline static int32_t get_offset_of_offset_2() { return static_cast<int32_t>(offsetof(TileType_t2364590013, ___offset_2)); }
	inline Int3_t1974045594  get_offset_2() const { return ___offset_2; }
	inline Int3_t1974045594 * get_address_of_offset_2() { return &___offset_2; }
	inline void set_offset_2(Int3_t1974045594  value)
	{
		___offset_2 = value;
	}

	inline static int32_t get_offset_of_lastYOffset_3() { return static_cast<int32_t>(offsetof(TileType_t2364590013, ___lastYOffset_3)); }
	inline int32_t get_lastYOffset_3() const { return ___lastYOffset_3; }
	inline int32_t* get_address_of_lastYOffset_3() { return &___lastYOffset_3; }
	inline void set_lastYOffset_3(int32_t value)
	{
		___lastYOffset_3 = value;
	}

	inline static int32_t get_offset_of_lastRotation_4() { return static_cast<int32_t>(offsetof(TileType_t2364590013, ___lastRotation_4)); }
	inline int32_t get_lastRotation_4() const { return ___lastRotation_4; }
	inline int32_t* get_address_of_lastRotation_4() { return &___lastRotation_4; }
	inline void set_lastRotation_4(int32_t value)
	{
		___lastRotation_4 = value;
	}

	inline static int32_t get_offset_of_width_5() { return static_cast<int32_t>(offsetof(TileType_t2364590013, ___width_5)); }
	inline int32_t get_width_5() const { return ___width_5; }
	inline int32_t* get_address_of_width_5() { return &___width_5; }
	inline void set_width_5(int32_t value)
	{
		___width_5 = value;
	}

	inline static int32_t get_offset_of_depth_6() { return static_cast<int32_t>(offsetof(TileType_t2364590013, ___depth_6)); }
	inline int32_t get_depth_6() const { return ___depth_6; }
	inline int32_t* get_address_of_depth_6() { return &___depth_6; }
	inline void set_depth_6(int32_t value)
	{
		___depth_6 = value;
	}
};

struct TileType_t2364590013_StaticFields
{
public:
	// System.Int32[] Pathfinding.Util.TileHandler/TileType::Rotations
	Int32U5BU5D_t3230847821* ___Rotations_7;

public:
	inline static int32_t get_offset_of_Rotations_7() { return static_cast<int32_t>(offsetof(TileType_t2364590013_StaticFields, ___Rotations_7)); }
	inline Int32U5BU5D_t3230847821* get_Rotations_7() const { return ___Rotations_7; }
	inline Int32U5BU5D_t3230847821** get_address_of_Rotations_7() { return &___Rotations_7; }
	inline void set_Rotations_7(Int32U5BU5D_t3230847821* value)
	{
		___Rotations_7 = value;
		Il2CppCodeGenWriteBarrier(&___Rotations_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
