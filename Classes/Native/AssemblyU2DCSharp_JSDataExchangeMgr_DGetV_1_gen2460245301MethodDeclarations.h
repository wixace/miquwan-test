﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr_DGetV_1_gen4048467712MethodDeclarations.h"

// System.Void JSDataExchangeMgr/DGetV`1<CSGuideVO[]>::.ctor(System.Object,System.IntPtr)
#define DGetV_1__ctor_m2232133874(__this, ___object0, ___method1, method) ((  void (*) (DGetV_1_t2460245301 *, Il2CppObject *, IntPtr_t, const MethodInfo*))DGetV_1__ctor_m3356914363_gshared)(__this, ___object0, ___method1, method)
// T JSDataExchangeMgr/DGetV`1<CSGuideVO[]>::Invoke()
#define DGetV_1_Invoke_m1401368907(__this, method) ((  CSGuideVOU5BU5D_t2582593960* (*) (DGetV_1_t2460245301 *, const MethodInfo*))DGetV_1_Invoke_m1509146772_gshared)(__this, method)
// System.IAsyncResult JSDataExchangeMgr/DGetV`1<CSGuideVO[]>::BeginInvoke(System.AsyncCallback,System.Object)
#define DGetV_1_BeginInvoke_m3703540407(__this, ___callback0, ___object1, method) ((  Il2CppObject * (*) (DGetV_1_t2460245301 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))DGetV_1_BeginInvoke_m2681184398_gshared)(__this, ___callback0, ___object1, method)
// T JSDataExchangeMgr/DGetV`1<CSGuideVO[]>::EndInvoke(System.IAsyncResult)
#define DGetV_1_EndInvoke_m1605909569(__this, ___result0, method) ((  CSGuideVOU5BU5D_t2582593960* (*) (DGetV_1_t2460245301 *, Il2CppObject *, const MethodInfo*))DGetV_1_EndInvoke_m1776565962_gshared)(__this, ___result0, method)
