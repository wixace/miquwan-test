﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<BossAnimationInfo>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m2004123884(__this, ___l0, method) ((  void (*) (Enumerator_t1772194135 *, List_1_t1752521365 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<BossAnimationInfo>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m449884134(__this, method) ((  void (*) (Enumerator_t1772194135 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<BossAnimationInfo>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m3513868050(__this, method) ((  Il2CppObject * (*) (Enumerator_t1772194135 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<BossAnimationInfo>::Dispose()
#define Enumerator_Dispose_m3703507409(__this, method) ((  void (*) (Enumerator_t1772194135 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<BossAnimationInfo>::VerifyState()
#define Enumerator_VerifyState_m1489467530(__this, method) ((  void (*) (Enumerator_t1772194135 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<BossAnimationInfo>::MoveNext()
#define Enumerator_MoveNext_m1231414738(__this, method) ((  bool (*) (Enumerator_t1772194135 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<BossAnimationInfo>::get_Current()
#define Enumerator_get_Current_m505411841(__this, method) ((  BossAnimationInfo_t384335813 * (*) (Enumerator_t1772194135 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
