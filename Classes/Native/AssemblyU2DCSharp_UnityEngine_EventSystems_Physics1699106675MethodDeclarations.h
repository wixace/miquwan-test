﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_EventSystems_PhysicsRaycasterGenerated
struct UnityEngine_EventSystems_PhysicsRaycasterGenerated_t1699106675;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void UnityEngine_EventSystems_PhysicsRaycasterGenerated::.ctor()
extern "C"  void UnityEngine_EventSystems_PhysicsRaycasterGenerated__ctor_m2833253000 (UnityEngine_EventSystems_PhysicsRaycasterGenerated_t1699106675 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PhysicsRaycasterGenerated::PhysicsRaycaster_eventCamera(JSVCall)
extern "C"  void UnityEngine_EventSystems_PhysicsRaycasterGenerated_PhysicsRaycaster_eventCamera_m3164572124 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PhysicsRaycasterGenerated::PhysicsRaycaster_depth(JSVCall)
extern "C"  void UnityEngine_EventSystems_PhysicsRaycasterGenerated_PhysicsRaycaster_depth_m386300344 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PhysicsRaycasterGenerated::PhysicsRaycaster_finalEventMask(JSVCall)
extern "C"  void UnityEngine_EventSystems_PhysicsRaycasterGenerated_PhysicsRaycaster_finalEventMask_m21450689 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PhysicsRaycasterGenerated::PhysicsRaycaster_eventMask(JSVCall)
extern "C"  void UnityEngine_EventSystems_PhysicsRaycasterGenerated_PhysicsRaycaster_eventMask_m1253986485 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_EventSystems_PhysicsRaycasterGenerated::PhysicsRaycaster_Raycast__PointerEventData__ListT1_RaycastResult(JSVCall,System.Int32)
extern "C"  bool UnityEngine_EventSystems_PhysicsRaycasterGenerated_PhysicsRaycaster_Raycast__PointerEventData__ListT1_RaycastResult_m1485025730 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_EventSystems_PhysicsRaycasterGenerated::__Register()
extern "C"  void UnityEngine_EventSystems_PhysicsRaycasterGenerated___Register_m2592460607 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_EventSystems_PhysicsRaycasterGenerated::ilo_getObject1(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_EventSystems_PhysicsRaycasterGenerated_ilo_getObject1_m2891214119 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
