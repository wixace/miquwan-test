﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<HatredCtrl/stValue>
struct List_1_t499163666;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat518836436.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"

// System.Void System.Collections.Generic.List`1/Enumerator<HatredCtrl/stValue>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3702358741_gshared (Enumerator_t518836436 * __this, List_1_t499163666 * ___l0, const MethodInfo* method);
#define Enumerator__ctor_m3702358741(__this, ___l0, method) ((  void (*) (Enumerator_t518836436 *, List_1_t499163666 *, const MethodInfo*))Enumerator__ctor_m3702358741_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HatredCtrl/stValue>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1018585181_gshared (Enumerator_t518836436 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1018585181(__this, method) ((  void (*) (Enumerator_t518836436 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1018585181_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<HatredCtrl/stValue>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1220151379_gshared (Enumerator_t518836436 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m1220151379(__this, method) ((  Il2CppObject * (*) (Enumerator_t518836436 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m1220151379_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HatredCtrl/stValue>::Dispose()
extern "C"  void Enumerator_Dispose_m3851703802_gshared (Enumerator_t518836436 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m3851703802(__this, method) ((  void (*) (Enumerator_t518836436 *, const MethodInfo*))Enumerator_Dispose_m3851703802_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<HatredCtrl/stValue>::VerifyState()
extern "C"  void Enumerator_VerifyState_m542672947_gshared (Enumerator_t518836436 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m542672947(__this, method) ((  void (*) (Enumerator_t518836436 *, const MethodInfo*))Enumerator_VerifyState_m542672947_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<HatredCtrl/stValue>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2089574157_gshared (Enumerator_t518836436 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2089574157(__this, method) ((  bool (*) (Enumerator_t518836436 *, const MethodInfo*))Enumerator_MoveNext_m2089574157_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<HatredCtrl/stValue>::get_Current()
extern "C"  stValue_t3425945410  Enumerator_get_Current_m1131141004_gshared (Enumerator_t518836436 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m1131141004(__this, method) ((  stValue_t3425945410  (*) (Enumerator_t518836436 *, const MethodInfo*))Enumerator_get_Current_m1131141004_gshared)(__this, method)
