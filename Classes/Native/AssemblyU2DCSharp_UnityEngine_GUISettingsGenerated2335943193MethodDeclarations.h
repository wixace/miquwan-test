﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_GUISettingsGenerated
struct UnityEngine_GUISettingsGenerated_t2335943193;
// JSVCall
struct JSVCall_t3708497963;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_GUISettingsGenerated::.ctor()
extern "C"  void UnityEngine_GUISettingsGenerated__ctor_m3266680994 (UnityEngine_GUISettingsGenerated_t2335943193 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUISettingsGenerated::GUISettings_GUISettings1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_GUISettingsGenerated_GUISettings_GUISettings1_m2267268314 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISettingsGenerated::GUISettings_doubleClickSelectsWord(JSVCall)
extern "C"  void UnityEngine_GUISettingsGenerated_GUISettings_doubleClickSelectsWord_m3110242906 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISettingsGenerated::GUISettings_tripleClickSelectsLine(JSVCall)
extern "C"  void UnityEngine_GUISettingsGenerated_GUISettings_tripleClickSelectsLine_m2022719683 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISettingsGenerated::GUISettings_cursorColor(JSVCall)
extern "C"  void UnityEngine_GUISettingsGenerated_GUISettings_cursorColor_m4275301819 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISettingsGenerated::GUISettings_cursorFlashSpeed(JSVCall)
extern "C"  void UnityEngine_GUISettingsGenerated_GUISettings_cursorFlashSpeed_m276543479 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISettingsGenerated::GUISettings_selectionColor(JSVCall)
extern "C"  void UnityEngine_GUISettingsGenerated_GUISettings_selectionColor_m483383213 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISettingsGenerated::__Register()
extern "C"  void UnityEngine_GUISettingsGenerated___Register_m2166094181 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_GUISettingsGenerated::ilo_setBooleanS1(System.Int32,System.Boolean)
extern "C"  void UnityEngine_GUISettingsGenerated_ilo_setBooleanS1_m682586515 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_GUISettingsGenerated::ilo_getBooleanS2(System.Int32)
extern "C"  bool UnityEngine_GUISettingsGenerated_ilo_getBooleanS2_m1487211563 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_GUISettingsGenerated::ilo_setObject3(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UnityEngine_GUISettingsGenerated_ilo_setObject3_m2541415338 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityEngine_GUISettingsGenerated::ilo_getObject4(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UnityEngine_GUISettingsGenerated_ilo_getObject4_m3576898832 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
