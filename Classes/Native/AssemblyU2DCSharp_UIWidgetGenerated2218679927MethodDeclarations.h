﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UIWidgetGenerated
struct UIWidgetGenerated_t2218679927;
// JSVCall
struct JSVCall_t3708497963;
// UIWidget/OnDimensionsChanged
struct OnDimensionsChanged_t3695058769;
// CSRepresentedObject
struct CSRepresentedObject_t3994124630;
// UIWidget/OnPostFillCallback
struct OnPostFillCallback_t3030491454;
// UIDrawCall/OnRenderCallback
struct OnRenderCallback_t2651898963;
// UIWidget/HitCheck
struct HitCheck_t3889696652;
// System.Delegate
struct Delegate_t3310234105;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.Object
struct Il2CppObject;
// UIWidget
struct UIWidget_t769069560;
// UnityEngine.Texture
struct Texture_t2526458961;
// UnityEngine.Shader
struct Shader_t3191267369;
// UnityEngine.Transform
struct Transform_t1659122786;
// BetterList`1<UnityEngine.Vector3>
struct BetterList_1_t1484067282;
// BetterList`1<UnityEngine.Vector2>
struct BetterList_1_t1484067281;
// BetterList`1<UnityEngine.Color32>
struct BetterList_1_t2095821700;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "AssemblyU2DCSharp_CSRepresentedObject3994124630.h"
#include "mscorlib_System_Delegate3310234105.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_UIWidget769069560.h"
#include "AssemblyU2DCSharp_UIDrawCall_OnRenderCallback2651898963.h"
#include "UnityEngine_UnityEngine_Vector44282066567.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "AssemblyU2DCSharp_UIWidget_Pivot240933195.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Shader3191267369.h"
#include "UnityEngine_UnityEngine_Bounds2711641849.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"

// System.Void UIWidgetGenerated::.ctor()
extern "C"  void UIWidgetGenerated__ctor_m2130879700 (UIWidgetGenerated_t2218679927 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_UIWidget1(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_UIWidget1_m3031143710 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget/OnDimensionsChanged UIWidgetGenerated::UIWidget_onChange_GetDelegate_member0_arg0(CSRepresentedObject)
extern "C"  OnDimensionsChanged_t3695058769 * UIWidgetGenerated_UIWidget_onChange_GetDelegate_member0_arg0_m1803973960 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_onChange(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_onChange_m493792447 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget/OnPostFillCallback UIWidgetGenerated::UIWidget_onPostFill_GetDelegate_member1_arg0(CSRepresentedObject)
extern "C"  OnPostFillCallback_t3030491454 * UIWidgetGenerated_UIWidget_onPostFill_GetDelegate_member1_arg0_m2455943461 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_onPostFill(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_onPostFill_m778251596 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIDrawCall/OnRenderCallback UIWidgetGenerated::UIWidget_mOnRender_GetDelegate_member2_arg0(CSRepresentedObject)
extern "C"  OnRenderCallback_t2651898963 * UIWidgetGenerated_UIWidget_mOnRender_GetDelegate_member2_arg0_m3102661089 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_mOnRender(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_mOnRender_m2734663708 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_autoResizeBoxCollider(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_autoResizeBoxCollider_m2410347938 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_hideIfOffScreen(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_hideIfOffScreen_m3992694722 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_keepAspectRatio(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_keepAspectRatio_m3665362352 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_aspectRatio(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_aspectRatio_m3549392523 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget/HitCheck UIWidgetGenerated::UIWidget_hitCheck_GetDelegate_member7_arg0(CSRepresentedObject)
extern "C"  HitCheck_t3889696652 * UIWidgetGenerated_UIWidget_hitCheck_GetDelegate_member7_arg0_m309154896 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_hitCheck(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_hitCheck_m3042816185 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_panel(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_panel_m1961918522 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_geometry(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_geometry_m3905987100 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_fillGeometry(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_fillGeometry_m1097375449 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_drawCall(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_drawCall_m792218316 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_IsMult(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_IsMult_m3294294004 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIDrawCall/OnRenderCallback UIWidgetGenerated::UIWidget_onRender_GetDelegate_member0_arg0(CSRepresentedObject)
extern "C"  OnRenderCallback_t2651898963 * UIWidgetGenerated_UIWidget_onRender_GetDelegate_member0_arg0_m2493205570 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_onRender(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_onRender_m3378037145 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_drawRegion(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_drawRegion_m1728871926 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_pivotOffset(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_pivotOffset_m3581422569 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_width(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_width_m2976478904 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_height(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_height_m2731976455 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_color(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_color_m2199647675 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_alpha(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_alpha_m3346824736 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_isVisible(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_isVisible_m576313046 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_hasVertices(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_hasVertices_m3296981099 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_rawPivot(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_rawPivot_m1474512532 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_pivot(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_pivot_m2580571068 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_depth(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_depth_m2419795547 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_raycastDepth(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_raycastDepth_m304887220 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_localCorners(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_localCorners_m2676207835 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_localSize(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_localSize_m2164207922 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_localCenter(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_localCenter_m2001892382 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_worldCorners(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_worldCorners_m604487426 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_worldCenter(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_worldCenter_m1519420695 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_drawingDimensions(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_drawingDimensions_m465656723 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_material(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_material_m1979302151 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_mainTexture(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_mainTexture_m658631900 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_shader(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_shader_m4067390985 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_hasBoxCollider(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_hasBoxCollider_m1545590281 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_minWidth(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_minWidth_m3221435962 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_minHeight(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_minHeight_m1735710661 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::UIWidget_border(JSVCall)
extern "C"  void UIWidgetGenerated_UIWidget_border_m4271987106 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_CalculateBounds__Transform(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_CalculateBounds__Transform_m1101130798 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_CalculateBounds(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_CalculateBounds_m2792819968 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_CalculateCumulativeAlpha__Int32(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_CalculateCumulativeAlpha__Int32_m3171292110 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_CalculateFinalAlpha__Int32(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_CalculateFinalAlpha__Int32_m2837174621 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_CheckLayer(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_CheckLayer_m160354438 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_CreatePanel(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_CreatePanel_m1830972397 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_GetSides__Transform(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_GetSides__Transform_m121187947 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_Invalidate__Boolean(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_Invalidate__Boolean_m3109844242 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_MakePixelPerfect(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_MakePixelPerfect_m1505126674 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_MarkAsChanged(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_MarkAsChanged_m3107424922 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_OnFill__BetterListT1_Vector3__BetterListT1_Vector2__BetterListT1_Color32(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_OnFill__BetterListT1_Vector3__BetterListT1_Vector2__BetterListT1_Color32_m104563268 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_ParentHasChanged(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_ParentHasChanged_m3162999585 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_RemoveFromPanel(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_RemoveFromPanel_m1616170331 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_ResizeCollider(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_ResizeCollider_m456857829 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_SetDimensions__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_SetDimensions__Int32__Int32_m3084372500 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_SetDirty(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_SetDirty_m1987054509 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_SetRect__Single__Single__Single__Single(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_SetRect__Single__Single__Single__Single_m747456395 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_UpdateGeometry__Int32(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_UpdateGeometry__Int32_m3904927768 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_UpdateTransform__Int32(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_UpdateTransform__Int32_m3091587176 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_UpdateVisibility__Boolean__Boolean(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_UpdateVisibility__Boolean__Boolean_m2766299864 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_WriteToBuffers__BetterListT1_Vector3__BetterListT1_Vector2__BetterListT1_Color32__BetterListT1_Vector3__BetterListT1_Vector4(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_WriteToBuffers__BetterListT1_Vector3__BetterListT1_Vector2__BetterListT1_Color32__BetterListT1_Vector3__BetterListT1_Vector4_m2248168852 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_FullCompareFunc__UIWidget__UIWidget(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_FullCompareFunc__UIWidget__UIWidget_m3892664303 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::UIWidget_PanelCompareFunc__UIWidget__UIWidget(JSVCall,System.Int32)
extern "C"  bool UIWidgetGenerated_UIWidget_PanelCompareFunc__UIWidget__UIWidget_m1148059954 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::__Register()
extern "C"  void UIWidgetGenerated___Register_m1013496947 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget/OnDimensionsChanged UIWidgetGenerated::<UIWidget_onChange>m__16E()
extern "C"  OnDimensionsChanged_t3695058769 * UIWidgetGenerated_U3CUIWidget_onChangeU3Em__16E_m469595861 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget/OnPostFillCallback UIWidgetGenerated::<UIWidget_onPostFill>m__170()
extern "C"  OnPostFillCallback_t3030491454 * UIWidgetGenerated_U3CUIWidget_onPostFillU3Em__170_m2860447093 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIDrawCall/OnRenderCallback UIWidgetGenerated::<UIWidget_mOnRender>m__172()
extern "C"  OnRenderCallback_t2651898963 * UIWidgetGenerated_U3CUIWidget_mOnRenderU3Em__172_m3999780950 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget/HitCheck UIWidgetGenerated::<UIWidget_hitCheck>m__174()
extern "C"  HitCheck_t3889696652 * UIWidgetGenerated_U3CUIWidget_hitCheckU3Em__174_m1278274136 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIDrawCall/OnRenderCallback UIWidgetGenerated::<UIWidget_onRender>m__176()
extern "C"  OnRenderCallback_t2651898963 * UIWidgetGenerated_U3CUIWidget_onRenderU3Em__176_m562629971 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIWidgetGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t UIWidgetGenerated_ilo_getObject1_m4020847970 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool UIWidgetGenerated_ilo_attachFinalizerObject2_m3470119652 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::ilo_addJSFunCSDelegateRel3(System.Int32,System.Delegate)
extern "C"  void UIWidgetGenerated_ilo_addJSFunCSDelegateRel3_m1485760768 (Il2CppObject * __this /* static, unused */, int32_t ___funID0, Delegate_t3310234105 * ___del1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIWidgetGenerated::ilo_setObject4(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t UIWidgetGenerated_ilo_setObject4_m3915860893 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::ilo_setBooleanS5(System.Int32,System.Boolean)
extern "C"  void UIWidgetGenerated_ilo_setBooleanS5_m2478208705 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::ilo_setSingle6(System.Int32,System.Single)
extern "C"  void UIWidgetGenerated_ilo_setSingle6_m1701583269 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UIWidgetGenerated::ilo_getObject7(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * UIWidgetGenerated_ilo_getObject7_m3387953111 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::ilo_set_onRender8(UIWidget,UIDrawCall/OnRenderCallback)
extern "C"  void UIWidgetGenerated_ilo_set_onRender8_m3442236070 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, OnRenderCallback_t2651898963 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UIWidgetGenerated::ilo_get_drawRegion9(UIWidget)
extern "C"  Vector4_t4282066567  UIWidgetGenerated_ilo_get_drawRegion9_m1081014680 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::ilo_set_drawRegion10(UIWidget,UnityEngine.Vector4)
extern "C"  void UIWidgetGenerated_ilo_set_drawRegion10_m1085646769 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, Vector4_t4282066567  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UIWidgetGenerated::ilo_get_pivotOffset11(UIWidget)
extern "C"  Vector2_t4282066565  UIWidgetGenerated_ilo_get_pivotOffset11_m4230230734 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIWidgetGenerated::ilo_get_width12(UIWidget)
extern "C"  int32_t UIWidgetGenerated_ilo_get_width12_m1672392339 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIWidgetGenerated::ilo_getInt3213(System.Int32)
extern "C"  int32_t UIWidgetGenerated_ilo_getInt3213_m1574534464 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::ilo_setInt3214(System.Int32,System.Int32)
extern "C"  void UIWidgetGenerated_ilo_setInt3214_m641413108 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::ilo_set_color15(UIWidget,UnityEngine.Color)
extern "C"  void UIWidgetGenerated_ilo_set_color15_m3280121361 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, Color_t4194546905  ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::ilo_set_alpha16(UIWidget,System.Single)
extern "C"  void UIWidgetGenerated_ilo_set_alpha16_m1814220370 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, float ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::ilo_get_isVisible17(UIWidget)
extern "C"  bool UIWidgetGenerated_ilo_get_isVisible17_m3086717600 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::ilo_get_hasVertices18(UIWidget)
extern "C"  bool UIWidgetGenerated_ilo_get_hasVertices18_m34012012 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget/Pivot UIWidgetGenerated::ilo_get_pivot19(UIWidget)
extern "C"  int32_t UIWidgetGenerated_ilo_get_pivot19_m500482906 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIWidgetGenerated::ilo_getEnum20(System.Int32)
extern "C"  int32_t UIWidgetGenerated_ilo_getEnum20_m3337944723 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIWidgetGenerated::ilo_get_depth21(UIWidget)
extern "C"  int32_t UIWidgetGenerated_ilo_get_depth21_m2313267886 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIWidgetGenerated::ilo_get_raycastDepth22(UIWidget)
extern "C"  int32_t UIWidgetGenerated_ilo_get_raycastDepth22_m937425632 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::ilo_setVector3S23(System.Int32,UnityEngine.Vector3)
extern "C"  void UIWidgetGenerated_ilo_setVector3S23_m3956595561 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector3_t4282066566  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UIWidgetGenerated::ilo_get_localSize24(UIWidget)
extern "C"  Vector2_t4282066565  UIWidgetGenerated_ilo_get_localSize24_m3271611527 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::ilo_setVector2S25(System.Int32,UnityEngine.Vector2)
extern "C"  void UIWidgetGenerated_ilo_setVector2S25_m1479786029 (Il2CppObject * __this /* static, unused */, int32_t ___e0, Vector2_t4282066565  ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UIWidgetGenerated::ilo_get_worldCenter26(UIWidget)
extern "C"  Vector3_t4282066566  UIWidgetGenerated_ilo_get_worldCenter26_m2258100323 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Texture UIWidgetGenerated::ilo_get_mainTexture27(UIWidget)
extern "C"  Texture_t2526458961 * UIWidgetGenerated_ilo_get_mainTexture27_m82122132 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::ilo_set_mainTexture28(UIWidget,UnityEngine.Texture)
extern "C"  void UIWidgetGenerated_ilo_set_mainTexture28_m2180367930 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, Texture_t2526458961 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Shader UIWidgetGenerated::ilo_get_shader29(UIWidget)
extern "C"  Shader_t3191267369 * UIWidgetGenerated_ilo_get_shader29_m3786597439 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::ilo_set_shader30(UIWidget,UnityEngine.Shader)
extern "C"  void UIWidgetGenerated_ilo_set_shader30_m570188256 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, Shader_t3191267369 * ___value1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::ilo_get_hasBoxCollider31(UIWidget)
extern "C"  bool UIWidgetGenerated_ilo_get_hasBoxCollider31_m1529639651 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UIWidgetGenerated::ilo_get_minHeight32(UIWidget)
extern "C"  int32_t UIWidgetGenerated_ilo_get_minHeight32_m2394446756 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Bounds UIWidgetGenerated::ilo_CalculateBounds33(UIWidget,UnityEngine.Transform)
extern "C"  Bounds_t2711641849  UIWidgetGenerated_ilo_CalculateBounds33_m2028667594 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, Transform_t1659122786 * ___relativeParent1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::ilo_CheckLayer34(UIWidget)
extern "C"  void UIWidgetGenerated_ilo_CheckLayer34_m4124724277 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::ilo_moveSaveID2Arr35(System.Int32)
extern "C"  void UIWidgetGenerated_ilo_moveSaveID2Arr35_m1788243406 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::ilo_MarkAsChanged36(UIWidget)
extern "C"  void UIWidgetGenerated_ilo_MarkAsChanged36_m1364145759 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::ilo_OnFill37(UIWidget,BetterList`1<UnityEngine.Vector3>,BetterList`1<UnityEngine.Vector2>,BetterList`1<UnityEngine.Color32>)
extern "C"  void UIWidgetGenerated_ilo_OnFill37_m4093451872 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, BetterList_1_t1484067282 * ___verts1, BetterList_1_t1484067281 * ___uvs2, BetterList_1_t2095821700 * ___cols3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::ilo_SetDimensions38(UIWidget,System.Int32,System.Int32)
extern "C"  void UIWidgetGenerated_ilo_SetDimensions38_m2260091515 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, int32_t ___w1, int32_t ___h2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UIWidgetGenerated::ilo_SetDirty39(UIWidget)
extern "C"  void UIWidgetGenerated_ilo_SetDirty39_m2793363297 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UIWidgetGenerated::ilo_getSingle40(System.Int32)
extern "C"  float UIWidgetGenerated_ilo_getSingle40_m3057273302 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::ilo_UpdateGeometry41(UIWidget,System.Int32)
extern "C"  bool UIWidgetGenerated_ilo_UpdateGeometry41_m2257072360 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, int32_t ___frame1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::ilo_UpdateTransform42(UIWidget,System.Int32)
extern "C"  bool UIWidgetGenerated_ilo_UpdateTransform42_m2158323099 (Il2CppObject * __this /* static, unused */, UIWidget_t769069560 * ____this0, int32_t ___frame1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::ilo_getBooleanS43(System.Int32)
extern "C"  bool UIWidgetGenerated_ilo_getBooleanS43_m1726787428 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UIWidgetGenerated::ilo_isFunctionS44(System.Int32)
extern "C"  bool UIWidgetGenerated_ilo_isFunctionS44_m1796762741 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget/OnDimensionsChanged UIWidgetGenerated::ilo_UIWidget_onChange_GetDelegate_member0_arg045(CSRepresentedObject)
extern "C"  OnDimensionsChanged_t3695058769 * UIWidgetGenerated_ilo_UIWidget_onChange_GetDelegate_member0_arg045_m255022036 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget/OnPostFillCallback UIWidgetGenerated::ilo_UIWidget_onPostFill_GetDelegate_member1_arg046(CSRepresentedObject)
extern "C"  OnPostFillCallback_t3030491454 * UIWidgetGenerated_ilo_UIWidget_onPostFill_GetDelegate_member1_arg046_m2395654992 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// CSRepresentedObject UIWidgetGenerated::ilo_getFunctionS47(System.Int32)
extern "C"  CSRepresentedObject_t3994124630 * UIWidgetGenerated_ilo_getFunctionS47_m3752126335 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIWidget/HitCheck UIWidgetGenerated::ilo_UIWidget_hitCheck_GetDelegate_member7_arg048(CSRepresentedObject)
extern "C"  HitCheck_t3889696652 * UIWidgetGenerated_ilo_UIWidget_hitCheck_GetDelegate_member7_arg048_m934800185 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UIDrawCall/OnRenderCallback UIWidgetGenerated::ilo_UIWidget_onRender_GetDelegate_member0_arg049(CSRepresentedObject)
extern "C"  OnRenderCallback_t2651898963 * UIWidgetGenerated_ilo_UIWidget_onRender_GetDelegate_member0_arg049_m3179271370 (Il2CppObject * __this /* static, unused */, CSRepresentedObject_t3994124630 * ___objFunction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
