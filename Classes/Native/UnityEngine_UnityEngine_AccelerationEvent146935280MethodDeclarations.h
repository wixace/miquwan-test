﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AccelerationEvent
struct AccelerationEvent_t146935280;
struct AccelerationEvent_t146935280_marshaled_pinvoke;
struct AccelerationEvent_t146935280_marshaled_com;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_AccelerationEvent146935280.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// UnityEngine.Vector3 UnityEngine.AccelerationEvent::get_acceleration()
extern "C"  Vector3_t4282066566  AccelerationEvent_get_acceleration_m4025621108 (AccelerationEvent_t146935280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.AccelerationEvent::get_deltaTime()
extern "C"  float AccelerationEvent_get_deltaTime_m1252704101 (AccelerationEvent_t146935280 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;

// Methods for marshaling
struct AccelerationEvent_t146935280;
struct AccelerationEvent_t146935280_marshaled_pinvoke;

extern "C" void AccelerationEvent_t146935280_marshal_pinvoke(const AccelerationEvent_t146935280& unmarshaled, AccelerationEvent_t146935280_marshaled_pinvoke& marshaled);
extern "C" void AccelerationEvent_t146935280_marshal_pinvoke_back(const AccelerationEvent_t146935280_marshaled_pinvoke& marshaled, AccelerationEvent_t146935280& unmarshaled);
extern "C" void AccelerationEvent_t146935280_marshal_pinvoke_cleanup(AccelerationEvent_t146935280_marshaled_pinvoke& marshaled);

// Methods for marshaling
struct AccelerationEvent_t146935280;
struct AccelerationEvent_t146935280_marshaled_com;

extern "C" void AccelerationEvent_t146935280_marshal_com(const AccelerationEvent_t146935280& unmarshaled, AccelerationEvent_t146935280_marshaled_com& marshaled);
extern "C" void AccelerationEvent_t146935280_marshal_com_back(const AccelerationEvent_t146935280_marshaled_com& marshaled, AccelerationEvent_t146935280& unmarshaled);
extern "C" void AccelerationEvent_t146935280_marshal_com_cleanup(AccelerationEvent_t146935280_marshaled_com& marshaled);
