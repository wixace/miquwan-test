﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SevenZip.Compression.LZMA.Base
struct Base_t1184663047;

#include "codegen/il2cpp-codegen.h"

// System.Void SevenZip.Compression.LZMA.Base::.ctor()
extern "C"  void Base__ctor_m1990041184 (Base_t1184663047 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.UInt32 SevenZip.Compression.LZMA.Base::GetLenToPosState(System.UInt32)
extern "C"  uint32_t Base_GetLenToPosState_m4019751492 (Il2CppObject * __this /* static, unused */, uint32_t ___len0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
