﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_faca1
struct M_faca1_t82178104;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_faca182178104.h"

// System.Void GarbageiOS.M_faca1::.ctor()
extern "C"  void M_faca1__ctor_m1686617131 (M_faca1_t82178104 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_faca1::M_sahecal0(System.String[],System.Int32)
extern "C"  void M_faca1_M_sahecal0_m1392671023 (M_faca1_t82178104 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_faca1::M_yiyasvuDebishi1(System.String[],System.Int32)
extern "C"  void M_faca1_M_yiyasvuDebishi1_m3355200109 (M_faca1_t82178104 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_faca1::M_cuxusiDearas2(System.String[],System.Int32)
extern "C"  void M_faca1_M_cuxusiDearas2_m4236028855 (M_faca1_t82178104 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_faca1::M_moukee3(System.String[],System.Int32)
extern "C"  void M_faca1_M_moukee3_m1492565577 (M_faca1_t82178104 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_faca1::M_gaynee4(System.String[],System.Int32)
extern "C"  void M_faca1_M_gaynee4_m4013803443 (M_faca1_t82178104 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_faca1::ilo_M_yiyasvuDebishi11(GarbageiOS.M_faca1,System.String[],System.Int32)
extern "C"  void M_faca1_ilo_M_yiyasvuDebishi11_m4065311709 (Il2CppObject * __this /* static, unused */, M_faca1_t82178104 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_faca1::ilo_M_cuxusiDearas22(GarbageiOS.M_faca1,System.String[],System.Int32)
extern "C"  void M_faca1_ilo_M_cuxusiDearas22_m2185793926 (Il2CppObject * __this /* static, unused */, M_faca1_t82178104 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
