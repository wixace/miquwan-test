﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameObjectVisitor/<FindChild>c__AnonStorey163
struct U3CFindChildU3Ec__AnonStorey163_t3403182860;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void GameObjectVisitor/<FindChild>c__AnonStorey163::.ctor()
extern "C"  void U3CFindChildU3Ec__AnonStorey163__ctor_m1447492575 (U3CFindChildU3Ec__AnonStorey163_t3403182860 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameObjectVisitor/<FindChild>c__AnonStorey163::<>m__48D(UnityEngine.GameObject)
extern "C"  bool U3CFindChildU3Ec__AnonStorey163_U3CU3Em__48D_m1973956862 (U3CFindChildU3Ec__AnonStorey163_t3403182860 * __this, GameObject_t3674682005 * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
