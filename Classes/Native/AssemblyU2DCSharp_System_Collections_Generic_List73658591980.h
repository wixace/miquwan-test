﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// PropertyID
struct PropertyID_t1067426256;
// MethodID
struct MethodID_t3916401116;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System_Collections_Generic_List77_EnumeratorGenerated
struct  System_Collections_Generic_List77_EnumeratorGenerated_t3658591980  : public Il2CppObject
{
public:

public:
};

struct System_Collections_Generic_List77_EnumeratorGenerated_t3658591980_StaticFields
{
public:
	// PropertyID System_Collections_Generic_List77_EnumeratorGenerated::propertyID0
	PropertyID_t1067426256 * ___propertyID0_0;
	// MethodID System_Collections_Generic_List77_EnumeratorGenerated::methodID0
	MethodID_t3916401116 * ___methodID0_1;
	// MethodID System_Collections_Generic_List77_EnumeratorGenerated::methodID1
	MethodID_t3916401116 * ___methodID1_2;

public:
	inline static int32_t get_offset_of_propertyID0_0() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77_EnumeratorGenerated_t3658591980_StaticFields, ___propertyID0_0)); }
	inline PropertyID_t1067426256 * get_propertyID0_0() const { return ___propertyID0_0; }
	inline PropertyID_t1067426256 ** get_address_of_propertyID0_0() { return &___propertyID0_0; }
	inline void set_propertyID0_0(PropertyID_t1067426256 * value)
	{
		___propertyID0_0 = value;
		Il2CppCodeGenWriteBarrier(&___propertyID0_0, value);
	}

	inline static int32_t get_offset_of_methodID0_1() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77_EnumeratorGenerated_t3658591980_StaticFields, ___methodID0_1)); }
	inline MethodID_t3916401116 * get_methodID0_1() const { return ___methodID0_1; }
	inline MethodID_t3916401116 ** get_address_of_methodID0_1() { return &___methodID0_1; }
	inline void set_methodID0_1(MethodID_t3916401116 * value)
	{
		___methodID0_1 = value;
		Il2CppCodeGenWriteBarrier(&___methodID0_1, value);
	}

	inline static int32_t get_offset_of_methodID1_2() { return static_cast<int32_t>(offsetof(System_Collections_Generic_List77_EnumeratorGenerated_t3658591980_StaticFields, ___methodID1_2)); }
	inline MethodID_t3916401116 * get_methodID1_2() const { return ___methodID1_2; }
	inline MethodID_t3916401116 ** get_address_of_methodID1_2() { return &___methodID1_2; }
	inline void set_methodID1_2(MethodID_t3916401116 * value)
	{
		___methodID1_2 = value;
		Il2CppCodeGenWriteBarrier(&___methodID1_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
