﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_jutrortee210
struct  M_jutrortee210_t579802681  : public Il2CppObject
{
public:
	// System.String GarbageiOS.M_jutrortee210::_hurou
	String_t* ____hurou_0;
	// System.Single GarbageiOS.M_jutrortee210::_vawsoone
	float ____vawsoone_1;
	// System.String GarbageiOS.M_jutrortee210::_gateeje
	String_t* ____gateeje_2;
	// System.Int32 GarbageiOS.M_jutrortee210::_ledicairRadu
	int32_t ____ledicairRadu_3;
	// System.Single GarbageiOS.M_jutrortee210::_gispur
	float ____gispur_4;

public:
	inline static int32_t get_offset_of__hurou_0() { return static_cast<int32_t>(offsetof(M_jutrortee210_t579802681, ____hurou_0)); }
	inline String_t* get__hurou_0() const { return ____hurou_0; }
	inline String_t** get_address_of__hurou_0() { return &____hurou_0; }
	inline void set__hurou_0(String_t* value)
	{
		____hurou_0 = value;
		Il2CppCodeGenWriteBarrier(&____hurou_0, value);
	}

	inline static int32_t get_offset_of__vawsoone_1() { return static_cast<int32_t>(offsetof(M_jutrortee210_t579802681, ____vawsoone_1)); }
	inline float get__vawsoone_1() const { return ____vawsoone_1; }
	inline float* get_address_of__vawsoone_1() { return &____vawsoone_1; }
	inline void set__vawsoone_1(float value)
	{
		____vawsoone_1 = value;
	}

	inline static int32_t get_offset_of__gateeje_2() { return static_cast<int32_t>(offsetof(M_jutrortee210_t579802681, ____gateeje_2)); }
	inline String_t* get__gateeje_2() const { return ____gateeje_2; }
	inline String_t** get_address_of__gateeje_2() { return &____gateeje_2; }
	inline void set__gateeje_2(String_t* value)
	{
		____gateeje_2 = value;
		Il2CppCodeGenWriteBarrier(&____gateeje_2, value);
	}

	inline static int32_t get_offset_of__ledicairRadu_3() { return static_cast<int32_t>(offsetof(M_jutrortee210_t579802681, ____ledicairRadu_3)); }
	inline int32_t get__ledicairRadu_3() const { return ____ledicairRadu_3; }
	inline int32_t* get_address_of__ledicairRadu_3() { return &____ledicairRadu_3; }
	inline void set__ledicairRadu_3(int32_t value)
	{
		____ledicairRadu_3 = value;
	}

	inline static int32_t get_offset_of__gispur_4() { return static_cast<int32_t>(offsetof(M_jutrortee210_t579802681, ____gispur_4)); }
	inline float get__gispur_4() const { return ____gispur_4; }
	inline float* get_address_of__gispur_4() { return &____gispur_4; }
	inline void set__gispur_4(float value)
	{
		____gispur_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
