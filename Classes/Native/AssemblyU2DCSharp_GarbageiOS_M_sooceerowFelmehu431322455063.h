﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;

#include "mscorlib_System_Object4170816371.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GarbageiOS.M_sooceerowFelmehu43
struct  M_sooceerowFelmehu43_t1322455063  : public Il2CppObject
{
public:
	// System.Int32 GarbageiOS.M_sooceerowFelmehu43::_chowdordeTeawherdras
	int32_t ____chowdordeTeawherdras_0;
	// System.Boolean GarbageiOS.M_sooceerowFelmehu43::_xouneduCemirpa
	bool ____xouneduCemirpa_1;
	// System.Boolean GarbageiOS.M_sooceerowFelmehu43::_qerorSucasfee
	bool ____qerorSucasfee_2;
	// System.Single GarbageiOS.M_sooceerowFelmehu43::_cadal
	float ____cadal_3;
	// System.Boolean GarbageiOS.M_sooceerowFelmehu43::_bowsownouSermookaw
	bool ____bowsownouSermookaw_4;
	// System.UInt32 GarbageiOS.M_sooceerowFelmehu43::_yawsisHasfee
	uint32_t ____yawsisHasfee_5;
	// System.Single GarbageiOS.M_sooceerowFelmehu43::_pocojerNarra
	float ____pocojerNarra_6;
	// System.String GarbageiOS.M_sooceerowFelmehu43::_tide
	String_t* ____tide_7;
	// System.Single GarbageiOS.M_sooceerowFelmehu43::_xecheebu
	float ____xecheebu_8;
	// System.Int32 GarbageiOS.M_sooceerowFelmehu43::_norfowve
	int32_t ____norfowve_9;
	// System.String GarbageiOS.M_sooceerowFelmehu43::_voucaySoufa
	String_t* ____voucaySoufa_10;
	// System.Single GarbageiOS.M_sooceerowFelmehu43::_merejelTelstear
	float ____merejelTelstear_11;
	// System.UInt32 GarbageiOS.M_sooceerowFelmehu43::_germedu
	uint32_t ____germedu_12;
	// System.String GarbageiOS.M_sooceerowFelmehu43::_coliNedow
	String_t* ____coliNedow_13;
	// System.UInt32 GarbageiOS.M_sooceerowFelmehu43::_rawfe
	uint32_t ____rawfe_14;
	// System.UInt32 GarbageiOS.M_sooceerowFelmehu43::_bempe
	uint32_t ____bempe_15;
	// System.Boolean GarbageiOS.M_sooceerowFelmehu43::_wouporFersas
	bool ____wouporFersas_16;
	// System.UInt32 GarbageiOS.M_sooceerowFelmehu43::_yaynall
	uint32_t ____yaynall_17;
	// System.String GarbageiOS.M_sooceerowFelmehu43::_delwelere
	String_t* ____delwelere_18;
	// System.Int32 GarbageiOS.M_sooceerowFelmehu43::_rojer
	int32_t ____rojer_19;
	// System.String GarbageiOS.M_sooceerowFelmehu43::_wagere
	String_t* ____wagere_20;
	// System.String GarbageiOS.M_sooceerowFelmehu43::_wacizeLairtear
	String_t* ____wacizeLairtear_21;
	// System.Boolean GarbageiOS.M_sooceerowFelmehu43::_wumoseDraska
	bool ____wumoseDraska_22;
	// System.Int32 GarbageiOS.M_sooceerowFelmehu43::_wabiSacoukou
	int32_t ____wabiSacoukou_23;
	// System.Single GarbageiOS.M_sooceerowFelmehu43::_sebearkaw
	float ____sebearkaw_24;
	// System.Single GarbageiOS.M_sooceerowFelmehu43::_gica
	float ____gica_25;
	// System.Single GarbageiOS.M_sooceerowFelmehu43::_nemne
	float ____nemne_26;

public:
	inline static int32_t get_offset_of__chowdordeTeawherdras_0() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____chowdordeTeawherdras_0)); }
	inline int32_t get__chowdordeTeawherdras_0() const { return ____chowdordeTeawherdras_0; }
	inline int32_t* get_address_of__chowdordeTeawherdras_0() { return &____chowdordeTeawherdras_0; }
	inline void set__chowdordeTeawherdras_0(int32_t value)
	{
		____chowdordeTeawherdras_0 = value;
	}

	inline static int32_t get_offset_of__xouneduCemirpa_1() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____xouneduCemirpa_1)); }
	inline bool get__xouneduCemirpa_1() const { return ____xouneduCemirpa_1; }
	inline bool* get_address_of__xouneduCemirpa_1() { return &____xouneduCemirpa_1; }
	inline void set__xouneduCemirpa_1(bool value)
	{
		____xouneduCemirpa_1 = value;
	}

	inline static int32_t get_offset_of__qerorSucasfee_2() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____qerorSucasfee_2)); }
	inline bool get__qerorSucasfee_2() const { return ____qerorSucasfee_2; }
	inline bool* get_address_of__qerorSucasfee_2() { return &____qerorSucasfee_2; }
	inline void set__qerorSucasfee_2(bool value)
	{
		____qerorSucasfee_2 = value;
	}

	inline static int32_t get_offset_of__cadal_3() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____cadal_3)); }
	inline float get__cadal_3() const { return ____cadal_3; }
	inline float* get_address_of__cadal_3() { return &____cadal_3; }
	inline void set__cadal_3(float value)
	{
		____cadal_3 = value;
	}

	inline static int32_t get_offset_of__bowsownouSermookaw_4() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____bowsownouSermookaw_4)); }
	inline bool get__bowsownouSermookaw_4() const { return ____bowsownouSermookaw_4; }
	inline bool* get_address_of__bowsownouSermookaw_4() { return &____bowsownouSermookaw_4; }
	inline void set__bowsownouSermookaw_4(bool value)
	{
		____bowsownouSermookaw_4 = value;
	}

	inline static int32_t get_offset_of__yawsisHasfee_5() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____yawsisHasfee_5)); }
	inline uint32_t get__yawsisHasfee_5() const { return ____yawsisHasfee_5; }
	inline uint32_t* get_address_of__yawsisHasfee_5() { return &____yawsisHasfee_5; }
	inline void set__yawsisHasfee_5(uint32_t value)
	{
		____yawsisHasfee_5 = value;
	}

	inline static int32_t get_offset_of__pocojerNarra_6() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____pocojerNarra_6)); }
	inline float get__pocojerNarra_6() const { return ____pocojerNarra_6; }
	inline float* get_address_of__pocojerNarra_6() { return &____pocojerNarra_6; }
	inline void set__pocojerNarra_6(float value)
	{
		____pocojerNarra_6 = value;
	}

	inline static int32_t get_offset_of__tide_7() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____tide_7)); }
	inline String_t* get__tide_7() const { return ____tide_7; }
	inline String_t** get_address_of__tide_7() { return &____tide_7; }
	inline void set__tide_7(String_t* value)
	{
		____tide_7 = value;
		Il2CppCodeGenWriteBarrier(&____tide_7, value);
	}

	inline static int32_t get_offset_of__xecheebu_8() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____xecheebu_8)); }
	inline float get__xecheebu_8() const { return ____xecheebu_8; }
	inline float* get_address_of__xecheebu_8() { return &____xecheebu_8; }
	inline void set__xecheebu_8(float value)
	{
		____xecheebu_8 = value;
	}

	inline static int32_t get_offset_of__norfowve_9() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____norfowve_9)); }
	inline int32_t get__norfowve_9() const { return ____norfowve_9; }
	inline int32_t* get_address_of__norfowve_9() { return &____norfowve_9; }
	inline void set__norfowve_9(int32_t value)
	{
		____norfowve_9 = value;
	}

	inline static int32_t get_offset_of__voucaySoufa_10() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____voucaySoufa_10)); }
	inline String_t* get__voucaySoufa_10() const { return ____voucaySoufa_10; }
	inline String_t** get_address_of__voucaySoufa_10() { return &____voucaySoufa_10; }
	inline void set__voucaySoufa_10(String_t* value)
	{
		____voucaySoufa_10 = value;
		Il2CppCodeGenWriteBarrier(&____voucaySoufa_10, value);
	}

	inline static int32_t get_offset_of__merejelTelstear_11() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____merejelTelstear_11)); }
	inline float get__merejelTelstear_11() const { return ____merejelTelstear_11; }
	inline float* get_address_of__merejelTelstear_11() { return &____merejelTelstear_11; }
	inline void set__merejelTelstear_11(float value)
	{
		____merejelTelstear_11 = value;
	}

	inline static int32_t get_offset_of__germedu_12() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____germedu_12)); }
	inline uint32_t get__germedu_12() const { return ____germedu_12; }
	inline uint32_t* get_address_of__germedu_12() { return &____germedu_12; }
	inline void set__germedu_12(uint32_t value)
	{
		____germedu_12 = value;
	}

	inline static int32_t get_offset_of__coliNedow_13() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____coliNedow_13)); }
	inline String_t* get__coliNedow_13() const { return ____coliNedow_13; }
	inline String_t** get_address_of__coliNedow_13() { return &____coliNedow_13; }
	inline void set__coliNedow_13(String_t* value)
	{
		____coliNedow_13 = value;
		Il2CppCodeGenWriteBarrier(&____coliNedow_13, value);
	}

	inline static int32_t get_offset_of__rawfe_14() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____rawfe_14)); }
	inline uint32_t get__rawfe_14() const { return ____rawfe_14; }
	inline uint32_t* get_address_of__rawfe_14() { return &____rawfe_14; }
	inline void set__rawfe_14(uint32_t value)
	{
		____rawfe_14 = value;
	}

	inline static int32_t get_offset_of__bempe_15() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____bempe_15)); }
	inline uint32_t get__bempe_15() const { return ____bempe_15; }
	inline uint32_t* get_address_of__bempe_15() { return &____bempe_15; }
	inline void set__bempe_15(uint32_t value)
	{
		____bempe_15 = value;
	}

	inline static int32_t get_offset_of__wouporFersas_16() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____wouporFersas_16)); }
	inline bool get__wouporFersas_16() const { return ____wouporFersas_16; }
	inline bool* get_address_of__wouporFersas_16() { return &____wouporFersas_16; }
	inline void set__wouporFersas_16(bool value)
	{
		____wouporFersas_16 = value;
	}

	inline static int32_t get_offset_of__yaynall_17() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____yaynall_17)); }
	inline uint32_t get__yaynall_17() const { return ____yaynall_17; }
	inline uint32_t* get_address_of__yaynall_17() { return &____yaynall_17; }
	inline void set__yaynall_17(uint32_t value)
	{
		____yaynall_17 = value;
	}

	inline static int32_t get_offset_of__delwelere_18() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____delwelere_18)); }
	inline String_t* get__delwelere_18() const { return ____delwelere_18; }
	inline String_t** get_address_of__delwelere_18() { return &____delwelere_18; }
	inline void set__delwelere_18(String_t* value)
	{
		____delwelere_18 = value;
		Il2CppCodeGenWriteBarrier(&____delwelere_18, value);
	}

	inline static int32_t get_offset_of__rojer_19() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____rojer_19)); }
	inline int32_t get__rojer_19() const { return ____rojer_19; }
	inline int32_t* get_address_of__rojer_19() { return &____rojer_19; }
	inline void set__rojer_19(int32_t value)
	{
		____rojer_19 = value;
	}

	inline static int32_t get_offset_of__wagere_20() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____wagere_20)); }
	inline String_t* get__wagere_20() const { return ____wagere_20; }
	inline String_t** get_address_of__wagere_20() { return &____wagere_20; }
	inline void set__wagere_20(String_t* value)
	{
		____wagere_20 = value;
		Il2CppCodeGenWriteBarrier(&____wagere_20, value);
	}

	inline static int32_t get_offset_of__wacizeLairtear_21() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____wacizeLairtear_21)); }
	inline String_t* get__wacizeLairtear_21() const { return ____wacizeLairtear_21; }
	inline String_t** get_address_of__wacizeLairtear_21() { return &____wacizeLairtear_21; }
	inline void set__wacizeLairtear_21(String_t* value)
	{
		____wacizeLairtear_21 = value;
		Il2CppCodeGenWriteBarrier(&____wacizeLairtear_21, value);
	}

	inline static int32_t get_offset_of__wumoseDraska_22() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____wumoseDraska_22)); }
	inline bool get__wumoseDraska_22() const { return ____wumoseDraska_22; }
	inline bool* get_address_of__wumoseDraska_22() { return &____wumoseDraska_22; }
	inline void set__wumoseDraska_22(bool value)
	{
		____wumoseDraska_22 = value;
	}

	inline static int32_t get_offset_of__wabiSacoukou_23() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____wabiSacoukou_23)); }
	inline int32_t get__wabiSacoukou_23() const { return ____wabiSacoukou_23; }
	inline int32_t* get_address_of__wabiSacoukou_23() { return &____wabiSacoukou_23; }
	inline void set__wabiSacoukou_23(int32_t value)
	{
		____wabiSacoukou_23 = value;
	}

	inline static int32_t get_offset_of__sebearkaw_24() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____sebearkaw_24)); }
	inline float get__sebearkaw_24() const { return ____sebearkaw_24; }
	inline float* get_address_of__sebearkaw_24() { return &____sebearkaw_24; }
	inline void set__sebearkaw_24(float value)
	{
		____sebearkaw_24 = value;
	}

	inline static int32_t get_offset_of__gica_25() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____gica_25)); }
	inline float get__gica_25() const { return ____gica_25; }
	inline float* get_address_of__gica_25() { return &____gica_25; }
	inline void set__gica_25(float value)
	{
		____gica_25 = value;
	}

	inline static int32_t get_offset_of__nemne_26() { return static_cast<int32_t>(offsetof(M_sooceerowFelmehu43_t1322455063, ____nemne_26)); }
	inline float get__nemne_26() const { return ____nemne_26; }
	inline float* get_address_of__nemne_26() { return &____nemne_26; }
	inline void set__nemne_26(float value)
	{
		____nemne_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
