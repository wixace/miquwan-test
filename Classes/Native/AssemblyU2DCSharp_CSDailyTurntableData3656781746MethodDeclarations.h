﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CSDailyTurntableData
struct CSDailyTurntableData_t3656781746;

#include "codegen/il2cpp-codegen.h"

// System.Void CSDailyTurntableData::.ctor()
extern "C"  void CSDailyTurntableData__ctor_m983814953 (CSDailyTurntableData_t3656781746 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
