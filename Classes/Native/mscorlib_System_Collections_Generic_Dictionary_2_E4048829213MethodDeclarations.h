﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<Pathfinding.Int3,System.Int32>
struct Dictionary_2_t2731505821;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_E4048829213.h"
#include "mscorlib_System_Collections_DictionaryEntry1751606614.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22630286527.h"
#include "AssemblyU2DCSharp_Pathfinding_Int31974045594.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C"  void Enumerator__ctor_m688421822_gshared (Enumerator_t4048829213 * __this, Dictionary_2_t2731505821 * ___dictionary0, const MethodInfo* method);
#define Enumerator__ctor_m688421822(__this, ___dictionary0, method) ((  void (*) (Enumerator_t4048829213 *, Dictionary_2_t2731505821 *, const MethodInfo*))Enumerator__ctor_m688421822_gshared)(__this, ___dictionary0, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3963352035_gshared (Enumerator_t4048829213 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m3963352035(__this, method) ((  Il2CppObject * (*) (Enumerator_t4048829213 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3963352035_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1173191223_gshared (Enumerator_t4048829213 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m1173191223(__this, method) ((  void (*) (Enumerator_t4048829213 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m1173191223_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C"  DictionaryEntry_t1751606614  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2160857664_gshared (Enumerator_t4048829213 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2160857664(__this, method) ((  DictionaryEntry_t1751606614  (*) (Enumerator_t4048829213 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m2160857664_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3555687039_gshared (Enumerator_t4048829213 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3555687039(__this, method) ((  Il2CppObject * (*) (Enumerator_t4048829213 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m3555687039_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C"  Il2CppObject * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3564680529_gshared (Enumerator_t4048829213 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3564680529(__this, method) ((  Il2CppObject * (*) (Enumerator_t4048829213 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m3564680529_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2621075491_gshared (Enumerator_t4048829213 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m2621075491(__this, method) ((  bool (*) (Enumerator_t4048829213 *, const MethodInfo*))Enumerator_MoveNext_m2621075491_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Int32>::get_Current()
extern "C"  KeyValuePair_2_t2630286527  Enumerator_get_Current_m3160779949_gshared (Enumerator_t4048829213 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m3160779949(__this, method) ((  KeyValuePair_2_t2630286527  (*) (Enumerator_t4048829213 *, const MethodInfo*))Enumerator_get_Current_m3160779949_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Int32>::get_CurrentKey()
extern "C"  Int3_t1974045594  Enumerator_get_CurrentKey_m1030876464_gshared (Enumerator_t4048829213 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m1030876464(__this, method) ((  Int3_t1974045594  (*) (Enumerator_t4048829213 *, const MethodInfo*))Enumerator_get_CurrentKey_m1030876464_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Int32>::get_CurrentValue()
extern "C"  int32_t Enumerator_get_CurrentValue_m475023700_gshared (Enumerator_t4048829213 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m475023700(__this, method) ((  int32_t (*) (Enumerator_t4048829213 *, const MethodInfo*))Enumerator_get_CurrentValue_m475023700_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Int32>::Reset()
extern "C"  void Enumerator_Reset_m2496682256_gshared (Enumerator_t4048829213 * __this, const MethodInfo* method);
#define Enumerator_Reset_m2496682256(__this, method) ((  void (*) (Enumerator_t4048829213 *, const MethodInfo*))Enumerator_Reset_m2496682256_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Int32>::VerifyState()
extern "C"  void Enumerator_VerifyState_m1138793753_gshared (Enumerator_t4048829213 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m1138793753(__this, method) ((  void (*) (Enumerator_t4048829213 *, const MethodInfo*))Enumerator_VerifyState_m1138793753_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Int32>::VerifyCurrent()
extern "C"  void Enumerator_VerifyCurrent_m1909007937_gshared (Enumerator_t4048829213 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m1909007937(__this, method) ((  void (*) (Enumerator_t4048829213 *, const MethodInfo*))Enumerator_VerifyCurrent_m1909007937_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<Pathfinding.Int3,System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m948358624_gshared (Enumerator_t4048829213 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m948358624(__this, method) ((  void (*) (Enumerator_t4048829213 *, const MethodInfo*))Enumerator_Dispose_m948358624_gshared)(__this, method)
