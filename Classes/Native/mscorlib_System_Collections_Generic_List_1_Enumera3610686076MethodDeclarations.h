﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Poly2Tri.PolygonPoint>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m915369020(__this, ___l0, method) ((  void (*) (Enumerator_t3610686076 *, List_1_t3591013306 *, const MethodInfo*))Enumerator__ctor_m1029849669_gshared)(__this, ___l0, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Poly2Tri.PolygonPoint>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m1204023958(__this, method) ((  void (*) (Enumerator_t3610686076 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<Pathfinding.Poly2Tri.PolygonPoint>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m1438984130(__this, method) ((  Il2CppObject * (*) (Enumerator_t3610686076 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Poly2Tri.PolygonPoint>::Dispose()
#define Enumerator_Dispose_m881707297(__this, method) ((  void (*) (Enumerator_t3610686076 *, const MethodInfo*))Enumerator_Dispose_m2904289642_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<Pathfinding.Poly2Tri.PolygonPoint>::VerifyState()
#define Enumerator_VerifyState_m2709917658(__this, method) ((  void (*) (Enumerator_t3610686076 *, const MethodInfo*))Enumerator_VerifyState_m1522854819_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<Pathfinding.Poly2Tri.PolygonPoint>::MoveNext()
#define Enumerator_MoveNext_m3807106178(__this, method) ((  bool (*) (Enumerator_t3610686076 *, const MethodInfo*))Enumerator_MoveNext_m4284703760_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<Pathfinding.Poly2Tri.PolygonPoint>::get_Current()
#define Enumerator_get_Current_m1038332497(__this, method) ((  PolygonPoint_t2222827754 * (*) (Enumerator_t3610686076 *, const MethodInfo*))Enumerator_get_Current_m396252160_gshared)(__this, method)
