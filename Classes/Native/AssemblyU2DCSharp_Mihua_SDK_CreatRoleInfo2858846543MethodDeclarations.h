﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mihua.SDK.CreatRoleInfo
struct CreatRoleInfo_t2858846543;
// System.Object[]
struct ObjectU5BU5D_t1108656482;

#include "codegen/il2cpp-codegen.h"

// System.Void Mihua.SDK.CreatRoleInfo::.ctor()
extern "C"  void CreatRoleInfo__ctor_m1092982408 (CreatRoleInfo_t2858846543 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mihua.SDK.CreatRoleInfo Mihua.SDK.CreatRoleInfo::Parse(System.Object[])
extern "C"  CreatRoleInfo_t2858846543 * CreatRoleInfo_Parse_m3332817865 (Il2CppObject * __this /* static, unused */, ObjectU5BU5D_t1108656482* ___crtRoleOjs0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
