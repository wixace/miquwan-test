﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<HatredCtrl/stValue>
struct DefaultComparer_t1934191187;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_HatredCtrl_stValue3425945410.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<HatredCtrl/stValue>::.ctor()
extern "C"  void DefaultComparer__ctor_m101775176_gshared (DefaultComparer_t1934191187 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m101775176(__this, method) ((  void (*) (DefaultComparer_t1934191187 *, const MethodInfo*))DefaultComparer__ctor_m101775176_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<HatredCtrl/stValue>::Compare(T,T)
extern "C"  int32_t DefaultComparer_Compare_m1549564207_gshared (DefaultComparer_t1934191187 * __this, stValue_t3425945410  ___x0, stValue_t3425945410  ___y1, const MethodInfo* method);
#define DefaultComparer_Compare_m1549564207(__this, ___x0, ___y1, method) ((  int32_t (*) (DefaultComparer_t1934191187 *, stValue_t3425945410 , stValue_t3425945410 , const MethodInfo*))DefaultComparer_Compare_m1549564207_gshared)(__this, ___x0, ___y1, method)
