﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine_TextureGenerated
struct UnityEngine_TextureGenerated_t1021226012;
// JSVCall
struct JSVCall_t3708497963;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void UnityEngine_TextureGenerated::.ctor()
extern "C"  void UnityEngine_TextureGenerated__ctor_m2156656127 (UnityEngine_TextureGenerated_t1021226012 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextureGenerated::Texture_Texture1(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextureGenerated_Texture_Texture1_m3110496739 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextureGenerated::Texture_masterTextureLimit(JSVCall)
extern "C"  void UnityEngine_TextureGenerated_Texture_masterTextureLimit_m2179026908 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextureGenerated::Texture_anisotropicFiltering(JSVCall)
extern "C"  void UnityEngine_TextureGenerated_Texture_anisotropicFiltering_m1045392293 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextureGenerated::Texture_width(JSVCall)
extern "C"  void UnityEngine_TextureGenerated_Texture_width_m1120447624 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextureGenerated::Texture_height(JSVCall)
extern "C"  void UnityEngine_TextureGenerated_Texture_height_m1029581623 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextureGenerated::Texture_filterMode(JSVCall)
extern "C"  void UnityEngine_TextureGenerated_Texture_filterMode_m1302321571 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextureGenerated::Texture_anisoLevel(JSVCall)
extern "C"  void UnityEngine_TextureGenerated_Texture_anisoLevel_m491194578 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextureGenerated::Texture_wrapMode(JSVCall)
extern "C"  void UnityEngine_TextureGenerated_Texture_wrapMode_m2099779601 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextureGenerated::Texture_mipMapBias(JSVCall)
extern "C"  void UnityEngine_TextureGenerated_Texture_mipMapBias_m3608732765 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextureGenerated::Texture_texelSize(JSVCall)
extern "C"  void UnityEngine_TextureGenerated_Texture_texelSize_m662716479 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextureGenerated::Texture_GetNativeTexturePtr(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextureGenerated_Texture_GetNativeTexturePtr_m206288757 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextureGenerated::Texture_SetGlobalAnisotropicFilteringLimits__Int32__Int32(JSVCall,System.Int32)
extern "C"  bool UnityEngine_TextureGenerated_Texture_SetGlobalAnisotropicFilteringLimits__Int32__Int32_m1660090859 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextureGenerated::__Register()
extern "C"  void UnityEngine_TextureGenerated___Register_m1248489000 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine_TextureGenerated::ilo_attachFinalizerObject1(System.Int32)
extern "C"  bool UnityEngine_TextureGenerated_ilo_attachFinalizerObject1_m2094682048 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextureGenerated::ilo_addJSCSRel2(System.Int32,System.Object)
extern "C"  void UnityEngine_TextureGenerated_ilo_addJSCSRel2_m4286619068 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_TextureGenerated::ilo_getInt323(System.Int32)
extern "C"  int32_t UnityEngine_TextureGenerated_ilo_getInt323_m1833508424 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextureGenerated::ilo_setEnum4(System.Int32,System.Int32)
extern "C"  void UnityEngine_TextureGenerated_ilo_setEnum4_m37051837 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine_TextureGenerated::ilo_getEnum5(System.Int32)
extern "C"  int32_t UnityEngine_TextureGenerated_ilo_getEnum5_m1241287733 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine_TextureGenerated::ilo_setInt326(System.Int32,System.Int32)
extern "C"  void UnityEngine_TextureGenerated_ilo_setInt326_m904904338 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine_TextureGenerated::ilo_getSingle7(System.Int32)
extern "C"  float UnityEngine_TextureGenerated_ilo_getSingle7_m2258371598 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
