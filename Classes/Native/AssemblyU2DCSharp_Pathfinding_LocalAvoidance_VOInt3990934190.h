﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Pathfinding.LocalAvoidance/VO
struct VO_t2172220293;

#include "mscorlib_System_ValueType1744280289.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pathfinding.LocalAvoidance/VOIntersection
struct  VOIntersection_t3990934190 
{
public:
	// Pathfinding.LocalAvoidance/VO Pathfinding.LocalAvoidance/VOIntersection::vo1
	VO_t2172220293 * ___vo1_0;
	// Pathfinding.LocalAvoidance/VO Pathfinding.LocalAvoidance/VOIntersection::vo2
	VO_t2172220293 * ___vo2_1;
	// System.Single Pathfinding.LocalAvoidance/VOIntersection::factor1
	float ___factor1_2;
	// System.Single Pathfinding.LocalAvoidance/VOIntersection::factor2
	float ___factor2_3;
	// System.Boolean Pathfinding.LocalAvoidance/VOIntersection::inside
	bool ___inside_4;

public:
	inline static int32_t get_offset_of_vo1_0() { return static_cast<int32_t>(offsetof(VOIntersection_t3990934190, ___vo1_0)); }
	inline VO_t2172220293 * get_vo1_0() const { return ___vo1_0; }
	inline VO_t2172220293 ** get_address_of_vo1_0() { return &___vo1_0; }
	inline void set_vo1_0(VO_t2172220293 * value)
	{
		___vo1_0 = value;
		Il2CppCodeGenWriteBarrier(&___vo1_0, value);
	}

	inline static int32_t get_offset_of_vo2_1() { return static_cast<int32_t>(offsetof(VOIntersection_t3990934190, ___vo2_1)); }
	inline VO_t2172220293 * get_vo2_1() const { return ___vo2_1; }
	inline VO_t2172220293 ** get_address_of_vo2_1() { return &___vo2_1; }
	inline void set_vo2_1(VO_t2172220293 * value)
	{
		___vo2_1 = value;
		Il2CppCodeGenWriteBarrier(&___vo2_1, value);
	}

	inline static int32_t get_offset_of_factor1_2() { return static_cast<int32_t>(offsetof(VOIntersection_t3990934190, ___factor1_2)); }
	inline float get_factor1_2() const { return ___factor1_2; }
	inline float* get_address_of_factor1_2() { return &___factor1_2; }
	inline void set_factor1_2(float value)
	{
		___factor1_2 = value;
	}

	inline static int32_t get_offset_of_factor2_3() { return static_cast<int32_t>(offsetof(VOIntersection_t3990934190, ___factor2_3)); }
	inline float get_factor2_3() const { return ___factor2_3; }
	inline float* get_address_of_factor2_3() { return &___factor2_3; }
	inline void set_factor2_3(float value)
	{
		___factor2_3 = value;
	}

	inline static int32_t get_offset_of_inside_4() { return static_cast<int32_t>(offsetof(VOIntersection_t3990934190, ___inside_4)); }
	inline bool get_inside_4() const { return ___inside_4; }
	inline bool* get_address_of_inside_4() { return &___inside_4; }
	inline void set_inside_4(bool value)
	{
		___inside_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for marshalling of: Pathfinding.LocalAvoidance/VOIntersection
struct VOIntersection_t3990934190_marshaled_pinvoke
{
	VO_t2172220293 * ___vo1_0;
	VO_t2172220293 * ___vo2_1;
	float ___factor1_2;
	float ___factor2_3;
	int32_t ___inside_4;
};
// Native definition for marshalling of: Pathfinding.LocalAvoidance/VOIntersection
struct VOIntersection_t3990934190_marshaled_com
{
	VO_t2172220293 * ___vo1_0;
	VO_t2172220293 * ___vo2_1;
	float ___factor1_2;
	float ___factor2_3;
	int32_t ___inside_4;
};
