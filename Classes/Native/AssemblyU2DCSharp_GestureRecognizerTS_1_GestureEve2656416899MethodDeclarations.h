﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GestureRecognizerTS_1_GestureEve2002909991MethodDeclarations.h"

// System.Void GestureRecognizerTS`1/GestureEventHandler<SwipeGesture>::.ctor(System.Object,System.IntPtr)
#define GestureEventHandler__ctor_m1239688433(__this, ___object0, ___method1, method) ((  void (*) (GestureEventHandler_t2656416899 *, Il2CppObject *, IntPtr_t, const MethodInfo*))GestureEventHandler__ctor_m3273015516_gshared)(__this, ___object0, ___method1, method)
// System.Void GestureRecognizerTS`1/GestureEventHandler<SwipeGesture>::Invoke(T)
#define GestureEventHandler_Invoke_m1976498739(__this, ___gesture0, method) ((  void (*) (GestureEventHandler_t2656416899 *, SwipeGesture_t529355983 *, const MethodInfo*))GestureEventHandler_Invoke_m61391848_gshared)(__this, ___gesture0, method)
// System.IAsyncResult GestureRecognizerTS`1/GestureEventHandler<SwipeGesture>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define GestureEventHandler_BeginInvoke_m4008248456(__this, ___gesture0, ___callback1, ___object2, method) ((  Il2CppObject * (*) (GestureEventHandler_t2656416899 *, SwipeGesture_t529355983 *, AsyncCallback_t1369114871 *, Il2CppObject *, const MethodInfo*))GestureEventHandler_BeginInvoke_m2633461941_gshared)(__this, ___gesture0, ___callback1, ___object2, method)
// System.Void GestureRecognizerTS`1/GestureEventHandler<SwipeGesture>::EndInvoke(System.IAsyncResult)
#define GestureEventHandler_EndInvoke_m2057942913(__this, ___result0, method) ((  void (*) (GestureEventHandler_t2656416899 *, Il2CppObject *, const MethodInfo*))GestureEventHandler_EndInvoke_m1518633196_gshared)(__this, ___result0, method)
