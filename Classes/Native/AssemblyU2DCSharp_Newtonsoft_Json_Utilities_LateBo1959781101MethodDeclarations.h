﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateSet>c__AnonStorey146`1<System.Object>
struct U3CCreateSetU3Ec__AnonStorey146_1_t1959781101;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object4170816371.h"

// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateSet>c__AnonStorey146`1<System.Object>::.ctor()
extern "C"  void U3CCreateSetU3Ec__AnonStorey146_1__ctor_m1255944076_gshared (U3CCreateSetU3Ec__AnonStorey146_1_t1959781101 * __this, const MethodInfo* method);
#define U3CCreateSetU3Ec__AnonStorey146_1__ctor_m1255944076(__this, method) ((  void (*) (U3CCreateSetU3Ec__AnonStorey146_1_t1959781101 *, const MethodInfo*))U3CCreateSetU3Ec__AnonStorey146_1__ctor_m1255944076_gshared)(__this, method)
// System.Void Newtonsoft.Json.Utilities.LateBoundReflectionDelegateFactory/<CreateSet>c__AnonStorey146`1<System.Object>::<>m__3AD(T,System.Object)
extern "C"  void U3CCreateSetU3Ec__AnonStorey146_1_U3CU3Em__3AD_m2403638793_gshared (U3CCreateSetU3Ec__AnonStorey146_1_t1959781101 * __this, Il2CppObject * ___o0, Il2CppObject * ___v1, const MethodInfo* method);
#define U3CCreateSetU3Ec__AnonStorey146_1_U3CU3Em__3AD_m2403638793(__this, ___o0, ___v1, method) ((  void (*) (U3CCreateSetU3Ec__AnonStorey146_1_t1959781101 *, Il2CppObject *, Il2CppObject *, const MethodInfo*))U3CCreateSetU3Ec__AnonStorey146_1_U3CU3Em__3AD_m2403638793_gshared)(__this, ___o0, ___v1, method)
