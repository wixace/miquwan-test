﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System_DecimalGenerated
struct System_DecimalGenerated_t3663372622;
// JSVCall
struct JSVCall_t3708497963;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// System.Object
struct Il2CppObject;
// JSDataExchangeMgr
struct JSDataExchangeMgr_t3744712290;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_JSVCall3708497963.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_JSDataExchangeMgr3744712290.h"

// System.Void System_DecimalGenerated::.ctor()
extern "C"  void System_DecimalGenerated__ctor_m2138658269 (System_DecimalGenerated_t3663372622 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DecimalGenerated::.cctor()
extern "C"  void System_DecimalGenerated__cctor_m1391800688 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Decimal1(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Decimal1_m1634034801 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Decimal2(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Decimal2_m2878799282 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Decimal3(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Decimal3_m4123563763 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Decimal4(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Decimal4_m1073360948 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Decimal5(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Decimal5_m2318125429 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Decimal6(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Decimal6_m3562889910 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Decimal7(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Decimal7_m512687095 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Decimal8(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Decimal8_m1757451576 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Decimal9(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Decimal9_m3002216057 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DecimalGenerated::Decimal_MinValue(JSVCall)
extern "C"  void System_DecimalGenerated_Decimal_MinValue_m2870934547 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DecimalGenerated::Decimal_MaxValue(JSVCall)
extern "C"  void System_DecimalGenerated_Decimal_MaxValue_m555889573 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DecimalGenerated::Decimal_MinusOne(JSVCall)
extern "C"  void System_DecimalGenerated_Decimal_MinusOne_m3637525596 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DecimalGenerated::Decimal_One(JSVCall)
extern "C"  void System_DecimalGenerated_Decimal_One_m1122695284 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DecimalGenerated::Decimal_Zero(JSVCall)
extern "C"  void System_DecimalGenerated_Decimal_Zero_m2363381706 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_CompareTo__Object(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_CompareTo__Object_m791719152 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_CompareTo__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_CompareTo__Decimal_m3423939522 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Equals__Object(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Equals__Object_m3091846895 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Equals__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Equals__Decimal_m1713455523 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_GetHashCode(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_GetHashCode_m368967554 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_GetTypeCode(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_GetTypeCode_m2133667502 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_ToString__String__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_ToString__String__IFormatProvider_m676298917 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_ToString__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_ToString__IFormatProvider_m1158637014 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_ToString__String(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_ToString__String_m2473678894 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_ToString(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_ToString_m1005897437 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Add__Decimal__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Add__Decimal__Decimal_m3874068338 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Ceiling__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Ceiling__Decimal_m2037686693 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Compare__Decimal__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Compare__Decimal__Decimal_m474609590 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Divide__Decimal__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Divide__Decimal__Decimal_m2460184586 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Equals__Decimal__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Equals__Decimal__Decimal_m538260816 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Floor__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Floor__Decimal_m2453708918 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_FromOACurrency__Int64(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_FromOACurrency__Int64_m2873639889 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_GetBits__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_GetBits__Decimal_m1933078790 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Multiply__Decimal__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Multiply__Decimal__Decimal_m2146220725 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Negate__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Negate__Decimal_m2152788480 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_op_Addition__Decimal__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_op_Addition__Decimal__Decimal_m1256831403 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_op_Division__Decimal__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_op_Division__Decimal__Decimal_m2334648924 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_op_Equality__Decimal__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_op_Equality__Decimal__Decimal_m3182108073 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_op_GreaterThan__Decimal__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_op_GreaterThan__Decimal__Decimal_m3925360302 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_op_GreaterThanOrEqual__Decimal__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_op_GreaterThanOrEqual__Decimal__Decimal_m822375685 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_op_Implicit__Int64_to_Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_op_Implicit__Int64_to_Decimal_m3867941171 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_op_Implicit__UInt64_to_Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_op_Implicit__UInt64_to_Decimal_m3796159394 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_op_Implicit__Int16_to_Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_op_Implicit__Int16_to_Decimal_m1586487980 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_op_Implicit__UInt16_to_Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_op_Implicit__UInt16_to_Decimal_m1514706203 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_op_Implicit__Char_to_Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_op_Implicit__Char_to_Decimal_m1042167492 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_op_Implicit__Int32_to_Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_op_Implicit__Int32_to_Decimal_m935481778 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_op_Implicit__Byte_to_Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_op_Implicit__Byte_to_Decimal_m1780639570 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_op_Implicit__UInt32_to_Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_op_Implicit__UInt32_to_Decimal_m863700001 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_op_Implicit__SByte_to_Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_op_Implicit__SByte_to_Decimal_m868020677 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_op_Inequality__Decimal__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_op_Inequality__Decimal__Decimal_m1914357070 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_op_LessThan__Decimal__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_op_LessThan__Decimal__Decimal_m2047489929 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_op_LessThanOrEqual__Decimal__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_op_LessThanOrEqual__Decimal__Decimal_m2632769482 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_op_Multiply__Decimal__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_op_Multiply__Decimal__Decimal_m394157267 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_op_Subtraction__Decimal__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_op_Subtraction__Decimal__Decimal_m4278281575 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_op_UnaryNegation__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_op_UnaryNegation__Decimal_m20055788 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Parse__String__NumberStyles__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Parse__String__NumberStyles__IFormatProvider_m4016702803 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Parse__String__IFormatProvider(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Parse__String__IFormatProvider_m64475710 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Parse__String__NumberStyles(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Parse__String__NumberStyles_m3055413696 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Parse__String(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Parse__String_m1892244853 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Remainder__Decimal__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Remainder__Decimal__Decimal_m1386065142 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Round__Decimal__Int32__MidpointRounding(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Round__Decimal__Int32__MidpointRounding_m1055849560 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Round__Decimal__Int32(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Round__Decimal__Int32_m2824802844 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Round__Decimal__MidpointRounding(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Round__Decimal__MidpointRounding_m468375280 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Round__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Round__Decimal_m3087327924 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Subtract__Decimal__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Subtract__Decimal__Decimal_m97222565 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_ToByte__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_ToByte__Decimal_m1583269567 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_ToDouble__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_ToDouble__Decimal_m3096796566 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_ToInt16__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_ToInt16__Decimal_m2365793865 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_ToInt32__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_ToInt32__Decimal_m1743888079 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_ToInt64__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_ToInt64__Decimal_m4057551504 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_ToOACurrency__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_ToOACurrency__Decimal_m2034683076 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_ToSByte__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_ToSByte__Decimal_m2351638434 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_ToSingle__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_ToSingle__Decimal_m2275477055 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_ToUInt16__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_ToUInt16__Decimal_m1074956488 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_ToUInt32__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_ToUInt32__Decimal_m453050702 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_ToUInt64__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_ToUInt64__Decimal_m2766714127 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_Truncate__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_Truncate__Decimal_m2714638108 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_TryParse__String__NumberStyles__IFormatProvider__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_TryParse__String__NumberStyles__IFormatProvider__Decimal_m893489061 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::Decimal_TryParse__String__Decimal(JSVCall,System.Int32)
extern "C"  bool System_DecimalGenerated_Decimal_TryParse__String__Decimal_m2977856089 (Il2CppObject * __this /* static, unused */, JSVCall_t3708497963 * ___vc0, int32_t ___argc1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DecimalGenerated::__Register()
extern "C"  void System_DecimalGenerated___Register_m785664266 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] System_DecimalGenerated::<Decimal_Decimal9>m__C0()
extern "C"  Int32U5BU5D_t3230847821* System_DecimalGenerated_U3CDecimal_Decimal9U3Em__C0_m3328139093 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_DecimalGenerated::ilo_getObject1(System.Int32)
extern "C"  int32_t System_DecimalGenerated_ilo_getObject1_m3182188153 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::ilo_attachFinalizerObject2(System.Int32)
extern "C"  bool System_DecimalGenerated_ilo_attachFinalizerObject2_m718429691 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System_DecimalGenerated::ilo_getBooleanS3(System.Int32)
extern "C"  bool System_DecimalGenerated_ilo_getBooleanS3_m297684713 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte System_DecimalGenerated::ilo_getByte4(System.Int32)
extern "C"  uint8_t System_DecimalGenerated_ilo_getByte4_m1076784085 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DecimalGenerated::ilo_addJSCSRel5(System.Int32,System.Object)
extern "C"  void System_DecimalGenerated_ilo_addJSCSRel5_m2598605853 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObj1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_DecimalGenerated::ilo_setObject6(JSDataExchangeMgr,System.Int32,System.Object)
extern "C"  int32_t System_DecimalGenerated_ilo_setObject6_m2017234230 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, Il2CppObject * ___csObj2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DecimalGenerated::ilo_setInt327(System.Int32,System.Int32)
extern "C"  void System_DecimalGenerated_ilo_setInt327_m1153915347 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DecimalGenerated::ilo_changeJSObj8(System.Int32,System.Object)
extern "C"  void System_DecimalGenerated_ilo_changeJSObj8_m196306227 (Il2CppObject * __this /* static, unused */, int32_t ___jsObjID0, Il2CppObject * ___csObjNew1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System_DecimalGenerated::ilo_getObject9(JSDataExchangeMgr,System.Int32)
extern "C"  Il2CppObject * System_DecimalGenerated_ilo_getObject9_m1529664112 (Il2CppObject * __this /* static, unused */, JSDataExchangeMgr_t3744712290 * ____this0, int32_t ___e1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DecimalGenerated::ilo_setEnum10(System.Int32,System.Int32)
extern "C"  void System_DecimalGenerated_ilo_setEnum10_m3395877884 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System_DecimalGenerated::ilo_getStringS11(System.Int32)
extern "C"  String_t* System_DecimalGenerated_ilo_getStringS11_m1119176884 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DecimalGenerated::ilo_moveSaveID2Arr12(System.Int32)
extern "C"  void System_DecimalGenerated_ilo_moveSaveID2Arr12_m4246496420 (Il2CppObject * __this /* static, unused */, int32_t ___arrIndex0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DecimalGenerated::ilo_setArrayS13(System.Int32,System.Int32,System.Boolean)
extern "C"  void System_DecimalGenerated_ilo_setArrayS13_m689680541 (Il2CppObject * __this /* static, unused */, int32_t ___e0, int32_t ___count1, bool ___bClear2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_DecimalGenerated::ilo_getInt3214(System.Int32)
extern "C"  int32_t System_DecimalGenerated_ilo_getInt3214_m2243426456 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DecimalGenerated::ilo_setBooleanS15(System.Int32,System.Boolean)
extern "C"  void System_DecimalGenerated_ilo_setBooleanS15_m1540491923 (Il2CppObject * __this /* static, unused */, int32_t ___e0, bool ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_DecimalGenerated::ilo_getEnum16(System.Int32)
extern "C"  int32_t System_DecimalGenerated_ilo_getEnum16_m3722612291 (Il2CppObject * __this /* static, unused */, int32_t ___e0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DecimalGenerated::ilo_setDouble17(System.Int32,System.Double)
extern "C"  void System_DecimalGenerated_ilo_setDouble17_m4285904398 (Il2CppObject * __this /* static, unused */, int32_t ___e0, double ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DecimalGenerated::ilo_setSingle18(System.Int32,System.Single)
extern "C"  void System_DecimalGenerated_ilo_setSingle18_m1738391343 (Il2CppObject * __this /* static, unused */, int32_t ___e0, float ___v1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System_DecimalGenerated::ilo_setArgIndex19(System.Int32)
extern "C"  void System_DecimalGenerated_ilo_setArgIndex19_m674959775 (Il2CppObject * __this /* static, unused */, int32_t ___i0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System_DecimalGenerated::ilo_getArrayLength20(System.Int32)
extern "C"  int32_t System_DecimalGenerated_ilo_getArrayLength20_m4150324164 (Il2CppObject * __this /* static, unused */, int32_t ___id0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
