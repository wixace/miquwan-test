﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Camera
struct Camera_t2727095145;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FixScreenSizeBillBoard
struct  FixScreenSizeBillBoard_t1500673245  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean FixScreenSizeBillBoard::fixScreenSize
	bool ___fixScreenSize_2;
	// UnityEngine.Camera FixScreenSizeBillBoard::mainCamera
	Camera_t2727095145 * ___mainCamera_3;
	// UnityEngine.Vector3 FixScreenSizeBillBoard::localScale
	Vector3_t4282066566  ___localScale_4;
	// System.Single FixScreenSizeBillBoard::scaleAdjust
	float ___scaleAdjust_5;

public:
	inline static int32_t get_offset_of_fixScreenSize_2() { return static_cast<int32_t>(offsetof(FixScreenSizeBillBoard_t1500673245, ___fixScreenSize_2)); }
	inline bool get_fixScreenSize_2() const { return ___fixScreenSize_2; }
	inline bool* get_address_of_fixScreenSize_2() { return &___fixScreenSize_2; }
	inline void set_fixScreenSize_2(bool value)
	{
		___fixScreenSize_2 = value;
	}

	inline static int32_t get_offset_of_mainCamera_3() { return static_cast<int32_t>(offsetof(FixScreenSizeBillBoard_t1500673245, ___mainCamera_3)); }
	inline Camera_t2727095145 * get_mainCamera_3() const { return ___mainCamera_3; }
	inline Camera_t2727095145 ** get_address_of_mainCamera_3() { return &___mainCamera_3; }
	inline void set_mainCamera_3(Camera_t2727095145 * value)
	{
		___mainCamera_3 = value;
		Il2CppCodeGenWriteBarrier(&___mainCamera_3, value);
	}

	inline static int32_t get_offset_of_localScale_4() { return static_cast<int32_t>(offsetof(FixScreenSizeBillBoard_t1500673245, ___localScale_4)); }
	inline Vector3_t4282066566  get_localScale_4() const { return ___localScale_4; }
	inline Vector3_t4282066566 * get_address_of_localScale_4() { return &___localScale_4; }
	inline void set_localScale_4(Vector3_t4282066566  value)
	{
		___localScale_4 = value;
	}

	inline static int32_t get_offset_of_scaleAdjust_5() { return static_cast<int32_t>(offsetof(FixScreenSizeBillBoard_t1500673245, ___scaleAdjust_5)); }
	inline float get_scaleAdjust_5() const { return ___scaleAdjust_5; }
	inline float* get_address_of_scaleAdjust_5() { return &___scaleAdjust_5; }
	inline void set_scaleAdjust_5(float value)
	{
		___scaleAdjust_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
