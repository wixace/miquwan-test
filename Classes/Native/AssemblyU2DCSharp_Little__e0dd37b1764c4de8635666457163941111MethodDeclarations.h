﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Little._e0dd37b1764c4de8635666457be8c4de
struct _e0dd37b1764c4de8635666457be8c4de_t163941111;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_Little__e0dd37b1764c4de8635666457163941111.h"

// System.Void Little._e0dd37b1764c4de8635666457be8c4de::.ctor()
extern "C"  void _e0dd37b1764c4de8635666457be8c4de__ctor_m695533846 (_e0dd37b1764c4de8635666457be8c4de_t163941111 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e0dd37b1764c4de8635666457be8c4de::_e0dd37b1764c4de8635666457be8c4dem2(System.Int32)
extern "C"  int32_t _e0dd37b1764c4de8635666457be8c4de__e0dd37b1764c4de8635666457be8c4dem2_m254080569 (_e0dd37b1764c4de8635666457be8c4de_t163941111 * __this, int32_t ____e0dd37b1764c4de8635666457be8c4dea0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e0dd37b1764c4de8635666457be8c4de::_e0dd37b1764c4de8635666457be8c4dem(System.Int32,System.Int32,System.Int32)
extern "C"  int32_t _e0dd37b1764c4de8635666457be8c4de__e0dd37b1764c4de8635666457be8c4dem_m77341853 (_e0dd37b1764c4de8635666457be8c4de_t163941111 * __this, int32_t ____e0dd37b1764c4de8635666457be8c4dea0, int32_t ____e0dd37b1764c4de8635666457be8c4de601, int32_t ____e0dd37b1764c4de8635666457be8c4dec2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Little._e0dd37b1764c4de8635666457be8c4de::ilo__e0dd37b1764c4de8635666457be8c4dem21(Little._e0dd37b1764c4de8635666457be8c4de,System.Int32)
extern "C"  int32_t _e0dd37b1764c4de8635666457be8c4de_ilo__e0dd37b1764c4de8635666457be8c4dem21_m1799091358 (Il2CppObject * __this /* static, unused */, _e0dd37b1764c4de8635666457be8c4de_t163941111 * ____this0, int32_t ____e0dd37b1764c4de8635666457be8c4dea1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
