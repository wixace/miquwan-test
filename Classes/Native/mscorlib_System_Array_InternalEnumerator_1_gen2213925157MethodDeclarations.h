﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Il2CppArray;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen2213925157.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_Pathfinding_Voxels_CompactVoxelS3431582481.h"

// System.Void System.Array/InternalEnumerator`1<Pathfinding.Voxels.CompactVoxelSpan>::.ctor(System.Array)
extern "C"  void InternalEnumerator_1__ctor_m1478718313_gshared (InternalEnumerator_1_t2213925157 * __this, Il2CppArray * ___array0, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m1478718313(__this, ___array0, method) ((  void (*) (InternalEnumerator_1_t2213925157 *, Il2CppArray *, const MethodInfo*))InternalEnumerator_1__ctor_m1478718313_gshared)(__this, ___array0, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.Voxels.CompactVoxelSpan>::System.Collections.IEnumerator.Reset()
extern "C"  void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3623407511_gshared (InternalEnumerator_1_t2213925157 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3623407511(__this, method) ((  void (*) (InternalEnumerator_1_t2213925157 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m3623407511_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<Pathfinding.Voxels.CompactVoxelSpan>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3773155405_gshared (InternalEnumerator_1_t2213925157 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3773155405(__this, method) ((  Il2CppObject * (*) (InternalEnumerator_1_t2213925157 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m3773155405_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<Pathfinding.Voxels.CompactVoxelSpan>::Dispose()
extern "C"  void InternalEnumerator_1_Dispose_m3704454272_gshared (InternalEnumerator_1_t2213925157 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m3704454272(__this, method) ((  void (*) (InternalEnumerator_1_t2213925157 *, const MethodInfo*))InternalEnumerator_1_Dispose_m3704454272_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<Pathfinding.Voxels.CompactVoxelSpan>::MoveNext()
extern "C"  bool InternalEnumerator_1_MoveNext_m1672445895_gshared (InternalEnumerator_1_t2213925157 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m1672445895(__this, method) ((  bool (*) (InternalEnumerator_1_t2213925157 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m1672445895_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<Pathfinding.Voxels.CompactVoxelSpan>::get_Current()
extern "C"  CompactVoxelSpan_t3431582481  InternalEnumerator_1_get_Current_m3668934226_gshared (InternalEnumerator_1_t2213925157 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m3668934226(__this, method) ((  CompactVoxelSpan_t3431582481  (*) (InternalEnumerator_1_t2213925157 *, const MethodInfo*))InternalEnumerator_1_get_Current_m3668934226_gshared)(__this, method)
