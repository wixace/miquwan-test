﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MUIWrapContent
struct MUIWrapContent_t4054684270;
// UIPanel
struct UIPanel_t295209936;
// UnityEngine.Transform
struct Transform_t1659122786;
// UIScrollView
struct UIScrollView_t2113479878;
// MUIWrapContent/OnInitializeItem
struct OnInitializeItem_t4000839715;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_UIPanel295209936.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "AssemblyU2DCSharp_UIScrollView2113479878.h"
#include "AssemblyU2DCSharp_MUIWrapContent4054684270.h"
#include "AssemblyU2DCSharp_MUIWrapContent_OnInitializeItem4000839715.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"

// System.Void MUIWrapContent::.ctor()
extern "C"  void MUIWrapContent__ctor_m3776648685 (MUIWrapContent_t4054684270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::Start()
extern "C"  void MUIWrapContent_Start_m2723786477 (MUIWrapContent_t4054684270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::OnStart()
extern "C"  void MUIWrapContent_OnStart_m3493483918 (MUIWrapContent_t4054684270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::OnMove(UIPanel)
extern "C"  void MUIWrapContent_OnMove_m2159179323 (MUIWrapContent_t4054684270 * __this, UIPanel_t295209936 * ___panel0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::SortBasedOnScrollMovement()
extern "C"  void MUIWrapContent_SortBasedOnScrollMovement_m2907658043 (MUIWrapContent_t4054684270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::SortAlphabetically()
extern "C"  void MUIWrapContent_SortAlphabetically_m3895615226 (MUIWrapContent_t4054684270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MUIWrapContent::CacheScrollView()
extern "C"  bool MUIWrapContent_CacheScrollView_m1983474059 (MUIWrapContent_t4054684270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::ResetChildPositions()
extern "C"  void MUIWrapContent_ResetChildPositions_m3350054952 (MUIWrapContent_t4054684270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::ResetAllItem(System.Boolean)
extern "C"  void MUIWrapContent_ResetAllItem_m2585834227 (MUIWrapContent_t4054684270 * __this, bool ___isData0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::ResetAllItemDown()
extern "C"  void MUIWrapContent_ResetAllItemDown_m980369726 (MUIWrapContent_t4054684270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::ResetAllItemAndResetPositions()
extern "C"  void MUIWrapContent_ResetAllItemAndResetPositions_m1149652440 (MUIWrapContent_t4054684270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform MUIWrapContent::zhiDing(System.Int32,System.Int32)
extern "C"  Transform_t1659122786 * MUIWrapContent_zhiDing_m1642382208 (MUIWrapContent_t4054684270 * __this, int32_t ___indx0, int32_t ___count1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::TopByIndex(System.Int32,System.Int32,System.Int32,UIPanel)
extern "C"  void MUIWrapContent_TopByIndex_m2370818666 (MUIWrapContent_t4054684270 * __this, int32_t ___index0, int32_t ___count1, int32_t ___num2, UIPanel_t295209936 * ___panel3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::WrapContent()
extern "C"  void MUIWrapContent_WrapContent_m1865894042 (MUIWrapContent_t4054684270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::OnValidate()
extern "C"  void MUIWrapContent_OnValidate_m314593292 (MUIWrapContent_t4054684270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::RefreshItem(UnityEngine.Transform,System.Int32)
extern "C"  void MUIWrapContent_RefreshItem_m3259092755 (MUIWrapContent_t4054684270 * __this, Transform_t1659122786 * ___item0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::UpdateItem(UnityEngine.Transform,System.Int32)
extern "C"  void MUIWrapContent_UpdateItem_m2561703341 (MUIWrapContent_t4054684270 * __this, Transform_t1659122786 * ___item0, int32_t ___index1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::WrapChildPosWithRealIndex(System.Int32,UIScrollView)
extern "C"  void MUIWrapContent_WrapChildPosWithRealIndex_m2915938136 (MUIWrapContent_t4054684270 * __this, int32_t ___realIndex0, UIScrollView_t2113479878 * ___scrollView1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::RefreshChildData()
extern "C"  void MUIWrapContent_RefreshChildData_m3902397506 (MUIWrapContent_t4054684270 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::ilo_Start1(MUIWrapContent)
extern "C"  void MUIWrapContent_ilo_Start1_m268488715 (Il2CppObject * __this /* static, unused */, MUIWrapContent_t4054684270 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::ilo_UpdateItem2(MUIWrapContent,UnityEngine.Transform,System.Int32)
extern "C"  void MUIWrapContent_ilo_UpdateItem2_m647755584 (Il2CppObject * __this /* static, unused */, MUIWrapContent_t4054684270 * ____this0, Transform_t1659122786 * ___item1, int32_t ___index2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::ilo_Invoke3(MUIWrapContent/OnInitializeItem,UnityEngine.GameObject,System.Int32,System.Int32)
extern "C"  void MUIWrapContent_ilo_Invoke3_m53520962 (Il2CppObject * __this /* static, unused */, OnInitializeItem_t4000839715 * ____this0, GameObject_t3674682005 * ___go1, int32_t ___wrapIndex2, int32_t ___realIndex3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::ilo_WrapContent4(MUIWrapContent)
extern "C"  void MUIWrapContent_ilo_WrapContent4_m3631308417 (Il2CppObject * __this /* static, unused */, MUIWrapContent_t4054684270 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::ilo_ResetPositionDown5(UIScrollView)
extern "C"  void MUIWrapContent_ilo_ResetPositionDown5_m987524415 (Il2CppObject * __this /* static, unused */, UIScrollView_t2113479878 * ____this0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MUIWrapContent::ilo_IsPressed6(UnityEngine.GameObject)
extern "C"  bool MUIWrapContent_ilo_IsPressed6_m694558004 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MUIWrapContent::ilo_SetActive7(UnityEngine.GameObject,System.Boolean,System.Boolean)
extern "C"  void MUIWrapContent_ilo_SetActive7_m1774880945 (Il2CppObject * __this /* static, unused */, GameObject_t3674682005 * ___go0, bool ___state1, bool ___compatibilityMode2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
