﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GarbageiOS.M_sischo139
struct M_sischo139_t30009290;
// System.String[]
struct StringU5BU5D_t4054002952;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DCSharp_GarbageiOS_M_sischo13930009290.h"

// System.Void GarbageiOS.M_sischo139::.ctor()
extern "C"  void M_sischo139__ctor_m736255193 (M_sischo139_t30009290 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sischo139::M_tirrawwhu0(System.String[],System.Int32)
extern "C"  void M_sischo139_M_tirrawwhu0_m4105683755 (M_sischo139_t30009290 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sischo139::M_falguwou1(System.String[],System.Int32)
extern "C"  void M_sischo139_M_falguwou1_m1716931215 (M_sischo139_t30009290 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sischo139::M_stairhai2(System.String[],System.Int32)
extern "C"  void M_sischo139_M_stairhai2_m3963590535 (M_sischo139_t30009290 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sischo139::M_portelDrartra3(System.String[],System.Int32)
extern "C"  void M_sischo139_M_portelDrartra3_m4268131595 (M_sischo139_t30009290 * __this, StringU5BU5D_t4054002952* ____info0, int32_t ___i1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GarbageiOS.M_sischo139::ilo_M_falguwou11(GarbageiOS.M_sischo139,System.String[],System.Int32)
extern "C"  void M_sischo139_ilo_M_falguwou11_m2327145553 (Il2CppObject * __this /* static, unused */, M_sischo139_t30009290 * ____this0, StringU5BU5D_t4054002952* ____info1, int32_t ___i2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
