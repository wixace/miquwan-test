﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// StaticParticle
struct StaticParticle_t2206539572;

#include "codegen/il2cpp-codegen.h"

// System.Void StaticParticle::.ctor()
extern "C"  void StaticParticle__ctor_m2345568231 (StaticParticle_t2206539572 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
